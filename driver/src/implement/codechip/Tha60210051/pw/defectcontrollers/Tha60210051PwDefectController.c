/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module PW
 *
 * File        : Tha60210051PwDefectController.c
 *
 * Created Date: Sep 4, 2014
 *
 * Description : Defect controller of product 60210051
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../interruptprocessor/Tha60210051PwInterruptReg.h"
#include "Tha60210051PwDefectControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods             m_AtChannelOverride;
static tThaPwDefectControllerMethods m_ThaPwDefectControllerOverride;

/* Super implementation. */
static const tAtChannelMethods      *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwDefaultOffset(ThaPwDefectController self)
    {
    /* Someone may have better solution, the following way is to strictly
     * follow hardware RD which is subject to be changed in the near future.
     * By this way, it would be easier to make changes. See:
     * - $Grp2048ID(0-2): Pseudowire ID bits[12:11]
     * - $GrpID(0-63): Pseudowire ID bits[10:5]
     * - $BitID(0-31): Pseudowire ID bits [4:0] */
    uint32 pwId = AtChannelIdGet((AtChannel)mAdapter(self));
    uint32 grp2048Id = (pwId & cBit12_11) >> 11;
    uint32 grpId     = (pwId & cBit10_5)  >> 5;
    uint32 bitId     = (pwId & cBit4_0);
    return (grp2048Id * 8192UL) + (grpId * 32UL) + bitId;
    }

static uint32 MbitMask(ThaPwDefectController self)
    {
    return (AtPwTypeGet((AtPw)self) == cAtPwTypeCESoP) ? cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Mask : 0;
    }

static uint32 StrayMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Mask;
    }

static uint32 MalformedMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Mask;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    switch (alarmType)
        {
        /* Mbit just is supported with CESoP  */
        case cAtPwAlarmTypeMBit           :
            return (AtPwTypeGet((AtPw)self) == cAtPwTypeCESoP) ? cAtTrue : cAtFalse;
        case cAtPwAlarmTypeStrayPacket    : return cAtTrue;
        case cAtPwAlarmTypeMalformedPacket: return cAtTrue;

        default:
            return m_AtChannelMethods->AlarmIsSupported(self, alarmType);
        }
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, mMethodsGet(self), sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, PwDefaultOffset);
        mMethodOverride(m_ThaPwDefectControllerOverride, MbitMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, StrayMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, MalformedMask);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel controller = (AtChannel)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(controller), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(controller, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideThaPwDefectController(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051PwDefectController);
    }

ThaPwDefectController Tha60210051PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwDefectControllerPmcObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60210051PwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051PwDefectControllerObjectInit(newController, pw);
    }

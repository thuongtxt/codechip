/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210051PwDefectController.h
 * 
 * Created Date: Nov 4, 2015
 *
 * Description : PW defect controller interface for 60210051 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051PWDEFECTCONTROLLER_H_
#define _THA60210051PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60210051PwDefectControllerNew(AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051PWDEFECTCONTROLLER_H_ */


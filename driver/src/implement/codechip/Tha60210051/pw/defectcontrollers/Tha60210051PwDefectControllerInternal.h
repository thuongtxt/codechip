/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210051PwDefectControllerInternal.h
 * 
 * Created Date: Jul 4, 2017
 *
 * Description : Defect controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051PWDEFECTCONTROLLERINTERNAL_H_
#define _THA60210051PWDEFECTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/pw/defectcontrollers/Tha60210011PwDefectController.h"
#include "Tha60210051PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051PwDefectController
    {
    tTha60210011PwDefectControllerPmc super;
    }tTha60210051PwDefectController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60210051PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051PWDEFECTCONTROLLERINTERNAL_H_ */


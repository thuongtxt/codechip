/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210051PwEthPortBinder.c
 *
 * Created Date: Oct 26, 2015
 *
 * Description : 60210051 PW Ethernet port binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/pw/ethportbinder/ThaPwEthPortBinderInternal.h"
#include "../../../default/pw/activator/ThaPwActivator.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "Tha60210051ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051PwEthPortBinder
    {
    tThaPwEthPortBinder super;
    }tTha60210051PwEthPortBinder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwEthPortBinderMethods m_ThaPwEthPortBinderOverride;

/* Save super implementation */
static const tThaPwEthPortBinderMethods *m_ThaPwEthPortBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedToManageEthPortBandwidth(ThaPwEthPortBinder self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void EthPortSrcMacDidChange(AtEthPort ethPort, uint8* sourceMac, void* userData)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)userData;
    ThaPwHeaderController controller = ThaPwAdapterHeaderController(adapter);
    ThaPwHeaderController backupController = ThaPwAdapterBackupHeaderController(adapter);
    AtUnused(ethPort);
    if (ThaPwHeaderControllerSrcMacIsSetByUser(controller) == cAtFalse)
        ThaPwHeaderControllerEthPortSrcMacSet(controller, sourceMac);
    if (ThaPwHeaderControllerSrcMacIsSetByUser(backupController) == cAtFalse)
        ThaPwHeaderControllerEthPortSrcMacSet(backupController, sourceMac);
    }

static void SerdesPowerControlWillPowerDown(AtEthPort ethPort, void* userData)
    {
    AtUnused(ethPort);
    AtChannelEnable((AtChannel)userData, cAtFalse);
    }

static eAtRet RegisterListener(AtPw adapter, AtEthPort ethPort)
    {
    tAtEthPortPropertyListener listener;
    listener.SourceMacDidChange = EthPortSrcMacDidChange;
    listener.SerdesPowerControlWillPowerDown = SerdesPowerControlWillPowerDown;
    return ThaEthPortPropertyListenerAdd(ethPort, &listener, adapter);
    }

static eAtRet PwEthPortBind(ThaPwEthPortBinder self, AtPw adapter, AtEthPort ethPort)
    {
    eAtRet ret = m_ThaPwEthPortBinderMethods->PwEthPortBind(self, adapter, ethPort);
    if (ret != cAtOk)
        return ret;

    return RegisterListener(adapter, ethPort);
    }

static eAtRet PwEthPortUnBind(ThaPwEthPortBinder self, AtPw adapter, AtEthPort ethPort)
    {
    eAtRet ret = ThaPwEthPortBinderEthPortDisconnect(self, adapter, ethPort);
    ThaPwAdapterEthPortSet((ThaPwAdapter)adapter, NULL);

    if (ret != cAtOk)
        return ret;

    return Tha60210051PwEthPortBinderDeregisterListener(adapter, ethPort);
    }

static void OverrideThaPwEthPortBinder(ThaPwEthPortBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwEthPortBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwEthPortBinderOverride, m_ThaPwEthPortBinderMethods, sizeof(m_ThaPwEthPortBinderOverride));

        mMethodOverride(m_ThaPwEthPortBinderOverride, NeedToManageEthPortBandwidth);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwEthPortBind);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwEthPortUnBind);
        }

    mMethodsSet(self, &m_ThaPwEthPortBinderOverride);
    }

static void Override(ThaPwEthPortBinder self)
    {
    OverrideThaPwEthPortBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051PwEthPortBinder);
    }

static ThaPwEthPortBinder ObjectInit(ThaPwEthPortBinder self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwEthPortBinderObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwEthPortBinder Tha60210051PwEthPortBinderNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwEthPortBinder newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newActivator, module);
    }

eAtRet Tha60210051PwEthPortBinderDeregisterListener(AtPw pw, AtEthPort ethPort)
    {
    tAtEthPortPropertyListener listener;
    listener.SourceMacDidChange = EthPortSrcMacDidChange;
    listener.SerdesPowerControlWillPowerDown = SerdesPowerControlWillPowerDown;
    return ThaEthPortPropertyListenerRemove(ethPort, &listener, pw);
    }

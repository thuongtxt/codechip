/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210051ModulePw.c
 *
 * Created Date: Mar 8, 2021
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../Tha60210011/pda/Tha60210011ModulePda.h"
#include "../../Tha60210011/pw/defectcontrollers/Tha60210011PwDefectController.h"
#include "../../Tha60210011/pw/interruptprocessor/Tha60210011PwInterruptProcessor.h"
#include "interruptprocessor/Tha60210051PwInterruptProcessor.h"
#include "defectcontrollers/Tha60210051PwDefectController.h"
#include "Tha60210051ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods  m_AtModulePwOverride;
static tThaModulePwMethods m_ThaModulePwOverride;
static tTha60210011ModulePwMethods m_Tha60210011ModulePwOverride;

/* Save super implementation */
static const tThaModulePwMethods *m_ThaModulePwMethods = NULL;
static const tAtModulePwMethods  *m_AtModulePwMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 2048;
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 5376;
    }

static uint32 NumPwsSupportCounters(ThaModulePw self)
    {
    AtDevice device;

    if (ThaModulePwShouldReadAllCountersFromSurModule(self))
        return m_ThaModulePwMethods->NumPwsSupportCounters(self);

    device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= Tha60210011DeviceStartVersionSupportFullPwCounters(device))
        return m_ThaModulePwMethods->NumPwsSupportCounters(self);

    /* Due to HW resource limiting */
    return 4096;
    }

static ThaPwEthPortBinder PwEthPortBinderCreate(ThaModulePw self)
    {
    return Tha60210051PwEthPortBinderNew(self);
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210011PwDefectControllerSurNew(pw);
    else
        return Tha60210051PwDefectControllerNew(pw);
    }

static ThaPwDefectController DebugDefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210051PwDefectControllerNew(pw);
    else
        return Tha60210011PwDefectControllerSurNew(pw);
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210011PwInterruptProcessorSurNew(self);
    else
        return Tha60210051PwInterruptProcessorNew(self);
    }

static ThaPwInterruptProcessor DebugIntrProcessorCreate(ThaModulePw self)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210051PwInterruptProcessorNew(self);
    else
        return Tha60210011PwInterruptProcessorSurNew(self);
    }

static ThaModulePwe ModulePwe(ThaModulePw self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    }

static void PwEnablePrepare(ThaModulePw self, AtPw adapter)
    {
    if (AtModuleInAccessible((AtModule)self))
        return;

    /* Prepare enable */
    ThaPwAdapterStartCenterJitterBuffer(adapter);
    ThaModulePwePwAddingPrepare(ModulePwe(self), adapter);
    }

static void PwEnableFinish(ThaModulePw self, AtPw adapter)
    {
    if (AtModuleInAccessible((AtModule)self))
        return;

    /* Start PWE adding */
    ThaModulePwePwAddingStart(ModulePwe(self), adapter);
    ThaPwAdapterStopCenterJitterBuffer(adapter);
    }

static eAtRet DeletePw(AtModulePw self, uint32 pwId)
    {
    AtPw pw = (AtPw)ThaPwAdapterGet(AtModulePwGetPw(self, pwId));
    AtEthPort boundEthPort;
    
    if (pw == NULL)
        return cAtOk;

    boundEthPort = AtPwEthPortGet(pw);
    if (boundEthPort)
        Tha60210051PwEthPortBinderDeregisterListener(pw, boundEthPort);

    /* Let super do other things */
    return m_AtModulePwMethods->DeletePw(self, pwId);
    }

static uint32 DefaultDefectModule(ThaModulePw self)
    {
    AtUnused(self);
    return cAtModulePw;
    }

static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210011ModulePw self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x5, 0x1051);
    }

static void OverrideTha60210011ModulePw(AtModulePw self)
    {
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePwOverride, mMethodsGet(pwModule), sizeof(m_Tha60210011ModulePwOverride));

        mMethodOverride(m_Tha60210011ModulePwOverride, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(pwModule, &m_Tha60210011ModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, DeletePw);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(pwModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(tThaModulePwMethods));

        mMethodOverride(m_ThaModulePwOverride, NumPwsSupportCounters);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortBinderCreate);
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, InterruptProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, PwEnablePrepare);
        mMethodOverride(m_ThaModulePwOverride, PwEnableFinish);
        mMethodOverride(m_ThaModulePwOverride, DebugDefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, DefaultDefectModule);
        mMethodOverride(m_ThaModulePwOverride, DebugIntrProcessorCreate);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60210011ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModulePw);
    }

AtModulePw Tha60210051ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60210051ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModulePwObjectInit(newModule, device);
    }

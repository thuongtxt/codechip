/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210051PwInterruptProcessor.c
 *
 * Created Date: Oct 30, 2015
 *
 * Description : Interrupt processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../Tha60210011/pw/interruptprocessor/Tha60210011PwInterruptProcessor.h"
#include "Tha60210051PwInterruptProcessor.h"
#include "Tha60210051PwInterruptReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cPw_slice_intr_Mask(sl)     ((cBit0) << (sl))
#define cPw_grp_intr_Mask(grp)      ((cBit0) << (grp))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051PwInterruptProcessor
    {
    tTha60210011PwInterruptProcessorPmc super;
    } tTha60210051PwInterruptProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwInterruptProcessorMethods m_ThaPwInterruptProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Process(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    AtModulePw module = (AtModulePw)ThaPwInterruptProcessorModuleGet(self);
    const uint32 baseAddress = mMethodsGet(self)->BaseAddress(self);
    static const uint32 cNumberPwSlice  = 6;
    uint32 intrSta = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_slice_intr_or_stat);
    uint32 slice;
    AtUnused(glbIntr);

    for (slice = 0; slice < cNumberPwSlice; slice++)
        {
        if (intrSta & cPw_slice_intr_Mask(slice))
            {
            uint32 subSlice = slice & 0x1;
            uint32 grp2048 = slice >> 1;
            uint32 intrMask = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(grp2048, subSlice));
            uint32 intrStat = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_or_stat(grp2048, subSlice));
            uint32 grpid;

            intrStat &= intrMask;

            for (grpid = 0; grpid < 32; grpid++)
                {
                if (intrStat & cPw_grp_intr_Mask(grpid))
                    {
                    uint32 intrStat32 = AtHalRead(hal, baseAddress + cAf6Reg_Counter_Interrupt_OR_Status(grp2048, subSlice, grpid));
                    uint32 subid;

                    for (subid = 0; subid < 32; subid++)
                        {
                        if (intrStat32 & cPw_grp_intr_Mask(subid))
                            {
                            uint32 pwId = (slice << 10) + (grpid << 5) + subid;
                            uint32 pwOffset = (grp2048 << 13) + (subSlice << 10) + (grpid << 5) + subid;

                            /* Get PW channel */
                            AtPw pwChannel = AtModulePwGetPw(module, pwId);

                            /* Process channel interrupt. */
                            AtPwInterruptProcess(pwChannel, pwOffset, hal);
                            }
                        }
                    }
                }
            }
        }
    }

static eAtRet Enable(ThaPwInterruptProcessor self, eBool enable)
    {
    const uint32 baseAddress = ThaPwInterruptProcessorBaseAddress(self);
    AtDevice device = AtModuleDeviceGet((AtModule)ThaPwInterruptProcessorModuleGet(self));
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 grp2048, slice;

    for (grp2048 = 0; grp2048 < 3; grp2048++)
        {
        for (slice = 0; slice < 2; slice++)
            {
            uint32 regAddr = baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(grp2048, slice);
            uint32 regVal  = (enable) ? cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask : 0x0;
            AtHalWrite(hal, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static uint32 CurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Current_Status_Base;
    }

static uint32 InterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base;
    }

static uint32 InterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base;
    }

static void OverrideThaPwInterruptProcessor(ThaPwInterruptProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwInterruptProcessorOverride, mMethodsGet(self), sizeof(m_ThaPwInterruptProcessorOverride));

        mMethodOverride(m_ThaPwInterruptProcessorOverride, Process);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Enable);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptMaskRegister);
        }

    mMethodsSet(self, &m_ThaPwInterruptProcessorOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051PwInterruptProcessor);
    }

static ThaPwInterruptProcessor ObjectInit(ThaPwInterruptProcessor self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwInterruptProcessorPmcObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    OverrideThaPwInterruptProcessor(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwInterruptProcessor Tha60210051PwInterruptProcessorNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwInterruptProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, module);
    }

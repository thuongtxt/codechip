/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_INTALM_H_
#define _AF6_REG_AF6CCI0011_RD_INTALM_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Interrupt Enable Control
Reg Addr   : 0x060000
Reg Formula: 0x060000 + Grp2048ID*8192 + GrpID*32 + BitID
    Where  : 
           + $Grp2048ID(0-2):  Pseudowire ID bits[12:11]
           + $GrpID(0-63):  Pseudowire ID bits[10:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base                                       0x060000
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control(Grp2048ID, GrpID, BitID)(0x060000+(Grp2048ID)*8192+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_WidthVal                                         32
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_WriteMask                                       0x0

/*--------------------------------------
BitField Name: StrayStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Stray packet event to generate an interrupt
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Bit_End                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Mask                                   cBit8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Shift                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MalformStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Malform packet event to generate an interrupt
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Bit_Start                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Mask                                   cBit7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Shift                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Mbit packet event to generate an interrupt
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Bit_Start                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Bit_End                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Mask                                   cBit6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Shift                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: RbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Rbit packet event to generate an interrupt
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Bit_Start                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Bit_End                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Mask                                   cBit5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Shift                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsSyncStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Lost of frame Sync event to generate an interrupt
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Mask                                   cBit4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Shift                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change jitter buffer state event from normal to
underrun and vice versa in the related pseudowire to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Mask                                   cBit3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Shift                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change jitter buffer state event from normal to
overrun and vice versa in the related pseudowire to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Bit_Start                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Mask                                   cBit2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Shift                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change lost of frame state(LOFS) event from
normal to LOFS and vice versa in the related pseudowire to generate an
interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Bit_Start                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Bit_End                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Mask                                   cBit1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Shift                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Lbit packet event to generate an interrupt
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Bit_Start                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Bit_End                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Mask                                   cBit0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Shift                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Interrupt Status
Reg Addr   : 0x060800
Reg Formula: 0x060800 + Grp2048ID*8192 +  GrpID*32 + BitID
    Where  : 
           + $Grp2048ID(0-2):  Pseudowire ID bits[12:11]
           + $GrpID(0-63):  Pseudowire ID bits[10:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base                                               0x060800
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status(Grp2048ID, GrpID, BitID)(0x060800+(Grp2048ID)*8192+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_WidthVal                                                 32
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_WriteMask                                               0x0

/*--------------------------------------
BitField Name: StrayStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when Stray packet event is detected in the pseudowire
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Bit_End                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Mask                                   cBit8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Shift                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: MalformStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when Malform packet event is detected in the pseudowire
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Bit_Start                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Mask                                   cBit7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Shift                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: MbitStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a Mbit packet event is detected in the pseudowire
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Bit_Start                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Bit_End                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Mask                                   cBit6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Shift                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: RbitStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a Rbit packet event is detected in the pseudowire
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Bit_Start                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Bit_End                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Mask                                   cBit5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Shift                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsSyncStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a lost of frame Sync event is detected in the
pseudowire
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Mask                                   cBit4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Shift                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when there is a change in jitter buffer underrun state  in
the related pseudowire
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Mask                                   cBit3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Shift                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when there is a change in jitter buffer overrun state in
the related pseudowire
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Bit_Start                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Mask                                   cBit2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Shift                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when there is a change in lost of frame state(LOFS) in the
related pseudowire
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Bit_Start                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Bit_End                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Mask                                   cBit1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Shift                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a Lbit packet event is detected in the pseudowire
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Bit_Start                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Bit_End                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Mask                                   cBit0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Shift                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Current Status
Reg Addr   : 0x061000
Reg Formula: 0x061000 + Grp2048ID*8192 +  GrpID*32 + BitID
    Where  : 
           + $Grp2048ID(0-2):  Pseudowire ID bits[12:11]
           + $GrpID(0-63):  Pseudowire ID bits[10:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Current_Status_Base                                                 0x061000
#define cAf6Reg_Counter_Per_Alarm_Current_Status(Grp2048ID, GrpID, BitID)(0x061000+(Grp2048ID)*8192+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Current_Status_WidthVal                                                   32
#define cAf6Reg_Counter_Per_Alarm_Current_Status_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: StrayCurStatus
BitField Type: RW
BitField Desc: Stray packet state current status in the related pseudowire.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Bit_End                                         8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Mask                                        cBit8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Shift                                           8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_MaxVal                                        0x1
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_MinVal                                        0x0
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: MalformCurStatus
BitField Type: RW
BitField Desc: Malform packet state current status in the related pseudowire.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Bit_Start                                       7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Mask                                      cBit7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Shift                                         7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_MaxVal                                      0x1
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_MinVal                                      0x0
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_RstVal                                      0x0

/*--------------------------------------
BitField Name: MbitCurStatus
BitField Type: RW
BitField Desc: Mbit packet state current status in the related pseudowire.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Bit_Start                                        6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Bit_End                                          6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Mask                                         cBit6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Shift                                            6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_MaxVal                                         0x1
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_MinVal                                         0x0
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: RbitCurStatus
BitField Type: RW
BitField Desc: Rbit packet state current status in the related pseudowire.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Bit_Start                                        5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Bit_End                                          5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Mask                                         cBit5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Shift                                            5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_MaxVal                                         0x1
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_MinVal                                         0x0
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: LofsSyncCurStatus
BitField Type: RW
BitField Desc: Lost of frame Sync state current status in the related
pseudowire.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Mask                                     cBit4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Shift                                        4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunCurStatus
BitField Type: RW
BitField Desc: Jitter buffer underrun current status in the related pseudowire.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Mask                                     cBit3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Shift                                        3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunCurStatus
BitField Type: RW
BitField Desc: Jitter buffer overrun current status in the related pseudowire.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Bit_Start                                       2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Mask                                      cBit2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Shift                                         2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_MaxVal                                      0x1
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_MinVal                                      0x0
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_RstVal                                      0x0

/*--------------------------------------
BitField Name: LofsStateCurStatus
BitField Type: RW
BitField Desc: Lost of frame state current status in the related pseudowire.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Bit_Start                                       1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Bit_End                                       1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Mask                                    cBit1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Shift                                       1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateCurStatus
BitField Type: RW
BitField Desc: a Lbit packet state current status in the related pseudowire
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Bit_Start                                       0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Bit_End                                       0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Mask                                    cBit0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Shift                                       0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Interrupt OR Status
Reg Addr   : 0x061800
Reg Formula: 0x061800 + Grp2048ID*8192 +  Slice*1024 + GrpID
    Where  : 
           + $Grp2048ID(0-2):  Pseudowire ID bits[12:11]
           + $Slice(0-1):  Pseudowire ID bits[10]
           + $GrpID(0-31):  Pseudowire ID bits[9:5]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Interrupt_OR_Status_Base                                                      0x061800
#define cAf6Reg_Counter_Interrupt_OR_Status(Grp2048ID, Slice, GrpID)  (0x061800+(Grp2048ID)*8192+(Slice)*1024+(GrpID))
#define cAf6Reg_Counter_Interrupt_OR_Status_WidthVal                                                        32
#define cAf6Reg_Counter_Interrupt_OR_Status_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: IntrORStatus
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the Counter per Alarm Interrupt Status register of the related pseudowires to be
set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0,
bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32,
respectively.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Bit_Start                                              0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Bit_End                                               31
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Mask                                            cBit31_0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Shift                                                  0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_MaxVal                                        0xffffffff
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_MinVal                                               0x0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt OR Status
Reg Addr   : 0x061BFF
Reg Formula: 0x061BFF + Grp2048ID*8192 +  Slice*1024
    Where  : 
           + $Grp2048ID(0-2):  Pseudowire ID bits[12:11]
           + $Slice(0-1):  Pseudowire ID bits[10]
Reg Desc   : 
The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_or_stat_Base                                                   0x061BFF
#define cAf6Reg_counter_per_group_intr_or_stat(Grp2048ID, Slice)     (0x061BFF +(Grp2048ID)*8192+(Slice)*1024)
#define cAf6Reg_counter_per_group_intr_or_stat_WidthVal                                                     32
#define cAf6Reg_counter_per_group_intr_or_stat_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Group is set and
its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Bit_Start                                         0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Bit_End                                          31
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Mask                                       cBit31_0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Shift                                             0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_MaxVal                                   0xffffffff
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_MinVal                                          0x0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt Enable Control
Reg Addr   : 0x061BFE
Reg Formula: 0x061BFE + Grp2048ID*8192 +  Slice*1024
    Where  : 
           + $Grp2048ID(0-2):  Pseudowire ID bits[12:11]
           + $Slice(0-1):  Pseudowire ID bits[10]
Reg Desc   : 
The register consists of 8 interrupt enable bits for 8 group in the PW counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_en_ctrl_Base                                                   0x061BFE
#define cAf6Reg_counter_per_group_intr_en_ctrl(Grp2048ID, Slice)     (0x061BFE +(Grp2048ID)*8192+(Slice)*1024)
#define cAf6Reg_counter_per_group_intr_en_ctrl_WidthVal                                                     32
#define cAf6Reg_counter_per_group_intr_en_ctrl_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Group to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Bit_Start                                            0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Bit_End                                             31
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask                                          cBit31_0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Shift                                                0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_MaxVal                                      0xffffffff
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_MinVal                                             0x0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Slice Interrupt OR Status
Reg Addr   : 0x068000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 6 bits for 6 Slice of the PW Counter. Each bit is used to store Interrupt OR status of the related 1024 PWs Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_slice_intr_or_stat_Base                                                   0x068000
#define cAf6Reg_counter_per_slice_intr_or_stat                                                        0x068000
#define cAf6Reg_counter_per_slice_intr_or_stat_WidthVal                                                     32
#define cAf6Reg_counter_per_slice_intr_or_stat_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Slice is set and
its interrupt is enabled
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Bit_Start                                         0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Bit_End                                           5
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Mask                                        cBit5_0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Shift                                             0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_MaxVal                                         0x3f
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_MinVal                                          0x0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_RstVal                                          0x0

#endif /* _AF6_REG_AF6CCI0011_RD_INTALM_H_ */

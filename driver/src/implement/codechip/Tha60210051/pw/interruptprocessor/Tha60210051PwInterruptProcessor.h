/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210051PwInterruptProcessor.h
 * 
 * Created Date: Oct 30, 2015
 *
 * Description : PW interrupt processor for 60210051 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051PWINTERRUPTPROCESSOR_H_
#define _THA60210051PWINTERRUPTPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* 10G 8024 PWEs PMC. */
ThaPwInterruptProcessor Tha60210051PwInterruptProcessorNew(ThaModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051PWINTERRUPTPROCESSOR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210051ModulePwInternal.h
 * 
 * Created Date: Oct 8, 2015
 *
 * Description : PW module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEPWINTERNAL_H_
#define _THA60210051MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pw/Tha60210011ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModulePw
    {
    tTha60210011ModulePw super;
    }tTha60210051ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60210051ModulePwObjectInit(AtModulePw self, AtDevice device);
eAtRet Tha60210051PwEthPortBinderDeregisterListener(AtPw pw, AtEthPort ethPort);
AtModulePw Tha60210051ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEPWINTERNAL_H_ */


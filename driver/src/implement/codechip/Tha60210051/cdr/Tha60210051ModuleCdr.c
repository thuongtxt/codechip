/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210051ModuleCdr.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module CDR of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/util/AtLongRegisterAccess.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../man/Tha60210051Device.h"
#include "Tha60210051ModuleCdrInternal.h"
#include "Tha60210051ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#define cLoNumHoldRegister       3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051ModuleCdr)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtObjectMethods     m_AtObjectOverride;
static tThaModuleCdrMethods m_ThaModuleCdrOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *HoHoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x330000, 0x330001, 0x330002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess HoLongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = HoHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint32 *LoHoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters, uint8 loSlice)
    {
    static uint32 holdRegisters[] = {0xC30000, 0xC30001, 0xC30002,
                                     0xC70000, 0xC70001, 0xC70002,
                                     0xCB0000, 0xCB0001, 0xCB0002,
                                     0xCF0000, 0xCF0001, 0xCF0002,
                                     0xD30000, 0xD30001, 0xD30002,
                                     0xD70000, 0xD70001, 0xD70002,
                                     0xDB0000, 0xDB0001, 0xDB0002,
                                     0xDF0000, 0xDF0001, 0xDF0002,
                                     0xE30000, 0xE30001, 0xE30002,
                                     0xE70000, 0xE70001, 0xE70002,
                                     0xEB0000, 0xEB0001, 0xEB0002,
                                     0xEF0000, 0xEF0001, 0xEF0002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = cLoNumHoldRegister;

    return holdRegisters + loSlice * cLoNumHoldRegister;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    return HoHoldRegistersGet(self, numberOfHoldRegisters);
    }

static AtLongRegisterAccess LoLongRegisterAccessCreate(AtModule self, uint8 loIndex)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = LoHoldRegistersGet(self, &numHoldRegisters, loIndex);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess HoLongRegisterAccess(AtModule self)
    {
    if (mThis(self)->hoLongRegisterAccess)
        return mThis(self)->hoLongRegisterAccess;

    mThis(self)->hoLongRegisterAccess = HoLongRegisterAccessCreate(self);
    return mThis(self)->hoLongRegisterAccess;
    }

static AtLongRegisterAccess* AllLoLongRegisterAccessCreate(AtModule self)
    {
    uint32 numLoSlice = Tha60210011ModuleCdrMaxNumLoSlices((ThaModuleCdr)self);
    uint32 memSize = numLoSlice * sizeof(AtLongRegisterAccess);
    AtLongRegisterAccess* allLoLongRegAccess = AtOsalMemAlloc(memSize);
    if (allLoLongRegAccess)
        {
        AtOsalMemInit(allLoLongRegAccess, 0, memSize);
        mThis(self)->numLoSlice = numLoSlice;
        }

    return allLoLongRegAccess;
    }

static AtLongRegisterAccess LoLongRegisterAccess(AtModule self, uint8 loIndex)
    {
    if (mThis(self)->loLongRegisterAccess == NULL)
        mThis(self)->loLongRegisterAccess = AllLoLongRegisterAccessCreate(self);

    if (mThis(self)->loLongRegisterAccess == NULL)
        return NULL;

    if (mThis(self)->loLongRegisterAccess[loIndex])
        return mThis(self)->loLongRegisterAccess[loIndex];

    mThis(self)->loLongRegisterAccess[loIndex] = LoLongRegisterAccessCreate(self, loIndex);
    return mThis(self)->loLongRegisterAccess[loIndex];
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    uint8 loSlice;

    if (Tha60210051DeviceUseGlobalHold((Tha60210051Device)AtModuleDeviceGet(self)))
        return m_AtModuleMethods->LongRegisterAccess(self, localAddress);

    if (Tha60210051ModuleCdrAddressBelongToHoPart(self, localAddress, &loSlice))
        return HoLongRegisterAccess(self);

    return LoLongRegisterAccess(self, loSlice);
    }

static void DeleteAllLoLongRegisterAccess(Tha60210051ModuleCdr self)
    {
    uint32 slice_i;
    if (self->loLongRegisterAccess == NULL)
        return;

    for (slice_i = 0; slice_i < self->numLoSlice; slice_i++)
        AtObjectDelete((AtObject)(mThis(self)->loLongRegisterAccess[slice_i]));

    AtOsalMemFree(self->loLongRegisterAccess);
    self->loLongRegisterAccess = NULL;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->hoLongRegisterAccess));
    mThis(self)->hoLongRegisterAccess = NULL;
    DeleteAllLoLongRegisterAccess(mThis(self));

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210051ModuleCdr *object = mThis(self);
    uint32 slice_i;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(hoLongRegisterAccess);

    for (slice_i = 0; slice_i < mThis(self)->numLoSlice; slice_i++)
        mEncodeObject(loLongRegisterAccess[slice_i]);
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    AtUnused(self);
    return Tha60210051CdrDebuggerNew();
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, DebuggerObjectCreate);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    OverrideThaModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleCdr);
    }

AtModule Tha60210051ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210051ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModuleCdrObjectInit(newModule, device);
    }

eBool Tha60210051ModuleCdrAddressBelongToHoPart(AtModule self, uint32 localAddress, uint8 *loSlice)
    {
    uint32 baseAddress = localAddress & cBit23_20;
    uint32 numLoSlice = Tha60210011ModuleCdrMaxNumLoSlices((ThaModuleCdr)self);
    
    if (baseAddress == 0x300000)
        return cAtTrue;

    baseAddress = localAddress & cBit23_16;
    *loSlice = (uint8)(((baseAddress - 0xC00000) >> 16) / 4);
    if (*loSlice >= numLoSlice) /* Out of range, force to use hi-order */
        return cAtTrue;

    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      :  
 * 
 * File        : Tha60210051ModuleCdrInternal.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Author: phuongnm
 *
 * Description : To Do
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
 
#ifndef _THA60210051MODULECDRINTERNAL_H_
#define _THA60210051MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/cdr/Tha60210011ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModuleCdr
    {
    tTha60210011ModuleCdr super;

    AtLongRegisterAccess  hoLongRegisterAccess;
    AtLongRegisterAccess* loLongRegisterAccess;
    uint32 numLoSlice;
    }tTha60210051ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210051ModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210051MODULECDRINTERNAL_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210051Tfi5LineVcInternal.h
 * 
 * Created Date: Dec 30, 2015
 *
 * Description : TFI5 line AU VC header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051TFI5LINEVCINTERNAL_H_
#define _THA60210051TFI5LINEVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAuVcInternal.h"
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineVc1xInternal.h"
#include "Tha60210051Tfi5LineVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051Tfi5LineAuVc
    {
    tTha60210011Tfi5LineAuVc super;
    }tTha60210051Tfi5LineAuVc;

typedef struct tTha60210051Tfi5LineVc1x
    {
    tTha60210011Tfi5LineVc1x super;
    }tTha60210051Tfi5LineVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210051Tfi5LineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210051Tfi5LineVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051TFI5LINEVCINTERNAL_H_ */


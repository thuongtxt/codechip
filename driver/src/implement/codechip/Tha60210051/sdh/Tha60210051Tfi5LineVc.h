/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210051Tfi5LineAuVcInternal.h
 * 
 * Created Date: Mar 8, 2021
 *
 * Description : TFI5 line AU VC header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051TFI5LINEVC_H_
#define _THA60210051TFI5LINEVC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210051Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210051Tfi5LineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A000010Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051TFI5LINEVC_H_ */


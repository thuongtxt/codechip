/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210051ModuleSdh.c
 *
 * Created Date: Mar 8, 2021
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60210051ModuleSdhInternal.h"
#include "Tha60210051Tfi5LineVc.h"
#include "../physical/Tha60210051Physical.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210051ModuleSdh)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods;
static const tTha60210011ModuleSdhMethods *m_Tha60210011ModuleSdhMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumHoLines(Tha60210011ModuleSdh self)
    {
    return AtModuleSdhNumTfi5Lines((AtModuleSdh)self);
    }

static uint8 MaxNumLoLines(Tha60210011ModuleSdh self)
    {
    return AtModuleSdhNumTfi5Lines((AtModuleSdh)self);
    }

static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210051Tfi5SerdesControllerNew(line, serdesId);
    }

static AtSdhChannel AuVcObjectCreate(Tha60210011ModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    AtUnused(lineId);
    return (AtSdhChannel)Tha60210051Tfi5LineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static eBool UseStaticXc(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 DefaultVc1xSubMapping(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhVcMapTypeVc1xMapC1x;
    }

static uint32 SerdesDrpBaseAddress(AtModuleSdh self)
    {
    AtUnused(self);
    return 0xf41000;
    }

static uint32 DrpPortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);
    AtUnused(selectedPortId);
    return 0x200;
    }

static tAtDrpAddressCalculator *SerdesDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator calculator;
    static tAtDrpAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(tAtDrpAddressCalculator));
    calculator.PortOffset = DrpPortOffset;
    pCalculator = &calculator;

    return pCalculator;
    }

static AtDrp SerdesDrpCreate(AtModuleSdh self, uint32 serdesId)
    {
    AtDrp drp = m_AtModuleSdhMethods->SerdesDrpCreate(self, serdesId);
    AtDrpAddressCalculatorSet(drp, SerdesDrpAddressCalculator());
    return drp;
    }

static eBool SerdesHasDrp(AtModuleSdh self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210051Tfi5LineVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(moduleSdh), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, DefaultVc1xSubMapping);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, UseStaticXc);
        }

    mMethodsSet(moduleSdh, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        mMethodOverride(m_AtModuleSdhOverride, SerdesHasDrp);
        mMethodOverride(m_AtModuleSdhOverride, SerdesDrpCreate);
        mMethodOverride(m_AtModuleSdhOverride, SerdesDrpBaseAddress);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleSdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, m_Tha60210011ModuleSdhMethods, sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumHoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumLoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, AuVcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Vc1xObjectCreate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleSdh);
    }

AtModuleSdh Tha60210051ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60210051ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModuleSdhObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210051ModuleSdh.h
 * 
 * Created Date: Sep 30, 2015
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULESDH_H_
#define _THA60210051MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModuleSdh * Tha60210051ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULESDH_H_ */


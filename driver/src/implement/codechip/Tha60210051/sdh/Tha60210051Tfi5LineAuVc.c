/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210051Tfi5LineAuVc.c
 *
 * Created Date: Mar 8, 2021
 *
 * Description : AU-VC of product 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../Tha60210011/xc/Tha60210011ModuleXc.h"
#include "Tha60210051Tfi5LineVcInternal.h"
#include "Tha60210051ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods               m_AtChannelOverride;
static tAtSdhChannelMethods            m_AtSdhChannelOverride;
static tTha60210011Tfi5LineAuVcMethods m_Tha60210011Tfi5LineAuVcOverride;

/* Save super implementation */
static const tAtChannelMethods               *m_AtChannelMethods               = NULL;
static const tAtSdhChannelMethods            *m_AtSdhChannelMethods            = NULL;
static const tTha60210011Tfi5LineAuVcMethods *m_Tha60210011Tfi5LineAuVcMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    if (ThaModuleSdhUseStaticXc((ThaModuleSdh)AtChannelModuleGet((AtChannel)self)))
        return Tha60210011Tfi5LineAuVcMapTypeSet(self, mapType, AtSdhChannelAllHwSts(self), AtSdhChannelNumSts(self));

    return m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    }

static eAtRet LoLineResourceConnect(Tha60210011Tfi5LineAuVc self, uint16* selectedLoLineSts)
    {
    uint8 loSlice, loHwSts, sts_i, swSts;
    eAtRet ret = cAtOk;
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!ThaModuleSdhUseStaticXc((ThaModuleSdh)AtChannelModuleGet((AtChannel)self)))
        return m_Tha60210011Tfi5LineAuVcMethods->LoLineResourceConnect(self, selectedLoLineSts);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(channel); sts_i++)
        {
		loSlice = (uint8)(selectedLoLineSts[sts_i] >> 8);
		loHwSts = (uint8)((selectedLoLineSts[sts_i] & cBit7_0));

        swSts = (uint8)(AtSdhChannelSts1Get(channel) + sts_i);
        ret = Tha60210011ModuleXcConnectSdhChannelToLoSts(channel, swSts, loSlice, loHwSts);
        }

    return ret;
    }

static eBool NeedManageLoLineSts(Tha60210011Tfi5LineAuVc self)
	{
	AtUnused(self);
	return ThaModuleSdhUseStaticXc((ThaModuleSdh)AtChannelModuleGet((AtChannel)self)) ? cAtFalse : cAtTrue;
	}

static ThaModuleOcn OcnModule(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static eAtRet MoveRxForcingPointBehindCrossConnect(ThaModuleOcn ocnModule, AtSdhAu au)
    {
    eAtRet ret = cAtOk;

    ret |= Tha60210011Tfi5LineAuPiAlarmUnForce(au, cAtSdhPathAlarmAis);
    ret |= ThaModuleOcnPathAisRxForcingPointMove(ocnModule, au, cAtTrue);

    return ret;
    }

static eAtRet RevertRxForcingPoint(ThaModuleOcn ocnModule, AtSdhAu au)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleOcnPathAisRxForcingPointMove(ocnModule, au, cAtFalse);
    ret |= Tha60210011Tfi5LineAuPiAlarmForce(au, cAtSdhPathAlarmAis);

    return ret;
    }

static eAtRet HandleRxAisForcingOnLoopback(AtChannel self, uint8 loopbackMode)
    {
    ThaModuleOcn ocnModule = OcnModule(self);
    AtSdhAu au;

    au = (AtSdhAu)AtSdhChannelParentChannelGet((AtSdhChannel)self);
    if (loopbackMode == cAtLoopbackModeRemote)
        {
        if (Tha60210011Tfi5LineAuPiForcedAlarmGet(au) & cAtSdhPathAlarmAis)
            return MoveRxForcingPointBehindCrossConnect(ocnModule, au);
        return cAtOk;
        }

    if (ThaModuleOcnPathAisRxForcingPointIsMoved(ocnModule, au))
        return RevertRxForcingPoint(ocnModule, au);

    return cAtOk;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;

    /* Let super deal with loopback by using cross connect */
    ret = m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    if (ret != cAtOk)
        return ret;

    /* The AIS RX forcing point may need to be moved behind cross-connect */
    if (ThaModuleOcnCanMovePathAisRxForcingPoint(OcnModule(self)))
        return HandleRxAisForcingOnLoopback(self, loopbackMode);

    return ret;
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideTha60210011Tfi5LineAuVc(AtSdhVc self)
    {
    Tha60210011Tfi5LineAuVc channel = (Tha60210011Tfi5LineAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011Tfi5LineAuVcMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineAuVcOverride, mMethodsGet(channel), sizeof(m_Tha60210011Tfi5LineAuVcOverride));

        mMethodOverride(m_Tha60210011Tfi5LineAuVcOverride, LoLineResourceConnect);
        mMethodOverride(m_Tha60210011Tfi5LineAuVcOverride, NeedManageLoLineSts);
        }

    mMethodsSet(channel, &m_Tha60210011Tfi5LineAuVcOverride);
    }


static void Override(AtSdhVc self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideTha60210011Tfi5LineAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051Tfi5LineAuVc);
    }

AtSdhVc Tha60210051Tfi5LineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210051Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051Tfi5LineAuVcObjectInit(self, channelId, channelType, module);
    }

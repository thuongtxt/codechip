/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60210051ModuleClock.h
 * 
 * Created Date: Jun 9, 2016
 *
 * Description : Interface of the 60210051 Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULECLOCK_H_
#define _THA60210051MODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60210051ModuleClockNew(AtDevice device);
eBool Tha60210051ModuleClockTfi5SerdesCdrLockIsFailed(AtModuleClock self, uint32 serdesId, eBool r2c);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULECLOCK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60210051ModuleClockInternal.h
 * 
 * Created Date: Oct 6, 2016
 *
 * Description : Tha60210051ModuleClockInternal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULECLOCKINTERNAL_H_
#define _THA60210051MODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/clock/Tha60210011ModuleClockInternal.h"
#include "Tha60210051ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModuleClock
    {
    tTha60210011ModuleClock super;
    }tTha60210051ModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60210051ModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULECLOCKINTERNAL_H_ */


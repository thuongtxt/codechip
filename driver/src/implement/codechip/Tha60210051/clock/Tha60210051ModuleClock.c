/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock Module Name
 *
 * File        : Tha60210051ModuleClock.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : Implement clock features such as monitoring input clocks for the device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051ModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6RegTfi5SerdesClockStatus 0xF00052  /* Sticky write 1 to clear */
#define cAf6RegTfi5SerdesPllGlobalMask cBit24
#define cAf6RegTfi5SerdesPllGlobalShift 24
#define cAf6RegTfi5SerdesRxCdrLockedMask(portId) (cBit28 << (portId))
#define cAf6RegTfi5SerdesRxCdrLockedShift(portId) (28 + (portId))
#define cAf6RegTfi5SerdesRxCdrLockedValue 0
#define cAf6RegTfi5SerdesRxCdrUnLockedValue 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleClockMethods  m_AtModuleClockOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SdhSerdesStatusIsSupported(AtDevice device)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersion(versionReader);
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x1006);

    if (hwVersion >= startVersion)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AllClockCheck(AtModuleClock self)
    {
    uint32 address, value, retVal;

    if (SdhSerdesStatusIsSupported(AtModuleDeviceGet((AtModule)self)))
        return 0;

    address = cAf6RegTfi5SerdesClockStatus;
    value = mModuleHwRead(self, address);
    retVal = (value & cAf6RegTfi5SerdesPllGlobalMask);

    if (retVal != 0)
        {
        mModuleHwWrite(self, address, retVal);
        return cAtModuleClockAlarmSdhSerdesPllFail;
        }

    return 0;
    }

static eBool Tfi5SerdesCdrLockIsFailed(AtModuleClock self, uint32 serdesId, eBool r2c)
    {
    uint32 address, mask, value, retVal;

    if (SdhSerdesStatusIsSupported(AtModuleDeviceGet((AtModule)self)))
        return cAtFalse;

    address = cAf6RegTfi5SerdesClockStatus;
    mask    = cAf6RegTfi5SerdesRxCdrLockedMask(serdesId);
    value   = mModuleHwRead(self, address);
    retVal  = (value & mask);

    if ((r2c == cAtTrue) && (retVal != 0))
        mModuleHwWrite(self, address, retVal);

    return retVal != 0 ? cAtTrue : cAtFalse;
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, AllClockCheck);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModuleClock(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleClock);
    }

AtModuleClock Tha60210051ModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60210051ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModuleClockObjectInit(newModule, device);
    }

eBool Tha60210051ModuleClockTfi5SerdesCdrLockIsFailed(AtModuleClock self, uint32 serdesId, eBool r2c)
    {
    if (self)
        return Tfi5SerdesCdrLockIsFailed(self, serdesId, r2c);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Serder
 * 
 * File        : Tha60210051EthPortSerdesReg.h
 * 
 * Created Date: Dec 10, 2015
 *
 * Description : Ethernet port serdes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051ETHPORTSERDESREG_H_
#define _THA60210051ETHPORTSERDESREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Top XFI-Port Control 4
Reg Addr   : 0xF0_0044
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_top_o_control4_Base                                                                   0xF00044
#define cAf6Reg_top_o_control4                                                                        0xF00044
#define cAf6Reg_top_o_control4_WidthVal                                                                     32
#define cAf6Reg_top_o_control4_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: p6_txpd
BitField Type: RW
BitField Desc: XFI Port6 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_top_o_control4_p6_txpd_Bit_Start                                                               30
#define cAf6_top_o_control4_p6_txpd_Bit_End                                                                 31
#define cAf6_top_o_control4_p6_txpd_Mask                                                             cBit31_30
#define cAf6_top_o_control4_p6_txpd_Shift                                                                   30
#define cAf6_top_o_control4_p6_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p6_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p6_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p6_rxpd
BitField Type: RW
BitField Desc: XFI Port6 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_top_o_control4_p6_rxpd_Bit_Start                                                               28
#define cAf6_top_o_control4_p6_rxpd_Bit_End                                                                 29
#define cAf6_top_o_control4_p6_rxpd_Mask                                                             cBit29_28
#define cAf6_top_o_control4_p6_rxpd_Shift                                                                   28
#define cAf6_top_o_control4_p6_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p6_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p6_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p5_txpd
BitField Type: RW
BitField Desc: XFI Port5 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_top_o_control4_p5_txpd_Bit_Start                                                               26
#define cAf6_top_o_control4_p5_txpd_Bit_End                                                                 27
#define cAf6_top_o_control4_p5_txpd_Mask                                                             cBit27_26
#define cAf6_top_o_control4_p5_txpd_Shift                                                                   26
#define cAf6_top_o_control4_p5_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p5_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p5_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p5_rxpd
BitField Type: RW
BitField Desc: XFI Port5 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [25:24]
--------------------------------------*/
#define cAf6_top_o_control4_p5_rxpd_Bit_Start                                                               24
#define cAf6_top_o_control4_p5_rxpd_Bit_End                                                                 25
#define cAf6_top_o_control4_p5_rxpd_Mask                                                             cBit25_24
#define cAf6_top_o_control4_p5_rxpd_Shift                                                                   24
#define cAf6_top_o_control4_p5_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p5_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p5_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p8_txpd
BitField Type: RW
BitField Desc: XFI Port8 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_top_o_control4_p8_txpd_Bit_Start                                                               22
#define cAf6_top_o_control4_p8_txpd_Bit_End                                                                 23
#define cAf6_top_o_control4_p8_txpd_Mask                                                             cBit23_22
#define cAf6_top_o_control4_p8_txpd_Shift                                                                   22
#define cAf6_top_o_control4_p8_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p8_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p8_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p8_rxpd
BitField Type: RW
BitField Desc: XFI Port8 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_top_o_control4_p8_rxpd_Bit_Start                                                               20
#define cAf6_top_o_control4_p8_rxpd_Bit_End                                                                 21
#define cAf6_top_o_control4_p8_rxpd_Mask                                                             cBit21_20
#define cAf6_top_o_control4_p8_rxpd_Shift                                                                   20
#define cAf6_top_o_control4_p8_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p8_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p8_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p7_txpd
BitField Type: RW
BitField Desc: XFI Port7 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_top_o_control4_p7_txpd_Bit_Start                                                               18
#define cAf6_top_o_control4_p7_txpd_Bit_End                                                                 19
#define cAf6_top_o_control4_p7_txpd_Mask                                                             cBit19_18
#define cAf6_top_o_control4_p7_txpd_Shift                                                                   18
#define cAf6_top_o_control4_p7_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p7_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p7_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p7_rxpd
BitField Type: RW
BitField Desc: XFI Port7 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_top_o_control4_p7_rxpd_Bit_Start                                                               16
#define cAf6_top_o_control4_p7_rxpd_Bit_End                                                                 17
#define cAf6_top_o_control4_p7_rxpd_Mask                                                             cBit17_16
#define cAf6_top_o_control4_p7_rxpd_Shift                                                                   16
#define cAf6_top_o_control4_p7_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p7_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p7_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p4_txpd
BitField Type: RW
BitField Desc: XFI Port4 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_top_o_control4_p4_txpd_Bit_Start                                                               14
#define cAf6_top_o_control4_p4_txpd_Bit_End                                                                 15
#define cAf6_top_o_control4_p4_txpd_Mask                                                             cBit15_14
#define cAf6_top_o_control4_p4_txpd_Shift                                                                   14
#define cAf6_top_o_control4_p4_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p4_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p4_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p4_rxpd
BitField Type: RW
BitField Desc: XFI Port4 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_top_o_control4_p4_rxpd_Bit_Start                                                               12
#define cAf6_top_o_control4_p4_rxpd_Bit_End                                                                 13
#define cAf6_top_o_control4_p4_rxpd_Mask                                                             cBit13_12
#define cAf6_top_o_control4_p4_rxpd_Shift                                                                   12
#define cAf6_top_o_control4_p4_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p4_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p4_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p3_txpd
BitField Type: RW
BitField Desc: XFI Port3 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_top_o_control4_p3_txpd_Bit_Start                                                               10
#define cAf6_top_o_control4_p3_txpd_Bit_End                                                                 11
#define cAf6_top_o_control4_p3_txpd_Mask                                                             cBit11_10
#define cAf6_top_o_control4_p3_txpd_Shift                                                                   10
#define cAf6_top_o_control4_p3_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p3_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p3_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p3_rxpd
BitField Type: RW
BitField Desc: XFI Port3 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_top_o_control4_p3_rxpd_Bit_Start                                                                8
#define cAf6_top_o_control4_p3_rxpd_Bit_End                                                                  9
#define cAf6_top_o_control4_p3_rxpd_Mask                                                               cBit9_8
#define cAf6_top_o_control4_p3_rxpd_Shift                                                                    8
#define cAf6_top_o_control4_p3_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p3_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p3_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p2_txpd
BitField Type: RW
BitField Desc: XFI Port2 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_top_o_control4_p2_txpd_Bit_Start                                                                6
#define cAf6_top_o_control4_p2_txpd_Bit_End                                                                  7
#define cAf6_top_o_control4_p2_txpd_Mask                                                               cBit7_6
#define cAf6_top_o_control4_p2_txpd_Shift                                                                    6
#define cAf6_top_o_control4_p2_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p2_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p2_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p2_rxpd
BitField Type: RW
BitField Desc: XFI Port2 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_top_o_control4_p2_rxpd_Bit_Start                                                                4
#define cAf6_top_o_control4_p2_rxpd_Bit_End                                                                  5
#define cAf6_top_o_control4_p2_rxpd_Mask                                                               cBit5_4
#define cAf6_top_o_control4_p2_rxpd_Shift                                                                    4
#define cAf6_top_o_control4_p2_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p2_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p2_rxpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p1_txpd
BitField Type: RW
BitField Desc: XFI Port1 TX Power Down 11 : Normal mode. Transceiver TX is
active sending or receiving data. 00 : Power-down mode. Transceiver TX is idle.
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_top_o_control4_p1_txpd_Bit_Start                                                                2
#define cAf6_top_o_control4_p1_txpd_Bit_End                                                                  3
#define cAf6_top_o_control4_p1_txpd_Mask                                                               cBit3_2
#define cAf6_top_o_control4_p1_txpd_Shift                                                                    2
#define cAf6_top_o_control4_p1_txpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p1_txpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p1_txpd_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: p1_rxpd
BitField Type: RW
BitField Desc: XFI Port1 RX Power Down 11 : Normal mode. Transceiver RX is
active sending or receiving data. 00 : Power-down mode. Transceiver RX is idle.
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_top_o_control4_p1_rxpd_Bit_Start                                                                0
#define cAf6_top_o_control4_p1_rxpd_Bit_End                                                                  1
#define cAf6_top_o_control4_p1_rxpd_Mask                                                               cBit1_0
#define cAf6_top_o_control4_p1_rxpd_Shift                                                                    0
#define cAf6_top_o_control4_p1_rxpd_MaxVal                                                                 0x3
#define cAf6_top_o_control4_p1_rxpd_MinVal                                                                 0x0
#define cAf6_top_o_control4_p1_rxpd_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 5
Reg Addr   : 0xF0_0045
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_top_o_control5_Base                                                                   0xF00045
#define cAf6Reg_top_o_control5                                                                        0xF00045
#define cAf6Reg_top_o_control5_WidthVal                                                                     32
#define cAf6Reg_top_o_control5_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: p6_txelecidle
BitField Type: RW
BitField Desc: Port 6 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
6.
BitField Bits: [07]
--------------------------------------*/
#define cAf6_top_o_control5_p6_txelecidle_Bit_Start                                                          7
#define cAf6_top_o_control5_p6_txelecidle_Bit_End                                                            7
#define cAf6_top_o_control5_p6_txelecidle_Mask                                                           cBit7
#define cAf6_top_o_control5_p6_txelecidle_Shift                                                              7
#define cAf6_top_o_control5_p6_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p6_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p6_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p5_txelecidle
BitField Type: RW
BitField Desc: Port 5 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
5.
BitField Bits: [06]
--------------------------------------*/
#define cAf6_top_o_control5_p5_txelecidle_Bit_Start                                                          6
#define cAf6_top_o_control5_p5_txelecidle_Bit_End                                                            6
#define cAf6_top_o_control5_p5_txelecidle_Mask                                                           cBit6
#define cAf6_top_o_control5_p5_txelecidle_Shift                                                              6
#define cAf6_top_o_control5_p5_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p5_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p5_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p8_txelecidle
BitField Type: RW
BitField Desc: Port 8 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
8.
BitField Bits: [05]
--------------------------------------*/
#define cAf6_top_o_control5_p8_txelecidle_Bit_Start                                                          5
#define cAf6_top_o_control5_p8_txelecidle_Bit_End                                                            5
#define cAf6_top_o_control5_p8_txelecidle_Mask                                                           cBit5
#define cAf6_top_o_control5_p8_txelecidle_Shift                                                              5
#define cAf6_top_o_control5_p8_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p8_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p8_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p7_txelecidle
BitField Type: RW
BitField Desc: Port 7 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
7.
BitField Bits: [04]
--------------------------------------*/
#define cAf6_top_o_control5_p7_txelecidle_Bit_Start                                                          4
#define cAf6_top_o_control5_p7_txelecidle_Bit_End                                                            4
#define cAf6_top_o_control5_p7_txelecidle_Mask                                                           cBit4
#define cAf6_top_o_control5_p7_txelecidle_Shift                                                              4
#define cAf6_top_o_control5_p7_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p7_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p7_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p4_txelecidle
BitField Type: RW
BitField Desc: Port 4 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
4.
BitField Bits: [03]
--------------------------------------*/
#define cAf6_top_o_control5_p4_txelecidle_Bit_Start                                                          3
#define cAf6_top_o_control5_p4_txelecidle_Bit_End                                                            3
#define cAf6_top_o_control5_p4_txelecidle_Mask                                                           cBit3
#define cAf6_top_o_control5_p4_txelecidle_Shift                                                              3
#define cAf6_top_o_control5_p4_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p4_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p4_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p3_txelecidle
BitField Type: RW
BitField Desc: Port 3 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
3.
BitField Bits: [02]
--------------------------------------*/
#define cAf6_top_o_control5_p3_txelecidle_Bit_Start                                                          2
#define cAf6_top_o_control5_p3_txelecidle_Bit_End                                                            2
#define cAf6_top_o_control5_p3_txelecidle_Mask                                                           cBit2
#define cAf6_top_o_control5_p3_txelecidle_Shift                                                              2
#define cAf6_top_o_control5_p3_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p3_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p3_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p2_txelecidle
BitField Type: RW
BitField Desc: Port 2 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
2.
BitField Bits: [01]
--------------------------------------*/
#define cAf6_top_o_control5_p2_txelecidle_Bit_Start                                                          1
#define cAf6_top_o_control5_p2_txelecidle_Bit_End                                                            1
#define cAf6_top_o_control5_p2_txelecidle_Mask                                                           cBit1
#define cAf6_top_o_control5_p2_txelecidle_Shift                                                              1
#define cAf6_top_o_control5_p2_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p2_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p2_txelecidle_RstVal                                                           0x0

/*--------------------------------------
BitField Name: p1_txelecidle
BitField Type: RW
BitField Desc: Port 1 TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
1.
BitField Bits: [00]
--------------------------------------*/
#define cAf6_top_o_control5_p1_txelecidle_Bit_Start                                                          0
#define cAf6_top_o_control5_p1_txelecidle_Bit_End                                                            0
#define cAf6_top_o_control5_p1_txelecidle_Mask                                                           cBit0
#define cAf6_top_o_control5_p1_txelecidle_Shift                                                              0
#define cAf6_top_o_control5_p1_txelecidle_MaxVal                                                           0x1
#define cAf6_top_o_control5_p1_txelecidle_MinVal                                                           0x0
#define cAf6_top_o_control5_p1_txelecidle_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 6
Reg Addr   : 0xF0_0046
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_top_o_control6_Base                                                                   0xF00046
#define cAf6Reg_top_o_control6                                                                        0xF00046
#define cAf6Reg_top_o_control6_WidthVal                                                                     32
#define cAf6Reg_top_o_control6_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: p6_gtrxreset
BitField Type: RW
BitField Desc: Port 6 GTRXRESET-After the RXPD signal is de asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_top_o_control6_p6_gtrxreset_Bit_Start                                                          15
#define cAf6_top_o_control6_p6_gtrxreset_Bit_End                                                            15
#define cAf6_top_o_control6_p6_gtrxreset_Mask                                                           cBit15
#define cAf6_top_o_control6_p6_gtrxreset_Shift                                                              15
#define cAf6_top_o_control6_p6_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p6_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p6_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p5_gtrxreset
BitField Type: RW
BitField Desc: Port 5 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_top_o_control6_p5_gtrxreset_Bit_Start                                                          14
#define cAf6_top_o_control6_p5_gtrxreset_Bit_End                                                            14
#define cAf6_top_o_control6_p5_gtrxreset_Mask                                                           cBit14
#define cAf6_top_o_control6_p5_gtrxreset_Shift                                                              14
#define cAf6_top_o_control6_p5_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p5_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p5_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p8_gtrxreset
BitField Type: RW
BitField Desc: Port 8 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_top_o_control6_p8_gtrxreset_Bit_Start                                                          13
#define cAf6_top_o_control6_p8_gtrxreset_Bit_End                                                            13
#define cAf6_top_o_control6_p8_gtrxreset_Mask                                                           cBit13
#define cAf6_top_o_control6_p8_gtrxreset_Shift                                                              13
#define cAf6_top_o_control6_p8_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p8_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p8_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p7_gtrxreset
BitField Type: RW
BitField Desc: Port 7 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_top_o_control6_p7_gtrxreset_Bit_Start                                                          12
#define cAf6_top_o_control6_p7_gtrxreset_Bit_End                                                            12
#define cAf6_top_o_control6_p7_gtrxreset_Mask                                                           cBit12
#define cAf6_top_o_control6_p7_gtrxreset_Shift                                                              12
#define cAf6_top_o_control6_p7_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p7_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p7_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p4_gtrxreset
BitField Type: RW
BitField Desc: Port 4 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_top_o_control6_p4_gtrxreset_Bit_Start                                                          11
#define cAf6_top_o_control6_p4_gtrxreset_Bit_End                                                            11
#define cAf6_top_o_control6_p4_gtrxreset_Mask                                                           cBit11
#define cAf6_top_o_control6_p4_gtrxreset_Shift                                                              11
#define cAf6_top_o_control6_p4_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p4_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p4_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p3_gtrxreset
BitField Type: RW
BitField Desc: Port 3 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_top_o_control6_p3_gtrxreset_Bit_Start                                                          10
#define cAf6_top_o_control6_p3_gtrxreset_Bit_End                                                            10
#define cAf6_top_o_control6_p3_gtrxreset_Mask                                                           cBit10
#define cAf6_top_o_control6_p3_gtrxreset_Shift                                                              10
#define cAf6_top_o_control6_p3_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p3_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p3_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p2_gtrxreset
BitField Type: RW
BitField Desc: Port 2 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [09]
--------------------------------------*/
#define cAf6_top_o_control6_p2_gtrxreset_Bit_Start                                                           9
#define cAf6_top_o_control6_p2_gtrxreset_Bit_End                                                             9
#define cAf6_top_o_control6_p2_gtrxreset_Mask                                                            cBit9
#define cAf6_top_o_control6_p2_gtrxreset_Shift                                                               9
#define cAf6_top_o_control6_p2_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p2_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p2_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p1_gtrxreset
BitField Type: RW
BitField Desc: Port 1 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [08]
--------------------------------------*/
#define cAf6_top_o_control6_p1_gtrxreset_Bit_Start                                                           8
#define cAf6_top_o_control6_p1_gtrxreset_Bit_End                                                             8
#define cAf6_top_o_control6_p1_gtrxreset_Mask                                                            cBit8
#define cAf6_top_o_control6_p1_gtrxreset_Shift                                                               8
#define cAf6_top_o_control6_p1_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p1_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p1_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p6_gttxreset
BitField Type: RW
BitField Desc: Port 6 GTTXRESET-After the TXPD signal is de asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [07]
--------------------------------------*/
#define cAf6_top_o_control6_p6_gttxreset_Bit_Start                                                           7
#define cAf6_top_o_control6_p6_gttxreset_Bit_End                                                             7
#define cAf6_top_o_control6_p6_gttxreset_Mask                                                            cBit7
#define cAf6_top_o_control6_p6_gttxreset_Shift                                                               7
#define cAf6_top_o_control6_p6_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p6_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p6_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p5_gttxreset
BitField Type: RW
BitField Desc: Port 5 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [06]
--------------------------------------*/
#define cAf6_top_o_control6_p5_gttxreset_Bit_Start                                                           6
#define cAf6_top_o_control6_p5_gttxreset_Bit_End                                                             6
#define cAf6_top_o_control6_p5_gttxreset_Mask                                                            cBit6
#define cAf6_top_o_control6_p5_gttxreset_Shift                                                               6
#define cAf6_top_o_control6_p5_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p5_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p5_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p8_gttxreset
BitField Type: RW
BitField Desc: Port 8 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [05]
--------------------------------------*/
#define cAf6_top_o_control6_p8_gttxreset_Bit_Start                                                           5
#define cAf6_top_o_control6_p8_gttxreset_Bit_End                                                             5
#define cAf6_top_o_control6_p8_gttxreset_Mask                                                            cBit5
#define cAf6_top_o_control6_p8_gttxreset_Shift                                                               5
#define cAf6_top_o_control6_p8_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p8_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p8_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p7_gttxreset
BitField Type: RW
BitField Desc: Port 7 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [04]
--------------------------------------*/
#define cAf6_top_o_control6_p7_gttxreset_Bit_Start                                                           4
#define cAf6_top_o_control6_p7_gttxreset_Bit_End                                                             4
#define cAf6_top_o_control6_p7_gttxreset_Mask                                                            cBit4
#define cAf6_top_o_control6_p7_gttxreset_Shift                                                               4
#define cAf6_top_o_control6_p7_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p7_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p7_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p4_gttxreset
BitField Type: RW
BitField Desc: Port 4 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [03]
--------------------------------------*/
#define cAf6_top_o_control6_p4_gttxreset_Bit_Start                                                           3
#define cAf6_top_o_control6_p4_gttxreset_Bit_End                                                             3
#define cAf6_top_o_control6_p4_gttxreset_Mask                                                            cBit3
#define cAf6_top_o_control6_p4_gttxreset_Shift                                                               3
#define cAf6_top_o_control6_p4_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p4_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p4_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p3_gttxreset
BitField Type: RW
BitField Desc: Port 3 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [02]
--------------------------------------*/
#define cAf6_top_o_control6_p3_gttxreset_Bit_Start                                                           2
#define cAf6_top_o_control6_p3_gttxreset_Bit_End                                                             2
#define cAf6_top_o_control6_p3_gttxreset_Mask                                                            cBit2
#define cAf6_top_o_control6_p3_gttxreset_Shift                                                               2
#define cAf6_top_o_control6_p3_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p3_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p3_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p2_gttxreset
BitField Type: RW
BitField Desc: Port 2 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [01]
--------------------------------------*/
#define cAf6_top_o_control6_p2_gttxreset_Bit_Start                                                           1
#define cAf6_top_o_control6_p2_gttxreset_Bit_End                                                             1
#define cAf6_top_o_control6_p2_gttxreset_Mask                                                            cBit1
#define cAf6_top_o_control6_p2_gttxreset_Shift                                                               1
#define cAf6_top_o_control6_p2_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p2_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p2_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p1_gttxreset
BitField Type: RW
BitField Desc: Port 1 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [00]
--------------------------------------*/
#define cAf6_top_o_control6_p1_gttxreset_Bit_Start                                                           0
#define cAf6_top_o_control6_p1_gttxreset_Bit_End                                                             0
#define cAf6_top_o_control6_p1_gttxreset_Mask                                                            cBit0
#define cAf6_top_o_control6_p1_gttxreset_Shift                                                               0
#define cAf6_top_o_control6_p1_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p1_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p1_gttxreset_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 0
Reg Addr   : 0xF45040
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_cursor_top_o_control0_Base                                                                   0xF45040
#define cAf6Reg_cursor_top_o_control0                                                                        0xF45040
#define cAf6Reg_cursor_top_o_control0_WidthVal                                                                     32
#define cAf6Reg_cursor_top_o_control0_WriteMask                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 2
Reg Addr   : 0xF45042
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_cursor_top_o_control2_Base                                                                   0xF45042
#define cAf6Reg_cursor_top_o_control2                                                                        0xF45042
#define cAf6Reg_cursor_top_o_control2_WidthVal                                                                     32
#define cAf6Reg_cursor_top_o_control2_WriteMask                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 4
Reg Addr   : 0xF45044
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_cursor_top_o_control4_Base                                                                   0xF45044
#define cAf6Reg_cursor_top_o_control4                                                                        0xF45044
#define cAf6Reg_cursor_top_o_control4_WidthVal                                                                     32
#define cAf6Reg_cursor_top_o_control4_WriteMask                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 5
Reg Addr   : 0xF45045
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_cursor_top_o_control5_Base                                                                   0xF45045
#define cAf6Reg_cursor_top_o_control5                                                                        0xF45045
#define cAf6Reg_cursor_top_o_control5_WidthVal                                                                     32
#define cAf6Reg_cursor_top_o_control5_WriteMask                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Top ETH-Port Control 6
Reg Addr   : 0xF0_0046 (Note)
Reg Formula:
    Where  :
Reg Desc   :
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_cursor_top_o_control6_Base                                                                   0xF00046
#define cAf6Reg_cursor_top_o_control6                                                                        0xF00046
#define cAf6Reg_cursor_top_o_control6_WidthVal                                                                     32
#define cAf6Reg_cursor_top_o_control6_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: p6_gtrxreset
BitField Type: RW
BitField Desc: Port 6 GTRXRESET-After the RXPD signal is de asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_top_o_control6_p6_gtrxreset_Bit_Start                                                          15
#define cAf6_top_o_control6_p6_gtrxreset_Bit_End                                                            15
#define cAf6_top_o_control6_p6_gtrxreset_Mask                                                           cBit15
#define cAf6_top_o_control6_p6_gtrxreset_Shift                                                              15
#define cAf6_top_o_control6_p6_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p6_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p6_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p5_gtrxreset
BitField Type: RW
BitField Desc: Port 5 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_top_o_control6_p5_gtrxreset_Bit_Start                                                          14
#define cAf6_top_o_control6_p5_gtrxreset_Bit_End                                                            14
#define cAf6_top_o_control6_p5_gtrxreset_Mask                                                           cBit14
#define cAf6_top_o_control6_p5_gtrxreset_Shift                                                              14
#define cAf6_top_o_control6_p5_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p5_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p5_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p8_gtrxreset
BitField Type: RW
BitField Desc: Port 8 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_top_o_control6_p8_gtrxreset_Bit_Start                                                          13
#define cAf6_top_o_control6_p8_gtrxreset_Bit_End                                                            13
#define cAf6_top_o_control6_p8_gtrxreset_Mask                                                           cBit13
#define cAf6_top_o_control6_p8_gtrxreset_Shift                                                              13
#define cAf6_top_o_control6_p8_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p8_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p8_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p7_gtrxreset
BitField Type: RW
BitField Desc: Port 7 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_top_o_control6_p7_gtrxreset_Bit_Start                                                          12
#define cAf6_top_o_control6_p7_gtrxreset_Bit_End                                                            12
#define cAf6_top_o_control6_p7_gtrxreset_Mask                                                           cBit12
#define cAf6_top_o_control6_p7_gtrxreset_Shift                                                              12
#define cAf6_top_o_control6_p7_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p7_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p7_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p4_gtrxreset
BitField Type: RW
BitField Desc: Port 4 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_top_o_control6_p4_gtrxreset_Bit_Start                                                          11
#define cAf6_top_o_control6_p4_gtrxreset_Bit_End                                                            11
#define cAf6_top_o_control6_p4_gtrxreset_Mask                                                           cBit11
#define cAf6_top_o_control6_p4_gtrxreset_Shift                                                              11
#define cAf6_top_o_control6_p4_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p4_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p4_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p3_gtrxreset
BitField Type: RW
BitField Desc: Port 3 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_top_o_control6_p3_gtrxreset_Bit_Start                                                          10
#define cAf6_top_o_control6_p3_gtrxreset_Bit_End                                                            10
#define cAf6_top_o_control6_p3_gtrxreset_Mask                                                           cBit10
#define cAf6_top_o_control6_p3_gtrxreset_Shift                                                              10
#define cAf6_top_o_control6_p3_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p3_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p3_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p2_gtrxreset
BitField Type: RW
BitField Desc: Port 2 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [09]
--------------------------------------*/
#define cAf6_top_o_control6_p2_gtrxreset_Bit_Start                                                           9
#define cAf6_top_o_control6_p2_gtrxreset_Bit_End                                                             9
#define cAf6_top_o_control6_p2_gtrxreset_Mask                                                            cBit9
#define cAf6_top_o_control6_p2_gtrxreset_Shift                                                               9
#define cAf6_top_o_control6_p2_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p2_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p2_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p1_gtrxreset
BitField Type: RW
BitField Desc: Port 1 GTRXRESET-After the RXPD signal is de-asserted, GTRXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [08]
--------------------------------------*/
#define cAf6_top_o_control6_p1_gtrxreset_Bit_Start                                                           8
#define cAf6_top_o_control6_p1_gtrxreset_Bit_End                                                             8
#define cAf6_top_o_control6_p1_gtrxreset_Mask                                                            cBit8
#define cAf6_top_o_control6_p1_gtrxreset_Shift                                                               8
#define cAf6_top_o_control6_p1_gtrxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p1_gtrxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p1_gtrxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p6_gttxreset
BitField Type: RW
BitField Desc: Port 6 GTTXRESET-After the TXPD signal is de asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [07]
--------------------------------------*/
#define cAf6_top_o_control6_p6_gttxreset_Bit_Start                                                           7
#define cAf6_top_o_control6_p6_gttxreset_Bit_End                                                             7
#define cAf6_top_o_control6_p6_gttxreset_Mask                                                            cBit7
#define cAf6_top_o_control6_p6_gttxreset_Shift                                                               7
#define cAf6_top_o_control6_p6_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p6_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p6_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p5_gttxreset
BitField Type: RW
BitField Desc: Port 5 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [06]
--------------------------------------*/
#define cAf6_top_o_control6_p5_gttxreset_Bit_Start                                                           6
#define cAf6_top_o_control6_p5_gttxreset_Bit_End                                                             6
#define cAf6_top_o_control6_p5_gttxreset_Mask                                                            cBit6
#define cAf6_top_o_control6_p5_gttxreset_Shift                                                               6
#define cAf6_top_o_control6_p5_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p5_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p5_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p8_gttxreset
BitField Type: RW
BitField Desc: Port 8 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [05]
--------------------------------------*/
#define cAf6_top_o_control6_p8_gttxreset_Bit_Start                                                           5
#define cAf6_top_o_control6_p8_gttxreset_Bit_End                                                             5
#define cAf6_top_o_control6_p8_gttxreset_Mask                                                            cBit5
#define cAf6_top_o_control6_p8_gttxreset_Shift                                                               5
#define cAf6_top_o_control6_p8_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p8_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p8_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p7_gttxreset
BitField Type: RW
BitField Desc: Port 7 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [04]
--------------------------------------*/
#define cAf6_top_o_control6_p7_gttxreset_Bit_Start                                                           4
#define cAf6_top_o_control6_p7_gttxreset_Bit_End                                                             4
#define cAf6_top_o_control6_p7_gttxreset_Mask                                                            cBit4
#define cAf6_top_o_control6_p7_gttxreset_Shift                                                               4
#define cAf6_top_o_control6_p7_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p7_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p7_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p4_gttxreset
BitField Type: RW
BitField Desc: Port 4 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [03]
--------------------------------------*/
#define cAf6_top_o_control6_p4_gttxreset_Bit_Start                                                           3
#define cAf6_top_o_control6_p4_gttxreset_Bit_End                                                             3
#define cAf6_top_o_control6_p4_gttxreset_Mask                                                            cBit3
#define cAf6_top_o_control6_p4_gttxreset_Shift                                                               3
#define cAf6_top_o_control6_p4_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p4_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p4_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p3_gttxreset
BitField Type: RW
BitField Desc: Port 3 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [02]
--------------------------------------*/
#define cAf6_top_o_control6_p3_gttxreset_Bit_Start                                                           2
#define cAf6_top_o_control6_p3_gttxreset_Bit_End                                                             2
#define cAf6_top_o_control6_p3_gttxreset_Mask                                                            cBit2
#define cAf6_top_o_control6_p3_gttxreset_Shift                                                               2
#define cAf6_top_o_control6_p3_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p3_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p3_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p2_gttxreset
BitField Type: RW
BitField Desc: Port 2 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [01]
--------------------------------------*/
#define cAf6_top_o_control6_p2_gttxreset_Bit_Start                                                           1
#define cAf6_top_o_control6_p2_gttxreset_Bit_End                                                             1
#define cAf6_top_o_control6_p2_gttxreset_Mask                                                            cBit1
#define cAf6_top_o_control6_p2_gttxreset_Shift                                                               1
#define cAf6_top_o_control6_p2_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p2_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p2_gttxreset_RstVal                                                            0x0

/*--------------------------------------
BitField Name: p1_gttxreset
BitField Type: RW
BitField Desc: Port 1 GTTXRESET-After the TXPD signal is de-asserted, GTTXRESET
must be toggled 1:Reset /0: Normal.
BitField Bits: [00]
--------------------------------------*/
#define cAf6_top_o_control6_p1_gttxreset_Bit_Start                                                           0
#define cAf6_top_o_control6_p1_gttxreset_Bit_End                                                             0
#define cAf6_top_o_control6_p1_gttxreset_Mask                                                            cBit0
#define cAf6_top_o_control6_p1_gttxreset_Shift                                                               0
#define cAf6_top_o_control6_p1_gttxreset_MaxVal                                                            0x1
#define cAf6_top_o_control6_p1_gttxreset_MinVal                                                            0x0
#define cAf6_top_o_control6_p1_gttxreset_RstVal                                                            0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051ETHPORTSERDESREG_H_ */


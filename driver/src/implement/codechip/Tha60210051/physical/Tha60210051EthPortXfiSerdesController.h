/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210051EthPortXfiSerdesController.h
 * 
 * Created Date: May 25, 2016
 *
 * Description : 60210051 Ethernet XFI serdes controller definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051ETHPORTXFISERDESCONTROLLER_H_
#define _THA60210051ETHPORTXFISERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/physical/Tha60210011EthPortXfiSerdesControllerInternal.h"
#include "../eth/Tha60210051ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051EthPortXfiSerdesController
    {
    tTha60210011EthPortXfiSerdesController super;
    void *haCache;
    }tTha60210051EthPortXfiSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60210051EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051ETHPORTXFISERDESCONTROLLER_H_ */


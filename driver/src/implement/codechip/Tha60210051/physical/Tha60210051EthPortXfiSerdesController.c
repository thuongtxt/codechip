/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210051EthPortXfiSerdesController.c
 * 
 * Created Date: Oct 31, 2015
 *
 * Description : SERDES manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "Tha60210051EthPortSerdesReg.h"
#include "Tha60210051EthPortXfiSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210051EthPortXfiSerdesController *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                          m_AtObjectOverride;
static tAtSerdesControllerMethods                m_AtSerdesControllerOverride;
static tTha6021EthPortXfiSerdesControllerMethods m_Tha6021EthPortXfiSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods    m_Tha6021EthPortSerdesControllerOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldUseMdioForEnabling(Tha6021EthPortXfiSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 TxHwXfiToMask(uint32 hwSerdesId)
    {
    switch (hwSerdesId)
        {
        case 0: return cBit0;
        case 4: return cBit1;
        case 2: return cBit2;
        case 6: return cBit3;
        case 1: return cBit4;
        case 5: return cBit5;
        case 3: return cBit6;
        case 7: return cBit7;
        default: return 0;
        }
    }

static uint32 TxHwXfiToShift(uint32 hwSerdesId)
    {
    switch (hwSerdesId)
        {
        case 0: return 0;
        case 4: return 1;
        case 2: return 2;
        case 6: return 3;
        case 1: return 4;
        case 5: return 5;
        case 3: return 6;
        case 7: return 7;
        default: return 0;
        }
    }

static uint32 TxSerdesEnableMask(uint32 serdesHwId)
    {
    return TxHwXfiToMask(serdesHwId);
    }

static uint32 TxSerdesEnableShift(uint32 serdesHwId)
    {
    return TxHwXfiToShift(serdesHwId);
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_cursor_top_o_control5_Base;
    }

static uint32 PowerControlMaskId(uint32 serdesId)
    {
    if (serdesId == 5)
        return 7;
    if (serdesId == 4)
        return 6;
    if (serdesId == 7)
        return 5;
    if (serdesId == 6)
        return 4;
    return serdesId;
    }

static AtModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModuleEth);
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return AtDeviceIsSimulated(AtChannelDeviceGet(channel));
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 regVal, maskId;
    uint8 powerDownHwVal, electricIdleVal;
    uint32 powerDownFieldMask, powerDownFieldShift;
    uint32 electricIdleFieldMask, electricIdleFieldShift;
    uint32 txResetFieldMask, txResetFieldShift;
    uint32 rxResetFieldMask, rxResetFieldShift;

    if (Tha60210051ModuleEthPowerControlIsSupported(EthModule(self)) == cAtFalse)
        return cAtOk;

    if (AtSerdesControllerPowerIsDown(self) == powerDown)
        return cAtOk;

    /* Notify to listeners that serdes will power down to protect HW engines */
    if (powerDown)
        {
        AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
        AtSerdesController controller = AtEthPortSerdesController(port);
        if (controller == self)
            Tha60210051EthPortSerdesWillPowerDownNotify((ThaEthPort)port);
        }

    powerDownHwVal = powerDown ? 0 : 0xF;
    electricIdleVal = powerDown ? 0 : 1;
    maskId = PowerControlMaskId(mMethodsGet(self)->HwIdGet(self));
    powerDownFieldMask  = (cAf6_top_o_control4_p1_rxpd_Mask | cAf6_top_o_control4_p1_txpd_Mask) << (maskId * 4);
    powerDownFieldShift = AtRegMaskToShift(powerDownFieldMask);
    electricIdleFieldMask  = cAf6_top_o_control5_p1_txelecidle_Mask << maskId;
    electricIdleFieldShift = AtRegMaskToShift(electricIdleFieldMask);
    txResetFieldMask  = cAf6_top_o_control6_p1_gttxreset_Mask << maskId;
    txResetFieldShift = AtRegMaskToShift(txResetFieldMask);
    rxResetFieldMask  = cAf6_top_o_control6_p1_gtrxreset_Mask << maskId;
    rxResetFieldShift = AtRegMaskToShift(rxResetFieldMask);

    regVal = AtSerdesControllerRead(self, cAf6Reg_top_o_control4_Base, cAtModuleEth);
    mRegFieldSet(regVal, powerDownField, powerDownHwVal);
    AtSerdesControllerWrite(self, cAf6Reg_top_o_control4_Base, regVal, cAtModuleEth);

    regVal = AtSerdesControllerRead(self, cAf6Reg_top_o_control5_Base, cAtModuleEth);
    mRegFieldSet(regVal, electricIdleField, electricIdleVal);
    AtSerdesControllerWrite(self, cAf6Reg_top_o_control5_Base, regVal, cAtModuleEth);

    /* Reset serdes */
    regVal = 0;
    mRegFieldSet(regVal, txResetField, 1);
    mRegFieldSet(regVal, rxResetField, 1);
    AtSerdesControllerWrite(self, cAf6Reg_top_o_control6_Base, regVal, cAtModuleEth);
    if (!IsSimulated(self))
        AtOsalUSleep(100000);
    AtSerdesControllerWrite(self, cAf6Reg_top_o_control6_Base, 0, cAtModuleEth);

    return cAtOk;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    uint32 regVal;
    uint32 maskId;
    uint32 powerDownFieldMask, powerDownFieldShift;

    if (Tha60210051ModuleEthPowerControlIsSupported(EthModule(self)) == cAtFalse)
        return cAtFalse;

    maskId = PowerControlMaskId(mMethodsGet(self)->HwIdGet(self));
    powerDownFieldMask  = (cAf6_top_o_control4_p1_rxpd_Mask | cAf6_top_o_control4_p1_txpd_Mask) << (maskId * 4);
    powerDownFieldShift = AtRegMaskToShift(powerDownFieldMask);
    regVal = AtSerdesControllerRead(self, cAf6Reg_top_o_control4_Base, cAtModuleEth);
    return (mRegField(regVal, powerDownField) == 0) ? cAtTrue : cAtFalse;
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(channel);

    if (Tha60210051ModuleEthPowerControlIsSupported(ethModule))
        return cAtTrue;
        
    return cAtFalse;
    }

static void **StandbyCacheMemoryAddress(AtSerdesController self)
    {
    return &(mThis(self)->haCache);
    }

static uint32 LocalSerdesId(uint32 serdesId)
    {
    return serdesId % 4;
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return cBit4_0 << (LocalSerdesId(serdesId) * 8);
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return LocalSerdesId(serdesId) * 8;
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_cursor_top_o_control4_Base;
    }

static uint32 TxCursorAddress(Tha6021EthPortSerdesController self, uint32 cursorBaseAddress)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 hwSerdesId = mMethodsGet(controller)->HwIdGet(controller);
    return (hwSerdesId / 4) ? (cursorBaseAddress + 1) : cursorBaseAddress;
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    return TxCursorAddress(self, cAf6Reg_cursor_top_o_control0_Base);
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    return TxCursorAddress(self, cAf6Reg_cursor_top_o_control2_Base);
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6021PhysicalParamIsSupported(self, param);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(haCache);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, StandbyCacheMemoryAddress);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortXfiSerdesController(AtSerdesController self)
    {
    Tha6021EthPortXfiSerdesController controller = (Tha6021EthPortXfiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortXfiSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortXfiSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, ShouldUseMdioForEnabling);
        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableMask);
        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableShift);
        }

    mMethodsSet(controller, &m_Tha6021EthPortXfiSerdesControllerOverride);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortXfiSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthPortXfiSerdesController);
    }

AtSerdesController Tha60210051EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011EthPortXfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210051EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051EthPortXfiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

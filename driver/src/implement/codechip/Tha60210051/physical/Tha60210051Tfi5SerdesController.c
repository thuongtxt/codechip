/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical Module Name
 *
 * File        : Tha60210051Tfi5SerdesController.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : TFI5 serdes controller status
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleClock.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../clock/Tha60210051ModuleClock.h"
#include "Tha60210051Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60210051Tfi5SerdesController
    {
    tTha60210011Tfi5SerdesController super;
    }tTha60210051Tfi5SerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleClock ClockModuleGet(AtSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet(self);
    AtDevice device = AtChannelDeviceGet((AtChannel)line);
    return (AtModuleClock)AtDeviceModuleGet(device, cAtModuleClock);
    }

static uint32 AlarmRead2Clear(AtSerdesController self, eBool r2c)
    {
    AtModuleClock clockModule = ClockModuleGet(self);
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);

    if (Tha60210051ModuleClockTfi5SerdesCdrLockIsFailed(clockModule, serdesId, r2c))
        return cAtSerdesAlarmTypeLinkDown;

    return 0;
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtTrue);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051Tfi5SerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5SerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210051Tfi5SerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

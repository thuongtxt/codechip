/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210051EthPortSerdesManagerInternal.h
 * 
 * Created Date: May 20, 2016
 *
 * Description : SERDES manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051ETHPORTSERDESMANAGERINTERNAL_H_
#define _THA60210051ETHPORTSERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/physical/Tha60210011EthSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051EthPortSerdesManager * Tha60210051EthPortSerdesManager;

typedef struct tTha60210051EthPortSerdesManagerMethods
    {
    uint32 (*StartHwVersionCanControlEqualizer)(Tha60210051EthPortSerdesManager self);
    }tTha60210051EthPortSerdesManagerMethods;

typedef struct tTha60210051EthPortSerdesManager
    {
    tTha60210011EthSerdesManager super;
    const tTha60210051EthPortSerdesManagerMethods *methods;
    }tTha60210051EthPortSerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthSerdesManager Tha60210051EthPortSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051ETHPORTSERDESMANAGERINTERNAL_H_ */


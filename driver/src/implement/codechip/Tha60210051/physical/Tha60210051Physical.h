/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : physical
 * 
 * File        : Tha60210051Physical.h
 * 
 * Created Date: Jun 9, 2016
 *
 * Description : interface from the physical module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051PHYSICAL_H_
#define _THA60210051PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60210051Tfi5SerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051PHYSICAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210051EthPortSerdesManager.c
 *
 * Created Date: Oct 31, 2015
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../eth/Tha60210051ModuleEth.h"
#include "Tha60210051EthPortSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051EthPortSerdesManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210051EthPortSerdesManagerMethods m_methods;

/* Override */
static tThaEthSerdesManagerMethods m_ThaEthSerdesManagerOverride;

/* Save super implementation */
static const tThaEthSerdesManagerMethods *m_ThaEthSerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth ModuleEth(ThaEthSerdesManager self)
    {
    return ThaEthSerdesManagerModuleGet(self);
    }

static AtSerdesController SerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported(ModuleEth(self)))
        return Tha60210051EthPortXfiSerdesControllerCrossPointNew(port, serdesId);

    return Tha60210051EthPortXfiSerdesControllerNew(port, serdesId);
    }

static uint32 StartHwVersionCanControlEqualizer(Tha60210051EthPortSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x7, 0);
    }

static eBool CanControlEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)(self->ethModule));
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);

    AtUnused(serdes);
    if (ThaVersionReaderHardwareVersion(versionReader) >= mMethodsGet(mThis(self))->StartHwVersionCanControlEqualizer(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool HasXfiGroups(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumXfiGroups(ThaEthSerdesManager self)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported(ModuleEth(self)))
        return 4;

    return 2;
    }

static const uint32 *XfisForGroup(ThaEthSerdesManager self, uint32 groupId, uint32 *numPorts)
    {
    static const uint32 group0Ports[] = {0, 1, 4, 5};
    static const uint32 group1Ports[] = {2, 3, 6, 7};
    static const uint32 group2Ports[] = {8, 9, 12, 13};
    static const uint32 group3Ports[] = {10, 11, 14, 15};

    AtUnused(self);

    if (numPorts)
        *numPorts = mCount(group0Ports);

    switch (groupId)
        {
        case 0: return group0Ports;
        case 1: return group1Ports;
        default:
            break;
        }

    if (Tha60210051ModuleEthCrossPointIsSupported(ModuleEth(self)))
        {
        switch (groupId)
            {
            case 2: return group2Ports;
            case 3: return group3Ports;
            default:
                break;
            }
        }

    *numPorts = 0;
    return NULL;
    }

static uint32 NumSerdesController(ThaEthSerdesManager self)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported(ModuleEth(self)))
        return 16;

    return m_ThaEthSerdesManagerMethods->NumSerdesController(self);
    }

static void MethodsInit(Tha60210051EthPortSerdesManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartHwVersionCanControlEqualizer);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaEthSerdesManager(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthSerdesManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthSerdesManagerOverride, m_ThaEthSerdesManagerMethods, sizeof(m_ThaEthSerdesManagerOverride));

        mMethodOverride(m_ThaEthSerdesManagerOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaEthSerdesManagerOverride, CanControlEqualizer);
        mMethodOverride(m_ThaEthSerdesManagerOverride, HasXfiGroups);
        mMethodOverride(m_ThaEthSerdesManagerOverride, NumXfiGroups);
        mMethodOverride(m_ThaEthSerdesManagerOverride, XfisForGroup);
        mMethodOverride(m_ThaEthSerdesManagerOverride, NumSerdesController);
        }

    mMethodsSet(self, &m_ThaEthSerdesManagerOverride);
    }

static void Override(ThaEthSerdesManager self)
    {
    OverrideThaEthSerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthPortSerdesManager);
    }

ThaEthSerdesManager Tha60210051EthPortSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011EthSerdesManagerObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((Tha60210051EthPortSerdesManager)self);
    m_methodsInit = 1;

    return self;
    }

ThaEthSerdesManager Tha60210051EthPortSerdesManagerNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthSerdesManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return Tha60210051EthPortSerdesManagerObjectInit(newManager, ethModule);
    }

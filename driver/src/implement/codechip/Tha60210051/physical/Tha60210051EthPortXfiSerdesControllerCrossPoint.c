/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210051EthPortXfiSerdesControllerCrossPoint.c
 *
 * Created Date: May 25, 2016
 *
 * Description : XFI SERDES controller version 2 with Cross-point supported
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051EthPortXfiSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#define cStartExtraSerdesId   8
#define cGlobalBaseAddress    0xF45000
#define cMaxResetWaitTimeInMs 10

/*--------------------------- Macros -----------------------------------------*/
#define mIsCemSerdes(hwId) (hwId < cStartExtraSerdesId)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051EthPortXfiSerdesControllerCrossPoint
    {
    tTha60210051EthPortXfiSerdesController super;
    }tTha60210051EthPortXfiSerdesControllerCrossPoint;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6021EthPortXfiSerdesControllerMethods m_Tha6021EthPortXfiSerdesControllerOverride;
static tAtSerdesControllerMethods                m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods    m_Tha6021EthPortSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (AtModuleEth)AtChannelModuleGet(channel);
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return AtDeviceIsSimulated(AtChannelDeviceGet(channel));
    }

static uint32 SerdesIdToRegGroup(uint32 serdesId)
    {
    switch (serdesId)
        {
        case 0 : return 0;
        case 1 : return 0;
        case 10: return 0;
        case 11: return 0;

        case 2 : return 1;
        case 3 : return 1;
        case 6 : return 1;
        case 7 : return 1;

        case 4 : return 2;
        case 5 : return 2;
        case 14: return 2;
        case 15: return 2;

        case 8 : return 3;
        case 9 : return 3;
        case 12: return 3;
        case 13: return 3;

        default:
            return 0;
        }
    }

static uint32 SerdesIdToRegSerdes(uint32 serdesId)
    {
    switch (serdesId)
        {
        case 0 : return 2;
        case 1 : return 3;
        case 2 : return 0;
        case 3 : return 1;
        case 4 : return 2;
        case 5 : return 3;
        case 6 : return 2;
        case 7 : return 3;
        case 8 : return 0;
        case 9 : return 1;
        case 10: return 0;
        case 11: return 1;
        case 12: return 2;
        case 13: return 3;
        case 14: return 0;
        case 15: return 1;

        default:
            return 0;
        }
    }

static uint32 PowerdownRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x03 + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 RxPowerdownMask(uint32 regSerdes)
    {
    return cBit1_0 << (2 * regSerdes);
    }

static uint32 RxPowerdownShift(uint32 regSerdes)
    {
    return 2 * regSerdes;
    }

static uint32 TxPowerdownMask(uint32 regSerdes)
    {
    return cBit9_8 << (2 * regSerdes);
    }

static uint32 TxPowerdownShift(uint32 regSerdes)
    {
    return 8 + (2 * regSerdes);
    }

static uint32 TxResetRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x0C + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 TxElectricIdleMask(uint32 regSerdes)
    {
    return cBit4 << regSerdes;
    }

static uint32 TxElectricIdleShift(uint32 regSerdes)
    {
    return 4 + regSerdes;
    }

static uint32 TxResetMask(uint32 regSerdes)
    {
    return cBit0 << regSerdes;
    }

static uint32 TxResetShift(uint32 regSerdes)
    {
    return regSerdes;
    }

static uint32 TxResetDoneMask(uint32 regSerdes)
    {
    return cBit16 << regSerdes;
    }

static uint32 TxResetDoneShift(uint32 regSerdes)
    {
    return 16 + regSerdes;
    }

static uint32 RxResetRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x0D + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 RxResetMask(uint32 regSerdes)
    {
    return cBit0 << regSerdes;
    }

static uint32 RxResetShift(uint32 regSerdes)
    {
    return regSerdes;
    }

static uint32 RxResetDoneMask(uint32 regSerdes)
    {
    return cBit16 << regSerdes;
    }

static uint32 RxResetDoneShift(uint32 regSerdes)
    {
    return 16 + regSerdes;
    }

static eBool IsCheckResetTimeout(void)
    {
    return cAtFalse;
    }

static eAtRet RxPmaReset(AtSerdesController self)
    {
    uint32 address, regVal, regSerdes, serdesId;
    uint32 rxResetFieldMask, rxResetFieldShift, rxResetDoneFieldMask, rxResetDoneFieldShift;
    tAtOsalCurTime prevTime, curTime;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    rxResetFieldMask  = RxResetMask(regSerdes);
    rxResetFieldShift = RxResetShift(regSerdes);
    rxResetDoneFieldMask  = RxResetDoneMask(regSerdes);
    rxResetDoneFieldShift = RxResetDoneShift(regSerdes);

    /* Reset serdes */
    address = RxResetRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, rxResetField, 0);
    mRegFieldSet(regVal, rxResetDoneField, 0);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cMaxResetWaitTimeInMs);

    address = RxResetRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, rxResetField, 1);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    AtOsalCurTimeGet(&prevTime);
    while (1)
        {
        uint32 isDone = 0;

        address = RxResetRegAddress(serdesId);
        regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
        isDone = mRegField(regVal, rxResetDoneField);

        if (IsSimulated(self))
            return cAtOk;

        if (isDone)
            return cAtOk;

        if (!IsCheckResetTimeout())
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        if (AtOsalDifferenceTimeInMs(&curTime, &prevTime) > cMaxResetWaitTimeInMs)
            return cAtErrorSerdesResetTimeout;
        }

    return cAtOk;
    }

static eAtRet TxPmaReset(AtSerdesController self)
    {
    uint32 address, regVal, regSerdes, serdesId;
    uint32 txResetFieldMask, txResetFieldShift, txResetDoneFieldMask, txResetDoneFieldShift;
    tAtOsalCurTime prevTime, curTime;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    txResetFieldMask  = TxResetMask(regSerdes);
    txResetFieldShift = TxResetShift(regSerdes);
    txResetDoneFieldMask  = TxResetDoneMask(regSerdes);
    txResetDoneFieldShift = TxResetDoneShift(regSerdes);

    /* Reset serdes */
    address = TxResetRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, txResetField, 0);
    mRegFieldSet(regVal, txResetDoneField, 0);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cMaxResetWaitTimeInMs);

    address = TxResetRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, txResetField, 1);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    AtOsalCurTimeGet(&prevTime);
    while (1)
        {
        uint32 isDone = 0;
        address = TxResetRegAddress(serdesId);
        regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
        isDone = mRegField(regVal, txResetDoneField);

        if (IsSimulated(self))
            return cAtOk;

        if (isDone)
            return cAtOk;

        if (!IsCheckResetTimeout())
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        if (AtOsalDifferenceTimeInMs(&curTime, &prevTime) > cMaxResetWaitTimeInMs)
            return cAtErrorSerdesResetTimeout;
        }

    return cAtOk;
    }

static uint32 RxLpmRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x0E + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 RxLpmModeMask(uint32 regSerdes)
    {
    return cBit0 << regSerdes;
    }

static uint32 RxLpmModeShift(uint32 regSerdes)
    {
    return regSerdes;
    }
static uint32 RxLpmResetRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x0F + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 RxLpmResetMask(uint32 regSerdes)
    {
    return cBit0 << regSerdes;
    }

static uint32 RxLpmResetShift(uint32 regSerdes)
    {
    return regSerdes;
    }

static eAtRet RxLpmReset(AtSerdesController self)
    {
    uint32 regVal, address, serdesId, regSerdes, fieldMask, fieldShift;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    address = RxLpmResetRegAddress(serdesId);
    fieldMask = RxLpmResetMask(regSerdes);
    fieldShift = RxLpmResetShift(regSerdes);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, field, 0);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, field, 1);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, field, 0);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtDrp drp;
    uint32 regVal, address, serdesId, regSerdes, mask, shift;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    /* Step1: switch RXLPMEN: 0(DFE), 1(LPM) */
    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    address = RxLpmRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mask  = RxLpmModeMask(regSerdes);
    shift = RxLpmModeShift(regSerdes);
    mFieldIns(&regVal, mask, shift, Tha6021EthPortSerdesEqualizerModeSw2Hw(mode));
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    /* Step2: set coefficience */
    drp = AtSerdesControllerDrp(self);
    AtDrpPortSelect(drp, AtSerdesControllerIdGet(self));
    AtDrpWrite(drp, cEqualizerModeRegister, Tha6021EthPortSerdesEqualizerModeCoeficSw2Hw(mode));

    /* Step3: reset */
    return RxLpmReset(self);
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 regVal, address, serdesId, regSerdes, mask, shift, lpmEnabled;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    address = RxLpmRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mask = RxLpmModeMask(regSerdes);
    shift = RxLpmModeShift(regSerdes);
    mFieldGet(regVal, mask, shift, uint32, &lpmEnabled);

    return Tha6021EthPortSerdesEqualizerModeHw2Sw(lpmEnabled);
    }

static uint32 TxDiffCtrlRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x10 + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return cBit4_0 << (5*SerdesIdToRegSerdes(serdesId));
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return 5 * SerdesIdToRegSerdes(serdesId);
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return cBit3_0 << (4 * SerdesIdToRegSerdes(serdesId));
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return 4 * SerdesIdToRegSerdes(serdesId);
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 hwSerdesId = mMethodsGet(controller)->HwIdGet(controller);
    return TxDiffCtrlRegAddress(hwSerdesId);
    }

static uint32 TxPostCursorRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x11 + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 TxPreCursorRegAddress(uint32 serdesId)
    {
    return cGlobalBaseAddress + 0x12 + (0x100 * SerdesIdToRegGroup(serdesId));
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 hwSerdesId = mMethodsGet(controller)->HwIdGet(controller);
    return TxPreCursorRegAddress(hwSerdesId);
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 hwSerdesId = mMethodsGet(controller)->HwIdGet(controller);
    return TxPostCursorRegAddress(hwSerdesId);
    }

static uint32 TxSerdesEnableMask(uint32 serdesHwId)
    {
    if (mIsCemSerdes(serdesHwId))
        return (uint32)(cBit8 << serdesHwId);

    return (uint32)(cBit24 << (serdesHwId - cStartExtraSerdesId));
    }

static uint32 TxSerdesEnableShift(uint32 serdesHwId)
    {
    if (mIsCemSerdes(serdesHwId))
        return (uint32)(8 + serdesHwId);

    return (uint32)(24 + (serdesHwId - cStartExtraSerdesId));
    }

static eAtRet TxPowerdown(AtSerdesController self, eBool powerDown)
    {
    uint32 address, regVal, regSerdes, serdesId;
    uint8 powerDownHwVal, electricIdleVal;
    uint32 powerDownFieldMask, powerDownFieldShift;
    uint32 electricIdleFieldMask, electricIdleFieldShift;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    powerDownHwVal = powerDown ? 0x3 : 0x0;

    /* power down TX */
    electricIdleVal = powerDown ? 1 : 0;
    powerDownFieldMask  = TxPowerdownMask(regSerdes);
    powerDownFieldShift = TxPowerdownShift(regSerdes);
    electricIdleFieldMask  = TxElectricIdleMask(regSerdes);
    electricIdleFieldShift = TxElectricIdleShift(regSerdes);

    address = PowerdownRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, powerDownField, powerDownHwVal);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    address = TxResetRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, electricIdleField, electricIdleVal);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return TxPmaReset(self);
    }

static eAtRet RxPowerdown(AtSerdesController self, eBool powerDown)
    {
    uint32 address, regVal, regSerdes, serdesId;
    uint32 powerDownHwVal, powerDownFieldMask, powerDownFieldShift;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    powerDownHwVal = powerDown ? 0x3 : 0x0;

    powerDownFieldMask  = RxPowerdownMask(regSerdes);
    powerDownFieldShift = RxPowerdownShift(regSerdes);

    address = PowerdownRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, powerDownField, powerDownHwVal);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return RxPmaReset(self);
    }

static eBool IsSerdesBelongToCem(AtSerdesController self)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    if (mIsCemSerdes(serdesId))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    eAtRet ret = cAtOk;

    if (!Tha60210051ModuleEthPowerControlIsSupported(EthModule(self)))
        return cAtOk;

    /* Had been configured */
    if (Tha60210051ModuleEthXfiGroupIsSetup(EthModule(self)))
        {
        eBool previousPowerdown = AtSerdesControllerPowerIsDown(self);
        if ((powerDown && previousPowerdown) || (!powerDown && !previousPowerdown))
            return cAtOk;
        }

    /* Notify to listeners that serdes will power down to protect HW engines
     * by disable all PWs belong to thi MAC */
    if (powerDown && IsSerdesBelongToCem(self))
        {
        AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
        AtSerdesController controller = AtEthPortSerdesController(port);
        if (controller == self)
            Tha60210051EthPortSerdesWillPowerDownNotify((ThaEthPort)port);
        }

    ret = TxPowerdown(self, powerDown);
    if (ret != cAtOk)
        return ret;

    return RxPowerdown(self, powerDown);
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    uint32 address, regVal, powerDownHwVal;
    uint32 regSerdes, serdesId;
    uint32 powerDownFieldMask, powerDownFieldShift;

    if (Tha60210051ModuleEthPowerControlIsSupported(EthModule(self)) == cAtFalse)
        return cAtFalse;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    regSerdes = SerdesIdToRegSerdes(serdesId);
    powerDownHwVal = 0x3;
    powerDownFieldMask  = TxPowerdownMask(regSerdes);
    powerDownFieldShift = TxPowerdownShift(regSerdes);
    address = PowerdownRegAddress(serdesId);
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    return (mRegField(regVal, powerDownField) == powerDownHwVal) ? cAtTrue : cAtFalse;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortXfiSerdesController(AtSerdesController self)
    {
    Tha6021EthPortXfiSerdesController controller = (Tha6021EthPortXfiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortXfiSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortXfiSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableMask);
        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, TxSerdesEnableShift);
        }

    mMethodsSet(controller, &m_Tha6021EthPortXfiSerdesControllerOverride);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, PreCursorShift);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlMask);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, DiffCtrlShift);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortXfiSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthPortXfiSerdesControllerCrossPoint);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortXfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210051EthPortXfiSerdesControllerCrossPointNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

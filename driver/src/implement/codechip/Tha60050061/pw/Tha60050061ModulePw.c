/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60050061ModulePw.c
 *
 * Created Date: Apr 26, 2015
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60030081/pw/Tha60030081ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60050061ModulePw
    {
    tTha60030081ModulePw super;
    }tTha60050061ModulePw;

/*--------------------------- Global variables -------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods  m_AtModulePwOverride;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 512;
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(tAtModulePwMethods));
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60050061ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, (void *)self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030081ModulePwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60050061ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newPwModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPwModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPwModule, device);
    }

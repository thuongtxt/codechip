/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6A000010ModulePw.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : Pseudowire module of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/pw/Tha60210051ModulePwInternal.h"
#include "../../Tha60210011/pw/activator/Tha60210011PwActivator.h"
#include "Tha6A000010ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods         m_ThaModulePwOverride;
static tAtModuleMethods            m_AtModuleOverride;
static tTha60210011ModulePwMethods m_Tha60210011ModulePwOverride;

/* Save super implementation */
static const tAtModuleMethods   *m_AtModuleMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha6A000010PwDynamicActivatorNew((AtModulePw)self);
    }

static eBool CasIsSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultCounterModule(ThaModulePw self)
    {
    return AtModuleTypeGet((AtModule)self);
    }

static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210011ModulePw self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void OverrideTha60210011ModulePw(AtModulePw self)
    {
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePwOverride, mMethodsGet(pwModule), sizeof(m_Tha60210011ModulePwOverride));

        mMethodOverride(m_Tha60210011ModulePwOverride, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(pwModule, &m_Tha60210011ModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, CasIsSupported);
        mMethodOverride(m_ThaModulePwOverride, DefaultCounterModule);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw(self);
    OverrideAtModule(self);
    OverrideTha60210011ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010ModulePw);
    }

AtModulePw Tha6A000010ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha6A000010ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010ModulePwObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A000010ModulePwInternal.h
 * 
 * Created Date: Oct 18, 2019
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A000010_PW_THA6A000010MODULEPWINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A000010_PW_THA6A000010MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "../../Tha60210051/pw/Tha60210051ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A000010ModulePw
    {
    tTha60210051ModulePw super;
    }tTha6A000010ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha6A000010ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A000010_PW_THA6A000010MODULEPWINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010AttPrbsRegProvider.c
 *
 * Created Date: Jan 6, 2018
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A000010AttPrbsRegProviderInternal.h"
#include "../prbs/Tha6A000010PrbsEngineReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttPrbsRegProviderMethods      m_ThaAttPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 PrbsModeMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection?c_prbshogenctl_PrbsHoGenCtlSeqMod_Mask: c_prbshomon_PrbsHoMonCtlSeqMod_Mask;
	return isTxDirection?c_prbslogenctl_PrbsLoGenCtlSeqMod_Mask: c_prbslomon_PrbsLoMonCtlSeqMod_Mask;
	}

static uint32 PrbsStepMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection ? cAtt_prbshogenctl_seqstep_Mask : cAtt_prbshomon_seqstep_Mask;
	return isTxDirection ?cAtt_prbslogenctl_seqstep_Mask: cAtt_prbslomon_seqstep_Mask;
	}

static uint32 PrbsEnableMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection ? c_prbshogenctl_PrbsHoGenCtlEnb_Mask: cInvalidUint32;
	return isTxDirection ?c_prbslogenctl_PrbsLoGenCtlEnb_Mask: cInvalidUint32;
	}

static uint32 PrbsFixPatternMask(ThaAttPrbsRegProvider self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection? cAtt_prbshogenctl_seqdat_Mask: cAtt_prbshomon_seqdat_Mask;
	return isTxDirection ?cAtt_prbslogenctl_seqdat_Mask: cAtt_prbslomon_seqdat_Mask;
	}

static uint32 PrbsDelayEnableMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	AtUnused(self);
	AtUnused(isHo);
	return cInvalidUint32;
	}

static uint32 PrbsMaxDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsMinDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsAverageDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsGenReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cInvalidUint32;
    }

static uint32 PrbsMonReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cInvalidUint32;
    }

static uint8 LoPrbsEngineOc48IdShift(ThaAttPrbsRegProvider self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }
static uint8 LoPrbsEngineOc24SliceIdShift(ThaAttPrbsRegProvider self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static uint32 PrbsCounterMask(ThaAttPrbsRegProvider self, uint16 counterType)
	{
	AtUnused(self);
	AtUnused(counterType);
	return cInvalidUint32;
	}

static uint32 PrbsCounterReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	return cInvalidUint32;
	}

static void OverrideThaAttPrbsRegProvider(ThaAttPrbsRegProvider self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (!m_methodsInit)
        {
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaAttPrbsRegProviderOverride));

        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsGenReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsMonReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, LoPrbsEngineOc48IdShift);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, LoPrbsEngineOc24SliceIdShift);

        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsDelayEnableMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsMinDelayReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsAverageDelayReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsMaxDelayReg);

        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsModeMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsStepMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsEnableMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsFixPatternMask);

        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsCounterMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsCounterReg);
        }

    mMethodsSet(self, &m_ThaAttPrbsRegProviderOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010AttPrbsRegProvider);
    }

static void Override(ThaAttPrbsRegProvider self)
    {
	OverrideThaAttPrbsRegProvider((ThaAttPrbsRegProvider)self);
    }

ThaAttPrbsRegProvider Tha6A000010AttPrbsRegProviderObjectInit(ThaAttPrbsRegProvider self, ThaAttModulePrbs prbsModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAttPrbsRegProviderObjectInit(self, prbsModule) == NULL)
        return NULL;

    /* Setup class */
    m_methodsInit = 1;
    Override(self);
    return self;
    }

ThaAttPrbsRegProvider Tha6A000010AttPrbsRegProviderNew(ThaAttModulePrbs prbsModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaAttPrbsRegProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010AttPrbsRegProviderObjectInit(newProvider, prbsModule);
    }



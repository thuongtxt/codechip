/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha6A000010Device.c
 *
 * Created Date: Sep 8, 2015
 *
 * Description : Device of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/man/Tha60210051DeviceInternal.h"
#include "../../Tha61031031/prbs/Tha61031031ModulePrbs.h"
#include "../prbs/Tha6A000010ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A000011Device
    {
    tTha60210051Device super;
    }tTha6A000011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60210051DeviceMethods m_Tha60210051DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;

    if (_moduleId  == cAtModuleSdh)    return (AtModule)Tha6A000011ModuleSdhNew(self);
    if (_moduleId  == cAtModulePrbs)   return (AtModule)Tha6A000010ModulePrbsNew(self);
    if (_moduleId  == cAtModulePw)     return (AtModule)Tha6A000010ModulePwNew(self);
    if (_moduleId  == cAtModuleEth)    return (AtModule)Tha6A000010ModuleEthNew(self);
    if (_moduleId  == cAtModulePdh)    return (AtModule)Tha6A000010ModulePdhNew(self);

    /* HW still use POH of 8xTFI-5 product */
    if (_moduleId  == cThaModuleMap)   return Tha6A000010ModuleMapNew(self);
    if (_moduleId  == cThaModuleDemap) return Tha6A000010ModuleDemapNew(self);
    if (_moduleId  == cThaModulePoh)   return Tha6A000010ModulePohNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;

    AtUnused(self);
    switch (_moduleId)
        {
        case cThaModuleDemap: return cAtTrue;
        case cThaModuleMap:   return cAtTrue;
        case cThaModuleOcn:   return cAtTrue;
        case cThaModulePoh:   return cAtTrue;
        case cThaModuleCdr:   return cAtTrue;
        default:
            break;
        }

    switch (_moduleId)
        {
        case cAtModulePdh:    return cAtTrue;
        case cAtModuleSdh :   return cAtTrue;
        case cAtModulePw:     return cAtTrue;
        case cAtModulePrbs:   return cAtTrue;
        case cAtModuleXc:     return cAtTrue;
        case cAtModuleEth:    return cAtTrue;

        default:
            break;
        }

    return cAtFalse;
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModuleSdh,
                                                 cAtModulePdh,
                                                 cAtModulePrbs,
                                                 cAtModuleXc,
                                                 cAtModulePw,
                                                 cAtModuleEth};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60210051VersionReaderNew((AtDevice)self);
    }

static eBool UseGlobalHold(Tha60210051Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportDiagnosticCheck(Tha60210011Device self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool DdrCalibIsGood(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60210051Device(AtDevice self)
    {
    Tha60210051Device device = (Tha60210051Device)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210051DeviceOverride));

        mMethodOverride(m_Tha60210051DeviceOverride, UseGlobalHold);
        }

    mMethodsSet(device, &m_Tha60210051DeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckHwResource);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, StartVersionSupportDiagnosticCheck);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha6015011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, DdrCalibIsGood);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60210051Device(self);
    OverrideTha60210011Device(self);
    OverrideTha6015011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000011Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A000010DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

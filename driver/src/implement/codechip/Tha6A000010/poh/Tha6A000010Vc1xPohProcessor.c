/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha6A000010Vc1xPohProcessor.c
 *
 * Created Date: Dec 29, 2015
 *
 * Description : POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*
 * POH Termintate Insert Control VT/TU3
 * 0x04_4000
*/
#define cTha6A0210021PohVtK4V5EnableInsMask   cBit19
#define cTha6A0210021PohVtK4V5EnableInsShift  19

#define cTha6A0210021PohVtV5InsValMask        cBit18
#define cTha6A0210021PohVtV5InsValShift       18

#define cTha6A0210021PohVtK4InsValMask        cBit17_15
#define cTha6A0210021PohVtK4InsValShift       15

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A000010Vc1xPohProcessor
    {
    tTha60210051Vc1xPohProcessor super;
    }tTha6A000010Vc1xPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                    m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TxRdiForce(AtChannel self, eBool force)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    if (AtSdhPathERdiIsEnabled((AtSdhPath)ThaPohProcessorVcGet((ThaPohProcessor)self)))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor) +
                         mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cTha6A0210021PohVtK4InsVal, 0x0);
    mRegFieldSet(regVal, cTha6A0210021PohVtV5InsVal, 0x0);
    if (force)
        mRegFieldSet(regVal, cTha6A0210021PohVtV5InsVal, 0x1);

    mRegFieldSet(regVal, cTha6A0210021PohVtK4V5EnableIns, mBoolToBin(force));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 ERDIValueSw2Hw(uint32 alarmType)
    {
    if (alarmType & cAtSdhPathAlarmErdiC) return 0x6;
    if (alarmType & cAtSdhPathAlarmErdiS) return 0x5;
    if (alarmType & cAtSdhPathAlarmErdiP) return 0x2;

    /* Invalid */
    return 0x0;
    }

static eAtRet TxERdiForce(AtChannel self, eBool force, uint32 alarmType)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    if (!AtSdhPathERdiIsEnabled((AtSdhPath)ThaPohProcessorVcGet((ThaPohProcessor)self)))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor) +
                         mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    /* Default clear all configuration */
    mRegFieldSet(regVal, cTha6A0210021PohVtK4InsVal, 0x0);
    mRegFieldSet(regVal, cTha6A0210021PohVtV5InsVal, 0x0);

    if (force)
        {
        mRegFieldSet(regVal, cTha6A0210021PohVtK4InsVal, ERDIValueSw2Hw(alarmType));

        if ((alarmType & cAtSdhPathAlarmErdiS) ||
            (alarmType & cAtSdhPathAlarmErdiC))
            mRegFieldSet(regVal, cTha6A0210021PohVtV5InsVal, 0x1);
        }

    mRegFieldSet(regVal, cTha6A0210021PohVtK4V5EnableIns, mBoolToBin(force));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    if (alarmType & cAtSdhPathAlarmRdi)
        return TxRdiForce(self, force);

    if ((alarmType & cAtSdhPathAlarmErdiC) ||
        (alarmType & cAtSdhPathAlarmErdiP) ||
        (alarmType & cAtSdhPathAlarmErdiS))
        return TxERdiForce(self, force, alarmType);

    return cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmForce(self, alarmType);
    return HelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    return HelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forcibleAlarm = m_AtChannelMethods->TxForcibleAlarmsGet(self);

    forcibleAlarm |= cAtSdhPathAlarmRdi;
    forcibleAlarm |= cAtSdhPathAlarmErdiC;
    forcibleAlarm |= cAtSdhPathAlarmErdiP;
    forcibleAlarm |= cAtSdhPathAlarmErdiS;
    return forcibleAlarm;
    }

static void OverrideAtChannel(ThaPohProcessor self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);

        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010Vc1xPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Vc1xPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha6A000010Vc1xPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

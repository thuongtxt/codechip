/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha6A000010Tu3VcPohProcessor.c
 *
 * Created Date: Dec 30, 2015
 *
 * Description : POH Processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"
#include "Tha6A000010PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A000010Tu3VcPohProcessor
    {
    tTha60210051Tu3VcPohProcessor super;
    }tTha6A000010Tu3VcPohProcessor;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                    m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmForce(self, alarmType);
    return Tha6A000010PohProcessorHelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    return Tha6A000010PohProcessorHelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forcibleAlarm = m_AtChannelMethods->TxForcibleAlarmsGet(self);

    forcibleAlarm |= cAtSdhPathAlarmRdi;
    forcibleAlarm |= cAtSdhPathAlarmErdiC;
    forcibleAlarm |= cAtSdhPathAlarmErdiP;
    forcibleAlarm |= cAtSdhPathAlarmErdiS;
    return forcibleAlarm;
    }

static void OverrideAtChannel(ThaPohProcessor self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);

        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010Tu3VcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Tu3VcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha6A000010Tu3VcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

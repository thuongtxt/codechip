/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha6A000010PohProcessor.h
 * 
 * Created Date: Dec 30, 2015
 *
 * Description : POH Processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A000010POHPROCESSOR_H_
#define _THA6A000010POHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha6A000010PohProcessorHelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A000010POHPROCESSOR_H_ */


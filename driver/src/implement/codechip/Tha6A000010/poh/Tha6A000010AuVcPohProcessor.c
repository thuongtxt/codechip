/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha6A000010AuVcPohProcessor.c
 *
 * Created Date: Dec 30, 2015
 *
 * Description : POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"
#include "Tha6A000010PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/*
 * POH Termintate Insert Control VT/TU3
 * 0x04_4000
*/
#define cTha6A0210021PohG1EnableInsMask     cBit10
#define cTha6A0210021PohG1EnableInsShift  10

#define cTha6A0210021PohG1InsValMask        cBit9_7
#define cTha6A0210021PohG1InsValShift       7

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A000010AuVcPohProcessor
    {
    tTha60210051AuVcPohProcessor super;
    }tTha6A000010AuVcPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                    m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ERDIValueSw2Hw(uint32 alarmType)
    {
    if (alarmType & cAtSdhPathAlarmErdiC) return 0x6;
    if (alarmType & cAtSdhPathAlarmErdiS) return 0x5;
    if (alarmType & cAtSdhPathAlarmErdiP) return 0x2;

    /* Invalid */
    return 0x0;
    }

static eAtRet TxERdiForce(AtChannel self, eBool force, uint32 alarmType)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    if (!AtSdhPathERdiIsEnabled((AtSdhPath)ThaPohProcessorVcGet((ThaPohProcessor)self)))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor) +
                         mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cTha6A0210021PohG1InsVal, 0x0);
    if (force)
        mRegFieldSet(regVal, cTha6A0210021PohG1InsVal, ERDIValueSw2Hw(alarmType));

    mRegFieldSet(regVal, cTha6A0210021PohG1EnableIns, mBoolToBin(force));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtRet TxRdiForce(AtChannel self, eBool force)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    if (AtSdhPathERdiIsEnabled((AtSdhPath)ThaPohProcessorVcGet((ThaPohProcessor)self)))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor) +
                         mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cTha6A0210021PohG1InsVal, 0x0);
    if (force)
        mRegFieldSet(regVal, cTha6A0210021PohG1InsVal, 0x4);

    mRegFieldSet(regVal, cTha6A0210021PohG1EnableIns, mBoolToBin(force));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmForce(self, alarmType);
    return Tha6A000010PohProcessorHelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    return Tha6A000010PohProcessorHelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forcibleAlarm = m_AtChannelMethods->TxForcibleAlarmsGet(self);

    forcibleAlarm |= cAtSdhPathAlarmRdi;
    forcibleAlarm |= cAtSdhPathAlarmErdiC;
    forcibleAlarm |= cAtSdhPathAlarmErdiP;
    forcibleAlarm |= cAtSdhPathAlarmErdiS;
    return forcibleAlarm;
    }

static void OverrideAtChannel(ThaPohProcessor self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);

        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010AuVcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha6A000010AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

eAtRet Tha6A000010PohProcessorHelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    if (alarmType & cAtSdhPathAlarmRdi)
        return TxRdiForce(self, force);

    if ((alarmType & cAtSdhPathAlarmErdiC) ||
        (alarmType & cAtSdhPathAlarmErdiP) ||
        (alarmType & cAtSdhPathAlarmErdiS))
        return TxERdiForce(self, force, alarmType);

    return cAtErrorModeNotSupport;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha6A000010ModulePoh.c
 *
 * Created Date: Oct 13, 2015
 *
 * Description : POH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/poh/Tha60210051ModulePohInternal.h"
#include "../../Tha60210051/poh/Tha60210051PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A000010ModulePoh
    {
    tTha60210051ModulePoh super;
    }tTha6A000010ModulePoh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportSdSfInterrupt(Tha60210011ModulePoh self)
    {
    /* This ATT has not supported this yet */
    AtUnused(self);
    return cBit31_0;
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha6A000010Vc1xPohProcessorNew(vc);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha6A000010AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha6A000010Tu3VcPohProcessorNew(vc);
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, mMethodsGet(pohModule), sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSdSfInterrupt);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010ModulePoh);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A000010ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngineVc1x.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : Low-Order PRBS of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtSdhPath.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "Tha6A000010PrbsEngineInternal.h"
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../../../default/man/ThaDevice.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCepCreate(pwModule, (uint16)pwId, cAtPwCepModeBasic);
    }

static eBool HwPrbsEngineEnable(Tha6A000010PrbsEngine self, AtPw pw, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    AtSdhVc vc1x = (AtSdhVc)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaModuleAbstractMap mapDemapModule;
    eAtRet ret;

    if (Tha6A000010PrbsEngineHasPrbsHoBus(self))
        return mMethodsGet(self)->HwPrbsEngineEnable(self, pw, enable);

    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret = ThaModuleAbstractMapVc1xEncapConnectionEnable(mapDemapModule, vc1x, enable);
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    ret |= ThaModuleAbstractMapVc1xEncapConnectionEnable(mapDemapModule, vc1x, enable);

    return ret;
    }

static eAtModulePrbsRet POHDefaultSet(Tha6A000010PrbsEngine self, AtSdhChannel sdhChannel)
    {
    eAtRet ret = Tha6A0000POHDefaultSet((AtPrbsEngine)self);

    ThaOcnVtPohInsertEnable(sdhChannel, AtSdhChannelSts1Get(sdhChannel), AtSdhChannelTug2Get(sdhChannel), AtSdhChannelTu1xGet(sdhChannel), cAtTrue);
    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);
    return ret;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);

    if (ret != cAtOk)
        return ret;

    return Tha6A000010PrbsEnginePohLoPathDefaultSet(self);
    }

static eAtModulePrbsRet PohLoPathDefaultSet(AtPrbsEngine self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    uint8 vtgId, vtId;
    eAtModulePrbsRet ret = cAtOk;

    vtgId = AtSdhChannelTug2Get(sdhChannel);
    vtId  = AtSdhChannelTu1xGet(sdhChannel);
    ThaOcnVtPohInsertEnable(sdhChannel, AtSdhChannelSts1Get(sdhChannel), vtgId, vtId, cAtTrue);

    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);

    return Tha6A0000POHDefaultSet(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, HwPrbsEngineEnable);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, POHDefaultSet);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngineVc1x);
    }

AtPrbsEngine Tha6A000010PrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)vc1x) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A000010PrbsEngineVc1xNew(AtSdhChannel vc1x)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010PrbsEngineVc1xObjectInit(newEngine, vc1x);
    }

eAtModulePrbsRet Tha6A000010PrbsEnginePohLoPathDefaultSet(AtPrbsEngine self)
    {
    if (self)
        return PohLoPathDefaultSet(self);
    return cAtErrorNullPointer;
    }

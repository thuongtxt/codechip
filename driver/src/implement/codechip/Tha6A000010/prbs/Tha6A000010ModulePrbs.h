/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010ModulePrbs.h
 * 
 * Created Date: Dec 3, 2015
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A000010MODULEPRBS_H_
#define _THA6A000010MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A000010ModulePrbs * Tha6A000010ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A000010ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A000010ModulePrbsV2New(AtDevice device);
eBool Tha6A000010ModulePrbsNeedShiftUp(Tha6A000010ModulePrbs self);

uint32 Tha6A000010ModulePrbsMonReg(Tha6A000010ModulePrbs self, eBool isHo);
uint32 Tha6A000010ModulePrbsGenReg(Tha6A000010ModulePrbs self, eBool isHo);

eAtPrbsMode Tha6A000010ModulePrbsModeFromHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step);
uint8 Tha6A000010ModulePrbsModeToHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode);
uint8 Tha6A000010ModulePrbsHwStepFromPrbsMode(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode);

uint32 Tha6A000010ModulePrbsModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsDataModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsStepMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsEnableMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsFixPatternMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsFixPatternReg(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection);

uint32 Tha6A000010ModulePrbsCounterReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c);
uint32 Tha6A000010ModulePrbsCounterMask(Tha6A000010ModulePrbs self, uint16 counterType);
eBool Tha6A000010ModuleCounterIsSupported(Tha6A000010ModulePrbs self, uint16 counterType);

uint32 Tha6A000010ModulePrbsDelayConfigReg(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice);
uint32 Tha6A000010ModulePrbsDelayIdMask(Tha6A000010ModulePrbs self, eBool isHo);
uint32 Tha6A000010ModulePrbsDelayEnableMask(Tha6A000010ModulePrbs self, eBool isHo);
eBool Tha6A000010ModulePrbsModeIsSupported(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode);
eBool Tha6A000010ModulePrbsDataModeIsSupported(Tha6A000010ModulePrbs self);

uint32 Tha6A000010ModulePrbsConvertSwToHwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);
uint32 Tha6A000010ModulePrbsConvertHwToSwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);
uint32 Tha6A000010ModulePrbsEngineAlarmGet(Tha6A000010ModulePrbs self, AtPw pw);
uint32 Tha6A000010ModulePrbsEngineAlarmHistoryGet(Tha6A000010ModulePrbs self, AtPw pw);
eAtRet Tha6A000010ModulePrbsEngineModeSet(Tha6A000010ModulePrbs self, AtPw pw, uint8 mode, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsEngineModeGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsEngineFixPatternGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection);
eAtRet Tha6A000010ModulePrbsEngineFixPatternSet(Tha6A000010ModulePrbs self, AtPw pw, uint32 fixPattern, eBool isTxDirection);
uint32 Tha6A000010ModulePrbsEngineCounterGet(Tha6A000010ModulePrbs self, AtPrbsEngine engine, AtPw pw, uint16 counterType, eBool r2c);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A000010MODULEPRBS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngineTu3Vc.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : TU3VC PRBS of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A000010PrbsEngineInternal.h"
#include "AtSdhPath.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCepCreate(pwModule, (uint16)pwId, cAtPwCepModeBasic);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);

    if (ret != cAtOk)
        return ret;

    return Tha6A0000POHDefaultSet(self);
    }

static eBool HwPrbsEngineEnable(Tha6A000010PrbsEngine self, AtPw pw, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    AtSdhVc vcx = (AtSdhVc)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaModuleAbstractMap mapDemapModule;
    eAtRet ret;

    if (Tha6A000010PrbsEngineHasPrbsHoBus(self))
        return mMethodsGet(self)->HwPrbsEngineEnable(self, pw, enable);

    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret = ThaModuleAbstractMapVcxEncapConnectionEnable(mapDemapModule, vcx, enable);
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    ret |= ThaModuleAbstractMapVcxEncapConnectionEnable(mapDemapModule, vcx, enable);

    return ret;
    }

static eAtModulePrbsRet POHDefaultSet(Tha6A000010PrbsEngine self, AtSdhChannel sdhChannel)
    {
    AtUnused(sdhChannel);
    return Tha6A0000POHDefaultSet((AtPrbsEngine)self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, HwPrbsEngineEnable);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, POHDefaultSet);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngineTu3Vc);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A000010PrbsEngineTu3VcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, vc);
    }

eAtModulePrbsRet Tha6A0000POHDefaultSet(AtPrbsEngine self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    uint8 startSts, sts_i;
    eAtModulePrbsRet ret = cAtOk;

    startSts = AtSdhChannelSts1Get(sdhChannel);
    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        ret |= ThaOcnStsPohInsertEnable(sdhChannel, (uint8)(startSts + sts_i), cAtTrue);
    if (AtSdhPathHasPohProcessor((AtSdhPath)sdhChannel))
		{
		ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
		ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);
		}

    return ret;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngineNxDs0.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : PRBS of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "../Tha6A000010PrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngineNxDs0);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    }

AtPrbsEngine Tha6A000010PrbsEngineNxDs0V2ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineV2ObjectInit(self, (AtChannel)nxDs0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A000010PrbsEngineNxDs0V2New(AtPdhNxDS0 nxDs0)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010PrbsEngineNxDs0V2ObjectInit(newEngine, nxDs0);
    }

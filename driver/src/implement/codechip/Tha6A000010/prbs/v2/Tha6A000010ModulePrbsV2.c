/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010ModulePrbs.c
 *
 * Created Date: Sep 8, 2015
 *
 * Description : PRBS module of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../Tha6A000010ModulePrbsInternal.h"

#include "../../../../default/pw/ThaModulePw.h"
#include "../Tha6A000010PrbsEngine.h"
#include "../../../../default/att/ThaAttPrbsManager.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tAtModuleMethods      m_AtModuleOverride;
static tTha6A000010ModulePrbsMethods m_Tha6A000010ModulePrbsOverride;
/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedShiftUp(Tha6A000010ModulePrbs self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static uint32 NumPwEachSlice(void)
    {
    return 2048;
    }

static uint32 NumSlice(void)
    {
    return 8;
    }

static AtAttPrbsManager AttPrbsManagerCreate(AtModulePrbs self)
    {
    AtUnused(self);
    return ThaAttPrbsManagerNew(self);
    }

static eAtRet SdhLineInit(Tha6A000010ModulePrbs _self)
    {
    AtUnused(_self);
    return cAtOk;
    }

static eAtRet SdhHoPathInit(Tha6A000010ModulePrbs _self)
    {
    AtUnused(_self);
    return cAtOk;
    }

static eAtRet SdhLoPathInit(Tha6A000010ModulePrbs _self)
    {
    AtUnused(_self);
    return cAtOk;
    }          
    
static eAtRet PdhDe1Init(Tha6A000010ModulePrbs _self)
    {
    AtUnused(_self);
    return cAtOk;
    }
static eAtRet PdhDe3Init(Tha6A000010ModulePrbs _self)
    {
    AtUnused(_self);
    return cAtOk;
    }
        
static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;
    uint32 slice, pwId;
    ret |= m_AtModuleMethods->Init(self);
    for (slice = 0; slice < NumSlice(); slice++)
        {
        for (pwId = 0; pwId < NumPwEachSlice(); pwId++)
            ((Tha6A000010ModulePrbs)self)->PwIdUsed[slice][pwId] = cAtFalse;
        }

    return ret;
    }
    
uint32 Tha6A000010ModulePrbsGetFreePw(Tha6A000010ModulePrbs self, uint8 slice)
    {
    uint32 pwId;
    for (pwId = 0; pwId < NumPwEachSlice(); pwId++)
        {
        if (((Tha6A000010ModulePrbs)self)->PwIdUsed[slice][pwId]==cAtFalse)
            return pwId;
        }
    return 0xFFFFFFFF;
    }

eAtRet Tha6A000010ModulePrbsPwUsedSet(Tha6A000010ModulePrbs self, uint8 slice, uint32 pwId, eBool used)
    {
    if (slice >= NumSlice()|| pwId >= NumPwEachSlice())
        return cAtError;
    ((Tha6A000010ModulePrbs)self)->PwIdUsed[slice][pwId] = used;
    return cAtOk;
    }


static eBool NeedPwInitConfig(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }    
static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A000010PrbsEngineDe1V2New(de1);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A000010PrbsEngineNxDs0V2New(nxDs0);
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);
    AtUnused(engineId);

    if (channelType == cAtSdhChannelTypeVc4 || channelType ==cAtSdhChannelTypeVc4_4c || channelType ==cAtSdhChannelTypeVc4_16c)
        return Tha6A000010PrbsEngineHoVcV2New(sdhVc);

    if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            return Tha6A000010PrbsEngineHoVcV2New(sdhVc);
        return Tha6A000010PrbsEngineTu3VcV2New(sdhVc);
        }

    if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        return Tha6A000010PrbsEngineVc1xV2New(sdhVc);

    return NULL;
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A000010PrbsEngineDe3V2New(de3);
    }

static eBool AllChannelsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "vc3, vc4, vc1x, de1, nxds0";
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, NeedPwInitConfig);
        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, AllChannelsSupported);
        mMethodOverride(m_AtModulePrbsOverride, AttPrbsManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }
static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModulePrbs(self);
    OverrideAtModule(self);
    }
    
static void MethodsInit(Tha6A000010ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PdhDe3Init);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PdhDe1Init);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, SdhLineInit);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, SdhLoPathInit);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, SdhHoPathInit);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, NeedShiftUp);
        }

    mMethodsSet(self, &m_Tha6A000010ModulePrbsOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010ModulePrbs);
    }

AtModulePrbs Tha6A000010ModulePrbsV2ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha6A000010ModulePrbs)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A000010ModulePrbsV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010ModulePrbsV2ObjectInit(newModule, device);
    }

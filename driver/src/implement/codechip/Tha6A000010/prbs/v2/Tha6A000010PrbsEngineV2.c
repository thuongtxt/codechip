/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngine.c
 *
 * Created Date: Dec 2, 2015
 *
 * Description : LO-Path PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../Tha6A000010PrbsEngineReg.h"
#include "../../../../default/pdh/ThaPdhDe1.h"
#include "../../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../../Tha60210011/map/Tha60210011ModuleMap.h"
#include "../../../../../../include/sdh/AtSdhChannel.h"
#include "../../../../../../include/man/AtModule.h"
#include "../../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../../default/map/ThaModuleStmMapInternal.h"
#include "../../../../default/cdr/ThaModuleCdrStm.h"
#include "../Tha6A000010ModulePrbsInternal.h"
#include "../Tha6A000010PrbsEngineInternal.h"
#include "../../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../../../util/coder/AtCoderUtil.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A000010PrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8  m_methodsInit = 0;
static tTha6A000010PrbsEngineMethods m_methods;

/* Override */
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/
eBool SdhChannelBelongsToLoLine(AtSdhChannel sdhChannel);
eBool SdhChannelIsVc3(AtSdhChannel sdhChannel);
eBool SdhChannelIsVcx(AtSdhChannel sdhChannel);
eBool SdhChannelIsTu3Vc3(AtSdhChannel sdhChannel);

/*--------------------------- Implementation ---------------------------------*/

static uint8 PrbsEngineSliceGet(AtPrbsEngine self, AtChannel channel)
	{
	AtModule module = AtChannelModuleGet(channel);
	uint8 slice, hwSts;
	eAtModule moduleType = AtModuleTypeGet(module);

	AtUnused(self);
	if (moduleType==cAtModuleSdh)
		{
		uint8 stsId =  AtSdhChannelSts1Get((AtSdhChannel)channel);
		ThaSdhChannelHwStsGet((AtSdhChannel)channel, cThaModuleMap, stsId, &slice, &hwSts);
		}
	else
		{
		eAtPdhChannelType pdhChannelType = AtPdhChannelTypeGet((AtPdhChannel)channel);
		if (pdhChannelType==cAtPdhChannelTypeE3 ||pdhChannelType== cAtPdhChannelTypeDs3)
			ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleMap, &slice, &hwSts);
		else
			{
			if  (pdhChannelType==cAtPdhChannelTypeE1 || pdhChannelType==cAtPdhChannelTypeDs1)
				ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleMap, &slice, &hwSts);
			else if (pdhChannelType==cAtPdhChannelTypeNxDs0)
				ThaPdhChannelHwIdGet((AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0) channel), cThaModuleMap, &slice, &hwSts);
			}
		}
	return slice;
	}

static Tha6A000010ModulePrbs PrbsModule(Tha6A000010PrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (Tha6A000010ModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    }

eBool SdhChannelIsVc3(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType type;
    type = AtSdhChannelTypeGet(sdhChannel);
    if ((type == cAtSdhChannelTypeVc3) &&
        (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeAu3))
        return cAtTrue;
    return cAtFalse;
    }

eBool SdhChannelIsTu3Vc3(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType type;
    type = AtSdhChannelTypeGet(sdhChannel);
    if ((type == cAtSdhChannelTypeVc3) &&
        (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeTu3))
        return cAtTrue;
    return cAtFalse;
    }

eBool SdhChannelIsVcx(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType type;
    type = AtSdhChannelTypeGet(sdhChannel);
    if ((type == cAtSdhChannelTypeVc3)     ||
		(type == cAtSdhChannelTypeVc4)     ||
		(type == cAtSdhChannelTypeVc4_4c)  ||
        (type == cAtSdhChannelTypeVc4_16c) ||
		(type == cAtSdhChannelTypeVc4_64c) ||
        (type == cAtSdhChannelTypeVc4_nc))
        return cAtTrue;

    return cAtFalse;
    }

eBool SdhChannelBelongsToLoLine(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType type;

    type = AtSdhChannelTypeGet(sdhChannel);
    if ((type == cAtSdhChannelTypeVc4)     ||
		(type == cAtSdhChannelTypeVc4_4c)  ||
        (type == cAtSdhChannelTypeVc4_16c) ||
		(type == cAtSdhChannelTypeVc4_64c) ||
		(type == cAtSdhChannelTypeVc4_16nc) ||
        (type == cAtSdhChannelTypeVc4_nc))
        return cAtFalse;

    if ((type == cAtSdhChannelTypeVc3) &&
        (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeAu3))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 MaxDelayReg(AtPrbsEngine self, AtChannel channel, eBool r2c)
	{
	AtModule module = AtChannelModuleGet(channel);
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8 slice = PrbsEngineSliceGet(self, channel);
	eAtModule moduleType = AtModuleTypeGet(module);
	if (moduleType == cAtModuleSdh)
		{
		if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
			return mMethodsGet(prbs)->PrbsMaxDelayReg(prbs, cAtTrue, r2c, slice);
		}
	return mMethodsGet(prbs)->PrbsMaxDelayReg(prbs, cAtFalse, r2c, slice);
	}

static uint32 MinDelayReg(AtPrbsEngine self, AtChannel channel, eBool r2c)
	{
	AtModule module = AtChannelModuleGet(channel);
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8 slice = PrbsEngineSliceGet(self, channel);
	eAtModule moduleType = AtModuleTypeGet(module);
	if (moduleType == cAtModuleSdh)
		{
		if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
			return mMethodsGet(prbs)->PrbsMinDelayReg(prbs, cAtTrue, r2c, slice);
		}
	return mMethodsGet(prbs)->PrbsMinDelayReg(prbs, cAtFalse, r2c, slice);
	}

static uint32 AverageDelayReg(AtPrbsEngine self, AtChannel channel, eBool r2c)
	{
	AtModule module = AtChannelModuleGet(channel);
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8 slice = PrbsEngineSliceGet(self, channel);
	eAtModule moduleType = AtModuleTypeGet(module);
	if (moduleType == cAtModuleSdh)
		{
		if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
			return mMethodsGet(prbs)->PrbsAverageDelayReg(prbs, cAtTrue, r2c, slice);
		}
	return mMethodsGet(prbs)->PrbsAverageDelayReg(prbs, cAtFalse, r2c, slice);
	}

static float ConvertToMs(AtPrbsEngine self, uint32 value)
	{
	AtPw pw = ((Tha6A000010PrbsEngine)self)->pw;

	if (Tha60210011PwCircuitBelongsToLoLine(pw))
		return (float)value/155000;

	return (float)value/311000;
	}

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngine);
    }

static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtUnused(pwId);
    AtUnused(self);
    /* Concrete class should do */
    return NULL;
    }

static uint32 HoPrbsEngineOffset(AtPrbsEngine self, AtChannel sdhChannel)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8 slice, hwSts;
    uint8 stsId =  AtSdhChannelSts1Get((AtSdhChannel)sdhChannel);
    ThaSdhChannelHwStsGet((AtSdhChannel)sdhChannel, cThaModuleMap, stsId, &slice, &hwSts);
    AtUnused(self);
    if (Tha6A000010ModulePrbsNeedShiftUp(prbs))
        return  (uint32)((2048*slice)+hwSts);
    return  (uint32)((1024*slice)+hwSts);
    }

static uint32 LoPrbsEngineOffset(AtPrbsEngine self, AtChannel channel)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    uint8 stsId =0,slice = 0, hwSts=0, slice24=0, slice48=0;
    AtUnused(self);
    if (moduleType==cAtModuleSdh)
        {
        stsId=  AtSdhChannelSts1Get((AtSdhChannel)channel);
        ThaSdhChannelHwStsGet((AtSdhChannel)channel, cThaModuleMap, stsId, &slice, &hwSts);
        }
    else
        {
        eAtPdhChannelType pdhChannelType = AtPdhChannelTypeGet((AtPdhChannel)channel);
        if (pdhChannelType==cAtPdhChannelTypeE3 ||pdhChannelType== cAtPdhChannelTypeDs3)
            ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleMap, &slice, &hwSts);
        else
            {
            if  (pdhChannelType==cAtPdhChannelTypeE1 || pdhChannelType==cAtPdhChannelTypeDs1)
                ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleMap, &slice, &hwSts);
            else if (pdhChannelType==cAtPdhChannelTypeNxDs0)
                ThaPdhChannelHwIdGet((AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0) channel), cThaModuleMap, &slice, &hwSts);
            }
        }

    slice24 = slice & cBit0;
    slice48 = (slice >> 1) & cBit2_0;
    if (mThis(self)->pwId==cInvalidUint32)
        return cInvalidUint32;
    /*AtPrintf("PRBS %s pwID=%d slice=%d hwSts=%d slice48=%d slice24=%d\r\n",AtObjectToString((AtObject)sdhChannel), mThis(self)->pwId, slice, hwSts,slice48, slice24);*/
    if (Tha6A000010ModulePrbsNeedShiftUp(prbs))
        return (uint32)(65536*2*(uint32)slice48+2048*(uint32)slice24+mThis(self)->pwId);
    return (uint32)(0x20000*(uint32)slice48+1024*(uint32)slice24+mThis(self)->pwId);
    }

static eAtModulePrbsRet HoPathPrbsEngineMonitorEnable(AtPrbsEngine self, AtChannel sdhChannel, eBool enable)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base       = Tha6A000010ModulePrbsGenReg(prbs, cAtTrue);
    uint32 address    = base + HoPrbsEngineOffset(self, sdhChannel);
    uint32 regValue   = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 regFieldMask = 0, regFieldShift = 0;

    regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtTrue, cAtTrue);
	regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regValue, regField, mBoolToBin(enable));
    AtPrbsEngineWrite(self, address, regValue, cThaModuleMap);
    return cAtOk;
    }

static eAtModulePrbsRet LoPathPrbsEngineMonitorEnable(AtPrbsEngine self,AtChannel sdhChannel, eBool enable)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base    = Tha6A000010ModulePrbsGenReg(prbs, cAtFalse);
    uint32 offset  = LoPrbsEngineOffset(self, sdhChannel);
    uint32 regFieldMask = 0, regFieldShift = 0;
    if (offset==cInvalidUint32)
        return cAtOk;
    else
        {
        uint32 address = base + offset;
        uint32 regValue = AtPrbsEngineRead(self, address, cThaModuleMap);
        regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtFalse, cAtTrue);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regValue, regField, mBoolToBin(enable));
        AtPrbsEngineWrite(self, address, regValue, cThaModuleMap);
        }
    return cAtOk;
    }

static eAtModulePrbsRet HwPrbsEngineEnable(AtPrbsEngine self, AtChannel channel, eBool enable)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (AtSdhChannelIsHoVc((AtSdhChannel)channel))
            return HoPathPrbsEngineMonitorEnable(self, channel, enable);
        }
    return LoPathPrbsEngineMonitorEnable(self, channel, enable);
    }

static eBool LoPathPrbsEngineMonitorIsEnabled(AtPrbsEngine self, AtChannel sdhChannel)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base    = Tha6A000010ModulePrbsGenReg(prbs, cAtFalse);
    uint32 offset  = LoPrbsEngineOffset(self, sdhChannel);
    uint32 regFieldMask = 0, regFieldShift = 0;

    if (offset == cInvalidUint32)
        return cAtFalse;
    else
        {
        uint32 address = base + offset;
        uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
        regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtFalse, cAtTrue);
		regFieldShift = AtRegMaskToShift(regFieldMask);
        return mRegField(regValue, regField) ? cAtTrue : cAtFalse;
        }
    }

static eBool HoPathPrbsEngineMonitorIsEnabled(AtPrbsEngine self, AtChannel sdhChannel)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base    = Tha6A000010ModulePrbsGenReg(prbs, cAtTrue);
    uint32 address = base + HoPrbsEngineOffset(self, sdhChannel);
    uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 regFieldMask = 0, regFieldShift = 0;

    regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtTrue, cAtTrue);
	regFieldShift = AtRegMaskToShift(regFieldMask);
    return mRegField(regValue, regField) ? cAtTrue : cAtFalse;
    }

static eBool HwPrbsEngineIsEnabled(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);

    if (moduleType == cAtModuleSdh)
        {
        if (AtSdhChannelIsHoVc((AtSdhChannel)channel))
            return HoPathPrbsEngineMonitorIsEnabled(self, channel);
        }
    return LoPathPrbsEngineMonitorIsEnabled(self, channel);
    }

static eAtModulePrbsRet SdhEnable(AtPrbsEngine self, eBool enable, ThaModuleAbstractMap moduleMap, ThaModuleAbstractMap moduleDemap, Tha6A000010ModulePrbs prbs)
    {
    eAtRet ret = cAtOk;
    AtChannel channel = AtPrbsEngineChannelGet(self);
    if (SdhChannelIsVcx((AtSdhChannel)channel))
        {
        uint32 pwId = cInvalidUint32;
        if (SdhChannelIsTu3Vc3((AtSdhChannel)channel))
           {
           uint8 hwSlice=0, hwSts=0;
           ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cThaModuleMap, &hwSlice, &hwSts);
           if (mThis(self)->pwId == cInvalidUint32)
                {
                pwId=Tha6A000010ModulePrbsGetFreePw(prbs, hwSlice);
                if (pwId==cInvalidUint32)
                    return cAtError;
                mThis(self)->pwId =pwId;
                self->engineId = pwId;
                }
           pwId = mThis(self)->pwId;
           }
        else
           pwId = enable?0:cInvalidUint32;

        ret |= mMethodsGet(moduleDemap)->HwBindAuVcToPseudowire(moduleDemap, (AtSdhVc)channel, pwId);
        ret |= mMethodsGet(moduleDemap)->VcxEncapConnectionEnable(moduleDemap, (AtSdhVc)channel, enable);

        /*Map*/
        ret |= mMethodsGet(moduleMap)->HwBindAuVcToPseudowire(moduleMap, (AtSdhVc)channel, pwId);
        ret |= mMethodsGet(moduleMap)->VcxEncapConnectionEnable(moduleMap, (AtSdhVc)channel, enable);

        if (SdhChannelIsTu3Vc3((AtSdhChannel)channel))
           {
           AtDevice device = AtChannelDeviceGet((AtChannel)channel);
           ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
           uint8 hwSlice=0, hwSts=0;
           ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cThaModuleMap, &hwSlice, &hwSts);
           ThaCdrVc3CepModeEnable(cdrModule, (AtSdhChannel)channel, AtSdhChannelSts1Get((AtSdhChannel)channel), enable);
           Tha6A000010ModulePrbsPwUsedSet(prbs, hwSlice, pwId,enable?cAtTrue:cAtFalse);
           }

        }
    else
        {
        uint8 hwSlice=0, hwSts=0;
        uint32 pwId =cInvalidUint32;
        ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cThaModuleMap, &hwSlice, &hwSts);
        if (mThis(self)->pwId == cInvalidUint32)
            {
            pwId=Tha6A000010ModulePrbsGetFreePw(prbs, hwSlice);
            if (pwId==cInvalidUint32)
                return cAtError;
            mThis(self)->pwId =pwId;
            self->engineId = pwId;
            }
        pwId = mThis(self)->pwId;
        /*AtPrintf("PRBS %s pwID=%d\r\n",AtObjectToString((AtObject)channel), pwId);*/
        ret |= mMethodsGet(moduleDemap)->HwBindVc1xToPseudowire(moduleDemap, (AtSdhVc)channel, enable?pwId:cInvalidUint32);
        ret |= mMethodsGet(moduleDemap)->Vc1xEncapConnectionEnable(moduleDemap, (AtSdhVc)channel, enable);

        /*Map*/
        ret |= mMethodsGet(moduleMap)->HwBindVc1xToPseudowire(moduleMap, (AtSdhVc)channel, enable?pwId:cInvalidUint32);
        ret |= mMethodsGet(moduleMap)->Vc1xEncapConnectionEnable(moduleMap, (AtSdhVc)channel, enable);
        Tha6A000010ModulePrbsPwUsedSet(prbs, hwSlice, pwId,enable?cAtTrue:cAtFalse);
        if (enable==cAtFalse)
            {
            mThis(self)->pwId = cInvalidUint32;
            self->engineId = 0;
            }
        }
    return ret;
    }

static eAtModulePrbsRet PdhEnable(AtPrbsEngine self, eBool enable, ThaModuleAbstractMap moduleMap, ThaModuleAbstractMap moduleDemap, Tha6A000010ModulePrbs prbs)
    {
    eAtRet ret = cAtOk;
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtChannel sdhChannel=NULL;
    eAtPdhChannelType pdhChannelType = AtPdhChannelTypeGet((AtPdhChannel)channel);
    if (pdhChannelType==cAtPdhChannelTypeE3 ||pdhChannelType== cAtPdhChannelTypeDs3)
        {
        uint8 hwSlice, hwSts;
        uint32 pwId = cInvalidUint32;
        sdhChannel= (AtChannel)AtPdhChannelVcInternalGet((AtPdhChannel)channel);
        ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModuleMap, &hwSlice, &hwSts);
        if (mThis(self)->pwId == cInvalidUint32)
            {
            pwId =Tha6A000010ModulePrbsGetFreePw(prbs, hwSlice);
            if (pwId==cInvalidUint32)
                {
                AtPrintf("hwSlice=%d is FULL\n",hwSlice);
                return cAtError;
                }
            mThis(self)->pwId =pwId;
            self->engineId = pwId;
            }
        pwId = mThis(self)->pwId;

        ret |= mMethodsGet(moduleDemap)->HwBindDe3ToPseudowire(moduleDemap, (AtPdhDe3)channel,enable?pwId:cInvalidUint32);
        ret |= mMethodsGet(moduleDemap)->De3EncapConnectionEnable(moduleDemap, (AtPdhDe3)channel, enable);

        /*Map*/
        ret |= mMethodsGet(moduleMap)->HwBindDe3ToPseudowire(moduleMap,   (AtPdhDe3)channel,enable?pwId:cInvalidUint32);
        ret |= mMethodsGet(moduleMap)->De3EncapConnectionEnable(moduleMap,(AtPdhDe3)channel, enable);
        Tha6A000010ModulePrbsPwUsedSet(prbs, hwSlice, pwId, enable?cAtTrue:cAtFalse);
        if (enable==cAtFalse)
            {
            mThis(self)->pwId = cInvalidUint32;
            self->engineId = 0;
            }

        }
    else
        {
        AtPdhDe1 de1 = NULL;
        uint32 ds0BitMask = 0;
        uint8 hwSlice, hwSts;
        uint32 pwId=cInvalidUint32;

        if      (pdhChannelType==cAtPdhChannelTypeE1 || pdhChannelType==cAtPdhChannelTypeDs1)
            {
            ds0BitMask= ThaPdhDe1EncapDs0BitMaskGet((ThaPdhDe1)channel);
            /*ds0BitMask = cBit31_0;*/
            de1        = (AtPdhDe1)channel;
            sdhChannel = (AtChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
            }
        else if (pdhChannelType==cAtPdhChannelTypeNxDs0)
            {
            ds0BitMask =  AtPdhNxDS0BitmapGet((AtPdhNxDS0) channel);
            de1        = AtPdhNxDS0De1Get((AtPdhNxDS0) channel);
            }

        sdhChannel =  (AtChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
        if (sdhChannel==NULL)
            {
            AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)de1);
            AtPdhChannel de3 = AtPdhChannelParentChannelGet(de2);
            sdhChannel = (AtChannel)AtPdhChannelVcInternalGet(de3);
            }

        ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModuleMap, &hwSlice, &hwSts);
        if (mThis(self)->pwId == cInvalidUint32)
            {
            pwId = Tha6A000010ModulePrbsGetFreePw(prbs, hwSlice);
            if (pwId==cInvalidUint32)
                {
                AtPrintf("hwSlice=%d is FULL\n",hwSlice);
                return cAtError;
                }

            mThis(self)->pwId =pwId;
            self->engineId = pwId;
            }
        pwId = mThis(self)->pwId;
        ret |= mMethodsGet(moduleDemap)->HwBindDs0ToPseudowire(moduleDemap, (AtPdhDe1)de1, ds0BitMask, enable?pwId:cInvalidUint32);
        ret |= mMethodsGet(moduleDemap)->Ds0EncapConnectionEnable(moduleDemap,(AtChannel)de1, ds0BitMask, enable);

        /*Map*/
        ret |= mMethodsGet(moduleMap)->HwBindDs0ToPseudowire(moduleMap, (AtPdhDe1)de1, ds0BitMask, enable?pwId:cInvalidUint32);
        ret |= mMethodsGet(moduleMap)->Ds0EncapConnectionEnable(moduleMap, (AtChannel)de1, ds0BitMask, enable);
        Tha6A000010ModulePrbsPwUsedSet(prbs, hwSlice, pwId, enable?cAtTrue:cAtFalse);
        if (enable==cAtFalse)
            {
            mThis(self)->pwId = cInvalidUint32;
            self->engineId = 0;
            }
        }
    return ret;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    AtDevice device = AtChannelDeviceGet(channel);
    ThaModuleAbstractMap moduleMap =(ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ThaModuleAbstractMap moduleDemap =(ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    Tha6A000010ModulePrbs prbs = (Tha6A000010ModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);

    /* Enable corresponding PW PRBS engine */
    /*Demap*/
    if (moduleType == cAtModuleSdh)
        ret |= SdhEnable(self, enable, moduleMap, moduleDemap, prbs);
    else if (moduleType == cAtModulePdh)
        ret |= PdhEnable(self, enable, moduleMap, moduleDemap, prbs);

    ret |= HwPrbsEngineEnable(self, channel, enable);
    ret |= AtPrbsEngineTxEnable(self, enable);
    ret |= AtPrbsEngineRxEnable(self, enable);
    if (enable)
        {
        ret |= AtPrbsEngineTxModeSet(self, cAtPrbsModePrbs15);
        ret |= AtPrbsEngineRxModeSet(self, cAtPrbsModePrbs15);
        ret |= AtPrbsEngineGeneratingSideSet(self,cAtPrbsSideTdm);
        ret |= AtPrbsEngineMonitoringSideSet(self,cAtPrbsSideTdm);
        }
    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
	AtChannel channel = AtPrbsEngineChannelGet(self);
    return HwPrbsEngineIsEnabled(self, channel);
    }

static uint32 LoPathPrbsEngineAlarmClear(AtPrbsEngine self, AtChannel sdhChannel)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 alarm = 0;
    uint32 base = Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 offset  = LoPrbsEngineOffset(self, sdhChannel);
    if (offset == cInvalidUint32)
        return alarm;
    else
        {
        uint32 address = base + offset;
        uint32 regValue  = AtPrbsEngineRead(self, address, cAtModulePrbs);
        return  regValue;
        }
    }

static uint32 LoPathPrbsEngineAlarmGet(AtPrbsEngine self, AtChannel sdhChannel)
    {
	uint32 alarm = 0;
	uint32 regValue  = LoPathPrbsEngineAlarmClear(self, sdhChannel);

	if (regValue & c_prbslomon_PrbsLoMonErr_Mask)
		alarm |= cAtPrbsEngineAlarmTypeError;

	if ((regValue & c_prbslomon_PrbsLoMonSync_Mask) == 0)
		alarm |= cAtPrbsEngineAlarmTypeLossSync;

    return alarm;
    }

static uint32 HoPathPrbsEngineAlarmClear(AtPrbsEngine self, AtChannel sdhChannel)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint32 base = Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 address = base + HoPrbsEngineOffset(self, sdhChannel);
    uint32 regValue  = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return regValue;
    }

static uint32 HoPathPrbsEngineAlarmGet(AtPrbsEngine self, AtChannel sdhChannel)
    {
    uint32 alarm = 0;
    uint32 regValue  = HoPathPrbsEngineAlarmClear(self, sdhChannel);

    if (regValue & c_prbshomon_PrbsHoMonErr_Mask)
        alarm |= cAtPrbsEngineAlarmTypeError;

    if ((regValue & c_prbshomon_PrbsHoMonSync_Mask) == 0)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;

    return alarm;
    }

static uint32 HwPrbsEngineAlarmGet(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoPathPrbsEngineAlarmGet(self, channel);
        }

    return LoPathPrbsEngineAlarmGet(self, channel);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
	AtChannel channel = AtPrbsEngineChannelGet(self);

    return HwPrbsEngineAlarmGet(self, channel);
    }

static uint32 HwPrbsEngineAlarmClear(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoPathPrbsEngineAlarmClear(self, channel);
        }
    return LoPathPrbsEngineAlarmClear(self, channel);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
	{
	AtChannel channel = AtPrbsEngineChannelGet(self);
	HwPrbsEngineAlarmClear(self, channel);
    return 0;
	}

static eBool ModeIsSupported(Tha6A000010PrbsEngine self, eAtPrbsMode prbsMode)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule(self);
	return Tha6A000010ModulePrbsModeIsSupported(prbs, prbsMode);
    }

static eAtModulePrbsRet LoPathPrbsEngineModeSet(AtPrbsEngine self, AtChannel sdhChannel, eAtPrbsMode prbsMode, eBool isTxDirection)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtFalse)  : Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 offset  = LoPrbsEngineOffset(self, sdhChannel);
    uint32 regFieldMask = 0, regFieldShift = 0;
    if (!ModeIsSupported(mThis(self), prbsMode))
        return cAtErrorModeNotSupport;
    if (offset == cInvalidUint32)
        return cAtOk;
    else
        {
        uint32 regVal;
        /* Mode 0: PRBS 15 and 1: SEQ */
        address = address + offset;
        regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

        regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtFalse, isTxDirection);
		regFieldShift = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, Tha6A000010ModulePrbsModeToHwValue(prbs, prbsMode));

		regFieldMask = Tha6A000010ModulePrbsStepMask(prbs, cAtFalse, isTxDirection);
		if (regFieldMask != cInvalidUint32)
			{
			regFieldShift = AtRegMaskToShift(regFieldMask);
			mRegFieldSet(regVal, regField,  Tha6A000010ModulePrbsHwStepFromPrbsMode(prbs, prbsMode));
			}

        AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
        }
    return cAtOk;
    }

static eAtModulePrbsRet HoPathPrbsEngineModeSet(AtPrbsEngine self, AtChannel sdhChannel, eAtPrbsMode prbsMode, eBool isTxDirection)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address, regValue;
    uint32 hwAddress = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtTrue) : Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 regFieldMask = 0, regFieldShift = 0;

    if (!ModeIsSupported(mThis(self), prbsMode))
        return cAtErrorModeNotSupport;


    address = hwAddress + HoPrbsEngineOffset(self, sdhChannel);
    regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);

    /* 0: PRBS 15 and 1: SEQ */
    regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtTrue, isTxDirection);
	regFieldShift = AtRegMaskToShift(regFieldMask);
    mFieldIns(&regValue, regFieldMask, regFieldShift, Tha6A000010ModulePrbsModeToHwValue(prbs, prbsMode));

    regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtTrue, isTxDirection);
    if (regFieldMask != cInvalidUint32)
		{
		regFieldShift = AtRegMaskToShift(regFieldMask);
		mFieldIns(&regValue, regFieldMask, regFieldShift, Tha6A000010ModulePrbsHwStepFromPrbsMode(prbs, prbsMode));
		}

    AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);
    return cAtOk;
    }

static uint32 HwTxPrbsEngineModeSet(AtPrbsEngine self,AtChannel channel, eAtPrbsMode prbsMode)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoPathPrbsEngineModeSet(self, channel, prbsMode, cAtTrue);
        }
    return LoPathPrbsEngineModeSet(self, channel, prbsMode, cAtTrue);
    }

static uint32 HwRxPrbsEngineModeSet(AtPrbsEngine self, AtChannel channel, eAtPrbsMode prbsMode)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoPathPrbsEngineModeSet(self, channel, prbsMode, cAtFalse);
        }
    return LoPathPrbsEngineModeSet(self, channel, prbsMode, cAtFalse);
    }

static eAtPrbsMode LoPathPrbsEngineModeGet(AtPrbsEngine self, AtChannel sdhChannel, eBool isTxDirection)
    {
	uint32 offset  =  LoPrbsEngineOffset(self, sdhChannel);
    uint8  mode = 0, step = 0;
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
		uint32 address = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtFalse) : Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
        uint32 regVal = AtPrbsEngineRead(self, address +offset, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;

		regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtFalse, isTxDirection);
		regFieldShift = AtRegMaskToShift(regFieldMask);
		mode = (uint8) mRegField(regVal, regField);

		regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtFalse, isTxDirection);
		if (regFieldMask != cInvalidUint32)
			{
			regFieldShift = AtRegMaskToShift(regFieldMask);
			step = (uint8) mRegField(regVal, regField);
			}
		return Tha6A000010ModulePrbsModeFromHwValue(prbs,isTxDirection? mThis(self)->txPrbsMode:mThis(self)->rxPrbsMode, mode, step);
        }
    }

static eAtPrbsMode HoPathPrbsEngineModeGet(AtPrbsEngine self, AtChannel sdhChannel, eBool isTxDirection)
    {
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 hwAddress = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtTrue) : Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 regVal = AtPrbsEngineRead(self, hwAddress + HoPrbsEngineOffset(self, sdhChannel), cAtModulePrbs);
    uint32 regFieldMask = 0, regFieldShift = 0;
	uint8  mode = 0, step = 0;

	regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtTrue, isTxDirection);
	regFieldShift = AtRegMaskToShift(regFieldMask);
	mode = (uint8) mRegField(regVal, regField);

	regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtTrue, isTxDirection);
	if (regFieldMask != cInvalidUint32)
		{
		regFieldShift = AtRegMaskToShift(regFieldMask);
		step = (uint8) mRegField(regVal, regField);
		}
	return Tha6A000010ModulePrbsModeFromHwValue(prbs,isTxDirection? mThis(self)->txPrbsMode:mThis(self)->rxPrbsMode, mode, step);

    }

static eAtPrbsMode HwTxPrbsEngineModeGet(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoPathPrbsEngineModeGet(self, channel, cAtTrue);
        }
    return LoPathPrbsEngineModeGet(self, channel, cAtTrue);
    }

static eAtPrbsMode HwRxPrbsEngineModeGet(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoPathPrbsEngineModeGet(self, channel, cAtFalse);
        }
    return LoPathPrbsEngineModeGet(self, channel, cAtFalse);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtRet ret = AtPrbsEngineTxModeSet(self, prbsMode);

    if (ret != cAtOk)
        return ret;

    return AtPrbsEngineRxModeSet(self, prbsMode);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxModeGet(self);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwTxPrbsEngineModeSet(self, sdhChannel, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
	AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwTxPrbsEngineModeGet(self, sdhChannel);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
	AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwRxPrbsEngineModeSet(self, sdhChannel, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
	AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwRxPrbsEngineModeGet(self, sdhChannel);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
	m_AtObjectMethods->Serialize(self, encoder);
    mEncodeNone(pw);
    mEncodeNone(pwId);
    mEncodeNone(lackCounterValue);
    mEncodeNone(txEnable);
    mEncodeNone(txPrbsMode);
    mEncodeNone(rxEnable);
    mEncodeNone(rxPrbsMode);
    }

static void Delete(AtObject self)
    {
    m_AtObjectMethods->Delete(self);
    }

static AtChannel CircuitToBind(Tha6A000010PrbsEngine self, AtChannel channel)
    {
    AtUnused(self);
    return channel;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtUnused(self);
    return force ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
	mThis(self)->txEnable = enable;
    return cAtOk;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    return mThis(self)->txEnable;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
	mThis(self)->rxEnable = enable;
    return cAtOk;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return mThis(self)->rxEnable;
    }

static uint32 DisruptionTimeGet(AtPrbsEngine self)
    {
    if (mMethodsGet((Tha6A000010PrbsEngine)self)->HasPrbsDisruption((Tha6A000010PrbsEngine)self))
        {
        uint32 disruptionActualTime;
        uint32 address = mMethodsGet((Tha6A000010PrbsEngine)self)->RegDisruption((Tha6A000010PrbsEngine)self) /*+ AtChannelIdGet((AtChannel)mThis(self)->pw)*/;
        uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
        uint32 mask = mMethodsGet((Tha6A000010PrbsEngine)self)->DisruptionMask((Tha6A000010PrbsEngine)self);
        uint32 shift = mMethodsGet((Tha6A000010PrbsEngine)self)->DisruptionShift((Tha6A000010PrbsEngine)self);
        mFieldGet(regVal, mask, shift, uint32, &disruptionActualTime);
        return disruptionActualTime;
        }
    return 0;
    }

static uint32 HoRxFixedPatternGet(AtPrbsEngine self, AtChannel channel)
    {
    uint32 offset    = HoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtTrue, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;
        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtFalse);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal, regField);
        }
    return 0;
    }

static uint32 LoRxFixedPatternGet(AtPrbsEngine self, AtChannel channel)
    {
    uint32 offset  =  LoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
       return cAtPrbsModePrbs15;
    else
       {
       Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
       uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse);
       uint32 regAddr   = hwAddress + offset;
       uint32 regVal    = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
       uint32 regFieldMask = 0, regFieldShift = 0;

       regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
	   regFieldShift  = AtRegMaskToShift(regFieldMask);
       return mRegField(regVal, regField);
       }
    return 0;
    }

static uint32 HwRxFixedPatternGet(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);

    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoRxFixedPatternGet(self, channel);
        }
    return LoRxFixedPatternGet(self, channel);
    }

static uint32 RxCurrentFixedPatternGet(AtPrbsEngine self)
    {
    AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwRxFixedPatternGet(self, sdhChannel);
    }

static eAtModulePrbsRet HoTxFixedPatternSet(AtPrbsEngine self, AtChannel channel, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;
    uint32 offset  =  HoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtTrue, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;

        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtTrue);
	    regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, fixedPattern);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static eAtModulePrbsRet LoTxFixedPatternSet(AtPrbsEngine self, AtChannel channel, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;
    uint32 offset  =  LoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtFalse, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;

        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, fixedPattern);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static eAtModulePrbsRet HwTxFixedPatternSet(AtPrbsEngine self, AtChannel channel, uint32 fixedPattern)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);

    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoTxFixedPatternSet(self, channel, fixedPattern);
        }
    return LoTxFixedPatternSet(self, channel, fixedPattern);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwTxFixedPatternSet(self, sdhChannel, fixedPattern);
    }

static uint32 HoTxFixedPatternGet(AtPrbsEngine self, AtChannel channel)
    {
    uint32 offset  =  HoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtOk;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtTrue, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;

        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal, regField);
        }
    return 0;
    }

static uint32 LoTxFixedPatternGet(AtPrbsEngine self, AtChannel channel)
    {
    uint32 offset  =  LoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtOk;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtFalse, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;
        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal, regField);
        }
    return 0;
    }

static uint32 HwTxFixedPatternGet(AtPrbsEngine self, AtChannel channel)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);
    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoTxFixedPatternGet(self, channel);
        }
    return LoTxFixedPatternGet(self, channel);
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwTxFixedPatternGet(self, sdhChannel);
    }

static eAtModulePrbsRet HoRxFixedPatternSet(AtPrbsEngine self, AtChannel channel, uint32 fixedPattern)
    {
    uint32 offset  =  HoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtTrue, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;
		regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtFalse);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, fixedPattern);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        return cAtOk;
        }
    }

static eAtModulePrbsRet LoRxFixedPatternSet(AtPrbsEngine self, AtChannel channel, uint32 fixedPattern)
    {
    uint32 offset  =  LoPrbsEngineOffset(self, channel);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
    	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    	uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask = 0, regFieldShift = 0;
		regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, fixedPattern);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        return cAtOk;
        }
    }

static uint32 HwRxFixedPatternSet(AtPrbsEngine self, AtChannel channel, uint32 fixedPattern)
    {
    AtModule module = AtChannelModuleGet(channel);
    eAtModule moduleType = AtModuleTypeGet(module);

    if (moduleType == cAtModuleSdh)
        {
        if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
        	return HoRxFixedPatternSet(self, channel, fixedPattern);
        }
    return LoRxFixedPatternSet(self, channel, fixedPattern);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwRxFixedPatternSet(self, sdhChannel, fixedPattern);
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    AtChannel sdhChannel = AtPrbsEngineChannelGet(self);
    return HwRxFixedPatternGet(self, sdhChannel);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet ret = cAtOk;
    ret |= AtPrbsEngineTxFixedPatternSet(self, fixedPattern);
    ret |= AtPrbsEngineRxFixedPatternSet(self, fixedPattern);
    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxFixedPatternGet(self);
    }

static uint32 RegClearStatus(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eBool  HasPrbsModeChange(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool  HasPrbsDisruption(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32  RegDisruption(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint32  DisruptionMask(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint8  DisruptionShift(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(Tha6A000010PrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwCreate);
        mMethodOverride(m_methods, CircuitToBind);

        mMethodOverride(m_methods, HasPrbsModeChange);
        mMethodOverride(m_methods, RegClearStatus);

        mMethodOverride(m_methods, HasPrbsDisruption);
        mMethodOverride(m_methods, RegDisruption);
        mMethodOverride(m_methods, DisruptionMask);
        mMethodOverride(m_methods, DisruptionShift);        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    if (invert==cAtTrue)
        return cAtErrorModeNotSupport;
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    return AtPrbsEngineTxInvert(self, invert);
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    return AtPrbsEngineTxInvert(self, invert);
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

/* Counters */
static uint32 HoHwCounterGet(AtPrbsEngine self, AtChannel channel,uint16 counterType, eBool r2c, eBool isLatch)
	{
	uint32 offset  =  HoPrbsEngineOffset(self, channel);
	if (offset==cInvalidUint32)
		return 0;
	else
		{
		Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
		uint32 hwAddress = Tha6A000010ModulePrbsCounterReg(prbs, cAtTrue, r2c);
		uint32 regAddr   = hwAddress + offset;
		uint32 regVal    = 0;
		uint32 regFieldMask=0, regFieldShift=0;
		if (isLatch && counterType==cAtPrbsEngineCounterRxSync)
			{
			regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
			((Tha6A000010PrbsEngine)self)->lackCounterValue = regVal;
			}
		else
			regVal = ((Tha6A000010PrbsEngine)self)->lackCounterValue;

		regFieldMask   = Tha6A000010ModulePrbsCounterMask(prbs, counterType);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		return mRegField(regVal, regField);
		}
	}

static uint32 LoHwCounterGet(AtPrbsEngine self, AtChannel channel, uint16 counterType, eBool r2c, eBool isLatch)
	{
	uint32 offset  =  HoPrbsEngineOffset(self, channel);
	if (offset==cInvalidUint32)
		return 0;
	else
		{
		Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
		uint32 hwAddress = Tha6A000010ModulePrbsCounterReg(prbs, cAtFalse, r2c);
		uint32 regAddr = hwAddress + offset;
		uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
		uint32 regFieldMask=0, regFieldShift=0;

		if (isLatch && counterType==cAtPrbsEngineCounterRxSync)
			{
			regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
			((Tha6A000010PrbsEngine)self)->lackCounterValue = regVal;
			}
		else
			regVal = ((Tha6A000010PrbsEngine)self)->lackCounterValue;

		regFieldMask   = Tha6A000010ModulePrbsCounterMask(prbs, counterType);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		return mRegField(regVal, regField);
		}
	}

static uint32 HwCounterGet(AtPrbsEngine self, uint16 counterType, eBool r2c, eBool isLatch)
	{
	AtChannel channel    = AtPrbsEngineChannelGet(self);
	AtModule  module     = AtChannelModuleGet(channel);
	eAtModule moduleType = AtModuleTypeGet(module);
	if (moduleType == cAtModuleSdh)
		{
		if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
			return HoHwCounterGet(self, channel, counterType, r2c, isLatch);
		}
	return LoHwCounterGet(self, channel, counterType, r2c, isLatch);
	}

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
	{
	if (AtPrbsEngineCounterIsSupported(self, counterType))
		return HwCounterGet(self,counterType, cAtFalse, cAtFalse);

	return 0;
	}

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
	{
	if (AtPrbsEngineCounterIsSupported(self, counterType))
		return HwCounterGet(self,counterType, cAtTrue, cAtFalse);

	return 0;
	}

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
	{
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);

	return Tha6A000010ModuleCounterIsSupported(prbs, counterType);
	}

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
	uint16 counterType_i = 0;
	uint16 counterType[] = {cAtPrbsEngineCounterRxSync,
							cAtPrbsEngineCounterRxErrorFrame};
	for (counterType_i=0; counterType_i< mCount(counterType); counterType_i++)
		{
		if (AtPrbsEngineCounterIsSupported(self, counterType[counterType_i]))
			HwCounterGet(self, counterType[counterType_i], clear, cAtTrue);
    	}

	return cAtOk;
    }

static float DelayGet(AtPrbsEngine self, uint8 mode, eBool r2c)
	{
	uint32 regVal = 0;
	AtChannel channel = AtPrbsEngineChannelGet(self);
	AtModule module = AtChannelModuleGet(channel);
	uint32 regAddr = MaxDelayReg(self,channel, r2c);

	if (mode == cAf6MinDelay)
		regAddr = MinDelayReg(self, channel, r2c);

	if (mode == cAf6AverageDelay)
		{
		ThaAttModulePrbs prbs = (ThaAttModulePrbs)PrbsModule((Tha6A000010PrbsEngine)self);
		if (ThaAttModulePrbsNeedDelayAverage(prbs))
			regAddr = AverageDelayReg(self, channel, r2c);
		else
			regAddr = cInvalidUint32;
		}

	if (regAddr!= cInvalidUint32)
		{
		regVal = mModuleHwRead(module, regAddr);
		if (mode == cAf6MinDelay && regVal == 0xFFFFFFFF)
			regVal = 0;
		return ConvertToMs(self,regVal);
		}

	return 0;
	}

static eAtRet HoHwDelayEnable(AtPrbsEngine self, AtChannel channel, eBool enable)
	{
	uint32 offset  =  HoPrbsEngineOffset(self, channel);
	uint8 slice = PrbsEngineSliceGet(self, channel);
	if (offset==cInvalidUint32)
		return cAtError;
	else
		{
		Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
		uint32 regAddr = Tha6A000010ModulePrbsDelayConfigReg(prbs, cAtTrue, slice);
		uint32 regVal    = 0;
		uint32 regFieldMask=0, regFieldShift=0;

		regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
		regFieldMask   = Tha6A000010ModulePrbsDelayEnableMask(prbs, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, mBoolToBin(enable));

		regFieldMask   = Tha6A000010ModulePrbsDelayIdMask(prbs, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, offset);
	    AtPrbsEngineWrite(self, regAddr, regVal, cThaModuleMap);
		return cAtOk;
		}
	}

static eAtRet LoHwDelayEnable(AtPrbsEngine self, AtChannel channel, eBool enable)
	{
	uint32 offset  =  LoPrbsEngineOffset(self, channel);
	uint8 slice = PrbsEngineSliceGet(self, channel);
	if (offset==cInvalidUint32)
		return cAtError;
	else
		{
		Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
		uint32 regAddr = Tha6A000010ModulePrbsDelayConfigReg(prbs, cAtFalse, slice);
		uint32 regVal    = 0;
		uint32 regFieldMask=0, regFieldShift=0;

		regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
		regFieldMask   = Tha6A000010ModulePrbsDelayEnableMask(prbs, cAtFalse);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, mBoolToBin(enable));

		regFieldMask   = Tha6A000010ModulePrbsDelayIdMask(prbs, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, offset);
	    AtPrbsEngineWrite(self, regAddr, regVal, cThaModuleMap);
		return cAtOk;
		}
	}

static eAtRet DelayEnable(AtPrbsEngine self, eBool enable)
	{
	AtChannel channel    = AtPrbsEngineChannelGet(self);
	AtModule  module     = AtChannelModuleGet(channel);
	eAtModule moduleType = AtModuleTypeGet(module);
	if (moduleType == cAtModuleSdh)
		{
		if (!SdhChannelBelongsToLoLine((AtSdhChannel)channel))
			return HoHwDelayEnable(self, channel, enable);
		}
	return LoHwDelayEnable(self, channel, enable);
	}

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxCurrentFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, DelayGet);
        mMethodOverride(m_AtPrbsEngineOverride, DelayEnable);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6A000010PrbsEngineV2ObjectInit(AtPrbsEngine self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;
    mThis(self)->pwId = cInvalidUint32;
    return self;
    }

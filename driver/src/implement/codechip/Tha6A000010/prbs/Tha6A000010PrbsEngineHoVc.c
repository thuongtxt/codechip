/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngineHoVc.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : High-Order PRBS of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "Tha6A000010PrbsEngineInternal.h"
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290081/prbs/Tha60290081PrbsEngineAuVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPrbsEngineChannelGet(self);

    if (ret != cAtOk)
        return ret;

    return Tha6A0000PrbsEnginePOHDefaultSet(self, sdhChannel);
    }

static eBool HwPrbsEngineEnable(Tha6A000010PrbsEngine self, AtPw pw, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    AtSdhVc vcx = (AtSdhVc)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaModuleAbstractMap mapDemapModule;
    eAtRet ret = cAtOk;

    if (Tha6A000010PrbsEngineHasPrbsHoBus(self))
        return mMethodsGet(self)->HwPrbsEngineEnable(self, pw, enable);

    /*mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);*/
    Tha60290081PrbsEngineAuVcHoBertHwDefault((ThaPrbsEngine)self, (AtSdhChannel)vcx);
  /*  Tha60290081ModuleMapVc4xMasterStsIndicate((Tha60290081ModuleMap)mapDemapModule, vcx);*/
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    ret = ThaModuleAbstractMapVcxEncapConnectionEnable(mapDemapModule, vcx, enable);
    /*mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);*/
    ret |= ThaModuleAbstractMapVcxEncapConnectionEnable(mapDemapModule, vcx, enable);

    return ret;
    }

static eAtModulePrbsRet POHDefaultSet(Tha6A000010PrbsEngine self, AtSdhChannel sdhChannel)
    {
    AtUnused(sdhChannel);
    return Tha6A0000POHDefaultSet((AtPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngineHoVc);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, HwPrbsEngineEnable);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, POHDefaultSet);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6A000010PrbsEngineHoVcObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A000010PrbsEngineHoVcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010PrbsEngineHoVcObjectInit(newEngine, vc);
    }

/*eAtModulePrbsRet Tha6A0000POHDefaultSet(AtPrbsEngine self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    uint8 sts1Id, sts_i;
    eAtModulePrbsRet ret = cAtOk;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        sts1Id = AtSdhChannelSts1GetAtIndex(sdhChannel, sts_i);
        ret |= ThaOcnStsPohInsertEnable(sdhChannel, sts1Id, cAtTrue);
        ThaOcnVtPohInsertEnable(sdhChannel, sts1Id, 0, 0, cAtTrue);
        }

    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);

    return ret;
    }*/

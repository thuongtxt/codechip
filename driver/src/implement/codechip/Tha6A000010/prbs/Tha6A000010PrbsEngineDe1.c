/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngineDe1.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : DE1 PRBS of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A000010PrbsEngineInternal.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/map/ThaModuleAbstractMap.h"

/*--------------------------- Define -----------------------------------------*/
#define cDs0MaskOfE1ForPrbs  cBit31_1
#define cDs0MaskOfDs1ForPrbs cBit23_0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A000010PrbsEngineDe1 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A000010PrbsEngineDe1 *Tha6A000010PrbsEngineDe1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsUnFrameMode(uint16 frameType)
    {
    return AtPdhDe1IsUnframeMode(frameType);
    }

static eBool IsDs1FrameMode(uint16 frameValue)
    {
    if ((frameValue == cAtPdhDs1FrmSf)  ||
        (frameValue == cAtPdhDs1FrmEsf) ||
        (frameValue == cAtPdhDs1FrmDDS) ||
        (frameValue == cAtPdhDs1FrmSLC))
        return cAtTrue;

    return cAtFalse;
    }

static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    if (IsUnFrameMode(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return (AtPw)AtModulePwSAToPCreate(pwModule, (uint16)pwId);

    /* When E1 is in frame mode, we should create a CESoP instead of SAToP */
    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static eAtRet PrbsEnableAtDe1LevelMark(Tha6A000010PrbsEngineDe1 self, eBool en)
    {
    mThis(self)->enableE1Level = en;
    return cAtOk;
    }

static eBool PrbsEnableAtDe1LevelIsMark(Tha6A000010PrbsEngineDe1 self)
    {
    return mThis(self)->enableE1Level;
    }

static uint32 NxDs0BitMaskGet(AtPdhChannel channel)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet(channel);
    uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;

    if (IsUnFrameMode(frameType))
        nxDs0BitMask = ThaPdhDe1EncapDs0BitMaskGet((ThaPdhDe1)channel);

    return nxDs0BitMask;
    }

static AtChannel CircuitToBind(Tha6A000010PrbsEngine self, AtChannel channel)
    {
    uint32 nxDs0BitMask;
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);

    if (frameType == cAtPdhDe1FrameUnknown)
        {
        mChannelLog(channel, cAtLogLevelWarning, "Frame type of circuit is unknown");
        return NULL;
        }

    PrbsEnableAtDe1LevelMark(mThis(self), cAtFalse);
    if (IsUnFrameMode(frameType))
        return channel;

    nxDs0BitMask = NxDs0BitMaskGet((AtPdhChannel)channel);
    AtPdhDe1NxDs0Create((AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self), nxDs0BitMask);
    PrbsEnableAtDe1LevelMark(mThis(self), cAtTrue);

    return (AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)channel, nxDs0BitMask);
    }

static eAtRet CircuitUnBind(Tha6A000010PrbsEngine self)
    {
    AtPdhDe1 de1 = (AtPdhDe1) AtPrbsEngineChannelGet((AtPrbsEngine) self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel) de1);

    if (IsUnFrameMode(frameType))
        return cAtOk;

    if (PrbsEnableAtDe1LevelIsMark(mThis(self)))
        {
        AtIterator nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate(de1);
        AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator);
        if (nxDs0)
            AtPdhDe1NxDs0Delete(de1, nxDs0);
        AtObjectDelete((AtObject)nxDs0Iterator);
        }

    PrbsEnableAtDe1LevelMark(mThis(self), cAtFalse);

    return cAtOk;
    }

static eBool HwPrbsEngineEnable(Tha6A000010PrbsEngine self, AtPw pw, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    AtChannel de1 = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaModuleAbstractMap mapDemapModule;
    uint32 mask = NxDs0BitMaskGet((AtPdhChannel)de1);
    eAtRet ret;

    if (Tha6A000010PrbsEngineHasPrbsHoBus(self))
        return mMethodsGet(self)->HwPrbsEngineEnable(self, pw, enable);

    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret = ThaModuleAbstractMapDs0EncapConnectionEnable(mapDemapModule, de1, mask, enable);
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    ret = ThaModuleAbstractMapDs0EncapConnectionEnable(mapDemapModule, de1, mask, enable);

    return ret;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngineDe1);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, CircuitToBind);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, CircuitUnBind);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, HwPrbsEngineEnable);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    }

AtPrbsEngine Tha6A000010PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A000010PrbsEngineDe1New(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010PrbsEngineDe1ObjectInit(newEngine, de1);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010PrbsEngine.h
 * 
 * Created Date: Dec 2, 2015
 *
 * Description : LO-Path PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A000010PRBSENGINE_H_
#define _THA6A000010PRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A000010PrbsEngine *Tha6A000010PrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/
AtPw Tha6A000010PrbsEnginePwGet(AtPrbsEngine self);

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A000010PrbsEngineDe1New(AtPdhDe1 de1);
AtPrbsEngine Tha6A000010PrbsEngineDe3New(AtPdhDe3 de3);
AtPrbsEngine Tha6A000010PrbsEngineHoVcNew(AtSdhChannel vc);
AtPrbsEngine Tha6A000010PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0);
AtPrbsEngine Tha6A000010PrbsEngineVc1xNew(AtSdhChannel vc1x);
AtPrbsEngine Tha6A000010PrbsEngineTu3VcNew(AtSdhChannel vc);

AtPrbsEngine Tha6A000010PrbsEngineDe1V2New(AtPdhDe1 de1);
AtPrbsEngine Tha6A000010PrbsEngineDe3V2New(AtPdhDe3 de3);
AtPrbsEngine Tha6A000010PrbsEngineHoVcV2New(AtSdhChannel vc);
AtPrbsEngine Tha6A000010PrbsEngineNxDs0V2New(AtPdhNxDS0 nxDs0);
AtPrbsEngine Tha6A000010PrbsEngineVc1xV2New(AtSdhChannel vc1x);
AtPrbsEngine Tha6A000010PrbsEngineTu3VcV2New(AtSdhChannel vc);

eBool Tha6A000010PrbsEngineHasPrbsHoBus(Tha6A000010PrbsEngine self);
void Tha6A000010PrbsEngineCounterLatch(AtPrbsEngine self, uint32 value);
uint32 Tha6A000010PrbsEngineCounterLatchGet(AtPrbsEngine self);
void Tha6A000010PrbsEngineCounterLatchIncrease(AtPrbsEngine self);
uint8 Tha6A000010PrbsEngineNumberCounterLatch(AtPrbsEngine self);
void Tha6A000010PrbsEngineNumberCounterLatchReset(AtPrbsEngine self);
eAtModulePrbsRet Tha6A0000PrbsEnginePOHDefaultSet(AtPrbsEngine self, AtSdhChannel sdhChannel);
eBool Tha6A000010PrbsEngineHasPrbsHoBus(Tha6A000010PrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A000010PRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngine.c
 *
 * Created Date: Dec 2, 2015
 *
 * Description : LO-Path PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A000010PrbsEngineInternal.h"
#include "Tha6A000010PrbsEngineReg.h"
#include "Tha6A000010ModulePrbsInternal.h"

#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../Tha60210011/map/Tha60210011ModuleMap.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A000010PrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8  m_methodsInit = 0;
static tTha6A000010PrbsEngineMethods m_methods;

/* Override */
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static Tha6A000010ModulePrbs PrbsModule(Tha6A000010PrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (Tha6A000010ModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eBool HasPrbsHoBus(Tha6A000010PrbsEngine self)
    {
    Tha60210011Device device = (Tha60210011Device)AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return Tha60210011DeviceHasHoBus(device);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngine);
    }

static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtUnused(pwId);
    AtUnused(self);
    return NULL;
    }

static eAtRet PwCircuitBind(Tha6A000010PrbsEngine self, AtPw pw, AtChannel circuit)
    {
    AtChannel realCircuit = mMethodsGet(self)->CircuitToBind(self, circuit);

    if (realCircuit == NULL)
        return cAtErrorNullPointer;

    return AtPwCircuitBind(pw, realCircuit);;
    }

static void PseudowireDelete(Tha6A000010PrbsEngine self)
    {
    if ((AtChannel)self->pw)
        {
        AtModulePw pwModule = (AtModulePw)AtChannelModuleGet((AtChannel)self->pw);
        AtPwCircuitUnbind((AtPw)self->pw);
        AtModulePwDeletePw(pwModule, (uint16)AtChannelIdGet((AtChannel)self->pw));
        mMethodsGet(self)->CircuitUnBind(self);
        }

    self->pw = NULL;
    }

static AtPw PseudowireCreate(Tha6A000010PrbsEngine self)
    {
    uint32 freePwId;
    AtPw pw = NULL;
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthPort port = AtModuleEthPortGet(ethModule, 0);

    /* Find one free PW */
    freePwId = ThaModulePwFreePwGet(pwModule);
    if (freePwId >= AtModulePwMaxPwsGet((AtModulePw)pwModule))
        return NULL;

    /* Create PW */
    pw = mMethodsGet(self)->PwCreate(self, freePwId);
    if (pw == NULL)
        return NULL;

    AtPwEthPortSet(pw, port);

    if (PwCircuitBind(self, pw, AtPrbsEngineChannelGet((AtPrbsEngine)self)) != cAtOk)
        {
        AtModulePwDeletePw((AtModulePw)pwModule, (uint16)freePwId);
        return NULL;
        }

    AtPrbsEngineEngineIdSet((AtPrbsEngine)self, AtChannelHwIdGet((AtChannel)pw));

    return pw;
    }

static uint8 PrbsEngineSliceGet(AtPrbsEngine self, AtPw pw)
	{
	uint8 slice = 0;
	uint32 hwIdInSlice = 0;
	AtUnused(self);

	if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
		mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

	return slice;
	}

static uint32 HoPrbsEngineOffset(AtPrbsEngine self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 hwIdInSlice = 0;
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return ((uint32)(slice << 11) + hwIdInSlice);
    }

static uint32 LoPrbsEngineOffset(AtPrbsEngine self, AtPw pw)
    {
    uint8 oc48Id = 0, oc24SliceId = 0;
    uint8 slice = 0;
    uint32 hwIdInSlice = 0;
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint8 oc48IdShift = mMethodsGet(prbs)->LoPrbsEngineOc48IdShift(prbs);
    uint8 oc24SliceIdShift = mMethodsGet(prbs)->LoPrbsEngineOc24SliceIdShift(prbs);
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    oc48Id = slice / 2;
    oc24SliceId = slice % 2;

    return ((uint32)(oc48Id << oc48IdShift) + (uint32)(oc24SliceId << oc24SliceIdShift) + AtChannelHwIdGet((AtChannel)pw));
    }

static eBool HoPathPrbsEngineEnable(AtPrbsEngine self, AtPw pw, eBool enable)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device   = AtChannelDeviceGet(channel);
    uint32 base       = Tha6A000010ModulePrbsGenReg(prbs, cAtTrue);
    uint32 address    = base + HoPrbsEngineOffset(self, pw);
    uint32 regFieldMask=0, regFieldShift = 0;

    regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtTrue, cAtTrue);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regValue   = AtPrbsEngineRead(self, address, cAtModulePrbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regValue, regField, mBoolToBin(enable));
        AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);
        }

    Tha60210011MapHoBertHwDefault((ThaModuleMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhChannel)channel);

    return cAtTrue;
    }

static eBool LoPathPrbsEngineEnable(AtPrbsEngine self, AtPw pw, eBool enable)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base     = Tha6A000010ModulePrbsGenReg(prbs, cAtFalse);
    uint32 address  = base + LoPrbsEngineOffset(self, pw);
    uint32 regFieldMask, regFieldShift = 0;

    regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtFalse, cAtTrue);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regValue, regField, mBoolToBin(enable));
        AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);
        }

    return cAtTrue;
    }

static eBool HwPrbsEngineEnable(Tha6A000010PrbsEngine self, AtPw pw, eBool enable)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineEnable((AtPrbsEngine)self, pw, enable);

    return HoPathPrbsEngineEnable((AtPrbsEngine)self, pw, enable);
    }

static eBool LoPathPrbsEngineIsEnabled(AtPrbsEngine self, AtPw pw)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base    = Tha6A000010ModulePrbsGenReg(prbs, cAtFalse);
    uint32 address = base + LoPrbsEngineOffset(self, pw);
    uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 regFieldMask = 0, regFieldShift = 0;

    regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtFalse, cAtTrue);
    regFieldShift = AtRegMaskToShift(regFieldMask);

    return mRegField(regValue,  regField) ? cAtTrue : cAtFalse;
    }

static eBool HoPathPrbsEngineIsEnabled(AtPrbsEngine self, AtPw pw)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base    = Tha6A000010ModulePrbsGenReg(prbs, cAtTrue);
    uint32 address = base + HoPrbsEngineOffset(self, pw);
    uint32 regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
    uint32 regFieldMask = 0, regFieldShift = 0;

    regFieldMask = Tha6A000010ModulePrbsEnableMask(prbs, cAtTrue, cAtTrue);
    regFieldShift = AtRegMaskToShift(regFieldMask);

    return mRegField(regValue, regField) ? cAtTrue : cAtFalse;
    }

static eBool HwPrbsEngineIsEnabled(Tha6A000010PrbsEngine self, AtPw pw)
    {
    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return self->enable;
        /*return Tha6A000010ModulePrbsHwPrbsEngineIsEnable(PrbsModule(mThis(self)), mThis(self)->pw);*/

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineIsEnabled((AtPrbsEngine)self, pw);

    return HoPathPrbsEngineIsEnabled((AtPrbsEngine)self, pw);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    /* Enable corresponding PW PRBS engine */
    if (enable)
        {
        if (mThis(self)->pw == NULL)
            {
            mThis(self)->pw = PseudowireCreate(mThis(self));
            if (mThis(self)->pw == NULL)
                return cAtOk;

            ret |= AtPrbsEngineTxModeSet(self, cAtPrbsModePrbs15);
            ret |= AtPrbsEngineRxModeSet(self, cAtPrbsModePrbs15);
            ret |= AtPrbsEngineGeneratingSideSet(self,cAtPrbsSideTdm);
            ret |= AtPrbsEngineMonitoringSideSet(self,cAtPrbsSideTdm);
            }

        mMethodsGet(mThis(self))->HwPrbsEngineEnable(mThis(self), mThis(self)->pw, enable);
        mThis(self)->enable = enable;
        }

    /* In case of disable, delete PW  to save PW resources */
    else
        {
        mMethodsGet(mThis(self))->HwPrbsEngineEnable(mThis(self), mThis(self)->pw, enable);
        mThis(self)->enable = enable;
        PseudowireDelete(mThis(self));
        }

    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtFalse;

    return HwPrbsEngineIsEnabled((Tha6A000010PrbsEngine)self, mThis(self)->pw);
    }

static uint32 LoPathPrbsEngineAlarmClear(AtPrbsEngine self, AtPw pw)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base      = Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 address   = base + LoPrbsEngineOffset(self, pw);
    uint32 regValue  = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return regValue;
    }

static uint32 LoPathPrbsEngineAlarmGet(AtPrbsEngine self, AtPw pw)
    {
    uint32 regValue  = LoPathPrbsEngineAlarmClear(self, pw);

    if (regValue & c_prbslomon_PrbsLoMonErr_Mask)
        return cAtPrbsEngineAlarmTypeError;

    if ((regValue & c_prbslomon_PrbsLoMonSync_Mask) == 0)
        return cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static uint32 HoPathPrbsEngineAlarmClear(AtPrbsEngine self, AtPw pw)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 base      = Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 address   = base + HoPrbsEngineOffset(self, pw);
    uint32 regValue  = AtPrbsEngineRead(self, address, cAtModulePrbs);

    return regValue;
    }

static uint32 HoPathPrbsEngineAlarmGet(AtPrbsEngine self, AtPw pw)
    {
    uint32 regValue  = HoPathPrbsEngineAlarmClear(self, pw);

    if (regValue & c_prbshomon_PrbsHoMonErr_Mask)
        return cAtPrbsEngineAlarmTypeError;

    if ((regValue & c_prbshomon_PrbsHoMonSync_Mask) == 0)
        return cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static uint32 HwPrbsEngineAlarmGet(AtPrbsEngine self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineAlarmGet(self, pw);

    return HoPathPrbsEngineAlarmGet(self, pw);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return 0;

    if (HasPrbsHoBus(mThis(self)))
        return HwPrbsEngineAlarmGet(self, mThis(self)->pw);

    return Tha6A000010ModulePrbsEngineAlarmGet(PrbsModule(mThis(self)), mThis(self)->pw);
    }

static uint32 HwPrbsEngineAlarmClear(AtPrbsEngine self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineAlarmClear(self, pw);

    return HoPathPrbsEngineAlarmClear(self, pw);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return 0;

    if (HasPrbsHoBus(mThis(self)))
        return HwPrbsEngineAlarmClear(self, mThis(self)->pw);

    return Tha6A000010ModulePrbsEngineAlarmHistoryGet(PrbsModule(mThis(self)), mThis(self)->pw);
    }

static eAtModulePrbsRet LoPathPrbsEngineModeSet(AtPrbsEngine self,  AtPw pw, eAtPrbsMode prbsMode, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtFalse) : Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 offset  = LoPrbsEngineOffset(self, pw);
    uint32 regVal  = 0;
    uint32 regFieldMask, regFieldShift = 0;

    if (!Tha6A000010ModulePrbsModeIsSupported(prbs, prbsMode))
        return cAtErrorModeNotSupport;

    if (offset == cInvalidUint32)
        return cAtOk;

    /* Mode 0: PRBS 15 and 1: SEQ */
    address = address + offset;
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtFalse, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, Tha6A000010ModulePrbsModeToHwValue(prbs, prbsMode));

    regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtFalse, isTxDirection);
    if (regFieldMask!= cInvalidUint32)
        {
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField,  Tha6A000010ModulePrbsHwStepFromPrbsMode(prbs, prbsMode));
        }

    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet HoPathPrbsEngineModeSet(AtPrbsEngine self,  AtPw pw, eAtPrbsMode prbsMode, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address, regValue;
    uint32 hwAddress = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtTrue) : Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 regFieldMask, regFieldShift = 0;


    if (!Tha6A000010ModulePrbsModeIsSupported(prbs, prbsMode))
        return cAtErrorModeNotSupport;

    /* 0: PRBS 15 and 1: SEQ */
    address  = hwAddress + HoPrbsEngineOffset(self, pw);
    regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
    regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtTrue, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mFieldIns(&regValue, regFieldMask, regFieldShift, Tha6A000010ModulePrbsModeToHwValue(prbs, mThis(self)->txPrbsMode));

    regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtTrue, isTxDirection);
    if (regFieldMask!= cInvalidUint32)
        {
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mFieldIns(&regValue, regFieldMask, regFieldMask, Tha6A000010ModulePrbsHwStepFromPrbsMode(prbs, mThis(self)->txPrbsMode));
        }

    AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);
    return cAtOk;
    }

static eAtRet LoPathPrbsEngineDataModeSet(AtPrbsEngine self, AtPw pw, eAtPrbsDataMode prbsMode, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtFalse) : Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 offset  = LoPrbsEngineOffset(self, pw);
    uint32 regVal  = 0;
    uint32 regFieldMask, regFieldShift = 0;


    if (!Tha6A000010ModulePrbsDataModeIsSupported(prbs))
        return cAtErrorModeNotSupport;

    if (offset == cInvalidUint32)
        return cAtOk;

    /* Mode 0: PRBS 15 and 1: SEQ */
    address = address + offset;
    regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);
    regFieldMask  = Tha6A000010ModulePrbsDataModeMask(prbs, cAtFalse, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, prbsMode==cAtPrbsDataMode64kb?0:1);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet HoPathPrbsEngineDataModeSet(AtPrbsEngine self, AtPw pw, eAtPrbsDataMode prbsMode, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address, regValue;
    uint32 hwAddress = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtTrue) : Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 regFieldMask, regFieldShift = 0;


    if (!Tha6A000010ModulePrbsDataModeIsSupported(prbs))
        return cAtErrorModeNotSupport;

    /* 0: PRBS 15 and 1: SEQ */
    address  = hwAddress + HoPrbsEngineOffset(self, pw);
    regValue = AtPrbsEngineRead(self, address, cAtModulePrbs);
    regFieldMask  = Tha6A000010ModulePrbsDataModeMask(prbs, cAtTrue, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mFieldIns(&regValue, regFieldMask, regFieldShift, prbsMode==cAtPrbsDataMode64kb?0:1);

    AtPrbsEngineWrite(self, address, regValue, cAtModulePrbs);
    return cAtOk;
    }


static uint32 HwTxPrbsEngineModeSet(AtPrbsEngine self, AtPw pw, eAtPrbsMode prbsMode)
    {
    eBool isTxDirection = cAtTrue;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineModeSet(PrbsModule(mThis(self)), mThis(self)->pw, prbsMode, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineModeSet(self, pw, prbsMode, isTxDirection);
    return HoPathPrbsEngineModeSet(self, pw, prbsMode, isTxDirection);
    }

static uint32 HwRxPrbsEngineModeSet(AtPrbsEngine self, AtPw pw, eAtPrbsMode prbsMode)
    {
    eBool isTxDirection = cAtFalse;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineModeSet(PrbsModule(mThis(self)), mThis(self)->pw, prbsMode, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineModeSet(self, pw, prbsMode, isTxDirection);
    return HoPathPrbsEngineModeSet(self, pw, prbsMode, isTxDirection);
    }

static uint32 HwTxPrbsEngineDataModeSet(AtPrbsEngine self, AtPw pw, eAtPrbsDataMode prbsMode)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineDataModeSet(self, pw, prbsMode, cAtTrue);
    return HoPathPrbsEngineDataModeSet(self, pw, prbsMode, cAtTrue);
    }

static uint32 HwRxPrbsEngineDataModeSet(AtPrbsEngine self, AtPw pw, eAtPrbsDataMode prbsMode)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineDataModeSet(self, pw, prbsMode, cAtFalse);
    return HoPathPrbsEngineDataModeSet(self, pw, prbsMode, cAtFalse);
    }

static eAtPrbsMode LoPathPrbsEngineModeGet(AtPrbsEngine self,  AtPw pw, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtFalse)  : Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 offset  =  LoPrbsEngineOffset(self, pw);
    uint32 regVal;
    uint8  mode=0,step=0;
    uint32 regFieldMask, regFieldShift = 0;

    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;

    regVal = AtPrbsEngineRead(self, address +offset, cAtModulePrbs);
    regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtFalse, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mode = (uint8) mRegField(regVal, regField);

    regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtFalse, isTxDirection);
    if (regFieldMask!= cInvalidUint32)
        {
        regFieldShift = AtRegMaskToShift(regFieldMask);
        step = (uint8) mRegField(regVal, regField);
        }
    return Tha6A000010ModulePrbsModeFromHwValue(prbs,isTxDirection?mThis(self)->txPrbsMode: mThis(self)->rxPrbsMode, mode, step);
    }

static eAtPrbsDataMode LoPathPrbsEngineDataModeGet(AtPrbsEngine self, AtPw pw, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 address = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtFalse) : Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
    uint32 offset  =  LoPrbsEngineOffset(self, pw);
    uint32 regVal;
    uint8  mode=0;
    uint32 regFieldMask, regFieldShift = 0;

    if (offset == cInvalidUint32)
        return cAtPrbsModePrbs15;

    regVal = AtPrbsEngineRead(self, address +offset, cAtModulePrbs);
    regFieldMask  = Tha6A000010ModulePrbsDataModeMask(prbs, cAtFalse, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mode = (uint8) mRegField(regVal, regField);
    return (mode==0)? cAtPrbsDataMode64kb: cAtPrbsDataMode56kb;
    }

static eAtPrbsMode HoPathPrbsEngineModeGet(AtPrbsEngine self,  AtPw pw, eBool isTxDirection)
    {
    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 hwAddress = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtTrue) : Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 regVal = AtPrbsEngineRead(self, hwAddress + HoPrbsEngineOffset(self, pw), cAtModulePrbs);
    uint8  mode=0,step=0;
    uint32 regFieldMask, regFieldShift = 0;

    regFieldMask  = Tha6A000010ModulePrbsModeMask(prbs, cAtTrue, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mode = (uint8) mRegField(regVal, regField);

    regFieldMask  = Tha6A000010ModulePrbsStepMask(prbs, cAtTrue, isTxDirection);
    if (regFieldMask!= cInvalidUint32)
        {
        regFieldShift = AtRegMaskToShift(regFieldMask);
        step = (uint8) mRegField(regVal, regField);
        }

    return Tha6A000010ModulePrbsModeFromHwValue(prbs, isTxDirection? mThis(self)->txPrbsMode: mThis(self)->rxPrbsMode, mode, step);
    }

static eAtPrbsDataMode HoPathPrbsEngineDataModeGet(AtPrbsEngine self, AtPw pw, eBool isTxDirection)
    {

    Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
    uint32 hwAddress = isTxDirection ? Tha6A000010ModulePrbsGenReg(prbs, cAtTrue) : Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
    uint32 regVal = AtPrbsEngineRead(self, hwAddress + HoPrbsEngineOffset(self, pw), cAtModulePrbs);
    uint8  mode=0;
    uint32 regFieldMask, regFieldShift = 0;

    regFieldMask  = Tha6A000010ModulePrbsDataModeMask(prbs, cAtTrue, isTxDirection);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mode = (uint8) mRegField(regVal, regField);

    return (mode==0)? cAtPrbsDataMode64kb:cAtPrbsDataMode56kb;
    }

static eAtPrbsMode HwTxPrbsEngineModeGet(AtPrbsEngine self, AtPw pw)
    {
    eBool isTxDirection = cAtTrue;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineModeGet(PrbsModule(mThis(self)), mThis(self)->pw, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineModeGet(self, pw, isTxDirection);
    return HoPathPrbsEngineModeGet(self, pw, isTxDirection);
    }

static eAtPrbsMode HwRxPrbsEngineModeGet(AtPrbsEngine self, AtPw pw)
    {
    eBool isTxDirection = cAtFalse;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineModeGet(PrbsModule(mThis(self)), mThis(self)->pw, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineModeGet(self, pw, isTxDirection);
    return HoPathPrbsEngineModeGet(self, pw, isTxDirection);
    }

static eAtPrbsMode HwTxPrbsEngineDataModeGet(AtPrbsEngine self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineDataModeGet(self, pw, cAtTrue);
    return HoPathPrbsEngineDataModeGet(self, pw, cAtTrue);
    }

static eAtPrbsMode HwRxPrbsEngineDataModeGet(AtPrbsEngine self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoPathPrbsEngineDataModeGet(self, pw, cAtFalse);
    return HoPathPrbsEngineDataModeGet(self, pw, cAtFalse);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {

    eAtRet ret = AtPrbsEngineTxModeSet(self, prbsMode);

    if (ret != cAtOk)
        return ret;

    return AtPrbsEngineRxModeSet(self, prbsMode);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxModeGet(self);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (mThis(self)->pw == NULL)
        return cAtErrorNullPointer;

    mThis(self)->txPrbsMode = prbsMode;
    return HwTxPrbsEngineModeSet(self, mThis(self)->pw, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtFalse;

    return HwTxPrbsEngineModeGet(self, mThis(self)->pw);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (mThis(self)->pw == NULL)
        return cAtErrorNullPointer;

    mThis(self)->rxPrbsMode = prbsMode;
    return HwRxPrbsEngineModeSet(self, mThis(self)->pw, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtFalse;

    return HwRxPrbsEngineModeGet(self, mThis(self)->pw);
    }

static eAtRet DataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    eAtRet ret = AtPrbsEngineTxDataModeSet(self, prbsMode);

    if (ret != cAtOk)
        return ret;

    return AtPrbsEngineRxDataModeSet(self, prbsMode);
    }

static eAtPrbsDataMode DataModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxDataModeGet(self);
    }

static eAtRet TxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    if (mThis(self)->pw == NULL)
        mThis(self)->pw = PseudowireCreate(mThis(self));

    mThis(self)->txPrbsMode = prbsMode;
    return HwTxPrbsEngineDataModeSet(self, mThis(self)->pw, prbsMode);
    }

static eAtPrbsDataMode TxDataModeGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        mThis(self)->pw = PseudowireCreate(mThis(self));

    return HwTxPrbsEngineDataModeGet(self, mThis(self)->pw);
    }

static eAtRet RxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    if (mThis(self)->pw == NULL)
        mThis(self)->pw = PseudowireCreate(mThis(self));

    mThis(self)->rxPrbsMode = prbsMode;
    return HwRxPrbsEngineDataModeSet(self, mThis(self)->pw, prbsMode);
    }

static eAtPrbsDataMode RxDataModeGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        mThis(self)->pw = PseudowireCreate(mThis(self));

    return HwRxPrbsEngineDataModeGet(self, mThis(self)->pw);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
	m_AtObjectMethods->Serialize(self, encoder);
    mEncodeNone(pw);
    mEncodeNone(pwId);
    mEncodeNone(lackCounterValue);
    mEncodeNone(txEnable);
    mEncodeNone(txPrbsMode);
    mEncodeNone(rxEnable);
    mEncodeNone(rxPrbsMode);
    }

static void Delete(AtObject self)
    {
    PseudowireDelete(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static AtChannel CircuitToBind(Tha6A000010PrbsEngine self, AtChannel channel)
    {
    AtUnused(self);
    return channel;
    }

static eAtRet CircuitUnBind(Tha6A000010PrbsEngine self)
	{
	AtUnused(self);
	return cAtOk;
	}

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtPrbsSideUnknown;
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtPrbsSideUnknown;
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtPrbsSideUnknown;
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtUnused(self);
    return force ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtFalse;
    return cAtTrue;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    if (mThis(self)->pw == NULL)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 DisruptionTimeGet(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = mThis(self);

    if (mMethodsGet(engine)->HasPrbsDisruption(engine))
        {
        uint32 disruptionActualTime;
        uint32 address = mMethodsGet(engine)->RegDisruption(engine) + AtChannelIdGet((AtChannel)mThis(self)->pw);
        uint32 regVal = AtPrbsEngineRead(self, address, cThaModulePda);
        uint32 mask = mMethodsGet(engine)->DisruptionMask(engine);
        uint32 shift = mMethodsGet(engine)->DisruptionShift(engine);
        mFieldGet(regVal, mask, shift, uint32, &disruptionActualTime);
        return disruptionActualTime;
        }

    return 0;
    }

static uint32 HoRxFixedPatternGet(AtPrbsEngine self, AtPw pw)
    {
    uint32 offset  =  HoPrbsEngineOffset(self, pw);

    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;

    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtTrue, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask, regFieldShift = 0;

        regFieldMask = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtFalse);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return Tha6A000010ModulePrbsConvertHwToSwFixedPattern(prbs, mThis(self)->rxPrbsMode, mRegField(regVal, regField));
        }
    return 0;
    }

static uint32 LoRxFixedPatternGet(AtPrbsEngine self, AtPw pw)
    {
    uint32 offset  =  LoPrbsEngineOffset(self, pw);

    if (offset==cInvalidUint32)
       return cAtPrbsModePrbs15;

    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask, regFieldShift = 0;

        regFieldMask = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return Tha6A000010ModulePrbsConvertHwToSwFixedPattern(prbs, mThis(self)->rxPrbsMode, mRegField(regVal, regField));
        }
    return 0;
    }

static uint32 HwRxFixedPatternGet(AtPrbsEngine self, AtPw pw)
    {
    eBool isTxDirection = cAtFalse;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineFixPatternGet(PrbsModule(mThis(self)), mThis(self)->pw, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoRxFixedPatternGet(self, pw);
    return HoRxFixedPatternGet(self, pw);
    }

static uint32 RxCurrentFixedPatternGet(AtPrbsEngine self)
    {
    return HwRxFixedPatternGet(self,  mThis(self)->pw);
    }

static eAtModulePrbsRet HoTxFixedPatternSet(AtPrbsEngine self, AtPw pw, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;
    uint32 offset  =  HoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
        uint32 regFieldMask, regFieldShift = 0;
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtTrue, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        regFieldMask = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtTrue);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, Tha6A000010ModulePrbsConvertSwToHwFixedPattern(prbs,mThis(self)->txPrbsMode,fixedPattern));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static eAtModulePrbsRet LoTxFixedPatternSet(AtPrbsEngine self, AtPw pw, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;
    uint32 offset  =  LoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
        uint32 regFieldMask, regFieldShift = 0;
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsFixPatternReg(prbs, cAtFalse, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, Tha6A000010ModulePrbsConvertSwToHwFixedPattern(prbs,mThis(self)->txPrbsMode,fixedPattern));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static eAtModulePrbsRet HwTxFixedPatternSet(AtPrbsEngine self, AtPw pw, uint32 fixedPattern)
    {
    eBool isTxDirection = cAtTrue;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineFixPatternSet(PrbsModule(mThis(self)), mThis(self)->pw, fixedPattern, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoTxFixedPatternSet(self, pw,fixedPattern);
    return HoTxFixedPatternSet(self, pw, fixedPattern);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    return HwTxFixedPatternSet(self, mThis(self)->pw, fixedPattern);
    }

static uint32 HoTxFixedPatternGet(AtPrbsEngine self, AtPw pw)
    {
    uint32 offset  =  HoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return cAtOk;
    else
        {
        uint32 regFieldMask, regFieldShift = 0;
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsGenReg(prbs, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtTrue);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return Tha6A000010ModulePrbsConvertHwToSwFixedPattern(prbs,mThis(self)->txPrbsMode, mRegField(regVal, regField));
        }
    return 0;
    }

static uint32 LoTxFixedPatternGet(AtPrbsEngine self, AtPw pw)
    {
    uint32 offset  =  LoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return cAtOk;
    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsGenReg(prbs, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask, regFieldShift = 0;
        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return Tha6A000010ModulePrbsConvertHwToSwFixedPattern(prbs, mThis(self)->txPrbsMode, mRegField(regVal, regField));
        }
    return 0;
    }

static uint32 HwTxFixedPatternGet(AtPrbsEngine self, AtPw pw)
    {
    eBool isTxDirection = cAtTrue;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineFixPatternGet(PrbsModule(mThis(self)), mThis(self)->pw, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoTxFixedPatternGet(self, pw);
    return HoTxFixedPatternGet(self, pw);
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    return HwTxFixedPatternGet(self, mThis(self)->pw);
    }

static eAtModulePrbsRet HoRxFixedPatternSet(AtPrbsEngine self, AtPw pw, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;
    uint32 offset  =  HoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsMonReg(prbs, cAtTrue);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask, regFieldShift = 0;

        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtTrue, cAtFalse);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, Tha6A000010ModulePrbsConvertSwToHwFixedPattern(prbs,mThis(self)->rxPrbsMode, fixedPattern));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static eAtModulePrbsRet LoRxFixedPatternSet(AtPrbsEngine self, AtPw pw, uint32 fixedPattern)
    {
    eAtRet ret = cAtOk;
    uint32 offset  =  LoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return cAtPrbsModePrbs15;
    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsMonReg(prbs, cAtFalse);
        uint32 regAddr = hwAddress + offset;
        uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask, regFieldShift = 0;

        regFieldMask   = Tha6A000010ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, Tha6A000010ModulePrbsConvertSwToHwFixedPattern(prbs,mThis(self)->rxPrbsMode, fixedPattern));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static uint32 HwRxFixedPatternSet(AtPrbsEngine self, AtPw pw, uint32 fixedPattern)
    {
    eBool isTxDirection = cAtFalse;

    if (HasPrbsHoBus(mThis(self)) == cAtFalse)
        return Tha6A000010ModulePrbsEngineFixPatternSet(PrbsModule(mThis(self)), mThis(self)->pw, fixedPattern, isTxDirection);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoRxFixedPatternSet(self, pw, fixedPattern);
    return HoRxFixedPatternSet(self, pw, fixedPattern);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    return HwRxFixedPatternSet(self, mThis(self)->pw, fixedPattern);
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    return HwRxFixedPatternGet(self, mThis(self)->pw);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet ret = cAtOk;
    ret |= AtPrbsEngineTxFixedPatternSet(self, fixedPattern);
    ret |= AtPrbsEngineRxFixedPatternSet(self, fixedPattern);
    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxFixedPatternGet(self);
    }

static uint32 RegClearStatus(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eBool  HasPrbsModeChange(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool  HasPrbsDisruption(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32  RegDisruption(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }
    
static uint32  DisruptionMask(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint8  DisruptionShift(Tha6A000010PrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePrbsRet POHDefaultSet(Tha6A000010PrbsEngine self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAtErrorNotImplemented;
    }

static uint32 MaxDelayReg(AtPrbsEngine self, AtPw pw, eBool r2c)
	{
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8 slice = PrbsEngineSliceGet(self, pw);

	if (!Tha60210011PwCircuitBelongsToLoLine(pw))
		return mMethodsGet(prbs)->PrbsMaxDelayReg(prbs, cAtTrue, r2c, slice);

	return mMethodsGet(prbs)->PrbsMaxDelayReg(prbs, cAtFalse, r2c, slice);
	}

static uint32 MinDelayReg(AtPrbsEngine self, AtPw pw, eBool r2c)
	{
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8  slice   =  PrbsEngineSliceGet(self, pw);

	if (!Tha60210011PwCircuitBelongsToLoLine(pw))
		return mMethodsGet(prbs)->PrbsMinDelayReg(prbs, cAtTrue, r2c, slice);

	return mMethodsGet(prbs)->PrbsMinDelayReg(prbs, cAtFalse, r2c, slice);
	}

static uint32 AverageDelayReg(AtPrbsEngine self, AtPw pw, eBool r2c)
	{
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint8  slice   =  PrbsEngineSliceGet(self, pw);

	if (!Tha60210011PwCircuitBelongsToLoLine(pw))
		return mMethodsGet(prbs)->PrbsAverageDelayReg(prbs, cAtTrue, r2c, slice);

	return mMethodsGet(prbs)->PrbsAverageDelayReg(prbs, cAtFalse, r2c, slice);
	}

static float ConvertToMs(AtPrbsEngine self, uint32 value)
	{
	AtPw pw = ((Tha6A000010PrbsEngine)self)->pw;
	if (Tha60210011PwCircuitBelongsToLoLine(pw))
		return (float)value/155520;
	return (float)value/311040;
	}

static float DelayGet(AtPrbsEngine self, uint8 mode, eBool r2c)
	{
	AtPw pw = ((Tha6A000010PrbsEngine)self)->pw;
	uint32 regVal = 0;
	uint32 regAddr = MaxDelayReg(self,pw, r2c);

	if (mode == cAf6MinDelay)
		regAddr = MinDelayReg(self, pw, r2c);
	else if (mode == cAf6AverageDelay)
		{
		ThaAttModulePrbs prbs = (ThaAttModulePrbs)PrbsModule((Tha6A000010PrbsEngine)self);
		if (ThaAttModulePrbsNeedDelayAverage(prbs))
			regAddr = AverageDelayReg(self, pw, r2c);
		else
			regAddr = cInvalidUint32;
		}
	if (regAddr!= cInvalidUint32)
		{
		regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
		if (mode == cAf6MinDelay && regVal==0xFFFFFFFF)
			regVal = 0; /* Value Maximum to compare */
		return ConvertToMs(self,regVal);
		}

	return 0;
	}

static eAtRet HoHwDelayEnable(AtPrbsEngine self, AtPw pw, eBool enable)
	{
	uint32 offset  =  HoPrbsEngineOffset(self, pw);
	uint8  slice   =  PrbsEngineSliceGet(self, pw);
	if (offset==cInvalidUint32)
		return cAtError;
	else
		{
		Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
		uint32 regAddr = Tha6A000010ModulePrbsDelayConfigReg(prbs, cAtTrue, slice);
		uint32 regVal    = 0;
		uint32 regFieldMask=0, regFieldShift=0;

		regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
		regFieldMask   = Tha6A000010ModulePrbsDelayEnableMask(prbs, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, mBoolToBin(enable));

		regFieldMask   = Tha6A000010ModulePrbsDelayIdMask(prbs, cAtTrue);
		regFieldShift  = AtRegMaskToShift(regFieldMask);
		mRegFieldSet(regVal, regField, offset);
	    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
		return cAtOk;
		}
	}


static eAtRet LoHwDelayEnable(AtPrbsEngine self, AtPw pw, eBool enable)
	{
	uint32 offset  =  LoPrbsEngineOffset(self, pw);
	uint8  slice   =  PrbsEngineSliceGet(self, pw);
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	uint32 regAddr = Tha6A000010ModulePrbsDelayConfigReg(prbs, cAtFalse, slice);
	uint32 regVal    = 0;
	uint32 regFieldMask=0, regFieldShift=0;

	regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
	regFieldMask   = Tha6A000010ModulePrbsDelayEnableMask(prbs, cAtFalse);
	regFieldShift  = AtRegMaskToShift(regFieldMask);
	mRegFieldSet(regVal, regField, mBoolToBin(enable));

	regFieldMask   = Tha6A000010ModulePrbsDelayIdMask(prbs, cAtFalse);
	regFieldShift  = AtRegMaskToShift(regFieldMask);
	mRegFieldSet(regVal, regField, offset);
	AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
	return cAtOk;

	}

static eAtRet DelayEnable(AtPrbsEngine self, eBool enable)
	{
	AtPw pw = ((Tha6A000010PrbsEngine)self)->pw;
	if (pw)
		{
		if (!Tha60210011PwCircuitBelongsToLoLine(pw))
			return HoHwDelayEnable(self, pw, enable);
		return LoHwDelayEnable(self, pw, enable);
		}
	return cAtOk;
	}


/* Counters */
static uint32 HoHwCounterGet(AtPrbsEngine self, AtPw pw,uint16 counterType, eBool r2c, eBool isLack)
    {
    uint32 offset  =  HoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return 0;

    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress = Tha6A000010ModulePrbsCounterReg(prbs, cAtTrue, r2c);
        uint32 regAddr   = hwAddress + offset;
        uint32 regVal    = 0;
        uint32 regFieldMask=0, regFieldShift=0;

        if (isLack && counterType==cAtPrbsEngineCounterRxSync)
            {
            regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
            ((Tha6A000010PrbsEngine)self)->lackCounterValue = regVal;
            }
        else
            regVal = ((Tha6A000010PrbsEngine)self)->lackCounterValue;

        regFieldMask   = Tha6A000010ModulePrbsCounterMask(prbs, counterType);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal, regField);
        }
    }

static uint32 LoHwCounterGet(AtPrbsEngine self, AtPw pw, uint16 counterType, eBool r2c, eBool isLack)
    {
    uint32 offset  =  LoPrbsEngineOffset(self, pw);
    if (offset==cInvalidUint32)
        return 0;

    else
        {
        Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
        uint32 hwAddress    = Tha6A000010ModulePrbsCounterReg(prbs, cAtFalse, r2c);
        uint32 regAddr      = hwAddress + offset;
        uint32 regVal       = 0;
        uint32 regFieldMask = 0, regFieldShift = 0;

        if (isLack && counterType==cAtPrbsEngineCounterRxSync)
            {
            regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
            ((Tha6A000010PrbsEngine)self)->lackCounterValue = regVal;
            }
        else
            regVal = ((Tha6A000010PrbsEngine)self)->lackCounterValue;

        regFieldMask   = Tha6A000010ModulePrbsCounterMask(prbs, counterType);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal, regField);
        }
    }

static uint32 HwCounterGet(AtPrbsEngine self, uint16 counterType, eBool r2c, eBool isLack)
    {
    AtPw pw = ((Tha6A000010PrbsEngine)self)->pw;
    if (pw)
        {
        if (HasPrbsHoBus(mThis(self)) == cAtFalse)
            return Tha6A000010ModulePrbsEngineCounterGet(PrbsModule(mThis(self)), self, mThis(self)->pw, counterType, r2c);

        if (!Tha60210011PwCircuitBelongsToLoLine(pw))
            return HoHwCounterGet(self, pw, counterType, r2c, isLack);
        return LoHwCounterGet(self, pw, counterType, r2c, isLack);
        }

    return 0;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
	{
	if (AtPrbsEngineCounterIsSupported(self, counterType))
		return HwCounterGet(self, counterType, cAtFalse, cAtFalse);

	return 0;
	}

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
	{
	if (AtPrbsEngineCounterIsSupported(self, counterType))
		return HwCounterGet(self, counterType, cAtTrue, cAtFalse);

	return 0;
	}

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
	{
	Tha6A000010ModulePrbs prbs = PrbsModule((Tha6A000010PrbsEngine)self);
	return Tha6A000010ModuleCounterIsSupported(prbs, counterType);
	}

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
	uint16 counterType_i = 0;
	uint16 counterType[] = {cAtPrbsEngineCounterRxSync,
							cAtPrbsEngineCounterRxErrorFrame};
	for (counterType_i=0; counterType_i< mCount(counterType); counterType_i++)
		{
		if (AtPrbsEngineCounterIsSupported(self, counterType[counterType_i]))
			HwCounterGet(self, counterType[counterType_i], clear, cAtTrue);
    	}

	return cAtOk;
    }

static void MethodsInit(Tha6A000010PrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwCreate);
        mMethodOverride(m_methods, CircuitToBind);
        mMethodOverride(m_methods, CircuitUnBind);

        mMethodOverride(m_methods, HasPrbsModeChange);
        mMethodOverride(m_methods, RegClearStatus);

        mMethodOverride(m_methods, HasPrbsDisruption);
        mMethodOverride(m_methods, RegDisruption);
        mMethodOverride(m_methods, DisruptionMask);
        mMethodOverride(m_methods, DisruptionShift);
        mMethodOverride(m_methods, HwPrbsEngineEnable);
        mMethodOverride(m_methods, POHDefaultSet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    return invert ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    return AtPrbsEngineTxInvert(self, invert);
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    return AtPrbsEngineTxInvert(self, invert);
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionTimeGet);

        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxCurrentFixedPatternGet);

        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);

        mMethodOverride(m_AtPrbsEngineOverride, DelayGet);
        mMethodOverride(m_AtPrbsEngineOverride, DelayEnable);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);

        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6A000010PrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtPw Tha6A000010PrbsEnginePwGet(AtPrbsEngine self)
    {
    if (self)
        return mThis(self)->pw;
    return NULL;
    }

uint8 Tha6A000010PrbsEngineSliceGet(AtPrbsEngine self, AtPw pw)
	{
	return PrbsEngineSliceGet(self, pw);
	}

uint32 Tha6A000010HoPrbsEngineOffset(AtPrbsEngine self, AtPw pw)
	{
	return HoPrbsEngineOffset(self, pw);
	}

uint32 Tha6A000010LoPrbsEngineOffset(AtPrbsEngine self, AtPw pw)
	{
	return LoPrbsEngineOffset(self, pw);
	}

void Tha6A000010PrbsEngineNumberCounterLatchReset(AtPrbsEngine self)
    {
    if (self)
        mThis(self)->numberCounterLatch = 0;
    }

uint8 Tha6A000010PrbsEngineNumberCounterLatch(AtPrbsEngine self)
    {
    if (self)
        return mThis(self)->numberCounterLatch;
    return cAtFalse;
    }

void Tha6A000010PrbsEngineCounterLatch(AtPrbsEngine self, uint32 value)
    {
    if (self)
        mThis(self)->lackCounterValue = value;
    }

uint32 Tha6A000010PrbsEngineCounterLatchGet(AtPrbsEngine self)
    {
    if (self)
        return mThis(self)->lackCounterValue;
    return 0;
    }

void Tha6A000010PrbsEngineCounterLatchIncrease(AtPrbsEngine self)
    {
    if (self)
        mThis(self)->numberCounterLatch = (uint8)(mThis(self)->numberCounterLatch + 1);
    }

eAtModulePrbsRet Tha6A0000PrbsEnginePOHDefaultSet(AtPrbsEngine self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(mThis(self))->POHDefaultSet(mThis(self), sdhChannel);

    return cAtErrorNullPointer;
    }

eBool Tha6A000010PrbsEngineHasPrbsHoBus(Tha6A000010PrbsEngine self)
    {
    if (self)
        return HasPrbsHoBus(self);

    return cAtErrorNullPointer;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010ModulePrbsInternal.h
 * 
 * Created Date: Nov 14, 2017
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A000010MODULEPRBSINTERNAL_H_
#define _THA6A000010MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/att/ThaAttModulePrbsInternal.h"
#include "Tha6A000010ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct Tha6A000010ModulePrbsMethods
    {
	uint32 (*StartVersionSupportShiftUp)(Tha6A000010ModulePrbs self);
    eAtRet (*SdhLineInit)(Tha6A000010ModulePrbs self);
    eAtRet (*SdhHoPathInit)(Tha6A000010ModulePrbs self);
    eAtRet (*SdhLoPathInit)(Tha6A000010ModulePrbs self);
    eAtRet (*PdhDe3Init)(Tha6A000010ModulePrbs self);
    eAtRet (*PdhDe1Init)(Tha6A000010ModulePrbs self);

    uint32 (*PrbsDelayConfigReg)(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice);
    uint32 (*PrbsDelayIdMask)(Tha6A000010ModulePrbs self, eBool isHo);
    uint32 (*PrbsDelayEnableMask)(Tha6A000010ModulePrbs self, eBool isHo);
    uint32 (*PrbsGenReg)(Tha6A000010ModulePrbs self, eBool isHo);
	uint32 (*PrbsMonReg)(Tha6A000010ModulePrbs self, eBool isHo);
	uint32 (*PrbsMaxDelayReg)(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice);
	uint32 (*PrbsMinDelayReg)(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice);
	uint32 (*PrbsAverageDelayReg)(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice);
    uint8 (*LoPrbsEngineOc48IdShift)(Tha6A000010ModulePrbs self);
    uint8 (*LoPrbsEngineOc24SliceIdShift)(Tha6A000010ModulePrbs self);
    eBool (*NeedShiftUp)(Tha6A000010ModulePrbs self);
    eAtPrbsMode (*PrbsModeFromHwValue)(Tha6A000010ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step);
    uint8 (*PrbsModeToHwValue)(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode);
    uint8 (*HwStepFromPrbsMode)(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode);
    uint32 (*PrbsModeMask)(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsDataModeMask)(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsStepMask)(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsEnableMask)(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsFixPatternMask)(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsFixPatternReg)(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx);

    uint32 (*PrbsCounterReg)(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c);
    uint32 (*PrbsCounterMask)(Tha6A000010ModulePrbs self, uint16 counterType);
    eBool (*CounterIsSupported)(Tha6A000010ModulePrbs self, uint16 counterType);
    eBool (*PrbsModeIsSupported)(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode);
    eBool (*PrbsDataModeIsSupported)(Tha6A000010ModulePrbs self);

    uint32 (*ConvertSwToHwFixedPattern)(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);
    uint32 (*ConvertHwToSwFixedPattern)(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);

    uint32 (*PrbsEngineIdFromPw)(Tha6A000010ModulePrbs self, AtPw pw);
    uint32 (*EngineCounterGet)(Tha6A000010ModulePrbs self, AtPrbsEngine engine, AtPw pw, uint16 counterType, eBool r2c);
    uint32 (*EngineFixPatternGet)(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection);
    eAtRet (*EngineFixPatternSet)(Tha6A000010ModulePrbs self, AtPw pw, uint32 fixPattern, eBool isTxDirection);
    eAtRet (*EngineModeSet)(Tha6A000010ModulePrbs self, AtPw pw, uint8 mode, eBool isTxDirection);
    uint32 (*EngineModeGet)(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection);
    uint32 (*EngineAlarmGet)(Tha6A000010ModulePrbs self, AtPw pw);
    uint32 (*EngineAlarmHistoryGet)(Tha6A000010ModulePrbs self, AtPw pw);
    }tTha6A000010ModulePrbsMethods;

typedef struct tTha6A000010ModulePrbs
    {
	tThaAttModulePrbs super;
    const tTha6A000010ModulePrbsMethods *methods;
    eBool PwIdUsed[8][2048];
    }tTha6A000010ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A000010ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);


AtModulePrbs Tha6A000010ModulePrbsV2ObjectInit(AtModulePrbs self, AtDevice device);
uint32 Tha6A000010ModulePrbsGetFreePw(Tha6A000010ModulePrbs self, uint8 slice);
eAtRet Tha6A000010ModulePrbsPwUsedSet(Tha6A000010ModulePrbs self, uint8 slice, uint32 pwId, eBool used);
#ifdef __cplusplus
}
#endif
#endif /* _THA6A000010MODULEPRBSINTERNAL_H_ */


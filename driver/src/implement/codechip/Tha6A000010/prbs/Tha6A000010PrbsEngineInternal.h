/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010PrbsEngineInternal.h
 * 
 * Created Date: Dec 3, 2015
 *
 * Description : PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A000010PRBSENGINEINTERNAL_H_
#define _THA6A000010PRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha6A000010PrbsEngine.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A000010PrbsEngineMethods
    {
    AtPw (*PwCreate)(Tha6A000010PrbsEngine self, uint32 pwId);
    AtChannel (*CircuitToBind)(Tha6A000010PrbsEngine self, AtChannel channel);
    eAtRet    (*CircuitUnBind)(Tha6A000010PrbsEngine self);

    eBool   (*HasPrbsModeChange)(Tha6A000010PrbsEngine self);
    eBool   (*HasPrbsDisruption)(Tha6A000010PrbsEngine self);

    uint32  (*RegDisruption)(Tha6A000010PrbsEngine self);
    uint32  (*DisruptionMask)(Tha6A000010PrbsEngine self);
    uint8   (*DisruptionShift)(Tha6A000010PrbsEngine self);
    uint32  (*RegClearStatus)(Tha6A000010PrbsEngine self);
    eBool (*HwPrbsEngineEnable)(Tha6A000010PrbsEngine self, AtPw pw, eBool enable);
    eAtModulePrbsRet (*POHDefaultSet)(Tha6A000010PrbsEngine self, AtSdhChannel sdhChannel);
    }tTha6A000010PrbsEngineMethods;

typedef struct tTha6A000010PrbsEngine
    {
    tAtPrbsEngine super;
    const tTha6A000010PrbsEngineMethods *methods;
    /* TODO: Must serialize the internal data */
    /* Private data */
    AtPw pw;
    uint32 pwId; /* Store to unused */
    uint32 lackCounterValue;
    eBool txEnable;
    eAtPrbsMode txPrbsMode;
    eBool rxEnable;
    eAtPrbsMode rxPrbsMode;
    eBool enable;
    uint8 numberCounterLatch;
	}tTha6A000010PrbsEngine;

typedef struct tTha6A000010PrbsEngineHoVc
    {
    tTha6A000010PrbsEngine super;
    }tTha6A000010PrbsEngineHoVc;

typedef struct tTha6A000010PrbsEngineTu3Vc
    {
    tTha6A000010PrbsEngine super;
    }tTha6A000010PrbsEngineTu3Vc;

typedef struct tTha6A000010PrbsEngineVc1x
    {
    tTha6A000010PrbsEngine super;
    }tTha6A000010PrbsEngineVc1x;

typedef struct tTha6A000010PrbsEngineDe1
    {
    tTha6A000010PrbsEngine super;

    /* Private data */
    eBool enableE1Level;
    }tTha6A000010PrbsEngineDe1;

typedef struct tTha6A000010PrbsEngineDe3
    {
    tTha6A000010PrbsEngine super;
    }tTha6A000010PrbsEngineDe3;

typedef struct tTha6A000010PrbsEngineNxDs0
    {
    tTha6A000010PrbsEngine super;
    }tTha6A000010PrbsEngineNxDs0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A000010PrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel);
AtPrbsEngine Tha6A000010PrbsEngineV2ObjectInit(AtPrbsEngine self, AtChannel channel);
eAtModulePrbsRet Tha6A0000POHDefaultSet(AtPrbsEngine self);
eAtModulePrbsRet Tha6A000010PrbsEnginePohLoPathDefaultSet(AtPrbsEngine self);
uint8 Tha6A000010PrbsEngineSliceGet(AtPrbsEngine self, AtPw pw);
uint32 Tha6A000010HoPrbsEngineOffset(AtPrbsEngine self, AtPw pw);
uint32 Tha6A000010LoPrbsEngineOffset(AtPrbsEngine self, AtPw pw);

AtPrbsEngine Tha6A000010PrbsEngineHoVcObjectInit(AtPrbsEngine self, AtSdhChannel vc);
AtPrbsEngine Tha6A000010PrbsEngineTu3VcObjectInit(AtPrbsEngine self, AtSdhChannel vc);
AtPrbsEngine Tha6A000010PrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x);
AtPrbsEngine Tha6A000010PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1);
AtPrbsEngine Tha6A000010PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3);
AtPrbsEngine Tha6A000010PrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0);

AtPrbsEngine Tha6A000010PrbsEngineHoVcV2ObjectInit(AtPrbsEngine self, AtSdhChannel vc);
AtPrbsEngine Tha6A000010PrbsEngineTu3VcV2ObjectInit(AtPrbsEngine self, AtSdhChannel vc);
AtPrbsEngine Tha6A000010PrbsEngineVc1xV2ObjectInit(AtPrbsEngine self, AtSdhChannel vc1x);
AtPrbsEngine Tha6A000010PrbsEngineDe1V2ObjectInit(AtPrbsEngine self, AtPdhDe1 de1);
AtPrbsEngine Tha6A000010PrbsEngineDe3V2ObjectInit(AtPrbsEngine self, AtPdhDe3 de3);
AtPrbsEngine Tha6A000010PrbsEngineNxDs0V2ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0);
void Tha6A000010PrbsEngineNumberCounterLatchReset(AtPrbsEngine self);
uint8 Tha6A000010PrbsEngineNumberCounterLatch(AtPrbsEngine self);
void Tha6A000010PrbsEngineCounterLatch(AtPrbsEngine self, uint32 value);
uint32 Tha6A000010PrbsEngineCounterLatchGet(AtPrbsEngine self);
void Tha6A000010PrbsEngineCounterLatchIncrease(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A000010PRBSENGINEINTERNAL_H_ */


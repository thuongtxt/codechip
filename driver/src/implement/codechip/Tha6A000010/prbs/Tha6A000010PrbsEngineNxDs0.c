/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010PrbsEngineNxDs0.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : PRBS of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "Tha6A000010PrbsEngineInternal.h"
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static eBool HwPrbsEngineEnable(Tha6A000010PrbsEngine self, AtPw pw, eBool enable)
    {
    ThaModuleAbstractMap mapDemapModule;
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint32 mask = AtPdhNxDS0BitmapGet(nxDs0);
    AtChannel de1 = (AtChannel)AtPdhNxDS0De1Get(nxDs0);
    eAtRet ret;

    if (Tha6A000010PrbsEngineHasPrbsHoBus(self))
        return mMethodsGet(self)->HwPrbsEngineEnable(self, pw, enable);

    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret = ThaModuleAbstractMapDs0EncapConnectionEnable(mapDemapModule, de1, mask, enable);
    mapDemapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    ret = ThaModuleAbstractMapDs0EncapConnectionEnable(mapDemapModule, de1, mask, enable);

    return ret;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010PrbsEngineNxDs0);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, HwPrbsEngineEnable);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)nxDs0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A000010PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, nxDs0);
    }

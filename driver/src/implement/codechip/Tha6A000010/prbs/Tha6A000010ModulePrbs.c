/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010ModulePrbs.c
 *
 * Created Date: Sep 8, 2015
 *
 * Description : PRBS module of 6A000010
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/att/ThaAttPrbsManager.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha6A000010PrbsEngine.h"
#include "Tha6A000010ModulePrbsInternal.h"
#include "Tha6A000010PrbsEngineReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010ModulePrbsMethods m_Tha6A000010ModulePrbsOverride;
static tThaAttModulePrbsMethods  m_ThaAttModulePrbsOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tAtModuleMethods      m_AtModuleOverride;
static tTha6A000010ModulePrbsMethods m_Tha6A000010ModulePrbsOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static ThaAttPrbsRegProvider RegProviderCreate(ThaAttModulePrbs self)
    {
    return Tha6A000010AttPrbsRegProviderNew(self);
    }

static uint32 ConvertSwToHwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
	{
	AtUnused(self);
	AtUnused(prbsMode);
	return fixedPattern;
	}

static uint32 ConvertHwToSwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
	{
	AtUnused(self);
	AtUnused(prbsMode);
	return fixedPattern;
	}

static eBool PrbsModeIsSupported(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	AtUnused(self);
	return ((prbsMode == cAtPrbsModePrbs15)
			|| (prbsMode == cAtPrbsModePrbsSeq)
			|| (prbsMode == cAtPrbsModePrbsFixedPattern1Byte))
			? cAtTrue : cAtFalse;
	}

static eBool PrbsDataModeIsSupported(Tha6A000010ModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportShiftUp(Tha6A000010ModulePrbs self)
	{
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x2, 0x1067);
	}

static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool NeedShiftUp(Tha6A000010ModulePrbs self)
	{
	uint32 startVersionHasThis = StartVersionSupportShiftUp(self);
	uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
	return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
	}

static uint32 PrbsEnableMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection ? c_prbshogenctl_PrbsHoGenCtlEnb_Mask: cInvalidUint32;
	return isTxDirection ?c_prbslogenctl_PrbsLoGenCtlEnb_Mask: cInvalidUint32;
	}

static uint32 PrbsModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection?c_prbshogenctl_PrbsHoGenCtlSeqMod_Mask: c_prbshomon_PrbsHoMonCtlSeqMod_Mask;
	return isTxDirection?c_prbslogenctl_PrbsLoGenCtlSeqMod_Mask: c_prbslomon_PrbsLoMonCtlSeqMod_Mask;
	}

static uint32 PrbsDataModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    AtUnused(self);
    AtUnused(isHo);
    AtUnused(isTxDirection);
    return cInvalidUint32;
    }

static uint32 PrbsStepMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection ? cAtt_prbshogenctl_seqstep_Mask : cAtt_prbshomon_seqstep_Mask;
	return isTxDirection ?cAtt_prbslogenctl_seqstep_Mask: cAtt_prbslomon_seqstep_Mask;
	}

static uint32 PrbsFixPatternMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection? cAtt_prbshogenctl_seqdat_Mask: cAtt_prbshomon_seqdat_Mask;
	return isTxDirection ?cAtt_prbslogenctl_seqdat_Mask: cAtt_prbslomon_seqdat_Mask;
	}

static uint8 PrbsModeToHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	AtUnused(self);
    return (prbsMode == cAtPrbsModePrbsSeq) ? 1 : 0;
	}

static eAtPrbsMode PrbsModeFromHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
	{
	AtUnused(self);
	AtUnused(catchedPrbsMode);
	if (mode == 0)
		return cAtPrbsModePrbs15;
	else
		return (step ? cAtPrbsModePrbsSeq : cAtPrbsModePrbsFixedPattern1Byte);
	}

static uint8 HwStepFromPrbsMode(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
    {
	uint8 step = 0;
	AtUnused(self);
    if (prbsMode==cAtPrbsModePrbsSeq)
        step = 1;
    return step;
    }

static uint32 PrbsFixPatternReg(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
	{
	if (isHo)
		return isTx? mMethodsGet(self)->PrbsGenReg(self, cAtTrue):mMethodsGet(self)->PrbsMonReg(self, cAtTrue);
	return isTx? mMethodsGet(self)->PrbsGenReg(self, cAtFalse):mMethodsGet(self)->PrbsMonReg(self, cAtFalse);
	}

static eBool CounterIsSupported(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	AtUnused(self);
	AtUnused(counterType);
	return cAtFalse;
	}

static uint32 PrbsCounterMask(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	AtUnused(self);
	AtUnused(counterType);
	return cInvalidUint32;
	}

static uint32 PrbsCounterReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	return cInvalidUint32;
	}


static AtAttPrbsManager AttPrbsManagerCreate(AtModulePrbs self)
    {
    AtUnused(self);
    return ThaAttPrbsManagerNew(self);
    }

static eAtRet InitRoundStripDelay(Tha6A000010ModulePrbs self)
	{
	uint32 address = 0;
	uint8 slice = 0;

	for (slice=0; slice<4; slice++)
		{
		address = Tha6A000010ModulePrbsDelayConfigReg(self, cAtTrue, slice);
		if (address != cInvalidUint32)
			mModuleHwWrite(self, address, 0);
		}
	for (slice=0; slice<2; slice++)
		{
		address = Tha6A000010ModulePrbsDelayConfigReg(self, cAtFalse, slice);
		if (address != cInvalidUint32)
			mModuleHwWrite(self, address, 0);
		}

	return cAtOk;
	}

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;
    ret |= InitRoundStripDelay((Tha6A000010ModulePrbs)self);
    ret |= m_AtModuleMethods->Init(self);
    return ret;
    }
    
static eBool NeedPwInitConfig(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A000010PrbsEngineDe1New(de1);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A000010PrbsEngineNxDs0New(nxDs0);
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);
    AtUnused(engineId);

    if (channelType == cAtSdhChannelTypeVc4 || channelType ==cAtSdhChannelTypeVc4_4c || channelType ==cAtSdhChannelTypeVc4_16c)
        return Tha6A000010PrbsEngineHoVcNew(sdhVc);

    if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            return Tha6A000010PrbsEngineHoVcNew(sdhVc);
        return Tha6A000010PrbsEngineTu3VcNew(sdhVc);
        }

    if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        return Tha6A000010PrbsEngineVc1xNew(sdhVc);

    return NULL;
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A000010PrbsEngineDe3New(de3);
    }

static eBool AllChannelsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "vc3, vc4, vc1x, de1, nxds0";
    }

static uint32 PrbsGenReg(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return 0x068000/*cReg_prbshomon_Base*/;

	if (Tha6A000010ModulePrbsNeedShiftUp(self))
		return 0x500800;
	return 0x500000/*cReg_prbslomon_Base*/;
	}

static uint32 PrbsMonReg(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return 0x068040/*cReg_prbshomon_Base*/;
	return 0x501000/*cReg_prbslomon_Base*/;
	}

static uint8 LoPrbsEngineOc48IdShift(Tha6A000010ModulePrbs self)
    {
    AtUnused(self);
    if (Tha6A000010ModulePrbsNeedShiftUp(self))
    	return 21;
    return 19;
    }

static uint8 LoPrbsEngineOc24SliceIdShift(Tha6A000010ModulePrbs self)
    {
	if (Tha6A000010ModulePrbsNeedShiftUp(self))
		return 13;
    return 11;
    }

static uint32 PrbsDelayConfigReg(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsDelayIdMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	AtUnused(isHo);
	return cInvalidUint32;
	}

static uint32 PrbsDelayEnableMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	AtUnused(isHo);
	return cInvalidUint32;
	}

static uint32 PrbsMaxDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsMinDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static uint32 PrbsAverageDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	AtUnused(self);
	AtUnused(isHo);
	AtUnused(r2c);
	AtUnused(slice);
	return cInvalidUint32;
	}

static eBool NeedDelayAverage(ThaAttModulePrbs self)
	{
	uint32 startVersionHasThis = mMethodsGet((ThaAttModulePrbs)self)->StartVersionSupportDelayAverage((ThaAttModulePrbs)self);
	uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
	return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
	}

static uint32 PrbsEngineIdFromPw(Tha6A000010ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 EngineCounterGet(Tha6A000010ModulePrbs self, AtPrbsEngine engine, AtPw pw, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(engine);
    AtUnused(counterType);
    AtUnused(r2c);
    return 0;
    }

static uint32 EngineFixPatternGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(isTxDirection);
    return 0;
    }

static eAtRet EngineFixPatternSet(Tha6A000010ModulePrbs self, AtPw pw, uint32 fixPattern, eBool isTxDirection)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(fixPattern);
    AtUnused(isTxDirection);
    return cAtErrorNotImplemented;
    }

static eAtRet EngineModeSet(Tha6A000010ModulePrbs self, AtPw pw, uint8 mode, eBool isTxDirection)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(mode);
    AtUnused(isTxDirection);
    return cAtErrorNotImplemented;
    }

static uint32 EngineModeGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(isTxDirection);
    return 0;
    }

static uint32 EngineAlarmGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static uint32 EngineAlarmHistoryGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static void OverrideThaAttModulePrbs(ThaAttModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttModulePrbsOverride, mMethodsGet(self), sizeof(m_ThaAttModulePrbsOverride));

        mMethodOverride(m_ThaAttModulePrbsOverride, RegProviderCreate);
        mMethodOverride(m_ThaAttModulePrbsOverride, NeedDelayAverage);
        }

    mMethodsSet(self, &m_ThaAttModulePrbsOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, NeedPwInitConfig);
        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, AllChannelsSupported);
        mMethodOverride(m_AtModulePrbsOverride, AttPrbsManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
		mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideThaAttModulePrbs((ThaAttModulePrbs) self);
    }

static void MethodsInit(Tha6A000010ModulePrbs self)
    {
    if (!m_methodsInit)
        {
    	mMethodOverride(m_Tha6A000010ModulePrbsOverride, NeedShiftUp);
    	mMethodOverride(m_Tha6A000010ModulePrbsOverride, StartVersionSupportShiftUp);
    	mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMonReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsGenReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, LoPrbsEngineOc48IdShift);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, LoPrbsEngineOc24SliceIdShift);

        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayConfigReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayIdMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayEnableMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMaxDelayReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMinDelayReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsAverageDelayReg);

        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeFromHwValue);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeToHwValue);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, HwStepFromPrbsMode);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDataModeMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsStepMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsEnableMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, CounterIsSupported);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeIsSupported);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDataModeIsSupported);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, ConvertSwToHwFixedPattern);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, ConvertHwToSwFixedPattern);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsEngineIdFromPw);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineCounterGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineFixPatternGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineFixPatternSet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineModeSet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineModeGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineAlarmGet);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, EngineAlarmHistoryGet);
        }

    mMethodsSet(self, &m_Tha6A000010ModulePrbsOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A000010ModulePrbs);
    }

AtModulePrbs Tha6A000010ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAttModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha6A000010ModulePrbs)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A000010ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A000010ModulePrbsObjectInit(newModule, device);
    }

uint32 Tha6A000010ModulePrbsMonReg(Tha6A000010ModulePrbs self, eBool isHo)
	{
	if (self)
		return mMethodsGet(self)->PrbsMonReg(self, isHo);
	return cInvalidUint32;
	}

uint32 Tha6A000010ModulePrbsGenReg(Tha6A000010ModulePrbs self, eBool isHo)
	{
	if (self)
		return mMethodsGet(self)->PrbsGenReg(self, isHo);
	return cInvalidUint32;
	}

eBool Tha6A000010ModulePrbsNeedShiftUp(Tha6A000010ModulePrbs self)
    {
    if (self)
    	return mMethodsGet(self)->NeedShiftUp(self);
    return cAtFalse;
    }
eAtPrbsMode Tha6A000010ModulePrbsModeFromHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
	{
	if (self)
		return mMethodsGet(self)->PrbsModeFromHwValue(self, catchedPrbsMode, mode, step);
	return cAtFalse;
	}

uint8 Tha6A000010ModulePrbsHwStepFromPrbsMode(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	if (self)
		return mMethodsGet(self)->HwStepFromPrbsMode(self, prbsMode);
	return 0;
	}

uint8 Tha6A000010ModulePrbsModeToHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	if (self)
		return mMethodsGet(self)->PrbsModeToHwValue(self, prbsMode);
	return 0;
	}
uint32 Tha6A000010ModulePrbsModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (self)
		return mMethodsGet(self)->PrbsModeMask(self, isHo, isTxDirection);
	return 0;
	}

uint32 Tha6A000010ModulePrbsDataModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsDataModeMask(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A000010ModulePrbsStepMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (self)
		return mMethodsGet(self)->PrbsStepMask(self, isHo, isTxDirection);
	return 0;
	}

uint32 Tha6A000010ModulePrbsEnableMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (self)
		return mMethodsGet(self)->PrbsEnableMask(self, isHo, isTxDirection);
	return 0;
	}

uint32 Tha6A000010ModulePrbsFixPatternMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (self)
		return mMethodsGet(self)->PrbsFixPatternMask(self, isHo, isTxDirection);
	return 0;
	}

uint32 Tha6A000010ModulePrbsFixPatternReg(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (self)
		return mMethodsGet(self)->PrbsFixPatternReg(self, isHo, isTxDirection);
	return 0;
	}

uint32 Tha6A000010ModulePrbsCounterReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c)
	{
	if (self)
		return mMethodsGet(self)->PrbsCounterReg(self, isHo, r2c);
	return cInvalidUint32;
	}

uint32 Tha6A000010ModulePrbsCounterMask(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	if (self)
		return mMethodsGet(self)->PrbsCounterMask(self, counterType);
	return cInvalidUint32;
	}

eBool Tha6A000010ModuleCounterIsSupported(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	if (self)
		return mMethodsGet(self)->CounterIsSupported(self, counterType);
	return cAtFalse;
	}

uint32 Tha6A000010ModulePrbsDelayConfigReg(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice)
	{
	if (self)
		return mMethodsGet(self)->PrbsDelayConfigReg(self, isHo, slice);
	return cInvalidUint32;
	}

uint32 Tha6A000010ModulePrbsDelayIdMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	if (self)
		return mMethodsGet(self)->PrbsDelayIdMask(self, isHo);
	return cInvalidUint32;
	}

uint32 Tha6A000010ModulePrbsDelayEnableMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	if (self)
		return mMethodsGet(self)->PrbsDelayEnableMask(self, isHo);
	return cInvalidUint32;
	}

eBool Tha6A000010ModulePrbsModeIsSupported(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	if (self)
		return mMethodsGet(self)->PrbsModeIsSupported(self, prbsMode);
	return cAtFalse;
	}

eBool Tha6A000010ModulePrbsDataModeIsSupported(Tha6A000010ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsDataModeIsSupported(self);
    return cAtFalse;
    }

uint32 Tha6A000010ModulePrbsConvertSwToHwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
	{
	if (self)
		return mMethodsGet(self)->ConvertSwToHwFixedPattern(self, prbsMode, fixedPattern);
	return 0;
	}

uint32 Tha6A000010ModulePrbsConvertHwToSwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
	{
	if (self)
		return mMethodsGet(self)->ConvertHwToSwFixedPattern(self, prbsMode, fixedPattern);
	return 0;
	}

uint32 Tha6A000010ModulePrbsEngineAlarmGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->EngineAlarmGet(self, pw);

    return 0;
    }

uint32 Tha6A000010ModulePrbsEngineAlarmHistoryGet(Tha6A000010ModulePrbs self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->EngineAlarmHistoryGet(self, pw);

    return 0;
    }

eAtRet Tha6A000010ModulePrbsEngineModeSet(Tha6A000010ModulePrbs self, AtPw pw, uint8 mode, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->EngineModeSet(self, pw, mode, isTxDirection);

    return cAtErrorNullPointer;
    }

uint32 Tha6A000010ModulePrbsEngineModeGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->EngineModeGet(self, pw, isTxDirection);

    return cAtPrbsModeInvalid;
    }

uint32 Tha6A000010ModulePrbsEngineFixPatternGet(Tha6A000010ModulePrbs self, AtPw pw, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->EngineFixPatternGet(self, pw, isTxDirection);

    return 0;
    }

eAtRet Tha6A000010ModulePrbsEngineFixPatternSet(Tha6A000010ModulePrbs self, AtPw pw, uint32 fixPattern, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->EngineFixPatternSet(self, pw, fixPattern, isTxDirection);

    return cAtErrorNullPointer;
    }

uint32 Tha6A000010ModulePrbsEngineCounterGet(Tha6A000010ModulePrbs self, AtPrbsEngine engine, AtPw pw, uint16 counterType, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EngineCounterGet(self, engine, pw, counterType, r2c);

    return 0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010PrbsEngineReg.h
 * 
 * Created Date: Sep 8, 2015
 *
 * Description : PRBS register of 6A000010
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A000010PRBSENGINEREG_H_
#define _THA6A000010PRBSENGINEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : PRBS_LOMAP Generator Control
Reg Addr   : 0x50_0000 - 0x50_5fff
Reg Formula: 0x50_0000 + 65536*2*Slice48Id + 2048*Slice24Id + PwId
    Where  :
           + $Slice48Id(0-5):
           + $Slice24Id(0-1):
           + $PwId(0-671) : 1344 div 2
Reg Desc   :
Each register is used to configure PRBS generator at MAP.

------------------------------------------------------------------------------*/
#define cReg_prbslogenctl_Base                                                                        0x500000
#define cReg_prbslogenctl(Slice48Id, Slice24Id, PwId)                 (0x500000+65536*2*(Slice48Id)+2048*(Slice24Id)+(PwId))
#define cReg_prbslogenctl_WidthVal                                                                          32
#define cReg_prbslogenctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [16:10]
--------------------------------------*/
#define cAtt_prbslogenctl_seqstep_Mask                                cBit16_10
#define cAtt_prbslogenctl_seqstep_Shift                                      10


/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [9:2]
--------------------------------------*/
#define cAtt_prbslogenctl_seqdat_Mask                                   cBit9_2
#define cAtt_prbslogenctl_seqdat_Shift                                        2

/*--------------------------------------
BitField Name: PrbsLoGenCtlSeqMod
BitField Type: RW
BitField Desc: Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode
BitField Bits: [1]
--------------------------------------*/
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_Bit_Start                                                          1
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_Bit_End                                                            1
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_Mask                                                           cBit1
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_Shift                                                              1
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_MaxVal                                                           0x1
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_MinVal                                                           0x0
#define c_prbslogenctl_PrbsLoGenCtlSeqMod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PrbsLoGenCtlEnb
BitField Type: RW
BitField Desc: Enable PRBS generator. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define c_prbslogenctl_PrbsLoGenCtlEnb_Bit_Start                                                             0
#define c_prbslogenctl_PrbsLoGenCtlEnb_Bit_End                                                               0
#define c_prbslogenctl_PrbsLoGenCtlEnb_Mask                                                              cBit0
#define c_prbslogenctl_PrbsLoGenCtlEnb_Shift                                                                 0
#define c_prbslogenctl_PrbsLoGenCtlEnb_MaxVal                                                              0x1
#define c_prbslogenctl_PrbsLoGenCtlEnb_MinVal                                                              0x0
#define c_prbslogenctl_PrbsLoGenCtlEnb_RstVal                                                              0x0

/*------------------------------------------------------------------------------
Reg Name   : PRBS_LOMAP Monitor
Reg Addr   : 0x501000 - 0x50_ffff
Reg Formula: 0x501000 + 65536*2*Slice48Id + 2048*Slice24Id + PwId
    Where  :
           + $Slice48Id(0-5):
           + $Slice24Id(0-1):
           + $PwId(0-671) : 1344 div 2
Reg Desc   :
Each register is used for PRBS monitor at DEMAP.

------------------------------------------------------------------------------*/
#define cReg_prbslomon_Base                                                                           0x501000
#define cReg_prbslomon(Slice48Id, Slice24Id, PwId)                    (0x501000+65536*2*(Slice48Id)+2048*(Slice24Id)+(PwId))
#define cReg_prbslomon_WidthVal                                                                             32
#define cReg_prbslomon_WriteMask                                                                           0x0

/*--------------------------------------
BitField Name: PrbsLoMonErr
BitField Type: RC
BitField Desc: PRBS error. 1: Error 0: Not error
BitField Bits: [17]
--------------------------------------*/
#define c_prbslomon_PrbsLoMonErr_Bit_Start                                                                  17
#define c_prbslomon_PrbsLoMonErr_Bit_End                                                                    17
#define c_prbslomon_PrbsLoMonErr_Mask                                                                   cBit17
#define c_prbslomon_PrbsLoMonErr_Shift                                                                      17
#define c_prbslomon_PrbsLoMonErr_MaxVal                                                                    0x1
#define c_prbslomon_PrbsLoMonErr_MinVal                                                                    0x0
#define c_prbslomon_PrbsLoMonErr_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PrbsLoMonSync
BitField Type: RO
BitField Desc: PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not
error) 0: Not sync
BitField Bits: [16]
--------------------------------------*/
#define c_prbslomon_PrbsLoMonSync_Bit_Start                                                                 16
#define c_prbslomon_PrbsLoMonSync_Bit_End                                                                   16
#define c_prbslomon_PrbsLoMonSync_Mask                                                                  cBit16
#define c_prbslomon_PrbsLoMonSync_Shift                                                                     16
#define c_prbslomon_PrbsLoMonSync_MaxVal                                                                   0x1
#define c_prbslomon_PrbsLoMonSync_MinVal                                                                   0x0
#define c_prbslomon_PrbsLoMonSync_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [15:9]
--------------------------------------*/
#define cAtt_prbslomon_seqstep_Mask                                                                     cBit15_9
#define cAtt_prbslomon_seqstep_Shift                                                                           9

/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [8:1]
--------------------------------------*/
#define cAtt_prbslomon_seqdat_Mask                                                                       cBit8_1
#define cAtt_prbslomon_seqdat_Shift                                                                            1

/*--------------------------------------
BitField Name: PrbsLoMonCtlSeqMod
BitField Type: RW
BitField Desc: Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode
BitField Bits: [0]
--------------------------------------*/
#define c_prbslomon_PrbsLoMonCtlSeqMod_Bit_Start                                                             0
#define c_prbslomon_PrbsLoMonCtlSeqMod_Bit_End                                                               0
#define c_prbslomon_PrbsLoMonCtlSeqMod_Mask                                                              cBit0
#define c_prbslomon_PrbsLoMonCtlSeqMod_Shift                                                                 0
#define c_prbslomon_PrbsLoMonCtlSeqMod_MaxVal                                                              0x1
#define c_prbslomon_PrbsLoMonCtlSeqMod_MinVal                                                              0x0
#define c_prbslomon_PrbsLoMonCtlSeqMod_RstVal                                                              0x0

/*------------------------------------------------------------------------------
Reg Name   : PRBS_HOMAP Generator Control
Reg Addr   : 0x06_8000 - 0x06_F03F
Reg Formula: 0x06_8000 + 2048*Slice48Id + stsId
    Where  :
           + $Slice48Id(0-7):
           + $stsId(0-47) :
Reg Desc   :
Each register is used to configure PRBS generator at MAP.

------------------------------------------------------------------------------*/
#define cReg_prbshogenctl_Base                                                                        0x068000
#define cReg_prbshogenctl(Slice48Id, stsId)                                (0x068000+2048*(Slice48Id)+(stsId))
#define cReg_prbshogenctl_WidthVal                                                                          32
#define cReg_prbshogenctl_WriteMask                                                                        0x0


/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [16:10]
--------------------------------------*/
#define cAtt_prbshogenctl_seqstep_Mask                                cBit16_10
#define cAtt_prbshogenctl_seqstep_Shift                                      10

/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [9:2]
--------------------------------------*/
#define cAtt_prbshogenctl_seqdat_Mask                                   cBit9_2
#define cAtt_prbshogenctl_seqdat_Shift                                        2


/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [9:2]
--------------------------------------*/
#define cAtt_prbshogenctl_seqdat_Mask                                   cBit9_2
#define cAtt_prbshogenctl_seqdat_Shift                                        2


/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [9:2]
--------------------------------------*/
#define cAtt_prbshogenctl_seqdat_Mask                                   cBit9_2
#define cAtt_prbshogenctl_seqdat_Shift                                        2


/*--------------------------------------
BitField Name: PrbsHoGenCtlSeqMod
BitField Type: RW
BitField Desc: Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode
BitField Bits: [1]
--------------------------------------*/
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_Bit_Start                                                          1
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_Bit_End                                                            1
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_Mask                                                           cBit1
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_Shift                                                              1
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_MaxVal                                                           0x1
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_MinVal                                                           0x0
#define c_prbshogenctl_PrbsHoGenCtlSeqMod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PrbsHoGenCtlEnb
BitField Type: RW
BitField Desc: Enable PRBS generator. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define c_prbshogenctl_PrbsHoGenCtlEnb_Bit_Start                                                             0
#define c_prbshogenctl_PrbsHoGenCtlEnb_Bit_End                                                               0
#define c_prbshogenctl_PrbsHoGenCtlEnb_Mask                                                              cBit0
#define c_prbshogenctl_PrbsHoGenCtlEnb_Shift                                                                 0
#define c_prbshogenctl_PrbsHoGenCtlEnb_MaxVal                                                              0x1
#define c_prbshogenctl_PrbsHoGenCtlEnb_MinVal                                                              0x0
#define c_prbshogenctl_PrbsHoGenCtlEnb_RstVal                                                              0x0

/*------------------------------------------------------------------------------
Reg Name   : PRBS_HOMAP Monitor
Reg Addr   : 0x06_8040 - 0x06_F07F
Reg Formula: 0x06_8040 + 2048*Slice48Id + stsId
    Where  :
           + $Slice48Id(0-7):
           + $stsId(0-47) :
Reg Desc   :
Each register is used for PRBS monitor at DEMAP.

------------------------------------------------------------------------------*/
#define cReg_prbshomon_Base                                                                           0x068040
#define cReg_prbshomon(Slice48Id, stsId)                                   (0x068040+2048*(Slice48Id)+(stsId))
#define cReg_prbshomon_WidthVal                                                                             32
#define cReg_prbshomon_WriteMask                                                                           0x0

/*--------------------------------------
BitField Name: PrbsHoMonErr
BitField Type: RC
BitField Desc: PRBS error. 1: Error 0: Not error
BitField Bits: [17]
--------------------------------------*/
#define c_prbshomon_PrbsHoMonErr_Bit_Start                                                                  17
#define c_prbshomon_PrbsHoMonErr_Bit_End                                                                    17
#define c_prbshomon_PrbsHoMonErr_Mask                                                                   cBit17
#define c_prbshomon_PrbsHoMonErr_Shift                                                                      17
#define c_prbshomon_PrbsHoMonErr_MaxVal                                                                    0x1
#define c_prbshomon_PrbsHoMonErr_MinVal                                                                    0x0
#define c_prbshomon_PrbsHoMonErr_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PrbsHoMonSync
BitField Type: RO
BitField Desc: PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not
error) 0: Not sync
BitField Bits: [16]
--------------------------------------*/
#define c_prbshomon_PrbsHoMonSync_Bit_Start                                                                 16
#define c_prbshomon_PrbsHoMonSync_Bit_End                                                                   16
#define c_prbshomon_PrbsHoMonSync_Mask                                                                  cBit16
#define c_prbshomon_PrbsHoMonSync_Shift                                                                     16
#define c_prbshomon_PrbsHoMonSync_MaxVal                                                                   0x1
#define c_prbshomon_PrbsHoMonSync_MinVal                                                                   0x0
#define c_prbshomon_PrbsHoMonSync_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [15:9]
--------------------------------------*/
#define cAtt_prbshomon_seqstep_Mask                                                                     cBit15_9
#define cAtt_prbshomon_seqstep_Shift                                                                           9

/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [8:1]
--------------------------------------*/
#define cAtt_prbshomon_seqdat_Mask                                                                       cBit8_1
#define cAtt_prbshomon_seqdat_Shift                                                                            1

/*--------------------------------------
BitField Name: PrbsHoMonCtlSeqMod
BitField Type: RW
BitField Desc: Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode
BitField Bits: [0]
--------------------------------------*/
#define c_prbshomon_PrbsHoMonCtlSeqMod_Bit_Start                                                             0
#define c_prbshomon_PrbsHoMonCtlSeqMod_Bit_End                                                               0
#define c_prbshomon_PrbsHoMonCtlSeqMod_Mask                                                              cBit0
#define c_prbshomon_PrbsHoMonCtlSeqMod_Shift                                                                 0
#define c_prbshomon_PrbsHoMonCtlSeqMod_MaxVal                                                              0x1
#define c_prbshomon_PrbsHoMonCtlSeqMod_MinVal                                                              0x0
#define c_prbshomon_PrbsHoMonCtlSeqMod_RstVal                                                              0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA6A000010PRBSENGINEREG_H_ */


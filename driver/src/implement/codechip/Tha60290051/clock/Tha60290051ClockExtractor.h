/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60290051ClockExtractor.h
 * 
 * Created Date: Sep 7, 2018
 *
 * Description : 60290051 clock extractor interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051CLOCKEXTRACTOR_H_
#define _THA60290051CLOCKEXTRACTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleClock.h"
#include "AtClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor Tha60290051ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051CLOCKEXTRACTOR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha60290051ModuleClock.c
 *
 * Created Date: Sep 7, 2018
 *
 * Description : 60290051 module clock implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290051ModuleClockInternal.h"
#include "Tha60290051ClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClockMethods m_ThaModuleClockOverride;

/* Super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SquelchingIsSupported(ThaModuleClock self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return Tha60290051ClockExtractorNew((AtModuleClock)self, extractorId);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, SquelchingIsSupported);
        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideThaModuleClock(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051ModuleClock);
    }

static AtModuleClock ObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60290051ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

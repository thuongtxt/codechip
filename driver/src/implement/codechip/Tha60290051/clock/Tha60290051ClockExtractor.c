/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha60290051ClockExtractor.c
 *
 * Created Date: Sep 7, 2018
 *
 * Description : Implementation for clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "Tha60290051ClockExtractorInternal.h"
#include "../physical/diag/Tha60290051TOPGLBReg.h"
#include "Tha60290051ClockExtractor.h"
#include "../man/Tha60290051GlbReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cTenGeSerdesId_0 0
#define cTenGeSerdesId_1 8
#define cTenGeSerdesId_2 16
#define cTenGeSerdesId_3 24
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClockExtractorMethods m_ThaClockExtractorOverride;
static tTha60290021ClockExtractorMethods m_Tha60290021ClockExtractorOverride;
static tTha60290022ClockExtractorMethods m_Tha60290022ClockExtractorOverride;

/* Supper implementations */
static const tThaClockExtractorMethods *m_ThaClockExtractorMethods = NULL;
static const tTha60290021ClockExtractorMethods *m_Tha60290021ClockExtractorMethods = NULL;
static const tTha60290022ClockExtractorMethods *m_Tha60290022ClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return cAf6Reg_ProductID_Base;
    }

static uint8 SerdesModeSw2Hw(AtSerdesController serdes)
    {
    eAtSerdesMode serdesMode = AtSerdesControllerOverSamplingMode(serdes);

    if ((serdesMode == cAtSerdesModeEth1G) ||
        (serdesMode == cAtSerdesModeEth100M))
        return 0x0;

    if (serdesMode == cAtSerdesModeEth10G)
        return 0x1;

    return 0x0;
    }

static eAtRet HwRateSet(AtClockExtractor self, uint32 portId, uint8 hwRateValue)
    {
    uint32 regAddr = cAf6Reg_o_control8_Base + BaseAddress();
    uint32 regVal = AtClockExtractorRead(self, regAddr);
    mFieldIns(&regVal, cAf6_o_control8_cfgrate_Mask(portId), cAf6_o_control8_cfgrate_Shift(portId), hwRateValue);
    AtClockExtractorWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtModuleClockRet HwSerdesClockExtract(ThaClockExtractor self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;
    uint32 regAddr = cAf6Reg_o_control7_Base + BaseAddress();
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor) self);
    uint32 regVal = AtClockExtractorRead((AtClockExtractor)self, regAddr);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    uint32 diagEnForSquelching = Tha60290021ClockExtractorHwClockExtractShouldUseDiag((Tha60290021ClockExtractor)self, serdes) ? 1 : 0;

    if (extractorId == 1) /* Need to write this field on extractor 2 */
        mRegFieldSet(regVal, cAf6_o_control7_cfgref2mod_, 0);

    if (extractorId == 0)
        mFieldIns(&regVal, cAf6_o_control7_cfgref1pid_Mask, cAf6_o_control7_cfgref1pid_Shift, serdesId);
    else if (extractorId == 1)
        mFieldIns(&regVal, cAf6_o_control7_cfgref2pid_Mask, cAf6_o_control7_cfgref2pid_Shift, serdesId);

    AtClockExtractorWrite((AtClockExtractor)self, regAddr, regVal);

    /*cfg refout8k*/
    regAddr = cAf6Reg_o_control10_Base + BaseAddress();
    regVal = AtClockExtractorRead((AtClockExtractor)self, regAddr);
    mFieldIns(&regVal, cAf6_o_control10_cfgdiagen_Mask(serdesId), cAf6_o_control10_cfgdiagen_Shift(serdesId), diagEnForSquelching);
    AtClockExtractorWrite((AtClockExtractor)self, regAddr, regVal);

    ret |= HwRateSet((AtClockExtractor)self, serdesId, SerdesModeSw2Hw(serdes));
    ret |= Tha60290022ClockExtractorEthSerdesDefaultSquelchingOptionSet(self, serdes);
    return ret;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtErrorModeNotSupport;
    }

static eBool HwClockExtractShouldUseDiag(Tha60290021ClockExtractor self, AtSerdesController serdes)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet((AtClockExtractor)self));
    AtUnused(serdes);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static uint8 TenGeSerdesIdToLocalHwId(uint8 serdesId)
    {
    if (serdesId == cTenGeSerdesId_0)
        return 0;
    if (serdesId == cTenGeSerdesId_1)
        return 1;
    if (serdesId == cTenGeSerdesId_2)
        return 2;
    if (serdesId == cTenGeSerdesId_3)
        return 3;
    return 0;
    }

static eBool IsValidTenGeSerdesId(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    AtUnused(self);
    if ((serdesId == cTenGeSerdesId_0) || (serdesId == cTenGeSerdesId_1) || (serdesId == cTenGeSerdesId_2) || (serdesId == cTenGeSerdesId_3))
        return cAtTrue;
    return cAtFalse;
    }

static ThaModuleClock ModuleClock(Tha60290022ClockExtractor self)
    {
    ThaModuleClock moduleClock = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    return moduleClock;
    }

static eAtModuleClockRet Hw1GeSerdesClockSquelOptionSet(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options)
    {
    uint32 address = cAf6Reg_GlbEth1GClkEerSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 errField_Mask, errField_Shift, errBinVal = 0, linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    errField_Mask = cAf6_EthGeExcessErrRateSchelEn_Mask(serdesId);
    errField_Shift = cAf6_EthGeExcessErrRateSchelEn_Shift(serdesId);
    if (options & cAtClockExtractorSquelchingEthEer)
        errBinVal = 1;
    mRegFieldSet(regVal, errField_, errBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);

    address = cAf6Reg_GlbEth1GClkLinkDownSquelControl_Base;
    regVal = mModuleHwRead(ModuleClock(self), address);
    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    if (options & cAtClockExtractorSquelchingEthLinkDown)
        linkDownBinVal = 1;
    mRegFieldSet(regVal, linkDownField_, linkDownBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);
    return cAtOk;
    }

static eAtClockExtractorSquelching Hw1GeSerdesClockSquelOptionGet(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    eAtClockExtractorSquelching options = cAtClockExtractorSquelchingNone;
    uint32 address = cAf6Reg_GlbEth1GClkEerSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 errField_Mask, errField_Shift, errBinVal = 0, linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    errField_Mask = cAf6_EthGeExcessErrRateSchelEn_Mask(serdesId);
    errField_Shift = cAf6_EthGeExcessErrRateSchelEn_Shift(serdesId);
    errBinVal = mRegField(regVal, errField_);
    if (errBinVal == 1)
        options |= cAtClockExtractorSquelchingEthEer;

    address = cAf6Reg_GlbEth1GClkLinkDownSquelControl_Base;
    regVal = mModuleHwRead(ModuleClock(self), address);
    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    linkDownBinVal = mRegField(regVal, linkDownField_);
    if (linkDownBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLinkDown;

    return options;
    }

static eAtModuleClockRet Hw100MSerdesClockSquelOptionSet(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options)
    {
    uint32 address = cAf6Reg_GlbEth1GClkLinkDownSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    if (options & cAtClockExtractorSquelchingEthLf)
        linkDownBinVal = 1;

    mRegFieldSet(regVal, linkDownField_, linkDownBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);
    return cAtOk;
    }

static eAtClockExtractorSquelching Hw100MSerdesClockSquelOptionGet(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    eAtClockExtractorSquelching options = cAtClockExtractorSquelchingNone;
    uint32 address = cAf6Reg_GlbEth1GClkLinkDownSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    linkDownBinVal = mRegField(regVal, linkDownField_);
    if (linkDownBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLf;

    return options;
    }

static eAtModuleClockRet HwTenGeSerdesClockSquelOptionSet(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options)
    {
    uint32 address = cAf6Reg_GlbEth10GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 lfField_Mask, lfField_Shift, lfBinVal = 0;
    uint32 rfField_Mask, rfField_Shift, rfBinVal = 0;
    uint32 lossyncField_Mask, lossyncField_Shift, lossyncBinVal = 0;
    uint32 errField_Mask, errField_Shift, errBinVal = 0;
    uint8 tenGeLocalHwId = TenGeSerdesIdToLocalHwId(serdesId);

    lfField_Mask = cAf6_TenGeLocalFaultSchelEn_Mask(tenGeLocalHwId);
    lfField_Shift = cAf6_TenGeLocalFaultSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthLf)
        lfBinVal = 1;

    rfField_Mask = cAf6_TenGeRemoteFaultSchelEn_Mask(tenGeLocalHwId);
    rfField_Shift = cAf6_TenGeRemoteFaultSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthRf)
        rfBinVal = 1;

    lossyncField_Mask = cAf6_TenGeLossDataSyncSchelEn_Mask(tenGeLocalHwId);
    lossyncField_Shift = cAf6_TenGeLossDataSyncSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthLossync)
        lossyncBinVal = 1;

    errField_Mask = cAf6_TenGeExcessErrRateSchelEn_Mask(tenGeLocalHwId);
    errField_Shift = cAf6_TenGeExcessErrRateSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthEer)
        errBinVal = 1;

    mRegFieldSet(regVal, lfField_, lfBinVal);
    mRegFieldSet(regVal, rfField_, rfBinVal);
    mRegFieldSet(regVal, lossyncField_, lossyncBinVal);
    mRegFieldSet(regVal, errField_, errBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);
    return cAtOk;
    }

static eAtClockExtractorSquelching HwTenGeSerdesClockSquelOptionGet(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    eAtClockExtractorSquelching options = cAtClockExtractorSquelchingNone;
    uint32 address = cAf6Reg_GlbEth10GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 lfField_Mask, lfField_Shift, lfBinVal = 0;
    uint32 rfField_Mask, rfField_Shift, rfBinVal = 0;
    uint32 lossyncField_Mask, lossyncField_Shift, lossyncBinVal = 0;
    uint32 errField_Mask, errField_Shift, errBinVal = 0;
    uint8 tenGeLocalHwId = TenGeSerdesIdToLocalHwId(serdesId);

    lfField_Mask = cAf6_TenGeLocalFaultSchelEn_Mask(tenGeLocalHwId);
    lfField_Shift = cAf6_TenGeLocalFaultSchelEn_Shift(tenGeLocalHwId);
    lfBinVal = mRegField(regVal, lfField_);
    if (lfBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLf;

    rfField_Mask = cAf6_TenGeRemoteFaultSchelEn_Mask(tenGeLocalHwId);
    rfField_Shift = cAf6_TenGeRemoteFaultSchelEn_Shift(tenGeLocalHwId);
    rfBinVal = mRegField(regVal, rfField_);
    if (rfBinVal == 1)
        options |= cAtClockExtractorSquelchingEthRf;

    lossyncField_Mask = cAf6_TenGeLossDataSyncSchelEn_Mask(tenGeLocalHwId);
    lossyncField_Shift = cAf6_TenGeLossDataSyncSchelEn_Shift(tenGeLocalHwId);
    lossyncBinVal = mRegField(regVal, lossyncField_);
    if (lossyncBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLossync;

    errField_Mask = cAf6_TenGeExcessErrRateSchelEn_Mask(tenGeLocalHwId);
    errField_Shift = cAf6_TenGeExcessErrRateSchelEn_Shift(tenGeLocalHwId);
    errBinVal = mRegField(regVal, errField_);
    if (errBinVal == 1)
        options |= cAtClockExtractorSquelchingEthEer;

    return options;
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, m_ThaClockExtractorMethods, sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, HwSerdesClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideTha60290021ClockExtractor(AtClockExtractor self)
    {
    Tha60290021ClockExtractor extractor = (Tha60290021ClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ClockExtractorOverride, m_Tha60290021ClockExtractorMethods, sizeof(m_Tha60290021ClockExtractorOverride));

        mMethodOverride(m_Tha60290021ClockExtractorOverride, HwClockExtractShouldUseDiag);
        }

    mMethodsSet(extractor, &m_Tha60290021ClockExtractorOverride);
    }

static void OverrideTha60290022ClockExtractor(AtClockExtractor self)
    {
    Tha60290022ClockExtractor extractor = (Tha60290022ClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ClockExtractorOverride, m_Tha60290022ClockExtractorMethods, sizeof(m_Tha60290022ClockExtractorOverride));

        mMethodOverride(m_Tha60290022ClockExtractorOverride, IsValidTenGeSerdesId);
        mMethodOverride(m_Tha60290022ClockExtractorOverride, Hw1GeSerdesClockSquelOptionSet);
        mMethodOverride(m_Tha60290022ClockExtractorOverride, Hw1GeSerdesClockSquelOptionGet);
        mMethodOverride(m_Tha60290022ClockExtractorOverride, Hw100MSerdesClockSquelOptionSet);
        mMethodOverride(m_Tha60290022ClockExtractorOverride, Hw100MSerdesClockSquelOptionGet);
        mMethodOverride(m_Tha60290022ClockExtractorOverride, HwTenGeSerdesClockSquelOptionSet);
        mMethodOverride(m_Tha60290022ClockExtractorOverride, HwTenGeSerdesClockSquelOptionGet);
        }

    mMethodsSet(extractor, &m_Tha60290022ClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideThaClockExtractor(self);
    OverrideTha60290021ClockExtractor(self);
    OverrideTha60290022ClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051ClockExtractor);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ClockExtractorV2ObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60290051ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

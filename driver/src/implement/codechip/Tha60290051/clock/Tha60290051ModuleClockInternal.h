/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLOCK
 * 
 * File        : Tha60290051ModuleClockInternal.h
 * 
 * Created Date: Sep 7, 2018
 *
 * Description : Internal data of the 60290051 module clock
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051MODULECLOCKINTERNAL_H_
#define _THA60290051MODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/clock/Tha60290022ModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051ModuleClock
    {
    tTha60290022ModuleClock super;
    }tTha60290051ModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290051MODULECLOCKINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290051ModuleEthInternal.h
 * 
 * Created Date: Aug 20, 2018
 *
 * Description : 60290051 module ETH internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051MODULEETHINTERNAL_H_
#define _THA60290051MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/eth/Tha60290022ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051ModuleEth
    {
    tTha60290022ModuleEth super;
    }tTha60290051ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290051MODULEETHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290051SerdesBackplaneEthPortInternal.h
 * 
 * Created Date: Dec 19, 2018
 *
 * Description : Internal data of the serdes backplane eth port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051SERDESBACKPLANEETHPORTINTERNAL_H_
#define _THA60290051SERDESBACKPLANEETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/eth/Tha60290022SerdesBackplaneEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051SerdesBackplaneEthPort
    {
    tTha60290022SerdesBackplaneEthPort super;
    }tTha60290051SerdesBackplaneEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290051SerdesBackplaneEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051SERDESBACKPLANEETHPORTINTERNAL_H_ */


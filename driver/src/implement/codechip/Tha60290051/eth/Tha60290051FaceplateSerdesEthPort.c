/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290051FaceplateSerdesEthPort.c
 *
 * Created Date: Sep 16, 2018
 *
 * Description : 60290051 faceplate serdes eth port implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60290051FaceplateSerdesEthPortInternal.h"
#include "Tha60290051ETHPASSReg.h"
#include "Tha60290051ModuleEth.h"
#include "../prbs/Tha60290051FaceplateEthPortPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290051FaceplateSerdesEthPort)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;
static tTha60290021FaceplateSerdesEthPortMethods m_Tha60290021FaceplateSerdesEthPortOverride;

/* Cache super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;
static const tTha60290021FaceplateSerdesEthPortMethods *m_Tha60290021FaceplateSerdesEthPortMethod = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Af6Reg_ramrxepacfg1_Base(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ramrxepacfg1_Base;
    }

static uint32 Af6Reg_upen_cfginsvlan_Base(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfginsvlan_Base;
    }

static uint32 Af6Reg_ramrxepacfg0_Base(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ramrxepacfg0_Base;
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (!mThis(self)->prbs)
        mThis(self)->prbs = Tha60290051FaceplateEthPortPrbsEngineNew((AtEthPort)self);

    return mThis(self)->prbs;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbs)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;
    /*TODO: TBD*/
    return ret;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    eAtEthPortSpeed oldSpeed = AtEthPortSpeedGet(self);
    eAtRet ret = m_AtEthPortMethods->SpeedSet(self, speed);
    if (ret != cAtOk)
        return ret;
    if (oldSpeed != speed)
        {
        AtPrbsEngine prbs = PrbsEngineGet((AtChannel)self);
        ret = AtPrbsEngineInit(prbs);
        }

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290051FaceplateSerdesEthPort object = (Tha60290051FaceplateSerdesEthPort)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(prbs);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->prbs);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride,
                                  m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride,
                                  m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride,
                                  m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290021FaceplateSerdesEthPort(AtEthPort self)
    {
    Tha60290021FaceplateSerdesEthPort port = (Tha60290021FaceplateSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021FaceplateSerdesEthPortMethod = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FaceplateSerdesEthPortOverride,
                                  m_Tha60290021FaceplateSerdesEthPortMethod, sizeof(m_Tha60290021FaceplateSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, Af6Reg_ramrxepacfg1_Base);
        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, Af6Reg_upen_cfginsvlan_Base);
        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, Af6Reg_ramrxepacfg0_Base);
        }

    mMethodsSet(port, &m_Tha60290021FaceplateSerdesEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290021FaceplateSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051FaceplateSerdesEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesEthPortV3ObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290051FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

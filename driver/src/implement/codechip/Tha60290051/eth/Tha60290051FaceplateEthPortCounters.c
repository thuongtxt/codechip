/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290051FaceplateEthPortCounters.c
 *
 * Created Date: Mar 22, 2019
 *
 * Description : 60290051 Faceplate Ethernet port counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290022/eth/Tha60290022FaceplateEthPortCountersInternal.h"
#include "Tha60290051ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051FaceplateEthPortCounters
    {
    tTha60290022FaceplateEthPortCounters super;
    }tTha60290051FaceplateEthPortCounters;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021EthPortCountersMethods m_Tha60290021EthPortCountersOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 CounterOffsetFactor(Tha60290021EthPortCounters self)
    {
    AtUnused(self);
    return 32;
    }

static uint32 UpperCounterGet(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal)
    {
    AtUnused(self);
    AtUnused(numBits);
    return longRegVal[1];
    }

static uint32 LowerCounterGet(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal)
    {
    AtUnused(self);
    AtUnused(numBits);
    return longRegVal[0];
    }

static void OverrideTha60290021EthPortCounters(Tha60290021EthPortCounters self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021EthPortCountersOverride,
                                  mMethodsGet(self), sizeof(m_Tha60290021EthPortCountersOverride));

        mMethodOverride(m_Tha60290021EthPortCountersOverride, CounterOffsetFactor);
        mMethodOverride(m_Tha60290021EthPortCountersOverride, UpperCounterGet);
        mMethodOverride(m_Tha60290021EthPortCountersOverride, LowerCounterGet);
        }

    mMethodsSet(self, &m_Tha60290021EthPortCountersOverride);
    }

static void Override(Tha60290021EthPortCounters self)
    {
    OverrideTha60290021EthPortCounters(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051FaceplateEthPortCounters);
    }

static Tha60290021EthPortCounters ObjectInit(Tha60290021EthPortCounters self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateEthPortCountersObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60290021EthPortCounters Tha60290051FaceplateEthPortCountersNew(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290021EthPortCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCounters == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCounters, port);
    }


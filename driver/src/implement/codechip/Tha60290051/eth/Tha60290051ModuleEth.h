/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290051ModuleEth.h
 * 
 * Created Date: Aug 21, 2018
 *
 * Description : Tha60290051 module eth interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051MODULEETH_H_
#define _THA60290051MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "../../Tha60290021/eth/Tha60290021EthPortCounters.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290051Int40GEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290051FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290051SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module);

Tha60290021EthPortCounters Tha60290051FaceplateEthPortCountersNew(AtEthPort port);

/* Debugging */
eAtRet Tha60290051ModuleEthCounterTickModeSet(AtModuleEth self, eTha602900xxEthPortCounterTickMode mode);
eTha602900xxEthPortCounterTickMode Tha60290051ModuleEthCounterTickModeGet(AtModuleEth self);
eAtRet Tha60290051ModuleEthAllCountersLatchAndClear(AtModuleEth self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051MODULEETH_H_ */


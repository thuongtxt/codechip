/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290051ModuleEth.c
 *
 * Created Date: Aug 20, 2018
 *
 * Description : 60290051 module ETh implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290051ModuleEthInternal.h"
#include "../../Tha60290021/man/Tha60290021Device.h"
#include "Tha60290051ModuleEth.h"
#include "Tha60290051ETHPASSReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cMDIO1GBaseAddress                  0xF56000

#define cThaRegBackplaneIntTickControl   0xF0004C
#define cThaRegBackplaneIntTickPageMask  cBit1
#define cThaRegBackplaneIntTickPageShift 1
#define cThaRegBackplaneIntTickEnMask    cBit0
#define cThaRegBackplaneIntTickEnShift   0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021ModuleEthMethods         m_Tha60290021ModuleEthOverride;
static tThaModuleEthMethods                 m_ThaModuleEthOverride;
static tAtModuleEthMethods                 m_AtModuleEthOverride;

/* Save super implementation */
static const tTha60290021ModuleEthMethods  *m_Tha60290021ModuleEthMethods = NULL;
static const tThaModuleEthMethods  *m_ThaModuleEthMethods = NULL;
static const tAtModuleEthMethods  *m_AtModuleEthMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static eBool IsSgmiiPort(Tha60290021ModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtFalse;
    }

static eBool IsBackplaneMac(Tha60290021ModuleEth self, uint8 portId)
    {
    Tha60290021Device device = (Tha60290021Device)AtModuleDeviceGet((AtModule)self);
    uint32 startId = Tha60290021DeviceNumFaceplateSerdes(device) + 1;
    uint32 stopId  = startId + Tha60290021DeviceNumBackplaneSerdes(device) - 1;
    return ((portId >= startId) && (portId <= stopId));
    }

static AtEthPort PwPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290051Int40GEthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort KByteDccPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return NULL;
    }

static uint32 MultirateBaseAddress(Tha60290021ModuleEth self, uint8 sixteenPortGroup)
    {
    AtUnused(self);
    if (sixteenPortGroup < 1)
        return 0x0030000;
    return 0x0050000;
    }

static uint8 NumSixteenPortGroup(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 NumberOfFaceplateGroupsGet(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 MDIO1GBaseAddress(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cMDIO1GBaseAddress;
    }

static AtEthPort FaceplatePortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290051FaceplateSerdesEthPortNew(portId, (AtModuleEth)self);
    }

static uint32 Af6Reg_upen_enbflwctrl_Base(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cAf6Reg_upen_enbflwctrl_Base;
    }

static eBool PortHasSourceMac(ThaModuleEth self, uint8 portId)
    {
    if (portId == 0)
        return cAtFalse;

    return m_ThaModuleEthMethods->PortHasSourceMac(self, portId);
    }

static eBool PortCanBeUsed(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtTrue;
    }

static AtEthPort SerdesBackplaneEthPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290051SerdesBackplaneEthPortNew(portId, (AtModuleEth)self);
    }

static Tha60290021EthPortCounters FaceplatePortCountersCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    return Tha60290051FaceplateEthPortCountersNew(port);
    }

static AtEthFlowControl FaceplatePortFlowControlCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    return Tha60290051EthPortFlowControlNew((AtModule)self, port);
    }

static void OverrideTha60290021ModuleEth(AtModuleEth self)
    {
    Tha60290021ModuleEth module = (Tha60290021ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleEthOverride, m_Tha60290021ModuleEthMethods, sizeof(m_Tha60290021ModuleEthOverride));

        mMethodOverride(m_Tha60290021ModuleEthOverride, IsSgmiiPort);
        mMethodOverride(m_Tha60290021ModuleEthOverride, IsBackplaneMac);
        mMethodOverride(m_Tha60290021ModuleEthOverride, PwPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, KByteDccPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, MultirateBaseAddress);
        mMethodOverride(m_Tha60290021ModuleEthOverride, NumSixteenPortGroup);
        mMethodOverride(m_Tha60290021ModuleEthOverride, NumberOfFaceplateGroupsGet);
        mMethodOverride(m_Tha60290021ModuleEthOverride, MDIO1GBaseAddress);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, Af6Reg_upen_enbflwctrl_Base);
        mMethodOverride(m_Tha60290021ModuleEthOverride, SerdesBackplaneEthPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortCountersCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortFlowControlCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth module = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, m_ThaModuleEthMethods, sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortHasSourceMac);
        }

    mMethodsSet(module, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCanBeUsed);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideTha60290021ModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideAtModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60290051ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60290051ModuleEthCounterTickModeSet(AtModuleEth self, eTha602900xxEthPortCounterTickMode mode)
    {
    uint32 regAddr, regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    regAddr = cThaRegBackplaneIntTickControl;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cThaRegBackplaneIntTickEn, (mode == cTha602900xxEthPortCounterTickModeManual) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

eTha602900xxEthPortCounterTickMode Tha60290051ModuleEthCounterTickModeGet(AtModuleEth self)
    {
    uint32 regAddr, regVal;

    if (self == NULL)
        return cTha602900xxEthPortCounterTickModeUnknown;

    regAddr = cThaRegBackplaneIntTickControl;
    regVal = mModuleHwRead(self, regAddr);
    if (mRegField(regVal, cThaRegBackplaneIntTickEn))
        return cTha602900xxEthPortCounterTickModeManual;

    return cTha602900xxEthPortCounterTickModeAuto;
    }

eAtRet Tha60290051ModuleEthAllCountersLatchAndClear(AtModuleEth self)
    {
    uint32 regAddr, regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    regAddr = cThaRegBackplaneIntTickControl;
    regVal = mModuleHwRead(self, regAddr);

    /* Make trigger to latch counters */
    mRegFieldSet(regVal, cThaRegBackplaneIntTickPage, 0);
    mModuleHwWrite(self, regAddr, regVal);
    mRegFieldSet(regVal, cThaRegBackplaneIntTickPage, 1);
    mModuleHwWrite(self, regAddr, regVal);

    /* Clear */
    mRegFieldSet(regVal, cThaRegBackplaneIntTickPage, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }


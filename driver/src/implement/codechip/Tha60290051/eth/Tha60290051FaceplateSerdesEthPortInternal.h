/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290051FaceplateSerdesEthPortInternal.h
 * 
 * Created Date: Sep 16, 2018
 *
 * Description : 60290051 faceplate serdes eth port internal datas
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051FACEPLATESERDESETHPORTINTERNAL_H_
#define _THA60290051FACEPLATESERDESETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/eth/Tha60290022FaceplateSerdesEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051FaceplateSerdesEthPort *Tha60290051FaceplateSerdesEthPort;

typedef struct tTha60290051FaceplateSerdesEthPort
    {
    tTha60290022FaceplateSerdesEthPortV3 super;
    AtPrbsEngine prbs;
    }tTha60290051FaceplateSerdesEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290051FACEPLATESERDESETHPORTINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0051_RD_ETHPASS_H_
#define _AF6_REG_AF6CNC0051_RD_ETHPASS_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED PER GROUP 2 PORT
Reg Addr   : 0x00 + ($PID)/2
Reg Formula: 
    Where  : 
           + $PID(0-31): PORT ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan_Base                                                                       0x00 +

/*--------------------------------------
BitField Name: prior_odd_port
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan_prior_odd_port_Mask                                                        cBit31_29
#define cAf6_upen_cfgvlan_prior_odd_port_Shift                                                              29

/*--------------------------------------
BitField Name: cfi_odd_port
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan_cfi_odd_port_Mask                                                             cBit28
#define cAf6_upen_cfgvlan_cfi_odd_port_Shift                                                                28

/*--------------------------------------
BitField Name: vlanID_odd_port
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan_vlanID_odd_port_Mask                                                       cBit27_16
#define cAf6_upen_cfgvlan_vlanID_odd_port_Shift                                                             16

/*--------------------------------------
BitField Name: prior_even_port
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan_prior_even_port_Mask                                                       cBit15_13
#define cAf6_upen_cfgvlan_prior_even_port_Shift                                                             13

/*--------------------------------------
BitField Name: cfi_even_port
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan_cfi_even_port_Mask                                                            cBit12
#define cAf6_upen_cfgvlan_cfi_even_port_Shift                                                               12

/*--------------------------------------
BitField Name: vlanID_even_port
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan_vlanID_even_port_Mask                                                       cBit11_0
#define cAf6_upen_cfgvlan_vlanID_even_port_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE REMOVE VLAN TPID
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbtpid_Base                                                                         0x10

/*--------------------------------------
BitField Name: out_cfg_tpid
BitField Type: R/W
BitField Desc: bit corresponding port to config ( bit 0 is port 0),
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enbtpid_out_cfg_tpid_Mask                                                           cBit31_0
#define cAf6_upen_enbtpid_out_cfg_tpid_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE REMOVE VLAN TPID
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrmtpid_Base                                                                       0x11

/*--------------------------------------
BitField Name: out_cfgtpid2
BitField Type: R/W
BitField Desc: value TPID 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfgrmtpid_out_cfgtpid2_Mask                                                        cBit31_16
#define cAf6_upen_cfgrmtpid_out_cfgtpid2_Shift                                                              16

/*--------------------------------------
BitField Name: out_cfgtpid1
BitField Type: R/W
BitField Desc: value TPID 1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfgrmtpid_out_cfgtpid1_Mask                                                         cBit15_0
#define cAf6_upen_cfgrmtpid_out_cfgtpid1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN INSERTED
Reg Addr   : 0x120-0x13F
Reg Formula: 0x120+ $PID
    Where  : 
           + $PID(0-31) : Port ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfginsvlan_Base                                                                     0x120

/*--------------------------------------
BitField Name: prior
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfginsvlan_prior_Mask                                                              cBit15_13
#define cAf6_upen_cfginsvlan_prior_Shift                                                                    13

/*--------------------------------------
BitField Name: cfi
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfginsvlan_cfi_Mask                                                                   cBit12
#define cAf6_upen_cfginsvlan_cfi_Shift                                                                      12

/*--------------------------------------
BitField Name: vlanID
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfginsvlan_vlanID_Mask                                                              cBit11_0
#define cAf6_upen_cfginsvlan_vlanID_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE INSERT VLAN TPID
Reg Addr   : 0x110
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbinstpid_Base                                                                     0x110

/*--------------------------------------
BitField Name: out_enbtpid
BitField Type: R/W
BitField Desc: bit corresponding port to config ( bit 0 is port 0),(0)  use
config TPID1, (1) use config TPID 2
BitField Bits: [32:0]
--------------------------------------*/
#define cAf6_upen_enbinstpid_out_enbtpid_01_Mask                                                      cBit31_0
#define cAf6_upen_enbinstpid_out_enbtpid_01_Shift                                                            0
#define cAf6_upen_enbinstpid_out_enbtpid_02_Mask                                                      cBit32_0
#define cAf6_upen_enbinstpid_out_enbtpid_02_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE INSERT VLAN TPID
Reg Addr   : 0x111
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfginstpid_Base                                                                     0x111

/*--------------------------------------
BitField Name: out_cfgtpid2
BitField Type: R/W
BitField Desc: valud TPID 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfginstpid_out_cfgtpid2_Mask                                                       cBit31_16
#define cAf6_upen_cfginstpid_out_cfgtpid2_Shift                                                             16

/*--------------------------------------
BitField Name: out_cfgtpid1
BitField Type: R/W
BitField Desc: value TPID 1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfginstpid_out_cfgtpid1_Mask                                                        cBit15_0
#define cAf6_upen_cfginstpid_out_cfgtpid1_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE INSERT VLAN TPID
Reg Addr   : 0x112
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbins_Base                                                                         0x112

/*--------------------------------------
BitField Name: out_enbins
BitField Type: R/W
BitField Desc: bit corresponding port to config ( bit 0 is port 0), (0) is
disable, (1) is enable
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_enbins_out_enbins_Mask                                                              cBit31_0
#define cAf6_upen_enbins_out_enbins_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG THRESHOLD
Reg Addr   : 0x200-0x21F
Reg Formula: 0x200+ $PID
    Where  : 
           + $PID(0-31) : Port ID
Reg Desc   : 
Theshold in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgthreshold_Base                                                                   0x200
#define cAf6Reg_upen_cfgthreshold_WidthVal                                                                  32

/*--------------------------------------
BitField Name: hungry_thres
BitField Type: R/W
BitField Desc: report HUNGRY if below this threshold and over starving_thres,
report SATISFIED of over this threshold, default 3/4 of max buffer
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cfgthreshold_hungry_thres_Mask                                                      cBit15_8
#define cAf6_upen_cfgthreshold_hungry_thres_Shift                                                            8

/*--------------------------------------
BitField Name: starving_thres
BitField Type: R/W
BitField Desc: report STARVING if below this threshold, default 1/4 of max
buffer
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfgthreshold_starving_thres_Mask                                                     cBit7_0
#define cAf6_upen_cfgthreshold_starving_thres_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE FLOW CONTROL
Reg Addr   : 0x220
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbflwctrl_Base                                                                     0x220
#define cAf6Reg_upen_enbflwctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: out_enbflw
BitField Type: R/W
BitField Desc: (0) is disable, (1) is enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_enbflwctrl_out_enbflw_Mask                                                             cBit0
#define cAf6_upen_enbflwctrl_out_enbflw_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG THRESHOLD
Reg Addr   : 0x840-0x85F
Reg Formula: 0x840+ $PID
    Where  : 
           + $PID(0-31) : Port ID
Reg Desc   : 
Currest status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_stathreshold_Base                                                               0x840

/*--------------------------------------
BitField Name: BufferLengthCur
BitField Type: R/W
BitField Desc: Current buffer length in 1-kbyte unit
BitField Bits: [11:01]
--------------------------------------*/
#define cAf6_upen_stathreshold_BufferLengthCur_Mask                                                   cBit11_1
#define cAf6_upen_stathreshold_BufferLengthCur_Shift                                                         1


/*------------------------------------------------------------------------------
Reg Name   : CONFIG THRESHOLD
Reg Addr   : 0x860-0x87F
Reg Formula: 0x860+ $PID
    Where  : 
           + $PID(0-31) : Port ID
Reg Desc   : 
Currest status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stathreshold_Base                                                                   0x860
#define cAf6Reg_upen_stathreshold_WidthVal                                                                  32

/*--------------------------------------
BitField Name: BufferLength
BitField Type: R/W
BitField Desc: Current buffer length in 1-kbyte unit
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_stathreshold_BufferLength_Mask                                                      cBit10_0
#define cAf6_upen_stathreshold_BufferLength_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : STATUS MAX MIN BUFFER
Reg Addr   : 0x880-0x89F
Reg Formula: 0x880+ $PID
    Where  : 
           + $PID(0-31) : Port ID
Reg Desc   : 
Watermakr max min status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stabufwater_Base                                                                    0x880
#define cAf6Reg_upen_stabufwater_WidthVal                                                                   32

/*--------------------------------------
BitField Name: MaxBufferLength
BitField Type: R/W
BitField Desc: Max buffer length in 1-kbyte unit
BitField Bits: [21:11]
--------------------------------------*/
#define cAf6_upen_stabufwater_MaxBufferLength_Mask                                                   cBit21_11
#define cAf6_upen_stabufwater_MaxBufferLength_Shift                                                         11

/*--------------------------------------
BitField Name: MinBufferLength
BitField Type: R/W
BitField Desc: Min buffer length in 1-kbyte unit
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_stabufwater_MinBufferLength_Mask                                                    cBit10_0
#define cAf6_upen_stabufwater_MinBufferLength_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Hold Register Status
Reg Addr   : 0x80
Reg Formula: 0x80 +  HID
    Where  : 
           + $HID(0-0): Hold ID
Reg Desc   : 
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_epa_hold_status_Base                                                                      0x80

/*--------------------------------------
BitField Name: EpaHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_epa_hold_status_EpaHoldStatus_Mask                                                       cBit31_0
#define cAf6_epa_hold_status_EpaHoldStatus_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group1 Counter
Reg Addr   : 0xA00 - 0xA3F(RO)
Reg Formula: 0xA00 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txepagrp1cnt_Base                                                                   0xA00

/*--------------------------------------
BitField Name: txethgrp1cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : txpkt255byte cntoffset = 1 : txpkt1023byte
cntoffset = 2 : txpktjumbo
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt1_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: txethgrp1cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : txpkt127byte cntoffset = 1 : txpkt511byte
cntoffset = 2 : txpkt1518byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt0_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group2 Counter
Reg Addr   : 0xA80 - 0xABF(RO)
Reg Formula: 0xA80 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txepagrp2cnt_Base                                                                   0xA80

/*--------------------------------------
BitField Name: txethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : txpktgood cntoffset = 1 : unused cntoffset = 2 :
txoversize
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt1_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: txethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : txbytecnt cntoffset = 1 : txpkttotaldis cntoffset
= 2 : txundersize
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt0_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group3 Counter
Reg Addr   : 0xB00 - 0xB3F(RC)
Reg Formula: 0xB00 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txepagrp3cnt_Base                                                                   0xB00

/*--------------------------------------
BitField Name: txethgrp1cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : txpktoverrun cntoffset = 1 : txpktmulticast
cntoffset = 2 : unused
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt1_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: txethgrp1cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : txpktpausefrm cntoffset = 1 : txpktbroadcast
cntoffset = 2 : txpkt64byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt0_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group1 Counter
Reg Addr   : 0xE00 - 0xE3F(RO)
Reg Formula: 0xE00 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp1cnt_Base                                                                   0xE00

/*--------------------------------------
BitField Name: rxethgrp1cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpkt255byte cntoffset = 1 : rxpkt1023byte
cntoffset = 2 : rxpktjumbo
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt1_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: rxethgrp1cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpkt127byte cntoffset = 1 : rxpkt511byte
cntoffset = 2 : rxpkt1518byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt0_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group2 Counter
Reg Addr   : 0xE80 - 0xEBF(RO)
Reg Formula: 0xE80 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp2cnt_Base                                                                   0xE80

/*--------------------------------------
BitField Name: rxethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpkttotal cntoffset = 1 : unused cntoffset = 2 :
rxpktdaloop
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt1_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: rxethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxbytecnt cntoffset = 1 : rxpktfcserr cntoffset =
2 : rxpkttotaldis
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt0_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group3 Counter
Reg Addr   : 0xF00 - 0xF3F(RO)
Reg Formula: 0xF00 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp3cnt_Base                                                                   0xF00

/*--------------------------------------
BitField Name: rxethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktjabber cntoffset = 1 : rxpktpausefrm
cntoffset = 2 : rxpktoversize
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt1_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: rxethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktpcserr cntoffset = 1 : rxpktfragment
cntoffset = 2 : rxpktundersize
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt0_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group4 Counter
Reg Addr   : 0xF80 - 0xFBF(RO)
Reg Formula: 0xF80 + 32*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-31)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp4cnt_Base                                                                   0xF80

/*--------------------------------------
BitField Name: rxethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktbroadcast cntoffset = 1 : rxpkt64byte
cntoffset = 2 : unused
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt1_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: rxethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktoverrun cntoffset = 1 : rxpktmulticast
cntoffset = 2 : unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt0_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through MAC Address Control
Reg Addr   : 0xC60 - 0xC7F #The address format for these registers is 0xC30 + ethpid
Reg Formula: 0xC60 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramrxepacfg0_Base                                                                        0xC60
#define cAf6Reg_ramrxepacfg0_WidthVal                                                                       64

/*--------------------------------------
BitField Name: EthPassMacAdd
BitField Type: RW
BitField Desc: Ethernet pass through MAC address used for Loop DA counter
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_ramrxepacfg0_EthPassMacAdd_01_Mask                                                       cBit31_0
#define cAf6_ramrxepacfg0_EthPassMacAdd_01_Shift                                                             0
#define cAf6_ramrxepacfg0_EthPassMacAdd_02_Mask                                                       cBit15_0
#define cAf6_ramrxepacfg0_EthPassMacAdd_02_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Port Enable and MTU Control
Reg Addr   : 0xC80 - 0xC9F #The address format for these registers is 0xC40 + ethpid
Reg Formula: 0xC80 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramrxepacfg1_Base                                                                        0xC80
#define cAf6Reg_ramrxepacfg1_WidthVal                                                                       32

/*--------------------------------------
BitField Name: EthPassPortEn
BitField Type: RW
BitField Desc: Ethernet pass port enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassPortEn_Mask                                                            cBit20
#define cAf6_ramrxepacfg1_EthPassPortEn_Shift                                                               20

/*--------------------------------------
BitField Name: EthPassPcsErrDropEn
BitField Type: RW
BitField Desc: Ethernet PCS error drop enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassPcsErrDropEn_Mask                                                      cBit19
#define cAf6_ramrxepacfg1_EthPassPcsErrDropEn_Shift                                                         19

/*--------------------------------------
BitField Name: EthPassFcsErrDropEn
BitField Type: RW
BitField Desc: Ethernet FCS error drop enable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassFcsErrDropEn_Mask                                                      cBit18
#define cAf6_ramrxepacfg1_EthPassFcsErrDropEn_Shift                                                         18

/*--------------------------------------
BitField Name: EthPassUndersizeDropEn
BitField Type: RW
BitField Desc: Ethernet Undersize drop enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassUndersizeDropEn_Mask                                                   cBit17
#define cAf6_ramrxepacfg1_EthPassUndersizeDropEn_Shift                                                      17

/*--------------------------------------
BitField Name: EthPassOversizeDropEn
BitField Type: RW
BitField Desc: Ethernet Oversize drop enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassOversizeDropEn_Mask                                                    cBit16
#define cAf6_ramrxepacfg1_EthPassOversizeDropEn_Shift                                                       16

/*--------------------------------------
BitField Name: EthPassPausefrmDropEn
BitField Type: RW
BitField Desc: Ethernet Pausefrm drop enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassPausefrmDropEn_Mask                                                    cBit15
#define cAf6_ramrxepacfg1_EthPassPausefrmDropEn_Shift                                                       15

/*--------------------------------------
BitField Name: EthPassDaloopDropEn
BitField Type: RW
BitField Desc: Ethernet Daloop drop enable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassDaloopDropEn_Mask                                                      cBit14
#define cAf6_ramrxepacfg1_EthPassDaloopDropEn_Shift                                                         14

/*--------------------------------------
BitField Name: EthPassMtuSize
BitField Type: RW
BitField Desc: Ethernet pass through MTU size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassMtuSize_Mask                                                         cBit13_0
#define cAf6_ramrxepacfg1_EthPassMtuSize_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Global Diagnostic Enable Control
Reg Addr   : 0x300
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_epadiagglb_Base                                                                     0x300
#define cAf6Reg_upen_epadiagglb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: DiagSelect_En
BitField Type: R/W
BitField Desc: Set 1 to select source data to tx Faceplate port from diagnostic
engine, set 0 for  normal
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_epadiagglb_DiagSelect_En_Mask                                                          cBit4
#define cAf6_upen_epadiagglb_DiagSelect_En_Shift                                                             4

/*--------------------------------------
BitField Name: DiagTenGPort24_En
BitField Type: R/W
BitField Desc: set 1 indicate Port 24 of group port 24 to 31 is 10G
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_epadiagglb_DiagTenGPort24_En_Mask                                                      cBit3
#define cAf6_upen_epadiagglb_DiagTenGPort24_En_Shift                                                         3

/*--------------------------------------
BitField Name: DiagTenGPort16_En
BitField Type: R/W
BitField Desc: set 1 indicate Port 16 of group port 16 to 23 is 10G
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_epadiagglb_DiagTenGPort16_En_Mask                                                      cBit2
#define cAf6_upen_epadiagglb_DiagTenGPort16_En_Shift                                                         2

/*--------------------------------------
BitField Name: DiagTenGPort08_En
BitField Type: R/W
BitField Desc: set 1 indicate Port 8 of group port 8 to 15 is 10G
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_epadiagglb_DiagTenGPort08_En_Mask                                                      cBit1
#define cAf6_upen_epadiagglb_DiagTenGPort08_En_Shift                                                         1

/*--------------------------------------
BitField Name: DiagTenGPort00_En
BitField Type: R/W
BitField Desc: set 1 indicate Port 0 of group port 0 to 7 is 10G
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_epadiagglb_DiagTenGPort00_En_Mask                                                      cBit0
#define cAf6_upen_epadiagglb_DiagTenGPort00_En_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Enable and Speed Control
Reg Addr   : 0x320 - 0x33F #The address format for these registers is 0x320 + ethpid
Reg Formula: 0x320 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag1_Base                                                                            0x320
#define cAf6Reg_ramdiag1_WidthVal                                                                           32

/*--------------------------------------
BitField Name: DiagEthNcoSpeed
BitField Type: RW
BitField Desc: NCO of Ethernet clock corresponding to system clock 194.4Mhz ETH
10 Mbps: value 25 coressponding to 100% bandwidth, smaller value will be at
lower bandwidth ETH 100 Mbps: value 125 coressponding to 100% bandwidth, smaller
value will be at lower bandwidth ETH 1000 Mbps: value 625 coressponding to 100%
bandwidth, smaller value will be at lower bandwidth ETH 10 Gbps: value 6250
coressponding to 100% bandwidth, smaller value will be at lower bandwidth
BitField Bits: [26:14]
--------------------------------------*/
#define cAf6_ramdiag1_DiagEthNcoSpeed_Mask                                                           cBit26_14
#define cAf6_ramdiag1_DiagEthNcoSpeed_Shift                                                                 14

/*--------------------------------------
BitField Name: DiagSysNcoSpeed
BitField Type: RW
BitField Desc: NCO of System clock 194.4Mhz corresponding to each ETH speed ETH
10 Mbps: value must be 3888 ETH 100 Mbps: value must be 1944 ETH 1000 Mbps:
value must be 972 ETH 10 Gbps: value must be 972
BitField Bits: [13:1]
--------------------------------------*/
#define cAf6_ramdiag1_DiagSysNcoSpeed_Mask                                                            cBit13_1
#define cAf6_ramdiag1_DiagSysNcoSpeed_Shift                                                                  1

/*--------------------------------------
BitField Name: DiagPkGen_En
BitField Type: RW
BitField Desc: Diagnostic packet generator enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramdiag1_DiagPkGen_En_Mask                                                                  cBit0
#define cAf6_ramdiag1_DiagPkGen_En_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Burst Control
Reg Addr   : 0x360 - 0x37F #The address format for these registers is 0x360 + ethpid
Reg Formula: 0x360 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag2_Base                                                                            0x360
#define cAf6Reg_ramdiag2_WidthVal                                                                           32

/*--------------------------------------
BitField Name: DiagBurstByte
BitField Type: RW
BitField Desc: Number of byte generated per burst, value 0 will represent no
burst
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_ramdiag2_DiagBurstByte_Mask                                                              cBit19_0
#define cAf6_ramdiag2_DiagBurstByte_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Packet Length Control
Reg Addr   : 0x3A0 - 0x3BF #The address format for these registers is 0x360 + ethpid
Reg Formula: 0x3A0 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag3_Base                                                                            0x3A0
#define cAf6Reg_ramdiag3_WidthVal                                                                           32

/*--------------------------------------
BitField Name: DiagGenOffLen
BitField Type: RW
BitField Desc: Offset packet length equal to Maximum packet lenth subtract
minimum packet length
BitField Bits: [27:14]
--------------------------------------*/
#define cAf6_ramdiag3_DiagGenOffLen_Mask                                                             cBit27_14
#define cAf6_ramdiag3_DiagGenOffLen_Shift                                                                   14

/*--------------------------------------
BitField Name: DiagGenMinLen
BitField Type: RW
BitField Desc: Minimum packet length
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ramdiag3_DiagGenMinLen_Mask                                                              cBit13_0
#define cAf6_ramdiag3_DiagGenMinLen_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Generate Packet Mode Control
Reg Addr   : 0x3E0 - 0x3FF #The address format for these registers is 0x3E0 + ethpid
Reg Formula: 0x3E0 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag4_Base                                                                            0x3E0
#define cAf6Reg_ramdiag4_WidthVal                                                                           32

/*--------------------------------------
BitField Name: DiagGenNonPldByte
BitField Type: RW
BitField Desc: Total none payload byte in an eth frame include gap preamble sfd
fcs
BitField Bits: [17:12]
--------------------------------------*/
#define cAf6_ramdiag4_DiagGenNonPldByte_Mask                                                         cBit17_12
#define cAf6_ramdiag4_DiagGenNonPldByte_Shift                                                               12

/*--------------------------------------
BitField Name: DiagGenFixPat
BitField Type: RW
BitField Desc: Fix pattern value used in fix data mode
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_ramdiag4_DiagGenFixPat_Mask                                                              cBit11_4
#define cAf6_ramdiag4_DiagGenFixPat_Shift                                                                    4

/*--------------------------------------
BitField Name: DiagGenLenMode
BitField Type: RW
BitField Desc: Diagnostic generate length mode 0: fix length mode equal to
DiagGenMinLen 1: increase length mode 2: deccrease length mode 3: randome length
mode
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_ramdiag4_DiagGenLenMode_Mask                                                              cBit3_2
#define cAf6_ramdiag4_DiagGenLenMode_Shift                                                                   2

/*--------------------------------------
BitField Name: DiagGenDatMode
BitField Type: RW
BitField Desc: Diagnostic generate data mode 0: Sequential data mode others: fix
pattern mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ramdiag4_DiagGenDatMode_Mask                                                              cBit1_0
#define cAf6_ramdiag4_DiagGenDatMode_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Monitor Packet Mode Control
Reg Addr   : 0x400 - 0x41F #The address format for these registers is 0x400 + ethpid
Reg Formula: 0x400 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag5_Base                                                                            0x400
#define cAf6Reg_ramdiag5_WidthVal                                                                           32

/*--------------------------------------
BitField Name: DiagMonFixPat
BitField Type: RW
BitField Desc: Fix pattern value used in fix data mode
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_ramdiag5_DiagMonFixPat_Mask                                                              cBit11_4
#define cAf6_ramdiag5_DiagMonFixPat_Shift                                                                    4

/*--------------------------------------
BitField Name: DiagMonLenMode
BitField Type: RW
BitField Desc: Diagnostic monitoring length mode 0: fix length mode equal to
DiagMonMinLen 1: increase length mode 2: deccrease length mode 3: randome length
mode
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_ramdiag5_DiagMonLenMode_Mask                                                              cBit3_2
#define cAf6_ramdiag5_DiagMonLenMode_Shift                                                                   2

/*--------------------------------------
BitField Name: DiagMonDatMode
BitField Type: RW
BitField Desc: Diagnostic monitoring data mode 0: Sequential data mode others:
fix pattern mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ramdiag5_DiagMonDatMode_Mask                                                              cBit1_0
#define cAf6_ramdiag5_DiagMonDatMode_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Monitor Error Counter
Reg Addr   : 0x440 - 0x47F #The address format for these registers is 0x440 + ethpid
Reg Formula: 0x440 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register counter per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag6_Base                                                                            0x440

/*--------------------------------------
BitField Name: ErrorCounter
BitField Type: RW
BitField Desc: Error counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ramdiag6_ErrorCounter_Mask                                                               cBit31_0
#define cAf6_ramdiag6_ErrorCounter_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Monitor Packet Counter
Reg Addr   : 0x480 - 0x4BF #The address format for these registers is 0x480 + ethpid
Reg Formula: 0x480 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register counter per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag7_Base                                                                            0x480

/*--------------------------------------
BitField Name: PacketCounter
BitField Type: RW
BitField Desc: Packet counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ramdiag7_PacketCounter_Mask                                                              cBit31_0
#define cAf6_ramdiag7_PacketCounter_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Diagnostic Port Monitor Byte Counter
Reg Addr   : 0x4C0 - 0x4FF #The address format for these registers is 0x4C0 + ethpid
Reg Formula: 0x4C0 +  ethpid
    Where  : 
           + $ethpid(0-31): Ethernet port ID
Reg Desc   : 
This register counter per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdiag8_Base                                                                            0x4C0

/*--------------------------------------
BitField Name: ByteCounter
BitField Type: RW
BitField Desc: Byte counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ramdiag7_ByteCounter_Mask                                                                cBit31_0
#define cAf6_ramdiag7_ByteCounter_Shift                                                                      0

#endif /* _AF6_REG_AF6CNC0051_RD_ETHPASS_H_ */

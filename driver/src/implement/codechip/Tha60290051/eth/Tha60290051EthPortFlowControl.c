/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290051EthFlowControl.c
 *
 * Created Date: May 30, 2019
 *
 * Description : 60290051 ETH Flow Control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/eth/Tha60290021EthPortFlowControlInternal.h"
#include "../../Tha60290021/eth/Tha60290021ModuleEth.h"
#include "Tha60290051ETHPASSReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaEthFlowControlWaterMarkResetMax 0x0
#define cThaEthFlowControlWaterMarkResetMin cAf6_upen_stabufwater_MinBufferLength_Mask

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290051EthPortFlowControl *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051EthPortFlowControl
    {
    tTha60290021EthPortFlowControl super;
    }tTha60290051EthPortFlowControl;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthFlowControlMethods                 m_AtEthFlowControlOverride;
static tTha60290021EthPortFlowControlMethods    m_Tha60290021EthPortFlowControlOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 OffSet(AtEthFlowControl self)
    {
    AtChannel port = AtEthFlowControlChannelGet(self);
    AtModuleEth ethModule = (AtModuleEth) AtChannelModuleGet(port);
    return (uint32) Tha60290021ModuleEthFaceplateLocalId(ethModule, (uint8) AtChannelIdGet(port));
    }

static uint32 WaterMarkStatusReg(AtEthFlowControl self)
    {
    return OffSet(self) + cAf6Reg_upen_stabufwater_Base + Tha60290021ModuleEthBypassBaseAddress();
    }

static uint32 HwTxFifoWaterMarkMinClear(AtEthFlowControl self, eBool r2c)
    {
    uint32 regAddr, regVal, ret;

    regAddr = WaterMarkStatusReg(self);
    regVal = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    ret = mRegField(regVal, cAf6_upen_stabufwater_MinBufferLength_);

    if (r2c)
        {
        mRegFieldSet(regVal, cAf6_upen_stabufwater_MinBufferLength_, cThaEthFlowControlWaterMarkResetMin);
        AtEthFlowControlWrite(self, regAddr, regVal, cAtModuleEth);
        }

    return ret;
    }

static uint32 TxFifoWaterMarkMinGet(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMinClear(self, cAtFalse);
    }

static uint32 TxFifoWaterMarkMinClear(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMinClear(self, cAtTrue);
    }

static uint32 HwTxFifoWaterMarkMaxGet(AtEthFlowControl self, eBool r2c)
    {
    uint32 regAddr, regVal, ret;

    regAddr = WaterMarkStatusReg(self);
    regVal = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    ret = mRegField(regVal, cAf6_upen_stabufwater_MaxBufferLength_);

    if (r2c)
        {
        mRegFieldSet(regVal, cAf6_upen_stabufwater_MaxBufferLength_, cThaEthFlowControlWaterMarkResetMax);
        AtEthFlowControlWrite(self, regAddr, regVal, cAtModuleEth);
        }

    return ret;
    }

static uint32 TxFifoWaterMarkMaxGet(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMaxGet(self, cAtFalse);
    }

static uint32 TxFifoWaterMarkMaxClear(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMaxGet(self, cAtTrue);
    }

static uint32 FifoBufferStatusReg(AtEthFlowControl self)
    {
    return OffSet(self) + cAf6Reg_upen_stathreshold_Base + Tha60290021ModuleEthBypassBaseAddress();
    }

static uint32 TxFifoLevelGet(AtEthFlowControl self)
    {
    uint32 regAddr = FifoBufferStatusReg(self);
    uint32 regVal = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_stathreshold_BufferLength_);
    }

static uint32 InternalBufferStatusReg(AtEthFlowControl self)
    {
    return OffSet(self) + cAf6Reg_upen_int_stathreshold_Base + Tha60290021ModuleEthBypassBaseAddress();
    }

static uint32 TxBufferLevelGet(Tha60290021EthPortFlowControl self)
    {
    AtEthFlowControl flowControl = (AtEthFlowControl)self;
    uint32 regAddr = InternalBufferStatusReg(flowControl);
    uint32 regVal = AtEthFlowControlRead(flowControl, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_stathreshold_BufferLengthCur_);
    }

static void OverrideAtEthFlowControl(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowControlOverride, mMethodsGet(self), sizeof(m_AtEthFlowControlOverride));

        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMaxGet);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMaxClear);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMinGet);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMinClear);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoLevelGet);
        }

    mMethodsSet(self, &m_AtEthFlowControlOverride);
    }

static void OverrideTha60290021EthPortFlowControl(AtEthFlowControl self)
    {
    Tha60290021EthPortFlowControl object = (Tha60290021EthPortFlowControl)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021EthPortFlowControlOverride, mMethodsGet(object), sizeof(m_Tha60290021EthPortFlowControlOverride));

        mMethodOverride(m_Tha60290021EthPortFlowControlOverride, TxBufferLevelGet);
        }

    mMethodsSet(object, &m_Tha60290021EthPortFlowControlOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtEthFlowControl(self);
    OverrideTha60290021EthPortFlowControl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051EthPortFlowControl);
    }

static AtEthFlowControl ObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021EthPortFlowControlObjectInit(self, module, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlowControl Tha60290051EthPortFlowControlNew(AtModule module, AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlowControl newFlowCtrl = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlowCtrl == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlowCtrl, module, port);
    }

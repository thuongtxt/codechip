/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : Tha60290051BackplanePtpPort.c
 *
 * Created Date: Jan 31, 2019
 *
 * Description : 40G passthrough PTP backplane port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ptp/ThaModulePtp.h"
#include "../../../default/ptp/ThaPtpPortInternal.h"
#include "Tha60290051ModulePtpReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051BackplanePtpPort
    {
    tThaPtpBackplanePort super;
    }tTha60290051BackplanePtpPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPtpPortMethods   m_ThaPtpPortOverride;

/* Save super implementations */
static const tThaPtpPortMethods   *m_ThaPtpPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePtp PtpModule(AtPtpPort self)
    {
    return (AtModulePtp)AtChannelModuleGet((AtChannel)self);
    }

static uint32 BaseAddress(AtPtpPort self)
    {
    return ThaModulePtpBaseAddress((ThaModulePtp)PtpModule(self));
    }

static uint32 CounterRegister(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPtpPortCounterTypeTxPackets:
            return (r2c) ? cAf6Reg_upen_cnt_txpkt_out_r2c : cAf6Reg_upen_cnt_txpkt_out_ro;
        case cAtPtpPortCounterTypeTxSyncPackets:
            return (r2c) ? cAf6Reg_upen_tx_sync_cnt40_r2c_Base : cAf6Reg_upen_tx_sync_cnt40_ro_Base;
        case cAtPtpPortCounterTypeTxFollowUpPackets:
            return (r2c) ? cAf6Reg_upen_tx_folu_cnt40_r2c_Base : cAf6Reg_upen_tx_folu_cnt40_ro_Base;
        case cAtPtpPortCounterTypeTxDelayReqPackets:
            return (r2c) ? cAf6Reg_upen_tx_dreq_cnt40_r2c_Base : cAf6Reg_upen_tx_dreq_cnt40_ro_Base;
        case cAtPtpPortCounterTypeTxDelayRespPackets:
            return (r2c) ? cAf6Reg_upen_tx_dres_cnt40_r2c_Base : cAf6Reg_upen_tx_dres_cnt40_ro_Base;

        case cAtPtpPortCounterTypeRxPackets:
            return (r2c) ? cAf6Reg_upen_cnt_ptppkt_rx_r2c : cAf6Reg_upen_cnt_ptppkt_rx_ro;
        case cAtPtpPortCounterTypeRxSyncPackets:
            return (r2c) ? cAf6Reg_upen_rx_sync_cnt40_r2c_Base : cAf6Reg_upen_rx_sync_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxFollowUpPackets:
            return (r2c) ? cAf6Reg_upen_rx_folu_cnt40_r2c_Base : cAf6Reg_upen_rx_folu_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxDelayReqPackets:
            return (r2c) ? cAf6Reg_upen_rx_dreq_cnt40_r2c_Base : cAf6Reg_upen_rx_dreq_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxDelayRespPackets:
            return (r2c) ? cAf6Reg_upen_rx_dres_cnt40_r2c_Base : cAf6Reg_upen_rx_dres_cnt40_ro_Base;
        case cAtPtpPortCounterTypeRxErroredPackets:
            return cInvalidUint32;

        default:
            return cInvalidUint32;
        }
    }

static uint32 HwCounterRead2Clear(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    uint32 baseAddress = BaseAddress((AtPtpPort)self);
    uint32 regAddr = CounterRegister(self, counterType, r2c);

    if (regAddr == cInvalidUint32)
        return 0;

    return mChannelHwRead(self, baseAddress + regAddr, cAtModulePtp);
    }

static eBool ShouldOpenFeatureFromVersion(AtChannel self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static eBool HasNewCounter(AtChannel self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x2, 0x0000);
    }

static uint32 CounterRead2Clear(ThaPtpPort self, uint16 counterType, eBool r2c)
    {
    if (HasNewCounter((AtChannel)self))
        return HwCounterRead2Clear(self, counterType, r2c);

    return m_ThaPtpPortMethods->CounterRead2Clear(self, counterType, r2c);
    }

static void OverrideThaPtpPort(AtPtpPort self)
    {
    ThaPtpPort port = (ThaPtpPort)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPtpPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPtpPortOverride, mMethodsGet(port), sizeof(m_ThaPtpPortOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPtpPortOverride, CounterRead2Clear);
        }

    mMethodsSet(port, &m_ThaPtpPortOverride);
    }

static void Override(AtPtpPort self)
    {
    OverrideThaPtpPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPtpBackplanePort);
    }

static AtPtpPort ObjectInit(AtPtpPort self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPtpBackplanePortObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpPort Tha60290051BackplanePtpPortNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    return ObjectInit(newPort, channelId, module);
    }


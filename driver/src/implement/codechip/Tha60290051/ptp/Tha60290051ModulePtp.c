/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : Tha60290051ModulePtp.c
 *
 * Created Date: Nov 19, 2018
 *
 * Description : Implement the 60290051 module PTP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtpPsnGroup.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/ptp/ThaPtpPort.h"
#include "Tha60290051ModulePtp.h"
#include "Tha60290051ModulePtpInternal.h"
#include "Tha60290051ModulePtpReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290051ModulePtp)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtModuleMethods         m_AtModuleOverride;
static tAtModulePtpMethods      m_AtModulePtpOverride;
static tThaModulePtpMethods     m_ThaModulePtpOverride;

/* Save super implementations */
static const tAtObjectMethods       *m_AtObjectMethods  = NULL;
static const tAtModuleMethods       *m_AtModuleMethods  = NULL;
static const tAtModulePtpMethods    *m_AtModulePtpMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumPtpPorts(AtModulePtp self)
    {
    AtUnused(self);
    return 33;
    }

static uint32 NumL2PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 NumIpV4PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 NumIpV6PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 NumParts(ThaModulePtp self)
    {
    AtUnused(self);
    return 2;
    }

static eBool IsBackplanePortId(uint32 portId)
    {
    return portId ? cAtFalse : cAtTrue;
    }

static eBool PortCanJoinPsnGroup(ThaModulePtp self, AtPtpPort port, AtPtpPsnGroup group)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    AtUnused(self);

    if (IsBackplanePortId(portId))
        return cAtFalse;

    /* Faceplate 1-16 can join group 0-1 */
    if (portId < 17)
        return (AtPtpPsnGroupIdGet(group) < 2) ? cAtTrue : cAtFalse;

    /* Faceplate 17-33 can join group 2-3 */
    if (portId < 33)
        return (AtPtpPsnGroupIdGet(group) < 2) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static uint32 PortLocalId(ThaModulePtp self, AtPtpPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    AtUnused(self);

    if (IsBackplanePortId(portId))
        return 0;

    return (portId - 1U) % 16U;
    }

static uint32 BaseAddress(ThaModulePtp self)
    {
    AtUnused(self);
    return 0xE0000;
    }

static uint32 *FaceplateHoldRegistersGet(Tha60290051ModulePtp self, uint8 partId, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[2][3] = {
                                        {0xE0000, 0xE0001, 0xE0002},
                                        {0xF0000, 0xF0001, 0xF0002},
                                        };
    AtUnused(self);

    if (partId >= 2)
        return NULL;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters[partId]);

    return holdRegisters[partId];
    }

static AtLongRegisterAccess FaceplateLongRegisterAccessCreate(Tha60290051ModulePtp self, uint8 partId)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = FaceplateHoldRegistersGet(self, partId, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint8 LocalAddressToPartId(uint32 localAddress)
    {
    localAddress = localAddress & cBit19_16;

    if (localAddress == 0xE0000)
        return 0;

    if (localAddress == 0xF0000)
        return 1;

    return cInvalidUint8;
    }

static AtLongRegisterAccess FaceplatePortLongRegisterAccess(Tha60290051ModulePtp self, uint32 localAddress)
    {
    uint8 partId = LocalAddressToPartId(localAddress);

    if (partId >= ThaModulePtpNumParts((ThaModulePtp)self))
        return NULL;

    if (self->faceplateLongAccessPerPart[partId])
        return self->faceplateLongAccessPerPart[partId];

    self->faceplateLongAccessPerPart[partId] = FaceplateLongRegisterAccessCreate(self, partId);
    return self->faceplateLongAccessPerPart[partId];
    }

static eBool IsFaceplatePortLocalAddress(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    localAddress = localAddress & cBit15_0;
    if (localAddress < 0x8000)
        return cAtTrue;
    return cAtFalse;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (IsFaceplatePortLocalAddress(self, localAddress))
        return FaceplatePortLongRegisterAccess(mThis(self), localAddress);

    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static uint32 *BackplaneHoldRegistersGet(ThaModulePtp self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0xEB020, 0xEB021, 0xEB022};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if ((localAddress >= 0x00E0000) && (localAddress <= 0xFFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static AtPtpPort PtpPortObjectCreate(AtModulePtp self, uint32 portId)
    {
    if (IsBackplanePortId(portId))
        return Tha60290051BackplanePtpPortNew(portId, (AtModule)self);

    return Tha60290051PtpPortNew(portId, (AtModule)self);
    }

static void T1T3TimestampContentParser(ThaModulePtp self, uint32 *buffer, tAtPtpT1T3TimestampContent *content)
    {
    content->nanoSeconds = buffer[0];
    content->seconds = buffer[1];
    content->seconds += ((uint64)mRegField(buffer[2], cAf6_upen_cpuque40_egr_tmr_tod_03_) << 32);
    content->egressPort = ThaModulePtpEgressPortIdToPtpPort(self, mRegField(buffer[2], cAf6_upen_cpuque40_egr_pid_));
    content->sequenceNumber = (uint16)AtUtilLongFieldGet(buffer, 2,
                                                         cAf6_upen_cpuque40_seqid_01_Mask, cAf6_upen_cpuque40_seqid_01_Shift,
                                                         cAf6_upen_cpuque40_seqid_02_Mask, cAf6_upen_cpuque40_seqid_02_Shift);
    content->ptpMsgType = mRegField(buffer[3], cAf6_upen_cpuque40_typ_);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->faceplateLongAccessPerPart[0]));
    AtObjectDelete((AtObject)(mThis(self)->faceplateLongAccessPerPart[1]));

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290051ModulePtp object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(faceplateLongAccessPerPart, 2);
    }

static void OverrideAtObject(AtModulePtp self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePtp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePtp(AtModulePtp self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePtpMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePtpOverride, mMethodsGet(self), sizeof(m_AtModulePtpOverride));

        /* Setup methods */
        mMethodOverride(m_AtModulePtpOverride, NumPtpPorts);
        mMethodOverride(m_AtModulePtpOverride, NumL2PsnGroups);
        mMethodOverride(m_AtModulePtpOverride, NumIpV4PsnGroups);
        mMethodOverride(m_AtModulePtpOverride, NumIpV6PsnGroups);
        mMethodOverride(m_AtModulePtpOverride, PtpPortObjectCreate);
        }

    mMethodsSet(self, &m_AtModulePtpOverride);
    }

static void OverrideThaModulePtp(AtModulePtp self)
    {
    ThaModulePtp module = (ThaModulePtp)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePtpOverride, mMethodsGet(module), sizeof(m_ThaModulePtpOverride));

        /* Setup methods */
        mMethodOverride(m_ThaModulePtpOverride, NumParts);
        mMethodOverride(m_ThaModulePtpOverride, PortCanJoinPsnGroup);
        mMethodOverride(m_ThaModulePtpOverride, PortLocalId);
        mMethodOverride(m_ThaModulePtpOverride, BaseAddress);
        mMethodOverride(m_ThaModulePtpOverride, BackplaneHoldRegistersGet);
        mMethodOverride(m_ThaModulePtpOverride, T1T3TimestampContentParser);
        }

    mMethodsSet(module, &m_ThaModulePtpOverride);
    }

static void Override(AtModulePtp self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePtp(self);
    OverrideThaModulePtp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051ModulePtp);
    }

static AtModulePtp ObjectInit(AtModulePtp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePtpObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePtp Tha60290051ModulePtpNew(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePtp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return ObjectInit(newModule, self);
    }

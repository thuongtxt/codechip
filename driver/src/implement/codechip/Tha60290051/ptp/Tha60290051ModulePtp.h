/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : 60290051 PTP
 * 
 * File        : Tha60290051ModulePtp.h
 * 
 * Created Date: Nov 19, 2018
 *
 * Description : Interface of the 60290051 PTP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051MODULEPTP_H_
#define _THA60290051MODULEPTP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePtp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051ModulePtp *Tha60290051ModulePtp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051MODULEPTP_H_ */


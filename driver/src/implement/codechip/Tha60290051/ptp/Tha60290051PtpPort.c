/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : Tha60290051PtpPort.c
 *
 * Created Date: Feb 22, 2019
 *
 * Description : 60290051 Faceplate PTP port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ptp/ThaPtpPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051PtpPort
    {
    tThaPtpPort super;
    }tTha60290051PtpPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPtpPortMethods m_ThaPtpPortOverride;

/* Save super implementations */
static const tThaPtpPortMethods *m_ThaPtpPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxDelayAdjustDefaultVlue(ThaPtpPort self, eAtSerdesMode serdesMode)
    {
    if (serdesMode == cAtSerdesModeEth1G)
        return 0x210;

    return m_ThaPtpPortMethods->TxDelayAdjustDefaultVlue(self, serdesMode);
    }

static void OverrideThaPtpPort(AtPtpPort self)
    {
    ThaPtpPort port = (ThaPtpPort)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPtpPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPtpPortOverride, m_ThaPtpPortMethods, sizeof(m_ThaPtpPortOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPtpPortOverride, TxDelayAdjustDefaultVlue);
        }

    mMethodsSet(port, &m_ThaPtpPortOverride);
    }

static void Override(AtPtpPort self)
    {
    OverrideThaPtpPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051PtpPort);
    }

static AtPtpPort ObjectInit(AtPtpPort self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPtpPortObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtpPort Tha60290051PtpPortNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    return ObjectInit(newPort, channelId, module);
    }


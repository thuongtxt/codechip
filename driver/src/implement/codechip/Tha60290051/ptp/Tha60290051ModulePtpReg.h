/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : PTP
 *                                                                              
 * File        : Tha60290051ModulePtpReg.h
 *                                                                              
 * Created Date: Feb 13, 2019
 *                                                                              
 * Description : This file contain all constanst definitions of PTP block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290051MODULEPTPREG_H_
#define _THA60290051MODULEPTPREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 1
Reg Addr   : 0x00_0000
Reg Formula: 0x00_0000 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register hold value 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold1_Base                                                                       0x000000
#define cAf6Reg_upen_hold1(group16)                                                 (0x000000+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_hold1
BitField Type: RW
BitField Desc: hold value 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold1_cfg_hold1_Mask                                                                cBit31_0
#define cAf6_upen_hold1_cfg_hold1_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 2
Reg Addr   : 0x00_0001
Reg Formula: 0x00_0001 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register hold value 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold2_Base                                                                       0x000001
#define cAf6Reg_upen_hold2(group16)                                                 (0x000001+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_hold2
BitField Type: RW
BitField Desc: hold value 2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold2_cfg_hold2_Mask                                                                cBit31_0
#define cAf6_upen_hold2_cfg_hold2_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 3
Reg Addr   : 0x00_0002
Reg Formula: 0x00_0002 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register hold value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold3_Base                                                                       0x000002
#define cAf6Reg_upen_hold3(group16)                                                 (0x000002+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_hold3
BitField Type: RW
BitField Desc: hold value 3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold3_cfg_hold3_Mask                                                                cBit31_0
#define cAf6_upen_hold3_cfg_hold3_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PTP enable
Reg Addr   : 0x00_0003
Reg Formula: 0x00_0003 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
used to enable ptp for 16 port ingress

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp_en_Base                                                                      0x000003
#define cAf6Reg_upen_ptp_en(group16)                                                (0x000003+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_ptp_en
BitField Type: RW
BitField Desc: '1' : enable, '0': disable
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ptp_en_cfg_ptp_en_Mask                                                              cBit15_0
#define cAf6_upen_ptp_en_cfg_ptp_en_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PTP enable
Reg Addr   : 0x00_0004
Reg Formula: 0x00_0004 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
used to indicate 1-step/2-step mode for each port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp_stmode_Base                                                                  0x000004
#define cAf6Reg_upen_ptp_stmode(group16)                                            (0x000004+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_ptp_stmode
BitField Type: RW
BitField Desc: '1' : 2-step, '0': 1-step
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ptp_stmode_cfg_ptp_stmode_Mask                                                      cBit15_0
#define cAf6_upen_ptp_stmode_cfg_ptp_stmode_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : PTP enable
Reg Addr   : 0x00_0005
Reg Formula: 0x00_0005 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
used to indicate master/slaver mode for each port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp_ms_Base                                                                      0x000005
#define cAf6Reg_upen_ptp_ms(group16)                                                (0x000005+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_ptp_ms
BitField Type: RW
BitField Desc: '1' : master, '0': slaver
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ptp_ms_cfg_ptp_ms_Mask                                                              cBit15_0
#define cAf6_upen_ptp_ms_cfg_ptp_ms_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PTP Device
Reg Addr   : 0x00_0006
Reg Formula: 0x00_0006 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register is used to config PTP device

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp_dev_Base                                                                     0x000006
#define cAf6Reg_upen_ptp_dev(group16)                                               (0x000006+(group16)*65536)

/*--------------------------------------
BitField Name: device_mode
BitField Type: RW
BitField Desc: '00': BC mode, '01': reserved '10': TC Separate mode, '11': TC
General mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_ptp_dev_device_mode_Mask                                                             cBit1_0
#define cAf6_upen_ptp_dev_device_mode_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Config Master SPID
Reg Addr   : 0x00_0007
Reg Formula: 0x00_0007 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to checking sourcePortIdentity of Sync, Follow-up and Delay Response packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ms_srcpid_Base                                                                   0x000007
#define cAf6Reg_upen_ms_srcpid(group16)                                             (0x000007+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_ms_srcpid
BitField Type: RW
BitField Desc: sourcePortIdentity value of Master
BitField Bits: [79:00]
--------------------------------------*/
#define cAf6_upen_ms_srcpid_cfg_ms_srcpid_Mask_01                                                     cBit31_0
#define cAf6_upen_ms_srcpid_cfg_ms_srcpid_Shift_01                                                           0
#define cAf6_upen_ms_srcpid_cfg_ms_srcpid_Mask_02                                                     cBit31_0
#define cAf6_upen_ms_srcpid_cfg_ms_srcpid_Shift_02                                                           0
#define cAf6_upen_ms_srcpid_cfg_ms_srcpid_Mask_03                                                     cBit15_0
#define cAf6_upen_ms_srcpid_cfg_ms_srcpid_Shift_03                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Config Slaver ReqPID
Reg Addr   : 0x00_0008
Reg Formula: 0x00_0008 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to checking requestingPortIdentity of Delay Response packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sl_reqpid_Base                                                                   0x000008
#define cAf6Reg_upen_sl_reqpid(group16)                                             (0x000008+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_sl_reqpid
BitField Type: RW
BitField Desc: RequestingPortIdentity value of Slaver
BitField Bits: [79:00]
--------------------------------------*/
#define cAf6_upen_sl_reqpid_cfg_sl_reqpid_Mask_01                                                     cBit31_0
#define cAf6_upen_sl_reqpid_cfg_sl_reqpid_Shift_01                                                           0
#define cAf6_upen_sl_reqpid_cfg_sl_reqpid_Mask_02                                                     cBit31_0
#define cAf6_upen_sl_reqpid_cfg_sl_reqpid_Shift_02                                                           0
#define cAf6_upen_sl_reqpid_cfg_sl_reqpid_Mask_03                                                     cBit15_0
#define cAf6_upen_sl_reqpid_cfg_sl_reqpid_Shift_03                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Config chsv4 bypass
Reg Addr   : 0x00_0009
Reg Formula: 0x00_0009 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to indicate bypass ipv4 header checksum error

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_chsv4_bypass_Base                                                                0x000009
#define cAf6Reg_upen_chsv4_bypass(group16)                                          (0x000009+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_chsv4_bypass
BitField Type: RW
BitField Desc: '1': enable, '0': disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_chsv4_bypass_cfg_chsv4_bypass_Mask                                                     cBit0
#define cAf6_upen_chsv4_bypass_cfg_chsv4_bypass_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : PTP Ready
Reg Addr   : 0x00_0015
Reg Formula: 0x00_0015 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register is used to indicate config ptp done

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp_rdy_Base                                                                     0x000015
#define cAf6Reg_upen_ptp_rdy(group16)                                               (0x000015+(group16)*65536)

/*--------------------------------------
BitField Name: ptp_rdy
BitField Type: RW
BitField Desc: '1' : ready
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_ptp_rdy_ptp_rdy_Mask                                                                   cBit0
#define cAf6_upen_ptp_rdy_ptp_rdy_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter TX FACE PTP
Reg Addr   : 0x00_0400(R2C)
Reg Formula: 0x00_0400 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Total tx face packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_ptp_cnt_r2c_Base                                                              0x000400
#define cAf6Reg_upen_tx_ptp_cnt_r2c(group16, pid)                             (0x000400+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: tx_face_cnt
BitField Type: RC
BitField Desc: total tx face packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_ptp_cnt_r2c_tx_face_cnt_Mask                                                     cBit31_0
#define cAf6_upen_tx_ptp_cnt_r2c_tx_face_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter TX FACE PTP
Reg Addr   : 0x00_0410(RO)
Reg Formula: 0x00_0410 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Total tx face packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_ptp_cnt_ro_Base                                                               0x000410
#define cAf6Reg_upen_tx_ptp_cnt_ro(group16, pid)                              (0x000410+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: tx_face_cnt
BitField Type: RC
BitField Desc: total tx face packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_ptp_cnt_ro_tx_face_cnt_Mask                                                      cBit31_0
#define cAf6_upen_tx_ptp_cnt_ro_tx_face_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter PTP Packet
Reg Addr   : 0x00_0460(R2C)
Reg Formula: 0x00_0460 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Total rx face packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_ptp_cnt_r2c_Base                                                              0x000460
#define cAf6Reg_upen_rx_ptp_cnt_r2c(group16, pid)                             (0x000460+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_ptp_cnt
BitField Type: RC
BitField Desc: Total rx ptp packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_ptp_cnt_r2c_rx_ptp_cnt_Mask                                                      cBit31_0
#define cAf6_upen_rx_ptp_cnt_r2c_rx_ptp_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter PTP Packet
Reg Addr   : 0x00_0470(RO)
Reg Formula: 0x00_0470 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Total rx face packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_ptp_cnt_ro_Base                                                               0x000470
#define cAf6Reg_upen_rx_ptp_cnt_ro(group16, pid)                              (0x000470+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_ptp_cnt
BitField Type: RC
BitField Desc: Total rx ptp packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_ptp_cnt_ro_rx_ptp_cnt_Mask                                                       cBit31_0
#define cAf6_upen_rx_ptp_cnt_ro_rx_ptp_cnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Received
Reg Addr   : 0x00_480(R2C)
Reg Formula: 0x00_480 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Sync message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_sync_cnt_r2c_Base                                                              0x00480
#define cAf6Reg_upen_rx_sync_cnt_r2c(group16, pid)                             (0x00480+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: syncrx_cnt
BitField Type: RC
BitField Desc: number sync packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_sync_cnt_r2c_syncrx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_rx_sync_cnt_r2c_syncrx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Received
Reg Addr   : 0x00_4C0(RO)
Reg Formula: 0x00_4C0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Sync message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_sync_cnt_ro_Base                                                               0x004C0
#define cAf6Reg_upen_rx_sync_cnt_ro(group16, pid)                              (0x004C0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: syncrx_cnt
BitField Type: RC
BitField Desc: number sync packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_sync_cnt_ro_syncrx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_rx_sync_cnt_ro_syncrx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Received
Reg Addr   : 0x00_490(R2C)
Reg Formula: 0x00_490 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of folu message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_folu_cnt_r2c_Base                                                              0x00490
#define cAf6Reg_upen_rx_folu_cnt_r2c(group16, pid)                             (0x00490+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: folurx_cnt
BitField Type: RC
BitField Desc: number folu packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_folu_cnt_r2c_folurx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_rx_folu_cnt_r2c_folurx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Received
Reg Addr   : 0x00_4D0(RO)
Reg Formula: 0x00_4D0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of folu message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_folu_cnt_ro_Base                                                               0x004D0
#define cAf6Reg_upen_rx_folu_cnt_ro(group16, pid)                              (0x004D0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: folurx_cnt
BitField Type: RC
BitField Desc: number folu packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_folu_cnt_ro_folurx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_rx_folu_cnt_ro_folurx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq Received
Reg Addr   : 0x00_4A0(R2C)
Reg Formula: 0x00_4A0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dreq message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dreq_cnt_r2c_Base                                                              0x004A0
#define cAf6Reg_upen_rx_dreq_cnt_r2c(group16, pid)                             (0x004A0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: dreqrx_cnt
BitField Type: RC
BitField Desc: number dreq packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dreq_cnt_r2c_dreqrx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_rx_dreq_cnt_r2c_dreqrx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq Received
Reg Addr   : 0x00_4E0(RO)
Reg Formula: 0x00_4E0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dreq message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dreq_cnt_ro_Base                                                               0x004E0
#define cAf6Reg_upen_rx_dreq_cnt_ro(group16, pid)                              (0x004E0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: dreqrx_cnt
BitField Type: RC
BitField Desc: number dreq packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dreq_cnt_ro_dreqrx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_rx_dreq_cnt_ro_dreqrx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres Received
Reg Addr   : 0x00_4B0(R2C)
Reg Formula: 0x00_4B0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dres message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dres_cnt_r2c_Base                                                              0x004B0
#define cAf6Reg_upen_rx_dres_cnt_r2c(group16, pid)                             (0x004B0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: dresrx_cnt
BitField Type: RC
BitField Desc: number dres packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dres_cnt_r2c_dresrx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_rx_dres_cnt_r2c_dresrx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres Received
Reg Addr   : 0x00_4F0(RO)
Reg Formula: 0x00_4F0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dres message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dres_cnt_ro_Base                                                               0x004F0
#define cAf6Reg_upen_rx_dres_cnt_ro(group16, pid)                              (0x004F0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: dresrx_cnt
BitField Type: RC
BitField Desc: number dres packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dres_cnt_ro_dresrx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_rx_dres_cnt_ro_dresrx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Sync Extract Err
Reg Addr   : 0x00_500(R2C)
Reg Formula: 0x00_500 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err sync mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_syncnt_r2c_Base                                                       0x00500
#define cAf6Reg_upen_rx_extr_err_syncnt_r2c(group16, pid)                      (0x00500+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_synccnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_syncnt_r2c_rx_extrerr_synccnt_Mask                                      cBit31_0
#define cAf6_upen_rx_extr_err_syncnt_r2c_rx_extrerr_synccnt_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Sync Extract Err
Reg Addr   : 0x00_0540(RO)
Reg Formula: 0x00_0540 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err sync mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_syncnt_ro_Base                                                       0x000540
#define cAf6Reg_upen_rx_extr_err_syncnt_ro(group16, pid)                      (0x000540+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_synccnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_syncnt_ro_rx_extrerr_synccnt_Mask                                       cBit31_0
#define cAf6_upen_rx_extr_err_syncnt_ro_rx_extrerr_synccnt_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Folu Extract Err
Reg Addr   : 0x00_0510(R2C)
Reg Formula: 0x00_0510 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err folu mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_folucnt_r2c_Base                                                     0x000510
#define cAf6Reg_upen_rx_extr_err_folucnt_r2c(group16, pid)                    (0x000510+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_foluccnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_folucnt_r2c_rx_extrerr_foluccnt_Mask                                    cBit31_0
#define cAf6_upen_rx_extr_err_folucnt_r2c_rx_extrerr_foluccnt_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Folu Extract Err
Reg Addr   : 0x00_0550(RO)
Reg Formula: 0x00_0550 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err folu mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_folucnt_ro_Base                                                      0x000550
#define cAf6Reg_upen_rx_extr_err_folucnt_ro(group16, pid)                     (0x000550+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_foluccnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_folucnt_ro_rx_extrerr_foluccnt_Mask                                     cBit31_0
#define cAf6_upen_rx_extr_err_folucnt_ro_rx_extrerr_foluccnt_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dreq Extract Err
Reg Addr   : 0x00_0520(R2C)
Reg Formula: 0x00_0520 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err dreq mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_dreqcnt_r2c_Base                                                     0x000520
#define cAf6Reg_upen_rx_extr_err_dreqcnt_r2c(group16, pid)                    (0x000520+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_dreqcnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_dreqcnt_r2c_rx_extrerr_dreqcnt_Mask                                     cBit31_0
#define cAf6_upen_rx_extr_err_dreqcnt_r2c_rx_extrerr_dreqcnt_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dreq Extract Err
Reg Addr   : 0x00_0560(RO)
Reg Formula: 0x00_0560 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err dreq mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_dreqcnt_ro_Base                                                      0x000560
#define cAf6Reg_upen_rx_extr_err_dreqcnt_ro(group16, pid)                     (0x000560+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_dreqcnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_dreqcnt_ro_rx_extrerr_dreqcnt_Mask                                      cBit31_0
#define cAf6_upen_rx_extr_err_dreqcnt_ro_rx_extrerr_dreqcnt_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dres Extract Err
Reg Addr   : 0x00_0530(R2C)
Reg Formula: 0x00_0530 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err dres mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_drescnt_r2c_Base                                                     0x000530
#define cAf6Reg_upen_rx_extr_err_drescnt_r2c(group16, pid)                    (0x000530+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_drescnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_drescnt_r2c_rx_extrerr_drescnt_Mask                                     cBit31_0
#define cAf6_upen_rx_extr_err_drescnt_r2c_rx_extrerr_drescnt_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dres Extract Err
Reg Addr   : 0x00_0570(RO)
Reg Formula: 0x00_0570 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of extr err dres mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_extr_err_drescnt_ro_Base                                                      0x000570
#define cAf6Reg_upen_rx_extr_err_drescnt_ro(group16, pid)                     (0x000570+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_extrerr_drescnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_extr_err_drescnt_ro_rx_extrerr_drescnt_Mask                                      cBit31_0
#define cAf6_upen_rx_extr_err_drescnt_ro_rx_extrerr_drescnt_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Sync Chsum Err
Reg Addr   : 0x00_0580(R2C)
Reg Formula: 0x00_0580 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_syncnt_r2c_Base                                                       0x000580
#define cAf6Reg_upen_rx_chs_err_syncnt_r2c(group16, pid)                      (0x000580+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_synccnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_syncnt_r2c_rx_cherr_synccnt_Mask                                         cBit31_0
#define cAf6_upen_rx_chs_err_syncnt_r2c_rx_cherr_synccnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Sync Chsum Err
Reg Addr   : 0x00_05C0(RO)
Reg Formula: 0x00_05C0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_syncnt_ro_Base                                                        0x0005C0
#define cAf6Reg_upen_rx_chs_err_syncnt_ro(group16, pid)                       (0x0005C0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_synccnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_syncnt_ro_rx_cherr_synccnt_Mask                                          cBit31_0
#define cAf6_upen_rx_chs_err_syncnt_ro_rx_cherr_synccnt_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Folu Chsum Err
Reg Addr   : 0x00_0590(R2C)
Reg Formula: 0x00_0590 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_folucnt_r2c_Base                                                      0x000590
#define cAf6Reg_upen_rx_chs_err_folucnt_r2c(group16, pid)                     (0x000590+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_folucnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_folucnt_r2c_rx_cherr_folucnt_Mask                                        cBit31_0
#define cAf6_upen_rx_chs_err_folucnt_r2c_rx_cherr_folucnt_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Folu Chsum Err
Reg Addr   : 0x00_05D0(RO)
Reg Formula: 0x00_05D0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_folucnt_ro_Base                                                       0x0005D0
#define cAf6Reg_upen_rx_chs_err_folucnt_ro(group16, pid)                      (0x0005D0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_folucnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_folucnt_ro_rx_cherr_folucnt_Mask                                         cBit31_0
#define cAf6_upen_rx_chs_err_folucnt_ro_rx_cherr_folucnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dreq Chsum Err
Reg Addr   : 0x00_05A0(R2C)
Reg Formula: 0x00_05A0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Chs err dreq mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_dreqcnt_r2c_Base                                                      0x0005A0
#define cAf6Reg_upen_rx_chs_err_dreqcnt_r2c(group16, pid)                     (0x0005A0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_dreqcnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_dreqcnt_r2c_rx_cherr_dreqcnt_Mask                                        cBit31_0
#define cAf6_upen_rx_chs_err_dreqcnt_r2c_rx_cherr_dreqcnt_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dreq Chsum Err
Reg Addr   : 0x00_5E0(RO)
Reg Formula: 0x00_5E0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Chs err dreq mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_dreqcnt_ro_Base                                                        0x005E0
#define cAf6Reg_upen_rx_chs_err_dreqcnt_ro(group16, pid)                       (0x005E0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_dreqcnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_dreqcnt_ro_rx_cherr_dreqcnt_Mask                                         cBit31_0
#define cAf6_upen_rx_chs_err_dreqcnt_ro_rx_cherr_dreqcnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dres Chsum Err
Reg Addr   : 0x00_05B0(R2C)
Reg Formula: 0x00_05B0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Chs err dres mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_drescnt_r2c_Base                                                      0x0005B0
#define cAf6Reg_upen_rx_chs_err_drescnt_r2c(group16, pid)                     (0x0005B0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_drescnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_drescnt_r2c_rx_cherr_drescnt_Mask                                        cBit31_0
#define cAf6_upen_rx_chs_err_drescnt_r2c_rx_cherr_drescnt_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter Rx Dres Chsum Err
Reg Addr   : 0x00_05F0(RO)
Reg Formula: 0x00_05F0 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Chs err dres mess

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_chs_err_drescnt_ro_Base                                                       0x0005F0
#define cAf6Reg_upen_rx_chs_err_drescnt_ro(group16, pid)                      (0x0005F0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: rx_cherr_drescnt
BitField Type: RC
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_chs_err_drescnt_ro_rx_cherr_drescnt_Mask                                         cBit31_0
#define cAf6_upen_rx_chs_err_drescnt_ro_rx_cherr_drescnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Transmit
Reg Addr   : 0x00_0600(R2C)
Reg Formula: 0x00_0600 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Sync message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_sync_cnt_r2c_Base                                                             0x000600
#define cAf6Reg_upen_tx_sync_cnt_r2c(group16, pid)                            (0x000600+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: synctx_cnt
BitField Type: RC
BitField Desc: number sync packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_sync_cnt_r2c_synctx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_tx_sync_cnt_r2c_synctx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Transmit
Reg Addr   : 0x00_0640(RO)
Reg Formula: 0x00_0640 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of Sync message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_sync_cnt_ro_Base                                                              0x000640
#define cAf6Reg_upen_tx_sync_cnt_ro(group16, pid)                             (0x000640+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: synctx_cnt
BitField Type: RC
BitField Desc: number sync packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_sync_cnt_ro_synctx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_tx_sync_cnt_ro_synctx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Transmit
Reg Addr   : 0x00_0610(R2C)
Reg Formula: 0x00_0610 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of folu message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_folu_cnt_r2c_Base                                                             0x000610
#define cAf6Reg_upen_tx_folu_cnt_r2c(group16, pid)                            (0x000610+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: folutx_cnt
BitField Type: RC
BitField Desc: number folu packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_folu_cnt_r2c_folutx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_tx_folu_cnt_r2c_folutx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Transmit
Reg Addr   : 0x00_0650(RO)
Reg Formula: 0x00_0650 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of folu message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_folu_cnt_ro_Base                                                              0x000650
#define cAf6Reg_upen_tx_folu_cnt_ro(group16, pid)                             (0x000650+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: folutx_cnt
BitField Type: RC
BitField Desc: number folu packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_folu_cnt_ro_folutx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_tx_folu_cnt_ro_folutx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq transmit
Reg Addr   : 0x00_0620(R2C)
Reg Formula: 0x00_0620 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dreq message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dreq_cnt_r2c_Base                                                             0x000620
#define cAf6Reg_upen_tx_dreq_cnt_r2c(group16, pid)                            (0x000620+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: dreqtx_cnt
BitField Type: RC
BitField Desc: number dreq packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dreq_cnt_r2c_dreqtx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_tx_dreq_cnt_r2c_dreqtx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq transmit
Reg Addr   : 0x00_0660(RO)
Reg Formula: 0x00_0660 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dreq message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dreq_cnt_ro_Base                                                              0x000660
#define cAf6Reg_upen_tx_dreq_cnt_ro(group16, pid)                             (0x000660+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: dreqtx_cnt
BitField Type: RC
BitField Desc: number dreq packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dreq_cnt_ro_dreqtx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_tx_dreq_cnt_ro_dreqtx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres transmit
Reg Addr   : 0x00_0630(R2C)
Reg Formula: 0x00_0630 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dres message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dres_cnt_r2c_Base                                                             0x000630
#define cAf6Reg_upen_tx_dres_cnt_r2c(group16, pid)                            (0x000630+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: drestx_cnt
BitField Type: RC
BitField Desc: number dres packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dres_cnt_r2c_drestx_cnt_Mask                                                     cBit31_0
#define cAf6_upen_tx_dres_cnt_r2c_drestx_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres transmit
Reg Addr   : 0x00_0670(RO)
Reg Formula: 0x00_0670 +  $pid + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
           + $pid(0-15)
Reg Desc   : 
Number of dres message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dres_cnt_ro_Base                                                              0x000670
#define cAf6Reg_upen_tx_dres_cnt_ro(group16, pid)                             (0x000670+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: drestx_cnt
BitField Type: RC
BitField Desc: number dres packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dres_cnt_ro_drestx_cnt_Mask                                                      cBit31_0
#define cAf6_upen_tx_dres_cnt_ro_drestx_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : classify sticky
Reg Addr   : 0x00_0330
Reg Formula: 0x00_0330 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of classify

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla_stk_Base                                                                     0x000330
#define cAf6Reg_upen_cla_stk(group16)                                               (0x000330+(group16)*65536)

/*--------------------------------------
BitField Name: cla_stk
BitField Type: W1C
BitField Desc: sticky ge_classify
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cla_stk_cla_stk_Mask                                                                cBit31_0
#define cAf6_upen_cla_stk_cla_stk_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PTP check sticky
Reg Addr   : 0x00_0331
Reg Formula: 0x00_0331 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of ptp check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_chk_stk_Base                                                                     0x000331
#define cAf6Reg_upen_chk_stk(group16)                                               (0x000331+(group16)*65536)

/*--------------------------------------
BitField Name: chk_stk
BitField Type: W1C
BitField Desc: sticky ptp check
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_chk_stk_chk_stk_Mask                                                                cBit31_0
#define cAf6_upen_chk_stk_chk_stk_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : ptp getinfo sticky
Reg Addr   : 0x00_0332
Reg Formula: 0x00_0332 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of ptp get info

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_getinf_stk_Base                                                                  0x000332
#define cAf6Reg_upen_getinf_stk(group16)                                            (0x000332+(group16)*65536)

/*--------------------------------------
BitField Name: getinf_stk
BitField Type: W1C
BitField Desc: sticky ptp get info
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_getinf_stk_getinf_stk_Mask                                                          cBit31_0
#define cAf6_upen_getinf_stk_getinf_stk_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : trans sticky
Reg Addr   : 0x00_0333
Reg Formula: 0x00_0333 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of ptp trans

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_trans_stk_Base                                                                   0x000333
#define cAf6Reg_upen_trans_stk(group16)                                             (0x000333+(group16)*65536)

/*--------------------------------------
BitField Name: trans_stk
BitField Type: W1C
BitField Desc: sticky ptp trans
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_trans_stk_trans_stk_Mask                                                            cBit31_0
#define cAf6_upen_trans_stk_trans_stk_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : cpu queue sticky
Reg Addr   : 0x00_0334
Reg Formula: 0x00_0334 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of cpu que

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cpuque_stk_Base                                                                  0x000334
#define cAf6Reg_upen_cpuque_stk(group16)                                            (0x000334+(group16)*65536)

/*--------------------------------------
BitField Name: cpuque_stk
BitField Type: W1C
BitField Desc: sticky of cpu queue
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cpuque_stk_cpuque_stk_Mask                                                          cBit31_0
#define cAf6_upen_cpuque_stk_cpuque_stk_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Config t1t3mode
Reg Addr   : 0x00_0017
Reg Formula: 0x00_0017 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to config t1t3mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_t1t3mode_Base                                                                    0x000017
#define cAf6Reg_upen_t1t3mode(group16)                                              (0x000017+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_t1t3mode_enable
BitField Type: RW
BitField Desc: 1: enable this mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_t1t3mode_cfg_t1t3mode_enable_Mask                                                      cBit1
#define cAf6_upen_t1t3mode_cfg_t1t3mode_enable_Shift                                                         1

/*--------------------------------------
BitField Name: cfg_t1t3mode
BitField Type: RW
BitField Desc: 0: send timestamp to cpu queue 1: send back ptp packet mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_t1t3mode_cfg_t1t3mode_Mask                                                             cBit0
#define cAf6_upen_t1t3mode_cfg_t1t3mode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Config cpu flush
Reg Addr   : 0x00_0018
Reg Formula: 0x00_0018 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to config cpu flush

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cpu_flush_Base                                                                   0x000018
#define cAf6Reg_upen_cpu_flush(group16)                                             (0x000018+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_cpu_flush
BitField Type: RW
BitField Desc: cpu flush
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cpu_flush_cfg_cpu_flush_Mask                                                           cBit0
#define cAf6_upen_cpu_flush_cfg_cpu_flush_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Read CPU Queue
Reg Addr   : 0x00_0216
Reg Formula: 0x00_0216 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cpuque_Base                                                                      0x000216
#define cAf6Reg_upen_cpuque(group16)                                                (0x000216+(group16)*65536)

/*--------------------------------------
BitField Name: typ
BitField Type: RO
BitField Desc: typ of ptp packet
BitField Bits: [101:100]
--------------------------------------*/
#define cAf6_upen_cpuque_typ_Mask                                                                      cBit5_4
#define cAf6_upen_cpuque_typ_Shift                                                                           4

/*--------------------------------------
BitField Name: seqid
BitField Type: RO
BitField Desc: sequence of packet from sequenceId field
BitField Bits: [99:84]
--------------------------------------*/
#define cAf6_upen_cpuque_seqid_Mask_01                                                               cBit31_20
#define cAf6_upen_cpuque_seqid_Shift_01                                                                     20
#define cAf6_upen_cpuque_seqid_Mask_02                                                                 cBit3_0
#define cAf6_upen_cpuque_seqid_Shift_02                                                                      0

/*--------------------------------------
BitField Name: egr_pid
BitField Type: RO
BitField Desc: egress port id
BitField Bits: [83:80]
--------------------------------------*/
#define cAf6_upen_cpuque_egr_pid_Mask                                                                cBit19_16
#define cAf6_upen_cpuque_egr_pid_Shift                                                                      16

/*--------------------------------------
BitField Name: egr_tmr_tod
BitField Type: RO
BitField Desc: egress timer tod
BitField Bits: [79:00]
--------------------------------------*/
#define cAf6_upen_cpuque_egr_tmr_tod_Mask_01                                                          cBit31_0
#define cAf6_upen_cpuque_egr_tmr_tod_Shift_01                                                                0
#define cAf6_upen_cpuque_egr_tmr_tod_Mask_02                                                          cBit31_0
#define cAf6_upen_cpuque_egr_tmr_tod_Shift_02                                                                0
#define cAf6_upen_cpuque_egr_tmr_tod_Mask_03                                                          cBit15_0
#define cAf6_upen_cpuque_egr_tmr_tod_Shift_03                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Enb CPU Interupt
Reg Addr   : 0x00_0200
Reg Formula: 0x00_0200 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_en_Base                                                                      0x000200
#define cAf6Reg_upen_int_en(group16)                                                (0x000200+(group16)*65536)

/*--------------------------------------
BitField Name: int_en
BitField Type: RW
BitField Desc: set "1" ,interupt enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_en_int_en_Mask                                                                     cBit0
#define cAf6_upen_int_en_int_en_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Interupt CPU Queue
Reg Addr   : 0x00_0201
Reg Formula: 0x00_0201 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_Base                                                                         0x000201
#define cAf6Reg_upen_int(group16)                                                   (0x000201+(group16)*65536)

/*--------------------------------------
BitField Name: int_en
BitField Type: RW
BitField Desc: set"1" queue info ready ,write 1 to clear
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_int_en_Mask                                                                        cBit0
#define cAf6_upen_int_int_en_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Interupt CPU Queue
Reg Addr   : 0x00_0202
Reg Formula: 0x00_0202 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_Base                                                                         0x000202
#define cAf6Reg_upen_sta(group16)                                                   (0x000202+(group16)*65536)

/*--------------------------------------
BitField Name: upen_sta
BitField Type: RW
BitField Desc: current state of CPU queue, bit[17] "1" queue FULL, bit[16] "1"
NOT empty, ready for cpu read, bit[15:0] queue lengh
BitField Bits: [16:00]
--------------------------------------*/
#define cAf6_upen_sta_upen_sta_Mask                                                                   cBit16_0
#define cAf6_upen_sta_upen_sta_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : config unicast MAC address global
Reg Addr   : 0x00_4003
Reg Formula: 0x00_4003 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config unicast MAC address global for all port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_umac_glb_Base                                                                    0x004003
#define cAf6Reg_upen_umac_glb(group16)                                              (0x004003+(group16)*65536)
#define cAf6Reg_upen_umac_glb_WidthVal                                                                      64

/*--------------------------------------
BitField Name: cfg_umac_glb
BitField Type: RW
BitField Desc: uni MAC global
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_umac_glb_cfg_umac_glb_Mask_01                                                       cBit31_0
#define cAf6_upen_umac_glb_cfg_umac_glb_Shift_01                                                             0
#define cAf6_upen_umac_glb_cfg_umac_glb_Mask_02                                                       cBit15_0
#define cAf6_upen_umac_glb_cfg_umac_glb_Shift_02                                                             0


/*------------------------------------------------------------------------------
Reg Name   : config multicast MAC address global 1
Reg Addr   : 0x00_4004
Reg Formula: 0x00_4004 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config multicast MAC address global 1 for all port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mmac1_glb_Base                                                                   0x004004
#define cAf6Reg_upen_mmac1_glb(group16)                                             (0x004004+(group16)*65536)
#define cAf6Reg_upen_mmac1_glb_WidthVal                                                                     64

/*--------------------------------------
BitField Name: cfg_mmac1_glb
BitField Type: RW
BitField Desc: mul MAC global 1
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_mmac1_glb_cfg_mmac1_glb_Mask_01                                                     cBit31_0
#define cAf6_upen_mmac1_glb_cfg_mmac1_glb_Shift_01                                                           0
#define cAf6_upen_mmac1_glb_cfg_mmac1_glb_Mask_02                                                     cBit15_0
#define cAf6_upen_mmac1_glb_cfg_mmac1_glb_Shift_02                                                           0


/*------------------------------------------------------------------------------
Reg Name   : config multicast MAC address global 2
Reg Addr   : 0x00_4005
Reg Formula: 0x00_4005 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config multicast MAC address global 2 for all port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mmac2_glb_Base                                                                   0x004005
#define cAf6Reg_upen_mmac2_glb(group16)                                             (0x004005+(group16)*65536)
#define cAf6Reg_upen_mmac2_glb_WidthVal                                                                     64

/*--------------------------------------
BitField Name: cfg_mmac2_glb
BitField Type: RW
BitField Desc: mul MAC global 2
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_mmac2_glb_cfg_mmac2_glb_Mask_01                                                     cBit31_0
#define cAf6_upen_mmac2_glb_cfg_mmac2_glb_Shift_01                                                           0
#define cAf6_upen_mmac2_glb_cfg_mmac2_glb_Mask_02                                                     cBit15_0
#define cAf6_upen_mmac2_glb_cfg_mmac2_glb_Shift_02                                                           0


/*------------------------------------------------------------------------------
Reg Name   : config any MAC enable
Reg Addr   : 0x00_4006
Reg Formula: 0x00_4006 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
is used to enable any mac for each port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_anymac_en_Base                                                                   0x004006
#define cAf6Reg_upen_anymac_en(group16)                                             (0x004006+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_anymac_en
BitField Type: RW
BitField Desc: '1': enable , '0': disable
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_anymac_en_cfg_anymac_en_Mask                                                        cBit15_0
#define cAf6_upen_anymac_en_cfg_anymac_en_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : config MAC global matching enable
Reg Addr   : 0x00_4007
Reg Formula: 0x00_4007 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
is used to indicate MAC global matching enable

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_macglb_en_Base                                                                   0x004007
#define cAf6Reg_upen_macglb_en(group16)                                             (0x004007+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_anymac_en
BitField Type: RW
BitField Desc: [0] : en/dis unicast mac global matching [1] : en/dis multicast
mac global 1 matching [2] : en/dis multicast mac global 2 matching
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_macglb_en_cfg_anymac_en_Mask                                                         cBit2_0
#define cAf6_upen_macglb_en_cfg_anymac_en_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Config mul ipv4 global 1
Reg Addr   : 0x00_400A
Reg Formula: 0x00_400A + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config multicast ipv4 address global 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_v4mip1_glb_Base                                                                  0x00400A
#define cAf6Reg_upen_v4mip1_glb(group16)                                            (0x00400A+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_v4mip1_glb
BitField Type: RW
BitField Desc: used to check multicast ip
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_v4mip1_glb_cfg_v4mip1_glb_Mask                                                      cBit31_0
#define cAf6_upen_v4mip1_glb_cfg_v4mip1_glb_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config mul ipv4 global 2
Reg Addr   : 0x00_400B
Reg Formula: 0x00_400B + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config multicast ipv4 address global 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_v4mip2_glb_Base                                                                  0x00400B
#define cAf6Reg_upen_v4mip2_glb(group16)                                            (0x00400B+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_v4mip2_glb
BitField Type: RW
BitField Desc: used to check multicast ip
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_v4mip2_glb_cfg_v4mip2_glb_Mask                                                      cBit31_0
#define cAf6_upen_v4mip2_glb_cfg_v4mip2_glb_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config mul ipv6 global 1
Reg Addr   : 0x00_400C
Reg Formula: 0x00_400C + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config multicast ipv6 address global 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_v6mip1_glb_Base                                                                  0x00400C
#define cAf6Reg_upen_v6mip1_glb(group16)                                            (0x00400C+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_v6mip1_glb
BitField Type: RW
BitField Desc: used to check multicast ip
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Mask_01                                                   cBit31_0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Shift_01                                                         0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Mask_02                                                   cBit31_0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Shift_02                                                         0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Mask_03                                                   cBit31_0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Shift_03                                                         0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Mask_04                                                   cBit31_0
#define cAf6_upen_v6mip1_glb_cfg_v6mip1_glb_Shift_04                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Config mul ipv6 global 2
Reg Addr   : 0x00_400D
Reg Formula: 0x00_400D + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config multicast ipv6 address global 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_v6mip2_glb_Base                                                                  0x00400D
#define cAf6Reg_upen_v6mip2_glb(group16)                                            (0x00400D+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_v6mip2_glb
BitField Type: RW
BitField Desc: used to check multicast ip
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Mask_01                                                   cBit31_0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Shift_01                                                         0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Mask_02                                                   cBit31_0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Shift_02                                                         0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Mask_03                                                   cBit31_0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Shift_03                                                         0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Mask_04                                                   cBit31_0
#define cAf6_upen_v6mip2_glb_cfg_v6mip2_glb_Shift_04                                                         0


/*------------------------------------------------------------------------------
Reg Name   : config any IP enable
Reg Addr   : 0x00_400E
Reg Formula: 0x00_400E + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
is used to enable any ip for each port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_anyip_en_Base                                                                    0x00400E
#define cAf6Reg_upen_anyip_en(group16)                                              (0x00400E+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_anyip_en
BitField Type: RW
BitField Desc: '1': enable , '0': disable
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_anyip_en_cfg_anyip_en_Mask                                                          cBit15_0
#define cAf6_upen_anyip_en_cfg_anyip_en_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CLA Ready
Reg Addr   : 0x00_4011
Reg Formula: 0x00_4011 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register is used to indicate config ptp done

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla_rdy_Base                                                                     0x004011
#define cAf6Reg_upen_cla_rdy(group16)                                               (0x004011+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_cla_rdy
BitField Type: RW
BitField Desc: '1' : ready
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cla_rdy_cfg_cla_rdy_Mask                                                               cBit0
#define cAf6_upen_cla_rdy_cfg_cla_rdy_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : PTP layer force value
Reg Addr   : 0x00_4016
Reg Formula: 0x00_4016 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register is used to config ptp layer force value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_ptplayer_Base                                                              0x004016
#define cAf6Reg_upen_force_ptplayer(group16)                                        (0x004016+(group16)*65536)

/*--------------------------------------
BitField Name: ptp_layer
BitField Type: RW
BitField Desc: 3'b000: PTP L2 3'b001: IPv4 3'b010: IPv6 3'b011: IPv4 MPLS
unicast 3'b100: IPv6 MPLS unicast
BitField Bits: [03:01]
--------------------------------------*/
#define cAf6_upen_force_ptplayer_ptp_layer_Mask                                                        cBit3_1
#define cAf6_upen_force_ptplayer_ptp_layer_Shift                                                             1

/*--------------------------------------
BitField Name: ptp_ind
BitField Type: RW
BitField Desc: ptp indicate: 0: disable 1: enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_force_ptplayer_ptp_ind_Mask                                                            cBit0
#define cAf6_upen_force_ptplayer_ptp_ind_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MRU Packet Size
Reg Addr   : 0x00_4017
Reg Formula: 0x00_4017 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register is used to config MRU Value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mru_val_Base                                                                     0x004017
#define cAf6Reg_upen_mru_val(group16)                                               (0x004017+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_mru_en
BitField Type: RW
BitField Desc: MRU enable 1: enable 0: disable
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_mru_val_cfg_mru_en_Mask                                                               cBit14
#define cAf6_upen_mru_val_cfg_mru_en_Shift                                                                  14

/*--------------------------------------
BitField Name: cfg_mru_val
BitField Type: RW
BitField Desc: MRU value(Byte)
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_upen_mru_val_cfg_mru_val_Mask                                                            cBit13_0
#define cAf6_upen_mru_val_cfg_mru_val_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : cfg uni/mul mac
Reg Addr   : 0x00_5100-0x00_511F
Reg Formula: 0x00_5100 + $transtype*16 + $pid  + $group16*65536
    Where  : 
           + $transtype(0-1)
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config uni/mul MAC for each port
transtype = 0 : unicast
transtype = 1 : multicast
HDL_PATH     : inscorepi.insmac.membuf.ram.ram[$transtype*16 + $pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mac_Base                                                                         0x005100
#define cAf6Reg_upen_mac(transtype, pid, group16)                     (0x005100+(transtype)*16+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: macena
BitField Type: RW
BitField Desc: mac matching enable 0: disable 1: enable
BitField Bits: [48:48]
--------------------------------------*/
#define cAf6_upen_mac_macena_Mask                                                                       cBit16
#define cAf6_upen_mac_macena_Shift                                                                          16

/*--------------------------------------
BitField Name: cfg_mac
BitField Type: RW
BitField Desc: unicast/multicast mac for each port
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_mac_cfg_mac_Mask_01                                                                 cBit31_0
#define cAf6_upen_mac_cfg_mac_Shift_01                                                                       0
#define cAf6_upen_mac_cfg_mac_Mask_02                                                                 cBit15_0
#define cAf6_upen_mac_cfg_mac_Shift_02                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Config GE unicast/multicast IPv4/IPv6 address 1
Reg Addr   : 0x00_5120-0x00_513F
Reg Formula: 0x00_5120 + $transtype*16 + $pid + $group16*65536
    Where  : 
           + $transtype(0-1)
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config GE uni/mul IP 1 for each port
transtype = 0 : unicast
transtype = 1 : multicast
HDL_PATH     : inscorepi.insgeip1.membuf.ram.ram[$transtype*16 + $pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_geip1_Base                                                                       0x005120
#define cAf6Reg_upen_geip1(transtype, pid, group16)                   (0x005120+(transtype)*16+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_geip1
BitField Type: RW
BitField Desc: uni/mul ip 1 for each port ipv4:
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_geip1_cfg_geip1_Mask_01                                                             cBit31_0
#define cAf6_upen_geip1_cfg_geip1_Shift_01                                                                   0
#define cAf6_upen_geip1_cfg_geip1_Mask_02                                                             cBit31_0
#define cAf6_upen_geip1_cfg_geip1_Shift_02                                                                   0
#define cAf6_upen_geip1_cfg_geip1_Mask_03                                                             cBit31_0
#define cAf6_upen_geip1_cfg_geip1_Shift_03                                                                   0
#define cAf6_upen_geip1_cfg_geip1_Mask_04                                                             cBit31_0
#define cAf6_upen_geip1_cfg_geip1_Shift_04                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Config GE unicast/multicast IPv4/IPv6 address 2
Reg Addr   : 0x00_5140-0x00_515F
Reg Formula: 0x00_5140 + $transtype*16 + $pid + $group16*65536
    Where  : 
           + $transtype(0-1)
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config GE uni/mul IP 2 for each port
transtype = 0 : unicast
transtype = 1 : multicast
HDL_PATH     : inscorepi.insgeip2.membuf.ram.ram[$transtype*16 + $pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_geip2_Base                                                                       0x005140
#define cAf6Reg_upen_geip2(transtype, pid, group16)                   (0x005140+(transtype)*16+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_geip2
BitField Type: RW
BitField Desc: uni/mul ip 2 for each port ipv4:
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_geip2_cfg_geip2_Mask_01                                                             cBit31_0
#define cAf6_upen_geip2_cfg_geip2_Shift_01                                                                   0
#define cAf6_upen_geip2_cfg_geip2_Mask_02                                                             cBit31_0
#define cAf6_upen_geip2_cfg_geip2_Shift_02                                                                   0
#define cAf6_upen_geip2_cfg_geip2_Mask_03                                                             cBit31_0
#define cAf6_upen_geip2_cfg_geip2_Shift_03                                                                   0
#define cAf6_upen_geip2_cfg_geip2_Mask_04                                                             cBit31_0
#define cAf6_upen_geip2_cfg_geip2_Shift_04                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Config GE unicast/multicast IPv4/IPv6 address 3
Reg Addr   : 0x00_5160-0x00_517F
Reg Formula: 0x00_5160 + $transtype*16 + $pid + $group16*65536
    Where  : 
           + $transtype(0-1)
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config GE uni/mul IP 3 for each port
transtype = 0 : unicast
transtype = 1 : multicast
HDL_PATH     : inscorepi.insgeip3.membuf.ram.ram[$transtype*16 + $pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_geip3_Base                                                                       0x005160
#define cAf6Reg_upen_geip3(transtype, pid, group16)                   (0x005160+(transtype)*16+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_geip3
BitField Type: RW
BitField Desc: uni/mul ip 3 for each port ipv4:
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_geip3_cfg_geip3_Mask_01                                                             cBit31_0
#define cAf6_upen_geip3_cfg_geip3_Shift_01                                                                   0
#define cAf6_upen_geip3_cfg_geip3_Mask_02                                                             cBit31_0
#define cAf6_upen_geip3_cfg_geip3_Shift_02                                                                   0
#define cAf6_upen_geip3_cfg_geip3_Mask_03                                                             cBit31_0
#define cAf6_upen_geip3_cfg_geip3_Shift_03                                                                   0
#define cAf6_upen_geip3_cfg_geip3_Mask_04                                                             cBit31_0
#define cAf6_upen_geip3_cfg_geip3_Shift_04                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Config GE unicast/multicast IPv4/IPv6 address 4
Reg Addr   : 0x00_5180-0x00_519F
Reg Formula: 0x00_5180 + $transtype*16 + $pid + $group16*65536
    Where  : 
           + $transtype(0-1)
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config GE uni/mul IP 4 for each port
transtype = 0 : unicast
transtype = 1 : multicast
HDL_PATH     : inscorepi.insgeip4.membuf.ram.ram[$transtype*16 + $pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_geip4_Base                                                                       0x005180
#define cAf6Reg_upen_geip4(transtype, pid, group16)                   (0x005180+(transtype)*16+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_geip4
BitField Type: RW
BitField Desc: uni/mul ip 4 for each port ipv4:
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_geip4_cfg_geip4_Mask_01                                                             cBit31_0
#define cAf6_upen_geip4_cfg_geip4_Shift_01                                                                   0
#define cAf6_upen_geip4_cfg_geip4_Mask_02                                                             cBit31_0
#define cAf6_upen_geip4_cfg_geip4_Shift_02                                                                   0
#define cAf6_upen_geip4_cfg_geip4_Mask_03                                                             cBit31_0
#define cAf6_upen_geip4_cfg_geip4_Shift_03                                                                   0
#define cAf6_upen_geip4_cfg_geip4_Mask_04                                                             cBit31_0
#define cAf6_upen_geip4_cfg_geip4_Shift_04                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : cfg ptp protocol
Reg Addr   : 0x00_51E0-0x00_51EF
Reg Formula: 0x00_51E0 + $pid + $group16*65536
    Where  : 
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config protocol for each port at BC mode in PTP service

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptl_Base                                                                         0x0051E0
#define cAf6Reg_upen_ptl(pid, group16)                                        (0x0051E0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_ptl
BitField Type: RW
BitField Desc: protocol value: '000': L2 '001': ipv4 '010': ipv6 '011': ipv4 vpn
'100': ipv6 vpn
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_ptl_cfg_ptl_Mask                                                                     cBit2_0
#define cAf6_upen_ptl_cfg_ptl_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Config GE port ipv4/ipv6 matching enable
Reg Addr   : 0x00_51F0-0x00_51FF
Reg Formula: 0x00_51F0 + $pid + $group16*65536
    Where  : 
           + $pid(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
config IPv4/IPv6 matching for each GE port
HDL_PATH     : inscorepi.insptl.membuf.ram.ram[$pid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_geipen_Base                                                                      0x0051F0
#define cAf6Reg_upen_geipen(pid, group16)                                     (0x0051F0+(pid)+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_geipen
BitField Type: RW
BitField Desc: config ip matching for each GE port [6]: ena/dis mul ip global 2
matching [5]: ena/dis mul ip global 1 matching [4]: ena/dis mul ip 1 matching
[3]: ena/dis uni ip 4 matching [2]: ena/dis uni ip 3 matching [1]: ena/dis uni
ip 2 matching [0]: ena/dis uni ip 1 matching
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_upen_geipen_cfg_geipen_Mask                                                               cBit6_0
#define cAf6_upen_geipen_cfg_geipen_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : eth2cla sticky
Reg Addr   : 0x00_4100
Reg Formula: 0x00_4100 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of eth2cla interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth2cla_stk_Base                                                                 0x004100
#define cAf6Reg_upen_eth2cla_stk(group16)                                           (0x004100+(group16)*65536)

/*--------------------------------------
BitField Name: ethn_ptch
BitField Type: W1C
BitField Desc: ETH #n ptch sticky
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_ethn_ptch_Mask                                                          cBit15_8
#define cAf6_upen_eth2cla_stk_ethn_ptch_Shift                                                                8

/*--------------------------------------
BitField Name: eth0_ptch
BitField Type: W1C
BitField Desc: ETH_0 PTCH : 0: clear 1: set
BitField Bits: [36:36]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_ptch_Mask                                                             cBit4
#define cAf6_upen_eth2cla_stk_eth0_ptch_Shift                                                                4

/*--------------------------------------
BitField Name: ethn_stk
BitField Type: W1C
BitField Desc: ETH_n valid, sop, eop, err sticky
BitField Bits: [35:04]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_ethn_stk_Mask_01                                                        cBit31_4
#define cAf6_upen_eth2cla_stk_ethn_stk_Shift_01                                                              4
#define cAf6_upen_eth2cla_stk_ethn_stk_Mask_02                                                         cBit3_0
#define cAf6_upen_eth2cla_stk_ethn_stk_Shift_02                                                              0

/*--------------------------------------
BitField Name: eth0_err
BitField Type: W1C
BitField Desc: ETH_0 ERR  : 0: clear  1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_err_Mask                                                              cBit3
#define cAf6_upen_eth2cla_stk_eth0_err_Shift                                                                 3

/*--------------------------------------
BitField Name: eth0_eop
BitField Type: W1C
BitField Desc: ETH_0 EOP  : 0: clear  1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_eop_Mask                                                              cBit2
#define cAf6_upen_eth2cla_stk_eth0_eop_Shift                                                                 2

/*--------------------------------------
BitField Name: eth0_sop
BitField Type: W1C
BitField Desc: ETH_0 SOP  : 0: clear  1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_sop_Mask                                                              cBit1
#define cAf6_upen_eth2cla_stk_eth0_sop_Shift                                                                 1

/*--------------------------------------
BitField Name: eth0_vld
BitField Type: W1C
BitField Desc: ETH_0 VALID: 0: clear  1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_eth2cla_stk_eth0_vld_Mask                                                              cBit0
#define cAf6_upen_eth2cla_stk_eth0_vld_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : cla2epa sticky
Reg Addr   : 0x00_4101
Reg Formula: 0x00_4101 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of cla2epa interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla2epa_stk_Base                                                                 0x004101
#define cAf6Reg_upen_cla2epa_stk(group16)                                           (0x004101+(group16)*65536)

/*--------------------------------------
BitField Name: cla2epa_stk
BitField Type: W1C
BitField Desc: sticky cla to epa
BitField Bits: [71:00]
--------------------------------------*/
#define cAf6_upen_cla2epa_stk_cla2epa_stk_Mask_01                                                     cBit31_0
#define cAf6_upen_cla2epa_stk_cla2epa_stk_Shift_01                                                           0
#define cAf6_upen_cla2epa_stk_cla2epa_stk_Mask_02                                                     cBit31_0
#define cAf6_upen_cla2epa_stk_cla2epa_stk_Shift_02                                                           0
#define cAf6_upen_cla2epa_stk_cla2epa_stk_Mask_03                                                      cBit7_0
#define cAf6_upen_cla2epa_stk_cla2epa_stk_Shift_03                                                           0


/*------------------------------------------------------------------------------
Reg Name   : cla2ptp sticky
Reg Addr   : 0x00_4102
Reg Formula: 0x00_4102 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of cla2ptp interface

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla2ptp_stk_Base                                                                 0x004102
#define cAf6Reg_upen_cla2ptp_stk(group16)                                           (0x004102+(group16)*65536)

/*--------------------------------------
BitField Name: cla2ptp_stk
BitField Type: W1C
BitField Desc: sticky cla to ptp
BitField Bits: [71:00]
--------------------------------------*/
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk_Mask_01                                                     cBit31_0
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk_Shift_01                                                           0
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk_Mask_02                                                     cBit31_0
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk_Shift_02                                                           0
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk_Mask_03                                                      cBit7_0
#define cAf6_upen_cla2ptp_stk_cla2ptp_stk_Shift_03                                                           0


/*------------------------------------------------------------------------------
Reg Name   : parser sticky 0
Reg Addr   : 0x00_4104
Reg Formula: 0x00_4104 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky 0  of parser

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_parser_stk0_Base                                                                 0x004104
#define cAf6Reg_upen_parser_stk0(group16)                                           (0x004104+(group16)*65536)

/*--------------------------------------
BitField Name: parser stk0
BitField Type: W1C
BitField Desc: sticky parser
BitField Bits: [71:06]
--------------------------------------*/
#define cAf6_upen_parser_stk0_parser_stk0_Mask_01                                                     cBit31_6
#define cAf6_upen_parser_stk0_parser_stk0_Shift_01                                                           6
#define cAf6_upen_parser_stk0_parser_stk0_Mask_02                                                     cBit31_0
#define cAf6_upen_parser_stk0_parser_stk0_Shift_02                                                           0
#define cAf6_upen_parser_stk0_parser_stk0_Mask_03                                                      cBit7_0
#define cAf6_upen_parser_stk0_parser_stk0_Shift_03                                                           0

/*--------------------------------------
BitField Name: par0_free
BitField Type: W1C
BitField Desc: PARSER_0 FREE BLK : 1: clear , 0: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_free_Mask                                                             cBit5
#define cAf6_upen_parser_stk0_par0_free_Shift                                                                5

/*--------------------------------------
BitField Name: par0_eoseg
BitField Type: W1C
BitField Desc: PARSER_0 EOSEG    : 1: clear , 0: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_eoseg_Mask                                                            cBit4
#define cAf6_upen_parser_stk0_par0_eoseg_Shift                                                               4

/*--------------------------------------
BitField Name: par0_blkemp
BitField Type: W1C
BitField Desc: PARSER_0 BLK EMPTY: 1: clear , 0: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_blkemp_Mask                                                           cBit3
#define cAf6_upen_parser_stk0_par0_blkemp_Shift                                                              3

/*--------------------------------------
BitField Name: par0_errmru
BitField Type: W1C
BitField Desc: PARSER_0 MRU ERROR: 1: clear , 0: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_errmru_Mask                                                           cBit2
#define cAf6_upen_parser_stk0_par0_errmru_Shift                                                              2

/*--------------------------------------
BitField Name: par0_err
BitField Type: W1C
BitField Desc: PARSER_0 PKT SHORT: 1: clear , 0: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_err_Mask                                                              cBit1
#define cAf6_upen_parser_stk0_par0_err_Shift                                                                 1

/*--------------------------------------
BitField Name: par0_done
BitField Type: W1C
BitField Desc: PARSER_0 DONE:      1: clear , 0: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_parser_stk0_par0_done_Mask                                                             cBit0
#define cAf6_upen_parser_stk0_par0_done_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : parser sticky 1
Reg Addr   : 0x00_4105
Reg Formula: 0x00_4105 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky 1  of parser

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_parser_stk1_Base                                                                 0x004105
#define cAf6Reg_upen_parser_stk1(group16)                                           (0x004105+(group16)*65536)

/*--------------------------------------
BitField Name: parser stk1
BitField Type: W1C
BitField Desc: sticky parser
BitField Bits: [107:12]
--------------------------------------*/
#define cAf6_upen_parser_stk1_parser_stk1_Mask_01                                                    cBit31_12
#define cAf6_upen_parser_stk1_parser_stk1_Shift_01                                                          12
#define cAf6_upen_parser_stk1_parser_stk1_Mask_02                                                     cBit31_0
#define cAf6_upen_parser_stk1_parser_stk1_Shift_02                                                           0
#define cAf6_upen_parser_stk1_parser_stk1_Mask_03                                                     cBit11_0
#define cAf6_upen_parser_stk1_parser_stk1_Shift_03                                                           0

/*--------------------------------------
BitField Name: par0_udpprt
BitField Type: W1C
BitField Desc: PARSER_0 UDP PRT  : 0: clear, 1: set
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_udpprt_Mask                                                          cBit11
#define cAf6_upen_parser_stk1_par0_udpprt_Shift                                                             11

/*--------------------------------------
BitField Name: par0_ipda
BitField Type: W1C
BitField Desc: PARSER_0 IP DA    : 0: clear, 1: set
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ipda_Mask                                                            cBit10
#define cAf6_upen_parser_stk1_par0_ipda_Shift                                                               10

/*--------------------------------------
BitField Name: par0_ipsa
BitField Type: W1C
BitField Desc: PARSER_0 IP SA    : 0: clear, 1: set
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ipsa_Mask                                                             cBit9
#define cAf6_upen_parser_stk1_par0_ipsa_Shift                                                                9

/*--------------------------------------
BitField Name: par0_udpptl
BitField Type: W1C
BitField Desc: PARSER_0 UDP PTL  : 0: clear, 1: set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_udpptl_Mask                                                           cBit8
#define cAf6_upen_parser_stk1_par0_udpptl_Shift                                                              8

/*--------------------------------------
BitField Name: par0_ipver
BitField Type: W1C
BitField Desc: PARSER_0 IPVER  : 0: clear, 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ipver_Mask                                                            cBit7
#define cAf6_upen_parser_stk1_par0_ipver_Shift                                                               7

/*--------------------------------------
BitField Name: par0_mac
BitField Type: W1C
BitField Desc: PARSER_0 MAC    : 0: clear, 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_mac_Mask                                                              cBit6
#define cAf6_upen_parser_stk1_par0_mac_Shift                                                                 6

/*--------------------------------------
BitField Name: par0_mefecid
BitField Type: W1C
BitField Desc: PARSER_0 MEFECID: 0: clear, 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_mefecid_Mask                                                          cBit5
#define cAf6_upen_parser_stk1_par0_mefecid_Shift                                                             5

/*--------------------------------------
BitField Name: par0_pwlabel
BitField Type: W1C
BitField Desc: PARSER_0 PWLABEL: 0: clear, 1: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_pwlabel_Mask                                                          cBit4
#define cAf6_upen_parser_stk1_par0_pwlabel_Shift                                                             4

/*--------------------------------------
BitField Name: par0_ethtyp
BitField Type: W1C
BitField Desc: PARSER_0 ETHTYPE: 0: clear, 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ethtyp_Mask                                                           cBit3
#define cAf6_upen_parser_stk1_par0_ethtyp_Shift                                                              3

/*--------------------------------------
BitField Name: par0_sndvlan
BitField Type: W1C
BitField Desc: PARSER_0 SNDVLAN: 0: clear, 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_sndvlan_Mask                                                          cBit2
#define cAf6_upen_parser_stk1_par0_sndvlan_Shift                                                             2

/*--------------------------------------
BitField Name: par0_fstvlan
BitField Type: W1C
BitField Desc: PARSER_0 FSTVLAN: 0: clear, 1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_fstvlan_Mask                                                          cBit1
#define cAf6_upen_parser_stk1_par0_fstvlan_Shift                                                             1

/*--------------------------------------
BitField Name: par0_ptch
BitField Type: W1C
BitField Desc: PARSER_0 PTCH   : 0: clear, 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_parser_stk1_par0_ptch_Mask                                                             cBit0
#define cAf6_upen_parser_stk1_par0_ptch_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : lookup sticky
Reg Addr   : 0x00_4106
Reg Formula: 0x00_4106 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of lookup

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lkup_stk_Base                                                                    0x004106
#define cAf6Reg_upen_lkup_stk(group16)                                              (0x004106+(group16)*65536)

/*--------------------------------------
BitField Name: porttab_outen
BitField Type: W1C
BitField Desc: PORT    TAB OUT_EN: clear 1: set
BitField Bits: [86:86]
--------------------------------------*/
#define cAf6_upen_lkup_stk_porttab_outen_Mask                                                           cBit22
#define cAf6_upen_lkup_stk_porttab_outen_Shift                                                              22

/*--------------------------------------
BitField Name: ptchtab_outen
BitField Type: W1C
BitField Desc: PTCH    TAB OUT_EN: clear 1: set
BitField Bits: [85:85]
--------------------------------------*/
#define cAf6_upen_lkup_stk_ptchtab_outen_Mask                                                           cBit21
#define cAf6_upen_lkup_stk_ptchtab_outen_Shift                                                              21

/*--------------------------------------
BitField Name: fvlntab_outen
BitField Type: W1C
BitField Desc: FSTVLAN TAB OUT_EN: clear 1: set
BitField Bits: [84:84]
--------------------------------------*/
#define cAf6_upen_lkup_stk_fvlntab_outen_Mask                                                           cBit20
#define cAf6_upen_lkup_stk_fvlntab_outen_Shift                                                              20

/*--------------------------------------
BitField Name: svlntab_outen
BitField Type: W1C
BitField Desc: SNDVLAN TAB OUT_EN: clear 1: set
BitField Bits: [83:83]
--------------------------------------*/
#define cAf6_upen_lkup_stk_svlntab_outen_Mask                                                           cBit19
#define cAf6_upen_lkup_stk_svlntab_outen_Shift                                                              19

/*--------------------------------------
BitField Name: pwhash_outen
BitField Type: W1C
BitField Desc: PWHASH  TAB OUT_EN: clear 1: set
BitField Bits: [82:82]
--------------------------------------*/
#define cAf6_upen_lkup_stk_pwhash_outen_Mask                                                            cBit18
#define cAf6_upen_lkup_stk_pwhash_outen_Shift                                                               18

/*--------------------------------------
BitField Name: rulechk_outen
BitField Type: W1C
BitField Desc: RULECHK TAB OUT_EN: clear 1: set
BitField Bits: [80:80]
--------------------------------------*/
#define cAf6_upen_lkup_stk_rulechk_outen_Mask                                                           cBit16
#define cAf6_upen_lkup_stk_rulechk_outen_Shift                                                              16

/*--------------------------------------
BitField Name: porttab_win
BitField Type: W1C
BitField Desc: PORT    TABLE WIN: 0: clear 1: set
BitField Bits: [78:78]
--------------------------------------*/
#define cAf6_upen_lkup_stk_porttab_win_Mask                                                             cBit14
#define cAf6_upen_lkup_stk_porttab_win_Shift                                                                14

/*--------------------------------------
BitField Name: ptchtab_win
BitField Type: W1C
BitField Desc: PTCH    TABLE WIN: 0: clear 1: set
BitField Bits: [77:77]
--------------------------------------*/
#define cAf6_upen_lkup_stk_ptchtab_win_Mask                                                             cBit13
#define cAf6_upen_lkup_stk_ptchtab_win_Shift                                                                13

/*--------------------------------------
BitField Name: fvlntab_win
BitField Type: W1C
BitField Desc: FSTVLAN TABLE WIN: 0: clear 1: set
BitField Bits: [76:76]
--------------------------------------*/
#define cAf6_upen_lkup_stk_fvlntab_win_Mask                                                             cBit12
#define cAf6_upen_lkup_stk_fvlntab_win_Shift                                                                12

/*--------------------------------------
BitField Name: svlntab_win
BitField Type: W1C
BitField Desc: SNDVLAN TABLE WIN: 0: clear 1: set
BitField Bits: [75:75]
--------------------------------------*/
#define cAf6_upen_lkup_stk_svlntab_win_Mask                                                             cBit11
#define cAf6_upen_lkup_stk_svlntab_win_Shift                                                                11

/*--------------------------------------
BitField Name: pwhash_win
BitField Type: W1C
BitField Desc: PWHASH  TABLE WIN: 0: clear 1: set
BitField Bits: [74:74]
--------------------------------------*/
#define cAf6_upen_lkup_stk_pwhash_win_Mask                                                              cBit10
#define cAf6_upen_lkup_stk_pwhash_win_Shift                                                                 10

/*--------------------------------------
BitField Name: etypcam_win
BitField Type: W1C
BitField Desc: ETHTYPE TABLE WIN: 0: clear 1: set
BitField Bits: [73:73]
--------------------------------------*/
#define cAf6_upen_lkup_stk_etypcam_win_Mask                                                              cBit9
#define cAf6_upen_lkup_stk_etypcam_win_Shift                                                                 9

/*--------------------------------------
BitField Name: rulechk_win
BitField Type: W1C
BitField Desc: RULECHK TABLE WIN: 0: clear 1: set
BitField Bits: [72:72]
--------------------------------------*/
#define cAf6_upen_lkup_stk_rulechk_win_Mask                                                              cBit8
#define cAf6_upen_lkup_stk_rulechk_win_Shift                                                                 8

/*--------------------------------------
BitField Name: lkup0_stk1
BitField Type: W1C
BitField Desc: LOOKUP_0 sticky 1
BitField Bits: [71:08]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_stk1_Mask_01                                                         cBit31_8
#define cAf6_upen_lkup_stk_lkup0_stk1_Shift_01                                                               8
#define cAf6_upen_lkup_stk_lkup0_stk1_Mask_02                                                          cBit7_0
#define cAf6_upen_lkup_stk_lkup0_stk1_Shift_02                                                               0

/*--------------------------------------
BitField Name: lkup0_ptpind
BitField Type: W1C
BitField Desc: LOOKUP_0 PTP IND   : 0: clear 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_ptpind_Mask                                                             cBit7
#define cAf6_upen_lkup_stk_lkup0_ptpind_Shift                                                                7

/*--------------------------------------
BitField Name: lkup0_udpind
BitField Type: W1C
BitField Desc: LOOKUP_0 UDP IND   : 0: clear 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_udpind_Mask                                                             cBit6
#define cAf6_upen_lkup_stk_lkup0_udpind_Shift                                                                6

/*--------------------------------------
BitField Name: lkup0_eoseg
BitField Type: W1C
BitField Desc: LOOKUP_0 EOSEG     : 0: clear 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_eoseg_Mask                                                              cBit5
#define cAf6_upen_lkup_stk_lkup0_eoseg_Shift                                                                 5

/*--------------------------------------
BitField Name: lkup0_blkemp
BitField Type: W1C
BitField Desc: LOOKUP_0 BLK EMPTY : 0: clear 1: set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_blkemp_Mask                                                             cBit4
#define cAf6_upen_lkup_stk_lkup0_blkemp_Shift                                                                4

/*--------------------------------------
BitField Name: lkup0_errmru
BitField Type: W1C
BitField Desc: LOOKUP_0 MRU ERR: 0: clear 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_errmru_Mask                                                             cBit3
#define cAf6_upen_lkup_stk_lkup0_errmru_Shift                                                                3

/*--------------------------------------
BitField Name: lkup0_fail
BitField Type: W1C
BitField Desc: LOOKUP_0 FAIL   : 0: clear 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_fail_Mask                                                               cBit2
#define cAf6_upen_lkup_stk_lkup0_fail_Shift                                                                  2

/*--------------------------------------
BitField Name: lkup0_stk0
BitField Type: W1C
BitField Desc: LOOKUP_0 sticky 0
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_stk0_Mask                                                               cBit1
#define cAf6_upen_lkup_stk_lkup0_stk0_Shift                                                                  1

/*--------------------------------------
BitField Name: lkup0_done
BitField Type: W1C
BitField Desc: LOOKUP_0 DONE   : 0: clear 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_lkup_stk_lkup0_done_Mask                                                               cBit0
#define cAf6_upen_lkup_stk_lkup0_done_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : datashift sticky
Reg Addr   : 0x00_4107
Reg Formula: 0x00_4107 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of datashift

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_datashidt_stk_Base                                                               0x004107
#define cAf6Reg_upen_datashidt_stk(group16)                                         (0x004107+(group16)*65536)

/*--------------------------------------
BitField Name: datsh0_ssercpu
BitField Type: W1C
BitField Desc: DATSHF_0 SSER CPU : 0: clear 1: set
BitField Bits: [43:43]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_ssercpu_Mask                                                     cBit11
#define cAf6_upen_datashidt_stk_datsh0_ssercpu_Shift                                                        11

/*--------------------------------------
BitField Name: datsh0_sseroam
BitField Type: W1C
BitField Desc: DATSHF_0 SSER OAM : 0: clear 1: set
BitField Bits: [42:42]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_sseroam_Mask                                                     cBit10
#define cAf6_upen_datashidt_stk_datsh0_sseroam_Shift                                                        10

/*--------------------------------------
BitField Name: datsh0_sserdata
BitField Type: W1C
BitField Desc: DATSHF_0 SSER DATA: 0: clear 1: set
BitField Bits: [41:41]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_sserdata_Mask                                                     cBit9
#define cAf6_upen_datashidt_stk_datsh0_sserdata_Shift                                                        9

/*--------------------------------------
BitField Name: datsh0_sserpw
BitField Type: W1C
BitField Desc: DATSHF_0 SSER PW  : 0: clear 1: set
BitField Bits: [40:40]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_sserpw_Mask                                                       cBit8
#define cAf6_upen_datashidt_stk_datsh0_sserpw_Shift                                                          8

/*--------------------------------------
BitField Name: datsh0_serptp
BitField Type: W1C
BitField Desc: DATSHF_0 SER PTP   : 0: clear 1: set
BitField Bits: [37:37]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_serptp_Mask                                                       cBit5
#define cAf6_upen_datashidt_stk_datsh0_serptp_Shift                                                          5

/*--------------------------------------
BitField Name: datsh0_serepass
BitField Type: W1C
BitField Desc: DATSHF_0 SER EPASS : 0: clear 1: set
BitField Bits: [36:36]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_serepass_Mask                                                     cBit4
#define cAf6_upen_datashidt_stk_datsh0_serepass_Shift                                                        4

/*--------------------------------------
BitField Name: datsh0_serdcc
BitField Type: W1C
BitField Desc: DATSHF_0 SER DCC   : 0: clear 1: set
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_serdcc_Mask                                                       cBit3
#define cAf6_upen_datashidt_stk_datsh0_serdcc_Shift                                                          3

/*--------------------------------------
BitField Name: datsh0_serimsg
BitField Type: W1C
BitField Desc: DATSHF_0 SER IMSG  : 0: clear 1: set
BitField Bits: [34:34]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_serimsg_Mask                                                      cBit2
#define cAf6_upen_datashidt_stk_datsh0_serimsg_Shift                                                         2

/*--------------------------------------
BitField Name: datsh0_sercem
BitField Type: W1C
BitField Desc: DATSHF_0 SER CEM   : 0: clear 1: set
BitField Bits: [33:33]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_sercem_Mask                                                       cBit1
#define cAf6_upen_datashidt_stk_datsh0_sercem_Shift                                                          1

/*--------------------------------------
BitField Name: datsh0_serdrop
BitField Type: W1C
BitField Desc: DATSHF_0 SER DROP  : 0: clear 1: set
BitField Bits: [32:32]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_serdrop_Mask                                                   cBit32_0
#define cAf6_upen_datashidt_stk_datsh0_serdrop_Shift                                                         0

/*--------------------------------------
BitField Name: datsh0_mef
BitField Type: W1C
BitField Desc: DATSHF_0 MEF      : 0: clear 1: set
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_mef_Mask                                                         cBit29
#define cAf6_upen_datashidt_stk_datsh0_mef_Shift                                                            29

/*--------------------------------------
BitField Name: datsh0_oam
BitField Type: W1C
BitField Desc: DATSHF_0 OAM      : 0: clear 1: set
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_oam_Mask                                                         cBit28
#define cAf6_upen_datashidt_stk_datsh0_oam_Shift                                                            28

/*--------------------------------------
BitField Name: datsh0_mmpls
BitField Type: W1C
BitField Desc: DATSHF_0 MPLS MUL : 0: clear 1: set
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_mmpls_Mask                                                       cBit27
#define cAf6_upen_datashidt_stk_datsh0_mmpls_Shift                                                          27

/*--------------------------------------
BitField Name: datsh0_umpls
BitField Type: W1C
BitField Desc: DATSHF_0 MPLS UNI : 0: clear 1: set
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_umpls_Mask                                                       cBit26
#define cAf6_upen_datashidt_stk_datsh0_umpls_Shift                                                          26

/*--------------------------------------
BitField Name: datsh0_ipv6
BitField Type: W1C
BitField Desc: DATSHF_0 IPv6     : 0: clear 1: set
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_ipv6_Mask                                                        cBit25
#define cAf6_upen_datashidt_stk_datsh0_ipv6_Shift                                                           25

/*--------------------------------------
BitField Name: datsh0_ipv4
BitField Type: W1C
BitField Desc: DATSHF_0 IPv4     : 0: clear 1: set
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_ipv4_Mask                                                        cBit24
#define cAf6_upen_datashidt_stk_datsh0_ipv4_Shift                                                           24

/*--------------------------------------
BitField Name: datsh0_4label
BitField Type: W1C
BitField Desc: DATSHF_0 4LABEL : 0: clear 1: set
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_4label_Mask                                                      cBit20
#define cAf6_upen_datashidt_stk_datsh0_4label_Shift                                                         20

/*--------------------------------------
BitField Name: datsh0_3label
BitField Type: W1C
BitField Desc: DATSHF_0 3LABEL : 0: clear 1: set
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_3label_Mask                                                      cBit19
#define cAf6_upen_datashidt_stk_datsh0_3label_Shift                                                         19

/*--------------------------------------
BitField Name: datsh0_2label
BitField Type: W1C
BitField Desc: DATSHF_0 2LABEL : 0: clear 1: set
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_2label_Mask                                                      cBit18
#define cAf6_upen_datashidt_stk_datsh0_2label_Shift                                                         18

/*--------------------------------------
BitField Name: datsh0_1label
BitField Type: W1C
BitField Desc: DATSHF_0 1LABEL : 0: clear 1: set
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_1label_Mask                                                      cBit17
#define cAf6_upen_datashidt_stk_datsh0_1label_Shift                                                         17

/*--------------------------------------
BitField Name: datsh0_0label
BitField Type: W1C
BitField Desc: DATSHF_0 0LABEL : 0: clear 1: set
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_0label_Mask                                                      cBit16
#define cAf6_upen_datashidt_stk_datsh0_0label_Shift                                                         16

/*--------------------------------------
BitField Name: datsh0_2vlan
BitField Type: W1C
BitField Desc: DATSHF_0 2VLAN : 0: clear 1: set
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_2vlan_Mask                                                       cBit14
#define cAf6_upen_datashidt_stk_datsh0_2vlan_Shift                                                          14

/*--------------------------------------
BitField Name: datsh0_1vlan
BitField Type: W1C
BitField Desc: DATSHF_0 1VLAN : 0: clear 1: set
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_1vlan_Mask                                                       cBit13
#define cAf6_upen_datashidt_stk_datsh0_1vlan_Shift                                                          13

/*--------------------------------------
BitField Name: datsh0_0vlan
BitField Type: W1C
BitField Desc: DATSHF_0 0VLAN : 0: clear 1: set
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_0vlan_Mask                                                       cBit12
#define cAf6_upen_datashidt_stk_datsh0_0vlan_Shift                                                          12

/*--------------------------------------
BitField Name: datsh0_ptpind
BitField Type: W1C
BitField Desc: DATSHF_0 PTP  IND : 0: clear 1: set
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_ptpind_Mask                                                      cBit10
#define cAf6_upen_datashidt_stk_datsh0_ptpind_Shift                                                         10

/*--------------------------------------
BitField Name: datsh0_udpind
BitField Type: W1C
BitField Desc: DATSHF_0 UDP  IND : 0: clear 1: set
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_udpind_Mask                                                       cBit9
#define cAf6_upen_datashidt_stk_datsh0_udpind_Shift                                                          9

/*--------------------------------------
BitField Name: datsh0_lkwin
BitField Type: W1C
BitField Desc: DATSHF_0 LKUP WIN : 0: clear 1: set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_lkwin_Mask                                                        cBit8
#define cAf6_upen_datashidt_stk_datsh0_lkwin_Shift                                                           8

/*--------------------------------------
BitField Name: datsh0_err4
BitField Type: W1C
BitField Desc: DATSHF_0 MRU ERROR: 0: clear 1: set
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_err4_Mask                                                         cBit7
#define cAf6_upen_datashidt_stk_datsh0_err4_Shift                                                            7

/*--------------------------------------
BitField Name: datsh0_err3
BitField Type: W1C
BitField Desc: DATSHF_0 BLK EMPTY: 0: clear 1: set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_err3_Mask                                                         cBit6
#define cAf6_upen_datashidt_stk_datsh0_err3_Shift                                                            6

/*--------------------------------------
BitField Name: datsh0_err2
BitField Type: W1C
BitField Desc: DATSHF_0 LKUP FAIL: 0: clear 1: set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_err2_Mask                                                         cBit5
#define cAf6_upen_datashidt_stk_datsh0_err2_Shift                                                            5

/*--------------------------------------
BitField Name: datsh0_err0
BitField Type: W1C
BitField Desc: DATSHF_0 ETH ERROR: 0: clear 1: set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_err0_Mask                                                         cBit3
#define cAf6_upen_datashidt_stk_datsh0_err0_Shift                                                            3

/*--------------------------------------
BitField Name: datsh0_eop
BitField Type: W1C
BitField Desc: DATSHF_0 EOP      : 0: clear 1: set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_eop_Mask                                                          cBit2
#define cAf6_upen_datashidt_stk_datsh0_eop_Shift                                                             2

/*--------------------------------------
BitField Name: datsh0_sop
BitField Type: W1C
BitField Desc: DATSHF_0 SOP      : 0: clear 1: set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_sop_Mask                                                          cBit1
#define cAf6_upen_datashidt_stk_datsh0_sop_Shift                                                             1

/*--------------------------------------
BitField Name: datsh0_vld
BitField Type: W1C
BitField Desc: DATSHF_0 VALID    : 0: clear 1: set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_datashidt_stk_datsh0_vld_Mask                                                          cBit0
#define cAf6_upen_datashidt_stk_datsh0_vld_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : allc sticky
Reg Addr   : 0x00_410A
Reg Formula: 0x00_410A + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
sticky of egress header

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_allc_stk_Base                                                                    0x00410A
#define cAf6Reg_upen_allc_stk(group16)                                              (0x00410A+(group16)*65536)

/*--------------------------------------
BitField Name: ffge11_full
BitField Type: W1C
BitField Desc: FIFO GE11 FULL: 0: clear 1:set
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge11_full_Mask                                                             cBit19
#define cAf6_upen_allc_stk_ffge11_full_Shift                                                                19

/*--------------------------------------
BitField Name: ffge10_full
BitField Type: W1C
BitField Desc: FIFO GE10 FULL: 0: clear 1:set
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge10_full_Mask                                                             cBit18
#define cAf6_upen_allc_stk_ffge10_full_Shift                                                                18

/*--------------------------------------
BitField Name: ffge9_full
BitField Type: W1C
BitField Desc: FIFO GE9  FULL: 0: clear 1:set
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge9_full_Mask                                                              cBit17
#define cAf6_upen_allc_stk_ffge9_full_Shift                                                                 17

/*--------------------------------------
BitField Name: ffge8_full
BitField Type: W1C
BitField Desc: FIFO GE8  FULL: 0: clear 1:set
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge8_full_Mask                                                              cBit16
#define cAf6_upen_allc_stk_ffge8_full_Shift                                                                 16

/*--------------------------------------
BitField Name: ffge7_full
BitField Type: W1C
BitField Desc: FIFO GE7  FULL: 0: clear 1:set
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge7_full_Mask                                                              cBit15
#define cAf6_upen_allc_stk_ffge7_full_Shift                                                                 15

/*--------------------------------------
BitField Name: ffge6_full
BitField Type: W1C
BitField Desc: FIFO GE6  FULL: 0: clear 1:set
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge6_full_Mask                                                              cBit14
#define cAf6_upen_allc_stk_ffge6_full_Shift                                                                 14

/*--------------------------------------
BitField Name: ffge5_full
BitField Type: W1C
BitField Desc: FIFO GE5  FULL: 0: clear 1:set
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge5_full_Mask                                                              cBit13
#define cAf6_upen_allc_stk_ffge5_full_Shift                                                                 13

/*--------------------------------------
BitField Name: ffge4_full
BitField Type: W1C
BitField Desc: FIFO GE4  FULL: 0: clear 1:set
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffge4_full_Mask                                                              cBit12
#define cAf6_upen_allc_stk_ffge4_full_Shift                                                                 12

/*--------------------------------------
BitField Name: ffxfi_full
BitField Type: W1C
BitField Desc: FIFO XFI FULL: 0: clear 1:set
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_allc_stk_ffxfi_full_Mask                                                               cBit8
#define cAf6_upen_allc_stk_ffxfi_full_Shift                                                                  8

/*--------------------------------------
BitField Name: blkreq
BitField Type: W1C
BitField Desc: BLK REQUEST: 0: clear 1:set
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_allc_stk_blkreq_Mask                                                                   cBit4
#define cAf6_upen_allc_stk_blkreq_Shift                                                                      4

/*--------------------------------------
BitField Name: buffre
BitField Type: W1C
BitField Desc: BUFFER FREE: 0: clear 1:set
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_allc_stk_buffre_Mask                                                                   cBit3
#define cAf6_upen_allc_stk_buffre_Shift                                                                      3

/*--------------------------------------
BitField Name: parfre
BitField Type: W1C
BitField Desc: PARSER FREE: 0: clear 1:set
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_allc_stk_parfre_Mask                                                                   cBit2
#define cAf6_upen_allc_stk_parfre_Shift                                                                      2

/*--------------------------------------
BitField Name: blkemp
BitField Type: W1C
BitField Desc: BLK EMPTY  : 0: clear 1:set
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_allc_stk_blkemp_Mask                                                                   cBit1
#define cAf6_upen_allc_stk_blkemp_Shift                                                                      1

/*--------------------------------------
BitField Name: samefree
BitField Type: W1C
BitField Desc: SAME FREE  : 0: clear 1:set
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_allc_stk_samefree_Mask                                                                 cBit0
#define cAf6_upen_allc_stk_samefree_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : allc_blkfre_cnt
Reg Addr   : 0x00_4200
Reg Formula: 0x00_4200 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
allc block free counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_allc_blkfre_cnt_Base                                                             0x004200
#define cAf6Reg_upen_allc_blkfre_cnt(group16)                                       (0x004200+(group16)*65536)

/*--------------------------------------
BitField Name: allc_blkfre_cnt
BitField Type: RO
BitField Desc: block free counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_allc_blkfre_cnt_allc_blkfre_cnt_Mask                                                cBit31_0
#define cAf6_upen_allc_blkfre_cnt_allc_blkfre_cnt_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : allc_blkiss_cnt
Reg Addr   : 0x00_4201
Reg Formula: 0x00_4201 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
allc block issue counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_allc_blkiss_cnt_Base                                                             0x004201
#define cAf6Reg_upen_allc_blkiss_cnt(group16)                                       (0x004201+(group16)*65536)

/*--------------------------------------
BitField Name: allc_blkiss_cnt
BitField Type: RO
BitField Desc: block issue counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_allc_blkiss_cnt_allc_blkiss_cnt_Mask                                                cBit31_0
#define cAf6_upen_allc_blkiss_cnt_allc_blkiss_cnt_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : todinit_cfg
Reg Addr   : 0x2000
Reg Formula: 0x00_2000 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
init value tod

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_todinit_cfg_Base                                                                   0x2000
#define cAf6Reg_upen_todinit_cfg(group16)                                           (0x002000+(group16)*65536)

/*--------------------------------------
BitField Name: upen_todsecinit
BitField Type: RW
BitField Desc: cfg init sec
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_todinit_cfg_upen_todsecinit_Mask_01                                                 cBit31_0
#define cAf6_upen_todinit_cfg_upen_todsecinit_Shift_01                                                       0
#define cAf6_upen_todinit_cfg_upen_todsecinit_Mask_02                                                 cBit15_0
#define cAf6_upen_todinit_cfg_upen_todsecinit_Shift_02                                                       0


/*------------------------------------------------------------------------------
Reg Name   : tod_current
Reg Addr   : 0x2001
Reg Formula: 0x2001 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
current timer

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tod_cur_Base                                                                       0x2001
#define cAf6Reg_upen_tod_cur(group16)                                                 (0x2001+(group16)*65536)

/*--------------------------------------
BitField Name: cur_todsec
BitField Type: RO
BitField Desc: current second
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_tod_cur_cur_todsec_Mask_01                                                          cBit31_0
#define cAf6_upen_tod_cur_cur_todsec_Shift_01                                                                0
#define cAf6_upen_tod_cur_cur_todsec_Mask_02                                                          cBit15_0
#define cAf6_upen_tod_cur_cur_todsec_Shift_02                                                                0


/*------------------------------------------------------------------------------
Reg Name   : todmode_cfg
Reg Addr   : 0x2002
Reg Formula: 0x2002 + $group16*65536
    Where  : 
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
operaation ToD

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tod_mode_Base                                                                      0x2002
#define cAf6Reg_upen_tod_mode(group16)                                                (0x2002+(group16)*65536)

/*--------------------------------------
BitField Name: compensate_route_1pps
BitField Type: RW
BitField Desc: The ability to offset their internal time counter with respect
the 1PPS signal they receive. This is to compensate for the propagation delay of
the 1PPS signal in our system.
BitField Bits: [10:03]
--------------------------------------*/
#define cAf6_upen_tod_mode_compensate_route_1pps_Mask                                                 cBit10_3
#define cAf6_upen_tod_mode_compensate_route_1pps_Shift                                                       3

/*--------------------------------------
BitField Name: upen_todmode
BitField Type: RW
BitField Desc: select output tod "0" free-run, "1" external pps
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_tod_mode_upen_todmode_Mask                                                           cBit2_0
#define cAf6_upen_tod_mode_upen_todmode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : bp_latency_cfg
Reg Addr   : 0x2020 - 0x2021
Reg Formula: 0x2020 + $BackplanID
    Where  : 
           + $BackplanID(0-1)
Reg Desc   : 
This register configure latency adjust for tx/rx backplan serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_bp_latency_mode_Base                                                               0x2020
#define cAf6Reg_upen_bp_latency_mode(BackplanID)                                         (0x2020+(BackplanID))
#define cAf6Reg_upen_bp_latency_mode_WidthVal                                                               32

/*--------------------------------------
BitField Name: tx_latency_adj_bp
BitField Type: RW
BitField Desc: latency adjust from tx backplan MAC to serdes pin in nanosecond
BitField Bits: [21:11]
--------------------------------------*/
#define cAf6_upen_bp_latency_mode_tx_latency_adj_bp_Mask                                             cBit21_11
#define cAf6_upen_bp_latency_mode_tx_latency_adj_bp_Shift                                                   11

/*--------------------------------------
BitField Name: rx_latency_adj_bp
BitField Type: RW
BitField Desc: latency adjust from rx backplan serdes pin to MAC in nanosecond
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_bp_latency_mode_rx_latency_adj_bp_Mask                                              cBit10_0
#define cAf6_upen_bp_latency_mode_rx_latency_adj_bp_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : face_latency_cfg
Reg Addr   : 0x2040 - 0x204F
Reg Formula: 0x2040 + $FacePlateID + $group16*65536
    Where  : 
           + $FacePlateID(0-15)
           + $group16(0-1): group engine ID of 16 port face plate
Reg Desc   : 
This register configure latency adjust for tx/rx face plate serdes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_face_latency_mode_Base                                                             0x2040
#define cAf6Reg_upen_face_latency_mode(FacePlateID, group16)            (0x2040+(FacePlateID)+(group16)*65536)
#define cAf6Reg_upen_face_latency_mode_WidthVal                                                             32

/*--------------------------------------
BitField Name: tx_latency_adj_face
BitField Type: RW
BitField Desc: latency adjust from tx face plate MAC to serdes pin in nanosecond
calibrated value of TENGE is 0x82, calibrated value of GE is 0x198, calibrated
value of FX is 0x490
BitField Bits: [21:11]
--------------------------------------*/
#define cAf6_upen_face_latency_mode_tx_latency_adj_face_Mask                                         cBit21_11
#define cAf6_upen_face_latency_mode_tx_latency_adj_face_Shift                                               11

/*--------------------------------------
BitField Name: rx_latency_adj_face
BitField Type: RW
BitField Desc: latency adjust from rx face plate serdes pin to MAC in nanosecond
calibrated value of TENGE is 0x82, calibrated value of GE is 0x138, calibrated
value of FX is 0x581
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_face_latency_mode_rx_latency_adj_face_Mask                                          cBit10_0
#define cAf6_upen_face_latency_mode_rx_latency_adj_face_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 40G 1
Reg Addr   : 0x00_B020
Reg Formula: 
    Where  : 
Reg Desc   : 
This register hold value 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold_40g1                                                                        0x00B020

/*--------------------------------------
BitField Name: cfg_hold_40g1
BitField Type: RW
BitField Desc: hold 40g value 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold_40g1_cfg_hold_40g1_Mask                                                        cBit31_0
#define cAf6_upen_hold_40g1_cfg_hold_40g1_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 40G 2
Reg Addr   : 0x00_B021
Reg Formula: 
    Where  : 
Reg Desc   : 
This register hold value 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold_40g2                                                                        0x00B021

/*--------------------------------------
BitField Name: cfg_hold_40g2
BitField Type: RW
BitField Desc: hold 40g value 2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold_40g2_cfg_hold_40g2_Mask                                                        cBit31_0
#define cAf6_upen_hold_40g2_cfg_hold_40g2_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Hold Register 40G 3
Reg Addr   : 0x00_B022
Reg Formula: 
    Where  : 
Reg Desc   : 
This register hold value 3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hold_40g3                                                                        0x00B022

/*--------------------------------------
BitField Name: cfg_hold_40g3
BitField Type: RW
BitField Desc: hold 40g value 3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hold_40g3_cfg_hold_40g3_Mask                                                        cBit31_0
#define cAf6_upen_hold_40g3_cfg_hold_40g3_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : PTP enable
Reg Addr   : 0x00_8004
Reg Formula: 
    Where  : 
Reg Desc   : 
used to indicate 1-step/2-step mode for each port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp40_stmode                                                                     0x008004

/*--------------------------------------
BitField Name: cfg_ptp_stmode
BitField Type: RW
BitField Desc: '1' : 2-step, '0': 1-step
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ptp40_stmode_cfg_ptp_stmode_Mask                                                    cBit31_0
#define cAf6_upen_ptp40_stmode_cfg_ptp_stmode_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PTP enable
Reg Addr   : 0x00_8005
Reg Formula: 
    Where  : 
Reg Desc   : 
used to indicate master/slaver mode for each port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp40_ms                                                                         0x008005

/*--------------------------------------
BitField Name: cfg_ptp_ms
BitField Type: RW
BitField Desc: '1' : master, '0': slaver
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ptp40_ms_cfg_ptp_ms_Mask                                                            cBit31_0
#define cAf6_upen_ptp40_ms_cfg_ptp_ms_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : PTP Device
Reg Addr   : 0x00_8006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to config PTP device

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ptp40_dev                                                                        0x008006

/*--------------------------------------
BitField Name: device_mode
BitField Type: RW
BitField Desc: '00': BC mode, '01': reserved '10': TC Separate mode, '11': TC
General mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_ptp40_dev_device_mode_Mask                                                           cBit1_0
#define cAf6_upen_ptp40_dev_device_mode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B000
Reg Formula: 
    Where  : 
Reg Desc   : 
enable for any MAC/IPv4/IPv6

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ptp_bypass                                                                   0x00B000

/*--------------------------------------
BitField Name: cfg_ptp_bypass
BitField Type: RW
BitField Desc: bit[0] l2_sa bit[1] l2_da bit[2] ip4_sa bit[3] ip4_da bit[4]
ip6_sa bit[5] ip6_da
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_Mask                                                   cBit5_0
#define cAf6_upen_cfg_ptp_bypass_cfg_ptp_bypass_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B001
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp mac sa

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_mac_sa                                                                       0x00B001

/*--------------------------------------
BitField Name: cfg_mac_sa
BitField Type: RW
BitField Desc: MAC SA
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_cfg_mac_sa_cfg_mac_sa_Mask_01                                                       cBit31_0
#define cAf6_upen_cfg_mac_sa_cfg_mac_sa_Shift_01                                                             0
#define cAf6_upen_cfg_mac_sa_cfg_mac_sa_Mask_02                                                       cBit15_0
#define cAf6_upen_cfg_mac_sa_cfg_mac_sa_Shift_02                                                             0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B002
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp mac da

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_mac_da                                                                       0x00B002

/*--------------------------------------
BitField Name: cfg_mac_da
BitField Type: RW
BitField Desc: MAC DA
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_cfg_mac_da_cfg_mac_da_Mask_01                                                       cBit31_0
#define cAf6_upen_cfg_mac_da_cfg_mac_da_Shift_01                                                             0
#define cAf6_upen_cfg_mac_da_cfg_mac_da_Mask_02                                                       cBit15_0
#define cAf6_upen_cfg_mac_da_cfg_mac_da_Shift_02                                                             0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B003
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv4 sa1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_sa1                                                                      0x00B003

/*--------------------------------------
BitField Name: cfg_ipv4_sa1
BitField Type: RW
BitField Desc: IPv4 SA1
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_sa1_cfg_ipv4_sa1_Mask                                                       cBit31_0
#define cAf6_upen_cfg_ip4_sa1_cfg_ipv4_sa1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B004
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv4 sa2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_sa2                                                                      0x00B004

/*--------------------------------------
BitField Name: cfg_ipv4_sa2
BitField Type: RW
BitField Desc: IPv4 SA2
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_sa2_cfg_ipv4_sa2_Mask                                                       cBit31_0
#define cAf6_upen_cfg_ip4_sa2_cfg_ipv4_sa2_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B005
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv4 sa3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_sa3                                                                      0x00B005

/*--------------------------------------
BitField Name: cfg_ipv4_sa3
BitField Type: RW
BitField Desc: IPv4 SA3
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_sa3_cfg_ipv4_sa3_Mask                                                       cBit31_0
#define cAf6_upen_cfg_ip4_sa3_cfg_ipv4_sa3_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B006
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv4 sa4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_sa4                                                                      0x00B006

/*--------------------------------------
BitField Name: cfg_ipv4_sa4
BitField Type: RW
BitField Desc: IPv4 SA4
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_sa4_cfg_ipv4_sa4_Mask                                                       cBit31_0
#define cAf6_upen_cfg_ip4_sa4_cfg_ipv4_sa4_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B007
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ip4 da1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_da1                                                                      0x00B007

/*--------------------------------------
BitField Name: cfg_ip4_da1
BitField Type: RW
BitField Desc: MAC DA1
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_da1_cfg_ip4_da1_Mask                                                        cBit31_0
#define cAf6_upen_cfg_ip4_da1_cfg_ip4_da1_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B008
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ip4 da2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_da2                                                                      0x00B008

/*--------------------------------------
BitField Name: cfg_ip4_da2
BitField Type: RW
BitField Desc: MAC DA2
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_da2_cfg_ip4_da2_Mask                                                        cBit31_0
#define cAf6_upen_cfg_ip4_da2_cfg_ip4_da2_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B009
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ip4 da3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_da3                                                                      0x00B009

/*--------------------------------------
BitField Name: cfg_ip4_da3
BitField Type: RW
BitField Desc: MAC DA3
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_da3_cfg_ip4_da3_Mask                                                        cBit31_0
#define cAf6_upen_cfg_ip4_da3_cfg_ip4_da3_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B00A
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ip4 da4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip4_da4                                                                      0x00B00A

/*--------------------------------------
BitField Name: cfg_ip4_da4
BitField Type: RW
BitField Desc: MAC DA4
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip4_da4_cfg_ip4_da4_Mask                                                        cBit31_0
#define cAf6_upen_cfg_ip4_da4_cfg_ip4_da4_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B00B
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 sa1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_sa1                                                                      0x00B00B

/*--------------------------------------
BitField Name: cfg_ipv6_sa1
BitField Type: RW
BitField Desc: IPv6 SA1
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa1_cfg_ipv6_sa1_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B00C
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 sa2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_sa2                                                                      0x00B00C

/*--------------------------------------
BitField Name: cfg_ipv6_sa2
BitField Type: RW
BitField Desc: IPv6 SA2
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa2_cfg_ipv6_sa2_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B00D
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 sa3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_sa3                                                                      0x00B00D

/*--------------------------------------
BitField Name: cfg_ipv6_sa3
BitField Type: RW
BitField Desc: IPv6 SA3
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa3_cfg_ipv6_sa3_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B00E
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 sa4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_sa4                                                                      0x00B00E

/*--------------------------------------
BitField Name: cfg_ipv4_sa4
BitField Type: RW
BitField Desc: IPv4 SA4
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_sa4_cfg_ipv4_sa4_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B00F
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 da1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_da1                                                                      0x00B00F

/*--------------------------------------
BitField Name: cfg_ipv6_da1
BitField Type: RW
BitField Desc: IPv6 DA1
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da1_cfg_ipv6_da1_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B010
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 da2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_da2                                                                      0x00B010

/*--------------------------------------
BitField Name: cfg_ipv6_da2
BitField Type: RW
BitField Desc: IPv6 DA2
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da2_cfg_ipv6_da2_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B011
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 da3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_da3                                                                      0x00B011

/*--------------------------------------
BitField Name: cfg_ipv6_da3
BitField Type: RW
BitField Desc: IPv6 DA3
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da3_cfg_ipv6_da3_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_B012
Reg Formula: 
    Where  : 
Reg Desc   : 
cfg ptp ipv6 da4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ip6_da4                                                                      0x00B012

/*--------------------------------------
BitField Name: cfg_ipv6_da4
BitField Type: RW
BitField Desc: IPv6 DA4
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Mask_01                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Shift_01                                                          0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Mask_02                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Shift_02                                                          0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Mask_03                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Shift_03                                                          0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Mask_04                                                    cBit31_0
#define cAf6_upen_cfg_ip6_da4_cfg_ipv6_da4_Shift_04                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_8460(R2C)
Reg Formula: 0x00_8460 +  $ipid
    Where  : 
           + $ipid(0-15)
Reg Desc   : 
counter iport

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_pkt_in_r2c_Base                                                              0x008460
#define cAf6Reg_upen_cnt_pkt_in_r2c(ipid)                                                    (0x008460+(ipid))

/*--------------------------------------
BitField Name: cnt_input_port
BitField Type: RW
BitField Desc: Total RX counter input
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_pkt_in_r2c_cnt_input_port_Mask                                                  cBit31_0
#define cAf6_upen_cnt_pkt_in_r2c_cnt_input_port_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x008470(RO)
Reg Formula: 0x008470 +  $ipid
    Where  : 
           + $ipid(0-15)
Reg Desc   : 
counter iport

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_pkt_in_ro_Base                                                               0x008470
#define cAf6Reg_upen_cnt_pkt_in_ro(ipid)                                                     (0x008470+(ipid))

/*--------------------------------------
BitField Name: cnt_input_port
BitField Type: RW
BitField Desc: Total RX counter input
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_pkt_in_ro_cnt_input_port_Mask                                                   cBit31_0
#define cAf6_upen_cnt_pkt_in_ro_cnt_input_port_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_8440(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Total Rx counter ptp

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_ptppkt_rx_r2c                                                                0x008440

/*--------------------------------------
BitField Name: rxcnt_ptp
BitField Type: RW
BitField Desc: Total counter RX 40G port
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_ptppkt_rx_r2c_rxcnt_ptp_Mask                                                    cBit31_0
#define cAf6_upen_cnt_ptppkt_rx_r2c_rxcnt_ptp_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x008460(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Total Rx counter ptp

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_ptppkt_rx_ro                                                                 0x008460

/*--------------------------------------
BitField Name: rxcnt_ptp
BitField Type: RW
BitField Desc: Total counter RX 40G port
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_ptppkt_rx_ro_rxcnt_ptp_Mask                                                     cBit31_0
#define cAf6_upen_cnt_ptppkt_rx_ro_rxcnt_ptp_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x00_8400(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Total Tx counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_txpkt_out_r2c                                                                0x008400

/*--------------------------------------
BitField Name: tx40cnt_ptp
BitField Type: RW
BitField Desc: tx counter 40G output
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_txpkt_out_r2c_tx40cnt_ptp_Mask                                                  cBit31_0
#define cAf6_upen_cnt_txpkt_out_r2c_tx40cnt_ptp_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x008420(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Total Tx counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_txpkt_out_ro                                                                 0x008420

/*--------------------------------------
BitField Name: tx40cnt_ptp
BitField Type: RW
BitField Desc: tx counter 40G output
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_txpkt_out_ro_tx40cnt_ptp_Mask                                                   cBit31_0
#define cAf6_upen_cnt_txpkt_out_ro_tx40cnt_ptp_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Received 40G PORT
Reg Addr   : 0x00_8500(R2C)
Reg Formula: 0x00_8500+ $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of Sync message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_sync_cnt40_r2c_Base                                                           0x008500
#define cAf6Reg_upen_rx_sync_cnt40_r2c(pid)                                                   (0x008500+(pid))

/*--------------------------------------
BitField Name: syncrx_cnt40
BitField Type: RC
BitField Desc: number sync packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_sync_cnt40_r2c_syncrx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_rx_sync_cnt40_r2c_syncrx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Received 40G PORT
Reg Addr   : 0x00_8580(RO)
Reg Formula: 0x00_8580+ $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of Sync message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_sync_cnt40_ro_Base                                                            0x008580
#define cAf6Reg_upen_rx_sync_cnt40_ro(pid)                                                    (0x008580+(pid))

/*--------------------------------------
BitField Name: syncrx_cnt40
BitField Type: RC
BitField Desc: number sync packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_sync_cnt40_ro_syncrx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_rx_sync_cnt40_ro_syncrx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Received 40G PORT
Reg Addr   : 0x00_8520(R2C)
Reg Formula: 0x00_8520 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of folu message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_folu_cnt40_r2c_Base                                                           0x008520
#define cAf6Reg_upen_rx_folu_cnt40_r2c(pid)                                                   (0x008520+(pid))

/*--------------------------------------
BitField Name: folurx_cnt40
BitField Type: RC
BitField Desc: number folu packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_folu_cnt40_r2c_folurx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_rx_folu_cnt40_r2c_folurx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Received 40G PORT
Reg Addr   : 0x00_85A0(RO)
Reg Formula: 0x00_85A0 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of folu message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_folu_cnt40_ro_Base                                                            0x0085A0
#define cAf6Reg_upen_rx_folu_cnt40_ro(pid)                                                    (0x0085A0+(pid))

/*--------------------------------------
BitField Name: folurx_cnt40
BitField Type: RC
BitField Desc: number folu packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_folu_cnt40_ro_folurx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_rx_folu_cnt40_ro_folurx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq Received 40G PORT
Reg Addr   : 0x00_8540(R2C)
Reg Formula: 0x00_8540+$pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dreq message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dreq_cnt40_r2c_Base                                                           0x008540
#define cAf6Reg_upen_rx_dreq_cnt40_r2c(pid)                                                   (0x008540+(pid))

/*--------------------------------------
BitField Name: dreqrx_cnt40
BitField Type: RC
BitField Desc: number dreq packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dreq_cnt40_r2c_dreqrx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_rx_dreq_cnt40_r2c_dreqrx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq Received 40G PORT
Reg Addr   : 0x00_85C0(RO)
Reg Formula: 0x00_85C0+$pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dreq message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dreq_cnt40_ro_Base                                                            0x0085C0
#define cAf6Reg_upen_rx_dreq_cnt40_ro(pid)                                                    (0x0085C0+(pid))

/*--------------------------------------
BitField Name: dreqrx_cnt40
BitField Type: RC
BitField Desc: number dreq packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dreq_cnt40_ro_dreqrx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_rx_dreq_cnt40_ro_dreqrx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres Received 40G PORT
Reg Addr   : 0x00_8560(R2C)
Reg Formula: 0x00_8560 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dres message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dres_cnt40_r2c_Base                                                           0x008560
#define cAf6Reg_upen_rx_dres_cnt40_r2c(pid)                                                   (0x008560+(pid))

/*--------------------------------------
BitField Name: dresrx_cnt40
BitField Type: RC
BitField Desc: number dres packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dres_cnt40_r2c_dresrx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_rx_dres_cnt40_r2c_dresrx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres Received 40G PORT
Reg Addr   : 0x00_85E0(RO)
Reg Formula: 0x00_85E0 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dres message received

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rx_dres_cnt40_ro_Base                                                            0x0085E0
#define cAf6Reg_upen_rx_dres_cnt40_ro(pid)                                                    (0x0085E0+(pid))

/*--------------------------------------
BitField Name: dresrx_cnt40
BitField Type: RC
BitField Desc: number dres packet at Rx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_rx_dres_cnt40_ro_dresrx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_rx_dres_cnt40_ro_dresrx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Transmit 40G PORT
Reg Addr   : 0x00_8600(R2C)
Reg Formula: 0x00_8600 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of Sync message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_sync_cnt40_r2c_Base                                                           0x008600
#define cAf6Reg_upen_tx_sync_cnt40_r2c(pid)                                                   (0x008600+(pid))

/*--------------------------------------
BitField Name: synctx_cnt40
BitField Type: RC
BitField Desc: number sync packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_sync_cnt40_r2c_synctx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_tx_sync_cnt40_r2c_synctx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter Sync Transmit 40G PORT
Reg Addr   : 0x00_8680(RO)
Reg Formula: 0x00_8680 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of Sync message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_sync_cnt40_ro_Base                                                            0x008680
#define cAf6Reg_upen_tx_sync_cnt40_ro(pid)                                                    (0x008680+(pid))

/*--------------------------------------
BitField Name: synctx_cnt40
BitField Type: RC
BitField Desc: number sync packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_sync_cnt40_ro_synctx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_tx_sync_cnt40_ro_synctx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Transmit 40G PORT
Reg Addr   : 0x00_8620(R2C)
Reg Formula: 0x00_8620 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of folu message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_folu_cnt40_r2c_Base                                                           0x008620
#define cAf6Reg_upen_tx_folu_cnt40_r2c(pid)                                                   (0x008620+(pid))

/*--------------------------------------
BitField Name: folutx_cnt40
BitField Type: RC
BitField Desc: number folu packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_folu_cnt40_r2c_folutx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_tx_folu_cnt40_r2c_folutx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter Folu Transmit 40G PORT
Reg Addr   : 0x00_86A0(RO)
Reg Formula: 0x00_86A0 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of folu message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_folu_cnt40_ro_Base                                                            0x0086A0
#define cAf6Reg_upen_tx_folu_cnt40_ro(pid)                                                    (0x0086A0+(pid))

/*--------------------------------------
BitField Name: folutx_cnt40
BitField Type: RC
BitField Desc: number folu packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_folu_cnt40_ro_folutx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_tx_folu_cnt40_ro_folutx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq transmit
Reg Addr   : 0x00_8640(R2C)
Reg Formula: 0x00_8640 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dreq message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dreq_cnt40_r2c_Base                                                           0x008640
#define cAf6Reg_upen_tx_dreq_cnt40_r2c(pid)                                                   (0x008640+(pid))

/*--------------------------------------
BitField Name: dreqtx_cnt40
BitField Type: RC
BitField Desc: number dreq packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dreq_cnt40_r2c_dreqtx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_tx_dreq_cnt40_r2c_dreqtx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter dreq transmit
Reg Addr   : 0x00_86C0(RO)
Reg Formula: 0x00_86C0 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dreq message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dreq_cnt40_ro_Base                                                            0x0086C0
#define cAf6Reg_upen_tx_dreq_cnt40_ro(pid)                                                    (0x0086C0+(pid))

/*--------------------------------------
BitField Name: dreqtx_cnt40
BitField Type: RC
BitField Desc: number dreq packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dreq_cnt40_ro_dreqtx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_tx_dreq_cnt40_ro_dreqtx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres transmit
Reg Addr   : 0x00_8660(R2C)
Reg Formula: 0x00_8660 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dres message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dres_cnt40_r2c_Base                                                           0x008660
#define cAf6Reg_upen_tx_dres_cnt40_r2c(pid)                                                   (0x008660+(pid))

/*--------------------------------------
BitField Name: drestx_cnt40
BitField Type: RC
BitField Desc: number dres packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dres_cnt40_r2c_drestx_cnt40_Mask                                                 cBit31_0
#define cAf6_upen_tx_dres_cnt40_r2c_drestx_cnt40_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter dres transmit
Reg Addr   : 0x00_86E0(RO)
Reg Formula: 0x00_86E0 + $pid
    Where  : 
           + $pid(0)
Reg Desc   : 
Number of dres message transmit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tx_dres_cnt40_ro_Base                                                            0x0086E0
#define cAf6Reg_upen_tx_dres_cnt40_ro(pid)                                                    (0x0086E0+(pid))

/*--------------------------------------
BitField Name: drestx_cnt40
BitField Type: RC
BitField Desc: number dres packet at Tx
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_tx_dres_cnt40_ro_drestx_cnt40_Mask                                                  cBit31_0
#define cAf6_upen_tx_dres_cnt40_ro_drestx_cnt40_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Read CPU Queue
Reg Addr   : 0x00_8216
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cpuque40                                                                         0x008216

/*--------------------------------------
BitField Name: typ
BitField Type: RO
BitField Desc: typ of ptp packet
BitField Bits: [102:101]
--------------------------------------*/
#define cAf6_upen_cpuque40_typ_Mask                                                                    cBit6_5
#define cAf6_upen_cpuque40_typ_Shift                                                                         5

/*--------------------------------------
BitField Name: seqid
BitField Type: RO
BitField Desc: sequence of packet from sequenceId field
BitField Bits: [100:85]
--------------------------------------*/
#define cAf6_upen_cpuque40_seqid_01_Mask                                                             cBit31_21
#define cAf6_upen_cpuque40_seqid_01_Shift                                                                   21
#define cAf6_upen_cpuque40_seqid_02_Mask                                                               cBit4_0
#define cAf6_upen_cpuque40_seqid_02_Shift                                                                    0

/*--------------------------------------
BitField Name: egr_pid
BitField Type: RO
BitField Desc: egress port id
BitField Bits: [84:80]
--------------------------------------*/
#define cAf6_upen_cpuque40_egr_pid_Mask                                                              cBit20_16
#define cAf6_upen_cpuque40_egr_pid_Shift                                                                    16

/*--------------------------------------
BitField Name: egr_tmr_tod
BitField Type: RO
BitField Desc: egress timer tod
BitField Bits: [79:00]
--------------------------------------*/
#define cAf6_upen_cpuque40_egr_tmr_tod_01_Mask                                                        cBit31_0
#define cAf6_upen_cpuque40_egr_tmr_tod_01_Shift                                                              0
#define cAf6_upen_cpuque40_egr_tmr_tod_02_Mask                                                        cBit31_0
#define cAf6_upen_cpuque40_egr_tmr_tod_02_Shift                                                              0
#define cAf6_upen_cpuque40_egr_tmr_tod_03_Mask                                                        cBit15_0
#define cAf6_upen_cpuque40_egr_tmr_tod_03_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Enb CPU Interupt
Reg Addr   : 0x00_8200
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_en40                                                                         0x008200

/*--------------------------------------
BitField Name: int_en
BitField Type: RW
BitField Desc: set "1" ,interupt enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_en40_int_en_Mask                                                                   cBit0
#define cAf6_upen_int_en40_int_en_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Interupt CPU Queue
Reg Addr   : 0x00_8201
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int40                                                                            0x008201

/*--------------------------------------
BitField Name: int_en
BitField Type: RW
BitField Desc: set"1" queue info ready ,write 1 to clear
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int40_int_en_Mask                                                                      cBit0
#define cAf6_upen_int40_int_en_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Interupt CPU Queue
Reg Addr   : 0x00_8202
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read CPU queue

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta40                                                                            0x008202

/*--------------------------------------
BitField Name: upen_sta
BitField Type: RW
BitField Desc: current state of CPU queue, bit[17] "1" queue FULL, bit[16] "1"
NOT empty, ready for cpu read, bit[15:0] queue lengh
BitField Bits: [16:00]
--------------------------------------*/
#define cAf6_upen_sta40_upen_sta_Mask                                                                 cBit16_0
#define cAf6_upen_sta40_upen_sta_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Config t1t3mode
Reg Addr   : 0x00_8017
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to config t1t3mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_t1t3mode40g                                                                      0x008017

/*--------------------------------------
BitField Name: cfg_t1t3mode40g_enable
BitField Type: RW
BitField Desc: 1: enable this mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_t1t3mode40g_cfg_t1t3mode40g_enable_Mask                                                cBit1
#define cAf6_upen_t1t3mode40g_cfg_t1t3mode40g_enable_Shift                                                   1

/*--------------------------------------
BitField Name: cfg_t1t3mode40g
BitField Type: RW
BitField Desc: 0: send timestamp to cpu queue 1: send back ptp packet mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_t1t3mode40g_cfg_t1t3mode40g_Mask                                                       cBit0
#define cAf6_upen_t1t3mode40g_cfg_t1t3mode40g_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PTP interupt status
Reg Addr   : 0x00_80FE
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to detect interupt event

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_sta                                                                          0x0080FE

/*--------------------------------------
BitField Name: interupt of 40G BP port
BitField Type: RW
BitField Desc: 1: active, Read CPU Queue interupt 40G BP
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_int_sta_interupt_of_40G_BP_port_Mask                                                   cBit1
#define cAf6_upen_int_sta_interupt_of_40G_BP_port_Shift                                                      1

/*--------------------------------------
BitField Name: interupt of Face   port
BitField Type: RW
BitField Desc: 1: active, Read CPU Queue interupt FACE
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_int_sta_interupt_of_Face___port_Mask                                                   cBit0
#define cAf6_upen_int_sta_interupt_of_Face___port_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : config vlan id global
Reg Addr   : 0x00_4021-0x00_4024
Reg Formula: 0x00_4021 + $num
    Where  : 
           + $num(0-3)
Reg Desc   : 
config vlan id global  for all port FACE

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_face_vid_glb_Base                                                                0x004021
#define cAf6Reg_upen_face_vid_glb(num)                                                        (0x004021+(num))
#define cAf6Reg_upen_face_vid_glb_WidthVal                                                                  32

/*--------------------------------------
BitField Name: cfg_face_vidglb
BitField Type: RW
BitField Desc: global vlan id for FACE
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_face_vid_glb_cfg_face_vidglb_Mask                                                   cBit11_0
#define cAf6_upen_face_vid_glb_cfg_face_vidglb_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : config vlan id global enable
Reg Addr   : 0x00_4020
Reg Formula: 0x00_4020
    Where  : 
Reg Desc   : 
config vlan id global enable matching for all port FACE

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_face_vid_glb_en                                                                  0x004020
#define cAf6Reg_upen_face_vid_glb_en_WidthVal                                                               32

/*--------------------------------------
BitField Name: cfg_face_vidglb_ena
BitField Type: RW
BitField Desc: "1" enable checking global vlanid
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_face_vid_glb_en_cfg_face_vidglb_ena_Mask                                               cBit0
#define cAf6_upen_face_vid_glb_en_cfg_face_vidglb_ena_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : config vlan id per port
Reg Addr   : 0x00_51A0-0x00_51AF
Reg Formula: 0x00_51A0 + $pid + $group16*65536
    Where  : 
           + $pid(0-15): port ID within group 16port
           + $group16 (0-1): group engine ID of 16 port face plate
Reg Desc   : 
config vlan id per port FACE

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_face_vid_perport_Base                                                            0x0051A0
#define cAf6Reg_upen_face_vid_perport(pid, group16)                           (0x0051A0+(pid)+(group16)*65536)
#define cAf6Reg_upen_face_vid_perport_WidthVal                                                              32

/*--------------------------------------
BitField Name: ena_vlanid2
BitField Type: RW
BitField Desc: set "1" enable checking vlan id2
BitField Bits: [25]
--------------------------------------*/
#define cAf6_upen_face_vid_perport_ena_vlanid2_Mask                                                     cBit25
#define cAf6_upen_face_vid_perport_ena_vlanid2_Shift                                                        25

/*--------------------------------------
BitField Name: ena_vlanid1
BitField Type: RW
BitField Desc: set "1" enable checking vlan id1
BitField Bits: [24]
--------------------------------------*/
#define cAf6_upen_face_vid_perport_ena_vlanid1_Mask                                                     cBit24
#define cAf6_upen_face_vid_perport_ena_vlanid1_Shift                                                        24

/*--------------------------------------
BitField Name: vlanid2
BitField Type: RW
BitField Desc: perport vlan id1 for FACE
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_face_vid_perport_vlanid2_Mask                                                      cBit23_12
#define cAf6_upen_face_vid_perport_vlanid2_Shift                                                            12

/*--------------------------------------
BitField Name: vlanid1
BitField Type: RW
BitField Desc: perport vlan id2 for FACE
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_face_vid_perport_vlanid1_Mask                                                       cBit11_0
#define cAf6_upen_face_vid_perport_vlanid1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : config vlan40 id global
Reg Addr   : 0x00_B040-0x00_B043
Reg Formula: 0x00_B040 + $num
    Where  : 
           + $num(0-3)
Reg Desc   : 
config vlan id global  for all port 40G BACKPLAN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_bp_vid_glb_Base                                                                  0x00B040
#define cAf6Reg_upen_bp_vid_glb(num)                                                          (0x00B040+(num))
#define cAf6Reg_upen_bp_vid_glb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_bp_vidglb_enable
BitField Type: RW
BitField Desc: set "1" ( in case $num = 0, $num!= 0 dont care) enable  checking
bp global vlanid
BitField Bits: [12]
--------------------------------------*/
#define cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_Mask                                                  cBit12
#define cAf6_upen_bp_vid_glb_cfg_bp_vidglb_enable_Shift                                                     12

/*--------------------------------------
BitField Name: cfg_bp_vidglb
BitField Type: RW
BitField Desc: global vlan id for 40G port
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_bp_vid_glb_cfg_bp_vidglb_Mask                                                       cBit11_0
#define cAf6_upen_bp_vid_glb_cfg_bp_vidglb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : config vlan40 id perport
Reg Addr   : 0x0_A000-0x0_A00F
Reg Formula: 0x00_A000 + $opid
    Where  : 
           + $opid(0-31)
Reg Desc   : 
config vlan id per port 40G BACKPLAN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_bp_vid_perport_Base                                                               0x0A000
#define cAf6Reg_upen_bp_vid_perport(opid)                                                    (0x00A000+(opid))
#define cAf6Reg_upen_bp_vid_perport_WidthVal                                                                32

/*--------------------------------------
BitField Name: ena_bpvlanid2
BitField Type: RW
BitField Desc: set "1" enable checking vlan id2
BitField Bits: [25]
--------------------------------------*/
#define cAf6_upen_bp_vid_perport_ena_bpvlanid2_Mask                                                     cBit25
#define cAf6_upen_bp_vid_perport_ena_bpvlanid2_Shift                                                        25

/*--------------------------------------
BitField Name: ena_bpvlanid1
BitField Type: RW
BitField Desc: set "1" enable checking vlan id1
BitField Bits: [24]
--------------------------------------*/
#define cAf6_upen_bp_vid_perport_ena_bpvlanid1_Mask                                                     cBit24
#define cAf6_upen_bp_vid_perport_ena_bpvlanid1_Shift                                                        24

/*--------------------------------------
BitField Name: cfg_bp_vid2
BitField Type: RW
BitField Desc: vlan id2 for 40G port
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_bp_vid_perport_cfg_bp_vid2_Mask                                                    cBit23_12
#define cAf6_upen_bp_vid_perport_cfg_bp_vid2_Shift                                                          12

/*--------------------------------------
BitField Name: cfg_bp_vid1
BitField Type: RW
BitField Desc: vlan id1 for 40G port
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_bp_vid_perport_cfg_bp_vid1_Mask                                                     cBit11_0
#define cAf6_upen_bp_vid_perport_cfg_bp_vid1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : PTP Face Dis Timestaping
Reg Addr   : 0x00_0020
Reg Formula: 0x00_0020 + $group16*65536
    Where  : 
           + $group16 (0-1): group engine ID of 16 port face plate
Reg Desc   : 
used to disable ptp for TC update CF face 2 BP

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge_ptp_dis_timestamping_Base                                                     0x000020
#define cAf6Reg_upen_ge_ptp_dis_timestamping(group16)                               (0x000020+(group16)*65536)

/*--------------------------------------
BitField Name: cfg_timstap_dis_face_2_BP
BitField Type: RW
BitField Desc: '1' : disable update CF, '0': enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_ge_ptp_dis_timestamping_cfg_timstap_dis_face_2_BP_Mask                                   cBit0
#define cAf6_upen_ge_ptp_dis_timestamping_cfg_timstap_dis_face_2_BP_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : PTP BP Dis Timestamping
Reg Addr   : 0x00_8020
Reg Formula: 
    Where  : 
Reg Desc   : 
used to disable ptp for TC update CF face 2 BP

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_bp_ptp_dis_timestamping                                                          0x008020

/*--------------------------------------
BitField Name: cfg_timstap_dis_BP_2_face
BitField Type: RW
BitField Desc: '1' : disable update CF, '0': enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_bp_ptp_dis_timestamping_cfg_timstap_dis_BP_2_face_Mask                                   cBit0
#define cAf6_upen_bp_ptp_dis_timestamping_cfg_timstap_dis_BP_2_face_Shift                                       0

#endif /* _THA60290051MODULEPTPREG_H_ */

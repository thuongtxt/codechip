/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : Tha60290051ModulePtpInternal.h
 * 
 * Created Date: Nov 19, 2018
 *
 * Description : Internal data of the 60290051 module ptp
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051MODULEPTPINTERNAL_H_
#define _THA60290051MODULEPTPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/ptp/Tha60290022ModulePtpInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051ModulePtp
    {
    tTha60290022ModulePtp super;

    /* Private data */
    AtLongRegisterAccess faceplateLongAccessPerPart[2];
    }tTha60290051ModulePtp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051MODULEPTPINTERNAL_H_ */


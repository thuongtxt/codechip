/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290051Device.c
 *
 * Created Date: Aug 20, 2018
 *
 * Description : 60290051 Device implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesManagerInternal.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../Tha60290021/physical/Tha60290021SerdesManager.h"
#include "Tha60290051DeviceInternal.h"
#include "../physical/Tha60290051Physical.h"
#include "../physical/diag/Tha60290051TOPGLBReg.h"
#include "../ptp/Tha60290051ModulePtp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
/* OCN/ETH Port */
#define cDiagSfpOcnOrEthDiagEnableMask(lineId)   (cBit0 << (lineId))
#define cDiagSfpOcnOrEthDiagEnableShift(lineId)  (lineId)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60290021DeviceMethods m_Tha60290021DeviceOverride;
static tTha60290022DeviceMethods m_Tha60290022DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60290021DeviceMethods *m_Tha60290021DeviceMethods = NULL;
static const tTha60290022DeviceMethods *m_Tha60290022DeviceMethods = NULL;
static const tTha60210011DeviceMethods *m_Tha60210011DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldOpenFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static eBool DeviceCapacity15G(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasKByteFeatureOnly(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HwLogicOptimized(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccV2IsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DeviceV3OptimizationIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Pmc1PageIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HoTxG1OverWriteIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccV4dot1EnableRegAddressIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ClockSquelchingIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ClockEthSquelchingIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthTxIpgIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccTxFcsMsbIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccSgmiiTxIpg31ByteIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Eth100MFxLfRfSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccHdlcTransmissionBitOrderPerChannelIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Sts192cPrbsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SemIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool OcnSeparatedSectionAndLineDccIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthPortFlowControlWaterMarkIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ClockEthSquelchingSelectOnCoreOnlyIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Sts192cTxG1OverrideIsNotUsed(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool Sts192cPrbsInvertionIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DcrAcrTxShapperSupportedVersion(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0x0000);
    }

static eBool ClockExtractorMonitorPpmIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TohSonetSdhModeIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CesopAutoRxMBitConfigurableIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SeparateLbitAndLopsPktReplacementSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateEthCounter32bitIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Eth100FxSerdesClock31Dot25MhzIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 MapDemapNumLoSlices(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool HasStandardClearTime(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsNewBerInterrupt(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplatSerdesEthHasK30_7Feature(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 FaceplatSerdesEthStartVersionSupportTxAlarmForcing(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool FaceplatSerdesEthExcessiveErrorRatioIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthHasSgmiiDiagnosticLogic(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SgmiiSohEthUseNewMac(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumberOfMapLoPwPerSlice(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool NewTuVcLoopbackIsReady(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Sts192cIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PdaIsNewOc192cOffset(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PdaNumJitterBufferBlocksIncreased(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PdhStartVersionSupportPrm(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PdhStartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PdhStartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PdhRxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateSerdesPmaFarEndLoopbackSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool KByteSerdesIsControllable(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SgmiiSerdesShouldUseMdioLocalLoopback(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PohHasCpujnreqen(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PohStartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PrbsBertOptimizeIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwDccKbyteInterruptIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateFramerLocalLoopbackSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccHdlcHasPacketCounterDuringFifoFull(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FacePlateSerdesDataMaskOnResetConfigurable(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PtpSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x1, 0x0015);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 phyModule = moduleId;

    /* Public modules */
    if (moduleId == cAtModuleEth)   return (AtModule)Tha60290051ModuleEthNew(self);
    if (moduleId == cAtModulePrbs)  return (AtModule)Tha60290051ModulePrbsNew(self);
    if (moduleId == cAtModuleSdh)   return NULL;
    if (moduleId == cAtModulePdh)   return NULL;
    if (moduleId == cAtModuleXc)    return NULL;
    if (moduleId == cAtModulePw)    return NULL;

    if (moduleId == cAtModuleSur)   return NULL;
    if (moduleId == cAtModuleAps)   return NULL;
    if (moduleId == cAtModuleBer)   return NULL;
    if (moduleId == cAtModuleRam)   return (AtModule)Tha60290051ModuleRamNew(self);
    if (moduleId == cAtModuleClock) return (AtModule)Tha60290051ModuleClockNew(self);
    if (moduleId == cAtModulePtp)   return (AtModule)Tha60290051ModulePtpNew(self);

    /* Private modules */
    if (phyModule == cThaModuleOcn)   return NULL;
    if (phyModule == cThaModuleMap)   return NULL;
    if (phyModule == cThaModuleDemap) return NULL;
    if (phyModule == cThaModuleCdr)   return NULL;
    if (phyModule == cThaModulePda)   return NULL;
    if (phyModule == cThaModulePwe)   return NULL;
    if (phyModule == cThaModuleCos)   return NULL;
    if (phyModule == cThaModuleCla)   return NULL;
    if (phyModule == cThaModulePoh)   return NULL;

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModulePktAnalyzer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModulePtp};
    /*PTP module is placed at then end this array to avoid that its module is NULL will
     * make iterator of device modules miss modules behind the PTP module*/

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = (uint32)moduleId;
    AtUnused(self);

    if ((moduleValue == cAtModuleEth) ||
        (moduleValue == cAtModuleRam) ||
        (moduleValue == cAtModulePktAnalyzer) ||
        (moduleValue == cAtModulePrbs) ||
        (moduleValue == cAtModuleClock) ||
        (moduleValue == cAtModulePtp))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = cAtOk;

    AtDeviceAllDiagServiceStop(self);
    Tha60290021DeviceFaceplateSerdesTimingModeInvalidate(self);

    ret |= AtDeviceDiagnosticCheckEnable(self, cAtTrue);

    ret |= Tha60150011DeviceInit(self);

    AtSerdesManagerReset(AtDeviceSerdesManagerGet((AtDevice)self));

    Tha60290021DeviceInputClocksCheck(self);

    return ret;
    }

static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha60290051SerdesManagerNew(self);
    }

static eAtRet SdhSerdesReset(AtDevice self)
    {
    AtUnused(self);
    return cAtOk;
    }
static uint32 NumFaceplateSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 32;
    }

static uint32 NumMateSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumBackplaneSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumSgmiiSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 31;
    }

static uint8 MateSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 0xFF;
    }

static uint8 MateSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 0xFF;
    }

static uint8 BackplaneSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 32;
    }

static uint8 BackplaneSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 33;
    }

static uint8 SgmiiSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 0xFF;
    }

static uint8 SgmiiSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 0xFF;
    }

static uint32 RegisterWithLocalAddress(AtDevice self, uint32 localAddress)
    {
    AtUnused(self);
    return localAddress + cAf6Reg_ProductID_Base;
    }

static eAtRet HwDiagnosticModeEnable(AtDevice self, eBool enable)
    {
    uint32 i;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    uint32 regAddr = RegisterWithLocalAddress(self, cAf6Reg_o_control3_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    uint8 diag = enable ? 1 : 0;
    uint32 enableMask;
    uint32 enableShift;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(self);

    /* SFP OCN or ETH PORT */
    for (i = 0; i < Tha60290021SerdesManagerNumFaceplateSerdes(serdesManager); i++)
        {
        enableMask  = cDiagSfpOcnOrEthDiagEnableMask(i);
        enableShift = cDiagSfpOcnOrEthDiagEnableShift(i);
        mRegFieldSet(regVal, enable, diag);
        }
    AtIpCoreWrite(core, regAddr, regVal);
    return cAtOk;
    }

static const char* NameBuild(const char* name, uint32 id)
    {
    static char bufferName[64];
    AtSprintf(bufferName, "%s_%u", name, id);
    return bufferName;
    }

static void MultiRateClockStatusDisplay(AtDevice self)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(self);
    uint32 serdes_i;
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    mNoneDebuggerPrint(cSevInfo, "* SERDES Multi-rate clocks:\r\n");
    for (serdes_i = 0; serdes_i < Tha60290021SerdesManagerNumFaceplateSerdes(serdesManager); serdes_i++)
        {
        uint32 freqExpected;
        uint32 regAddr;
        uint32 serdesId = (uint32)Tha60290021SerdesManagerFaceplateSerdesStartId(serdesManager) + serdes_i;

        if (!AtSerdesManagerSerdesIsControllable(serdesManager, serdesId))
            continue;

        freqExpected = Tha60290021DeviceFaceplateFreqExpectedValue(self, serdes_i);
        regAddr = RegisterWithLocalAddress(self, cAf6Reg_clock_mon_highclock_mon_multi_rx_Base + serdes_i);
        ThaDeviceClockResultPrint(self, debugger, NameBuild(debugger ? "multi_rate_rxclk" : "  - multi_rate_rxclk", serdes_i), regAddr, freqExpected, 5);
        regAddr = RegisterWithLocalAddress(self, cAf6Reg_clock_mon_highclock_mon_multi_tx_Base + serdes_i);
        ThaDeviceClockResultPrint(self, debugger, NameBuild(debugger ? "multi_rate_rxclk" : "  - multi_rate_rxclk", serdes_i), regAddr, freqExpected, 5);
        }
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void ClockStatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;

    Tha60290021DeviceBackplaneClockStatusDisplay(device);
    Tha60290021DeviceRefClockStatusDisplay(device);
    MultiRateClockStatusDisplay(device);

    Tha60290021DeviceSlowClockStatusDisplay(device, 194400000);
    Tha60290021DeviceFsmFrequencyDisplay(device);
    Tha60290021DeviceSpareGpioFrequencyDisplay(device, 14);
    Tha60290021DeviceSpareFrequencyDisplay(device);
    Tha60290021DeviceRefout8kDisplay(device);
    }

static uint32 InterruptBaseAddress(Tha60210011Device self)
    {
    AtUnused(self);
    return 0x0090000;
    }

static eBool FaceplateSerdesHasNewParams(Tha60290022Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
    return Tha60290051IpCoreNew(coreId, self);
    }

static void HwFlush(Tha60150011Device self)
    {
    AtUnused(self);
    /* Nothing to do for this project */
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, HwDiagnosticModeEnable);
        mMethodOverride(m_Tha60210011DeviceOverride, InterruptBaseAddress);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60290021Device(AtDevice self)
    {
    Tha60290021Device device = (Tha60290021Device)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021DeviceOverride, m_Tha60290021DeviceMethods, sizeof(m_Tha60290021DeviceOverride));

        mMethodOverride(m_Tha60290021DeviceOverride, NumFaceplateSerdes);
        mMethodOverride(m_Tha60290021DeviceOverride, NumMateSerdes);
        mMethodOverride(m_Tha60290021DeviceOverride, NumSgmiiSerdes);
        mMethodOverride(m_Tha60290021DeviceOverride, NumBackplaneSerdes);
        mMethodOverride(m_Tha60290021DeviceOverride, FaceplateSerdesStartId);
        mMethodOverride(m_Tha60290021DeviceOverride, FaceplateSerdesEndId);
        mMethodOverride(m_Tha60290021DeviceOverride, MateSerdesStartId);
        mMethodOverride(m_Tha60290021DeviceOverride, MateSerdesEndId);
        mMethodOverride(m_Tha60290021DeviceOverride, SgmiiSerdesStartId);
        mMethodOverride(m_Tha60290021DeviceOverride, SgmiiSerdesEndId);
        mMethodOverride(m_Tha60290021DeviceOverride, BackplaneSerdesStartId);
        mMethodOverride(m_Tha60290021DeviceOverride, BackplaneSerdesEndId);
        }

    mMethodsSet(device, &m_Tha60290021DeviceOverride);
    }

static void OverrideTha60290022Device(AtDevice self)
    {
    Tha60290022Device device = (Tha60290022Device)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022DeviceOverride, m_Tha60290022DeviceMethods, sizeof(m_Tha60290022DeviceOverride));

        mMethodOverride(m_Tha60290022DeviceOverride, DeviceCapacity15G);
        mMethodOverride(m_Tha60290022DeviceOverride, HasKByteFeatureOnly);
        mMethodOverride(m_Tha60290022DeviceOverride, HwLogicOptimized);
        mMethodOverride(m_Tha60290022DeviceOverride, DccV2IsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DeviceV3OptimizationIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Pmc1PageIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, HoTxG1OverWriteIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccV4dot1EnableRegAddressIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockSquelchingIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockEthSquelchingIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, EthTxIpgIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccTxFcsMsbIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccSgmiiTxIpg31ByteIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Eth100MFxLfRfSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccHdlcTransmissionBitOrderPerChannelIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cPrbsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, SemIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, OcnSeparatedSectionAndLineDccIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, EthPortFlowControlWaterMarkIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockEthSquelchingSelectOnCoreOnlyIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cTxG1OverrideIsNotUsed);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cPrbsInvertionIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DcrAcrTxShapperSupportedVersion);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockExtractorMonitorPpmIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, TohSonetSdhModeIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, CesopAutoRxMBitConfigurableIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, SeparateLbitAndLopsPktReplacementSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateEthCounter32bitIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Eth100FxSerdesClock31Dot25MhzIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccEthMaxLengthConfigurationIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, MapDemapNumLoSlices);
        mMethodOverride(m_Tha60290022DeviceOverride, HasStandardClearTime);
        mMethodOverride(m_Tha60290022DeviceOverride, IsNewBerInterrupt);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplatSerdesEthHasK30_7Feature);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplatSerdesEthStartVersionSupportTxAlarmForcing);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplatSerdesEthExcessiveErrorRatioIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, EthHasSgmiiDiagnosticLogic);
        mMethodOverride(m_Tha60290022DeviceOverride, SgmiiSohEthUseNewMac);
        mMethodOverride(m_Tha60290022DeviceOverride, NumberOfMapLoPwPerSlice);
        mMethodOverride(m_Tha60290022DeviceOverride, NewTuVcLoopbackIsReady);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, PdaIsNewOc192cOffset);
        mMethodOverride(m_Tha60290022DeviceOverride, PdaNumJitterBufferBlocksIncreased);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhStartVersionSupportPrm);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhStartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhStartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhRxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateSerdesPmaFarEndLoopbackSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, KByteSerdesIsControllable);
        mMethodOverride(m_Tha60290022DeviceOverride, SgmiiSerdesShouldUseMdioLocalLoopback);
        mMethodOverride(m_Tha60290022DeviceOverride, PohHasCpujnreqen);
        mMethodOverride(m_Tha60290022DeviceOverride, PohStartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_Tha60290022DeviceOverride, PrbsBertOptimizeIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, PwDccKbyteInterruptIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateFramerLocalLoopbackSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, SurFailureHoldOnTimerConfigurable);
        mMethodOverride(m_Tha60290022DeviceOverride, DccHdlcHasPacketCounterDuringFifoFull);
        mMethodOverride(m_Tha60290022DeviceOverride, FacePlateSerdesDataMaskOnResetConfigurable);
        mMethodOverride(m_Tha60290022DeviceOverride, PtpSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateSerdesHasNewParams);
        }

    mMethodsSet(device, &m_Tha60290022DeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, SdhSerdesReset);
        mMethodOverride(m_Tha60150011DeviceOverride, ClockStatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    AtDevice object = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, IpCoreCreate);
        }

    mMethodsSet(object, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210011Device(self);
    OverrideTha60290021Device(self);
    OverrideTha60290022Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60290051DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

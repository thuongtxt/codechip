/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290051IpCore.c
 *
 * Created Date: Apr 18, 2019
 *
 * Description : IpCore implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../../platform/hal/AtHalDefault.h"
#include "../../../default/man/ThaIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051IpCore
    {
    tThaIpCorePwV2 super;
    }tTha60290051IpCore;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtIpCoreMethods         m_AtIpCoreOverride;

/* Save super implementation */
static const tAtIpCoreMethods  *m_AtIpCoreMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HalSet(AtIpCore self, AtHal hal)
    {
    eAtRet ret;

    ret  = m_AtIpCoreMethods->HalSet(self, hal);
    if (ret != cAtOk)
        return ret;

    if (AtDeviceIsSimulated(AtIpCoreDeviceGet(self)))
        return ret;

    AtHalDefaultAddressMaskSet(hal, cBit21_0);
    return ret;
    }

static void OverrideAtIpCore(AtIpCore self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtIpCoreMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, m_AtIpCoreMethods, sizeof(m_AtIpCoreOverride));
        mMethodOverride(m_AtIpCoreOverride, HalSet);
        }

    mMethodsSet(self, &m_AtIpCoreOverride);
    }

static void Override(AtIpCore self)
    {
    OverrideAtIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051IpCore);
    }

static AtIpCore ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIpCorePwV2ObjectInit(self, coreId, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIpCore Tha60290051IpCoreNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCore, coreId, device);
    }

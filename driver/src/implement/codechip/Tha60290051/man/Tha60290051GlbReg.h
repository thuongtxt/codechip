/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0051_RD_GLB_H_
#define _AF6_REG_AF6CNC0051_RD_GLB_H_

/*--------------------------- Define -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : GLB Sub-Core Active
Reg Addr   : 0x00_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_Active_Base                                                                           0x000001

/*--------------------------------------
BitField Name: SubCoreEn
BitField Type: RW
BitField Desc: Enable per sub-core
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Active_SubCoreEn_Mask                                                                    cBit31_0
#define cAf6_Active_SubCoreEn_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Chip Temperature Sticky
Reg Addr   : 0x00_0020
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to sticky temperature

------------------------------------------------------------------------------*/
#define cAf6Reg_ChipTempSticky_Base                                                                   0x000020
#define cAf6Reg_ChipTempSticky_WidthVal                                                                     32

/*--------------------------------------
BitField Name: ChipSlr2TempStk
BitField Type: WC
BitField Desc: Chip SLR2 temperature
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ChipTempSticky_ChipSlr2TempStk_Mask                                                         cBit2
#define cAf6_ChipTempSticky_ChipSlr2TempStk_Shift                                                            2

/*--------------------------------------
BitField Name: ChipSlr1TempStk
BitField Type: WC
BitField Desc: Chip SLR1 temperature
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ChipTempSticky_ChipSlr1TempStk_Mask                                                         cBit1
#define cAf6_ChipTempSticky_ChipSlr1TempStk_Shift                                                            1

/*--------------------------------------
BitField Name: ChipSlr0TempStk
BitField Type: WC
BitField Desc: Chip SLR0 temperature
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ChipTempSticky_ChipSlr0TempStk_Mask                                                         cBit0
#define cAf6_ChipTempSticky_ChipSlr0TempStk_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug Mac Loopback Control
Reg Addr   : 0x00_0013
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure debug loopback MAC parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugMacLoopControl_Base                                                              0x000013
#define cAf6Reg_DebugMacLoopControl_WidthVal                                                                32

/*--------------------------------------
BitField Name: MacMroLoopOut
BitField Type: RW
BitField Desc: Set 1 for Mac 10G/1G/100Fx Loop Out
BitField Bits: [1]
--------------------------------------*/
#define cAf6_DebugMacLoopControl_MacMroLoopOut_Mask                                                      cBit1
#define cAf6_DebugMacLoopControl_MacMroLoopOut_Shift                                                         1

/*--------------------------------------
BitField Name: Mac40gLoopIn
BitField Type: RW
BitField Desc: Set 1 for Mac40gLoopIn
BitField Bits: [0]
--------------------------------------*/
#define cAf6_DebugMacLoopControl_Mac40gLoopIn_Mask                                                       cBit0
#define cAf6_DebugMacLoopControl_Mac40gLoopIn_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : GLB Eth1G Clock EER Squelching Control
Reg Addr   : 0x00_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure debug PW parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_GlbEth1GClkEerSquelControl_Base                                                       0x000010

/*--------------------------------------
BitField Name: EthGeExcessErrRateSchelEn
BitField Type: RW
BitField Desc: Each Bit represent for each ETH port, value each bit is as below
1: Disable 8Khz refout when GE Excessive Error Rate occur 0: Enable 8Khz refout
when GE Excessive Error Rate occur
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_GlbEth1GClkEerSquelControl_EthGeExcessErrRateSchelEn_Mask                                cBit31_0
#define cAf6_GlbEth1GClkEerSquelControl_EthGeExcessErrRateSchelEn_Shift                                       0


#define cAf6_EthGeExcessErrRateSchelEn_Mask(localId) (cBit0 << (localId))
#define cAf6_EthGeExcessErrRateSchelEn_Shift(localId) (localId)
/*------------------------------------------------------------------------------
Reg Name   : GLB Eth1G Clock Squelching Control
Reg Addr   : 0x00_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure debug PW parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_GlbEth1GClkLinkDownSquelControl_Base                                                  0x000011

/*--------------------------------------
BitField Name: EthGeLinkDownSchelEn
BitField Type: RW
BitField Desc: Each Bit represent for each ETH port, value each bit is as below
1: Disable 8Khz refout when GE link down occur 0: Enable 8Khz refout when GE
link down occur
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_GlbEth1GClkLinkDownSquelControl_EthGeLinkDownSchelEn_Mask                                cBit31_0
#define cAf6_GlbEth1GClkLinkDownSquelControl_EthGeLinkDownSchelEn_Shift                                       0

#define cAf6_EthGeLinkDownSchelEn_Mask(localId) (cBit0 << (localId))
#define cAf6_EthGeLinkDownSchelEn_Shift(localId) (localId)

/*------------------------------------------------------------------------------
Reg Name   : GLB Eth10G Clock Squelching Control
Reg Addr   : 0x00_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure debug PW parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_GlbEth10GClkSquelControl_Base                                                         0x000012
#define cAf6Reg_GlbEth10GClkSquelControl_WidthVal                                                           32

/*--------------------------------------
BitField Name: TenGeLocalFaultSchelEn
BitField Type: RW
BitField Desc: Each Bit represent for each TenGe port0 and port8, value each bit
is as below 1: Disable 8Khz refout when TenGe Local Fault occur 0: Enable 8Khz
refout when TenGe Local Fault occur
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_GlbEth10GClkSquelControl_TenGeLocalFaultSchelEn_Mask                                    cBit15_12
#define cAf6_GlbEth10GClkSquelControl_TenGeLocalFaultSchelEn_Shift                                          12
#define cAf6_TenGeLocalFaultSchelEn_Mask(local10g) (cBit12 << (local10g))
#define cAf6_TenGeLocalFaultSchelEn_Shift(local10g) (12UL + (local10g))

/*--------------------------------------
BitField Name: TenGeRemoteFaultSchelEn
BitField Type: RW
BitField Desc: Each Bit represent for each TenGe port0 and port8, value each bit
is as below 1: Disable 8Khz refout when TenGe Remote Fault occur 0: Enable 8Khz
refout when TenGe Remote Fault occur
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_GlbEth10GClkSquelControl_TenGeRemoteFaultSchelEn_Mask                                    cBit11_8
#define cAf6_GlbEth10GClkSquelControl_TenGeRemoteFaultSchelEn_Shift                                          8
#define cAf6_TenGeRemoteFaultSchelEn_Mask(local10g) (cBit8 << (local10g))
#define cAf6_TenGeRemoteFaultSchelEn_Shift(local10g) (8UL + (local10g))

/*--------------------------------------
BitField Name: TenGeLossDataSyncSchelEn
BitField Type: RW
BitField Desc: Each Bit represent for each TenGe port0 and port8, value each bit
is as below 1: Disable 8Khz refout when TenGe Loss of Data Sync occur 0: Enable
8Khz refout when TenGe Loss of Data Sync occur
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_GlbEth10GClkSquelControl_TenGeLossDataSyncSchelEn_Mask                                    cBit7_4
#define cAf6_GlbEth10GClkSquelControl_TenGeLossDataSyncSchelEn_Shift                                         4
#define cAf6_TenGeLossDataSyncSchelEn_Mask(local10g) (cBit4 << (local10g))
#define cAf6_TenGeLossDataSyncSchelEn_Shift(local10g) (4UL + (local10g))

/*--------------------------------------
BitField Name: TenGeExcessErrRateSchelEn
BitField Type: RW
BitField Desc: Each Bit represent for each TenGe port0 and port8, value each bit
is as below 1: Disable 8Khz refout when TenGe Excessive Error Rate occur 0:
Enable 8Khz refout when TenGe Excessive Error Rate occur
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_GlbEth10GClkSquelControl_TenGeExcessErrRateSchelEn_Mask                                   cBit3_0
#define cAf6_GlbEth10GClkSquelControl_TenGeExcessErrRateSchelEn_Shift                                        0
#define cAf6_TenGeExcessErrRateSchelEn_Mask(local10g) (cBit0 << (local10g))
#define cAf6_TenGeExcessErrRateSchelEn_Shift(local10g) (local10g)

#endif /* _AF6_REG_AF6CNC0051_RD_GLB_H_ */

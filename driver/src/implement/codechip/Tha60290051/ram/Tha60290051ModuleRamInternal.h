/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290051ModuleRamInternal.h
 * 
 * Created Date: Sep 6, 2018
 *
 * Description : Internal data of the 60290051 module RAM
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051MODULERAMINTERNAL_H_
#define _THA60290051MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/ram/Tha60290022ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051ModuleRam
    {
    tTha60290022ModuleRam super;
    }tTha60290051ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290051MODULERAMINTERNAL_H_ */


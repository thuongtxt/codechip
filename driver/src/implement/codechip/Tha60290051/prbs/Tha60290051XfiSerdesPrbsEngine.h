/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290051XfiSerdesPrbsEngine.h
 * 
 * Created Date: Jun 26, 2018
 *
 * Description : XFI SERDES 60290051 PRBS engine.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051XFISERDESPRBSENGINE_H_
#define _THA60290051XFISERDESPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290051XfiSerdesPrbsEngineNew(AtSerdesController serdesController, uint32 baseAddress);
AtTimer Tha60290051XfiSerdesPrbsGatingTimerNew(AtPrbsEngine prbsEngine);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051XFISERDESPRBSENGINE_H_ */


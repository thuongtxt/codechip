/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290051XfiSerdesPrbsEngine.c
 *
 * Created Date: Sep 26, 2018
 *
 * Description : XFI SERDES Framed PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/prbs/ThaPrbsCounters.h"
#include "../../Tha60290021/prbs/Tha6029XfiSerdesPrbsEngineInternal.h"
#include "Tha60290051ESXilinxXGMIIRDDIAGReg.h"
#include "Tha60290051XfiSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290051XfiSerdesPrbsEngine*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051XfiSerdesPrbsEngine
    {
    tTha6029XfiSerdesPrbsEngine super;
    }tTha60290051XfiSerdesPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtPrbsEngine self)
    {
    return Tha6029XfiSerdesPrbsEngineBaseAddress(self);
    }

static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return localAddress + BaseAddress(self);
    }

static uint32 CounterRegAddress(AtPrbsEngine self, uint16 counterType, uint32 *highAddress)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame    :
            *highAddress = cReg_itxpkhigh_Base;
            return cReg_itxpklow_Base;
        case cAtPrbsEngineCounterTxBit:
            *highAddress = cReg_itxbytehigh_Base;
            return cReg_itxbyteclow_Base;
        case cAtPrbsEngineCounterRxFrame    :
            *highAddress = cReg_iRXpkhigh_Base;
            return cReg_iRXpklow_Base;
        case cAtPrbsEngineCounterRxBit:
            *highAddress = cReg_iRXbytehigh_Base;
            return cReg_iRXbyteclow_Base;
        case cAtPrbsEngineCounterRxErrorFrame:
            *highAddress = cInvalidUint32;
            return cReg_iRX_Pkg_Error_Base;
        case cAtPrbsEngineCounterRxBitError:
            *highAddress = cInvalidUint32;
            return cReg_iRX_Data_Error_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint64 HwCounterRead(AtPrbsEngine self, uint16 counterType)
    {
    uint32 lowregAddr, highregAddr;
    uint32 lowVal = 0;
    uint32 highVal = 0;
    uint64 value;

    lowregAddr = CounterRegAddress(self, counterType, &highregAddr);
    if (lowregAddr == cInvalidUint32)
        return 0;

    lowVal = AtPrbsEngineRead(self, AddressWithLocalAddress(self, lowregAddr), cAtModulePrbs);
    value = lowVal;

    if (highregAddr == cInvalidUint32)
        return value;

    highVal = AtPrbsEngineRead(self, AddressWithLocalAddress(self, highregAddr), cAtModulePrbs);
    value = value + (((uint64)highVal) << 32);

    return value;
    }

static uint32 ControlRegAddress(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cReg_MiiEthTestControl_Base);
    }

static void StatusClear(AtPrbsEngine self)
    {
    uint32 address = ControlRegAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModulePrbs);

    mRegFieldSet(regVal, c_MiiEthTestControl_iclear_status_, 0);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    mRegFieldSet(regVal, c_MiiEthTestControl_iclear_status_, 1);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    mRegFieldSet(regVal, c_MiiEthTestControl_iclear_status_, 0);
    AtPrbsEngineWrite(self, address, regVal, cAtModulePrbs);
    }

static eAtRet CounterLatchAndClear(AtPrbsEngine self, uint16 counterType)
    {
    uint64 value = HwCounterRead(self, counterType);

    if ((counterType == cAtPrbsEngineCounterRxBit) ||
        (counterType == cAtPrbsEngineCounterTxBit))
        value = value * 8; /* Convert byte to bit */

    AtPrbsCountersCounterUpdate(AtPrbsEnginePrbsCountersGet(self), counterType, value);
    return cAtOk;
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    eAtRet ret = cAtOk;
    AtUnused(clear);
    ret |= CounterLatchAndClear(self, cAtPrbsEngineCounterTxFrame);
    ret |= CounterLatchAndClear(self, cAtPrbsEngineCounterRxFrame);
    ret |= CounterLatchAndClear(self, cAtPrbsEngineCounterTxBit);
    ret |= CounterLatchAndClear(self, cAtPrbsEngineCounterRxBit);
    ret |= CounterLatchAndClear(self, cAtPrbsEngineCounterRxErrorFrame);
    ret |= CounterLatchAndClear(self, cAtPrbsEngineCounterRxBitError);
    StatusClear(self);

    return ret;
    }

static uint64 Counter64BitGet(AtPrbsEngine self, uint16 counterType)
    {
    return AtPrbsCountersCounterGet(AtPrbsEnginePrbsCountersGet(self), counterType);
    }

static uint64 Counter64BitClear(AtPrbsEngine self, uint16 counterType)
    {
    return AtPrbsCountersCounterClear(AtPrbsEnginePrbsCountersGet(self), counterType);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtPrbsEngineCounterRxBit)   ||
        (counterType == cAtPrbsEngineCounterRxBitError) ||
        (counterType == cAtPrbsEngineCounterTxBit)   ||
        (counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame) ||
        (counterType == cAtPrbsEngineCounterRxErrorFrame))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet DefaultSet(AtPrbsEngine self)
    {
    uint32 regAddr;

    regAddr = ControlRegAddress(self);
    AtPrbsEngineWrite(self, regAddr, 0, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = ControlRegAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_MiiEthTestControl_iinsert_crc_, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = ControlRegAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (eBool)((regVal & c_MiiEthTestControl_iinsert_crc_Mask) ? cAtTrue : cAtFalse);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = ControlRegAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    eBool shouldReStart = cAtFalse;

    if ((mRegField(regVal, c_MiiEthTestControl_test_en_) == 0) && enable)
        shouldReStart = cAtTrue;

    mRegFieldSet(regVal, c_MiiEthTestControl_test_en_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    /* restart prbs */
    if (shouldReStart)
        {
        mRegFieldSet(regVal, c_MiiEthTestControl_irestart_diag_, 0);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        mRegFieldSet(regVal, c_MiiEthTestControl_irestart_diag_, 1);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        mRegFieldSet(regVal, c_MiiEthTestControl_irestart_diag_, 0);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = ControlRegAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & c_MiiEthTestControl_test_en_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = AtPrbsEngineEnable(self, cAtFalse);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_MiiStatusGlb_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 alarm = 0;

    if ((regVal & c_MiiStatusGlb_rx_data_err_Mask) ||
        (regVal & c_MiiStatusGlb_rx_packet_error_Mask))
        alarm = cAtPrbsEngineAlarmTypeLossSync;

    if ((regVal & c_MiiStatusGlb_rx_protocol_error_Mask) || ((regVal & c_MiiStatusGlb_link_sta_Mask) == 0))
        alarm |= cAtPrbsEngineAlarmTypeError;

    return alarm;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool r2c)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_MiiAlarmGlb_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 alarm = 0;
    if (r2c)
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    if ((regVal & c_MiiAlarmGlb_rx_data_err_Mask) ||
        (regVal & c_MiiAlarmGlb_rx_packet_error_Mask))
        alarm = cAtPrbsEngineAlarmTypeLossSync;

    if ((regVal & c_MiiAlarmGlb_rx_protocol_error_Mask)
        ||(regVal & c_MiiAlarmGlb_link_sta_Mask))
        alarm |= cAtPrbsEngineAlarmTypeError;

    if (r2c)
        StatusClear(self);

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32) prbsMode)
        {
        case cAtPrbsModePrbs31 : return cAtTrue;
        default: return cAtFalse;
        }
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = cAtOk;

    if (ModeIsSupported(self, prbsMode))
        return cAtOk;

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs31;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = cAtOk;

    if (ModeIsSupported(self, prbsMode))
        return cAtOk;

    return ret;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs31;
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = cAtOk;

    if (ModeIsSupported(self, prbsMode))
        return cAtOk;

    return ret;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs31;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    return Tha60290051XfiSerdesPrbsGatingTimerNew(self);
    }

static AtPrbsCounters CountersObjectCreate(AtPrbsEngine self)
    {
    AtUnused(self);
    return (AtPrbsCounters)ThaPrbsFrameCountersNew();
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(txpacketCounter);
    mEncodeNone(txbyteCounter);
    mEncodeNone(rxpacketCounter);
    mEncodeNone(rxbyteCounter);
    mEncodeNone(rxpacketerrorCounter);
    mEncodeNone(rxdataerrorCounter);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtPrbsEngineOverride, Counter64BitGet);
        mMethodOverride(m_AtPrbsEngineOverride, Counter64BitClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, CountersObjectCreate);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051XfiSerdesPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController, uint32 baseAddress)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029XfiSerdesPrbsEngineObjectInit(self, serdesController, baseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290051XfiSerdesPrbsEngineNew(AtSerdesController serdesController, uint32 baseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController, baseAddress);
    }

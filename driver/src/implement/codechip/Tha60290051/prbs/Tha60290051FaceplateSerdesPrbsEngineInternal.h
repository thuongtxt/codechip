/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290051FaceplateSerdesPrbsInternal.h
 * 
 * Created Date: Sep 7, 2018
 *
 * Description : internal data of the 60290051 faceplate serdes prbs engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051FACEPLATESERDESPRBSINTERNAL_H_
#define _THA60290051FACEPLATESERDESPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/prbs/Tha60290022FacePlateSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051FacePlateSerdesPrbsEngine
    {
    tTha60290022FacePlateSerdesPrbsEngine super;
    }tTha60290051FacePlateSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290051FACEPLATESERDESPRBSINTERNAL_H_ */


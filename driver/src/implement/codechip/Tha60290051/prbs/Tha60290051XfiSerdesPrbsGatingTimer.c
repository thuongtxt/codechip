/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6029PrbsGatingTimerSerdesTuning.c
 *
 * Created Date: Oct 1, 2018
 *
 * Description : PRBS gating timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/prbs/AtPrbsGatingTimerInternal.h"
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../Tha60290021/prbs/Tha6029SerdesPrbsEngine.h"
#include "Tha60290051ESXilinxXGMIIRDDIAGReg.h"
#include "Tha60290051XfiSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290051XfiSerdesPrbsGatingTimer*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290051XfiSerdesPrbsGatingTimer
    {
    tAtHwTimer super;

    /* Private data */
    AtPrbsEngine prbs;
    }tTha60290051XfiSerdesPrbsGatingTimer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtTimerMethods   m_AtTimerOverride;
static tAtHwTimerMethods m_AtHwTimerOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(AtHwTimer self, uint32 address, eAtModule module)
    {
    return AtPrbsEngineRead(mThis(self)->prbs, address, module);
    }

static void Write(AtHwTimer self, uint32 address, uint32 value, eAtModule module)
    {
    AtPrbsEngineWrite(mThis(self)->prbs, address, value, module);
    }

static uint32 BaseAddress(AtHwTimer self)
    {
    return Tha6029XfiSerdesPrbsEngineBaseAddress(mThis(self)->prbs);
    }

static uint32 AddressWithLocalAddress(AtHwTimer self, uint32 localAddress)
    {
    return localAddress + BaseAddress(self);
    }

static uint32 ControlRegAddress(AtHwTimer self)
    {
    return AddressWithLocalAddress(self, cReg_MiiEthTestControl_Base);
    }

static eBool IsSimulated(AtHwTimer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 HwDurationMax(AtHwTimer self)
    {
    AtUnused(self);
    return c_gatetime_cfg_time_cfg_Mask;
    }

static uint32 GateTimeDurationRegAddress(AtHwTimer self)
    {
    return AddressWithLocalAddress(self, cReg_gatetime_cfg_Base);
    }

static eAtRet HwDurationSet(AtHwTimer self, uint32 hwDuration)
    {
    uint32 regAddr = GateTimeDurationRegAddress(self);
    uint32 regVal = 0;

    if (hwDuration > mMethodsGet(self)->HwDurationMax(self))
        return cAtErrorOutOfRangParm;

    mRegFieldSet(regVal, c_gatetime_cfg_time_cfg_, hwDuration);
    Write(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static uint32 HwDurationGet(AtHwTimer self)
    {
    uint32 regAddr = GateTimeDurationRegAddress(self);
    uint32 duration = 0, regVal = 0;

    regVal = Read(self, regAddr, cAtModulePrbs);
    duration = mRegField(regVal, c_gatetime_cfg_time_cfg_);

    return duration;
    }

static uint32 GateTimeDurationStatusRegAddress(AtHwTimer self)
    {
    return AddressWithLocalAddress(self, cReg_Gatetime_current_Base);
    }

static uint32 HwElapsedTimeGet(AtHwTimer self)
    {
    uint32 regAddr = GateTimeDurationStatusRegAddress(self);
    uint32 regVal = Read(self, regAddr, cAtModulePrbs);
    return mRegField(regVal, c_Gatetime_current_currert_gatetime_diag_);
    }

static eAtRet HwStart(AtHwTimer self, eBool start)
    {
    uint32 regAddr = ControlRegAddress(self);
    uint32 regVal = Read(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_MiiEthTestControl_start_diag_auto_, 0);
    Write(self, regAddr, regVal, cAtModulePrbs);

    if (start)
        {
        mRegFieldSet(regVal, c_MiiEthTestControl_start_diag_auto_, 1);
        Write(self, regAddr, regVal, cAtModulePrbs);
        }

    return cAtOk;
    }

static eBool IsStarted(AtTimer self)
    {
    uint32 regAddr = ControlRegAddress((AtHwTimer)self);
    uint32 regVal = Read((AtHwTimer)self, regAddr, cAtModulePrbs);
    uint32 isStarted = mRegField(regVal, c_MiiEthTestControl_start_diag_auto_);

    if (isStarted)
        return cAtTrue;
    return cAtFalse;
    }

static eBool HwIsRunning(AtHwTimer self)
    {
    uint32 regAddr = GateTimeDurationStatusRegAddress(self);
    uint32 regVal = Read(self, regAddr, cAtModulePrbs);

    if (mRegField(regVal, c_Gatetime_current_status_gatetime_diag_))
        return cAtFalse;

    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290051XfiSerdesPrbsGatingTimer *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(prbs);
    }

static void OverrideAtObject(AtTimer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtHwTimer(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHwTimerOverride, mMethodsGet(timer), sizeof(m_AtHwTimerOverride));

        mMethodOverride(m_AtHwTimerOverride, Read);
        mMethodOverride(m_AtHwTimerOverride, Write);
        mMethodOverride(m_AtHwTimerOverride, IsSimulated);
        mMethodOverride(m_AtHwTimerOverride, HwDurationSet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationGet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationMax );
        mMethodOverride(m_AtHwTimerOverride, HwElapsedTimeGet);
        mMethodOverride(m_AtHwTimerOverride, HwStart);
        mMethodOverride(m_AtHwTimerOverride, HwIsRunning);
        }

    mMethodsSet(timer, &m_AtHwTimerOverride);
    }

static void OverrideTimer(AtTimer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtTimerOverride, mMethodsGet(self), sizeof(m_AtTimerOverride));

        mMethodOverride(m_AtTimerOverride, IsStarted);
        }

    mMethodsSet(self, &m_AtTimerOverride);
    }

static void Override(AtTimer self)
    {
    OverrideAtObject(self);
    OverrideTimer(self);
    OverrideAtHwTimer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051XfiSerdesPrbsGatingTimer);
    }

static AtTimer ObjectInit(AtTimer self, AtPrbsEngine prbsEngine)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtHwTimerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->prbs = prbsEngine;

    return self;
    }

AtTimer Tha60290051XfiSerdesPrbsGatingTimerNew(AtPrbsEngine prbsEngine)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtTimer newTimer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newTimer, prbsEngine);
    }

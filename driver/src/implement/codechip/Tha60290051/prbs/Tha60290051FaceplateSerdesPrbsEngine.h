/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290051FacepateSerdesPrbsEngine.h
 * 
 * Created Date: Sep 7, 2018
 *
 * Description : Interface of the 60290051 faceplate serdes prbs engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051FACEPLATESERDESPRBSENGINE_H_
#define _THA60290051FACEPLATESERDESPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290051FaceplateSerdesPrbsEngineNew(AtSerdesController serdesController);/* frame prbs */
AtTimer Tha60290051FaceplateSerdesPrbsGatingTimerNew(AtPrbsEngine engine);
AtPrbsEngine Tha60290051FaceplateSerdesRawPrbsEngineNew(AtSerdesController serdesController); /* raw */

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051FACEPLATESERDESPRBSENGINE_H_ */


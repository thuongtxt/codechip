/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290051FaceplateEthPortPrbsEngine.h
 * 
 * Created Date: Sep 28, 2018
 *
 * Description : Faceplate eth port prbs interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051FACEPLATEETHPORTPRBSENGINE_H_
#define _THA60290051FACEPLATEETHPORTPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051FaceplateEthPortPrbsEngine *Tha60290051FaceplateEthPortPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290051FaceplateEthPortPrbsEngineNew(AtEthPort ethPort);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051FACEPLATEETHPORTPRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290051FaceplateEthPortPrbsEngine.c
 *
 * Created Date: Sep 28, 2018
 *
 * Description : Implementation for the faceplate eth port prbs engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../Tha60290021/eth/Tha60290021ModuleEth.h"
#include "Tha60290051FaceplateEthPortPrbsEngineInternal.h"
#include "../eth/Tha60290051ETHPASSReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290051FaceplateEthPortPrbsEngine)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule Module(AtPrbsEngine self, eAtModule module)
    {
    return AtDeviceModuleGet(AtPrbsEngineDeviceGet(self), module);
    }

static uint32 Read(AtPrbsEngine self, uint32 address, eAtModule module)
    {
    return mModuleHwRead(Module(self, module), address);
    }

static void Write(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module)
    {
    mModuleHwWrite(Module(self, module), address, value);
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static uint32 BaseAddress(AtPrbsEngine self)
    {
    AtUnused(self);
    return Tha60290021ModuleEthBypassBaseAddress();
    }

static uint32 Offset(AtPrbsEngine self)
    {
    return AtPrbsEngineIdGet(self);
    }

static uint32 AbsoluteOffset(AtPrbsEngine self)
    {
    return Offset(self) + BaseAddress(self);
    }

static uint32 EngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return AbsoluteOffset(self) + localAddress;
    }

static uint32 GlbAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return BaseAddress(self) + localAddress;
    }

static uint32 CounterLocalAddress(uint16 counterType, eBool r2c)
    {
    AtUnused(r2c);
    switch (counterType)
        {
        case cAtPrbsEngineCounterRxBitError: return cAf6Reg_ramdiag6_Base;
        case cAtPrbsEngineCounterRxFrame   : return cAf6Reg_ramdiag7_Base;
        case cAtPrbsEngineCounterRxBit: return cAf6Reg_ramdiag8_Base;
        default:
            return cInvalidUint32;
        }
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (CounterLocalAddress(counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddr = EngineAddressWithLocalAddress(self, CounterLocalAddress(counterType, read2Clear));
    uint32 counter = 0;

    if (regAddr == cInvalidUint32)
        return 0;

    counter = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    if (read2Clear)
        AtPrbsEngineWrite(self, regAddr, 0, cAtModuleEth);

    if (counterType == cAtPrbsEngineCounterRxBit)
        counter = counter * 8;

    return counter;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 errorCounter = CounterRead2Clear(self, cAtPrbsEngineCounterRxBitError, read2Clear);
    uint32 bitCounter = CounterRead2Clear(self, cAtPrbsEngineCounterRxBit, read2Clear);
    uint32 alarm = 0;

    if (errorCounter == 0 && bitCounter > 0)
        alarm = cAtPrbsEngineAlarmTypeNone;
    else
        alarm = cAtPrbsEngineAlarmTypeError;

    return alarm;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 ModeSw2Hw(eAtPrbsMode hwMode)
    {
    switch ((uint32)hwMode)
        {
        case cAtPrbsModePrbsSeq                : return 0;
        case cAtPrbsModePrbsFixedPattern1Byte  : return 1;
        default:
            return 0;
        }
    }

static eAtPrbsMode ModeHw2Sw(uint32 hwMode)
    {
    switch (hwMode)
        {
        case 0 : return cAtPrbsModePrbsSeq;
        case 1 : return cAtPrbsModePrbsFixedPattern1Byte;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static uint32 ModeGenAddress(AtPrbsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_ramdiag4_Base);
    }

static uint32 ModeMonAddress(AtPrbsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_ramdiag5_Base);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = ModeGenAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ramdiag4_DiagGenDatMode_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = ModeGenAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return ModeHw2Sw(mRegField(regVal, cAf6_ramdiag4_DiagGenDatMode_));
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = ModeMonAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ramdiag5_DiagMonDatMode_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = ModeMonAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return ModeHw2Sw(mRegField(regVal, cAf6_ramdiag5_DiagMonDatMode_));
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr = ModeGenAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ramdiag4_DiagGenFixPat_, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr = ModeGenAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return ModeHw2Sw(mRegField(regVal, cAf6_ramdiag4_DiagGenFixPat_));
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr = ModeMonAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ramdiag5_DiagMonFixPat_, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr = ModeMonAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return ModeHw2Sw(mRegField(regVal, cAf6_ramdiag5_DiagMonFixPat_));
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    TxModeSet(self, prbsMode);
    RxModeSet(self, prbsMode);
    return cAtOk;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return TxModeGet(self);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    TxFixedPatternSet(self, fixedPattern);
    RxFixedPatternSet(self, fixedPattern);
    return cAtOk;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return TxFixedPatternGet(self);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbsSeq               : return cAtTrue;
        case cAtPrbsModePrbsFixedPattern1Byte : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 EngineIdByEthPort(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet(channel);
    uint8 portId = (uint8)AtChannelIdGet(channel);
    uint32 engineId = Tha60290021ModuleEthFaceplateLocalId(module, portId);
    return engineId;
    }

static uint32 EngineEnableAddress(AtPrbsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_ramdiag1_Base);
    }

static uint32 EngineEnableAddressById(AtPrbsEngine self, uint32 Id)
    {
    return GlbAddressWithLocalAddress(self, (cAf6Reg_ramdiag1_Base + Id));
    }

static uint32 EngineEnableAddressByEthPort(AtPrbsEngine self)
    {
    AtEthPort ethPort = (AtEthPort)AtPrbsEngineChannelGet(self);
    uint32 Id = EngineIdByEthPort(ethPort);
    return GlbAddressWithLocalAddress(self, (cAf6Reg_ramdiag1_Base + Id));
    }

static uint32 GlbEnableAddress(AtPrbsEngine self)
    {
    return GlbAddressWithLocalAddress(self, cAf6Reg_upen_epadiagglb_Base);
    }

static uint32 GroupOf8PortIndex(AtPrbsEngine self)
    {
    uint32 engineId = AtPrbsEngineIdGet(self);
    return (uint32)(engineId / 8);
    }

static eAtModulePrbsRet Hw10GEnable(AtPrbsEngine self, eBool enable)
    {

    uint32 regAddr = GlbEnableAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 enField_Mask = cAf6_upen_epadiagglb_DiagTenGPort00_En_Mask << GroupOf8PortIndex(self);
    uint32 enField_Shift = cAf6_upen_epadiagglb_DiagTenGPort00_En_Shift + GroupOf8PortIndex(self);

    mRegFieldSet(regVal, enField_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModulePrbsRet HwGlbEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = GlbEnableAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_upen_epadiagglb_DiagSelect_En_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool Is10GPort(AtPrbsEngine self)
    {
    AtEthPort ethPort = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtSerdesController serdes = AtEthPortSerdesController(ethPort);
    eAtSerdesMode serdesMode = AtSerdesControllerModeGet(serdes);

    if (serdesMode == cAtSerdesModeEth10G)
        return cAtTrue;
    return cAtFalse;
    }

static eBool AtLeastOnePrbsIsEnabled(AtPrbsEngine self)
    {
    AtEthPort ethPort = (AtEthPort)AtPrbsEngineChannelGet(self);
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)ethPort);
    uint32 numFaceplatePorts = Tha60290021ModuleEthNumFaceplatePorts(module);
    uint32 id = 0, address = 0, regVal = 0;

    for (id = 0; id < numFaceplatePorts; id++)
        {
        address = EngineEnableAddressById(self, id);
        regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
        if ((regVal & cAf6_ramdiag1_DiagPkGen_En_Mask))
            return cAtTrue;
        }
    return cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = EngineEnableAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ramdiag1_DiagPkGen_En_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    if (Is10GPort(self))
        Hw10GEnable(self, enable);
    else
        Hw10GEnable(self, cAtFalse);

    HwGlbEnable(self, AtLeastOnePrbsIsEnabled(self));

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = EngineEnableAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ramdiag1_DiagPkGen_En_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 SwSpeedToHwSystemNco(eAtEthPortSpeed speed)
    {
    if (speed == cAtEthPortSpeed10M)
        return 3888;
    if (speed == cAtEthPortSpeed100M)
        return 1944;
    if (speed == cAtEthPortSpeed1000M)
        return 972;
    if (speed == cAtEthPortSpeed10G)
        return 972;
    return 0;
    }

static uint32 SwSpeedAndBandwidthToHwNco(eAtEthPortSpeed speed, uint32 percentBandwidth)
    {
    if (percentBandwidth > 100)
        percentBandwidth = 100;

    if (percentBandwidth == 0)
        return 0;

    if (speed == cAtEthPortSpeed10M)
        {
        uint32 hwVal = (uint32)((percentBandwidth * 25) / 100 );
        return hwVal == 0 ? 1 : hwVal;
        }

    if (speed == cAtEthPortSpeed100M)
        return ((percentBandwidth * 125) / 100);

    if (speed == cAtEthPortSpeed1000M)
        return ((percentBandwidth * 625) / 100);

    if (speed == cAtEthPortSpeed10G)
        return ((percentBandwidth * 6250) / 100);

    return 0;
    }

static uint32 HwSpeedAndBandwidthToPercentSwSpeed(eAtEthPortSpeed speed, uint32 hwBandwidth)
    {
    if (hwBandwidth == 0)
        return 0;

    if (speed == cAtEthPortSpeed10M)
        return (uint32)((hwBandwidth * 100) / 25 );

    if (speed == cAtEthPortSpeed100M)
        return ((hwBandwidth * 100) / 125);

    if (speed == cAtEthPortSpeed1000M)
        return ((hwBandwidth * 100) / 625);

    if (speed == cAtEthPortSpeed10G)
        return ((hwBandwidth * 100) / 6250);

    return 0;
    }

static eAtModulePrbsRet SpeedSet(AtPrbsEngine self, eAtEthPortSpeed speed)
    {
    uint32 regAddr = EngineEnableAddressByEthPort(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ramdiag1_DiagSysNcoSpeed_, SwSpeedToHwSystemNco(speed));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModulePrbsRet BandwidthSet(AtPrbsEngine self, eAtEthPortSpeed speed, uint32 percentBandwidth)
    {
    uint32 regAddr = EngineEnableAddressByEthPort(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ramdiag1_DiagEthNcoSpeed_, SwSpeedAndBandwidthToHwNco(speed, percentBandwidth));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModulePrbsRet BandwidthGet(AtPrbsEngine self, eAtEthPortSpeed speed)
    {
    uint32 regAddr = EngineEnableAddressByEthPort(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 hwBandwidth = mRegField(regVal, cAf6_ramdiag1_DiagEthNcoSpeed_);

    return HwSpeedAndBandwidthToPercentSwSpeed(speed, hwBandwidth);
    }

static uint32 BurstByteAddress(AtPrbsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_ramdiag2_Base);
    }

static eAtRet BurstByteSet(AtPrbsEngine self, uint32 burstBytes) /*20 bits burst bytes*/
    {
    uint32 regAddr = BurstByteAddress(self);

    if (burstBytes > cAf6_ramdiag2_DiagBurstByte_Mask)
        return cAtErrorOutOfRangParm;

    AtPrbsEngineWrite(self, regAddr, burstBytes, cAtModuleEth);
    return cAtOk;
    }

static uint32 BurstByteGet(AtPrbsEngine self) /*20 bits burst bytes*/
    {
    uint32 regAddr = BurstByteAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    return regVal;
    }

static eBool BandwidthControlIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePrbsRet BurstSizeSet(AtPrbsEngine self, uint32 burstSizeInBytes)
    {
    return BurstByteSet(self, burstSizeInBytes);
    }

static uint32 BurstSizeGet(AtPrbsEngine self)
    {
    return BurstByteGet(self);
    }

static eAtModulePrbsRet PercentageBandwidthSet(AtPrbsEngine self, uint32 percentageOfBandwith)
    {
    AtEthPort ethPort = (AtEthPort)AtPrbsEngineChannelGet(self);
    eAtEthPortSpeed speed = AtEthPortSpeedGet(ethPort);
    return BandwidthSet(self, speed, percentageOfBandwith);
    }

static uint32 PercentageBandwidthGet(AtPrbsEngine self)
    {
    AtEthPort ethPort = (AtEthPort)AtPrbsEngineChannelGet(self);
    eAtEthPortSpeed speed = AtEthPortSpeedGet(ethPort);
    return BandwidthGet(self, speed);
    }

static uint32 LengthModeSw2Hw(eAtPrbsPayloadLengthMode payloadLenMode)
    {
    switch (payloadLenMode)
        {
        case cAtPrbsLengthModeFixed: return 0;
        case cAtPrbsLengthModeIncreased: return 1;
        case cAtPrbsLengthModeDecreased: return 2;
        case cAtPrbsLengthModeRandom: return 3;
        default: return 0;
        }
    }

static uint32 LengthModeHw2Sw(uint32 payloadLenMode)
    {
    switch (payloadLenMode)
        {
        case 0: return cAtPrbsLengthModeFixed;
        case 1: return cAtPrbsLengthModeIncreased;
        case 2: return cAtPrbsLengthModeDecreased;
        case 3: return cAtPrbsLengthModeRandom;
        default: return cAtPrbsLengthModeFixed;
        }
    }

static eAtModulePrbsRet PayloadLenghtModeSet(AtPrbsEngine self, eAtPrbsPayloadLengthMode payloadLenMode)
    {
    uint32 address = ModeGenAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ramdiag4_DiagGenLenMode_, LengthModeSw2Hw(payloadLenMode));
    mRegFieldSet(regVal, cAf6_ramdiag4_DiagGenNonPldByte_, 0x1A);
    AtPrbsEngineWrite(self, address, regVal, cAtModuleEth);

    address = ModeMonAddress(self);
    regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ramdiag5_DiagMonLenMode_, LengthModeSw2Hw(payloadLenMode));
    AtPrbsEngineWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtPrbsPayloadLengthMode PayloadLengthModeGet(AtPrbsEngine self)
    {
    uint32 address = ModeGenAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    uint32 hwLenMode = mRegField(regVal, cAf6_ramdiag4_DiagGenLenMode_);

    return LengthModeHw2Sw(hwLenMode);
    }

static uint32 LengthAddress(AtPrbsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_ramdiag3_Base);
    }

#define cFcsLen 4UL

static eAtModulePrbsRet MinLengthSet(AtPrbsEngine self, uint32 minLength)
    {
    uint32 address = LengthAddress(self);
    uint32 regVal = 0;

    if ((minLength < cFcsLen) || ((minLength + cFcsLen)> cAf6_ramdiag3_DiagGenMinLen_Mask))
        return cAtErrorOutOfRangParm;

    regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    minLength -= cFcsLen;
    mRegFieldSet(regVal, cAf6_ramdiag3_DiagGenMinLen_, minLength);
    AtPrbsEngineWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 MinLengthGet(AtPrbsEngine self)
    {
    uint32 address = LengthAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    uint32 minLength = mRegField(regVal, cAf6_ramdiag3_DiagGenMinLen_);

    minLength += cFcsLen;

    return minLength;
    }

static eAtModulePrbsRet MaxLengthSet(AtPrbsEngine self, uint32 maxLength)
    {
    uint32 address = LengthAddress(self);
    uint32 regVal = 0;
    uint32 minLength = MinLengthGet(self);
    uint32 offsetMaxLen = (cAf6_ramdiag3_DiagGenOffLen_Mask >> cAf6_ramdiag3_DiagGenOffLen_Shift);
    if ((maxLength < minLength) || (maxLength > (minLength + offsetMaxLen)))
        return cAtErrorOutOfRangParm;

    maxLength -= cFcsLen;
    offsetMaxLen = (uint32)(maxLength - minLength);
    regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ramdiag3_DiagGenOffLen_, offsetMaxLen);
    AtPrbsEngineWrite(self, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 MaxLengthGet(AtPrbsEngine self)
    {
    uint32 address = LengthAddress(self);
    uint32 regVal = 0;
    uint32 minLength = MinLengthGet(self);
    uint32 offsetMaxLen = 0, maxLength = 0;

    regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    offsetMaxLen = mRegField(regVal, cAf6_ramdiag3_DiagGenOffLen_);
    maxLength = (uint32)(offsetMaxLen + minLength);
    return maxLength;
    }

static eAtRet DefaultSet(AtPrbsEngine self)
    {
    AtEthPort ethPort = (AtEthPort)AtPrbsEngineChannelGet(self);
    eAtEthPortSpeed speed = AtEthPortSpeedGet(ethPort);
    eAtRet ret = SpeedSet(self, speed);
    ret |= BandwidthSet(self, speed, 100); /*100%s bandwidth*/
    ret |= BurstByteSet(self, 0);
    ret |= MinLengthSet(self, 0x80);
    ret |= MaxLengthSet(self, 0x80);
    ret |= PayloadLenghtModeSet(self, cAtPrbsLengthModeFixed);

    return ret;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = AtPrbsEngineEnable(self, cAtFalse);
    ret |= DefaultSet(self);
    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Read);
        mMethodOverride(m_AtPrbsEngineOverride, Write);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);

        mMethodOverride(m_AtPrbsEngineOverride, BandwidthControlIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, PayloadLenghtModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, PayloadLengthModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, PercentageBandwidthSet);
        mMethodOverride(m_AtPrbsEngineOverride, PercentageBandwidthGet);
        mMethodOverride(m_AtPrbsEngineOverride, MinLengthSet);
        mMethodOverride(m_AtPrbsEngineOverride, MinLengthGet);
        mMethodOverride(m_AtPrbsEngineOverride, MaxLengthSet);
        mMethodOverride(m_AtPrbsEngineOverride, MaxLengthGet);
        mMethodOverride(m_AtPrbsEngineOverride, BurstSizeSet);
        mMethodOverride(m_AtPrbsEngineOverride, BurstSizeGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051FaceplateEthPortPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtEthPort ethPort)
    {
    AtChannel channel = (AtChannel)ethPort;
    uint32 engineId = EngineIdByEthPort(ethPort);
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290051FaceplateEthPortPrbsEngineNew(AtEthPort ethPort)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, ethPort);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290051FaceplateSerdesPrbsEngine.c
 *
 * Created Date: Sep 7, 2018
 *
 * Description : Implementation for the 60290051 faceplate serdes prbs engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "Tha60290051FaceplateSerdesPrbsEngineInternal.h"
#include "../physical/diag/Tha60290051TOPGLBReg.h"
#include "Tha60290051FaceplateSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#define cRaw16Prbs7  0x0
#define cRaw16Prbs23 0x1
#define cRaw16Prbs31 0x2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return cAf6Reg_ProductID_Base;
    }

static uint32 PortId(AtPrbsEngine self)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    return AtSerdesControllerHwIdGet(controller);
    }

static uint32 ErrInsShift(AtPrbsEngine self)
    {
    return PortId(self);
    }

static uint32 ErrInsMask(AtPrbsEngine self)
    {
    return cBit0 << PortId(self);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = cAf6Reg_o_control5_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 errInsFieldMask  = ErrInsMask(self);
    uint32 errInsFieldShift = ErrInsShift(self);
    mRegFieldSet(regVal, errInsField, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = cAf6Reg_o_control5_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & ErrInsMask(self)) ? cAtTrue : cAtFalse;
    }

static uint32 EnableShift(AtPrbsEngine self)
    {
    return PortId(self);
    }

static uint32 EnableMask(AtPrbsEngine self)
    {
    return cBit0 << PortId(self);
    }

static eAtModulePrbsRet RawPrbsEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_o_control4_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 enableFieldMask  = EnableMask(self);
    uint32 enableFieldShift = EnableShift(self);
    mRegFieldSet(regVal, enableField, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eBool RawPrbsIsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = cAf6Reg_o_control4_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & EnableMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    RawPrbsEnable(self, enable);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return RawPrbsIsEnabled(self);
    }

static uint32 NumberPortInGroup(void)
    {
    return 16;
    }

static uint32 SubPortInGroup(AtPrbsEngine self)
    {
    return PortId(self) % NumberPortInGroup();
    }

static uint32 PrbsControlAddress(AtPrbsEngine self)
    {
    if ((PortId(self) / NumberPortInGroup()) == 0)
        return cAf6Reg_o_control11_Base0 + BaseAddress();
    return cAf6Reg_o_control11_Base1 + BaseAddress();
    }

static uint32 ModeShift(AtPrbsEngine self)
    {
    return SubPortInGroup(self) * 2;
    }

static uint32 ModeMask(AtPrbsEngine self)
    {
    return cBit1_0 << (SubPortInGroup(self) * 2);
    }

static eAtModulePrbsRet PrbsHwModeSet(AtPrbsEngine self, uint32 mode)
    {
    uint32 regAddr = PrbsControlAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 modeFieldMask  = ModeMask(self);
    uint32 modeFieldShift = ModeShift(self);
    mRegFieldSet(regVal, modeField, mode);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 PrbsHwModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = PrbsControlAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 modeMask  = ModeMask(self);
    uint32 modeShift = ModeShift(self);
    return mRegField(regVal, mode);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32) prbsMode)
        {
        case cAtPrbsModePrbs7  : return cAtTrue;
        case cAtPrbsModePrbs15 : return cAtFalse;
        case cAtPrbsModePrbs23 : return cAtTrue;
        case cAtPrbsModePrbs31 : return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 LowRatePrbsModeSw2Hw(eAtPrbsMode mode)
    {
    switch ((uint32) mode)
        {
        case cAtPrbsModePrbs7 : return cRaw16Prbs7;
        case cAtPrbsModePrbs23: return cRaw16Prbs23;
        case cAtPrbsModePrbs31: return cRaw16Prbs31;
        default:
            return cRaw16Prbs7;
        }
    }

static eAtPrbsMode LowRatePrbsModeHw2Sw(AtPrbsEngine self, uint32 hwMode)
    {
    AtUnused(self);
    switch (hwMode)
        {
        case cRaw16Prbs7 : return cAtPrbsModePrbs7;
        case cRaw16Prbs23: return cAtPrbsModePrbs23;
        case cRaw16Prbs31: return cAtPrbsModePrbs31;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static uint32 PrbsModeSw2Hw(AtPrbsEngine self, eAtPrbsMode mode)
    {
    AtUnused(self);
    return LowRatePrbsModeSw2Hw(mode);
    }

static eAtPrbsMode PrbsModeHw2Sw(AtPrbsEngine self, uint32 hwMode)
    {
    AtUnused(self);
    return LowRatePrbsModeHw2Sw(self, hwMode);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    return PrbsHwModeSet(self, PrbsModeSw2Hw(self, prbsMode));
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 hwMode = PrbsHwModeGet(self);
    return PrbsModeHw2Sw(self, hwMode);
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    return Tha60290051FaceplateSerdesPrbsGatingTimerNew(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051FacePlateSerdesPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FacePlateSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290051FaceplateSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

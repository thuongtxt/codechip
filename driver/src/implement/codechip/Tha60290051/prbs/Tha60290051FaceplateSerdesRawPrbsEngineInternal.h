/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290051FaceplateSerdesRawPrbsEngineInternal.h
 * 
 * Created Date: Nov 2, 2018
 *
 * Description : Faceplate SERDES raw PRBS engine.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051FACEPLATESERDESRAWPRBSENGINEINTERNAL_H_
#define _THA60290051FACEPLATESERDESRAWPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/prbs/Tha60290022FacePlateSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051FacePlateSerdesRawPrbsEngine
    {
    tTha60290022FacePlateSerdesPrbsEngine super;
    }tTha60290051FacePlateSerdesRawPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290051FACEPLATESERDESRAWPRBSENGINEINTERNAL_H_ */


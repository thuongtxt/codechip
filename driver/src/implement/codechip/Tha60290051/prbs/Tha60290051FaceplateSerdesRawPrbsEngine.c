/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290051FacePlateSerdesDrawPrbsEngine.c
 *
 * Created Date: Oct 2, 2018
 *
 * Description : Eth FacePlate SERDES PRBS engine
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../Tha60290021/physical/Tha6029FaceplateSerdesController.h"
#include "../physical/diag/Tha60290051MrEth16chwrapReg.h"
#include "Tha60290051FaceplateSerdesPrbsEngine.h"
#include "Tha60290051FaceplateSerdesRawPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290051FacePlateSerdesRawPrbsEngine*)(self))
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);

    return Tha6029FaceplateSerdesControllerAddressWithLocalAddress(controller, localAddress);
    }

static uint32 PortId(AtPrbsEngine self)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    return AtSerdesControllerHwIdGet(controller);
    }

static uint32 NumberPortIn4PortGroup(void)
    {
    return 4;
    }

static uint32 NumberPortIn16PortGroup(void)
    {
    return 16;
    }

static uint32 SubPortIn4PortGroup(AtPrbsEngine self)
    {
    return (PortId(self) % NumberPortIn16PortGroup()) % NumberPortIn4PortGroup();
    }

static uint32 ModeShift(AtPrbsEngine self)
    {
    return (SubPortIn4PortGroup(self) * 4);
    }

static uint32 ModeMask(AtPrbsEngine self)
    {
    return cBit3_0 << (SubPortIn4PortGroup(self) * 4);
    }

static uint32 ErrInsShift(AtPrbsEngine self)
    {
    return SubPortIn4PortGroup(self);
    }

static uint32 ErrInsMask(AtPrbsEngine self)
    {
    return cBit0 << SubPortIn4PortGroup(self);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_FORCE_ERROR_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 errInsFieldMask  = ErrInsMask(self);
    uint32 errInsFieldShift = ErrInsShift(self);
    mRegFieldSet(regVal, errInsField, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_FORCE_ERROR_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & ErrInsMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet PrbsHwModeSet(AtPrbsEngine self, uint32 mode)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_SEL_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 modeFieldMask  = ModeMask(self);
    uint32 modeFieldShift = ModeShift(self);
    mRegFieldSet(regVal, modeField, mode);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static uint32 PrbsHwModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_SEL_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 modeMask  = ModeMask(self);
    uint32 modeShift = ModeShift(self);
    return mRegField(regVal, mode);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32) prbsMode)
        {
        case cAtPrbsModePrbs7  : return cAtTrue;
        case cAtPrbsModePrbs9  : return cAtTrue;
        case cAtPrbsModePrbs15 : return cAtTrue;
        case cAtPrbsModePrbs23 : return cAtTrue;
        case cAtPrbsModePrbs31 : return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 LowRatePrbsModeSw2Hw(eAtPrbsMode mode)
    {
    switch ((uint32) mode)
        {
        case cAtPrbsModePrbs7 : return 1;
        case cAtPrbsModePrbs9 : return 2;
        case cAtPrbsModePrbs15: return 3;
        case cAtPrbsModePrbs23: return 4;
        case cAtPrbsModePrbs31: return 5;
        default:
            return 1;
        }
    }

static uint32 PrbsModeSw2Hw(AtPrbsEngine self, eAtPrbsMode mode)
    {
    AtUnused(self);
    return LowRatePrbsModeSw2Hw(mode);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    ((Tha60290021FacePlateSerdesPrbsEngine)self)->mode = prbsMode;
    return PrbsHwModeSet(self, PrbsModeSw2Hw(self, prbsMode));
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return ((Tha60290021FacePlateSerdesPrbsEngine)self)->mode;
    }

static eAtRet TenGitGearBoxControlForRawPrbsHandle(AtPrbsEngine self, eBool enable)
    {
    AtSerdesController serdes = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);

    if (AtSerdesControllerLoopbackGet(serdes) == cAtLoopbackModeRemote)
        return cAtOk;

    return Tha6029FaceplateSerdesGearBoxEnable(serdes, enable ? cAtFalse : cAtTrue);
    }

static uint32 TxPcsResetBitfieldMask(AtPrbsEngine self)
    {
    return ErrInsMask(self);
    }

static uint32 TxPcsResetBitfieldShift(AtPrbsEngine self)
    {
    return ErrInsShift(self);
    }

static eAtRet TxPcsResetCfg(AtPrbsEngine self, uint32 value)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_TXPCS_RESET_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 pcsResetField_Mask  = TxPcsResetBitfieldMask(self);
    uint32 pcsResetField_Shift = TxPcsResetBitfieldShift(self);

    mRegFieldSet(regVal, pcsResetField_, value);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtRet TxBufEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_Async_GearBox_Enable_of_10Ge_mode_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_Async_GearBox_Enable_of_10Ge_mode_TXBUF_EN_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static uint32 TxResetDoneBitfieldMask(AtPrbsEngine self)
    {
    uint32 mask = (uint32)(ErrInsMask(self) << c_Mr_Eth_TX_Reset_txrst_done_Shift);
    return mask;
    }

static uint32 TxResetDoneBitfieldShift(AtPrbsEngine self)
    {
    uint32 shift = (uint32)(ErrInsShift(self) + c_Mr_Eth_TX_Reset_txrst_done_Shift);
    return shift;
    }

static eAtRet TxResetDoneClear(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_Mr_Eth_TX_Reset_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 txresetDone_Mask = TxResetDoneBitfieldMask(self);
    uint32 txresetDone_Shift = TxResetDoneBitfieldShift(self);

    mRegFieldSet(regVal, txresetDone_, 1);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool TxResetIsDone(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_Mr_Eth_TX_Reset_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 txresetDone_Mask = TxResetDoneBitfieldMask(self);
    uint32 txresetDone_Shift = TxResetDoneBitfieldShift(self);
    uint32 isDone = 0;

    isDone = mRegField(regVal, txresetDone_);
    return (eBool) (isDone ? cAtTrue : cAtFalse);
    }

static AtDevice DeviceGet(AtPrbsEngine self)
    {
    return AtPrbsEngineDeviceGet(self);
    }

static eAtRet TxResetDoneWait(AtPrbsEngine self)
    {
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;
    uint32 cTimeoutMs = 500;

    if (AtDeviceWarmRestoreIsStarted(DeviceGet(self)))
        return cAtOk;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        if (TxResetIsDone(self))
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        if (AtDeviceIsSimulated(DeviceGet(self)))
            return cAtOk;
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtModulePrbsRet TenGitEnableWithSequence(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= TxResetDoneClear(self);
    ret |= TxPcsResetCfg(self, 1);
    ret |= TenGitGearBoxControlForRawPrbsHandle(self, enable);

    if (enable)
        {
        ret |= TxBufEnable(self, cAtTrue);
        ret |= PrbsHwModeSet(self, PrbsModeSw2Hw(self, ((Tha60290021FacePlateSerdesPrbsEngine)self)->mode));
        }
    else
        {
        ret |= TxBufEnable(self, cAtFalse);
        ret |= PrbsHwModeSet(self, 0);
        }

    ret |= TxPcsResetCfg(self, 0);
    ret |= TxResetDoneWait(self);
    return ret;
    }

static eAtModulePrbsRet OneGitEnable(AtPrbsEngine self, eBool enable)
    {
    if (enable)
        return PrbsHwModeSet(self, PrbsModeSw2Hw(self, ((Tha60290021FacePlateSerdesPrbsEngine)self)->mode));

    return PrbsHwModeSet(self, 0);
    }

static uint32 RxPpmMonitorControlBitfieldMask(AtPrbsEngine self)
    {
    uint32 mask = ErrInsMask(self);
    return mask;
    }

static uint32 RxPpmMonitorControlBitfieldShift(AtPrbsEngine self)
    {
    uint32 shift = ErrInsShift(self);
    return shift;
    }

static eAtRet RxPpmMonitorControlEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_RX_Monitor_PPM_Control_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 rxPpmControlEnable_Mask = RxPpmMonitorControlBitfieldMask(self);
    uint32 rxPpmControlEnable_Shift = RxPpmMonitorControlBitfieldShift(self);

    mRegFieldSet(regVal, rxPpmControlEnable_, (enable ? 1 : 0));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    AtSerdesController serdes = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    eAtSerdesMode serdesMode = AtSerdesControllerModeGet(serdes);
    eAtRet ret = cAtOk;

    if (serdesMode == cAtSerdesModeEth10G)
        ret = TenGitEnableWithSequence(self, enable);
    else
        ret = OneGitEnable(self, enable);

    ret |= RxPpmMonitorControlEnable(self, !enable);
    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 hwMode = PrbsHwModeGet(self);

    if (hwMode == 0)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 AlarmMask(AtPrbsEngine self)
    {
    return cBit0 << SubPortIn4PortGroup(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_ERROR_STA_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    if (regVal & AlarmMask(self))
        return cAtPrbsEngineAlarmTypeError;

    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 StickyShift(AtPrbsEngine self)
    {
    return SubPortIn4PortGroup(self) + c_SERDES_PRBS_ERROR_STA_prbs_force_err_stk_Shift;
    }

static uint32 StickyMask(AtPrbsEngine self)
    {
    return cBit0 << StickyShift(self);
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_ERROR_STA_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 errorMask = StickyMask(self);

    if (read2Clear)
        AtPrbsEngineWrite(self, regAddr, errorMask, cAtModulePrbs);

    return (regVal & errorMask) ? cAtPrbsEngineAlarmTypeError : cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (counterType == cAtPrbsEngineCounterRxBitError)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 ErrorCounterFromXilinx(AtPrbsEngine self)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    AtDrp drp = AtSerdesControllerDrp(controller);
    uint32 regAddrLow  = 0x25E;
    uint32 regAddrHigh = 0x25F;
    uint32 regValLow = AtDrpRead(drp, regAddrLow);
    uint32 regValHigh = AtDrpRead(drp, regAddrHigh);
    uint32 error = regValLow + (regValHigh << 16);
    return error;
    }

static uint32 ErrorClear_Shift(AtPrbsEngine self)
    {
    return SubPortIn4PortGroup(self);
    }

static uint32 ErrorClear_Mask(AtPrbsEngine self)
    {
    return cBit0 << SubPortIn4PortGroup(self);
    }

static void ClearCounter(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cReg_SERDES_PRBS_RESET_ERR_CNT_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 error_Mask = ErrorClear_Mask(self);
    uint32 error_Shift = ErrorClear_Shift(self);

    mRegFieldSet(regVal, error_, 1);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    mRegFieldSet(regVal, error_, 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 error = 0;

    if (!CounterIsSupported(self, counterType))
        return 0;

    error = ErrorCounterFromXilinx(self);

    if (r2c)
        ClearCounter(self);

    return error;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool IsRawPrbs(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        mMethodOverride(m_AtPrbsEngineOverride, IsRawPrbs);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051FacePlateSerdesRawPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FacePlateSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    ((Tha60290021FacePlateSerdesPrbsEngine)self)->mode = cAtPrbsModePrbs7;

    return self;
    }

AtPrbsEngine Tha60290051FaceplateSerdesRawPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

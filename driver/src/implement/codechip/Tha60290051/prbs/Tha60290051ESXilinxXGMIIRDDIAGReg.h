/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef __REG_ES_Xilinx_XGMII_RD_DIAG_H_
#define __REG_ES_Xilinx_XGMII_RD_DIAG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : MiiAlarmGlb
Reg Addr   : 0x00
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status of packet diagnostic

------------------------------------------------------------------------------*/
#define cReg_MiiAlarmGlb_Base                                                                             0x00

/*--------------------------------------
BitField Name: rx_packet_error
BitField Type: W2C
BitField Desc: rx packet error Set 1 while error state change event happens when
rx packet error
BitField Bits: [8]
--------------------------------------*/
#define c_MiiAlarmGlb_rx_packet_error_Mask                                                               cBit8
#define c_MiiAlarmGlb_rx_packet_error_Shift                                                                  8

/*--------------------------------------
BitField Name: rx_data_err
BitField Type: W2C
BitField Desc: rx data packet error Indicates current status of the rx data Set
1 while error state change event happens when rxdata error
BitField Bits: [6]
--------------------------------------*/
#define c_MiiAlarmGlb_rx_data_err_Mask                                                                   cBit6
#define c_MiiAlarmGlb_rx_data_err_Shift                                                                      6

/*--------------------------------------
BitField Name: rx_protocol_error
BitField Type: W2C
BitField Desc: rx_protocol_error  Indicates current status of the
rx_protocol_error Set 1 while rx_protocol_error state change event happens when
rx_protocol_error
BitField Bits: [1]
--------------------------------------*/
#define c_MiiAlarmGlb_rx_protocol_error_Mask                                                             cBit1
#define c_MiiAlarmGlb_rx_protocol_error_Shift                                                                1

/*--------------------------------------
BitField Name: link_sta
BitField Type: W2C
BitField Desc: PHY Link up status Indicates current status of the link Set 1
while link down state change event happens when 10G link down
BitField Bits: [0]
--------------------------------------*/
#define c_MiiAlarmGlb_link_sta_Mask                                                                      cBit0
#define c_MiiAlarmGlb_link_sta_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MiiStatusGlb
Reg Addr   : 0x1
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status of packet diagnostic

------------------------------------------------------------------------------*/
#define cReg_MiiStatusGlb_Base                                                                             0x1

/*--------------------------------------
BitField Name: rx_packet_error
BitField Type: RO
BitField Desc: rx packet error
BitField Bits: [8]
--------------------------------------*/
#define c_MiiStatusGlb_rx_packet_error_Mask                                                              cBit8
#define c_MiiStatusGlb_rx_packet_error_Shift                                                                 8

/*--------------------------------------
BitField Name: rx_data_err
BitField Type: RO
BitField Desc: rx data packet error
BitField Bits: [6]
--------------------------------------*/
#define c_MiiStatusGlb_rx_data_err_Mask                                                                  cBit6
#define c_MiiStatusGlb_rx_data_err_Shift                                                                     6

/*--------------------------------------
BitField Name: rx_protocol_error
BitField Type: RO
BitField Desc: rx_protocol_error
BitField Bits: [1]
--------------------------------------*/
#define c_MiiStatusGlb_rx_protocol_error_Mask                                                            cBit1
#define c_MiiStatusGlb_rx_protocol_error_Shift                                                               1

/*--------------------------------------
BitField Name: link_sta
BitField Type: RO
BitField Desc: PHY Link up status
BitField Bits: [0]
--------------------------------------*/
#define c_MiiStatusGlb_link_sta_Mask                                                                     cBit0
#define c_MiiStatusGlb_link_sta_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : MiiEthTestControl
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 
Configuration Diagnostic,

------------------------------------------------------------------------------*/
#define cReg_MiiEthTestControl_Base                                                                       0x02

/*--------------------------------------
BitField Name: iinsert_crc
BitField Type: R/W
BitField Desc: PRBS test iinsert_crc for tx side
BitField Bits: [12]
--------------------------------------*/
#define c_MiiEthTestControl_iinsert_crc_Mask                                                            cBit12
#define c_MiiEthTestControl_iinsert_crc_Shift                                                               12

/*--------------------------------------
BitField Name: iclear_status
BitField Type: R/W
BitField Desc: PRBS test clear all count and diagnostic status (protocol_error &
rx_data_err) write 0 -> 1-> 0 for clear
BitField Bits: [8]
--------------------------------------*/
#define c_MiiEthTestControl_iclear_status_Mask                                                           cBit8
#define c_MiiEthTestControl_iclear_status_Shift                                                              8

/*--------------------------------------
BitField Name: irestart_diag
BitField Type: R/W
BitField Desc: PRBS test restart write 0 -> 1-> 0 for restart diagnostic
BitField Bits: [4]
--------------------------------------*/
#define c_MiiEthTestControl_irestart_diag_Mask                                                           cBit4
#define c_MiiEthTestControl_irestart_diag_Shift                                                              4

/*--------------------------------------
BitField Name: start_diag_auto
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [2]
--------------------------------------*/
#define c_MiiEthTestControl_start_diag_auto_Mask                                                         cBit2
#define c_MiiEthTestControl_start_diag_auto_Shift                                                            2

/*--------------------------------------
BitField Name: test_en
BitField Type: R/W
BitField Desc: PRBS test Enable
BitField Bits: [0]
--------------------------------------*/
#define c_MiiEthTestControl_test_en_Mask                                                                 cBit0
#define c_MiiEthTestControl_test_en_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : gatetime config value
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 
This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4

------------------------------------------------------------------------------*/
#define cReg_gatetime_cfg_Base                                                                            0x03

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:00]
--------------------------------------*/
#define c_gatetime_cfg_time_cfg_Mask                                                                  cBit16_0
#define c_gatetime_cfg_time_cfg_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Gatetime Current
Reg Addr   : 0x04
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cReg_Gatetime_current_Base                                                                        0x04

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic port 16 to port1 bit per port
1:Running 0:Done-Ready
BitField Bits: [17]
--------------------------------------*/
#define c_Gatetime_current_status_gatetime_diag_Mask                                                    cBit17
#define c_Gatetime_current_status_gatetime_diag_Shift                                                       17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define c_Gatetime_current_currert_gatetime_diag_Mask                                                 cBit16_0
#define c_Gatetime_current_currert_gatetime_diag_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MiiTxPkgCounter [31:0].
Reg Addr   : 0x08
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_itxpklow_Base                                                                                0x08

/*--------------------------------------
BitField Name: Tx_counterl
BitField Type: R0
BitField Desc: Tx Packet counter[31:0]
BitField Bits: [31:0]
--------------------------------------*/
#define c_itxpklow_Tx_counterl_Mask                                                                   cBit31_0
#define c_itxpklow_Tx_counterl_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MiiTxPkgCounter [47:32].
Reg Addr   : 0x09
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_itxpkhigh_Base                                                                               0x09

/*--------------------------------------
BitField Name: Tx_counterh
BitField Type: R0
BitField Desc: Tx Packet counter[47:32]
BitField Bits: [31:0]
--------------------------------------*/
#define c_itxpkhigh_Tx_counterh_Mask                                                                  cBit31_0
#define c_itxpkhigh_Tx_counterh_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : MiiTxByteCounter [31:0].
Reg Addr   : 0x0A
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_itxbyteclow_Base                                                                             0x0A

/*--------------------------------------
BitField Name: Tx_byte_counterl
BitField Type: R0
BitField Desc: Tx Byte counter[31:0]
BitField Bits: [31:0]
--------------------------------------*/
#define c_itxbyteclow_Tx_byte_counterl_Mask                                                           cBit31_0
#define c_itxbyteclow_Tx_byte_counterl_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MiiTxByteCounter [63:32].
Reg Addr   : 0x0B
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_itxbytehigh_Base                                                                             0x0B

/*--------------------------------------
BitField Name: Tx_byte_counterh
BitField Type: R0
BitField Desc: Tx Byte counter[63:32]
BitField Bits: [31:0]
--------------------------------------*/
#define c_itxbytehigh_Tx_byte_counterh_Mask                                                           cBit31_0
#define c_itxbytehigh_Tx_byte_counterh_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MiiRXPkgCounter [31:0].
Reg Addr   : 0x0C
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_iRXpklow_Base                                                                                0x0C

/*--------------------------------------
BitField Name: RX_counterl
BitField Type: R0
BitField Desc: RX Packet counter[31:0]
BitField Bits: [31:0]
--------------------------------------*/
#define c_iRXpklow_RX_counterl_Mask                                                                   cBit31_0
#define c_iRXpklow_RX_counterl_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MiiRXPkgCounter [47:32].
Reg Addr   : 0x0D
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_iRXpkhigh_Base                                                                               0x0D

/*--------------------------------------
BitField Name: RX_counterh
BitField Type: R0
BitField Desc: RX Packet counter[47:32]
BitField Bits: [31:0]
--------------------------------------*/
#define c_iRXpkhigh_RX_counterh_Mask                                                                  cBit31_0
#define c_iRXpkhigh_RX_counterh_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : MiiRXByteCounter [31:0].
Reg Addr   : 0x0E
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_iRXbyteclow_Base                                                                             0x0E

/*--------------------------------------
BitField Name: RX_byte_counterl
BitField Type: R0
BitField Desc: RX Byte counter[31:0]
BitField Bits: [31:0]
--------------------------------------*/
#define c_iRXbyteclow_RX_byte_counterl_Mask                                                           cBit31_0
#define c_iRXbyteclow_RX_byte_counterl_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MiiRXbyteCounter [63:32].
Reg Addr   : 0x0F
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_iRXbytehigh_Base                                                                             0x0F

/*--------------------------------------
BitField Name: RX_byte_counterh
BitField Type: R0
BitField Desc: RX Byte counter[63:32]
BitField Bits: [31:0]
--------------------------------------*/
#define c_iRXbytehigh_RX_byte_counterh_Mask                                                           cBit31_0
#define c_iRXbytehigh_RX_byte_counterh_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MiiRXPkgCounterError
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_iRX_Pkg_Error_Base                                                                           0x10

/*--------------------------------------
BitField Name: RX_Pkg_cntError
BitField Type: R0
BitField Desc: RX Pkg Error counter
BitField Bits: [31:0]
--------------------------------------*/
#define c_iRX_Pkg_Error_RX_Pkg_cntError_Mask                                                          cBit31_0
#define c_iRX_Pkg_Error_RX_Pkg_cntError_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MiiRXDataCounterError
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_iRX_Data_Error_Base                                                                          0x11

/*--------------------------------------
BitField Name: RX_Data_cntError
BitField Type: R0
BitField Desc: RX Data Error counter
BitField Bits: [31:0]
--------------------------------------*/
#define c_iRX_Data_Error_RX_Data_cntError_Mask                                                        cBit31_0
#define c_iRX_Data_Error_RX_Data_cntError_Shift                                                              0

#endif /* __REG_ES_Xilinx_XGMII_RD_DIAG_H_ */

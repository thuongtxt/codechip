/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290051FaceplateEthPortPrbsEngineInternal.h
 * 
 * Created Date: Sep 28, 2018
 *
 * Description : Internal data of the faceplate eth port prbs engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051FACEPLATEETHPORTPRBSENGINEINTERNAL_H_
#define _THA60290051FACEPLATEETHPORTPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha60290051FaceplateEthPortPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290051FaceplateEthPortPrbsEngine
    {
    tAtPrbsEngine super;
    }tTha60290051FaceplateEthPortPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051FACEPLATEETHPORTPRBSENGINEINTERNAL_H_ */


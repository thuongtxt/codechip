/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290051SerdesManager.c
 *
 * Created Date: Aug 20, 2018
 *
 * Description : 60290051 serdes manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha60290051Physical.h"
#include "Tha60290051SerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesXfi0DiagBaseAdress           0xFA0000
#define cSerdesXfi8DiagBaseAdress           0xFA0800
#define cSerdesXfi16DiagBaseAdress          0xFA1000
#define cSerdesXfi24DiagBaseAdress          0xFA1800

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesManagerMethods m_AtSerdesManagerOverride;
static tTha60290021SerdesManagerMethods m_Tha60290021SerdesManagerOverride;

/* Save super implementation */
static const tTha60290021SerdesManagerMethods *m_Tha60290021SerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SerdesIsControllable(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static AtSerdesController FaceplateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha60290051FaceplateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static AtSerdesController SgmiiSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(physicalPort);
    AtUnused(serdesId);
    return NULL;
    }

static uint32 TenG_FaceplateSerdes_XfiDiagnosticBaseAddress(AtSerdesManager self, AtSerdesController serdes)
    {
    uint32 portId = AtSerdesControllerHwIdGet(serdes);
    AtUnused(self);

    if (portId == 0)
        return cSerdesXfi0DiagBaseAdress;

    if (portId == 8)
        return cSerdesXfi8DiagBaseAdress;

    if (portId == 16)
        return cSerdesXfi0DiagBaseAdress;

    if (portId == 24)
        return cSerdesXfi8DiagBaseAdress;

    return cInvalidUint32;
    }

static ThaVersionReader VersionReader(AtSerdesManager self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool ShouldOpenFeature(Tha60290021SerdesManager self, uint32 fromVersion)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesManager)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= fromVersion) ? cAtTrue : cAtFalse;
    }

static eBool SerdesEyeScanResetIsSupported(Tha60290021SerdesManager self)
    {
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x2, 0x0000);
    return ShouldOpenFeature(self, startVersion);
    }


static void OverrideAtSerdesManager(AtSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesManagerOverride, mMethodsGet(self), sizeof(m_AtSerdesManagerOverride));

        mMethodOverride(m_AtSerdesManagerOverride, SerdesIsControllable);
        }

    mMethodsSet(self, &m_AtSerdesManagerOverride);
    }

static void OverrideTha60290021SerdesManager(AtSerdesManager self)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SerdesManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha60290021SerdesManagerOverride));

        mMethodOverride(m_Tha60290021SerdesManagerOverride, FaceplateSerdesControllerObjectCreate);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, SgmiiSerdesControllerObjectCreate);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, TenG_FaceplateSerdes_XfiDiagnosticBaseAddress);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, SerdesEyeScanResetIsSupported);
        }

    mMethodsSet(manager, &m_Tha60290021SerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtSerdesManager(self);
    OverrideTha60290021SerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051SerdesManager);
    }

static AtSerdesManager ObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha60290051SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

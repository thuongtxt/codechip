/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290051FaceplateSerdesController.c
 *
 * Created Date: Aug 20, 2018
 *
 * Description : 60290051 faceplate serdes controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/physical/Tha60290021SerdesManager.h"
#include "../prbs/Tha60290051FaceplateSerdesPrbsEngine.h"
#include "../prbs/Tha60290051XfiSerdesPrbsEngine.h"
#include "diag/Tha60290051MrEth16chwrapReg.h"
#include "Tha60290051FaceplateSerdesControllerInternal.h"
#include "Tha60290051Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290051FaceplateSerdesController*)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods               m_AtSerdesControllerOverride;
static tTha6029SerdesControllerMethods          m_Tha6029SerdesControllerOverride;
static tTha6029FaceplateSerdesControllerMethods m_Tha6029FaceplateSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods        *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth1G;
    }

static eAtModulePrbsRet PrbsHwSerdesModeSet(Tha6029FaceplateSerdesController self, uint32 mode)
    {
    AtEthPort ethPort = (AtEthPort)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    AtUnused(mode);
    if (ethPort)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)ethPort);
        if (!AtDeviceDiagnosticModeIsEnabled(device))
            {
            AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)ethPort);
            return AtPrbsEngineInit(prbs);
            }
        }
    return cAtOk;
    }

static eBool IsRawPrbsUsed(void)
    {
    return cAtTrue;
    }

static AtPrbsEngine RawPrbsEngineObjectCreate(Tha6029FaceplateSerdesController self)
    {
    if (IsRawPrbsUsed())
        return Tha60290051FaceplateSerdesRawPrbsEngineNew((AtSerdesController)self);

    return Tha60290051FaceplateSerdesPrbsEngineNew((AtSerdesController)self);
    }

static AtPrbsEngine XfiPrbsEngineObjectCreate(Tha6029FaceplateSerdesController self, uint32 baseAddress)
    {
    if (IsRawPrbsUsed())
        return Tha60290051FaceplateSerdesRawPrbsEngineNew((AtSerdesController)self);

    return Tha60290051XfiSerdesPrbsEngineNew((AtSerdesController)self, baseAddress);
    }

static eAtRet TenGitPrbsSwitch(AtSerdesController self, eAtSerdesPrbsEngineType type)
    {
    AtPrbsEngine prbs = AtSerdesControllerPrbsEngine(self);
    eBool isOldRawPrbs = AtPrbsEngineIsRawPrbs(prbs);
    eBool newTypeIsRaw = (eBool)(type == cAtSerdesPrbsEngineTypeRaw ? cAtTrue : cAtFalse);
    eAtRet ret = cAtOk;

    if (newTypeIsRaw && isOldRawPrbs)
        return cAtOk;

    ret = AtPrbsEngineInit(prbs);
    AtObjectDelete((AtObject)prbs);

    if (newTypeIsRaw)
        prbs = Tha60290051FaceplateSerdesRawPrbsEngineNew(self);
    else
        {
        uint32 baseAddress = Tha60290021SerdesManagerXfiDiagnosticBaseAddress(self);
        prbs = Tha60290051XfiSerdesPrbsEngineNew(self, baseAddress);
        }

    ret |= AtPrbsEngineInit(prbs);
    Tha6029FaceplateSerdesXfiPrbsEngineSet(self, prbs);
    return ret;
    }

static eAtRet PrbsEngineTypeSet(AtSerdesController self, eAtSerdesPrbsEngineType type)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(self);

    if (mode != cAtSerdesModeEth10G)
        {
        if (type == cAtSerdesPrbsEngineTypeRaw)
            return cAtOk;
        return cAtErrorModeNotSupport;
        }

    return TenGitPrbsSwitch(self, type);
    }

static eAtSerdesPrbsEngineType PrbsEngineTypeGet(AtSerdesController self)
    {
    AtPrbsEngine prbs = AtSerdesControllerPrbsEngine(self);
    eBool isRawPrbs = AtPrbsEngineIsRawPrbs(prbs);

    if (isRawPrbs)
        return cAtSerdesPrbsEngineTypeRaw;
    return cAtSerdesPrbsEngineTypeFramed;
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    uint32 portId = AtSerdesControllerHwIdGet(self);

    if ((mode == cAtSerdesModeEth10G))
        {
        if ((portId == 0) || (portId == 8) || (portId == 16) || (portId == 24))
            return cAtTrue;

        return cAtFalse;
        }

    if ((mode == cAtSerdesModeEth1G || mode == cAtSerdesModeEth100M))
        return cAtTrue;

    return cAtFalse;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    return Tha6029FaceplateSerdesPllIsLockedWithSerdesMode(self);
    }

static eBool PllChanged(AtSerdesController self)
    {
    return Tha6029FaceplateSerdesPllChangedWithSerdesMode(self);
    }

static eBool ShouldChangeGearBoxFor10GLoopback(Tha6029FaceplateSerdesController self, eAtLoopbackMode newLoopback)
    {
    AtSerdesController serdes = (AtSerdesController)self;
    if (AtSerdesControllerPrbsEngineTypeGet(serdes) == cAtSerdesPrbsEngineTypeRaw)
        {
        AtPrbsEngine prbs = AtSerdesControllerPrbsEngine(serdes);
        if (!AtPrbsEngineIsEnabled(prbs))
            return cAtTrue;

        if (newLoopback == cAtLoopbackModeRemote)
            return cAtTrue;

        return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet DefaultSet(AtSerdesController self)
    {
    eAtRet ret = cAtOk;
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxDiffCtrl, 0x10);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxPreCursor, 0x0);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamTxPostCursor, 0x4);
    return ret;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = m_AtSerdesControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static uint32 EyeScanResetRegAddress(Tha6029SerdesController self)
    {
    AtUnused(self);
    return cReg_SERDES_EYESCAN_RESET_Base;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineTypeSet);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineTypeGet);
        mMethodOverride(m_AtSerdesControllerOverride, Init);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6029SerdesController(AtSerdesController self)
    {
    Tha6029SerdesController controller = (Tha6029SerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029SerdesControllerOverride));

        mMethodOverride(m_Tha6029SerdesControllerOverride, EyeScanResetRegAddress);
        }

    mMethodsSet(controller, &m_Tha6029SerdesControllerOverride);
    }

static void OverrideTha6029FaceplateSerdesController(AtSerdesController self)
    {
    Tha6029FaceplateSerdesController controller = (Tha6029FaceplateSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029FaceplateSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029FaceplateSerdesControllerOverride));

        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, PrbsHwSerdesModeSet);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, RawPrbsEngineObjectCreate);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, XfiPrbsEngineObjectCreate);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, ShouldChangeGearBoxFor10GLoopback);
        }

    mMethodsSet(controller, &m_Tha6029FaceplateSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6029SerdesController(self);
    OverrideTha6029FaceplateSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290051FaceplateSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290051FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

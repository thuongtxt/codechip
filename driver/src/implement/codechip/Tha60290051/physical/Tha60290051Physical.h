/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290051Physical.h
 * 
 * Created Date: Aug 20, 2018
 *
 * Description : Interface of the 60290051 physical module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290051PHYSICAL_H_
#define _THA60290051PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtClasses.h"
#include "AtChannelClasses.h"
#include "AtPhysicalClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60290051SerdesManagerNew(AtDevice device);

/* SERDES controller */
AtSerdesController Tha60290051FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290051PHYSICAL_H_ */


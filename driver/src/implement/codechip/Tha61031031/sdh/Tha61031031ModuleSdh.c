/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha61031031ModuleSdh.c
 *
 * Created Date: Oct 21, 2013
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/sdh/ThaStmPwProductModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61031031ModuleSdh
    {
    tThaStmPwProductModuleSdh super;
    }tTha61031031ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods         m_AtModuleSdhOverride;
static tThaModuleSdhMethods        m_ThaModuleSdhOverride;

/* Save super implementations */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031ModuleSdh);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    return 4;
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)ThaSdhLineNew(channelId, (AtModuleSdh)self, version);

    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(sdhModule, &m_AtModuleSdhOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha61031031ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

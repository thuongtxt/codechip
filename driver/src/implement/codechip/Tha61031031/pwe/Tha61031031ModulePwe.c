/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha61031031ModulePwe.c
 *
 * Created Date: Oct 11, 2013
 *
 * Description : PWE module
 *
 * Notes       : This product reuse hardware logic of product 60031031, add PW
 *               PRBS feature to support PRBS. But the old hardware logic has
 *               issue about header buffer. The header buffer range is overlapped
 *               with other configuration registers, so for PW that > 256,
 *               traffic of other PWs are affected.
 *
 *               So, to temporary work on this, software does not need to
 *               configure header for PRBS PW because this configuration is not
 *               necessary. Without it, PRBS engine still works.
 *
 *               In the future, if hardware brings correct logic from 60031031
 *               product to this product, this class should be removed.
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmPw/pwe/ThaStmPwModulePweV2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61031031ModulePwe
    {
    tThaStmPwModulePweV2 super;
    }tTha61031031ModulePwe;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaModulePweMethods m_ThaModulePweOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
	AtUnused(headerLenInByte);
	AtUnused(buffer);
	AtUnused(pw);
	AtUnused(self);
	AtUnused(psn);
    return cAtOk;
    }

static eAtRet HeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
	AtUnused(bufferSize);
	AtUnused(buffer);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, HeaderWrite);
        mMethodOverride(m_ThaModulePweOverride, HeaderRead);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwModulePweV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha61031031ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

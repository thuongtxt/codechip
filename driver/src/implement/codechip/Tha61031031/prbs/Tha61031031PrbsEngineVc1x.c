/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineVc1x.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61031031PrbsEngine.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha61031031PrbsEngineVc1x *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtPrbsEngineMethods       m_AtPrbsEngineOverride;
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineTdmPwOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineVc1x);
    }

static AtPrbsEngine De1PrbsEngineCreate(AtPrbsEngine self)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    if (AtSdhChannelMapTypeSet(vc1x, cAtSdhVcMapTypeVc1xMapDe1) != cAtOk)
        return NULL;

    return Tha61031031PrbsEngineDe1New((AtPdhDe1)AtSdhChannelMapChannelGet(vc1x));
    }

static AtPrbsEngine De1PrbsEngine(AtPrbsEngine self)
    {
    if (mThis(self)->de1PrbsEngine == NULL)
        mThis(self)->de1PrbsEngine = De1PrbsEngineCreate(self);
    return mThis(self)->de1PrbsEngine;
    }

static AtPrbsEngine PwPrbsEngine(Tha61031031PrbsEngineTdmPw self)
    {
    return Tha61031031PrbsEngineTdmPwPrbsEngine((Tha61031031PrbsEngineTdmPw)De1PrbsEngine((AtPrbsEngine)self));
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    return AtPrbsEngineEnable(De1PrbsEngine(self), enable);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(De1PrbsEngine(self));
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->de1PrbsEngine);
    mThis(self)->de1PrbsEngine = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwPrbsEngine);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideTha61031031PrbsEngineTdmPw(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha61031031PrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)vc1x) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineVc1xNew(AtSdhChannel vc1x)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031PrbsEngineVc1xObjectInit(newEngine, vc1x);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaModulePrbsReg.h
 * 
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61031031MODULEPRBSREG_H_
#define _THA61031031MODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* PRBS control register */
#define cRegPrbsEnable       0x4C000
#define cPrbsEnableMask      cBit0
#define cPrbsEnableShift     0
#define cPrbsErrorForceMask  cBit1
#define cPrbsErrorForceShift 1

/* PRBS status register */
#define cRegPrbsState        0x48000
#define cPrbsLossSyncMask    cBit16
#define cPrbsErrorMask       cBit17

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA61031031MODULEPRBSREG_H_ */


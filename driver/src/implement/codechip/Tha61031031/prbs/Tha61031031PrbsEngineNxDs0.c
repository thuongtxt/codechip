/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineNxDs0.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for NxDS0
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "Tha61031031PrbsEngine.h"
#include "../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineTdmPwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineNxDs0);
    }

static AtModulePw PwModule(Tha61031031PrbsEngineTdmPw self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    return (AtPw)AtModulePwCESoPCreate(PwModule(self), (uint16)pwId, cAtPwCESoPModeBasic);
    }

static uint8 PartOfChannel(Tha61031031PrbsEngineTdmPw self)
    {
    return ThaPdhDe1PartId((ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self)));
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwCreate);
        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PartOfChannel);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

AtPrbsEngine Tha61031031PrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)nxDs0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031PrbsEngineNxDs0ObjectInit(newEngine, nxDs0);
    }

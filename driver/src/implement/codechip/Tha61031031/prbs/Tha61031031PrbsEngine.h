/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha61031031PrbsEngine.h
 * 
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61031031PRBSENGINE_H_
#define _THA61031031PRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha61031031ModulePrbs.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61031031PrbsEngineTdmPw * Tha61031031PrbsEngineTdmPw;

typedef struct tTha61031031PrbsEngineTdmPwMethods
    {
    AtPw         (*PwCreate)(Tha61031031PrbsEngineTdmPw self, uint32 pwId);
    uint8        (*PartOfChannel)(Tha61031031PrbsEngineTdmPw self);
    AtPrbsEngine (*PwPrbsEngine)(Tha61031031PrbsEngineTdmPw self);
    AtChannel    (*CircuitToBind)(Tha61031031PrbsEngineTdmPw self, AtChannel channel);
    eAtRet       (*DefaultPayloadSizeSet)(Tha61031031PrbsEngineTdmPw self, AtPw pw);
    }tTha61031031PrbsEngineTdmPwMethods;

typedef struct tTha61031031PrbsEngineTdmPw
    {
    tAtPrbsEngine super;
    const tTha61031031PrbsEngineTdmPwMethods *methods;

    /* Private data */
    AtPrbsEngine pwPrbsEngine;
    }tTha61031031PrbsEngineTdmPw;

typedef struct tTha61031031PrbsEnginePw
    {
    tAtPrbsEngine super;
    }tTha61031031PrbsEnginePw;

typedef struct tTha61031031PrbsEngineDe3
    {
    tTha61031031PrbsEngineTdmPw super;
    }tTha61031031PrbsEngineDe3;

typedef struct tTha61031031PrbsEngineDe1
    {
    tTha61031031PrbsEngineTdmPw super;
    }tTha61031031PrbsEngineDe1;

typedef struct tTha61031031PrbsEngineTu3Vc * Tha61031031PrbsEngineTu3Vc;
typedef struct tTha61031031PrbsEngineTu3VcMethods
    {
    AtPrbsEngine (*De3PrbsEngineCreate)(AtPrbsEngine self);
    }tTha61031031PrbsEngineTu3VcMethods;

typedef struct tTha61031031PrbsEngineTu3Vc
    {
    tTha61031031PrbsEngineTdmPw super;
    const tTha61031031PrbsEngineTu3VcMethods* methods;

    /* Private data */
    eBool mapped;
    uint8 txPsl;
    uint8 expPsl;
    AtPrbsEngine de3PrbsEngine;

    }tTha61031031PrbsEngineTu3Vc;

typedef struct tTha61031031PrbsEngineVcx
    {
    tTha61031031PrbsEngineTdmPw super;

    /* Private data */
    eBool mapped;
    uint8 txPsl;
    uint8 expPsl;
    }tTha61031031PrbsEngineVcx;

typedef struct tTha61031031PrbsEngineNxDs0
    {
    tTha61031031PrbsEngineTdmPw super;
    }tTha61031031PrbsEngineNxDs0;

typedef struct tTha61031031PrbsEngineVc1x
    {
    tTha61031031PrbsEngineTdmPw super;

    /* Private data */
    AtPrbsEngine de1PrbsEngine;
    }tTha61031031PrbsEngineVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtPrbsEngine Tha61031031PrbsEngineTdmPwObjectInit(AtPrbsEngine self, AtChannel tdmChannel);
AtPrbsEngine Tha61031031PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw);
AtPrbsEngine Tha61031031PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3);
AtPrbsEngine Tha61031031PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1);
AtPrbsEngine Tha61031031PrbsEngineTu3VcObjectInit(AtPrbsEngine self, AtSdhChannel vc);
AtPrbsEngine Tha61031031PrbsEngineHoVcObjectInit(AtPrbsEngine self, AtSdhChannel vc);
AtPrbsEngine Tha61031031PrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0);
AtPrbsEngine Tha61031031PrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x);

/* Concrete engines */
AtPrbsEngine Tha61031031PrbsEnginePwNew(AtPw pw);
AtPrbsEngine Tha61031031PrbsEngineDe1New(AtPdhDe1 de1);
AtPrbsEngine Tha61031031PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0);
AtPrbsEngine Tha61031031PrbsEngineVc1xNew(AtSdhChannel vc1x);
AtPrbsEngine Tha61031031PrbsEngineHoVcNew(AtSdhChannel vc);
AtPrbsEngine Tha61031031PrbsEngineTu3VcNew(AtSdhChannel vc);
AtPrbsEngine Tha61031031PrbsEngineDe3New(AtPdhDe3 de3);
AtPrbsEngine Tha6A210031PrbsEnginePwNew(AtPw pw);

/* Utils */
AtPrbsEngine Tha61031031PrbsEngineTdmPwPrbsEngine(Tha61031031PrbsEngineTdmPw self);
AtPrbsEngine Tha61031031PrbsEngineTdmPwPrbsEngineCreate(Tha61031031PrbsEngineTdmPw self);
void Tha61031031PrbsEngineTdmPwPrbsEngineDelete(Tha61031031PrbsEngineTdmPw self);

#ifdef __cplusplus
}
#endif
#endif /* _THA61031031PRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineDe1.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for DE1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61031031PrbsEngine.h"
#include "../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define cDs0MaskOfE1ForPrbs  cBit31_1
#define cDs0MaskOfDs1ForPrbs cBit23_1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods        m_AtPrbsEngineOverride;
static tTha61031031PrbsEngineTdmPwMethods  m_Tha61031031PrbsEngineTdmPwOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineDe1);
    }

static AtModulePw PwModule(Tha61031031PrbsEngineTdmPw self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eBool IsUnFrameMode(uint16 frameType)
    {
    if ((frameType == cAtPdhE1UnFrm) || (frameType == cAtPdhDs1J1UnFrm))
        return cAtTrue;

    return cAtFalse;
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);

    if (IsUnFrameMode(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return (AtPw)AtModulePwSAToPCreate(PwModule(self), (uint16)pwId);

    /* When E1 is in frame mode, we should create a CESoP instead of SAToP */
    return (AtPw)AtModulePwCESoPCreate(PwModule(self), (uint16)pwId, cAtPwCESoPModeBasic);
    }

static uint8 PartOfChannel(Tha61031031PrbsEngineTdmPw self)
    {
    return ThaPdhDe1PartId((ThaPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self));
    }

static eBool IsDs1FrameMode(uint16 frameValue)
    {
    if ((frameValue == cAtPdhDs1FrmSf) ||
        (frameValue == cAtPdhDs1FrmEsf) ||
        (frameValue == cAtPdhDs1FrmDDS) ||
        (frameValue == cAtPdhDs1FrmSLC))
        return cAtTrue;

    return cAtFalse;
    }

static AtChannel CircuitToBind(Tha61031031PrbsEngineTdmPw self, AtChannel channel)
    {
    uint32 nxDs0BitMask;
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);

    if (frameType == cAtPdhDe1FrameUnknown)
        {
        mChannelLog(channel, cAtLogLevelWarning, "Frame type of circuit is unknown");
        return NULL;
        }

    if (IsUnFrameMode(frameType))
        return channel;

    nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
    AtPdhDe1NxDs0Create((AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self), nxDs0BitMask);
    return (AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)channel, nxDs0BitMask);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    /* Need to delete NxDS0 created when enable PRBS */
    if (!enable)
        {
        AtPdhNxDS0 nxDs0;
        AtPdhDe1 de1 = (AtPdhDe1)AtPrbsEngineChannelGet(self);
        uint32 nxDs0BitMask = IsDs1FrameMode(AtPdhChannelFrameTypeGet((AtPdhChannel)de1)) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
        nxDs0 = AtPdhDe1NxDs0Get(de1, nxDs0BitMask);
        if (nxDs0)
            return AtPdhDe1NxDs0Delete(de1, nxDs0);
        }

    return cAtOk;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwCreate);
        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PartOfChannel);
        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, CircuitToBind);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

AtPrbsEngine Tha61031031PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineDe1New(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031PrbsEngineDe1ObjectInit(newEngine, de1);
    }

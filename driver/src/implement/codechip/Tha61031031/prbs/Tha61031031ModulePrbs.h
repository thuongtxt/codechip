/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha61031031ModulePrbs.h
 * 
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61031031MODULEPRBS_H_
#define _THA61031031MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtModulePrbsInternal.h"
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61031031ModulePrbs * Tha61031031ModulePrbs;

typedef struct tTha61031031ModulePrbsMethods
    {
    AtPrbsEngine (*PwPrbsEngineObjectCreate)(Tha61031031ModulePrbs self, AtPw pw);
    uint32 (*DefaultJitterBuffer)(Tha61031031ModulePrbs self);
    AtPrbsEngine (*PwPrbsEngineCreate)(Tha61031031ModulePrbs self, AtPrbsEngine tdmPw);
    }tTha61031031ModulePrbsMethods;

typedef struct tTha61031031ModulePrbs
    {
    tAtModulePrbs super;
    const tTha61031031ModulePrbsMethods *methods;
    }tTha61031031ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha61031031ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

/* Product concretes */
AtModulePrbs Tha61031031ModulePrbsNew(AtDevice device);
AtModulePrbs Tha61150011ModulePrbsNew(AtDevice device);
AtModulePrbs Tha61210031ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A210031ModulePrbsNew(AtDevice device);

uint32 Tha61031031ModulePrbsDefaultJitterBuffer(Tha61031031ModulePrbs self);

#ifdef __cplusplus
}
#endif
#endif /* _THA61031031MODULEPRBS_H_ */


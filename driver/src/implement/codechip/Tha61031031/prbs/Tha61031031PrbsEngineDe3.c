/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineDe3.c
 *
 * Created Date: Apr 20, 2015
 *
 * Description : PRBS engine for DE3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61031031PrbsEngine.h"
#include "../../../default/pdh/ThaPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#define cDs0MaskForPrbs cBit31_1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha61031031PrbsEngineTdmPwMethods  m_Tha61031031PrbsEngineTdmPwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineDe3);
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwSAToPCreate(pwModule, pwId);
    }

static uint8 PartOfChannel(Tha61031031PrbsEngineTdmPw self)
    {
    return ThaPdhDe3PartId((ThaPdhDe3)AtPrbsEngineChannelGet((AtPrbsEngine)self));
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwCreate);
        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PartOfChannel);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

AtPrbsEngine Tha61031031PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha61031031PrbsEngineDe3ObjectInit(newEngine, de3);
    }

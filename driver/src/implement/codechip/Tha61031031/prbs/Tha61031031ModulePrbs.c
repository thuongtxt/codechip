/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031ModulePrbs.c
 *
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha61031031ModulePrbs.h"
#include "Tha61031031ModulePrbsReg.h"
#include "Tha61031031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha61031031ModulePrbsMethods m_methods;

/* Override */
static tAtModuleMethods      m_AtModuleOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha61031031PrbsEngineDe1New(de1);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha61031031PrbsEngineNxDs0New(nxDs0);
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
	AtUnused(self);
    AtUnused(engineId);

    if (channelType == cAtSdhChannelTypeVc4)
        return Tha61031031PrbsEngineHoVcNew(sdhVc);

    if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            return Tha61031031PrbsEngineHoVcNew(sdhVc);
        return Tha61031031PrbsEngineTu3VcNew(sdhVc);
        }

    if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        return Tha61031031PrbsEngineVc1xNew(sdhVc);

    return NULL;
    }

static void PartDefaultSet(AtModule self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    uint32 numPwsPerPart = ThaModulePwNumPwsPerPart((ThaModulePw)AtDeviceModuleGet((AtDevice)device, cAtModulePw));
    uint32 partOffset = ThaDeviceModulePartOffset(device, cAtModulePrbs, partId);
    uint32 pw_i;

    for (pw_i = 0; pw_i < numPwsPerPart; pw_i++)
        {
        uint32 address  = cRegPrbsEnable + pw_i + partOffset;
        mModuleHwWrite(self, address, cPrbsEnableMask); /* To disable engine and disable error forcing */
        }
    }

static void DefaultSet(AtModule self)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePrbs); part_i++)
        PartDefaultSet(self, part_i);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static const char *CapacityDescription(AtModule self)
    {
	AtUnused(self);
    return "vc3, vc4, vc1x, de1, nxds0";
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(Tha61031031ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return Tha61031031PrbsEnginePwNew(pw);
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha61031031PrbsEngineDe3New(de3);
    }

static eBool AllChannelsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DefaultJitterBuffer(Tha61031031ModulePrbs self)
    {
    AtUnused(self);
    return 16000;/* 16ms */
    }

static AtPrbsEngine PwPrbsEngineCreate(Tha61031031ModulePrbs self, AtPrbsEngine tdmPw)
    {
    AtUnused(self);
    return Tha61031031PrbsEngineTdmPwPrbsEngineCreate((Tha61031031PrbsEngineTdmPw)tdmPw);
    }

static void MethodsInit(AtModulePrbs self)
    {
    Tha61031031ModulePrbs module = (Tha61031031ModulePrbs)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, PwPrbsEngineObjectCreate);
        mMethodOverride(m_methods, DefaultJitterBuffer);
        mMethodOverride(m_methods, PwPrbsEngineCreate);
        }

    mMethodsSet(module, &m_methods);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, AllChannelsSupported);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031ModulePrbs);
    }

AtModulePrbs Tha61031031ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha61031031ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031ModulePrbsObjectInit(newModule, device);
    }

uint32 Tha61031031ModulePrbsDefaultJitterBuffer(Tha61031031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->DefaultJitterBuffer(self);
    return 0;
    }

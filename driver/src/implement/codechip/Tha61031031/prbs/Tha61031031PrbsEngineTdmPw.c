/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineTdmPw.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for TDM which can be SDH VC or PDH channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "Tha61031031ModulePrbs.h"
#include "Tha61031031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha61031031PrbsEngineTdmPw *)self)

#define mPrbsEngineFuncGet(_func, _retCode)                                    \
    {                                                                          \
    AtPrbsEngine pwPrbsEngine = PwPrbsEngine(mThis(self));                     \
    if (pwPrbsEngine)                                                          \
        return AtPrbsEngine##_func(pwPrbsEngine);                              \
    return _retCode;                                                           \
    }

#define mPrbsEngineFuncSet(_func, _param, _retCode)                            \
    {                                                                          \
    AtPrbsEngine pwPrbsEngine = PwPrbsEngine(mThis(self));                     \
    if (pwPrbsEngine)                                                          \
        return AtPrbsEngine##_func(pwPrbsEngine, _param);                      \
    return _retCode;                                                           \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                      m_methodsInit = 0;
static tTha61031031PrbsEngineTdmPwMethods m_methods;

/* Override */
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineTdmPw);
    }

static AtPrbsEngine PwPrbsEngine(Tha61031031PrbsEngineTdmPw self)
    {
    return self->pwPrbsEngine;
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
	AtUnused(pwId);
	AtUnused(self);
    /* Concrete class should do */
    return NULL;
    }

static AtModuleEth EthModule(Tha61031031PrbsEngineTdmPw self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static AtEthPort EthPortForPart(Tha61031031PrbsEngineTdmPw self, uint8 partId)
    {
    return AtModuleEthPortGet(EthModule(self), partId);
    }

static eAtRet PsnSetup(AtPw pw)
    {
    AtPwMplsPsn mpls = AtPwMplsPsnNew();
    tAtPwMplsLabel label;
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    eAtRet ret;

    AtPwMplsPsnInnerLabelSet(mpls, AtPwMplsLabelMake(pwId + 1, pwId % 8, (uint8)(pwId % 256), &label));
    AtPwMplsPsnExpectedLabelSet(mpls, pwId + 1);
    ret = AtPwPsnSet(pw, (AtPwPsn)mpls);
    AtObjectDelete((AtObject)mpls);

    return ret;
    }

static uint32 DefaultJitterBuffer(Tha61031031PrbsEngineTdmPw self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    Tha61031031ModulePrbs module = (Tha61031031ModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModulePrbs);
    return Tha61031031ModulePrbsDefaultJitterBuffer(module);
    }

static eAtRet PwDefaultSet(Tha61031031PrbsEngineTdmPw self, AtPw pw, AtChannel circuit, AtEthPort ethPort)
    {
    uint8 mac[6];
    eAtRet ret = cAtOk;
    AtChannel realCircuit = mMethodsGet(self)->CircuitToBind(self, circuit);
    uint32 jitterBufer = DefaultJitterBuffer(self);

    if (realCircuit == NULL)
        return cAtErrorNullPointer;

    ret |= AtEthPortSourceMacAddressSet(ethPort, mac);
    ret |= AtPwCircuitBind(pw, realCircuit);
    ret |= AtPwEthPortSet(pw, ethPort);
    ret |= PsnSetup(pw);
    ret |= AtPwEthHeaderSet(pw, mac, NULL, NULL);
    ret |= AtChannelEnable((AtChannel)pw, cAtTrue);
    ret |= mMethodsGet(self)->DefaultPayloadSizeSet(self, pw);
    ret |= AtPwJitterBufferSizeSet(pw, jitterBufer);
    ret |= AtPwJitterBufferDelaySet(pw, jitterBufer / 2);

    return ret;
    }

static uint8 PartOfChannel(Tha61031031PrbsEngineTdmPw self)
    {
	AtUnused(self);
    /* Sub class should override */
    return 0;
    }

static AtModulePw PwModule(Tha61031031PrbsEngineTdmPw self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static void PwPrbsEngineDelete(Tha61031031PrbsEngineTdmPw self)
    {
    /* Delete PW first */
    AtChannel pw;
    AtModulePw pwModule;

    if (self->pwPrbsEngine == NULL)
        return;

    pw = AtPrbsEngineChannelGet(self->pwPrbsEngine);
    pwModule = (AtModulePw)AtChannelModuleGet(pw);
    AtPwCircuitUnbind((AtPw)pw);
    AtModulePwDeletePw(pwModule, (uint16)AtChannelIdGet(pw));

    /* Then delete the PW PRBS engine */
    AtObjectDelete((AtObject)(self->pwPrbsEngine));
    self->pwPrbsEngine = NULL;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    Tha61031031ModulePrbs prbsModule = (Tha61031031ModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    AtPrbsEngine engine = mThis(self)->pwPrbsEngine;

    /* Enable corresponding PW PRBS engine */
    if (enable)
        {
        if (mThis(self)->pwPrbsEngine == NULL)
            {
            mThis(self)->pwPrbsEngine = mMethodsGet(prbsModule)->PwPrbsEngineCreate(prbsModule, self);
            engine =mThis(self)->pwPrbsEngine;
            }
        if (!engine)
            return cAtError;

        ret |= AtPrbsEngineEnable(engine, enable);
        if (mMethodsGet(engine)->NeedSetDefaultMode(engine))
            {
            AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs15);
            AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs15);
            AtPrbsEngineTxDataModeSet(engine, cAtPrbsDataMode64kb);
            AtPrbsEngineRxDataModeSet(engine, cAtPrbsDataMode64kb);
            }
        if (mMethodsGet(engine)->NeedSetDefaultPrbsLoss(engine))
            {
            AtPrbsEngineDisruptionLossSigThresSet(engine, 10);
            AtPrbsEngineDisruptionPrbsSyncDectectThresSet(engine, 3);
            AtPrbsEngineDisruptionPrbsLossDectectThresSet(engine, 3);
            AtPrbsEngineDisruptionMaxExpectedTimeSet(engine, 0xFFFF);
            }
        }

    /* In case of disable, delete PW PRBS engine to save PW resources */
    else
        PwPrbsEngineDelete(mThis(self));

    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(IsEnabled, cAtFalse);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    mPrbsEngineFuncSet(ErrorForce, force, cAtOk);
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(ErrorIsForced, cAtFalse);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(AlarmGet, 0);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(AlarmHistoryClear, 0);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    mPrbsEngineFuncSet(ModeSet, prbsMode, cAtOk);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(ModeGet, cAtPrbsModePrbs15);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    mPrbsEngineFuncSet(TxModeSet, prbsMode, cAtOk);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(TxModeGet, cAtPrbsModePrbs15);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    mPrbsEngineFuncSet(RxModeSet, prbsMode, cAtOk);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(RxModeGet, cAtPrbsModePrbs15);
    }

static eAtRet DataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    mPrbsEngineFuncSet(DataModeSet, prbsMode, cAtOk);
    }

static eAtPrbsDataMode DataModeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(DataModeGet, cAtPrbsDataMode64kb);
    }

static eAtRet TxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    mPrbsEngineFuncSet(TxDataModeSet, prbsMode, cAtOk);
    }

static eAtPrbsDataMode TxDataModeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(TxDataModeGet, cAtPrbsDataMode64kb);
    }

static eAtRet RxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    mPrbsEngineFuncSet(RxDataModeSet, prbsMode, cAtOk);
    }

static eAtPrbsDataMode RxDataModeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(RxDataModeGet, cAtPrbsDataMode64kb);
    }


static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    mPrbsEngineFuncSet(Invert, invert, cAtOk);
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(IsInverted, cAtFalse);
    }

static void Delete(AtObject self)
    {
    PwPrbsEngineDelete(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static AtChannel CircuitToBind(Tha61031031PrbsEngineTdmPw self, AtChannel channel)
    {
	AtUnused(self);
    return channel;
    }

static eAtRet DefaultPayloadSizeSet(Tha61031031PrbsEngineTdmPw self, AtPw pw)
    {
    /* Let circuit know it */
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    mPrbsEngineFuncSet(CounterGet, counterType, 0);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    mPrbsEngineFuncSet(CounterClear, counterType, 0);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    mPrbsEngineFuncSet(CounterIsSupported, counterType, cAtFalse);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    mPrbsEngineFuncSet(TxFixedPatternSet, fixedPattern, cAtOk);
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(TxFixedPatternGet, cAtOk);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    mPrbsEngineFuncSet(RxFixedPatternSet, fixedPattern, cAtOk);
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(RxFixedPatternGet, cAtOk);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    mPrbsEngineFuncSet(FixedPatternSet, fixedPattern, cAtOk);
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(FixedPatternGet, cAtOk);
    }

static float DelayGet(AtPrbsEngine self, uint8 mode, eBool r2c)
    {
    AtPrbsEngine pwPrbsEngine = PwPrbsEngine(mThis(self));
    AtUnused(mode);
    if (pwPrbsEngine)
        {
        if (mode == cAf6MaxDelay)
            return AtPrbsEngineMaxDelayGet(pwPrbsEngine, r2c);
        else if  (mode == cAf6MinDelay)
            return AtPrbsEngineMinDelayGet(pwPrbsEngine, r2c);

        return AtPrbsEngineAverageDelayGet(pwPrbsEngine, r2c);
        }
    return 0.0;
    }

static eAtRet DelayEnable(AtPrbsEngine self, eBool enable)
    {
    AtPrbsEngine pwPrbsEngine = PwPrbsEngine(mThis(self));
    if (pwPrbsEngine)
        {
        if (enable)
            return AtPrbsEngineDelayEnable(pwPrbsEngine);
        return AtPrbsEngineDelayDisable(pwPrbsEngine);
        }
    return cAtOk;
    }

static eAtRet DisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    mPrbsEngineFuncSet(DisruptionMaxExpectedTimeSet, time, cAtOk);
    }

static uint32 DisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    mPrbsEngineFuncGet(DisruptionMaxExpectedTimeGet, 0);
    }

static uint32 DisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    AtPrbsEngine pwPrbsEngine = PwPrbsEngine(mThis(self));
    if (pwPrbsEngine)
        return AtPrbsEngineDisruptionCurTimeGet(pwPrbsEngine, r2c);                              \
    return 0;
    }

static eAtRet DisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    mPrbsEngineFuncSet(DisruptionLossSigThresSet, thres, cAtOk);
    }

static eAtRet DisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    mPrbsEngineFuncSet(DisruptionPrbsSyncDectectThresSet, thres, cAtOk);
    }

static eAtRet DisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    mPrbsEngineFuncSet(DisruptionPrbsLossDectectThresSet, thres, cAtOk);
    }


static void MethodsInit(Tha61031031PrbsEngineTdmPw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwCreate);
        mMethodOverride(m_methods, PartOfChannel);
        mMethodOverride(m_methods, PwPrbsEngine);
        mMethodOverride(m_methods, CircuitToBind);
        mMethodOverride(m_methods, DefaultPayloadSizeSet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, DelayGet);
        mMethodOverride(m_AtPrbsEngineOverride, DelayEnable);

        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionCurTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLossSigThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsSyncDectectThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsLossDectectThresSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha61031031PrbsEngineTdmPwObjectInit(AtPrbsEngine self, AtChannel tdmChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, tdmChannel, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineTdmPwPrbsEngine(Tha61031031PrbsEngineTdmPw self)
    {
    if (self)
        return mMethodsGet(self)->PwPrbsEngine(self);
    return NULL;
    }

AtPrbsEngine Tha61031031PrbsEngineTdmPwPrbsEngineCreate(Tha61031031PrbsEngineTdmPw self)
    {
    uint32 freePwId;
    AtPw pw = NULL;
    AtEthPort ethPort;
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    Tha61031031ModulePrbs prbsModule = (Tha61031031ModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);

    /* Find one free PW */
    freePwId = ThaModulePwFreePwGet(pwModule);
    if (freePwId >= AtModulePwMaxPwsGet((AtModulePw)pwModule))
        return NULL;

    /* Create PW */
    pw = mMethodsGet(self)->PwCreate(self, freePwId);
    if (pw == NULL)
        return NULL;

    /* Set default configuration for this PW */
    ethPort = EthPortForPart(self, mMethodsGet(self)->PartOfChannel(self));
    if (PwDefaultSet(self, pw, AtPrbsEngineChannelGet((AtPrbsEngine)self), ethPort) != cAtOk)
        {
        AtModulePwDeletePw(PwModule(self), (uint16)freePwId);
        return NULL;
        }

    return mMethodsGet(prbsModule)->PwPrbsEngineObjectCreate(prbsModule, pw);
    }

void Tha61031031PrbsEngineTdmPwPrbsEngineDelete(Tha61031031PrbsEngineTdmPw self)
    {
    PwPrbsEngineDelete(self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEnginePw.c
 *
 * Created Date: Aug 23, 2013
 *
 * Description : PW PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPw.h"

#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/pwe/ThaModulePwe.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha61031031PrbsEngine.h"
#include "Tha61031031ModulePrbsReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEnginePw);
    }

static AtPw Pw(AtPrbsEngine self)
    {
    return (AtPw)AtPrbsEngineChannelGet(self);
    }

static ThaModulePw PwModule(AtPrbsEngine self)
    {
    return (ThaModulePw)AtChannelModuleGet((AtChannel)Pw(self));
    }

static ThaModulePwe PweModule(AtPrbsEngine self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet(self));
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static uint32 DefaultOffset(AtPrbsEngine self)
    {
    ThaModulePw pwModule = PwModule(self);
    AtPw pw              = Pw(self);
    uint8 partId         = ThaModulePwPartOfPw(pwModule, pw);
    uint32 localPwId     = ThaModulePwLocalPwId(pwModule, pw);

    return ThaModulePwePartOffset(PweModule(self), partId) + localPwId;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = cRegPrbsEnable + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    mFieldIns(&regVal, cPrbsEnableMask, cPrbsEnableShift, enable ? 0 : 1);
    AtPrbsEngineWrite(self, regAddr, regVal, cThaModulePwe);

    /* Read to clear current status */
    if (enable)
        AtPrbsEngineAlarmGet(self);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regVal  = AtPrbsEngineRead(self, cRegPrbsEnable + DefaultOffset(self), cThaModulePwe);
    return (regVal & cPrbsEnableMask) ? cAtFalse : cAtTrue;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = cRegPrbsEnable + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cPrbsErrorForce, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = cRegPrbsEnable + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);
    return (regVal & cPrbsErrorForceMask) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddr = cRegPrbsState + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);
    uint32 alarm   = 0;

    if ((regVal & cPrbsLossSyncMask) == 0)
        alarm |= cAtPrbsEngineAlarmTypeLossSync;
    if (regVal & cPrbsErrorMask)
        alarm = cAtPrbsEngineAlarmTypeError;

    return alarm;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
	AtUnused(self);
    if (prbsMode == cAtPrbsModePrbs15)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtPrbsModePrbs15;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
	AtUnused(self);
    if (invert)
        return cAtErrorModeNotSupport;
    return cAtOk;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha61031031PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, (AtChannel)pw, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEnginePwNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031PrbsEnginePwObjectInit(newEngine, pw);
    }

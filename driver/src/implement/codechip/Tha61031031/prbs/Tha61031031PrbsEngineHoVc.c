/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineVcx.c
 *
 * Created Date: Nov 12, 2013
 *
 * Description : PRBS engine for VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"

#include "../../../default/sdh/ThaModuleSdh.h"
#include "Tha61031031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
typedef eAtModulePrbsRet (*PrbsAttributeSet)(AtPrbsEngine self, uint32 value);

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha61031031PrbsEngineVcx *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineVcx);
    }

static eAtRet Tug2MapVc1x(AtSdhChannel tug2)
    {
    return AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    }

static eAtRet Tug3MapVc1x(AtSdhChannel tug3)
    {
    uint8 tug_i;
    eAtRet ret = AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s) ;

    for (tug_i = 0; tug_i < AtSdhChannelNumberOfSubChannelsGet(tug3); tug_i++)
        ret |= Tug2MapVc1x(AtSdhChannelSubChannelGet(tug3, tug_i));

    return ret;
    }

static eAtRet Vc4MapVc1x(AtSdhChannel vc4)
    {
    uint8 tug_i;
    eAtRet ret = AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);

    for (tug_i = 0; tug_i < AtSdhChannelNumberOfSubChannelsGet(vc4); tug_i++)
        ret |= Tug3MapVc1x(AtSdhChannelSubChannelGet(vc4, tug_i));

    return ret;
    }

static void BackupPsl(AtPrbsEngine self, AtSdhChannel vc3)
    {
    mThis(self)->txPsl  = AtSdhPathTxPslGet((AtSdhPath)vc3);
    mThis(self)->expPsl = AtSdhPathExpectedPslGet((AtSdhPath)vc3);
    }

static void RestorePsl(AtPrbsEngine self, AtSdhChannel vc3)
    {
    AtSdhPathTxPslSet((AtSdhPath)vc3, mThis(self)->txPsl);
    AtSdhPathExpectedPslSet((AtSdhPath)vc3, mThis(self)->expPsl);
    }

static eAtRet Vc3MapVc1x(AtPrbsEngine self, AtSdhChannel vc3)
    {
    uint8  tug_i;
    eAtRet ret;

    BackupPsl(self, vc3);
    ret = AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);
    RestorePsl(self, vc3);
    for (tug_i = 0; tug_i < AtSdhChannelNumberOfSubChannelsGet(vc3); tug_i++)
        ret |= Tug2MapVc1x(AtSdhChannelSubChannelGet(vc3, tug_i));

    return ret;
    }

static eAtRet VcMapVc1x(AtPrbsEngine self)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel hoVc;

    if (mThis(self)->mapped)
        return cAtOk;

    hoVc = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    if (AtSdhChannelTypeGet(hoVc) == cAtSdhChannelTypeVc3)
        ret = Vc3MapVc1x(self, hoVc);
    else
        ret = Vc4MapVc1x(hoVc);

    mThis(self)->mapped = cAtTrue;

    return ret;
    }

static eAtModulePrbsRet SdhChannelPrbsEnginesAttributeSet(AtSdhChannel sdhChannel, uint32 value, PrbsAttributeSet AttributeSetFunc)
    {
    uint8 i;
    eAtRet ret = cAtOk;

    /* This VC-12, configure directly */
    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc12)
        return AttributeSetFunc(AtChannelPrbsEngineGet((AtChannel)sdhChannel), value);

    /* Configure all sub channels */
    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(sdhChannel); i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(sdhChannel, i);
        ret |= SdhChannelPrbsEnginesAttributeSet(subChannel, value, AttributeSetFunc);
        }

    return ret;
    }

static eAtModulePrbsRet PrbsEnginesAttributeSet(AtPrbsEngine self, uint32 value, PrbsAttributeSet AttributeSetFunc)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel vc;

    /* Try to map VC-1x */
    ret = VcMapVc1x(self);
    if (ret != cAtOk)
        return ret;

    /* Set PRBS attribute */
    vc = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    return SdhChannelPrbsEnginesAttributeSet(vc, value, AttributeSetFunc);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    return PrbsEnginesAttributeSet(self, enable, (PrbsAttributeSet)AtPrbsEngineEnable);
    }

static AtSdhChannel FirstVc1x(AtSdhChannel vc)
    {
    uint8 sts1 = AtSdhChannelSts1Get(vc);
    uint8 aug1Id    = sts1 / 3;
    uint8 au3Tug3Id = sts1 % 3;
    return (AtSdhChannel)AtSdhLineVc1xGet(ThaSdhLineFromChannel(vc), aug1Id, au3Tug3Id, 0, 0);
    }

static AtPrbsEngine FirstEngine(AtPrbsEngine self)
    {
    AtChannel firstVc1x = (AtChannel)FirstVc1x((AtSdhChannel)AtPrbsEngineChannelGet(self));
    return AtChannelPrbsEngineGet(firstVc1x);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(FirstEngine(self));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    return PrbsEnginesAttributeSet(self, force, (PrbsAttributeSet)AtPrbsEngineErrorForce);
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    return AtPrbsEngineErrorIsForced(FirstEngine(self));
    }

static uint32 SdhChannelPrbsEngineAlarmGet(AtSdhChannel sdhChannel)
    {
    uint8 i;
    uint32 alarmStatus = 0;

    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc12)
        return AtPrbsEngineAlarmGet(AtChannelPrbsEngineGet((AtChannel)sdhChannel));

    /* Check all of PRBS status of all sub channels */
    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(sdhChannel); i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(sdhChannel, i);

        /* Note, do not break if one engine is fail, all of engines must be
         * checked to clear their status */
        alarmStatus |= SdhChannelPrbsEngineAlarmGet(subChannel);
        }

    return alarmStatus;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return SdhChannelPrbsEngineAlarmGet((AtSdhChannel)AtPrbsEngineChannelGet(self));
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return PrbsEnginesAttributeSet(self, (uint32)prbsMode, (PrbsAttributeSet)AtPrbsEngineModeSet);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(FirstEngine(self));
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    return PrbsEnginesAttributeSet(self, invert, (PrbsAttributeSet)AtPrbsEngineInvert);
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineIsInverted(FirstEngine(self));
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha61031031PrbsEngineHoVcObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineHoVcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031PrbsEngineHoVcObjectInit(newEngine, vc);
    }

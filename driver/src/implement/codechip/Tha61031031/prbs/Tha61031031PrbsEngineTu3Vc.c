/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61031031PrbsEngineTu3Vc.c
 *
 * Created Date: Jul 23, 2015
 *
 * Description : PRBS engine for TU3-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"

#include "../../../default/sdh/ThaModuleSdh.h"
#include "Tha61031031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
typedef eAtModulePrbsRet (*PrbsAttributeSet)(AtPrbsEngine self, uint32 value);

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha61031031PrbsEngineTu3Vc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tTha61031031PrbsEngineTu3VcMethods m_methods;
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tAtObjectMethods      m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61031031PrbsEngineTu3Vc);
    }

static void BackupPsl(AtPrbsEngine self, AtSdhChannel vc3)
    {
    mThis(self)->txPsl  = AtSdhPathTxPslGet((AtSdhPath)vc3);
    mThis(self)->expPsl = AtSdhPathExpectedPslGet((AtSdhPath)vc3);
    }

static void RestorePsl(AtPrbsEngine self, AtSdhChannel vc3)
    {
    AtSdhPathTxPslSet((AtSdhPath)vc3, mThis(self)->txPsl);
    AtSdhPathExpectedPslSet((AtSdhPath)vc3, mThis(self)->expPsl);
    }

static eAtRet VcMapDe3(AtPrbsEngine self)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel tu3Vc;

    if (mThis(self)->mapped)
        return cAtOk;

    /*
     * Need backup and restore psl value that configured before,
     * while set mapping Vc3 to De3 reset this value to default.
     *  */
    tu3Vc = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    BackupPsl(self, tu3Vc);
    ret = AtSdhChannelMapTypeSet(tu3Vc, cAtSdhVcMapTypeVc3MapDe3);
    RestorePsl(self, tu3Vc);
    mThis(self)->mapped = cAtTrue;

    return ret;
    }

static AtPrbsEngine De3PrbsEngineCreate(AtPrbsEngine self)
    {
    AtSdhChannel vc3 = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    if (AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3) != cAtOk)
        return NULL;

    return Tha61031031PrbsEngineDe3New((AtPdhDe3)AtSdhChannelMapChannelGet(vc3));
    }

static AtPrbsEngine De3PrbsEngine(AtPrbsEngine self)
    {
    if (mThis(self)->de3PrbsEngine == NULL)
        mThis(self)->de3PrbsEngine = mMethodsGet(mThis(self))->De3PrbsEngineCreate(self);
    return mThis(self)->de3PrbsEngine;
    }

static eAtModulePrbsRet PrbsEnginesAttributeSet(AtPrbsEngine self, uint32 value, PrbsAttributeSet AttributeSetFunc)
    {
    eAtRet ret = cAtOk;

    /* Try to map DE3 */
    ret = VcMapDe3(self);
    if (ret != cAtOk)
        return ret;

    /* Set PRBS attribute */
    return AttributeSetFunc(De3PrbsEngine(self), value);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    return PrbsEnginesAttributeSet(self, enable, (PrbsAttributeSet)AtPrbsEngineEnable);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(De3PrbsEngine(self));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    return PrbsEnginesAttributeSet(self, (uint32)force,  (PrbsAttributeSet)AtPrbsEngineErrorForce);
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    return AtPrbsEngineErrorIsForced(De3PrbsEngine(self));
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return AtPrbsEngineAlarmGet(De3PrbsEngine(self));
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return PrbsEnginesAttributeSet(self, (uint32)prbsMode,  (PrbsAttributeSet)AtPrbsEngineModeSet);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(De3PrbsEngine(self));
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    return PrbsEnginesAttributeSet(self, (uint32)invert,  (PrbsAttributeSet)AtPrbsEngineInvert);
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineIsInverted(De3PrbsEngine(self));
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->de3PrbsEngine);
    mThis(self)->de3PrbsEngine = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void MethodsInit(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTu3Vc tu3VcEngine = (Tha61031031PrbsEngineTu3Vc)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, De3PrbsEngineCreate);
        }

    mMethodsSet(tu3VcEngine, &m_methods);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideAtObject(self);
    }

AtPrbsEngine Tha61031031PrbsEngineTu3VcObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61031031PrbsEngineTu3VcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61031031PrbsEngineTu3VcObjectInit(newEngine, vc);
    }

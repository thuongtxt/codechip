/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha600a0011Device.c
 *
 * Created Date: May 6, 2013
 *
 * Description : Product 0x600a0011 (STM PW)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmPw/man/ThaStmPwDeviceInternal.h"
#include "../../../default/ber/ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha600a0011Device
    {
    tThaStmPwDevice super;

    /* Private data */
    }tTha600a0011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha600a0011Device);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModulePw)  return (AtModule)Tha600a0011ModulePwNew(self);
    if (moduleId == cAtModuleBer) return (AtModule)ThaStmModuleBerNew(self, cThaModuleBerDefaultMaxNumEngines);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModuleBer,
                                                 cAtModulePktAnalyzer};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModuleBer)
        return cAtTrue;
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha600a0011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS module
 *
 * File        : Tha60030080CosEthPortController.c
 *
 * Created Date: Dec 16, 2013
 *
 * Description : Ovveride OAM counters in this product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cos/controllers/ThaCosEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCosEthPortControllerMethods m_ThaCosEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 EthPortOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static void OverrideThaCosEthPortController(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaCosEthPortControllerOverride));

        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamTxPktRead2Clear);
        }

    mMethodsSet(self, &m_ThaCosEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideThaCosEthPortController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCosEthPortController);
    }

static ThaCosEthPortController ObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosEthPortControllerV2ObjectInit(self, cos) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController Tha60030080CosEthPortControllerNew(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cos);
    }

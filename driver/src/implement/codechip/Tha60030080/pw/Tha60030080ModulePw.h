/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60030080ModulePw.h
 * 
 * Created Date: Jan 22, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030080MODULEPW_H_
#define _THA60030080MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60030081/pw/Tha60030081ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030080ModulePw
    {
    tTha60030081ModulePw super;
    }tTha60030080ModulePw;

/*--------------------------- Forward declarations ---------------------------*/
AtModulePw Tha60030080ModulePwObjectInit(AtModulePw self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60030080MODULEPW_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60030080ModulePw.c
 *
 * Created Date: Dec 13, 2013
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60030080ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods  m_AtModulePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwSAToP SAToPObjectCreate(AtModulePw self, uint32 pwId)
    {
	AtUnused(pwId);
	AtUnused(self);
    return NULL;
    }

static AtPwCESoP CESoPObjectCreate(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode)
    {
	AtUnused(mode);
	AtUnused(pwId);
	AtUnused(self);
    return NULL;
    }

static AtPwCep CepObjectCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    return ThaModulePwCepObjectCreate(self, pwId, mode);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(m_AtModulePwOverride));

        mMethodOverride(m_AtModulePwOverride, SAToPObjectCreate);
        mMethodOverride(m_AtModulePwOverride, CESoPObjectCreate);
        mMethodOverride(m_AtModulePwOverride, CepObjectCreate);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030080ModulePw);
    }

AtModulePw Tha60030080ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030081ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60030080ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60030080ModulePwObjectInit(newModule, device);
    }


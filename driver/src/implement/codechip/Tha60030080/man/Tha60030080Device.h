/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device
 * 
 * File        : Tha60030080Device.h
 * 
 * Created Date: Jan 22, 2015
 *
 * Description : 60030080 Device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030080DEVICE_H_
#define _THA60030080DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60030081/man/Tha60030081Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030080Device
    {
    tTha60030081Device super;
    }tTha60030080Device;

/*--------------------------- Forward declarations ---------------------------*/
AtDevice Tha60030080DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60030080DEVICE_H_ */


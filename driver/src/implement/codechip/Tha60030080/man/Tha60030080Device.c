/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030080Device.c
 *
 * Created Date: Dec 9, 2013
 *
 * Description : Product 60030080
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60030080Device.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha60030080ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)   return (AtModule)Tha60030080ModulePwNew(self);

    /* Private modules */
    if (phyModule == cThaModulePwe) return Tha60030080ModulePweNew(self);
    if (phyModule == cThaModuleBmt) return Tha60030080ModuleBmtNew(self);
    if (phyModule == cThaModuleCos) return Tha60030080ModuleCosNew(self);
    if (phyModule == cThaModuleCla) return Tha60030080ModuleClaNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60030080SSKeyCheckerNew(self);
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x14, 0x4, 0x17, 0x0);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, CanHaveNewOcn);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030080Device);
    }

AtDevice Tha60030080DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030081DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030080DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60030080DeviceObjectInit(newDevice, driver, productCode);
    }


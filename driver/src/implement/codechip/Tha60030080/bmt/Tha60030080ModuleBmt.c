/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BMT
 *
 * File        : Tha60030080ModuleBmt.c
 *
 * Created Date: Dec 18, 2013
 *
 * Description : BMT module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/bmt/ThaModuleBmtInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030080ModuleBmt
    {
    tThaModuleBmt super;
    }tTha60030080ModuleBmt;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleBmtMethods m_ThaModuleBmtOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedInitCache(ThaModuleBmt self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleBmt(AtModule self)
    {
    ThaModuleBmt bmtModule = (ThaModuleBmt)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleBmtOverride, mMethodsGet(bmtModule), sizeof(m_ThaModuleBmtOverride));

        mMethodOverride(m_ThaModuleBmtOverride, NeedInitCache);
        }

    mMethodsSet(bmtModule, &m_ThaModuleBmtOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleBmt(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030080ModuleBmt);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleBmtObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60030080ModuleBmtNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module PWE
 *
 * File        : Tha60030080ModulePwe.c
 *
 * Created Date: Dec 2, 2014
 *
 * Description : Tha60030080 module PWE implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePweInternal.h"
#include "../../../default/pw/ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegPDHPWRemovingProtocol   0x2F0000

#define cRemovePwIdMask             cBit31_2
#define cRemovePwIdShift            2

#define cRemovePwStateMask          cBit1_0
#define cRemovePwStateShift         0

#define cRemovePwStateStop          0
#define cRemovePwStateStart         1
#define cRemovePwStateReady         2

/*--------------------------- Macros -----------------------------------------*/
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030080ModulePwe
    {
    tThaModulePwe super;
    }tTha60030080ModulePwe;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods m_ThaModulePweOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RemovingProtocolIsSupported(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x14, 0x12, 0x03, 0x10);
    
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool PwRemovingIsReady(ThaModulePwe self, AtPw pw)
    {
    const uint32 cTimeoutMs = 32;
    uint32 regAddr = cRegPDHPWRemovingProtocol + ThaPwPartOffset(pw, cThaModulePwe);
    uint32 regVal  = 0;
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
	AtUnused(self);

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);

    while (elapseTime <= cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
        if (mRegField(regVal, cRemovePwState) == cRemovePwStateReady)
            return cAtTrue;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "Cannot wait before removing\r\n");

    return cAtFalse;
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    if (!RemovingProtocolIsSupported(self))
        return cAtOk;

    regAddr = cRegPDHPWRemovingProtocol + ThaPwPartOffset(pw, cThaModulePwe);
    regVal  = 0;

    /* Make request */
    mRegFieldSet(regVal, cRemovePwId, mPwHwId(pw));
    mRegFieldSet(regVal, cRemovePwState, cRemovePwStateStart);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    /* Wait till hardware done */
    return PwRemovingIsReady(self, pw) ? cAtOk : cAtErrorDevBusy;
    }

static eAtRet PwRemovingFinish(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    if (!RemovingProtocolIsSupported(self))
        return cAtOk;

    regAddr = cRegPDHPWRemovingProtocol + ThaPwPartOffset(pw, cThaModulePwe);
    regVal  = 0;

    mRegFieldSet(regVal, cRemovePwId, mPwHwId(pw));
    mRegFieldSet(regVal, cRemovePwState, cRemovePwStateStop);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingFinish);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030080ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60030080ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60030080ModuleSdh.c
 *
 * Created Date: Dec 13, 2013
 *
 * Description : SDH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhVc.h"
#include "Tha60030080ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleSdhMethods m_ThaModuleSdhOverride;
static tAtModuleSdhMethods  m_AtModuleSdhOverride;

static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 DefaultVc1xSubMapping(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtSdhVcMapTypeVc1xMapC1x;
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    /* Create AU-VC */
    if (channelType == cAtSdhChannelTypeVc4)
        return (AtSdhChannel)Tha60030080SdhAuVcNew(channelId, channelType, self);

    return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
    }

static eBool ShouldRemoveLoopbackOnBinding(ThaModuleSdh self)
    {
    /* Before path is bound to CEP PW, it is in loopback state as required by HW.
     * So when binding, this loopback should be removed */
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, DefaultVc1xSubMapping);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldRemoveLoopbackOnBinding);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh(self);
    OverrideAtModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030080ModuleSdh);
    }

AtModuleSdh Tha60030080ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030081ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60030080ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60030080ModuleSdhObjectInit(newModule, device);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60030080ModuleSdh.h
 * 
 * Created Date: Jan 22, 2015
 *
 * Description : Module SDH of product 60030080
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030080MODULESDH_H_
#define _THA60030080MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60030081/sdh/Tha60030081ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030080ModuleSdh
    {
    tTha60030081ModuleSdh super;
    }tTha60030080ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/
AtModuleSdh Tha60030080ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60030080MODULESDH_H_ */


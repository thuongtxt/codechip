/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60030080SdhAuVc.c
 *
 * Created Date: Mar 17, 2014
 *
 * Description : SDH VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/sdh/ThaSdhVcInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cVc4LoopBack 0x04001C
#define cVc4LoopInEnableMask(id)    (cBit0 << (id))
#define cVc4LoopInEnableShift(id)   (id)
#define cVc4LoopInBypassMask        cBit15
#define cVc4LoopInBypassShift       15

#define cVc4LoopOutEnableMask(id)  (cBit16 << (id))
#define cVc4LoopOutEnableShift(id) (16 + id)
#define cVc4LoopOutBypassMask      cBit31
#define cVc4LoopOutBypassShift     31

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60030080SdhAuVc)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030080SdhAuVc * Tha60030080SdhAuVc;

typedef struct tTha60030080SdhAuVc
    {
    tThaSdhAuVc super;

    /* Private data */
    eBool isTerminated;
    eBool pohInsert;
    eAtTimingMode timingMode;
    AtChannel timingSource;
    uint8 stsPayloadType;
    }tTha60030080SdhAuVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods   m_AtObjectOverride;
static tAtChannelMethods  m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PartOffset(AtModuleSdh self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleOcn, partId);
    }

static AtModuleSdh ModuleSdh(AtChannel self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSdh);
    }

static uint8 Vc4IdGet(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    AtSdhLine line = AtSdhChannelLineObjectGet(sdhChannel);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if (rate == cAtSdhLineRateStm1)
        return (uint8)ThaModuleSdhLineLocalId(line);

    if (rate == cAtSdhLineRateStm4)
        return (uint8)AtChannelIdGet(self);

    return 0;
    }

static eAtRet BypassLoopbackFunction(AtChannel self, eBool isBypass)
    {
    AtModuleSdh moduleSdh = ModuleSdh(self);
    uint32 address = cVc4LoopBack + PartOffset(moduleSdh, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    uint32 regValue = mChannelHwRead(self, address, cAtModuleSdh);

    mRegFieldSet(regValue, cVc4LoopInBypass, mBoolToBin(isBypass));
    mRegFieldSet(regValue, cVc4LoopOutBypass, mBoolToBin(isBypass));

    mChannelHwWrite(self, address, regValue, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet HwLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtModuleSdh moduleSdh = ModuleSdh(self);
    uint32 address = cVc4LoopBack + PartOffset(moduleSdh, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    uint32 regValue = mChannelHwRead(self, address, cAtModuleSdh);
    uint8 vc4Id = Vc4IdGet(self);
    uint8 loopin = 0, loopout = 0;

    if (loopbackMode == cAtLoopbackModeLocal)
        loopin = 1;

    if (loopbackMode == cAtLoopbackModeRemote)
        loopout = 1;

    mFieldIns(&regValue, cVc4LoopOutEnableMask(vc4Id), cVc4LoopOutEnableShift(vc4Id), loopout);
    mFieldIns(&regValue, cVc4LoopInEnableMask(vc4Id), cVc4LoopInEnableShift(vc4Id), loopin);
    mChannelHwWrite(self, address, regValue, cAtModuleSdh);

    return BypassLoopbackFunction(self, cAtFalse);
    }

static eBool IsVc4Fractional(AtSdhChannel vc4)
    {
    if (AtSdhChannelMapTypeGet(vc4) != cAtSdhVcMapTypeVc4Map3xTug3s)
        return cAtFalse;

    if (AtChannelBoundPwGet((AtChannel)vc4))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    eBool terminated, pohInsert;
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;
    uint8 currentLoopbackMode;
    uint8 stsPayloadType;
    eAtSdhVcMapType vcMapType;

    if (AtSdhChannelTypeGet(sdhChannel) != cAtSdhChannelTypeVc4)
        return cAtErrorModeNotSupport;

    /* Not support loopin incase of fractional */
    if (IsVc4Fractional(sdhChannel) && (loopbackMode == cAtLoopbackModeLocal))
        return cAtErrorModeNotSupport;

    /* Nothing change on loopback */
    currentLoopbackMode = AtChannelLoopbackGet(self);
    if (currentLoopbackMode == loopbackMode)
        return cAtOk;

    /* Need to backup STS termination */
    if ((currentLoopbackMode == cAtLoopbackModeRelease) && (loopbackMode != cAtLoopbackModeRelease))
        {
        mThis(self)->isTerminated = ThaModuleOcnRxStsIsTerminated(sdhChannel, startSts);
        mThis(self)->pohInsert = ThaOcnStsPohInsertIsEnabled(sdhChannel, startSts);
        mThis(self)->timingMode = AtChannelTimingModeGet(self);
        mThis(self)->timingSource = AtChannelTimingSourceGet(self);
        mThis(self)->stsPayloadType = ThaOcnStsTxPayloadTypeGet(sdhChannel, startSts);
        }

    /* AS hardware spec, STS termination must be disabled when loopout */
    terminated = mThis(self)->isTerminated;
    stsPayloadType = mThis(self)->stsPayloadType;
    vcMapType = AtSdhChannelMapTypeGet(sdhChannel);
    pohInsert = mThis(self)->pohInsert;

    if (loopbackMode == cAtLoopbackModeRemote)
        {
        terminated = cAtFalse;
        pohInsert = cAtFalse;
        stsPayloadType = cThaStsDontCareVtTu;
        vcMapType = cAtSdhVcMapTypeVc4MapC4;
        }

    /* Perform change */
    ret = HwLoopbackSet(self, loopbackMode);
    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        uint8 stsId = (uint8)(startSts + sts_i);

        ret |= ThaOcnStsPohInsertEnable(sdhChannel, stsId, pohInsert);
        ret |= ThaModuleOcnRxStsTermEnable(sdhChannel, stsId, terminated);
        ret |= ThaOcnStsTxPayloadTypeSet(sdhChannel, stsId, stsPayloadType);
        }

    /* In case of VC4 fractional, need to set slaver indicate with type VC4-C4 */
    if (IsVc4Fractional(sdhChannel))
        vcMapType = cAtSdhVcMapTypeVc4MapC4;

    ThaSdhAuVcPgStsSlaverIndicate(sdhChannel, vcMapType);
    return ret;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    AtModuleSdh moduleSdh = ModuleSdh(self);
    uint32 address, regValue;
    eAtSdhChannelType type = AtSdhChannelTypeGet((AtSdhChannel)self);
    uint8 vc4Id, loopin = 0, loopout = 0;

    /* Just support Vc4 */
    if (type != cAtSdhChannelTypeVc4)
        return cAtLoopbackModeRelease;

    address = cVc4LoopBack + PartOffset(moduleSdh, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    regValue = mChannelHwRead(self, address, cAtModuleSdh);
    vc4Id = Vc4IdGet(self);

    mFieldGet(regValue, cVc4LoopInEnableMask(vc4Id), cVc4LoopInEnableShift(vc4Id), uint8, &loopin);
    if (loopin)
        return cAtLoopbackModeLocal;

    mFieldGet(regValue, cVc4LoopOutEnableMask(vc4Id), cVc4LoopOutEnableShift(vc4Id), uint8, &loopout);
    if (loopout)
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    /* Standard loopback modes are assumed to be supported. Concrete class should
     * override this method */
    AtUnused(self);
    return mOutOfRange(loopbackMode, cAtLoopbackModeRelease, cAtLoopbackModeRemote) ? cAtFalse : cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60030080SdhAuVc object = (Tha60030080SdhAuVc)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(isTerminated);
    mEncodeUInt(pohInsert);
    mEncodeUInt(timingMode);
    mEncodeUInt(stsPayloadType);
    mEncodeChannelIdString(timingSource);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030080SdhAuVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60030080SdhAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc newVc = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVc == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newVc, channelId, channelType, module);
    }

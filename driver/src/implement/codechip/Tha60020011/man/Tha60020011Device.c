/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60020011Device.c
 *
 * Created Date: May 20, 2015
 *
 * Description : Device 60020011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmPw/man/ThaStmPwDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60020011DeviceMethods
    {
    uint8 dummy;
    }tTha60020011DeviceMethods;

typedef struct tTha60020011Device
    {
    tThaStmPwDevice super;
    const tTha60020011DeviceMethods *methods;
    }tTha60020011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Supper implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtModulePw Tha60020011ModulePwNew(AtDevice device);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60020011Device);
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
    AtUnused(self);
    return 2;
    }

static eAtRet DefaultIpCoreSetup(AtDevice self)
    {
    static const uint8 secondCore = 1;

    if (AtDeviceNumIpCoresGet(self) == 1)
        return cAtOk;

    /* Just set default core for modules that belong to the second core */
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cAtModulePdh)   , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModulePwe)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleBmt)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleCos)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cAtModuleEth)   , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleCla)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleDemap), secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModuleCdr)  , secondCore);
    AtModuleDefaultCoreSet(AtDeviceModuleGet(self, cThaModulePwPmc), secondCore);

    return cAtOk;
    }

static eAtRet SSKeyCheck(AtDevice self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModulePw)   return (AtModule)Tha60020011ModulePwNew(self);
    if (phyModule == cThaModulePoh) return (AtModule)ThaModulePohV1New(self);

    /* Let super create other modules */
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool CanCheckPll(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice thaDev = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(thaDev), sizeof(tThaDeviceMethods));
        mMethodOverride(m_ThaDeviceOverride, CanCheckPll);
        }

    mMethodsSet(thaDev, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, NumIpCoresGet);
        mMethodOverride(m_AtDeviceOverride, DefaultIpCoreSetup);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheck);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60020011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

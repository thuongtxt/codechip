/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60031131EthHigigPacket.c
 *
 * Created Date: Sep 26, 2014
 *
 * Description : ETH packet of Product 60031131
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031131EthHigigPacket
    {
    tThaStmPwProductEthPacket super;
    }tTha60031131EthHigigPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPacketMethods m_AtEthPacketOverride;

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 ThaEthHigigPacketHeaderDisplay(AtEthPacket self, uint8 level);
extern uint8 ThaEthHigigPacketHeaderLength(AtEthPacket self);

/*--------------------------- Implementation ---------------------------------*/
static uint8 DisplayPreamble(AtEthPacket self, uint8 level)
    {
    return ThaEthHigigPacketHeaderDisplay(self, level);
    }

static uint8 PreambleLen(AtEthPacket self)
    {
    return ThaEthHigigPacketHeaderLength(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031131EthHigigPacket);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket packet = (AtEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, mMethodsGet(packet), sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, DisplayPreamble);
        mMethodOverride(m_AtEthPacketOverride, PreambleLen);
        }

    mMethodsSet(packet, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtEthPacket(self);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket Tha60031131EthHigigPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

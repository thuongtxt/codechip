/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60031131ModulePktAnalyzer.c
 *
 * Created Date: Sep 26, 2014
 *
 * Description : Packet analyzer module of product 60031131
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031131ModulePktAnalyzer
    {
    tThaStmPwProductModulePktAnalyzer super;
    }tTha60031131ModulePktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePktAnalyzerMethods  m_AtModulePktAnalyzerOverride;
static tThaModulePktAnalyzerMethods m_ThaModulePktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
	AtUnused(self);
    return Tha60031131EthPortPktAnalyzerNew(port);
    }

static ThaVersionReader VersionReader(ThaModulePktAnalyzer self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportsHigig(ThaModulePktAnalyzer self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x14, 0x09, 0x29, 0x22);
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void OverrideThaModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    ThaModulePktAnalyzer analyzer = (ThaModulePktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaModulePktAnalyzerOverride));

        mMethodOverride(m_ThaModulePktAnalyzerOverride, StartVersionSupportsHigig);
        }

    mMethodsSet(analyzer, &m_ThaModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtModulePktAnalyzer(self);
    OverrideThaModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031131ModulePktAnalyzer);
    }

static AtModulePktAnalyzer ObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer Tha60031131ModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60031131EthPortPktAnalyzer.c
 *
 * Created Date: Sep 26, 2014
 *
 * Description : Ethernet Port Analyzer of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031131EthPortPktAnalyzer
    {
    tThaStmPwProductEthPortPktAnalyzer super;
    }tTha60031131EthPortPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/* Save super implementation */
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern uint32 Tha10GProductPktAnalyzerStartAddress(ThaPktAnalyzer self);
extern uint32 Tha10GPktAnalyzerEthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode newPktDumpMode);
extern uint32 Tha10GPktAnalyzerEthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 newPktDumpMode);
extern eBool Tha10GbPktAnalyzerShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031131EthPortPktAnalyzer);
    }

static eBool HigigIsSupported(AtPktAnalyzer self)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    ThaModulePktAnalyzer analyzer = (ThaModulePktAnalyzer)AtDeviceModuleGet(device, cAtModulePktAnalyzer);
    return ThaModulePktAnalyzerHigigEthPacketIsSupported(analyzer);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    if (HigigIsSupported(self))
        return Tha60031131EthHigigPacketNew(data, length, cAtPacketCacheModeCacheData);
    return m_AtPktAnalyzerMethods->PacketCreate(self, data, length, direction);
    }

static uint32 StartAddress(ThaPktAnalyzer self)
    {
    return Tha10GProductPktAnalyzerStartAddress(self);
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    return Tha10GPktAnalyzerEthPortHwDumpMode(self, pktDumpMode);
    }

static uint32 EthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
    return Tha10GPktAnalyzerEthPortDumpModeHw2Sw(self, pktDumpMode);
    }

static ThaPacketReader TxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static ThaPacketReader RxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static eBool ShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode)
    {
    return Tha10GbPktAnalyzerShouldRecapture(self, newPktDumpMode, currentDumpMode);
    }

static void Recapture(AtPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    uint32 regVal;
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(pktAnalyzer);

    regVal = ThaPktAnalyzerRead(pktAnalyzer, regAddr);
    mFieldIns(&regVal, cThaDebugDumPktCtlRoMask, cThaDebugDumPktCtlRoShift, 0);
    ThaPktAnalyzerWrite(pktAnalyzer, regAddr,  regVal);
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	ThaPktAnalyzerPktDumpModeSet(self, pktDumpMode);
	}

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortHwDumpMode);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortDumpModeHw2Sw);
        mMethodOverride(m_ThaPktAnalyzerOverride, ShouldRecapture);
        mMethodOverride(m_ThaPktAnalyzerOverride, TxPacketReaderCreate);
        mMethodOverride(m_ThaPktAnalyzerOverride, RxPacketReaderCreate);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60031131EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

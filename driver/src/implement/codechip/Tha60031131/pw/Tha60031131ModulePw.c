/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60031131ModulePw.c
 *
 * Created Date: Oct 10, 2014
 *
 * Description : Module PW of product 60031131
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pw/ThaStmPwProductModulePw.h"
#include "../../default/ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031131ModulePw
    {
    tThaStmPwProductModulePw super;
    }tTha60031131ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DidChangeCVlanOnPw(ThaModulePw self, AtPw pwAdapter, const tAtEthVlanTag *cVlan)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);

    /* If not cVlan, do nothing */
    if (cVlan == NULL)
        return cAtOk;

    return ThaPwAdapterStmPortSet(pwAdapter, ThaStmPwProductModuleEthPortIdFromCVlan(ethModule, cVlan));
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, DidChangeCVlanOnPw);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031131ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60031131ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

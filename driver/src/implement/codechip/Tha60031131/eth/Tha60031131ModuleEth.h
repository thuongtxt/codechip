/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module Ethernet
 * 
 * File        : Tha60031131ModuleEth.h
 * 
 * Created Date: Oct 14, 2014
 *
 * Description : Module ethernet of 60031131
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031131MODULEETH_H_
#define _THA60031131MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031131ModuleEth
    {
    tThaStmPwProductModuleEth super;
    }tTha60031131ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60031131ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#endif /* _THA60031131MODULEETH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth
 *
 * File        : Tha60031131ModuleEth.c
 *
 * Created Date: Jun 30, 2014
 *
 * Description : Module Ethernet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031131ModuleEth.h"
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/eth/ThaEthPort10GbReg.h"
#include "../../../default/prbs/ThaPrbsEngineSerdes.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/* Save super implementation */
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return ThaEthPort10GbNew(portId, self);
    }

static eBool PortShouldByPassFcs(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtTrue;
    }

static AtEthFlowControl PortFlowControlCreate(AtModuleEth self, AtEthPort port)
    {
    return ThaEthPortFlowControl10GbNew((AtModule)self, port);
    }

static eBool PortPllCanBeChecked(ThaModuleEth self, uint8 portId)
    {
    if (portId == 1)
        return cAtFalse;
    return m_ThaModuleEthMethods->PortPllCanBeChecked(self, portId);
    }

static eBool Port10GbSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool ApsIsSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
	AtUnused(self);
	AtUnused(serdesId);
    return Tha60091132PrbsEngineSerdesEthPortNew(AtEthPortSerdesController(ethPort));
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return cThaReg10GMacBaseAddress;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031131ModuleEth);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, PortFlowControlCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth thaModuleEth = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(thaModuleEth);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, m_ThaModuleEthMethods, sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortPllCanBeChecked);
        mMethodOverride(m_ThaModuleEthOverride, PortShouldByPassFcs);
        mMethodOverride(m_ThaModuleEthOverride, Port10GbSupported);
        mMethodOverride(m_ThaModuleEthOverride, ApsIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        }

    mMethodsSet(thaModuleEth, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

AtModuleEth Tha60031131ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60031131ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60031131ModuleEthObjectInit(newModule, device);
    }

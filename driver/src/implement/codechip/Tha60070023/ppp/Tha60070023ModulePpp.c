/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60070023ModulePpp.c
 *
 * Created Date: Jan 14, 2014
 *
 * Description : PPP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/ppp/ThaStmMlpppModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070023ModulePpp
    {
    tThaStmMlpppModulePpp super;
    }tTha60070023ModulePpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePppMethods m_ThaModulePppOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DirectOam(ThaModulePpp self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePpp(AtModulePpp self)
    {
    ThaModulePpp thaPppModule = (ThaModulePpp)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, mMethodsGet(thaPppModule), sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, DirectOam);
        }

    mMethodsSet(thaPppModule, &m_ThaModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideThaModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070023ModulePpp);
    }

static AtModulePpp ObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60070023ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }


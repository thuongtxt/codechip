/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60070023Device.h
 * 
 * Created Date: Dec 18, 2013
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070023DEVICE_H_
#define _THA60070023DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../ThaStmMlppp/man/ThaStmMlpppDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60070023Device
    {
    tThaStmMlpppDevice super;

    /* Private */
    }tTha60070023Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60070023DEVICE_H_ */


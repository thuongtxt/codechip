/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60070023SdhLine.c
 *
 * Created Date: May 8, 2013
 *
 * Description : SDH Line of product 60070023
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/sdh/ThaStmMlpppModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define Tha60070023SerdesLosStatus 0xF00062

/*
 * bit3_0 for OCN_LOS from PIN_SFP
 * bit8,6,5,4 for OCN_LOS from output XCVR_OCN
 */
#define Tha60070023SfpPinStatus(line) (cBit0 << (line))
#define Tha60070023XcvrOcnStatus(line) ((line == 3) ? cBit8 : cBit4 << (line))

#define Tha60070023RxClockAndSysClockPpmStatus 0x40010
#define Tha60070023RxClockAndSysClockPpmLineMask(line) (cBit20 << (line))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070023SdhLine
    {
    tThaStmMlpppSdhLine super;

    /* Private data */
    }tTha60070023SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070023SdhLine);
    }

static void PrintStatus(uint32 status, const char *raiseStr, const char* clearStr)
    {
    if (status)
        AtPrintc(cSevCritical, "%s", raiseStr);
    else
        AtPrintc(cSevNormal, "%s", clearStr);
    }

static eAtRet Debug(AtChannel self)
    {
    uint32 regVal, lineId;

    /* Super */
    m_AtChannelMethods->Debug(self);

    /* Serdes status */
    lineId = AtChannelIdGet(self);
    AtPrintc(cSevInfo, "\r\nSerdes line %u :", lineId);

    regVal = mChannelHwRead(self, Tha60070023SerdesLosStatus, cAtModuleSdh);

    AtPrintc(cSevNormal, "   SFP_Pin: ");
    PrintStatus(Tha60070023SfpPinStatus(lineId) & regVal, "LOS", "clear");

    AtPrintc(cSevNormal, "   XCVR_OCN: ");
    PrintStatus(Tha60070023XcvrOcnStatus(lineId) & regVal, "LOS", "clear");

    AtPrintc(cSevNormal, "\r\n");

    /* PPM of RX clock and system clock is too large */
    AtPrintc(cSevInfo, "PPM of RX clock and system clock line %u : ", lineId);

    /* Clear */
    mChannelHwWrite(self, Tha60070023RxClockAndSysClockPpmStatus, Tha60070023RxClockAndSysClockPpmLineMask(lineId), cAtModuleSdh);

    regVal = mChannelHwRead(self, Tha60070023RxClockAndSysClockPpmStatus, cAtModuleSdh);
    PrintStatus(Tha60070023RxClockAndSysClockPpmLineMask(lineId) & regVal, "TOO LARGE", "normal");

    AtPrintc(cSevNormal, "\r\n\r\n");
    return cAtOk;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forcibleAlarms = m_AtChannelMethods->RxForcibleAlarmsGet(self);
    return forcibleAlarms & (uint32)(~cAtSdhLineAlarmLos);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60070023SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module, version);
    }

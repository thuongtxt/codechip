/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60070023ModuleSdh.c
 *
 * Created Date: May 8, 2013
 *
 * Description : SDH module of product 60070023
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/sdh/ThaStmMlpppModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070023ModuleSdh
    {
    tThaStmMlpppModuleSdh super;
    }tTha60070023ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementations */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha60070023SdhLineNew(channelId, (AtModuleSdh)self, version);

    /* Ask super for other channel types */
    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static eBool SsBitCompareIsEnabledAsDefault(AtModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    uint8 lineId = (uint8)ThaModuleSdhLineLocalId(line);
    AtUnused(self);

    if ((lineId == 0) || (lineId == 1))
        return lineId;

    if (lineId == 2) return 4;
    if (lineId == 3) return 8;
    if (lineId == 4) return 5;
    if (lineId == 5) return 9;

    return 0;
    }

static eAtSerdesTimingMode DefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line)
    {
    if (AtSdhLineRateGet(line) != cAtSdhLineRateStm1)
        return cAtSerdesTimingModeAuto;

    return m_ThaModuleSdhMethods->DefaultSerdesTimingModeForLine(self, line);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, DefaultSerdesTimingModeForLine);
        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));
        mMethodOverride(m_AtModuleSdhOverride, SsBitCompareIsEnabledAsDefault);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070023ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60070023ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

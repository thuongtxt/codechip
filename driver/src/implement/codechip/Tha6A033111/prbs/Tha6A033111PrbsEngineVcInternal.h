/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010PrbsEngine.h
 * 
 * Created Date: Dec 2, 2015
 *
 * Description : LO-Path PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha6A033111PRBSENGINEVCINTERNAL_H_
#define _Tha6A033111PRBSENGINEVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
#include "../../Tha60290021/sdh/Tha60290021SdhLineSideAug.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A033111PrbsEngineVcNew(AtSdhChannel vc);

#ifdef __cplusplus
}
#endif
#endif /* _Tha6A033111PRBSENGINEVCINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031PrbsEngineVc.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : PRBS engine VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha6A033111ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A033111LineDccHdlcPrbsEngine *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tSimulationDb
    {
    eBool txEnable;
    eBool rxEnable;

    eBool txInvert;
    eBool rxInvert;

    eAtPrbsMode txPrbsMode;
    eAtPrbsMode rxPrbsMode;

    uint32 txFixedPattern;
    uint32 rxFixedPattern;

    eAtPrbsSide generatingSide;
    eAtPrbsSide monitoringSide;

    eAtPrbsBitOrder txOrder;
    eAtPrbsBitOrder rxOrder;

    eAtBerRate errorRate;
    eBool force;
    uint32 numErrorForced;
    }tSimulationDb;

typedef struct tTha6A033111LineDccHdlcPrbsEngine
    {
	tAtPrbsEngine super;

	/* Private data */
	tSimulationDb simDb;
    }tTha6A033111LineDccHdlcPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HwIsReady(void)
    {
    /* TODO: Remove this when HW is ready. Also remove all of logics related to
     * simulation */
    return cAtFalse;
    }

static tSimulationDb *SimDb(AtPrbsEngine self)
    {
    /* If hardware is ready, hardware must be directly accessed. Detail
     * implementation do not need to check NULL, let's crashing happen to
     * enforce real HW implementation */
    if (HwIsReady())
        return NULL;

    return &(mThis(self)->simDb);
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
	{
    SimDb(self)->txEnable = enable;
	return cAtOk;
	}

static eBool TxIsEnabled(AtPrbsEngine self)
	{
	return SimDb(self)->txEnable;
	}

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
	{
    SimDb(self)->rxEnable = enable;
	return cAtOk;
	}

static eBool RxIsEnabled(AtPrbsEngine self)
	{
	return SimDb(self)->rxEnable;
	}

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
	{
    SimDb(self)->txPrbsMode = prbsMode;
	return cAtOk;
	}

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
	{
	return SimDb(self)->txPrbsMode;
	}

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
	{
    SimDb(self)->rxPrbsMode = prbsMode;
	return cAtOk;
	}

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
	{
	return SimDb(self)->rxPrbsMode;
	}

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
	{
	if (SimDb(self))
	    return cAtTrue;
	return m_AtPrbsEngineMethods->ModeIsSupported(self, prbsMode);
	}

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
	{
    SimDb(self)->txInvert = invert;
	return cAtOk;
	}

static eBool TxIsInverted(AtPrbsEngine self)
    {
	return SimDb(self)->txInvert;
	}

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
	{
    SimDb(self)->rxInvert = invert;
	return cAtOk;
	}

static eBool RxIsInverted(AtPrbsEngine self)
	{
	return SimDb(self)->rxInvert;
	}

static eBool InversionIsSupported(AtPrbsEngine self)
	{
	if (SimDb(self))
	    return cAtTrue;
	return m_AtPrbsEngineMethods->InversionIsSupported(self);
	}

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
	{
    SimDb(self)->txFixedPattern = fixedPattern;
	return cAtOk;
	}

static uint32 TxFixedPatternGet(AtPrbsEngine self)
	{
	return SimDb(self)->txFixedPattern;
	}

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
	{
    SimDb(self)->rxFixedPattern = fixedPattern;
	return cAtOk;
	}

static uint32 RxFixedPatternGet(AtPrbsEngine self)
	{
	return SimDb(self)->rxFixedPattern;
	}

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
	{
	if (SimDb(self))
	    return cAtTrue;
	return m_AtPrbsEngineMethods->SideIsSupported(self, side);
	}

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
	{
    SimDb(self)->generatingSide = side;
	return cAtOk;
	}

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
	{
	return SimDb(self)->generatingSide;
	}

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
	{
    SimDb(self)->monitoringSide = side;
	return cAtOk;
	}

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
	{
    return SimDb(self)->monitoringSide;
	}

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
	{
    SimDb(self)->txOrder = order;
	return cAtOk;
	}

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
	{
    return SimDb(self)->txOrder;
	}

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
	{
    SimDb(self)->rxOrder = order;
	return cAtOk;
	}

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
	{
    return SimDb(self)->rxOrder;
	}

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
	{
    SimDb(self)->force = force;
	return cAtOk;
	}

static eBool ErrorIsForced(AtPrbsEngine self)
	{
	return SimDb(self)->force;
	}

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrorForced)
	{
    SimDb(self)->numErrorForced = numErrorForced;
	return cAtOk;
	}

static eBool TxErrorInjectionIsSupported(AtPrbsEngine self)
	{
	if (SimDb(self))
	    return cAtTrue;
	return m_AtPrbsEngineMethods->TxErrorInjectionIsSupported(self);
	}

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
	{
    SimDb(self)->errorRate = errorRate;
	return cAtOk;
	}

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
	{
	return SimDb(self)->errorRate;
	}

static eBool ErrorForcingRateIsSupported(AtPrbsEngine self, eAtBerRate errorRate)
	{
	if (SimDb(self))
	    return cAtTrue;
	return m_AtPrbsEngineMethods->ErrorForcingRateIsSupported(self, errorRate);
	}

static eBool SeparateTwoDirections(AtPrbsEngine self)
	{
    if (SimDb(self))
        return cAtTrue;
    return m_AtPrbsEngineMethods->SeparateTwoDirections(self);
	}

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
		mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
		mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
		mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
		mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
		mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
		mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
		mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
		mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
		mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
		mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
		mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
		mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
		mMethodOverride(m_AtPrbsEngineOverride, InversionIsSupported);
		mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
		mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
		mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
		mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
		mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
		mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
		mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
		mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
		mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
		mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
		mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderGet);
		mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
		mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderGet);
		mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
		mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
		mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);
		mMethodOverride(m_AtPrbsEngineOverride, TxErrorInjectionIsSupported);
		mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
		mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
		mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingRateIsSupported);
		mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);

        }
    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }


static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111LineDccHdlcPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtHdlcChannel hdlc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, (AtChannel)hdlc, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A033111LineDccHdlcPrbsEngineNew(AtHdlcChannel hdlc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, hdlc);
    }

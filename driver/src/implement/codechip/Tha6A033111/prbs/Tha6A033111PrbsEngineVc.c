/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A033111PrbsEngineHoVc.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : High-Order PRBS of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtSdhPath.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021XcHiding.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Tha6A033111PrbsEngineVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A033111PrbsEngineVc
    {
    tTha6A000010PrbsEngine super;
    }tTha6A033111PrbsEngineVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;
static tAtPrbsEngineMethods           m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel XcHidingPairAugTerVc(AtSdhChannel self, AtSdhLine pairLine, uint8 partId)
    {
    uint8 pairSts1 = (uint8) (Tha60290021XcHidingPairSts1Id(self) + (partId * 48));
    uint32 augType = AtSdhChannelTypeGet(self);
    if (!pairLine)
        return NULL;

    switch (augType)
        {
        case cAtSdhChannelTypeVc4_16c:
            return (AtSdhChannel)AtSdhLineVc4_16cGet(pairLine, partId);

        case cAtSdhChannelTypeVc4_4c:
            return (AtSdhChannel)AtSdhLineVc4_4cGet(pairLine, pairSts1 / 12);

        case cAtSdhChannelTypeVc4:
            return (AtSdhChannel)AtSdhLineVc4Get(pairLine, pairSts1 / 3);

        case cAtSdhChannelTypeVc3:
            return (AtSdhChannel)AtSdhLineVc3Get(pairLine, pairSts1 / 3, pairSts1 % 3);

        default:
            return NULL;
        }
    }

static AtModuleXc XcModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eTha60290021XcHideMode XcHideMode(AtSdhChannel self)
    {
    return Tha60290021ModuleXcHideModeGet(XcModule(self));
    }

static eBool FacePlateOnStm64(AtSdhChannel self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh) AtChannelModuleGet((AtChannel) self);
    AtSdhLine line = AtModuleSdhLineGet(moduleSdh, 0);
    return (AtSdhLineRateGet(line) == cAtSdhLineRateStm64);
    }

static AtModuleSdh SdhModule(AtChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet(self);
    }

static uint8 PartId(AtSdhChannel self)
    {
    if (FacePlateOnStm64(self))
        {
        uint8 lineId = AtSdhChannelLineGet(self);
        return (uint8) (lineId - Tha60290021ModuleSdhTerminatedLineStartId(SdhModule((AtChannel) self)));
        }

    return 0;
    }

static AtSdhLine XcHidingFacePlateLineGet(AtSdhChannel self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh) AtChannelModuleGet((AtChannel) self);
    uint8 lineId = AtSdhChannelLineGet(self);

    if (!Tha60290021ModuleSdhLineIsTerminated(moduleSdh, lineId))
        return NULL;

    if (FacePlateOnStm64(self))
        return AtModuleSdhLineGet(moduleSdh, 0);

    /* Another Rate */
    lineId = (uint8)(lineId - Tha60290021ModuleSdhTerminatedLineStartId(moduleSdh));
    return AtModuleSdhLineGet(moduleSdh, lineId);
    }

static AtSdhChannel TerminateVcGetFacePlateVc(AtSdhChannel self)
    {
    eTha60290021XcHideMode hideMode = XcHideMode(self);
    if (hideMode == cTha60290021XcHideModeDirect)
        return XcHidingPairAugTerVc(self, XcHidingFacePlateLineGet(self), PartId(self));

    return self;
    }

static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);
    }

static AtChannel CircuitToBind(Tha6A000010PrbsEngine self, AtChannel channel)
    {
    AtUnused(self);
    return (AtChannel)TerminateVcGetFacePlateVc((AtSdhChannel)channel);
    }

static eAtModulePrbsRet PohDefaultSet(Tha6A000010PrbsEngine self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)mMethodsGet(self)->CircuitToBind(self, AtPrbsEngineChannelGet((AtPrbsEngine)self));
    uint8 startSts, sts_i;
    eAtModulePrbsRet ret = cAtOk;

    startSts = AtSdhChannelSts1Get(sdhChannel);
    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        ret |= ThaOcnStsPohInsertEnable(sdhChannel, (uint8)(startSts + sts_i), cAtTrue);

    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);

    return ret;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);

    if (ret != cAtOk)
        return ret;

    return PohDefaultSet((Tha6A000010PrbsEngine)self);
    }

static eBool XcHidingShouldRedirect(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet(self);
    return (Tha60290021ModuleXcHideModeGet((AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc)) == cTha60290021XcHideModeDirect);
    }

static const char *ChannelDescription(AtPrbsEngine self)
    {
    if (XcHidingShouldRedirect(self))
        {
        AtChannel terVc = AtPrbsEngineChannelGet(self);
        return AtChannelIdDescriptionBuild(AtChannelSourceGet(terVc));
        }

    return m_AtPrbsEngineMethods->ChannelDescription(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, ChannelDescription);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111PrbsEngineVc);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, CircuitToBind);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A033111PrbsEngineVcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, vc);
    }

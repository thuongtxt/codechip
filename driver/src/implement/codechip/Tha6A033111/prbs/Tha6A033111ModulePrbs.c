/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031ModulePrbs.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha6A210031 module PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../Tha6A000010/prbs/Tha6A000010ModulePrbsInternal.h"
#include "Tha6A033111ModulePrbs.h"

#include "Tha6A033111PrbsEngineVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A033111ModulePrbs
    {
    tTha6A000010ModulePrbs super;
    }tTha6A033111ModulePrbs;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsMethods m_AtModulePrbsOverride;

/* Save super implementation */
static const tAtModulePrbsMethods *m_AtModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine LineDccHdlcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtHdlcChannel hdlc)
	{
	AtUnused(self);
	AtUnused(engineId);
	return Tha6A033111LineDccHdlcPrbsEngineNew(hdlc);
	}

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);
    AtUnused(engineId);

    if ((channelType == cAtSdhChannelTypeVc4) ||
        (channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeVc4_16c))
        return Tha6A033111PrbsEngineVcNew(sdhVc);

    if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            return Tha6A033111PrbsEngineVcNew(sdhVc);
        return Tha6A033111PrbsEngineVcNew(sdhVc);
        }

    return m_AtModulePrbsMethods->SdhVcPrbsEngineCreate(self, engineId, sdhVc);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, LineDccHdlcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);

    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A033111ModulePrbsNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return ObjectInit(newModule, device);
    }

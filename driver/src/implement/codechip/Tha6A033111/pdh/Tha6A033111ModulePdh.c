/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A033111ModulePdh.c
 *
 * Created Date: Dec 3, 2015
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha6A210031/pdh/Tha6A210031ModulePdh.h"
#include "../../Tha60210011/pdh/Tha60210011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cReg_upen_mch_cfg_Base 0x00080f80

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A033111ModulePdh
    {
    tTha60210011ModulePdh super;
    }tTha6A033111ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModulePdhMethods m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eAtRet Init(AtModule self)
    {
    uint32 i;
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    for (i = 0; i < ThaModulePdhNumSlices((ThaModulePdh)self); i++)
        {
        uint32 regAddr = cReg_upen_mch_cfg_Base + ThaModulePdhSliceBase((ThaModulePdh)self, i);
        mModuleHwWrite(self, regAddr, 0x1);
        }

    return cAtOk;
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

#if 0
static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }
#endif

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    /*OverrideAtModulePdh(self);*/
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha6A033111ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

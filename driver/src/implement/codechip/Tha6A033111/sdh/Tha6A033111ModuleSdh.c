/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021ModuleSdh.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : PWCodechip-60290021 SDH module.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdhInternal.h"
#include "../encap/Tha6A033111HdlcChannel.h"
#include "Tha6A033111SdhFacePlateLineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A033111ModuleSdh
    {
	tTha60290021ModuleSdh super;
    }tTha6A033111ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tTha60290021ModuleSdhMethods m_Tha60290021ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods          = NULL;
static const tTha60290021ModuleSdhMethods *m_Tha60290021ModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHdlcChannel DccChannelObjectCreate(Tha60290021ModuleSdh self, uint32 dccId,  AtSdhLine line, eAtSdhLineDccLayer layers)
	{
	return Tha6A033111HdlcChannelNew(dccId, line, layers, (AtModuleSdh)self);
	}

static uint8 FaceplateLineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateLineStopId(AtModuleSdh self)
    {
    AtUnused(self);
    return 15;
    }

static uint8 MateLineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 16;
    }

static uint8 MateLineStopId(AtModuleSdh self)
    {
    AtUnused(self);
    return 23;
    }

static uint8 TerminatedLineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 24;
    }

static uint8 TerminatedLineStopId(AtModuleSdh self)
    {
    AtUnused(self);
    return 31;
    }

static eBool IsFaceplateLine(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, FaceplateLineStartId(self), FaceplateLineStopId(self));
    }

static eBool IsMateLine(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, MateLineStartId(self), MateLineStopId(self));
    }

static eBool IsTerminatedLine(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, TerminatedLineStartId(self), TerminatedLineStopId(self));
    }

static AtSdhVc VcCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
        return (AtSdhVc)m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);

    if (IsFaceplateLine(self, lineId))
        return Tha6A033111SdhFacePlateLineAuVcNew(channelId, channelType, self);

    if (IsMateLine(self, lineId))
        return Tha6029SdhMateLineAuVcNew(channelId, channelType, self);

    if (IsTerminatedLine(self, lineId))
        return Tha6029SdhTerminatedLineAuVcNew(channelId, channelType, self);

    return NULL;
    }


static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    switch (channelType)
        {
        /* Create VC */
        case cAtSdhChannelTypeVc4_16c:
        case cAtSdhChannelTypeVc4_4c:
        case cAtSdhChannelTypeVc4:
        case cAtSdhChannelTypeVc3:
            return (AtSdhChannel)VcCreate(self, lineId, parent, channelType, channelId);

        default:
            return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
        }
    }

static void OverrideTha60290021ModuleSdh(AtModuleSdh self)
    {
    Tha60290021ModuleSdh sdh = (Tha60290021ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleSdhMethods = mMethodsGet(sdh);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleSdhOverride, m_Tha60290021ModuleSdhMethods, sizeof(m_Tha60290021ModuleSdhOverride));

        mMethodOverride(m_Tha60290021ModuleSdhOverride, DccChannelObjectCreate);
        }

    mMethodsSet(sdh, &m_Tha60290021ModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideTha60290021ModuleSdh(self);
    OverrideAtModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha6A033111ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhFacePlateLineAuVc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC FacePlate Line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A033111SdhFacePlateLineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A033111SdhFacePlateLineAuVc *)self)

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha6A033111SdhFacePlateLineAuVc
    {
    tTha6029SdhFacePlateLineAuVc super;
    }tTha6A033111SdhFacePlateLineAuVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;

/* Super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eBool XcHidingShouldRedirect(AtChannel self)
    {
    return (Tha60290021ModuleXcHideModeGet(XcModule(self)) == cTha60290021XcHideModeDirect);
    }

static AtChannel TerminatedVcFromFacePlateVc(AtChannel self)
    {
    AtCrossConnect crossConnect = AtModuleXcVcCrossConnectGet(XcModule(self));
    return AtCrossConnectDestChannelGetByIndex(crossConnect, self, 0);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (XcHidingShouldRedirect(self))
        return m_AtChannelMethods->CounterGet(TerminatedVcFromFacePlateVc(self), counterType);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (XcHidingShouldRedirect(self))
        {
        return m_AtChannelMethods->CounterClear(TerminatedVcFromFacePlateVc(self), counterType);
        }
    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111SdhFacePlateLineAuVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhFacePlateLineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6A033111SdhFacePlateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029SerdesManager.h
 * 
 * Created Date: Oct 1, 2016
 *
 * Description : Abstract SERDES manager for 6029xxxx products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A033111SERDESMANAGER_H_
#define _THA6A033111SERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha6A033111SerdesManagerNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A033111SERDESMANAGER_H_ */


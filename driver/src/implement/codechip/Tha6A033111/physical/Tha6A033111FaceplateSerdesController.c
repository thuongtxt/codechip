/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6029FaceplateSerdesController.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Faceplate SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A033111FaceplateSerdesControllerInternal.h"
#include "../../../default/physical/ThaSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesEpLoopRegAddr    (0xF50008)
#define cSerdesResetRegAddr     (0xF50006)

#define cLoopOutMode            (0x6)
#define cLoopInMode             (0x2)
/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029FaceplateSerdesController *)self)
#define mSerdesLoopMask(_portId) (cBit2_0 << (_portId * 3))
#define mSerdesLoopShift(_portId) (_portId*3)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    uint32 portId = AtSerdesControllerHwIdGet(self);

    if ((portId == 0) || (portId == 4) || (portId == 8) || (portId == 12))
        return cAtSerdesModeStm16;

    return cAtSerdesModeStm4;
    }

static uint32 BaseAddress(AtSerdesController self)
    {
    AtUnused(self);
    return 0xF50000;
    }

static eAtRet Reset(AtSerdesController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtOk;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    /* K7 board: fix mode stm64 */
    AtUnused(self);
    return cAtSerdesModeStm64;
    }

static uint32 SwToHwLoopMode(eAtLoopbackMode loopbackMode)
    {
    if (cAtLoopbackModeLocal == loopbackMode)
        return cLoopInMode;

    if (cAtLoopbackModeRemote == loopbackMode)
        return cLoopOutMode;

    return 0x0;
    }

static void SerdesReset(AtSerdesController self)
    {
    eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(self)));
    AtSerdesControllerWrite(self, cSerdesResetRegAddr, 0xB, moduleId);
    AtOsalSleep(1);
    AtSerdesControllerWrite(self, cSerdesResetRegAddr, 0x8, moduleId);

    /* HW need 5s to get stable on EP5 serdes */
    AtOsalSleep(5);
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(self)));
    uint32 lineId = AtChannelIdGet(AtSerdesControllerPhysicalPortGet(self));
    uint32 regVal = AtSerdesControllerRead(self, cSerdesEpLoopRegAddr, moduleId);
    uint32 hwLoopMode = SwToHwLoopMode(loopbackMode);

    mFieldIns(&regVal, mSerdesLoopMask(lineId), mSerdesLoopShift(lineId), enable ? hwLoopMode : 0x0);
    AtSerdesControllerWrite(self, cSerdesEpLoopRegAddr, regVal, moduleId);
    SerdesReset(self);
    
    return cAtOk;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(self)));
    uint32 lineId = AtChannelIdGet(AtSerdesControllerPhysicalPortGet(self));
    uint32 loopMask = mSerdesLoopMask(lineId);
    uint32 loopShift = mSerdesLoopShift(lineId);
    uint32 regVal = AtSerdesControllerRead(self, cSerdesEpLoopRegAddr, moduleId);
    uint32 loopMode = SwToHwLoopMode(loopbackMode);

    return (mRegField(regVal, loop) == loopMode) ? cAtTrue : cAtFalse;
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111FaceplateSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029FaceplateSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6A033111FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

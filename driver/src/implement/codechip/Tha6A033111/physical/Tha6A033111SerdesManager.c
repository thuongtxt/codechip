/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha6A033111SerdesManager.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/physical/Tha60290021SerdesManagerInternal.h"
#include "Tha6A033111FaceplateSerdesControllerInternal.h"
#include "Tha6A033111SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesManagerMethods m_AtSerdesManagerOverride;

/* To save super implementation */
static const tAtSerdesManagerMethods *m_AtSerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumFaceplateSerdes(void)
    {
    return 4;
    }

static uint8 FaceplateSerdesStartId(AtSerdesManager self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateSerdesEndId(AtSerdesManager self)
    {
    AtUnused(self);
    return 3;
    }

static eBool IsFaceplateSerdes(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    return ((serdesId >= FaceplateSerdesStartId(self)) && (serdesId <= FaceplateSerdesEndId(self))) ? cAtTrue : cAtFalse;
    }

static  AtSerdesController SerdesControllerObjectCreate (AtSerdesManager self, uint32 serdesId)
    {
    if (IsFaceplateSerdes(self, serdesId))
        return Tha6A033111FaceplateSerdesControllerNew(self, NULL, serdesId);

    return NULL;
    }

static uint32 NumSerdesControllers(AtSerdesManager self)
    {
    AtUnused(self);
    return NumFaceplateSerdes();
    }

static eBool CanAccessRegister(AtSerdesManager self, uint32 serdesId, uint32 address, eAtModule moduleId)
    {
    AtUnused(moduleId);
    AtUnused(serdesId);
    AtUnused(address);
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtSerdesManager(AtSerdesManager self)
    {
    AtSerdesManager object = (AtSerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesManagerMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesManagerOverride, m_AtSerdesManagerMethods, sizeof(m_AtSerdesManagerOverride));

        mMethodOverride(m_AtSerdesManagerOverride, NumSerdesControllers);
        mMethodOverride(m_AtSerdesManagerOverride, SerdesControllerObjectCreate);
        mMethodOverride(m_AtSerdesManagerOverride, CanAccessRegister);
        }

    mMethodsSet(object, &m_AtSerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtSerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SerdesManager);
    }

static AtSerdesManager ObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha6A033111SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

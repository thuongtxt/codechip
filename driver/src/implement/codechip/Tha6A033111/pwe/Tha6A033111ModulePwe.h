/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210051ModulePwe.h
 * 
 * Created Date: Nov 6, 2015
 *
 * Description : 60210051 PWE interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A033111MODULEPWE_H_
#define _THA6A033111MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/pwe/Tha60210051ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A033111ModulePweNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A033111MODULEPWE_H_ */


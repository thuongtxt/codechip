/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha6A033111ModulePwe.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module PWE of 6A033111
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A033111ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A033111ModulePwe
    {
    tTha60210051ModulePwe super;
    }tTha6A033111ModulePwe;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods         m_ThaModulePweOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwIsEnabled);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A033111ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

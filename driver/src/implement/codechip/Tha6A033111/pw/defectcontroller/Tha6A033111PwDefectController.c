/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PW
 *
 * File        : Tha6A033111PwDefectController.c
 *
 * Created Date: Aug 5, 2015
 *
 * Description : PW channel defect control.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A033111PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha6A033111PwDefectController
    {
    tTha60210021PwDefectController super;
    }tTha6A033111PwDefectController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtPwMethods      m_AtPwOverride;

/* Save super's implementation */
static const tAtPwMethods *m_AtPwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefectGet(AtChannel self)
    {
    AtUnused(self);

    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void InterruptProcess(AtPw self, uint32 pwOffset, AtHal hal)
    {
    AtUnused(self);
    AtUnused(pwOffset);
    AtUnused(hal);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, InterruptProcess);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel pw = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(pw), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        }

    mMethodsSet(pw, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideAtPw(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwDefectController);
    }

static ThaPwDefectController ObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021PwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha6A033111PwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, pw);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290021ModuleOcn.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDK_THA6A033111MODULEOCNINTERNAL_H_
#define _ATSDK_THA6A033111MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/ocn/Tha60290021ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A033111ModuleOcn
    {
    tTha60290021ModuleOcn super;
    }tTha6A033111ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A033111ModuleOcnNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDK_THA6A033111MODULEOCNINTERNAL_H_ */


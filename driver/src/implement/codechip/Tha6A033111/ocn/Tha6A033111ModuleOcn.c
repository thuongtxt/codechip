/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha6A033111ModuleOcn.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : OCN source code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/ocn/Tha60290021ModuleOcnInternal.h"
#include "Tha6A033111ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods m_ThaModuleOcnOverride;

/* Supper implementation */
static const tThaModuleOcnMethods *m_ThaModuleOcnMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    ret |= Tha60290021ModuleOcnPohProcessorSideSet(self, cTha6029LineSideFaceplate);

    return ret;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111ModuleOcn);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A033111ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

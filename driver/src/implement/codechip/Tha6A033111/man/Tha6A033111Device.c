/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A210031Device.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha60210031 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/man/Tha60290021DeviceInternal.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"
#include "../physical/Tha6A033111SerdesManager.h"
#include "../pwe/Tha6A033111ModulePwe.h"
#include "../ocn/Tha6A033111ModuleOcnInternal.h"
#include "Tha6A033111InterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A033111Device
    {
	tTha60290021Device super;
    }tTha6A033111Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60290021DeviceMethods m_Tha60290021DeviceOverride;

/* Super implementations */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    if (moduleId  == cAtModulePrbs)    return (AtModule)Tha6A033111ModulePrbsNew(self);
    if (moduleId  == cAtModuleSdh)     return (AtModule)Tha6A033111ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)      return (AtModule)Tha6A033111ModulePwNew(self);
    if (moduleId  == cAtModuleEth)     return (AtModule)Tha6A033111ModuleEthNew(self);
    if (moduleId  == cAtModulePdh)     return (AtModule)Tha6A033111ModulePdhNew(self);

    /* PhyModule */
    if (phyModule  == cThaModuleMap)   return Tha6A033111ModuleMapNew(self);
    if (phyModule  == cThaModuleDemap) return Tha6A033111ModuleDemapNew(self);
    if (phyModule  == cThaModulePwe)   return Tha6A033111ModulePweNew(self);
    if (phyModule  == cThaModuleOcn)   return Tha6A033111ModuleOcnNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha6A033111SerdesManagerNew(self);
    }

static eAtRet AllModulesInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    /* Initialize all physical modules */
    ret |= AtDeviceModuleInit(self, cAtModuleXc);
    ret |= AtDeviceModuleInit(self, cAtModuleSdh);
    ret |= AtDeviceModuleInit(self, cAtModulePdh);
    ret |= AtDeviceModuleInit(self, cAtModulePw);

    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Init(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    ret |= Tha60290021ModuleXcHideModeSet((AtModuleXc)AtDeviceModuleGet(self, cAtModuleXc), cTha60290021XcHideModeDirect);
    AtHalWrite(hal, 0x1f0002, 0x13c7388f);
    AtHalWrite(hal, 0x1f0004, 0x33c);
    AtHalWrite(hal, 0xf00041, 0x2);

    return ret;
    }

static void HwFlush(Tha60150011Device self)
    {
    AtUnused(self);
    }

static eBool DiagnosticModeIsEnabled(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha6A033111IntrControllerNew(core);
    }

static eBool IsRunningOnOtherPlatform(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, AllModulesInit);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeIsEnabled);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideTha60290021Device(AtDevice self)
    {
    Tha60290021Device device = (Tha60290021Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021DeviceOverride, mMethodsGet(device), sizeof(m_Tha60290021DeviceOverride));

        mMethodOverride(m_Tha60290021DeviceOverride, IsRunningOnOtherPlatform);
        }

    mMethodsSet(device, &m_Tha60290021DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60290021Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A033111DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return ObjectInit(newDevice, driver, productCode);
    }

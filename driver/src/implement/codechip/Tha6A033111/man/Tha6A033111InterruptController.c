/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A033111InterruptController.c
 *
 * Created Date: Jul 14, 2015
 *
 * Description : PW CodeChip Interrupt Controller.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A033111InterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtIpCoreMethods  m_AtIpCoreOverride;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtIpCore self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtIpCore(ThaIntrController self)
    {
    AtIpCore core = (AtIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, mMethodsGet(core), sizeof(m_AtIpCoreOverride));

        mMethodOverride(m_AtIpCoreOverride, InterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, InterruptIsEnabled);
        }
    mMethodsSet(core, &m_AtIpCoreOverride);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideAtIpCore(self);
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A033111InterruptController);
    }

static ThaIntrController ObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011IntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha6A033111IntrControllerNew(AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, core);
    }

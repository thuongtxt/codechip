/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A033111InterruptControllerInternal.h
 *
 * Created Date: Jul 27, 2015
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A033111INTERRUPTCONTROLLERINTERNAL_H_
#define _THA6A033111INTERRUPTCONTROLLERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/man/Tha60210011InterruptControllerInternal.h"


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A033111InterruptController
    {
    tTha60210011InterruptController super;
    } tTha6A033111InterruptController;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha60210011IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);
ThaIntrController Tha6A033111IntrControllerNew(AtIpCore core);

#ifdef __cplusplus
}
#endif

#endif /* _THA6A033111INTERRUPTCONTROLLERINTERNAL_H_ */

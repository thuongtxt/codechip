/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60291022Device.c
 *
 * Created Date: Jul 23, 2018
 *
 * Description : 60291022 Device management module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#include "Tha60291022DeviceInternal.h"
#include "../physical/Tha60291022Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60290022DeviceMethods m_Tha60290022DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60290022DeviceMethods *m_Tha60290022DeviceMethods = NULL;
static const tTha60210011DeviceMethods *m_Tha60210011DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 phyModule = moduleId;

    /* Public modules */
    if (moduleId == cAtModulePdh)   return (AtModule)Tha60291022ModulePdhNew(self);
    if (moduleId == cAtModulePw)   return (AtModule)Tha60291022ModulePwNew(self);
    if (moduleId == cAtModuleSdh)   return (AtModule)Tha60291022ModuleSdhNew(self);

    /* Private modules */
    if (phyModule == cThaModuleCdr)   return Tha60291022ModuleCdrNew(self);
    if (phyModule == cThaModuleOcn)   return Tha60291022ModuleOcnNew(self);
    if (phyModule == cThaModuleCla)   return Tha60291022ModuleClaNew(self);
    if (phyModule == cThaModulePda)   return Tha60291022ModulePdaNew(self);
    if (phyModule == cThaModulePwe)   return Tha60291022ModulePweNew(self);
    if (phyModule == cThaModuleMap)   return Tha60291022ModuleMapNew(self);
    if (phyModule == cThaModuleDemap)   return Tha60291022ModuleDemapNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool ShouldOpenFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static eBool DeviceCapacity15G(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasKByteFeatureOnly(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HwLogicOptimized(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccV2IsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DeviceV3OptimizationIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Pmc1PageIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HoTxG1OverWriteIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccV4dot1EnableRegAddressIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ClockSquelchingIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ClockEthSquelchingIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthTxIpgIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccTxFcsMsbIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccSgmiiTxIpg31ByteIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Eth100MFxLfRfSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccHdlcTransmissionBitOrderPerChannelIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Sts192cPrbsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SemIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x2, 0x0000);
    }

static eBool OcnSeparatedSectionAndLineDccIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthPortFlowControlWaterMarkIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ClockEthSquelchingSelectOnCoreOnlyIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Sts192cTxG1OverrideIsNotUsed(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool Sts192cPrbsInvertionIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DcrAcrTxShapperSupportedVersion(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0x0000);
    }

static eBool ClockExtractorMonitorPpmIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TohSonetSdhModeIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CesopAutoRxMBitConfigurableIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SeparateLbitAndLopsPktReplacementSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateEthCounter32bitIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Eth100FxSerdesClock31Dot25MhzIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 MapDemapNumLoSlices(AtDevice self)
    {
    AtUnused(self);
    return 4;
    }

static eBool HasStandardClearTime(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsNewBerInterrupt(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplatSerdesEthHasK30_7Feature(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 FaceplatSerdesEthStartVersionSupportTxAlarmForcing(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool FaceplatSerdesEthExcessiveErrorRatioIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthHasSgmiiDiagnosticLogic(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SgmiiSohEthUseNewMac(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumberOfMapLoPwPerSlice(AtDevice self)
    {
    AtUnused(self);
    return 2688;
    }

static eBool NewTuVcLoopbackIsReady(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Sts192cIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PdaIsNewOc192cOffset(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PdaNumJitterBufferBlocksIncreased(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PdhStartVersionSupportPrm(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PdhStartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PdhStartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PdhRxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateSerdesPmaFarEndLoopbackSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool KByteSerdesIsControllable(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SgmiiSerdesShouldUseMdioLocalLoopback(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PohHasCpujnreqen(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PohStartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PrbsBertOptimizeIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwDccKbyteInterruptIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateFramerLocalLoopbackSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FaceplateSerdesStm1SamplingFromStm4(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccHdlcHasPacketCounterDuringFifoFull(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FacePlateSerdesDataMaskOnResetConfigurable(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ModuleBerMeasureTimeEngineIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PtpSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x2, 0x0000);
    }

static eBool HasApsWtr3SecondResolution(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x2, 0x0000);
    }

static eBool HasSoftResetForSem(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x2, 0x0000);
    }

static uint8 NumUsedOc48Slice(Tha60210011Device self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 DefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return cThaModulePmc;
    }

static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha60291022SerdesManagerNew(self);
    }

static eBool FaceplateSerdesHasNewParams(Tha60290022Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60290022Device(AtDevice self)
    {
    Tha60290022Device device = (Tha60290022Device)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022DeviceOverride, m_Tha60290022DeviceMethods, sizeof(m_Tha60290022DeviceOverride));

        mMethodOverride(m_Tha60290022DeviceOverride, DeviceCapacity15G);
        mMethodOverride(m_Tha60290022DeviceOverride, HasKByteFeatureOnly);
        mMethodOverride(m_Tha60290022DeviceOverride, HwLogicOptimized);
        mMethodOverride(m_Tha60290022DeviceOverride, DccV2IsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DeviceV3OptimizationIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Pmc1PageIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, HoTxG1OverWriteIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccV4dot1EnableRegAddressIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockSquelchingIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockEthSquelchingIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, EthTxIpgIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccTxFcsMsbIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccSgmiiTxIpg31ByteIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Eth100MFxLfRfSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccHdlcTransmissionBitOrderPerChannelIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cPrbsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, SemIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, OcnSeparatedSectionAndLineDccIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, EthPortFlowControlWaterMarkIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockEthSquelchingSelectOnCoreOnlyIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cTxG1OverrideIsNotUsed);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cPrbsInvertionIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DcrAcrTxShapperSupportedVersion);
        mMethodOverride(m_Tha60290022DeviceOverride, ClockExtractorMonitorPpmIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, TohSonetSdhModeIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, CesopAutoRxMBitConfigurableIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, SeparateLbitAndLopsPktReplacementSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateEthCounter32bitIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, Eth100FxSerdesClock31Dot25MhzIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, DccEthMaxLengthConfigurationIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, MapDemapNumLoSlices);
        mMethodOverride(m_Tha60290022DeviceOverride, HasStandardClearTime);
        mMethodOverride(m_Tha60290022DeviceOverride, IsNewBerInterrupt);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplatSerdesEthHasK30_7Feature);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplatSerdesEthStartVersionSupportTxAlarmForcing);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplatSerdesEthExcessiveErrorRatioIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, EthHasSgmiiDiagnosticLogic);
        mMethodOverride(m_Tha60290022DeviceOverride, SgmiiSohEthUseNewMac);
        mMethodOverride(m_Tha60290022DeviceOverride, NumberOfMapLoPwPerSlice);
        mMethodOverride(m_Tha60290022DeviceOverride, NewTuVcLoopbackIsReady);
        mMethodOverride(m_Tha60290022DeviceOverride, Sts192cIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, PdaIsNewOc192cOffset);
        mMethodOverride(m_Tha60290022DeviceOverride, PdaNumJitterBufferBlocksIncreased);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhStartVersionSupportPrm);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhStartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhStartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_Tha60290022DeviceOverride, PdhRxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateSerdesPmaFarEndLoopbackSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, KByteSerdesIsControllable);
        mMethodOverride(m_Tha60290022DeviceOverride, SgmiiSerdesShouldUseMdioLocalLoopback);
        mMethodOverride(m_Tha60290022DeviceOverride, PohHasCpujnreqen);
        mMethodOverride(m_Tha60290022DeviceOverride, PohStartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_Tha60290022DeviceOverride, PrbsBertOptimizeIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, PwDccKbyteInterruptIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateFramerLocalLoopbackSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, SurFailureHoldOnTimerConfigurable);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateSerdesStm1SamplingFromStm4);
        mMethodOverride(m_Tha60290022DeviceOverride, DccHdlcHasPacketCounterDuringFifoFull);
        mMethodOverride(m_Tha60290022DeviceOverride, FacePlateSerdesDataMaskOnResetConfigurable);
        mMethodOverride(m_Tha60290022DeviceOverride, ModuleBerMeasureTimeEngineIsSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, PtpSupported);
        mMethodOverride(m_Tha60290022DeviceOverride, HasApsWtr3SecondResolution);
        mMethodOverride(m_Tha60290022DeviceOverride, HasSoftResetForSem);
        mMethodOverride(m_Tha60290022DeviceOverride, FaceplateSerdesHasNewParams);
        }

    mMethodsSet(device, &m_Tha60290022DeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device this = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011DeviceMethods = mMethodsGet(this);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, m_Tha60210011DeviceMethods, sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, NumUsedOc48Slice);
        }

    mMethodsSet(this, &m_Tha60210011DeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, DefaultCounterModule);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    AtDevice object = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        }

    mMethodsSet(object, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60290022Device(self);
    OverrideTha60210011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022Device);
    }

AtDevice Tha60291022DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60291022DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60291022DeviceObjectInit(newDevice, driver, productCode);
    }

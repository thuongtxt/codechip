/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60291022ModuleCdrInternal.h
 * 
 * Created Date: Jul 23, 2018
 *
 * Description : 60290022 CDR internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULECDRINTERNAL_H_
#define _THA60291022MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/cdr/Tha60290022ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291022ModuleCdr
    {
    tTha60290022ModuleCdrV3 super;
    }tTha60291022ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MODULECDRINTERNAL_H_ */


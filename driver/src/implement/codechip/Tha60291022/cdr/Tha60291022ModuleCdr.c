/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60291022ModuleCdr.c
 *
 * Created Date: Jul 23, 2018
 *
 * Description : 60291022 CDR implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tTha60210011ModuleCdrMethods m_Tha60210011ModuleCdrOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumLoSlices(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 MaxNumCdrRams(AtModule self)
    {
    return Tha60210011ModuleCdrMaxNumLoSlices((ThaModuleCdr)self) * 3;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (AtInternalRamLocalIdGet(ram) < MaxNumCdrRams(self))
        return cAtFalse;

    return cAtTrue;
    }

static void OverrideTha60210011ModuleCdr(AtModule self)
    {
    Tha60210011ModuleCdr module = (Tha60210011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleCdrOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleCdrOverride));

        mMethodOverride(m_Tha60210011ModuleCdrOverride, MaxNumLoSlices);
        }

    mMethodsSet(module, &m_Tha60210011ModuleCdrOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60210011ModuleCdr(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleCdrV3ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

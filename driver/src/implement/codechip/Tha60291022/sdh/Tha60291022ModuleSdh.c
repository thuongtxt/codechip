/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60291022ModuleSdh.c
 *
 * Created Date: Aug 7, 2018
 *
 * Description : Tha60291022 module SDH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "Tha60291022ModuleSdhInternal.h"
#include "Tha60291022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tTha60290021ModuleSdhMethods m_Tha60290021ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods          = NULL;
static const tTha60290021ModuleSdhMethods *m_Tha60290021ModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumUseableMateLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 NumUseableTerminatedLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static eBool IsFaceplateLineSupportStm64(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return (lineId == 0) ? cAtTrue : cAtFalse;
    }

static eBool IsLineSupportStm16(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate)
    {
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)line);

    if (Tha60290021ModuleSdhLineIsFaceplate(self, lineId))
        return ((lineId % 4) == 0) ? cAtTrue : cAtFalse;

    return m_AtModuleSdhMethods->LineRateIsSupported(self, line, rate);
    }

static eBool LineRateIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm16)
        return IsLineSupportStm16(self, line, rate);

    if (rate == cAtSdhLineRateStm64)
        return IsFaceplateLineSupportStm64(self, (uint8)AtChannelIdGet((AtChannel)line));

    return m_AtModuleSdhMethods->LineRateIsSupported(self, line, rate);
    }

static eBool IsFaceplateLineIdsUnused(AtModuleSdh self)
    {
    /* Configure line rate of line 0 to OC–192/STM–64 will make line 1 to 15 be unused */
    if (Tha60290021ModuleSdhFaceplateLineRateGet(self, 0) == cAtSdhLineRateStm64)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsFaceplateLineQuartIdsUnused(AtModuleSdh self, uint8 lineId)
    {
    if ((lineId % 4) == 0)
        return cAtFalse;

    /* Configure line rate of line 0,4,8,13 to OC–48/STM–16 will make remaining 3 ascending lines be unused */
    if (Tha60290021ModuleSdhFaceplateLineRateGet(self, (uint8)((lineId / 4) * 4)) == cAtSdhLineRateStm16)
        return cAtTrue;

    return cAtFalse;
    }

static eBool FacePlateLineIsUsed(Tha60290021ModuleSdh self, uint8 lineId)
    {
    if (IsFaceplateLineSupportStm64((AtModuleSdh)self, lineId))
        return cAtTrue;

    if (IsFaceplateLineIdsUnused((AtModuleSdh)self))
        return cAtFalse;

    if (IsFaceplateLineQuartIdsUnused((AtModuleSdh)self, lineId))
        return cAtFalse;

    return cAtTrue;
    }

static ThaModuleOcn ModuleOcn(Tha60290021ModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet HelperFaceplateLineRateWillChange(Tha60290021ModuleSdh self, uint8 lineId, uint8 numAffectFaceplateLines)
    {
    ThaModuleOcn ocnModule = ModuleOcn(self);
    eAtRet ret = cAtOk;
    uint8 line_i;

    for (line_i = 1; line_i < numAffectFaceplateLines; line_i++)
        {
        uint8 unusedLineId = (uint8)(lineId + line_i);
        ret |= Tha60290021ModuleOcnFaceplateLineEnable(ocnModule, unusedLineId, cAtFalse);
        ret |= AtModuleSdhLineUnuse((AtModuleSdh)self, unusedLineId);

        /* Invalidate unused lines */
        self->rate[unusedLineId] = cAtSdhLineRateInvalid;
        }

    return ret;
    }

static eAtRet FaceplateLineRateWillChangeToStm64(Tha60290021ModuleSdh self, uint8 lineId)
    {
    uint8 numFaceplateLines = Tha60290021ModuleSdhNumUseableFaceplateLines((AtModuleSdh)self);
    eAtRet ret;

    /* If STM-64 is applied, all of other ports must be disabled */
    ret = HelperFaceplateLineRateWillChange(self, lineId, numFaceplateLines);
    if (ret == cAtOk)
        return cAtOk;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                "Set line rate to STM-64 fail with ret = %s\r\n",
                AtRet2String(ret));

    return ret;
    }

static eAtRet FaceplateLineRateWillChangeToStm16(Tha60290021ModuleSdh self, uint8 lineId)
    {
    eAtRet ret;

    /* If STM-16 is applied, the next three lines must be disabled */
    ret = HelperFaceplateLineRateWillChange(self, lineId, 4);
    if (ret == cAtOk)
        return cAtOk;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                "Set line rate to STM-16 fail with ret = %s\r\n",
                AtRet2String(ret));

    return ret;
    }

static eAtRet FaceplateLineRateDidChangeFromStm64(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    eAtRet ret = cAtOk;
    uint32 line_i;
    AtUnused(oldRate);

    /* When changing from STM-64 back to STM-16, it would be better to have
     * other ports enabled with STM-16 rate */
    for (line_i = 4; line_i < 16; line_i += 4)
        {
        AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(line_i + lineId));
        ret |= AtSdhChannelModeSet((AtSdhChannel)line, AtModuleSdhLineDefaultMode((AtModuleSdh)self, line));
        ret |= AtSdhLineRateSet(line, cAtSdhLineRateStm16);
        }

    /* When changing to STM-4/STM-1/STM-0 from STM-64, it is better to have all
     * of ports in the first group to have the same rate */
    if (newRate != cAtSdhLineRateStm16)
        {
        for (line_i = 1; line_i < 4; line_i++)
            {
            AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(line_i + lineId));
            ret |= AtSdhChannelModeSet((AtSdhChannel)line, AtModuleSdhLineDefaultMode((AtModuleSdh)self, line));
            ret |= AtSdhLineRateSet(line, newRate);
            }
        }

    if (ret != cAtOk)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Set line rate to %d fail with ret = %s\r\n", newRate,
                    AtRet2String(ret));
        }

    return ret;
    }

static eAtRet FaceplateLineRateDidChangeFromStm16(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    eAtRet ret = cAtOk;
    uint32 line_i;

    /* To handle initializing time when STM-16 is set to default */
    if ((newRate == cAtSdhLineRateStm64) || (newRate == oldRate))
        return cAtOk;

    /* When changing to STM-4/STM-1/STM-0 from STM-16, it is better to have the
     * next port have the same rate */
    for (line_i = 1; line_i < 4; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(line_i + lineId));
        ret |= AtSdhChannelModeSet((AtSdhChannel)line, AtModuleSdhLineDefaultMode((AtModuleSdh)self, line));
        ret |= AtSdhLineRateSet(line, newRate);
        }

    if (ret != cAtOk)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Set line rate to %d fail with ret = %s\r\n", newRate,
                    AtRet2String(ret));
        }

    return ret;
    }

static eAtRet FaceplateLineRateWillChange(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    AtUnused(oldRate);

    if (newRate == cAtSdhLineRateStm64)
        return FaceplateLineRateWillChangeToStm64(self, lineId);

    if (newRate == cAtSdhLineRateStm16)
        return FaceplateLineRateWillChangeToStm16(self, lineId);

    return cAtOk;
    }

static eAtRet FaceplateLineRateDidChange(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    if (oldRate == cAtSdhLineRateStm64)
        return FaceplateLineRateDidChangeFromStm64(self, lineId, oldRate, newRate);

    if (oldRate == cAtSdhLineRateStm16)
        return FaceplateLineRateDidChangeFromStm16(self, lineId, oldRate, newRate);

    return cAtOk;
    }

static AtSdhLine FaceplateLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha60291022SdhFacePlateLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhAu FacePlateLineAuCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60291022SdhFacePlateLineAuNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhAug FacePlateLineAugCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60291022SdhFacePlateLineAugNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhLine HidingXcTerminatedChannelToFaceplateLineGet(Tha60290021ModuleSdh self, AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint32 localLineId = ThaModuleSdhLineIdLocalId((ThaModuleSdh)sdhModule, AtSdhChannelLineGet(channel));
    uint32 flatSts1;
    uint32 faceplateLineId;
    AtSdhLine line;

    /* Line 0 */
    line = AtModuleSdhLineGet(sdhModule, 0);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm64)
        return line;

    flatSts1 = localLineId * 48U + AtSdhChannelSts1Get(channel);

    /* First line ID of each 4 lines */
    faceplateLineId = (flatSts1 / 48) * 4;
    line = AtModuleSdhLineGet(sdhModule, (uint8)faceplateLineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm16)
        return line;

    faceplateLineId = ((flatSts1 / 48) * 4 + (flatSts1 / 12));
    line = AtModuleSdhLineGet(sdhModule, (uint8)faceplateLineId);
    return line;
    }

static void OverrideTha60290021ModuleSdh(AtModuleSdh self)
    {
    Tha60290021ModuleSdh sdh = (Tha60290021ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleSdhMethods = mMethodsGet(sdh);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleSdhOverride, mMethodsGet(sdh), sizeof(m_Tha60290021ModuleSdhOverride));

        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableMateLines);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableTerminatedLines);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FacePlateLineIsUsed);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FaceplateLineRateWillChange);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FaceplateLineRateDidChange);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FaceplateLineCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FacePlateLineAuCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FacePlateLineAugCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, HidingXcTerminatedChannelToFaceplateLineGet);
        }

    mMethodsSet(sdh, &m_Tha60290021ModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, LineRateIsSupported);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideTha60290021ModuleSdh(self);
    OverrideAtModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModuleSdh);
    }

AtModuleSdh Tha60291022ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleSdhV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60291022ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291022ModuleSdhObjectInit(newModule, device);
    }

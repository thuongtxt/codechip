/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60291022SdhFacePlateLineAug.c
 *
 * Created Date: Aug 22, 2018
 *
 * Description : Faceplate AUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/sdh/Tha60290022SdhFacePlateAugInternal.h"
#include "Tha60291022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60291022SdhFacePlateAug
    {
    tTha60290022SdhFacePlateAug super;
    }tTha60291022SdhFacePlateAug;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021SdhLineSideAugMethods    m_Tha60290021SdhLineSideAugOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtSdhChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static uint8 LocalIdGet(AtSdhLine line)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule((AtSdhChannel)line), (uint8)AtChannelIdGet((AtChannel)line));
    }

static uint8 StartSts1InTerminatedLineGet(Tha60290021SdhLineSideAug self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet((AtSdhChannel)self);
    uint8 rate = AtSdhLineRateGet(line);

    if ((rate == cAtSdhLineRateStm0) ||
        (rate == cAtSdhLineRateStm1) ||
        (rate == cAtSdhLineRateStm4))
        return (uint8)((LocalIdGet(line) % 4) * 12);

    return 0;
    }

static void OverrideTha60290021SdhLineSideAug(AtSdhAug self)
    {
    Tha60290021SdhLineSideAug channel = (Tha60290021SdhLineSideAug)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SdhLineSideAugOverride, mMethodsGet(channel), sizeof(m_Tha60290021SdhLineSideAugOverride));

        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, StartSts1InTerminatedLineGet);
        }

    mMethodsSet(channel, &m_Tha60290021SdhLineSideAugOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideTha60290021SdhLineSideAug(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022SdhFacePlateAug);
    }

static AtSdhAug ObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SdhFacePlateAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60291022SdhFacePlateLineAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug newAug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAug == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newAug, channelId, channelType, module);
    }

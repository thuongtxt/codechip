/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60291022SdhFacePlateLineAu.c
 *
 * Created Date: Aug 22, 2018
 *
 * Description : FacePlate Line AU path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../Tha60290022/sdh/Tha60290022SdhFacePlateLineAuInternal.h"
#include "Tha60291022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60291022SdhFacePlateLineAu
    {
    tTha60290022SdhFacePlateLineAu super;
    }tTha60291022SdhFacePlateLineAu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods       m_AtSdhChannelOverride;

/* Super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtSdhChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static uint8 LocalIdGet(AtSdhLine line)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule((AtSdhChannel)line), (uint8)AtChannelIdGet((AtChannel)line));
    }

static AtSdhChannel HideChannelGet(AtSdhChannel self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(self);

    /* STM-0/AU-3 */
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        {
        AtSdhLine hideLine = AtSdhLineHideLineGet(line, 0);
        uint8 aug1Id = (uint8)((LocalIdGet(line) % 4) * 4); /* AUG-1 #0, 4, 8 and 12 of STM16 */

        return (AtSdhChannel)AtSdhLineAu3Get(hideLine, aug1Id, 0);
        }

    return m_AtSdhChannelMethods->HideChannelGet(self);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, HideChannelGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022SdhFacePlateLineAu);
    }

static AtSdhAu ObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SdhFacePlateLineAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha60291022SdhFacePlateLineAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

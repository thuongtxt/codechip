/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60291022ModuleSdh.h
 * 
 * Created Date: Aug 21, 2018
 *
 * Description : SDH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULESDH_H_
#define _THA60291022MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Line */
AtSdhLine Tha60291022SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module);

/* AUG */
AtSdhAug Tha60291022SdhFacePlateLineAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* AU */
AtSdhAu Tha60291022SdhFacePlateLineAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);


#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MODULESDH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60291022SdhFacePlateLine.c
 *
 * Created Date: Aug 22, 2018
 *
 * Description : Faceplate line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290022/sdh/Tha60290022SdhFacePlateLineInternal.h"
#include "../../Tha60290022/sdh/Tha60290022ModuleSdh.h"
#include "Tha60291022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60291022SdhFacePlateLine
    {
    tTha60290022SdhFacePlateLine super;
    }tTha60291022SdhFacePlateLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineMethods        m_AtSdhLineOverride;

/* Save super implementation */
static const tAtSdhLineMethods      *m_AtSdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtChannel self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSdh);
    }

static uint8 LocalIdGet(AtSdhLine self)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    AtModuleSdh sdhModule = SdhModule((AtChannel)self);
    AtSdhLine terminatedLine = Tha60290021ModuleSdhTerminatedLineGet(sdhModule, 0);

    /* If faceplate line is not STM64, the terminated line must be STM16. */
    if (AtSdhLineRateGet(self) != cAtSdhLineRateStm64)
        {
        if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
            Tha60290022SdhTerminatedLineRateSet(terminatedLine, cAtSdhLineRateStm16);

        /* Four consecutive faceplate lines are mapped to one terminated line STM16. */
        return Tha60290021ModuleSdhTerminatedLineGet(sdhModule, (uint8)(LocalIdGet(self) / 4));
        }

    /* One faceplate line STM64 is mapped to one terminated line STM64. */
    if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
        return terminatedLine;

    /* One faceplate line STM64 is mapped to four terminated lines STM16. */
    return Tha60290021ModuleSdhTerminatedLineGet(sdhModule, (uint8)(sts1 / 48));
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, HideLineGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022SdhFacePlateLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SdhFacePlateLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60291022SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, lineId, module);
    }

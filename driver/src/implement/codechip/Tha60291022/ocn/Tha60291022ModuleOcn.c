/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60291022ModuleOcn.c
 *
 * Created Date: Jul 23, 2018
 *
 * Description : 60291022 OCN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022ModuleOcnInternal.h"
#include "../../Tha60290022/ocn/Tha60290022ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;

/* Save super implementation */
static const tThaModuleOcnMethods         *m_ThaModuleOcnMethods         = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet MateDefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret  = Tha60290022ModuleOcnHelperMateSts192cEnable((Tha60290022ModuleOcn)self, 0, cAtFalse);  /* Mate #0-3 */
    ret  = Tha60290022ModuleOcnHelperMateSts192cEnable((Tha60290022ModuleOcn)self, 4, cAtFalse);  /* Mate #4-7 */

    return ret;
    }

static eAtRet FacePlateDefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret  = Tha60290022ModuleOcnFaceplateSts192cEnable((Tha60290022ModuleOcn)self, 0, cAtFalse);
    ret  = Tha60290022ModuleOcnFaceplateSts192cEnable((Tha60290022ModuleOcn)self, 8, cAtFalse);

    return ret;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    ret |= MateDefaultSet(self);
    ret |= FacePlateDefaultSet(self);

    return ret;
    }

static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn module = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthIsSupported);
        }

    mMethodsSet(module, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModuleOcn);
    }

AtModule Tha60291022ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60291022ModuleOcnObjectInit(newModule, device);
    }

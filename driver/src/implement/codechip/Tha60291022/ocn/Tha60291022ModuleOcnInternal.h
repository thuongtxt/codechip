/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60291022ModuleOcnInternal.h
 * 
 * Created Date: Jul 23, 2018
 *
 * Description : 60291022 OCN internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULEOCNINTERNAL_H_
#define _THA60291022MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/ocn/Tha60290022ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291022ModuleOcn
    {
    tTha60290022ModuleOcn super;
    }tTha60291022ModuleOcn;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60291022ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MODULEOCNINTERNAL_H_ */


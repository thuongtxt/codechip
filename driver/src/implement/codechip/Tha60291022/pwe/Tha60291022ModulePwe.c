/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60291022ModulePwe.c
 *
 * Created Date: Jul 25, 2018
 *
 * Description : 60291022 module PWE implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"
#include "../ram/Tha60291022InternalRam.h"
#include "Tha60291022ModulePweInternal.h"
#include "Tha60291022ModulePwePlaReg.h"
#include "Tha60291022ModulePwePlaDebugReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPweBaseAddress(self) ThaModulePweBaseAddress((ThaModulePwe)self)
#define mPlaBaseAddress(self) ThaModulePwePlaBaseAddress((ThaModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods                     m_AtModuleOverride;
static tThaModulePweMethods                 m_ThaModulePweOverride;
static tTha60210011ModulePweMethods         m_Tha60210011ModulePweOverride;
static tTha60290022ModulePweMethods         m_Tha60290022ModulePweOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods = NULL;
static const tThaModulePweMethods         *m_ThaModulePweMethods = NULL;
static const tTha60290022ModulePweMethods *m_Tha60290022ModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 IdleControlAddress(ThaModulePwe self, ThaPwAdapter adapter)
    {
    uint32 moduleBase = mPlaBaseAddress(self);
    uint32 tdmOffset = Tha60290022ModulePweHwPwTdmDefaultOffset((Tha60290022ModulePwe)self, (AtPw)adapter);
    uint32 address = cAf6Reg_pla_cas_idle_pat_ctrl_Base +  moduleBase + tdmOffset;
    return address;
    }

static eAtRet PwIdleCodeSet(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 payloadIdleCode)
    {
    uint32 address = IdleControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_pla_cas_idle_pat_ctrl_DatIdlePattern_, payloadIdleCode);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static eAtRet PwCesopCasIdleCode1Set(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 abcd1)
    {
    uint32 address = IdleControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_pla_cas_idle_pat_ctrl_CasIdlePattern_, abcd1);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static uint8 PwCesopCasIdleCode1Get(ThaModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 address = IdleControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);
    uint32 abcd1 = mRegField(regVal, cAf6_pla_cas_idle_pat_ctrl_CasIdlePattern_);

    return (uint8)abcd1;
    }

static eAtRet PwIdleCodeInsertionEnable(ThaModulePwe self, ThaPwAdapter pwAdapter, eBool autoIdleEnable)
    {
    uint32 address = IdleControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);
    uint32 hwVal = (uint32)(autoIdleEnable ? 1 : 0);

    mRegFieldSet(regVal, cAf6_pla_cas_idle_pat_ctrl_CasIdlePatEn_, hwVal);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static eAtRet PwNxDs0Set(ThaModulePwe self, AtPw pw, AtPdhNxDS0 nxDs0)
    {
    /* Temporarily ignore this method until this product supports CAS */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(nxDs0);
    return cAtOk;
    }

static uint32 HwOc192OffsetFactor(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 65536;
    }

static uint32 HwOc96OffsetFactor(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 65536;
    }

static uint32 HwOc48InOc96OffsetFactor(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 16384;
    }

static uint32 HwNumPwInOc48(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 2688;
    }

static uint32 LoPayloadCtrl(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_pld_ctrl_Base;
    }

static uint32 LoAddRemoveProtocolReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_add_rmv_pw_ctrl_Base;
    }

static uint32 PlaPwHeaderControl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_pw_hdr_ctrl_Base;
    }

static uint32 PlaOc192cPayloadControl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_192c_pld_ctrl_Base;
    }

static uint32 PlaOc192cAddRemovePwControl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_oc192c_add_rmv_pw_ctrl_Base;
    }

static uint32 PlaOutClk311Control(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_clk311_ctrl_Base;
    }

static uint32 PlaOc48Clk311Sticky(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_oc48_clk311_stk_Base;
    }

static uint32 PlaOc192cClk311Sticky(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_oc192c_clk311_stk_Base;
    }

static uint32 HwNumOc96Slices(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 2;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *ramName = AtInternalRamName(ram);
    static char reservedRamOc96Slice2[] = "OC96#2";
    static char reservedRamOc96Slice3[] = "OC96#3";

    if ((AtStrstr(ramName, reservedRamOc96Slice2) != NULL) ||
        (AtStrstr(ramName, reservedRamOc96Slice3) != NULL))
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    Tha60210011ModulePwe pweModule = (Tha60210011ModulePwe)self;

    if (localRamId < mMethodsGet(pweModule)->StartPweRamLocalId(pweModule))
        return Tha60291022InternalRamPlaNew(self, ramId, localRamId);

    return m_AtModuleMethods->InternalRamCreate(self, ramId, localRamId);
    }

static void OverrideTha60290022ModulePwe(AtModule self)
    {
    Tha60290022ModulePwe pweModule = (Tha60290022ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModulePweOverride, m_Tha60290022ModulePweMethods, sizeof(m_Tha60290022ModulePweOverride));

        mMethodOverride(m_Tha60290022ModulePweOverride, HwOc192OffsetFactor);
        mMethodOverride(m_Tha60290022ModulePweOverride, HwOc96OffsetFactor);
        mMethodOverride(m_Tha60290022ModulePweOverride, HwOc48InOc96OffsetFactor);
        mMethodOverride(m_Tha60290022ModulePweOverride, HwNumPwInOc48);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaPwHeaderControl);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOc192cPayloadControl);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOc192cAddRemovePwControl);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOutClk311Control);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOc48Clk311Sticky);
        mMethodOverride(m_Tha60290022ModulePweOverride, PlaOc192cClk311Sticky);
        mMethodOverride(m_Tha60290022ModulePweOverride, HwNumOc96Slices);
        }

    mMethodsSet(pweModule, &m_Tha60290022ModulePweOverride);
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe pweModule = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, mMethodsGet(pweModule), sizeof(m_Tha60210011ModulePweOverride));

        mMethodOverride(m_Tha60210011ModulePweOverride, LoPayloadCtrl);
        mMethodOverride(m_Tha60210011ModulePweOverride, LoAddRemoveProtocolReg);
        }

    mMethodsSet(pweModule, &m_Tha60210011ModulePweOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwIdleCodeSet);
        mMethodOverride(m_ThaModulePweOverride, PwCesopCasIdleCode1Set);
        mMethodOverride(m_ThaModulePweOverride, PwCesopCasIdleCode1Get);
        mMethodOverride(m_ThaModulePweOverride, PwNxDs0Set);
        mMethodOverride(m_ThaModulePweOverride, PwIdleCodeInsertionEnable);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    OverrideTha60210011ModulePwe(self);
    OverrideTha60290022ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

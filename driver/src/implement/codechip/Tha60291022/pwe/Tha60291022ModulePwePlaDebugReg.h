/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PLA_DEBUG_H_
#define _AF6_REG_AF6CNC0022_RD_PLA_DEBUG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler clk311 Control
Reg Addr   : 0x8_0000 - 0xB4000
Reg Formula: 0x8_0000 + $Oc96Slice*65536 + $Oc48Slice*16384
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
Reg Desc   : 
This register is used to control pwid for debug

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_clk311_ctrl_Base                                                               0x80000

/*--------------------------------------
BitField Name: PWID_for_debug
BitField Type: RW
BitField Desc:
BitField Bits: [22:12]
--------------------------------------*/
#define cAf6_pla_out_clk311_ctrl_PWID_for_debug_Mask                                                 cBit22_12
#define cAf6_pla_out_clk311_ctrl_PWID_for_debug_Shift                                                       12


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler OC48 clk311 Sticky
Reg Addr   : 0x8_0001 - 0xB_4001
Reg Formula: 0x8_0001 + $Oc96Slice*65536 + $Oc48Slice*16384
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
Reg Desc   : 
This register is used to used to sticky some alarms for debug per OC-48 @clk311.02 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_oc48_clk311_stk_Base                                                               0x80001
#define cAf6Reg_pla_oc48_clk311_stk_WidthVal                                                                32

/*--------------------------------------
BitField Name: Pla311OC48_ConvErr
BitField Type: W2C
BitField Desc: OC48 conver clock error
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_ConvErr_Mask                                                cBit11
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_ConvErr_Shift                                                   11

/*--------------------------------------
BitField Name: Pla311OC48_InPosPW
BitField Type: W2C
BitField Desc: OC48 input CEP Pos based PW
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPosPW_Mask                                                 cBit9
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPosPW_Shift                                                    9

/*--------------------------------------
BitField Name: Pla311OC48_InNegPW
BitField Type: W2C
BitField Desc: OC48 input CEP Neg based PW
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNegPW_Mask                                                 cBit8
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNegPW_Shift                                                    8

/*--------------------------------------
BitField Name: Pla311OC48_InValodPW
BitField Type: W2C
BitField Desc: OC48 input Valid based PW
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValodPW_Mask                                               cBit7
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValodPW_Shift                                                  7

/*--------------------------------------
BitField Name: Pla311OC48_InValid
BitField Type: W2C
BitField Desc: OC48 input valid
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValid_Mask                                                 cBit6
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InValid_Shift                                                    6

/*--------------------------------------
BitField Name: Pla311OC48_InAisPW
BitField Type: W2C
BitField Desc: OC48 input AIS based PW
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAisPW_Mask                                                 cBit5
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAisPW_Shift                                                    5

/*--------------------------------------
BitField Name: Pla311OC48_InAIS
BitField Type: W2C
BitField Desc: OC48 input AIS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAIS_Mask                                                   cBit4
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InAIS_Shift                                                      4

/*--------------------------------------
BitField Name: Pla311OC48_InJ1PosPW
BitField Type: W2C
BitField Desc: OC48 input CEP J1 Posiion based PW
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1PosPW_Mask                                               cBit3
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1PosPW_Shift                                                  3

/*--------------------------------------
BitField Name: Pla311OC48_InPos
BitField Type: W2C
BitField Desc: OC48 input CEP Pos Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPos_Mask                                                   cBit2
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InPos_Shift                                                      2

/*--------------------------------------
BitField Name: Pla311OC48_InNeg
BitField Type: W2C
BitField Desc: OC48 input CEP Neg Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNeg_Mask                                                   cBit1
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InNeg_Shift                                                      1

/*--------------------------------------
BitField Name: Pla311OC48_InJ1Pos
BitField Type: W2C
BitField Desc: OC48 input CEP J1 Position
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1Pos_Mask                                                 cBit0
#define cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1Pos_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Buffer Sticky 1
Reg Addr   : 0x4_2000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to used to sticky some alarms for debug#1

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_buf_stk1_Base                                                                      0x42000

/*--------------------------------------
BitField Name: PlaGetAllBlkErr
BitField Type: W2C
BitField Desc: Get All Block Type Error
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaGetAllBlkErr_Mask                                                          cBit30
#define cAf6_pla_buf_stk1_PlaGetAllBlkErr_Shift                                                             30

/*--------------------------------------
BitField Name: PlaGetHiBlkErr
BitField Type: W2C
BitField Desc: Get Hi-Block Error
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaGetHiBlkErr_Mask                                                           cBit29
#define cAf6_pla_buf_stk1_PlaGetHiBlkErr_Shift                                                              29

/*--------------------------------------
BitField Name: PlaGetLoBlkErr
BitField Type: W2C
BitField Desc: Get Lo-Block Error
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaGetLoBlkErr_Mask                                                           cBit28
#define cAf6_pla_buf_stk1_PlaGetLoBlkErr_Shift                                                              28

/*--------------------------------------
BitField Name: PlaAllCaEmpt
BitField Type: W2C
BitField Desc: All Cache Empty Error
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaAllCaEmpt_Mask                                                             cBit27
#define cAf6_pla_buf_stk1_PlaAllCaEmpt_Shift                                                                27

/*--------------------------------------
BitField Name: PlaGetCa512Err
BitField Type: W2C
BitField Desc: Get cache-512 Error
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaGetCa512Err_Mask                                                           cBit26
#define cAf6_pla_buf_stk1_PlaGetCa512Err_Shift                                                              26

/*--------------------------------------
BitField Name: PlaGetCa256Err
BitField Type: W2C
BitField Desc: Get cache-256 Error
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaGetCa256Err_Mask                                                           cBit25
#define cAf6_pla_buf_stk1_PlaGetCa256Err_Shift                                                              25

/*--------------------------------------
BitField Name: PlaGetCa128Err
BitField Type: W2C
BitField Desc: Get cache-128 Error
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaGetCa128Err_Mask                                                           cBit24
#define cAf6_pla_buf_stk1_PlaGetCa128Err_Shift                                                              24

/*--------------------------------------
BitField Name: PlaWrHiBlkNearFul
BitField Type: W2C
BitField Desc: Monitor high block near full
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrHiBlkNearFul_Mask                                                        cBit23
#define cAf6_pla_buf_stk1_PlaWrHiBlkNearFul_Shift                                                           23

/*--------------------------------------
BitField Name: PlaWrLoBlkNearFul
BitField Type: W2C
BitField Desc: Monitor low block near full
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrLoBlkNearFul_Mask                                                        cBit22
#define cAf6_pla_buf_stk1_PlaWrLoBlkNearFul_Shift                                                           22

/*--------------------------------------
BitField Name: PlaWrCaNearFul
BitField Type: W2C
BitField Desc: Monitor total cache near full
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrCaNearFul_Mask                                                           cBit21
#define cAf6_pla_buf_stk1_PlaWrCaNearFul_Shift                                                              21

/*--------------------------------------
BitField Name: PlaWrBlkErr
BitField Type: W2C
BitField Desc: block fail due to near empty cache/block
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrBlkErr_Mask                                                              cBit20
#define cAf6_pla_buf_stk1_PlaWrBlkErr_Shift                                                                 20

/*--------------------------------------
BitField Name: PlaWrDDRFifoFul
BitField Type: W2C
BitField Desc: Write DDR Fifo Full (due to 1st or 2nd buffer)
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrDDRFifoFul_Mask                                                          cBit19
#define cAf6_pla_buf_stk1_PlaWrDDRFifoFul_Shift                                                             19

/*--------------------------------------
BitField Name: PlaWrDDRLenErr
BitField Type: W2C
BitField Desc: Write DDR Check Length Error
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrDDRLenErr_Mask                                                           cBit18
#define cAf6_pla_buf_stk1_PlaWrDDRLenErr_Shift                                                              18

/*--------------------------------------
BitField Name: PlaHiBlkEmpt
BitField Type: W2C
BitField Desc: High Block Empty Error
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaHiBlkEmpt_Mask                                                             cBit17
#define cAf6_pla_buf_stk1_PlaHiBlkEmpt_Shift                                                                17

/*--------------------------------------
BitField Name: PlaLoBlkEmpt
BitField Type: W2C
BitField Desc: Low Block Empty Error
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaLoBlkEmpt_Mask                                                             cBit16
#define cAf6_pla_buf_stk1_PlaLoBlkEmpt_Shift                                                                16

/*--------------------------------------
BitField Name: PlaWrDDRReorRdFifoFul
BitField Type: W2C
BitField Desc: Write DDR Reorder Mux Read Full
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrDDRReorRdFifoFul_Mask                                                    cBit15
#define cAf6_pla_buf_stk1_PlaWrDDRReorRdFifoFul_Shift                                                       15

/*--------------------------------------
BitField Name: PlaWrDDRReorWrFifoFul
BitField Type: W2C
BitField Desc: Write DDR Reorder Mux Write Full
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaWrDDRReorWrFifoFul_Mask                                                    cBit14
#define cAf6_pla_buf_stk1_PlaWrDDRReorWrFifoFul_Shift                                                       14

/*--------------------------------------
BitField Name: PlaHiBlkSame
BitField Type: W2C
BitField Desc: High Block Same Error
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaHiBlkSame_Mask                                                             cBit13
#define cAf6_pla_buf_stk1_PlaHiBlkSame_Shift                                                                13

/*--------------------------------------
BitField Name: PlaLoBlkSame
BitField Type: W2C
BitField Desc: Low Block Same Error
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaLoBlkSame_Mask                                                             cBit12
#define cAf6_pla_buf_stk1_PlaLoBlkSame_Shift                                                                12

/*--------------------------------------
BitField Name: PlaCa512Empt
BitField Type: W2C
BitField Desc: Cache 512 Empty Error
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaCa512Empt_Mask                                                             cBit10
#define cAf6_pla_buf_stk1_PlaCa512Empt_Shift                                                                10

/*--------------------------------------
BitField Name: PlaCa256Empt
BitField Type: W2C
BitField Desc: Cache 256 Empty Error
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaCa256Empt_Mask                                                              cBit9
#define cAf6_pla_buf_stk1_PlaCa256Empt_Shift                                                                 9

/*--------------------------------------
BitField Name: PlaCa128Empt
BitField Type: W2C
BitField Desc: Cache 128 Empty Error
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaCa128Empt_Mask                                                              cBit8
#define cAf6_pla_buf_stk1_PlaCa128Empt_Shift                                                                 8

/*--------------------------------------
BitField Name: PlaCa512Same
BitField Type: W2C
BitField Desc: Cache 512 Same Error
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaCa512Same_Mask                                                              cBit6
#define cAf6_pla_buf_stk1_PlaCa512Same_Shift                                                                 6

/*--------------------------------------
BitField Name: PlaCa256Same
BitField Type: W2C
BitField Desc: Cache 256 Same Error
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaCa256Same_Mask                                                              cBit5
#define cAf6_pla_buf_stk1_PlaCa256Same_Shift                                                                 5

/*--------------------------------------
BitField Name: PlaCa128Same
BitField Type: W2C
BitField Desc: Cache 128 Same Error
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaCa128Same_Mask                                                              cBit4
#define cAf6_pla_buf_stk1_PlaCa128Same_Shift                                                                 4

/*--------------------------------------
BitField Name: PlaMaxLengthErr
BitField Type: W2C
BitField Desc: Max Length Error
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaMaxLengthErr_Mask                                                           cBit3
#define cAf6_pla_buf_stk1_PlaMaxLengthErr_Shift                                                              3

/*--------------------------------------
BitField Name: PlaReqWrDdrNearFul
BitField Type: W2C
BitField Desc: Request write DDR near full
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaReqWrDdrNearFul_Mask                                                        cBit2
#define cAf6_pla_buf_stk1_PlaReqWrDdrNearFul_Shift                                                           2

/*--------------------------------------
BitField Name: PlaFreeBlockSet
BitField Type: W2C
BitField Desc: Free Block Set
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaFreeBlockSet_Mask                                                           cBit1
#define cAf6_pla_buf_stk1_PlaFreeBlockSet_Shift                                                              1

/*--------------------------------------
BitField Name: PlaFreeCacheSet
BitField Type: W2C
BitField Desc: Free Cache Set
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_buf_stk1_PlaFreeCacheSet_Mask                                                           cBit0
#define cAf6_pla_buf_stk1_PlaFreeCacheSet_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Buffer Sticky 2
Reg Addr   : 0x4_2001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to used to sticky some alarms for debug#2

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_buf_stk2_Base                                                                      0x42001

/*--------------------------------------
BitField Name: PlaWrDDRReorderErr
BitField Type: W2C
BitField Desc: Write DDR Reorder Error
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaWrDDRReorderErr_Mask                                                       cBit31
#define cAf6_pla_buf_stk2_PlaWrDDRReorderErr_Shift                                                          31

/*--------------------------------------
BitField Name: PlaWrDDRBuf1stFifoFul
BitField Type: W2C
BitField Desc: Write DDR Buffer 1st Fifo Full
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaWrDDRBuf1stFifoFul_Mask                                                    cBit30
#define cAf6_pla_buf_stk2_PlaWrDDRBuf1stFifoFul_Shift                                                       30

/*--------------------------------------
BitField Name: PlaWrDDRBuf2ndFifoFul
BitField Type: W2C
BitField Desc: Write DDR Buffer 2nd Fifo Full
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaWrDDRBuf2ndFifoFul_Mask                                                    cBit29
#define cAf6_pla_buf_stk2_PlaWrDDRBuf2ndFifoFul_Shift                                                       29

/*--------------------------------------
BitField Name: PlaWrDDRAckFifoFul
BitField Type: W2C
BitField Desc: Write DDR ACK Fifo Full
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaWrDDRAckFifoFul_Mask                                                       cBit28
#define cAf6_pla_buf_stk2_PlaWrDDRAckFifoFul_Shift                                                          28

/*--------------------------------------
BitField Name: PlaPkBufFFFul
BitField Type: W2C
BitField Desc: Packet Buffer Fifo Full
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaPkBufFFFul_Mask                                                            cBit27
#define cAf6_pla_buf_stk2_PlaPkBufFFFul_Shift                                                               27

/*--------------------------------------
BitField Name: PlaPkBufFFRdy
BitField Type: W2C
BitField Desc: Packet Buffer Fifo Ready
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaPkBufFFRdy_Mask                                                            cBit26
#define cAf6_pla_buf_stk2_PlaPkBufFFRdy_Shift                                                               26

/*--------------------------------------
BitField Name: PlaPkBufFull
BitField Type: W2C
BitField Desc: Packet Buffer Full
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaPkBufFull_Mask                                                             cBit25
#define cAf6_pla_buf_stk2_PlaPkBufFull_Shift                                                                25

/*--------------------------------------
BitField Name: PlaPkBufNotEmpt
BitField Type: W2C
BitField Desc: Packet Buffer Not Empty
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaPkBufNotEmpt_Mask                                                          cBit24
#define cAf6_pla_buf_stk2_PlaPkBufNotEmpt_Shift                                                             24

/*--------------------------------------
BitField Name: PlaRdPortPktDis
BitField Type: W2C
BitField Desc: Read Port Side Packet Disabled
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdPortPktDis_Mask                                                          cBit19
#define cAf6_pla_buf_stk2_PlaRdPortPktDis_Shift                                                             19

/*--------------------------------------
BitField Name: PlaRdPortPktFFErr
BitField Type: W2C
BitField Desc: Read Port Side Packet Fifo Error
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdPortPktFFErr_Mask                                                        cBit18
#define cAf6_pla_buf_stk2_PlaRdPortPktFFErr_Shift                                                           18

/*--------------------------------------
BitField Name: PlaRdPortDatFFErr
BitField Type: W2C
BitField Desc: Read Port Side Data Fifo Error
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdPortDatFFErr_Mask                                                        cBit17
#define cAf6_pla_buf_stk2_PlaRdPortDatFFErr_Shift                                                           17

/*--------------------------------------
BitField Name: PlaRdPortInfFFErr
BitField Type: W2C
BitField Desc: Read Port Side Info Fifo Error
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdPortInfFFErr_Mask                                                        cBit16
#define cAf6_pla_buf_stk2_PlaRdPortInfFFErr_Shift                                                           16

/*--------------------------------------
BitField Name: PlaRdBufDatPkVldFFErr
BitField Type: W2C
BitField Desc: Read Buf Side Pkt Vld Fifo Error
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdBufDatPkVldFFErr_Mask                                                    cBit15
#define cAf6_pla_buf_stk2_PlaRdBufDatPkVldFFErr_Shift                                                       15

/*--------------------------------------
BitField Name: PlaRdBufDatReqVldFFErr
BitField Type: W2C
BitField Desc: Read Buf Side Data Request Vld Fifo Error
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdBufDatReqVldFFErr_Mask                                                   cBit14
#define cAf6_pla_buf_stk2_PlaRdBufDatReqVldFFErr_Shift                                                      14

/*--------------------------------------
BitField Name: PlaRdBufPSNAckFFErr
BitField Type: W2C
BitField Desc: Read Buf Side PSN Ack Fifo Error
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdBufPSNAckFFErr_Mask                                                      cBit13
#define cAf6_pla_buf_stk2_PlaRdBufPSNAckFFErr_Shift                                                         13

/*--------------------------------------
BitField Name: PlaRdBufPSNVldFFErr
BitField Type: W2C
BitField Desc: Read Buf Side PSN Vld Fifo Error
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaRdBufPSNVldFFErr_Mask                                                      cBit12
#define cAf6_pla_buf_stk2_PlaRdBufPSNVldFFErr_Shift                                                         12

/*--------------------------------------
BitField Name: PlaMuxPWSl96_3_SL48_1_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW SLice48#1 of Slice96#3 Error
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_3_SL48_1_Err_Mask                                                cBit11
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_3_SL48_1_Err_Shift                                                   11

/*--------------------------------------
BitField Name: PlaMuxPWSl96_3_SL48_0_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice48#0 of Slice96#3 Error
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_3_SL48_0_Err_Mask                                                cBit10
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_3_SL48_0_Err_Shift                                                   10

/*--------------------------------------
BitField Name: PlaMuxPWSl96_2_SL48_1_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice48#1 of Slice96#2 Error
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_2_SL48_1_Err_Mask                                                 cBit9
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_2_SL48_1_Err_Shift                                                    9

/*--------------------------------------
BitField Name: PlaMuxPWSl96_2_SL48_0_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice48#0 of Slice96#2 Error
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_2_SL48_0_Err_Mask                                                 cBit8
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_2_SL48_0_Err_Shift                                                    8

/*--------------------------------------
BitField Name: PlaMuxPWSl96_1_SL48_1_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW SLice48#1 of Slice96#1 Error
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_1_SL48_1_Err_Mask                                                 cBit7
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_1_SL48_1_Err_Shift                                                    7

/*--------------------------------------
BitField Name: PlaMuxPWSl96_1_SL48_0_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice48#0 of Slice96#1 Error
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_1_SL48_0_Err_Mask                                                 cBit6
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_1_SL48_0_Err_Shift                                                    6

/*--------------------------------------
BitField Name: PlaMuxPWSl96_0_SL48_1_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice48#1 of Slice96#0 Error
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_0_SL48_1_Err_Mask                                                 cBit5
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_0_SL48_1_Err_Shift                                                    5

/*--------------------------------------
BitField Name: PlaMuxPWSl96_0_SL48_0_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice48#0 of Slice96#0 Error
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_0_SL48_0_Err_Mask                                                 cBit4
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_0_SL48_0_Err_Shift                                                    4

/*--------------------------------------
BitField Name: PlaMuxPWSl96_3_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice96#3 Error
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_3_Err_Mask                                                        cBit3
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_3_Err_Shift                                                           3

/*--------------------------------------
BitField Name: PlaMuxPWSl96_2_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice96#2 Error
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_2_Err_Mask                                                        cBit2
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_2_Err_Shift                                                           2

/*--------------------------------------
BitField Name: PlaMuxPWSl96_1_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice96#1 Error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_1_Err_Mask                                                        cBit1
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_1_Err_Shift                                                           1

/*--------------------------------------
BitField Name: PlaMuxPWSl96_0_Err
BitField Type: W2C
BitField Desc: Fifo Mux PW Slice96#0 Error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_0_Err_Mask                                                        cBit0
#define cAf6_pla_buf_stk2_PlaMuxPWSl96_0_Err_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Buffer Sticky 3
Reg Addr   : 0x4_2002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to used to sticky some alarms for debug#3

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_buf_stk3_Base                                                                      0x42002

/*--------------------------------------
BitField Name: PlaWrDDRBak7FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#7 Fifo Full
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak7FifoFul_Mask                                                      cBit31
#define cAf6_pla_buf_stk3_PlaWrDDRBak7FifoFul_Shift                                                         31

/*--------------------------------------
BitField Name: PlaWrDDRBak6FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#6 Fifo Full
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak6FifoFul_Mask                                                      cBit30
#define cAf6_pla_buf_stk3_PlaWrDDRBak6FifoFul_Shift                                                         30

/*--------------------------------------
BitField Name: PlaWrDDRBak5FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#5 Fifo Full
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak5FifoFul_Mask                                                      cBit29
#define cAf6_pla_buf_stk3_PlaWrDDRBak5FifoFul_Shift                                                         29

/*--------------------------------------
BitField Name: PlaWrDDRBak4FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#4 Fifo Full
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak4FifoFul_Mask                                                      cBit28
#define cAf6_pla_buf_stk3_PlaWrDDRBak4FifoFul_Shift                                                         28

/*--------------------------------------
BitField Name: PlaWrDDRBak3FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#3 Fifo Full
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak3FifoFul_Mask                                                      cBit27
#define cAf6_pla_buf_stk3_PlaWrDDRBak3FifoFul_Shift                                                         27

/*--------------------------------------
BitField Name: PlaWrDDRBak2FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#2 Fifo Full
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak2FifoFul_Mask                                                      cBit26
#define cAf6_pla_buf_stk3_PlaWrDDRBak2FifoFul_Shift                                                         26

/*--------------------------------------
BitField Name: PlaWrDDRBak1FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#1 Fifo Full
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak1FifoFul_Mask                                                      cBit25
#define cAf6_pla_buf_stk3_PlaWrDDRBak1FifoFul_Shift                                                         25

/*--------------------------------------
BitField Name: PlaWrDDRBak0FifoFul
BitField Type: W2C
BitField Desc: Write DDR Bak#0 Fifo Full
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrDDRBak0FifoFul_Mask                                                      cBit24
#define cAf6_pla_buf_stk3_PlaWrDDRBak0FifoFul_Shift                                                         24

/*--------------------------------------
BitField Name: PlaPktInfoFifoNearFul
BitField Type: W2C
BitField Desc: Packet Info Fifo near full
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaPktInfoFifoNearFul_Mask                                                    cBit23
#define cAf6_pla_buf_stk3_PlaPktInfoFifoNearFul_Shift                                                       23

/*--------------------------------------
BitField Name: PlaQDRPSNReqSet
BitField Type: W2C
BitField Desc: QDR PSN Request set
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaQDRPSNReqSet_Mask                                                          cBit22
#define cAf6_pla_buf_stk3_PlaQDRPSNReqSet_Shift                                                             22

/*--------------------------------------
BitField Name: PlaQDRPSNAckSet
BitField Type: W2C
BitField Desc: QDR PSN Ack set
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaQDRPSNAckSet_Mask                                                          cBit21
#define cAf6_pla_buf_stk3_PlaQDRPSNAckSet_Shift                                                             21

/*--------------------------------------
BitField Name: PlaQDRPSNVldSet
BitField Type: W2C
BitField Desc: QDR PSN valid set
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaQDRPSNVldSet_Mask                                                          cBit20
#define cAf6_pla_buf_stk3_PlaQDRPSNVldSet_Shift                                                             20

/*--------------------------------------
BitField Name: PlaFreBlkReFiFoErr
BitField Type: W2C
BitField Desc: Free Block Return Fifo Error
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaFreBlkReFiFoErr_Mask                                                       cBit19
#define cAf6_pla_buf_stk3_PlaFreBlkReFiFoErr_Shift                                                          19

/*--------------------------------------
BitField Name: PlaDDRRdReqSet
BitField Type: W2C
BitField Desc: DDR Read Request set
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaDDRRdReqSet_Mask                                                           cBit18
#define cAf6_pla_buf_stk3_PlaDDRRdReqSet_Shift                                                              18

/*--------------------------------------
BitField Name: PlaDDRRdAckSet
BitField Type: W2C
BitField Desc: DDR Read Ack set
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaDDRRdAckSet_Mask                                                           cBit17
#define cAf6_pla_buf_stk3_PlaDDRRdAckSet_Shift                                                              17

/*--------------------------------------
BitField Name: PlaDDRRdVldSet
BitField Type: W2C
BitField Desc: DDR Read valid set
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaDDRRdVldSet_Mask                                                           cBit16
#define cAf6_pla_buf_stk3_PlaDDRRdVldSet_Shift                                                              16

/*--------------------------------------
BitField Name: PlaBlkMdOutEOPSet
BitField Type: W2C
BitField Desc: Output block module EOP set
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaBlkMdOutEOPSet_Mask                                                        cBit15
#define cAf6_pla_buf_stk3_PlaBlkMdOutEOPSet_Shift                                                           15

/*--------------------------------------
BitField Name: PlaDDRWrReqSet
BitField Type: W2C
BitField Desc: DDR Write Request set
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaDDRWrReqSet_Mask                                                           cBit14
#define cAf6_pla_buf_stk3_PlaDDRWrReqSet_Shift                                                              14

/*--------------------------------------
BitField Name: PlaDDRWrAckSet
BitField Type: W2C
BitField Desc: DDR Write Ack set
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaDDRWrAckSet_Mask                                                           cBit13
#define cAf6_pla_buf_stk3_PlaDDRWrAckSet_Shift                                                              13

/*--------------------------------------
BitField Name: PlaDDRWrVldSet
BitField Type: W2C
BitField Desc: DDR Write valid set
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaDDRWrVldSet_Mask                                                           cBit12
#define cAf6_pla_buf_stk3_PlaDDRWrVldSet_Shift                                                              12

/*--------------------------------------
BitField Name: PlaBlkMdInVldSet
BitField Type: W2C
BitField Desc: Input block module valid set
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaBlkMdInVldSet_Mask                                                         cBit11
#define cAf6_pla_buf_stk3_PlaBlkMdInVldSet_Shift                                                            11

/*--------------------------------------
BitField Name: PlaBlkMdInEOPSet
BitField Type: W2C
BitField Desc: Input block module EOP set
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaBlkMdInEOPSet_Mask                                                         cBit10
#define cAf6_pla_buf_stk3_PlaBlkMdInEOPSet_Shift                                                            10

/*--------------------------------------
BitField Name: PlaPWEGetPSNErr1
BitField Type: W2C
BitField Desc: PWE to get PSN Error due to empty
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaPWEGetPSNErr1_Mask                                                          cBit9
#define cAf6_pla_buf_stk3_PlaPWEGetPSNErr1_Shift                                                             9

/*--------------------------------------
BitField Name: PlaPWEGetPSNErr0
BitField Type: W2C
BitField Desc: PWE to get PSN Error due to PSN len
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaPWEGetPSNErr0_Mask                                                          cBit8
#define cAf6_pla_buf_stk3_PlaPWEGetPSNErr0_Shift                                                             8

/*--------------------------------------
BitField Name: PlaPWEGetPSN
BitField Type: W2C
BitField Desc: PWE to get PSN from PLA
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaPWEGetPSN_Mask                                                              cBit7
#define cAf6_pla_buf_stk3_PlaPWEGetPSN_Shift                                                                 7

/*--------------------------------------
BitField Name: PlaReadyDat4PWE
BitField Type: W2C
BitField Desc: Ready data for PWE
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaReadyDat4PWE_Mask                                                           cBit6
#define cAf6_pla_buf_stk3_PlaReadyDat4PWE_Shift                                                              6

/*--------------------------------------
BitField Name: PlaPWEGetDat
BitField Type: W2C
BitField Desc: PWE to get data from PLA
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaPWEGetDat_Mask                                                              cBit5
#define cAf6_pla_buf_stk3_PlaPWEGetDat_Shift                                                                 5

/*--------------------------------------
BitField Name: PlaReady2RdPkt
BitField Type: W2C
BitField Desc: Read Port ready to read packet Info
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaReady2RdPkt_Mask                                                            cBit4
#define cAf6_pla_buf_stk3_PlaReady2RdPkt_Shift                                                               4

/*--------------------------------------
BitField Name: PlaRdpkInfVldSet
BitField Type: W2C
BitField Desc: Read Packet Info Valid Set
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaRdpkInfVldSet_Mask                                                          cBit3
#define cAf6_pla_buf_stk3_PlaRdpkInfVldSet_Shift                                                             3

/*--------------------------------------
BitField Name: PlaWrpkInfVldSet
BitField Type: W2C
BitField Desc: Write Packet Info Valid Set
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaWrpkInfVldSet_Mask                                                          cBit2
#define cAf6_pla_buf_stk3_PlaWrpkInfVldSet_Shift                                                             2

/*--------------------------------------
BitField Name: PlaBlkMdOutVldSet
BitField Type: W2C
BitField Desc: Output block module valid set
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaBlkMdOutVldSet_Mask                                                         cBit1
#define cAf6_pla_buf_stk3_PlaBlkMdOutVldSet_Shift                                                            1

/*--------------------------------------
BitField Name: PlaPWCoreOutVldSet
BitField Type: W2C
BitField Desc: Output pwcore valid set
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_buf_stk3_PlaPWCoreOutVldSet_Mask                                                        cBit0
#define cAf6_pla_buf_stk3_PlaPWCoreOutVldSet_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Buffer Block Number Status
Reg Addr   : 0x4_2010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to store current blocks number

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_buf_blk_num_sta_Base                                                               0x42010

/*--------------------------------------
BitField Name: HiBlkNum
BitField Type: RO
BitField Desc: Current High Block Number (init value is 0x6000)
BitField Bits: [35:20]
--------------------------------------*/
#define cAf6_pla_buf_blk_num_sta_HiBlkNum_01_Mask                                                    cBit31_20
#define cAf6_pla_buf_blk_num_sta_HiBlkNum_01_Shift                                                          20
#define cAf6_pla_buf_blk_num_sta_HiBlkNum_02_Mask                                                      cBit3_0
#define cAf6_pla_buf_blk_num_sta_HiBlkNum_02_Shift                                                           0

/*--------------------------------------
BitField Name: LoBlkNum
BitField Type: RO
BitField Desc: Current Low Block Number (init value is 0x10000)
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_pla_buf_blk_num_sta_LoBlkNum_Mask                                                        cBit16_0
#define cAf6_pla_buf_blk_num_sta_LoBlkNum_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Buffer Cache Number Status
Reg Addr   : 0x4_2011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to store current caches number

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_buf_blk_cache_sta_Base                                                             0x42011

/*--------------------------------------
BitField Name: Ca512Num
BitField Type: RO
BitField Desc: Current Cache-512 Number (init value is 0x800)
BitField Bits: [43:32]
--------------------------------------*/
#define cAf6_pla_buf_blk_cache_sta_Ca512Num_Mask                                                      cBit11_0
#define cAf6_pla_buf_blk_cache_sta_Ca512Num_Shift                                                            0

/*--------------------------------------
BitField Name: Ca256Num
BitField Type: RO
BitField Desc: Current Cache-256 Number (init value is 0x2000)
BitField Bits: [29:16]
--------------------------------------*/
#define cAf6_pla_buf_blk_cache_sta_Ca256Num_Mask                                                     cBit29_16
#define cAf6_pla_buf_blk_cache_sta_Ca256Num_Shift                                                           16

/*--------------------------------------
BitField Name: Ca128Num
BitField Type: RO
BitField Desc: Current Cache-128 Number (init value is 0x2A00)
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pla_buf_blk_cache_sta_Ca128Num_Mask                                                      cBit14_0
#define cAf6_pla_buf_blk_cache_sta_Ca128Num_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler OC192c clk311 Sticky
Reg Addr   : 0xE_0002-0xF_0002
Reg Formula: 0xE_0002 + $Oc192Slice*65536
    Where  : 
           + $Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G
Reg Desc   : 
This register is used to used to sticky some alarms for debug per OC-192c @clk311.02 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_oc192c_clk311_stk_Base                                                             0xE0002
#define cAf6Reg_pla_oc192c_clk311_stk_WidthVal                                                              32

/*--------------------------------------
BitField Name: Pla311OC192c_OutAIS
BitField Type: W2C
BitField Desc: OC192c Output AIS
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutAIS_Mask                                              cBit7
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutAIS_Shift                                                 7

/*--------------------------------------
BitField Name: Pla311OC192c_OutPos
BitField Type: W2C
BitField Desc: OC192c Output Pos Pointer
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutPos_Mask                                              cBit6
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutPos_Shift                                                 6

/*--------------------------------------
BitField Name: Pla311OC192c_OutNeg
BitField Type: W2C
BitField Desc: OC192c Output Neg Pointer
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutNeg_Mask                                              cBit5
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutNeg_Shift                                                 5

/*--------------------------------------
BitField Name: Pla311OC192c_InValid
BitField Type: W2C
BitField Desc: OC192c input valid
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InValid_Mask                                             cBit4
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InValid_Shift                                                4

/*--------------------------------------
BitField Name: Pla311OC192c_InAIS
BitField Type: W2C
BitField Desc: OC192c input AIS
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InAIS_Mask                                               cBit3
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InAIS_Shift                                                  3

/*--------------------------------------
BitField Name: Pla311OC192c_InPos
BitField Type: W2C
BitField Desc: OC192c input CEP Pos Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InPos_Mask                                               cBit2
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InPos_Shift                                                  2

/*--------------------------------------
BitField Name: Pla311OC192c_InNeg
BitField Type: W2C
BitField Desc: OC192c input CEP Neg Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InNeg_Mask                                               cBit1
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InNeg_Shift                                                  1

/*--------------------------------------
BitField Name: Pla311OC192c_InJ1Pos
BitField Type: W2C
BitField Desc: OC192c input CEP J1 Position
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InJ1Pos_Mask                                             cBit0
#define cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InJ1Pos_Shift                                                0

#endif /* _AF6_REG_AF6CNC0022_RD_PLA_DEBUG_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60291022ModulePweInternal.h
 * 
 * Created Date: Jul 25, 2018
 *
 * Description : 60291022 module PWE internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULEPWEINTERNAL_H_
#define _THA60291022MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pwe/Tha60290022ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291022ModulePwe
    {
    tTha60290022ModulePwe super;
    }tTha60291022ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MODULEPWEINTERNAL_H_ */


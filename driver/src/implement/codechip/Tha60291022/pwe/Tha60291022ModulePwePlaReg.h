/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PLA_H_
#define _AF6_REG_AF6CNC0022_RD_PLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x8_2000-0xB_6A7F
Reg Formula: 0x8_2000 + $Oc96Slice*65536 + $Oc48Slice*16384 + $Pwid
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2687): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice
HDL_PATH  : ipwcore.ipwslcore.isl48core[$Oc96Slice*2 + $Oc48Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$Pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pld_ctrl_Base                                                                      0x82000
#define cAf6Reg_pla_pld_ctrl_WidthVal                                                                       64

/*--------------------------------------
BitField Name: PlaLkPWIDCtrl
BitField Type: RW
BitField Desc: Lookup to a output psedowire
BitField Bits: [31:18]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLkPWIDCtrl_Mask                                                         cBit31_18
#define cAf6_pla_pld_ctrl_PlaLkPWIDCtrl_Shift                                                               18

/*--------------------------------------
BitField Name: PlaLoPldTSSrcCtrl
BitField Type: RW
BitField Desc: Timestamp Source 0: PRC global 1: TS from CDR engine
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldTSSrcCtrl_Mask                                                        cBit17
#define cAf6_pla_pld_ctrl_PlaLoPldTSSrcCtrl_Shift                                                           17

/*--------------------------------------
BitField Name: PlaLoPldTypeCtrl
BitField Type: RW
BitField Desc: Payload Type 0: satop 1: ces without cas 2: cep 3: ces with cas
BitField Bits: [16:15]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldTypeCtrl_Mask                                                      cBit16_15
#define cAf6_pla_pld_ctrl_PlaLoPldTypeCtrl_Shift                                                            15

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size satop/cep mode: bit[13:0] payload size (ex: value as
0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot
bit[14:6] number of NxDS0 frame
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldSizeCtrl_Mask                                                       cBit14_0
#define cAf6_pla_pld_ctrl_PlaLoPldSizeCtrl_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x8_3000-0xB_7A7F
Reg Formula: 0x8_3000 + $Oc96Slice*65536 + $Oc48Slice*16384 + $Pwid
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2687): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-48 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_add_rmv_pw_ctrl_Base                                                               0x83000
#define cAf6Reg_pla_add_rmv_pw_ctrl_WidthVal                                                                32

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                               cBit1_0
#define cAf6_pla_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Pseudowire Header Control
Reg Addr   : 0xC_0000-0xC_29FF
Reg Formula: 0xC_0000 + $pwid
    Where  : 
           + $pwid(0-10751): pseudowire channel
Reg Desc   : 
This register is used to config Control Word for the psedowire channels
HDL_PATH  : ipwcore.ipwcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_hdr_ctrl_Base                                                                   0xC0000
#define cAf6Reg_pla_pw_hdr_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaPwHiRateCtrl
BitField Type: RW
BitField Desc: set 1 for high rate path (12c/48c/192c), other is set 0
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwHiRateCtrl_Mask                                                        cBit9
#define cAf6_pla_pw_hdr_ctrl_PlaPwHiRateCtrl_Shift                                                           9

/*--------------------------------------
BitField Name: PlaPwSuprCtrl
BitField Type: RW
BitField Desc: Suppresion Enable 1: enable 0: disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Mask                                                          cBit8
#define cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Shift                                                             8

/*--------------------------------------
BitField Name: PlaPwRbitDisCtrl
BitField Type: RW
BitField Desc: R bit disable 1: disable R bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Mask                                                       cBit7
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Shift                                                          7

/*--------------------------------------
BitField Name: PlaPwMbitDisCtrl
BitField Type: RW
BitField Desc: M or NP bits disable 1: disable M or NP bits in the Control Word
(assign zero in the packet) 0: normal
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Mask                                                       cBit6
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Shift                                                          6

/*--------------------------------------
BitField Name: PlaPwLbitDisCtrl
BitField Type: RW
BitField Desc: L bit disable 1: disable L bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Mask                                                       cBit5
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Shift                                                          5

/*--------------------------------------
BitField Name: PlaPwRbitCPUCtrl
BitField Type: RW
BitField Desc: R bit value from CPU (low priority than PlaPwRbitDisCtrl)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Mask                                                       cBit4
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Shift                                                          4

/*--------------------------------------
BitField Name: PlaPwMbitCPUCtrl
BitField Type: RW
BitField Desc: M or NP bits value from CPU (low priority than
PlaLoPwMbitDisCtrl)
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Mask                                                     cBit3_2
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Shift                                                          2

/*--------------------------------------
BitField Name: PlaPwLbitCPUCtrl
BitField Type: RW
BitField Desc: L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Mask                                                       cBit1
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Shift                                                          1

/*--------------------------------------
BitField Name: PlaPwEnCtrl
BitField Type: RW
BitField Desc: Pseudowire enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Mask                                                            cBit0
#define cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Pseudowire PSN Control
Reg Addr   : 0x4_8000-0x4_A9FF
Reg Formula: 0x4_8000 + $pwid
    Where  : 
           + $pwid(0-10751): pseudowire channel
Reg Desc   : 
This register is used to config psedowire PSN and APS at output
HDL_PATH  : irdport.iflowcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_psn_ctrl_Base                                                                   0x48000
#define cAf6Reg_pla_pw_psn_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaOutPSNLenCtrl
BitField Type: RW
BitField Desc: PSN length
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_Mask                                                     cBit6_0
#define cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Psedowire Protection Control
Reg Addr   : 0x5_0000-0x5_29FF
Reg Formula: 0x5_0000 + $pwid
    Where  : 
           + $pwid(0-10751): flow channel
Reg Desc   : 
This register is used to config UPSR/HSPW for protect psedowire
HDL_PATH  : irdport.iapslkcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_pro_ctrl_Base                                                                   0x50000
#define cAf6Reg_pla_pw_pro_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaOutUpsrGrpCtr
BitField Type: RW
BitField Desc: UPRS group
BitField Bits: [29:16]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Mask                                                   cBit29_16
#define cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Shift                                                         16

/*--------------------------------------
BitField Name: PlaOutHspwGrpCtr
BitField Type: RW
BitField Desc: HSPW group
BitField Bits: [15:2]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Mask                                                    cBit15_2
#define cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Shift                                                          2

/*--------------------------------------
BitField Name: PlaFlowUPSRUsedCtrl
BitField Type: RW
BitField Desc: Flow UPSR is used or not 0:not used, all configurations for UPSR
will be not effected 1:used
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Mask                                                    cBit1
#define cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Shift                                                       1

/*--------------------------------------
BitField Name: PlaFlowHSPWUsedCtrl
BitField Type: RW
BitField Desc: Flow HSPW is used or not 0:not used, all configurations for HSPW
will be not effected 1:used
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_Mask                                                    cBit0
#define cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output UPSR Control
Reg Addr   : 0x5_8000-0x5_A9FF
Reg Formula: 0x5_8000 + $upsrgrp
    Where  : 
           + $upsrgrp(0-671): UPSR group address
Reg Desc   : 
This register is used to config UPSR group
HDL_PATH  : irdport.iupsrcfg.membuf.ram.ram[$upsrgrp]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_upsr_ctrl_Base                                                                 0x58000
#define cAf6Reg_pla_out_upsr_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: PlaOutUpsrEnCtr
BitField Type: RW
BitField Desc: Total is 10752 upsrID, divide into 672 group address for
configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for
upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask                                                   cBit15_0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output HSPW Control
Reg Addr   : 0x6_0000-0x6_029F
Reg Formula: 0x6_0000 + $hspwgrp
    Where  : 
           + $hspwgrp(0-671): HSPW group address
Reg Desc   : 
This register is used to config HSPW group
HDL_PATH  : irdport.ihspwcfg.membuf.ram.ram[$hspwgrp]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_hspw_ctrl_Base                                                                 0x60000
#define cAf6Reg_pla_out_hspw_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: PlaOutHSPWCtr
BitField Type: RW
BitField Desc: Total is 10752 hspwID, divide into 672 group address for
configuration, 16-bit is correlative with 16 hspwID each group. Bit#0 is for
hspwID#0, bit#1 is for hspwID#1...
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_out_hspw_ctrl_PlaOutHSPWCtr_Mask                                                     cBit15_0
#define cAf6_pla_out_hspw_ctrl_PlaOutHSPWCtr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x4_0000
Reg Formula: 0x4_0000
    Where  : 
Reg Desc   : 
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ctrl_Base                                                               0x40000

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask                                                cBit31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Shift                                                   31

/*--------------------------------------
BitField Name: PlaOutPsnProRnWCtr
BitField Type: RW
BitField Desc: read or write PSN 1: read PSN 0: write PSN
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Mask                                                cBit30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Shift                                                   30

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Mask                                             cBit22_16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Shift                                                   16

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask                                               cBit15
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift                                                  15

/*--------------------------------------
BitField Name: PlaOutPsnProFlowCtr
BitField Type: RW
BitField Desc: Flow channel
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask                                             cBit13_0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Buffer Control
Reg Addr   : 0x4_0010-0x4_0015
Reg Formula: 0x4_0010 + $segid
    Where  : 
           + $segid(0-5): segment 16-byte PSN, total is 6 segments for maximum 96 bytes PSN  %
Reg Desc   : 
This register is used to store PSN data which is before CPU request write or after CPU request read %%
The header format from entry#0 to entry#4 is as follow: %%
{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%
Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%

PSN header is MEF-8: %%
EthType:  0x88D8 %%
4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %%
PSN header is MPLS:  %%
EthType:  0x8847 %%
4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%
Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%
Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%
PSN header is UDP/Ipv4:  %%
EthType:  0x0800 %%
28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
{IP_SrcAdr[31:0]} %%
{IP_DesAdr[31:0]} %%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%
{UDP_Sum[31:0] (optional)} %%
Case: %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%
IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%
{IP_TTL[7:0], IP_Protocol[7:0]} + %%
IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%
IP_DesAdr[31:16] + IP_DesAdr[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%
IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Protocol[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
PSN header is UDP/Ipv6:  %%
EthType:  0x86DD%%
48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%
{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%
{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%
{IP_SrcAdr[127:96]}%%
{IP_SrcAdr[95:64]}%%
{IP_SrcAdr[63:32]}%%
{IP_SrcAdr[31:0]}%%
{IP_DesAdr[127:96]}%%
{IP_DesAdr[95:64]}%%
{IP_DesAdr[63:32]}%%
{IP_DesAdr[31:0]}%%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
{UDP_Sum[31:0]}%%
Case:%%
IP_Next_Header[7:0]: 0x11 to signify UDP %%
UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%
IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Next_Header[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%
PSN header is MPLS over Ipv4:%%
IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%
After IPv4 header is MPLS labels where inner label is used for PW identification%%
PSN header is MPLS over Ipv6:%%
IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%
After IPv6 header is MPLS labels where inner label is used for PW identification%%
PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%%
Case:%%
RTPSSRC[31:0] : This is the SSRC value of RTP header%%
RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnbuf_ctrl_Base                                                               0x40010

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: PSN buffer
BitField Bits: [127:0]
--------------------------------------*/
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_01_Mask                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_01_Shift                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_02_Mask                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_02_Shift                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_03_Mask                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_03_Shift                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_04_Mask                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_04_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Hold Register Control
Reg Addr   : 0x7_0000-0x7_0002
Reg Formula: 0x7_0000 + $holdreg
    Where  : 
           + $holdreg(0-2): hold register with (holdreg=0) for bit63_32, (holdreg=1) for bit95_64, (holdreg=2) for bit127_96
Reg Desc   : 
This register is used to control hold register.

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_hold_reg_ctrl_Base                                                                 0x70000

/*--------------------------------------
BitField Name: PlaHoldRegCtr
BitField Type: RW
BitField Desc: hold register value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Mask                                                     cBit31_0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x70100 - 0x7010F
Reg Formula: 0x70100 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x70100
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x70110 - 0x7011F
Reg Formula: 0x70110 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x70110
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x70120 - 0x7012F
Reg Formula: 0x70120 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x70120
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x070130 - 0x7013F
Reg Formula: 0x070130 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                0x070130
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x70140 - 0x7014F
Reg Formula: 0x70140 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x70140
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x70150 - 0x7015F
Reg Formula: 0x70150 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x70150
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x70160 - 0x70161
Reg Formula: 0x70160 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x70160

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RO
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x70170
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x70170

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RO
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x70171
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x70171

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RO
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x70172
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x70172

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RO
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x4_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ha_ctrl_Base                                                            0x40001

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Mask                                             cBit31
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Shift                                                31

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Mask                                          cBit22_16
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Shift                                                16

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Mask                                            cBit13
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Shift                                               13

/*--------------------------------------
BitField Name: PlaOutPsnProPWIDCtr
BitField Type: RW
BitField Desc: Pseudowire channel
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPWIDCtr_Mask                                          cBit12_0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPWIDCtr_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Force Parity Error Control
Reg Addr   : 0x42040
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to force parity error

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_force_par_err_control_Base                                                         0x42040
#define cAf6Reg_pla_force_par_err_control_WidthVal                                                          32

/*--------------------------------------
BitField Name: ForceParErrOutUPSRCtrReg
BitField Type: RW
BitField Desc: Force Parity Error Ouput UPSR Control Register
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrOutUPSRCtrReg_Mask                                    cBit20
#define cAf6_pla_force_par_err_control_ForceParErrOutUPSRCtrReg_Shift                                       20

/*--------------------------------------
BitField Name: ForceParErrPWProCtrReg
BitField Type: RW
BitField Desc: Force Parity Error PW Protection Control Register
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPWProCtrReg_Mask                                      cBit18
#define cAf6_pla_force_par_err_control_ForceParErrPWProCtrReg_Shift                                         18

/*--------------------------------------
BitField Name: ForceParErrPWPSNCtrReg
BitField Type: RW
BitField Desc: Force Parity Error PW PSNr Control Register
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPWPSNCtrReg_Mask                                      cBit17
#define cAf6_pla_force_par_err_control_ForceParErrPWPSNCtrReg_Shift                                         17

/*--------------------------------------
BitField Name: ForceParErrPWHdrCtrReg
BitField Type: RW
BitField Desc: Force Parity Error PW Header Control Register
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPWHdrCtrReg_Mask                                      cBit16
#define cAf6_pla_force_par_err_control_ForceParErrPWHdrCtrReg_Shift                                         16

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_3
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#3
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_3_Mask                                  cBit14
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_3_Shift                                      14

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_3_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#3
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_1_Mask                                  cBit13
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_1_Shift                                      13

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_3_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#3
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_0_Mask                                  cBit12
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_3_OC48_0_Shift                                      12

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_2
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_2_Mask                                  cBit10
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_2_Shift                                      10

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_2_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#2
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_1_Mask                                   cBit9
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_1_Shift                                       9

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_2_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#2
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_0_Mask                                   cBit8
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_2_OC48_0_Shift                                       8

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_1
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_1_Mask                                   cBit6
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_1_Shift                                       6

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_1_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_1_Mask                                   cBit5
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_1_Shift                                       5

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_1_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_0_Mask                                   cBit4
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_1_OC48_0_Shift                                       4

/*--------------------------------------
BitField Name: ForceParErrLkPWCtrRegOC96_0
BitField Type: RW
BitField Desc: Force Parity Error Lookup PW  Control Register OC96#0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_0_Mask                                   cBit2
#define cAf6_pla_force_par_err_control_ForceParErrLkPWCtrRegOC96_0_Shift                                       2

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_0_OC48_1
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#1 of OC96#0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_1_Mask                                   cBit1
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_1_Shift                                       1

/*--------------------------------------
BitField Name: ForceParErrPldCtrRegOC96_0_OC48_0
BitField Type: RW
BitField Desc: Force Parity Error Payload Control Register OC48#0 of OC96#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_0_Mask                                   cBit0
#define cAf6_pla_force_par_err_control_ForceParErrPldCtrRegOC96_0_OC48_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Disable Parity Control
Reg Addr   : 0x42041
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to disable parity check

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_dis_par_control_Base                                                               0x42041
#define cAf6Reg_pla_dis_par_control_WidthVal                                                                32

/*--------------------------------------
BitField Name: DisParErrOutUPSRCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error Ouput UPSR Control Register
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrOutUPSRCtrReg_Mask                                            cBit20
#define cAf6_pla_dis_par_control_DisParErrOutUPSRCtrReg_Shift                                               20

/*--------------------------------------
BitField Name: DisParErrPWProCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error PW Protection Control Register
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPWProCtrReg_Mask                                              cBit18
#define cAf6_pla_dis_par_control_DisParErrPWProCtrReg_Shift                                                 18

/*--------------------------------------
BitField Name: DisParErrPWPSNCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error PW PSNr Control Register
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPWPSNCtrReg_Mask                                              cBit17
#define cAf6_pla_dis_par_control_DisParErrPWPSNCtrReg_Shift                                                 17

/*--------------------------------------
BitField Name: DisParErrPWHdrCtrReg
BitField Type: RW
BitField Desc: Dis Parity Error PW Header Control Register
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPWHdrCtrReg_Mask                                              cBit16
#define cAf6_pla_dis_par_control_DisParErrPWHdrCtrReg_Shift                                                 16

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_3
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#3
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_3_Mask                                         cBit14
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_3_Shift                                            14

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_3_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#3
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_1_Mask                                   cBit13
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_1_Shift                                      13

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_3_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#3
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_0_Mask                                   cBit12
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_3_OC48_0_Shift                                      12

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_2
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_2_Mask                                         cBit10
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_2_Shift                                            10

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_2_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#2
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_1_Mask                                    cBit9
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_1_Shift                                       9

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_2_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#2
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_0_Mask                                    cBit8
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_2_OC48_0_Shift                                       8

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_1
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_1_Mask                                          cBit6
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_1_Shift                                             6

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_1_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_1_Mask                                    cBit5
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_1_Shift                                       5

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_1_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_0_Mask                                    cBit4
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_1_OC48_0_Shift                                       4

/*--------------------------------------
BitField Name: DisParErrLkPWCtrRegOC96_0
BitField Type: RW
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_0_Mask                                          cBit2
#define cAf6_pla_dis_par_control_DisParErrLkPWCtrRegOC96_0_Shift                                             2

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_0_OC48_1
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_1_Mask                                    cBit1
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_1_Shift                                       1

/*--------------------------------------
BitField Name: DisParErrPldCtrRegOC96_0_OC48_0
BitField Type: RW
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_0_Mask                                    cBit0
#define cAf6_pla_dis_par_control_DisParErrPldCtrRegOC96_0_OC48_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Parity Error Sticky
Reg Addr   : 0x42042
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to sticky parity check

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_par_err_stk_Base                                                                   0x42042
#define cAf6Reg_pla_par_err_stk_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ParErrOutUPSRCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error Ouput UPSR Control Register
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrOutUPSRCtrStk_Mask                                                   cBit20
#define cAf6_pla_par_err_stk_ParErrOutUPSRCtrStk_Shift                                                      20

/*--------------------------------------
BitField Name: ParErrPWProCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error PW Protection Control Register
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPWProCtrStk_Mask                                                     cBit18
#define cAf6_pla_par_err_stk_ParErrPWProCtrStk_Shift                                                        18

/*--------------------------------------
BitField Name: ParErrPWPSNCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error PW PSNr Control Register
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPWPSNCtrStk_Mask                                                     cBit17
#define cAf6_pla_par_err_stk_ParErrPWPSNCtrStk_Shift                                                        17

/*--------------------------------------
BitField Name: ParErrPWHdrCtrStk
BitField Type: W1C
BitField Desc: Dis Parity Error PW Header Control Register
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPWHdrCtrStk_Mask                                                     cBit16
#define cAf6_pla_par_err_stk_ParErrPWHdrCtrStk_Shift                                                        16

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_3Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#3
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_3Stk_Mask                                             cBit14
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_3Stk_Shift                                                14

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_3_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#3
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_1Stk_Mask                                       cBit13
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_1Stk_Shift                                          13

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_3_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#3
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_0Stk_Mask                                       cBit12
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_3_OC48_0Stk_Shift                                          12

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_2Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#2
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_2Stk_Mask                                             cBit10
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_2Stk_Shift                                                10

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_2_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#2
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_1Stk_Mask                                        cBit9
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_1Stk_Shift                                           9

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_2_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#2
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_0Stk_Mask                                        cBit8
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_2_OC48_0Stk_Shift                                           8

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_1Stk_Mask                                              cBit6
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_1Stk_Shift                                                 6

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_1_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_1Stk_Mask                                        cBit5
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_1Stk_Shift                                           5

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_1_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_0Stk_Mask                                        cBit4
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_1_OC48_0Stk_Shift                                           4

/*--------------------------------------
BitField Name: ParErrLkPWCtrRegOC96_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Lookup PW  Control Register OC96#0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_0Stk_Mask                                              cBit2
#define cAf6_pla_par_err_stk_ParErrLkPWCtrRegOC96_0Stk_Shift                                                 2

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_0_OC48_1Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#1 of OC96#0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_1Stk_Mask                                        cBit1
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_1Stk_Shift                                           1

/*--------------------------------------
BitField Name: ParErrPldCtrRegOC96_0_OC48_0Stk
BitField Type: W1C
BitField Desc: Dis Parity Error Payload Control Register OC48#0 of OC96#0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_0Stk_Mask                                        cBit0
#define cAf6_pla_par_err_stk_ParErrPldCtrRegOC96_0_OC48_0Stk_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Force CRC Error Control
Reg Addr   : 0x42030
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to force CRC error

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_force_crc_err_control_Base                                                         0x42030
#define cAf6Reg_pla_force_crc_err_control_WidthVal                                                          32

/*--------------------------------------
BitField Name: PLACrcErrForeverCfg
BitField Type: RW
BitField Desc: Force crc error mode  1: forever when field PLACrcErrNumberCfg
differ zero 0: burst
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_force_crc_err_control_PLACrcErrForeverCfg_Mask                                         cBit28
#define cAf6_pla_force_crc_err_control_PLACrcErrForeverCfg_Shift                                            28

/*--------------------------------------
BitField Name: PLACrcErrNumberCfg
BitField Type: RW
BitField Desc: number of CRC error inserted to PLA DDR
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_pla_force_crc_err_control_PLACrcErrNumberCfg_Mask                                        cBit27_0
#define cAf6_pla_force_crc_err_control_PLACrcErrNumberCfg_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : PLA CRC Error Sticky
Reg Addr   : 0x42031
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report sticky of PLA CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_crc_sticky_Base                                                                    0x42031
#define cAf6Reg_pla_crc_sticky_WidthVal                                                                     32

/*--------------------------------------
BitField Name: PLACrcError
BitField Type: WC
BitField Desc: PLA CRC error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_crc_sticky_PLACrcError_Mask                                                             cBit0
#define cAf6_pla_crc_sticky_PLACrcError_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : PLA CRC Error Counter
Reg Addr   : 0x42032
Reg Formula: 0x42032 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts PLA CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_PLA_crc_counter_Base                                                                   0x42032
#define cAf6Reg_PLA_crc_counter_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PLACrcErrorCounter
BitField Type: RO
BitField Desc: PLA Crc error counter
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_PLA_crc_counter_PLACrcErrorCounter_Mask                                                  cBit27_0
#define cAf6_PLA_crc_counter_PLACrcErrorCounter_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0xE_0000-0xF_0000
Reg Formula: 0xE_0000 + $Oc192Slice*65536
    Where  : 
           + $Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_192c_pld_ctrl_Base                                                                 0xE0000
#define cAf6Reg_pla_192c_pld_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: Pla192PWIDCtrl
BitField Type: RW
BitField Desc: lookup pwid
BitField Bits: [29:16]
--------------------------------------*/
#define cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_Mask                                                   cBit29_16
#define cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_Shift                                                         16

/*--------------------------------------
BitField Name: Pla192PldCtrl
BitField Type: RW
BitField Desc: Payload Size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_Mask                                                     cBit13_0
#define cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler OC192c Add/Remove Pseudowire Protocol Control
Reg Addr   : 0xE_0001-0xF_0001
Reg Formula: 0xE_0001 + $Oc192Slice*65536
    Where  : 
           + $Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-192 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_oc192c_add_rmv_pw_ctrl_Base                                                        0xE0001
#define cAf6Reg_pla_oc192c_add_rmv_pw_ctrl_WidthVal                                                         32

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_oc192c_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                        cBit1_0
#define cAf6_pla_oc192c_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : BERT GEN CONTROL
Reg Addr   : 0x4_4000 - 0x4_5000
Reg Formula: 0x4_4000 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
This register config mode for bert

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_montdm_config_Base                                                                0x44000

/*--------------------------------------
BitField Name: BERT_inv
BitField Type: RW
BitField Desc: Set 1 to enbale invert mode
BitField Bits: [10]
--------------------------------------*/
#define cAf6_bert_montdm_config_BERT_inv_Mask                                                           cBit10
#define cAf6_bert_montdm_config_BERT_inv_Shift                                                              10

/*--------------------------------------
BitField Name: Thr_Err
BitField Type: RW
BitField Desc: Thrhold declare lost syn sta
BitField Bits: [9:7]
--------------------------------------*/
#define cAf6_bert_montdm_config_Thr_Err_Mask                                                           cBit9_7
#define cAf6_bert_montdm_config_Thr_Err_Shift                                                                7

/*--------------------------------------
BitField Name: Thr_Syn
BitField Type: RW
BitField Desc: Thrhold declare syn sta
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_bert_montdm_config_Thr_Syn_Mask                                                           cBit6_4
#define cAf6_bert_montdm_config_Thr_Syn_Shift                                                                4

/*--------------------------------------
BitField Name: BertEna
BitField Type: RW
BitField Desc: Set 1 to enable BERT
BitField Bits: [3]
--------------------------------------*/
#define cAf6_bert_montdm_config_BertEna_Mask                                                             cBit3
#define cAf6_bert_montdm_config_BertEna_Shift                                                                3

/*--------------------------------------
BitField Name: BertMode
BitField Type: RW
BitField Desc: "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5"
all1,"6" all0
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_bert_montdm_config_BertMode_Mask                                                          cBit2_0
#define cAf6_bert_montdm_config_BertMode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm  r2c
Reg Addr   : 0x4_4001 - 0x4_5001
Reg Formula: 0x4_4001 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_r2c_Base                                                           0x44001
#define cAf6Reg_goodbit_pen_tdm_mon_r2c_WidthVal                                                            64

/*--------------------------------------
BitField Name: goodmonr2c
BitField Type: RC
BitField Desc: mon goodbit mode read 2clear
BitField Bits: [33:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_r2c_goodmonr2c_01_Mask                                               cBit31_0
#define cAf6_goodbit_pen_tdm_mon_r2c_goodmonr2c_01_Shift                                                     0
#define cAf6_goodbit_pen_tdm_mon_r2c_goodmonr2c_02_Mask                                                cBit1_0
#define cAf6_goodbit_pen_tdm_mon_r2c_goodmonr2c_02_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm  ro
Reg Addr   : 0x4_4002 - 0x4_5002
Reg Formula: 0x4_4002 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_ro_Base                                                            0x44002
#define cAf6Reg_goodbit_pen_tdm_mon_ro_WidthVal                                                             64

/*--------------------------------------
BitField Name: goodmonro
BitField Type: RO
BitField Desc: mon goodbit mode read only
BitField Bits: [33:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_ro_goodmonro_01_Mask                                                 cBit31_0
#define cAf6_goodbit_pen_tdm_mon_ro_goodmonro_01_Shift                                                       0
#define cAf6_goodbit_pen_tdm_mon_ro_goodmonro_02_Mask                                                  cBit1_0
#define cAf6_goodbit_pen_tdm_mon_ro_goodmonro_02_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : error counter tdm  r2c
Reg Addr   : 0x4_4003 - 0x4_5003
Reg Formula: 0x4_4003 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errorbit_pen_tdm_mon_r2c_Base                                                          0x44003

/*--------------------------------------
BitField Name: errormonr2c
BitField Type: RC
BitField Desc: mon errorbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errorbit_pen_tdm_mon_r2c_errormonr2c_Mask                                                cBit31_0
#define cAf6_errorbit_pen_tdm_mon_r2c_errormonr2c_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : error counter tdm  ro
Reg Addr   : 0x4_4004 - 0x4_5004
Reg Formula: 0x4_4004 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errorbit_pen_tdm_mon_ro_Base                                                           0x44004

/*--------------------------------------
BitField Name: errormonro
BitField Type: RO
BitField Desc: mon errorbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errorbit_pen_tdm_mon_ro_errormonro_Mask                                                  cBit31_0
#define cAf6_errorbit_pen_tdm_mon_ro_errormonro_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  r2c
Reg Addr   : 0x4_4005 - 0x4_5005
Reg Formula: 0x4_4005 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_tdm_mon_r2c_Base                                                           0x44005

/*--------------------------------------
BitField Name: lostmonr2c
BitField Type: RC
BitField Desc: mon lostbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lostbit_pen_tdm_mon_r2c_lostmonr2c_Mask                                                  cBit31_0
#define cAf6_lostbit_pen_tdm_mon_r2c_lostmonr2c_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  ro
Reg Addr   : 0x4_4006 - 0x4_5006
Reg Formula: 0x4_4006 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_tdm_mon_ro_Base                                                            0x44006

/*--------------------------------------
BitField Name: lostmonro
BitField Type: RO
BitField Desc: mon lostbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lostbit_pen_tdm_mon_ro_lostmonro_Mask                                                    cBit31_0
#define cAf6_lostbit_pen_tdm_mon_ro_lostmonro_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  ro
Reg Addr   : 0x4_4007 - 0x4_5007
Reg Formula: 0x4_4007 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Status

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_tdm_mon_ro_staprbs_Base                                                            0x44007

/*--------------------------------------
BitField Name: staprbs
BitField Type: R0
BitField Desc: "3" SYNC , other LOST
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_lostbit_pen_tdm_mon_ro_staprbs_Mask                                                       cBit1_0
#define cAf6_lostbit_pen_tdm_mon_ro_staprbs_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  ro
Reg Addr   : 0x4_4008 - 0x4_5008
Reg Formula: 0x4_4008 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_tdm_mon_ro_stickyprbs_Base                                                            0x44008

/*--------------------------------------
BitField Name: stickyprbs
BitField Type: W1C
BitField Desc: "1" LOSTSYN
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_lostbit_pen_tdm_mon_ro_stickyprbs_Mask                                                      cBit0
#define cAf6_lostbit_pen_tdm_mon_ro_stickyprbs_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x8_1000-0xB_5A7F
Reg Formula: 0x8_1000 + $Oc96Slice*65536 + $Oc48Slice*16384 + $Pwid
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2687): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice
HDL_PATH  : ipwcore.ipwslcore.isl48core[$Oc96Slice*2 + $Oc48Slice].irtlpla.idlecasctrl.membuf.ram.ram[$Pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_cas_idle_pat_ctrl_Base                                                             0x81000
#define cAf6Reg_pla_cas_idle_pat_ctrl_WidthVal                                                              64

/*--------------------------------------
BitField Name: CasIdlePatEn
BitField Type: RW
BitField Desc: enable idle pattern for CAS when AIS
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_cas_idle_pat_ctrl_CasIdlePatEn_Mask                                                cBit12
#define cAf6_pla_cas_idle_pat_ctrl_CasIdlePatEn_Shift                                                   12

/*--------------------------------------
BitField Name: CasIdlePattern
BitField Type: RW
BitField Desc: idle pattern for CAS when AIS
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pla_cas_idle_pat_ctrl_CasIdlePattern_Mask                                                cBit11_8
#define cAf6_pla_cas_idle_pat_ctrl_CasIdlePattern_Shift                                                      8

/*--------------------------------------
BitField Name: DatIdlePattern
BitField Type: RW
BitField Desc: idle pattern for data when AIS
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pla_cas_idle_pat_ctrl_DatIdlePattern_Mask                                                 cBit7_0
#define cAf6_pla_cas_idle_pat_ctrl_DatIdlePattern_Shift                                                      0

#endif /* _AF6_REG_AF6CNC0022_RD_PLA_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60291022ModulePwInternal.h
 *
 * Created Date: Jul 10, 2018
 *
 * Description : Internal Interface for Pw Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULEPWINTERNAL_H_
#define _THA60291022MODULEPWINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290022/pw/Tha60290022ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60291022ModulePw
    {
    tTha60290022ModulePw super;
    }tTha60291022ModulePw;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* _THA60291022MODULEPWINTERNAL_H_ */

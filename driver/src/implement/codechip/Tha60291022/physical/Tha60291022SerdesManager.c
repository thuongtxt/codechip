/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : physical
 *
 * File        : Tha60291022SerdesManager.c
 *
 * Created Date: Sep 11, 2018
 *
 * Description : 60291022 serdes manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha60291022Physical.h"
#include "Tha60291022SerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021SerdesManagerMethods m_Tha60290021SerdesManagerOverride;

/* Save super implementation */
static const tTha60290021SerdesManagerMethods *m_Tha60290021SerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController FaceplateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha60291022FaceplateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static ThaVersionReader VersionReader(AtSerdesManager self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool ShouldOpenFeature(Tha60290021SerdesManager self, uint32 fromVersion)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesManager)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= fromVersion) ? cAtTrue : cAtFalse;
    }

static eBool SerdesEyeScanResetIsSupported(Tha60290021SerdesManager self)
    {
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x4, 0x0000);
    return ShouldOpenFeature(self, startVersion);
    }

static void OverrideTha60290021SerdesManager(AtSerdesManager self)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SerdesManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha60290021SerdesManagerOverride));

        mMethodOverride(m_Tha60290021SerdesManagerOverride, FaceplateSerdesControllerObjectCreate);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, SerdesEyeScanResetIsSupported);
        }

    mMethodsSet(manager, &m_Tha60290021SerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideTha60290021SerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022SerdesManager);
    }

static AtSerdesManager ObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha60291022SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

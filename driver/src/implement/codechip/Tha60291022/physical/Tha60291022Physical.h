/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : physical
 * 
 * File        : Tha60291022Physical.h
 * 
 * Created Date: Sep 11, 2018
 *
 * Description : interface of the 60291022 physical module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022PHYSICAL_H_
#define _THA60291022PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtClasses.h"
#include "AtChannelClasses.h"
#include "AtPhysicalClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60291022SerdesManagerNew(AtDevice device);

/* SERDES controller */
AtSerdesController Tha60291022FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291022PHYSICAL_H_ */


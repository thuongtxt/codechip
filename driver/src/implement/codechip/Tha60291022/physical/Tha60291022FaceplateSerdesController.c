/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : physical
 *
 * File        : Tha60291022FaceplateSerdesController.c
 *
 * Created Date: Sep 11, 2018
 *
 * Description : 60291022 faceplate serdes controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022FaceplateSerdesControllerInternal.h"
#include "Tha60291022Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods               m_AtSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    uint32 portId = AtSerdesControllerHwIdGet(self);

    if ((mode == cAtSerdesModeEth10G) || (mode == cAtSerdesModeStm64))
        {
        if (portId == 0)
            return cAtTrue;

        return cAtFalse;
        }

    if (mode == cAtSerdesModeStm16)
        {
        if ((portId == 0) || (portId == 4) || (portId == 8) || (portId == 12))
            return cAtTrue;

        return cAtFalse;
        }

    switch ((uint32) mode)
        {
        case cAtSerdesModeStm4    : return cAtTrue;
        case cAtSerdesModeStm0    : return cAtTrue;
        case cAtSerdesModeStm1    : return cAtTrue;
        case cAtSerdesModeEth100M : return cAtTrue;
        case cAtSerdesModeEth1G   : return cAtTrue;

        case cAtSerdesModeEth2500M: return cAtFalse;
        case cAtSerdesModeEth40G  : return cAtFalse;
        case cAtSerdesModeOcn     : return cAtFalse;
        case cAtSerdesModeGe      : return cAtFalse;
        case cAtSerdesModeEth10M  : return cAtFalse;
        case cAtSerdesModeUnknown : return cAtFalse;

        default:
            return cAtFalse;
        }
    }

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    uint32 portId = AtSerdesControllerHwIdGet(self);

    if ((portId == 0) || (portId == 4) || (portId == 8) || (portId == 12))
        return cAtSerdesModeStm16;

    return cAtSerdesModeStm4;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022FaceplateSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60291022FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

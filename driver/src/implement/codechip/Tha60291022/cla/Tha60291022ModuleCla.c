/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60291022ModuleCla.c
 *
 * Created Date: Jul 24, 2018
 *
 * Description : 60291022 CLA implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegCasLen_Mask  cBit31_28
#define cRegCasLen_Shift 28

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods     m_ThaModuleClaOverride;

/* Save Super Implementation */
static const tThaModuleClaMethods *m_ThaModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CESoPCASLengthSet(ThaModuleCla self, AtPw pw, uint8 casLength)
    {
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    uint32 address = ThaModuleClaPwV2ClaPwTypeCtrlReg((ThaModuleClaPwV2)self) + pwId;
    uint32 longReg[cThaLongRegMaxSize];
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtIpCore core = AtDeviceIpCoreGet(device, 0);

    mModuleHwLongRead(self, address, longReg, cThaLongRegMaxSize, core);
    mRegFieldSet(longReg[1], cRegCasLen_, casLength);
    mModuleHwLongWrite(self, address, longReg, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, m_ThaModuleClaMethods, sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, CESoPCASLengthSet);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModuleCla);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleClaV3ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

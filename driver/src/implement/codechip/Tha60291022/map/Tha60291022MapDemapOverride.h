/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60291022MapDemapOverride.h
 * 
 * Created Date: Jul 30, 2018
 *
 * Description : Common code for 60291022 MAP and 60291022 DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MAPDEMAPOVERRIDE_H_
#define _THA60291022MAPDEMAPOVERRIDE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "Tha60291022MapDemap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
static tThaModuleAbstractMapMethods   m_ThaModuleAbstractMapOverride;

static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

static eAtRet TimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    return Tha60291022ModuleMapDemapHwTimeslotInfoSet(self, channel, timeslot, address, slotInfo);
    }

static eAtRet TimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    return Tha60291022ModuleMapDemapHwTimeslotInfoGet(self, channel, timeslot, address, slotInfo);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoGet);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MAPDEMAPOVERRIDE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60291022MapDemap.h
 * 
 * Created Date: Jul 30, 2018
 *
 * Description : Interface of 60291022 MapDemap.c
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MAPDEMAP_H_
#define _THA60291022MAPDEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleAbstractMap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60291022ModuleMapDemapHwTimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo);
eAtRet Tha60291022ModuleMapDemapHwTimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MAPDEMAP_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEMAP
 * 
 * File        : Tha60291022ModuleDemapInternal.h
 * 
 * Created Date: Jul 30, 2018
 *
 * Description : Internal data for 60291022 module DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULEDEMAPINTERNAL_H_
#define _THA60291022MODULEDEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/map/Tha60290022ModuleDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291022ModuleDemap
    {
    tTha60290022ModuleDemap super;
    }tTha60291022ModuleDemap;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MODULEDEMAPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60291022ModuleMap.c
 *
 * Created Date: Jul 30, 2018
 *
 * Description : Implementation for 60291022 module MAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022ModuleMapInternal.h"
#include "Tha60291022ModuleMapDemapReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleMapMethods         m_ThaModuleMapOverride;

/* Save super implementation */
static const tThaModuleMapMethods         *m_ThaModuleMapMethods         = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 MapChnTypeMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapChannelType0_Mask;
    }

static uint8  MapChnTypeShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapChannelType0_Shift;
    }

static uint32 MapFirstTsMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapFirstTs0_Mask;
    }

static uint8  MapFirstTsShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapFirstTs0_Shift;
    }

static uint32 MapTsEnMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapPwEn0_Mask;
    }

static uint8  MapTsEnShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapPwEn0_Shift;
    }

static uint32 MapPWIdFieldMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapPWIdField0_Mask;
    }

static uint8  MapPWIdFieldShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_mapPWIdField0_Shift;
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleMapOverride));

        mMethodOverride(m_ThaModuleMapOverride, MapChnTypeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapChnTypeShift);
        mMethodOverride(m_ThaModuleMapOverride, MapFirstTsMask);
        mMethodOverride(m_ThaModuleMapOverride, MapFirstTsShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTsEnMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTsEnShift);
        mMethodOverride(m_ThaModuleMapOverride, MapPWIdFieldMask);
        mMethodOverride(m_ThaModuleMapOverride, MapPWIdFieldShift);
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

#include "Tha60291022MapDemapOverride.h"

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

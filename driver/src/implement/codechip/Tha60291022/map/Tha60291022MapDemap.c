/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP/DEMAP
 *
 * File        : Tha60291022MapDemap.c
 *
 * Created Date: Jul 30, 2018
 *
 * Description : common implementation for 60291022 MAP and 60291022 DEMAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60291022MapDemap.h"
#include "Tha60291022ModuleMapDemapReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumTimeslotPerRegs 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eAtRet Tha60291022ModuleMapDemapHwTimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    uint32 localTsId = timeslot % cNumTimeslotPerRegs;
    uint32 longReg[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longReg, cThaLongRegMaxSize, AtModuleTypeGet((AtModule)self));

    switch (localTsId)
        {
        case 0:
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapChannelType0_, slotInfo->chnType);
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapFirstTs0_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapPwEn0_, slotInfo->enabled);
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapPWIdField0_, slotInfo->pwId);
            break;

        case 1:
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapChannelType1_01_, slotInfo->chnType & cBit1_0);
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapChannelType1_02_, ((slotInfo->chnType & cBit2) >> 2));
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapFirstTs1_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapPwEn1_, slotInfo->enabled);
            mRegFieldSet(longReg[0], cAf6_demap_channel_ctrl_DemapPWIdField1_, slotInfo->pwId);
            break;

        case 2:
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapChannelType2_, slotInfo->chnType);
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapFirstTs2_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapPwEn2_, slotInfo->enabled);
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapPWIdField2_, slotInfo->pwId);
            break;

        case 3:
            mRegFieldSet(longReg[2], cAf6_demap_channel_ctrl_DemapChannelType3_, slotInfo->chnType);
            mRegFieldSet(longReg[2], cAf6_demap_channel_ctrl_DemapFirstTs3_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapPwEn3_, slotInfo->enabled);
            mRegFieldSet(longReg[1], cAf6_demap_channel_ctrl_DemapPWIdField3_, slotInfo->pwId);
            break;

        default:
            AtChannelLog(channel, cAtLogLevelCritical, AtSourceLocation, "Unknown local index %d\r\n", localTsId);
            break;
        }

    mChannelHwLongWrite(channel, address, longReg, cThaLongRegMaxSize, AtModuleTypeGet((AtModule)self));

    return cAtOk;
    }

eAtRet Tha60291022ModuleMapDemapHwTimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    uint32 localTsId = timeslot % cNumTimeslotPerRegs;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 channelType02 = 0;

    mChannelHwLongRead(channel, address, longReg, cThaLongRegMaxSize, AtModuleTypeGet((AtModule)self));

    switch (localTsId)
        {
        case 0:
            slotInfo->chnType = (uint8)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapChannelType0_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapFirstTs0_);
            slotInfo->enabled = (uint8)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapPwEn0_);
            slotInfo->pwId = (uint16)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapPWIdField0_);
            break;

        case 1:
            slotInfo->chnType = (uint8)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapChannelType1_01_);
            channelType02 = mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapChannelType1_02_);
            slotInfo->chnType = (uint8)(slotInfo->chnType + (uint8)(cBit2 * channelType02));
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapFirstTs1_);
            slotInfo->enabled = (uint8)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapPwEn1_);
            slotInfo->pwId = (uint16)mRegField(longReg[0], cAf6_demap_channel_ctrl_DemapPWIdField1_);
            break;

        case 2:
            slotInfo->chnType = (uint8)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapChannelType2_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapFirstTs2_);
            slotInfo->enabled = (uint8)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapPwEn2_);
            slotInfo->pwId = (uint16)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapPWIdField2_);
            break;

        case 3:
            slotInfo->chnType = (uint8)mRegField(longReg[2], cAf6_demap_channel_ctrl_DemapChannelType3_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[2], cAf6_demap_channel_ctrl_DemapFirstTs3_);
            slotInfo->enabled = (uint8)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapPwEn3_);
            slotInfo->pwId = (uint16)mRegField(longReg[1], cAf6_demap_channel_ctrl_DemapPWIdField3_);
            break;

        default:
            AtChannelLog(channel, cAtLogLevelCritical, AtSourceLocation, "Unknown local index %d\r\n", localTsId);
            break;
        }

    return cAtOk;
    }

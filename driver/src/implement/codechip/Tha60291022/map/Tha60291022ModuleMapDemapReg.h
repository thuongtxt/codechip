/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_MAP_H_
#define _AF6_REG_AF6CNC0022_RD_MAP_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : MAP CPU  Reg Hold Control
Reg Addr   : 0x00F000-0x00F002
Reg Formula: 0x00F000 + HoldId
    Where  : 
           + $HoldId(0-2): Hold register
Reg Desc   : 
The register provides hold register for three word 32-bits MSB when CPU access to engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_cpu_hold_ctrl_Base                                                                0x00F000

/*--------------------------------------
BitField Name: HoldReg0
BitField Type: RW
BitField Desc: Hold 32 bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_map_cpu_hold_ctrl_HoldReg0_Mask                                                          cBit31_0
#define cAf6_map_cpu_hold_ctrl_HoldReg0_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Demap Channel Control
Reg Addr   : 0x000000-0x001FFF
Reg Formula: 0x000000 + 168*stsid + 24*vtgid + 6*vtid + slotid
    Where  : 
           + $stsid(0-47): is STS identification number)
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
           + $slotid(0-5): is time slot number. It varies from 0 to 5 in DS1 and from 0 to 7 in E1
Reg Desc   : 
The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.

------------------------------------------------------------------------------*/
#define cAf6Reg_demap_channel_ctrl_Base                                                               0x000000
#define cAf6Reg_demap_channel_ctrl_WidthVal                                                                 96

/*--------------------------------------
BitField Name: DemapFirstTs3
BitField Type: RW
BitField Desc:
BitField Bits: [67]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapFirstTs3_Mask                                                       cBit3
#define cAf6_demap_channel_ctrl_DemapFirstTs3_Shift                                                          3

/*--------------------------------------
BitField Name: DemapChannelType3
BitField Type: RW
BitField Desc:
BitField Bits: [66:64]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChannelType3_Mask                                                 cBit2_0
#define cAf6_demap_channel_ctrl_DemapChannelType3_Shift                                                      0

/*--------------------------------------
BitField Name: DemapPwEn3
BitField Type: RW
BitField Desc:
BitField Bits: [63]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPwEn3_Mask                                                         cBit31
#define cAf6_demap_channel_ctrl_DemapPwEn3_Shift                                                            31

/*--------------------------------------
BitField Name: DemapPWIdField3
BitField Type: RW
BitField Desc:
BitField Bits: [62:51]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPWIdField3_Mask                                                 cBit30_19
#define cAf6_demap_channel_ctrl_DemapPWIdField3_Shift                                                       19

/*--------------------------------------
BitField Name: DemapFirstTs2
BitField Type: RW
BitField Desc:
BitField Bits: [50]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapFirstTs2_Mask                                                      cBit18
#define cAf6_demap_channel_ctrl_DemapFirstTs2_Shift                                                         18

/*--------------------------------------
BitField Name: DemapChannelType2
BitField Type: RW
BitField Desc:
BitField Bits: [49:47]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChannelType2_Mask                                               cBit17_15
#define cAf6_demap_channel_ctrl_DemapChannelType2_Shift                                                     15

/*--------------------------------------
BitField Name: DemapPwEn2
BitField Type: RW
BitField Desc:
BitField Bits: [46]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPwEn2_Mask                                                         cBit14
#define cAf6_demap_channel_ctrl_DemapPwEn2_Shift                                                            14

/*--------------------------------------
BitField Name: DemapPWIdField2
BitField Type: RW
BitField Desc:
BitField Bits: [45:34]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPWIdField2_Mask                                                  cBit13_2
#define cAf6_demap_channel_ctrl_DemapPWIdField2_Shift                                                        2

/*--------------------------------------
BitField Name: DemapFirstTs1
BitField Type: RW
BitField Desc:
BitField Bits: [33]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapFirstTs1_Mask                                                       cBit1
#define cAf6_demap_channel_ctrl_DemapFirstTs1_Shift                                                          1

/*--------------------------------------
BitField Name: DemapChannelType1
BitField Type: RW
BitField Desc:
BitField Bits: [32:30]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChannelType1_01_Mask                                            cBit31_30
#define cAf6_demap_channel_ctrl_DemapChannelType1_01_Shift                                                  30
#define cAf6_demap_channel_ctrl_DemapChannelType1_02_Mask                                                cBit0
#define cAf6_demap_channel_ctrl_DemapChannelType1_02_Shift                                                   0

/*--------------------------------------
BitField Name: DemapPwEn1
BitField Type: RW
BitField Desc:
BitField Bits: [29]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPwEn1_Mask                                                         cBit29
#define cAf6_demap_channel_ctrl_DemapPwEn1_Shift                                                            29

/*--------------------------------------
BitField Name: DemapPWIdField1
BitField Type: RW
BitField Desc:
BitField Bits: [28:17]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPWIdField1_Mask                                                 cBit28_17
#define cAf6_demap_channel_ctrl_DemapPWIdField1_Shift                                                       17

/*--------------------------------------
BitField Name: DemapFirstTs0
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only
BitField Bits: [16]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapFirstTs0_Mask                                                      cBit16
#define cAf6_demap_channel_ctrl_DemapFirstTs0_Shift                                                         16

/*--------------------------------------
BitField Name: DemapChannelType0
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP
encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over
DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5:
MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7:
VCAT/LCAS/PLCP
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChannelType0_Mask                                               cBit15_13
#define cAf6_demap_channel_ctrl_DemapChannelType0_Shift                                                     13

/*--------------------------------------
BitField Name: DemapPwEn0
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPwEn0_Mask                                                         cBit12
#define cAf6_demap_channel_ctrl_DemapPwEn0_Shift                                                            12

/*--------------------------------------
BitField Name: DemapPWIdField0
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPWIdField0_Mask                                                  cBit11_0
#define cAf6_demap_channel_ctrl_DemapPWIdField0_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Demap HO Channel Control
Reg Addr   : 0x8300
Reg Formula: 0x8300 + stsid
    Where  : 
           + $stsid(0-47): is STS in OC48 HDL_PATH     : rtldemap1.hodemap.demap_pw_cfg_ins.membuf.array.ram[$stsid]
Reg Desc   : 
The registers are used by the hardware to configure PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_demapho_channel_ctrl_Base                                                               0x8300

/*--------------------------------------
BitField Name: DemapPWIdField
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation
BitField Bits: [19:8]
--------------------------------------*/
#define cAf6_demapho_channel_ctrl_DemapPWIdField_Mask                                                 cBit19_8
#define cAf6_demapho_channel_ctrl_DemapPWIdField_Shift                                                       8

/*--------------------------------------
BitField Name: DemapChEn
BitField Type: RW
BitField Desc: 0:Disable,  1:Enable as HO path
BitField Bits: [6]
--------------------------------------*/
#define cAf6_demapho_channel_ctrl_DemapChEn_Mask                                                         cBit6
#define cAf6_demapho_channel_ctrl_DemapChEn_Shift                                                            6

/*--------------------------------------
BitField Name: DemapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: Unused 1: Unused 2: Unused 3: Unused 4: HDLC/PPP/LAPS over
DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP
encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP
BitField Bits: [5:3]
--------------------------------------*/
#define cAf6_demapho_channel_ctrl_DemapChannelType_Mask                                                cBit5_3
#define cAf6_demapho_channel_ctrl_DemapChannelType_Shift                                                     3

/*--------------------------------------
BitField Name: Demapsr_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [2]
--------------------------------------*/
#define cAf6_demapho_channel_ctrl_Demapsr_vc3n3c_Mask                                                    cBit2
#define cAf6_demapho_channel_ctrl_Demapsr_vc3n3c_Shift                                                       2

/*--------------------------------------
BitField Name: Demapsrctype
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [1]
--------------------------------------*/
#define cAf6_demapho_channel_ctrl_Demapsrctype_Mask                                                      cBit1
#define cAf6_demapho_channel_ctrl_Demapsrctype_Shift                                                         1

/*--------------------------------------
BitField Name: DemapPwEn
BitField Type: RW
BitField Desc: PW Channels Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_demapho_channel_ctrl_DemapPwEn_Mask                                                         cBit0
#define cAf6_demapho_channel_ctrl_DemapPwEn_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Map Line Control
Reg Addr   : 0x010000-0x0107FF
Reg Formula: 0x010000 + 32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-47): is STS identification number
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number HDL_PATH     : rtlmap1.map_src_cfg_ram.array.ram[32*stsid + 4*vtgid + vtid]
Reg Desc   : 
The registers provide the per line configurations for STS/VT/DS1/E1 line.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_line_ctrl_Base                                                                    0x010000

/*--------------------------------------
BitField Name: IDLEForce
BitField Type: RW
BitField Desc: Force IDLE Signal
BitField Bits: [29]
--------------------------------------*/
#define cAf6_map_line_ctrl_IDLEForce_Mask                                                               cBit29
#define cAf6_map_line_ctrl_IDLEForce_Shift                                                                  29

/*--------------------------------------
BitField Name: IDLECode
BitField Type: RW
BitField Desc: IDLECode pattern
BitField Bits: [28:21]
--------------------------------------*/
#define cAf6_map_line_ctrl_IDLECode_Mask                                                             cBit28_21
#define cAf6_map_line_ctrl_IDLECode_Shift                                                                   21

/*--------------------------------------
BitField Name: MapDs1LcEn
BitField Type: RW
BitField Desc: DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU
Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapDs1LcEn_Mask                                                           cBit20_18
#define cAf6_map_line_ctrl_MapDs1LcEn_Shift                                                                 18

/*--------------------------------------
BitField Name: MapFrameType
BitField Type: RW
BitField Desc: depend on MapSigType[3:0], the MapFrameType[1:0] bit field can
have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3
framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3
G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP
basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapFrameType_Mask                                                         cBit17_16
#define cAf6_map_line_ctrl_MapFrameType_Shift                                                               16

/*--------------------------------------
BitField Name: MapSigType
BitField Type: RW
BitField Desc: 0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6:
unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c 11: STS48c/VC4_16c 12: TU3
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapSigType_Mask                                                           cBit15_12
#define cAf6_map_line_ctrl_MapSigType_Shift                                                                 12

/*--------------------------------------
BitField Name: MapTimeSrcMaster
BitField Type: RW
BitField Desc: This bit is used to indicate the master timing or the master VC3
in VC4/VC4-Xc
BitField Bits: [11]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Mask                                                        cBit11
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Shift                                                           11

/*--------------------------------------
BitField Name: MapTimeSrcId
BitField Type: RW
BitField Desc: The reference line ID used for timing reference or the master VC3
ID in VC4/VC4-Xc
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcId_Mask                                                          cBit10_0
#define cAf6_map_line_ctrl_MapTimeSrcId_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : map Channel Control
Reg Addr   : 0x018000-0x019FFF
Reg Formula: 0x018000 + 168*stsid + 24*vtgid + 6*vtid + slotid
    Where  : 
           + $stsid(0-47): is STS identification number)
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
           + $slotid(0-5): is time slot number. It varies from 0 to 5 in DS1 and from 0 to 7 in E1
Reg Desc   : 
The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_channel_ctrl_Base                                                                 0x018000
#define cAf6Reg_map_channel_ctrl_WidthVal                                                                   96

/*--------------------------------------
BitField Name: mapFirstTs3
BitField Type: RW
BitField Desc:
BitField Bits: [67]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapFirstTs3_Mask                                                           cBit3
#define cAf6_map_channel_ctrl_mapFirstTs3_Shift                                                              3

/*--------------------------------------
BitField Name: mapChannelType3
BitField Type: RW
BitField Desc:
BitField Bits: [66:64]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapChannelType3_Mask                                                     cBit2_0
#define cAf6_map_channel_ctrl_mapChannelType3_Shift                                                          0

/*--------------------------------------
BitField Name: mapPwEn3
BitField Type: RW
BitField Desc:
BitField Bits: [63]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPwEn3_Mask                                                             cBit31
#define cAf6_map_channel_ctrl_mapPwEn3_Shift                                                                31

/*--------------------------------------
BitField Name: mapPWIdField3
BitField Type: RW
BitField Desc:
BitField Bits: [62:51]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPWIdField3_Mask                                                     cBit30_19
#define cAf6_map_channel_ctrl_mapPWIdField3_Shift                                                           19

/*--------------------------------------
BitField Name: mapFirstTs2
BitField Type: RW
BitField Desc:
BitField Bits: [50]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapFirstTs2_Mask                                                          cBit18
#define cAf6_map_channel_ctrl_mapFirstTs2_Shift                                                             18

/*--------------------------------------
BitField Name: mapChannelType2
BitField Type: RW
BitField Desc:
BitField Bits: [49:47]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapChannelType2_Mask                                                   cBit17_15
#define cAf6_map_channel_ctrl_mapChannelType2_Shift                                                         15

/*--------------------------------------
BitField Name: mapPwEn2
BitField Type: RW
BitField Desc:
BitField Bits: [46]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPwEn2_Mask                                                             cBit14
#define cAf6_map_channel_ctrl_mapPwEn2_Shift                                                                14

/*--------------------------------------
BitField Name: mapPWIdField2
BitField Type: RW
BitField Desc:
BitField Bits: [45:34]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPWIdField2_Mask                                                      cBit13_2
#define cAf6_map_channel_ctrl_mapPWIdField2_Shift                                                            2

/*--------------------------------------
BitField Name: mapFirstTs1
BitField Type: RW
BitField Desc:
BitField Bits: [33]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapFirstTs1_Mask                                                           cBit1
#define cAf6_map_channel_ctrl_mapFirstTs1_Shift                                                              1

/*--------------------------------------
BitField Name: mapChannelType1
BitField Type: RW
BitField Desc:
BitField Bits: [32:30]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapChannelType1_01_Mask                                                cBit31_30
#define cAf6_map_channel_ctrl_mapChannelType1_01_Shift                                                      30
#define cAf6_map_channel_ctrl_mapChannelType1_02_Mask                                                    cBit0
#define cAf6_map_channel_ctrl_mapChannelType1_02_Shift                                                       0

/*--------------------------------------
BitField Name: mapPwEn1
BitField Type: RW
BitField Desc:
BitField Bits: [29]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPwEn1_Mask                                                             cBit29
#define cAf6_map_channel_ctrl_mapPwEn1_Shift                                                                29

/*--------------------------------------
BitField Name: mapPWIdField1
BitField Type: RW
BitField Desc:
BitField Bits: [28:17]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPWIdField1_Mask                                                     cBit28_17
#define cAf6_map_channel_ctrl_mapPWIdField1_Shift                                                           17

/*--------------------------------------
BitField Name: mapFirstTs0
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only
BitField Bits: [16]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapFirstTs0_Mask                                                          cBit16
#define cAf6_map_channel_ctrl_mapFirstTs0_Shift                                                             16

/*--------------------------------------
BitField Name: mapChannelType0
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP
encapsulation from DS1/E1 2: CESoP with CAS 3: IMA over DS1/E1,
SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over
DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapChannelType0_Mask                                                   cBit15_13
#define cAf6_map_channel_ctrl_mapChannelType0_Shift                                                         13

/*--------------------------------------
BitField Name: mapPwEn0
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPwEn0_Mask                                                             cBit12
#define cAf6_map_channel_ctrl_mapPwEn0_Shift                                                                12

/*--------------------------------------
BitField Name: mapPWIdField0
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_map_channel_ctrl_mapPWIdField0_Mask                                                      cBit11_0
#define cAf6_map_channel_ctrl_mapPWIdField0_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Map IDLE Code
Reg Addr   : 0x010800
Reg Formula: 
    Where  : 
Reg Desc   : 
The registers provide the idle pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_map_idle_code_Base                                                                    0x010800
#define cAf6Reg_map_idle_code_WidthVal                                                                      32

/*--------------------------------------
BitField Name: IdleCode
BitField Type: RW
BitField Desc: Idle code
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_map_idle_code_IdleCode_Mask                                                               cBit7_0
#define cAf6_map_idle_code_IdleCode_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map Parity Force Control
Reg Addr   : 0x10808
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Force_Control_Base                                                      0x10808

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Line Control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Mask                                     cBit1
#define cAf6_RAM_Map_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Shift                                        1

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Channel Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Mask                                      cBit0
#define cAf6_RAM_Map_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map Parity Disable Control
Reg Addr   : 0x10809
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Disable_Control_Base                                                    0x10809

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Line Control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPLineCtrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPLineCtrl_ParErrDis_Shift                                       1

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Channel Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPChlCtrl_ParErrDis_Mask                                    cBit0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPChlCtrl_ParErrDis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map parity Error Sticky
Reg Addr   : 0x1080a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Error_Sticky_Base                                                       0x1080a

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "MAP Thalassa Map Line Control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPLineCtrl_ParErrStk_Mask                                      cBit1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPLineCtrl_ParErrStk_Shift                                         1

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "MAP Thalassa Map Channel Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPChlCtrl_ParErrStk_Mask                                       cBit0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPChlCtrl_ParErrStk_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap Parity Force Control
Reg Addr   : 0x08218
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Force_Control_Base                                                    0x08218

/*--------------------------------------
BitField Name: DeMAPChlCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "DeMAP Thalassa DeMap Channel
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_DeMAPChlCtrl_ParErrFrc_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Force_Control_DeMAPChlCtrl_ParErrFrc_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap Parity Disable Control
Reg Addr   : 0x08219
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Disable_Control_Base                                                  0x08219

/*--------------------------------------
BitField Name: DeMAPChlCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "DeMAP Thalassa DeMap Channel
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_DeMAPChlCtrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Disable_Control_DeMAPChlCtrl_ParErrDis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap parity Error Sticky
Reg Addr   : 0x0821a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Error_Sticky_Base                                                     0x0821a

/*--------------------------------------
BitField Name: DeMAPChlCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "DeMAP Thalassa DeMap Channel
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_DeMAPChlCtrl_ParErrStk_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_DeMAPChlCtrl_ParErrStk_Shift                                       0

#endif /* _AF6_REG_AF6CNC0022_RD_MAP_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60291022ModuleDemap.c
 *
 * Created Date: Jul 30, 2018
 *
 * Description : implementation for 60291022 module DEMAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022ModuleDemapInternal.h"
#include "Tha60291022ModuleMapDemapReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleDemapMethods                 m_ThaModuleDemapOverride;
static tTha60290022ModuleDemapMethods         m_Tha60290022ModuleDemapOverride;

/* Save super implementation */
static const tThaModuleDemapMethods               *m_ThaModuleDemapMethods = NULL;
static const tTha60290022ModuleDemapMethods       *m_Tha60290022ModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DmapChnTypeMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChannelType0_Mask;
    }

static uint8  DmapChnTypeShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChannelType0_Shift;
    }

static uint32 DmapFirstTsMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapFirstTs0_Mask;
    }

static uint8  DmapFirstTsShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapFirstTs0_Shift;
    }

static uint32 DmapTsEnMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapPwEn0_Mask;
    }

static uint8  DmapTsEnShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapPwEn0_Shift;
    }

static uint32 DmapPwIdMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapPWIdField0_Mask;
    }

static uint8  DmapPwIdShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapPWIdField0_Shift;
    }

static uint32 HoDemapPWIdFieldMask(Tha60290022ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapPWIdField_Mask;
    }

static uint32 HoDemapPWIdFieldShift(Tha60290022ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapPWIdField_Shift;
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_ThaModuleDemapOverride));

        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapPwIdMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapPwIdShift);
        }

    mMethodsSet(demapModule, &m_ThaModuleDemapOverride);
    }

static void OverrideTha60290022ModuleDemap(AtModule self)
    {
    Tha60290022ModuleDemap demapModule = (Tha60290022ModuleDemap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_Tha60290022ModuleDemapOverride));

        mMethodOverride(m_Tha60290022ModuleDemapOverride, HoDemapPWIdFieldMask);
        mMethodOverride(m_Tha60290022ModuleDemapOverride, HoDemapPWIdFieldShift);
        }

    mMethodsSet(demapModule, &m_Tha60290022ModuleDemapOverride);
    }

#include "Tha60291022MapDemapOverride.h"

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleDemap(self);
    OverrideTha60290022ModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModuleDemap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

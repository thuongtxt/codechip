/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60291022ModulePdh.c
 *
 * Created Date: Jul 25, 2018
 *
 * Description : 60291022 module PDH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60291011/pdh/Tha60291011ModulePdh.h"
#include "Tha60291022ModulePdhInternal.h"
#include "Tha60291022PdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhMethods         m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtModulePdhMethods *m_AtModulePdhMethods = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CasSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 TimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    return Tha60291011ModulePdhTimeslotDefaultOffset(self, de1);
    }

static AtPdhNxDS0 NxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
    AtUnused(self);
    return Tha60291022PdhNxDs0New(de1, timeslotBitmap);
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 4;
    }

static eBool  De1RxSlipBufferIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 RxSlipBufferCounterRoOffset(ThaModulePdh self, eBool r2c)
    {
    AtUnused(self);
    if (r2c)
        return 0;
    return 2048;
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, CasSupported);
        mMethodOverride(m_ThaModulePdhOverride, TimeslotDefaultOffset);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        mMethodOverride(m_ThaModulePdhOverride, De1RxSlipBufferIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, RxSlipBufferCounterRoOffset);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, NxDs0Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePdhV3ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60291022ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60291022PdhNxDs0Internal.h
 * 
 * Created Date: Oct 11, 2018
 *
 * Description : 60291022 PDH NxDs0 internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022PDHNXDS0INTERNAL_H_
#define _THA60291022PDHNXDS0INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60291011/pdh/Tha60291011PdhNxDs0Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291022PdhNxDs0
    {
    tTha60291011PdhNxDs0 super;
    }tTha60291022PdhNxDs0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291022PDHNXDS0INTERNAL_H_ */


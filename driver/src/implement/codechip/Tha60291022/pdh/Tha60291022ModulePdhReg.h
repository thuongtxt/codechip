/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PDH_v3_H_
#define _AF6_REG_AF6CNC0022_RD_PDH_v3_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : PDH Global Registers
Reg Addr   : 0x00000000 - 0x00000000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the PDH

------------------------------------------------------------------------------*/
#define cAf6Reg_glb_Base                                                                            0x00000000
#define cAf6Reg_glb_WidthVal                                                                                32

/*--------------------------------------
BitField Name: PDHE13IdConv
BitField Type: RW
BitField Desc: Set 1 to enable ID conversion of the E1 in an E3 as following ID
in an E3 (bit 4:0) ID at Data Map (bit 4:0) 5'd0                  5'd0 5'd1
5'd1 5'd2                  5'd2 5'd3                  5'd4 5'd4
5'd5 5'd5                  5'd6 5'd6                  5'd8 5'd7
5'd9 5'd8                  5'd10 5'd9                  5'd12 5'd10
5'd13 5'd11                 5'd14 5'd12                 5'd16 5'd13
5'd17 5'd14                 5'd18 5'd15                 5'd20
BitField Bits: [9]
--------------------------------------*/
#define cAf6_glb_PDHE13IdConv_Mask                                                                       cBit9
#define cAf6_glb_PDHE13IdConv_Shift                                                                          9

/*--------------------------------------
BitField Name: PDHFullSonetSDH
BitField Type: RW
BitField Desc: Set 1 to enable full SONET/SDH mode (no traffic on PDH interface)
BitField Bits: [8]
--------------------------------------*/
#define cAf6_glb_PDHFullSonetSDH_Mask                                                                    cBit8
#define cAf6_glb_PDHFullSonetSDH_Shift                                                                       8

/*--------------------------------------
BitField Name: PDHFullLineLoop
BitField Type: RW
BitField Desc: Set 1 to enable full Line Loop Back at Data map side
BitField Bits: [7]
--------------------------------------*/
#define cAf6_glb_PDHFullLineLoop_Mask                                                                    cBit7
#define cAf6_glb_PDHFullLineLoop_Shift                                                                       7

/*--------------------------------------
BitField Name: PDHFullPayLoop
BitField Type: RW
BitField Desc: Set 1 to enable full Payload Loop back at Data map side
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glb_PDHFullPayLoop_Mask                                                                     cBit6
#define cAf6_glb_PDHFullPayLoop_Shift                                                                        6

/*--------------------------------------
BitField Name: PDHFullPDHBus
BitField Type: RW
BitField Desc: Set 1 to enable Full traffic on PDH Bus
BitField Bits: [5]
--------------------------------------*/
#define cAf6_glb_PDHFullPDHBus_Mask                                                                      cBit5
#define cAf6_glb_PDHFullPDHBus_Shift                                                                         5

/*--------------------------------------
BitField Name: PDHParallelPDHMd
BitField Type: RW
BitField Desc: Set 1 to enable Parallel PDH Bus mode
BitField Bits: [4]
--------------------------------------*/
#define cAf6_glb_PDHParallelPDHMd_Mask                                                                   cBit4
#define cAf6_glb_PDHParallelPDHMd_Shift                                                                      4

/*--------------------------------------
BitField Name: PDHSaturation
BitField Type: RW
BitField Desc: Set 1 to enable Saturation mode of all PDH counters. Set 0 for
Roll-Over mode
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glb_PDHSaturation_Mask                                                                      cBit3
#define cAf6_glb_PDHSaturation_Shift                                                                         3

/*--------------------------------------
BitField Name: PDHReservedBit
BitField Type: RW
BitField Desc: Transmit Reserved Bit
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glb_PDHReservedBit_Mask                                                                     cBit2
#define cAf6_glb_PDHReservedBit_Shift                                                                        2

/*--------------------------------------
BitField Name: PDHStufftBit
BitField Type: RW
BitField Desc: Transmit Stuffing Bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glb_PDHStufftBit_Mask                                                                       cBit1
#define cAf6_glb_PDHStufftBit_Shift                                                                          1

/*--------------------------------------
BitField Name: PDHNoIDConv
BitField Type: RW
BitField Desc: Set 1 to enable no ID convert at PDH Block
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glb_PDHNoIDConv_Mask                                                                        cBit0
#define cAf6_glb_PDHNoIDConv_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap Control
Reg Addr   : 0x00024000 - 0x000247FF
Reg Formula: 0x00024000 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-47):
           + $vtgid(0-6):
           + $vtid(0-3) HDL_PATH     : ctrlbuf.array.ram[$stsid*32 + $vtgid*4 + $vtid]
Reg Desc   : 
The STS/VT Demap Control is use to configure for per channel STS/VT demap operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_ctrl_Base                                                               0x00024000

/*--------------------------------------
BitField Name: Pdhoversdhmd
BitField Type: RW
BitField Desc: Set 1 to PDH LIU over OCN
BitField Bits: [9]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_Pdhoversdhmd_Mask                                                          cBit9
#define cAf6_stsvt_demap_ctrl_Pdhoversdhmd_Shift                                                             9

/*--------------------------------------
BitField Name: StsVtDemapFrcAis
BitField Type: RW
BitField Desc: This bit is set to 1 to force the AIS to downstream data. 1:
force AIS. 0: not force AIS.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_Mask                                                      cBit8
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrcAis_Shift                                                         8

/*--------------------------------------
BitField Name: StsVtDemapAutoAis
BitField Type: RW
BitField Desc: This bit is set to 1 to automatically generate AIS to downstream
data whenever the high-order STS/VT is in AIS. 1: auto AIS generation. 0: not
auto AIS generation
BitField Bits: [7]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_Mask                                                     cBit7
#define cAf6_stsvt_demap_ctrl_StsVtDemapAutoAis_Shift                                                        7

/*--------------------------------------
BitField Name: StsVtDemapCepMode
BitField Type: RW
BitField Desc: STS/VC CEP fractional mode
BitField Bits: [6]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_Mask                                                     cBit6
#define cAf6_stsvt_demap_ctrl_StsVtDemapCepMode_Shift                                                        6

/*--------------------------------------
BitField Name: StsVtDemapFrmtype
BitField Type: RW
BitField Desc: STS/VT Frmtype configure
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_Mask                                                   cBit5_4
#define cAf6_stsvt_demap_ctrl_StsVtDemapFrmtype_Shift                                                        4

/*--------------------------------------
BitField Name: StsVtDemapSigtype
BitField Type: RW
BitField Desc: STS/VT Sigtype configure Sigtype[3:0] Frmtype[1:0] CepMode
Operation Mode 0 x x VT/TU to DS1 Demap 1 x x VT/TU to E1 Demap 2 x 0 STS1/VC3
to DS3 Demap 2 x 1 TUG3/TU3 to DS3 Demap 3 x 0 STS1/VC3 to E3 Demap 3 x 1
TUG3/TU3 to E3 Demap 4 0 x Packet over VT1.5/TU11 4 1 x Reserved 4 2 x Reserved
4 3 x VT1.5/TU11 Basic CEP 5 0 x Packet over VT2/TU12 5 1 x Reserved 5 2 x
Reserved 5 3 x VT2/TU12 Basic CEP 6 x x Reserved 7 x x Reserved 8 0 x Packet
over STS1/VC3 8 1 0 STS1/VC3 Fractional CEP carrying VT2/TU12 8 1 1 STS1/VC3
Fractional CEP carrying VT15/TU11 8 2 0 STS1/VC3 Fractional CEP carrying E3 8 2
1 STS1/VC3 Fractional CEP carrying DS3 8 3 x STS1/VC3 Basic CEP 9 0 x Packet
over STS3c/VC4 9 1 0 STS3c/VC4 Fractional CEP carrying VT2/TU12 9 1 1 STS3c/VC4
Fractional CEP carrying VT15/TU11 9 2 0 STS3c/VC4 Fractional CEP carrying E3 9 2
1 STS3c/VC4 Fractional CEP carrying DS3 9 3 x STS3c/VC4 Basic CEP 10 3 x
STS12c/VC4_4c Basic CEP 11 x x Reserved 12 x x Packet over TU3 13 x x Reserved
14 x x Reserved 15 x x Disable STS/VC/VT/TU/DS1/E1/DS3/E3
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_Mask                                                   cBit3_0
#define cAf6_stsvt_demap_ctrl_StsVtDemapSigtype_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Demap HW Status
Reg Addr   : 0x00024800 - 0x00024FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_demap_hw_stat_Base                                                            0x00024800
#define cAf6Reg_stsvt_demap_hw_stat_WidthVal                                                                32

/*--------------------------------------
BitField Name: STSVTDemapStatus
BitField Type: RO
BitField Desc: for HW debug only
BitField Bits: [26:0]
--------------------------------------*/
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_Mask                                                cBit26_0
#define cAf6_stsvt_demap_hw_stat_STSVTDemapStatus_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PDH Rx Per STS/VC Payload Control
Reg Addr   : 0x00030080 - 0x000300BF
Reg Formula: 0x00030080 +  stsid
    Where  : 
           + $stsid(0-47): HDL_PATH     : pdhrxmux.vtcnt.stsctrlbuf.array.ram[$stsid]
Reg Desc   : 
The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_stsvc_payload_ctrl_Base                                                          0x00030080
#define cAf6Reg_rx_stsvc_payload_ctrl_WidthVal                                                              32

/*--------------------------------------
BitField Name: PDHVtRxMapTug26Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2
type 2: reserved 3: VC/STS/DS3/E3
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Mask                                          cBit13_12
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Shift                                                12

/*--------------------------------------
BitField Name: PDHVtRxMapTug25Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-5
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Mask                                          cBit11_10
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Shift                                                10

/*--------------------------------------
BitField Name: PDHVtRxMapTug24Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-4
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Mask                                            cBit9_8
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Shift                                                 8

/*--------------------------------------
BitField Name: PDHVtRxMapTug23Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-3
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Mask                                            cBit7_6
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Shift                                                 6

/*--------------------------------------
BitField Name: PDHVtRxMapTug22Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-2
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Mask                                            cBit5_4
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Shift                                                 4

/*--------------------------------------
BitField Name: PDHVtRxMapTug21Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-1
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Mask                                            cBit3_2
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Shift                                                 2

/*--------------------------------------
BitField Name: PDHVtRxMapTug20Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-0
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Mask                                            cBit1_0
#define cAf6_rx_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Control
Reg Addr   : 0x00040000 - 0x0004003F
Reg Formula: 0x00040000 +  de3id
    Where  : 
           + $de3id(0-47): HDL_PATH     : rxm13e13.rxm23e23.ctrlbufarb.membuf.array.ram[$de3id]
Reg Desc   : 
The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_ctrl_Base                                                                  0x00040000

/*--------------------------------------
BitField Name: DE3AISAll1sFrwDis
BitField Type: RW
BitField Desc: Forward DE3 AIS all-1s to generate L-bit to PSN Disable, set 1 to
disable
BitField Bits: [22]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_DE3AISAll1sFrwDis_Mask                                                       cBit22
#define cAf6_rxm23e23_ctrl_DE3AISAll1sFrwDis_Shift                                                          22

/*--------------------------------------
BitField Name: DS3LOFFrwDis
BitField Type: RW
BitField Desc: Forward DS3 LOF to generate L-bit to PSN in case of MonOnly
Disable, set 1 to disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_DS3LOFFrwDis_Mask                                                            cBit21
#define cAf6_rxm23e23_ctrl_DS3LOFFrwDis_Shift                                                               21

/*--------------------------------------
BitField Name: DS3AISSigFrwEnb
BitField Type: RW
BitField Desc: Forward DS3 AIS signal to generate L-bit to PSN in case of
MonOnly Enable, set 1 to enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_DS3AISSigFrwEnb_Mask                                                         cBit20
#define cAf6_rxm23e23_ctrl_DS3AISSigFrwEnb_Shift                                                            20

/*--------------------------------------
BitField Name: RXDS3ForceAISLbit
BitField Type: RW
BitField Desc: Force DS3 AIS, L-bit to PSN, data all one to PSN for Payload/Line
Remote Loopback
BitField Bits: [19]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RXDS3ForceAISLbit_Mask                                                       cBit19
#define cAf6_rxm23e23_ctrl_RXDS3ForceAISLbit_Shift                                                          19

/*--------------------------------------
BitField Name: RxDE3MonOnly
BitField Type: RW
BitField Desc: Rx Framer Monitor only
BitField Bits: [18]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDE3MonOnly_Mask                                                            cBit18
#define cAf6_rxm23e23_ctrl_RxDE3MonOnly_Shift                                                               18

/*--------------------------------------
BitField Name: RxDS3E3ChEnb
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Mask                                                            cBit17
#define cAf6_rxm23e23_ctrl_RxDS3E3ChEnb_Shift                                                               17

/*--------------------------------------
BitField Name: RxDS3E3LosThres
BitField Type: RW
BitField Desc:
BitField Bits: [16:14]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Mask                                                      cBit16_14
#define cAf6_rxm23e23_ctrl_RxDS3E3LosThres_Shift                                                            14

/*--------------------------------------
BitField Name: RxDS3E3AisThres
BitField Type: RW
BitField Desc:
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Mask                                                      cBit13_11
#define cAf6_rxm23e23_ctrl_RxDS3E3AisThres_Shift                                                            11

/*--------------------------------------
BitField Name: RxDS3E3LofThres
BitField Type: RW
BitField Desc:
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Mask                                                       cBit10_8
#define cAf6_rxm23e23_ctrl_RxDS3E3LofThres_Shift                                                             8

/*--------------------------------------
BitField Name: RxDS3E3PayAllOne
BitField Type: RW
BitField Desc: This bit is set to 1 to force the DS3/E3 framer payload AIS
downstream
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Mask                                                         cBit7
#define cAf6_rxm23e23_ctrl_RxDS3E3PayAllOne_Shift                                                            7

/*--------------------------------------
BitField Name: RxDS3E3LineAllOne
BitField Type: RW
BitField Desc: This bit is set to 1 to force the DS3/E3 framer line AIS
downstream
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Mask                                                        cBit6
#define cAf6_rxm23e23_ctrl_RxDS3E3LineAllOne_Shift                                                           6

/*--------------------------------------
BitField Name: RxDS3E3LineLoop
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Mask                                                          cBit5
#define cAf6_rxm23e23_ctrl_RxDS3E3LineLoop_Shift                                                             5

/*--------------------------------------
BitField Name: RxDS3E3FrcLof
BitField Type: RW
BitField Desc: This bit is set to 1 to force the Rx DS3/E3 framer into LOF
status. 1: force LOF. 0: not force LOF.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Mask                                                            cBit4
#define cAf6_rxm23e23_ctrl_RxDS3E3FrcLof_Shift                                                               4

/*--------------------------------------
BitField Name: RxDS3E3Md
BitField Type: RW
BitField Desc: Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity
carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3
C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111:
E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Mask                                                              cBit3_0
#define cAf6_rxm23e23_ctrl_RxDS3E3Md_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 HW Status
Reg Addr   : 0x00040080-0x000400BF
Reg Formula: 0x00040080 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_hw_stat_Base                                                               0x00040080

/*--------------------------------------
BitField Name: RxDS3_CrrAIC
BitField Type: RW
BitField Desc: Current AIC Bit value
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAIC_Mask                                                          cBit6
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAIC_Shift                                                             6

/*--------------------------------------
BitField Name: RxDS3_CrrAISDownStr
BitField Type: RW
BitField Desc: AIS Down Stream due to HI_Level AIS of DS3E3
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISDownStr_Mask                                                   cBit5
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISDownStr_Shift                                                      5

/*--------------------------------------
BitField Name: RxDS3_CrrLosAllZero
BitField Type: RW
BitField Desc: LOS All zero of DS3E3
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrLosAllZero_Mask                                                   cBit4
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrLosAllZero_Shift                                                      4

/*--------------------------------------
BitField Name: RxDS3_CrrAISAllOne
BitField Type: RW
BitField Desc: AIS All one of DS3E3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISAllOne_Mask                                                    cBit3
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISAllOne_Shift                                                       3

/*--------------------------------------
BitField Name: RxDS3_CrrAISSignal
BitField Type: RW
BitField Desc: AIS signal of DS3
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISSignal_Mask                                                    cBit2
#define cAf6_rxm23e23_hw_stat_RxDS3_CrrAISSignal_Shift                                                       2

/*--------------------------------------
BitField Name: RxDS3E3State
BitField Type: RW
BitField Desc: 0:LOF, 1:Searching, 2: Verify, 3:InFrame
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm23e23_hw_stat_RxDS3E3State_Mask                                                        cBit1_0
#define cAf6_rxm23e23_hw_stat_RxDS3E3State_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro Control
Reg Addr   : 0x040840-0x04087F
Reg Formula: 0x040840 +  de3id
    Where  : 
           + $de3id(0-47): HDL_PATH     : rxm13e13.rxm23e23.ctrlbufarb.membuf.array.ram[$de3id]
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rx_de3_oh_ctrl_Base                                                                   0x040840
#define cAf6Reg_rx_de3_oh_ctrl_WidthVal                                                                     32

/*--------------------------------------
BitField Name: DLEn
BitField Type: RW
BitField Desc: DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_DLEn_Mask                                                                    cBit5
#define cAf6_rx_de3_oh_ctrl_DLEn_Shift                                                                       5

/*--------------------------------------
BitField Name: Ds3E3DLSL
BitField Type: RW
BitField Desc: 00: DL 01: UDL 10: FEAC 11: NA
BitField Bits: [4:3]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Mask                                                             cBit4_3
#define cAf6_rx_de3_oh_ctrl_Ds3E3DLSL_Shift                                                                  3

/*--------------------------------------
BitField Name: Ds3FEACEn
BitField Type: RW
BitField Desc: Enable detection FEAC state change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Mask                                                               cBit2
#define cAf6_rx_de3_oh_ctrl_Ds3FEACEn_Shift                                                                  2

/*--------------------------------------
BitField Name: FBEFbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts F-Bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Mask                                                            cBit1
#define cAf6_rx_de3_oh_ctrl_FBEFbitCntEn_Shift                                                               1

/*--------------------------------------
BitField Name: FBEMbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts M-Bit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Mask                                                            cBit0
#define cAf6_rx_de3_oh_ctrl_FBEMbitCntEn_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Rx DS3E3 SSM Invert
Reg Addr   : 0x04040B
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_invert_ssm_Base                                                                  0x04040B

/*--------------------------------------
BitField Name: SSMMessInv
BitField Type: RO
BitField Desc: SSM message Invert - 1: invert 11111111_11_0xxxxxx0(bit0-5) - 0:
non_invert 0xxxxxx0_11_11111111(bit5-0)
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_upen_invert_ssm_SSMMessInv_Mask                                                          cBit23_0
#define cAf6_upen_invert_ssm_SSMMessInv_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Rx DS3E3 SSM status
Reg Addr   : 0x040EC0
Reg Formula: 0x040EC0 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_ssm_Base                                                                     0x040EC0

/*--------------------------------------
BitField Name: SSMMessSta
BitField Type: RO
BitField Desc: SSM Status - 0: Seaching Status - 1: Checking Status - 2: Inframe
Status
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_sta_ssm_SSMMessSta_Mask                                                            cBit15_12
#define cAf6_upen_sta_ssm_SSMMessSta_Shift                                                                  12

/*--------------------------------------
BitField Name: SSMMessCnt
BitField Type: RO
BitField Desc: SSM message matching counter
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_upen_sta_ssm_SSMMessCnt_Mask                                                             cBit11_8
#define cAf6_upen_sta_ssm_SSMMessCnt_Shift                                                                   8

/*--------------------------------------
BitField Name: SSMMessVal
BitField Type: RO
BitField Desc: SSM message - [3:0]: SSM E3 G832 Message - [5:0]: SSM DS3 C-bit
Message
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_sta_ssm_SSMMessVal_Mask                                                              cBit7_0
#define cAf6_upen_sta_ssm_SSMMessVal_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro Threshold
Reg Addr   : 0x040801
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rxde3_oh_thrsh_cfg_Base                                                               0x040801

/*--------------------------------------
BitField Name: IdleThresDS3
BitField Type: RW
BitField Desc:
BitField Bits: [23:21]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_Mask                                                    cBit23_21
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_Shift                                                          21

/*--------------------------------------
BitField Name: AisThresDS3
BitField Type: RW
BitField Desc:
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_Mask                                                     cBit20_18
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_Shift                                                           18

/*--------------------------------------
BitField Name: TimingMakerE3G832
BitField Type: RW
BitField Desc:
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_Mask                                               cBit17_15
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_Shift                                                     15

/*--------------------------------------
BitField Name: NewPayTypeE3G832
BitField Type: RW
BitField Desc:
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_Mask                                                cBit14_12
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_Shift                                                      12

/*--------------------------------------
BitField Name: AICThres
BitField Type: RW
BitField Desc:
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_Mask                                                         cBit11_9
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_Shift                                                               9

/*--------------------------------------
BitField Name: RdiE3G832Thres
BitField Type: RW
BitField Desc:
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_Mask                                                    cBit8_6
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_Shift                                                         6

/*--------------------------------------
BitField Name: RdiE3G751Thres
BitField Type: RW
BitField Desc:
BitField Bits: [5:3]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_Mask                                                    cBit5_3
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_Shift                                                         3

/*--------------------------------------
BitField Name: RdiDS3Thres
BitField Type: RW
BitField Desc:
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_Mask                                                       cBit2_0
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer REI Threshold Control
Reg Addr   : 0x00040803
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_rei_thrsh_cfg_Base                                                         0x00040803
#define cAf6Reg_rxm23e23_rei_thrsh_cfg_WidthVal                                                             32

/*--------------------------------------
BitField Name: RxM23E23ReiThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the REI counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_Mask                                             cBit15_0
#define cAf6_rxm23e23_rei_thrsh_cfg_RxM23E23ReiThres_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer FBE Threshold Control
Reg Addr   : 0x00040804
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_fbe_thrsh_cfg_Base                                                         0x00040804
#define cAf6Reg_rxm23e23_fbe_thrsh_cfg_WidthVal                                                             32

/*--------------------------------------
BitField Name: RxM23E23FbeThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_Mask                                             cBit15_0
#define cAf6_rxm23e23_fbe_thrsh_cfg_RxM23E23FbeThres_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Parity Threshold Control
Reg Addr   : 0x00040805
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_parity_thrsh_cfg_Base                                                      0x00040805
#define cAf6Reg_rxm23e23_parity_thrsh_cfg_WidthVal                                                          32

/*--------------------------------------
BitField Name: RxM23E23ParThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the PAR counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_Mask                                          cBit15_0
#define cAf6_rxm23e23_parity_thrsh_cfg_RxM23E23ParThres_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Cbit Parity Threshold Control
Reg Addr   : 0x00040806
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS3/E3 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_cbit_parity_thrsh_cfg_Base                                                 0x00040806
#define cAf6Reg_rxm23e23_cbit_parity_thrsh_cfg_WidthVal                                                     32

/*--------------------------------------
BitField Name: RxM23E23CParThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the Cbit Parity counter exceed
this threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_Mask                                    cBit15_0
#define cAf6_rxm23e23_cbit_parity_thrsh_cfg_RxM23E23CParThres_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer REI Counter
Reg Addr   : 0x00040880 - 0x000408BF
Reg Formula: 0x00040880 +  64*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-47):)
Reg Desc   : 
This is the per channel REI counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_rei_cnt_Base                                                               0x00040880
#define cAf6Reg_rxm23e23_rei_cnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: RxM23E23ReiCnt
BitField Type: RO
BitField Desc: DS3/E3 REI error accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_Mask                                                     cBit15_0
#define cAf6_rxm23e23_rei_cnt_RxM23E23ReiCnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer FBE Counter
Reg Addr   : 0x00040900 - 0x0004093F
Reg Formula: 0x00040900 +  64*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel FBE counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_fbe_cnt_Base                                                               0x00040900
#define cAf6Reg_rxm23e23_fbe_cnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: count
BitField Type: RO
BitField Desc: frame FBE counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_fbe_cnt_count_Mask                                                              cBit15_0
#define cAf6_rxm23e23_fbe_cnt_count_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Parity Counter
Reg Addr   : 0x00040980 - 0x000409BF
Reg Formula: 0x00040980 +  64*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel Parity counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_parity_cnt_Base                                                            0x00040980
#define cAf6Reg_rxm23e23_parity_cnt_WidthVal                                                                32

/*--------------------------------------
BitField Name: RxM23E23ParCnt
BitField Type: RO
BitField Desc: DS3/E3 Parity error accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_Mask                                                  cBit15_0
#define cAf6_rxm23e23_parity_cnt_RxM23E23ParCnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer Cbit Parity Counter
Reg Addr   : 0x00040A00 - 0x00040A3F
Reg Formula: 0x00040A00 +  64*Rd2Clr + de3id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23):
Reg Desc   : 
This is the per channel Cbit Parity counter for DS3/E3 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_cbit_parity_cnt_Base                                                       0x00040A00
#define cAf6Reg_rxm23e23_cbit_parity_cnt_WidthVal                                                           32

/*--------------------------------------
BitField Name: RxM23E23CParCnt
BitField Type: RO
BitField Desc: DS3/E3 Cbit Parity error accumulator
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_Mask                                            cBit15_0
#define cAf6_rxm23e23_cbit_parity_cnt_RxM23E23CParCnt_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 FEAC Buffer
Reg Addr   : 0x040B40-0x040B7F
Reg Formula: 0x040B40 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rx_de3_feac_buffer_Base                                                               0x040B40
#define cAf6Reg_rx_de3_feac_buffer_WidthVal                                                                 32

/*--------------------------------------
BitField Name: FEAC_State
BitField Type: RW
BitField Desc: 00: idle codeword 01: active codeword 10: de-active codeword 11:
alarm/status codeword
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_rx_de3_feac_buffer_FEAC_State_Mask                                                        cBit7_6
#define cAf6_rx_de3_feac_buffer_FEAC_State_Shift                                                             6

/*--------------------------------------
BitField Name: FEAC_Codeword
BitField Type: RW
BitField Desc: 6 x-bits cw in format 111111110xxxxxx0
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_Mask                                                     cBit5_0
#define cAf6_rx_de3_feac_buffer_FEAC_Codeword_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00040D00-0x00040D3F
Reg Formula: 0x00040D00 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_per_chn_intr_cfg_Base                                                      0x00040D00

/*--------------------------------------
BitField Name: DE3BERTCA_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 BER TCA event to generate an
interrupt.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BERTCA_En_Mask                                                cBit15
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BERTCA_En_Shift                                                   15

/*--------------------------------------
BitField Name: DE3BERSD_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 BER SD event to generate an interrupt.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BERSD_En_Mask                                                 cBit14
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BERSD_En_Shift                                                    14

/*--------------------------------------
BitField Name: DE3BERSF_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 BER SF event to generate an interrupt.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BERSF_En_Mask                                                 cBit13
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3BERSF_En_Shift                                                    13

/*--------------------------------------
BitField Name: DE3LCVCnt_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 LCV counter event to generate an
interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LCVCnt_En_Mask                                                cBit12
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LCVCnt_En_Shift                                                   12

/*--------------------------------------
BitField Name: DS3CP_E3Tr_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 CP counter or E3G831 Trace event to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3CP_E3Tr_En_Mask                                               cBit11
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3CP_E3Tr_En_Shift                                                  11

/*--------------------------------------
BitField Name: DE3ParCnt_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 Parity counter event to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3ParCnt_En_Mask                                                cBit10
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3ParCnt_En_Shift                                                   10

/*--------------------------------------
BitField Name: DE3FBECnt_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 FBE counter event to generate an
interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3FBECnt_En_Mask                                                 cBit9
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3FBECnt_En_Shift                                                    9

/*--------------------------------------
BitField Name: DE3REICnt_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE3 REI counter event to generate an
interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3REICnt_En_Mask                                                 cBit8
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3REICnt_En_Shift                                                    8

/*--------------------------------------
BitField Name: DS3FEAC_E3SSM_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 FEAC or E3G832 SSM event to generate
an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_Mask                                             cBit7
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3FEAC_E3SSM_En_Shift                                                7

/*--------------------------------------
BitField Name: DS3AIC_E3TM_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 AIC or E3G832 TM event to generate an
interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_Mask                                               cBit6
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AIC_E3TM_En_Shift                                                  6

/*--------------------------------------
BitField Name: DS3IDLESig_E3PT_En
BitField Type: RW
BitField Desc: Set 1 to enable change DS3 IDLE signal or E3G832 PayloadType
event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_Mask                                           cBit5
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3IDLESig_E3PT_En_Shift                                              5

/*--------------------------------------
BitField Name: DE3AisAllOneIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS All One event to generate an
interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_Mask                                           cBit4
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3AisAllOneIntrEn_Shift                                              4

/*--------------------------------------
BitField Name: DE3LosAllZeroIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOS All Zero event to generate an
interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_Mask                                          cBit3
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LosAllZeroIntrEn_Shift                                             3

/*--------------------------------------
BitField Name: DS3AISSigIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS Signal event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_Mask                                              cBit2
#define cAf6_rxm23e23_per_chn_intr_cfg_DS3AISSigIntrEn_Shift                                                 2

/*--------------------------------------
BitField Name: DE3RDIIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_Mask                                                 cBit1
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3RDIIntrEn_Shift                                                    1

/*--------------------------------------
BitField Name: DE3LofIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_Mask                                                 cBit0
#define cAf6_rxm23e23_per_chn_intr_cfg_DE3LofIntrEn_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Interrupt Change Status
Reg Addr   : 0x00040D40-0x00040D7F
Reg Formula: 0x00040D40 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
This is the per Channel interrupt tus of DS3/E3 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_per_chn_intr_stat_Base                                                     0x00040D40

/*--------------------------------------
BitField Name: DE3BERTCA_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DE3 BER TCA event to generate an
interrupt.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3BERTCA_Intr_Mask                                             cBit15
#define cAf6_rxm23e23_per_chn_intr_stat_DE3BERTCA_Intr_Shift                                                15

/*--------------------------------------
BitField Name: DE3BERSD_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DE3 BER SD event to generate an
interrupt.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3BERSD_Intr_Mask                                              cBit14
#define cAf6_rxm23e23_per_chn_intr_stat_DE3BERSD_Intr_Shift                                                 14

/*--------------------------------------
BitField Name: DE3BERSF_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DE3 BER SF event to generate an
interrupt.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3BERSF_Intr_Mask                                              cBit13
#define cAf6_rxm23e23_per_chn_intr_stat_DE3BERSF_Intr_Shift                                                 13

/*--------------------------------------
BitField Name: DE3LCVCnt_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DE3 LCV counter event to generate an
interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LCVCnt_Intr_Mask                                             cBit12
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LCVCnt_Intr_Shift                                                12

/*--------------------------------------
BitField Name: DS3CP_E3Tr_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DS3 CP counter or E3G831 Trace event to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3CP_E3Tr_Intr_Mask                                            cBit11
#define cAf6_rxm23e23_per_chn_intr_stat_DS3CP_E3Tr_Intr_Shift                                               11

/*--------------------------------------
BitField Name: DE3ParCnt_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DE3 Parity counter event to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3ParCnt_Intr_Mask                                             cBit10
#define cAf6_rxm23e23_per_chn_intr_stat_DE3ParCnt_Intr_Shift                                                10

/*--------------------------------------
BitField Name: DE3FBECnt_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is change DE3 FBE counter event to generate an
interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3FBECnt_Intr_Mask                                              cBit9
#define cAf6_rxm23e23_per_chn_intr_stat_DE3FBECnt_Intr_Shift                                                 9

/*--------------------------------------
BitField Name: DE3REICnt_Intr
BitField Type: W1C
BitField Desc: Set 1 to enable change DE3 REI counter event to generate an
interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3REICnt_Intr_Mask                                              cBit8
#define cAf6_rxm23e23_per_chn_intr_stat_DE3REICnt_Intr_Shift                                                 8

/*--------------------------------------
BitField Name: DS3FEAC_E3SSM_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a change DS3 FEAC or E3G832 SSM event to
generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_Intr_Mask                                          cBit7
#define cAf6_rxm23e23_per_chn_intr_stat_DS3FEAC_E3SSM_Intr_Shift                                             7

/*--------------------------------------
BitField Name: DS3AIC_E3TM_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a change DS3 AIC or E3G832 TM event to generate
an interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_Intr_Mask                                            cBit6
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AIC_E3TM_Intr_Shift                                               6

/*--------------------------------------
BitField Name: DS3IDLESig_E3PT_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a change DS3 IDLE signal or E3G832 PayloadType
event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_Intr_Mask                                        cBit5
#define cAf6_rxm23e23_per_chn_intr_stat_DS3IDLESig_E3PT_Intr_Shift                                           5

/*--------------------------------------
BitField Name: DE3AisAllOne_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a change AIS All One event to generate an
interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOne_Intr_Mask                                           cBit4
#define cAf6_rxm23e23_per_chn_intr_stat_DE3AisAllOne_Intr_Shift                                              4

/*--------------------------------------
BitField Name: DE3LosAllZero_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a change LOS All Zero event to generate an
interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZero_Intr_Mask                                          cBit3
#define cAf6_rxm23e23_per_chn_intr_stat_DE3LosAllZero_Intr_Shift                                             3

/*--------------------------------------
BitField Name: DS3AISSig_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a change AIS Signal event to generate an
interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSig_Intr_Mask                                              cBit2
#define cAf6_rxm23e23_per_chn_intr_stat_DS3AISSig_Intr_Shift                                                 2

/*--------------------------------------
BitField Name: DE3RDI_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a  change RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDI_Intr_Mask                                                 cBit1
#define cAf6_rxm23e23_per_chn_intr_stat_DE3RDI_Intr_Shift                                                    1

/*--------------------------------------
BitField Name: DE3Lof_Intr
BitField Type: W1C
BitField Desc: Set 1 if there is a  change LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_intr_stat_DE3Lof_Intr_Mask                                                 cBit0
#define cAf6_rxm23e23_per_chn_intr_stat_DE3Lof_Intr_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Current Status
Reg Addr   : 0x00040D80-0x00040DBF
Reg Formula: 0x00040D80 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
This is the per Channel Current tus of DS3/E3 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm23e23_per_chn_curr_stat_Base                                                     0x00040D80

/*--------------------------------------
BitField Name: DE3BERTCA_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 BER TCA event to generate an interrupt.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3BERTCA_CurrSta_Mask                                          cBit15
#define cAf6_rxm23e23_per_chn_curr_stat_DE3BERTCA_CurrSta_Shift                                             15

/*--------------------------------------
BitField Name: DE3BERSD_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 BER SD event to generate an interrupt.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3BERSD_CurrSta_Mask                                           cBit14
#define cAf6_rxm23e23_per_chn_curr_stat_DE3BERSD_CurrSta_Shift                                              14

/*--------------------------------------
BitField Name: DE3BERSF_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 BER SF event to generate an interrupt.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3BERSF_CurrSta_Mask                                           cBit13
#define cAf6_rxm23e23_per_chn_curr_stat_DE3BERSF_CurrSta_Shift                                              13

/*--------------------------------------
BitField Name: DE3LCVCnt_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 LCV counter event to generate an interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LCVCnt_CurrSta_Mask                                          cBit12
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LCVCnt_CurrSta_Shift                                             12

/*--------------------------------------
BitField Name: DS3CP_E3Tr_CurrSta
BitField Type: RW
BitField Desc: Current status DS3 CP counter or E3G831 Trace event to generate
an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3CP_E3Tr_CurrSta_Mask                                         cBit11
#define cAf6_rxm23e23_per_chn_curr_stat_DS3CP_E3Tr_CurrSta_Shift                                            11

/*--------------------------------------
BitField Name: DE3ParCnt_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 Parity counter event to generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3ParCnt_CurrSta_Mask                                          cBit10
#define cAf6_rxm23e23_per_chn_curr_stat_DE3ParCnt_CurrSta_Shift                                             10

/*--------------------------------------
BitField Name: DE3FBECnt_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 FBE counter event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3FBECnt_CurrSta_Mask                                           cBit9
#define cAf6_rxm23e23_per_chn_curr_stat_DE3FBECnt_CurrSta_Shift                                              9

/*--------------------------------------
BitField Name: DE3REICnt_CurrSta
BitField Type: RW
BitField Desc: Current status DE3 REI counter event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3REICnt_CurrSta_Mask                                           cBit8
#define cAf6_rxm23e23_per_chn_curr_stat_DE3REICnt_CurrSta_Shift                                              8

/*--------------------------------------
BitField Name: DS3FEAC_E3SSM_CurrSta
BitField Type: RW
BitField Desc: Current status of DS3 FEAC or E3G832 SSM event to generate an
interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_Mask                                       cBit7
#define cAf6_rxm23e23_per_chn_curr_stat_DS3FEAC_E3SSM_CurrSta_Shift                                          7

/*--------------------------------------
BitField Name: DS3AIC_E3TM_CurrSta
BitField Type: RW
BitField Desc: Current status of DS3 AIC or E3G832 TM event to generate an
interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_Mask                                         cBit6
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AIC_E3TM_CurrSta_Shift                                            6

/*--------------------------------------
BitField Name: DS3IDLESig_E3PT_CurrSta
BitField Type: RW
BitField Desc: Current status of DS3 IDLE signal or E3G832 PayloadType event to
generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_Mask                                     cBit5
#define cAf6_rxm23e23_per_chn_curr_stat_DS3IDLESig_E3PT_CurrSta_Shift                                        5

/*--------------------------------------
BitField Name: DE3AisAllOneCurrSta
BitField Type: RW
BitField Desc: Current status of AIS All One event to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_Mask                                         cBit4
#define cAf6_rxm23e23_per_chn_curr_stat_DE3AisAllOneCurrSta_Shift                                            4

/*--------------------------------------
BitField Name: DE3LosAllZeroCurrSta
BitField Type: RW
BitField Desc: Current status of LOS All Zero event to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_Mask                                        cBit3
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LosAllZeroCurrSta_Shift                                           3

/*--------------------------------------
BitField Name: DS3AISSigCurrSta
BitField Type: RW
BitField Desc: Current status of AIS Signal event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_Mask                                            cBit2
#define cAf6_rxm23e23_per_chn_curr_stat_DS3AISSigCurrSta_Shift                                               2

/*--------------------------------------
BitField Name: DE3RDICurrSta
BitField Type: RW
BitField Desc: Current status of RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_Mask                                               cBit1
#define cAf6_rxm23e23_per_chn_curr_stat_DE3RDICurrSta_Shift                                                  1

/*--------------------------------------
BitField Name: DE3LofCurrSta
BitField Type: RW
BitField Desc: Current status of LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_Mask                                               cBit0
#define cAf6_rxm23e23_per_chn_curr_stat_DE3LofCurrSta_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Channel Interrupt OR Status
Reg Addr   : 0x00040DC0-0x00040DC1
Reg Formula: 0x00040DC0 +  GrpID
    Where  : 
           + $GrpID(0-1): grpid = 0 for de3id 0-31, grpid = 1 for de3id 32-47
Reg Desc   : 
The register consists of 2 bits for 2 Group of the related STS/VC in the RxM23E23. Each bit is used to store Interrupt OR status of the related Group DS3/E3.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM23E23_Framer_per_chn_intr_or_stat_Base                                           0x00040DC0

/*--------------------------------------
BitField Name: DE3GrpIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding DS3/E3 is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_RxM23E23_Framer_per_chn_intr_or_stat_DE3GrpIntrOrSta_Mask                                cBit31_0
#define cAf6_RxM23E23_Framer_per_chn_intr_or_stat_DE3GrpIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Group Interrupt OR Status
Reg Addr   : 0x00040DFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 Group of the RxM23E23 Framer. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM23E23_Framer_per_grp_intr_or_stat_Base                                           0x00040DFF

/*--------------------------------------
BitField Name: DE3GrpIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding Group is
set and its interrupt is enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_RxM23E23_Framer_per_grp_intr_or_stat_DE3GrpIntrOrSta_Mask                                 cBit1_0
#define cAf6_RxM23E23_Framer_per_grp_intr_or_stat_DE3GrpIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RxM23E23 Framer per Group Interrupt Enable Control
Reg Addr   : 0x00040DFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits for 2 Group in the Rx DS3/E3 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM23E23_Framer_per_Group_intr_en_ctrl_Base                                         0x00040DFE

/*--------------------------------------
BitField Name: DE3GrpIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Group to generate interrupt.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_RxM23E23_Framer_per_Group_intr_en_ctrl_DE3GrpIntrEn_Mask                                  cBit1_0
#define cAf6_RxM23E23_Framer_per_Group_intr_en_ctrl_DE3GrpIntrEn_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro DLK Swap Bit Enable
Reg Addr   : 0x04080B
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_rxde3_oh_dlk_swap_bit_cfg_Base                                                        0x04080B
#define cAf6Reg_rxde3_oh_dlk_swap_bit_cfg_WidthVal                                                          32

/*--------------------------------------
BitField Name: DlkSwapBitDs3En
BitField Type: RW
BitField Desc:
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_Mask                                              cBit2
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitDs3En_Shift                                                 2

/*--------------------------------------
BitField Name: DlkSwapBitE3g751En
BitField Type: RW
BitField Desc:
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_Mask                                           cBit1
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g751En_Shift                                              1

/*--------------------------------------
BitField Name: DlkSwapBitE3g823En
BitField Type: RW
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_Mask                                           cBit0
#define cAf6_rxde3_oh_dlk_swap_bit_cfg_DlkSwapBitE3g823En_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Control
Reg Addr   : 0x00041000 - 0x000411FF
Reg Formula: 0x00041000 +  8*de3id + de2id
    Where  : 
           + $de3id(0-47)
           + $de2id(0-6): HDL_PATH     : rxm13e13.rxm12e12.ctrlbufarb.membuf.array.ram[$de3id*8 + $de2id]
Reg Desc   : 
The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_ctrl_Base                                                                  0x00041000
#define cAf6Reg_rxm12e12_ctrl_WidthVal                                                                      32

/*--------------------------------------
BitField Name: RxDS2E2FrcLof
BitField Type: RW
BitField Desc: This bit is set to 1 to force the Rx DS3/E3 framer into LOF tus.
1: force LOF. 0: not force LOF.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Mask                                                            cBit2
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Shift                                                               2

/*--------------------------------------
BitField Name: RxDS2E2Md
BitField Type: RW
BitField Desc: Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2
G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Mask                                                              cBit1_0
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : PDH RxM12E12 Per STS/VC Payload Control
Reg Addr   : 0x00041600 - 0x0004163F
Reg Formula: 0x00041600 +  stsid
    Where  : 
           + $stsid(0-47):
Reg Desc   : 
The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_stsvc_payload_ctrl_Base                                                    0x00041600
#define cAf6Reg_rxm12e12_stsvc_payload_ctrl_WidthVal                                                        32

/*--------------------------------------
BitField Name: PDHVtRxMapTug26Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2
type 2: reserved 3: reserved
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Mask                                    cBit13_12
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug26Type_Shift                                          12

/*--------------------------------------
BitField Name: PDHVtRxMapTug25Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-5
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Mask                                    cBit11_10
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug25Type_Shift                                          10

/*--------------------------------------
BitField Name: PDHVtRxMapTug24Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-4
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Mask                                      cBit9_8
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug24Type_Shift                                           8

/*--------------------------------------
BitField Name: PDHVtRxMapTug23Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-3
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Mask                                      cBit7_6
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug23Type_Shift                                           6

/*--------------------------------------
BitField Name: PDHVtRxMapTug22Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-2
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Mask                                      cBit5_4
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug22Type_Shift                                           4

/*--------------------------------------
BitField Name: PDHVtRxMapTug21Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-1
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Mask                                      cBit3_2
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug21Type_Shift                                           2

/*--------------------------------------
BitField Name: PDHVtRxMapTug20Type
BitField Type: RW
BitField Desc: Define types of VT/TUs in TUG-2-0
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Mask                                      cBit1_0
#define cAf6_rxm12e12_stsvc_payload_ctrl_PDHVtRxMapTug20Type_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 HW Status
Reg Addr   : 0x00041400 - 0x000415FF
Reg Formula: 0x00041400 +  8*de3id + de2id
    Where  : 
           + $de3id(0-47)
           + $de2id(0-6):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_hw_stat_Base                                                               0x00041400

/*--------------------------------------
BitField Name: RxM12E12Status
BitField Type: RO
BitField Desc: for HW debug only
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Mask                                                      cBit1_0
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00041700-0x0004173F
Reg Formula: 0x00041700 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
This is the per Channel interrupt enable of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_intr_cfg_Base                                                      0x00041700
#define cAf6Reg_RxM12E12_per_chn_intr_cfg_WidthVal                                                          32

/*--------------------------------------
BitField Name: DE2LOFIntrEn
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_Mask                                               cBit6_0
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LOFIntrEn_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt Change Status
Reg Addr   : 0x00041740-0x0004177F
Reg Formula: 0x00041740 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
This is the per Channel interrupt status of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_intr_stat_Base                                                     0x00041740
#define cAf6Reg_RxM12E12_per_chn_intr_stat_WidthVal                                                         32

/*--------------------------------------
BitField Name: DE2LOFIntr
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_Mask                                                cBit6_0
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LOFIntr_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Current Status
Reg Addr   : 0x00041780-0x000417BF
Reg Formula: 0x00041780 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
This is the per Channel Current status of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_curr_stat_Base                                                     0x00041780
#define cAf6Reg_RxM12E12_per_chn_curr_stat_WidthVal                                                         32

/*--------------------------------------
BitField Name: DE2LOFCurrSta
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS3/E3 0: No
Interrupt 1: Have Interrupt
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_Mask                                             cBit6_0
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LOFCurrSta_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt OR Status
Reg Addr   : 0x000417C0-0x000417C1
Reg Formula: 0x000417C0 +  GrpID
    Where  : 
           + $GrpID(0-1): grpid = 0 for de3id 0-31, grpid = 1 for de3id 32-47
Reg Desc   : 
The register consists of 32 bits for 2 Group of the related STS/VC in the RxM12E12. Each bit is used to store Interrupt OR tus of the related Group DS2/E2.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_Framer_per_chn_intr_or_stat_Base                                           0x000417C0

/*--------------------------------------
BitField Name: DE2GrpIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding Group
DS3/E3 is set and its interrupt is enabled.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_RxM12E12_Framer_per_chn_intr_or_stat_DE2GrpIntrOrSta_Mask                                 cBit1_0
#define cAf6_RxM12E12_Framer_per_chn_intr_or_stat_DE2GrpIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Group Interrupt OR Status
Reg Addr   : 0x000417FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 Group of the RxM12E12 Framer. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_Framer_per_grp_intr_or_stat_Base                                           0x000417FF

/*--------------------------------------
BitField Name: DE2GrpIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding Group is
set and its interrupt is enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_RxM12E12_Framer_per_grp_intr_or_stat_DE2GrpIntrOrSta_Mask                                 cBit1_0
#define cAf6_RxM12E12_Framer_per_grp_intr_or_stat_DE2GrpIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Group Interrupt Enable Control
Reg Addr   : 0x000417FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits for 2 Group in the Rx DS2/E2 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_Framer_per_Group_intr_en_ctrl_Base                                         0x000417FE

/*--------------------------------------
BitField Name: DE2GrpIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Group to generate interrupt.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_RxM12E12_Framer_per_Group_intr_en_ctrl_DE2GrpIntrEn_Mask                                  cBit1_0
#define cAf6_RxM12E12_Framer_per_Group_intr_en_ctrl_DE2GrpIntrEn_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer FBE Threshold Control
Reg Addr   : 0x00050003 - 0x00050003
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_fbe_thrsh_cfg_Base                                                             0x00050003

/*--------------------------------------
BitField Name: RxDE1FbeThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_Mask                                                    cBit23_0
#define cAf6_dej1_fbe_thrsh_cfg_RxDE1FbeThres_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer Control
Reg Addr   : 0x00054000 - 0x000547FF
Reg Formula: 0x00054000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-47)
           + $de2id(0-6):
           + $de1id(0-3): HDL_PATH     : ds1e1rxfrm.ctrlbuf.array.ram[$de3id*32 + $de2id*4 + $de1id]
Reg Desc   : 
DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_ctrl_Base                                                            0x00054000

/*--------------------------------------
BitField Name: RxDE1LOFFrwDis
BitField Type: RW
BitField Desc: Forward DE1 LOF to generate L-bit to PSN in case of MonOnly, set
1 to disable
BitField Bits: [31]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LOFFrwDis_Mask                                                    cBit31
#define cAf6_dej1_rx_framer_ctrl_RxDE1LOFFrwDis_Shift                                                       31

/*--------------------------------------
BitField Name: RxDE1FrcAISLBit
BitField Type: RW
BitField Desc: Force AIS alarm here,L-bit, data all one to PSN when Line/Payload
Remote Loopback
BitField Bits: [30]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLBit_Mask                                                   cBit30
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLBit_Shift                                                      30

/*--------------------------------------
BitField Name: RxDE1FrcAISLineDwn
BitField Type: RW
BitField Desc: Set 1 to force AIS Line DownStream
BitField Bits: [29]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_Mask                                                cBit29
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISLineDwn_Shift                                                   29

/*--------------------------------------
BitField Name: RxDE1FrcAISPldDwn
BitField Type: RW
BitField Desc: Set 1 to force AIS payload DownStream
BitField Bits: [28]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_Mask                                                 cBit28
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcAISPldDwn_Shift                                                    28

/*--------------------------------------
BitField Name: RxDE1FrcLos
BitField Type: RW
BitField Desc: Set 1 to force LOS
BitField Bits: [27]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_Mask                                                       cBit27
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLos_Shift                                                          27

/*--------------------------------------
BitField Name: RxDE1LomfDstrEn
BitField Type: RW
BitField Desc: Set 1 to generate AIS forwarding dowstream in case E1CRC LOMF
defect is present. This bit set 1 also to backwarding RDI upstream if E1 path is
terminated in case E1CRC LOMF defect is present
BitField Bits: [26]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LomfDstrEn_Mask                                                   cBit26
#define cAf6_dej1_rx_framer_ctrl_RxDE1LomfDstrEn_Shift                                                      26

/*--------------------------------------
BitField Name: RxDE1LosCntThres
BitField Type: RW
BitField Desc: Threshold of LOS monitoring block. This function always monitors
the number of bit 1 of the incoming signal in each of two consecutive frame
periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire
number, LOS will be set then.
BitField Bits: [25:23]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_Mask                                               cBit25_23
#define cAf6_dej1_rx_framer_ctrl_RxDE1LosCntThres_Shift                                                     23

/*--------------------------------------
BitField Name: RxDE1MonOnly
BitField Type: RW
BitField Desc: Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer
BitField Bits: [22]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_Mask                                                      cBit22
#define cAf6_dej1_rx_framer_ctrl_RxDE1MonOnly_Shift                                                         22

/*--------------------------------------
BitField Name: RxDE1LocalLineLoop
BitField Type: RW
BitField Desc: Set 1 to enable Local Line loop back
BitField Bits: [21]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_Mask                                                cBit21
#define cAf6_dej1_rx_framer_ctrl_RxDE1LocalLineLoop_Shift                                                   21

/*--------------------------------------
BitField Name: RxDe1LocalPayLoop
BitField Type: RW
BitField Desc: Set 1 to enable Local Payload loop back. Sharing for VT Loopback
in case of CEP path
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_Mask                                                 cBit20
#define cAf6_dej1_rx_framer_ctrl_RxDe1LocalPayLoop_Shift                                                    20

/*--------------------------------------
BitField Name: RxDE1E1SaCfg
BitField Type: RW
BitField Desc: Incase E1 Sa used for FDL configuration 0x0: No Sa bit used 0x3:
Sa4 is used for SSM 0x4: Sa5 is used for SSM 0x5: Sa6 is used for SSM 0x6: Sa7
is used for SSM 0x7: Sa8 is used for SSM Incase DS1 bit[19:18] dont care bit
[17]   set " 1" to enable Rx DLK
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_Mask                                                   cBit19_17
#define cAf6_dej1_rx_framer_ctrl_RxDE1E1SaCfg_Shift                                                         17

/*--------------------------------------
BitField Name: RxDE1LofThres
BitField Type: RW
BitField Desc: Threshold for FAS error to declare LOF in DS1/E1/J1 mode
BitField Bits: [16:13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_Mask                                                  cBit16_13
#define cAf6_dej1_rx_framer_ctrl_RxDE1LofThres_Shift                                                        13

/*--------------------------------------
BitField Name: RxDE1AisCntThres
BitField Type: RW
BitField Desc: Threshold of AIS monitoring block. This function always monitors
the number of bit 0 of the incoming signal in each of two consecutive frame
periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire
number, AIS will be set then.
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_Mask                                               cBit12_10
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisCntThres_Shift                                                     10

/*--------------------------------------
BitField Name: RxDE1RaiCntThres
BitField Type: RW
BitField Desc: Threshold of RAI monitoring block
BitField Bits: [9:7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_Mask                                                 cBit9_7
#define cAf6_dej1_rx_framer_ctrl_RxDE1RaiCntThres_Shift                                                      7

/*--------------------------------------
BitField Name: RxDE1AisMaskEn
BitField Type: RW
BitField Desc: Forward DE1 AIS to generate L-bit to PSN, set 1 to enable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_Mask                                                     cBit6
#define cAf6_dej1_rx_framer_ctrl_RxDE1AisMaskEn_Shift                                                        6

/*--------------------------------------
BitField Name: RxDE1En
BitField Type: RW
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_Mask                                                            cBit5
#define cAf6_dej1_rx_framer_ctrl_RxDE1En_Shift                                                               5

/*--------------------------------------
BitField Name: RxDE1FrcLof
BitField Type: RW
BitField Desc: Set 1 to force Re-frame
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_Mask                                                        cBit4
#define cAf6_dej1_rx_framer_ctrl_RxDE1FrcLof_Shift                                                           4

/*--------------------------------------
BitField Name: RxDs1Md
BitField Type: RW
BitField Desc: Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF
(D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1
Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_Mask                                                          cBit3_0
#define cAf6_dej1_rx_framer_ctrl_RxDs1Md_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer HW Status
Reg Addr   : 0x00054800-0x00054FFF
Reg Formula: 0x00054800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-47)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_hw_stat_Base                                                         0x00054800

/*--------------------------------------
BitField Name: RxDS1E1_CrrE1SSM
BitField Type: RO
BitField Desc: Current E1 SSM value
BitField Bits: [10:7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrE1SSM_Mask                                             cBit10_7
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrE1SSM_Shift                                                   7

/*--------------------------------------
BitField Name: RxDS1E1_CrrAISDownStr
BitField Type: RO
BitField Desc: AIS Down Stream due to HI_Level AIS of DS1E1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrAISDownStr_Mask                                           cBit6
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrAISDownStr_Shift                                              6

/*--------------------------------------
BitField Name: RxDS1E1_CrrLosAllZero
BitField Type: RO
BitField Desc: LOS All zero of DS1E1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrLosAllZero_Mask                                           cBit5
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrLosAllZero_Shift                                              5

/*--------------------------------------
BitField Name: RxDS1E1_CrrAISAllOne
BitField Type: RO
BitField Desc: AIS All one of DS1E1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrAISAllOne_Mask                                            cBit4
#define cAf6_dej1_rx_framer_hw_stat_RxDS1E1_CrrAISAllOne_Shift                                               4

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RO
BitField Desc: RxDE1Sta 000: Search 001: Shift 010: Wait 011: Verify 100: Fs
search 101: Inframe
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_Mask                                                      cBit2_0
#define cAf6_dej1_rx_framer_hw_stat_RxDE1Sta_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer FBE Counter
Reg Addr   : 0x0005C000-0x0005CFFF
Reg Formula: 0x0005C000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_fbe_cnt_Base                                                         0x0005C000
#define cAf6Reg_dej1_rx_framer_fbe_cnt_WidthVal                                                             32

/*--------------------------------------
BitField Name: DE1FbeCnt
BitField Type: RO
BitField Desc: DS1/E1/J1 F-bit error accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_Mask                                                     cBit7_0
#define cAf6_dej1_rx_framer_fbe_cnt_DE1FbeCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer CRC Error Counter
Reg Addr   : 0x0005D000 - 0x0005DFFF
Reg Formula: 0x0005D000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel CRC error counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_crcerr_cnt_Base                                                      0x0005D000
#define cAf6Reg_dej1_rx_framer_crcerr_cnt_WidthVal                                                          32

/*--------------------------------------
BitField Name: DE1CrcErrCnt
BitField Type: RO
BitField Desc: DS1/E1/J1 CRC Error Accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_Mask                                               cBit7_0
#define cAf6_dej1_rx_framer_crcerr_cnt_DE1CrcErrCnt_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer REI Counter
Reg Addr   : 0x0005E000 - 0x0005EFFF
Reg Formula: 0x0005E000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_rei_cnt_Base                                                         0x0005E000
#define cAf6Reg_dej1_rx_framer_rei_cnt_WidthVal                                                             32

/*--------------------------------------
BitField Name: DE1ReiCnt
BitField Type: RO
BitField Desc: DS1/E1/J1 REI error accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_Mask                                                     cBit7_0
#define cAf6_dej1_rx_framer_rei_cnt_DE1ReiCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00056000-0x000567FF
Reg Formula: 0x00056000 +  StsID*32 + Tug2ID*4 + VtnID
    Where  : 
           + $StsID(0-47): STS-1/VC-3 ID
           + $Tug2ID(0-6): TUG-2/VTG ID
           + $VtnID(0-3): VT/TU number ID in the TUG-2/VTG
Reg Desc   : 
This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_Base                                            0x00056000

/*--------------------------------------
BitField Name: DE1BerSdIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the BER-SD interrupt.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Mask                                    cBit13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Shift                                       13

/*--------------------------------------
BitField Name: DE1BerSfIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the BER-SF interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Mask                                    cBit12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Shift                                       12

/*--------------------------------------
BitField Name: DE1oblcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the new Outband LoopCode detected to generate an
interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Mask                                     cBit11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Shift                                        11

/*--------------------------------------
BitField Name: DE1iblcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the new Inband LoopCode detected to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Mask                                     cBit10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Shift                                        10

/*--------------------------------------
BitField Name: DE1SigRAIIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Signalling RAI event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Mask                                    cBit9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Shift                                       9

/*--------------------------------------
BitField Name: DE1SigLOFIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Signalling LOF event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Mask                                    cBit8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Shift                                       8

/*--------------------------------------
BitField Name: DE1FbeIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change FBE te event to generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Mask                                       cBit7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Shift                                          7

/*--------------------------------------
BitField Name: DE1CrcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change CRC te event to generate an interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Mask                                       cBit6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Shift                                          6

/*--------------------------------------
BitField Name: DE1ReiIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change REI te event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Mask                                       cBit5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Shift                                          5

/*--------------------------------------
BitField Name: DE1LomfIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOMF te event to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Mask                                      cBit4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Shift                                         4

/*--------------------------------------
BitField Name: DE1LosIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOS te event to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Mask                                       cBit3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Shift                                          3

/*--------------------------------------
BitField Name: DE1AisIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS te event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Mask                                       cBit2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Shift                                          2

/*--------------------------------------
BitField Name: DE1RaiIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change RAI te event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Mask                                       cBit1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Shift                                          1

/*--------------------------------------
BitField Name: DE1LofIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOF te event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Mask                                       cBit0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt Status
Reg Addr   : 0x00056800-0x00056FFF
Reg Formula: 0x00056800 +  StsID*32 + Tug2ID*4 + VtnID
    Where  : 
           + $StsID(0-47): STS-1/VC-3 ID
           + $Tug2ID(0-6): TUG-2/VTG ID
           + $VtnID(0-3): VT/TU number ID in the TUG-2/VTG
Reg Desc   : 
This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_Base                                               0x00056800

/*--------------------------------------
BitField Name: DE1BerSdIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change of BER-SD.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Mask                                         cBit13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Shift                                            13

/*--------------------------------------
BitField Name: DE1BerSfIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change of BER-SF.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Mask                                         cBit12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Shift                                            12

/*--------------------------------------
BitField Name: DE1oblcIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change the new Outband LoopCode detected to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Mask                                          cBit11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Shift                                             11

/*--------------------------------------
BitField Name: DE1iblcIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change the new Inband LoopCode detected to
generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Mask                                          cBit10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Shift                                             10

/*--------------------------------------
BitField Name: DE1SigRAIIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change Signalling RAI event to generate an
interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Mask                                         cBit9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Shift                                            9

/*--------------------------------------
BitField Name: DE1SigLOFIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change Signalling LOF event to generate an
interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Mask                                         cBit8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Shift                                            8

/*--------------------------------------
BitField Name: DE1FbeIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in FBE exceed threshold te event.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Mask                                            cBit7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Shift                                               7

/*--------------------------------------
BitField Name: DE1CrcIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in CRC exceed threshold te event.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Mask                                            cBit6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Shift                                               6

/*--------------------------------------
BitField Name: DE1ReiIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in REI exceed threshold te event.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Mask                                            cBit5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Shift                                               5

/*--------------------------------------
BitField Name: DE1LomfIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in LOMF te event.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Mask                                           cBit4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Shift                                              4

/*--------------------------------------
BitField Name: DE1LosIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in LOS te event.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Mask                                            cBit3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Shift                                               3

/*--------------------------------------
BitField Name: DE1AisIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in AIS te event.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Mask                                            cBit2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Shift                                               2

/*--------------------------------------
BitField Name: DE1RaiIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in RAI te event.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Mask                                            cBit1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Shift                                               1

/*--------------------------------------
BitField Name: DE1LofIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in LOF te event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Mask                                            cBit0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Current Status
Reg Addr   : 0x00057000-0x000577FF
Reg Formula: 0x00057000 +  StsID*32 + Tug2ID*4 + VtnID
    Where  : 
           + $StsID(0-47): STS-1/VC-3 ID
           + $Tug2ID(0-6): TUG-2/VTG ID
           + $VtnID(0-3): VT/TU number ID in the TUG-2/VTG
Reg Desc   : 
This is the per Channel Current tus of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_Base                                               0x00057000

/*--------------------------------------
BitField Name: DE1BerSdIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SD.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Mask                                         cBit13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Shift                                            13

/*--------------------------------------
BitField Name: DE1BerSfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SF.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Mask                                         cBit12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Shift                                            12

/*--------------------------------------
BitField Name: DE1oblcIntrSta
BitField Type: RW
BitField Desc: Current status the new Outband LoopCode detected to generate an
interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Mask                                       cBit11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Shift                                          11

/*--------------------------------------
BitField Name: DE1iblcIntrSta
BitField Type: RW
BitField Desc: Current status the new Inband LoopCode detected to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Mask                                       cBit10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Shift                                          10

/*--------------------------------------
BitField Name: DE1SigRAIIntrSta
BitField Type: RW
BitField Desc: Current status Signalling RAI event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Mask                                      cBit9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Shift                                         9

/*--------------------------------------
BitField Name: DE1SigLOFIntrSta
BitField Type: RW
BitField Desc: Current status Signalling LOF event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Mask                                      cBit8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Shift                                         8

/*--------------------------------------
BitField Name: DE1FbeCurrSta
BitField Type: RW
BitField Desc: Current tus of FBE exceed threshold event.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Mask                                         cBit7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Shift                                            7

/*--------------------------------------
BitField Name: DE1CrcCurrSta
BitField Type: RW
BitField Desc: Current tus of CRC exceed threshold event.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Mask                                         cBit6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Shift                                            6

/*--------------------------------------
BitField Name: DE1ReiCurrSta
BitField Type: RW
BitField Desc: Current tus of REI exceed threshold event.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Mask                                         cBit5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Shift                                            5

/*--------------------------------------
BitField Name: DE1LomfCurrSta
BitField Type: RW
BitField Desc: Current tus of LOMF event.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Mask                                        cBit4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Shift                                           4

/*--------------------------------------
BitField Name: DE1LosCurrSta
BitField Type: RW
BitField Desc: Current tus of LOS event.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Mask                                         cBit3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Shift                                            3

/*--------------------------------------
BitField Name: DE1AisCurrSta
BitField Type: RW
BitField Desc: Current tus of AIS event.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Mask                                         cBit2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Shift                                            2

/*--------------------------------------
BitField Name: DE1RaiCurrSta
BitField Type: RW
BitField Desc: Current tus of RAI event.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Mask                                         cBit1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Shift                                            1

/*--------------------------------------
BitField Name: DE1LofCurrSta
BitField Type: RW
BitField Desc: Current tus of LOF event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Mask                                         cBit0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt OR Status
Reg Addr   : 0x00057800
Reg Formula: 0x00057800 +  GrpID*32 + STSID
    Where  : 
           + $GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47
           + $STSID(0-31)
Reg Desc   : 
The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_Base                                            0x00057800

/*--------------------------------------
BitField Name: RxDE1VtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt tus bit of corresponding DS1/E1/J1 is
set and its interrupt is enabled.
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Mask                                cBit27_0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status
Reg Addr   : 0x00057FFE
Reg Formula: 0x00057FFE +  GrpID
    Where  : 
           + $GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47
Reg Desc   : 
The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Base                                          0x00057FFE

/*--------------------------------------
BitField Name: RxDE1StsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt tus bit of corresponding STS/VC is set
and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Mask                                cBit31_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control
Reg Addr   : 0x00057FFC
Reg Formula: 0x00057FFC +  GrpID
    Where  : 
           + $GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47
Reg Desc   : 
The register consists of 12 interrupt enable bits for 12 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Base                                          0x00057FFC

/*--------------------------------------
BitField Name: RxDE1StsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Mask                                cBit31_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Group Interrupt OR Status
Reg Addr   : 0x00050002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 Group of the Rx DS1/E1/J1 Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_grp_intr_or_stat_Base                                            0x00050002

/*--------------------------------------
BitField Name: RxDE1GrpIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt tus bit of corresponding Group is set
and its interrupt is enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_grp_intr_or_stat_RxDE1GrpIntrOrSta_Mask                                 cBit1_0
#define cAf6_dej1_rx_framer_per_grp_intr_or_stat_RxDE1GrpIntrOrSta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Map Control
Reg Addr   : 0x00076000 - 0x000767FF
Reg Formula: 0x00076000 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-47):
           + $vtgid(0-6):
           + $vtid(0-3): HDL_PATH     : stsvtmap.ctrlbuf.array.ram[$stsid*32 + $vtgid*4 + $vtid]
Reg Desc   : 
The STS/VT Map Control is use to configure for per channel STS/VT Map operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_map_ctrl_Base                                                                 0x00076000

/*--------------------------------------
BitField Name: Pdhoversdhmd
BitField Type: RW
BitField Desc: Set 1 to PDH LIU over OCN
BitField Bits: [20]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Mask                                                           cBit20
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Shift                                                              20

/*--------------------------------------
BitField Name: StsVtMapAismask
BitField Type: RW
BitField Desc: Set 1 to mask AIS from PDH to OCN
BitField Bits: [19]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_Mask                                                        cBit19
#define cAf6_stsvt_map_ctrl_StsVtMapAismask_Shift                                                           19

/*--------------------------------------
BitField Name: StsVtMapJitIDCfg
BitField Type: RW
BitField Desc: STS/VT Mapping Jitter ID configure, default value is 0
BitField Bits: [18:15]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_Mask                                                    cBit18_15
#define cAf6_stsvt_map_ctrl_StsVtMapJitIDCfg_Shift                                                          15

/*--------------------------------------
BitField Name: StsVtMapStuffThres
BitField Type: RW
BitField Desc: STS/VT Mapping Stuff Threshold Configure, default value is 12
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_Mask                                                  cBit14_10
#define cAf6_stsvt_map_ctrl_StsVtMapStuffThres_Shift                                                        10

/*--------------------------------------
BitField Name: StsVtMapJitterCfg
BitField Type: RW
BitField Desc: STS/VT Mapping Jitter configure - Vtmap: 4'b0000: looptiming mode
4'b1000: fine_stuff_mode, default - Spe Map: 4'b0000: full_stuff_mode 4'b1000:
fine_stuff_mode, default
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Mask                                                     cBit9_6
#define cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Shift                                                          6

/*--------------------------------------
BitField Name: StsVtMapCenterFifo
BitField Type: RW
BitField Desc: This bit is set to 1 to force center the mapping FIFO.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_Mask                                                      cBit5
#define cAf6_stsvt_map_ctrl_StsVtMapCenterFifo_Shift                                                         5

/*--------------------------------------
BitField Name: StsVtMapBypass
BitField Type: RW
BitField Desc: This bit is set to 1 to bypass the STS/VT map
BitField Bits: [4]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_Mask                                                          cBit4
#define cAf6_stsvt_map_ctrl_StsVtMapBypass_Shift                                                             4

/*--------------------------------------
BitField Name: StsVtMapMd
BitField Type: RW
BitField Desc: STS/VT Mapping mode StsVtMapMd   StsVtMapBypass  Operation Mode 0
0     E1 to VT2 Map 0           1     E1 to Bus 1               0     DS1 to
VT1.5 Map 1                1     DS1 to Bus 2              0     E3 to VC3 Map 2
1     E3 to TU3 Map 3           0     DS3 to VC3 Map 3                  1
DS3 to TU3 Map 4                  x     VT15/VT2 Basic CEP 5              x
Packet over STS1/VC3 6            0     STS1/VC3 Fractional CEP carrying
VT2/TU12 6               1     STS1/VC3 Fractional CEP carrying VT15/TU11 7
0     STS1/VC3 Fractional CEP carrying E3 7             1     STS1/VC3
Fractional CEP carrying DS3 8            x     STS1/VC3 Basic CEP 9
x     Packet over STS3c/VC4 10                0       STS3c/VC4 Fractional CEP
carrying VT2/TU12 10           1       STS3c/VC4 Fractional CEP carrying
VT15/TU11 11          0       STS3c/VC4 Fractional CEP carrying E3 11         1
STS3c/VC4 Fractional CEP carrying DS3 12                x
STS3c/VC4/STS12c/VC4_4c Basic CEP 13            x       Packet over TU3 14
0       E3 to Bus 14            1       DS3 to Bus 15           x       Disable
STS/VC/VT/TU/DS1/E1/DS3/E3
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_stsvt_map_ctrl_StsVtMapMd_Mask                                                            cBit3_0
#define cAf6_stsvt_map_ctrl_StsVtMapMd_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : STS/VT Map HW Status
Reg Addr   : 0x00074000-0x000747FF
Reg Formula: 0x00074000 +  32*stsid + 4*vtgid + vtid
    Where  : 
           + $stsid(0-47):
           + $vtgid(0-6):
           + $vtid(0-3):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_stsvt_map_hw_stat_Base                                                              0x00074000
#define cAf6Reg_stsvt_map_hw_stat_WidthVal                                                                  64

/*--------------------------------------
BitField Name: STSVTStatus
BitField Type: RO
BitField Desc: for HW debug only
BitField Bits: [35:0]
--------------------------------------*/
#define cAf6_stsvt_map_hw_stat_STSVTStatus_01_Mask                                                    cBit31_0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_01_Shift                                                          0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_02_Mask                                                     cBit3_0
#define cAf6_stsvt_map_hw_stat_STSVTStatus_02_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Control
Reg Addr   : 0x00094000 - 0x000947FF
Reg Formula: 0x00094000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3): HDL_PATH     : de1txfrm.ctrlbuf.array.ram[$de3id*32 + $de2id*4 + $de1id]
Reg Desc   : 
DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_ctrl_Base                                                            0x00094000

/*--------------------------------------
BitField Name: TxDE1ForceAllOne
BitField Type: RW
BitField Desc: Force all one to TDM side when Line Local Loopback only side
BitField Bits: [24]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1ForceAllOne_Mask                                                  cBit24
#define cAf6_dej1_tx_framer_ctrl_TxDE1ForceAllOne_Shift                                                     24

/*--------------------------------------
BitField Name: TxDE1FbitBypass
BitField Type: RW
BitField Desc: Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer
BitField Bits: [21]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_Mask                                                   cBit21
#define cAf6_dej1_tx_framer_ctrl_TxDE1FbitBypass_Shift                                                      21

/*--------------------------------------
BitField Name: TxDE1LineAisIns
BitField Type: RW
BitField Desc: Set 1 to enable Line AIS Insert
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_Mask                                                   cBit20
#define cAf6_dej1_tx_framer_ctrl_TxDE1LineAisIns_Shift                                                      20

/*--------------------------------------
BitField Name: TxDE1PayAisIns
BitField Type: RW
BitField Desc: Set 1 to enable Payload AIS Insert
BitField Bits: [19]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_Mask                                                    cBit19
#define cAf6_dej1_tx_framer_ctrl_TxDE1PayAisIns_Shift                                                       19

/*--------------------------------------
BitField Name: TxDE1RmtLineloop
BitField Type: RW
BitField Desc: Set 1 to enable remote Line Loop back
BitField Bits: [18]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_Mask                                                  cBit18
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtLineloop_Shift                                                     18

/*--------------------------------------
BitField Name: TxDE1RmtPayloop
BitField Type: RW
BitField Desc: Set 1 to enable remote Payload Loop back. Sharing for VT Loopback
in case of CEP path
BitField Bits: [17]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_Mask                                                   cBit17
#define cAf6_dej1_tx_framer_ctrl_TxDE1RmtPayloop_Shift                                                      17

/*--------------------------------------
BitField Name: TxDE1AutoAis
BitField Type: RW
BitField Desc: Set 1 to enable AIS indication from data map block to
automatically transmit all 1s at DS1/E1/J1 Tx framer
BitField Bits: [16]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_Mask                                                      cBit16
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoAis_Shift                                                         16

/*--------------------------------------
BitField Name: TxDE1DLCfg
BitField Type: RW
BitField Desc: Incase E1 mode : use for SSM in Sa, bit[15:13] is Sa position
0=disable, 3=Sa4, 4=Sa5, 5=Sa6, 6=Sa7,7=Sa8. Bit[12:9] is SSM value. Incase DS1
mode : bit[12] Set 1 to Enable FDL transmit , dont care other bits
BitField Bits: [15:9]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_Mask                                                      cBit15_9
#define cAf6_dej1_tx_framer_ctrl_TxDE1DLCfg_Shift                                                            9

/*--------------------------------------
BitField Name: TxDE1AutoYel
BitField Type: RW
BitField Desc: Auto Yellow generation enable 1: Yellow alarm detected from Rx
Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow
alarm transmitted
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_Mask                                                       cBit8
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoYel_Shift                                                          8

/*--------------------------------------
BitField Name: TxDE1FrcYel
BitField Type: RW
BitField Desc: SW force to Tx Yellow alarm
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_Mask                                                        cBit7
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcYel_Shift                                                           7

/*--------------------------------------
BitField Name: TxDE1AutoCrcErr
BitField Type: RW
BitField Desc: Auto CRC error enable 1: CRC Error detected from Rx Framer will
be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC
Error alarm transmitted
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_Mask                                                    cBit6
#define cAf6_dej1_tx_framer_ctrl_TxDE1AutoCrcErr_Shift                                                       6

/*--------------------------------------
BitField Name: TxDE1FrcCrcErr
BitField Type: RW
BitField Desc: SW force to Tx CRC Error alarm (REI bit)
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_Mask                                                     cBit5
#define cAf6_dej1_tx_framer_ctrl_TxDE1FrcCrcErr_Shift                                                        5

/*--------------------------------------
BitField Name: TxDE1En
BitField Type: RW
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_Mask                                                            cBit4
#define cAf6_dej1_tx_framer_ctrl_TxDE1En_Shift                                                               4

/*--------------------------------------
BitField Name: RxDE1Md
BitField Type: RW
BitField Desc: Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF
(D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1
Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_Mask                                                          cBit3_0
#define cAf6_dej1_tx_framer_ctrl_RxDE1Md_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Status
Reg Addr   : 0x00095000 - 0x000957FF
Reg Formula: 0x00095000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
These registers are used for Hardware tus only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_stat_Base                                                            0x00095000

/*--------------------------------------
BitField Name: TxDE1Sta
BitField Type: RO
BitField Desc: HW status only
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_stat_TxDE1Sta_Mask                                                        cBit31_0
#define cAf6_dej1_tx_framer_stat_TxDE1Sta_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Insertion Control
Reg Addr   : 0x00094800 - 0x00094FFF
Reg Formula: 0x00094800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3): HDL_PATH     : de1txfrm.siginsbuf.array.ram[$de3id*32 + $de2id*4 + $de1id]
Reg Desc   : 
DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_insertion_ctrl_Base                                             0x00094800

/*--------------------------------------
BitField Name: TxDE1SigMfrmEn
BitField Type: RW
BitField Desc: Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling
multiframe to the incoming data flow. No applicable for DS1
BitField Bits: [30]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Mask                                     cBit30
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Shift                                        30

/*--------------------------------------
BitField Name: TxDE1SigBypass
BitField Type: RW
BitField Desc: This is signaling bypass per DS0. Set 1 to bypass Signaling
insertion in Tx DS1/E1 Framer In E1 PMC30 mode, bit[0] for TS1, bit[1] for
TS2,.., bit[15] for TS17,.., bit29] for TS31. In DS1 mode, only 24 LSB bits is
used, bit[23:0].
BitField Bits: [29:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Mask                                   cBit29_0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling DS0ID Conversion Control
Reg Addr   : 0x000C2000
Reg Formula: 0x000C2000 +  pwid
    Where  : 
           + $pwid(0-2687):
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_ds0id_conv_ctrl_Base                                            0x000C2000

/*--------------------------------------
BitField Name: TxDE1SigEnb
BitField Type: RW
BitField Desc: 32 signaling enable bit for 32 DS0 in an E1 frame. In DS1 mode,
only 24 LSB bits is used.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_ds0id_conv_ctrl_TxDE1SigEnb_Mask                                     cBit31_0
#define cAf6_dej1_tx_framer_sign_ds0id_conv_ctrl_TxDE1SigEnb_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling ID Conversion Control
Reg Addr   : 0x000C3000
Reg Formula: 0x000C3000 +  pwid
    Where  : 
           + $pwid(0-2687):
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl_Base                                               0x000C3000
#define cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl_WidthVal                                                   32

/*--------------------------------------
BitField Name: TxSigOOSEnb
BitField Type: RW
BitField Desc: OOS replacement enable in case of PW failure, set 1 to enable.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask                                          cBit20
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift                                             20

/*--------------------------------------
BitField Name: TxSigOOSPattern2
BitField Type: RW
BitField Desc: OOS pattern to replace in case of PW failure.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask                                  cBit19_16
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift                                        16

/*--------------------------------------
BitField Name: TxSigOOSPattern1
BitField Type: RW
BitField Desc: OOS pattern to replace in case of PW failure.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask                                  cBit15_12
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift                                        12

/*--------------------------------------
BitField Name: TxDE1SigDS1
BitField Type: RW
BitField Desc: Set 1 if the PW carry DS0 for the DS1/J1, set 0 for E1
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask                                          cBit11
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift                                             11

/*--------------------------------------
BitField Name: TxDE1SigLineID
BitField Type: RW
BitField Desc: Output DS1/E1/J1 Line ID of the conversion.
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask                                     cBit10_0
#define cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Buffer
Reg Addr   : 0x000C8000 - 0x000CFF00
Reg Formula: 0x000C8000 +  672*de3id + 96*de2id + 32*de1id + tscnt
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_buffer_Base                                                     0x000C8000
#define cAf6Reg_dej1_tx_framer_sign_buffer_WidthVal                                                         32

/*--------------------------------------
BitField Name: TxDE1SigABCD
BitField Type: RW
BitField Desc: 4-bit ABCD signaling for each DS0. CPU need set default IDLE
value for full TS when create DE1 channel. CPU need set default IDLE value for
per TS when disable/unbind PW
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_Mask                                              cBit3_0
#define cAf6_dej1_tx_framer_sign_buffer_TxDE1SigABCD_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Payload Error Insert Enable Configuration
Reg Addr   : 0x00090002
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_errins_en_cfg_Base                                                             0x00090002

/*--------------------------------------
BitField Name: TxDE1errorins_en
BitField Type: RW
BitField Desc: Force payload error, inclued CRC
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_errins_en_cfg_TxDE1errorins_en_Mask                                                   cBit11
#define cAf6_dej1_errins_en_cfg_TxDE1errorins_en_Shift                                                      11

/*--------------------------------------
BitField Name: TxDE1errorfbit_en
BitField Type: RW
BitField Desc: Force Fbit error
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_errins_en_cfg_TxDE1errorfbit_en_Mask                                                  cBit10
#define cAf6_dej1_errins_en_cfg_TxDE1errorfbit_en_Shift                                                     10

/*--------------------------------------
BitField Name: TxDE1ErrInsID
BitField Type: RW
BitField Desc: Force ID channel
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_dej1_errins_en_cfg_TxDE1ErrInsID_Mask                                                     cBit9_0
#define cAf6_dej1_errins_en_cfg_TxDE1ErrInsID_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Payload Error Insert threshold Configuration
Reg Addr   : 0x00090003
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_errins_thr_cfg_Base                                                            0x00090003

/*--------------------------------------
BitField Name: TxDE1PayErrInsThr
BitField Type: RW
BitField Desc: Error Threshold in byte unit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_errins_thr_cfg_TxDE1PayErrInsThr_Mask                                               cBit31_0
#define cAf6_dej1_errins_thr_cfg_TxDE1PayErrInsThr_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion BOM Control
Reg Addr   : 0x097000-0x0977FF
Reg Formula: 0x097000 +  de3id*32 + de2id*4 + de1id
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_Base                                                  0x097000
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_WidthVal                                                    32

/*--------------------------------------
BitField Name: DlkBOMEn
BitField Type: RW
BitField Desc: enable transmitting BOM. By setting this field to 1, DLK engine
will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is
transmitted first. When transmitting finishes, engine automatically clears this
bit to inform to CPU that the BOM message has been already transmitted.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Mask                                              cBit10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Shift                                                 10

/*--------------------------------------
BitField Name: DlkBOMRptTime
BitField Type: RW
BitField Desc: indicate the number of time that the BOM is transmitted
repeatedly.
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Mask                                        cBit9_6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Shift                                             6

/*--------------------------------------
BitField Name: DlkBOMMsg
BitField Type: RW
BitField Desc: 6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the
MSB is transmitted first.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Mask                                            cBit5_0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 Control
Reg Addr   : 0x00080000 - 0x0008003F
Reg Formula: 0x00080000 +  de3id
    Where  : 
           + $de3id(0-47) HDL_PATH     : txm13e13.txm23e23.ctrlbufarb.membuf.array.ram[$de3id]
Reg Desc   : 
The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_ctrl_Base                                                                  0x00080000

/*--------------------------------------
BitField Name: TxDS3UnfrmAISMode
BitField Type: RW
BitField Desc: Set 1 to send out AIS signal pattern in DS3Unframe, default set 0
to send out AIS all 1's pattern in DS3Unframe
BitField Bits: [30]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3UnfrmAISMode_Mask                                                       cBit30
#define cAf6_txm23e23_ctrl_TxDS3UnfrmAISMode_Shift                                                          30

/*--------------------------------------
BitField Name: TxDS3E3FrcAllone
BitField Type: RW
BitField Desc: Force All One to remote only for Line Local Loopback(control bit5
of RX M23 0x40000)
BitField Bits: [29]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FrcAllone_Mask                                                        cBit29
#define cAf6_txm23e23_ctrl_TxDS3E3FrcAllone_Shift                                                           29

/*--------------------------------------
BitField Name: TxDS3E3ChEnb
BitField Type: RW
BitField Desc:
BitField Bits: [28]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Mask                                                            cBit28
#define cAf6_txm23e23_ctrl_TxDS3E3ChEnb_Shift                                                               28

/*--------------------------------------
BitField Name: TxDS3E3DLKMode
BitField Type: RW
BitField Desc: 0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Mask                                                       cBit27_26
#define cAf6_txm23e23_ctrl_TxDS3E3DLKMode_Shift                                                             26

/*--------------------------------------
BitField Name: TxDS3E3DLKEnb
BitField Type: RW
BitField Desc: Set 1 to enable DLK DS3
BitField Bits: [25]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Mask                                                           cBit25
#define cAf6_txm23e23_ctrl_TxDS3E3DLKEnb_Shift                                                              25

/*--------------------------------------
BitField Name: TxDS3E3FeacThres
BitField Type: RW
BitField Desc: Number of word repeat, 0xF for send continuos
BitField Bits: [24:21]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Mask                                                     cBit24_21
#define cAf6_txm23e23_ctrl_TxDS3E3FeacThres_Shift                                                           21

/*--------------------------------------
BitField Name: TxDS3E3LineAllOne
BitField Type: RW
BitField Desc:
BitField Bits: [20]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Mask                                                       cBit20
#define cAf6_txm23e23_ctrl_TxDS3E3LineAllOne_Shift                                                          20

/*--------------------------------------
BitField Name: TxDS3E3PayLoop
BitField Type: RW
BitField Desc:
BitField Bits: [19]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Mask                                                          cBit19
#define cAf6_txm23e23_ctrl_TxDS3E3PayLoop_Shift                                                             19

/*--------------------------------------
BitField Name: TxDS3E3LineLoop
BitField Type: RW
BitField Desc:
BitField Bits: [18]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Mask                                                         cBit18
#define cAf6_txm23e23_ctrl_TxDS3E3LineLoop_Shift                                                            18

/*--------------------------------------
BitField Name: TxDS3E3FrcYel
BitField Type: RW
BitField Desc:
BitField Bits: [17]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Mask                                                           cBit17
#define cAf6_txm23e23_ctrl_TxDS3E3FrcYel_Shift                                                              17

/*--------------------------------------
BitField Name: TxDS3E3AutoYel
BitField Type: RW
BitField Desc:
BitField Bits: [16]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Mask                                                          cBit16
#define cAf6_txm23e23_ctrl_TxDS3E3AutoYel_Shift                                                             16

/*--------------------------------------
BitField Name: TxDS3E3LoopMd
BitField Type: RW
BitField Desc: DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate
cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal:
Loop Mode configuration
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Mask                                                        cBit15_14
#define cAf6_txm23e23_ctrl_TxDS3E3LoopMd_Shift                                                              14

/*--------------------------------------
BitField Name: TxDS3E3LoopEn
BitField Type: RW
BitField Desc: DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]:
Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per
channel
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Mask                                                         cBit13_7
#define cAf6_txm23e23_ctrl_TxDS3E3LoopEn_Shift                                                               7

/*--------------------------------------
BitField Name: TxDS3E3Ohbypass
BitField Type: RW
BitField Desc:
BitField Bits: [6]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Mask                                                          cBit6
#define cAf6_txm23e23_ctrl_TxDS3E3Ohbypass_Shift                                                             6

/*--------------------------------------
BitField Name: TxDS3E3IdleSet
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Mask                                                           cBit5
#define cAf6_txm23e23_ctrl_TxDS3E3IdleSet_Shift                                                              5

/*--------------------------------------
BitField Name: TxDS3E3AisSet
BitField Type: RW
BitField Desc:
BitField Bits: [4]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Mask                                                            cBit4
#define cAf6_txm23e23_ctrl_TxDS3E3AisSet_Shift                                                               4

/*--------------------------------------
BitField Name: TxDS3E3Md
BitField Type: RW
BitField Desc: Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity
carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3
C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111:
E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Mask                                                              cBit3_0
#define cAf6_txm23e23_ctrl_TxDS3E3Md_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 Control2
Reg Addr   : 0x00080040 - 0x0008007F
Reg Formula: 0x00080040 +  de3id
    Where  : 
           + $de3id(0-47) HDL_PATH     : txm13e13.txm23e23.ctrlbufarb.membuf.array.ram[$de3id]
Reg Desc   : 
The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_ctrl2_Base                                                                 0x00080040

/*--------------------------------------
BitField Name: TxDS3E3AutoFEBE
BitField Type: RW
BitField Desc:
BitField Bits: [8]
--------------------------------------*/
#define cAf6_txm23e23_ctrl2_TxDS3E3AutoFEBE_Mask                                                         cBit8
#define cAf6_txm23e23_ctrl2_TxDS3E3AutoFEBE_Shift                                                            8

/*--------------------------------------
BitField Name: DS3Cbit_SSMEn
BitField Type: RW
BitField Desc: DS3 Cbit SSM message to transmit Enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_txm23e23_ctrl2_DS3Cbit_SSMEn_Mask                                                           cBit7
#define cAf6_txm23e23_ctrl2_DS3Cbit_SSMEn_Shift                                                              7

/*--------------------------------------
BitField Name: SSMInv
BitField Type: RW
BitField Desc: Set 1 to invert SSM - 1:invert mode , SSM format
11111111_11_0xxxxxx0 - 1:non_invert mode , SSM format 0xxxxxx0_11_11111111
BitField Bits: [6]
--------------------------------------*/
#define cAf6_txm23e23_ctrl2_SSMInv_Mask                                                                  cBit6
#define cAf6_txm23e23_ctrl2_SSMInv_Shift                                                                     6

/*--------------------------------------
BitField Name: SSMMess
BitField Type: RW
BitField Desc: SSM Message - [3:0]: G832 SSM Message - [5:0]: G832 SSM Message
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_txm23e23_ctrl2_SSMMess_Mask                                                               cBit5_0
#define cAf6_txm23e23_ctrl2_SSMMess_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : TxM23E23 HW Status
Reg Addr   : 0x00080080-0x000800BF
Reg Formula: 0x00080080 +  de3id
    Where  : 
           + $de3id(0-47):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_txm23e23_hw_stat_Base                                                               0x00080080
#define cAf6Reg_txm23e23_hw_stat_WidthVal                                                                   64

/*--------------------------------------
BitField Name: TxM23E23Status
BitField Type: RO
BitField Desc: for HW debug only
BitField Bits: [42:0]
--------------------------------------*/
#define cAf6_txm23e23_hw_stat_TxM23E23Status_01_Mask                                                  cBit31_0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_01_Shift                                                        0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_02_Mask                                                  cBit10_0
#define cAf6_txm23e23_hw_stat_TxM23E23Status_02_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : TxM12E12 Control
Reg Addr   : 0x00081000 - 0x000811FF
Reg Formula: 0x00081000 +  8*de3id + de2id
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6): HDL_PATH     : txm13e13.txm12e12.ctrlbufarb.membuf.array.ram[$de3id*8 + $de2id]
Reg Desc   : 
The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_txm12e12_ctrl_Base                                                                  0x00081000
#define cAf6Reg_txm12e12_ctrl_WidthVal                                                                      32

/*--------------------------------------
BitField Name: TxDS2E2Md
BitField Type: RW
BitField Desc: Tx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2
G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_txm12e12_ctrl_TxDS2E2Md_Mask                                                              cBit1_0
#define cAf6_txm12e12_ctrl_TxDS2E2Md_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : TxM12E12 HW Status
Reg Addr   : 0x00081200 - 0x000813FF
Reg Formula: 0x00081200 +  8*de3id + de2id
    Where  : 
           + $de3id(0-47):
           + $de2id(0-6):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_txm12e12_hw_stat_Base                                                               0x00081200

/*--------------------------------------
BitField Name: TxM12E12Status
BitField Type: RO
BitField Desc: for HW debug only
BitField Bits: [80:0]
--------------------------------------*/
#define cAf6_txm12e12_hw_stat_TxM12E12Status_01_Mask                                                  cBit31_0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_01_Shift                                                        0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_02_Mask                                                  cBit31_0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_02_Shift                                                        0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_03_Mask                                                  cBit16_0
#define cAf6_txm12e12_hw_stat_TxM12E12Status_03_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Config Limit error
Reg Addr   : 0xE_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
Check pattern error in state matching pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lim_err_Base                                                                      0xE0002

/*--------------------------------------
BitField Name: cfg_lim_err
BitField Type: RW
BitField Desc: if cnt err more than limit error,search failed
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_upen_lim_err_cfg_lim_err_Mask                                                             cBit3_0
#define cAf6_upen_lim_err_cfg_lim_err_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Config limit match
Reg Addr   : 0xE_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to check parttern match

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lim_mat_Base                                                                      0xE0003

/*--------------------------------------
BitField Name: cfg_lim_mat
BitField Type: RW
BitField Desc: if cnt match more than limit mat, serch ok
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_lim_mat_cfg_lim_mat_Mask                                                            cBit11_0
#define cAf6_upen_lim_mat_cfg_lim_mat_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Config thresh fdl parttern
Reg Addr   : 0xE_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
Check fdl message codes match

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_th_fdl_mat_Base                                                                   0xE0004

/*--------------------------------------
BitField Name: cfg_th_fdl_mat
BitField Type: RW
BitField Desc: if message codes repeat more than thresh, search ok,
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_Mask                                                       cBit7_0
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config thersh error
Reg Addr   : 0xE_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
Max error for one pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_th_err_Base                                                                       0xE0005

/*--------------------------------------
BitField Name: cfg_th_err
BitField Type: RW
BitField Desc: if pattern error more than thresh , change to check another
pattern
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_th_err_cfg_th_err_Mask                                                               cBit3_0
#define cAf6_upen_th_err_cfg_th_err_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Current Inband Code
Reg Addr   : 0xE_2100 - 0xE_27FF
Reg Formula: 0xE_2100 + id
    Where  : 
           + $id (0-1535):
Reg Desc   : 
Used to report current status of chid

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_info_Base                                                                         0xE2100

/*--------------------------------------
BitField Name: updo_sta
BitField Type: RO
BitField Desc: bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] ==
0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] ==
0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to nomal
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_info_updo_sta_Mask                                                                   cBit2_0
#define cAf6_upen_info_updo_sta_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Config User programmble Pattern User Codes
Reg Addr   : 0xE_08F8
Reg Formula: 
    Where  : 
Reg Desc   : 
check fdl message codes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fdlpat_Base                                                                       0xE08F8

/*--------------------------------------
BitField Name: updo_fdl_mess
BitField Type: RO
BitField Desc: mess codes
BitField Bits: [55:00]
--------------------------------------*/
#define cAf6_upen_fdlpat_updo_fdl_mess_01_Mask                                                        cBit31_0
#define cAf6_upen_fdlpat_updo_fdl_mess_01_Shift                                                              0
#define cAf6_upen_fdlpat_updo_fdl_mess_02_Mask                                                        cBit23_0
#define cAf6_upen_fdlpat_updo_fdl_mess_02_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Currenr Message FDL Detected
Reg Addr   : 0xE_1800 - 0xE_1FFF
Reg Formula: 0xE_1800 + $fdl_len
    Where  : 
           + $fdl_len (0-1535)
Reg Desc   : 
Info fdl message detected

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fdl_info_Base                                                                     0xE1800

/*--------------------------------------
BitField Name: mess_info_l
BitField Type: RO
BitField Desc: message info bit[15:8] : 8bit BOM pattern format 0xxxxxx0 bit[4]
: set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1
line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up
bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6
smartjact deact bit[3:0]== 0x7 idle
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_fdl_info_mess_info_l_Mask                                                           cBit15_0
#define cAf6_upen_fdl_info_mess_info_l_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Config Theshold Pattern Report
Reg Addr   : 0xE_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
Maximum Pattern Report

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_th_stt_int_Base                                                                   0xE0006

/*--------------------------------------
BitField Name: updo_th_stt_int
BitField Type: RW
BitField Desc: if number of pattern detected more than threshold, interrupt
enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_th_stt_int_updo_th_stt_int_Mask                                                     cBit31_0
#define cAf6_upen_th_stt_int_updo_th_stt_int_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RAM Parity Force Control
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Force_Control_Base                                                             0x10

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH VT Async Map Control"
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Mask                                  cBit12
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Shift                                      12

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Map Control"
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Mask                                    cBit11
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Shift                                       11

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Mask                                   cBit10
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Shift                                      10

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 Control"
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Mask                                     cBit9
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Shift                                        9

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM12E12 Control"
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Mask                                     cBit8
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Shift                                        8

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling
Control"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Mask                                     cBit7
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Shift                                        7

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Mask                                     cBit6
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Shift                                        6

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Mask                                     cBit5
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Shift                                        5

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM12E12 Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Mask                                     cBit4
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Shift                                        4

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxDS3E3 OH Pro Control"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Mask                                    cBit3
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Shift                                       3

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM23E23 Control"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Mask                                     cBit2
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Shift                                        2

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Demap Control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Mask                                   cBit1
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Shift                                       1

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Mask                                          cBit0
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : RAM Parity Disable Control
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Disable_Control_Base                                                           0x11

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH VT Async Map Control"
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrl_ParErrDis_Mask                                  cBit12
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrl_ParErrDis_Shift                                      12

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Map Control"
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrl_ParErrDis_Mask                                  cBit11
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrl_ParErrDis_Shift                                      11

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23Trace_ParErrDis_Mask                                  cBit10
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23Trace_ParErrDis_Shift                                      10

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 Control"
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23Ctrl_ParErrDis_Mask                                   cBit9
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23Ctrl_ParErrDis_Shift                                       9

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM12E12 Control"
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12Ctrl_ParErrDis_Mask                                   cBit8
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12Ctrl_ParErrDis_Shift                                       8

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrl_ParErrDis_Mask                                   cBit7
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrl_ParErrDis_Shift                                       7

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrl_ParErrDis_Mask                                   cBit6
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrl_ParErrDis_Shift                                       6

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrl_ParErrDis_Mask                                   cBit5
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrl_ParErrDis_Shift                                       5

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM12E12 Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12Ctrl_ParErrDis_Mask                                   cBit4
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12Ctrl_ParErrDis_Shift                                       4

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrl_ParErrDis_Mask                                   cBit3
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrl_ParErrDis_Shift                                       3

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM23E23 Control"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23Ctrl_ParErrDis_Mask                                   cBit2
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23Ctrl_ParErrDis_Shift                                       2

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Demap Control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrl_ParErrDis_Shift                                       1

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParErrDis_Mask                                        cBit0
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParErrDis_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Error Sticky
Reg Addr   : 0x12
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Error_Sticky_Base                                                              0x12

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH VT Async Map Control"
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_ParErrStk_Mask                                   cBit12
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_ParErrStk_Shift                                      12

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH STS/VT Map Control"
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_ParErrStk_Mask                                     cBit11
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_ParErrStk_Shift                                        11

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Mask                                    cBit10
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Shift                                       10

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH TxM23E23 Control"
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_ParErrStk_Mask                                      cBit9
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_ParErrStk_Shift                                         9

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH TxM12E12 Control"
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_ParErrStk_Mask                                      cBit8
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_ParErrStk_Shift                                         8

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling
Control"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_ParErrStk_Mask                                      cBit7
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_ParErrStk_Shift                                         7

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_ParErrStk_Mask                                      cBit6
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_ParErrStk_Shift                                         6

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_ParErrStk_Mask                                      cBit5
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_ParErrStk_Shift                                         5

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH RxM12E12 Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_ParErrStk_Mask                                      cBit4
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_ParErrStk_Shift                                         4

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH RxDS3E3 OH Pro Control"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_ParErrStk_Mask                                     cBit3
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_ParErrStk_Shift                                        3

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH RxM23E23 Control"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_ParErrStk_Mask                                      cBit2
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_ParErrStk_Shift                                         2

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH STS/VT Demap Control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_ParErrStk_Mask                                    cBit1
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_ParErrStk_Shift                                       1

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_ParErrStk_Mask                                           cBit0
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_ParErrStk_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Rx Slip Enable
Reg Addr   : 0x00060000-0x000603FF
Reg Formula: 0x00060000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-47)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
These registers are used to control Rx Slip

------------------------------------------------------------------------------*/
#define cAf6Reg_rxslipbuf_pen_Base                                                                  0x00060000

/*--------------------------------------
BitField Name: rxslpmd
BitField Type: RO
BitField Desc: RX Slip Mode - 2'b00,2'b10: E1 - 2'b01: SF - 2'b11: ESF
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_rxslipbuf_pen_rxslpmd_Mask                                                                cBit2_1
#define cAf6_rxslipbuf_pen_rxslpmd_Shift                                                                     1

/*--------------------------------------
BitField Name: rxslpen
BitField Type: RO
BitField Desc: Rx Slip Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxslipbuf_pen_rxslpen_Mask                                                                  cBit0
#define cAf6_rxslipbuf_pen_rxslpen_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Rx Slip Counter
Reg Addr   : 0x00061000-0x000617FF
Reg Formula: 0x00061000 + 2048*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-47)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This is the per channel Rx Slip event occur

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrm_rei_cnt_Base                                                                  0x00061000
#define cAf6Reg_rxfrm_rei_cnt_WidthVal                                                                      32

/*--------------------------------------
BitField Name: RxSlipCnt
BitField Type: RW
BitField Desc: Rx Slip Counter
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_rxfrm_rei_cnt_RxSlipCnt_Mask                                                             cBit15_0
#define cAf6_rxfrm_rei_cnt_RxSlipCnt_Shift                                                                   0

#endif /* _AF6_REG_AF6CNC0022_RD_PDH_v3_H_ */

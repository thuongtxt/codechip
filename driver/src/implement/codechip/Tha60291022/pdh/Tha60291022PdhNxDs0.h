/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60291022PdhNxDs0.h
 * 
 * Created Date: Oct 11, 2018
 *
 * Description : 60291022 PDH NxDs0 interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022PDHNXDS0_H_
#define _THA60291022PDHNXDS0_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhNxDS0 Tha60291022PdhNxDs0New(AtPdhDe1 de1, uint32 mask);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291022PDHNXDS0_H_ */


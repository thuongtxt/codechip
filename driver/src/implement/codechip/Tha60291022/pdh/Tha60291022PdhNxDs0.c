/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60291022PdhNxDs0.c
 *
 * Created Date: Oct 11, 2018
 *
 * Description : 60291022 PDH NxDs0 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022PdhNxDs0Internal.h"
#include "Tha60291022ModulePdhReg.h"
#include "Tha60291022PdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60291011PdhNxDs0Methods    m_Tha60291011PdhNxDs0Override;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift;
    }

static void OverrideTha60291011PdhNxDs0(AtPdhNxDS0 self)
    {
    Tha60291011PdhNxDs0 object = (Tha60291011PdhNxDs0)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60291011PdhNxDs0Override, mMethodsGet(object), sizeof(m_Tha60291011PdhNxDs0Override));

        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask);
        mMethodOverride(m_Tha60291011PdhNxDs0Override, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift);
        }

    mMethodsSet(object, &m_Tha60291011PdhNxDs0Override);
    }

static void Override(AtPdhNxDS0 self)
    {
    OverrideTha60291011PdhNxDs0(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022PdhNxDs0);
    }

static AtPdhNxDS0 ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (Tha60291011PdhNxDs0ObjectInit(self, de1, mask) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhNxDS0 Tha60291022PdhNxDs0New(AtPdhDe1 de1, uint32 mask)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhNxDS0 newDs0 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDs0 == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDs0, de1, mask);
    }

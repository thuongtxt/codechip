/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60291022InternalRamPla.c
 *
 * Created Date: Aug 28, 2018
 *
 * Description : Internal RAM for PLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../../Tha60290022/ram/Tha60290022InternalRamInternal.h"
#include "Tha60291022InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60291022InternalRamPla
    {
    tTha60290022InternalRamPla super;
    }tTha60291022InternalRamPla;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/* Super implementations */
static const tThaInternalRamMethods *m_ThaInternalRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(ThaInternalRam self)
    {
    return (ThaModulePwe)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static eBool RamIsPla(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < 12)
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsPsn(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < Tha60210011ModulePweStartHSPWRamLocalId((Tha60210011ModulePwe)ModulePwe(self)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PlaSliceOffsetGet(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)(0x10000 * (localId / 3));
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 ramId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (RamIsPla(self))
        {
        static const uint32 cellAddressOfPla[] = {0x82000, 0x86000, cInvalidUint32};
        return cellAddressOfPla[ramId % 3] + PlaSliceOffsetGet(self);
        }

    if (RamIsPsn(self))
        {
        static const uint32 cellAddressOfPla[] = {0xC0000, 0x48000};
        return cellAddressOfPla[ramId % 2];
        }

    return 0;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);

    if (offset == 0)
        return m_ThaInternalRamMethods->FirstCellAddress(self);

    return (uint32)(ThaModulePwePlaBaseAddress(ModulePwe(self)) + offset);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022InternalRamPla);
    }
    
static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam overrideObject = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaInternalRamMethods = mMethodsGet(overrideObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, m_ThaInternalRamMethods, sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(overrideObject, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022InternalRamPlaObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60291022InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, phyModule, ramId, localId);
    }
     

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60291022ModulePdaInternal.h
 * 
 * Created Date: Jul 24, 2018
 *
 * Description : 60291022 PDA internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291022MODULEPDAINTERNAL_H_
#define _THA60291022MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pda/Tha60290022ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291022ModulePda
    {
    tTha60290022ModulePda super;
    }tTha60291022ModulePda;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291022MODULEPDAINTERNAL_H_ */


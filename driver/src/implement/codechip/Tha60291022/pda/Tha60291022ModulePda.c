/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60291022ModulePda.c
 *
 * Created Date: Jul 24, 2018
 *
 * Description : 60291022 PDA implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291022ModulePdaInternal.h"
#include "Tha60291022ModulePdaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;
static tTha60290022ModulePdaMethods m_Tha60290022ModulePdaOverride;

/* Save super implementation */
static const tThaModulePdaMethods         *m_ThaModulePdaMethods = NULL;
static const tTha60210011ModulePdaMethods *m_Tha60210011ModulePdaMethods = NULL;
static const tTha60290022ModulePdaMethods *m_Tha60290022ModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PWPdaTdmModeCtrlAbsoluteAddress(ThaModulePda self, AtPw adapter)
    {
    return mMethodsGet(self)->PWPdaTdmModeCtrl(self, adapter) + mTdmOffset(self, adapter);
    }

static eAtRet CESoPModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    AtPwCESoP cesop = (AtPwCESoP)ThaPwAdapterPwGet(adapter);
    eAtPwCESoPMode cesopMode = AtPwCESoPModeGet(cesop);
    uint8 hwMode = (uint8)((cesopMode == cAtPwCESoPModeWithCas) ? 3 : 1);
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, hwMode);
    }

static uint32 CesopCasPwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit)
    {
    uint32 regAddr, regVal;
    AtPdhDe1 de1 = NULL;
    uint8 numTs = 0;
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)circuit;
    uint8 isE1 = 0;

    de1 = AtPdhNxDS0De1Get(nxds0);
    isE1 = (uint8)(AtPdhDe1IsE1(de1) ? 1 : 0);
    numTs = AtPdhNxDs0NumTimeslotsGet(nxds0);
    regAddr = PWPdaTdmModeCtrlAbsoluteAddress(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDAStsId5_1_Or_Nds0_, numTs);
    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDAStsId0_Or_E1CasEn_, isE1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit)
    {
    eAtRet ret = m_ThaModulePdaMethods->PwCircuitTypeSet(self, pw, circuit);
    eAtPwType pwType = 0;
    AtPw realPw = ThaPwAdapterPwGet((ThaPwAdapter)pw);

    if (ret == cAtOk)
        {
        pwType = AtPwTypeGet(realPw);
        if (pwType == cAtPwTypeCESoP)
            {
            eAtPwCESoPMode cesopMode = AtPwCESoPModeGet((AtPwCESoP)realPw);
            if (cesopMode == cAtPwCESoPModeWithCas)
                return CesopCasPwCircuitTypeSet(self, pw, circuit);
            }
        }

    return ret;
    }

static eAtRet PwDefaultHwConfigure(Tha60210011ModulePda self, AtPw pwAdapter)
    {
    eAtRet ret = m_Tha60210011ModulePdaMethods->PwDefaultHwConfigure(self, pwAdapter);
    eAtPwType pwType = 0;
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 casEnable = 0;
    AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);

    if (ret != cAtOk)
        return ret;

    pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeCESoP)
        {
        eAtPwCESoPMode cesopMode = AtPwCESoPModeGet((AtPwCESoP)pw);
        if (cesopMode == cAtPwCESoPModeWithCas)
            casEnable = 1;
        }

    address = ThaModulePdaV2JitBufCtrlAddress((ThaModulePdaV2)self, pwAdapter);
    mChannelHwLongRead(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cAf6_ramjitbufcfg_PwCasMode_, casEnable);
    mChannelHwLongWrite(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 Oc192cTdmLookupCtrl(Tha60290022ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramtdm192lkupcfg_Base;
    }

static uint32 Oc192cTdmModeCtrl(Tha60290022ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramtdmmode192cfg_Base;
    }

static void OverrideTha60290022ModulePda(AtModule self)
    {
    Tha60290022ModulePda module = (Tha60290022ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModulePdaMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModulePdaOverride, m_Tha60290022ModulePdaMethods, sizeof(m_Tha60290022ModulePdaOverride));

        mMethodOverride(m_Tha60290022ModulePdaOverride, Oc192cTdmLookupCtrl);
        mMethodOverride(m_Tha60290022ModulePdaOverride, Oc192cTdmModeCtrl);
        }

    mMethodsSet(module, &m_Tha60290022ModulePdaOverride);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, CESoPModeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitTypeSet);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, m_Tha60210011ModulePdaMethods, sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, PwDefaultHwConfigure);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePda(self);
    OverrideTha60210011ModulePda(self);
    OverrideTha60290022ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291022ModulePda);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291022ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

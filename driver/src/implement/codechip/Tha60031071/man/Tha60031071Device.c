/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60031071Device.c
 *
 * Created Date: Jan 13, 2015
 *
 * Description : Product 60031071
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031071Device
    {
    tThaPdhPwProductDevice super;
    }tTha60031071Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031071Device);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId  == cAtModuleEth) return (AtModule)Tha60031071ModuleEthNew(self);
    if (moduleId  == cAtModulePdh) return (AtModule)Tha60031071ModulePdhNew(self);
    if (moduleId  == cAtModuleRam) return (AtModule)Tha60031071ModuleRamNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
	AtUnused(self);
    /* TODO: */
    return NULL;
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    /* TODO: updated sskey checker */
    return cAtFalse;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60031071DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

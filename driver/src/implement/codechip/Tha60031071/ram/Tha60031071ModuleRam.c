/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60031071ModuleRam.c
 *
 * Created Date: Jan 13, 2015
 *
 * Description : RAM module of product 60031071
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/ram/ThaPdhPwProductModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031071ModuleRam
    {
    tThaPdhPwProductModuleRam super;
    }tTha60031071ModuleRam;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods m_AtModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumDdrGet(AtModuleRam self)
    {
	AtUnused(self);
    return 1;
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031071ModuleRam);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleRamObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60031071ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

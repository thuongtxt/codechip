/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : ThaPdhPwProductModulePwe.c
 *
 * Created Date: Jun 10, 2013
 *
 * Description : PWE module of PDH CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pwe/ThaModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods   m_ThaModulePweOverride;
static tThaModulePweV2Methods m_ThaModulePweV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductModulePwe);
    }

static uint32 PwTxEnCtrl(ThaModulePwe self)
    {
	AtUnused(self);
    return 0x321100;
    }

static uint32 PwRamStatusRegister(ThaModulePweV2 self)
    {
	AtUnused(self);
    return 0x321000;
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, mMethodsGet(pweModule), sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, PwRamStatusRegister);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwTxEnCtrl);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    OverrideThaModulePweV2(self);
    }

AtModule ThaPdhPwProductModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePweV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaPdhPwProductModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductModulePweObjectInit(newModule, device);
    }

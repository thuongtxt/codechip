/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaPdhPwProductModuleClockInternal.h
 * 
 * Created Date: May 6, 2014
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTMODULECLOCKINTERNAL_H_
#define _THAPDHPWPRODUCTMODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/clock/ThaModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductModuleClock
    {
    tThaModuleClock super;
    }tThaPdhPwProductModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock ThaPdhPwProductModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTMODULECLOCKINTERNAL_H_ */


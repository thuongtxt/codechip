/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhPwProductClockExtractor.c
 *
 * Created Date: Sep 10, 2013
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/clock/ThaClockExtractorInternal.h"
#include "../../../../default/pdh/ThaPdhDe1.h"
#include "../../ThaStmPwProduct/clock/ThaStmPwProductClockExtractor.h"
#include "ThaPdhPwProductModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cCDRReference8KOutputControl      0x2A0000
#define cCdrRefPdhTypeMask(extractorId)  (((extractorId) == 0) ? cBit3 : cBit19)
#define cCdrRefPdhTypeShift(extractorId) (((extractorId) == 0) ? 3 : 19)
#define cCdrRefModeMask(extractorId)     (((extractorId) == 0) ? cBit2_0 : cBit18_16)
#define cCdrRefModeShift(extractorId)    (((extractorId) == 0) ? 0 : 16)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPdhPwProductClockExtractor
    {
    tThaClockExtractor super;
    }tThaPdhPwProductClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/* Save super implementation */
static const tAtClockExtractorMethods *m_AtClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductClockExtractor);
    }

static uint32 ReferenceSync8KOutputControl(ThaClockExtractor self)
    {
    AtUnused(self);
    return cCDRReference8KOutputControl;
    }

static uint32 CdrRefIdMask(ThaClockExtractor self)
    {
    ThaModuleClock ModuleClock = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    return mMethodsGet(ModuleClock)->RefIdMask(ModuleClock, (AtClockExtractor)self);
    }

static uint32 CdrRefIdShift(ThaClockExtractor self)
    {
    ThaModuleClock ModuleClock = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    return mMethodsGet(ModuleClock)->RefIdShift(ModuleClock, (AtClockExtractor)self);
    }

static eAtModuleClockRet HwPdhDe1LiuClockExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), AtPdhDe1IsE1(de1) ? 0 : 1);
    mFieldIns(&regVal, CdrRefIdMask(self), CdrRefIdShift(self), AtChannelIdGet((AtChannel)de1));
    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 1); /* Loop timing mode (or LIU) */

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eAtModuleClockRet HwSystemClockExtract(ThaClockExtractor self)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), 0);
    mFieldIns(&regVal, CdrRefIdMask(self), CdrRefIdShift(self), 0);
    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 0); /* System */

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eBool SystemClockIsExtracted(AtClockExtractor self)
    {
    uint8 extractMode;
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);
    uint32 regVal = AtClockExtractorRead(self, ReferenceSync8KOutputControl((ThaClockExtractor)self));
    mFieldGet(regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), uint8, &extractMode);
    return (extractMode == 0) ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    return ThaPwCommonProductClockExtractEnable((ThaClockExtractor)self, enable);
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    return ThaPwCommonProductClockExtractIsEnabled((ThaClockExtractor)self);
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtTrue;
    }

static eBool CanExtractPdhDe1LiuClock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtTrue;
    }

static eAtModuleClockRet De1AcrDcrTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 2); /* CDR timing mode */
    mFieldIns(&regVal, CdrRefIdMask(self), CdrRefIdShift(self), ThaCdrControllerIdGet(ThaPdhDe1CdrControllerGet((ThaPdhDe1)de1)));
    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), AtPdhDe1IsE1(de1) ? 0 : 1);

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eAtModuleClockRet De1LoopTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 1); /* Loop timing mode */
    mFieldIns(&regVal, CdrRefIdMask(self), CdrRefIdShift(self), AtChannelIdGet((AtChannel)de1));
    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), AtPdhDe1IsE1(de1) ? 0 : 1);

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1Clock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1LiuClock);

        mMethodOverride(m_ThaClockExtractorOverride, HwSystemClockExtract);

        mMethodOverride(m_ThaClockExtractorOverride, HwPdhDe1LiuClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De1AcrDcrTimingModeExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De1LoopTimingModeExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtClockExtractorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, SystemClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor ThaPdhPwProductClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

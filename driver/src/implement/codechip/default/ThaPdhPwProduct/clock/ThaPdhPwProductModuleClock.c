/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhPwProductModuleClock.c
 *
 * Created Date: Sep 10, 2013
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtClockExtractor.h"
#include "ThaPdhPwProductModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cCdrRefIdMask(extractorId)       (((extractorId) == 0) ? cBit8_4 : cBit24_20)
#define cCdrRefIdShift(self)             (((extractorId) == 0) ? 4 : 20)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

/* Save super implementation */
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductModuleClock);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
	AtUnused(self);
    return 1;
    }

static uint32 RefIdMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 extractorId = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return cCdrRefIdMask(extractorId);
    }

static uint32 RefIdShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 extractorId = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return cCdrRefIdShift(extractorId);
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return ThaPdhPwProductClockExtractorNew((AtModuleClock)self, extractorId);
    }

static uint8 ExtractorPart(ThaModuleClock self, uint8 extractorId)
    {
	AtUnused(extractorId);
	AtUnused(self);
    return 0;
    }

static eAtModuleClockRet ClockExtractorInit(ThaModuleClock self, AtClockExtractor extractor)
    {
	AtUnused(self);
    return AtClockExtractorEnable(extractor, cAtFalse);
    }

static ThaClockMonitor ClockMonitorCreate(ThaModuleClock self)
    {
    return ThaStmPwProductClockMonitorNew((AtModuleClock)self);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        mMethodOverride(m_ThaModuleClockOverride, ExtractorPart);
        mMethodOverride(m_ThaModuleClockOverride, ClockExtractorInit);
        mMethodOverride(m_ThaModuleClockOverride, ClockMonitorCreate);
        mMethodOverride(m_ThaModuleClockOverride, RefIdMask);
        mMethodOverride(m_ThaModuleClockOverride, RefIdShift);

        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    }

AtModuleClock ThaPdhPwProductModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock ThaPdhPwProductModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductModuleClockObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaPdhPwProductDe1CdrController.c
 *
 * Created Date: Jun 19, 2013
 *
 * Description : DE1 CDR controller of common PDH CES product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhPwProductDe1CdrController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet RxNcoPackLenSet(ThaCdrController self, uint32 packetLength)
    {
	AtUnused(packetLength);
	AtUnused(self);
    /* This configuration is not applicable for this product */
    return cAtOk;
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, mMethodsGet(self), sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, RxNcoPackLenSet);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductDe1CdrController);
    }

ThaCdrController ThaPdhPwProductDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDe1CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaPdhPwProductDe1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductDe1CdrControllerObjectInit(newController, engineId, channel);
    }

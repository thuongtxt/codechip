/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaPdhPwProductDe1CdrController.h
 * 
 * Created Date: May 15, 2015
 *
 * Description : CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTDE1CDRCONTROLLER_H_
#define _THAPDHPWPRODUCTDE1CDRCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cdr/controllers/ThaCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductDe1CdrController
    {
    tThaDe1CdrController super;
    }tThaPdhPwProductDe1CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaPdhPwProductDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTDE1CDRCONTROLLER_H_ */


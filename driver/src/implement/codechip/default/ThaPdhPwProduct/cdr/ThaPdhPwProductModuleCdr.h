/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaPdhPwProductModuleCdr.h
 * 
 * Created Date: Oct 31, 2013
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTMODULECDR_H_
#define _THAPDHPWPRODUCTMODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cdr/ThaModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductModuleCdr
    {
    tThaModuleCdr super;
    }tThaPdhPwProductModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaPdhPwProductModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTMODULECDR_H_ */


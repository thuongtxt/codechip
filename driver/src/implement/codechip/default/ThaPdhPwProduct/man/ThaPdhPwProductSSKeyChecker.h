/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhPwProductSSKeyChecker.h
 * 
 * Created Date: Oct 16, 2013
 *
 * Description : SSKey checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTSSKEYCHECKER_H_
#define _THAPDHPWPRODUCTSSKEYCHECKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/man/ThaSSKeyCheckerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductSSKeyChecker
    {
    tThaSSKeyCheckerPwV2 super;
    }tThaPdhPwProductSSKeyChecker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSSKeyChecker ThaPdhPwProductSSKeyCheckerObjectInit(AtSSKeyChecker self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTSSKEYCHECKER_H_ */


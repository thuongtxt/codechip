/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhPwProductDeviceInternal.h
 * 
 * Created Date: Jun 18, 2013
 *
 * Description : PDH CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTDEVICEINTERNAL_H_
#define _THAPDHPWPRODUCTDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"
#include "../../../../ThaPdhPw/man/ThaPdhPwDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductDevice
    {
    tThaPdhPwDeviceV2 super;
    }tThaPdhPwProductDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaPdhPwProductDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTDEVICEINTERNAL_H_ */


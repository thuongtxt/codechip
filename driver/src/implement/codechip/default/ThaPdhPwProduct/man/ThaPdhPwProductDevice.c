/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaPdhPwProductDevice.c
 *
 * Created Date: May 6, 2013
 *
 * Description : PDH CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRam.h"
#include "AtModuleClock.h"
#include "ThaPdhPwProductDeviceInternal.h"
#include "../../../../default/util/ThaUtil.h"
#include "../../../../default/man/ThaIpCore.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../ThaStmPwProduct/man/ThaStmPwProductDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductDevice);
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
    AtUnused(self);
    return 1;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtDevice self, AtModule module)
    {
    AtUnused(self);
    AtUnused(module);
    return NULL;
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
    AtUnused(self);
    return Tha32BitGlobalLongRegisterAccessNew();
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleRam,
                                                 cAtModuleEth,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Private modules */
    if (phyModule == cThaModulePda)  return ThaPdhPwProductModulePdaNew(self);
    if (phyModule == cThaModulePwe)  return ThaPdhPwProductModulePweNew(self);
    if (phyModule == cThaModuleCla)  return ThaPdhPwProductModuleClaNew(self);
    if (phyModule == cThaModuleCdr)  return ThaPdhPwProductModuleCdrNew(self);

    /* Public modules */
    if (moduleId  == cAtModuleRam)   return (AtModule)ThaPdhPwProductModuleRamNew(self);
    if (moduleId  == cAtModulePdh)   return (AtModule)ThaPdhPwProductModulePdhNew(self);
    if (moduleId  == cAtModulePw)    return (AtModule)ThaPdhPwProductModulePwNew(self);
    if (moduleId  == cAtModuleClock) return (AtModule)ThaPdhPwProductModuleClockNew(self);
    if (moduleId  == cAtModuleEth)   return (AtModule)ThaPdhPwProductModuleEthNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)ThaStmPwProductModulePktAnalyzerNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
    return ThaIpCorePwV2New(coreId, self);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return ThaPdhPwProductSSKeyCheckerNew(self);
    }

static uint32 PllClkSysStatReg(ThaDevice self)
    {
    return ThaPwCommonProductPllClkSysStatReg(self);
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    return ThaPwCommonProductPllClkSysStatBitMask(self);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModuleClock)
        return cAtTrue;
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, NumIpCoresGet);
        mMethodOverride(m_AtDeviceOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, IpCoreCreate);
        mMethodOverride(m_AtDeviceOverride, GlobalLongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatReg);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

AtDevice ThaPdhPwProductDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwDeviceV2ObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice ThaPdhPwProductDeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductDeviceObjectInit(newDevice, driver, productCode);
    }

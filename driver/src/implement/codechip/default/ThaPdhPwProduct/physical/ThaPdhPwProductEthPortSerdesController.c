/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaPdhPwProductEthPortSerdesController.c
 *
 * Created Date: Oct 17, 2013
 *
 * Description : ETH port SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhPwProductEthPortSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPwProductEthPortSerdesControllerMethods m_ThaStmPwProductEthPortSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductEthPortSerdesController);
    }

static uint32 PllRegister(ThaStmPwProductEthPortSerdesController self)
    {
	AtUnused(self);
    return 0xf00053;
    }

static void OverrideThaStmPwProductEthPortSerdesController(AtSerdesController self)
    {
    ThaStmPwProductEthPortSerdesController controller = (ThaStmPwProductEthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductEthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaStmPwProductEthPortSerdesControllerOverride));

        mMethodOverride(m_ThaStmPwProductEthPortSerdesControllerOverride, PllRegister);
        }

    mMethodsSet(controller, &m_ThaStmPwProductEthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideThaStmPwProductEthPortSerdesController(self);
    }

AtSerdesController ThaPdhPwProductEthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPortSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaPdhPwProductEthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductEthPortSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

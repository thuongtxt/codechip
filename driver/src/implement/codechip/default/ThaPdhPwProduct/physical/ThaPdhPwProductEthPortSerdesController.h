/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : ThaPdhPwProductEthPortSerdesController.h
 * 
 * Created Date: Jul 27, 2015
 *
 * Description : ETH port SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_DEFAULT_THAPDHPWPRODUCT_PHYSICAL_THAPDHPWPRODUCTETHPORTSERDESCONTROLLER_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_DEFAULT_THAPDHPWPRODUCT_PHYSICAL_THAPDHPWPRODUCTETHPORTSERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../ThaStmPwProduct/physical/ThaStmPwProductEthPortSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductEthPortSerdesController
    {
    tThaStmPwProductEthPortSerdesController super;
    }tThaPdhPwProductEthPortSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController ThaPdhPwProductEthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_DEFAULT_THAPDHPWPRODUCT_PHYSICAL_THAPDHPWPRODUCTETHPORTSERDESCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPdhPwProductModulePw.h
 * 
 * Created Date: Feb 19, 2014
 *
 * Description : PW module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTMODULEPW_H_
#define _THAPDHPWPRODUCTMODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/ThaModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductModulePw
    {
    tThaModulePw super;
    }tThaPdhPwProductModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw ThaPdhPwProductModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTMODULEPW_H_ */


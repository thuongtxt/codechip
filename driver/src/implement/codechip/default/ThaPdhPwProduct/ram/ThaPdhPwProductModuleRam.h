/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaPdhPwProductModuleRam.h
 * 
 * Created Date: Jul 1, 2014
 *
 * Description : RAM module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTMODULERAM_H_
#define _THAPDHPWPRODUCTMODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../ThaStmPwProduct/ram/ThaStmPwProductModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductModuleRam
    {
    tThaStmPwProductModuleRam super;
    }tThaPdhPwProductModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam ThaPdhPwProductModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTMODULERAM_H_ */


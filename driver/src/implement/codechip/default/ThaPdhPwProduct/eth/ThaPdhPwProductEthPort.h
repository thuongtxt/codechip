/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaPdhPwProductEthPort.h
 * 
 * Created Date: Apr 7, 2015
 *
 * Description : ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTETHPORT_H_
#define _THAPDHPWPRODUCTETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/eth/ThaEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaPdhPwProductEthPortSwitch(ThaEthPort self, ThaEthPort toPort);
eAtRet ThaPdhPwProductEthPortSwitchRelease(ThaEthPort self);
AtEthPort ThaPdhPwProductEthPortBridgedPortGet(AtEthPort self);
eAtRet ThaPdhPwProductEthPortZeroCounters(AtChannel self, void *pAllCounters);
eBool ThaPdhPwProductEthPortPortIsActive(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTETHPORT_H_ */


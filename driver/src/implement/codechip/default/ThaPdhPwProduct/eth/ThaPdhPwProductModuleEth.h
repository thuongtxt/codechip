/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaPdhPwProductModuleEth.h
 * 
 * Created Date: Oct 17, 2013
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTMODULEETH_H_
#define _THAPDHPWPRODUCTMODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductModuleEth
    {
    tThaStmPwProductModuleEth super;
    }tThaPdhPwProductModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth ThaPdhPwProductModuleEthObjectInit(AtModuleEth self, AtDevice device);

uint32 ThaPdhPwProductEthPortDefectGet(AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTMODULEETH_H_ */


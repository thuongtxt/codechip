/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaPdhPwProductModuleEth.c
 *
 * Created Date: Oct 17, 2013
 *
 * Description : ETH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhPwProductModuleEth.h"
#include "../../../../default/physical/ThaSerdesController.h"
#include "../../../../default/eth/ThaEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegEthPortStatus                0xf00063
#define cThaRegEthPortStatusMask(portId)    (cBit4 << (portId))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    AtUnused(self);
    return ThaPdhPwProductEthPortSerdesControllerNew(ethPort, AtChannelIdGet((AtChannel)ethPort));
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 2;
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return ThaPdhPwProductEthPortNew(portId, self);
    }

static eBool ApsIsSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool HasSerdesControllers(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, HasSerdesControllers);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, ApsIsSupported);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductModuleEth);
    }

AtModuleEth ThaPdhPwProductModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth ThaPdhPwProductModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductModuleEthObjectInit(newModule, device);
    }

uint32 ThaPdhPwProductEthPortDefectGet(AtEthPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    uint32 regVal = mChannelHwRead(port, cThaRegEthPortStatus, cAtModuleEth);
    return (regVal & cThaRegEthPortStatusMask(portId)) ? cAtEthPortAlarmLinkUp : cAtEthPortAlarmLinkDown;
    }

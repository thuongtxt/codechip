/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaPdhPwProductEthPortInternal.h
 * 
 * Created Date: Apr 25, 2014
 *
 * Description : ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTETHPORTINTERNAL_H_
#define _THAPDHPWPRODUCTETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/eth/ThaEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductEthPort
    {
    tThaEthPort super;
    }tThaPdhPwProductEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort ThaPdhPwProductEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTETHPORTINTERNAL_H_ */


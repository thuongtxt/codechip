/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaPdhPwProductEthPort.c
 *
 * Created Date: Jan 3, 2014
 *
 * Description : ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/ThaModuleEth.h"
#include "ThaPdhPwProductEthPortInternal.h"
#include "ThaPdhPwProductModuleEth.h"
#include "ThaPdhPwProductEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegEthPortApsGlobalModeControl  0xf00002
#define cThaRegEthPortApsSwitchMask         cBit0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPortMethods  m_AtEthPortOverride;
static tAtChannelMethods  m_AtChannelOverride;
static tThaEthPortMethods m_ThaEthPortOverride;

static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtEthPort self)
    {
    return (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eAtRet HwSwitch(ThaEthPort self, ThaEthPort toPort)
    {
    return ThaPdhPwProductEthPortSwitch(self, toPort);
    }

static eAtRet HwSwitchRelease(ThaEthPort self)
    {
    return ThaPdhPwProductEthPortSwitchRelease(self);
    }

static eAtModuleEthRet Bridge(AtEthPort self, AtEthPort toPort)
    {
	AtUnused(toPort);
	AtUnused(self);
    return cAtOk;
    }

static AtEthPort BridgedPortGet(AtEthPort self)
    {
    return ThaPdhPwProductEthPortBridgedPortGet(self);
    }

/* For direction from TDM to PSN, hardware always bridge traffic on two GE ports */
static eAtModuleEthRet BridgeRelease(AtEthPort self)
    {
	AtUnused(self);
    return cAtError;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    if (ThaPdhPwProductEthPortPortIsActive(self))
        return m_AtChannelMethods->AllCountersGet(self, pAllCounters);
    else
        return ThaPdhPwProductEthPortZeroCounters(self, pAllCounters);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    if (ThaPdhPwProductEthPortPortIsActive(self))
        return m_AtChannelMethods->AllCountersClear(self, pAllCounters);
    else
        return ThaPdhPwProductEthPortZeroCounters(self, pAllCounters);
    }

static uint32 DefectGet(AtChannel self)
    {
    return ThaPdhPwProductEthPortDefectGet((AtEthPort)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductEthPort);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(ethPort);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, HwSwitch);
        mMethodOverride(m_ThaEthPortOverride, HwSwitchRelease);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, Bridge);
        mMethodOverride(m_AtEthPortOverride, BridgedPortGet);
        mMethodOverride(m_AtEthPortOverride, BridgeRelease);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtEthPort(self);
    OverrideAtChannel(self);
    OverrideThaEthPort(self);
    }

AtEthPort ThaPdhPwProductEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort ThaPdhPwProductEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductEthPortObjectInit(newPort, portId, module);
    }

eAtRet ThaPdhPwProductEthPortSwitch(ThaEthPort self, ThaEthPort toPort)
    {
    uint32 selectedPortId = AtChannelIdGet((AtChannel)toPort);
    mChannelHwWrite(self, cThaRegEthPortApsGlobalModeControl, (selectedPortId == 0) ? 0 : 1, cAtModuleEth);

    return cAtOk;
    }

eAtRet ThaPdhPwProductEthPortSwitchRelease(ThaEthPort self)
    {
    uint32 selectedPortId = AtChannelIdGet((AtChannel)self);
    mChannelHwWrite(self, cThaRegEthPortApsGlobalModeControl, (selectedPortId == 0) ? 0 : 1, cAtModuleEth);

    return cAtOk;
    }

AtEthPort ThaPdhPwProductEthPortBridgedPortGet(AtEthPort self)
    {
    uint32 portId = AtChannelIdGet((AtChannel)self);
    return AtModuleEthPortGet(EthModule(self), (portId == 0) ? 1 : 0);
    }

eAtRet ThaPdhPwProductEthPortZeroCounters(AtChannel self, void *pAllCounters)
    {
    tAtEthPortCounters *counters = (tAtEthPortCounters *)pAllCounters;
	AtUnused(self);

    if (pAllCounters)
        AtOsalMemInit(counters, 0, sizeof(tAtEthPortCounters));

    return cAtOk;
    }

eBool ThaPdhPwProductEthPortPortIsActive(AtChannel self)
    {
    uint32 selectedPort;

    if (!ThaModuleEthApsIsSupported((ThaModuleEth)EthModule((AtEthPort)self)))
        return cAtTrue;

    selectedPort = mChannelHwRead(self, cThaRegEthPortApsGlobalModeControl, cAtModuleEth) & cThaRegEthPortApsSwitchMask;
    return (AtChannelIdGet(self) == selectedPort) ? cAtTrue : cAtFalse;
    }

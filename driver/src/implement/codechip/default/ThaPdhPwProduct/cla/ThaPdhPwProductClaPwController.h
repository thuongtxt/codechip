/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaPdhPwProductClaPwController.h
 * 
 * Created Date: Dec 11, 2013
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTCLAPWCONTROLLER_H_
#define _THAPDHPWPRODUCTCLAPWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../ThaStmPwProduct/cla/ThaStmPwProductClaPwController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductClaPwController
    {
    tThaStmPwProductClaPwController super;
    }tThaPdhPwProductClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController ThaPdhPwProductClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTCLAPWCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaPdhPwProductClaPwController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhPwProductClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPwProductClaPwControllerMethods m_ThaStmPwProductClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PortIdFromCVlan(ThaStmPwProductClaPwController self, tAtEthVlanTag *cVlan)
    {
	AtUnused(cVlan);
	AtUnused(self);
    return 0;
    }

static AtModulePw PwModule(ThaStmPwProductClaPwController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eAtRet LookupTableReset(ThaStmPwProductClaPwController self)
    {
    uint32 numPws = AtModulePwMaxPwsGet(PwModule(self));
    uint32 entry_i;

    for (entry_i = 0; entry_i < numPws; entry_i++)
        ThaClaControllerWrite((ThaClaController)self, cThaRegPwVlanLookup + entry_i, numPws - 1);

    return cAtOk;
    }

static void OverrideThaStmPwProductClaPwController(ThaClaPwController self)
    {
    ThaStmPwProductClaPwController controller = (ThaStmPwProductClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductClaPwControllerOverride, mMethodsGet(controller), sizeof(m_ThaStmPwProductClaPwControllerOverride));

        mMethodOverride(m_ThaStmPwProductClaPwControllerOverride, PortIdFromCVlan);
        mMethodOverride(m_ThaStmPwProductClaPwControllerOverride, LookupTableReset);
        }

    mMethodsSet(controller, &m_ThaStmPwProductClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaStmPwProductClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductClaPwController);
    }

ThaClaPwController ThaPdhPwProductClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController ThaPdhPwProductClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductClaPwControllerObjectInit(newController, cla);
    }

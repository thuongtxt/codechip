/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module CLA
 * 
 * File        : ThaPdhPwProductModuleCla.h
 * 
 * Created Date: Sep 9, 2013
 *
 * Description : Module CLA of PDH CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWPRODUCTMODULECLA_H_
#define _THAPDHPWPRODUCTMODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../ThaStmPwProduct/cla/ThaStmPwProductModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwProductModuleCla
    {
    tThaStmPwProductModuleCla super;
    }tThaPdhPwProductModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaPdhPwProductModuleClaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWPRODUCTMODULECLA_H_ */


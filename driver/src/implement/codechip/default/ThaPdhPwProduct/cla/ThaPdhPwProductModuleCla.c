/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaPdhPwProductModuleCla.c
 *
 * Created Date: Jul 8, 2013
 *
 * Description : CLA module of product PDH CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhPwProductModuleCla.h"
#include "../../../../default/cla/pw/ThaModuleClaPwV2Reg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods     m_ThaModuleClaOverride;
static tThaModuleClaPwV2Methods m_ThaModuleClaPwV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlCdrDisMask;
    }

static uint8  CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlCdrDisShift;
    }

static uint32 CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlLineIdMask;
    }

static uint8 CDRPwLookupCtrlLineIdShift(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlLineIdShift;
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return ThaPdhPwProductClaPwControllerNew(self);
    }

static void OverrideThaModuleClaPwV2(AtModule self)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisShift);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdShift);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwV2Override);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPwV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductModuleCla);
    }

AtModule ThaPdhPwProductModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaPdhPwProductModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPwProductModuleClaObjectInit(newModule, device);
    }

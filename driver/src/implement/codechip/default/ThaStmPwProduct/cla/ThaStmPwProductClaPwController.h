/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaPdhStmProductClaPwController.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA PW controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTCLAPWCONTROLLER_H_
#define _THASTMPWPRODUCTCLAPWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cla/controllers/ThaClaPwControllerInternal.h"
#include "ThaStmPwProductModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductClaPwController * ThaStmPwProductClaPwController;

typedef struct tThaStmPwProductClaPwControllerMethods
    {
    uint8 (*PortIdFromCVlan)(ThaStmPwProductClaPwController self, tAtEthVlanTag *cVlan);
    eAtRet (*LookupTableReset)(ThaStmPwProductClaPwController self);
    uint16 (*LocalChannelIdFromCVlan)(ThaStmPwProductClaPwController self, tAtEthVlanTag *tag);
    }tThaStmPwProductClaPwControllerMethods;

typedef struct tThaStmPwProductClaPwController
    {
    tThaClaPwControllerV2 super;
    const tThaStmPwProductClaPwControllerMethods *methods;
    }tThaStmPwProductClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController ThaStmPwProductClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /*_THASTMPWPRODUCTCLAPWCONTROLLER_H_ */


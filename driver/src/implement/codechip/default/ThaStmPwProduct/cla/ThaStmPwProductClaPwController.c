/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaPdhStmProductClaPwController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cla/pw/ThaModuleClaPwV2Reg.h"
#include "../../../../default/cla/ThaModuleCla.h"
#include "../../../../default/cla/pw/ThaModuleClaPw.h"
#include "../../../../default/cla/hbce/ThaHbce.h"
#include "../../../../default/sdh/ThaModuleSdh.h"
#include "../../../../default/pw/ThaModulePw.h"

#include "../eth/ThaStmPwProductModuleEth.h"
#include "ThaStmPwProductModuleCla.h"
#include "ThaStmPwProductClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaStmPwProductClaPwController)self)
#define mPwPartOffset(self, pw) ThaClaPwControllerPartOffset((ThaClaPwController)self, pw)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)
#define mDevice(self) AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPwProductClaPwControllerMethods m_methods;

/* Override */
static tThaClaControllerMethods   m_ThaClaControllerOverride;
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/* Save super implementation */
static const tThaClaControllerMethods   *m_ThaClaControllerMethods   = NULL;
static const tThaClaPwControllerMethods *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePw PwModule(ThaClaPwController self)
    {
    return (ThaModulePw)AtDeviceModuleGet(mDevice(self), cAtModulePw);
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return ThaStmPwProductHbceNew(hbceId, (AtModule)ThaClaControllerModuleGet((ThaClaController)self), core);
    }

static eAtRet PartPwLookupModeSet(ThaClaPwController self, uint8 partId, eThaPwLookupMode lookupMode)
    {
    ThaClaController controller = (ThaClaController)self;
    uint32 regAddr = ThaClaControllerClaGlbPsnCtrl(controller) + ThaModuleClaPartOffset(ThaClaControllerModuleGet(controller), partId);
    uint32 regVal  = mClaControllerRead(self, regAddr);
    mRegFieldSet(regVal, cThaRegClaVlanTagModeEn, (lookupMode == cThaPwLookupMode2Vlans) ? 1 : 0);
    mClaControllerWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eThaPwLookupMode PartPwLookupModeGet(ThaClaPwController self, uint8 partId)
    {
    ThaClaController controller = (ThaClaController)self;
    uint32 regAddr = ThaClaControllerClaGlbPsnCtrl(controller) + ThaModuleClaPartOffset(ThaClaControllerModuleGet(controller), partId);
    uint32 regVal  = mClaControllerRead(self, regAddr);
    return (regVal & cThaRegClaVlanTagModeEnMask) ? cThaPwLookupMode2Vlans : cThaPwLookupModePsn;
    }

static eBool PwLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    if (lookupMode == cThaPwLookupMode2Vlans)
        return cAtTrue;

    return m_ThaClaPwControllerMethods->PwLookupModeIsSupported(self, pw, lookupMode);
    }

static eAtRet PwLookupModeSet(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    return PartPwLookupModeSet(self, ThaModulePwPartOfPw(PwModule(self), pw), lookupMode);
    }

static eThaPwLookupMode PwLookupModeGet(ThaClaPwController self, AtPw pw)
    {
    uint8 partId = (uint8)((pw) ? ThaModulePwPartOfPw(PwModule(self), pw) : 0);
    return ThaClaPwControllerPartPwLookupModeGet(self, partId);
    }

static eAtRet DefaultPwLookupModeSet(ThaClaPwController self, eThaPwLookupMode lookupMode)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet((ThaClaController)self)); part_i++)
        {
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        ret |= PartPwLookupModeSet(self, part_i, lookupMode);
        }

    return ret;
    }

static eAtRet PartLookupTableReset(ThaClaController self, uint8 partId)
    {
    uint8 line_i;
    uint32 partOffset = ThaModuleClaPartOffset(ThaClaControllerModuleGet(self), partId);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(mDevice(self), cAtModuleSdh);
    static const uint32 cNumEntriesPerStm = 256;

    /* Note, although there are 4 lines for each part, but this
     * lookup table is for 8 ports for each part. Need to reset all of them */
    for (line_i = 0; line_i < AtModuleSdhMaxLinesGet(sdhModule); line_i++)
        {
        uint16 entry_i;
        for (entry_i = 0; entry_i < cNumEntriesPerStm; entry_i++)
            {
            uint32 regAddr = cThaRegPwVlanLookup + (line_i * cNumEntriesPerStm) + entry_i + partOffset;
            mClaControllerWrite(self, regAddr, 0);
            }
        }

    return cAtOk;
    }

static eAtRet LookupTableReset(ThaStmPwProductClaPwController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet((ThaClaController)self)); part_i++)
        {
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        ret |= PartLookupTableReset((ThaClaController)self, part_i);
        }

    return ret;
    }

static eAtRet DefaultSet(ThaClaController self)
    {
    eAtRet ret = cAtOk;
    eThaPwLookupMode defaultMode = Tha60031031CommonDefaultPwLookupModeGet((ThaStmPwProductModuleCla)(ThaClaControllerModuleGet(self)));
    ret |= DefaultPwLookupModeSet((ThaClaPwController)self, defaultMode);
    ret |= mMethodsGet(mThis(self))->LookupTableReset(mThis(self));

    return ret;
    }

static eAtRet PwLabelSet(ThaClaPwController self, AtPw pw, AtPwPsn psn)
    {
    eAtRet ret = cAtOk;

    ret  = m_ThaClaPwControllerMethods->PwLabelSet(self, pw, psn);
    ret |= ThaModulePwLookupModeSet(PwModule(self), pw, cThaPwLookupModePsn);

    return ret;
    }

static uint16 LocalChannelIdFromCVlan(ThaStmPwProductClaPwController self, tAtEthVlanTag *tag)
    {
	AtUnused(self);
    return tag->vlanId & cBit7_0;
    }

static uint32 VlanLookupOffset(ThaStmPwProductClaPwController self, tAtEthVlanTag *vlanTag)
    {
    uint8 port = mMethodsGet(self)->PortIdFromCVlan(self, vlanTag);
    uint16 localChannelId = mMethodsGet(self)->LocalChannelIdFromCVlan(self, vlanTag);

    return (port * 256UL) + localChannelId;
    }

static uint8 PortIdFromCVlan(ThaStmPwProductClaPwController self, tAtEthVlanTag *cVlan)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(mDevice(self), cAtModuleEth);
    return ThaStmPwProductModuleEthPortIdFromCVlan(ethModule, cVlan);
    }

static eAtRet PwLookupReset(ThaClaPwController self, AtPw pw)
    {
    tAtEthVlanTag cVlan;
    uint32 regAddr;

    if (AtPwEthExpectedCVlanGet(pw, &cVlan) == NULL)
        return cAtOk;

    regAddr = cThaRegPwVlanLookup + mPwPartOffset(self, pw) + VlanLookupOffset(mThis(self), &cVlan);
    mChannelHwWrite(pw, regAddr, 0, cThaModuleCla);

    return cAtOk;
    }

static eBool PwLookupEntryIsEnabled(ThaClaPwController self, uint8 partId, tAtEthVlanTag *cVlan)
    {
    uint32 regAddr;

    regAddr = cThaRegPwVlanLookup +
              ThaModuleClaPartOffset(ThaClaControllerModuleGet((ThaClaController)self), partId) +
              VlanLookupOffset(mThis(self), cVlan);

    return (mClaControllerRead(self, regAddr) & cThaRegPwVlanLookupEnableMask) ? cAtTrue : cAtFalse;
    }

static eBool ExpectedCVlanTagNeedsToBeChanged(ThaClaPwController self, AtPw pw, tAtEthVlanTag *newCVlanTag)
    {
    tAtEthVlanTag currentTag;
    uint16 newChannelId, currentChannelId;
    uint8 newPortId, currentPortId;

    if (AtPwEthExpectedCVlanGet(pw, &currentTag) == NULL)
        return cAtTrue;

    currentPortId    = PortIdFromCVlan(mThis(self), &currentTag);
    currentChannelId = mMethodsGet(mThis(self))->LocalChannelIdFromCVlan(mThis(self), &currentTag);
    newPortId        = PortIdFromCVlan(mThis(self), newCVlanTag);
    newChannelId     = mMethodsGet(mThis(self))->LocalChannelIdFromCVlan(mThis(self), newCVlanTag);
    if ((newPortId == currentPortId) && (newChannelId == currentChannelId))
        return cAtFalse;

    return cAtTrue;
    }

static eBool LookupEntryIsBusy(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan)
    {
    if (AtDeviceWarmRestoreIsStarted(mDevice(self)))
        return cAtFalse;

    if (PwLookupEntryIsEnabled(self, ThaModulePwPartOfPw(PwModule(self), pw), cVlan))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwExpectedCVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan)
    {
    uint32 regAddr, regVal;
    eAtRet ret;

    /* Always switch to mode lookup by VLAN in all cases */
    ret = ThaClaPwControllerLookupModeSet(self, pw, cThaPwLookupMode2Vlans);
    if (ret != cAtOk)
        return ret;

    if (!ExpectedCVlanTagNeedsToBeChanged(self, pw, cVlan))
        return cAtOk;

    /* This entry may be busy for other PW */
    if (LookupEntryIsBusy(self, pw, cVlan))
        return cAtErrorResourceBusy;

    PwLookupReset(self, pw);
    regAddr = cThaRegPwVlanLookup + mPwPartOffset(self, pw) + VlanLookupOffset(mThis(self), cVlan);
    regVal = mPwHwId(pw);
    mRegFieldSet(regVal, cThaRegPwVlanLookupEnable, 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwExpectedSVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtPw pw = (AtPw)channel;

    regAddr = ThaClaPwControllerV2PwTypeCtrlAddress((ThaClaPwControllerV2)self, pw) + mClaPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[cThaRegClaPwEnableDwordId], cThaRegClaPwEnable, enable ? 1 : 0);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    /* Enable/disable HBCE */
    ThaPwAdapterHbceEnable(pw, enable);

    return cAtOk;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtPw pw = (AtPw)channel;

    /* Ask HBCE first */
    if (ThaClaPwControllerHbceGet((ThaClaPwController)self, pw) && ThaPwAdapterHbceIsEnabled(pw))
        return cAtTrue;

    /* HBCE may not exist, ask this module itself */
    regAddr = ThaClaPwControllerV2PwTypeCtrlAddress((ThaClaPwControllerV2)self, pw) + mClaPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (longRegVal[cThaRegClaPwEnableDwordId] & cThaRegClaPwEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwLookupRemove(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware)
    {
    if (ThaClaPwControllerLookupModeGet(self, pwAdapter) == cThaPwLookupModePsn)
        return m_ThaClaPwControllerMethods->PwLookupRemove(self, pwAdapter, applyHardware);

    if (!applyHardware)
        return cAtOk;

    return PwLookupReset(self, pwAdapter);
    }

static eAtRet PwLookupEnable(ThaClaPwController self, AtPw pwAdapter, eBool enable)
    {
    uint32 regAddr, regVal;
    tAtEthVlanTag cVlan, *pCVlan;

    if (ThaClaPwControllerLookupModeGet(self, pwAdapter) == cThaPwLookupModePsn)
        return m_ThaClaPwControllerMethods->PwLookupEnable(self, pwAdapter, enable);

    pCVlan = AtPwEthExpectedCVlanGet(pwAdapter, &cVlan);
    if (pCVlan == NULL)
        return cAtOk;

    regAddr = cThaRegPwVlanLookup + mPwPartOffset(self, pwAdapter) + VlanLookupOffset(mThis(self), pCVlan);
    regVal = mPwHwId(pwAdapter);
    mRegFieldSet(regVal, cThaRegPwVlanLookupEnable, enable ? 1 : 0);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool PwExpectedVlanTagsConflict(ThaClaPwController self,
                                        tAtEthVlanTag *expectedCVlan1, tAtEthVlanTag *expectedSVlan1,
                                        tAtEthVlanTag *expectedCVlan2, tAtEthVlanTag *expectedSVlan2)
    {
    uint8 port1, port2;
    uint16 localChannelId1, localChannelId2;
	AtUnused(expectedSVlan2);
	AtUnused(expectedSVlan1);

    if ((expectedCVlan1 == NULL) || (expectedCVlan2 == NULL))
        return cAtFalse;

    port1           = mMethodsGet(mThis(self))->PortIdFromCVlan(mThis(self), expectedCVlan1);
    port2           = mMethodsGet(mThis(self))->PortIdFromCVlan(mThis(self), expectedCVlan2);
    localChannelId1 = mMethodsGet(mThis(self))->LocalChannelIdFromCVlan(mThis(self), expectedCVlan1);
    localChannelId2 = mMethodsGet(mThis(self))->LocalChannelIdFromCVlan(mThis(self), expectedCVlan2);

    if ((port1 == port2) && (localChannelId1 == localChannelId2))
        return cAtTrue;

    return cAtFalse;
    }

static void MethodsInit(ThaStmPwProductClaPwController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PortIdFromCVlan);
        mMethodOverride(m_methods, LookupTableReset);
        mMethodOverride(m_methods, LocalChannelIdFromCVlan);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, DefaultSet);
        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        mMethodOverride(m_ThaClaControllerOverride, ChannelIsEnabled);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    ThaClaPwController claController = (ThaClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(claController);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(claController), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeIsSupported);
        mMethodOverride(m_ThaClaPwControllerOverride, PartPwLookupModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLabelSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwExpectedCVlanSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwExpectedSVlanSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwExpectedVlanTagsConflict);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupRemove);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupEnable);
        }

    mMethodsSet(claController, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaController(self);
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductClaPwController);
    }

ThaClaPwController ThaStmPwProductClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaPwControllerV2ObjectInit((ThaClaPwController)self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController ThaStmPwProductClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductClaPwControllerObjectInit(newController, cla);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaPdhStmProductModuleClaReg.h
 * 
 * Created Date: Jul 11, 2013
 *
 * Description : Additional CLA registers of STM CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULECLAREG_H_
#define _THASTMPWPRODUCTMODULECLAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* Address : 0x44C000 + stm_id*256 + chan_no */
#define cThaRegPwVlanLookup 0x44C000
#define cThaRegPwVlanLookupEnableMask  cBit12
#define cThaRegPwVlanLookupEnableShift 12

/* Bit 44 of "Classify Pseudowire Type Control" */
#define cThaRegClaPwEnableMask     cBit12
#define cThaRegClaPwEnableShift    12
#define cThaRegClaPwEnableDwordId  1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULECLAREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaPdhStmProductModuleCla.c
 *
 * Created Date: Jun 20, 2013
 *
 * Description : CLA module of STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPwProductModuleClaMethods m_methods;

/* Override */
static tThaModuleClaMethods   m_ThaModuleClaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return ThaStmPwProductClaPwControllerNew(self);
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return ThaStmPwProductClaPwEthPortControllerV2New(self);
    }

static eThaPwLookupMode DefaultPwLookupModeGet(ThaStmPwProductModuleCla self)
    {
    AtUnused(self);
    return cThaPwLookupModePsn;
    }

static void MethodsInit(AtModule self)
    {
    ThaStmPwProductModuleCla module = (ThaStmPwProductModuleCla)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultPwLookupModeGet);
        }

    mMethodsSet(module, &m_methods);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModuleCla);
    }

AtModule ThaStmPwProductModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClaStmPwV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaStmPwProductModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaStmPwProductModuleClaObjectInit(newModule, device);
    }

eThaPwLookupMode Tha60031031CommonDefaultPwLookupModeGet(ThaStmPwProductModuleCla self)
    {
    return (self) ? mMethodsGet(self)->DefaultPwLookupModeGet(self) : cThaPwLookupModePsn;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaPdhStmProductModuleCla.h
 * 
 * Created Date: Apr 2, 2015
 *
 * Description : CLA module of STM CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULECLA_H_
#define _THASTMPWPRODUCTMODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cla/pw/ThaModuleClaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModuleCla * ThaStmPwProductModuleCla;

typedef struct tThaStmPwProductModuleClaMethods
    {
    eThaPwLookupMode (*DefaultPwLookupModeGet)(ThaStmPwProductModuleCla self);
    }tThaStmPwProductModuleClaMethods;

typedef struct tThaStmPwProductModuleCla
    {
    tThaModuleClaStmPwV2 super;
    const tThaStmPwProductModuleClaMethods *methods;
    }tThaStmPwProductModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaStmPwProductModuleClaObjectInit(AtModule self, AtDevice device);

eThaPwLookupMode Tha60031031CommonDefaultPwLookupModeGet(ThaStmPwProductModuleCla self);

#ifdef __cplusplus
}
#endif
#endif /*_THASTMPWPRODUCTMODULECLA_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaStmPwProductModulePdh.h
 * 
 * Created Date: Apr 3, 2015
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULEPDH_H_
#define _THASTMPWPRODUCTMODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pdh/ThaModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModulePdh
    {
    tThaStmModulePdh super;
    }tThaStmPwProductModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh ThaStmPwProductModulePdhObjectInit(AtModulePdh self, AtDevice device);

eBool ThaPwProductTxDe1AutoAisSwAutoControl(ThaModulePdh self);
eBool ThaPwProductTxDe1AutoAisByDefault(ThaModulePdh self);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULEPDH_H_ */


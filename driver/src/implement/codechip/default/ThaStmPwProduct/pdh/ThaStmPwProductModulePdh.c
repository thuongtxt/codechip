/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaStmPwProductCommonModulePdh.c
 *
 * Created Date: Mar 16, 2015
 *
 * Description : PDH common module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdhMethods m_ThaModulePdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModulePdh);
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    AtUnused(self);
    AtUnused(partId);
    return cAtFalse;
    }

static eBool MFASChecked(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(module), sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, MFASChecked);
        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideThaModulePdh(self);
    }

AtModulePdh ThaStmPwProductModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh ThaStmPwProductModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaStmPwProductModulePdhObjectInit(newModule, device);
    }

eBool ThaPwProductTxDe1AutoAisSwAutoControl(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

eBool ThaPwProductTxDe1AutoAisByDefault(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }


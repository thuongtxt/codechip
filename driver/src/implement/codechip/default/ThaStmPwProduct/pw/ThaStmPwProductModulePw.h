/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : ThaPdhStmProductModulePw.h
 * 
 * Created Date: Jul 8, 2013
 *
 * Description : PW module of STM CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULEPW_H_
#define _THASTMPWPRODUCTMODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/ThaModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModulePw
    {
    tThaModulePw super;
    }tThaStmPwProductModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw ThaStmPwProductModulePwObjectInit(AtModulePw self, AtDevice device);

eBool ThaCommonPwProductUseLbitPacketsForAcrDcr(ThaModulePw self);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULEPW_H_ */


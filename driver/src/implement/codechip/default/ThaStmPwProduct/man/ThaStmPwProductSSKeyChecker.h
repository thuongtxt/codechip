/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhStmProductSSKeyChecker.h
 * 
 * Created Date: Oct 16, 2013
 *
 * Description : SSKey checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTSSKEYCHECKER_H_
#define _THASTMPWPRODUCTSSKEYCHECKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/man/ThaSSKeyCheckerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductSSKeyChecker
    {
    tThaSSKeyCheckerPwV2 super;
    }tThaStmPwProductSSKeyChecker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSSKeyChecker ThaStmPwProductSSKeyCheckerObjectInit(AtSSKeyChecker self, AtDevice device);
AtSSKeyChecker ThaStmPwProductSSKeyCheckerNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTSSKEYCHECKER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaStmPwProductDevice.h
 * 
 * Created Date: Apr 2, 2015
 *
 * Description : Default device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTDEVICE_H_
#define _THASTMPWPRODUCTDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"
#include "../../../../ThaStmPw/man/ThaStmPwDeviceInternal.h"
#include "../../../../default/util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductDevice
    {
    tThaStmPwDeviceV2 super;
    }tThaStmPwProductDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaStmPwProductDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

uint32 ThaPwCommonProductPllClkSysStatReg(ThaDevice self);
uint32 ThaPwCommonProductPllClkSysStatBitMask(ThaDevice self);

#ifdef __cplusplus
}
#endif
#endif /*_THASTMPWPRODUCTDEVICE_H_ */


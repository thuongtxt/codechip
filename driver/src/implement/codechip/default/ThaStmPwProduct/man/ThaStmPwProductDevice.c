/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaPdhStmProductDevice.c
 *
 * Created Date: May 6, 2013
 *
 * Description : STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRam.h"
#include "AtModuleClock.h"
#include "ThaStmPwProductDevice.h"
#include "ThaStmPwProductSSKeyChecker.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductDevice);
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
	AtUnused(self);
    return 1;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtDevice self, AtModule module)
    {
	AtUnused(module);
	AtUnused(self);
    return NULL;
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
	AtUnused(self);
    return Tha32BitGlobalLongRegisterAccessNew();
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;
    switch (moduleValue)
        {
        case cAtModulePdh:         return cAtTrue;
        case cAtModulePw:          return cAtTrue;
        case cAtModuleEth:         return cAtTrue;
        case cAtModuleRam:         return cAtTrue;
        case cAtModuleSdh:         return cAtTrue;
        case cAtModuleClock:       return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;

        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleRam)         return (AtModule)ThaStmPwProductModuleRamNew(self);
    if (moduleId  == cAtModulePw)          return (AtModule)ThaStmPwProductModulePwNew(self);
    if (moduleId  == cAtModuleSdh)         return (AtModule)ThaStmPwProductModuleSdhNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)ThaStmPwProductModuleEthNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)ThaStmPwProductModulePktAnalyzerNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)ThaStmPwProductModuleClockNew(self);
    if (moduleId  == cAtModulePdh)         return (AtModule)ThaStmPwProductModulePdhNew(self);

    if (phyModule == cThaModuleCla) return ThaStmPwProductModuleClaNew(self);
    if (phyModule == cThaModuleCdr) return ThaStmPwProductModuleCdrNew(self);
    if (phyModule == cThaModulePda) return ThaStmPwProductModulePdaNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 MaxNumParts(ThaDevice self)
    {
    return ThaDeviceIsEp(self) ? 1 : 2;
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(moduleId);

    if (ThaDeviceIsEp(self))
        return 1;

    return ThaDeviceMaxNumParts(self);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return ThaStmPwProductSSKeyCheckerNew(self);
    }

static uint32 PllClkSysStatReg(ThaDevice self)
    {
    return ThaPwCommonProductPllClkSysStatReg(self);
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    return ThaPwCommonProductPllClkSysStatBitMask(self);
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return 0x14032700;
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatReg);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        mMethodOverride(m_ThaDeviceOverride, CanHaveNewOcn);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, NumIpCoresGet);
        mMethodOverride(m_AtDeviceOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, GlobalLongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

AtDevice ThaStmPwProductDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwDeviceV2ObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint32 ThaPwCommonProductPllClkSysStatReg(ThaDevice self)
    {
	AtUnused(self);
    return 0xf0000c;
    }

uint32 ThaPwCommonProductPllClkSysStatBitMask(ThaDevice self)
    {
	AtUnused(self);
    return cBit0;
    }


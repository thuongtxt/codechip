/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60031031ModulePda.h
 * 
 * Created Date: Jun 25, 2014
 *
 * Description : PDA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULEPDA_H_
#define _THASTMPWPRODUCTMODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../ThaStmPw/pda/ThaStmPwModulePdaV2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModulePda
    {
    tThaStmPwModulePdaV2 super;
    }tThaStmPwProductModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaStmPwProductModulePdaObjectInit(AtModule self, AtDevice device);

#endif /* _THASTMPWPRODUCTMODULEPDA_H_ */


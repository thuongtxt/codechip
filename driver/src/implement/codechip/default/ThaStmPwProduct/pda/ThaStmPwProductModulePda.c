/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : ThaPdhStmProductModulePda.c
 *
 * Created Date: May 22, 2013
 *
 * Description : PDA module of STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductModulePda.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods   m_ThaModulePdaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool JitterBufferCenterIsSupported(ThaModulePda self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModulePda);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, JitterBufferCenterIsSupported);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePda(self);
    }

AtModule ThaStmPwProductModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwModulePdaV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaStmPwProductModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductModulePdaObjectInit(newModule, device);
    }

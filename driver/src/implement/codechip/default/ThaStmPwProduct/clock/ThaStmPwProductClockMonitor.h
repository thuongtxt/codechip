/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLOCK
 * 
 * File        : ThaPdhStmProductClockMonitor.h
 * 
 * Created Date: May 6, 2014
 *
 * Description : Clock Monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTCLOCKMONITORINTERNAL_H_
#define _THASTMPWPRODUCTCLOCKMONITORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/clock/ThaClockMonitorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductClockMonitor
    {
    tThaClockMonitor super;
    }tThaStmPwProductClockMonitor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClockMonitor ThaStmPwProductClockMonitorObjectInit(ThaClockMonitor self, AtModuleClock clockModule);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTCLOCKMONITORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhStmProductClockMonitor.c
 *
 * Created Date: Nov 22, 2013
 *
 * Description : Clock monitor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/man/AtDriverInternal.h"
#include "ThaStmPwProductClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

#define cClockMonitoring_1 0xF0006F
#define cClockMonitoring_2 0xF0006E
#define cClockMonitoring_3 0xF0006D
#define cClockMonitoring_4 0xF0006C

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClockMonitorMethods m_ThaClockMonitorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DdrClockCheckRegister(ThaClockMonitor self, uint32 ddrId)
    {
	AtUnused(self);
    if (ddrId == 0)
        return cClockMonitoring_1;
    if ((ddrId == 1) || (ddrId == 2))
        return cClockMonitoring_2;

    return cInvalidValue;
    }

static uint32 DdrClockCheckMask(ThaClockMonitor self, uint32 ddrId)
    {
	AtUnused(self);
    if (ddrId == 0) return cBit31_16;
    if (ddrId == 1) return cBit15_0;
    if (ddrId == 2) return cBit31_16;

    return cInvalidValue;
    }

static uint32 DdrClockCheckShift(ThaClockMonitor self, uint32 ddrId)
    {
	AtUnused(self);
    if (ddrId == 0) return 16;
    if (ddrId == 1) return 0;
    if (ddrId == 2) return 16;

    return cInvalidValue;
    }

static uint32 SystemClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    return cClockMonitoring_1;
    }

static uint32 SystemClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    return cBit15_0;
    }

static uint32 SystemClockCheckShift(ThaClockMonitor self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 OcnNumClocks(ThaClockMonitor self)
    {
	AtUnused(self);
    return 2;
    }

static uint32 OcnClockCheckRegister(ThaClockMonitor self, uint32 clockId)
    {
	AtUnused(clockId);
	AtUnused(self);
    return cClockMonitoring_3;
    }

static uint32 OcnClockCheckMask(ThaClockMonitor self, uint32 clockId)
    {
	AtUnused(self);
    if (clockId == 0) return cBit15_0;
    if (clockId == 1) return cBit31_16;

    return cInvalidValue;
    }

static uint32 OcnClockCheckShift(ThaClockMonitor self, uint32 clockId)
    {
	AtUnused(self);
    if (clockId == 0) return 0;
    if (clockId == 1) return 16;

    return cInvalidValue;
    }

static uint32 GeClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    return cClockMonitoring_4;
    }

static uint32 GeClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    return cBit15_0;
    }

static uint32 GeClockCheckShift(ThaClockMonitor self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 LocalBusClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    return cClockMonitoring_4;
    }

static uint32 LocalBusClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    return cBit31_16;
    }

static uint32 LocalBusClockCheckShift(ThaClockMonitor self)
    {
	AtUnused(self);
    return 16;
    }

static uint32 *LocalBusClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {50000, 33320};
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static void OverrideThaClockMonitor(ThaClockMonitor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockMonitorOverride, mMethodsGet(self), sizeof(m_ThaClockMonitorOverride));

        /* DDR clock checking */
        mMethodOverride(m_ThaClockMonitorOverride, DdrClockCheckRegister);
        mMethodOverride(m_ThaClockMonitorOverride, DdrClockCheckMask);
        mMethodOverride(m_ThaClockMonitorOverride, DdrClockCheckShift);

        /* System clock checking */
        mMethodOverride(m_ThaClockMonitorOverride, SystemClockCheckRegister);
        mMethodOverride(m_ThaClockMonitorOverride, SystemClockCheckMask);
        mMethodOverride(m_ThaClockMonitorOverride, SystemClockCheckShift);

        /* OCN clock checking */
        mMethodOverride(m_ThaClockMonitorOverride, OcnNumClocks);
        mMethodOverride(m_ThaClockMonitorOverride, OcnClockCheckRegister);
        mMethodOverride(m_ThaClockMonitorOverride, OcnClockCheckMask);
        mMethodOverride(m_ThaClockMonitorOverride, OcnClockCheckShift);

        /* Local bus clock checking */
        mMethodOverride(m_ThaClockMonitorOverride, LocalBusClockCheckRegister);
        mMethodOverride(m_ThaClockMonitorOverride, LocalBusClockCheckMask);
        mMethodOverride(m_ThaClockMonitorOverride, LocalBusClockCheckShift);
        mMethodOverride(m_ThaClockMonitorOverride, LocalBusClockExpectedFrequenciesInKhz);

        /* GE clock checking */
        mMethodOverride(m_ThaClockMonitorOverride, GeClockCheckRegister);
        mMethodOverride(m_ThaClockMonitorOverride, GeClockCheckMask);
        mMethodOverride(m_ThaClockMonitorOverride, GeClockCheckShift);
        }

    mMethodsSet(self, &m_ThaClockMonitorOverride);
    }

static void Override(ThaClockMonitor self)
    {
    OverrideThaClockMonitor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductClockMonitor);
    }

ThaClockMonitor ThaStmPwProductClockMonitorObjectInit(ThaClockMonitor self, AtModuleClock clockModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClockMonitorObjectInit(self, clockModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClockMonitor ThaStmPwProductClockMonitorNew(AtModuleClock clockModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClockMonitor newMonitor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMonitor == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductClockMonitorObjectInit(newMonitor, clockModule);
    }

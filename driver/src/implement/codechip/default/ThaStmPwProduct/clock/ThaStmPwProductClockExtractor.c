/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhStmProductClockExtractor.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/clock/ThaClockExtractorInternal.h"
#include "../../../../default/sdh/ThaModuleSdh.h"
#include "../../../../default/pdh/ThaPdhDe1.h"
#include "../cdr/ThaStmPwProductModuleCdrReg.h"
#include "ThaStmPwProductClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/
#define cClockExtractDisableReg 0x101
#define mThis(self) ((ThaStmPwProductClockExtractor)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPwProductClockExtractorMethods m_methods;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductClockExtractor);
    }

static uint32 ReferenceSync8KOutputControl(ThaClockExtractor self)
    {
    return cThaRegCDRReferenceSync8KOutputControl + ThaClockExtractorDefaultOffset(self);
    }

static AtDevice Device(ThaClockExtractor self)
    {
    return AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet((AtClockExtractor)self));
    }

static eBool CanExtractSdhSystemClock(ThaClockExtractor self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool CanExtractSdhLineClock(ThaClockExtractor self, AtSdhLine line)
    {
    if (line == NULL)
        return cAtFalse;

    if (ThaClockExtractorPartId(self) == ThaModuleSdhPartOfChannel((AtSdhChannel)line))
        return cAtTrue;

    AtDeviceLog(Device(self),
                cAtLogLevelCritical, AtSourceLocation,
                "Cannot extract clock from line %d to clock output ID %d, they must be in the same part\r\n",
                AtChannelIdGet((AtChannel)line),
                AtClockExtractorIdGet((AtClockExtractor)self));

    return cAtFalse;
    }

static eBool CanExtractExternalClock(ThaClockExtractor self, uint8 externalId)
    {
	AtUnused(externalId);
	AtUnused(self);
    return cAtTrue;
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
    ThaPdhDe1 thaDe1 = (ThaPdhDe1)de1;

    if (de1 == NULL)
        return cAtFalse;

    if (ThaPdhDe1PartId(thaDe1) == ThaClockExtractorPartId(self))
        return cAtTrue;

    AtDeviceLog(Device(self),
                cAtLogLevelCritical, AtSourceLocation,
                "Cannot extract clock from DE1 %s to clock output ID %d, they must be in the same part\r\n",
                AtChannelIdString((AtChannel)de1),
                AtClockExtractorIdGet((AtClockExtractor)self));

    return cAtFalse;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    uint32 regVal = 0;
    uint8 hwMode  = (uint8)(2 + ThaModuleSdhLineLocalId(line));

    mRegFieldSet(regVal, cThaRefOutTimeMode, hwMode);
    AtClockExtractorWrite((AtClockExtractor)self, ReferenceSync8KOutputControl(self), hwMode);
    mMethodsGet(mThis(self))->HwTimingModeDidChange(mThis(self), hwMode);

    return cAtOk;
    }

static eAtModuleClockRet HwSystemClockExtract(ThaClockExtractor self)
    {
    static const uint8 cHwMode = 0;
    AtClockExtractorWrite((AtClockExtractor)self, ReferenceSync8KOutputControl(self), cHwMode);
    mMethodsGet(mThis(self))->HwTimingModeDidChange(mThis(self), cHwMode);
    return cAtOk;
    }

static eBool SystemClockIsExtracted(AtClockExtractor self)
    {
    ThaClockExtractor clockExtractor = (ThaClockExtractor)self;
    return (AtClockExtractorRead(self, ReferenceSync8KOutputControl(clockExtractor)) == 0) ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet HwSdhSystemClockExtract(ThaClockExtractor self)
    {
    return mMethodsGet(self)->HwSystemClockExtract(self);
    }

static eBool SdhSystemClockIsExtracted(AtClockExtractor self)
    {
    return mMethodsGet(self)->SystemClockIsExtracted(self);
    }

static eAtModuleClockRet HwExternalClockExtract(ThaClockExtractor self, uint8 externalId)
    {
    uint8 hwMode = 0;

    if (externalId == 0) hwMode = 6;
    if (externalId == 1) hwMode = 7;

    if (hwMode == 0)
        return cAtErrorOutOfRangParm;

    AtClockExtractorWrite((AtClockExtractor)self, ReferenceSync8KOutputControl(self), hwMode);
    mMethodsGet(mThis(self))->HwTimingModeDidChange(mThis(self), hwMode);

    return cAtOk;
    }

static uint8 ExternalClockGet(AtClockExtractor self)
    {
    ThaClockExtractor clockExtractor = (ThaClockExtractor)self;
    uint32 regVal = AtClockExtractorRead(self, ReferenceSync8KOutputControl(clockExtractor));
    uint32 hwMode = mRegField(regVal, cThaRefOutTimeMode);

    if (hwMode == 6) return 0;
    if (hwMode == 7) return 1;

    /* Return invalid ID */
    return ThaModuleClockMaxNumExternalClockSources((ThaModuleClock)AtClockExtractorModuleGet(self));
    }

static eAtModuleClockRet De1AcrDcrTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    uint32 regVal = 0;
    static const uint8 cHwMode = 8;
    ThaPdhDe1 thaDe1 = (ThaPdhDe1)de1;

    mRegFieldSet(regVal, cThaRefOutTimeMode, cHwMode);
    mRegFieldSet(regVal, cThaRefOutPDHLineID, ThaCdrControllerIdGet(ThaPdhDe1CdrControllerGet(thaDe1)));

    mRegFieldSet(regVal, cThaRefOutPDHLineType, AtPdhDe1IsE1(de1) ? 0 : 1);
    AtClockExtractorWrite((AtClockExtractor)self, ReferenceSync8KOutputControl(self), regVal);
    mMethodsGet(mThis(self))->HwTimingModeDidChange(mThis(self), cHwMode);

    return cAtOk;
    }

static eAtModuleClockRet De1LoopTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    uint32 regVal = 0;
    static const uint8 cHwMode = 1;
    ThaPdhDe1 thaDe1 = (ThaPdhDe1)de1;

    mRegFieldSet(regVal, cThaRefOutTimeMode, cHwMode); /* Loop timing mode */
    mRegFieldSet(regVal, cThaRefOutPDHLineID, ThaPdhDe1FlatId(thaDe1));
    mRegFieldSet(regVal, cThaRefOutPDHLineType, AtPdhDe1IsE1(de1) ? 0 : 1);
    AtClockExtractorWrite((AtClockExtractor)self, ReferenceSync8KOutputControl(self), regVal);
    mMethodsGet(mThis(self))->HwTimingModeDidChange(mThis(self), cHwMode);

    return cAtOk;
    }

static uint32 DisableMask(ThaClockExtractor self)
    {
    return (cBit18 << ThaClockExtractorLocalId(self));
    }

static uint32 DisableShift(ThaClockExtractor self)
    {
    return 18UL + ThaClockExtractorLocalId(self);
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    ThaClockExtractor clockExtractor = (ThaClockExtractor)self;
    uint32 address = cClockExtractDisableReg + ThaClockExtractorPartOffset(clockExtractor);
    uint32 regVal  = AtClockExtractorRead(self, address);
    uint32 disableMask = DisableMask(clockExtractor);
    uint32 disableShift = DisableShift(clockExtractor);
    mFieldIns(&regVal, disableMask, disableShift, (enable == cAtFalse) ? 1 : 0);
    AtClockExtractorWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    ThaClockExtractor clockExtractor = (ThaClockExtractor)self;
    uint32 address = cClockExtractDisableReg + ThaClockExtractorPartOffset(clockExtractor);
    return (AtClockExtractorRead(self, address) & DisableMask(clockExtractor)) ? cAtFalse : cAtTrue;
    }

static void HwTimingModeDidChange(ThaStmPwProductClockExtractor self, uint8 newMode)
    {
	AtUnused(newMode);
	AtUnused(self);
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhSystemClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhLineClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractExternalClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1Clock);
        mMethodOverride(m_ThaClockExtractorOverride, De1AcrDcrTimingModeExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De1LoopTimingModeExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSystemClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhSystemClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwExternalClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, SystemClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, SdhSystemClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, ExternalClockGet);
        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static void MethodInit(ThaStmPwProductClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwTimingModeDidChange);
        }

    mMethodsSet(self, &m_methods);
    }

AtClockExtractor ThaStmPwProductClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor ThaStmPwProductClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductClockExtractorObjectInit(newExtractor, clockModule, extractorId);
    }

eAtModuleClockRet ThaPwCommonProductClockExtractEnable(ThaClockExtractor self, eBool enable)
    {
    return Enable((AtClockExtractor)self, enable);
    }

eBool ThaPwCommonProductClockExtractIsEnabled(ThaClockExtractor self)
    {
    return IsEnabled((AtClockExtractor)self);
    }

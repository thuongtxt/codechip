/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhStmProductModuleClock.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSdh.h"
#include "ThaStmPwProductModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
/* IMPORTANT: These masks should not be changed. If they must be changed for
 * some conditions, the corresponding customer source code need to be updated.
 * See products document for more detail */
#define cThaStmPwProductClockStatusSystemClockFail     cBit0
#define cThaStmPwProductClockStatusLocalBusClockFail   cBit1
#define cThaStmPwProductClockStatusGeClockFail         cBit2
#define cThaStmPwProductClockStatusOcnClock1Fail       cBit3
#define cThaStmPwProductClockStatusOcnClock2Fail       cBit4
#define cThaStmPwProductClockStatusDdr1ClockFail       cBit5
#define cThaStmPwProductClockStatusDdr2ClockFail       cBit6
#define cThaStmPwProductClockStatusDdr3ClockFail       cBit7
#define cThaStmPwProductClockStatusReference1ClockFail cBit8
#define cThaStmPwProductClockStatusReference2ClockFail cBit9

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtModuleClockMethods *m_AtModuleClockMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModuleClock);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return (ThaDeviceIsEp(device) ? 2 : 4);
    }

static AtModuleSdh SdhModule(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eAtRet ClockDefaultExtract(AtModule self)
    {
    AtModuleSdh sdhModule     = SdhModule(self);
    AtModuleClock clockModule = (AtModuleClock)self;
    eAtRet ret = cAtOk;

    /* Part 1 */
    ret |= AtClockExtractorExtract(AtModuleClockExtractorGet(clockModule, 0),
                                   cAtTimingModeSdhLineRef,
                                   (AtChannel)AtModuleSdhLineGet(sdhModule, 0));
    ret |= AtClockExtractorExtract(AtModuleClockExtractorGet(clockModule, 1),
                                   cAtTimingModeSdhLineRef,
                                   (AtChannel)AtModuleSdhLineGet(sdhModule, 1));

    /* Part 2 */
    if (ThaDeviceNumPartsOfModule((ThaDevice)AtModuleDeviceGet(self), cAtModuleClock) == 2)
        {
        ret |= AtClockExtractorExtract(AtModuleClockExtractorGet(clockModule, 2),
                                       cAtTimingModeSdhLineRef,
                                       (AtChannel)AtModuleSdhLineGet(sdhModule, 4));
        ret |= AtClockExtractorExtract(AtModuleClockExtractorGet(clockModule, 3),
                                       cAtTimingModeSdhLineRef,
                                       (AtChannel)AtModuleSdhLineGet(sdhModule, 5));
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ClockDefaultExtract(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint8 ExtractorPart(ThaModuleClock self, uint8 extractorId)
    {
    static const uint8 cNumExtractorsPerPart = 2;
	AtUnused(self);
    return extractorId / cNumExtractorsPerPart;
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return ThaStmPwProductClockExtractorNew((AtModuleClock)self, extractorId);
    }

static ThaClockMonitor ClockMonitorCreate(ThaModuleClock self)
    {
    return ThaStmPwProductClockMonitorNew((AtModuleClock)self);
    }

/* This overriding just to map clock monitor status to the context of this product */
static uint32 AllClockCheck(AtModuleClock self)
    {
    return ThaStmPwProductClockStatusConvert(m_AtModuleClockMethods->AllClockCheck(self));
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleClockMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        mMethodOverride(m_AtModuleClockOverride, AllClockCheck);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        mMethodOverride(m_ThaModuleClockOverride, ExtractorPart);
        mMethodOverride(m_ThaModuleClockOverride, ClockMonitorCreate);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModule(self);
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    }

AtModuleClock ThaStmPwProductModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock ThaStmPwProductModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductModuleClockObjectInit(newModule, device);
    }

uint32 ThaStmPwProductClockStatusConvert(uint32 clockStatus)
    {
    uint32 status = 0;

    if (clockStatus & cThaClockStatusSystemClockFail)
        status |= cThaStmPwProductClockStatusSystemClockFail;
    if (clockStatus & cThaClockStatusLocalBusClockFail)
        status |= cThaStmPwProductClockStatusLocalBusClockFail;
    if (clockStatus & cThaClockStatusGeClockFail)
        status |= cThaStmPwProductClockStatusGeClockFail;

    if (clockStatus & cThaClockStatusOcnClockFail(0))
        status |= cThaStmPwProductClockStatusOcnClock1Fail;
    if (clockStatus & cThaClockStatusOcnClockFail(1))
        status |= cThaStmPwProductClockStatusOcnClock2Fail;

    if (clockStatus & cThaClockStatusDdrClockFail(0))
        status |= cThaStmPwProductClockStatusDdr1ClockFail;
    if (clockStatus & cThaClockStatusDdrClockFail(1))
        status |= cThaStmPwProductClockStatusDdr2ClockFail;
    if (clockStatus & cThaClockStatusDdrClockFail(2))
        status |= cThaStmPwProductClockStatusDdr3ClockFail;

    if (clockStatus & cThaClockStatusReferenceClockFail(0))
        status |= cThaStmPwProductClockStatusReference1ClockFail;
    if (clockStatus & cThaClockStatusReferenceClockFail(1))
        status |= cThaStmPwProductClockStatusReference2ClockFail;

    return status;
    }

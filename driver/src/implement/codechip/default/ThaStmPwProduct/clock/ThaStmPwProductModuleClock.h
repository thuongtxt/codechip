/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaPdhStmProductModuleClock.h
 * 
 * Created Date: Sep 3, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULECLOCK_H_
#define _THASTMPWPRODUCTMODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtClockExtractor.h"
#include "../../../../default/clock/ThaModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModuleClock
    {
    tThaModuleClock super;
    }tThaStmPwProductModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock ThaStmPwProductModuleClockObjectInit(AtModuleClock self, AtDevice device);

uint32 ThaStmPwProductClockStatusConvert(uint32 clockStatus);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULECLOCK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : ThaPdhStmProductClockExtractor.h
 * 
 * Created Date: Sep 13, 2013
 *
 * Description : Clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTCLOCKEXTRACTOR_H_
#define _THASTMPWPRODUCTCLOCKEXTRACTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/clock/ThaClockExtractorInternal.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductClockExtractor *ThaStmPwProductClockExtractor;

typedef struct tThaStmPwProductClockExtractorMethods
    {
    void (*HwTimingModeDidChange)(ThaStmPwProductClockExtractor self, uint8 newMode);
    }tThaStmPwProductClockExtractorMethods;

typedef struct tThaStmPwProductClockExtractor
    {
    tThaClockExtractor super;
    const tThaStmPwProductClockExtractorMethods *methods;
    }tThaStmPwProductClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor ThaStmPwProductClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);

eAtModuleClockRet ThaPwCommonProductClockExtractEnable(ThaClockExtractor self, eBool enable);
eBool ThaPwCommonProductClockExtractIsEnabled(ThaClockExtractor self);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTCLOCKEXTRACTOR_H_ */


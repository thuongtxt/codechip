/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaPdhStmProductModuleRamInternal.h
 * 
 * Created Date: Jun 19, 2013
 *
 * Description : RAM module of STM CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULERAMINTERNAL_H_
#define _THASTMPWPRODUCTMODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/ram/ThaModuleRamInternal.h"
#include "ThaStmPwProductDdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModuleRam
    {
    tThaModuleRam super;
    }tThaStmPwProductModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam ThaStmPwProductModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULERAMINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : ThaPdhStmProductDdr32Bit.c
 *
 * Created Date: May 27, 2013
 *
 * Description : DDR for STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductDdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaStmPwProductDdr32Bit
    {
    tThaStmPwProductDdr super;
    }tThaStmPwProductDdr32Bit;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods m_AtRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductDdr32Bit);
    }

static uint32 CellSizeGet(AtRam self)
    {
	AtUnused(self);
    return 4;
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, CellSizeGet);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam ThaStmPwProductDdr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDdr, ramModule, core, ramId);
    }


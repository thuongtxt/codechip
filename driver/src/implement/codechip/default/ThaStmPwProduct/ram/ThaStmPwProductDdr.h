/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : ThaPdhStmProductDdr.h
 * 
 * Created Date: Jun 24, 2013
 *
 * Description : DDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTDDR_H_
#define _THASTMPWPRODUCTDDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/ram/ThaDdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductDdr
    {
    tThaDdr super;
    }tThaStmPwProductDdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam ThaStmPwProductDdr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam ThaStmPwProductDdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam ThaStmPwProductDdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTDDR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaPdhStmProductModuleEth.c
 *
 * Created Date: Jun 27, 2013
 *
 * Description : Ethernet module of STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductModuleEth.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/pw/ThaModulePw.h"
#include "../../../../default/physical/ThaSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModuleEth);
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    uint32 portId = AtChannelIdGet((AtChannel)ethPort);
	AtUnused(self);
    if (AtEthPortSpeedGet(ethPort) == cAtEthPortSpeed10G)
        return ThaStmPwProductEthPort10GSerdesControllerNew(ethPort, portId);
    return ThaStmPwProductEthPortSerdesControllerNew(ethPort, portId);
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceIsEp(device) ? 1 : 2;
    }

static eBool PortSerdesPrbsIsSupported(ThaModuleEth self, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(self);
    return cAtTrue;
    }

static eBool HasSerdesControllers(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, HasSerdesControllers);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsIsSupported);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

AtModuleEth ThaStmPwProductModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEthPwV2ObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth ThaStmPwProductModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductModuleEthObjectInit(newModule, device);
    }

uint8 ThaStmPwProductModuleEthPortIdFromCVlan(ThaModuleEth self, const tAtEthVlanTag *cVlan)
    {
	AtUnused(self);
    return (uint8)((cVlan->vlanId & cBit11_9) >> 9);
    }

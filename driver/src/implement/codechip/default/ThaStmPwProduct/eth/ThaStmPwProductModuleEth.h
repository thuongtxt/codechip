/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaPdhStmProductModuleEth.h
 * 
 * Created Date: Jun 28, 2013
 *
 * Description : Ethernet module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULEETH_H_
#define _THASTMPWPRODUCTMODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModuleEth
    {
    tThaModuleEthPwV2 super;
    }tThaStmPwProductModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth ThaStmPwProductModuleEthObjectInit(AtModuleEth self, AtDevice device);

uint8 ThaStmPwProductModuleEthPortIdFromCVlan(ThaModuleEth self, const tAtEthVlanTag *cVlan);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULEETH_H_ */


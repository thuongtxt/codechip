/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPdhStmProductEthPacket.c
 *
 * Created Date: Jun 27, 2013
 *
 * Description : ETH packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductEthPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaStmPwProductEthPacket)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPwProductEthPacketMethods m_methods;

/* Override */
static tAtEthPacketMethods m_AtEthPacketOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* EncapTypeString(ThaStmPwProductEthPacket self, uint8 encapType)
    {
    static char encapTypeStr[8];
	AtUnused(self);

    if (encapType == 0x1)  return "IPCP";
    if (encapType == 0x2)  return "BCP";
    if (encapType == 0x3)  return "MPLSCP";
    if (encapType == 0x4)  return "HDLC Bridge";
    if (encapType == 0x5)  return "HDLC Route";
    if (encapType == 0x6)  return "FR Bridge";
    if (encapType == 0x7)  return "FR Route";
    if (encapType == 0x8)  return "ATM AAL5 PDU Bridge";
    if (encapType == 0x9)  return "ATM AAL5 PDU Route";
    if (encapType == 0xA)  return "TDM Transparent";
    if (encapType == 0xB)  return "ATM OAM";
    if (encapType == 0xC)  return "IMA";
    if (encapType == 0xD)  return "IMA-Bridge";
    if (encapType == 0xE)  return "Mlppp-IPCP";
    if (encapType == 0xF)  return "Mlppp-BCP";
    if (encapType == 0x12) return "ETH";
    if (encapType == 0x13) return "SLIP";
    if (encapType == 0x14) return "LAPB";
    if (encapType == 0x15) return "X.25";
    if (encapType == 0x16) return "ATM unmatch";
    if (encapType == 0x17) return "Cisco HDLC-Route";

    AtSprintf(encapTypeStr, "0x%02x", encapType);
    return encapTypeStr;
    }

static void DisplayFirstVlanTag(ThaStmPwProductEthPacket self, tAtVlanTag *pVlanTag, uint8 level)
    {
    uint8 cpuPktIndicator;
    uint8 encapType;
    uint8 pktLen;

    cpuPktIndicator = (pVlanTag->vlanId & cBit11) ? 1 : 0;
    mFieldGet(pVlanTag->vlanId, cBit10_6, 6, uint8, &encapType);
    mFieldGet(pVlanTag->vlanId, cBit5_0, 0, uint8, &pktLen);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal,
             "* Tag 1: PRI = %d, CFI = %d, CPU_pkt_indicator = %d, ENCAP_TYPE = %s, PKT_LEN = %d\r\n",
             pVlanTag->pcp, pVlanTag->cfi,
             cpuPktIndicator,
             mMethodsGet(mThis(self))->EncapTypeString(mThis(self), encapType), pktLen);
    }

static void DisplaySecondVlanTag(ThaStmPwProductEthPacket self, tAtVlanTag *pVlanTag, uint8 level)
    {
    uint8 stmPortId;
    uint16 channelNo;
    uint8 isMlppp;
	AtUnused(level);
	AtUnused(self);

    mFieldGet(pVlanTag->vlanId, cBit11_9, 9, uint8 , &stmPortId);
    mFieldGet(pVlanTag->vlanId, cBit8   , 8, uint8 , &isMlppp);
    mFieldGet(pVlanTag->vlanId, cBit7_0 , 0, uint16, &channelNo);

    AtPrintc(cSevNormal,
             "* Tag 2: PRI = %d, CFI = %d, STM port ID = %d, MLPPP/IMA = %d, ChannelId = %d\r\n",
             pVlanTag->pcp,
             pVlanTag->cfi,
             stmPortId,
             isMlppp,
             channelNo);
    }

static tAtVlanTag *VlanAtOffset(AtEthPacket self, uint32 offset, tAtVlanTag *vlanTag)
    {
    uint32 length, remainingBytes;

    AtPacketDataBuffer((AtPacket)self, &length);
    remainingBytes = length - offset;
    if (remainingBytes < cVlanLength)
        return NULL;

    return mMethodsGet(self)->VlagTagFromBuffer(self, vlanTag, &(AtPacketDataBuffer((AtPacket)self, &length)[offset]), offset);
    }

static uint16 SecondVlanOffset(AtEthPacket self)
    {
    return (uint16)(mMethodsGet(self)->FirstVlanOffset(self) + cVlanLength);
    }

static void DisplayVlans(AtEthPacket self, uint8 level)
    {
    tAtVlanTag vlanTag, *pVlanTag;

    /* The first VLAN */
    pVlanTag = VlanAtOffset(self, mMethodsGet(self)->FirstVlanOffset(self), &vlanTag);
    if (pVlanTag == NULL)
        return;
    mMethodsGet(mThis(self))->DisplayFirstVlanTag(mThis(self), pVlanTag, level);

    /* And the second one */
    pVlanTag = VlanAtOffset(self, SecondVlanOffset(self), &vlanTag);
    if (pVlanTag == NULL)
        return;
    mMethodsGet(mThis(self))->DisplaySecondVlanTag(mThis(self), pVlanTag, level);
    return;
    }

static eBool HasEthTypeLength(AtEthPacket self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool HasCrc(AtEthPacket self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductEthPacket);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket packet = (AtEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, mMethodsGet(packet), sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, HasEthTypeLength);
        mMethodOverride(m_AtEthPacketOverride, HasCrc);
        mMethodOverride(m_AtEthPacketOverride, DisplayVlans);
        }

    mMethodsSet(packet, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtEthPacket(self);
    }

static void MethodsInit(AtPacket self)
    {
    ThaStmPwProductEthPacket packet = (ThaStmPwProductEthPacket)self;
    
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EncapTypeString);
        mMethodOverride(m_methods, DisplayFirstVlanTag);
        mMethodOverride(m_methods, DisplaySecondVlanTag);
        }

    mMethodsSet(packet, &m_methods);
    }

AtPacket ThaStmPwProductEthPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket ThaStmPwProductEthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductEthPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPdhStmProductEthPacket.h
 * 
 * Created Date: Nov 7, 2013
 *
 * Description : Packet analyzer module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTETHPACKET_H_
#define _THASTMPWPRODUCTETHPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/pktanalyzer/AtEthPacketInternal.h"
#include "../../../../default/pktanalyzer/ThaPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductEthPacket * ThaStmPwProductEthPacket;

typedef struct tThaStmPwProductEthPacketMethods
    {
    const char* (*EncapTypeString)(ThaStmPwProductEthPacket self, uint8 encapType);
    void (*DisplayFirstVlanTag)(ThaStmPwProductEthPacket self, tAtVlanTag *pVlanTag, uint8 level);
    void (*DisplaySecondVlanTag)(ThaStmPwProductEthPacket self, tAtVlanTag *pVlanTag, uint8 level);
    }tThaStmPwProductEthPacketMethods;

typedef struct tThaStmPwProductEthPacket
    {
    tAtEthPacket super;
    const tThaStmPwProductEthPacketMethods *methods;
    }tThaStmPwProductEthPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket ThaStmPwProductEthPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTETHPACKET_H_ */


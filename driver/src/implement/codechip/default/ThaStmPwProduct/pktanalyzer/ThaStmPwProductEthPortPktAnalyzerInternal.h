/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : ThaPdhStmProductEthPortPktAnalyzerInternal.h
 * 
 * Created Date: Apr 29, 2014
 *
 * Description : Packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTETHPORTPKTANALYZERINTERNAL_H_
#define _THASTMPWPRODUCTETHPORTPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pktanalyzer/ThaEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductEthPortPktAnalyzer
    {
    tThaEthPortPktAnalyzerV2 super;
    }tThaStmPwProductEthPortPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaStmPwProductEthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTETHPORTPKTANALYZERINTERNAL_H_ */


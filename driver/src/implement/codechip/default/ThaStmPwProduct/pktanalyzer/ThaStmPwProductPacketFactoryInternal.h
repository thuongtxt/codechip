/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : ThaStmPwProductPacketFactoryInternal.h
 * 
 * Created Date: May 10, 2016
 *
 * Description : Packet factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTPACKETFACTORYINTERNAL_H_
#define _THASTMPWPRODUCTPACKETFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pktanalyzer/ThaPacketFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductPacketFactory
    {
    tThaPacketFactory super;
    }tThaStmPwProductPacketFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacketFactory ThaStmPwProductPacketFactoryNewObjectInit(AtPacketFactory self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTPACKETFACTORYINTERNAL_H_ */


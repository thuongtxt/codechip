/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPdhStmProductEthPortPktAnalyzer.c
 *
 * Created Date: Jun 27, 2013
 *
 * Description : Packet analyzer of STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductEthPortPktAnalyzerInternal.h"
#include "../../../../default/cla/ThaModuleCla.h"
#include "../../../../default/pw/ThaModulePw.h"
#include "../../../../default/pktanalyzer/ThaPacketFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Save super implementation */
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductEthPortPktAnalyzer);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);

    if (ThaModulePwLookupModeGet(pwModule, NULL) == cThaPwLookupMode2Vlans)
        return ThaStmPwProductEthPacketNew(data, length, cAtPacketCacheModeCacheData);
    else
        return m_AtPktAnalyzerMethods->PacketCreate(self, data, length, direction);
    }

static AtPacketFactory PacketFactoryCreate(AtPktAnalyzer self)
    {
    return ThaStmPwProductPacketFactoryNew(AtChannelDeviceGet(AtPktAnalyzerChannelGet(self)));
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketFactoryCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    }

AtPktAnalyzer ThaStmPwProductEthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortPktAnalyzerV2ObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer ThaStmPwProductEthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductEthPortPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

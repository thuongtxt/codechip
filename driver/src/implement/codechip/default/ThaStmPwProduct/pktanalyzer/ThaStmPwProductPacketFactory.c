/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : ThaPdhStmProductPacketFactory.c
 *
 * Created Date: Mar 31, 2014
 *
 * Description : Packet factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMplsPacket.h"
#include "ThaStmPwProductPacketFactoryInternal.h"
#include "../../../../default/cla/ThaModuleCla.h"
#include "../../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketFactoryMethods m_AtPacketFactoryOverride;

/* Save super implementation */
static const tAtPacketFactoryMethods *m_AtPacketFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPacket MplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(ThaPacketFactoryDeviceGet(self), cThaModuleCla);
    eThaPwLookupMode lookupMode = ThaClaPwControllerLookupModeGet(ThaModuleClaPwControllerGet(claModule), NULL);
    if (lookupMode == cThaPwLookupMode2Vlans)
        return AtMplsPacketNew(dataBuffer, length, cacheMode);
    return m_AtPacketFactoryMethods->MplsPacketCreate(self, dataBuffer, length, cacheMode, direction);
    }

static void OverrideAtPacketFactory(AtPacketFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketFactoryMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketFactoryOverride, m_AtPacketFactoryMethods, sizeof(m_AtPacketFactoryOverride));

        mMethodOverride(m_AtPacketFactoryOverride, MplsPacketCreate);
        }

    mMethodsSet(self, &m_AtPacketFactoryOverride);
    }

static void Override(AtPacketFactory self)
    {
    OverrideAtPacketFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductPacketFactory);
    }

AtPacketFactory ThaStmPwProductPacketFactoryNewObjectInit(AtPacketFactory self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPacketFactoryObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacketFactory ThaStmPwProductPacketFactoryNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacketFactory newFactory = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFactory == NULL)
        return NULL;

    return ThaStmPwProductPacketFactoryNewObjectInit(newFactory, device);
    }

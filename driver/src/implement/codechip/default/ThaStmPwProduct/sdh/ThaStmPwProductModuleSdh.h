/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaPdhStmProductModuleSdh.h
 * 
 * Created Date: Oct 24, 2014
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULESDH_H_
#define _THASTMPWPRODUCTMODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/sdh/ThaModuleSdh.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModuleSdh * ThaStmPwProductModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaStmPwProductModuleSdhStartVersionSupportsSerdesTimingMode(ThaStmPwProductModuleSdh self);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULESDH_H_ */


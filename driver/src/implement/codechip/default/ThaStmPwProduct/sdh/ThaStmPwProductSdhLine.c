/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaPdhStmProductSdhLine.c
 *
 * Created Date: Jul 23, 2014
 *
 * Description : SDH Line of STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/sdh/ThaModuleSdhInternal.h"
#include "ThaStmPwProductSdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineMethods m_AtSdhLineOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtSdhLineMethods *m_AtSdhLineMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 TxK2Get(AtSdhLine self)
    {
    const uint8 cK2MsAisFieldVal = 0x7;
    const uint8 cK2MsRdiFieldVal = 0x6;
    AtChannel channel = (AtChannel)self;
    uint32 forcedAlarms = AtChannelTxForcedAlarmGet(channel);
    uint32 currentAlarms;
    uint32 autoMsRdiAlarms;
    uint8 txK2 = m_AtSdhLineMethods->TxK2Get(self);

    /* Insert AIS value to K2 byte if forcing Tx MS-AIS */
    if (forcedAlarms & cAtSdhLineAlarmAis)
    	return txK2 | cK2MsAisFieldVal;

    /* Insert RDI value to K2 byte if forcing Tx MS-RDI */
    if (forcedAlarms & cAtSdhLineAlarmRdi)
    	return (uint8)((txK2 & (~((uint8)cBit2_0))) | cK2MsRdiFieldVal);

    /* Insert RDI value to K2 byte if detect Rx critical alarms and Auto MS-RDI is enabled */
    currentAlarms   = AtChannelDefectGet(channel);
    autoMsRdiAlarms = ThaModuleSdhAutoMsRdiAlarms((ThaModuleSdh)AtChannelModuleGet(channel));
    if (currentAlarms & autoMsRdiAlarms)
        return (uint8)((txK2 & (~((uint8)cBit2_0))) | cK2MsRdiFieldVal);

    return txK2;
    }

static eBool IsFirstPortInGroup(AtSdhLine self)
    {
    AtChannel line = (AtChannel)self;
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet(line);

    if ((AtChannelHwIdGet(line) % ThaModuleSdhNumLinesPerPart(sdhModule)) == 0)
        return cAtTrue;

    return cAtFalse;
    }

static void ApplySameSerdesTimingModeForOtherLines(AtSdhLine self)
    {
    uint8 lineId;
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    uint8 partStartLineId = (uint8)(partId * ThaModuleSdhNumLinesPerPart(sdhModule));
    uint8 partEndLineId = (uint8)(partStartLineId + ThaModuleSdhNumLinesPerPart(sdhModule));
    eAtSerdesTimingMode timingMode = AtSerdesControllerTimingModeGet(AtSdhLineSerdesController(self));

    for (lineId = (uint8)(partStartLineId + 1); lineId < partEndLineId; lineId++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        AtSerdesController serdesController = AtSdhLineSerdesController(line);
        AtSerdesControllerTimingModeSet(serdesController, timingMode);
        }
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtSdhLine line = (AtSdhLine)self;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!ThaModuleSdhShouldUpdateSerdesTimingWhenLineRateChange((ThaModuleSdh)AtChannelModuleGet(self)))
        return ret;

    /* When run stm4 need to synchronize all line in one group same timing mode is auto */
    if (IsFirstPortInGroup(line) && (AtSdhLineRateGet(line) == cAtSdhLineRateStm4))
        ApplySameSerdesTimingModeForOtherLines(line);

    return ret;
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, TxK2Get);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtSdhLine(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductSdhLine);
    }

AtSdhLine ThaStmPwProductSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine ThaStmPwProductSdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductSdhLineObjectInit(newLine, channelId, module, version);
    }

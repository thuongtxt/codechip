/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaPdhStmProductModuleSdhInternal.h
 * 
 * Created Date: Aug 19, 2013
 *
 * Description : SDH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULESDHINTERNAL_H_
#define _THASTMPWPRODUCTMODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/sdh/ThaModuleSdhInternal.h"
#include "ThaStmPwProductModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductModuleSdhMethods
    {
    uint32 (*StartVersionSupportsSerdesTimingMode)(ThaStmPwProductModuleSdh self);
    }tThaStmPwProductModuleSdhMethods;

typedef struct tThaStmPwProductModuleSdh
    {
    tThaModuleSdh super;
    const tThaStmPwProductModuleSdhMethods *methods;
    }tThaStmPwProductModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh ThaStmPwProductModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /*_THASTMPWPRODUCTMODULESDHINTERNAL_H_ */


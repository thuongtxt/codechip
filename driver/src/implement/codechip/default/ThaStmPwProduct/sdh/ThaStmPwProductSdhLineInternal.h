/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaPdhStmProductSdhLineInternal.h
 * 
 * Created Date: Nov 18, 2014
 *
 * Description : This file contains declarations of SDH line for AF6LTT0061
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTSDHLINEINTERNAL_H_
#define _THASTMPWPRODUCTSDHLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/sdh/ThaSdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductSdhLine
    {
    tThaSdhLine super;
    }tThaStmPwProductSdhLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine ThaStmPwProductSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version);

#endif /* _THASTMPWPRODUCTSDHLINEINTERNAL_H_ */


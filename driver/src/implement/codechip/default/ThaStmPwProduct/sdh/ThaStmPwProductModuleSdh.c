/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : ThaPdhStmProductModuleSdh.c
 *
 * Created Date: Jun 10, 2013
 *
 * Description : SDH module for STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductModuleSdhInternal.h"
#include "../../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesStickyReg            0xF00052
#define cSerdesStickyPll1NotLocked  cBit0
#define cSerdesStickyPll2NotLocked  cBit17_16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPwProductModuleSdhMethods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModuleSdh);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceIsEp(device) ? 4 : 8;
    }

static eAtRet Debug(AtModule self)
    {
    static const uint32 cStickyReg = 0xF00052;
    uint32 regVal;
    eBool locked;

    /* Common information */
    m_AtModuleMethods->Debug(self);

    /* Additional information */
    regVal = mModuleHwRead(self, cStickyReg);

    /* PLL#1 status */
    AtPrintc(cSevNormal, "- PLL#1: ");
    locked = (regVal & cSerdesStickyPll1NotLocked) ? cAtFalse : cAtTrue;
    AtPrintc(locked ? cSevInfo : cSevCritical, "%s\r\n", locked ? "locked" : "not lock");

    /* PLL#2 status */
    AtPrintc(cSevNormal, "- PLL#2: ");
    locked = (regVal & cSerdesStickyPll2NotLocked) ? cAtFalse : cAtTrue;
    AtPrintc(locked ? cSevInfo : cSevCritical, "%s\r\n", locked ? "locked" : "not lock");

    /* Clear sticky */
    mModuleHwWrite(self, cStickyReg, regVal);

    return cAtOk;
    }

static eBool AlwaysDisableRdiBackwardWhenTimNotDownstreamAis(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    return ThaStmPwProductSdhLineSerdesControllerNew(line, serdesId);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)line);
	AtUnused(self);

    /* Hardware just accept line Id from 0-3 in 8 ports
     * SW: 0 1 2 3 4 5 6 7
       HW: 3 0 2 1 0 3 1 2 */
    if (lineId == 0) return 3;
    if (lineId == 1) return 0;
    if (lineId == 2) return 2;
    if (lineId == 3) return 1;
    if (lineId == 4) return 0;
    if (lineId == 5) return 3;
    if (lineId == 6) return 1;
    if (lineId == 7) return 2;

    /* Return invalid port */
    return 255;
    }

static uint8 SerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)line);
	AtUnused(self);

    /* Hardware just accept line Id from 0-3 in 8 ports
     * SW: 0 1 2 3 4 5 6 7
       HW: 3 0 2 1 4 7 5 6 */
    if (lineId == 0) return 3;
    if (lineId == 1) return 0;
    if (lineId == 2) return 2;
    if (lineId == 3) return 1;
    if (lineId == 4) return 4;
    if (lineId == 5) return 7;
    if (lineId == 6) return 5;
    if (lineId == 7) return 6;

    /* Return invalid port */
    return 255;
    }

static eBool BerHardwareInterruptIsSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool LineSerdesPrbsIsSupported(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
	AtUnused(line);
	AtUnused(self);
	AtUnused(serdes);
    return cAtTrue;
    }

static eBool ErdiIsEnabledByDefault(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool BlockErrorCountersSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)ThaStmPwProductSdhLineNew(channelId, (AtModuleSdh)self, version);

    /* Ask super for other channel types */
    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static eBool CanDisablePwPacketsToPsn(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x14, 0x07, 0x22, 0x17);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportsSerdesTimingMode(ThaStmPwProductModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x14, 0x10, 0x23, 0x00);
    }

static void MethodsInit(ThaStmPwProductModuleSdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionSupportsSerdesTimingMode);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, BerHardwareInterruptIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, LineSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, ErdiIsEnabledByDefault);
        mMethodOverride(m_ThaModuleSdhOverride, BlockErrorCountersSupported);
        mMethodOverride(m_ThaModuleSdhOverride, AlwaysDisableRdiBackwardWhenTimNotDownstreamAis);
        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        mMethodOverride(m_ThaModuleSdhOverride, CanDisablePwPacketsToPsn);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModule(AtModuleSdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModule(self);
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

AtModuleSdh ThaStmPwProductModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaStmPwProductModuleSdh)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh ThaStmPwProductModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductModuleSdhObjectInit(newModule, device);
    }

uint32 ThaStmPwProductModuleSdhStartVersionSupportsSerdesTimingMode(ThaStmPwProductModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->StartVersionSupportsSerdesTimingMode(self);
    return cBit31_0;
    }

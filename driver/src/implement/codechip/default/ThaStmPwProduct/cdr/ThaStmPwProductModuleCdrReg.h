/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaPdhStmProductModuleCdrReg.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Register of CDR module of STM CES common product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTMODULECDRREG_H_
#define _THASTMPWPRODUCTMODULECDRREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*==============================================================================
Reg Name: CDR Reference Sync 8K Output control
Address : 0x0280411, 0x0280412
Description:  This register is used to configure timing source to generate the
              reference sync master output signal.
Note: hardware defines two registers for two output clocks, their names are:
      - CDR Reference Sync 8K Master Output control, and
      - CDR Reference Sync 8K Slaver Output control
      But, there is no master/slaver relationship here, just two output clocks.
      Software redefines to one register type
==============================================================================*/
#define cThaRegCDRReferenceSync8KOutputControl    0x0280411

#define cThaRefOutPDHLineTypeMask  cBit15_13
#define cThaRefOutPDHLineTypeShift 13
#define cThaRefOutPDHLineIDMask    cBit12_4
#define cThaRefOutPDHLineIDShift   4
#define cThaRefOutTimeModeMask     cBit3_0
#define cThaRefOutTimeModeShift    0
#define cThaRefOutTimeModeUnused   cThaRefOutTimeModeMask

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTMODULECDRREG_H_ */


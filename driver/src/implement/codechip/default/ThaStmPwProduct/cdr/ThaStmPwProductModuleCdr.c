/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaPdhStmProductModuleCdr.c
 *
 * Created Date: Jul 18, 2013
 *
 * Description : CDR module of STM CES common product
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductModuleCdr.h"
#include "../../../../default/cdr/ThaModuleCdrStmReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrStmMethods m_ThaModuleCdrStmOverride;

/* Save super implementation */
static const tThaModuleCdrStmMethods *m_ThaModuleCdrStmMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductModuleCdr);
    }

static eAtRet PartDefaultSet(ThaModuleCdr self, uint8 partId)
    {
    static const uint32 cDefaultCoefficient = 0x25f8;
    uint32 partOffset;
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    eAtRet ret;
    AtIpCore core;

    ret = m_ThaModuleCdrStmMethods->PartDefaultSet(self, partId);
    if (ret != cAtOk)
        return ret;

    core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    partOffset = ThaModuleCdrPartOffset(self, partId);
    regAddr = cThaRegCDRTimingExternalReferenceControl + partOffset;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cThaDcrExt1N2k, cDefaultCoefficient);
    mRegFieldSet(longRegVal[0], cThaDcrExt2N2k, cDefaultCoefficient);
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, PartDefaultSet);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdrStm(self);
    }

AtModule ThaStmPwProductModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCdrStmObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaStmPwProductModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductModuleCdrObjectInit(newModule, device);
    }

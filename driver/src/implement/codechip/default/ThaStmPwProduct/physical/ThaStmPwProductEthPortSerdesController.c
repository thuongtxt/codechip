/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaPdhStmProductEthPortSerdesController.c
 *
 * Created Date: Aug 16, 2013
 *
 * Description : SERDES controller for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductEthPortSerdesController.h"
#include "../../../../../generic/eth/AtModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaStmPwProductEthPortSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPwProductEthPortSerdesControllerMethods m_methods;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods  = NULL;
static const tThaSerdesControllerMethods *m_ThaSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf50000;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductEthPortSerdesController);
    }

static uint32 PhysicalParamDefaultValue(ThaSerdesController self, eAtSerdesParam param)
    {
    uint32 paramValue = param;
    switch (paramValue)
        {
        case cAtSerdesParamVod                    : return 50;
        case cAtSerdesParamPreEmphasisFirstPostTap: return 2;
        default:
            return m_ThaSerdesControllerMethods->PhysicalParamDefaultValue(self, param);
        }
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = cAtOk;
    ThaSerdesController controller = (ThaSerdesController)self;

    ret |= m_AtSerdesControllerMethods->Init(self);

    ret |= ThaSerdesControllerDefaultParamSet(controller, cAtSerdesParamVod);
    ret |= ThaSerdesControllerDefaultParamSet(controller, cAtSerdesParamPreEmphasisFirstPostTap);

    return ret;
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf50861;
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
    return cBit0 << ThaEthPortIdGet(self);
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
    return ThaEthPortIdGet(self);
    }

static uint32 PllRegister(ThaStmPwProductEthPortSerdesController self)
    {
	AtUnused(self);
    return 0xf00051;
    }

static uint32 PllStickyMask(ThaStmPwProductEthPortSerdesController self)
    {
	AtUnused(self);
    return cBit0;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->PllRegister(mThis(self));
    uint32 pllStickyMask = mMethodsGet(mThis(self))->PllStickyMask(mThis(self));
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    AtOsal osal    = AtSharedDriverOsalGet();

    /* Clear sticky and give hardware a moment */
    AtSerdesControllerWrite(self, regAddr, regVal | pllStickyMask, cAtModuleEth);
    mMethodsGet(osal)->USleep(osal, 1000);

    /* Get back */
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return ((regVal & pllStickyMask) == 0) ? cAtTrue : cAtFalse;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    return ThaModuleEthPortSerdesPrbsEngineCreate(ethModule, port, AtSerdesControllerIdGet(self));
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(channel);
    return AtModuleEthSerdesMdio(ethModule, self);
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeGe) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeGe;
    }

static void MethodsInit(ThaStmPwProductEthPortSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PllRegister);
        mMethodOverride(m_methods, PllStickyMask);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSerdesControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInMask);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInShift);
        mMethodOverride(m_ThaSerdesControllerOverride, PhysicalParamDefaultValue);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    }

AtSerdesController ThaStmPwProductEthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSerdesControllerObjectInit(self, (AtChannel)ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaStmPwProductEthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductEthPortSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

uint32 ThaEthPortIdGet(ThaSerdesController self)
    {
    return AtChannelIdGet((AtChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self));
    }

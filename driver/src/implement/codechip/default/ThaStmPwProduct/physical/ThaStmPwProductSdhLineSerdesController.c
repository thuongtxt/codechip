/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaPdhStmProductSdhLineSerdesController.c
 *
 * Created Date: Aug 17, 2013
 *
 * Description : SDH Line SERDES controller for STM CES common product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductSdhLineSerdesController.h"
#include "../sdh/ThaStmPwProductModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesRegPortEnable    0xf00041
#define cSerdesRegAlarmStatus   0xf00066
#define cSerdesRegPllStatus     0xf00052

/*--------------------------- Macros -----------------------------------------*/
#define cPortMask(portId)  (cBit0 << (portId))
#define cPortShift(portId) (portId)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods         m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods        m_ThaSerdesControllerOverride;
static tThaSdhLineSerdesControllerMethods m_ThaSdhLineSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf40000;
    }

static eBool OnFirstPart(ThaSerdesController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return ((ThaModuleSdhPartOfChannel(channel) == 0) ? cAtTrue : cAtFalse);
    }

static uint32 PartOffset(ThaSerdesController self)
    {
    return OnFirstPart(self) ? 0 : 0x800;
    }

static uint32 LineId(AtSerdesController self)
    {
    return AtChannelIdGet(AtSerdesControllerPhysicalPortGet(self));
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    uint32 regVal;
    uint32 lineId = LineId(self);

    regVal = AtSerdesControllerRead(self, cSerdesRegPortEnable, cThaModuleOcn);
    mFieldIns(&regVal, cPortMask(lineId), cPortShift(lineId), enable ? 1 : 0);
    AtSerdesControllerWrite(self, cSerdesRegPortEnable, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cSerdesRegPortEnable, cThaModuleOcn);
    return (regVal & cPortMask(LineId(self))) ? cAtTrue : cAtFalse;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
    return OnFirstPart(self) ? 0xf40461 : 0xf40C61;
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
    return cPortMask(AtSerdesControllerIdGet((AtSerdesController)self));
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
    return cPortShift(AtSerdesControllerIdGet((AtSerdesController)self));
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    AtSdhChannel line = (AtSdhChannel)AtSerdesControllerPhysicalPortGet(self);
    uint8 partId      = ThaModuleSdhPartOfChannel(line);
    uint32 regAddr    = cSerdesRegPllStatus;
    uint32 regVal     = AtSerdesControllerRead(self, regAddr, cAtModuleSdh);
    uint32 mask       = (partId == 0) ? cBit0 : cBit16;
    AtOsal osal       = AtSharedDriverOsalGet();

    /* Clear sticky and give hardware a moment */
    AtSerdesControllerWrite(self, regAddr, regVal | mask, cAtModuleSdh);
    mMethodsGet(osal)->USleep(osal, 1000);

    /* Get back */
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleSdh);
    return ((regVal & mask) == 0) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cSerdesRegAlarmStatus, cAtModuleSdh);
    uint32 losBitValue = regVal & (cBit0 << LineId(self));
    return losBitValue ? cAtSerdesAlarmTypeLos : cAtSerdesAlarmTypeNone;
    }

static uint32 LocalLineId(ThaSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return ThaModuleSdhLineLocalId(line);
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
    return OnFirstPart((ThaSerdesController)self) ? 0xf40464 : 0xf40C64;
    }

static uint32 LockToDataRegMask(ThaSdhLineSerdesController self)
    {
    return cBit0 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToDataRegShift(ThaSdhLineSerdesController self)
    {
    return LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
    return OnFirstPart((ThaSerdesController)self) ? 0xf40465 : 0xf40C65;
    }

static uint32 LockToRefRegMask(ThaSdhLineSerdesController self)
    {
    return cBit0 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegShift(ThaSdhLineSerdesController self)
    {
    return LocalLineId((ThaSerdesController)self);
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
    AtChannel line = AtSerdesControllerPhysicalPortGet(self);
    ThaStmPwProductModuleSdh sdhModule = (ThaStmPwProductModuleSdh)AtChannelModuleGet(line);
    uint32 startSupportedVersion = ThaStmPwProductModuleSdhStartVersionSupportsSerdesTimingMode(sdhModule);
    AtDevice device = AtChannelDeviceGet(line);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController sdhLineSerdes = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride, mMethodsGet(sdhLineSerdes), sizeof(m_ThaSdhLineSerdesControllerOverride));

        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegShift);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegShift);
        }

    mMethodsSet(sdhLineSerdes, &m_ThaSdhLineSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInMask);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInShift);
        mMethodOverride(m_ThaSerdesControllerOverride, PartOffset);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    OverrideThaSdhLineSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductSdhLineSerdesController);
    }

AtSerdesController ThaStmPwProductSdhLineSerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaStmPwProductSdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductSdhLineSerdesControllerObjectInit(newController, sdhLine, serdesId);
    }

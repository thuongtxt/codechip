/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : ThaPdhStmProductEthPort10GSerdesController.c
 *
 * Created Date: Mar 14, 2014
 *
 * Description : 10G ETH Port SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmPwProductEthPortSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;
static tThaStmPwProductEthPortSerdesControllerMethods m_ThaStmPwProductEthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods  = NULL;
static const tThaSerdesControllerMethods *m_ThaSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xF60000;
    }

static uint32 PhysicalParamDefaultValue(ThaSerdesController self, eAtSerdesParam param)
    {
    uint32 paramValue = param;
    switch (paramValue)
        {
        case cAtSerdesParamVod                    : return 0x3F;
        case cAtSerdesParamRxEqualizationDcGain   : return 0;
        case cAtSerdesParamPreEmphasisFirstPostTap: return 0x1f;
        case cAtSerdesParamRxEqualizationControl  : return 1;
        default:
            return m_ThaSerdesControllerMethods->PhysicalParamDefaultValue(self, param);
        }
    }

static eAtRet Init(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;
    eAtRet ret = cAtOk;

    ret |= m_AtSerdesControllerMethods->Init(self);
    ret |= ThaSerdesControllerDefaultParamSet(controller, cAtSerdesParamRxEqualizationDcGain);
    ret |= ThaSerdesControllerDefaultParamSet(controller, cAtSerdesParamRxEqualizationControl);

    return ret;
    }

static uint32 PllStickyMask(ThaStmPwProductEthPortSerdesController self)
    {
	AtUnused(self);
    /* Bit0: for 1G part, and bit16 for 10G part */
    return cBit0 | cBit16;
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xF60861;
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
    return cBit0 << ThaEthPortIdGet(self);
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
    return ThaEthPortIdGet(self);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSerdesControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, PhysicalParamDefaultValue);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInMask);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInShift);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void OverrideThaStmPwProductEthPortSerdesController(AtSerdesController self)
    {
    ThaStmPwProductEthPortSerdesController controller = (ThaStmPwProductEthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductEthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaStmPwProductEthPortSerdesControllerOverride));
        mMethodOverride(m_ThaStmPwProductEthPortSerdesControllerOverride, PllStickyMask);
        }

    mMethodsSet(controller, &m_ThaStmPwProductEthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    OverrideThaStmPwProductEthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPwProductEthPort10GSerdesController);
    }

AtSerdesController ThaStmPwProductEthPort10GSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPortSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController ThaStmPwProductEthPort10GSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPwProductEthPort10GSerdesControllerObjectInit(newController, ethPort, serdesId);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaPdhStmProductEthPortSerdesController.h
 * 
 * Created Date: Oct 17, 2013
 *
 * Description : SERDES controller for ETH port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTETHPORTSERDESCONTROLLER_H_
#define _THASTMPWPRODUCTETHPORTSERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/physical/ThaSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductEthPortSerdesController * ThaStmPwProductEthPortSerdesController;

typedef struct tThaStmPwProductEthPortSerdesControllerMethods
    {
    uint32 (*PllRegister)(ThaStmPwProductEthPortSerdesController self);
    uint32 (*PllStickyMask)(ThaStmPwProductEthPortSerdesController self);
    }tThaStmPwProductEthPortSerdesControllerMethods;

typedef struct tThaStmPwProductEthPortSerdesController
    {
    tThaSerdesController super;
    const tThaStmPwProductEthPortSerdesControllerMethods *methods;
    }tThaStmPwProductEthPortSerdesController;

typedef struct tThaStmPwProductEthPort10GSerdesController
    {
    tThaStmPwProductEthPortSerdesController super;
    }tThaStmPwProductEthPort10GSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController ThaStmPwProductEthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);
AtSerdesController ThaStmPwProductEthPort10GSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

uint32 ThaEthPortIdGet(ThaSerdesController self);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTETHPORTSERDESCONTROLLER_H_ */


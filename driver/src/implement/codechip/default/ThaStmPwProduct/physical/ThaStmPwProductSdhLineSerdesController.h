/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : ThaPdhStmProductSdhLineSerdesController.h
 * 
 * Created Date: Sep 25, 2013
 *
 * Description : Line SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPWPRODUCTSDHLINESERDESCONTROLLER_H_
#define _THASTMPWPRODUCTSDHLINESERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/physical/ThaSdhLineSerdesControllerInternal.h"
#include "../../../../default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPwProductSdhLineSerdesController
    {
    tThaSdhLineSerdesController super;
    }tThaStmPwProductSdhLineSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController ThaStmPwProductSdhLineSerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPWPRODUCTSDHLINESERDESCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60031035EthFlowHeaderProvider.h
 * 
 * Created Date: Jan 8, 2015
 *
 * Description : Header provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031035ETHFLOWHEADERPROVIDER_H_
#define _THA60031035ETHFLOWHEADERPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60031032/eth/Tha60031032EthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031035EthFlowHeaderProvider
    {
    tTha60031032EthFlowHeaderProvider super;
    }tTha60031035EthFlowHeaderProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowHeaderProvider Tha60031035EthFlowHeaderProviderObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60031035EthFlowHeaderProviderNew(AtModuleEth ethModule);

#endif /* _THA60031035ETHFLOWHEADERPROVIDER_H_ */


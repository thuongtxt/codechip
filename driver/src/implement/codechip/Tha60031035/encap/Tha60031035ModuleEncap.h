/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60031035ModuleEncap.h
 * 
 * Created Date: Jan 30, 2015
 *
 * Description : Ecanp module of product 60091135
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031035MODULEENCAP_H_
#define _THA60031035MODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../codechip/Tha60031032/encap/Tha60031032ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031035ModuleEncap
    {
    tTha60031032ModuleEncap super;
    }tTha60031035ModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap Tha60031035ModuleEncapObjectInit(AtModuleEncap self, AtDevice device);

#endif /* _THA60031035MODULEENCAP_H_ */


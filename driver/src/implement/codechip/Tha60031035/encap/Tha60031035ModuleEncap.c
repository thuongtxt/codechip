/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60031035ModuleEncap.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Encapsulation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031035ModuleEncap.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaModuleEncapMethods m_ThaModuleEncapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031035ModuleEncap);
    }

static uint32 StartDeviceVersionThatSupportsFrameRelay(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 1, 0);
    }

static uint32 StartDeviceVersionThatSupportsIdleByteCounter(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 4, 0);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap encap = (ThaModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(encap), sizeof(m_ThaModuleEncapOverride));

        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsFrameRelay);
        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsIdleByteCounter);
        }

    mMethodsSet(encap, &m_ThaModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideThaModuleEncap(self);
    }

AtModuleEncap Tha60031035ModuleEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60031035ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return Tha60031035ModuleEncapObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60031035ModuleSdhInternal.h
 * 
 * Created Date: Oct 30, 2014
 *
 * Description : SDH module of product 60031035
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031035MODULESDHINTERNAL_H_
#define _THA60031035MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60031032/sdh/Tha60031032ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031035ModuleSdh
    {
    tTha60031032ModuleSdh super;
    }tTha60031035ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60031035ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031035MODULESDHINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60031035ModulePdh.c
 *
 * Created Date: Aug 19, 2013
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/pdh/Tha60031032ModulePdhInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031035ModulePdh
    {
    tTha60031031ModulePdh super;
    }tTha60031035ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdhMethods m_ThaModulePdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031035ModulePdh);
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    AtUnused(self);
    AtUnused(partId);
    return cAtTrue;
    }

static uint32 StartVersionSupportEbitAlarm(ThaModulePdh self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 4, 0);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(module), sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportEbitAlarm);
        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideThaModulePdh(self);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60031035ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

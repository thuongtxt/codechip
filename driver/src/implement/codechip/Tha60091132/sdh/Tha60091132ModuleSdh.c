/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60091132ModuleSdh.c
 *
 * Created Date: Oct 23, 2014
 *
 * Description : SDH module of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/sdh/Tha60031032ModuleSdhInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132ModuleSdh
    {
    tTha60031032ModuleSdh super;
    }tTha60091132ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleSdhMethods               m_ThaModuleSdhOverride;
static tThaStmPwProductModuleSdhMethods m_ThaStmPwProductModuleSdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ModuleSdh);
    }

static uint32 StartVersionSupportsSerdesTimingMode(ThaStmPwProductModuleSdh self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x1, 0x1, 0x6);
    }

static uint32 StartVersionSupport24BitsBlockErrorCounter(ThaModuleSdh self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 5, 0);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, StartVersionSupport24BitsBlockErrorCounter);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideThaStmPwProductModuleSdh(AtModuleSdh self)
    {
    ThaStmPwProductModuleSdh sdhModule = (ThaStmPwProductModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaStmPwProductModuleSdhOverride));

        mMethodOverride(m_ThaStmPwProductModuleSdhOverride, StartVersionSupportsSerdesTimingMode);
        }

    mMethodsSet(sdhModule, &m_ThaStmPwProductModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh(self);
    OverrideThaStmPwProductModuleSdh(self);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60091132ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60091132ClaEthFlowController.c
 *
 * Created Date: Jan 13, 2015
 *
 * Description : CLA ETH FLOW Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/cla/Tha60031032ClaEthFlowController.h"
#include "../../../default/cla/ppp/ThaModuleClaPppReg.h"

#include "../eth/Tha60091132ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * This bit field of register CLA Operation Control
 * 0x44C020
 * ClaZTPPortIDSelect: enable to select PortID from higig header for lookup
       0x0: disable
       0x1: enable
 */
#define cThaClaZTPPortIDSelectMask  cBit7
#define cThaClaZTPPortIDSelectShift 7

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132ClaEthFlowController
    {
    tTha60031032ClaEthFlowController super;
    }tTha60091132ClaEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods  m_ThaClaControllerOverride;
static tThaClaEthFlowControllerMethods  m_ThaClaEthFlowControllerOverride;

/* Save super implementation */
static const tThaClaControllerMethods *m_ThaClaControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 VlanLookupOffset(ThaClaEthFlowController self, uint16 lkType)
    {
	AtUnused(lkType);
	AtUnused(self);
    return 0x0;
    }

static ThaModuleCla ClaModule(ThaClaController self)
    {
    return ThaClaControllerModuleGet(self);
    }

static void HigigPortLookupEnable(ThaClaController self, uint8 partId)
    {
    ThaModuleCla claModule  = ClaModule(self);
    ThaModuleEth ethModule  = (ThaModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)claModule), cAtModuleEth);
    uint8 hwHigigPortLookup = ThaModuleEthPort10GbSupported(ethModule) ? 0x1 : 0x0;
    uint32 regAddr          = cThaClaOperationCtrl + ThaModuleClaPartOffset(claModule, partId);
    uint32 regVal           = mModuleHwRead(claModule, regAddr);

    mRegFieldSet(regVal, cThaClaZTPPortIDSelect, hwHigigPortLookup);
    mModuleHwWrite(claModule, regAddr, regVal);
    }

static eAtRet PartDefaultSet(ThaClaController self, uint8 partId)
    {
    eAtRet ret;
    AtDevice device = AtModuleDeviceGet((AtModule)ClaModule(self));
    Tha60091132ModuleEth ethModule = (Tha60091132ModuleEth)AtDeviceModuleGet(device, cAtModuleEth);

    /* Call super */
    ret = m_ThaClaControllerMethods->PartDefaultSet(self, partId);
    if (ret != cAtOk)
        return ret;

    if (!Tha60091132HigigUseStmPortIdFromTags(ethModule))
        HigigPortLookupEnable(self, partId);

    return cAtOk;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ClaEthFlowController);
    }

static void OverrideThaClaController(ThaClaEthFlowController self)
    {
    ThaClaController controller = (ThaClaController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, PartDefaultSet);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void OverrideThaClaEthFlowController(ThaClaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthFlowControllerOverride));
        mMethodOverride(m_ThaClaEthFlowControllerOverride, VlanLookupOffset);
        }

    mMethodsSet(self, &m_ThaClaEthFlowControllerOverride);
    }

static void Override(ThaClaEthFlowController self)
    {
    OverrideThaClaEthFlowController(self);
    OverrideThaClaController(self);
    }

static ThaClaEthFlowController ObjectInit(ThaClaEthFlowController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ClaEthFlowControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthFlowController Tha60091132ClaEthFlowControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60091132EthFlowHeaderProvider.c
 *
 * Created Date: Jan 8, 2015
 *
 * Description : ETH Flow header provider of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "../../Tha60031032/eth/Tha60031032EthFlowHeaderProvider.h"
#include "../eth/Tha60091132ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132EthFlowHeaderProvider
    {
    tTha60031032EthFlowHeaderProvider super;
    }tTha60091132EthFlowHeaderProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowHeaderProviderMethods m_ThaEthFlowHeaderProviderOverride;

/* Save super implementation */
static const tThaEthFlowHeaderProviderMethods *m_ThaEthFlowHeaderProviderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
uint32 Tha6009EthFlowHeaderProviderVlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId);

/*--------------------------- Implementation ---------------------------------*/
static uint32 VlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId)
    {
    Tha60091132ModuleEth moduleEth = (Tha60091132ModuleEth)ThaEthFlowHeaderProviderModuleGet(self);
	AtUnused(self);

    if (Tha60091132HigigUseStmPortIdFromTags(moduleEth))
        return m_ThaEthFlowHeaderProviderMethods->VlanIdSw2Hw(self, flow, vlanId);

    return Tha6009EthFlowHeaderProviderVlanIdSw2Hw(self, flow, vlanId);
    }

static void OverrideThaEthFlowHeaderProvider(ThaEthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowHeaderProviderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowHeaderProviderOverride, m_ThaEthFlowHeaderProviderMethods, sizeof(m_ThaEthFlowHeaderProviderOverride));

        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, VlanIdSw2Hw);
        }

    mMethodsSet(self, &m_ThaEthFlowHeaderProviderOverride);
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    OverrideThaEthFlowHeaderProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132EthFlowHeaderProvider);
    }

static ThaEthFlowHeaderProvider ObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032EthFlowHeaderProviderObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowHeaderProvider Tha60091132EthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProvider, ethModule);
    }

uint32 Tha6009EthFlowHeaderProviderVlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId)
    {
    uint8 channelId;
    uint16 stmPortId, isMlpppIma;
    AtUnused(self);

    if (flow == NULL)
        return cBit7_0;

    channelId  =  vlanId & cBit7_0;
    isMlpppIma =  vlanId & cBit11;

    /* As require of ZTE, they don't want our FPGA checked STM port when
     * received packet at ETH side */
    if (Tha6009EthFlowIgnoreHigigStmPortLookup(flow))
        stmPortId = 0x0;
    else
        stmPortId  = (uint16)(Tha60091132EthFlowExpectedStmPortFromCache(flow) << 8);

    return (uint32)(isMlpppIma | stmPortId | channelId);
    }

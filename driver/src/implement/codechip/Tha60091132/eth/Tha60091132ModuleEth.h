/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60091132ModuleEth.h
 * 
 * Created Date: Jan 16, 2015
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132MODULEETH_H_
#define _THA60091132MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60091132ModuleEth * Tha60091132ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60091132HigigUseStmPortIdFromTags(Tha60091132ModuleEth self);

eAtRet Tha60091132EthFlowTxStmPortSet(AtEthFlow flow, uint8 stmPortId);
uint8 Tha60091132EthFlowTxStmPortGet(AtEthFlow flow);
eAtRet Tha60091132EthFlowExpectedStmPortSet(AtEthFlow flow, uint8 stmPortId, uint8 isMlpppIma, uint16 channelId);
eAtRet Tha60091132EthFlowExpectedStmPortClear(AtEthFlow flow, uint8 stmPortId, uint8 isMlpppIma, uint16 channelId);
uint8 Tha60091132EthFlowExpectedStmPortGet(AtEthFlow flow);
eBool Tha60091132HigigUseStmPortIdFromTags(Tha60091132ModuleEth self);
eBool Tha6009EthFlowIgnoreHigigStmPortLookup(AtEthFlow flow);

#endif /* _THA60091132MODULEETH_H_ */


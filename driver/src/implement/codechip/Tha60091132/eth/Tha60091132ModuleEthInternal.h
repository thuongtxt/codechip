/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60091132ModuleEthInternal.h
 * 
 * Created Date: Oct 23, 2014
 *
 * Description : ETH module of product 60091132
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132MODULEETHINTERNAL_H_
#define _THA60091132MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60031032/eth/Tha60031032ModuleEthInternal.h"
#include "Tha60091132ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60091132ModuleEthMethods
    {
    uint32 (*StartVersionSupportHigigStmPortLookup)(Tha60091132ModuleEth self);
    uint32 (*StartVersionIgnoreMpFlowHigigStmPortLookup)(Tha60091132ModuleEth self);
    }tTha60091132ModuleEthMethods;

typedef struct tTha60091132ModuleEth
    {
    tTha60031032ModuleEth super;
    const tTha60091132ModuleEthMethods *methods;
    }tTha60091132ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60091132ModuleEthObjectInit(AtModuleEth self, AtDevice device);
eBool Tha6009EthFlowIgnoreHigigStmPortLookup(AtEthFlow flow);

AtEthFlow Tha60091132EthFlowNew(uint32 flowId, AtModuleEth module);
void Tha60091132EthFlowTxStmPortIdCache(AtEthFlow self, uint8 txStmPortId);
uint8 Tha60091132EthFlowTxStmPortIdFromCache(AtEthFlow self);
void Tha60091132EthFlowExpectedStmPortIdCache(AtEthFlow self, uint8 expectStmPortId);
uint8 Tha60091132EthFlowExpectedStmPortFromCache(AtEthFlow self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60091132MODULEETHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60091132ModuleEthReg.h
 * 
 * Created Date: Jan 6, 2015
 *
 * Description : ETH module of product 60091132
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132MODULEETHREG_H_
#define _THA60091132MODULEETHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: Ip Ethernet 10G Speed RX ZTE Tag Lookup Port1 Control
Reg Addr: 0x3c2000-0x3c2FFF
    The address format for these registers is 0x3c2000 + VLANID[8]*2048 + higig_portid*256 + VLANID[7:0]
    Where:
        � VLANID[8] (0-1): is bit 8 th in field CHANNEL_ID of ZTE Tag2st
        � higig_portid (0-7) is portID field in the higig header
        � VLANID[7:0] (0-256) is bit[7:0] in field CHANNEL_ID of ZTE Tag2st
Description: This register is used to select STM core group which the incoming packets of 10G port1 will be
transferred. Core#1 is group of STM1#1-4, and Core#2 is group of STM1#5-8.
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbRxZteTagLookupPort     0x3c2000

/*------------------------------------------------------------------
BitField Name: IpEth10GPortLkGrpID
BitField Type: R/W
BitField Desc: Group STM1 port which the packets will be transferred
               0: group of STM1#1-4
               1: group of STM1#5-8
BitField Bits: 0
--------------------------------------------------------------------*/
#define cThaIpEth10GPortLookupGrpIdMask            cBit0
#define cThaIpEth10GPortLookupGrpIdShift           0

/*------------------------------------------------------------------
BitField Name: IpEth10GPortLkEn
BitField Type: R/W
BitField Desc: Discard the incoming packets or not
               0: discard
               1: not discard
BitField Bits: 1
--------------------------------------------------------------------*/
#define cThaIpEth10GPortLookupEnMask                cBit1
#define cThaIpEth10GPortLookupEnShift               1

#endif /* _THA60091132MODULEETHREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60091132EthFlow.c
 *
 * Created Date: Aug 25, 2016
 *
 * Description : Ethernet flow of 60091032
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "Tha60091132ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60091132EthFlow*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132EthFlow
    {
    tThaEthFlow super;

    uint8 txStmPortId;
    uint8 expectStmPortId;
    }tTha60091132EthFlow;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132EthFlow);
    }

static AtEthFlow ObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowObjectInit((AtEthFlow)self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
    m_methodsInit = 1;

    return self;
    }

AtEthFlow Tha60091132EthFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, flowId, module);
    }

void Tha60091132EthFlowTxStmPortIdCache(AtEthFlow self, uint8 txStmPortId)
    {
    if (self)
        mThis(self)->txStmPortId = txStmPortId;
    }

uint8 Tha60091132EthFlowTxStmPortIdFromCache(AtEthFlow self)
    {
    if (self)
        return mThis(self)->txStmPortId;
    return cInvalidUint8;
    }

void Tha60091132EthFlowExpectedStmPortIdCache(AtEthFlow self, uint8 expectStmPortId)
    {
    if (self)
        mThis(self)->expectStmPortId = expectStmPortId;
    }

uint8 Tha60091132EthFlowExpectedStmPortFromCache(AtEthFlow self)
    {
    if (self)
        return mThis(self)->expectStmPortId;
    return cInvalidUint8;
    }

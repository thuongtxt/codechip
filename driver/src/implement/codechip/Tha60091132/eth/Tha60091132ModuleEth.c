/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60091132ModuleEth.c
 *
 * Created Date: Feb 7, 2014
 *
 * Description : ETH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60091132ModuleEthInternal.h"
#include "Tha60091132ModuleEthReg.h"

#include "../../default/ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"
#include "../../Tha60031031/eth/Tha60031031ModuleEthInternal.h"
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "../../../default/eth/controller/ThaEthFlowHeaderProvider.h"
#include "../../../default/eth/ThaEthPort10GbReg.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/prbs/ThaPrbsEngineSerdes.h"
#include "../../../default/ppp/ThaMpigReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidUint8  0xFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((Tha60091132ModuleEth)self)
#define mEthFlow(self)  ((ThaEthFlow)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60091132ModuleEthMethods m_methods;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/* Save super implementation */
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController FlowController(AtEthFlow flow)
    {
    return ThaEthFlowControllerGet((ThaEthFlow)flow);
    }

static Tha60091132ModuleEth EthModule(AtEthFlow flow)
    {
    return mThis(AtChannelModuleGet((AtChannel)flow));
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return ThaEthPort10GbNew(portId, self);
    }

static eBool PortShouldByPassFcs(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtFalse;
    }

static AtEthFlowControl PortFlowControlCreate(AtModuleEth self, AtEthPort port)
    {
    return ThaEthPortFlowControl10GbNew((AtModule)self, port);
    }

static eBool PortPllCanBeChecked(ThaModuleEth self, uint8 portId)
    {
    if (portId == 1)
        return cAtFalse;
    return m_ThaModuleEthMethods->PortPllCanBeChecked(self, portId);
    }

static eBool Port10GbSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool ApsIsSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool UsePortSourceMacForAllFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 MpigOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint8 partId = ThaEthFlowControllerPartIdGet(self, flow);
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet((AtChannel)flow), cThaModuleMpig, partId);
    }

static uint32 MpigFlowOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 moduleOffset = MpigOffset(self, flow);
    return ThaEthFlowHwFlowIdGet((ThaEthFlow)flow) + moduleOffset;
    }

static eAtRet FlowControllerStmPortSet(ThaModuleEth self, AtEthFlow flow, uint8 stmPortId)
    {
    uint32 address, regVal;
    ThaEthFlowController controller = FlowController(flow);
    if (controller == NULL)
        return cAtErrorNullPointer;

    if (!ThaModuleEthPort10GbSupported(self))
        return cAtOk;

    address = ThaEthFlowControllerMpigDatDeQueRegisterAddress(controller) + MpigFlowOffset(controller, flow);
    regVal  = mChannelHwRead(controller, address, cThaModuleMpig);
    mRegFieldSet(regVal, cThaFlowInStmPort, stmPortId);
    mChannelHwWrite(controller, address, regVal, cThaModuleMpig);

    /* Updated database */
    Tha60091132EthFlowTxStmPortIdCache(flow, stmPortId);

    return cAtOk;
    }

static eAtRet DidChangeFlowEgressVlan(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *vlanDesc)
    {
    uint8 stmPortId;

    /* No need to configure here if FPGA supports control HIGIG STM port
     * separately with user tag */
    if (!Tha60091132HigigUseStmPortIdFromTags((Tha60091132ModuleEth)self))
        return cAtOk;

    stmPortId = ThaStmPwProductModuleEthPortIdFromCVlan(self, &(vlanDesc->vlans[0]));
    return FlowControllerStmPortSet(self, flow, stmPortId);
    }

static uint32 ExpectedChannelId(uint8 isMlpppIma, uint8 stmPortId, uint16 channelId)
    {
    return (uint32)((isMlpppIma & cBit0) << 11) + (uint32)(stmPortId << 8) + channelId;
    }

static eAtRet EthFlowExpectedStmPortSet(AtEthFlow flow, uint8 stmPortId, uint8 isMlpppIma, uint16 channelId)
    {
    uint8 partId;
    uint32 regAddr, regValue, offset;
    ThaEthFlowController controller = FlowController(flow);
    uint8 expectStmPort = stmPortId;

    if (controller == NULL)
        return cAtErrorNotActivate;

    /* As require of ZTE, they don't want our FPGA checked STM port when
     * received packet at ETH side */
    if (Tha6009EthFlowIgnoreHigigStmPortLookup(flow))
        expectStmPort = 0;

    partId = ThaEthFlowControllerPartIdGet(controller, flow);
    offset = ExpectedChannelId(isMlpppIma, expectStmPort, channelId);

    /* Configure 10G PHY port#1 */
    regAddr  = cThaRegIpEthernet10GbRxZteTagLookupPort + offset;
    regValue = mChannelHwRead(controller, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GPortLookupGrpId, partId ? 0x1 : 0x0);
    mRegFieldSet(regValue, cThaIpEth10GPortLookupEn, 0x1);
    mChannelHwWrite(controller, regAddr, regValue, cAtModuleEth);

    /* Configure 10G PHY port#2 */
    regAddr  = cThaRegIpEthernet10GbRxZteTagLookupPort + 0x1000 + offset;
    regValue = mChannelHwRead(controller, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GPortLookupGrpId, partId ? 0x1 : 0x0);
    mRegFieldSet(regValue, cThaIpEth10GPortLookupEn, 0x1);
    mChannelHwWrite(controller, regAddr, regValue, cAtModuleEth);

    Tha60091132EthFlowExpectedStmPortIdCache(flow, stmPortId);

    return cAtOk;
    }

static eAtRet EthFlowExpectedStmPortClear(AtEthFlow flow, uint8 stmPortId, uint8 isMlpppIma, uint16 channelId)
    {
    uint32 regAddr, offset;
    ThaEthFlowController controller = FlowController(flow);

    if (controller == NULL)
        return cAtErrorNotActivate;

    offset = ExpectedChannelId(isMlpppIma, stmPortId, channelId);

    /* Configure 10G PHY port#1 */
    regAddr  = cThaRegIpEthernet10GbRxZteTagLookupPort + offset;
    mChannelHwWrite(controller, regAddr, 0, cAtModuleEth);

    /* Configure 10G PHY port#2 */
    regAddr  = cThaRegIpEthernet10GbRxZteTagLookupPort + 0x1000 + offset;
    mChannelHwWrite(controller, regAddr, 0, cAtModuleEth);

    Tha60091132EthFlowExpectedStmPortIdCache(flow, 0xFF);

    return cAtOk;
    }

static uint32 StartVersionSupportHigigStmPortLookup(Tha60091132ModuleEth self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x1, 0x3, 0x0);
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return Tha60091132EthFlowHeaderProviderNew((AtModuleEth)self);
    }

static uint32 VlanIdFromVlanDesc(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    uint8 vlanIndexToLookup = mMethodsGet(self)->VlanToLookUp(self);
    uint8 isMlppp    = (uint8)((desc->vlans[vlanIndexToLookup].vlanId & cBit8) >> 8);
    uint16 channelId = (uint16)(desc->vlans[vlanIndexToLookup].vlanId & cBit7_0);
    uint8 stmPortId  = (uint8)(Tha6009EthFlowIgnoreHigigStmPortLookup(flow) ? 0x0 : Tha60091132EthFlowExpectedStmPortFromCache(flow));
    return ExpectedChannelId(isMlppp, stmPortId, channelId);
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return Tha60091132PrbsEngineSerdesEthPortNew(AtEthPortSerdesController(ethPort));
    }

static uint32 StartVersionIgnoreMpFlowHigigStmPortLookup(Tha60091132ModuleEth self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x1, 0x4, 0x0);
    }

static uint32 StartVersionHas10GDiscardCounter(ThaModuleEth self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x1, 0x4, 0x0);
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return cThaReg10GMacBaseAddress;
    }

static AtEthFlow FlowCreate(AtModuleEth self, uint16 flowId)
    {
    AtEthFlow flow = Tha60091132EthFlowNew(flowId, self);
    AtChannelInit((AtChannel)flow);
    return flow;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ModuleEth);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, PortFlowControlCreate);
        mMethodOverride(m_AtModuleEthOverride, FlowCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth thaModuleEth = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(thaModuleEth);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, m_ThaModuleEthMethods, sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortPllCanBeChecked);
        mMethodOverride(m_ThaModuleEthOverride, PortShouldByPassFcs);
        mMethodOverride(m_ThaModuleEthOverride, Port10GbSupported);
        mMethodOverride(m_ThaModuleEthOverride, ApsIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, UsePortSourceMacForAllFlows);
        mMethodOverride(m_ThaModuleEthOverride, DidChangeFlowEgressVlan);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, VlanIdFromVlanDesc);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionHas10GDiscardCounter);
        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        }

    mMethodsSet(thaModuleEth, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static void MethodsInit(Tha60091132ModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionSupportHigigStmPortLookup);
        mMethodOverride(m_methods, StartVersionIgnoreMpFlowHigigStmPortLookup);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleEth Tha60091132ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60091132ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60091132ModuleEthObjectInit(newModule, device);
    }

eAtRet Tha60091132EthFlowTxStmPortSet(AtEthFlow flow, uint8 stmPortId)
    {
    Tha60091132ModuleEth moduleEth;

    if (flow == NULL)
        return cAtErrorNullPointer;

    moduleEth = EthModule(flow);
    if (Tha60091132HigigUseStmPortIdFromTags(moduleEth))
        return cAtOk;

    return FlowControllerStmPortSet((ThaModuleEth)moduleEth, flow, stmPortId);
    }

uint8 Tha60091132EthFlowTxStmPortGet(AtEthFlow flow)
    {
    return Tha60091132EthFlowTxStmPortIdFromCache(flow);
    }

eAtRet Tha60091132EthFlowExpectedStmPortSet(AtEthFlow flow, uint8 stmPortId, uint8 isMlpppIma, uint16 channelId)
    {
    if (flow == NULL)
        return cAtErrorNullPointer;

    if (Tha60091132HigigUseStmPortIdFromTags(EthModule(flow)))
        return cAtOk;

    return EthFlowExpectedStmPortSet(flow, stmPortId, isMlpppIma, channelId);
    }

eAtRet Tha60091132EthFlowExpectedStmPortClear(AtEthFlow flow, uint8 stmPortId, uint8 isMlpppIma, uint16 channelId)
    {
    if (flow == NULL)
        return cAtErrorNullPointer;

    if (Tha60091132HigigUseStmPortIdFromTags(EthModule(flow)))
        return cAtOk;

    return EthFlowExpectedStmPortClear(flow, stmPortId, isMlpppIma, channelId);
    }

uint8 Tha60091132EthFlowExpectedStmPortGet(AtEthFlow flow)
    {
    if (flow == NULL)
        return cInvalidUint8;

    if (Tha60091132HigigUseStmPortIdFromTags(EthModule(flow)))
        return cInvalidUint8;

    return Tha60091132EthFlowExpectedStmPortFromCache(flow);
    }

eBool Tha60091132HigigUseStmPortIdFromTags(Tha60091132ModuleEth self)
    {
    AtDevice device;
    uint32 startSupportedVersion;

    if (self == NULL)
        return cAtFalse;

    device = AtModuleDeviceGet((AtModule)self);
    startSupportedVersion = mMethodsGet(self)->StartVersionSupportHigigStmPortLookup(self);

    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtFalse : cAtTrue;
    }

eBool Tha6009EthFlowIgnoreHigigStmPortLookup(AtEthFlow flow)
    {
    if (AtEthFlowMpBundleGet(flow))
        {
        Tha60091132ModuleEth moduleEth = EthModule(flow);
        AtDevice device = AtModuleDeviceGet((AtModule)moduleEth);
        uint32 startVersionSupport = mMethodsGet(moduleEth)->StartVersionIgnoreMpFlowHigigStmPortLookup(moduleEth);
        return (AtDeviceVersionNumber(device) >= startVersionSupport) ? cAtTrue : cAtFalse;
        }

    return cAtFalse;
    }

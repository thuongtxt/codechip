/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60091132EthPortPktAnalyzer.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : Packet Analyzer of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60091132EthPortPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Save supper implement */
static tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
uint32 Tha10GProductPktAnalyzerStartAddress(ThaPktAnalyzer self);
uint32 Tha10GPktAnalyzerEthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode);
uint32 Tha10GPktAnalyzerEthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode);
eBool Tha10GbPktAnalyzerShouldRecapture(ThaPktAnalyzer self, uint8 pktDumpMode, uint8 currentDumpMode);


/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132EthPortPktAnalyzer);
    }

static uint32 StartAddress(ThaPktAnalyzer self)
    {
    return Tha10GProductPktAnalyzerStartAddress(self);
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    return Tha10GPktAnalyzerEthPortHwDumpMode(self, pktDumpMode);
    }

static uint32 EthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
    return Tha10GPktAnalyzerEthPortDumpModeHw2Sw(self, pktDumpMode);
    }

static eBool HigigIsSupported(AtPktAnalyzer self)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    ThaModulePktAnalyzer analyzer = (ThaModulePktAnalyzer)AtDeviceModuleGet(device, cAtModulePktAnalyzer);
    return ThaModulePktAnalyzerHigigEthPacketIsSupported(analyzer);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    if (HigigIsSupported(self))
        return Tha60091132EthHigigPacketNew(data, length, cAtPacketCacheModeCacheData);

    return m_AtPktAnalyzerMethods->PacketCreate(self, data, length, direction);
    }

static eBool ShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode)
    {
    return Tha10GbPktAnalyzerShouldRecapture(self, newPktDumpMode, currentDumpMode);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortHwDumpMode);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortDumpModeHw2Sw);
        mMethodOverride(m_ThaPktAnalyzerOverride, ShouldRecapture);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032EthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60091132EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

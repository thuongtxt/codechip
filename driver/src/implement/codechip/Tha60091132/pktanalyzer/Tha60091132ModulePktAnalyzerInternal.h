/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60091132ModulePktAnalyzerInternal.h
 * 
 * Created Date: Nov 18, 2014
 *
 * Description : Module Packet Analyzer of product 60091132
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132MODULEPKTANALYZERINTERNAL_H_
#define _THA60091132MODULEPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60031032/pktanalyzer/Tha60031032ModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60091132ModulePktAnalyzer
    {
    tTha60031032ModulePktAnalyzer super;
    }tTha60091132ModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer Tha60091132ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60091132MODULEPKTANALYZERINTERNAL_H_ */


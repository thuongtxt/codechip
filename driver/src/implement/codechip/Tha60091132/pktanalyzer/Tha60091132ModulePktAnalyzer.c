/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60091132ModulePktAnalyzer.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : Packet analyzer module of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60091132ModulePktAnalyzerInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePktAnalyzerMethods  m_AtModulePktAnalyzerOverride;
static tThaModulePktAnalyzerMethods m_ThaModulePktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
	AtUnused(self);
    return Tha60091132EthPortPktAnalyzerNew(port);
    }

static AtPktAnalyzer EthFlowPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthFlow flow)
    {
	AtUnused(self);
    return Tha60091132EthFlowPktAnalyzerNew(flow);
    }

static AtPktAnalyzer PppLinkPktAnalyzerCreate(AtModulePktAnalyzer self, AtPppLink link)
    {
	AtUnused(self);
    return Tha60091132PppLinkPktAnalyzerNew(link);
    }

static uint32 StartVersionSupportsHigig(ThaModulePktAnalyzer self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 0, 4);
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, PppLinkPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, EthFlowPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void OverrideThaModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    ThaModulePktAnalyzer analyzer = (ThaModulePktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaModulePktAnalyzerOverride));

        mMethodOverride(m_ThaModulePktAnalyzerOverride, StartVersionSupportsHigig);
        }

    mMethodsSet(analyzer, &m_ThaModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtModulePktAnalyzer(self);
    OverrideThaModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ModulePktAnalyzer);
    }

AtModulePktAnalyzer Tha60091132ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer Tha60091132ModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60091132ModulePktAnalyzerObjectInit(newModule, device);
    }

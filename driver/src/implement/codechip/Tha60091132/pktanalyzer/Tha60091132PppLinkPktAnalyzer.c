/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60091132PppLinkPktAnalyzer.c
 *
 * Created Date: Jul 1, 2014
 *
 * Description : Packet Analyzer of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pktanalyzer/ThaPppLinkPktAnalyzerInternal.h"
#include "../../../default/ppp/ThaPppLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132PppLinkPktAnalyzer
    {
    tThaPppLinkPktAnalyzer super;
    }tTha60091132PppLinkPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/
extern uint32 Tha10GProductPktAnalyzerStartAddress(ThaPktAnalyzer self);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132PppLinkPktAnalyzer);
    }

static uint32 StartAddress(ThaPktAnalyzer self)
    {
    return Tha10GProductPktAnalyzerStartAddress(self);
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 PppLinkHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    ThaPppLink link = (ThaPppLink)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);

    if (pktDumpMode == cThaPktAnalyzerDumpMpegToEnc)
        return (ThaPppLinkPartOffset(link) & cBit24) ? 0x20 : 0x10;

    if (pktDumpMode == cThaPktAnalyzerDumpDecToMpig)
        return (ThaPppLinkPartOffset(link) & cBit24) ? 0x25 : 0x15;

    /* Invalid value */
    return 0x8;
    }

static uint32 PppLinkDumpModeHw2Sw(ThaPktAnalyzer self, uint32 hwPktDumpMode)
    {
	AtUnused(self);
    if ((hwPktDumpMode == 0x10) || (hwPktDumpMode == 0x20))
        return cThaPktAnalyzerDumpMpegToEnc;

    if ((hwPktDumpMode == 0x15) || (hwPktDumpMode == 0x25))
        return cThaPktAnalyzerDumpDecToMpig;

    return cThaPktAnalyzerDumpUnknown;
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, PppLinkHwDumpMode);
        mMethodOverride(m_ThaPktAnalyzerOverride, PppLinkDumpModeHw2Sw);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideThaPktAnalyzer(self);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtPppLink link)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkPktAnalyzerObjectInit(self, link) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60091132PppLinkPktAnalyzerNew(AtPppLink link)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, link);
    }

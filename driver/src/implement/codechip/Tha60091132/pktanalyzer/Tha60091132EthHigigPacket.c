/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60091132EthHigigPacket.c
 *
 * Created Date: Sep 26, 2014
 *
 * Description : ETH Higig packet of Product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/pktanalyzer/Tha60031032EthPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132EthHigigPacket
    {
    tTha60031032EthPacket super;
    }tTha60091132EthHigigPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPacketMethods m_AtEthPacketOverride;

/*--------------------------- Forward declarations ---------------------------*/
uint8 ThaEthHigigPacketHeaderDisplay(AtEthPacket self, uint8 level);
uint8 ThaEthHigigPacketHeaderLength(AtEthPacket self);

/*--------------------------- Implementation ---------------------------------*/
static uint8 DisplayPreamble(AtEthPacket self, uint8 level)
    {
	AtUnused(self);
    return ThaEthHigigPacketHeaderDisplay(self, level);
    }

static uint8 PreambleLen(AtEthPacket self)
    {
    return ThaEthHigigPacketHeaderLength(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132EthHigigPacket);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket packet = (AtEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, mMethodsGet(packet), sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, DisplayPreamble);
        mMethodOverride(m_AtEthPacketOverride, PreambleLen);
        }

    mMethodsSet(packet, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtEthPacket(self);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032EthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket Tha60091132EthHigigPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

uint8 ThaEthHigigPacketHeaderDisplay(AtEthPacket self, uint8 level)
    {
    static const uint8 cStartOfFrame = 0xFB;
    uint8 *higigs = AtPacketDataBuffer((AtPacket)self, NULL);
    eBool headerValid = (higigs[0] == cStartOfFrame) ? cAtTrue : cAtFalse;
    uint8 len = ThaEthHigigPacketHeaderLength(self);

    AtPacketPrintSpaces(level);
    AtPrintc(headerValid ? cSevNormal : cSevCritical, "%s", headerValid ? "* Higig: " : "* Higig (Error): ");
    AtPacketDisplayBufferInHex((AtPacket)self, higigs, len, ".");
    AtPrintc(cSevNormal, "\r\n");

    return len;
    }

uint8 ThaEthHigigPacketHeaderLength(AtEthPacket self)
    {
	AtUnused(self);
    return 8;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60091132EthPortPktAnalyzer.h
 * 
 * Created Date: Sep 26, 2014
 *
 * Description : Packet Analyzer of product 60091132
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132ETHPORTPKTANALYZER_H_
#define _THA60091132ETHPORTPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../codechip/Tha60031032/pktanalyzer/Tha60031032EthPortPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60091132EthPortPktAnalyzer
    {
    tTha60031032EthPortPktAnalyzer super;
    }tTha60091132EthPortPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60091132ETHPORTPKTANALYZER_H_ */


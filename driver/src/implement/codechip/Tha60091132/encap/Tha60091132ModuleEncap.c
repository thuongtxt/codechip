/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60091132ModuleEncap.c
 *
 * Created Date: Jan 30, 2015
 *
 * Description : Encap module of 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/encap/Tha60031032ModuleEncap.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132ModuleEncap
    {
    tTha60031032ModuleEncap super;
    }tTha60091132ModuleEncap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEncapMethods m_ThaModuleEncapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ModuleEncap);
    }

static uint32 StartDeviceVersionThatSupportsFrameRelay(ThaModuleEncap self)
    {
	AtUnused(self);
    return 0x0;
    }

static uint32 StartDeviceVersionThatSupportsIdleByteCounter(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 5, 0);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap encap = (ThaModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(encap), sizeof(m_ThaModuleEncapOverride));

        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsFrameRelay);
        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsIdleByteCounter);
        }

    mMethodsSet(encap, &m_ThaModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideThaModuleEncap(self);
    }

static AtModuleEncap ObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60091132ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return ObjectInit(newModule, device);
    }

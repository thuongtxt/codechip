/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60091132ClockMonitor.h
 * 
 * Created Date: Jun 30, 2014
 *
 * Description : Clock Monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132CLOCKMONITOR_H_
#define _THA60091132CLOCKMONITOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
typedef struct tTha60091132ClockMonitor
    {
    tThaStmPwProductClockMonitor super;
    }tTha60091132ClockMonitor;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60091132CLOCKMONITOR_H_ */


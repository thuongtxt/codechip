/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha60091023ModuleClock.c
 *
 * Created Date: May 6, 2014
 *
 * Description : Clock Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/clock/Tha60031032ModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132ModuleClock
    {
    tTha60031032ModuleClock super;
    }tTha60091132ModuleClock;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClockMethods       m_ThaModuleClockOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClockMonitor ClockMonitorCreate(ThaModuleClock self)
    {
    return Tha60091132ClockMonitorNew((AtModuleClock)self);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ClockMonitorCreate);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideThaModuleClock(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ModuleClock);
    }

static AtModuleClock ObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60091132ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

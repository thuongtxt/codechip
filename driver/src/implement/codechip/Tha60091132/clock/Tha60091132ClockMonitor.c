/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60091023ClockMonitor.c
 *
 * Created Date: May 6, 2014
 *
 * Description : Clock Monitor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/clock/ThaModuleClock.h"

#include "Tha60091132ClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClockMonitorMethods m_ThaClockMonitorOverride;

/* Save super implementation */
static const tThaClockMonitorMethods *m_ThaClockMonitorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *GeClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    static uint32 geClockExpectedInKhz[] = {644532};
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(geClockExpectedInKhz);
    return geClockExpectedInKhz;
    }

static uint32 GeClockCheckRegister(ThaClockMonitor self)
    {
	AtUnused(self);
    return 0xF00067;
    }

static uint32 GeClockCheckMask(ThaClockMonitor self)
    {
	AtUnused(self);
    return cBit31_0;
    }

static uint32 GeClockMonitorKhzCalculation(ThaClockMonitor self, uint32 frequency)
    {
	AtUnused(self);
    return (frequency * 32); /* KHz */
    }

static void OverrideThaClockMonitor(ThaClockMonitor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClockMonitorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockMonitorOverride, mMethodsGet(self), sizeof(m_ThaClockMonitorOverride));

        mMethodOverride(m_ThaClockMonitorOverride, GeClockMonitorKhzCalculation);
        mMethodOverride(m_ThaClockMonitorOverride, GeClockCheckMask);
        mMethodOverride(m_ThaClockMonitorOverride, GeClockCheckRegister);
        mMethodOverride(m_ThaClockMonitorOverride, GeClockExpectedFrequenciesInKhz);
        }

    mMethodsSet(self, &m_ThaClockMonitorOverride);
    }

static void Override(ThaClockMonitor self)
    {
    OverrideThaClockMonitor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091132ClockMonitor);
    }

static ThaClockMonitor ObjectInit(ThaClockMonitor self, AtModuleClock clockModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClockMonitorObjectInit(self, clockModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClockMonitor Tha60091132ClockMonitorNew(AtModuleClock clockModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClockMonitor newMonitor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMonitor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newMonitor, clockModule);
    }

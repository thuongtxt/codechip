/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60091132PrbsEngineSerdesEthPort10Gb.c
 *
 * Created Date: Jan 19, 2015
 *
 * Description : ETH Prbs of product 60091132
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineSerdesInternal.h"

#include "Tha60091132PrbsEngineSerdesEthPort10GbReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPrbsEngine(self) ((ThaPrbsEngineSerdes)(self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091132PrbsEngineSerdesEthPort10Gb
    {
    tThaPrbsEngineSerdesEthPort super;
    }tTha60091132PrbsEngineSerdesEthPort10Gb;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implement */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModule ModuleId(AtPrbsEngine self)
    {
    AtModule module = AtChannelModuleGet(AtPrbsEngineChannelGet(self));
    return AtModuleTypeGet(module);
    }

static uint32 SerdesControllerId(AtPrbsEngine self)
    {
    return mMethodsGet(mPrbsEngine(self))->SerdesControllerId(mPrbsEngine(self));
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    uint32 serdesId = SerdesControllerId(self);

    mFieldIns(&regValue,
              cThaRegPrbs10GbPrbsTransmitEnableMask(serdesId),
              cThaRegPrbs10GbPrbsTransmitEnableShift(serdesId),
              enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regValue, ModuleId(self));

    return cAtOk;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    return (regValue & cThaRegPrbs10GbPrbsTransmitEnableMask(SerdesControllerId(self))) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    uint32 serdesId = SerdesControllerId(self);

    mFieldIns(&regValue,
              cThaRegPrbs10GbPrbsReceivedEnableMask(serdesId),
              cThaRegPrbs10GbPrbsReceivedEnableShift(serdesId),
              enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regValue, ModuleId(self));

    return cAtOk;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    return (regValue & cThaRegPrbs10GbPrbsReceivedEnableMask(SerdesControllerId(self))) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtPrbsEngineTxEnable(self, enable);
    ret |= AtPrbsEngineRxEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsEnabled(self) || AtPrbsEngineRxIsEnabled(self);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if ((prbsMode == cAtPrbsModePrbs15) || (prbsMode == cAtPrbsModePrbs31))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 ModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbs15) return 0x2;
    if (prbsMode == cAtPrbsModePrbs31) return 0x0;

    /* Invalid value */
    return cBit7_0;
    }

static eAtPrbsMode ModeHw2Sw(uint8 prbsMode)
    {
    if (prbsMode == 0x2) return cAtPrbsModePrbs15;
    if (prbsMode == 0x0) return cAtPrbsModePrbs31;

    /* Invalid value */
    return cAtPrbsModeInvalid;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regValue, serdesId;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    serdesId = SerdesControllerId(self);
    mFieldIns(&regValue,
              cThaRegPrbs10GbTransmitPrbsModeMask(serdesId),
              cThaRegPrbs10GbTransmitPrbsModeShift(serdesId),
              ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regValue, ModuleId(self));

    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint8 hwMode;
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    uint32 serdesId = SerdesControllerId(self);

    mFieldGet(regValue,
              cThaRegPrbs10GbTransmitPrbsModeMask(serdesId),
              cThaRegPrbs10GbTransmitPrbsModeShift(serdesId),
              uint8,
              &hwMode);

    return ModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regValue, serdesId;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    serdesId = SerdesControllerId(self);
    mFieldIns(&regValue,
              cThaRegPrbs10GbReceivedPrbsModeMask(serdesId),
              cThaRegPrbs10GbReceivedPrbsModeShift(serdesId),
              ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regValue, ModuleId(self));

    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint8 hwMode;
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    uint32 serdesId = SerdesControllerId(self);

    mFieldGet(regValue,
              cThaRegPrbs10GbReceivedPrbsModeMask(serdesId),
              cThaRegPrbs10GbReceivedPrbsModeShift(serdesId),
              uint8,
              &hwMode);

    return ModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    return AtPrbsEngineTxModeSet(self, prbsMode) | AtPrbsEngineRxModeSet(self, prbsMode);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    eAtPrbsMode mode = AtPrbsEngineTxModeGet(self);

    if (AtPrbsEngineModeIsSupported(self, mode))
        return mode;

    return AtPrbsEngineRxModeGet(self);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    uint32 serdesId = SerdesControllerId(self);

    mFieldIns(&regValue,
              cThaRegPrbs10GbPrbsForceErrorMask(serdesId),
              cThaRegPrbs10GbPrbsForceErrorShift(serdesId),
              mBoolToBin(force));
    AtPrbsEngineWrite(self, regAddr, regValue, ModuleId(self));

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr  = mMethodsGet(mPrbsEngine(self))->DiagnosticControlRegister(mPrbsEngine(self));
    uint32 regValue = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    return (regValue & cThaRegPrbs10GbPrbsForceErrorMask(SerdesControllerId(self))) ? cAtTrue : cAtFalse;
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 offset;

    if (!AtPrbsEngineCounterIsSupported(self, counterType))
        return cAtErrorModeNotSupport;

    offset = (SerdesControllerId(self) * 2048) + (r2c ? 0x0 : 0x1);
    switch(counterType)
        {
        case cAtPrbsEngineCounterTxFrame:
            return AtPrbsEngineRead(self, cThaRegPrbs10GbTransmitFrameCount + offset, ModuleId(self));
        case cAtPrbsEngineCounterRxFrame:
            return AtPrbsEngineRead(self, cThaRegPrbs10GbReceivedFrameCount + offset, ModuleId(self));
        case cAtPrbsEngineCounterRxBitError:
            return AtPrbsEngineRead(self, cThaRegPrbs10GbReceivedBitErrorCount + offset, ModuleId(self));

        default:
            return 0;
        }

    return 0;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 offset = SerdesControllerId(self) * 2048;
    uint32 regAddress = cThaRegPrbs10GbReceivedErrorSticky + offset;
    uint32 regValue = AtPrbsEngineRead(self, regAddress, ModuleId(self));

    AtPrbsEngineWrite(self, regAddress, regValue, ModuleId(self));

    if (regValue & cThaRegPrbs10GbReceivedLossSyncMask)
        return cAtPrbsEngineAlarmTypeLossSync;

    return cAtPrbsEngineAlarmTypeNone;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineSerdesEthPort);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineSerdesEthPortObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60091132PrbsEngineSerdesEthPortNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

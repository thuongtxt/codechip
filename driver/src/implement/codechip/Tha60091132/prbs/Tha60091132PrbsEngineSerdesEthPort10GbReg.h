/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60091132PrbsEngineSerdesEthPort10GbReg.h
 * 
 * Created Date: Jan 19, 2015
 *
 * Description : Register for ETH PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091132PRBSENGINESERDESETHPORT10GBREG_H_
#define _THA60091132PRBSENGINESERDESETHPORT10GBREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Reg Addr: 0xF00043 */
/*------------------------------------------------------------------
BitField Name: PRBS10GRxMd
BitField Type: R/W
BitField Desc: Receive PRBS mode at port#1,2
               0: prbs31
               2: prbs15
BitField Bits:
--------------------------------------------------------------------*/
#define cThaRegPrbs10GbReceivedPrbsModeMask(portId)   (((portId) == 0) ? cBit1_0 : cBit17_16)
#define cThaRegPrbs10GbReceivedPrbsModeShift(portId)  (((portId) == 0) ? 0 : 16)

/*------------------------------------------------------------------
BitField Name: PRBS10GRxEn
BitField Type: R/W
BitField Desc: Enable receive PRBS test at port#1,2
               0: disable
               1: enable
BitField Bits:
--------------------------------------------------------------------*/
#define cThaRegPrbs10GbPrbsReceivedEnableMask(portId)   (((portId) == 0) ? cBit3 : cBit19)
#define cThaRegPrbs10GbPrbsReceivedEnableShift(portId)  (((portId) == 0) ? 3 : 19)

/*------------------------------------------------------------------
BitField Name: PRBS10GTxErrForce
BitField Type: R/W
BitField Desc: Enable forced PRBS Error at port#1,2
               0: disable
               1: enable
BitField Bits:
--------------------------------------------------------------------*/
#define cThaRegPrbs10GbPrbsForceErrorMask(portId)   (((portId) == 0) ? cBit6 : cBit22)
#define cThaRegPrbs10GbPrbsForceErrorShift(portId)  (((portId) == 0) ? 6 : 22)

/*------------------------------------------------------------------
BitField Name: PRBS10GTxMd
BitField Type: R/W
BitField Desc: Transmit PRBS mode at port#1,2
               0: prbs31
               2: prbs1
BitField Bits:
--------------------------------------------------------------------*/
#define cThaRegPrbs10GbTransmitPrbsModeMask(portId)   (((portId) == 0) ? cBit5_4 : cBit21_20)
#define cThaRegPrbs10GbTransmitPrbsModeShift(portId)  (((portId) == 0) ? 4 : 20)

/*------------------------------------------------------------------
BitField Name: PRBS10GTxEn
BitField Type: R/W
BitField Desc: Enable transmit PRBS test at port#1,2
               0: disable
               1: enable
BitField Bits:
--------------------------------------------------------------------*/
#define cThaRegPrbs10GbPrbsTransmitEnableMask(portId)   (((portId) == 0) ? cBit7 : cBit23)
#define cThaRegPrbs10GbPrbsTransmitEnableShift(portId)  (((portId) == 0) ? 7 : 23)

/*------------------------------------------------------------------------------
Reg Name: PRBS 10G Receive PRBS Frame Counter
Reg Addr: 0xF61010 + PortID*2048 + R2C
    Where: PortID (0 � 1)
           R2C (0 � 1) : 0: read-to-clear, 1: read only
Description: This register is used to count the received PRBS frame
------------------------------------------------------------------------------*/
#define cThaRegPrbs10GbReceivedFrameCount      0xF61010

#define cThaRegPrbs10GbReceivedFrameCountMask  cBit15_0
#define cThaRegPrbs10GbReceivedFrameCountShift 0

/*------------------------------------------------------------------------------
Reg Name: PRBS 10G Receive PRBS Bit Error Counter
Reg Addr: 0xF61030 + PortID*2048 + R2C
    Where: PortID (0 � 1)
           R2C (0 � 1) : 0: read-to-clear, 1: read only
Description: This register is used to count the transmit PRBS frame
------------------------------------------------------------------------------*/
#define cThaRegPrbs10GbTransmitFrameCount      0xF61030

#define cThaRegPrbs10GbTransmitFrameCountMask  cBit15_0
#define cThaRegPrbs10GbTransmitFrameCountShift 0

/*------------------------------------------------------------------------------
Reg Name: PRBS 10G Receive PRBS Bit Error Counter
Reg Addr: 0xF61020 + PortID*2048 + R2C
    Where:PortID (0 � 1)
          R2C (0 � 1) : 0: read-to-clear, 1: read only
Description: This register is used to count the received bit error
------------------------------------------------------------------------------*/
#define cThaRegPrbs10GbReceivedBitErrorCount      0xF61020

#define cThaRegPrbs10GbReceivedBitErrorCountMask  cBit31_0
#define cThaRegPrbs10GbReceivedBitErrorCountShift 0

/*------------------------------------------------------------------------------
Reg Name: PRBS 10G Error Sticky
Reg Addr: 0xF61000 + PortID*2048
    Where:PortID (0 � 1)
Description: This register is used to sticky error at receive
------------------------------------------------------------------------------*/
#define cThaRegPrbs10GbReceivedErrorSticky         0xF61000

#define cThaRegPrbs10GbReceivedLossSyncMask        cBit0
#define cThaRegPrbs10GbReceivedLossSyncShift       0

#endif /* _THA60091132PRBSENGINESERDESETHPORT10GBREG_H_ */


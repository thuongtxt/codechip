/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60291011ModuleCla.h
 * 
 * Created Date: Oct 9, 2018
 *
 * Description : 60291011 module CLA interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULECLA_H_
#define _THA60291011MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60291011ModuleClaHbceLookingUpInformationCtrlBase(ThaModuleCla self);
ThaHbceMemoryPool Tha60291011HbceMemoryPoolNew(ThaHbce hbce);
ThaHbce Tha60291011HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaClaPwController Tha60291011ClaPwControllerNew(ThaModuleCla cla);
AtModule Tha60291011ModuleClaV2New(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULECLA_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60291011ClaPwControllerInternal.h
 * 
 * Created Date: Oct 8, 2018
 *
 * Description : 60291011 CLA PW controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011CLAPWCONTROLLERINTERNAL_H_
#define _THA60291011CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60290011/cla/controllers/Tha60290011ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011ClaPwController
    {
    tTha60290011ClaPwController super;
    }tTha60291011ClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291011CLAPWCONTROLLERINTERNAL_H_ */


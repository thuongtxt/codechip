/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60291011ClaPwController.c
 *
 * Created Date: Oct 8, 2018
 *
 * Description : 60291011 CLA PW controler implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ClaPwControllerInternal.h"
#include "../Tha60291011ModuleClaReg.h"
#include "../Tha60291011ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/* Save super implementation */
static const tThaClaPwControllerMethods *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 HbceMaxFlowsPerEntry(ThaClaPwController self)
    {
    AtUnused(self);
    return 8;
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60291011HbceNew(hbceId, (AtModule)mClaModule(self), core);
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaClaPwController self)
    {
    ThaModuleCla module = ThaClaControllerModuleGet((ThaClaController)self);
    return Tha60291011ModuleClaHbceLookingUpInformationCtrlBase(module);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));
        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceMaxFlowsPerEntry);
        mMethodOverride(m_ThaClaPwControllerOverride, ClaHbceLookingUpInformationCtrl);
        }
    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ClaPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60291011ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60291011HbceMemoryPool.c
 *
 * Created Date: Oct 9, 2018
 *
 * Description : 60291011 HBCE memory pool implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/util/AtUtil.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/util/ThaUtil.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"

#include "../Tha60291011ModuleClaReg.h"
#include "./Tha60291011HbceMemoryPoolInternal.h"
#include "../Tha60291011ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
/* Dword 1 */
#define cCLAHbceFlowDirectMask     cBit4 /* Bit 36 */
#define cCLAHbceFlowDirectShift    4
#define cCLAHbceGrpWorkingMask     cBit3 /* Bit 35 */
#define cCLAHbceGrpWorkingShift    3
#define cCLAHbceGrpIDFlowHeadMask  cBit2_0 /* Bit 34_32 */
#define cCLAHbceGrpIDFlowHeadShift 0

/* Dword 0 */
#define cCLAHbceGrpIDFlowTailMask  cBit31_25
#define cCLAHbceGrpIDFlowTailShift 25
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override  */
static tThaHbceMemoryPoolMethods         m_ThaHbceMemoryPoolOverride;
static tTha60210011HbceMemoryPoolMethods m_Tha60210011HbceMemoryPoolOverride;

/* Save super implementation */
static const tThaHbceMemoryPoolMethods         *m_ThaHbceMemoryPoolMethods         = NULL;
static const tTha60210011HbceMemoryPoolMethods *m_Tha60210011HbceMemoryPoolMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    AtUnused(self);
    return 8 * 2048;
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaHbceMemoryPool self)
    {
    ThaModuleCla module = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return Tha60291011ModuleClaHbceLookingUpInformationCtrlBase(module);
    }

static uint32 OffsetForCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    uint32 maxNumHashIdsPerColision = ThaHbceMaxNumEntries(self->hbce);
    uint32 colisionIndex   = cellIndex / maxNumHashIdsPerColision;
    uint32 hashIndexInOneColision = cellIndex % maxNumHashIdsPerColision;
    uint32 offset = colisionIndex * 0x800 + hashIndexInOneColision;

    return offset;
    }

static uint32 ClaHbceLookingUpInformationCtrlPerCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    uint32 offset = OffsetForCell(self, cellIndex);
    uint32 base = ClaHbceLookingUpInformationCtrl(self);
    uint32 address = base + offset;

    return address;
    }

static AtIpCore Core(ThaHbceMemoryPool self)
    {
    return ThaHbceCoreGet(self->hbce);
    }

static eAtRet StandardCellInfoSet(ThaHbceMemoryPool self, uint32 cellIndex, eBool enable, uint32 flowId, uint32 storeId)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, cellIndex);
    AtModule module = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 hwEnabled = enable ? 1 : 0;
    const uint8 cFlowNormalMode = 1;

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    mRegFieldSet(longReg[1], cCLAHbceFlowDirect, cFlowNormalMode);

    mRegFieldSet(longReg[0], cAf6_hbce_lkup_info_CLAHbceStoreID_, storeId);
    mRegFieldSet(longReg[0], cAf6_hbce_lkup_info_CLAHbceFlowEnb_, hwEnabled);
    mRegFieldSet(longReg[0], cAf6_hbce_lkup_info_CLAHbceFlowID_, flowId);

    mModuleHwLongWrite(module, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    return cAtOk;
    }

static eAtRet StandardCellInfoGet(ThaHbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, cellIndex);
    AtModule module = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    mAssign(storeId, mRegField(longReg[0], cAf6_hbce_lkup_info_CLAHbceStoreID_));
    mAssign(enable , (longReg[0] & cAf6_hbce_lkup_info_CLAHbceFlowEnb_Mask) ? cAtTrue : cAtFalse);
    mAssign(flowId , mRegField(longReg[0], cAf6_hbce_lkup_info_CLAHbceFlowID_));

    return cAtOk;
    }

static eAtRet CellInfoGet(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;

    return StandardCellInfoGet(pool, cellIndex, flowId, enable, storeId);
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);
    uint32 flowId, storeId;
    eAtRet ret = cAtOk;

    Tha60210011HbceMemoryPoolCellInfoGet(self, cellIndex, &flowId, NULL, &storeId);
    ret |= StandardCellInfoSet(self, cellIndex, enable, flowId, storeId);
    ret |= ThaHbceMemoryCellContentEnable(ThaHbceMemoryCellContentGet(cell), enable);

    return ret;
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    eBool enabled = cAtFalse;
    uint32 flowId = 0, storeId = cAf6_hbce_lkup_info_CLAHbceStoreID_Mask >> cAf6_hbce_lkup_info_CLAHbceStoreID_Shift;

    return StandardCellInfoSet(self, cellIndex, enabled, flowId, storeId);
    }

static void ApplyCell(Tha60210011HbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;
    ThaHbceMemoryCellContent cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex(pool, cellIndex));
    uint32 flowId = 0;
    ThaPwAdapter pwAdapter;
    ThaPwHeaderController headerController;
    uint32 storeId;
    eBool enabled = cAtFalse;

    if (cellContent == NULL)
        return;

    headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
    pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    if (pwAdapter)
        {
        enabled = AtChannelIsEnabled((AtChannel)pwAdapter) ? 1 : 0;
        flowId = AtChannelIdGet((AtChannel)pwAdapter);
        }

    storeId = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
    StandardCellInfoSet(pool, cellIndex, enabled, flowId, storeId);
    return;
    }

static eBool CellIsPrimary(ThaHbceMemoryPool self, uint32 absoluteCellIndex, uint32* groupId)
    {
    AtUnused(self);
    AtUnused(absoluteCellIndex);
    AtUnused(groupId);
    return cAtTrue;
    }

static void OverrideTha60210011HbceMemoryPool(ThaHbceMemoryPool self)
    {
    Tha60210011HbceMemoryPool pool = (Tha60210011HbceMemoryPool)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011HbceMemoryPoolMethods = mMethodsGet(pool);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceMemoryPoolOverride, m_Tha60210011HbceMemoryPoolMethods, sizeof(m_Tha60210011HbceMemoryPoolOverride));

        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ClaHbceLookingUpInformationCtrlPerCell);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, CellIsPrimary);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ApplyCell);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, CellInfoGet);
        }

    mMethodsSet(pool, &m_Tha60210011HbceMemoryPoolOverride);
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMemoryPoolMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, mMethodsGet(self), sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellInit);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, OffsetForCell);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    OverrideTha60210011HbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011HbceMemoryPool);
    }

static ThaHbceMemoryPool ObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60291011HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPool, hbce);
    }

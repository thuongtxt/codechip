/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60291011HbceMemoryPoolInternal.h
 * 
 * Created Date: Oct 9, 2018
 *
 * Description : 60291011 HBCE memory pool internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011HBCEMEMORYPOOLINTERNAL_H_
#define _THA60291011HBCEMEMORYPOOLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define mAssign(pointer, value)                                                \
    do                                                                         \
        {                                                                      \
        if (pointer)                                                           \
            *pointer = value;                                                  \
        }while(0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011HbceMemoryPool
    {
    tTha60210011HbceMemoryPool super;
    }tTha60291011HbceMemoryPool;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291011HBCEMEMORYPOOLINTERNAL_H_ */


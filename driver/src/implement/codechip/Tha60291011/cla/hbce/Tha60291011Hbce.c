/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60291011Hbce.c
 *
 * Created Date: Oct 9, 2018
 *
 * Description : HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cla/pw/ThaModuleClaPw.h"
#include "../../../../default/man/ThaDevice.h"
#include "Tha60291011HbceInternal.h"
#include "../Tha60291011ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#define cCLAHbceGrpWorking_Mask          cBit3
#define cCLAHbceGrpWorking_Shift         3
#define cThaClaHbceCodingSelectedModeDwIndex 0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60291011Hbce)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60291011Hbce *Tha60291011Hbce;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods       m_ThaHbceOverride;
static tTha60210011HbceMethods m_Tha60210011HbceOverride;

/* Save super implementation */
static const tThaHbceMethods       *m_ThaHbceMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClaPwController ClaPwController(ThaHbce self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return ThaModuleClaPwControllerGet(claModule);
    }

static uint32 HashIndexGetOnPart(ThaHbce self, uint8 partId, const uint32 hwLabel)
    {
    uint32 regVal;
    uint16 codeLevel;
    uint32 swHbcePatern21_11;
    uint32 swHbcePatern10_0;
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    AtDevice device = AtModuleDeviceGet((AtModule)claModule);
    uint32 partOffset = ThaDeviceModulePartOffset((ThaDevice)device, cThaModuleCla, partId);
    uint32 regAddr = ThaClaPwControllerCLAHBCEGlbCtrl(ThaModuleClaPwControllerGet(claModule)) + partOffset;
    ThaClaPwController pwController = ClaPwController(self);

    /* On the standby, reduce read operations as much as possible */
    if (AtModuleInAccessible((AtModule)claModule))
        ThaHbceStandbyGlbCtrlCached(self, &regVal, 1);

    /* On active, no limit on reading */
    else
        regVal = mModuleHwRead(claModule, regAddr);

    mFieldGet(regVal,
              ThaClaPwControllerClaHbceCodingSelectedModeMask(pwController),
              ThaClaPwControllerClaHbceCodingSelectedModeShift(pwController),
              uint16,
              &codeLevel);

    mFieldGet(hwLabel, cBit21_11, 11, uint32, &swHbcePatern21_11);
    mFieldGet(hwLabel, cBit10_0, 0, uint32, &swHbcePatern10_0);

    if (codeLevel == 0)
        return swHbcePatern10_0;
    if (codeLevel == 1)
        return swHbcePatern21_11 ^ swHbcePatern10_0;

    return 0x0;
    }

static uint32 HwHcbeLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType)
    {
    uint32 hwLabel = 0;
    uint8 ethPortId = (uint8)(ethPort ? (uint8)AtChannelHwIdGet((AtChannel)ethPort) : 0);
    AtUnused(self);

    mFieldIns(&hwLabel, cBit22_3, 3, label);
    mFieldIns(&hwLabel, cBit2_1, 1, ThaHbcePktType(psnType));
    mFieldIns(&hwLabel, cBit0, 0, ethPortId);

    return hwLabel;
    }

static eAtRet RemainBitsGet(const uint32 hwLabel, uint32 *remainBits)
    {
    uint32 lsb;

    mFieldGet(hwLabel, cBit22_11, 11, uint32, &lsb);
    remainBits[0] = lsb;

    return cAtOk;
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel   = HwHcbeLabel(self, ethPort, label, psnType);
    uint32 hashIndex = HashIndexGetOnPart(self, partId, hwLabel);

    if (remainBits)
        RemainBitsGet(hwLabel, remainBits);

    return hashIndex;
    }

static uint32 MaxNumEntries(ThaHbce self)
    {
    AtUnused(self);
    return 2048;
    }

static uint32 CellGroupWorkingMask(ThaHbce self)
    {
    AtUnused(self);
    return cCLAHbceGrpWorking_Mask;
    }

static uint32 CellGroupWorkingShift(ThaHbce self)
    {
    AtUnused(self);
    return cCLAHbceGrpWorking_Shift;
    }

static uint8 NumHashCollisionInOneHashIndex(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 8;
    }

static uint8  NumHashCollisionInOneExtraPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumExtraMemoryPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxNumHashCells(Tha60210011Hbce self)
    {
    return ThaHbceMaxNumEntries((ThaHbce)self) * Tha60210011HbceNumHashCollisionInOneHashIndex(self);
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60291011HbceMemoryPoolNew(self);
    }

static uint32 CLAHBCEHashingTabCtrl(ThaHbce self)
    {
    return ThaClaPwControllerCLAHBCEHashingTabCtrl(ClaPwController(self)) + ThaHbcePartOffset(self);
    }

static void Write(ThaHbce self, uint32 address, uint32 value)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    mModuleHwWriteByHal(claModule, address, value, AtIpCoreHalGet(self->core));
    }

static void EntryInit(ThaHbce self, uint32 entryIndex)
    {
    uint32 address = CLAHBCEHashingTabCtrl(self) + mMethodsGet(self)->EntryOffset(self, entryIndex, 0);
    Write(self, address, 0);
    }

static uint32 CLAHBCEGlbCtrl(ThaHbce self)
    {
    return ThaClaPwControllerCLAHBCEGlbCtrl(ClaPwController(self)) + ThaHbcePartOffset(self);
    }

static void MasterInit(ThaHbce self)
    {
    uint32 regVal = 0;
    ThaClaPwController pwController = ClaPwController(self);

    mFieldIns(&regVal,
              ThaClaPwControllerClaHbceCodingSelectedModeMask(pwController),
              ThaClaPwControllerClaHbceCodingSelectedModeShift(pwController),
              mMethodsGet(self)->CodeLevel(self));

    mFieldIns(&regVal,
              ThaClaPwControllerClaHbceTimeoutValMask(pwController),
              ThaClaPwControllerClaHbceTimeoutValShift(pwController),
              0x100);

    Write(self, CLAHBCEGlbCtrl(self), regVal);
    }

static void OverrideTha60210011Hbce(ThaHbce self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceOverride, mMethodsGet(hbce), sizeof(m_Tha60210011HbceOverride));

        mMethodOverride(m_Tha60210011HbceOverride, MaxNumHashCells);
        mMethodOverride(m_Tha60210011HbceOverride, NumHashCollisionInOneHashIndex);
        mMethodOverride(m_Tha60210011HbceOverride, NumHashCollisionInOneExtraPointer);
        mMethodOverride(m_Tha60210011HbceOverride, NumExtraMemoryPointer);
        }

    mMethodsSet(hbce, &m_Tha60210011HbceOverride);
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, m_ThaHbceMethods, sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, PsnHash);
        mMethodOverride(m_ThaHbceOverride, CellGroupWorkingMask);
        mMethodOverride(m_ThaHbceOverride, CellGroupWorkingShift);
        mMethodOverride(m_ThaHbceOverride, EntryInit);
        mMethodOverride(m_ThaHbceOverride, MasterInit);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    OverrideTha60210011Hbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Hbce);
    }

static ThaHbce ObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60291011HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHbce, hbceId, claModule, core);
    }

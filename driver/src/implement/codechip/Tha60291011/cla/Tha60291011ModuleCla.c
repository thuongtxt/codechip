/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60291011ModuleCla.c
 *
 * Created Date: Jul 15, 2018
 *
 * Description : Implementation for the 60291011 Module CLA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/ThaModuleClaReg.h"
#include "Tha60291011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegCasLenBit1_0_Mask  cBit31_30
#define cRegCasLenBit1_0_Shift 30
#define cRegCasLenBit7_2_Mask  cBit5_0
#define cRegCasLenBit7_2_Shift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods     m_ThaModuleClaOverride;

/* Save Super Implementation */
static const tThaModuleClaMethods *m_ThaModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CESoPCASLengthSet(ThaModuleCla self, AtPw pw, uint8 casLength)
    {
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    uint32 address = ThaModuleClaPwV2ClaPwTypeCtrlReg((ThaModuleClaPwV2)self) + pwId;
    uint32 longReg[cThaLongRegMaxSize];
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtIpCore core = AtDeviceIpCoreGet(device, 0);
    uint8 lenBit1_0 = casLength & cBit1_0;
    uint8 lenBit7_2 = (casLength >> 2);

    mModuleHwLongRead(self, address, longReg, cThaLongRegMaxSize, core);
    mRegFieldSet(longReg[1], cRegCasLenBit1_0_, lenBit1_0);
    mRegFieldSet(longReg[2], cRegCasLenBit7_2_, lenBit7_2);
    mModuleHwLongWrite(self, address, longReg, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, m_ThaModuleClaMethods, sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, CESoPCASLengthSet);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModuleCla);
    }

AtModule Tha60291011ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011ModuleClaObjectInit(newModule, device);
    }

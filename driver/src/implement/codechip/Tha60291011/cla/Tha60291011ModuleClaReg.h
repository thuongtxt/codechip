/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC1011_RD_CLA_H_
#define _AF6_REG_AF6CNC1011_RD_CLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Classify Global PSN Control
Reg Addr   : 0x00000000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register controls operation modes of PSN interface.

------------------------------------------------------------------------------*/
#define cAf6Reg_glb_psn_Base                                                                        0x00000000

/*--------------------------------------
BitField Name: SubPortVlanEn
BitField Type: RW
BitField Desc: Enable SubPort Vlan
BitField Bits: [31]
--------------------------------------*/
#define cAf6_glb_psn_SubPortVlanEn_Mask                                                                 cBit31
#define cAf6_glb_psn_SubPortVlanEn_Shift                                                                    31

/*--------------------------------------
BitField Name: RxPsnVlanTagModeEn
BitField Type: RW
BitField Desc: 1: Enable (default) VLAN TAG mode which contain PWID 0:  Disable
VLAN TAG mode, normal PSN operation
BitField Bits: [30]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnVlanTagModeEn_Mask                                                            cBit30
#define cAf6_glb_psn_RxPsnVlanTagModeEn_Shift                                                               30

/*--------------------------------------
BitField Name: RxSendLbit2CdrEn
BitField Type: RW
BitField Desc: Enable to send Lbit to CDR engine 1: Enable to send Lbit 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glb_psn_RxSendLbit2CdrEn_Mask                                                              cBit29
#define cAf6_glb_psn_RxSendLbit2CdrEn_Shift                                                                 29

/*--------------------------------------
BitField Name: RxPsnMplsOutLabelCheckEn
BitField Type: RW
BitField Desc: Enable to check MPLS outer 1: Enable checking 0: Disable checking
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnMplsOutLabelCheckEn_Mask                                                      cBit28
#define cAf6_glb_psn_RxPsnMplsOutLabelCheckEn_Shift                                                         28

/*--------------------------------------
BitField Name: RxPsnCpuBfdCtlEn
BitField Type: RW
BitField Desc: Enable VCCV BFD control packet sending to CPU for processing 1:
Enable sending 0: Discard
BitField Bits: [27]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnCpuBfdCtlEn_Mask                                                              cBit27
#define cAf6_glb_psn_RxPsnCpuBfdCtlEn_Shift                                                                 27

/*--------------------------------------
BitField Name: RxMacCheckDis
BitField Type: RW
BitField Desc: Disable to check MAC address at Ethernet port receive direction
1: Disable checking 0: Enable checking
BitField Bits: [26]
--------------------------------------*/
#define cAf6_glb_psn_RxMacCheckDis_Mask                                                                 cBit26
#define cAf6_glb_psn_RxMacCheckDis_Shift                                                                    26

/*--------------------------------------
BitField Name: RxPsnCpuIcmpEn
BitField Type: RW
BitField Desc: Enable ICMP control packet sending to CPU for processing 1:
Enable sending 0: Discard
BitField Bits: [25]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnCpuIcmpEn_Mask                                                                cBit25
#define cAf6_glb_psn_RxPsnCpuIcmpEn_Shift                                                                   25

/*--------------------------------------
BitField Name: RxPsnCpuArpEn
BitField Type: RW
BitField Desc: Enable ARP control packet sending to CPU for processing 1: Enable
sending 0: Discard
BitField Bits: [24]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnCpuArpEn_Mask                                                                 cBit24
#define cAf6_glb_psn_RxPsnCpuArpEn_Shift                                                                    24

/*--------------------------------------
BitField Name: PweLoopClaEn
BitField Type: RW
BitField Desc: Enable Loop back traffic from PW Encapsulation to Classification
1: Enable Loop back mode 0: Normal, not loop back
BitField Bits: [23]
--------------------------------------*/
#define cAf6_glb_psn_PweLoopClaEn_Mask                                                                  cBit23
#define cAf6_glb_psn_PweLoopClaEn_Shift                                                                     23

/*--------------------------------------
BitField Name: RxPsnIpUdpMode
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1:
Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or
Destination) is used to identify pseudowire packet 0: Classify engine will
automatically search for value 0x85E in source or destination UDP port. The
remaining UDP port is used to identify pseudowire packet
BitField Bits: [22]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnIpUdpMode_Mask                                                                cBit22
#define cAf6_glb_psn_RxPsnIpUdpMode_Shift                                                                   22

/*--------------------------------------
BitField Name: RxPsnIpUdpSel
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 using to select Source or
Destination to identify pseudowire packet from Ethernet side. It is not use when
RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify
pseudowire packet 0: Classify engine selects destination UDP port to identify
pseudowire packet
BitField Bits: [21]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnIpUdpSel_Mask                                                                 cBit21
#define cAf6_glb_psn_RxPsnIpUdpSel_Shift                                                                    21

/*--------------------------------------
BitField Name: RxPsnIpTtlChkEn
BitField Type: RW
BitField Desc: Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1:
Enable checking 0: Disable checking
BitField Bits: [20]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnIpTtlChkEn_Mask                                                               cBit20
#define cAf6_glb_psn_RxPsnIpTtlChkEn_Shift                                                                  20

/*--------------------------------------
BitField Name: RxPsnMplsOutLabel
BitField Type: RW
BitField Desc: Received 2-label MPLS packet from PSN side will be discarded when
it's outer label is different than RxPsnMplsOutLabel
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_glb_psn_RxPsnMplsOutLabel_Mask                                                           cBit19_0
#define cAf6_glb_psn_RxPsnMplsOutLabel_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Type Control
Reg Addr   : 0x0000C000-0x0000C53F #The address format for these registers is 0x0000C000 + PWID
Reg Formula: 0x0000C000 +  $PWID
    Where  : 
           + $PWID(0-1343): Pseudowire ID
Reg Desc   : 
This register configures identification types per pseudowire

------------------------------------------------------------------------------*/
#define cAf6Reg_per_pw_ctrl_Base                                                                    0x0000C000
#define cAf6Reg_per_pw_ctrl_WidthVal                                                                        96

/*--------------------------------------
BitField Name: RxCASbyte
BitField Type: RW
BitField Desc: number byte of signal in case of cesoPSN with CAS
BitField Bits: [70:62]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxCASbyte_01_Mask                                                           cBit31_30
#define cAf6_per_pw_ctrl_RxCASbyte_01_Shift                                                                 30
#define cAf6_per_pw_ctrl_RxCASbyte_02_Mask                                                             cBit6_0
#define cAf6_per_pw_ctrl_RxCASbyte_02_Shift                                                                  0

/*--------------------------------------
BitField Name: RxEthPwOnflyVC34
BitField Type: RW
BitField Desc: CEP EBM on-fly fractional, length changed
BitField Bits: [61]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthPwOnflyVC34_Mask                                                          cBit29
#define cAf6_per_pw_ctrl_RxEthPwOnflyVC34_Shift                                                             29

/*--------------------------------------
BitField Name: RxEthPwLen
BitField Type: RW
BitField Desc: length of packet to check malform
BitField Bits: [60:47]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthPwLen_Mask                                                             cBit28_15
#define cAf6_per_pw_ctrl_RxEthPwLen_Shift                                                                   15

/*--------------------------------------
BitField Name: RxEthCepMode
BitField Type: RW
BitField Desc: CEP mode working 0,1: CEP basic 2: VC3 fractional 3: VC4
fractional
BitField Bits: [46:45]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthCepMode_Mask                                                           cBit14_13
#define cAf6_per_pw_ctrl_RxEthCepMode_Shift                                                                 13

/*--------------------------------------
BitField Name: RxEthPwEn
BitField Type: RW
BitField Desc: PW enable working 1: Enable PW 0: Disable PW
BitField Bits: [44]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthPwEn_Mask                                                                 cBit12
#define cAf6_per_pw_ctrl_RxEthPwEn_Shift                                                                    12

/*--------------------------------------
BitField Name: RxEthRtpSsrcValue
BitField Type: RW
BitField Desc: This value is used to compare with SSRC value in RTP header of
received TDM PW packets
BitField Bits: [43:12]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthRtpSsrcValue_01_Mask                                                   cBit31_12
#define cAf6_per_pw_ctrl_RxEthRtpSsrcValue_01_Shift                                                         12
#define cAf6_per_pw_ctrl_RxEthRtpSsrcValue_02_Mask                                                    cBit11_0
#define cAf6_per_pw_ctrl_RxEthRtpSsrcValue_02_Shift                                                          0

/*--------------------------------------
BitField Name: RxEthRtpPtValue
BitField Type: RW
BitField Desc: This value is used to compare with PT value in RTP header of
received TDM PW packets
BitField Bits: [11:5]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthRtpPtValue_Mask                                                         cBit11_5
#define cAf6_per_pw_ctrl_RxEthRtpPtValue_Shift                                                               5

/*--------------------------------------
BitField Name: RxEthRtpEn
BitField Type: RW
BitField Desc: Enable RTP 1: Enable RTP 0: Disable RTP
BitField Bits: [4]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthRtpEn_Mask                                                                 cBit4
#define cAf6_per_pw_ctrl_RxEthRtpEn_Shift                                                                    4

/*--------------------------------------
BitField Name: RxEthRtpSsrcChkEn
BitField Type: RW
BitField Desc: Enable checking SSRC field of RTP header in received TDM PW
packet 1: Enable checking 0: Disable checking
BitField Bits: [3]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthRtpSsrcChkEn_Mask                                                          cBit3
#define cAf6_per_pw_ctrl_RxEthRtpSsrcChkEn_Shift                                                             3

/*--------------------------------------
BitField Name: RxEthRtpPtChkEn
BitField Type: RW
BitField Desc: Enable checking PT field of RTP header in received TDM PW packet
1: Enable checking 0: Disable checking
BitField Bits: [2]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthRtpPtChkEn_Mask                                                            cBit2
#define cAf6_per_pw_ctrl_RxEthRtpPtChkEn_Shift                                                               2

/*--------------------------------------
BitField Name: RxEthPwType
BitField Type: RW
BitField Desc: this is PW type working 0: CES mode without CAS 1: CES mode with
CAS 2: CEP mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_per_pw_ctrl_RxEthPwType_Mask                                                              cBit1_0
#define cAf6_per_pw_ctrl_RxEthPwType_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Global Control
Reg Addr   : 0x00008000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure global for HBCE module

------------------------------------------------------------------------------*/
#define cAf6Reg_hbce_glb_Base                                                                       0x00008000
#define cAf6Reg_hbce_glb_WidthVal                                                                           32

/*--------------------------------------
BitField Name: CLAHbceTimeoutValue
BitField Type: RW
BitField Desc: this is time out value for HBCE module, packing be discarded
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_hbce_glb_CLAHbceTimeoutValue_Mask                                                        cBit19_4
#define cAf6_hbce_glb_CLAHbceTimeoutValue_Shift                                                              4

/*--------------------------------------
BitField Name: CLAHbceHashingTableSelectedPage
BitField Type: RW
BitField Desc: this is to select hashing table page to access looking up
information buffer 1: PageID 1 0: PageID 0
BitField Bits: [3]
--------------------------------------*/
#define cAf6_hbce_glb_CLAHbceHashingTableSelectedPage_Mask                                               cBit3
#define cAf6_hbce_glb_CLAHbceHashingTableSelectedPage_Shift                                                  3

/*--------------------------------------
BitField Name: CLAHbceCodingSelectedMode
BitField Type: RW
BitField Desc: selects mode to code origin id 4/5/6/7: code level 4/5/6/7 3:
code level 3 (xor 4 subgroups origin id together) 2: code level 2 1: code level
1 0: none xor origin id
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_hbce_glb_CLAHbceCodingSelectedMode_Mask                                                   cBit2_0
#define cAf6_hbce_glb_CLAHbceCodingSelectedMode_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Hashing Table Control
Reg Addr   : 0x0000A000 - 0x0000B7FF
Reg Formula: 0x0000A000 + $HashID
    Where  : 
           + $HashID(0-2047): HashID
Reg Desc   : 
HBCE module uses 10 bits for tabindex. tabindex is generated by hasding function applied to the origin id. The collisions due to hashing are handled by pointer to variable size blocks. // // Handling variable  size blocks requires a dynamic memory management scheme. The number of entries in each variable size block is defined by the number of rules that collide within a specific entry of //the tabindex. Hashing function applies an XOR function to all bits of tabindex. The indexes to the tabindex are generated by hashing function and therefore collisions may occur. In order to resolve //these collisions efficiently, HBCE define a complex data structure associated with each entry of the orgin id. Hashing table has two fields, flow number (flownum) (number of labelid mapped to this //particular table entry) and memory start pointer (memsptr) (hold a pointer to the variable size block and the number of  rules that collide). In case, a table entry might be empty  which means that it //is not mapped to any flow address rule, flownum = 0. Moreover, a table entry may be mapped to many flow address rules. In this case, where collisions occur, hashing table have to store a pointer to  //the variable size block and the number of rules that collide. The number of colliding rules also indicates the size of the block. The formating of hashing table page0 in each case is shown as below.%%
Hash ID:%%
PATERN HASH %%
Label ID (20bit)  +  Lookup mode (2bit) + port (1bit)%%
With Lookup Mode: 0/1/2 ~ PSN MEF/UDP/MPLS%%
+ NOXOR       :  Hashid[10:0]  = Patern Hash [10:0], StoreID[11:0] = Patern Hash [22:11]%%
+ ONEXOR     :  Hashid[10:0]  = Patern Hash [10:0] XOR Patern Hash [21:11],%%
StoreID = Patern Hash [22]%%
+ TWOXOR    :   Hashid  = Patern Hash [10:0] XOR Patern Hash [21:11] XOR {7'b0,Patern Hash[22]}%%
StoreID = Patern Hash [22:11]

------------------------------------------------------------------------------*/
#define cAf6Reg_hbce_hash_table_Base                                                                0x0000A000
#define cAf6Reg_hbce_hash_table_WidthVal                                                                    32

/*--------------------------------------
BitField Name: CLAHbceFlowNumber
BitField Type: RW
BitField Desc: this is to identify HashID is empty or win or collision 0: empty
format 1: normal format others: collision format
BitField Bits: [16:13]
--------------------------------------*/
#define cAf6_hbce_hash_table_CLAHbceFlowNumber_Mask                                                  cBit16_13
#define cAf6_hbce_hash_table_CLAHbceFlowNumber_Shift                                                        13

/*--------------------------------------
BitField Name: CLAHbceMemoryStartPointer
BitField Type: RW
BitField Desc: this is a MemPtr to read the Classify HBCE Looking Up Information
Control
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_hbce_hash_table_CLAHbceMemoryStartPointer_Mask                                           cBit12_0
#define cAf6_hbce_hash_table_CLAHbceMemoryStartPointer_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control
Reg Addr   : 0x00004000 - 0x00007FFF #The address format for these registers is 0x0000E000 +  $CellID
Reg Formula: 0x00004000 + CollisHashID*0x800 + HashID
    Where  : 
           + $CollisHashID(0-7): Collision
           + $HashID(0-2047): HashID
Reg Desc   : 
This memory contain 8 entries (collisions) to examine one Pseudowire label whether match or not. In HBCE module, all operations access this memory in order to examine whether an exact match exists or not. The Memory Pool implements dynamic memory management scheme and supports //variable size blocks. It supports requests for allocation and deallocation of variable size blocks when inserting and deleting lookup address occur. Linking between multiple blocks is implemented by //writing the address of the  next  block  in  the  previous block. In general, HBCE implementation is based on sequential accesses to memory pool and to the dynamically allocated collision nodes. The //formating of memory pool word is shown as below.The format of memory pool word is shown as below. There are two case formats depend on field[23]%%
HDL_PATH: begin:
IF($CollisHashID==0)
ibank0.mem_memlk0.ram[$HashID]
ELSEIF($CollisHashID==1)
ibank0.mem_memlk1.ram[$HashID]
ELSEIF($CollisHashID==2)
ibank0.mem_memlk2.ram[$HashID]
ELSEIF($CollisHashID==3)
ibank0.mem_memlk3.ram[$HashID]
ELSEIF($CollisHashID==4)
ibank0.mem_memlk4.ram[$HashID]
ELSEIF($CollisHashID==5)
ibank0.mem_memlk5.ram[$HashID]
ELSEIF($CollisHashID==6)
ibank0.mem_memlk6.ram[$HashID]
ELSE
ibank0.mem_memlk7.ram[$HashID]
HDL_PATH: end:

------------------------------------------------------------------------------*/
#define cAf6Reg_hbce_lkup_info_Base                                                                 0x00004000
#define cAf6Reg_hbce_lkup_info_WidthVal                                                                     64

/*--------------------------------------
BitField Name: CLAHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [36]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceFlowDirect_Mask                                                       cBit4
#define cAf6_hbce_lkup_info_CLAHbceFlowDirect_Shift                                                          4

/*--------------------------------------
BitField Name: CLAHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [35]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceGrpWorking_Mask                                                       cBit3
#define cAf6_hbce_lkup_info_CLAHbceGrpWorking_Shift                                                          3

/*--------------------------------------
BitField Name: CLAHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [34:25]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceGrpIDFlow_01_Mask                                                 cBit31_25
#define cAf6_hbce_lkup_info_CLAHbceGrpIDFlow_01_Shift                                                       25
#define cAf6_hbce_lkup_info_CLAHbceGrpIDFlow_02_Mask                                                   cBit2_0
#define cAf6_hbce_lkup_info_CLAHbceGrpIDFlow_02_Shift                                                        0

/*--------------------------------------
BitField Name: CLAHbceMemoryStatus
BitField Type: RW
BitField Desc: This is zero for "normal format"
BitField Bits: [24]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceMemoryStatus_Mask                                                    cBit24
#define cAf6_hbce_lkup_info_CLAHbceMemoryStatus_Shift                                                       24

/*--------------------------------------
BitField Name: CLAHbceFlowID
BitField Type: RW
BitField Desc: a number identifying the output port of HBCE module  this
configure a group ID that FlowID following
BitField Bits: [23:13]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceFlowID_Mask                                                       cBit23_13
#define cAf6_hbce_lkup_info_CLAHbceFlowID_Shift                                                             13

/*--------------------------------------
BitField Name: CLAHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [12]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceFlowEnb_Mask                                                         cBit12
#define cAf6_hbce_lkup_info_CLAHbceFlowEnb_Shift                                                            12

/*--------------------------------------
BitField Name: CLAHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_hbce_lkup_info_CLAHbceStoreID_Mask                                                       cBit11_0
#define cAf6_hbce_lkup_info_CLAHbceStoreID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CDR Pseudowire Look Up Control
Reg Addr   : 0x00003000 - 0x0000353F
Reg Formula: 0x00003000 + PWID
    Where  : 
           + $PWID(0-1343): pwid
Reg Desc   : 
Used for TDM PW. This register is used to select PW for CDR function.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_pw_lkup_ctrl_Base                                                               0x00003000

/*--------------------------------------
BitField Name: PwCdrSlice
BitField Type: RW
BitField Desc: 0: Slice 0 corresponding to sts 0,2,4,...,46 1: Slice 1
corresponding to sts 1,3,5,...,47
BitField Bits: [11]
--------------------------------------*/
#define cAf6_cdr_pw_lkup_ctrl_PwCdrSlice_Mask                                                           cBit11
#define cAf6_cdr_pw_lkup_ctrl_PwCdrSlice_Shift                                                              11

/*--------------------------------------
BitField Name: PwCdrDis
BitField Type: RW
BitField Desc: applicable for pseudowire that is enabled CDR function (eg:
ACR/DCR) 0: Enable CDR (default) 1: Disable CDR
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cdr_pw_lkup_ctrl_PwCdrDis_Mask                                                             cBit10
#define cAf6_cdr_pw_lkup_ctrl_PwCdrDis_Shift                                                                10

/*--------------------------------------
BitField Name: PwCdrLineId
BitField Type: RW
BitField Desc: CDR line ID lookup from pseudowire Ids.
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_cdr_pw_lkup_ctrl_PwCdrLineId_Mask                                                         cBit9_0
#define cAf6_cdr_pw_lkup_ctrl_PwCdrLineId_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Classify Pseudowire MEF over MPLS Control
Reg Addr   : 0x0000D000 - 0x0000D53F
Reg Formula: 0x0000D000 + $PWID
    Where  : 
           + $PWID(0-1344): pwid
Reg Desc   : 
Used for TDM PW. This register is used to configure MEF over MPLS

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_mef_over_mpls_ctrl_Base                                                          0x0000D000
#define cAf6Reg_pw_mef_over_mpls_ctrl_WidthVal                                                              64

/*--------------------------------------
BitField Name: RxPwMEFoMPLSDaValue
BitField Type: RW
BitField Desc: DA value used to compare with DA in Ethernet MEF packet
BitField Bits: [50:3]
--------------------------------------*/
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSDaValue_01_Mask                                        cBit31_3
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSDaValue_01_Shift                                              3
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSDaValue_02_Mask                                        cBit18_0
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSDaValue_02_Shift                                              0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSDaCheckEn
BitField Type: RW
BitField Desc: 1: Enable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF
packet 0: Disable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSDaCheckEn_Mask                                            cBit2
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSDaCheckEn_Shift                                               2

/*--------------------------------------
BitField Name: RxPwMEFoMPLSEcidCheckEn
BitField Type: RW
BitField Desc: 1: Enable check  ECID in Ethernet MEF packet with MPLS PW label
0: Disable check  ECID in Ethernet MEF packet with MPLS PW label
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSEcidCheckEn_Mask                                          cBit1
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSEcidCheckEn_Shift                                             1

/*--------------------------------------
BitField Name: RxPwMEFoMPLSEn
BitField Type: RW
BitField Desc: 1: Enable MEF over MPLS PW mode, in this case, after MPLS PW
label is ETH MEF packet 0: Normal case, disable MEF over MPLS PW mode, in this
case, after MPLS PW label is PW control word
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSEn_Mask                                                   cBit0
#define cAf6_pw_mef_over_mpls_ctrl_RxPwMEFoMPLSEn_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Group Enable Control
Reg Addr   : 0x0000D000 - 0x0000D3FF
Reg Formula: 0x0000D000 + Working_ID*0x400 + Grp_ID
    Where  : 
           + $Working_ID(0-1): CLAHbceGrpWorking
           + $Grp_ID(0-1023): CLAHbceGrpIDFlow
Reg Desc   : 
This register configures Group that Flowid (or Pseudowire ID) is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_grp_enb_Base                                                                0x0000D000
#define cAf6Reg_cla_per_grp_enb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: CLAGrpPWEn
BitField Type: RW
BitField Desc: This indicate the FlowID (or Pseudowire ID) is enable or not 1:
FlowID enable 0: FlowID disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Mask                                                             cBit0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Shift                                                                0

#endif /* _AF6_REG_AF6CNC1011_RD_CLA_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60291011ModuleClaV2.c
 *
 * Created Date: Oct 8, 2018
 *
 * Description : Implementation for the module CLA to support the new HASH lookup
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ModuleClaInternal.h"
#include "Tha60291011ModuleClaReg.h"
#include "Tha60291011ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegClaHbceLookingUpInformationCtrl1      (0x440000 + cAf6Reg_hbce_lkup_info_Base)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods     m_ThaModuleClaOverride;

/* Save Super Implementation */
static const tThaModuleClaMethods *m_ThaModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HbceLookingUpInformationCtrlBase(ThaModuleCla self)
    {
    AtUnused(self);
    return cThaRegClaHbceLookingUpInformationCtrl1;
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60291011ClaPwControllerNew(self);
    }

static AtModulePw PwModule(ThaModuleCla self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static ThaPwAdapter HbcePwFromFlowId(ThaModuleCla self, uint32 flowId)
    {
    return ThaPwAdapterGet(AtModulePwGetPw(PwModule(self), flowId));
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, m_ThaModuleClaMethods, sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, HbcePwFromFlowId);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModuleClaV2);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291011ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModuleClaV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint32 Tha60291011ModuleClaHbceLookingUpInformationCtrlBase(ThaModuleCla self)
    {
    return HbceLookingUpInformationCtrlBase(self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60291011ModuleClaInternal.h
 * 
 * Created Date: Jul 15, 2018
 *
 * Description : 60291011 module CLA internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULECLAINTERNAL_H_
#define _THA60291011MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/cla/Tha60290011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011ModuleCla
    {
    tTha60290011ModuleCla super;

    }tTha60291011ModuleCla;

typedef struct tTha60291011ModuleClaV2
    {
    tTha60291011ModuleCla super;

    }tTha60291011ModuleClaV2;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60291011ModuleClaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULECLAINTERNAL_H_ */


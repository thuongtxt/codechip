/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60291011PdhNxDs0.c
 *
 * Created Date: Jul 19, 2018
 *
 * Description : Implementation for 60291011 NxDs0
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "Tha60291011PdhNxDs0Internal.h"
#include "Tha60291011ModulePdhReg.h"
#include "Tha60291011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cDefaultPdhModuleBase 0x700000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60291011PdhNxDs0)(self))

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60291011PdhNxDs0Methods    m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtPdhNxDs0Methods     m_AtPdhNxDs0Override;
static tThaPdhNxDs0Methods    m_ThaPdhNxDs0Override;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods  = NULL;
static const tAtChannelMethods   *m_AtChannelMethods = NULL;
static const tAtPdhNxDs0Methods  *m_AtPdhNxDs0Methods  = NULL;
static const tThaPdhNxDs0Methods *m_ThaPdhNxDs0Methods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwHwIdGet(AtPw pw)
    {
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    uint32 pwId = ThaModulePwPwAdapterHwId(modulePw, pw);
    return pwId;
    }

static uint32 TxFramerSignIdConvCtrl2Address(ThaPdhNxDs0 self, AtPw pw)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    int32 sliceOffset;
    int32 address;
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)self);
    ThaModulePdh modulePdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 pwId = PwHwIdGet(pw);

    ThaPdhDe1HwIdGet(de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    sliceOffset = ThaModulePdhSliceOffset(modulePdh, slice);
    address = (int32)cAf6Reg_dej1_tx_framer_sign_id_conv_ctrl_Base + (int32)cDefaultPdhModuleBase + (int32)pwId + sliceOffset;

    return (uint32)address;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask;
    }

static uint32 Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift(Tha60291011PdhNxDs0 self)
    {
    AtUnused(self);
    return cAf6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift;
    }

static uint32 TxFramerSignalingDs0IdConversionRegisterAddress(ThaPdhNxDs0 self, AtPw pw)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    int32 sliceOffset;
    int32 address;
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)self);
    ThaModulePdh modulePdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 pwId = PwHwIdGet(pw);

    ThaPdhDe1HwIdGet(de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    sliceOffset = ThaModulePdhSliceOffset(modulePdh, slice);
    address = (int32)cAf6Reg_dej1_tx_framer_sign_ds0id_conv_ctrl_Base + (int32)cDefaultPdhModuleBase + (int32)pwId + sliceOffset;

    return (uint32)address;
    }

static eAtRet TxFramerSignalingIDConversionSet(ThaPdhNxDs0 self, AtPw pw)
    {
    ThaPdhDe1 de1  = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)self);
    uint32 address;
    uint32 regVal, field_Mask, field_Shift;

    address = TxFramerSignIdConvCtrl2Address(self, pw);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    /* DS1/E1 indication */
    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift(mThis(self));
    mRegFieldSet(regVal, field_, AtPdhDe1IsE1((AtPdhDe1)de1) ? 0 : 1);
    /* DE1 ID */
    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift(mThis(self));
    mRegFieldSet(regVal, field_, ThaPdhDe1FlatId(de1));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    /* Signaling mask */
    address = TxFramerSignalingDs0IdConversionRegisterAddress(self, pw);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_ds0id_conv_ctrl_TxDE1SigEnb_, AtPdhNxDS0BitmapGet((AtPdhNxDS0)self));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet TxFramerSignalingIDConversionUnSet(ThaPdhNxDs0 self, AtPw pw)
    {
    uint32 address;
    uint32 regVal;

    /* Clear Signaling mask */
    address = TxFramerSignalingDs0IdConversionRegisterAddress(self, pw);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_ds0id_conv_ctrl_TxDE1SigEnb_, 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet PwCasIdleCode1Set(ThaPdhNxDs0 self, AtPw pw, uint8 abcd1)
    {
    uint32 address = TxFramerSignIdConvCtrl2Address(self, pw);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 field_Mask, field_Shift;

    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift(mThis(self));
    mRegFieldSet(regVal, field_, abcd1);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static uint8 PwCasIdleCode1Get(ThaPdhNxDs0 self, AtPw pw)
    {
    uint32 address = TxFramerSignIdConvCtrl2Address(self, pw);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 abcd1, field_Mask, field_Shift;

    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift(mThis(self));
    abcd1 = mRegField(regVal, field_);
    return (uint8)abcd1;
    }

static eAtRet PwCasIdleCode2Set(ThaPdhNxDs0 self, AtPw pw, uint8 abcd2)
    {
    uint32 address = TxFramerSignIdConvCtrl2Address(self, pw);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 field_Mask, field_Shift;

    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift(mThis(self));
    mRegFieldSet(regVal, field_, abcd2);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static uint8 PwCasIdleCode2Get(ThaPdhNxDs0 self, AtPw pw)
    {
    uint32 address = TxFramerSignIdConvCtrl2Address(self, pw);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 abcd2, field_Mask, field_Shift;

    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift(mThis(self));

    abcd2 = mRegField(regVal, field_);
    return (uint8)abcd2;
    }

static eAtRet PwCasAutoIdleEnable(ThaPdhNxDs0 self, AtPw pw, eBool enable)
    {
    uint32 address = TxFramerSignIdConvCtrl2Address(self, pw);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 field_Mask, field_Shift;

    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift(mThis(self));

    mRegFieldSet(regVal, field_, enable ? 1:0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static eBool PwCasAutoIdleIsEnabled(ThaPdhNxDs0 self, AtPw pw)
    {
    uint32 address = TxFramerSignIdConvCtrl2Address(self, pw);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 enable, field_Mask, field_Shift;

    field_Mask = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask(mThis(self));
    field_Shift = mMethodsGet(mThis(self))->Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift(mThis(self));

    enable = mRegField(regVal, field_);
    return (eBool)(enable ? cAtTrue : cAtFalse);
    }

static eAtRet CasIdleCodeSet(AtPdhNxDS0 self, uint8 abcd1)
    {
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = NULL;
    uint32 bitmap = 0;
    uint32 ts;
    uint32 maxTs = 0;
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);

    if (pw)
        {
        mThis(self)->casIdleCode = abcd1;
        return cAtOk;
        }

    de1 = (ThaPdhDe1)AtPdhNxDS0De1Get(self);
    bitmap = AtPdhNxDS0BitmapGet(self);
    maxTs = AtPdhDe1IsE1((AtPdhDe1)de1) ? 32 : 24;
    for (ts = 0; ts < maxTs; ts++)
        {
        uint32 mask = cBit0 << ts;
        if (mask & bitmap)
            ret |= ThaPdhDe1HwCasIdleCodeSet(de1, ts, abcd1);
        }
    if (ret == cAtOk)
        mThis(self)->casIdleCode = abcd1;

    return ret;
    }

static uint8 CasIdleCodeGet(AtPdhNxDS0 self)
    {
    ThaPdhDe1 de1 = NULL;
    uint32 bitmap = 0;
    uint32 ts;
    uint32 maxTs = 0;
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);

    if (pw)
        return mThis(self)->casIdleCode;
    else
        {
        de1 = (ThaPdhDe1)AtPdhNxDS0De1Get(self);
        bitmap = AtPdhNxDS0BitmapGet(self);
        maxTs = AtPdhDe1IsE1((AtPdhDe1)de1) ? 32 : 24;
        for (ts = 0; ts < maxTs; ts++)
            {
            uint32 mask = cBit0 << ts;
            if (mask & bitmap)
                return ThaPdhDe1HwCasIdleCodeGet(de1, ts);
            }
        }

    return 0;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;

    /*Clear signaling mask when unbind to avoid impact to other CAS of cesop pws */
    if (!pseudowire)
        {
        AtPw pw = AtChannelBoundPwGet(self);
        if (pw)
            ret |= TxFramerSignalingIDConversionUnSet((ThaPdhNxDs0)self, (AtPw)ThaPwAdapterGet(pw));
        }

    ret |= m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    if (!pseudowire) /* RTL required to set the idle back to HW due to its share memory between PW CAS idle and DS0 CAS IDLE */
        ret = AtPdhNxDs0CasIdleCodeSet((AtPdhNxDS0)self, mThis(self)->casIdleCode);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60291011PdhNxDs0 object = (Tha60291011PdhNxDs0)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(casIdleCode);
    }

static void MethodsInit(AtPdhNxDS0 self)
    {
    Tha60291011PdhNxDs0 object = (Tha60291011PdhNxDs0)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask);
        mMethodOverride(m_methods, Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift);
        }

    mMethodsSet(object, &m_methods);
    }

static void OverrideAtObject(AtPdhNxDS0 self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhNxDS0 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhNxDs0(AtPdhNxDS0 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhNxDs0Methods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhNxDs0Override, m_AtPdhNxDs0Methods, sizeof(m_AtPdhNxDs0Override));

        mMethodOverride(m_AtPdhNxDs0Override, CasIdleCodeSet);
        mMethodOverride(m_AtPdhNxDs0Override, CasIdleCodeGet);
        }

    mMethodsSet(self, &m_AtPdhNxDs0Override);
    }

static void OverrideThaPdhNxDs0(AtPdhNxDS0 self)
    {
    ThaPdhNxDs0 thaNxDs0 = (ThaPdhNxDs0)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhNxDs0Methods = mMethodsGet(thaNxDs0);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhNxDs0Override, m_ThaPdhNxDs0Methods, sizeof(m_ThaPdhNxDs0Override));


        mMethodOverride(m_ThaPdhNxDs0Override, TxFramerSignalingIDConversionSet);
        mMethodOverride(m_ThaPdhNxDs0Override, PwCasIdleCode1Set);
        mMethodOverride(m_ThaPdhNxDs0Override, PwCasIdleCode1Get);
        mMethodOverride(m_ThaPdhNxDs0Override, PwCasIdleCode2Set);
        mMethodOverride(m_ThaPdhNxDs0Override, PwCasIdleCode2Get);
        mMethodOverride(m_ThaPdhNxDs0Override, PwCasAutoIdleEnable);
        mMethodOverride(m_ThaPdhNxDs0Override, PwCasAutoIdleIsEnabled);
        }

    mMethodsSet(thaNxDs0, &m_ThaPdhNxDs0Override);
    }

static void Override(AtPdhNxDS0 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhNxDs0(self);
    OverrideThaPdhNxDs0(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011PdhNxDs0);
    }

AtPdhNxDS0 Tha60291011PdhNxDs0ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (ThaPdhNxDs0ObjectInit(self, de1, mask) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhNxDS0 Tha60291011PdhNxDs0New(AtPdhDe1 de1, uint32 mask)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhNxDS0 newDs0 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDs0 == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011PdhNxDs0ObjectInit(newDs0, de1, mask);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60291011ModulePdh.c
 *
 * Created Date: Jul 15, 2018
 *
 * Description : 60291011 Module PDH implementation with CAS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60291011ModulePdhInternal.h"
#include "Tha60291011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhMethods         m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtModulePdhMethods *m_AtModulePdhMethods = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CasSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 TimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint8 slice, hwIdInSlice, vtg, vt, maxTs;
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)de1);
    uint32 partOffset = ThaDeviceModulePartOffset(device, cAtModulePdh, ThaPdhDe1PartId(de1));
    int32 sliceOffset;
    ThaPdhDe1HwIdGet(de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    sliceOffset = ThaModulePdhSliceOffset(self, slice);
    maxTs = AtPdhDe1IsE1((AtPdhDe1)de1) ? 32 : 24;

    return (uint32)((672 * hwIdInSlice) + (96 * vtg) + (maxTs * vt) + (int32)partOffset + sliceOffset);
    }

static AtPdhNxDS0 NxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
    AtUnused(self);
    return Tha60291011PdhNxDs0New(de1, timeslotBitmap);
    }

static eBool  De1RxSlipBufferIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TxRetimingSignalingIsSupported(ThaModulePdh self)
    {
    return ThaDeviceSupportFeatureFromVersion(AtModuleDeviceGet((AtModule)self), 0, 3, 0x11);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, CasSupported);
        mMethodOverride(m_ThaModulePdhOverride, TimeslotDefaultOffset);
        mMethodOverride(m_ThaModulePdhOverride, De1RxSlipBufferIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, TxRetimingSignalingIsSupported);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, NxDs0Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModulePdh);
    }

AtModulePdh Tha60291011ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60291011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011ModulePdhObjectInit(newModule, device);
    }

uint32 Tha60291011ModulePdhTimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    return TimeslotDefaultOffset(self, de1);
    }

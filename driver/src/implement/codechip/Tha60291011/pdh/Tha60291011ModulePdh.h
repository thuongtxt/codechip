/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60291011ModulePdh.h
 * 
 * Created Date: Jul 25, 2018
 *
 * Description : 60291011 module PDH interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEPDH_H_
#define _THA60291011MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePdh.h"
#include "../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhNxDS0 Tha60291011PdhNxDs0New(AtPdhDe1 de1, uint32 mask);
uint32 Tha60291011ModulePdhTimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULEPDH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60291011PdhNxDs0Internal.h
 * 
 * Created Date: Jul 19, 2018
 *
 * Description : 60291011 NxDs0 internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011PDHNXDS0INTERNAL_H_
#define _THA60291011PDHNXDS0INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011PdhNxDs0 *Tha60291011PdhNxDs0;

typedef struct tTha60291011PdhNxDs0Methods
    {
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Mask)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSEnb_Shift)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Mask)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern2_Shift)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Mask)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxSigOOSPattern1_Shift)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Mask)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigDS1_Shift)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Mask)(Tha60291011PdhNxDs0 self);
    uint32 (*Af6_dej1_tx_framer_sign_id_conv_ctrl_TxDE1SigLineID_Shift)(Tha60291011PdhNxDs0 self);
    }tTha60291011PdhNxDs0Methods;

typedef struct tTha60291011PdhNxDs0
    {
    tThaPdhNxDs0 super;
    const tTha60291011PdhNxDs0Methods *methods;
    uint8 casIdleCode;
    }tTha60291011PdhNxDs0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhNxDS0 Tha60291011PdhNxDs0ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011PDHNXDS0INTERNAL_H_ */


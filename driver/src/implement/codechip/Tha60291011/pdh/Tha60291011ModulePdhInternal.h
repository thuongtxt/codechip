/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60291011ModulePdhInternal.h
 * 
 * Created Date: Jul 15, 2018
 *
 * Description : 60291011 Module PDH with CAS internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEPDHINTERNAL_H_
#define _THA60291011MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011ModulePdh
    {
    tTha60290011ModulePdh super;

    }tTha60291011ModulePdh;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60291011ModulePdhObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULEPDHINTERNAL_H_ */


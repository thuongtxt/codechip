/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60291011ModuleSur.c
 *
 * Created Date: Aug 1, 2018
 *
 * Description : 60291011 SUR implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290011ModuleSurMethods m_Tha60290011ModuleSurOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PwNumSlice(Tha60290011ModuleSur self)
    {
    AtUnused(self);
    return 2;
    }

static void OverrideTha60290011ModuleSur(AtModuleSur self)
    {
    Tha60290011ModuleSur module = (Tha60290011ModuleSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModuleSurOverride, mMethodsGet(module), sizeof(m_Tha60290011ModuleSurOverride));

        mMethodOverride(m_Tha60290011ModuleSurOverride, PwNumSlice);
        }

    mMethodsSet(module, &m_Tha60290011ModuleSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideTha60290011ModuleSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModuleSur);
    }

AtModuleSur Tha60291011ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60291011ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011ModuleSurObjectInit(newModule, device);
    }

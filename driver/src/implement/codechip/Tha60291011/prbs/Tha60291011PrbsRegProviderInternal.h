/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60291011PrbsRegProviderInternal.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : 60291011 PRBS reg provider internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011PRBSREGPROVIDERINTERNAL_H_
#define _THA60291011PRBSREGPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/prbs/Tha60290011PrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011PrbsRegProvider
    {
    tTha60290011PrbsRegProvider super;
    }tTha60291011PrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291011PRBSREGPROVIDERINTERNAL_H_ */


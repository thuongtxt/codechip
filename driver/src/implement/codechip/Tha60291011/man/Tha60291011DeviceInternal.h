/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60291011DeviceInternal.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : Internal data of the 60291011 device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011DEVICEINTERNAL_H_
#define _THA60291011DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/man/Tha60290011DeviceInternal.h"
#include "Tha60291011Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011Device
    {
    tTha60290011Device super;

    } tTha60291011Device;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60291011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011DEVICEINTERNAL_H_ */


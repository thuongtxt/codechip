/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60291011Device.c
 *
 * Created Date: Jul 10, 2018
 *
 * Description : Implementation for 60291011 device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011DeviceInternal.h"
#include "../pmc/Tha60291011ModulePmc.h"
#include "../cla/Tha60291011ModuleCla.h"
#include "../ram/Tha60291011ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)         ((Tha60291011Device)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods                   m_AtDeviceOverride;
static tThaDeviceMethods                   m_ThaDeviceOverride;
static tTha60290011DeviceMethods          m_Tha60290011DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods             *m_AtDeviceMethods          = NULL;
static const tThaDeviceMethods             *m_ThaDeviceMethods          = NULL;
static const tTha60290011DeviceMethods    *m_Tha60290011DeviceMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldOpenFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static AtModule ModuleClaNew(AtDevice self)
    {
    if (Tha60291011Device8ColissionInLookupTableIsSupported(self))
        return Tha60291011ModuleClaV2New(self);

    return Tha60291011ModuleClaNew(self);
    }

static AtModule ModuleRamNew(AtDevice self)
    {
    if (Tha60291011Device8ColissionInLookupTableIsSupported(self))
        return (AtModule)Tha60291011ModuleRamNew(self);

    return (AtModule)m_AtDeviceMethods->ModuleCreate(self, cAtModuleRam);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModulePrbs)        return (AtModule)Tha60291011ModulePrbsNew(self);
    if (moduleId  == cAtModulePw)          return (AtModule)Tha60291011ModulePwNew(self);
    if (moduleId  == cAtModulePdh)         return (AtModule)Tha60291011ModulePdhNew(self);
    if (moduleId  == cAtModuleSur)         return (AtModule)Tha60291011ModuleSurNew(self);
    if (moduleId  == cAtModuleRam)         return ModuleRamNew(self);

    /* Internal module */
    if (phyModule == cThaModulePmc)     return Tha60291011ModulePmcNew(self);
    if (phyModule == cThaModulePda)     return Tha60291011ModulePdaNew(self);
    if (phyModule == cThaModuleCla)     return ModuleClaNew(self);
    if (phyModule == cThaModuleMap)     return Tha60291011ModuleMapNew(self);
    if (phyModule == cThaModuleDemap)   return Tha60291011ModuleDemapNew(self);
    if (phyModule == cThaModulePwe)     return Tha60291011ModulePweNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PohEnableToHwValue(AtDevice self, eBool enable)
    {
    AtUnused(self);
    return enable ? 1 : 0;
    }

static uint32 DefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return cThaModulePmc;
    }

static eBool DccTxFcsMsbIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccHdlcFrameTransmissioniBitOderPerChannelIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool JnDdrInitIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TohSonetSdhModeIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TwoDot5GitSerdesPrbsIsRemoved(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasStandardClearTime(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwEthPortHasMacFunctionality(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RxFsmPinSelV2IsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthInterruptIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthSohTransparentIsReady(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool OcnSohOverEthIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool OcnVcNewLoopbackIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool OcnTohByteIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PdaCanControlLopsPktReplaceMode(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportDe1Ssm(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool RxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool De1RetimingIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionHasPrmRxEnabledCfgRegister(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool IsMroSerdesBackplaneImplemented(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool JnRequestDdrAndPohCpuDisableIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PwDccKbyteInterruptIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SurFailureForwardingStatusIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool OcnTxDccRsMsInsControlIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool BackplaneSerdesEthPortFecIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool BackplaneSerdesEthPortInterruptIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60290011Device(AtDevice self)
    {
    Tha60290011Device object = (Tha60290011Device)(self);
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011DeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011DeviceOverride, m_Tha60290011DeviceMethods, sizeof(m_Tha60290011DeviceOverride));

        mMethodOverride(m_Tha60290011DeviceOverride, PohEnableToHwValue);
        mMethodOverride(m_Tha60290011DeviceOverride, DccTxFcsMsbIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, DccHdlcFrameTransmissioniBitOderPerChannelIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, JnDdrInitIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, TohSonetSdhModeIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, TwoDot5GitSerdesPrbsIsRemoved);
        mMethodOverride(m_Tha60290011DeviceOverride, HasStandardClearTime);
        mMethodOverride(m_Tha60290011DeviceOverride, PwEthPortHasMacFunctionality);
        mMethodOverride(m_Tha60290011DeviceOverride, RxFsmPinSelV2IsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, EthInterruptIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, EthSohTransparentIsReady);
        mMethodOverride(m_Tha60290011DeviceOverride, OcnSohOverEthIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, OcnVcNewLoopbackIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, OcnTohByteIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, PdaCanControlLopsPktReplaceMode);
        mMethodOverride(m_Tha60290011DeviceOverride, StartVersionSupportDe1Ssm);
        mMethodOverride(m_Tha60290011DeviceOverride, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_Tha60290011DeviceOverride, StartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_Tha60290011DeviceOverride, De1RetimingIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, StartVersionHasPrmRxEnabledCfgRegister);
        mMethodOverride(m_Tha60290011DeviceOverride, IsMroSerdesBackplaneImplemented);
        mMethodOverride(m_Tha60290011DeviceOverride, JnRequestDdrAndPohCpuDisableIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, StartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_Tha60290011DeviceOverride, PwDccKbyteInterruptIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, SurFailureForwardingStatusIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, SurFailureHoldOnTimerConfigurable);
        mMethodOverride(m_Tha60290011DeviceOverride, OcnTxDccRsMsInsControlIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, DccEthMaxLengthConfigurationIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, BackplaneSerdesEthPortFecIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, BackplaneSerdesEthPortInterruptIsSupported);
        }

    mMethodsSet(object, &m_Tha60290011DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, DefaultCounterModule);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60290011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011Device);
    }

AtDevice Tha60291011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60291011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011DeviceObjectInit(newDevice, driver, productCode);
    }

eBool Tha60291011Device8ColissionInLookupTableIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0, 2, 00);
    }

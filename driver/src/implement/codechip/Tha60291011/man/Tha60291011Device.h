/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60291011Device.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : Interface of the 60291011 device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011DEVICE_H_
#define _THA60291011DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011Device *Tha60291011Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60291011Device8ColissionInLookupTableIsSupported(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011DEVICE_H_ */


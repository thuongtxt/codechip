/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60291011ModuleDemap.c
 *
 * Created Date: Jul 16, 2018
 *
 * Description : Implement 60291011 module DEMAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ModuleMapDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaDmapFirstTsMask     cBit15
#define cThaDmapFirstTsShift    15
#define cThaDmapChnTypeMask     cBit14_12
#define cThaDmapChnTypeShift    12
#define cThaDmapTsEnMask        cBit11
#define cThaDmapTsEnShift       11
#define cThaDmapPwIdMask        cBit10_0
#define cThaDmapPwIdShift       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleDemapMethods m_ThaModuleDemapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap mapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapChnType)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapFirstTs)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapTsEn)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapPwId)
        }

    mMethodsSet(mapModule, &m_ThaModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModuleDemap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

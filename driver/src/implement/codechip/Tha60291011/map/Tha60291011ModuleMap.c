/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60291011ModuleMap.c
 *
 * Created Date: Jul 16, 2018
 *
 * Description : Implement 60291011 module MAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ModuleMapDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaMapFirstTsMask     cBit15
#define cThaMapFirstTsShift    15

#define cThaMapChnTypeMask     cBit14_12
#define cThaMapChnTypeShift    12

#define cThaMapTsEnMask        cBit11
#define cThaMapTsEnShift       11

#define cThaMapPWIdFieldMask   cBit10_0
#define cThaMapPWIdFieldShift  0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleMapMethods         m_ThaModuleMapOverride;

/* Save Super Implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)

static void OverrideThaModuleMap(ThaModuleMap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(self), sizeof(m_ThaModuleMapOverride));

        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapPWIdField)
        }

    mMethodsSet(self, &m_ThaModuleMapOverride);
    }

static void Override(ThaModuleMap self)
    {
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaModuleMap)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

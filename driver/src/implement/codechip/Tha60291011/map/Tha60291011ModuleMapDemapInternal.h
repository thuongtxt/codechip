/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60291011ModuleMapDemapInternal.h
 * 
 * Created Date: Jul 16, 2018
 *
 * Description : Internal data of the module MAP and module DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEMAPDEMAPINTERNAL_H_
#define _THA60291011MODULEMAPDEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/map/Tha60290011ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011ModuleMap
    {
    tTha60290011ModuleMap super;
    }tTha60291011ModuleMap;

typedef struct tTha60291011ModuleDemap
    {
    tTha60290011ModuleDemap super;
    }tTha60291011ModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULEMAPDEMAPINTERNAL_H_ */


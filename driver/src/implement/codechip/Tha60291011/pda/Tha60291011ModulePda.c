/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60291011ModulePda.c
 *
 * Created Date: Jul 10, 2018
 *
 * Description : PDA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods         m_ThaModulePdaOverride;

/* Save super implementation */
static const tThaModulePdaMethods  *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 CESoPCasModeSw2Hw(eAtPwCESoPCasMode mode)
    {
    static const uint8 cBasicWithDs1CasMode = 3;
    static const uint8 cBasicWithE1CasMode = 4;

    if (mode == cAtPwCESoPCasModeE1)
        return cBasicWithE1CasMode;

    return cBasicWithDs1CasMode;
    }

static uint8 CESoPCasMode(ThaPwAdapter adapter)
    {
    AtPw pw = ThaPwAdapterPwGet(adapter);
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPwBoundCircuitGet(pw);
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxds0);

    if (AtPdhDe1IsE1(de1))
        return CESoPCasModeSw2Hw(cAtPwCESoPCasModeE1);

    return CESoPCasModeSw2Hw(cAtPwCESoPCasModeDs1);
    }

static uint8 CESoPMode(ThaPwAdapter adapter)
    {
    static const uint8 cBasicMode        = 2;
    AtPwCESoP pw = (AtPwCESoP)ThaPwAdapterPwGet(adapter);
    uint8 cesopType = AtPwCESoPModeGet(pw);

    switch (cesopType)
        {
        case cAtPwCESoPModeBasic  : return cBasicMode;
        case cAtPwCESoPModeWithCas: return CESoPCasMode(adapter);
        default                   : return cBasicMode;
        }
    }

static eAtRet CESoPModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, CESoPMode(adapter));
    }

static eAtRet CESoPCasModeSet(ThaModulePda self, ThaPwAdapter adapter, eAtPwCESoPCasMode mode)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, CESoPCasModeSw2Hw(mode));
    }

static eAtPwCESoPCasMode CESoPCasModeGet(ThaModulePda self, ThaPwAdapter adapter)
    {
    static const uint8 cBasicWithDs1CasMode = 3;
    static const uint8 cBasicWithE1CasMode = 4;
    uint8 mode = ThaModulePdaPwPdaModeGet(self, (AtPw)adapter);

    if (mode == cBasicWithDs1CasMode)   return cAtPwCESoPCasModeDs1;
    if (mode == cBasicWithE1CasMode)    return cAtPwCESoPCasModeE1;

    return cAtPwCESoPCasModeUnknown;
    }

static eBool CanControlLopsPktReplaceMode(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_ThaModulePdaOverride, CESoPModeSet);
        mMethodOverride(m_ThaModulePdaOverride, CESoPCasModeSet);
        mMethodOverride(m_ThaModulePdaOverride, CESoPCasModeGet);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void Override(AtModule self)
    {
	OverrideThaModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModulePda);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

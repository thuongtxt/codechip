/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60291011ModulePwe.c
 *
 * Created Date: Jul 18, 2018
 *
 * Description : 60291011 module PWE implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"
#include "Tha60291011ModulePweInternal.h"
#include "Tha60291011ModulePwePlaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPweBaseAddress(self) ThaModulePweBaseAddress((ThaModulePwe)self)
#define mPlaBaseAddress(self) ThaModulePwePlaBaseAddress((ThaModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods         m_ThaModulePweOverride;

/* Save super implementation */
static const tThaModulePweMethods *m_ThaModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PayloadSizeControlAddress(ThaModulePwe self, ThaPwAdapter adapter)
    {
    uint32 address = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, (AtPw)adapter);
    return address;
    }

static eAtRet PwIdleCodeSet(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 payloadIdleCode)
    {
    uint32 address = PayloadSizeControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_pyld_size_ctrl_DatIdlePattern_, payloadIdleCode);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static eAtRet PwCesopCasIdleCode1Set(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 abcd1)
    {
    uint32 address = PayloadSizeControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_pyld_size_ctrl_CasIdlePattern_, abcd1);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static uint8 PwCesopCasIdleCode1Get(ThaModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 address = PayloadSizeControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);
    uint32 abcd1 = mRegField(regVal, cAf6_pyld_size_ctrl_CasIdlePattern_);

    return (uint8)abcd1;
    }

static eAtRet PwIdleCodeInsertionEnable(ThaModulePwe self, ThaPwAdapter pwAdapter, eBool autoIdleEnable)
    {
    uint32 address = PayloadSizeControlAddress(self, pwAdapter);
    uint32 regVal = mModuleHwRead(self, address);
    uint32 hwVal = (uint32)(autoIdleEnable ? 1 : 0);

    mRegFieldSet(regVal, cAf6_pyld_size_ctrl_CasIdlePatEn_, hwVal);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwIdleCodeSet);
        mMethodOverride(m_ThaModulePweOverride, PwCesopCasIdleCode1Set);
        mMethodOverride(m_ThaModulePweOverride, PwCesopCasIdleCode1Get);
        mMethodOverride(m_ThaModulePweOverride, PwIdleCodeInsertionEnable);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModulePwe);
    }

AtModule Tha60291011ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011ModulePweObjectInit(newModule, device);
    }

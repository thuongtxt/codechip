/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC1011_RD_PLA_H_
#define _AF6_REG_AF6CNC1011_RD_PLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble Signaling Convert 1 to 4 Status
Reg Addr   : 0x00008000-0x0000AF77
Reg Formula: 0x00008000  + 1024*$stsid + 128*$vtg + 32*$vtn + $ds0id
    Where  : 
           + $stsid(0-3)
           + $vtg(0-6)
           + $vtn(0-3)
           + $ds0id(0-31)
Reg Desc   : 
These registers are used to convert

------------------------------------------------------------------------------*/
#define cAf6Reg_sig_conv_sta_Base                                                                   0x00008000

/*--------------------------------------
BitField Name: PDHPWPlaAsmSigConv1to4Sta
BitField Type: RW
BitField Desc: convert 1 to 4 signaling bits of timeslots of DS1 mode
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_sig_conv_sta_PDHPWPlaAsmSigConv1to4Sta_Mask                                               cBit2_0
#define cAf6_sig_conv_sta_PDHPWPlaAsmSigConv1to4Sta_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Adding Protocol To Prevent Burst
Reg Addr   : 0x00000003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is to add a pseudo-wire that does not cause bottle-neck at transmit PW cause ACR performance bad at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_add_pro_2_prevent_burst_Base                                                        0x00000003

/*--------------------------------------
BitField Name: AddPwId
BitField Type: RW
BitField Desc: PWID to be added
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_add_pro_2_prevent_burst_AddPwId_Mask                                                     cBit13_4
#define cAf6_add_pro_2_prevent_burst_AddPwId_Shift                                                           4

/*--------------------------------------
BitField Name: AddPwState
BitField Type: RW
BitField Desc: State machine to add a PDH PW Step1: Write pseudo-wire ID to be
added to AddPwId and set AddPwState to 0x1 to prepare add PW Step2: Do all
configuration to add a pseudo-wire such as MAP,DEMAP,PLA,PDA,CLA,PSN.... Step3:
Write AddPwState value 0x2 to start add PW protocol Step4: Poll AddPwState until
return value of AddPwState equal to 0x0 the finish
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_add_pro_2_prevent_burst_AddPwState_Mask                                                   cBit3_0
#define cAf6_add_pro_2_prevent_burst_AddPwState_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble Payload Size Control
Reg Addr   : 0x00002000-0x0000253F
Reg Formula: 0x00002000 +  $PWID
    Where  : 
           + $PWID(0-1343): Pseudowire ID
Reg Desc   : 
These registers are used to configure payload size in each PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_pyld_size_ctrl_Base                                                                 0x00002000

/*--------------------------------------
BitField Name: DatIdlePatEnb
BitField Type: RW
BitField Desc: enable idle pattern for CAS when AIS
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_CasIdlePatEn_Mask                                                      cBit31
#define cAf6_pyld_size_ctrl_CasIdlePatEn_Shift                                                         31

/*--------------------------------------
BitField Name: CasIdlePattern
BitField Type: RW
BitField Desc: idle pattern for CAS when AIS
BitField Bits: [30:27]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_CasIdlePattern_Mask                                                      cBit30_27
#define cAf6_pyld_size_ctrl_CasIdlePattern_Shift                                                            27

/*--------------------------------------
BitField Name: DatIdlePattern
BitField Type: RW
BitField Desc: idle pattern for data when AIS
BitField Bits: [26:19]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_DatIdlePattern_Mask                                                      cBit26_19
#define cAf6_pyld_size_ctrl_DatIdlePattern_Shift                                                            19

/*--------------------------------------
BitField Name: RTPTsShapperEnb
BitField Type: RW
BitField Desc: RTP TS Shapper Enable. Should be enable in DS1/E1 SAToP. In
CESoPSN only enable for PW which is used as Source for DCR
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_RTPTsShapperEnb_Mask                                                        cBit18
#define cAf6_pyld_size_ctrl_RTPTsShapperEnb_Shift                                                           18

/*--------------------------------------
BitField Name: PDHPWPlaAsmPldTypeCtrl
BitField Type: RW
BitField Desc: Payload type in the PW channel 0: SAToP 4: CES with CAS 5: CES
without CAS 6: CEP
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldTypeCtrl_Mask                                              cBit17_15
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldTypeCtrl_Shift                                                    15

/*--------------------------------------
BitField Name: PDHPWPlaAsmPldSizeCtrl
BitField Type: RW
BitField Desc: Payload size in the PW channel SAToP mode: [11:0] payload size in
a packet CES mode [5:0] number of DS0 [14:6] number of NxDS0 basic CEP mode:
[11:0] payload size in a packetizer fractional CEP mode: [3:0] Because value of
payload size may be 3/9, 4/9, 5/9, 6/9, 7/9, 8/9, or 9/9 of VC-3 or VC-4 SPE,
depended on fractional VC-3 or fractional VC-4 CEP mode. And a VC-3 or VC-4
//SPE has 9 row. So configured value will be number of rows of a SPE. Example,
if value of payload size is 3/9, it means packetizer will assemble payload data
of 3 rows of a SPE,  so value of the field //will be 3.
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldSizeCtrl_Mask                                               cBit14_0
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldSizeCtrl_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble uP Address Convert Control
Reg Addr   : 0x00000000
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used to enable converting mode for uP address as accessing into some RAMs which request to convert identification.

------------------------------------------------------------------------------*/
#define cAf6Reg_up_add_conv_ctrl_Base                                                               0x00000000

/*--------------------------------------
BitField Name: PDHPWPlaAsmCPUAdrConvEnCtrl
BitField Type: RW
BitField Desc: Enable uP address converting 1: enable 0: disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_up_add_conv_ctrl_PDHPWPlaAsmCPUAdrConvEnCtrl_Mask                                           cBit1
#define cAf6_up_add_conv_ctrl_PDHPWPlaAsmCPUAdrConvEnCtrl_Shift                                              1


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble Engine Status
Reg Addr   : 0x00028000-0x0002853F #The address format for these registers is 0x0028000 + $PWID
Reg Formula: 0x00028000 +  $PWID
    Where  : 
           + $PWID(0-1343): Pseudowire ID
Reg Desc   : 
These registers are used to save status in the engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_txeth_enable_ctrl_Base                                                              0x00028000

/*--------------------------------------
BitField Name: PDHPWPlaAsmAAL1PtrSetSta
BitField Type: RW
BitField Desc: indicate that the pointer is already set in that AAL1 cycle 1:
enable 0: disable
BitField Bits: [78]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmAAL1PtrSetSta_Mask                                            cBit14
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmAAL1PtrSetSta_Shift                                               14

/*--------------------------------------
BitField Name: PDHPWPlaAsmSigNumStartSta
BitField Type: RW
BitField Desc: indicate number of signaling bytes to append into the last
payload bytes to become a 4-bytes data valid.
BitField Bits: [77:76]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmSigNumStartSta_Mask                                        cBit13_12
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmSigNumStartSta_Shift                                              12

/*--------------------------------------
BitField Name: PDHPWPlaAsmGenSigCntSta
BitField Type: RW
BitField Desc: indicate number of the remaining signaling bytes which need to be
append into PW packet.
BitField Bits: [75:71]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmGenSigCntSta_Mask                                           cBit11_7
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmGenSigCntSta_Shift                                                 7

/*--------------------------------------
BitField Name: PDHPWPlaAsmPDUNumCntSta
BitField Type: RW
BitField Desc: indicate the current number of the PDU counter in AAL1 mode
BitField Bits: [70:65]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUNumCntSta_Mask                                            cBit6_1
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUNumCntSta_Shift                                                 1

/*--------------------------------------
BitField Name: PDHPWPlaAsmPDUCylCntSta
BitField Type: RW
BitField Desc: indicate the current number of the cycle PDU counter in AAL1 mode
BitField Bits: [64:62]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUCylCntSta_01_Mask                                       cBit31_30
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUCylCntSta_01_Shift                                             30
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUCylCntSta_02_Mask                                           cBit0
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUCylCntSta_02_Shift                                              0

/*--------------------------------------
BitField Name: PDHPWPlaAsmPDUByteCntSta
BitField Type: RW
BitField Desc: indicate the current number of bytes of a PDU in AAL1 mode
BitField Bits: [61:58]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUByteCntSta_Mask                                         cBit29_26
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmPDUByteCntSta_Shift                                               26

/*--------------------------------------
BitField Name: PDHPWPlaAsmSatopCESByteCntSta
BitField Type: RW
BitField Desc: indicate number of bytes in Satop and CES mode With Satop mode
number of bytes of the payload in PW packet With CES mode [57:49]  indicate the
current number of NxDS0 counter in CES mode [48:43]  indicate the current number
of DS0 byte counter in CES mode With basic CEP mode: number of bytes of the
payload in PW packet With fractional CEP mode [57:49] indicate the current
number of bytes of actual payload in PW packet [48:43] indicate the current row
of payload in PW packet
BitField Bits: [57:43]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmSatopCESByteCntSta_Mask                                    cBit25_11
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmSatopCESByteCntSta_Shift                                          11

/*--------------------------------------
BitField Name: PDHPWPlaAsmDataNumSta
BitField Type: RW
BitField Desc: indicate the current number of data bytes in the data buffer
BitField Bits: [42:40]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmDataNumSta_Mask                                             cBit10_8
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmDataNumSta_Shift                                                   8

/*--------------------------------------
BitField Name: PDHPWPlaAsmDataBufSta
BitField Type: RW
BitField Desc: as data buffer to save data with the maximum 5 bytes
BitField Bits: [39:0]
--------------------------------------*/
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmDataBufSta_01_Mask                                          cBit31_0
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmDataBufSta_01_Shift                                                0
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmDataBufSta_02_Mask                                           cBit7_0
#define cAf6_txeth_enable_ctrl_PDHPWPlaAsmDataBufSta_02_Shift                                                0

#endif /* _AF6_REG_AF6CNC1011_RD_PLA_H_ */

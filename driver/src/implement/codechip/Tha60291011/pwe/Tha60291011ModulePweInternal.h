/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60291011ModulePweInternal.h
 * 
 * Created Date: Jul 18, 2018
 *
 * Description : 60291011 module PWE internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEPWEINTERNAL_H_
#define _THA60291011MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/pwe/Tha60290011ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011ModulePwe
    {
    tTha60290011ModulePwe super;
    }tTha60291011ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60291011ModulePweObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULEPWEINTERNAL_H_ */


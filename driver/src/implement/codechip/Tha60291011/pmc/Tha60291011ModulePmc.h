/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60291011ModulePmc.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : Interface of the 60291011 PMC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEPMC_H_
#define _THA60291011MODULEPMC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60291011ModulePmcNew(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULEPMC_H_ */


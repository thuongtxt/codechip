/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60291011ModulePmcInternal.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : 60291011 module PMC internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEPMCINTERNAL_H_
#define _THA60291011MODULEPMCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/pmc/Tha60290011ModulePmcInternal.h"
#include "Tha60291011ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011ModulePmc
    {
    tTha60290011ModulePmcV3dot4 super;
    }tTha60291011ModulePmc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULEPMCINTERNAL_H_ */


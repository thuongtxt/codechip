/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60291011ModulePmc.c
 *
 * Created Date: Jan 24, 2018
 *
 * Description : 60291011 PMC module implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60291011ModulePmcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwCounterOffset_txrbit 0x00000
#define cPwCounterOffset_txpbit 0x01000
#define cPwCounterOffset_txnbit 0x02000
#define cPwCounterOffset_txlbit 0x03000
#define cPwCounterOffset_txpkt  0x04000
#define cPwCounterOffset_txpwcntbyte 0x80000

#define cPwCounterOffset_rxpwcnt 0x10000
#define cPwCounterOffset_rxpkt   0x11000
#define cPwCounterOffset_rxrbit  0x20000
#define cPwCounterOffset_rxpbit  0x21000
#define cPwCounterOffset_rxnbit  0x22000
#define cPwCounterOffset_rxlbit  0x23000
#define cPwCounterOffset_rxstray 0x24000
#define cPwCounterOffset_rxmalform 0x25000
#define cPwCounterOffset_rxunderrun 0x30000
#define cPwCounterOffset_rxoverrun  0x31000
#define cPwCounterOffset_rxlops     0x32000
#define cPwCounterOffset_rxlost     0x33000
#define cPwCounterOffset_rxearly    0x34000
#define cPwCounterOffset_rxlate     0x35000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290011ModulePmcV3Methods m_Tha60290011ModulePmcV3Override;
static tThaModulePmcMethods m_ThaModulePmcOverride;

/* Save super */
static const tTha60290011ModulePmcV3Methods *m_Tha60290011ModulePmcV3Methods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwPwCounterR2COffset(Tha60290011ModulePmcV3 self, AtPw pw, eBool clear)
    {
    uint32 r2cOffset = clear ? 2048 : 0;
    AtUnused(self);
    AtUnused(pw);
    return r2cOffset;
    }

static uint32 PwCounterRead2Clear(ThaModulePmc self, AtPw pw, uint32 offset, eBool clear)
    {
    return Tha60290011ModulePmcV3PwCounterRead2Clear(self, pw, offset, clear);
    }

static uint32 PwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txpkt, clear);
    }

static uint32 PwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txpwcntbyte, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txlbit, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txrbit, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txnbit, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txpbit, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txnbit, clear);
    }

static uint32 PwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxpkt, clear);
    }

static uint32 PwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxmalform, clear);
    }

static uint32 PwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxstray, clear);
    }

static uint32 PwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlbit, clear);
    }

static uint32 PwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxrbit, clear);
    }

static uint32 PwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxnbit, clear);
    }

static uint32 PwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxpbit, clear);
    }

static uint32 PwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxnbit, clear);
    }

static uint32 PwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxpwcnt, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlost, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxearly, clear);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlate, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxoverrun, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxunderrun, clear);
    }

static uint32 PwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlops, clear);
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMalformedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxStrayPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLopsGet);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }
static void OverrideTha60290011ModulePmcV3(AtModule self)
    {
    Tha60290011ModulePmcV3 module = (Tha60290011ModulePmcV3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ModulePmcV3Methods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModulePmcV3Override, m_Tha60290011ModulePmcV3Methods, sizeof(m_Tha60290011ModulePmcV3Override));

        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwPwCounterR2COffset);
        }

    mMethodsSet(module, &m_Tha60290011ModulePmcV3Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    OverrideTha60290011ModulePmcV3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModulePmc);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePmcV3dot4ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60291011ModulePmcNew(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(module, self);
    }

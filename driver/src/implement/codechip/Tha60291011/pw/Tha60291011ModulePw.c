/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60291011ModulePw.c
 *
 * Created Date: Jul 10, 2018
 *
 * Description : PW module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60291011Device.h"
#include "headercontrollers/Tha60291011PwHeaderController.h"
#include "Tha60291011ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods          m_AtModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;

/* Save super implementation */
static const tAtModulePwMethods     *m_AtModulePwMethods = NULL;
static const tThaModulePwMethods    *m_ThaModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1344;
    }

static eBool CasIdleCodeIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NewColissionLookupTable(ThaModulePw self)
    {
    return Tha60291011Device8ColissionInLookupTableIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    if (NewColissionLookupTable(self))
        return Tha60291011PwHeaderControllerNew((ThaPwAdapter)adapter);

    return m_ThaModulePwMethods->HeaderControllerObjectCreate(self, adapter);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw module = (ThaModulePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, m_ThaModulePwMethods, sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, HeaderControllerObjectCreate);
        }

    mMethodsSet(module, &m_ThaModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, CasIdleCodeIsSupported);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw(self);
    OverrideAtModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModulePw);
    }

AtModulePw Tha60291011ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60291011ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011ModulePwObjectInit(newModule, device);
    }

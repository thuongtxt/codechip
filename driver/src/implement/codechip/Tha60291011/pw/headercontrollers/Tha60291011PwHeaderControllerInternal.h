/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60291011PwHeaderControllerInternal.h
 * 
 * Created Date: Oct 10, 2018
 *
 * Description : Header controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011PWHEADERCONTROLLERINTERNAL_H_
#define _THA60291011PWHEADERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210031/pw/headercontrollers/Tha60210031PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60291011PwHeaderController
    {
    tTha60210031PwHeaderController super;
    }tTha60291011PwHeaderController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011PWHEADERCONTROLLERINTERNAL_H_ */


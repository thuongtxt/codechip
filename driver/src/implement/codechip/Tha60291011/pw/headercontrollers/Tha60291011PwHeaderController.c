/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60291011PwHeaderController.c
 *
 * Created Date: Oct 10, 2018
 *
 * Description : PW header controller of 60291011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../Tha60210011/cla/hbce/Tha60210011Hbce.h"
#include "Tha60291011PwHeaderControllerInternal.h"
#include "Tha60291011PwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ApplyCellToHw(ThaPwHeaderController self)
    {
    ThaHbceMemoryCell cell;
    ThaHbce hbce;
    ThaHbceMemoryPool memPool;
    ThaHbceEntry entry;
    ThaHbceMemoryCellContent content;

    if (ThaPwHeaderControllerInAccessible(self))
        return cAtOk;

    content = ThaPwHeaderControllerHbceMemoryCellContentGet(self);
    if (content == NULL)
        return cAtOk;

    cell = ThaHbceMemoryCellContentCellGet(content);
    hbce = ThaPwAdapterHbceGet((AtPw)ThaPwHeaderControllerAdapterGet(self));
    memPool = ThaHbceMemoryPoolGet(hbce);
    entry = ThaHbceMemoryCellContentHashEntryGet(content);

    Tha60210011HbceMemoryPoolApplyCellToHardware(memPool, ThaHbceMemoryCellIndexGet(cell));
    Tha60210011HbceHashEntryTabCtrlApply(hbce, ThaHbceEntryIndexGet(entry));
    return cAtOk;
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, ApplyCellToHw);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011PwHeaderController);
    }

static ThaPwHeaderController ObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60291011PwHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProvider, adapter);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60291011PwHeaderController.h
 * 
 * Created Date: Oct 10, 2018
 *
 * Description : PW header controller of 60291011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011PWHEADERCONTROLLER_H_
#define _THA60291011PWHEADERCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController Tha60291011PwHeaderControllerNew(ThaPwAdapter adapter);

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011PWHEADERCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60291011ModulePwInternal.h
 *
 * Created Date: Jul 10, 2018
 *
 * Description : Internal Interface for Pw Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULEPWINTERNAL_H_
#define _THA60291011MODULEPWINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/pw/Tha60290011ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60291011ModulePw
    {
    tTha60290011ModulePw super;
    }tTha60291011ModulePw;

typedef struct tTha60291011ModulePwV2
    {
    tTha60291011ModulePw super;
    }tTha60291011ModulePwV2;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60291011ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60291011MODULEPWINTERNAL_H_ */

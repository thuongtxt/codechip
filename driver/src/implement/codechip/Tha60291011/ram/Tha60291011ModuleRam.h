/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60291011ModuleRam.h
 * 
 * Created Date: Oct 29, 2018
 *
 * Description : RAM module for 60291011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60291011MODULERAM_H_
#define _THA60291011MODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtModuleRam Tha60291011ModuleRamNew(AtDevice device);
/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60291011MODULERAM_H_ */


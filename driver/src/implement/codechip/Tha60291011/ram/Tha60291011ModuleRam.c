/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290011ModuleRam.c
 *
 * Created Date: Oct 29, 2018
 *
 * Description : Module RAM for 60291011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60291011ModuleRamInternal.h"
#include "Tha60291011ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60291011ModuleRam*)(self))
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/* Super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tThaModuleRamMethods *m_ThaModuleRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const tThaInternalRamEntry* LocalAllInternalRamEntry(ThaModuleRam self, uint32 *numRams)
    {
    static const tThaInternalRamEntry allEntries[] = {
                                          {"OCN Tx J0 Insertion Buffer SliceId 0",                           0xA21200},
                                          {"OCN Tx J0 Insertion Buffer SliceId 1",                           0xA21600},
                                          {"OCN Tx Framer Per Channel Control SliceId 0",                    0xA21000},
                                          {"OCN Tx Framer Per Channel Control SliceId 1",                    0xA21400},
                                          {"OCN STS Pointer Generator Per Channel Control SliceId 0",        0xA23000},
                                          {"OCN STS Pointer Generator Per Channel Control SliceId 1",        0xA23200},
                                          {"OCN VTTU Pointer Generator Per Channel Control SliceId 0",       0xA60800},
                                          {"OCN VTTU Pointer Generator Per Channel Control SliceId 1",       0xA64800},
                                          {"OCN TXPP Per STS Multiplexing Control SliceId 0",                0xA60000},
                                          {"OCN TXPP Per STS Multiplexing Control SliceId 1",                0xA64000},
                                          {"OCN VTTU Pointer Interpreter Per Channel Control SliceId 0",     0xA40800},
                                          {"OCN VTTU Pointer Interpreter Per Channel Control SliceId 1",     0xA44800},
                                          {"OCN RXPP Per STS payload Control SliceId 0",                     0xA40000},
                                          {"OCN RXPP Per STS payload Control SliceId 1",                     0xA44000},
                                          {"OCN STS Pointer Interpreter Per Channel Control SliceId 0",      0xA22000},
                                          {"OCN STS Pointer Interpreter Per Channel Control SliceId 1",      0xA22200},
                                          {"OCN TOH Monitoring Per Channel Control SliceId 1",               0xA24100},
                                          {"POH CPE STS/TU3 Control Register",                               0xB2A000},
                                          {"POH CPE VT Control Register",                                    0xB28000},
                                          {"POH Termintate Insert Control STS",                              0xB40400},
                                          {"POH Termintate Insert Control VT/TU3",                           0xB44000},
                                          {"POH BER Threshold 2",                                            0xB60400},
                                          {"POH BER Control VT/DSN, POH BER Control STS/TU3",                0xB62000},
                                          {"PDH DS1/E1/J1 Rx Framer Mux Control SliceId 0",                  0x730000},
                                          {"PDH STS/VT Demap Control SliceId 0",                             0x724000},
                                          {"PDH RxM23E23 Control SliceId 0",                                 0x740000},
                                          {"PDH RxDS3E3 OH Pro Control SliceId 0",                           0x740420},
                                          {"PDH RxM12E12 Control SliceId 0",                                 0x741000},
                                          {"PDH DS1/E1/J1 Rx Framer Control SliceId 0",                      0x754000},
                                          {"PDH DS1/E1/J1 Tx Framer Control SliceId 0",                      0x794000},
                                          {"PDH DS1/E1/J1 Tx Framer Signalling Control SliceId 0",           0x794800},
                                          {"PDH TxM12E12 Control SliceId 0",                                 0x781000},
                                          {"PDH TxM23E23 Control SliceId 0",                                 0x780000},
                                          {"PDH TxM23E23 E3g832 Trace Byte SliceId 0",                       0x780200},
                                          {"PDH STS/VT Map Control SliceId 0",                               0x776000},
                                          {sPDH_VT_Async_Map_Control" SliceId 0",                            0x776800},
                                          {"PDH DS1/E1/J1 Rx Framer Mux Control SliceId 1",                  0x830000},
                                          {"PDH STS/VT Demap Control SliceId 1",                             0x824000},
                                          {"PDH RxM23E23 Control SliceId 1",                                 0x840000},
                                          {"PDH RxDS3E3 OH Pro Control SliceId 1",                           0x840420},
                                          {"PDH RxM12E12 Control SliceId 1",                                 0x841000},
                                          {"PDH DS1/E1/J1 Rx Framer Control SliceId 1",                      0x854000},
                                          {"PDH DS1/E1/J1 Tx Framer Control SliceId 1",                      0x894000},
                                          {"PDH DS1/E1/J1 Tx Framer Signalling Control SliceId 1",           0x894800},
                                          {"PDH TxM12E12 Control SliceId 1",                                 0x881000},
                                          {"PDH TxM23E23 Control SliceId 1",                                 0x880000},
                                          {"PDH TxM23E23 E3g832 Trace Byte SliceId 1",                       0x880200},
                                          {"PDH STS/VT Map Control SliceId 1",                               0x876000},
                                          {sPDH_VT_Async_Map_Control" SliceId 1",                            0x876800},
                                          {"MAP Thalassa Map Channel Control SliceId 0",                     0x1B4000},
                                          {"MAP Thalassa Map Line Control SliceId 0",                        0x1B0000},
                                          {"MAP Thalassa Map Channel Control SliceId 1",                     0x1D4000},
                                          {"MAP Thalassa Map Line Control SliceId 1",                        0x1D0000},
                                          {"DEMAP Thalassa Demap Channel Control SliceId 0",                 0x1A0000},
                                          {"DEMAP Thalassa Demap Channel Control SliceId 1",                 0x1C0000},
                                          {"CDR STS Timing control SliceId 0",                               0x280800},
                                          {"CDR VT Timing control SliceId 0",                                0x280C00},
                                          {"CDR ACR Engine Timing control SliceId 0",                        0x2A0800},
                                          {"CDR STS Timing control SliceId 1",                               0x2C0800},
                                          {"CDR VT Timing control SliceId 1",                                0x2C0C00},
                                          {"CDR ACR Engine Timing control SliceId 1",                        0x2E0800},
                                          {"Thalassa PDHPW Payload Assemble Payload Size Control SliceId 0", 0x202000},
                                          {"Thalassa PDHPW Payload Assemble Payload Size Control SliceId 1", 0x212000},
                                          {"Pseudowire PDA Reorder Control",                                 0x244000},
                                          {"Pseudowire PDA Jitter Buffer Control",                           0x243000},
                                          {"Pseudowire PDA TDM Mode Control",                                0x245000},
                                          {"PDA DDR CRC error", /* 66 */                                     0x0},
                                          {"Pseudowire Transmit Enable Control",                             0x321000},
                                          {sPseudowire_Transmit_Ethernet_Header_Value_Control,               0x308000},
                                          {"Pseudowire Transmit Ethernet Header Length Control",             0x301000},
                                          {"Pseudowire Transmit Header RTP SSRC Value Control",              0x303000},
                                          {"Pseudowire Transmit HSPW Label Control",                         0x305000},
                                          {"Pseudowire Transmit UPSR and HSPW mode Control",                 0x306000},
                                          {"Pseudowire Transmit UPSR Group Enable Control",                  0x307000},
                                          {"Pseudowire Transmit HSPW Group Protection Control",              0x304000},
                                          {"Classify HBCE Hashing Table Control PageID 0",                   0x44A000},
                                          {"Classify HBCE Hashing Table Control PageID 1",                   0x44B000},
                                          {"Classify HBCE Looking Up Information Control",                   0x444000},
                                          {"Classify Per Pseudowire Type Control",                           0x44C000},
                                          {"Classify Pseudowire MEF over MPLS Control",                      0x44D000},
                                          {"Classify Per Group Enable Control",                              0x449000},
                                          {"CDR Pseudowire Look Up Control",                                 0x443000},
                                          {sSoftwave_Control,                                                cInvalidUint32},
                                          {"Rx DDR ECC", /* 83 */                                            cInvalidUint32},
                                          {"Tx DDR ECC",  /* 84 */                                            cInvalidUint32}
                                          };
    AtUnused(self);
    *numRams = mCount(allEntries);
    return allEntries;
    }

static const tThaInternalRamEntry* AllInternalRamEntry(ThaModuleRam self, uint32 *numRams)
    {
    return LocalAllInternalRamEntry(self, numRams);
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    uint32 numRams = 0;
    if (mThis(self)->numInternalRams == 0)
        {
        LocalAllInternalRamEntry((ThaModuleRam)self, &numRams);
        mThis(self)->numInternalRams = numRams;
        }

    return mThis(self)->numInternalRams;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60291011ModuleRam *object = (tTha60291011ModuleRam *)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(numInternalRams);
    }

static void OverrideAtObject(AtModuleRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule ramModule = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(ramModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(ramModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        }

    mMethodsSet(ramModule, &m_AtModuleOverride);
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleRamMethods = mMethodsGet(ramModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, m_ThaModuleRamMethods, sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, AllInternalRamEntry);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60291011ModuleRam);
    }

AtModuleRam Tha60291011ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60291011ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60291011ModuleRamObjectInit(newModule, device);
    }

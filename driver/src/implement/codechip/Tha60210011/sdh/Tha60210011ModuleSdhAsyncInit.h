/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011ModuleSdhAsyncInit.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : Async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULESDHASYNCINIT_H_
#define _THA60210011MODULESDHASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011ModuleSdhAsyncInit(AtModule self);
eAtRet Tha60210011ModuleSdhSuperAsyncInit(AtModule self);
void Tha60210011ModuleSdhLoLineDbInit(AtModule self);
eAtRet Tha60210011ModuleSdhPohMonitorDefaultSet(AtModule self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULESDHASYNCINIT_H_ */


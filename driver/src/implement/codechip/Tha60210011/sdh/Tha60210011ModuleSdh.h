/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH module name
 * 
 * File        : Tha60210011ModuleSdh.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : 60210011SDH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULESDH_H_
#define _THA60210011MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/sdh/Tha60150011ModuleSdh.h"
#include "AtCrossConnect.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumStsInLoLine 48
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleSdh * Tha60210011ModuleSdh;
typedef struct tTha60210011Tfi5LineAuVc * Tha60210011Tfi5LineAuVc;
typedef struct tTha60210011Tfi5LineTu3Vc * Tha60210011Tfi5LineTu3Vc;
typedef struct tTha60210011Tfi5LineVc1x * Tha60210011Tfi5LineVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60210011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
AtSdhLine   Tha60210011Tfi5LineNew(uint32 tfi5LineId, AtModuleSdh module);

eBool Tha60210011ModuleSdhIsTfi5Line(AtModuleSdh self, uint8 lineId);
eBool Tha60210011ModuleSdhIsHoLine(AtModuleSdh self, uint8 lineId);
eBool Tha60210011ModuleSdhIsLoLine(AtModuleSdh self, uint8 lineId);
uint8 Tha60210011ModuleSdhHoLineStartId(AtModuleSdh self);
uint8 Tha60210011ModuleSdhLoLineStartId(AtModuleSdh self);

uint8 Tha60210011ModuleSdhMaxNumHoLines(AtModuleSdh self);
uint8 Tha60210011ModuleSdhMaxNumLoLines(AtModuleSdh self);

AtSdhAug Tha60210011Tfi5LineAugNew(uint32 augId, uint8 augType, AtModuleSdh module);
AtSdhAu  Tha60210011Tfi5LineAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhVc  Tha60210011Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTug Tha60210011Tfi5LineTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc  Tha60210011Tfi5LineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc  Tha60210011Tfi5LineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTu Tha60210011Tfi5LineTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
eBool Tha60210011ModuleSdhChannelIsHoPath(AtSdhChannel channel);
AtPrbsEngine Tha60210011PrbsEngineSerdesTfi5New(AtSerdesController serdesController);

/* To get Alarm.Error from PM/FM or From Registers */
eBool Tha60210011ModuleSdhHasChannelStatus(AtModuleSdh self);
AtSdhChannel Tha60210011ModuleSdhAuFromHwStsGet(AtSdhLine line, uint8 swSts);
AtSdhLine Tha60210011ModuleSdhLineFromLoLineStsGet(AtModuleSdh self, uint8 loSlice, uint8 loHwStsInSlice, uint8 *stsInLine);
AtSdhChannel Tha60210011SdhTuPathFromAuPathHwStsGet(AtSdhChannel au, uint8 tfi5HwSts, uint8 vtgId, uint8 vtId);

/* Dynamic lo-line resource management */
eAtRet Tha60210011ModuleSdhLoLineStsAllocate(AtModuleSdh self, AtSdhChannel hoVc, uint8 *slice, uint8 *hwSts);
eAtRet Tha60210011ModuleSdhLoLineStsDeallocate(AtModuleSdh self, uint8 slice, uint8 hwSts);
eAtRet Tha60210011ModuleSdhLoLineStsInUsedSet(AtModuleSdh self, uint8 lineId, uint8 sts, eBool isInUsed);
eBool Tha60210011ModuleSdhFreeStsIsAvailable(AtModuleSdh self, uint8 numSts);
eAtRet Tha60210011Tfi5LineAuVcLomCheckingEnable(AtSdhChannel self, eBool enable);
eAtRet Tha60210011Tfi5LineAuVcLomCheckingUpdate(AtSdhChannel self);
uint16* Tha60210011Tfi5LineAuVcAllTfi5Sts(AtSdhChannel self);
eBool Tha60210011ModuleSdhLoLineStsIsInUsed(AtModuleSdh self, uint8 slice, uint8 hwSts);
eAtRet Tha60210011ModuleSdhLoLineStsInUsedSet(AtModuleSdh self, uint8 lineId, uint8 sts, eBool isInUsed);

uint32 Tha60210011ModuleSdhUpsrStsFromTfi5SwStsGet(uint8 lineId, uint8 stsId);

eAtModuleSdhRet Tha60210011Tfi5LineAuVcMapTypeSet(AtSdhChannel self, uint8 mapType, uint16* selectedLoLineSts, uint8 numSelectedSts);
AtSdhChannel Tha60210011ModuleSdhHoVcSubChannelHwStsUpdate(AtSdhChannel parent, AtSdhChannel vcSubChannel);
eBool Tha60210011ModuleSdhVcBelongsToLoLine(AtSdhChannel vc);
uint32 Tha60210011ModuleSdhUpsrStsFromTfi5SwStsGet(uint8 lineId, uint8 stsId);
eBool Tha60210011ModuleSdhAutoCrossConnect(AtModuleSdh self);
uint32 Tha60210011ModuleSdhSerdesPrbsBaseAddress(AtModuleSdh self, AtSerdesController serdes);

eAtRet Tha60210011ModuleSdhUpsrSfAlarmMaskSet(AtModuleSdh self, uint32 alarmMask);
uint32 Tha60210011ModuleSdhUpsrSfAlarmMaskGet(AtModuleSdh self);
eAtRet Tha60210011ModuleSdhUpsrSdAlarmMaskSet(AtModuleSdh self, uint32 alarmMask);
uint32 Tha60210011ModuleSdhUpsrSdAlarmMaskGet(AtModuleSdh self);

uint32 Tha60210011Tfi5LineAuPiForcedAlarmGet(AtSdhAu self);
eAtRet Tha60210011Tfi5LineAuPiAlarmForce(AtSdhAu self, uint32 alarmType);
eAtRet Tha60210011Tfi5LineAuPiAlarmUnForce(AtSdhAu self, uint32 alarmType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULESDH_H_ */


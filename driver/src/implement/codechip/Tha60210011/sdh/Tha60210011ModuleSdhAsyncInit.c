/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011ModuleSdhAsyncInit.c
 *
 * Created Date: Aug 22, 2016
 *
 * Description : Async init
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ModuleSdhInternal.h"
#include "Tha60210011ModuleSdhAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eAtRet Tha60210011ModuleSdhAsyncInit(AtModule self)
    {
    eAtRet ret = cAtOk;
    uint32 state = ((Tha60210011ModuleSdh)self)->asyncInitState;

    switch (state)
        {
        case 0:
            ret = Tha60210011ModuleSdhSuperAsyncInit(self);
            if (ret == cAtOk)
                {
                state = 1;
                ret = cAtErrorAgain;
                }
            break;
        case 1:
            Tha60210011ModuleSdhLoLineDbInit(self);
            Tha60210011ModuleSdhPohMonitorDefaultSet(self);
            ret = cAtOk;
            state = 0;
            break;
        default:
            state = 0;
            ret = cAtErrorDevFail;
        }

    ((Tha60210011ModuleSdh)self)->asyncInitState = state;
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, "AtChannelInit");
    return ret;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineAu.c
 *
 * Created Date: Apr 22, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 AU.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "Tha60210011ModuleSdh.h"
#include "../poh/Tha60210011PohReg.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "Tha60210011Tfi5LineAuInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumStsInSts3c 3

/*--------------------------- Macros -----------------------------------------*/
#define mSdhModule(self) ((AtModuleSdh)AtChannelModuleGet((AtChannel)self))
#define mThis(self) ((tTha60210011Tfi5LineAu*)self)
#define mThaOcnModule(self) (ThaModuleOcn)OcnModule((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011Tfi5LineAuMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(AtSdhPath self)
    {
    return Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    }

static Tha60210011ModuleOcn OcnModule(AtChannel self)
    {
    return (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(self);
    }

static uint32 OcnStsPointerGeneratorPerChannelControl(AtSdhPath self)
    {
    return Tha60210011ModuleOcnStsPointerGeneratorPerChannelControl(OcnModule((AtChannel)self), (AtSdhChannel)self);
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
    uint32 address = OcnStsPointerGeneratorPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgSSInsPatt_, value);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 TxSsGet(AtSdhPath self)
    {
    uint32 address = OcnStsPointerGeneratorPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return (uint8)mRegField(regVal, cAf6_spgramctl_StsPgSSInsPatt_);
    }

static eAtRet SsInsertionEnable(AtSdhPath self, eBool enable)
    {
    uint32 address = OcnStsPointerGeneratorPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgSSInsEn_, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool SsInsertionIsEnabled(AtSdhPath self)
    {
    uint32 address = OcnStsPointerGeneratorPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return (regVal & cAf6_spgramctl_StsPgSSInsEn_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 OcnStsPointerInterpreterPerChannelControl(AtSdhPath self)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    return Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ocnModule, (AtSdhChannel)self);
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
    uint32 address = OcnStsPointerInterpreterPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spiramctl_StsPiSSDetPatt_, value);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
    uint32 address = OcnStsPointerInterpreterPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    return (uint8)mRegField(regVal, cAf6_spiramctl_StsPiSSDetPatt_);
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
    uint32 address = OcnStsPointerInterpreterPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spiramctl_StsPiSSDetEn_, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
    uint32 address = OcnStsPointerInterpreterPerChannelControl(self) + DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return (regVal & cAf6_spiramctl_StsPiSSDetEn_Mask) ? cAtTrue : cAtFalse;
    }

static void InterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    AtSdhPath pohProcessor = (AtSdhPath)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    AtSdhPathInterruptProcess(pohProcessor, slice, sts, vt, hal);
    }

static ThaPohProcessor PohProcessor(AtSdhChannel self)
    {
    AtSdhVc vc = (AtSdhVc)AtSdhChannelSubChannelGet(self, 0);
    return ThaSdhVcPohProcessorGet(vc);
    }

static eBool HoldOffTimerSupported(AtSdhChannel self)
    {
    return AtSdhChannelHoldOffTimerSupported((AtSdhChannel)PohProcessor(self));
    }

static eBool HoldOnTimerSupported(AtSdhChannel self)
    {
    return AtSdhChannelHoldOnTimerSupported((AtSdhChannel)PohProcessor(self));
    }

static uint32 DefectGet(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    uint32 defects;

    if (pohProcessor == NULL)
        return 0;

    defects = AtChannelDefectGet(pohProcessor);

    /* Convert it to software value */
    if (defects & cAtSdhPathAlarmAis)
        return cAtSdhPathAlarmAis;
    if (defects & cAtSdhPathAlarmLop)
        return cAtSdhPathAlarmLop;

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    return AtChannelDefectInterruptGet(pohProcessor);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    return AtChannelDefectInterruptClear(pohProcessor);
    return 0;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    	return AtChannelSupportedInterruptMasks(pohProcessor);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor == NULL)
        return cAtErrorNotApplicable;
    return AtChannelInterruptMaskSet(pohProcessor, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtSdhVc vc = (AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    if (vc)
        {
        AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet(vc);
    if (pohProcessor)
    return AtChannelInterruptMaskGet(pohProcessor);
        }

    return 0;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    return AtChannelInterruptEnable(pohProcessor);
    return cAtErrorNotApplicable;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    return AtChannelInterruptDisable(pohProcessor);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    return AtChannelInterruptIsEnabled(pohProcessor);
    return cAtFalse;
    }

static eAtRet HwTxAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    uint32 address = OcnStsPointerGeneratorPerChannelControl((AtSdhPath)self) + DefaultOffset((AtSdhPath)self);
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (alarmType & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_spgramctl_StsPgAisFrc_, mBoolToBin(enable));

    if (alarmType & cAtSdhPathAlarmLop)
        mRegFieldSet(regVal, cAf6_spgramctl_StsPgLopFrc_, mBoolToBin(enable));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet CacheTxAisAlarmForce(AtChannel self, uint32 alarmType, eBool forced)
    {
    if (alarmType & cAtSdhPathAlarmAis)
        mThis(self)->txAisExplicitlyForced = forced;
    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool forced)
    {
    eAtRet ret;

    if ((alarmType & AtChannelTxForcableAlarmsGet(self)) == 0)
        return cAtErrorModeNotSupport;

    ret = HwTxAlarmForce(self, alarmType, forced);
    if (ret == cAtOk)
        return CacheTxAisAlarmForce(self, alarmType, forced);

    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;
    uint32 regAddr = OcnStsPointerGeneratorPerChannelControl((AtSdhPath)self) + DefaultOffset((AtSdhPath)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_spgramctl_StsPgAisFrc_Mask)
        forcedAlarms |= cAtSdhPathAlarmAis;

    if (regVal & cAf6_spgramctl_StsPgLopFrc_Mask)
        forcedAlarms |= cAtSdhPathAlarmLop;

    return forcedAlarms;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static eAtRet PiAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    ThaModuleOcn moduleOcn = ThaModuleOcnFromChannel(self);
    uint32 address = OcnStsPointerInterpreterPerChannelControl((AtSdhPath)self);
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)self);
    uint8 stsId = AtSdhChannelSts1Get((AtSdhChannel)self);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = address + ThaModuleOcnStsDefaultOffset(moduleOcn, (AtSdhChannel)self, (uint8)(stsId + sts_i));
        uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

        if (alarmType & cAtSdhPathAlarmAis)
            mRegFieldSet(regVal, cAf6_spiramctl_StsPiAisFrc_, mBoolToBin(enable));

        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static eAtRet MoveRxAlarmForcingPoint(AtChannel self, uint32 alarmType, eBool enable)
    {
    AtChannel vc = (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);

    if (AtChannelLoopbackGet(vc) == cAtLoopbackModeRemote)
        return ThaModuleOcnPathAisRxForcingPointMove(mThaOcnModule(self), (AtSdhAu)self, enable);

    return PiAlarmForce(self, alarmType, enable);
    }

static eAtRet HwRxAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    if (ThaModuleOcnCanMovePathAisRxForcingPoint(mThaOcnModule(self)))
        return MoveRxAlarmForcingPoint(self, alarmType, enable);
    return PiAlarmForce(self, alarmType, enable);
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if ((alarmType & AtChannelRxForcableAlarmsGet(self)) == 0)
        return cAtErrorModeNotSupport;
    return HwRxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if ((alarmType & AtChannelRxForcableAlarmsGet(self)) == 0)
        return cAtErrorModeNotSupport;
    return HwRxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 PiForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;
    uint32 regAddr = OcnStsPointerInterpreterPerChannelControl((AtSdhPath)self) + DefaultOffset((AtSdhPath)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_spiramctl_StsPiAisFrc_Mask)
        forcedAlarms |= cAtSdhPathAlarmAis;

    return forcedAlarms;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;
    ThaModuleOcn ocnModule;

    /* PI is being forced */
    forcedAlarms = PiForcedAlarmGet(self);
    if (forcedAlarms & cAtSdhPathAlarmAis)
        return forcedAlarms;

    /* PI is not forced, just because the forcing point is now moved behind cross-connect */
    ocnModule = mThaOcnModule(self);
    if (ThaModuleOcnCanMovePathAisRxForcingPoint(ocnModule) &&
        ThaModuleOcnPathAisRxForcingPointIsMoved(ocnModule, (AtSdhAu)self))
        forcedAlarms |= cAtSdhPathAlarmAis;

    return forcedAlarms;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return (uint32)(cAtSdhPathAlarmAis);
    }

static void AllAlarmsUnforce(AtChannel self)
    {
	(void)AtChannelRxAlarmUnForce(self, AtChannelRxForcableAlarmsGet(self));
    }

static eAtRet AlarmForcingPointReset(AtChannel self)
    {
    ThaModuleOcn ocnModule = mThaOcnModule(self);

    if (ThaModuleOcnCanMovePathAisRxForcingPoint(ocnModule))
        return ThaModuleOcnPathAisRxForcingPointMove(ocnModule, (AtSdhAu)self, cAtFalse);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    AtSdhPath path;
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    path = (AtSdhPath)self;
    if (AtSdhPathHasPointerProcessor(path))
        ret = AtSdhPathSsInsertionEnable(path, cAtTrue);

    if (AtSdhPathHasPohProcessor((AtSdhPath)self))
        {
    	AllAlarmsUnforce(self);
    	AlarmForcingPointReset(self);
        }

    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    uint32 moduleId_ = moduleId;

    AtUnused(self);
    AtUnused(address);
    if (moduleId_ == cThaModuleOcn)
        return cAtTrue;
    return cAtFalse;
    }

static void AlarmDisplay(Tha60210011Tfi5LineAu self)
    {
    uint32 regAddr = Tha60210011ModuleOcnStsPointerGeneratorPerChannelInterrupt((Tha60210011ModuleOcn)mThaOcnModule(self), (AtSdhChannel)self) + DefaultOffset((AtSdhPath)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    AtPrintc(cSevInfo, "\n\rSticky Alarm of Tx-Path: ");

    if (regVal & cAf6_stspgstkram_StsPgAisIntr_Mask)
        AtPrintc(cSevCritical, "AIS ");
    if (regVal & cAf6_stspgstkram_StsPgFiFoOvfIntr_Mask)
        AtPrintc(cSevCritical, "FiFoOvf ");

    AtPrintc(cSevCritical, "\r\n");

    /* Only clear non-public events */
    regVal = cAf6_stspgstkram_StsPgAisIntr_Mask | cAf6_stspgstkram_StsPgFiFoOvfIntr_Mask;
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    }

static eAtRet Debug(AtChannel self)
	{
    m_AtChannelMethods->Debug(self);
    mMethodsGet(mThis(self))->AlarmDisplay(mThis(self));
    return cAtOk;
	}

static eBool CanConcate(Tha60210011Tfi5LineAu self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    uint8 slave_i;
    uint8 lineId = AtSdhChannelLineGet((AtSdhChannel) self);

    for (slave_i = 0; slave_i < numSlaves; slave_i++)
        {
        /* Master and slave channels must belong to a single line. */
        if (AtSdhChannelLineGet(slaveList[slave_i]) != lineId)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet ConcateCheck(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
        {
    eAtRet ret = m_AtSdhChannelMethods->ConcateCheck(self, slaveList, numSlaves);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(mThis(self))->CanConcate(mThis(self), slaveList, numSlaves))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    if (AtSdhPathHasPointerProcessor((AtSdhPath)self))
        {
        uint32 address = OcnStsPointerInterpreterPerChannelControl((AtSdhPath)self) + DefaultOffset((AtSdhPath)self);
        uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);
        mRegFieldSet(regVal, cAf6_spiramctl_StsPiAdjRule_, 0); /* 8 of 10 only */
        mChannelHwWrite(self, address, regVal, cThaModuleOcn);
        }

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    if (squelched)
        return HwTxAlarmForce(self, cAtSdhPathAlarmAis, cAtTrue);

    return HwTxAlarmForce(self, cAtSdhPathAlarmAis, mThis(self)->txAisExplicitlyForced);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011Tfi5LineAu *object = (tTha60210011Tfi5LineAu *)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txAisExplicitlyForced);
    }

static void OverrideAtObject(AtSdhAu self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(sdhChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, ConcateCheck);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, HoldOnTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        }

    mMethodsSet(sdhChannel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, TxSsGet);
        mMethodOverride(m_AtSdhPathOverride, TxSsSet);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareEnable);
        mMethodOverride(m_AtSdhPathOverride, SsCompareIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, InterruptProcess);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxSquelch);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtObject(self);
    OverrideAtSdhPath(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(AtSdhAu self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, AlarmDisplay);
        mMethodOverride(m_methods, CanConcate);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5LineAu);
    }

AtSdhAu Tha60210011Tfi5LineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha60210011Tfi5LineAuNew(uint32 auId, uint8 auType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineAuObjectInit(self, auId, auType, module);
    }

uint32 Tha60210011Tfi5LineAuPiForcedAlarmGet(AtSdhAu self)
    {
    if (self)
        return PiForcedAlarmGet((AtChannel)self);
    return 0;
    }

eAtRet Tha60210011Tfi5LineAuPiAlarmForce(AtSdhAu self, uint32 alarmType)
    {
    if (self)
        return PiAlarmForce((AtChannel)self, alarmType, cAtTrue);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210011Tfi5LineAuPiAlarmUnForce(AtSdhAu self, uint32 alarmType)
    {
    if (self)
        return PiAlarmForce((AtChannel)self, alarmType, cAtFalse);
    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineVc1x.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 TFI-5 VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../poh/Tha60210011PohProcessor.h"
#include "Tha60210011Tfi5LineAuVcInternal.h"
#include "Tha60210011Tfi5LineVc1xInternal.h"
#include "Tha60210011ModuleSdh.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210011Tfi5LineVc1x)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhVcMethods  m_ThaSdhVcOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tThaSdhVcMethods  *m_ThaSdhVcMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasOcnEpar(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PohDefaultSet(ThaSdhVc self)
    {
    eAtRet ret = m_ThaSdhVcMethods->PohDefaultSet(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize this byte due to use the same buffer for many SDH path layers */
    return AtSdhChannelTxOverheadByteSet((AtSdhChannel)self, cAtSdhPathOverheadByteN2, 0);
    }

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isPosAdj)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isPosAdj) ? 0 : 1;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(16384UL * slice + 2048UL * adjMode + 32UL * hwStsInSlice + 4UL * vtgId + vtId + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtChannel)self)));

    return cBit31_0;
    }

static uint32 AdjcntperstsramBase(ThaSdhVc self)
    {
    return Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAdjustCount((Tha60210011ModuleOcn)ModuleOcn((AtChannel)self), (AtSdhChannel)self);
    }

static uint32 AdjcntpgperstsramBase(ThaSdhVc self)
    {
    return Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelAdjustCount((Tha60210011ModuleOcn)ModuleOcn((AtChannel)self), (AtSdhChannel)self);
    }

static eBool PointerCounterGet(ThaSdhVc self, uint16 counterType, uint32 *counter)
    {
    ThaSdhVc vc = (ThaSdhVc)self;
    eBool isOcnCounter = cAtTrue;
    uint32 ppOffset = mMethodsGet(vc)->OcnPJCounterOffset(vc, cAtTrue);
    uint32 npOffset = mMethodsGet(vc)->OcnPJCounterOffset(vc, cAtFalse);

    if (counterType == cAtSdhPathCounterTypeRxPPJC)
        *counter = mChannelHwRead(self, AdjcntperstsramBase(vc) + ppOffset, cThaModuleOcn);
    else if (counterType == cAtSdhPathCounterTypeRxNPJC)
        *counter = mChannelHwRead(self, AdjcntperstsramBase(vc) + npOffset, cThaModuleOcn);
    else if (counterType == cAtSdhPathCounterTypeTxPPJC)
        *counter = mChannelHwRead(self, AdjcntpgperstsramBase(vc) + ppOffset, cThaModuleOcn);
    else if (counterType == cAtSdhPathCounterTypeTxNPJC)
        *counter = mChannelHwRead(self, AdjcntpgperstsramBase(vc) + npOffset, cThaModuleOcn);
    else
        isOcnCounter = cAtFalse;

    return isOcnCounter;
    }

static ThaSdhPointerAccumulator *PointerAccumulatorAddress(ThaSdhVc self)
    {
    return &(mThis(self)->pointerAccumulator);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    uint32 counterVal;
    ThaSdhVc vc1x = (ThaSdhVc)self;
    eBool isPointerCounter = PointerCounterGet(vc1x, counterType, &counterVal);
    if (isPointerCounter)
        return ThaSdhVcPointerCounterUpdateThenClear((ThaSdhVc)self, counterType, counterVal, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    uint32 counterVal;
    ThaSdhVc vc1x = (ThaSdhVc)self;
    eBool isPointerCounter = PointerCounterGet(vc1x, counterType, &counterVal);
    if (isPointerCounter)
        return ThaSdhVcPointerCounterUpdateThenClear((ThaSdhVc)self, counterType, counterVal, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    return Tha6021OcnTuVcLoopbackIsSupported(self, loopbackMode);
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return Tha6021OcnTuVcRxAlarmForce(self, alarmType);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return Tha6021OcnTuVcRxAlarmUnForce(self, alarmType);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    return Tha6021OcnTuVcRxForcedAlarmGet(self);
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    return Tha6021OcnTuVcRxForcibleAlarms(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011Tfi5LineVc1x object = (Tha60210011Tfi5LineVc1x)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(pointerAccumulator);
    }

static ThaModuleOcn OcnModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    return ThaModuleOcnNotifyLoVcAisToPohReset(OcnModule(self), (AtSdhVc)self);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(thaVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, m_ThaSdhVcMethods, sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, HasOcnEpar);
        mMethodOverride(m_ThaSdhVcOverride, PohDefaultSet);
        mMethodOverride(m_ThaSdhVcOverride, PointerAccumulatorAddress);
        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5LineVc1x);
    }

AtSdhVc Tha60210011Tfi5LineVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhVc1xObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210011Tfi5LineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineVc1xObjectInit(self, channelId, channelType, module);
    }

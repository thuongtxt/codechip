/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011ModuleSdh.c
 *
 * Created Date: Jun 12, 2014
 *
 * Description : PWCodechip-60210011 SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhAug.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../Tha60150011/sdh/Tha60150011ModuleSdh.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../cdr/Tha60210011ModuleCdr.h"
#include "../xc/Tha60210011ModuleXc.h"
#include "../physical/Tha6021Physical.h"
#include "../man/Tha60210011DeviceReg.h"
#include "../poh/Tha60210011PohReg.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../physical/Tha60210011Tfi5SerdesControllerReg.h"
#include "Tha60210011ModuleSdhInternal.h"
#include "Tha60210011ModuleSdhAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaNumbSts1InSts24         24
#define cAf6_sts1_in_sts24_Mask(id) (cBit0 << (id))
#define cAf6_vt_in_vt28_Mask(id)    (cBit0 << (id))
#define cAf6_slice_Mask(id)         (cBit0 << (id))
#define cAf6_slice_Shift(id)        ((id))

#define cRegTxResyncReg         0x100001
#define cRegTxResyncMask        cBit1
#define cRegTxResyncShift       1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleSdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleSdhMethods m_methods;

/* Override */
static tThaModuleSdhMethods m_ThaModuleSdhOverride;
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tAtObjectMethods     m_AtObjectOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210011Tfi5SerdesControllerNew(line, serdesId);
    }

static eBool HasLedEngine(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasCounters(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaModuleOcn OcnModule(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static void AlarmDownstreamEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModuleOcn ocnModule = OcnModule(self);
    Tha60210011ModuleOcnAisDownstreamEnable(ocnModule, enable);
    Tha60210011ModuleOcnLopDownstreamEnable(ocnModule, enable);
    }

static eBool HasLineInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_global_interrupt_status_TFI5IntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static eBool HasStsInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_global_interrupt_status_STSIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static eBool HasVtInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_global_interrupt_status_VTIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static eBool BerHardwareInterruptIsSupported(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StsInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    uint32 pohIntr = AtHalRead(hal, Tha60210011ModulePohBaseAddress(modulePoh) + cAf6Reg_alm_glbchgo);
    uint32 maxSlices = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh) / 2;
    uint32 evenIntr = pohIntr & cBit7_0;
    uint32 oddIntr = (pohIntr >> 8) & cBit7_0;
    uint32 slice;
    uint32 stsIntr = 0;
    AtUnused(self);
    AtUnused(glbIntr);

    for (slice = 0; slice < maxSlices; slice++)
        {
        uint32 sliceIntr;

        mFieldGet(evenIntr, cAf6_slice_Mask(slice), cAf6_slice_Shift(slice), uint32, &sliceIntr);
        stsIntr |= sliceIntr << (slice * 2);
        mFieldGet(oddIntr, cAf6_slice_Mask(slice), cAf6_slice_Shift(slice), uint32, &sliceIntr);
        stsIntr |= sliceIntr << (slice * 2 + 1);
        }
    return stsIntr;
    }

static uint32 VtInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    uint32 pohIntr = AtHalRead(hal, Tha60210011ModulePohBaseAddress(modulePoh) + cAf6Reg_alm_glbchgo);
    uint32 maxSlices = Tha60210011ModulePohNumberVtSlices((Tha60210011ModulePoh)modulePoh) / 2;
    uint32 evenIntr = (pohIntr >> 16) & cBit5_0;
    uint32 oddIntr = (pohIntr >> 22) & cBit5_0;
    uint32 slice;
    uint32 vtIntr = 0;
    AtUnused(self);
    AtUnused(glbIntr);

    for (slice = 0 ;slice < maxSlices; slice++)
        {
        uint32 sliceIntr;

        mFieldGet(evenIntr, cAf6_slice_Mask(slice), cAf6_slice_Shift(slice), uint32, &sliceIntr);
        vtIntr |= sliceIntr << (slice * 2);
        mFieldGet(oddIntr, cAf6_slice_Mask(slice), cAf6_slice_Shift(slice), uint32, &sliceIntr);
        vtIntr |= sliceIntr << (slice * 2 + 1);
        }
    return vtIntr;
    }

static eBool LineSerdesPrbsIsSupported(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(serdes);
    return cAtTrue;
    }

static AtPrbsEngine LineSerdesPrbsEngineCreate(ThaModuleSdh self, AtSdhLine line, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    AtUnused(line);
    return Tha60210011PrbsEngineSerdesTfi5New(serdes);
    }

static eAtRet CommonRegisterOffsetsDisplay(ThaModuleSdh self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);

    /* TODO: provide register offset with hardware register. */
    return cAtOk;
    }

static uint32 MaxLineRate(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm16;
    }

static eAtSdhLineRate LineDefaultRate(ThaModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtSdhLineRateStm16;
    }

static uint8 MaxNumHoLines(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 Tfi5LineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 Tfi5LineStopId(AtModuleSdh self)
    {
    return (uint8)(AtModuleSdhNumTfi5Lines(self) - 1);
    }

static uint8 MaxNumLoLines(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return 6;
    }

static uint8 HoLineStopId(AtModuleSdh self)
    {
    return (uint8)(Tha60210011ModuleSdhHoLineStartId(self) + mMethodsGet(mThis(self))->MaxNumHoLines(mThis(self)) - 1);
    }

static uint8 LoLineStopId(AtModuleSdh self)
    {
    return (uint8)(Tha60210011ModuleSdhLoLineStartId(self) + mMethodsGet(mThis(self))->MaxNumLoLines(mThis(self)) - 1);
    }

static AtSdhLine LineCreate(AtModuleSdh self, uint8 lineId)
    {
    return Tha60210011Tfi5LineNew(lineId, self);
    }

static AtSdhAug AugCreate(AtModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    AtUnused(lineId);
    return Tha60210011Tfi5LineAugNew(channelId, channelType, self);
    }

static AtSdhAu AuCreate(AtModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    AtUnused(lineId);
    return Tha60210011Tfi5LineAuNew(channelId, channelType, self);
    }

static eAtRet HoVcSubChannelHwStsAssignByParrentHwSts(AtSdhChannel parent, AtSdhChannel subChannel)
    {
    uint16 *allSubChannelHwSts, *allVcSts;
    eAtSdhChannelType parentType;
    uint32 subChannelId;

    /* Sub Channel need to know it parent now because it's number of STS depends on it's parent */
    SdhChannelParentSet(subChannel, parent);

    parentType = AtSdhChannelTypeGet(parent);
    allSubChannelHwSts = AtSdhChannelAllHwSts(subChannel);
    allVcSts = AtSdhChannelAllHwSts(parent);

    if (allSubChannelHwSts == NULL || allVcSts == NULL)
        return cAtErrorRsrcNoAvail;

    if (parentType == cAtSdhChannelTypeVc4)
        {
        subChannelId = AtChannelIdGet((AtChannel)subChannel);
        if (subChannelId >= AtSdhChannelNumSts(parent))
            return cAtErrorRsrcNoAvail;

        allSubChannelHwSts[0] = allVcSts[subChannelId];
        return cAtOk;
        }

    allSubChannelHwSts[0] = allVcSts[0];
    return cAtOk;
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210011Tfi5LineTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel AuVcObjectCreate(Tha60210011ModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    AtUnused(lineId);
    return (AtSdhChannel)Tha60210011Tfi5LineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel VcCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
        {
        AtSdhChannel newVc = mMethodsGet(mThis(self))->Tu3VcObjectCreate(mThis(self), channelType, channelId);
        return Tha60210011ModuleSdhHoVcSubChannelHwStsUpdate(parent, newVc);
        }

    return mMethodsGet(mThis(self))->AuVcObjectCreate(mThis(self), lineId, channelType, channelId);
    }

static AtSdhChannel TuObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210011Tfi5LineTuNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel TuCreate(AtModuleSdh self, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    AtSdhChannel newTu = mMethodsGet(mThis(self))->TuObjectCreate(mThis(self), channelType, channelId);
    return Tha60210011ModuleSdhHoVcSubChannelHwStsUpdate(parent, newTu);
    }

static AtSdhChannel TugObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210011Tfi5LineTugNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel TugCreate(AtModuleSdh self, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    AtSdhChannel newTug = mMethodsGet(mThis(self))->TugObjectCreate(mThis(self), channelType, channelId);
    return Tha60210011ModuleSdhHoVcSubChannelHwStsUpdate(parent, newTug);
    }

static AtSdhChannel Vc1xCreate(AtModuleSdh self, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    AtSdhChannel newVc = mMethodsGet(mThis(self))->Vc1xObjectCreate(mThis(self), channelType, channelId);
    return Tha60210011ModuleSdhHoVcSubChannelHwStsUpdate(parent, newVc);
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210011Tfi5LineVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    switch (channelType)
        {
        case cAtSdhChannelTypeLine:
            return (AtSdhChannel)LineCreate(self, channelId);

        /* Create AUG */
        case cAtSdhChannelTypeAug16:
        case cAtSdhChannelTypeAug4:
        case cAtSdhChannelTypeAug1:
            return (AtSdhChannel)AugCreate(self, lineId, channelType, channelId);

        /* Create AU */
        case cAtSdhChannelTypeAu4_16c:
        case cAtSdhChannelTypeAu4_4c:
        case cAtSdhChannelTypeAu4:
        case cAtSdhChannelTypeAu3:
            return (AtSdhChannel)AuCreate(self, lineId, channelType, channelId);

        /* Create VC */
        case cAtSdhChannelTypeVc4_16c:
        case cAtSdhChannelTypeVc4_4c:
        case cAtSdhChannelTypeVc4:
        case cAtSdhChannelTypeVc3:
            return VcCreate(self, lineId, parent, channelType, channelId);

        case cAtSdhChannelTypeTug3:
        case cAtSdhChannelTypeTug2:
            return TugCreate(self, parent, channelType, channelId);

        case cAtSdhChannelTypeTu3:
        case cAtSdhChannelTypeTu12:
        case cAtSdhChannelTypeTu11:
            return TuCreate(self, parent, channelType, channelId);

        case cAtSdhChannelTypeVc12:
        case cAtSdhChannelTypeVc11:
            return Vc1xCreate(self, parent, channelType, channelId);

        default:
            return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
        }
    }

static eAtRet Resync(AtModule self)
    {
    uint32 regVal;

    regVal = mModuleHwRead(self, cRegTxResyncReg);
    mFieldIns(&regVal, cRegTxResyncMask, cRegTxResyncShift, 1);
    mModuleHwWrite(self, cRegTxResyncReg, regVal);

    regVal = mModuleHwRead(self, cRegTxResyncReg);
    mFieldIns(&regVal, cRegTxResyncMask, cRegTxResyncShift, 0);
    mModuleHwWrite(self, cRegTxResyncReg, regVal);

    return cAtOk;
    }

static eAtRet AllLinesLoopbackSet(AtModule self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    uint8 line_i;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine sdhLine = AtModuleSdhLineGet(sdhModule, line_i);
        AtSerdesController serdes = AtSdhLineSerdesController(sdhLine);
        ret |= AtSerdesControllerLoopbackSet(serdes, loopbackMode);
        }

    return ret;
    }

static eAtRet LoopbackSet(AtModule self, uint8 loopbackMode)
    {
	eAtRet ret;

	ret = AllLinesLoopbackSet(self, loopbackMode);
	if (ret != cAtOk)
	    return ret;

    return Resync(self);
    }

static uint8 LoopbackGet(AtModule self)
    {
	AtSdhLine sdhLine = AtModuleSdhLineGet((AtModuleSdh)self, 0);
	AtSerdesController serdes = AtSdhLineSerdesController(sdhLine);
	return AtSerdesControllerLoopbackGet(serdes);
    }

static eBool LoopbackIsSupported(AtModule self, uint8 loopbackMode)
    {
    AtUnused(self);

    switch (loopbackMode)
    	{
    	case cAtLoopbackModeRelease: 	return cAtTrue;
    	case cAtLoopbackModeLocal:   	return cAtTrue;
    	case cAtLoopbackModeRemote:  	return cAtTrue;
    	default: 						return cAtFalse;
    	}
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = AtModuleLoopbackShow(self);
    ThaModuleSdhDebugCounterModuleShow((ThaModuleSdh)self);
    return ret;
    }

static void LineInterruptProcess(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    }

static void StsInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    const uint32 intrBase = Tha60210011ModulePohBaseAddress(modulePoh);
    uint32 numbSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint8 slice, sts1;

    for (slice = 0; slice < numbSlice; slice++)
        {
        if (pohIntr & cAf6_slice_Mask(slice))
            {
            /* Get STS24 OR interrupt status */
            uint32 intrSts24Val = AtHalRead(hal, intrBase + (uint32)cAf6Reg_alm_glbchghi(slice));
            uint32 intrSts24Mask = AtHalRead(hal, intrBase + (uint32)cAf6Reg_alm_glbmskhi(slice));

            intrSts24Val &= intrSts24Mask;
            for (sts1 = 0; sts1 < cThaNumbSts1InSts24; sts1++)
                {
                if (intrSts24Val & cAf6_sts1_in_sts24_Mask(sts1))
                    {
                    AtSdhPath auPath;

                    /* Get AU path from hardware ID */
                    auPath = ThaModuleSdhAuPathFromHwIdGet((ThaModuleSdh)self, cAtModuleSdh, slice, sts1);

                    /* Execute path interrupt process */
                    AtSdhPathInterruptProcess(auPath, slice, sts1, 0, hal);
                    }
                }
            }
        }
    }

static void VtInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    uint32 intrBase = Tha60210011ModulePohBaseAddress(modulePoh);
    uint32 numbSlice = Tha60210011ModulePohNumberVtSlices((Tha60210011ModulePoh)modulePoh);
    uint8 slice, sts1;

    for (slice = 0; slice < numbSlice; slice++)
        {
        if (pohIntr & cAf6_slice_Mask(slice))
            {
            /* Get STS24 OR interrupt status */
            uint32 intrSts24Val = AtHalRead(hal, intrBase + (uint32)cAf6Reg_alm_glbchglo(slice));
            uint32 intrSts24Mask = AtHalRead(hal, intrBase + (uint32)cAf6Reg_alm_glbmsklo(slice));

            intrSts24Val &= intrSts24Mask;
            for (sts1 = 0; sts1 < cThaNumbSts1InSts24; sts1++)
                {
                if (intrSts24Val & cAf6_sts1_in_sts24_Mask(sts1))
                    {
                    uint8 vtg, vt, vt28 = 0;
                    uint32 intrVt28Val = AtHalRead(hal, intrBase + (uint32)cAf6Reg_alm_orstalo(sts1, slice));

                    for (vtg = 0; vtg < 7; vtg++)
                        {
                        for (vt = 0; vt < 4; vt++)
                            {
                            if (intrVt28Val & cAf6_vt_in_vt28_Mask(vt28))
                                {
                                AtSdhPath tuPath;

                                /* Get TU path from hardware ID */
                                tuPath = ThaModuleSdhTuPathFromHwIdGet((ThaModuleSdh)self, cAtModuleSdh, slice, sts1, vtg, vt);

                                /* Execute path interrupt process */
                                AtSdhPathInterruptProcess(tuPath, slice, sts1, vt28, hal);
                                }
                            vt28++;
                            }
                        }
                    }
                }
            }
        }
    }

static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static AtSdhLine Tfi5LineGet(AtModuleSdh self, uint8 tfi5LineId)
    {
    if (tfi5LineId >= AtModuleSdhNumTfi5Lines(self))
        return NULL;
    return AtModuleSdhLineGet(self, tfi5LineId);
    }

static uint8 NumLinesPerPart(ThaModuleSdh self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)self;
    return (uint8)(AtModuleSdhNumTfi5Lines(moduleSdh) + mMethodsGet(mThis(self))->MaxNumHoLines(mThis(self)) + mMethodsGet(mThis(self))->MaxNumLoLines(mThis(self)));
    }

static eBool HasChannelStatus(AtModuleSdh self)
    {
    /* All of status of channels are moved to PM module, but that module may be
     * not available for some reasons. So this method is used to determine whether
     * channel alarm status can be retrieved directly from debug registers */
    AtUnused(self);
    return cAtTrue;
    }

static eBool PohIsReady(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasClockOutput(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    return (uint8)AtModuleSdhNumTfi5Lines(self);
    }

static uint8 PartOfLine(ThaModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    AtUnused(lineId);
    return 0;
    }

AtSdhChannel Tha60210011ModuleSdhAuFromHwStsGet(AtSdhLine line, uint8 hwSts)
    {
    AtSdhChannel aug16;

    /* Line only support rate STM16 */
    if (AtSdhLineRateGet(line) != cAtSdhLineRateStm16)
        return NULL;

    aug16 = (AtSdhChannel)AtSdhLineAug16Get(line, 0);
    if (AtSdhChannelMapTypeGet(aug16) == cAtSdhAugMapTypeAug16MapVc4_16c)
        return AtSdhChannelSubChannelGet(aug16, 0);
    else
        {
        uint8 aug4Id = (uint8)((hwSts % 16) / 4);
        AtSdhChannel aug4 = (AtSdhChannel)AtSdhLineAug4Get(line, aug4Id);
        if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
            return AtSdhChannelSubChannelGet(aug4, 0);

        else if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4Map4xAug1s)
            {
            uint8 aug1Id = (uint8)(hwSts % 16);
            AtSdhChannel aug1 = (AtSdhChannel)AtSdhLineAug1Get(line, aug1Id);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
                return AtSdhChannelSubChannelGet(aug1, 0);

            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
                return AtSdhChannelSubChannelGet(aug1, (uint8)(hwSts / 16));
            }
        }

    return NULL;
    }

static AtModuleXc ModuleXc(AtModuleSdh self)
    {
    return (AtModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleXc);
    }

static AtSdhPath AuPathFromOcnPpHwIdGet(ThaModuleSdh self, uint8 ocnPpSliceId, uint8 ocnPpStsId)
    {
    AtSdhLine sdhLine;
    uint8 lineId, tfi5StsId;

    Tha60210011ModuleXcTfi5StsFromOcnStsGet(ModuleXc((AtModuleSdh)self), ocnPpSliceId, ocnPpStsId, &lineId, &tfi5StsId);
    sdhLine = AtModuleSdhLineGet((AtModuleSdh)self, lineId);
    if (sdhLine == NULL)
        return NULL;

    return (AtSdhPath)Tha60210011ModuleSdhAuFromHwStsGet(sdhLine, tfi5StsId);
    }

static AtSdhPath AuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId)
    {
    uint8 ocnPpSlice, ocnPpSts;
    AtUnused(phyModule);

    /* Convert interrupt ID to OCN PP ID */
    ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, stsId, &ocnPpSlice, &ocnPpSts);

    return AuPathFromOcnPpHwIdGet(self, ocnPpSlice, ocnPpSts);
    }

static eBool SliceAndHwStsIsValid(uint8 slice, uint8 hwSts)
    {
    if ((slice == cInvalidSlice) || (hwSts == cInvalidHwSts))
        return cAtFalse;
    return cAtTrue;
    }

static AtSdhPath TuPathFromLoSliceHwIdGet(ThaModuleSdh self, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 hwStsInLine;
    AtSdhChannel au;
    AtSdhLine line = Tha60210011ModuleSdhLineFromLoLineStsGet((AtModuleSdh)self, sliceId, stsId, &hwStsInLine);
    if (line == NULL)
        return NULL;

    au = Tha60210011ModuleSdhAuFromHwStsGet(line, hwStsInLine);
    return (AtSdhPath)Tha60210011SdhTuPathFromAuPathHwStsGet((AtSdhChannel)au, hwStsInLine, vtgId, vtId);
    }

AtSdhChannel Tha60210011SdhTuPathFromAuPathHwStsGet(AtSdhChannel au, uint8 hwStsInLine, uint8 vtgId, uint8 vtId)
    {
    AtSdhChannel vc;

    if (au == NULL)
        return NULL;

    vc = AtSdhChannelSubChannelGet(au, 0);
    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelMapTypeGet(vc) != cAtSdhVcMapTypeVc3Map7xTug2s)
           return NULL;

        return AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(vc, vtgId), vtId);
        }

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4)
        {
        AtSdhChannel tug3;
        if (AtSdhChannelMapTypeGet(vc) != cAtSdhVcMapTypeVc4Map3xTug3s)
           return NULL;

        tug3 = AtSdhChannelSubChannelGet(vc, hwStsInLine / 16);
        if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
           return AtSdhChannelSubChannelGet(tug3, 0);

        if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3Map7xTug2s)
           return AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug3, vtgId), vtId);
        }

    return NULL;
    }

static AtSdhPath TuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 loSlice, loStsInSlice;
    AtUnused(phyModule);

    /* Convert interrupt ID to Lo-slice ID */
    ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, stsId, &loSlice, &loStsInSlice);

    return TuPathFromLoSliceHwIdGet(self, loSlice, loStsInSlice, vtgId, vtId);
    }

static uint8 HoLineStartId(Tha60210011ModuleSdh self)
    {
    return (uint8)(Tfi5LineStopId((AtModuleSdh)self) + 1);
    }

static uint8 LoLineStartId(Tha60210011ModuleSdh self)
    {
    return (uint8)(HoLineStopId((AtModuleSdh)self) + 1);
    }

static tTha60210011LoLineDatabase* AllLoLineDbGet(AtModuleSdh self)
    {
    uint32 size = Tha60210011ModuleSdhMaxNumLoLines(self) * sizeof(tTha60210011LoLineDatabase);

    if (mThis(self)->allLoLineDb == NULL)
        {
        mThis(self)->allLoLineDb = AtOsalMemAlloc(size);
        if (mThis(self)->allLoLineDb)
            AtOsalMemInit(mThis(self)->allLoLineDb, 0, size);
        }

    return mThis(self)->allLoLineDb;
    }

static tTha60210011LoLineDatabase* LoLineDb(AtModuleSdh self, uint8 lineId)
    {
    tTha60210011LoLineDatabase* allLoLineDb = AllLoLineDbGet(self);
    if (allLoLineDb == NULL)
        return NULL;

    if (lineId >= Tha60210011ModuleSdhMaxNumLoLines(self))
        return NULL;

    return allLoLineDb + lineId;
    }

static eAtRet FreeStsGet(AtModuleSdh self, uint8 numRequireSts, uint8* freeSts, uint8 *loLineId)
    {
    uint8 line_i, sts_i, numSts = 0;

    for (line_i = 0; line_i < Tha60210011ModuleSdhMaxNumLoLines(self); line_i++)
        {
        tTha60210011LoLineDatabase* lineDb = LoLineDb(self, line_i);
        if (lineDb == NULL)
            break;

        for (sts_i = 0; sts_i < cNumStsInLoLine; sts_i++)
            {
            if (lineDb->stsIsInUsed[sts_i])
                continue;

            if (freeSts)
                freeSts[numSts] = sts_i;

            numSts++;
            if (numSts < numRequireSts)
                continue;

            if (loLineId)
                *loLineId = line_i;

            return cAtOk;
            }

        /* Finish in one line but not find enough STS, need to restart searching because current FPGA does not support
         * concatenate STS of multiple lines */
        numSts = 0;
        }

    return cAtErrorRsrcNoAvail;
    }

static void DeleteAllLoLineDb(tTha60210011LoLineDatabase* db)
    {
    if (db == NULL)
        return;

    AtOsalMemFree(db);
    }

static void Delete(AtObject self)
    {
    tTha60210011LoLineDatabase* db = mThis(self)->allLoLineDb;
    m_AtObjectMethods->Delete(self);
    DeleteAllLoLineDb(db);
    }

static void LoLineDbInit(AtModule self)
    {
    uint32 size = Tha60210011ModuleSdhMaxNumLoLines((AtModuleSdh)self) * sizeof(tTha60210011LoLineDatabase);
    if (mThis(self)->allLoLineDb)
        AtOsalMemInit(mThis(self)->allLoLineDb, 0, size);
    }

static eAtRet PohMonitorDefaultSet(Tha60210011ModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    uint32 regAddr = cAf6RegLinePohEnableBase + Tha60210011ModulePohBaseAddress(pohModule);
    uint32 regVal = 0;
    uint32 lineId;

    for (lineId = 0; lineId < Tha60210011ModuleSdhMaxNumHoLines((AtModuleSdh)self); lineId++)
        {
        mFieldIns(&regVal,
                  cAf6_HoLinePohEnableMask(lineId),
                  cAf6_HoLinePohEnableShift(lineId),
                  1);
        }

    for (lineId = 0; lineId < Tha60210011ModuleSdhMaxNumLoLines((AtModuleSdh)self); lineId++)
        {
        mFieldIns(&regVal,
                  cAf6_LoLinePohEnableMask(lineId),
                  cAf6_LoLinePohEnableShift(lineId),
                  1);
        }

    mModuleHwWrite(pohModule, regAddr, regVal);

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    LoLineDbInit(self);
    mMethodsGet(mThis(self))->PohMonitorDefaultSet(mThis(self));

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Tha60210011ModuleSdhAsyncInit(self);
    }

static void SerializeAllLoLineDb(AtObject self, AtCoder encoder)
    {
    Tha60210011ModuleSdh object =  mThis(self);
    uint32 numLoLines = Tha60210011ModuleSdhMaxNumLoLines((AtModuleSdh)self);
    uint32 loLine_i;

    if (object->allLoLineDb == NULL)
        {
        AtCoderEncodeString(encoder, "none", "allLoLineDb");
        return;
        }

    for (loLine_i = 0; loLine_i < numLoLines; loLine_i++)
        {
        char key[32];

        tTha60210011LoLineDatabase *loLineDb = &(object->allLoLineDb[loLine_i]);
        AtSnprintf(key, sizeof(key) - 1, "allLoLineDb[%u]", loLine_i);
        AtCoderEncodeBoolArray(encoder, loLineDb->stsIsInUsed, cNumStsInLoLine, key);
        }
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011ModuleSdh object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    SerializeAllLoLineDb(self, encoder);
    mEncodeUInt(asyncInitState);

    /* Just to make the tool be happy */
    mEncodeNone(allLoLineDb); /* Serialized by SerializeAllLoLineDb */
    }

static eBool HasAps(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSdhLine LineFromLoLineStsGet(Tha60210011ModuleSdh self, uint8 loSlice, uint8 loHwStsInSlice, uint8 *stsInLine)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint8 ocnPpSlice, ocnPpSts, tfi5LineId;

    /* From lo-line domain to OCN PP domain */
    Tha60210011ModuleXcOcnStsFromLoLineStsGet(ModuleXc(sdhModule), loSlice, loHwStsInSlice, &ocnPpSlice, &ocnPpSts);
    if (!SliceAndHwStsIsValid(ocnPpSlice, ocnPpSts))
        return NULL;

    /* From OCN PP domain to Tfi5 domain */
    Tha60210011ModuleXcTfi5StsFromOcnStsGet(ModuleXc(sdhModule), ocnPpSlice, ocnPpSts, &tfi5LineId, stsInLine);

    return AtModuleSdhLineGet(sdhModule, tfi5LineId);
    }

static void OverrideAtObject(AtModuleSdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(AtSharedDriverOsalGet(), &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    return (uint8)AtChannelIdGet((AtChannel)line);
    }

static eAtRet HwStsPayloadInit(AtModuleSdh self, AtSdhChannel hoVc, uint8 slice, uint8 hwSts)
    {
    return Tha60210011ModuleOcnLoLineHwSts1PayloadInit(OcnModule((ThaModuleSdh)self), hoVc, slice, hwSts);
    }

static eAtSdhTtiMode DefaultTtiMode(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhTtiMode16Byte;
    }

static eBool AutoCrossConnect(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool LineHasFramer(AtModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtFalse;
    }

static uint32 SerdesPrbsBaseAddress(Tha60210011ModuleSdh self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAf6RegTfi5DiagBaseAddress;
    }

static eBool SdhChannelIsHoPath(Tha60210011ModuleSdh self, AtSdhChannel channel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtUnused(self);

    if ((channelType == cAtSdhChannelTypeLine)    ||
        (channelType == cAtSdhChannelTypeAug16)   ||
        (channelType == cAtSdhChannelTypeAug4)    ||
        (channelType == cAtSdhChannelTypeAug1)    ||
        (channelType == cAtSdhChannelTypeAu4_16c) ||
        (channelType == cAtSdhChannelTypeAu4_4c)  ||
        (channelType == cAtSdhChannelTypeAu4_nc)  ||
        (channelType == cAtSdhChannelTypeAu4)     ||
        (channelType == cAtSdhChannelTypeAu3))
        return cAtTrue;

    if (AtSdhChannelIsHoVc(channel) && !AtSdhChannelIsChannelizedVc(channel))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LineInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static ThaModulePoh ModulePoh(ThaModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static eAtRet StsInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint32 regAddr, regVal;
    uint32 slice;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk_glbmskhi_Mask) : (regVal & ~cAf6_alm_glbmsk_glbmskhi_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmskhi_glbmsk_Mask : 0x0;
    for (slice = 0; slice < numSlice; slice++)
        mModuleHwWrite(self, baseAddress + cAf6Reg_alm_glbmskhi(slice), regVal);

    return cAtOk;
    }

static eAtRet VtInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberVtSlices((Tha60210011ModulePoh)modulePoh);
    uint32 regAddr, regVal;
    uint32 slice;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk_glbmsklo_Mask) : (regVal & ~cAf6_alm_glbmsk_glbmsklo_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmsklo_glbmsk_Mask : 0x0;
    for (slice = 0; slice < numSlice; slice++)
        mModuleHwWrite(self, baseAddress + cAf6Reg_alm_glbmsklo(slice), regVal);

    return cAtOk;
    }

static eBool SdhVcBelongsToLoLine(Tha60210011ModuleSdh self, AtSdhChannel vc)
    {
    eAtSdhChannelType type = AtSdhChannelTypeGet(vc);

    AtUnused(self);
    if ((type == cAtSdhChannelTypeVc11) || (type == cAtSdhChannelTypeVc12))
        return cAtTrue;

    if (type == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc)) == cAtSdhChannelTypeTu3)
            return cAtTrue;

        if (AtSdhChannelMapTypeGet(vc) == cAtSdhVcMapTypeVc3MapDe3)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool ChannelConcateIsSupported(AtModuleSdh self, uint8 channelType, uint8 numChannels)
    {
    static const uint8 cNumSts3cInAug16 = 16;
    AtUnused(self);

    /* Only AU-4 concatenation is required by this product */
    if (channelType != cAtSdhChannelTypeAu4)
        return cAtFalse;

    if (numChannels > cNumSts3cInAug16)
        return cAtFalse;

    return cAtTrue;
    }

static eBool BitErrorCounterModeAsDefault(AtModuleSdh self)
    {
    /* Until error block mode is supported, should use bit mode only. */
    if (Tha60210011ModulePohBlockModeCountersIsSupported(ModulePoh((ThaModuleSdh)self)))
        return cAtFalse;

    return cAtTrue;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, LineDefaultRate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, HasLedEngine);
        mMethodOverride(m_ThaModuleSdhOverride, HasCounters);
        mMethodOverride(m_ThaModuleSdhOverride, AlarmDownstreamEnable);
        mMethodOverride(m_ThaModuleSdhOverride, HasLineInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, HasStsInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, HasVtInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, BerHardwareInterruptIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, LineSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, LineSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleSdhOverride, CommonRegisterOffsetsDisplay);
        mMethodOverride(m_ThaModuleSdhOverride, NumLinesPerPart);
        mMethodOverride(m_ThaModuleSdhOverride, PohIsReady);
        mMethodOverride(m_ThaModuleSdhOverride, HasClockOutput);
        mMethodOverride(m_ThaModuleSdhOverride, PartOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, AuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, TuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, HasAps);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptEnable);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        mMethodOverride(m_AtModuleSdhOverride, Tfi5LineGet);
        mMethodOverride(m_AtModuleSdhOverride, DefaultTtiMode);
        mMethodOverride(m_AtModuleSdhOverride, LineHasFramer);
        mMethodOverride(m_AtModuleSdhOverride, ChannelConcateIsSupported);
        mMethodOverride(m_AtModuleSdhOverride, BitErrorCounterModeAsDefault);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideAtModule(AtModuleSdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, LoopbackSet);
        mMethodOverride(m_AtModuleOverride, LoopbackGet);
        mMethodOverride(m_AtModuleOverride, LoopbackIsSupported);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh(self);
    OverrideAtModuleSdh(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MaxNumHoLines);
        mMethodOverride(m_methods, MaxNumLoLines);
        mMethodOverride(m_methods, HoLineStartId);
        mMethodOverride(m_methods, LoLineStartId);
        mMethodOverride(m_methods, PohMonitorDefaultSet);
        mMethodOverride(m_methods, AutoCrossConnect);
        mMethodOverride(m_methods, SerdesPrbsBaseAddress);
        mMethodOverride(m_methods, LineFromLoLineStsGet);
        mMethodOverride(m_methods, TugObjectCreate);
        mMethodOverride(m_methods, TuObjectCreate);
        mMethodOverride(m_methods, Tu3VcObjectCreate);
        mMethodOverride(m_methods, Vc1xObjectCreate);
        mMethodOverride(m_methods, AuVcObjectCreate);
        mMethodOverride(m_methods, SdhChannelIsHoPath);
        mMethodOverride(m_methods, SdhVcBelongsToLoLine);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleSdh);
    }

AtModuleSdh Tha60210011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60210011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleSdhObjectInit(newModule, device);
    }

eBool Tha60210011ModuleSdhIsTfi5Line(AtModuleSdh self, uint8 lineId)
    {
    if (self == NULL)
        return cAtFalse;

    if ((lineId >= Tfi5LineStartId(self)) &&
        (lineId <= Tfi5LineStopId(self)))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011ModuleSdhIsHoLine(AtModuleSdh self, uint8 lineId)
    {
    if (self == NULL)
        return cAtFalse;

    if ((lineId >= Tha60210011ModuleSdhHoLineStartId(self)) &&
        (lineId <= HoLineStopId(self)))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011ModuleSdhIsLoLine(AtModuleSdh self, uint8 lineId)
    {
    if (self == NULL)
        return cAtFalse;

    if ((lineId >= Tha60210011ModuleSdhLoLineStartId(self)) &&
        (lineId <= LoLineStopId(self)))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011ModuleSdhHasChannelStatus(AtModuleSdh self)
    {
    if (self)
        return HasChannelStatus(self);
    return cAtFalse;
    }

uint8 Tha60210011ModuleSdhHoLineStartId(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HoLineStartId(mThis(self));
    return 0;
    }

uint8 Tha60210011ModuleSdhLoLineStartId(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->LoLineStartId(mThis(self));
    return 0;
    }

uint8 Tha60210011ModuleSdhMaxNumHoLines(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->MaxNumHoLines(mThis(self));
    return 0;
    }

uint8 Tha60210011ModuleSdhMaxNumLoLines(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->MaxNumLoLines(mThis(self));
    return 0;
    }

eAtRet Tha60210011ModuleSdhLoLineStsAllocate(AtModuleSdh self, AtSdhChannel hoVc, uint8 *slice, uint8 *hwSts)
    {
    uint8 loLineId, freeSts;

    if (FreeStsGet(self, 1, &freeSts, &loLineId) != cAtOk)
        return cAtErrorRsrcNoAvail;

    *hwSts = freeSts;
    *slice = loLineId;
    Tha60210011ModuleSdhLoLineStsInUsedSet(self, loLineId, freeSts, cAtTrue);

    if (HwStsPayloadInit(self, hoVc, loLineId, freeSts) == cAtOk)
        return cAtOk;

    Tha60210011ModuleSdhLoLineStsDeallocate(self, loLineId, freeSts);
    return cAtError;
    }

eAtRet Tha60210011ModuleSdhLoLineStsDeallocate(AtModuleSdh self, uint8 slice, uint8 hwSts)
    {
    Tha60210011ModuleSdhLoLineStsInUsedSet(self, slice, hwSts, cAtFalse);
    return cAtOk;
    }

eBool Tha60210011ModuleSdhFreeStsIsAvailable(AtModuleSdh self, uint8 numSts)
    {
    return ((FreeStsGet(self, numSts, NULL, NULL) == cAtOk) ? cAtTrue : cAtFalse);
    }

eBool Tha60210011ModuleSdhLoLineStsIsInUsed(AtModuleSdh self, uint8 slice, uint8 hwSts)
    {
    tTha60210011LoLineDatabase* lineDb = LoLineDb(self, slice);
    if ((lineDb == NULL) || (hwSts >= cNumStsInLoLine))
        return cAtFalse;

    return lineDb->stsIsInUsed[hwSts];
    }

eBool Tha60210011ModuleSdhChannelIsHoPath(AtSdhChannel channel)
    {
    Tha60210011ModuleSdh moduleSdh;
    if (channel == NULL)
        return cAtFalse;

    moduleSdh = (Tha60210011ModuleSdh)AtChannelModuleGet((AtChannel)channel);
    if (moduleSdh)
        return mMethodsGet(moduleSdh)->SdhChannelIsHoPath(moduleSdh, channel);

    return cAtFalse;
    }

AtSdhLine Tha60210011ModuleSdhLineFromLoLineStsGet(AtModuleSdh self, uint8 loSlice, uint8 loHwStsInSlice, uint8 *stsInLine)
    {
    if (self)
        return mMethodsGet(mThis(self))->LineFromLoLineStsGet(mThis(self), loSlice, loHwStsInSlice, stsInLine);
    return NULL;
    }

uint32 Tha60210011ModuleSdhUpsrStsFromTfi5SwStsGet(uint8 lineId, uint8 stsId)
    {
    return (uint32)(lineId * 48 + stsId);
    }

eAtRet Tha60210011ModuleSdhLoLineStsInUsedSet(AtModuleSdh self, uint8 lineId, uint8 sts, eBool isInUsed)
    {
    tTha60210011LoLineDatabase* lineDb = LoLineDb(self, lineId);
    if (lineDb == NULL)
        return cAtErrorRsrcNoAvail;

    lineDb->stsIsInUsed[sts] = isInUsed;
    return cAtOk;
    }

AtSdhChannel Tha60210011ModuleSdhHoVcSubChannelHwStsUpdate(AtSdhChannel parent, AtSdhChannel vcSubChannel)
    {
    if (vcSubChannel == NULL)
        return NULL;

    if (HoVcSubChannelHwStsAssignByParrentHwSts(parent, vcSubChannel) != cAtOk)
        {
        AtObjectDelete((AtObject)vcSubChannel);
        return NULL;
        }

    return vcSubChannel;
    }

eBool Tha60210011ModuleSdhVcBelongsToLoLine(AtSdhChannel vc)
    {
    Tha60210011ModuleSdh moduleSdh;

    if (vc == NULL)
        return cAtFalse;

    moduleSdh = (Tha60210011ModuleSdh)AtChannelModuleGet((AtChannel)vc);
    if (moduleSdh)
        return mMethodsGet(moduleSdh)->SdhVcBelongsToLoLine(moduleSdh, vc);

    return cAtFalse;
    }

eBool Tha60210011ModuleSdhAutoCrossConnect(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->AutoCrossConnect(mThis(self));
    return cAtFalse;
    }

eAtRet Tha60210011ModuleSdhSuperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

void Tha60210011ModuleSdhLoLineDbInit(AtModule self)
    {
    LoLineDbInit(self);
    }
    
eAtRet Tha60210011ModuleSdhPohMonitorDefaultSet(AtModule self)
    {
    if (self)
        return mMethodsGet(mThis(self))->PohMonitorDefaultSet(mThis(self));
    return cAtErrorNullPointer;
    }

uint32 Tha60210011ModuleSdhSerdesPrbsBaseAddress(AtModuleSdh self, AtSerdesController serdes)
    {
    if (self)
        return mMethodsGet(mThis(self))->SerdesPrbsBaseAddress(mThis(self), serdes);
    return cInvalidUint32;
    }

eAtRet Tha60210011ModuleSdhUpsrSfAlarmMaskSet(AtModuleSdh self, uint32 alarmMask)
    {
    AtModule pohModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    return Tha60210011ModulePohUpsrSfAlarmMaskSet((Tha60210011ModulePoh)pohModule, alarmMask);
    }

uint32 Tha60210011ModuleSdhUpsrSfAlarmMaskGet(AtModuleSdh self)
    {
    AtModule pohModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    return Tha60210011ModulePohUpsrSfAlarmMaskGet((Tha60210011ModulePoh)pohModule);
    }

eAtRet Tha60210011ModuleSdhUpsrSdAlarmMaskSet(AtModuleSdh self, uint32 alarmMask)
    {
    AtModule pohModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    return Tha60210011ModulePohUpsrSdAlarmMaskSet((Tha60210011ModulePoh)pohModule, alarmMask);
    }

uint32 Tha60210011ModuleSdhUpsrSdAlarmMaskGet(AtModuleSdh self)
    {
    AtModule pohModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    return Tha60210011ModulePohUpsrSdAlarmMaskGet((Tha60210011ModulePoh)pohModule);
    }

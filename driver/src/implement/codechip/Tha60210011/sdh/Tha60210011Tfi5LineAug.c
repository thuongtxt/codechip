/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineAug.c
 *
 * Created Date: Apr 22, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 TFI-5 AUG.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60210011ModuleSdh.h"
#include "Tha60210011Tfi5LineAugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhAugMethods     m_AtSdhAugOverride;
static tThaSdhAugMethods    m_ThaSdhAugOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods = NULL;
static const tAtSdhChannelMethods   *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedPdhConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedMapConcate(ThaSdhAug self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    uint32 moduleId_ = moduleId;

    AtUnused(self);
    AtUnused(address);
    if (moduleId_ == cThaModuleOcn)
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideAtChannel(AtSdhAug self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static eBool NeedToRestoreSubMapping(AtSdhAug self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);

    /* Channel may be deleted when delete AU4-nc but still need to clean up here */
    if (numSubChannels == 0)
        SdhChannelSubChannelsCleanUp(channel);

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;

    ret = m_AtSdhChannelMethods->CanChangeMapping(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* Sub channel of AUG1 is in concatenation if AUG1 map VC4 but contain no sub channel */
    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAug1)
        {
        if (mapType != cAtSdhAugMapTypeAug1MapVc4)
            return cAtOk;

        if (AtSdhChannelNumberOfSubChannelsGet(self) == 0)
            return cAtErrorChannelBusy;

        return cAtOk;
        }

    return ret;
    }

static void OverrideAtObject(AtSdhAug self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhAug(AtSdhAug self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhAugOverride, mMethodsGet(self), sizeof(tAtSdhAugMethods));
        mMethodOverride(m_AtSdhAugOverride, NeedToRestoreSubMapping);
        }

    mMethodsSet(self, &m_AtSdhAugOverride);
    }

static void OverrideAtSdhChannel(AtSdhAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideThaSdhAug(AtSdhAug self)
    {
    ThaSdhAug aug = (ThaSdhAug)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAugOverride, mMethodsGet(aug), sizeof(m_ThaSdhAugOverride));

        mMethodOverride(m_ThaSdhAugOverride, NeedPdhConcate);
        mMethodOverride(m_ThaSdhAugOverride, NeedMapConcate);
        }

    mMethodsSet(aug, &m_ThaSdhAugOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideAtObject(self);
    OverrideThaSdhAug(self);
    OverrideAtSdhAug(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5LineAug);
    }

AtSdhAug Tha60210011Tfi5LineAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011SdhAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60210011Tfi5LineAugNew(uint32 augId, uint8 augType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineAugObjectInit(self, augId, augType, module);
    }

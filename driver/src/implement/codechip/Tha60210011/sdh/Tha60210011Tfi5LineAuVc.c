/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineAuVc.c
 *
 * Created Date: Apr 22, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 TFI-5 VC.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/cdr/controllers/ThaCdrControllerInternal.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../xc/Tha60210011ModuleXc.h"
#include "Tha60210011ModuleSdh.h"
#include "Tha60210011Tfi5LineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011Tfi5LineAuVc *)self)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011Tfi5LineAuVcMethods m_methods;

/* Override */
static tAtObjectMethods   m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tThaSdhAuVcMethods   m_ThaSdhAuVcOverride;
static tThaSdhVcMethods     m_ThaSdhVcOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Super implementation */
static const tAtObjectMethods     *m_AtObjectMethods  = NULL;
static const tAtChannelMethods    *m_AtChannelMethods = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 XcBaseAddressGet(ThaSdhVc self)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    return Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static AtList DestinationChannels(AtChannel self)
    {
    if (mThis(self)->destChannels == NULL)
        mThis(self)->destChannels = AtListCreate(0);
    return mThis(self)->destChannels;
    }

static eAtRet SourceSet(AtChannel self, AtChannel source)
    {
    mThis(self)->sourceChannel = source;
    return cAtOk;
    }

static AtChannel SourceGet(AtChannel self)
    {
    return mThis(self)->sourceChannel;
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
        uint32 moduleId_ = moduleId;

        AtUnused(self);
        AtUnused(address);

        if ((moduleId_ == cThaModuleOcn) ||
            (moduleId_ == cThaModulePoh) ||
            (moduleId_ == cThaModuleCdr) ||
            (moduleId_ == cAtModulePdh)  ||
            (moduleId_ == cThaModuleMap) ||
            (moduleId_ == cThaModuleDemap))
            return cAtTrue;
        return cAtFalse;
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);

    return ThaModulePohAuVcPohProcessorCreate(pohModule, (AtSdhVc)self);
    }

static eBool NeedConfigurePdh(ThaSdhAuVc self)
    {
    return Tha60210011ModuleSdhChannelIsHoPath((AtSdhChannel)self) ? cAtFalse : cAtTrue;
    }

static uint8 *CachedLoopbackMode(AtChannel self)
    {
    return &(mThis(self)->loopbackMode);
    }

static eBool OnlySupportXcLoopback(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool XcLoopbackModeIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (AtSdhChannelMapTypeGet((AtSdhChannel)self) == cAtSdhVcMapTypeNone)
        return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;

    return cAtTrue;
    }

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isPosAdj)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isPosAdj) ? 0 : 1;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice);
    return (uint32)(512UL * slice + 64UL * adjMode + hwStsInSlice + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtChannel)self)));
    }

static uint32 AdjcntperstsramBase(ThaSdhVc self)
    {
    return Tha60210011ModuleOcnStsPointerInterpreterPerChannelAdjustCount((Tha60210011ModuleOcn)ModuleOcn((AtChannel)self), (AtSdhChannel)self);
    }

static uint32 AdjcntpgperstsramBase(ThaSdhVc self)
    {
    return Tha60210011ModuleOcnStsPointerGeneratorPerChannelAdjustCount((Tha60210011ModuleOcn)ModuleOcn((AtChannel)self), (AtSdhChannel)self);
    }

static uint32 CounterAddress(AtChannel self, uint16 counterType)
    {
    ThaSdhVc vc = (ThaSdhVc)self;
    uint32 ppOffset = mMethodsGet(vc)->OcnPJCounterOffset(vc, cAtTrue);
    uint32 npOffset = mMethodsGet(vc)->OcnPJCounterOffset(vc, cAtFalse);
    uint32 adjcntperstsram_Base = AdjcntperstsramBase(vc);
    uint32 adjcntpgperstsram_Base = AdjcntpgperstsramBase(vc);

    if (counterType == cAtSdhPathCounterTypeRxPPJC)
        return adjcntperstsram_Base + ppOffset;

    if (counterType == cAtSdhPathCounterTypeRxNPJC)
        return adjcntperstsram_Base + npOffset;

    if (counterType == cAtSdhPathCounterTypeTxPPJC)
        return adjcntpgperstsram_Base + ppOffset;

    if (counterType == cAtSdhPathCounterTypeTxNPJC)
        return adjcntpgperstsram_Base + npOffset;

    return cInvalidUint32;
    }

static eBool PointerCounterGet(AtChannel self, uint16 counterType, uint32 *counter)
    {
    uint32 address = CounterAddress(self, counterType);

    if (address == cInvalidUint32)
        return cAtFalse;

    *counter = mChannelHwRead(self, address, cThaModuleOcn);

    return cAtTrue;
    }

static ThaSdhPointerAccumulator *PointerAccumulatorAddress(ThaSdhVc self)
    {
    return &(mThis(self)->pointerAccumulator);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtSdhPathHasPointerProcessor((AtSdhPath)self))
        {
        uint32 counterVal;
        eBool isPointerCounter = PointerCounterGet(self, counterType, &counterVal);
        if (isPointerCounter)
            return ThaSdhVcPointerCounterUpdateThenClear((ThaSdhVc)self, counterType, counterVal, cAtFalse);
        }

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtSdhPathHasPointerProcessor((AtSdhPath)self))
        {
        uint32 counterVal;
        eBool isPointerCounter = PointerCounterGet(self, counterType, &counterVal);
        if (isPointerCounter)
            return ThaSdhVcPointerCounterUpdateThenClear((ThaSdhVc)self, counterType, counterVal, cAtTrue);
        }

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eBool HasOcnEpar(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsChannelized(uint8 mapType)
    {
    if ((mapType == cAtSdhVcMapTypeVc4Map3xTug3s) ||
        (mapType == cAtSdhVcMapTypeVc3Map7xTug2s) ||
        (mapType == cAtSdhVcMapTypeVc3MapDe3))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LoLineStsAllocate(AtSdhChannel self, uint8 swSts, uint8 *loSlice, uint8 *loHwSts)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    AtUnused(swSts);
        return Tha60210011ModuleSdhLoLineStsAllocate(moduleSdh, self, loSlice, loHwSts);
    }

static eAtRet LoLineResourceConnect(Tha60210011Tfi5LineAuVc self, uint16* selectedLoLineSts)
    {
    AtModuleSdh moduleSdh;
    uint16 *allHwSts;
    uint8 loSlice, loHwSts, sts_i, swSts;
    eAtRet ret = cAtOk;
    AtSdhChannel channel = (AtSdhChannel)self;

    moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    allHwSts = AtSdhChannelAllHwSts(channel);
    if (allHwSts == NULL)
        return cAtErrorRsrcNoAvail;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(channel); sts_i++)
        {
        swSts = (uint8)(AtSdhChannelSts1Get(channel) + sts_i);

        if (selectedLoLineSts)
            {
            loSlice = (uint8)(selectedLoLineSts[sts_i] / cNumStsInOc48);
            loHwSts = (uint8)(selectedLoLineSts[sts_i] % cNumStsInOc48);
            Tha60210011ModuleSdhLoLineStsInUsedSet(moduleSdh, loSlice, loHwSts, cAtTrue);
            }
        else
            {
            ret = LoLineStsAllocate(channel, swSts, &loSlice, &loHwSts);
            if (ret != cAtOk)
                {
                AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation,
                             "Lo STS allocation fail, ret = %s\r\n", AtRet2String(ret));
                return ret;
                }
            }

        ret = Tha60210011ModuleXcConnectSdhChannelToLoSts(channel, swSts, loSlice, loHwSts);
        allHwSts[sts_i] = Tha60210011ModuleOcnSliceAndStsCache(loSlice, loHwSts);
        }

    return ret;
    }

static eAtRet HoLineResourceHandle(AtSdhChannel self)
    {
    return Tha60210011ModuleXcConnectSdhChannelToHoSts(self);
    }

static void CdrControllerDelete(AtSdhChannel self)
    {
    AtObjectDelete((AtObject)((ThaSdhVc)self)->cdrController);
    ((ThaSdhVc)self)->cdrController = NULL;
    }

static void XcResourceRelease(AtSdhChannel self, uint8 currentMapType)
    {
    uint8 sts_i, swSts;
    uint16* allHwSts = AtSdhChannelAllHwSts(self);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 loSlice, loSts;

    if (!IsChannelized(currentMapType))
        Tha60210011ModuleXcDisconnectSdhChannelToHoSts(self);

    else
        {
        /* Channelized VC */
        allHwSts = AtSdhChannelAllHwSts(self);
        if (allHwSts == NULL)
            return;

        for(sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
            {
            loSlice = mSliceFromCache(allHwSts[sts_i]);
            loSts   = mHwStsFromCache(allHwSts[sts_i]);
            swSts   = (uint8)(AtSdhChannelSts1Get(self) + sts_i);

            Tha60210011ModuleXcDisconnectSdhChannelToLoSts(self, swSts, loSlice, loSts);

            if (mMethodsGet(mThis(self))->NeedManageLoLineSts(mThis(self)))
                Tha60210011ModuleSdhLoLineStsDeallocate(moduleSdh, loSlice, loSts);

            allHwSts[sts_i] = cBit15_0;
            }
        }
    }

static void XcSourceReferenceRemove(AtChannel self)
    {
    AtChannel source = AtChannelSourceGet(self);
    if (source)
        AtChannelDestinationRemove(source, self);
    AtChannelSourceSet(self, NULL);
    }

static void XcDestinationReferencesRemove(AtChannel self)
    {
    AtList destinations = AtChannelDestinationChannels(self);
    AtChannel dest;
    while ((dest = (AtChannel)AtListObjectRemoveAtIndex(destinations, 0)) != NULL)
        AtChannelSourceSet(dest, NULL);
    }

static void XcReferencesRemove(AtChannel self)
    {
    XcSourceReferenceRemove(self);
    XcDestinationReferencesRemove(self);
    }

static void CurrentResourceRelease(AtSdhChannel self, uint8 currentMapType)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    eBool autoXC = Tha60210011ModuleSdhAutoCrossConnect(moduleSdh);

    if (autoXC && (currentMapType == cAtSdhVcMapTypeNone))
        return;

    if (autoXC)
        XcResourceRelease(self, currentMapType);
    else
        XcReferencesRemove((AtChannel)self);

    /* CDR controller need to be re-create so it can know new mapping type */
    CdrControllerDelete(self);
    }

static eBool NeedHoResource(uint8 currentMapType, uint8 newMapType)
    {
    /* Change from None or channelized to un-channelized */
    if (((currentMapType == cAtSdhVcMapTypeNone) || IsChannelized(currentMapType))
        && !IsChannelized(newMapType))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    uint8 currentMapType = AtSdhChannelActualMapTypeGet(self);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);

    if (currentMapType == mapType)
        return cAtOk;

    if (!IsChannelized(mapType))
        return cAtOk;

    return (Tha60210011ModuleSdhFreeStsIsAvailable(moduleSdh, AtSdhChannelNumSts(self)) ? cAtOk : cAtErrorRsrcNoAvail);
    }

static eBool LoLineStsNeedToUpdate(AtSdhChannel self, uint16 *selectedLoLineSts)
    {
    uint16* allHwSts;
    uint8 sts_i, loSlice, loSts;

    if (!mMethodsGet(mThis(self))->NeedManageLoLineSts(mThis(self)))
    	return cAtFalse;

    /* Don't care if lo line sts is not selected */
    if (selectedLoLineSts == NULL)
        return cAtFalse;

    /* Is channelized */
    allHwSts = AtSdhChannelAllHwSts(self);
    if (allHwSts == NULL)
        return cAtTrue;

    for(sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        if (allHwSts[sts_i] == cBit15_0)
            return cAtTrue;

        loSlice = mSliceFromCache(allHwSts[sts_i]);
        loSts   = mHwStsFromCache(allHwSts[sts_i]);

        if ((loSlice != selectedLoLineSts[sts_i] / cNumStsInOc48) ||
           (loSts != selectedLoLineSts[sts_i] % cNumStsInOc48))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet HoLoLineResourceHandle(AtSdhChannel self, uint8 newMapType, uint16 *selectedLoLineSts)
    {
    uint8 currentMapType = AtSdhChannelMapTypeGet(self);
    if (newMapType == cAtSdhVcMapTypeNone)
        {
        CurrentResourceRelease(self, currentMapType);
        return cAtOk;
        }

    if (NeedHoResource(currentMapType, newMapType))
        {
        CurrentResourceRelease(self, currentMapType);
        return HoLineResourceHandle(self);
        }

    /* Change from Unchannelized to channelized, or current map type is channelized but LO line resource change */
    if ((!IsChannelized(currentMapType) && IsChannelized(newMapType)) ||
        (IsChannelized(currentMapType) && LoLineStsNeedToUpdate(self, selectedLoLineSts)))
        {
        CurrentResourceRelease(self, currentMapType);
        return mMethodsGet(mThis(self))->LoLineResourceConnect(mThis(self), selectedLoLineSts);
        }

    return cAtOk;
    }

static eAtRet SelectedStsIsValid(AtSdhChannel self, uint8 mapType, uint16* selectedLoLineSts, uint8 numSelectedSts)
    {
    uint8 sts_i;
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);

    if (!mMethodsGet(mThis(self))->NeedManageLoLineSts(mThis(self)))
    	return cAtOk;

    /* Ignore selected STS if map type is un-channelized */
    if (!IsChannelized(mapType))
        return cAtOk;

    if (numSelectedSts != AtSdhChannelNumSts(self))
        return cAtErrorInvlParm;

    for (sts_i = 0; sts_i < numSelectedSts; sts_i++)
        {
        uint8 slice = (uint8)(selectedLoLineSts[sts_i] / cNumStsInOc48);
        uint8 hwSts = (uint8)(selectedLoLineSts[sts_i] % cNumStsInOc48);

        if (Tha60210011ModuleSdhLoLineStsIsInUsed(moduleSdh, slice, hwSts))
            return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

static eBool XcIsSupported(AtSdhChannel self)
    {
    return (AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleXc) != NULL) ? cAtTrue : cAtFalse;
    }

static AtModuleSdh ModuleSdh(AtSdhChannel self)
    {
    return (AtModuleSdh) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel) self), cAtModuleSdh);
    }

eAtModuleSdhRet Tha60210011Tfi5LineAuVcMapTypeSet(AtSdhChannel self, uint8 mapType, uint16* selectedLoLineSts, uint8 numSelectedSts)
    {
    eAtRet ret = cAtOk;
    AtChannel channel = (AtChannel)self;
    eAtLoopbackMode loopbackMode = AtChannelLoopbackGet(channel);
    
    if (!AtSdhChannelNeedToUpdateMapType(self, mapType))
        {
        /* Map type is not change, but LO selected resource may be changed */
        if (!IsChannelized(AtSdhChannelMapTypeGet(self)))
            return cAtOk;

        if (!LoLineStsNeedToUpdate(self, selectedLoLineSts))
            return cAtOk;
        }

    if (selectedLoLineSts)
        ret = SelectedStsIsValid(self, mapType, selectedLoLineSts, numSelectedSts);
    else
        ret = AtSdhChannelCanChangeMapping(self, mapType);

    if (ret != cAtOk)
        return ret;

    if ((ThaSdhVcCachedPrbsEngine((ThaSdhVc)self) != NULL) && !mMethodsGet(mThis(self))->CanChangedMappingWhenHasPrbsEngine(mThis(self)))
        return cAtErrorChannelBusy;

    if (XcIsSupported(self))
        {
        if (Tha60210011ModuleSdhAutoCrossConnect(ModuleSdh(self)))
            {
            /* Need to release and re-loopback because XC will change, map type NONE will not support loopback */
            ret = AtChannelLoopbackSet(channel, cAtLoopbackModeRelease);

            if (HoLoLineResourceHandle(self, mapType, selectedLoLineSts) != cAtOk)
                return cAtErrorRsrcNoAvail;
            }
        }

    /* Super's job */
    ret |= m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    if ((XcIsSupported(self)) && (loopbackMode != cAtLoopbackModeRelease) && (mapType != cAtSdhVcMapTypeNone))
        ret = AtChannelLoopbackSet(channel, loopbackMode);

    /* Disable checking LOM as default, TUG-2 will enable when it is mapped TU1x */
    ret |= ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)self, cAtFalse);

    return ret;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    return Tha60210011Tfi5LineAuVcMapTypeSet(self, mapType, NULL, 0);
    }

static void Delete(AtObject self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint16 *allTfi5Sts = NULL;

    /* Delete PDH channel before release lo-line resource,
     * PDH channel still need HW sts when it is deleting */
    ThaSdhVcPdhChannelDelete((AtSdhVc)self);

    if (XcIsSupported(sdhChannel))
        CurrentResourceRelease(sdhChannel, AtSdhChannelMapTypeGet(sdhChannel));

    AtObjectDelete((AtObject)mThis(self)->destChannels);
    mThis(self)->destChannels = NULL;
    allTfi5Sts = mThis(self)->allTfi5Sts;
    m_AtObjectMethods->Delete(self);

    if (allTfi5Sts)
        AtOsalMemFree(allTfi5Sts);
    }

static eAtRet XcLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    uint8 *cache;
    eAtRet ret;
    uint8 currentLoopback = AtChannelLoopbackGet(self);

    if (loopbackMode == currentLoopback)
        return cAtOk;

    cache = mMethodsGet(self)->CachedLoopbackMode(self);
    if (cache)
        *cache = loopbackMode;

    /* Release current loopback mode */
    if (currentLoopback != cAtLoopbackModeRelease)
        {
        ret = Tha60210011ModuleXcSdhChannelLoopRelease((AtSdhChannel)self);
        if (ret != cAtOk)
            return ret;
        }

    if (loopbackMode == cAtLoopbackModeLocal)
        return Tha60210011ModuleXcSdhChannelLoopLocal((AtSdhChannel)self);

    if (loopbackMode == cAtLoopbackModeRemote)
        return Tha60210011ModuleXcSdhChannelLoopRemote((AtSdhChannel)self);

    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint8 XcLoopbackGet(AtChannel self)
    {
    return Tha60210011ModuleXcSdhChannelLoopModeGet((AtSdhChannel)self);
    }

static eAtRet PgStsSlaverIndicate(ThaSdhAuVc self, eAtSdhVcMapType vcMapType)
    {
    /* This product has nothing to do with this */
    AtUnused(self);
    AtUnused(vcMapType);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011Tfi5LineAuVc *object = (tTha60210011Tfi5LineAuVc *)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 slice, sts;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(loopbackMode);
    mEncodeChannelIdString(sourceChannel);
    mEncodeObject(pointerAccumulator);

    /* Trick here, this field is lazy updated, call ID converting function to
     * have this update. Otherwise, database of active/standby drivers will never
     * be the same */
    ThaSdhChannelHwStsGet(sdhChannel, cThaModulePoh, AtSdhChannelSts1Get(sdhChannel), &slice, &sts);
    mEncodeUInt16Array(allTfi5Sts, AtSdhChannelNumSts((AtSdhChannel)self));

    AtCoderEncodeObjectDescriptionInList(encoder, object->destChannels, "destChannels");
    }

static eBool NeedManageLoLineSts(Tha60210011Tfi5LineAuVc self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static eBool NeedReconfigureSSBitWhenBindingPw(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaCdrController CdrController(AtChannel self)
    {
    return ThaSdhVcCdrControllerGet((AtSdhVc)self);
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    return ThaCdrControllerTimingSet(CdrController(self), timingMode, timingSource);
    }

static int32 Offset(AtChannel self)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModulePdh, &slice, &hwIdInSlice);
    vtg = AtSdhChannelTug2Get(sdhChannel);
    vt = AtSdhChannelTu1xGet(sdhChannel);
    return (int32)((hwIdInSlice * 32UL) + (vtg * 4UL) + vt) + ThaModulePdhSliceOffset(pdhModule, slice);
    }

static eAtRet FpgaPayloadLoopinEnable(AtChannel self, eBool enable)
    {
    uint32 address = (uint32)(cThaRegDS1E1J1RxFrmrCtrl + Offset(self));
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1LocalPayLoopMask, cThaRxDE1LocalPayLoopShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FpgaPayloadLoopinIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, (uint32)(cThaRegDS1E1J1RxFrmrCtrl + Offset(self)), cAtModulePdh);
    return (regVal & cThaRxDE1LocalPayLoopMask) ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaPayloadLoopoutEnable(AtChannel self, eBool enable)
    {
    uint32 address = (uint32)(cThaRegDS1E1J1TxFrmrCtrl + Offset(self));
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1RmtPayLoopMask, cThaTxDE1RmtPayLoopShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FpgaPayloadLoopoutIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, (uint32)(cThaRegDS1E1J1TxFrmrCtrl + Offset(self)), cAtModulePdh);
    return (regVal & cThaTxDE1RmtPayLoopMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PohAisNotifyOnRxAisForced(AtChannel self, eBool forced)
    {
    ThaModuleOcn ocnModule = ModuleOcn(self);

    if (AtChannelLoopbackGet(self) == cAtLoopbackModeRemote)
        return ThaModuleOcnNotifyLoVcAisToPoh(ocnModule, (AtSdhVc)self, forced);

    return cAtOk;
    }

static eAtRet FpgaRxAisForceEnable(AtChannel self, eBool enable)
    {
    uint32 address = (uint32)(cThaRegDS1E1J1RxFrmrCtrl + Offset(self));
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1FrcAISPldDwnMask, cThaRxDE1FrcAISPldDwnShift, enable ? 1:0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    if (ThaModuleOcnCanMovePathAisRxForcingPoint(ModuleOcn(self)))
        return PohAisNotifyOnRxAisForced(self, enable);

    return cAtOk;
    }

static eBool FpgaRxAisForceIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, (uint32)(cThaRegDS1E1J1RxFrmrCtrl + Offset(self)), cAtModulePdh);
    return (regVal & cThaRxDE1FrcAISPldDwnMask) ? cAtTrue : cAtFalse;
    }

static eBool CanChangedMappingWhenHasPrbsEngine(Tha60210011Tfi5LineAuVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool VcPdhLoopbackIsApplicable(AtChannel self, uint8 loopbackMode)
    {
    AtPw pw = AtChannelBoundPwGet(self);
    AtPrbsEngine prbs = AtChannelPrbsEngineGet(self);

    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtTrue;

    /* If PW have not been bound, can remote loopback, local loopback does not
     * work and it does not make any sense */
    if (pw == NULL)
        return (loopbackMode == cAtLoopbackModeRemote) ? cAtTrue : cAtFalse;

    /* PRBS engine has not been provisioned, can loopback with any mode */
    if (prbs == NULL)
        return cAtTrue;

    /* PRBS engine is enabled, local loopback will never work because POH is fail.
     *  But some application still need Cx loopback only for system side bert feature.
     *  So SDK still allow to support local loopback in this case */
    if (AtPrbsEngineIsEnabled(prbs) && (loopbackMode == cAtLoopbackModeLocal))
        return cAtTrue;

    return cAtTrue;
    }

static eAtRet NotifyLoVcAisToPohOnLoopback(AtChannel self, uint8 loopbackMode)
    {
    ThaModuleOcn ocnModule = ModuleOcn(self);

    if (loopbackMode == cAtLoopbackModeRemote)
        {
        /* Need to notify POH block if AIS is being forced */
        if (AtChannelRxForcedAlarmGet(self) & cAtSdhPathAlarmAis)
            return ThaModuleOcnNotifyLoVcAisToPoh(ocnModule, (AtSdhVc)self, cAtTrue);

        return cAtOk;
        }

    /* When releasing, need to stop notifying POH. Let it work as it is */
    if (AtChannelRxForcedAlarmGet(self) & cAtSdhPathAlarmAis)
        return ThaModuleOcnNotifyLoVcAisToPoh(ocnModule, (AtSdhVc)self, cAtFalse);

    return cAtOk;
    }

static eAtRet LomMonitorEnable(ThaSdhAuVc self, eBool enable)
    {
    return Tha60210011ModuleOcnLomCheckingEnable((AtSdhChannel)self, enable);
    }

static eBool LomMonitorIsEnabled(ThaSdhAuVc self)
    {
    return Tha60210011ModuleOcnLomCheckingIsEnabled((AtSdhChannel)self);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(AtSharedDriverOsalGet(), &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, XcBaseAddressGet);
        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, HasOcnEpar);
        mMethodOverride(m_ThaSdhVcOverride, PointerAccumulatorAddress);
        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc sdhAuVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, mMethodsGet(sdhAuVc), sizeof(m_ThaSdhAuVcOverride));

        mMethodOverride(m_ThaSdhAuVcOverride, NeedConfigurePdh);
        mMethodOverride(m_ThaSdhAuVcOverride, PgStsSlaverIndicate);
        mMethodOverride(m_ThaSdhAuVcOverride, NeedReconfigureSSBitWhenBindingPw);
        mMethodOverride(m_ThaSdhAuVcOverride, LomMonitorEnable);
        mMethodOverride(m_ThaSdhAuVcOverride, LomMonitorIsEnabled);
        }

    mMethodsSet(sdhAuVc, &m_ThaSdhAuVcOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DestinationChannels);
        mMethodOverride(m_AtChannelOverride, SourceSet);
        mMethodOverride(m_AtChannelOverride, SourceGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, XcLoopbackModeIsSupported);
        mMethodOverride(m_AtChannelOverride, OnlySupportXcLoopback);
        mMethodOverride(m_AtChannelOverride, CachedLoopbackMode);
        mMethodOverride(m_AtChannelOverride, XcLoopbackSet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, XcLoopbackGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideThaSdhVc(self);
    OverrideThaSdhAuVc(self);
    }

static void MethodsInit(AtSdhVc self)
    {
    Tha60210011Tfi5LineAuVc sdhVc = (Tha60210011Tfi5LineAuVc)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, LoLineResourceConnect);
        mMethodOverride(m_methods, NeedManageLoLineSts);
        mMethodOverride(m_methods, CanChangedMappingWhenHasPrbsEngine);
        }

    mMethodsSet(sdhVc, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5LineAuVc);
    }

AtSdhVc Tha60210011Tfi5LineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210011Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineAuVcObjectInit(self, channelId, channelType, module);
    }

eAtRet Tha60210011Tfi5LineAuVcLomCheckingEnable(AtSdhChannel self, eBool enable)
    {
    return ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)self, enable);
    }

eAtRet Tha60210011Tfi5LineAuVcLomCheckingUpdate(AtSdhChannel self)
    {
    eBool enable = AtSdhVcTu1xChannelized((AtSdhVc)self);
    return ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)self, enable);
    }

uint16* Tha60210011Tfi5LineAuVcAllTfi5Sts(AtSdhChannel self)
    {
    uint8 sts_i, numSts = AtSdhChannelNumSts(self);
    if (mThis(self)->allTfi5Sts)
        return mThis(self)->allTfi5Sts;

    mThis(self)->allTfi5Sts = AtOsalMemAlloc(numSts * sizeof(uint16));
    if (mThis(self)->allTfi5Sts == NULL)
        return NULL;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        mThis(self)->allTfi5Sts[sts_i] = cBit15_0;

    return mThis(self)->allTfi5Sts;
    }

eBool Tha60210011SdhVcPdhLoopbackIsApplicable(AtChannel self, uint8 loopbackMode)
    {
    if (self)
        return VcPdhLoopbackIsApplicable(self, loopbackMode);
    return cAtFalse;
    }

eAtRet Tha60210011SdhVcPdhLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    /* Local loopback */
    ret |= FpgaPayloadLoopinEnable(self, (loopbackMode == cAtLoopbackModeLocal) ? cAtTrue : cAtFalse);

    /* Remote loopback */
    ret |= FpgaPayloadLoopoutEnable(self, (loopbackMode == cAtLoopbackModeRemote) ? cAtTrue : cAtFalse);

    if ((ret == cAtOk) && ThaModuleOcnCanMovePathAisRxForcingPoint(ModuleOcn(self)))
        ret |= NotifyLoVcAisToPohOnLoopback(self, loopbackMode);

    return ret;
    }

uint8 Tha60210011SdhVcPdhLoopbackGet(AtChannel self)
    {
    /* Check local loopback */
    if (FpgaPayloadLoopinIsEnabled(self))  return cAtLoopbackModeLocal;

    /* Check remote loopback */
    if (FpgaPayloadLoopoutIsEnabled(self)) return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

eAtRet Tha6021SdhLoVcRxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    if ((AtChannelRxForcableAlarmsGet(self) & cAtSdhPathAlarmAis) &&
        (alarmType & cAtSdhPathAlarmAis) && ThaModulePdhFramerForcedAisCorrectionIsSupported(modulePdh))
        {
        return FpgaRxAisForceEnable(self, force);
        }

    return cAtErrorModeNotSupport;
    }

uint32 Tha6021SdhLoVcRxForcedAlarmGet(AtChannel self)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    if ((AtChannelRxForcableAlarmsGet(self) & cAtSdhPathAlarmAis) &&
        ThaModulePdhFramerForcedAisCorrectionIsSupported(modulePdh))
        {
        return FpgaRxAisForceIsEnabled(self);
        }

    return 0;
    }

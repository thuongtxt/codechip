/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineTug.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 TFI-5 TUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../xc/Tha60210011ModuleXc.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "Tha60210011ModuleSdh.h"
#include "Tha60210011Tfi5LineTugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* To save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel AuVc(AtSdhChannel self)
    {
    AtSdhChannel parent = AtSdhChannelParentChannelGet(self);
    if (parent == NULL)
        return NULL;

    if (AtSdhChannelIsHoVc(parent))
        return parent;

    return AuVc(parent);
    }

static eAtModuleSdhRet Tug2MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    if (mapType == cAtSdhTugMapTypeTug2Map3xTu12s || mapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        return Tha60210011Tfi5LineAuVcLomCheckingEnable(AuVc(self), cAtTrue);

    if (mapType == cAtSdhTugMapTypeTugMapNone)
        return Tha60210011Tfi5LineAuVcLomCheckingUpdate(AuVc(self));

    return cAtOk;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;

    if(!AtSdhChannelNeedToUpdateMapType(self, mapType))
        return cAtOk;

    /* Super */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeTug2)
        return Tug2MapTypeSet(self, mapType);

    return cAtOk;
    }

static void OverrideAtSdhChannel(AtSdhTug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhTug self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5LineTug);
    }

AtSdhTug Tha60210011Tfi5LineTugObjectInit(AtSdhTug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhTugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhTug Tha60210011Tfi5LineTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhTug newTug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newTug == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineTugObjectInit(newTug, channelId, channelType, module);
    }

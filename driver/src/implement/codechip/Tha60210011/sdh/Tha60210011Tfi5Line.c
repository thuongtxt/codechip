/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5Line.c
 *
 * Created Date: Apr 22, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 TFI-5 line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "Tha60210011ModuleSdh.h"
#include "Tha60210011Tfi5LineInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Alarm/error force */
#define cDiagOcnGlobalTxFramerControl       0xf43001
#define cDiagOcnTxLineOofForceMask(lineId)  (cBit24 << (lineId))
#define cDiagOcnTxLineOofForceShift(lineId) ((lineId) + 24)
#define cDiagOcnTxLineB1ForceMask(lineId)   (cBit16 << (lineId))
#define cDiagOcnTxLineB1ForceShift(lineId)  ((lineId) + 16)

/* OOF status */
#define cDiagOcnRxFramerStatus(lineId) (0xf43100 + (16 * (lineId)))
#define cDiagOcnRxFramerSticky(lineId) (0xf43101 + (16 * (lineId)))
#define cDiagOcnOofStatusMask cBit0

/* B1 counters */
#define cDiagOcnRxFramerB1ErrorCounterRo(lineId)  (0xf43102 + (16 * (lineId)))
#define cDiagOcnRxFramerB1ErrorCounterR2c(lineId) (0xf43103 + (16 * (lineId)))

/*--------------------------- Macros -----------------------------------------*/
#define mSdhModule(self) ((AtModuleSdh)AtChannelModuleGet((AtChannel)self))
#define mThis(self) ((Tha60210011Tfi5Line)self)

/*--------------------------- Local Typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011Tfi5LineMethods m_methods;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhLineMethods    m_AtSdhLineOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tThaSdhLineMethods   m_ThaSdhLineOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtSdhLineMethods    *m_AtSdhLineMethods    = NULL;

/*--------------------------- Macros -----------------------------------------*/
#define mOcnModule(self) OcnModule((AtChannel)self)

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FpgaLoopOutIsApplicable(ThaSdhLine self)
    {
    /* Loopback is done at SERDES */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet FpgaLoopInEnable(ThaSdhLine self, eBool enable)
    {
    /* Loopback is done at SERDES */
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool FpgaLoopInIsEnable(ThaSdhLine self)
    {
    AtUnused(self);

    /* Will get serdes status for loop in state */
    return cAtFalse;
    }

static ThaModuleOcn OcnModule(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    if (enable == Tha60210011ModuleOcnLineScrambleIsEnabled(mOcnModule(self)))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    return Tha60210011ModuleOcnLineScrambleIsEnabled(mOcnModule(self));
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    uint32 moduleId_ = moduleId;

    AtUnused(self);
    AtUnused(address);
    if ((moduleId_ == cThaModuleOcn) || (moduleId_ == cAtModuleSdh))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 DefaultOffset(ThaSdhLine self)
    {
    return (uint32)(ThaModuleSdhLineLocalId((AtSdhLine)self) * 512) + Tha60210011ModuleOcnBaseAddress(OcnModule((AtChannel)self));
    }

static eBool IsInDiagnosticMode(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static uint32 DiagB1CounterRead2Clear(ThaSdhLine self, eBool read2Clear)
    {
    uint32 lineId = HwIdGet((AtChannel)self);
    uint32 regAddr = cDiagOcnRxFramerB1ErrorCounterRo(lineId);
    if (read2Clear)
        regAddr = cDiagOcnRxFramerB1ErrorCounterR2c(lineId);
    return mChannelHwRead(self, regAddr, cThaModuleOcn);
    }

static uint32 B1CounterRead2Clear(ThaSdhLine self, eBool read2Clear)
    {
    uint32 regAddr;

    if (IsInDiagnosticMode((AtChannel)self))
        return DiagB1CounterRead2Clear(self, read2Clear);

    regAddr = read2Clear ?
              mMethodsGet(mThis(self))->Rxfrmb1cntr2cBase(mThis(self)) :
              mMethodsGet(mThis(self))->Rxfrmb1cntroBase(mThis(self));

    return (mChannelHwRead(self, regAddr + DefaultOffset((ThaSdhLine)self), cThaModuleOcn) & cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Mask);
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool clear)
    {
    if (counterType == cAtSdhLineCounterTypeB1)
        return B1CounterRead2Clear((ThaSdhLine)self, clear);

    /* Unknown counter */
    return 0x0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (Tha60210011ModuleSdhHasChannelStatus(mSdhModule(self)))
        return CounterRead2Clear(self, counterType, cAtFalse);

    /* Unknown counter */
    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (Tha60210011ModuleSdhHasChannelStatus(mSdhModule(self)))
        return CounterRead2Clear(self, counterType, cAtTrue);

    /* Unknown counter */
    return 0x0;
    }

static uint32 AlarmHw2Sw(uint32 hwAlarm)
    {
    uint32 alarm = 0;

    if (hwAlarm & cAf6_rxfrmsta_StaLos_Mask)
        alarm |= cAtSdhLineAlarmLos;
    if (hwAlarm & cAf6_rxfrmsta_StaOof_Mask)
        alarm |= cAtSdhLineAlarmOof;

    return alarm;
    }

static uint32 HwDefectGet(AtChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RxfrmstaBase(mThis(self)) + DefaultOffset((ThaSdhLine)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return AlarmHw2Sw(regVal);
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool clear)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RxfrmstkBase(mThis(self)) + DefaultOffset((ThaSdhLine)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (clear)
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return AlarmHw2Sw(regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (Tha60210011ModuleSdhHasChannelStatus(mSdhModule(self)))
        return HwDefectGet(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (Tha60210011ModuleSdhHasChannelStatus(mSdhModule(self)))
        return DefectHistoryRead2Clear(self, cAtFalse);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (Tha60210011ModuleSdhHasChannelStatus(mSdhModule(self)))
        return DefectHistoryRead2Clear(self, cAtTrue);
    return 0;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhLineAlarmOof;
    }

static uint32 TxFramerControlRegister(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return cDiagOcnGlobalTxFramerControl;
    return Tha60210011ModuleOcnTfi5GlbtfmBase(OcnModule(self)) + Tha60210011ModuleOcnBaseAddress(OcnModule(self));
    }

static eAtRet TxOofForce(AtChannel self, eBool forced)
    {
    uint32 regAddr = TxFramerControlRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 lineId = HwIdGet(self);
    mFieldIns(&regVal,
              cDiagOcnTxLineOofForceMask(lineId),
              cDiagOcnTxLineOofForceShift(lineId),
              forced ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool TxOofIsForced(AtChannel self)
    {
    uint32 regAddr = TxFramerControlRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cDiagOcnTxLineOofForceMask(HwIdGet(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet B1Force(AtChannel self, eBool forced)
    {
    uint32 regAddr = TxFramerControlRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 lineId = HwIdGet(self);
    mFieldIns(&regVal,
              cDiagOcnTxLineB1ForceMask(lineId),
              cDiagOcnTxLineB1ForceShift(lineId),
              forced ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool B1IsForced(AtChannel self)
    {
    uint32 regAddr = TxFramerControlRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cDiagOcnTxLineB1ForceMask(HwIdGet(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (alarmType == cAtSdhLineAlarmOof)
        return TxOofForce(self, cAtTrue);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (alarmType == cAtSdhLineAlarmOof)
        return TxOofForce(self, cAtFalse);
    return cAtOk;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (TxOofIsForced(self))
        return cAtSdhLineAlarmOof;
    return 0;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhLineErrorB1)
        return B1Force(self, cAtTrue);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhLineErrorB1)
        return B1Force(self, cAtFalse);
    return cAtOk;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (B1IsForced(self))
        return cAtSdhLineErrorB1;
    return cAtSdhLineErrorNone;
    }

static uint32 DiagOofAlarmGet(AtChannel self)
    {
    uint32 regAddr = cDiagOcnRxFramerStatus(HwIdGet(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cDiagOcnOofStatusMask) ? cAtSdhLineAlarmOof : 0;
    }

static uint32 DiagOofAlarmHistoryRead2Clear(AtChannel self, eBool clear)
    {
    uint32 regAddr = cDiagOcnRxFramerSticky(HwIdGet(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (clear)
        mChannelHwWrite(self, regAddr, regVal, cDiagOcnOofStatusMask);
    return (regVal & cDiagOcnOofStatusMask) ? cAtSdhLineAlarmOof : 0;
    }

static uint32 AlarmGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return DiagOofAlarmGet(self);
    return m_AtChannelMethods->AlarmGet(self);
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return DiagOofAlarmHistoryRead2Clear(self, cAtFalse);
    return m_AtChannelMethods->AlarmHistoryGet(self);
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return DiagOofAlarmHistoryRead2Clear(self, cAtTrue);
    return m_AtChannelMethods->AlarmHistoryClear(self);
    }

static eBool HasBerControllers(AtSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtUnused(self);
    return (rate == cAtSdhLineRateStm16) ? cAtTrue : cAtFalse;
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm16;
    }

static eAtModuleSdhRet TxS1Set(AtSdhLine self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
    }

static uint8 TxS1Get(AtSdhLine self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxS1Get(AtSdhLine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleSdhRet TxK1Set(AtSdhLine self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
    }

static uint8 TxK1Get(AtSdhLine self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxK1Get(AtSdhLine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleSdhRet TxK2Set(AtSdhLine self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
    }

static uint8 TxK2Get(AtSdhLine self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxK2Get(AtSdhLine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleSdhRet LedStateSet(AtSdhLine self, eAtLedState ledState)
    {
    AtUnused(self);
    AtUnused(ledState);
    return cAtErrorNotApplicable;
    }

static eAtLedState LedStateGet(AtSdhLine self)
    {
    AtUnused(self);
    return cAtLedStateOff;
    }

static eAtModuleSdhRet Switch(AtSdhLine self, AtSdhLine toLine)
    {
    AtUnused(self);
    AtUnused(toLine);
    return cAtErrorNotApplicable;
    }

static AtSdhLine SwitchedLineGet(AtSdhLine self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleSdhRet SwitchRelease(AtSdhLine self)
    {
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModuleSdhRet Bridge(AtSdhLine self, AtSdhLine toLine)
    {
    AtUnused(self);
    AtUnused(toLine);
    return cAtErrorNotApplicable;
    }

static AtSdhLine BridgedLineGet(AtSdhLine self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleSdhRet BridgeRelease(AtSdhLine self)
    {
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModuleSdhRet BerControllersReCreate(AtSdhLine self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(enable);
    return cAtErrorNotApplicable;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static eAtRet InterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear)
    {
    AtUnused(self);
    AtUnused(read2Clear);
    if (changedDefects)
        *changedDefects = 0;
    if (currentStatus)
        *currentStatus = 0;
    return cAtOk;
    }

static eAtRet InterruptAndAlarmGet(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear)
    {
    AtUnused(self);
    AtUnused(read2Clear);
    if (changedAlarms)
        *changedAlarms = 0;
    if (currentStatus)
        *currentStatus = 0;
    return cAtOk;
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(enableMask);
    return (defectMask == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return (alarmType == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return (alarmType == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (Tha60210011ModuleSdhHasChannelStatus(mSdhModule(self)))
        return (counterType == cAtSdhLineCounterTypeB1) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtErrorModeNotSupport;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtErrorModeNotSupport;
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool HasRateHwConfiguration(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasToh(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanSetTxK1(AtSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanSetTxK2(AtSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanSetTxS1(AtSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return enabled;
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return enabled;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 Rxfrmb1cntr2cBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_rxfrmb1cntr2c_Base;
    }

static uint32 Rxfrmb1cntroBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_rxfrmb1cntro_Base;
    }

static uint32 RxfrmstaBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_rxfrmsta_Base;
    }

static uint32 RxfrmstkBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_rxfrmstk_Base;
    }

static void MethodsInit(Tha60210011Tfi5Line self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Rxfrmb1cntr2cBase);
        mMethodOverride(m_methods, Rxfrmb1cntroBase);
        mMethodOverride(m_methods, RxfrmstaBase);
        mMethodOverride(m_methods, RxfrmstkBase);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BoundPseudowireGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));
        mMethodOverride(m_AtSdhLineOverride, ScrambleEnable);
        mMethodOverride(m_AtSdhLineOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtSdhLineOverride, HasBerControllers);
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        mMethodOverride(m_AtSdhLineOverride, RateGet);
        mMethodOverride(m_AtSdhLineOverride, TxS1Set);
        mMethodOverride(m_AtSdhLineOverride, TxS1Get);
        mMethodOverride(m_AtSdhLineOverride, RxS1Get);
        mMethodOverride(m_AtSdhLineOverride, TxK1Set);
        mMethodOverride(m_AtSdhLineOverride, TxK1Get);
        mMethodOverride(m_AtSdhLineOverride, RxK1Get);
        mMethodOverride(m_AtSdhLineOverride, TxK2Set);
        mMethodOverride(m_AtSdhLineOverride, TxK2Get);
        mMethodOverride(m_AtSdhLineOverride, RxK2Get);
        mMethodOverride(m_AtSdhLineOverride, LedStateSet);
        mMethodOverride(m_AtSdhLineOverride, LedStateGet);
        mMethodOverride(m_AtSdhLineOverride, Switch);
        mMethodOverride(m_AtSdhLineOverride, SwitchedLineGet);
        mMethodOverride(m_AtSdhLineOverride, SwitchRelease);
        mMethodOverride(m_AtSdhLineOverride, Bridge);
        mMethodOverride(m_AtSdhLineOverride, BridgedLineGet);
        mMethodOverride(m_AtSdhLineOverride, BridgeRelease);
        mMethodOverride(m_AtSdhLineOverride, BerControllersReCreate);
        mMethodOverride(m_AtSdhLineOverride, CanSetTxK1);
        mMethodOverride(m_AtSdhLineOverride, CanSetTxK2);
        mMethodOverride(m_AtSdhLineOverride, CanSetTxS1);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, InterruptAndDefectGet);
        mMethodOverride(m_AtSdhChannelOverride, InterruptAndAlarmGet);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopOutIsApplicable);
        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopInEnable);
        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopInIsEnable);
        mMethodOverride(m_ThaSdhLineOverride, HasRateHwConfiguration);
        mMethodOverride(m_ThaSdhLineOverride, HasToh);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhLine(self);
    OverrideAtSdhChannel(self);
    OverrideThaSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5Line);
    }

AtSdhLine Tha60210011Tfi5LineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineObjectInit(self, channelId, module, 0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60210011Tfi5LineNew(uint32 tfi5LineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineObjectInit(newLine, tfi5LineId, module);
    }


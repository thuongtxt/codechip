/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011Tfi5LineAuVcInternal.h
 * 
 * Created Date: Mar 8, 2021
 *
 * Description : TFI5 line AU VC implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5LINEAUVCINTERNAL_H_
#define _THA60210011TFI5LINEAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhVcInternal.h"
#include "Tha60210011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumStsInOc48 48

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5LineAuVcMethods
    {
    eAtRet (*LoLineResourceConnect)(Tha60210011Tfi5LineAuVc self, uint16* selectedLoLineSts);
    eBool (*NeedManageLoLineSts)(Tha60210011Tfi5LineAuVc self);
    eBool (*CanChangedMappingWhenHasPrbsEngine)(Tha60210011Tfi5LineAuVc self);
    }tTha60210011Tfi5LineAuVcMethods;

typedef struct tTha60210011Tfi5LineAuVc
    {
    tThaSdhAuVc super;
    const tTha60210011Tfi5LineAuVcMethods *methods;

    /* Private data */
    AtList destChannels;
    AtChannel sourceChannel;
    uint8 loopbackMode;
    uint16 *allTfi5Sts;
    ThaSdhPointerAccumulator pointerAccumulator;
    }tTha60210011Tfi5LineAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210011Tfi5LineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);
eBool Tha60210011SdhVcPdhLoopbackIsApplicable(AtChannel self, uint8 loopbackMode);
eAtRet Tha60210011SdhVcPdhLoopbackSet(AtChannel self, uint8 loopbackMode);
uint8 Tha60210011SdhVcPdhLoopbackGet(AtChannel self);
eAtRet Tha6021SdhLoVcRxAlarmForce(AtChannel self, uint32 alarmType, eBool force);
uint32 Tha6021SdhLoVcRxForcedAlarmGet(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011TFI5LINEAUVCINTERNAL_H_ */


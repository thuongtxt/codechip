/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011ModuleSdhInternal.h
 * 
 * Created Date: Sep 10, 2015
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULESDHINTERNAL_H_
#define _THA60210011MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60210011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumStsInLoLine 48

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleSdhMethods
    {
    uint8 (*MaxNumHoLines)(Tha60210011ModuleSdh self);
    uint8 (*MaxNumLoLines)(Tha60210011ModuleSdh self);
    uint8 (*HoLineStartId)(Tha60210011ModuleSdh self);
    uint8 (*LoLineStartId)(Tha60210011ModuleSdh self);
    eBool (*AutoCrossConnect)(Tha60210011ModuleSdh self);
    eAtRet (*PohMonitorDefaultSet)(Tha60210011ModuleSdh self);
    uint32 (*SerdesPrbsBaseAddress)(Tha60210011ModuleSdh self, AtSerdesController serdes);
    AtSdhLine (*LineFromLoLineStsGet)(Tha60210011ModuleSdh self, uint8 loSlice, uint8 loHwStsInSlice, uint8 *stsInLine);
    eBool (*SdhChannelIsHoPath)(Tha60210011ModuleSdh self, AtSdhChannel channel);
    eBool (*SdhVcBelongsToLoLine)(Tha60210011ModuleSdh self, AtSdhChannel vc);

    /* Channel factory */
    AtSdhChannel (*AuVcObjectCreate)(Tha60210011ModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId);
    AtSdhChannel (*TugObjectCreate)(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId);
    AtSdhChannel (*TuObjectCreate)(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId);
    AtSdhChannel (*Tu3VcObjectCreate)(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId);
    AtSdhChannel (*Vc1xObjectCreate)(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId);
    }tTha60210011ModuleSdhMethods;

typedef struct tTha60210011LoLineDatabase
    {
    eBool stsIsInUsed[cNumStsInLoLine];
    }tTha60210011LoLineDatabase;

typedef struct tTha60210011ModuleSdh
    {
    tTha60150011ModuleSdh super;
    const tTha60210011ModuleSdhMethods *methods;

    /* Private data */
    tTha60210011LoLineDatabase* allLoLineDb;

    /*Async init*/
    uint32 asyncInitState;
    }tTha60210011ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60210011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULESDHINTERNAL_H_ */

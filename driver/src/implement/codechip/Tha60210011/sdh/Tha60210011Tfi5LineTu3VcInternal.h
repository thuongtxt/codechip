/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011Tfi5LineTu3VcInternal.h
 * 
 * Created Date: Sep 10, 2015
 *
 * Description : Abstract class of PWCodechip-60210011 SDH Tfi5LineTu3Vc.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5LINETU3VCINTERNAL_H_
#define _THA60210011TFI5LINETU3VCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5LineTu3Vc
    {
    tThaSdhTu3Vc super;

    /* Private data */
    ThaSdhPointerAccumulator pointerAccumulator;
    }tTha60210011Tfi5LineTu3Vc;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210011Tfi5LineTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011TFI5LINETU3VCINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineTu.c
 *
 * Created Date: Apr 22, 2015
 *
 * Description : Concrete class of PWCodechip-60210011 TU.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "Tha60210011ModuleSdh.h"
#include "../poh/Tha60210011PohReg.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../poh/Tha60210011PohProcessorInternal.h"
#include "Tha60210011Tfi5LineTuInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSdhModule(self) ((AtModuleSdh)AtChannelModuleGet((AtChannel)self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(AtSdhPath self)
    {
    return Tha60210011ModuleOcnVtDefaultOffset((AtSdhChannel)self);
    }

static void InterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    AtSdhPath pohProcessor = (AtSdhPath)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    AtSdhPathInterruptProcess(pohProcessor, slice, sts, vt, hal);
    }

static ThaPohProcessor PohProcessor(AtSdhChannel self)
    {
    return ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet(self, 0));
    }

static eBool HoldOffTimerSupported(AtSdhChannel self)
    {
    return AtSdhChannelHoldOffTimerSupported((AtSdhChannel)PohProcessor(self));
    }

static eBool HoldOnTimerSupported(AtSdhChannel self)
    {
    return AtSdhChannelHoldOnTimerSupported((AtSdhChannel)PohProcessor(self));
    }

static uint32 DefectGet(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    uint32 defects = AtChannelDefectGet(pohProcessor);

    /* Convert it to software value */
    if (defects & cAtSdhPathAlarmAis)
        return cAtSdhPathAlarmAis;
    if (defects & cAtSdhPathAlarmLop)
        return cAtSdhPathAlarmLop;

    return defects;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    if (pohProcessor)
    	return AtChannelSupportedInterruptMasks(pohProcessor);
    return 0;
    }

static Tha60210011ModuleOcn OcnModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (Tha60210011ModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static uint32 AddressWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    uint32 offset = Tha60210011ModuleOcnVtDefaultOffset((AtSdhChannel)self);
    return localAddress + offset;
    }

static uint32 PointerInterpreterEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 localBase = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelInterrupt(OcnModule(self), (AtSdhChannel)self);
    uint32 regAddr = AddressWithLocalAddress(self, localBase);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 events = 0;

    if (regVal & cAf6_upvtchstkram_VtPiStsNewDetIntr_Mask)
        events |= cAtSdhPathAlarmNewPointer;

    if (regVal & cAf6_upvtchstkram_VtPiStsNdfIntr_Mask)
        events |= cAtSdhPathAlarmPiNdf;

    if (read2Clear)
        {
        regVal = cAf6_upvtchstkram_VtPiStsNewDetIntr_Mask |
                 cAf6_upvtchstkram_VtPiStsNdfIntr_Mask;
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return events;
    }

static uint32 PointerGeneratorEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 localBase = Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelInterrupt(OcnModule(self), (AtSdhChannel)self);
    uint32 regAddr = AddressWithLocalAddress(self, localBase);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 events = 0;
    uint32 mask = Tha60210011ModuleOcnVtTuPointerGeneratorNdfIntrMask(OcnModule(self));

    if (regVal & mask)
        events |= cAtSdhPathAlarmPgNdf;

    if (read2Clear)
        mChannelHwWrite(self, regAddr, mask, cThaModuleOcn);

    return events;
    }

static uint32 PointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 piEvents = PointerInterpreterEventRead2Clear(self, read2Clear);
    uint32 pgEvents = PointerGeneratorEventRead2Clear(self, read2Clear);
    return piEvents | pgEvents;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 defects = 0;
    AtChannel pohProcessor = (AtChannel)PohProcessor((AtSdhChannel)self);

    defects |= AtChannelDefectInterruptGet(pohProcessor);
    defects |= PointerEventRead2Clear(self, cAtFalse);

    return defects;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 defects = 0;
    AtChannel pohProcessor = (AtChannel)PohProcessor((AtSdhChannel)self);

    defects |= AtChannelDefectInterruptClear(pohProcessor);
    defects |= PointerEventRead2Clear(self, cAtTrue);

    return defects;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtChannel pohProcessor = (AtChannel)PohProcessor((AtSdhChannel)self);
    return AtChannelInterruptMaskSet(pohProcessor, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)PohProcessor((AtSdhChannel)self);
    return AtChannelInterruptMaskGet(pohProcessor);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)PohProcessor((AtSdhChannel)self);
    return AtChannelInterruptEnable(pohProcessor);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)PohProcessor((AtSdhChannel)self);
    return AtChannelInterruptDisable(pohProcessor);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtChannel pohProcessor = (AtChannel)ThaSdhVcPohProcessorGet((AtSdhVc)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    return AtChannelInterruptIsEnabled(pohProcessor);
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void AlarmDisplay(AtChannel self)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
	uint32 regAddr = Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelInterrupt(ocnModule, (AtSdhChannel)self) + DefaultOffset((AtSdhPath)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal == 0)
        return;

    AtPrintc(cSevInfo, "\r\nSticky Alarm of Tx-Path: ");

    if (regVal & cAf6_vtpgstkram_VtPgAisIntr_Mask)
        AtPrintc(cSevCritical, "AIS ");
    if (regVal & cAf6_vtpgstkram_VtPgFiFoOvfIntr_Mask)
        AtPrintc(cSevCritical, "FiFoOvf ");

    AtPrintc(cSevCritical, "\r\n");

    /* Only clear non-public events. */
    regVal = cAf6_vtpgstkram_VtPgAisIntr_Mask|cAf6_vtpgstkram_VtPgFiFoOvfIntr_Mask;
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    }

static eAtRet Debug(AtChannel self)
	{
    m_AtChannelMethods->Debug(self);
    AlarmDisplay(self);
    return cAtOk;
	}

static uint32 OcnVtTuPointerInterpreterPerChannelControl(AtSdhPath self)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    return Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(ocnModule, (AtSdhChannel)self);
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    uint32 address, regVal;

    address = OcnVtTuPointerInterpreterPerChannelControl((AtSdhPath)self) + DefaultOffset((AtSdhPath)self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_vpiramctl_VtPiAdjRule_,  0); /* 8 of 10 only */
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static void OverrideAtSdhPath(AtSdhTu self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, InterruptProcess);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtSdhChannel(AtSdhTu self)
    {
    AtSdhChannel path = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(path), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, HoldOnTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        }

    mMethodsSet(path, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhTu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods =  mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhTu self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5LineTu);
    }

AtSdhTu Tha60210011Tfi5LineTuObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhTuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhTu Tha60210011Tfi5LineTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhTu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5LineTuObjectInit(self, channelId, channelType, module);
    }

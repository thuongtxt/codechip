/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5LineInternal.h
 *
 * Created Date: Apr 22, 2015
 *
 * Description : Abstract class of PWCodechip-60210011 SDH line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5LINEINTERNAL_H_
#define _THA60210011TFI5LINEINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaSdhLineInternal.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5Line * Tha60210011Tfi5Line;

typedef struct tTha60210011Tfi5LineMethods
    {
    uint32 (*Rxfrmb1cntr2cBase)(Tha60210011Tfi5Line self);
    uint32 (*Rxfrmb1cntroBase)(Tha60210011Tfi5Line self);
    uint32 (*RxfrmstaBase)(Tha60210011Tfi5Line self);
    uint32 (*RxfrmstkBase)(Tha60210011Tfi5Line self);
    }tTha60210011Tfi5LineMethods;

typedef struct tTha60210011Tfi5Line
    {
    tThaSdhLine super;
    const tTha60210011Tfi5LineMethods *methods;
    }tTha60210011Tfi5Line;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtSdhLine Tha60210011Tfi5LineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210011TFI5LINEINTERNAL_H_ */

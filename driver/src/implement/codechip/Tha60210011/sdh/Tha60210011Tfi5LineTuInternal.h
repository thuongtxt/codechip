/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011Tfi5LineTuInternal.h
 * 
 * Created Date: Jul 19, 2016
 *
 * Description : SDH TU internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5LINETUINTERNAL_H_
#define _THA60210011TFI5LINETUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhTuInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5LineTu
    {
    tThaSdhTu super;
    }tTha60210011Tfi5LineTu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhTu Tha60210011Tfi5LineTuObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011TFI5LINETUINTERNAL_H_ */


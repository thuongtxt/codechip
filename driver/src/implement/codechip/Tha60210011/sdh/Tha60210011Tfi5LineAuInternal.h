/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011Tfi5LineAuInternal.h
 * 
 * Created Date: Sep 21, 2015
 *
 * Description : TFI5 line AU internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5LINEAUINTERNAL_H_
#define _THA60210011TFI5LINEAUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhAuInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5LineAu * Tha60210011Tfi5LineAu;

typedef struct tTha60210011Tfi5LineAuMethods
    {
    void (*AlarmDisplay)(Tha60210011Tfi5LineAu self);

    /* Although some contiguous concatenations are regular, but hardware has limited to support it. */
    eBool (*CanConcate)(Tha60210011Tfi5LineAu self, AtSdhChannel* slaveList, uint8 numSlaves);
    }tTha60210011Tfi5LineAuMethods;

typedef struct tTha60210011Tfi5LineAu
    {
    tThaSdhAu super;
    tTha60210011Tfi5LineAuMethods *methods;

    /* Private data */
    /* To handle squelching */
    uint8 txAisExplicitlyForced;
    }tTha60210011Tfi5LineAu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha60210011Tfi5LineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011TFI5LINEAUINTERNAL_H_ */


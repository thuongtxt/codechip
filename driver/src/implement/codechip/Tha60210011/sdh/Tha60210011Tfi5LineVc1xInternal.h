/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210011Tfi5LineVc1xInternal.h
 * 
 * Created Date: Oct 5, 2015
 *
 * Description : 60210011 vc1x internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5LINEVC1XINTERNAL_H_
#define _THA60210011TFI5LINEVC1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhVcInternal.h"
#include "Tha60210011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5LineVc1x
    {
    tThaSdhVc1x super;

    /* Private data */
    ThaSdhPointerAccumulator pointerAccumulator; /* store pointer counter for RO */
    }tTha60210011Tfi5LineVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210011Tfi5LineVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011TFI5LINEVC1XINTERNAL_H_ */


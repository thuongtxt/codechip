/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60210011ModuleCos.c
 *
 * Created Date: May 6, 2015
 *
 * Description : COS Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePweInternal.h"
#include "../pwe/Tha60210011ModulePlaReg.h"
#include "../man/Tha60210011Device.h"
#include "../pwe/Tha60210011ModulePweReg.h"
#include "../pwe/Tha60210011ModulePwe.h"
#include "Tha60210011ModuleCosInternal.h"
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleCosMethods m_ThaModuleCosOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaCosEthPortController EthPortControllerCreate(ThaModuleCos self)
    {
    return Tha60210011CosEthPortControllerNew(self);
    }

static eAtRet PwRtpPayloadTypeSet(ThaModuleCos self, AtPw pw, uint8 payloadType)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(payloadType);

    /* In this product, PW header controller will take this job,
     * override here just for readable purpose */
    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsCosRegister(AtModuleDeviceGet(self), address);
    }

static uint8 PwRtpPayloadTypeGet(ThaModuleCos self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* In this product, PW header controller will take this job,
     * override here just for readable purpose */
    return 0;
    }

static eAtRet PwRtpSsrcSet(ThaModuleCos self, AtPw pw, uint32 ssrc)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(ssrc);

    /* In this product, PW header controller will take this job,
     * override here just for readable purpose */
    return cAtOk;
    }

static uint32 PwRtpSsrcGet(ThaModuleCos self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);

    /* In this product, PW header controller will take this job,
     * override here just for readable purpose */
    return 0;
    }

static AtIpCore Core(ThaModuleCos self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    }

static ThaModulePwe PweModule(ThaModuleCos self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    }

static AtModuleEth EthModule(ThaModuleCos self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    }

static eAtRet PwHeaderLengthResetOnPort(ThaModuleCos self, uint8 partId, uint32 localPwId, uint8 porId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = 65536UL * porId + localPwId;
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + offset + Tha60210011ModulePlaBaseAddress();
    ThaModulePwe pweModule = (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    AtUnused(partId);

    mModuleHwLongRead(pweModule, regAddr, longRegVal, cThaLongRegMaxSize, Core(self));
    mRegFieldSet(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_, 0x0);
    mModuleHwLongWrite(pweModule, regAddr, longRegVal, cThaLongRegMaxSize, Core(self));

    return cAtOk;
    }

static eAtRet PwHeaderLengthReset(ThaModuleCos self, uint8 partId, uint32 localPwId)
    {
    AtModuleEth ethModule = EthModule(self);
    uint8 numPort = AtModuleEthMaxPortsGet(ethModule);
    uint8 port_i;
    for (port_i = 0; port_i < numPort; port_i++)
        {
        if (ThaModuleEthPortIsInternalMac((ThaModuleEth)ethModule, port_i))
        PwHeaderLengthResetOnPort(self, partId, localPwId, port_i);
        }

    return cAtOk;
    }

static uint8 HeaderLengthWriteToHw(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)PweModule(self);
    return (uint8)(hdrLenInByte + Tha60210011ModulePweNumberOfAddtionalByte(modulePwe, pw));
    }

static eAtRet PwHeaderLengthHwSet(ThaModuleCos self, AtPw pw, uint8 hwHdrLenValue)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset(PweModule(self), pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_, hwHdrLenValue);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwHeaderLengthSet(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    /* Can not set header length if pw has not been bound Ethernet port */
    if (ThaPwAdapterEthPortGet((ThaPwAdapter)pw) == NULL)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "%s does not have eth port\r\n",
                    AtObjectToString((AtObject)pw));
        return cAtErrorNullPointer;
        }

    return mMethodsGet(self)->PwHeaderLengthHwSet(self, pw, HeaderLengthWriteToHw(self, pw, hdrLenInByte));
    }

static uint8 RealHeaderLength(ThaModuleCos self, AtPw pw, uint8 hwValue)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)PweModule(self);
    uint8 numAdditionalByte = Tha60210011ModulePweNumberOfAddtionalByte(modulePwe, pw);

    if (hwValue < numAdditionalByte)
        return 0;

    return (uint8)(hwValue - numAdditionalByte);
    }

static uint8 PwHeaderLengthHwGet(ThaModuleCos self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset(PweModule(self), pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    return (uint8)mRegField(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_);
    }

static uint8 PwHeaderLengthGet(ThaModuleCos self, AtPw pw)
    {
    uint8 hwLength = mMethodsGet(self)->PwHeaderLengthHwGet(self, pw);
    return RealHeaderLength(self, pw, hwLength);
    }

static eAtRet PwCwLengthModeSet(ThaModuleCos self, AtPw pw, eAtPwCwLengthMode lengthMode)
    {
    uint32 regAddr, regVal;
    uint8 hwLengthMode = (lengthMode == cAtPwCwLengthModeFullPacket) ? 0 : 1;

    if (lengthMode == cAtPwCwLengthModeInvalid)
        return cAtErrorModeNotSupport;

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(PweModule(self), pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPadMode_, hwLengthMode);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtPwCwLengthMode PwCwLengthModeGet(ThaModuleCos self, AtPw pw)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(PweModule(self), pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    uint8 hwLengthMode = (uint8)mRegField(regVal, cAf6_pw_txeth_hdr_mode_TxEthPadMode_);

    if (hwLengthMode == 0)
        return cAtPwCwLengthModeFullPacket;

    if (hwLengthMode == 1)
        return cAtPwCwLengthModePayload;

    return cAtPwCwLengthModeInvalid;
    }

static eBool PwCwLengthModeIsSupported(ThaModuleCos self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(self);
    if ((lengthMode == cAtPwCwLengthModeFullPacket) ||
        (lengthMode == cAtPwCwLengthModePayload))
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCos(AtModule self)
    {
    ThaModuleCos cos = (ThaModuleCos)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCosOverride, mMethodsGet(cos), sizeof(m_ThaModuleCosOverride));

        mMethodOverride(m_ThaModuleCosOverride, EthPortControllerCreate);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpPayloadTypeSet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpPayloadTypeGet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpPayloadTypeSet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpSsrcSet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpSsrcGet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthGet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthHwSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthHwGet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthReset);
        mMethodOverride(m_ThaModuleCosOverride, PwCwLengthModeSet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwLengthModeGet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwLengthModeIsSupported);
        }

    mMethodsSet(cos, &m_ThaModuleCosOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCos(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleCos);
    }

AtModule Tha60210011ModuleCosObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCosV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModuleCosNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleCosObjectInit(newModule, device);
    }

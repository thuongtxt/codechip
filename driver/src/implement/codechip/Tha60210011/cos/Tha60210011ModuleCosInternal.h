/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210011ModuleCosInternal.h
 * 
 * Created Date: Jul 6, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_COS_THA60210011MODULECOSINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_COS_THA60210011MODULECOSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cos/ThaModuleCosInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleCos
    {
    tThaModuleCosV2 super;
    }tTha60210011ModuleCos;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModuleCosObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_COS_THA60210011MODULECOSINTERNAL_H_ */


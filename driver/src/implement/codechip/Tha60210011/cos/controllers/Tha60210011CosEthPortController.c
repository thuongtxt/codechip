/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60210011CosEthPortController.c
 *
 * Created Date: May 6, 2015
 *
 * Description : COS ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cos/controllers/ThaCosEthPortControllerInternal.h"
#include "../../pwe/Tha60210011ModulePweReg.h"
#include "../../pwe/Tha60210011ModulePwe.h"
#include "../../../../default/pwe/ThaModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011CosEthPortController
    {
    tThaCosEthPortControllerV2 super;
    }tTha60210011CosEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCosEthPortControllerMethods m_ThaCosEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaCosControllerModuleGet((ThaCosController)self));
    ThaModulePwe pweModule = (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    return Tha60210012ModulePwePortCounterOffset((Tha60210011ModulePwe)pweModule, port, r2c);
    }

static uint32 EthPortOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_eth_tx_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_byte_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt0_64_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt65_127_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt128_255_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt256_511_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt512_1023_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt1024_1518_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom1529to2047bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt1519_2047_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_jumbo_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktUnicastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_Unicast_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktBcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_eth_tx_bcast_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktMcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_eth_tx_mcast_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCos);
    }

static void OverrideThaCosEthPortController(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaCosEthPortControllerOverride));

        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingbyteRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom1529to2047bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktJumboLenRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktUnicastPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktBcastPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktMcastPktRead2Clear);
        }

    mMethodsSet(self, &m_ThaCosEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideThaCosEthPortController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011CosEthPortController);
    }

static ThaCosEthPortController ObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosEthPortControllerV2ObjectInit(self, cos) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController Tha60210011CosEthPortControllerNew(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cos);
    }

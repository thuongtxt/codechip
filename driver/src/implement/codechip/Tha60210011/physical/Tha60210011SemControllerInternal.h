/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60210011SemControllerInternal.h
 * 
 * Created Date: Oct 19, 2016
 *
 * Description : Sem controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011SEMCONTROLLERINTERNAL_H_
#define _THA60210011SEMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/physical/ThaSemControllerV3Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011SemController
    {
    tThaSemControllerV3 super;
    char uartBuffer[2048];
    }tTha60210011SemController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60210011SemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011SEMCONTROLLERINTERNAL_H_ */


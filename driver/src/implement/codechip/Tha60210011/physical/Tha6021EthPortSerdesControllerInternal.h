/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6021EthPortSerdesControllerInternal.h
 * 
 * Created Date: May 7, 2016
 *
 * Description : Tha6021EthPortSerdesController declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021ETHPORTSERDESCONTROLLERINTERNAL_H_
#define _THA6021ETHPORTSERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cEqualizerLpmMode           0x0104
#define cEqualizerDfeMode           0x0954
#define cEqualizerModeRegister      0x29

#define cReceiveLinkStatusMask cBit2

#define cMdioRegRegister(page, address) page, address
#define cMdioRegPmaPmdStatus1Register cMdioRegRegister(1, 1)
#define cMdioRegPmaPmdControl1Register cMdioRegRegister(1, 0)

#define cPmaResetMask     cBit15
#define cPmaLoopbackMask  cBit0
#define cPmaLoopbackShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021EthPortSerdesController * Tha6021EthPortSerdesController;

typedef struct tTha6021EthPortSerdesControllerMethods
    {
    uint32 (*TxPreCursorAddress)(Tha6021EthPortSerdesController self);
    uint32 (*TxPostCursorAddress)(Tha6021EthPortSerdesController self);
    uint32 (*PreCursorMask)(Tha6021EthPortSerdesController self, uint32 serdesId);
    uint32 (*PreCursorShift)(Tha6021EthPortSerdesController self, uint32 serdesId);
    uint32 (*PostCursorMask)(Tha6021EthPortSerdesController self, uint32 serdesId);
    uint32 (*PostCursorShift)(Tha6021EthPortSerdesController self, uint32 serdesId);

    uint32 (*TxDiffCtrlAddress)(Tha6021EthPortSerdesController self);
    uint32 (*DiffCtrlMask)(Tha6021EthPortSerdesController self, uint32 serdesId);
    uint32 (*DiffCtrlShift)(Tha6021EthPortSerdesController self, uint32 serdesId);

    uint32 (*LpmEnableRegister)(Tha6021EthPortSerdesController self);
    uint32 (*LpmEnableMask)(Tha6021EthPortSerdesController self, uint32 serdesId);
    uint32 (*LpmEnableShift)(Tha6021EthPortSerdesController self, uint32 serdesId);
    eAtRet (*LpmDfeReset)(Tha6021EthPortSerdesController self);
    eAtRet (*CoefReConfigure)(Tha6021EthPortSerdesController self, eAtSerdesEqualizerMode mode);

    eAtRet (*HwLoopbackModeSet)(AtSerdesController self, uint8 loopbackMode);
    uint8 (*HwLoopbackModeGet)(AtSerdesController self);
    eBool (*ShouldInitPrbsEngine)(Tha6021EthPortSerdesController self);
    }tTha6021EthPortSerdesControllerMethods;

typedef struct tTha6021EthPortSerdesController
    {
    tAtSerdesController super;
    const tTha6021EthPortSerdesControllerMethods *methods;
    }tTha6021EthPortSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6021EthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021ETHPORTSERDESCONTROLLERINTERNAL_H_ */


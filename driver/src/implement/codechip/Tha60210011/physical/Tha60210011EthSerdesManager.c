/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210011EthSerdesManager.c
 *
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6021Physical.h"
#include "Tha60210011EthSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthSerdesManagerMethods m_ThaEthSerdesManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController SerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210011EthPortXfiSerdesControllerNew(port, serdesId);
    }

static eBool ShouldEnableSerdes(ThaEthSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static uint32 NumSerdesController(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return 8;
    }

static void OverrideThaEthSerdesManager(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthSerdesManagerOverride, mMethodsGet(self), sizeof(m_ThaEthSerdesManagerOverride));

        mMethodOverride(m_ThaEthSerdesManagerOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaEthSerdesManagerOverride, ShouldEnableSerdes);
        mMethodOverride(m_ThaEthSerdesManagerOverride, NumSerdesController);
        }

    mMethodsSet(self, &m_ThaEthSerdesManagerOverride);
    }

static void Override(ThaEthSerdesManager self)
    {
    OverrideThaEthSerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011EthSerdesManager);
    }

ThaEthSerdesManager Tha60210011EthSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthSerdesManagerObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthSerdesManager Tha60210011EthSerdesManagerNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthSerdesManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return Tha60210011EthSerdesManagerObjectInit(newManager, ethModule);
    }

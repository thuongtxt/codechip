/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6021EthPortSerdesController.c
 *
 * Created Date: May 7, 2016
 *
 * Description : Tha6021EthPortSerdesController implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6021EthPortSerdesControllerInternal.h"
#include "Tha6021Physical.h"
#include "../../../default/physical/ThaEthSerdesManager.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../../generic/eth/AtModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6021EthPortSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6021EthPortSerdesControllerMethods m_methods;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    AtUnused(self);

    if (param == cAtSerdesParamTxPreCursor)
        return (value > 31) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxPostCursor)
        return (value > 31) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxDiffCtrl)
        return (value > 15) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static eAtRet HwTxPreCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 address = mMethodsGet(mThis(self))->TxPreCursorAddress(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 mask = mMethodsGet(mThis(self))->PreCursorMask(mThis(self), serdesId);
    uint32 shift = mMethodsGet(mThis(self))->PreCursorShift(mThis(self), serdesId);

    mFieldIns(&regVal, mask, shift, value);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet HwTxPostCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 address = mMethodsGet(mThis(self))->TxPostCursorAddress(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 mask = mMethodsGet(mThis(self))->PostCursorMask(mThis(self), serdesId);
    uint32 shift = mMethodsGet(mThis(self))->PostCursorShift(mThis(self), serdesId);

    mFieldIns(&regVal, mask, shift, value);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 HwTxPreCursorGet(AtSerdesController self)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 address = mMethodsGet(mThis(self))->TxPreCursorAddress(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 mask = mMethodsGet(mThis(self))->PreCursorMask(mThis(self), serdesId);
    uint32 shift = mMethodsGet(mThis(self))->PreCursorShift(mThis(self), serdesId);
    uint32 value;
    mFieldGet(regVal, mask, shift, uint32, &value);

    return value;
    }

static uint32 HwTxPostCursorGet(AtSerdesController self)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 address = mMethodsGet(mThis(self))->TxPostCursorAddress(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 mask = mMethodsGet(mThis(self))->PostCursorMask(mThis(self), serdesId);
    uint32 shift = mMethodsGet(mThis(self))->PostCursorShift(mThis(self), serdesId);
    uint32 value;
    mFieldGet(regVal, mask, shift, uint32, &value);

    return value;
    }

static uint32 PreCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return cBit4_0 << (serdesId * 8);
    }

static uint32 PreCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return serdesId * 8;
    }

static uint32 PostCursorMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    return mMethodsGet(self)->PreCursorMask(self, serdesId);
    }

static uint32 PostCursorShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    return mMethodsGet(self)->PreCursorShift(self, serdesId);
    }

static uint32 DiffCtrlMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return cBit3_0 << (serdesId * 4);
    }

static uint32 DiffCtrlShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return serdesId * 4;
    }

static eAtRet HwTxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 address = mMethodsGet(mThis(self))->TxDiffCtrlAddress(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 mask = mMethodsGet(mThis(self))->DiffCtrlMask(mThis(self), serdesId);
    uint32 shift = mMethodsGet(mThis(self))->DiffCtrlShift(mThis(self), serdesId);
    mFieldIns(&regVal, mask, shift, value);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 HwTxDiffCtrlGet(AtSerdesController self)
    {
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 address = mMethodsGet(mThis(self))->TxDiffCtrlAddress(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    uint32 mask = mMethodsGet(mThis(self))->DiffCtrlMask(mThis(self), serdesId);
    uint32 shift = mMethodsGet(mThis(self))->DiffCtrlShift(mThis(self), serdesId);
    uint32 value;
    mFieldGet(regVal, mask, shift, uint32, &value);
    return value;
    }

static eAtRet HwPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxPreCursorSet(self, value);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxPostCursorSet(self, value);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlSet(self, value);

    return cAtOk;
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor : return cAtTrue;
        case cAtSerdesParamTxPostCursor: return cAtTrue;
        case cAtSerdesParamTxDiffCtrl  : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 HwPhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxPreCursorGet(self);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxPostCursorGet(self);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlGet(self);

    return cInvalidUint32;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (mMethodsGet(self)->PhysicalParamValueIsInRange(self, param, value))
        return HwPhysicalParamSet(self, param, value);
    return cAtErrorOutOfRangParm;
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    return HwPhysicalParamGet(self, param);
    }

static uint32 LpmEnableMask(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return cBit0 << serdesId;
    }

static uint32 LpmEnableShift(Tha6021EthPortSerdesController self, uint32 serdesId)
    {
    AtUnused(self);
    return serdesId;
    }

static void MdioSelect(AtSerdesController self, AtMdio mdio)
    {
    AtMdioPortSelect(mdio, mMethodsGet(self)->HwIdGet(self));
    }

static eAtRet IpReset(AtSerdesController self)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    uint32 elapse = 0;
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    MdioSelect(self, mdio);
    regVal  = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
    regVal |= cPmaResetMask;
    AtMdioWrite(mdio, cMdioRegPmaPmdControl1Register, regVal);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        regVal = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
        if ((regVal & cPmaResetMask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet CoefReConfigure(Tha6021EthPortSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtSerdesController controller = (AtSerdesController)self;
    AtDrp drp = AtSerdesControllerDrp(controller);
    AtDrpPortSelect(drp, AtSerdesControllerIdGet(controller));
    AtDrpWrite(drp, cEqualizerModeRegister, Tha6021EthPortSerdesEqualizerModeCoeficSw2Hw(mode));
    return cAtOk;
    }

static eAtRet LpmDfeReset(Tha6021EthPortSerdesController self)
    {
    return IpReset((AtSerdesController)self);
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    eAtRet ret;
    uint32 regVal, address, serdesId, mask, shift;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    /* Step1: switch RXLPMEN: 0(DFE), 1(LPM) */
    serdesId = mMethodsGet(self)->HwIdGet(self);
    address = mMethodsGet(mThis(self))->LpmEnableRegister(mThis(self));
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mask = mMethodsGet(mThis(self))->LpmEnableMask(mThis(self), serdesId);
    shift = mMethodsGet(mThis(self))->LpmEnableShift(mThis(self), serdesId);
    mFieldIns(&regVal, mask, shift, Tha6021EthPortSerdesEqualizerModeSw2Hw(mode));
    AtSerdesControllerWrite(self, address, regVal, cAtModuleEth);

    /* Step2: set coefficience */
    ret = mMethodsGet(mThis(self))->CoefReConfigure(mThis(self), mode);

    /* Step3: reset PMA by MDIO */
    ret |= mMethodsGet(mThis(self))->LpmDfeReset(mThis(self));
    return ret;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 regVal, address, serdesId, mask, shift, lpmEnabled;

    serdesId = mMethodsGet(self)->HwIdGet(self);
    address = mMethodsGet(mThis(self))->LpmEnableRegister(mThis(self));
    regVal = AtSerdesControllerRead(self, address, cAtModuleEth);
    mask = mMethodsGet(mThis(self))->LpmEnableMask(mThis(self), serdesId);
    shift = mMethodsGet(mThis(self))->LpmEnableShift(mThis(self), serdesId);
    mFieldGet(regVal, mask, shift, uint32, &lpmEnabled);

    return Tha6021EthPortSerdesEqualizerModeHw2Sw(lpmEnabled);
    }

static AtModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModuleEth);
    }

static ThaEthSerdesManager SerdesManager(AtSerdesController self)
    {
    return ThaModuleEthSerdesManager((ThaModuleEth)EthModule(self));
    }

static eBool LpmSupported(AtSerdesController self)
    {
    return ThaEthSerdesManagerCanControlEqualizer(SerdesManager(self), self);
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    return LpmSupported(self);
    }

static AtDrp Drp(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthSerdesDrp(ethModule, AtSerdesControllerIdGet(self));
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthSerdesMdio(ethModule, self);
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)
        return cAtTrue;

    if ((mode == cAtSerdesEqualizerModeLpm) && LpmSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool IsControllable(AtSerdesController self)
    {
    ThaEthSerdesManager manager = SerdesManager(self);
    return ThaEthSerdesManagerSerdesIsUsed(manager, self);
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeGe) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeGe;
    }

static eBool ShouldInitPrbsEngine(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, Drp);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static void MethodsInit(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TxPreCursorAddress);
        mMethodOverride(m_methods, TxPostCursorAddress);
        mMethodOverride(m_methods, TxDiffCtrlAddress);
        mMethodOverride(m_methods, LpmEnableRegister);
        mMethodOverride(m_methods, PreCursorMask);
        mMethodOverride(m_methods, PreCursorShift);
        mMethodOverride(m_methods, PostCursorMask);
        mMethodOverride(m_methods, PostCursorShift);
        mMethodOverride(m_methods, DiffCtrlMask);
        mMethodOverride(m_methods, DiffCtrlShift);
        mMethodOverride(m_methods, LpmEnableMask);
        mMethodOverride(m_methods, LpmEnableShift);
        mMethodOverride(m_methods, CoefReConfigure);
        mMethodOverride(m_methods, LpmDfeReset);
        mMethodOverride(m_methods, ShouldInitPrbsEngine);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021EthPortSerdesController);
    }

AtSerdesController Tha6021EthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesControllerObjectInit(self, (AtChannel)ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

eBool Tha6021PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    if (self)
        return PhysicalParamIsSupported(self, param);
    return cAtFalse;
    }

uint32 Tha6021EthPortSerdesEqualizerModeCoeficSw2Hw(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)  return cEqualizerDfeMode;
    if (mode == cAtSerdesEqualizerModeLpm)  return cEqualizerLpmMode;

    return 0x0;
    }

uint32 Tha6021EthPortSerdesEqualizerModeSw2Hw(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe) return 0;
    if (mode == cAtSerdesEqualizerModeLpm) return 1;

    return 0x0;
    }

eAtSerdesEqualizerMode Tha6021EthPortSerdesEqualizerModeHw2Sw(uint32 mode)
    {
    if (mode == 0) return cAtSerdesEqualizerModeDfe;
    if (mode == 1) return cAtSerdesEqualizerModeLpm;
    return cAtSerdesEqualizerModeUnknown;
    }

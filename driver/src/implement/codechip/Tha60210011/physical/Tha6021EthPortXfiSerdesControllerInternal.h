/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6021EthPortXfiSerdesControllerInternal.h
 * 
 * Created Date: Jul 12, 2015
 *
 * Description : SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021ETHPORTXFISERDESCONTROLLERINTERNAL_H_
#define _THA6021ETHPORTXFISERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "Tha6021EthPortSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021EthPortXfiSerdesController * Tha6021EthPortXfiSerdesController;

typedef struct tTha6021EthPortXfiSerdesControllerMethods
    {
    eBool (*ShouldUseMdioForEnabling)(Tha6021EthPortXfiSerdesController self);
    eBool (*FpgaAlarmSupportted)(Tha6021EthPortXfiSerdesController self);
    uint32 (*TxSerdesEnableMask)(uint32 serdesHwId);
    uint32 (*TxSerdesEnableShift)(uint32 serdesHwId);
    }tTha6021EthPortXfiSerdesControllerMethods;

typedef struct tTha6021EthPortXfiSerdesController
    {
    tTha6021EthPortSerdesController super;
    const tTha6021EthPortXfiSerdesControllerMethods *methods;

    /* Debug */
    uint32 maxSerdesResetTimeMs;
    }tTha6021EthPortXfiSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6021EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021ETHPORTXFISERDESCONTROLLERINTERNAL_H_ */


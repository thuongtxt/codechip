/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210011EthPortXfiSerdesController.c
 *
 * Created Date: Oct 30, 2015
 *
 * Description : XFI SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011EthPortXfiSerdesControllerInternal.h"
#include "../man/Tha60210011DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods                m_AtSerdesControllerOverride;
static tTha6021EthPortXfiSerdesControllerMethods m_Tha6021EthPortXfiSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldUseMdioForEnabling(Tha6021EthPortXfiSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FpgaAlarmSupportted(Tha6021EthPortXfiSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 AlarmRead2Clear(AtSerdesController self, eBool read2Clear)
    {
    uint32 regVal;
    uint32 portMask = cXfiFailMask(mMethodsGet(self)->HwIdGet(self));
    uint32 alarms = 0;

    regVal = AtSerdesControllerRead(self, cFpgaStickyRegister, cAtModuleEth);
    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeNotSync;
    if (read2Clear && (alarms & cAtSerdesAlarmTypeNotSync))
        AtSerdesControllerWrite(self, cFpgaStickyRegister, portMask, cAtModuleEth);

    return alarms;
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtTrue);
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return cAtFalse;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortXfiSerdesController(AtSerdesController self)
    {
    Tha6021EthPortXfiSerdesController controller = (Tha6021EthPortXfiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortXfiSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortXfiSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, ShouldUseMdioForEnabling);
        mMethodOverride(m_Tha6021EthPortXfiSerdesControllerOverride, FpgaAlarmSupportted);
        }

    mMethodsSet(controller, &m_Tha6021EthPortXfiSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortXfiSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011EthPortXfiSerdesController);
    }

AtSerdesController Tha60210011EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortXfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210011EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011EthPortXfiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

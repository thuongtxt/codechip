/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210011EthSerdesManagerInternal.h
 * 
 * Created Date: Oct 24, 2015
 *
 * Description : SERDES manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011ETHSERDESMANAGERINTERNAL_H_
#define _THA60210011ETHSERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/physical/ThaEthSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011EthSerdesManager
    {
    tThaEthSerdesManager super;
    }tTha60210011EthSerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthSerdesManager Tha60210011EthSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011ETHSERDESMANAGERINTERNAL_H_ */


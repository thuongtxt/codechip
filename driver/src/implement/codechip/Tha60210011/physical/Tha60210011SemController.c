/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60210011SemControllerV2.c
 *
 * Created Date: Apr 14, 2016
 *
 * Description : Tha60210011SemControllerV2 implementations
 *
 * Notes       : Following is output for reference to understand how this parsing works
 *
 * Q command result from the Ultrascale:
 * AT SDK > device sem uart send 1 Q C000000000
 * Processing UART command for UART #1: "Q C000000000"
 * AT SDK > device sem uart receive 1
 * Received data as below: (length = 1123)
 * Q C000000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 * 00000000
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60210011Device.h"
#include "Tha6021Physical.h"
#include "Tha60210011SemControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSeuPciCorrectableMask cBit10

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011SemController *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tThaSemControllerMethods m_ThaSemControllerOverride;
static tAtSemControllerMethods  m_AtSemControllerOverride;

/* Super implementations */
static const tAtObjectMethods         *m_AtObjectMethods        = NULL;
static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(ThaSemController self)
    {
    return 0xF44000 + (0x100 * AtSemControllerIdGet((AtSemController)self));
    }

static eBool NeedUseCmdCtrlDoneBit(ThaSemController self)
    {
    AtDevice device = AtSemControllerDeviceGet((AtSemController)self);
    return Tha60150011DeviceSemUartIsSupported(device);
    }

static uint32 DefaultFrameWordAddress(ThaSemController self)
    {
    /* Recommended to inject in ECC */
    AtUnused(self);
    return 61;
    }

static eAtRet MoveObservationModeBeforeActiveChecking(AtSemController self)
    {
    return (ThaSemControllerMoveToState(self, cFsmOBSERVATION) == cAtTrue) ? cAtOk : cAtErrorSemNotActive;
    }

static void ClearCorrectableSticky(ThaSemController self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSemControllerDeviceGet((AtSemController)self), 0);
    AtHalWrite(hal, ThaSemControllerHistoryAlarmRegister(self) + mDefaultOffset(self), cSeuPciCorrectableMask);
    }

static eAtRet SemCorrectionResultGet(ThaSemController self)
    {
    static uint32 cTimeoutMs = 1200;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapse < cTimeoutMs)
        {
        uint32 alarmHistory = AtSemControllerAlarmHistoryGet((AtSemController)self);
        if (alarmHistory & cAtSemControllerAlarmErrorNoncorrectable)
            return cAtErrorSemUncorrectable;

        if (alarmHistory & cAtSemControllerAlarmFatalError)
            return cAtErrorSemFatalError;

        return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtError;
    }

static void AddressForcableCommandBuild(ThaSemController self, uint32 address, uint32 wordIdx, uint32 bitIdx, char *commandBuffer, uint32 bufferSize)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    char addressString[34];

    mMethodsGet(osal)->MemInit(osal, commandBuffer, 0, bufferSize);
    mMethodsGet(osal)->MemInit(osal, addressString, 0, sizeof(addressString));
    AtStrcat(commandBuffer, "Q ");
    ThaSemControllerAddressCommandString(self, addressString, (uint32)sizeof(addressString), address, wordIdx, bitIdx);
    AtStrcat(commandBuffer, addressString);
    }

static char *UartBuffer(ThaSemController self, uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = sizeof(mThis(self)->uartBuffer);
    return mThis(self)->uartBuffer;
    }

static uint32 SendForceCommand(ThaSemController self, uint32 address, uint32 wordIdx, uint32 bitIdx, char *receiveBuffer, uint32 bufferSize)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    char command[64];
    AtUart uart = AtSemControllerUartGet((AtSemController)self);

    AddressForcableCommandBuild(self, address, wordIdx, bitIdx, command, sizeof(command));

    /* Try to clear all buffer before sending UART */
    mMethodsGet(osal)->MemInit(osal, receiveBuffer, 0, bufferSize);
    AtUartReceive(uart, receiveBuffer, bufferSize);
    AtUartTransmit(uart, command, AtStrlen(command));

    /* wait to receive data */
    AtUartReceiveDataWait(uart);

    /* receive data and check */
    mMethodsGet(osal)->MemInit(osal, receiveBuffer, 0, bufferSize);
    return AtUartReceive(uart, receiveBuffer, bufferSize);
    }

static eBool InjectErrorDoQuery(ThaSemController self, uint32 address, uint32 wordIdx, uint32 bitIdx, eBool injected)
    {
    char wordString[16];
    const char *injectionLine = NULL;
    uint32 wordValue = 0, injectionLineLength = 0, bufferSize;
    char *receiveBuffer = UartBuffer(self, &bufferSize);
    eBool configured = cAtFalse;
    uint32 fifoLen = SendForceCommand(self, address, wordIdx, bitIdx, receiveBuffer, bufferSize);
    uint32 frameSize = AtSemControllerFrameSize((AtSemController)self);

    if (AtUartReportLineCount(receiveBuffer, fifoLen) < (frameSize + 2))
        return cAtFalse;

    injectionLine = AtUartReportAlineAtLineIndex(receiveBuffer, fifoLen, wordIdx + 1, &injectionLineLength);
    AtOsalMemInit(wordString, 0, sizeof(wordString));
    if ((injectionLine == NULL) || (injectionLineLength == 0) || (injectionLineLength >= sizeof(wordString)))
        return cAtFalse;

    AtOsalMemCpy(wordString, injectionLine, injectionLineLength);
    wordValue = AtStrtoul(wordString, NULL, 16);
    configured = ((wordValue & (cBit0 << bitIdx)) == 0) ? cAtFalse : cAtTrue;

    return (mBoolToBin(configured) == mBoolToBin(injected)) ? cAtTrue : cAtFalse;
    }

static eAtRet ErrorInject(ThaSemController self, uint32 lfa, uint32 dwIdx, uint32 bitIdx, char *cmdString64bytes)
    {
    eAtRet ret = cAtOk;

    /* Query */
    if (InjectErrorDoQuery(self, lfa, dwIdx, bitIdx, cAtFalse) == cAtFalse)
        return cAtErrorSemLfaAddressGenerate;

    /* Inject */
    ret = ThaSemControllerOnlyErrorInject(self, lfa, dwIdx, bitIdx, cmdString64bytes);
    if (ret != cAtOk)
        return ret;

    /* Wait SEM automatically move to IDLE after INJECTION */
    ret = ThaSemControllerAsyncWaitMoveToState((AtSemController)self, cAtSemControllerAlarmIdle);
    if (ret != cAtOk)
        return ret;

    /* Query to make sure inject OK */
    if (InjectErrorDoQuery(self, lfa, dwIdx, bitIdx, cAtTrue) == cAtTrue)
        return cAtOk;

    return cAtErrorSemLfaAddressGenerate;
    }

static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return "XCVU190";
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x0000934A;
    }

static uint32 FrameSize(AtSemController self)
    {
    AtUnused(self);
    return 123;
    }

static uint32 NumDummyWordsInEbd(AtSemController self)
    {
    AtUnused(self);

    return 10;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(uartBuffer);
    }

static void OverrideAtObject(AtSemController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaSemController(AtSemController self)
    {
    ThaSemController controller = (ThaSemController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSemControllerOverride, mMethodsGet(controller), sizeof(m_ThaSemControllerOverride));

        mMethodOverride(m_ThaSemControllerOverride, DefaultOffset);
        mMethodOverride(m_ThaSemControllerOverride, NeedUseCmdCtrlDoneBit);
        mMethodOverride(m_ThaSemControllerOverride, DefaultFrameWordAddress);
        mMethodOverride(m_ThaSemControllerOverride, ClearCorrectableSticky);
        mMethodOverride(m_ThaSemControllerOverride, SemCorrectionResultGet);
        mMethodOverride(m_ThaSemControllerOverride, ErrorInject);
        }

    mMethodsSet(controller, &m_ThaSemControllerOverride);
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, MoveObservationModeBeforeActiveChecking);
        mMethodOverride(m_AtSemControllerOverride, DeviceDescription);
        mMethodOverride(m_AtSemControllerOverride, FrameSize);
        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        mMethodOverride(m_AtSemControllerOverride, NumDummyWordsInEbd);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtObject(self);
    OverrideThaSemController(self);
    OverrideAtSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011SemController);
    }

AtSemController Tha60210011SemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSemControllerV3ObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60210011SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011SemControllerObjectInit(controller, device, semId);
    }

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : Tha60210011Tfi5SerdesController.c
 *
 * Created Date: Jun 23, 2015
 *
 * Description : TFI-5 SERDES controller.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "Tha60210011Tfi5SerdesControllerReg.h"
#include "Tha6021Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtSerdesController self)
    {
    AtPrbsEngine engine;

    /* Release all loopbacks. */
    if (AtSerdesControllerLoopbackGet(self) != cAtLoopbackModeRelease)
        AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);

    /* Initialize built-in PRBS engine. */
    engine = AtSerdesControllerPrbsEngine(self);
    if (engine)
        return AtPrbsEngineInit(engine);

    return cAtOk;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    AtUnused(loopbackMode);
    return cAtTrue;
    }

static eBool UsePmaLoopback(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 PmaLoopbackModeSw2Hw(AtSerdesController self, uint8 loopbackMode)
    {
    AtUnused(self);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return cAf6Tfi5SerdesLoopbackNearEndPma;
        case cAtLoopbackModeRemote:
            return cAf6Tfi5SerdesLoopbackFarEndPma;
        default:
            return cAf6Tfi5SerdesLoopbackNormal;
        }
    }

static uint8 PmaLoopbackModeHw2Sw(AtSerdesController self, uint8 hwLoopbackMode)
    {
    AtUnused(self);
    switch (hwLoopbackMode)
        {
        case cAf6Tfi5SerdesLoopbackNearEndPma:
            return cAtLoopbackModeLocal;
        case cAf6Tfi5SerdesLoopbackFarEndPma:
            return cAtLoopbackModeRemote;
        default:
            return cAtLoopbackModeRelease;
        }
    }

static uint8 PcsLoopbackModeSw2Hw(AtSerdesController self, uint8 loopbackMode)
    {
    AtUnused(self);
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return cAf6Tfi5SerdesLoopbackNearEndPcs;
        case cAtLoopbackModeRemote:
            return cAf6Tfi5SerdesLoopbackFarEndPcs;
        default:
            return cAf6Tfi5SerdesLoopbackNormal;
        }
    }

static uint8 PcsLoopbackModeHw2Sw(AtSerdesController self, uint8 hwLoopbackMode)
    {
    AtUnused(self);
    switch (hwLoopbackMode)
        {
        case cAf6Tfi5SerdesLoopbackNearEndPcs:
            return cAtLoopbackModeLocal;
        case cAf6Tfi5SerdesLoopbackFarEndPcs:
            return cAtLoopbackModeRemote;
        default:
            return cAtLoopbackModeRelease;
        }
    }

static uint8 LoopbackModeSw2Hw(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (UsePmaLoopback(self))
        return PmaLoopbackModeSw2Hw(self, loopbackMode);
    return PcsLoopbackModeSw2Hw(self, loopbackMode);
    }

static eAtRet HwLoopbackSet(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    uint32 address = cAf6RegTfi5LoopbackControl;
    uint32 regVal = AtSerdesControllerRead(self, address, cAtModuleSdh);
    uint8 hwLoopbackMode = LoopbackModeSw2Hw(self, loopbackMode);
    uint32 lineId = AtSerdesControllerHwIdGet(self);

    mFieldIns(&regVal,
              cAf6Tfi5SerdesLoopbackMask(lineId),
              cAf6Tfi5SerdesLoopbackShift(lineId),
              hwLoopbackMode);
    AtSerdesControllerWrite(self, address, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtLoopbackMode HwLoopbackGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5LoopbackControl, cAtModuleSdh);
    uint32 lineId = AtSerdesControllerHwIdGet(self);
    uint8 hwLoopbackMode;
    mFieldGet(regVal,
              cAf6Tfi5SerdesLoopbackMask(lineId),
              cAf6Tfi5SerdesLoopbackShift(lineId),
              uint8,
              &hwLoopbackMode);

    if (UsePmaLoopback(self))
        return PmaLoopbackModeHw2Sw(self, hwLoopbackMode);
    else
        return PcsLoopbackModeHw2Sw(self, hwLoopbackMode);
    }

static eAtRet RemoteLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeRemote);

    if (HwLoopbackGet(self) == cAtLoopbackModeRemote)
        return HwLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static eAtRet LocalLoopbackEnable(AtSerdesController self, eBool enable)
    {
    if (enable)
        return HwLoopbackSet(self, cAtLoopbackModeLocal);

    if (HwLoopbackGet(self)== cAtLoopbackModeLocal)
        return HwLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (loopbackMode == cAtLoopbackModeRemote)
        return RemoteLoopbackEnable(self, enable);
    if (loopbackMode == cAtLoopbackModeLocal)
        return LocalLoopbackEnable(self, enable);

    return HwLoopbackSet(self, cAtLoopbackModeRelease);
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return (loopbackMode == HwLoopbackGet(self)) ? cAtTrue : cAtFalse;
    }

static ThaModuleSdh SdhModule(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);

    if (manager == NULL)
        {
        AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet(self);
        return (ThaModuleSdh)AtChannelModuleGet((AtChannel)line);
        }

    return (ThaModuleSdh)AtDeviceModuleGet(AtSerdesManagerDeviceGet(manager), cAtModuleSdh);
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet(self);
    ThaModuleSdh sdhModule = SdhModule(self);
    return ThaModuleSdhLineSerdesPrbsEngineCreate(sdhModule, line, self);
    }

static eAtRet Reset(AtSerdesController self)
    {
    /* Currently, don't need to reset */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeOcn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeOcn;
    }

static uint32 EqualizerModeSw2Hw(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)  return 0;
    if (mode == cAtSerdesEqualizerModeLpm)  return 1;

    return 0x0;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return AtDeviceIsSimulated(device);
    }

static eBool RxPMAResetDone(AtSerdesController self)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    uint32 elapse = 0;
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal;
    uint32  mask = cAf6RegTfi5RxPmaResetDoneMask(serdesId);
    eBool isSimulated = IsSimulated(self);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        regVal = AtSerdesControllerRead(self, cAf6RegTfi5PmaResetStatus, cAtModuleSdh);
        if (regVal & mask)
            return cAtTrue;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        if (isSimulated)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet RxPMAReset(AtSerdesController self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5PmaResetControl, cAtModuleSdh);

    mFieldIns(&regVal, cAf6RegTfi5RxPmaResetMask(serdesId), cAf6RegTfi5RxPmaResetShift(serdesId), 1);
    AtSerdesControllerWrite(self, cAf6RegTfi5PmaResetControl, regVal, cAtModuleSdh);
    mMethodsGet(osal)->USleep(osal, 5000);
    mFieldIns(&regVal, cAf6RegTfi5RxPmaResetMask(serdesId), cAf6RegTfi5RxPmaResetShift(serdesId), 0);
    AtSerdesControllerWrite(self, cAf6RegTfi5PmaResetControl, regVal, cAtModuleSdh);

    return RxPMAResetDone(self) ? cAtOk : cAtErrorSerdesResetTimeout;
    }

static eAtRet TxPMAReset(AtSerdesController self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5PmaResetControl, cAtModuleSdh);

    mFieldIns(&regVal, cAf6RegTfi5TxPmaResetMask(serdesId), cAf6RegTfi5TxPmaResetShift(serdesId), 1);
    AtSerdesControllerWrite(self, cAf6RegTfi5PmaResetControl, regVal, cAtModuleSdh);
    mMethodsGet(osal)->USleep(osal, 5000);
    mFieldIns(&regVal, cAf6RegTfi5TxPmaResetMask(serdesId), cAf6RegTfi5TxPmaResetShift(serdesId), 0);
    AtSerdesControllerWrite(self, cAf6RegTfi5PmaResetControl, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regVal, serdesId;

    if (!AtSerdesControllerEqualizerCanControl(self))
        return (mode == cAtSerdesEqualizerModeDfe) ? cAtOk : cAtErrorModeNotSupport;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    /* Step1: switch RXLPMEN: 0(DFE), 1(LPM) */
    serdesId = AtSerdesControllerHwIdGet(self);
    regVal = AtSerdesControllerRead(self, cAf6RegTfi5LpmEnableControl, cAtModuleSdh);
    mFieldIns(&regVal, cAf6RegTfi5LpmEnableMask(serdesId), cAf6RegTfi5LpmEnableShift(serdesId), EqualizerModeSw2Hw(mode));
    AtSerdesControllerWrite(self, cAf6RegTfi5LpmEnableControl, regVal, cAtModuleSdh);

    /* Step2: set coefficience: Do nothing */

    /* Step3: reset Rx PMA  */
    return RxPMAReset(self);
    }

static eAtSerdesEqualizerMode EqualizerModeHw2Sw(uint32 mode)
    {
    if (mode == 0) return cAtSerdesEqualizerModeDfe;
    if (mode == 1) return cAtSerdesEqualizerModeLpm;
    return cAtSerdesEqualizerModeUnknown;
    }

static eAtSerdesEqualizerMode HwEqualizerModeGet(AtSerdesController self)
    {
    uint32 lpmEnabled;
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5LpmEnableControl, cAtModuleSdh);
    mFieldGet(regVal, cAf6RegTfi5LpmEnableMask(serdesId), cAf6RegTfi5LpmEnableShift(serdesId), uint32, &lpmEnabled);
    return EqualizerModeHw2Sw(lpmEnabled);
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    if (AtSerdesControllerEqualizerCanControl(self))
        return HwEqualizerModeGet(self);

    return cAtSerdesEqualizerModeDfe;
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    if (mode == cAtSerdesEqualizerModeDfe) return cAtTrue;
    if (mode == cAtSerdesEqualizerModeLpm) return cAtTrue;
    return cAtFalse;
    }

static eBool EqualizerIsSupported(AtDevice self)
    {
    uint32 startVersionSupportEqualizer = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x1069);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));

    if (hwVersion >= startVersionSupportEqualizer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return EqualizerIsSupported(device);
    }

static eAtRet HwTxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5TxDiffCtrl, cAtModuleSdh);
    mFieldIns(&regVal, cAf6RegTfi5TxDiffMask(serdesId), cAf6RegTfi5TxDiffShift(serdesId), value);
    AtSerdesControllerWrite(self, cAf6RegTfi5TxDiffCtrl, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint32 HwTxDiffCtrlGet(AtSerdesController self)
    {
    uint32 value;
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5TxDiffCtrl, cAtModuleSdh);
    mFieldGet(regVal, cAf6RegTfi5TxDiffMask(serdesId), cAf6RegTfi5TxDiffShift(serdesId), uint32, &value);
    return value;
    }

static eAtRet HwTxPreCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5TxPreCursorCtrl, cAtModuleSdh);
    mFieldIns(&regVal, cAf6RegTfi5TxPreCursorMask(serdesId), cAf6RegTfi5TxPreCursorShift(serdesId), value);
    AtSerdesControllerWrite(self, cAf6RegTfi5TxPreCursorCtrl, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint32 HwTxPreCursorGet(AtSerdesController self)
    {
    uint32 value;
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5TxPreCursorCtrl, cAtModuleSdh);
    mFieldGet(regVal, cAf6RegTfi5TxPreCursorMask(serdesId), cAf6RegTfi5TxPreCursorShift(serdesId), uint32, &value);
    return value;
    }

static eAtRet HwxPostCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5TxPostCursorCtrl, cAtModuleSdh);
    mFieldIns(&regVal, cAf6RegTfi5TxPostCursorMask(serdesId), cAf6RegTfi5TxPostCursorShift(serdesId), value);
    AtSerdesControllerWrite(self, cAf6RegTfi5TxPostCursorCtrl, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint32 HwxPostCursorGet(AtSerdesController self)
    {
    uint32 value;
    uint32 serdesId = AtSerdesControllerHwIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAf6RegTfi5TxPostCursorCtrl, cAtModuleSdh);
    mFieldGet(regVal, cAf6RegTfi5TxPostCursorMask(serdesId), cAf6RegTfi5TxPostCursorShift(serdesId), uint32, &value);
    return value;
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    AtUnused(self);

    if (param == cAtSerdesParamTxPreCursor)
        return (value > 15) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxPostCursor)
        return (value > 15) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxDiffCtrl)
        return (value > 15) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor : return cAtTrue;
        case cAtSerdesParamTxPostCursor: return cAtTrue;
        case cAtSerdesParamTxDiffCtrl  : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtRet HwPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    eAtRet ret = cAtErrorInvlParm;
    if (param == cAtSerdesParamTxPreCursor)
        ret = HwTxPreCursorSet(self, value);

    if (param == cAtSerdesParamTxPostCursor)
        ret = HwxPostCursorSet(self, value);

    if (param == cAtSerdesParamTxDiffCtrl)
        ret = HwTxDiffCtrlSet(self, value);

    if (ret != cAtOk)
        return ret;

    return TxPMAReset(self);
    }

static uint32 HwPhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxPreCursorGet(self);

    if (param == cAtSerdesParamTxPostCursor)
        return HwxPostCursorGet(self);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlGet(self);

    return cInvalidUint32;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (!AtSerdesControllerEqualizerCanControl(self))
        return cAtErrorModeNotSupport;

    if (!mMethodsGet(self)->PhysicalParamValueIsInRange(self, param, value))
        return cAtErrorOutOfRangParm;

    if (AtSerdesControllerPhysicalParamGet(self, param) == value)
        return cAtOk;

    return HwPhysicalParamSet(self, param, value);
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (!AtSerdesControllerEqualizerCanControl(self))
        return 0x0;

    return HwPhysicalParamGet(self, param);
    }

static const char *ToString(AtObject self)
    {
    static char description[16];
    AtSerdesController serdes = (AtSerdesController)self;
    AtChannel channel = AtSerdesControllerPhysicalPortGet(serdes);

    if (channel)
        return m_AtObjectMethods->ToString(self);

    AtSnprintf(description, sizeof(description), "tfi5.%d", AtSerdesControllerIdGet(serdes) + 1);

    return description;
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5SerdesController);
    }

AtSerdesController Tha60210011Tfi5SerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesControllerObjectInit(self, (AtChannel)sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210011Tfi5SerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011Tfi5SerdesControllerObjectInit(newController, sdhLine, serdesId);
    }

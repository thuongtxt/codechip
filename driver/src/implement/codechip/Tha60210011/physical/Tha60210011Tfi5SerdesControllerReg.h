/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210011Tfi5SerdesControllerReg.h
 * 
 * Created Date: Jun 26, 2015
 *
 * Description : Register of Tfi5 SerdesController
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011TFI5SERDESCONTROLLERREG_H_
#define _THA60210011TFI5SERDESCONTROLLERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cAf6RegTfi5DiagBaseAddress      0xF43000UL

#define cAf6RegTfi5SerdesCtrl           0x0UL
#define cAf6Tfi5SerdesPrbsEnableMask(lineId)   (cBit0 << (lineId))
#define cAf6Tfi5SerdesPrbsEnableShift(lineId)  (lineId)
#define cAf6Tfi5SerdesPrbsErrInstMask(lineId)  (cBit8 << (lineId))
#define cAf6Tfi5SerdesPrbsErrInstShift(lineId) ((lineId) + 8)

#define cAf6RegTfi5LoopbackControl          0xf00042
#define cAf6Tfi5SerdesLoopbackMask(lineId)  (cBit2_0 << ((lineId) * 3))
#define cAf6Tfi5SerdesLoopbackShift(lineId) ((lineId) * 3)
#define cAf6Tfi5SerdesLoopbackNormal        0x0
#define cAf6Tfi5SerdesLoopbackNearEndPcs    0x1
#define cAf6Tfi5SerdesLoopbackNearEndPma    0x2
#define cAf6Tfi5SerdesLoopbackFarEndPcs     0x6
#define cAf6Tfi5SerdesLoopbackFarEndPma     0x4

#define cAf6RegTfi5SerdesPrbsStickyStatus   0x1UL
#define cAf6RegTfi5SerdesPrbsCurrentStatus  0x2UL
#define cAf6RegTfi5SerdesPrbsStatusMask(lineId)  (cBit0 << (lineId))
#define cAf6RegTfi5SerdesPrbsStatusShift(lineId) (lineId)

#define cAf6RegTfi5DrpBaseAddress(lineId)     (0xf41000 + (lineId)* 0x200)
#define cAf6RegTfi5LpmEnableControl           (0xf46040)
#define cAf6RegTfi5LpmEnableMask(lineId)      (cBit0 << (lineId))
#define cAf6RegTfi5LpmEnableShift(lineId)     (lineId)

#define cAf6RegTfi5PmaResetControl            (0xf46042)
#define cAf6RegTfi5TxPmaResetMask(lineId)     (cBit4 << (lineId))
#define cAf6RegTfi5TxPmaResetShift(lineId)    ((lineId) + 4)

#define cAf6RegTfi5RxPmaResetMask(lineId)     (cBit0 << (lineId))
#define cAf6RegTfi5RxPmaResetShift(lineId)    (lineId)

#define cAf6RegTfi5PmaResetStatus                   (0xf46060)
#define cAf6RegTfi5RxPmaResetDoneMask(lineId)       (cBit0 << (lineId))
#define cAf6RegTfi5RxPmaResetDoneShift(lineId)      (lineId)

#define cAf6RegTfi5TxPreCursorCtrl            (0xf46043)
#define cAf6RegTfi5TxPreCursorMask(lineId)    (cBit3_0 << ((lineId) * 4))
#define cAf6RegTfi5TxPreCursorShift(lineId)   ((lineId) * 4)

#define cAf6RegTfi5TxPostCursorCtrl           (0xf46044)
#define cAf6RegTfi5TxPostCursorMask(lineId)   (cBit3_0 << ((lineId) * 4))
#define cAf6RegTfi5TxPostCursorShift(lineId)  ((lineId) * 4)

#define cAf6RegTfi5TxDiffCtrl                 (0xf46045)
#define cAf6RegTfi5TxDiffMask(lineId)         (cBit3_0 << ((lineId) * 4))
#define cAf6RegTfi5TxDiffShift(lineId)        ((lineId) * 4)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THA60210011TFI5SERDESCONTROLLERREG_H_ */


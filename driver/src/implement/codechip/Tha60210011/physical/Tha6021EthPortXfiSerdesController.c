/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha6021EthPortXfiSerdesController.c
 *
 * Created Date: Jun 26, 2015
 *
 * Description : SERDES controller for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../Tha60210021/man/Tha60210021DeviceReg.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../eth/Tha60210011ModuleEthInternal.h"
#include "../eth/Tha6021ModuleEthReg.h"
#include "Tha6021EthPortXfiSerdesControllerInternal.h"
#include "Tha6021Physical.h"

/*--------------------------- Define -----------------------------------------*/
#define cLpmEnableRegister          0xF00047

#define cMdioReg10GPMDTransmitDisable cMdioRegRegister(1, 9)
#define cMdioReg10GPMDTransmitDisableMask  cBit0

#define cPcsRxLockedLl    cBit1
#define cPcsRxLocked      cBit0
#define cPcsRxLinkStatus  cBit12
#define cPmaRxLinkStatus  cBit11

#define cPcsErrBlockCountMask     cBit31_24
#define cPcsErrBlockCountShift    24
#define cPcsBerCountMask          cBit21_16
#define cPcsBerCountShift         16
#define cPcsTestPattErrCountMask  cBit15_0
#define cPcsTestPattErrCountShift 0

#define cAf6_linkalm_Eport_XFI_Mask(port)       (cBit16 << (port))
#define cAf6_linkalm_Eport_XFI_Shift(port)      ((port) + 16)

#define cMdioReg10GPmaPmdStatus2Register        cMdioRegRegister(1, 8)
#define cPmaPmdStatus2TransmitFaultMask         cBit11
#define cPmaPmdStatus2ReceiveFaultMask          cBit10

#define cMdioReg10GPmdSignalReceiveOkRegister   cMdioRegRegister(1, 10)
#define cPmdGlobalPMDReceiveSignalDetectMask    cBit0

#define cMdioRegPcsControl1Register             cMdioRegRegister(3, 0)
#define cPcsResetMask                           cBit15
#define cPcsResetShift                          15

#define cMdioRegPcsStatus1Register              cMdioRegRegister(3, 1)
#define cPcsLocalFaultMask                      cBit7
#define cPcsReceiveLinkStatusMask               cBit2

#define cMdioReg10GPcsStatus2Register           cMdioRegRegister(3, 8)
#define cPcsTransmitLocalFault                  cBit11
#define cPcsReceiveLocalFault                   cBit10

#define cMdioReg10GBaseRStatusRegister          cMdioRegRegister(3, 32)
#define c10GBASERLinkStatusMask                 cBit12
#define cHiBerMask                              cBit1
#define cBlockLockMask                          cBit0

#define cMdioReg10GBaseRStatus2Register         cMdioRegRegister(3, 33)
#define cLatchLowVersionOfBlockLockMask         cBit15
#define cLatchLowVersionOfBlockLockShift        15

#define cLatchHighVersionOfHiBerMask            cBit14
#define cLatchHighVersionOfHiBerShift           14

#define cBerCounterMask                         cBit13_8
#define cBerCounterShift                        8

#define cErroredBlocksCountMask                 cBit7_0
#define cErroredBlocksCountShift                0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6021EthPortXfiSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6021EthPortXfiSerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods                       m_AtObjectOverride;
static tAtSerdesControllerMethods             m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods m_Tha6021EthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModuleEth);
    }

static void MdioSelect(AtSerdesController self, AtMdio mdio)
    {
    AtMdioPortSelect(mdio, mMethodsGet(self)->HwIdGet(self));
    }

static eAtRet IpReset(AtSerdesController self)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    uint32 elapse = 0;
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    MdioSelect(self, mdio);
    regVal  = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
    regVal |= cPmaResetMask;
    AtMdioWrite(mdio, cMdioRegPmaPmdControl1Register, regVal);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        regVal = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
        if ((regVal & cPmaResetMask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeDfe;
    }

static eAtRet DefaultEqualizerModeSet(AtSerdesController self)
    {
    eAtSerdesEqualizerMode defaultMode = mMethodsGet(self)->DefaultEqualizerMode(self);

    if (defaultMode == AtSerdesControllerEqualizerModeGet(self))
        return cAtOk;

    if (AtSerdesControllerEqualizerModeIsSupported(self, defaultMode))
        return AtSerdesControllerEqualizerModeSet(self, defaultMode);

    return cAtOk;
    }

static eAtRet Init(AtSerdesController self)
    {
    AtPrbsEngine engine;
    eAtRet ret = cAtOk;
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    DefaultEqualizerModeSet(self);
    ret |= AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);

    /* Initialize PRBS engine */
    if (mMethodsGet(controller)->ShouldInitPrbsEngine(controller))
        {
        engine = AtSerdesControllerPrbsEngine(self);
        if (engine)
            ret |= AtPrbsEngineInit(engine);
        }

    return ret;
    }

static eAtRet FpgaEnable(AtSerdesController self, eBool enable)
    {
    uint32 regAddr  = cEthPortSerdesTxEnable;
    uint32 regVal   = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 mask = mMethodsGet(mThis(self))->TxSerdesEnableMask(serdesId);
    uint32 shift = mMethodsGet(mThis(self))->TxSerdesEnableShift(serdesId);

    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool FpgaIsEnabled(AtSerdesController self)
    {
    uint32 regAddr  = cEthPortSerdesTxEnable;
    uint32 regVal   = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = mMethodsGet(self)->HwIdGet(self);
    uint32 mask = mMethodsGet(mThis(self))->TxSerdesEnableMask(serdesId);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eBool TxIsEnabled(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    MdioSelect(self, mdio);
    return (AtMdioRead(mdio, cMdioReg10GPMDTransmitDisable) & cBit0) ? cAtFalse : cAtTrue;
    }

static eAtRet TxEnable(AtSerdesController self, eBool enable)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    /* Should not touch SERDES if there is nothing changed */
    if (enable == TxIsEnabled(self))
        return cAtOk;

    MdioSelect(self, mdio);
    return AtMdioWrite(mdio, cMdioReg10GPMDTransmitDisable, enable ? 0 : 1);
    }

static eBool FpgaCanEnableTx(AtSerdesController self)
    {
    return ThaModuleEthCanEnableTxSerdes((ThaModuleEth)EthModule(self));
    }

static eBool ShouldUseMdioForEnabling(Tha6021EthPortXfiSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 TxSerdesEnableMask(uint32 serdesHwId)
    {
    return cEthPortSerdesXfiEnableMask(serdesHwId);
    }

static uint32 TxSerdesEnableShift(uint32 serdesHwId)
    {
    return cEthPortSerdesXfiEnableShift(serdesHwId);
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cLpmEnableRegister;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    if (FpgaCanEnableTx(self))
        return AtSerdesControllerFpgaEnable(self, enable);

    if (mMethodsGet(mThis(self))->ShouldUseMdioForEnabling(mThis(self)))
        return TxEnable(self, enable);

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    if (FpgaCanEnableTx(self))
        return AtSerdesControllerFpgaIsEnabled(self);

    if (mMethodsGet(mThis(self))->ShouldUseMdioForEnabling(mThis(self)))
        return TxIsEnabled(self);

    return cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(enable);
    return FpgaCanEnableTx(self);
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    return ThaModuleEthPortSerdesPrbsEngineCreate(ethModule, port, AtSerdesControllerIdGet(self));
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    if (loopbackMode == cAtLoopbackModeLocal)
        return cAtTrue;
    return enable ? cAtFalse : cAtTrue;
    }

static eAtRet PmaLocalLoopbackEnable(AtSerdesController self, eBool enable)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    MdioSelect(self, mdio);
    regVal = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
    mRegFieldSet(regVal, cPmaLoopback, enable ? 1 : 0);
    return AtMdioWrite(mdio, cMdioRegPmaPmdControl1Register, regVal);
    }

static eBool PmaLocalLoopbackIsEnabled(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal;
    MdioSelect(self, mdio);
    regVal = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
    return (regVal & cPmaLoopbackMask) ? cAtTrue : cAtFalse;
    }

static eAtRet LocalLoopbackEnable(AtSerdesController self, eBool enable)
    {
    return PmaLocalLoopbackEnable(self, enable);
    }

static eBool LocalLoopbackIsEnabled(AtSerdesController self)
    {
    return PmaLocalLoopbackIsEnabled(self);
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (loopbackMode == cAtLoopbackModeLocal)
        return LocalLoopbackEnable(self, enable);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (loopbackMode == cAtLoopbackModeLocal)
        return LocalLoopbackIsEnabled(self);
    return cAtFalse;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal;
    if (AtSerdesControllerPowerIsDown(self))
        {
        MdioSelect(self, mdio);
        regVal = AtMdioRead(mdio, cMdioReg10GBaseRStatusRegister);
        if (((regVal & c10GBASERLinkStatusMask) == 0) && ((regVal & cBlockLockMask) == 0))
            return cAtSerdesLinkStatusDown;
        }

    /* IMPORTANT: As documented, this is "R/O Latches Low register. And as
     * experience, this should be read for two times to have latest status */
    MdioSelect(self, mdio);
    regVal = AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    if ((regVal & cPcsReceiveLinkStatusMask) == 0)
        return cAtSerdesLinkStatusDown;

    regVal = AtMdioRead(mdio, cMdioRegPmaPmdStatus1Register);
    return (regVal & cReceiveLinkStatusMask) ? cAtSerdesLinkStatusUp : cAtSerdesLinkStatusDown;
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(channel);
    return AtModuleEthSerdesMdio(ethModule, self);
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 FpgaAlarmGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linksta_Eport, cAtModuleEth);
    uint32 portId = mMethodsGet(self)->HwIdGet(self);
    uint32 alarms = 0;

    if (~regVal & cAf6_linkalm_Eport_XFI_Mask(portId))
        alarms |= cAtSerdesAlarmTypeLinkDown;

    return alarms;
    }

static uint32 FpgaAlarmRead2Clear(AtSerdesController self, eBool read2Clear)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linkalm_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_XFI_Mask(mMethodsGet(self)->HwIdGet(self));
    uint32 alarms = 0;

    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    if (read2Clear)
        AtSerdesControllerWrite(self, cAf6Reg_linkalm_Eport, portMask, cAtModuleEth);

    return alarms;
    }

static eBool FpgaAlarmSupportted(Tha6021EthPortXfiSerdesController self)
    {
    /* Not to affect running logic which use FPGA alarms */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 MdioAlarmGet(AtSerdesController self)
    {
    AtMdio mdio = Mdio(self);
    uint32 regVal;
    uint32 alarms = 0;

    MdioSelect(self, mdio);

    regVal = AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    if (regVal & cPcsLocalFaultMask)
        alarms |= cAtSerdesAlarmTypeRxFault;
    if ((regVal & cPcsReceiveLinkStatusMask) == 0)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    regVal = AtMdioRead(mdio, cMdioReg10GPcsStatus2Register);
    if (regVal & cPcsTransmitLocalFault)
        alarms |= cAtSerdesAlarmTypeTxFault;

    if (regVal & cPcsReceiveLocalFault)
        alarms |= cAtSerdesAlarmTypeRxFault;

    regVal = AtMdioRead(mdio, cMdioReg10GBaseRStatusRegister);
    if ((regVal & c10GBASERLinkStatusMask) == 0)
        alarms |= cAtSerdesAlarmTypeNotAligned;

    if ((regVal & cBlockLockMask) == 0)
        alarms |= cAtSerdesAlarmTypeNotSync;

    return alarms;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaAlarmGet(self);
    return MdioAlarmGet(self);
    }

static uint32 AlarmRead2Clear(AtSerdesController self, eBool read2Clear)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaAlarmRead2Clear(self, read2Clear);

    return 0x0;
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtTrue);
    }

static eAtRet FpgaInterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_inten_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_XFI_Mask(mMethodsGet(self)->HwIdGet(self));

    if (alarmMask & cAtSerdesAlarmTypeLinkDown)
        regVal = (enableMask & cAtSerdesAlarmTypeLinkDown) ? (regVal | portMask) : (regVal & ~portMask);
    AtSerdesControllerWrite(self, cAf6Reg_inten_Eport, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 FpgaInterruptMaskGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_inten_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_XFI_Mask(mMethodsGet(self)->HwIdGet(self));
    uint32 alarms = 0;

    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    return alarms;
    }

static eAtRet InterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaInterruptMaskSet(self, alarmMask, enableMask);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtSerdesController self)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaInterruptMaskGet(self);
    return 0;
    }

static eAtRet FpgaRxAlarmForce(AtSerdesController self, uint32 alarmType, eBool enable)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linkfrc_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_XFI_Mask(mMethodsGet(self)->HwIdGet(self));
    uint32 alarms = 0;

    if (alarmType & cAtSerdesAlarmTypeLinkDown)
        regVal = (enable) ? (regVal | portMask) : (regVal & ~portMask);
    AtSerdesControllerWrite(self, cAf6Reg_linkfrc_Eport, regVal, cAtModuleEth);

    return alarms;
    }

static eAtRet RxAlarmForce(AtSerdesController self, uint32 alarmType)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaRxAlarmForce(self, alarmType, cAtTrue);
    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnforce(AtSerdesController self, uint32 alarmType)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaRxAlarmForce(self, alarmType, cAtFalse);
    return cAtErrorModeNotSupport;
    }

static uint32 FpgaRxForcedAlarmGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linkfrc_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_XFI_Mask(mMethodsGet(self)->HwIdGet(self));
    uint32 alarms = 0;

    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    return alarms;
    }

static uint32 RxForcedAlarmGet(AtSerdesController self)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return FpgaRxForcedAlarmGet(self);
    return 0x0;
    }

static uint32 RxForcableAlarmsGet(AtSerdesController self)
    {
    if (mMethodsGet(mThis(self))->FpgaAlarmSupportted(mThis(self)))
        return cAtSerdesAlarmTypeLinkDown;
    return cAtSerdesAlarmTypeNone;
    }

static void DebugPrintRegName(const char* title, uint32 page, uint32 address, uint32 value)
    {
    Tha6021DebugPrintStart(NULL);
    AtPrintc(cSevInfo, "\r\n%s: (%d.%02d = 0x%08x)\r\n", title, page, address, value);
    return;
    }

static void StatusClear(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);

    MdioSelect(self, mdio);

    AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
    AtMdioRead(mdio, cMdioReg10GPMDTransmitDisable);
    AtMdioRead(mdio, cMdioReg10GPmaPmdStatus2Register);
    AtMdioRead(mdio, cMdioReg10GPmdSignalReceiveOkRegister);
    AtMdioRead(mdio, cMdioRegPcsControl1Register);
    AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    AtMdioRead(mdio, cMdioReg10GPcsStatus2Register);
    AtMdioRead(mdio, cMdioReg10GBaseRStatusRegister);
    AtMdioRead(mdio, cMdioReg10GBaseRStatus2Register);

    m_AtSerdesControllerMethods->StatusClear(self);
    }

static void Debug(AtSerdesController self)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regVal;

    MdioSelect(self, mdio);

    Tha6021DebugPrintNeedTwoCollumnsEnable(cAtFalse);

    regVal = AtMdioRead(mdio, cMdioRegPmaPmdControl1Register);
    DebugPrintRegName("PMA/PMD Control 1", cMdioRegPmaPmdControl1Register, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Reset", regVal, cPmaResetMask, "block reset", "normal operation");

    regVal = AtMdioRead(mdio, cMdioReg10GPMDTransmitDisable);
    DebugPrintRegName("10G PMD Transmit Disable", cMdioReg10GPMDTransmitDisable, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Transmit", regVal, cMdioReg10GPMDTransmitDisableMask, "disabled", "enabled");

    regVal = AtMdioRead(mdio, cMdioReg10GPmaPmdStatus2Register);
    DebugPrintRegName("10G PMA/PMD Status 2", cMdioReg10GPmaPmdStatus2Register, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Transmit Fault", regVal, cPmaPmdStatus2TransmitFaultMask, "detected", "no");
    Tha6021DebugPrintErrorBitWithLabel("Receive Fault", regVal, cPmaPmdStatus2ReceiveFaultMask, "detected", "no");

    regVal = AtMdioRead(mdio, cMdioReg10GPmdSignalReceiveOkRegister);
    DebugPrintRegName("10G PMD Signal Receive OK", cMdioReg10GPmdSignalReceiveOkRegister, regVal);
    Tha6021DebugPrintOkBitWithLabel("Global PMD receive signal detect", regVal, cPmdGlobalPMDReceiveSignalDetectMask, "detected", "not detected");

    regVal = AtMdioRead(mdio, cMdioRegPcsControl1Register);
    DebugPrintRegName("PCS Control 1", cMdioRegPcsControl1Register, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Reset", regVal, cPcsResetMask, "reset", "normal");

    regVal = AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    DebugPrintRegName("PCS Status 1", cMdioRegPcsStatus1Register, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Local Fault", regVal, cPcsLocalFaultMask, "detected", "no");
    Tha6021DebugPrintOkBitWithLabel("PCS Receive Link Status", regVal, cPcsReceiveLinkStatusMask, "up", "down");

    regVal = AtMdioRead(mdio, cMdioReg10GPcsStatus2Register);
    DebugPrintRegName("10G PCS Status 2", cMdioReg10GPcsStatus2Register, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Transmit local fault", regVal, cPcsTransmitLocalFault, "detected", "no");
    Tha6021DebugPrintErrorBitWithLabel("Receive local fault", regVal, cPcsReceiveLocalFault, "detected", "no");

    regVal = AtMdioRead(mdio, cMdioReg10GBaseRStatusRegister);
    DebugPrintRegName("10GBASE-R Status 1", cMdioReg10GBaseRStatusRegister, regVal);
    Tha6021DebugPrintOkBitWithLabel("10GBASE-R Link Status", regVal, c10GBASERLinkStatusMask, "aligned", "not aligned");
    Tha6021DebugPrintErrorBitWithLabel("Hi Ber", regVal, cHiBerMask, "hi-ber", "no");
    Tha6021DebugPrintOkBitWithLabel("Block lock", regVal, cBlockLockMask, "synchronized", "not synchronized");

    regVal = AtMdioRead(mdio, cMdioReg10GBaseRStatus2Register);
    DebugPrintRegName("10GBASE-R Status 2", cMdioReg10GBaseRStatus2Register, regVal);
    Tha6021DebugPrintBitFieldVal(NULL, "Latched Block Lock", regVal, cLatchLowVersionOfBlockLockMask, cLatchLowVersionOfBlockLockShift);
    Tha6021DebugPrintBitFieldVal(NULL, "Latched HiBER", regVal, cLatchHighVersionOfHiBerMask, cLatchHighVersionOfHiBerShift);
    Tha6021DebugPrintBitFieldVal(NULL, "BER counter", regVal, cBerCounterMask, cBerCounterShift);
    Tha6021DebugPrintBitFieldVal(NULL, "Errored Blocks Count", regVal, cErroredBlocksCountMask, cErroredBlocksCountShift);

    Tha6021DebugPrintNeedTwoCollumnsEnable(cAtTrue);

    /* SERDES reseting time */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "maxSerdesResetTime: ");
    AtPrintc(mThis(self)->maxSerdesResetTimeMs ? cSevWarning : cSevInfo, "%d (ms)\r\n", mThis(self)->maxSerdesResetTimeMs);
    mThis(self)->maxSerdesResetTimeMs = 0;
    }

static eBool NeedSafeReset(AtSerdesController self)
    {
    /* Not need anymore, but still keep this just in case failure happen and
     * need to analyze */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet NormalReset(AtSerdesController self)
    {
    tAtOsalCurTime startTime, curTime;
    eAtRet ret;
    uint32 elapseTime;

    AtOsalCurTimeGet(&startTime);

    ret = IpReset(self);

    /* Profile */
    AtOsalCurTimeGet(&curTime);
    elapseTime = mTimeIntervalInMsGet(startTime, curTime);
    if (elapseTime > mThis(self)->maxSerdesResetTimeMs)
        mThis(self)->maxSerdesResetTimeMs = elapseTime;

    return ret;
    }

static AtDevice Device(AtSerdesController self)
    {
    return AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    }

static eAtRet Reset(AtSerdesController self)
    {
    static const uint32 cTimeoutMs = 1000;
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, curTime;
    eAtRet ret = cAtOk;

    if (AtSerdesControllerInAccessible(self) || AtDeviceIsSimulated(Device(self)))
        return cAtOk;

    if (AtSerdesControllerPowerIsDown(self))
        return cAtOk;

    if (!NeedSafeReset(self))
        return NormalReset(self);

    AtOsalCurTimeGet(&startTime);
    while (elapseTime < cTimeoutMs)
        {
        uint32 checkTime;

        ret = IpReset(self);
        if (ret != cAtOk)
            return ret;

        ret = AtSerdesControllerLoopbackSet(self, cAtLoopbackModeLocal);
        if (ret != cAtOk)
            return ret;

        /* Retry checking for 3 times (or more) */
        for (checkTime = 0; checkTime < 3; checkTime++)
            {
            AtOsalUSleep(10000);

            if (!AtSerdesControllerIsGood(self))
                continue;

            /* Profile */
            AtOsalCurTimeGet(&curTime);
            elapseTime = mTimeIntervalInMsGet(startTime, curTime);
            if (elapseTime > mThis(self)->maxSerdesResetTimeMs)
                mThis(self)->maxSerdesResetTimeMs = elapseTime;

            /* Bring to normal state */
            return AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);
            }

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                            "Reset SERDES %d fail\r\n", AtSerdesControllerIdGet(self) + 1);
    mThis(self)->maxSerdesResetTimeMs = elapseTime;

    /* Fail, need to bring loopback to default state */
    AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtErrorSerdesFail;
    }

static void MdioStatusClear(AtSerdesController self)
    {
    AtMdio mdio;

    mdio = Mdio(self);
    MdioSelect(self, mdio);

    AtMdioRead(mdio, cMdioReg10GPcsStatus2Register); /* For self-clearing */
    AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    AtMdioRead(mdio, cMdioRegPcsStatus1Register);
    }

static eBool IsGood(AtSerdesController self)
    {
    MdioStatusClear(self);
    AtOsalUSleep(1000);
    if (MdioAlarmGet(self) == cAtSerdesAlarmTypeNone)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 SupportedInterruptMask(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesAlarmTypeLinkDown;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6021EthPortXfiSerdesController object = (Tha6021EthPortXfiSerdesController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(maxSerdesResetTimeMs);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, FpgaEnable);
        mMethodOverride(m_AtSerdesControllerOverride, FpgaIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtSerdesControllerOverride, SupportedInterruptMask);
        mMethodOverride(m_AtSerdesControllerOverride, RxAlarmForce);
        mMethodOverride(m_AtSerdesControllerOverride, RxAlarmUnforce);
        mMethodOverride(m_AtSerdesControllerOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, RxForcableAlarmsGet);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        mMethodOverride(m_AtSerdesControllerOverride, StatusClear);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, IsGood);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static void MethodsInit(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ShouldUseMdioForEnabling);
        mMethodOverride(m_methods, FpgaAlarmSupportted);
        mMethodOverride(m_methods, TxSerdesEnableMask);
        mMethodOverride(m_methods, TxSerdesEnableShift);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021EthPortXfiSerdesController);
    }

AtSerdesController Tha6021EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

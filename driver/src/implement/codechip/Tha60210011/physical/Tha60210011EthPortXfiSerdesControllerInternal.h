/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210011EthPortXfiSerdesControllerInternal.h
 *
 * Created Date: Oct 31, 2015
 *
 * Description : SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011ETHPORTXFISERDESCONTROLLERINTERNAL_H_
#define _THA60210011ETHPORTXFISERDESCONTROLLERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "Tha6021EthPortXfiSerdesControllerInternal.h"
#include "Tha6021Physical.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011EthPortXfiSerdesController
    {
    tTha6021EthPortXfiSerdesController super;
    }tTha60210011EthPortXfiSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtSerdesController Tha60210011EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011ETHPORTXFISERDESCONTROLLERINTERNAL_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6021Physical.h
 * 
 * Created Date: Jun 30, 2015
 *
 * Description : Physical common declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021PHYSICAL_H_
#define _THA6021PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "commacro.h"
#include "AtSdhLine.h"
#include "AtEthPort.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Tfi5SerdesController
    {
    tAtSerdesController super;
    }tTha60210011Tfi5SerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60210011SemControllerNew(AtDevice device, uint32 semId);

AtSerdesController Tha60210011Tfi5SerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);
AtSerdesController Tha60210011EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210021EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210021EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha6021EthPortSgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha6021EthPortQsgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210021EthPortSgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210031EthPortQsgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210031EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210051EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210051EthPortXfiSerdesControllerCrossPointNew(AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha60210012EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);

uint32 Tha6021ModuleEthDsCardXfiDiagBaseAddress(ThaModuleEth self, uint32 serdesId);
uint32 Tha6021ModuleEthQSgmiiDiagBaseAddress(ThaModuleEth self, uint32 serdesId);
uint32 Tha6021ModuleEthSgmiiDiagBaseAddress(ThaModuleEth self, uint32 serdesId);

eAtEthPortInterface Tha6021ModuleEthSerdesInterface(ThaModuleEth self, uint32 serdesId);
eBool Tha6021PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param);
uint32 Tha6021EthPortSerdesEqualizerModeCoeficSw2Hw(eAtSerdesEqualizerMode mode);
uint32 Tha6021EthPortSerdesEqualizerModeSw2Hw(eAtSerdesEqualizerMode mode);
eAtSerdesEqualizerMode Tha6021EthPortSerdesEqualizerModeHw2Sw(uint32 mode);

/* Constructor */
AtSerdesController Tha60210011Tfi5SerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021PHYSICAL_H_ */


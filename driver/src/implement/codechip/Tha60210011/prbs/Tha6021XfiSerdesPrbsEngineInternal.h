/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha6021XfiSerdesPrbsEngineInternal.h
 * 
 * Created Date: Jul 11, 2015
 *
 * Description : ETH Port SERDES PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021XFISERDESPRBSENGINEINTERNAL_H_
#define _THA6021XFISERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha6021EthPortSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021XfiSerdesPrbsEngine * Tha6021XfiSerdesPrbsEngine;

typedef struct tTha6021XfiSerdesPrbsEngineMethods
    {
    eAtModulePrbsRet (*DefaultSet)(Tha6021XfiSerdesPrbsEngine self);
    uint32 (*HwCounterReadToClear)(Tha6021XfiSerdesPrbsEngine self, uint16 counterType, eBool r2c);
    eBool (*OnlyOneEngine)(Tha6021XfiSerdesPrbsEngine self);
    }tTha6021XfiSerdesPrbsEngineMethods;

typedef struct tTha6021XfiSerdesPrbsEngine
    {
    tAtPrbsEngine super;
    const tTha6021XfiSerdesPrbsEngineMethods *methods;
    }tTha6021XfiSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6021XfiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

uint32 Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(AtPrbsEngine self, uint32 offset);
eBool Tha6021XfiSerdesPrbsEngineNothingReceived(AtPrbsEngine self);
void Tha6021XfiSerdesPrbsEngineClearFulledCounters(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021XFISERDESPRBSENGINEINTERNAL_H_ */

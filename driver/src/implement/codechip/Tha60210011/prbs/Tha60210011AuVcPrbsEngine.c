/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210011AuVcPrbsEngine.c
 *
 * Created Date: Sep 15, 2015
 *
 * Description : Tha60210011 AU VC PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../map/Tha60210011ModuleMapInternal.h"
#include "Tha60210011ModulePrbsReg.h"
#include "Tha60210011ModulePrbsInternal.h"
#include "Tha60210011AuVcPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011AuVcPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011AuVcPrbsEngineMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(uint32 engineId)
    {
    return cHoBaseAddress + engineId * 2048UL;
    }

static uint32 BertGenSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6Reg_sel_ho_bert_gen_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 BertGenEnableMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_sel_ho_bert_gen_gen_en_Mask;
    }

static uint32 BertGenChannelIdMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_sel_ho_bert_gen_stsid_Mask | cAf6_sel_ho_bert_gen_line_id_Mask;
    }

static uint32 BertMonTdmSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6Reg_sel_ho_bert_tdm_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 BertMonTdmEnableMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_sel_ho_bert_tdm_mon_montdmen_Mask;
    }

static uint32 BertMonTdmChannelIdMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_sel_ho_bert_tdm_mon_masterID_Mask | cAf6_sel_ho_bert_tdm_mon_tdmlineid_Mask;
    }

static uint32 BertMonPsnSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6Reg_sel_ho_bert_pw_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 BertMonPsnEnableMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_sel_ho_bert_pw_mon_monpwen_Mask;
    }

static uint32 BertMonPsnChannelIdMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_sel_ho_bert_pw_mon_masterID_Mask | cAf6_sel_ho_bert_pw_mon_pwlineid_Mask;
    }

static uint32 Read(AtPrbsEngine self, uint32 regAddr)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    return mChannelHwRead(channel, regAddr, cAtModulePrbs);
    }

static void Write(AtPrbsEngine self, uint32 regAddr, uint32 regVal)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePrbs);
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    uint32 regAddr;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);
    uint32 rxErrorBit;
    uint32 rxSyncBit;
    uint32 rxLossBit;

    AtUnused(clear);

    regAddr = mMethodsGet(mThis(self))->TxTdmGoodBitCounterReg(mThis(self));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterTxBit, Read(self, regAddr));

    if (side == cAtPrbsSideTdm)
        {
        rxErrorBit = Read(self, mMethodsGet(mThis(self))->RxTdmErrorBitCounterReg(mThis(self)));
        rxSyncBit  = Read(self, mMethodsGet(mThis(self))->RxTdmGoodBitCounterReg(mThis(self)));
        rxLossBit  = Read(self, mMethodsGet(mThis(self))->RxTdmLossBitCounterReg(mThis(self)));

        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxSyncBit + rxLossBit);
        return cAtOk;
        }

    if (side == cAtPrbsSidePsn)
        {
        rxErrorBit = Read(self, mMethodsGet(mThis(self))->RxPsnErrorBitCounterReg(mThis(self)));
        rxSyncBit  = Read(self, mMethodsGet(mThis(self))->RxPsnGoodBitCounterReg(mThis(self)));
        rxLossBit  = Read(self, mMethodsGet(mThis(self))->RxPsnLossBitCounterReg(mThis(self)));

        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxSyncBit + rxLossBit);
        return cAtOk;
        }

    return cAtErrorRsrcNoAvail;
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool r2c)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 alarmMask = 0;
    uint32 alarmRet = 0;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->RxTdmLossStickyReg(mThis(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->RxPsnLossStickyReg(mThis(self));
    else
        return 0;

    regVal = Read(self, regAddr);
    alarmMask = mMethodsGet(mThis(self))->LossAlarmStickyMask(mThis(self));
    alarmRet = (regVal & alarmMask) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;

    /* Clear Alarm when have Alarm actually and r2c Flag */
    if (r2c && (alarmRet != cAtPrbsEngineAlarmTypeNone))
        Write(self, regAddr, alarmMask);

    return alarmRet;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs15                : return cAtTrue;
        case cAtPrbsModePrbs20rStdO153        : return cAtTrue;
        case cAtPrbsModePrbs20StdO151         : return cAtTrue;
        case cAtPrbsModePrbs23                : return cAtTrue;
        case cAtPrbsModePrbsFixedPattern4Bytes: return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 TxPrbsModeSw2Hw(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs20rStdO153: return 0x4;
        case cAtPrbsModePrbs20StdO151 : return 0x8;
        case cAtPrbsModePrbs15        : return 0x2;
        case cAtPrbsModePrbs23        : return 0x10;
        case cAtPrbsModePrbs31        : return 0x20;
        default: break;
        }

    if (prbsMode != cAtPrbsModePrbsFixedPattern4Bytes)
        return 0;

    if (mThis(self)->txFixedPattern == 0)
        return 1;

    if (mThis(self)->txFixedPattern == 0xFFFFFFFF)
        return 0x80;

    return 0;
    }

static uint32 RxPrbsModeSw2Hw(AtPrbsEngine self, eAtPrbsMode prbsMode, eAtPrbsSide side)
    {
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs20rStdO153: return 0x8;
        case cAtPrbsModePrbs20StdO151 : return 0x10;
        case cAtPrbsModePrbs15        : return 0x4;
        case cAtPrbsModePrbs23        : return 0x20;
        case cAtPrbsModePrbs31        : return 0x40;
        default:
            break;
        }

    if (prbsMode != cAtPrbsModePrbsFixedPattern4Bytes)
        return 0;

    if (side == cAtPrbsSideTdm)
        {
        if (mThis(self)->rxTdmFixedPattern == 0)
            return 2;

        if (mThis(self)->rxTdmFixedPattern == 0xFFFFFFFF)
            return 1;
        }

    if (side == cAtPrbsSidePsn)
        {
        if (mThis(self)->rxPsnFixedPattern == 0)
            return 2;

        if (mThis(self)->rxPsnFixedPattern == 0xFFFFFFFF)
            return 1;
        }

    return 0;
    }

static uint32 TdmGenControlReg(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->TdmGenControlRegister(mThis(self));
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regVal;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddr = TdmGenControlReg(self);
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_ctrl_pen_gen_patt_mode_, TxPrbsModeSw2Hw(self, prbsMode));
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode TxPrbsModeHw2Sw(uint32 prbsMode)
    {
    switch (prbsMode)
        {
        case 0x4 : return cAtPrbsModePrbs20rStdO153;
        case 0x8 : return cAtPrbsModePrbs20StdO151;
        case 0x2 : return cAtPrbsModePrbs15;
        case 0x10: return cAtPrbsModePrbs23;
        case 0x20: return cAtPrbsModePrbs31;
        case 0x1 : return cAtPrbsModePrbsFixedPattern4Bytes;
        case 0x80: return cAtPrbsModePrbsFixedPattern4Bytes;
        default  : return cAtPrbsModeInvalid;
        }
    }

static eAtPrbsMode RxPrbsModeHw2Sw(uint32 prbsMode)
    {
    switch (prbsMode)
        {
        case 0x8 : return cAtPrbsModePrbs20rStdO153;
        case 0x10: return cAtPrbsModePrbs20StdO151;
        case 0x4 : return cAtPrbsModePrbs15;
        case 0x20: return cAtPrbsModePrbs23;
        case 0x40: return cAtPrbsModePrbs31;
        case 0x1 : return cAtPrbsModePrbsFixedPattern4Bytes;
        case 0x2 : return cAtPrbsModePrbsFixedPattern4Bytes;
        default  : return cAtPrbsModeInvalid;
        }
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = TdmGenControlReg(self);
    uint32 regVal = Read(self, regAddr);
    return TxPrbsModeHw2Sw(mRegField(regVal, cAf6_ctrl_pen_gen_patt_mode_));
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    if (side == cAtPrbsSideTdm)
        {
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cAf6_ctrl_pen_tdm_mon_patt_mode_, RxPrbsModeSw2Hw(self, prbsMode, cAtPrbsSideTdm));
        }
    else if (side == cAtPrbsSidePsn)
        {
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cAf6_ctrl_pen_pw_mon_patt_mode_, RxPrbsModeSw2Hw(self, prbsMode, cAtPrbsSidePsn));
        }
    else
        return cAtErrorRsrcNoAvail;

    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
        regVal  = Read(self, regAddr);
        return RxPrbsModeHw2Sw(mRegField(regVal, cAf6_ctrl_pen_tdm_mon_patt_mode_));
        }

    if (side == cAtPrbsSidePsn)
        {
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
        regVal  = Read(self, regAddr);
        return RxPrbsModeHw2Sw(mRegField(regVal, cAf6_ctrl_pen_pw_mon_patt_mode_));
        }

    return cAtPrbsModeInvalid;
    }

static uint8 HwRxFixedPattern(uint32 fixedPattern)
    {
    return (fixedPattern == 0) ? 2 : 1;
    }

static uint8 HwTxFixedPattern(uint32 fixedPattern)
    {
    return (fixedPattern == 0) ? 1 : 0x80;
    }

static uint32 SwRxFixedPattern(uint8 hwMode)
    {
    if (hwMode == 2)
        return 0;

    if (hwMode == 1)
        return 0xFFFFFFFF;

    return 0;
    }

static uint32 SwTxFixedPattern(uint8 hwMode)
    {
    if (hwMode == 1)
        return 0;

    if (hwMode == 0x80)
        return 0xFFFFFFFF;

    return 0;
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr, regVal;
    uint8 hwMode;

    if ((fixedPattern != 0) && (fixedPattern != 0xFFFFFFFF))
        return cAtErrorModeNotSupport;

    if (AtPrbsEngineTxModeGet(self) != cAtPrbsModePrbsFixedPattern4Bytes)
        {
        mThis(self)->txFixedPattern = fixedPattern;
        return cAtOk;
        }

    hwMode = HwTxFixedPattern(fixedPattern);
    regAddr = TdmGenControlReg(self);
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_ctrl_pen_gen_patt_mode_, hwMode);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr = TdmGenControlReg(self);
    uint32 regVal  = Read(self, regAddr);
    uint8 hwMode = (uint8)mRegField(regVal, cAf6_ctrl_pen_gen_patt_mode_);
    return SwTxFixedPattern(hwMode);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr, regVal;
    uint8 hwMode;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if ((fixedPattern != 0) && (fixedPattern != 0xFFFFFFFF))
        return cAtErrorModeNotSupport;

    if (AtPrbsEngineTxModeGet(self) != cAtPrbsModePrbsFixedPattern4Bytes)
        {
        if (side == cAtPrbsSideTdm)
            mThis(self)->rxTdmFixedPattern = fixedPattern;
        if (side == cAtPrbsSidePsn)
            mThis(self)->rxPsnFixedPattern = fixedPattern;
        return cAtOk;
        }

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
    else
        return cAtErrorRsrcNoAvail;

    hwMode = HwRxFixedPattern(fixedPattern);
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_ctrl_pen_tdm_mon_patt_mode_, hwMode);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;
    uint8 hwMode;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
    else
        return 0;

    regVal = Read(self, regAddr);
    hwMode = (uint8)mRegField(regVal, cAf6_ctrl_pen_tdm_mon_patt_mode_);
    return SwRxFixedPattern(hwMode);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddr = TdmGenControlReg(self);
    uint32 regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_ctrl_pen_gen_invmode_, mBoolToBin(invert));
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    uint32 regAddr = TdmGenControlReg(self);
    uint32 regVal  = Read(self, regAddr);
    return mBinToBool(mRegField(regVal, cAf6_ctrl_pen_gen_invmode_));
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
    else
        return cAtErrorRsrcNoAvail;

    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_ctrl_pen_tdm_mon_invmode_, mBoolToBin(invert));
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
    else
        return cAtErrorRsrcNoAvail;

    regVal  = Read(self, regAddr);
    return mBinToBool(mRegField(regVal, cAf6_ctrl_pen_tdm_mon_invmode_));
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    AtUnused(self);
    AtUnused(numErrors);
    return cAtErrorModeNotSupport;
    }

static uint32 BertGenErrorRateInsertRegAddress(ThaPrbsEngine self)
    {
    return cAf6Reg_ctrl_ber_pen_Base + DefaultOffset(mHwEngine(self));
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    uint32 regAddr, regVal;

    if (!ThaPrbsEngineBitOrderIsSupported(order))
        return cAtErrorModeNotSupport;

    regAddr = TdmGenControlReg(self);
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cAf6_ctrl_pen_gen_swapmode_, (order == cAtPrbsBitOrderMsb) ? 0 : 1);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
    {
    uint32 regAddr = TdmGenControlReg(self);
    uint32 regVal = Read(self, regAddr);
    return (mRegField(regVal, cAf6_ctrl_pen_gen_swapmode_) == 0) ? cAtPrbsBitOrderMsb : cAtPrbsBitOrderLsb;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if ((side == cAtPrbsSideTdm) || (side == cAtPrbsSidePsn))
        {
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
        regVal  = Read(self, regAddr);
        mRegFieldSet(regVal, cAf6_ctrl_pen_tdm_mon_swapmode_, (order == cAtPrbsBitOrderMsb) ? 0 : 1);
        Write(self, regAddr, regVal);

        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
        regVal  = Read(self, regAddr);
        mRegFieldSet(regVal, cAf6_ctrl_pen_tdm_mon_swapmode_, (order == cAtPrbsBitOrderMsb) ? 0 : 1);
        Write(self, regAddr, regVal);
        }
    else
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->TdmMonControlRegister(mThis(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->PsnMonControlRegister(mThis(self));
    else
        return cAtErrorRsrcNoAvail;

    regVal  = Read(self, regAddr);
    return (mRegField(regVal, cAf6_ctrl_pen_tdm_mon_swapmode_) == 0) ? cAtPrbsBitOrderMsb : cAtPrbsBitOrderLsb;
    }

static void HwDefault(ThaPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    Tha60210011MapHoBertHwDefault((ThaModuleMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhChannel)channel);
    ThaPrbsEngineChannelizedPrbsDefaultSet(self);
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    uint8 slice, hwSts;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);

    return (uint32)hwSts | ((uint32)slice << 6);
    }

static void ResourceRemove(ThaPrbsEngine self)
    {
    uint32 hwEngineId = ThaPrbsEngineHwEngineIdGet(self);
    ThaBitMaskClearBit(Tha60210011ModulePrbsHoResourceMask(ThaPrbsEngineModuleGet((AtPrbsEngine)self)), hwEngineId);
    }

static uint32 TdmMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return cAf6Reg_stt_tdm_mon_Base + DefaultOffset(engineId);
    }

static uint32 PsnMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return cAf6Reg_stt_pw_mon_Base + DefaultOffset(engineId);
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    uint8 slice, hwSts;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);

    return (uint32)hwSts | ((uint32)slice << 10);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011AuVcPrbsEngine *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txFixedPattern);
    mEncodeUInt(rxTdmFixedPattern);
    mEncodeUInt(rxPsnFixedPattern);
    }

static eBool TxErrorInjectionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Restore(ThaPrbsEngine self)
    {
    uint32 i;
    uint32 hwChannelId;
    uint32 freeHwId = cInvalidUint32;
    uint32 channelId = ThaPrbsEngineChannelId(self);
    AtPrbsEngine engine = (AtPrbsEngine)self;
    ThaModulePrbs module = (ThaModulePrbs)ThaPrbsEngineModuleGet(engine);

    for (i = 0; i < ThaModulePrbsMaxNumHoPrbsEngine(module); i++)
        {
        uint32 regAddr = TdmGenControlReg((AtPrbsEngine) self);
        uint32 regVal = Read(engine, regAddr);
        uint32 mask = Tha60210011AuVcPrbsEngineBertGenChannelIdMask();
        uint32 shift = AtRegMaskToShift(mask);
        mFieldGet(regVal, mask, shift, uint32, &hwChannelId);
        if (channelId == hwChannelId)
            freeHwId = i;
        }

    if (freeHwId == cInvalidUint32)
        {
        AtChannelLog(AtPrbsEngineChannelGet(engine), cAtLogLevelCritical, AtSourceLocation, "Can not find BERT HW ID during HA restore\r\n");
        return;
        }

    ThaPrbsEngineHwEngineIdSet(self, freeHwId);
    ThaBitMaskSetBit(Tha60210011ModulePrbsHoResourceMask(module), freeHwId);
    }

static uint32 TdmGenControlRegister(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_ctrl_pen_gen_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 TdmMonControlRegister(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_ctrl_pen_tdm_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 PsnMonControlRegister(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_ctrl_pen_pw_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 TxTdmGoodBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_goodbit_ber_pen_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxTdmGoodBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_goodbit_pen_tdm_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxTdmErrorBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_err_pen_tdm_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxTdmLossBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_lossbit_pen_tdm_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxPsnGoodBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_goodbit_pen_pw_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxPsnErrorBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_errbit_pen_pw_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxPsnLossBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_lossbit_pen_pw_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxTdmLossStickyReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_loss_tdm_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 RxPsnLossStickyReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_loss_pw_mon_Base + DefaultOffset(mHwEngine(self));
    }

static uint32 LossAlarmStickyMask(Tha60210011AuVcPrbsEngine self)
    {
    AtUnused(self);
    return cBit0;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInjectionIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, BertGenSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertGenEnableMask);
        mMethodOverride(m_ThaPrbsEngineOverride, BertGenChannelIdMask);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonTdmSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonTdmEnableMask);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonTdmChannelIdMask);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonPsnSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonPsnEnableMask);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonPsnChannelIdMask);
        mMethodOverride(m_ThaPrbsEngineOverride, HwDefault);
        mMethodOverride(m_ThaPrbsEngineOverride, BertGenErrorRateInsertRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, ResourceRemove);
        mMethodOverride(m_ThaPrbsEngineOverride, TdmMonitoringStatusRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, PsnMonitoringStatusRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, Restore);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011AuVcPrbsEngine);
    }

static void MethodsInit(Tha60210011AuVcPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TdmGenControlRegister);
        mMethodOverride(m_methods, TdmMonControlRegister);
        mMethodOverride(m_methods, PsnMonControlRegister);

        mMethodOverride(m_methods, TxTdmGoodBitCounterReg);
        mMethodOverride(m_methods, RxTdmGoodBitCounterReg);
        mMethodOverride(m_methods, RxTdmErrorBitCounterReg);
        mMethodOverride(m_methods, RxTdmLossBitCounterReg);
        mMethodOverride(m_methods, RxPsnGoodBitCounterReg);
        mMethodOverride(m_methods, RxPsnErrorBitCounterReg);
        mMethodOverride(m_methods, RxPsnLossBitCounterReg);
        mMethodOverride(m_methods, RxTdmLossStickyReg);
        mMethodOverride(m_methods, RxPsnLossStickyReg);
        mMethodOverride(m_methods, LossAlarmStickyMask);
        }

    mMethodsSet(self, &m_methods);
    }

AtPrbsEngine Tha60210011AuVcPrbsEngineObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineAuVcObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha60210011AuVcPrbsEngine) self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210011AuVcPrbsEngineNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210011AuVcPrbsEngineObjectInit(newEngine, vc, engineId);
    }

uint32 Tha60210011AuVcPrbsEngineDefaultOffset(uint32 engineId)
    {
    return DefaultOffset(engineId);
    }

uint32 Tha60210011AuVcPrbsEngineBertGenChannelIdMask(void)
    {
    return cAf6_sel_ho_bert_gen_stsid_Mask | cAf6_sel_ho_bert_gen_line_id_Mask;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210011ModulePrbsReg.h
 * 
 * Created Date: Nov 12, 2015
 *
 * Description : PRBS register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPRBSREG_H_
#define _THA60210011MODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen
Reg Addr   : 0x8_200 - 0xB_A00
Reg Formula: 0x8_200 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen_Base                                                                    0x8200
#define cAf6Reg_sel_ho_bert_gen(engid)                                                   (0x8200+2048*(engid))
#define cAf6Reg_sel_ho_bert_gen_WidthVal                                                                    32
#define cAf6Reg_sel_ho_bert_gen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_gen_en_Bit_Start                                                                9
#define cAf6_sel_ho_bert_gen_gen_en_Bit_End                                                                  9
#define cAf6_sel_ho_bert_gen_gen_en_Mask                                                                 cBit9
#define cAf6_sel_ho_bert_gen_gen_en_Shift                                                                    9
#define cAf6_sel_ho_bert_gen_gen_en_MaxVal                                                                 0x1
#define cAf6_sel_ho_bert_gen_gen_en_MinVal                                                                 0x0
#define cAf6_sel_ho_bert_gen_gen_en_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: line_id
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_line_id_Bit_Start                                                               6
#define cAf6_sel_ho_bert_gen_line_id_Bit_End                                                                 8
#define cAf6_sel_ho_bert_gen_line_id_Mask                                                              cBit8_6
#define cAf6_sel_ho_bert_gen_line_id_Shift                                                                   6
#define cAf6_sel_ho_bert_gen_line_id_MaxVal                                                                0x7
#define cAf6_sel_ho_bert_gen_line_id_MinVal                                                                0x0
#define cAf6_sel_ho_bert_gen_line_id_RstVal                                                                0x0

/*--------------------------------------
BitField Name: stsid
BitField Type: RW
BitField Desc: STS ID
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_stsid_Bit_Start                                                                 0
#define cAf6_sel_ho_bert_gen_stsid_Bit_End                                                                   5
#define cAf6_sel_ho_bert_gen_stsid_Mask                                                                cBit5_0
#define cAf6_sel_ho_bert_gen_stsid_Shift                                                                     0
#define cAf6_sel_ho_bert_gen_stsid_MaxVal                                                                 0x3f
#define cAf6_sel_ho_bert_gen_stsid_MinVal                                                                  0x0
#define cAf6_sel_ho_bert_gen_stsid_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert Gen
Reg Addr   : 0x8_300 - 0xB_B00
Reg Formula: 0x8_300 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_gen_Base                                                                       0x8300
#define cAf6Reg_ctrl_pen_gen(engid)                                                      (0x8300+2048*(engid))
#define cAf6Reg_ctrl_pen_gen_WidthVal                                                                       32
#define cAf6Reg_ctrl_pen_gen_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_swapmode_Bit_Start                                                                 9
#define cAf6_ctrl_pen_gen_swapmode_Bit_End                                                                   9
#define cAf6_ctrl_pen_gen_swapmode_Mask                                                                  cBit9
#define cAf6_ctrl_pen_gen_swapmode_Shift                                                                     9
#define cAf6_ctrl_pen_gen_swapmode_MaxVal                                                                  0x1
#define cAf6_ctrl_pen_gen_swapmode_MinVal                                                                  0x0
#define cAf6_ctrl_pen_gen_swapmode_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_invmode_Bit_Start                                                                  8
#define cAf6_ctrl_pen_gen_invmode_Bit_End                                                                    8
#define cAf6_ctrl_pen_gen_invmode_Mask                                                                   cBit8
#define cAf6_ctrl_pen_gen_invmode_Shift                                                                      8
#define cAf6_ctrl_pen_gen_invmode_MaxVal                                                                   0x1
#define cAf6_ctrl_pen_gen_invmode_MinVal                                                                   0x0
#define cAf6_ctrl_pen_gen_invmode_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x04 : prbs20r #
0x08 : prbs20 # 0x10 : prbs23 # 0x20 : prbs31 # other: all1
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_patt_mode_Bit_Start                                                                0
#define cAf6_ctrl_pen_gen_patt_mode_Bit_End                                                                  7
#define cAf6_ctrl_pen_gen_patt_mode_Mask                                                               cBit7_0
#define cAf6_ctrl_pen_gen_patt_mode_Shift                                                                    0
#define cAf6_ctrl_pen_gen_patt_mode_MaxVal                                                                0xff
#define cAf6_ctrl_pen_gen_patt_mode_MinVal                                                                 0x0
#define cAf6_ctrl_pen_gen_patt_mode_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Inser Error
Reg Addr   : 0x8_320 - 0xB_B20
Reg Formula: 0x8_320 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select rate inser error

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_ber_pen_Base                                                                       0x8320
#define cAf6Reg_ctrl_ber_pen(engid)                                                      (0x8320+2048*(engid))
#define cAf6Reg_ctrl_ber_pen_WidthVal                                                                       32
#define cAf6Reg_ctrl_ber_pen_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: ber_rate
BitField Type: RW
BitField Desc: TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to
Pattern Generator [31:0] == 32'd0          :disable  BER_level_val BER_level
1000:         BER 10e-3 10_000:       BER 10e-4 100_000:      BER 10e-5
1_000_000:    BER 10e-6 10_000_000:   BER 10e-7 100_000_000:  BER 10e-8
1_000_000_000 : BER 10e-9
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ctrl_ber_pen_ber_rate_Bit_Start                                                                 0
#define cAf6_ctrl_ber_pen_ber_rate_Bit_End                                                                  31
#define cAf6_ctrl_ber_pen_ber_rate_Mask                                                               cBit31_0
#define cAf6_ctrl_ber_pen_ber_rate_Shift                                                                     0
#define cAf6_ctrl_ber_pen_ber_rate_MaxVal                                                           0xffffffff
#define cAf6_ctrl_ber_pen_ber_rate_MinVal                                                                  0x0
#define cAf6_ctrl_ber_pen_ber_rate_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter num of bit gen
Reg Addr   : 0x8_380 - 0xB_B80
Reg Formula: 0x8_380 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers counter bit genertaie in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_ber_pen_Base                                                                    0x8380
#define cAf6Reg_goodbit_ber_pen(engid)                                                   (0x8380+2048*(engid))
#define cAf6Reg_goodbit_ber_pen_WidthVal                                                                    32
#define cAf6Reg_goodbit_ber_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: goodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_ber_pen_goodbit_Bit_Start                                                               0
#define cAf6_goodbit_ber_pen_goodbit_Bit_End                                                                31
#define cAf6_goodbit_ber_pen_goodbit_Mask                                                             cBit31_0
#define cAf6_goodbit_ber_pen_goodbit_Shift                                                                   0
#define cAf6_goodbit_ber_pen_goodbit_MaxVal                                                         0xffffffff
#define cAf6_goodbit_ber_pen_goodbit_MinVal                                                                0x0
#define cAf6_goodbit_ber_pen_goodbit_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert TDM Mon
Reg Addr   : 0x8_400 - 0xB_C00
Reg Formula: 0x8_400 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_tdm_mon_Base                                                                0x8400
#define cAf6Reg_sel_ho_bert_tdm_mon(engid)                                               (0x8400+2048*(engid))
#define cAf6Reg_sel_ho_bert_tdm_mon_WidthVal                                                                32
#define cAf6Reg_sel_ho_bert_tdm_mon_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: montdmen
BitField Type: RW
BitField Desc: set "1" to enable bert tdm mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Bit_Start                                                         13
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Bit_End                                                           13
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Mask                                                          cBit13
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Shift                                                             13
#define cAf6_sel_ho_bert_tdm_mon_montdmen_MaxVal                                                           0x1
#define cAf6_sel_ho_bert_tdm_mon_montdmen_MinVal                                                           0x0
#define cAf6_sel_ho_bert_tdm_mon_montdmen_RstVal                                                           0x0

/*--------------------------------------
BitField Name: tdmlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_Bit_Start                                                        10
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_Bit_End                                                          12
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_Mask                                                      cBit12_10
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_Shift                                                            10
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_MaxVal                                                          0x7
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_MinVal                                                          0x0
#define cAf6_sel_ho_bert_tdm_mon_tdmlineid_RstVal                                                          0x0

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon_masterID_Bit_Start                                                          0
#define cAf6_sel_ho_bert_tdm_mon_masterID_Bit_End                                                            9
#define cAf6_sel_ho_bert_tdm_mon_masterID_Mask                                                         cBit9_0
#define cAf6_sel_ho_bert_tdm_mon_masterID_Shift                                                              0
#define cAf6_sel_ho_bert_tdm_mon_masterID_MaxVal                                                         0x3ff
#define cAf6_sel_ho_bert_tdm_mon_masterID_MinVal                                                           0x0
#define cAf6_sel_ho_bert_tdm_mon_masterID_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert PW Mon
Reg Addr   : 0x8_600 - 0xB_E00
Reg Formula: 0x8_600 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_pw_mon_Base                                                                 0x8600
#define cAf6Reg_sel_ho_bert_pw_mon(engid)                                                (0x8600+2048*(engid))
#define cAf6Reg_sel_ho_bert_pw_mon_WidthVal                                                                 32
#define cAf6Reg_sel_ho_bert_pw_mon_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: monpwen
BitField Type: RW
BitField Desc: set "1" to enable bert pw mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_monpwen_Bit_Start                                                           13
#define cAf6_sel_ho_bert_pw_mon_monpwen_Bit_End                                                             13
#define cAf6_sel_ho_bert_pw_mon_monpwen_Mask                                                            cBit13
#define cAf6_sel_ho_bert_pw_mon_monpwen_Shift                                                               13
#define cAf6_sel_ho_bert_pw_mon_monpwen_MaxVal                                                             0x1
#define cAf6_sel_ho_bert_pw_mon_monpwen_MinVal                                                             0x0
#define cAf6_sel_ho_bert_pw_mon_monpwen_RstVal                                                             0x0

/*--------------------------------------
BitField Name: pwlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_pwlineid_Bit_Start                                                          10
#define cAf6_sel_ho_bert_pw_mon_pwlineid_Bit_End                                                            12
#define cAf6_sel_ho_bert_pw_mon_pwlineid_Mask                                                        cBit12_10
#define cAf6_sel_ho_bert_pw_mon_pwlineid_Shift                                                              10
#define cAf6_sel_ho_bert_pw_mon_pwlineid_MaxVal                                                            0x7
#define cAf6_sel_ho_bert_pw_mon_pwlineid_MinVal                                                            0x0
#define cAf6_sel_ho_bert_pw_mon_pwlineid_RstVal                                                            0x0

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_masterID_Bit_Start                                                           0
#define cAf6_sel_ho_bert_pw_mon_masterID_Bit_End                                                             9
#define cAf6_sel_ho_bert_pw_mon_masterID_Mask                                                          cBit9_0
#define cAf6_sel_ho_bert_pw_mon_masterID_Shift                                                               0
#define cAf6_sel_ho_bert_pw_mon_masterID_MaxVal                                                          0x3ff
#define cAf6_sel_ho_bert_pw_mon_masterID_MinVal                                                            0x0
#define cAf6_sel_ho_bert_pw_mon_masterID_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert TDM mon
Reg Addr   : 0x8_510 - 0xB_D10
Reg Formula: 0x8_510 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select mode bert mon in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_tdm_mon_Base                                                                   0x8510
#define cAf6Reg_ctrl_pen_tdm_mon(engid)                                                  (0x8510+2048*(engid))
#define cAf6Reg_ctrl_pen_tdm_mon_WidthVal                                                                   32
#define cAf6Reg_ctrl_pen_tdm_mon_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: thrhold pattern sync
BitField Type: RW
BitField Desc: minimum number of byte sync to go sync state
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Bit_Start                                                16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Bit_End                                                  19
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Mask                                              cBit19_16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Shift                                                    16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_MaxVal                                                  0xf
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_MinVal                                                  0x0
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_RstVal                                                  0x0

/*--------------------------------------
BitField Name: thrhold error
BitField Type: RW
BitField Desc: maximum err in state sync, if more than thrhold go loss sync
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Bit_Start                                                       12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Bit_End                                                         15
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Mask                                                     cBit15_12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Shift                                                           12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_MaxVal                                                         0xf
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_MinVal                                                         0x0
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_RstVal                                                         0x0

/*--------------------------------------
BitField Name: reservee
BitField Type: RW
BitField Desc: reserve
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_reservee_Bit_Start                                                            10
#define cAf6_ctrl_pen_tdm_mon_reservee_Bit_End                                                              11
#define cAf6_ctrl_pen_tdm_mon_reservee_Mask                                                          cBit11_10
#define cAf6_ctrl_pen_tdm_mon_reservee_Shift                                                                10
#define cAf6_ctrl_pen_tdm_mon_reservee_MaxVal                                                              0x3
#define cAf6_ctrl_pen_tdm_mon_reservee_MinVal                                                              0x0
#define cAf6_ctrl_pen_tdm_mon_reservee_RstVal                                                              0x0

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_swapmode_Bit_Start                                                             9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Bit_End                                                               9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Mask                                                              cBit9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Shift                                                                 9
#define cAf6_ctrl_pen_tdm_mon_swapmode_MaxVal                                                              0x1
#define cAf6_ctrl_pen_tdm_mon_swapmode_MinVal                                                              0x0
#define cAf6_ctrl_pen_tdm_mon_swapmode_RstVal                                                              0x0

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_invmode_Bit_Start                                                              8
#define cAf6_ctrl_pen_tdm_mon_invmode_Bit_End                                                                8
#define cAf6_ctrl_pen_tdm_mon_invmode_Mask                                                               cBit8
#define cAf6_ctrl_pen_tdm_mon_invmode_Shift                                                                  8
#define cAf6_ctrl_pen_tdm_mon_invmode_MaxVal                                                               0x1
#define cAf6_ctrl_pen_tdm_mon_invmode_MinVal                                                               0x0
#define cAf6_ctrl_pen_tdm_mon_invmode_RstVal                                                               0x0

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 #
0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Bit_Start                                                            0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Bit_End                                                              7
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Mask                                                           cBit7_0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Shift                                                                0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_MaxVal                                                            0xff
#define cAf6_ctrl_pen_tdm_mon_patt_mode_MinVal                                                             0x0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert PW mon
Reg Addr   : 0x8_710 - 0xB_F10
Reg Formula: 0x8_710 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers select mode bert mon in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_pw_mon_Base                                                                    0x8710
#define cAf6Reg_ctrl_pen_pw_mon(engid)                                                   (0x8710+2048*(engid))
#define cAf6Reg_ctrl_pen_pw_mon_WidthVal                                                                    32
#define cAf6Reg_ctrl_pen_pw_mon_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: thrhold pattern sync
BitField Type: RW
BitField Desc: minimum number of byte sync to go sync state
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Bit_Start                                                 16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Bit_End                                                   19
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Mask                                               cBit19_16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Shift                                                     16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_MaxVal                                                   0xf
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_MinVal                                                   0x0
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_RstVal                                                   0x0

/*--------------------------------------
BitField Name: thrhold error
BitField Type: RW
BitField Desc: maximum err in state sync, if more than thrhold go loss sync
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Bit_Start                                                        12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Bit_End                                                          15
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Mask                                                      cBit15_12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Shift                                                            12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_MaxVal                                                          0xf
#define cAf6_ctrl_pen_pw_mon_thrhold_error_MinVal                                                          0x0
#define cAf6_ctrl_pen_pw_mon_thrhold_error_RstVal                                                          0x0

/*--------------------------------------
BitField Name: pw_reserve
BitField Type: RW
BitField Desc: pw reserve
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Bit_Start                                                           10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Bit_End                                                             11
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Mask                                                         cBit11_10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Shift                                                               10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_MaxVal                                                             0x3
#define cAf6_ctrl_pen_pw_mon_pw_reserve_MinVal                                                             0x0
#define cAf6_ctrl_pen_pw_mon_pw_reserve_RstVal                                                             0x0

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_swapmode_Bit_Start                                                              9
#define cAf6_ctrl_pen_pw_mon_swapmode_Bit_End                                                                9
#define cAf6_ctrl_pen_pw_mon_swapmode_Mask                                                               cBit9
#define cAf6_ctrl_pen_pw_mon_swapmode_Shift                                                                  9
#define cAf6_ctrl_pen_pw_mon_swapmode_MaxVal                                                               0x1
#define cAf6_ctrl_pen_pw_mon_swapmode_MinVal                                                               0x0
#define cAf6_ctrl_pen_pw_mon_swapmode_RstVal                                                               0x0

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_invmode_Bit_Start                                                               8
#define cAf6_ctrl_pen_pw_mon_invmode_Bit_End                                                                 8
#define cAf6_ctrl_pen_pw_mon_invmode_Mask                                                                cBit8
#define cAf6_ctrl_pen_pw_mon_invmode_Shift                                                                   8
#define cAf6_ctrl_pen_pw_mon_invmode_MaxVal                                                                0x1
#define cAf6_ctrl_pen_pw_mon_invmode_MinVal                                                                0x0
#define cAf6_ctrl_pen_pw_mon_invmode_RstVal                                                                0x0

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 #
0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_patt_mode_Bit_Start                                                             0
#define cAf6_ctrl_pen_pw_mon_patt_mode_Bit_End                                                               7
#define cAf6_ctrl_pen_pw_mon_patt_mode_Mask                                                            cBit7_0
#define cAf6_ctrl_pen_pw_mon_patt_mode_Shift                                                                 0
#define cAf6_ctrl_pen_pw_mon_patt_mode_MaxVal                                                             0xff
#define cAf6_ctrl_pen_pw_mon_patt_mode_MinVal                                                              0x0
#define cAf6_ctrl_pen_pw_mon_patt_mode_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : TDM loss sync
Reg Addr   : 0x8_502 - 0xB_D02
Reg Formula: 0x8_502 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers indicate bert mon loss sync in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_loss_tdm_mon_Base                                                                       0x8502
#define cAf6Reg_loss_tdm_mon(engid)                                                      (0x8502+2048*(engid))
#define cAf6Reg_loss_tdm_mon_WidthVal                                                                       32
#define cAf6Reg_loss_tdm_mon_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: sticky_err
BitField Type: W2C
BitField Desc: "1" indicate loss sync
BitField Bits: [0]
--------------------------------------*/
#define cAf6_loss_tdm_mon_sticky_err_Bit_Start                                                               0
#define cAf6_loss_tdm_mon_sticky_err_Bit_End                                                                 0
#define cAf6_loss_tdm_mon_sticky_err_Mask                                                                cBit0
#define cAf6_loss_tdm_mon_sticky_err_Shift                                                                   0
#define cAf6_loss_tdm_mon_sticky_err_MaxVal                                                                0x1
#define cAf6_loss_tdm_mon_sticky_err_MinVal                                                                0x0
#define cAf6_loss_tdm_mon_sticky_err_RstVal                                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : TDM mon status
Reg Addr   : 0x8_503 - 0xB_D03
Reg Formula: 0x8_503 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers indicate status of bert mon  in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_stt_tdm_mon_Base                                                                        0x8503
#define cAf6Reg_stt_tdm_mon(engid)                                                       (0x8503+2048*(engid))
#define cAf6Reg_stt_tdm_mon_WidthVal                                                                        32
#define cAf6Reg_stt_tdm_mon_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: tdmstate
BitField Type: RO
BitField Desc: "0" : out of  sync , "1,2" searching , "3" : pattern sync
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_stt_tdm_mon_tdmstate_Bit_Start                                                                  0
#define cAf6_stt_tdm_mon_tdmstate_Bit_End                                                                    2
#define cAf6_stt_tdm_mon_tdmstate_Mask                                                                 cBit2_0
#define cAf6_stt_tdm_mon_tdmstate_Shift                                                                      0
#define cAf6_stt_tdm_mon_tdmstate_MaxVal                                                                   0x7
#define cAf6_stt_tdm_mon_tdmstate_MinVal                                                                   0x0
#define cAf6_stt_tdm_mon_tdmstate_RstVal                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : PW loss sync
Reg Addr   : 0x8_702 - 0xB_F02
Reg Formula: 0x8_702 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers indicate bert mon loss sync in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_loss_pw_mon_Base                                                                        0x8702
#define cAf6Reg_loss_pw_mon(engid)                                                       (0x8702+2048*(engid))
#define cAf6Reg_loss_pw_mon_WidthVal                                                                        32
#define cAf6Reg_loss_pw_mon_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: sticky_err
BitField Type: W2C
BitField Desc: "1" indicate loss sync
BitField Bits: [0]
--------------------------------------*/
#define cAf6_loss_pw_mon_sticky_err_Bit_Start                                                                0
#define cAf6_loss_pw_mon_sticky_err_Bit_End                                                                  0
#define cAf6_loss_pw_mon_sticky_err_Mask                                                                 cBit0
#define cAf6_loss_pw_mon_sticky_err_Shift                                                                    0
#define cAf6_loss_pw_mon_sticky_err_MaxVal                                                                 0x1
#define cAf6_loss_pw_mon_sticky_err_MinVal                                                                 0x0
#define cAf6_loss_pw_mon_sticky_err_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : PW mon stt
Reg Addr   : 0x8_703 - 0xB_F03
Reg Formula: 0x8_703 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers indicate bert mon state in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_stt_pw_mon_Base                                                                         0x8703
#define cAf6Reg_stt_pw_mon(engid)                                                        (0x8703+2048*(engid))
#define cAf6Reg_stt_pw_mon_WidthVal                                                                         32
#define cAf6Reg_stt_pw_mon_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: pwstate
BitField Type: RO
BitField Desc: "0" : out of  sync , "1,2" searching , "3" : pattern sync
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_stt_pw_mon_pwstate_Bit_Start                                                                    0
#define cAf6_stt_pw_mon_pwstate_Bit_End                                                                      2
#define cAf6_stt_pw_mon_pwstate_Mask                                                                   cBit2_0
#define cAf6_stt_pw_mon_pwstate_Shift                                                                        0
#define cAf6_stt_pw_mon_pwstate_MaxVal                                                                     0x7
#define cAf6_stt_pw_mon_pwstate_MinVal                                                                     0x0
#define cAf6_stt_pw_mon_pwstate_RstVal                                                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : counter good bit TDM mon
Reg Addr   : 0x8_580 - 0xB_D80
Reg Formula: 0x8_580 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_Base                                                                0x8580
#define cAf6Reg_goodbit_pen_tdm_mon(engid)                                               (0x8580+2048*(engid))
#define cAf6Reg_goodbit_pen_tdm_mon_WidthVal                                                                32
#define cAf6Reg_goodbit_pen_tdm_mon_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_goodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Bit_Start                                                       0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Bit_End                                                        31
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Mask                                                     cBit31_0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Shift                                                           0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_MaxVal                                                 0xffffffff
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_MinVal                                                        0x0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : counter error bit in TDM mon
Reg Addr   : 0x8_560 - 0xB_D60
Reg Formula: 0x8_560 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_err_pen_tdm_mon_Base                                                                    0x8560
#define cAf6Reg_err_pen_tdm_mon(engid)                                                   (0x8560+2048*(engid))
#define cAf6Reg_err_pen_tdm_mon_WidthVal                                                                    32
#define cAf6Reg_err_pen_tdm_mon_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_err_pen_tdm_mon_cnt_errbit_Bit_Start                                                            0
#define cAf6_err_pen_tdm_mon_cnt_errbit_Bit_End                                                             31
#define cAf6_err_pen_tdm_mon_cnt_errbit_Mask                                                          cBit31_0
#define cAf6_err_pen_tdm_mon_cnt_errbit_Shift                                                                0
#define cAf6_err_pen_tdm_mon_cnt_errbit_MaxVal                                                      0xffffffff
#define cAf6_err_pen_tdm_mon_cnt_errbit_MinVal                                                             0x0
#define cAf6_err_pen_tdm_mon_cnt_errbit_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8_5A0 - 0xB_DA0
Reg Formula: 0x8_5A0 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_tdm_mon_Base                                                                0x85A0
#define cAf6Reg_lossbit_pen_tdm_mon(engid)                                               (0x85A0+2048*(engid))
#define cAf6Reg_lossbit_pen_tdm_mon_WidthVal                                                                32
#define cAf6Reg_lossbit_pen_tdm_mon_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Bit_Start                                                        0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Bit_End                                                         31
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Mask                                                      cBit31_0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Shift                                                            0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_MaxVal                                                  0xffffffff
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_MinVal                                                         0x0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : counter good bit TDM mon
Reg Addr   : 0x8_780 - 0xB_F80
Reg Formula: 0x8_780 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_pw_mon_Base                                                                 0x8780
#define cAf6Reg_goodbit_pen_pw_mon(engid)                                                (0x8780+2048*(engid))
#define cAf6Reg_goodbit_pen_pw_mon_WidthVal                                                                 32
#define cAf6Reg_goodbit_pen_pw_mon_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pwgoodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Bit_Start                                                      0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Bit_End                                                       31
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Mask                                                    cBit31_0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Shift                                                          0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_MaxVal                                                0xffffffff
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_MinVal                                                       0x0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : counter error bit in TDM mon
Reg Addr   : 0x8_760 - 0xB_F60
Reg Formula: 0x8_760 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_errbit_pen_pw_mon_Base                                                                  0x8760
#define cAf6Reg_errbit_pen_pw_mon(engid)                                                 (0x8760+2048*(engid))
#define cAf6Reg_errbit_pen_pw_mon_WidthVal                                                                  32
#define cAf6Reg_errbit_pen_pw_mon_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_pwerrbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Bit_Start                                                        0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Bit_End                                                         31
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Mask                                                      cBit31_0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Shift                                                            0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_MaxVal                                                  0xffffffff
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_MinVal                                                         0x0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8_7A0 - 0xB_FA0
Reg Formula: 0x8_7A0 + 2048*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_pw_mon_Base                                                                 0x87A0
#define cAf6Reg_lossbit_pen_pw_mon(engid)                                                (0x87A0+2048*(engid))
#define cAf6Reg_lossbit_pen_pw_mon_WidthVal                                                                 32
#define cAf6Reg_lossbit_pen_pw_mon_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pwlossbi
BitField Type: R2C
BitField Desc: counter loss bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Bit_Start                                                       0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Bit_End                                                        31
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Mask                                                     cBit31_0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Shift                                                           0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_MaxVal                                                 0xffffffff
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_MinVal                                                        0x0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_RstVal                                                        0x0

#endif /* _AF6_REG_AF6CCI0011_RD_MAP_HO_H_ */

#ifdef __cplusplus
}
#endif /* _THA60210011MODULEPRBSREG_H_ */


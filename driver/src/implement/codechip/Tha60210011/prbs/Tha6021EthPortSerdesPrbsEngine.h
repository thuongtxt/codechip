/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6021EthPortSerdesPrbsEngine.h
 * 
 * Created Date: Jul 12, 2015
 *
 * Description : ETH SERDES PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021ETHPORTSERDESPRBSENGINE_H_
#define _THA6021ETHPORTSERDESPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6021XfiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210051XfiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha6021DsxXfiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210031EthPortSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210031EthPortQSgmiiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210021EthPortSgmiiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60210051XfiSerdesPrbsEngineV2New(AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021ETHPORTSERDESPRBSENGINE_H_ */


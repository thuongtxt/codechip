/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6021XfiSerdesPrbsEngine.c
 *
 * Created Date: Jun 26, 2015
 *
 * Description : Ethernet Port SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../eth/Tha60210011ModuleEthInternal.h"
#include "../eth/Tha6021ModuleEthReg.h"
#include "AtSerdesController.h"
#include "Tha6021XfiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6EthernetPortFrameLenMask              cBit15_0
#define cAf6EthernetPortFrameLenShift             0

#define cAf6EthernetPortPrbsErrorInsertMask       cBit12
#define cAf6EthernetPortPrbsErrorInsertShift      12
#define cAf6EthernetPortFrameLenModeMask          cBit5_4
#define cAf6EthernetPortFrameLenModeShift         4
#define cAf6EthernetPortFrameLenModeIncreaseBy3   3
#define cAf6EthernetPortPrbsEnableMask            cBit0
#define cAf6EthernetPortPrbsEnableShift           0

/*
 * - PRBS error: PRBS data error
 * - PRBS packet error: data of received packets does not
 *   match transmitted packet
 */
#define cAf6EthernetPortRxPrbsErrorMask           cBit10
#define cAf6EthernetPortRxPrbsPktErrorMask        cBit11

#define cAf6EthernetPortRxFrameCountersMask       cBit7_0
#define cAf6EthernetPortRxFrameCountersShift      0
#define cAf6EthernetPortTxFrameCountersMask       cBit15_8
#define cAf6EthernetPortTxFrameCountersShift      8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6021XfiSerdesPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6021XfiSerdesPrbsEngineMethods m_methods;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static uint32 SerdesId(AtPrbsEngine self)
    {
    uint32 serdesSwId = AtPrbsEngineIdGet(self);
    ThaModuleEth moduleEth = (ThaModuleEth)(EthModule(self));
    return mMethodsGet(moduleEth)->SerdesIdSwToHw(moduleEth, serdesSwId);
    }

static uint32 BaseAddress(AtPrbsEngine self)
    {
    return ThaModuleEthDiagBaseAddress((ThaModuleEth)EthModule(self), SerdesId(self));
    }

static eBool OnlyOneEngine(AtPrbsEngine self)
    {
    if (ThaModuleEthFullSerdesPrbsSupported((ThaModuleEth)EthModule(self)))
        return cAtFalse;
    return cAtTrue;
    }

static uint32 PrbsControlRegister(AtPrbsEngine self)
    {
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x60);
    }

static uint32 MinLenControlRegister(AtPrbsEngine self)
    {
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x62);
    }

static uint32 MaxLenControlRegister(AtPrbsEngine self)
    {
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x61);
    }

static uint32 PrbsHistoryStatusRegister(AtPrbsEngine self)
    {
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x48);
    }

static uint32 PrbsCountersRead2ClearRegister(AtPrbsEngine self, eBool r2c)
    {
    AtUnused(r2c);
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x65);
    }

static eAtModulePrbsRet DefaultSet(Tha6021XfiSerdesPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    /* Default min length */
    regAddress = MinLenControlRegister(engine);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6EthernetPortFrameLen, 64);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    /* Default max length */
    regAddress = MaxLenControlRegister(engine);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6EthernetPortFrameLen, 1500);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    /* Default length mode */
    regAddress = PrbsControlRegister(engine);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6EthernetPortFrameLenMode, cAf6EthernetPortFrameLenModeIncreaseBy3);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtRet SerdesSelect(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = cEthPortQSGMIIXFIControlReg;
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = SerdesId(self);

    /* In case of disabling, if this SERDES is not being selected, just do nothing */
    if (!enable && (mRegField(regVal, cEthPortXFISerDesIdSelect) != serdesId))
        return cAtOk;

    mRegFieldSet(regVal, cEthPortXFISerDesIdSelect, serdesId);
    mRegFieldSet(regVal, cEthPortXfiDiagEnable, enable ? 1 : 0);
    if (enable)
        mRegFieldSet(regVal, cEthPortQSGMIIDiagEnable, 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool SerdesIsSelected(AtPrbsEngine self)
    {
    uint32 regVal  = AtPrbsEngineRead(self, cEthPortQSGMIIXFIControlReg, cAtModuleEth);
    uint32 serdesId = SerdesId(self);
    return (mRegField(regVal, cEthPortXFISerDesIdSelect) == serdesId) ? cAtTrue : cAtFalse;
    }

static eBool CanWork(AtPrbsEngine self)
    {
    if (!OnlyOneEngine(self))
        return cAtTrue;
    return SerdesIsSelected(self);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    AtPrbsEngineEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);

    /* Will set default when engine is first enabled */
    if (OnlyOneEngine(self))
        return cAtOk;

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddress, regValue;

    if (!CanWork(self))
        return force ? cAtErrorNotReady : cAtOk;

    regAddress = PrbsControlRegister(self);
    regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6EthernetPortPrbsErrorInsert, mBoolToBin(force));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;

    if (!CanWork(self))
        return cAtFalse;

    regAddress = PrbsControlRegister(self);
    regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    return mRegField(regValue, cAf6EthernetPortPrbsErrorInsert) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0x0;
    }

static AtSerdesController SerdesController(AtPrbsEngine self)
    {
    return AtModuleEthSerdesController(EthModule(self), SerdesId(self));
    }

static eBool NothingReceived(AtPrbsEngine self)
    {
    uint32 originalRxFrames;
    static const uint32 cCheckingTimeMs = 100;
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, currentTime;

    if (AtSerdesControllerLoopbackIsEnabled(SerdesController(self), cAtLoopbackModeLocal))
        return cAtFalse;

    /* Make sure that nothing is received within a fix period (100ms) */
    originalRxFrames = AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame);
    AtOsalCurTimeGet(&startTime);
    while (elapseTime < cCheckingTimeMs)
        {
        if (AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame) != originalRxFrames)
            return cAtFalse;

        /* Just give CPU a rest then retry */
        AtOsalUSleep(10);
        AtOsalCurTimeGet(&currentTime);
        elapseTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    /* Nothing is received */
    return cAtTrue;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddress = PrbsHistoryStatusRegister(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 alarms = 0;

    if (!CanWork(self))
        return 0x0;

    if (regValue & (cAf6EthernetPortRxPrbsErrorMask | cAf6EthernetPortRxPrbsPktErrorMask))
        alarms = cAtPrbsEngineAlarmTypeError;

    if (read2Clear)
        AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    if (alarms)
        return alarms;

    if (NothingReceived(self))
        return cAtPrbsEngineAlarmTypeError;

    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddress, regValue;
    eBool apsSupported = OnlyOneEngine(self);
    eBool shouldInitEngine = cAtFalse;

    /* In case of disable, if it has not been selected, just do nothing */
    if (apsSupported && !enable && !SerdesIsSelected(self))
        return cAtOk;

    if (enable && !AtPrbsEngineIsEnabled(self))
        shouldInitEngine = cAtTrue;

    regAddress = PrbsControlRegister(self);
    regValue   = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6EthernetPortPrbsEnable, mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);

    if (apsSupported)
        {
        if (shouldInitEngine)
            mMethodsGet(mThis(self))->DefaultSet(mThis(self));
        return SerdesSelect(self, enable);
        }

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;

    if (OnlyOneEngine(self))
        {
        uint32 regVal  = AtPrbsEngineRead(self, cEthPortQSGMIIXFIControlReg, cAtModuleEth);
        if (mRegField(regVal, cEthPortXFISerDesIdSelect) != SerdesId(self))
            return cAtFalse;
        if ((regVal & cEthPortXfiDiagEnableMask) == 0)
            return cAtFalse;
        }

    regAddress = PrbsControlRegister(self);
    regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    return mRegField(regValue, cAf6EthernetPortPrbsEnable) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs15;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtTrue : cAtFalse;
    }

static uint32 HwCounterReadToClear(Tha6021XfiSerdesPrbsEngine self, uint16 counterType, eBool r2c)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 regAddress = PrbsCountersRead2ClearRegister(engine, r2c);
    uint32 regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return mRegField(regValue, cAf6EthernetPortTxFrameCounters);
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return mRegField(regValue, cAf6EthernetPortRxFrameCounters);

    return 0x0;
    }

static uint32 CounterReadToClear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    if (!AtPrbsEngineCounterIsSupported(self, counterType))
        return 0;

    if (!CanWork(self))
        return 0;

    return mMethodsGet(mThis(self))->HwCounterReadToClear(mThis(self), counterType, r2c);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame))
        return cAtTrue;
    return cAtFalse;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtTrue : cAtFalse);
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static void Debug(AtPrbsEngine self)
    {
    uint32 regAddress = PrbsHistoryStatusRegister(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    if (!CanWork(self))
        return;

    m_AtPrbsEngineMethods->Debug(self);

    AtPrintc(cSevNormal, "* PRBS error: ");

    /* No error */
    if ((regValue & (cAf6EthernetPortRxPrbsErrorMask | cAf6EthernetPortRxPrbsPktErrorMask)) == 0)
        {
        AtPrintc(cSevInfo, "none\r\n");
        return;
        }

    /* Show error in more detail */
    if (regValue & cAf6EthernetPortRxPrbsErrorMask)
        AtPrintc(cSevCritical, "dataError ");
    if (regValue & cAf6EthernetPortRxPrbsPktErrorMask)
        AtPrintc(cSevCritical, "packetError ");
    AtPrintc(cSevNormal, "\r\n");

    /* Clear for next time */
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    }

static const char *TypeString(AtPrbsEngine self)
    {
    AtUnused(self);
    return "XFI";
    }

static void MethodsInit(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, HwCounterReadToClear);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, TypeString);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021XfiSerdesPrbsEngine);
    }

AtPrbsEngine Tha6021XfiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self,
                               AtSerdesControllerPhysicalPortGet(serdesController),
                               AtSerdesControllerIdGet(serdesController)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6021XfiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6021XfiSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

uint32 Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(AtPrbsEngine self, uint32 offset)
    {
    uint32 baseAddress = BaseAddress(self);
    if (OnlyOneEngine(self))
        return (baseAddress+ offset);
    return baseAddress + (SerdesId(self) * 0x1000) + offset;
    }

eBool Tha6021XfiSerdesPrbsEngineNothingReceived(AtPrbsEngine self)
    {
    if (self)
        return NothingReceived(self);
    return cAtFalse;
    }

void Tha6021XfiSerdesPrbsEngineClearFulledCounters(AtPrbsEngine self)
    {
    uint32 rxFrames, rxLostFrames;

    if (self == NULL)
        return;

    if (AtSerdesControllerLoopbackIsEnabled(SerdesController(self), cAtLoopbackModeLocal))
        return;

    rxFrames = AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxFrame);
    if (rxFrames == 0xFFFFFFFF)
        {
        AtPrbsEngineCounterClear(self, cAtPrbsEngineCounterRxFrame);
        AtPrbsEngineCounterClear(self, cAtPrbsEngineCounterTxFrame);
        }
    else
        return;

    rxLostFrames = AtPrbsEngineCounterGet(self, cAtPrbsEngineCounterRxLostFrame);
    if (rxLostFrames == 0xFFFFFFFF)
        AtPrbsEngineCounterClear(self, cAtPrbsEngineCounterRxLostFrame);
    }

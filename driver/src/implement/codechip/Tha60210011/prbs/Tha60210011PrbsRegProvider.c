/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210011PrbsRegProvider.c
 *
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../map/Tha60210011ModuleMap.h"
#include "Tha60210011PrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegMapBERTGenTxBerErrMask                  cBit31_0
#define cThaRegMapBERTGenTxBerErrShift                 0
#define cThaRegMapBERTGenTxBerMdMask                   cBit31_0
#define cThaRegMapBERTGenTxBerMdShift                  0

#define cBertTdmGenBaseAddress                       (0x350000UL)
#define cBertTdmMonBaseAddress                       (0x360000UL)
#define cBertPsnMonBaseAddress                       (0x370000UL)
#define cThaRegMapBERTGenSelectedChannel             (0x8500 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenNxDs0ConcateControl         (0x8360 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenMode                        (0x8300 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenFixedPatternControl         (0x8310 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenErrorRateInsert             (0x8320 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenSingleBitErrInsert          (0x837E + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenGoodBitCounter              (0x8380 + cBertTdmGenBaseAddress)

#define cThaRegMapBERTMonSelectedChannel             (0x8510 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonNxDs0ConcateControl         (0x84C0 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonGlobalControl               (0x8400 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonStickyEnable                (0x8401 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonSticky                      (0x8402 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonMode                        (0x8410 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonFixedPatternControl         (0x8420 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonErrorCounter                (0x8460 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonGoodBitCounter              (0x8480 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonLossBitCounter              (0x84A0 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonCounterLoadId               (0x8403 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonErrorCounterLoading         (0x8404 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonGoodBitCounterLoading       (0x8405 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonLossBitCounterLoading       (0x8406 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonStatus                      (0x8440 + cBertPsnMonBaseAddress)

#define cThaRegDemapBERTMonSelectedChannel          (0x8510 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonNxDs0ConcateControl      (0x84C0 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonGlobalControl            (0x8400 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonStickyEnable             (0x8401 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonSticky                   (0x8402 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonMode                     (0x8410 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonFixedPatternControl      (0x8420 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonErrorCounter             (0x8460 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonGoodBitCounter           (0x8480 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonLossBitCounter           (0x84A0 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonCounterLoadId            (0x8403 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonErrorCounterLoading      (0x8404 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonGoodBitCounterLoading    (0x8405 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonLossBitCounterLoading    (0x8406 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonStatus                   (0x8440 + cBertTdmMonBaseAddress)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegMapBERTGenTxBerMdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdMask;
    }

static uint8  RegMapBERTGenTxBerMdShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdShift;
    }

static uint32 RegMapBERTGenTxBerErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrMask;
    }

static uint8  RegMapBERTGenTxBerErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrShift;
    }

static uint32 RegMapBERTGenSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSelectedChannel + engineId;
    }

static uint32 RegMapBERTGenNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenNxDs0ConcateControl + engineId;
    }

static uint32 RegMapBERTGenMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenMode + engineId;
    }

static uint32 RegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenFixedPatternControl + engineId;
    }

static uint32 RegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenErrorRateInsert + engineId;
    }

static uint32 RegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSingleBitErrInsert + engineId;
    }

static uint32 RegMapBERTGenGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenGoodBitCounter + engineId;
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonSelectedChannel + engineId;
    }

static uint32 RegMapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonNxDs0ConcateControl + engineId;
    }

static uint32 RegMapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGlobalControl + engineId;
    }

static uint32 RegMapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonStickyEnable + engineId;
    }

static uint32 RegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonSticky + engineId;
    }

static uint32 RegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonMode + engineId;
    }

static uint32 RegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonFixedPatternControl + engineId;
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonErrorCounter + engineId;
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGoodBitCounter + engineId;
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonLossBitCounter + engineId;
    }

static uint32 RegMapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonCounterLoadId + engineId;
    }

static uint32 RegMapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonErrorCounterLoading + engineId;
    }

static uint32 RegMapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGoodBitCounterLoading + engineId;
    }

static uint32 RegMapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonLossBitCounterLoading + engineId;
    }

static uint32 RegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonSelectedChannel + engineId;
    }

static uint32 RegDemapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonNxDs0ConcateControl + engineId;
    }

static uint32 RegDemapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGlobalControl + engineId;
    }

static uint32 RegDemapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonStickyEnable + engineId;
    }

static uint32 RegDemapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonSticky + engineId;
    }

static uint32 RegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonMode + engineId;
    }

static uint32 RegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonFixedPatternControl + engineId;
    }

static uint32 RegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonErrorCounter + engineId;
    }

static uint32 RegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGoodBitCounter + engineId;
    }

static uint32 RegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonLossBitCounter + engineId;
    }

static uint32 RegDemapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonCounterLoadId + engineId;
    }

static uint32 RegDemapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonErrorCounterLoading + engineId;
    }

static uint32 RegDemapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGoodBitCounterLoading + engineId;
    }

static uint32 RegDemapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonLossBitCounterLoading + engineId;
    }

static uint32 RegMapBERTGenTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit14;
    }

static uint32 RegMapBERTGenTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit13_0;
    }

static uint32 RegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit14;
    }

static uint32 RegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit13_0;
    }

static eBool BitFieldsNeedShiftUp(ThaModulePrbs prbsModule)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)prbsModule);
    ThaModuleMap moduleMap = (ThaModuleMap)AtDeviceModuleGet(device, cThaModuleMap);

    return Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)moduleMap);
    }

static uint32 RegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    if (BitFieldsNeedShiftUp(prbsModule))
        return cBit15;

    return cBit14;
    }

static uint32 MapMonPwTdmIdMask(ThaModulePrbs prbsModule)
    {
    if (BitFieldsNeedShiftUp(prbsModule))
        return cBit10_0;

    return cBit9_0;
    }

static uint32 MapMonPwTdmIdShift(ThaModulePrbs prbsModule)
    {
    AtUnused(prbsModule);
    return 0;
    }

static uint32 MapMonOc24SliceMask(ThaModulePrbs prbsModule)
    {
    if (BitFieldsNeedShiftUp(prbsModule))
        return cBit11;

    return cBit10;
    }

static uint32 MapMonOc24SliceShift(ThaModulePrbs prbsModule)
    {
    if (BitFieldsNeedShiftUp(prbsModule))
        return 11;

    return 10;
    }

static uint32 MapMonOc48SliceMask(ThaModulePrbs prbsModule)
    {
    if (BitFieldsNeedShiftUp(prbsModule))
        return cBit14_12;

    return cBit13_11;
    }

static uint32 MapMonOc48SliceShift(ThaModulePrbs prbsModule)
    {
    if (BitFieldsNeedShiftUp(prbsModule))
        return 12;

    return 11;
    }

static uint32 RegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    return MapMonPwTdmIdMask(prbsModule) | MapMonOc24SliceMask(prbsModule) | MapMonOc48SliceMask(prbsModule);
    }

static uint32 RegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonStatus + engineId;
    }

static uint32 RegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonStatus + engineId;
    }

static uint32 TdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 2048;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        mMethodOverride(m_ThaPrbsRegProviderOverride, TdmChannelIdSliceFactor);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenErrorRateInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSingleBitErrInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGlobalControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStickyEnable);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSticky);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonCounterLoadId);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGlobalControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStickyEnable);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSticky);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonCounterLoadId);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerMdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerMdShift);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerErrMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerErrShift);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PrbsRegProvider);
    }

ThaPrbsRegProvider Tha60210011PrbsRegProviderObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsRegProviderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60210011PrbsRegProvider(void)
    {
    static tTha60210011PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = Tha60210011PrbsRegProviderObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

uint32 Tha60210011PrbsRegProviderPsnMonIdByPw(ThaModulePrbs prbsModule, uint32 oc48Slice, uint32 oc24Slice, uint32 pwTdmId)
    {
    uint32 monId = 0;
    uint32 pwTdmIdMask    = MapMonPwTdmIdMask(prbsModule);
    uint32 pwTdmIdShift   = MapMonPwTdmIdShift(prbsModule);
    uint32 oc24SliceMask  = MapMonOc24SliceMask(prbsModule);
    uint32 oc24SliceShift = MapMonOc24SliceShift(prbsModule);
    uint32 oc48SliceMask  = MapMonOc48SliceMask(prbsModule);
    uint32 oc48SliceShift = MapMonOc48SliceShift(prbsModule);

    mRegFieldSet(monId, pwTdmId, pwTdmId);
    mRegFieldSet(monId, oc24Slice, oc24Slice);
    mRegFieldSet(monId, oc48Slice, oc48Slice);

    return monId;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210011ModulePrbsInternal.h
 * 
 * Created Date: Oct 20, 2015
 *
 * Description : Tha60210011 PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPRBSINTERNAL_H_
#define _THA60210011MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/util/ThaBitMask.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePrbs
    {
    tThaModulePrbs super;

    /* Private data */
    ThaBitMask loResource;
    ThaBitMask hoResource;
    }tTha60210011ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha60210011ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);
ThaBitMask Tha60210011ModulePrbsHoResourceMask(ThaModulePrbs self);
uint32 Tha6021ErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate);
eAtBerRate Tha6021ErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate);
uint32 Tha60210011AuVcPrbsEngineDefaultOffset(uint32 engineId);
uint32 Tha60210011AuVcPrbsEngineBertGenChannelIdMask(void);
eBool Tha6021ModulePrbsDe3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPRBSINTERNAL_H_ */


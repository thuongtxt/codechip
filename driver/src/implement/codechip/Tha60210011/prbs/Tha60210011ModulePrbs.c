/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210011ModulePrbs.c
 *
 * Created Date: July 1, 2015
 *
 * Description : PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/prbs/ThaPrbsEngineInternal.h"
#include "../../../default/prbs/ThaPrbsRegProvider.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pda/ThaModulePda.h"
#include "../map/Tha60210011ModuleMapInternal.h"
#include "../pw/Tha60210011ModulePw.h"
#include "Tha60210011ModulePrbsInternal.h"
#include "Tha60210011ModulePrbsReg.h"
#include "Tha60210011PrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011ModulePrbs *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtModuleMethods      m_AtModuleOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtModulePrbsMethods  *m_AtModulePrbsMethods = NULL;
static const tThaModulePrbsMethods *m_ThaModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    return Tha60210021NxDs0PrbsEngineNew(nxDs0, engineId);
    }

static ThaVersionReader VersionReader(ThaModulePrbs self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportHoPrbs(ThaModulePrbs self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x11, 0x12, 0x33);
    }

static eBool De3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode)
    {
    return Tha6021ModulePrbsDe3InvertModeShouldSwap(self, prbsMode);
    }

static uint32 StartBuiltNumberSupportHoPrbs(ThaModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x10, 0x5, 0);
    }

static eBool HoVcPrbsIsSupported(ThaModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self) ;

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) > StartVersionSupportHoPrbs(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartVersionSupportHoPrbs(self))
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportHoPrbs(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static AtPrbsEngine AuVcPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    if (ThaModulePrbsHoVcPrbsIsSupported(self) == cAtFalse)
        return NULL;

    return Tha60210011AuVcPrbsEngineNew(sdhVc, engineId);
    }

static uint32 ForceEnableValue(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    return ThaPrbsEngineErrorRateSw2Hw(engine, cAtBerRate1E3);
    }

static uint32 PwTdmOffset(ThaModulePrbs self, AtPw pw)
    {
    uint8 slice = 0;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        uint32 oc48Id = slice / 2;
        uint32 oc24SliceId = slice % 2;
        return Tha60210011PrbsRegProviderPsnMonIdByPw(self, oc48Id, oc24SliceId, AtChannelHwIdGet((AtChannel)pw));
        }

    return AtChannelHwIdGet((AtChannel)pw) | ((uint32)slice << 10);
    }

static uint32 PsnChannelId(ThaModulePrbs self, AtPrbsEngine engine)
    {
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    AtPw pw = AtChannelBoundPwGet(channel);

    return PwTdmOffset(self, pw);
    }

static eAtRet RegisterInit(ThaModulePrbs self)
    {
    uint8 engineId;
    uint32 mask = Tha60210011AuVcPrbsEngineBertGenChannelIdMask();

    for (engineId = 0; engineId < mMethodsGet(self)->MaxNumHoPrbsEngine(self); engineId++)
        {
        uint32 regOffset = Tha60210011AuVcPrbsEngineDefaultOffset(engineId);

        mModuleHwWrite(self, cAf6Reg_sel_ho_bert_gen_Base + regOffset, mask);
        mModuleHwWrite(self, cAf6Reg_ctrl_pen_tdm_mon_Base + regOffset, 0xA2000);
        mModuleHwWrite(self, cAf6Reg_ctrl_pen_pw_mon_Base + regOffset, 0xA2000);
        }

    return cAtOk;
    }

static eAtRet HoPrbsEngineRegisterInit(ThaModulePrbs self)
    {
    if (ThaModulePrbsHoVcPrbsIsSupported(self))
        return RegisterInit(self);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

static uint32 MaxNumLoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 MaxNumHoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 MaxNumEngines(AtModulePrbs self)
    {
    ThaModulePrbs prbsModule = (ThaModulePrbs)self;
    if (ThaModulePrbsHoVcPrbsIsSupported(prbsModule))
        return mMethodsGet(prbsModule)->MaxNumLoPrbsEngine(prbsModule) + mMethodsGet(prbsModule)->MaxNumHoPrbsEngine(prbsModule);
    return mMethodsGet(prbsModule)->MaxNumLoPrbsEngine(prbsModule);
    }

static ThaBitMask LoResourceMask(ThaModulePrbs self)
    {
    if (mThis(self)->loResource == NULL)
        mThis(self)->loResource = ThaBitMaskNew(mMethodsGet(self)->MaxNumLoPrbsEngine(self));
    return mThis(self)->loResource;
    }

static ThaBitMask HoResourceMask(ThaModulePrbs self)
    {
    if (mThis(self)->hoResource == NULL)
        mThis(self)->hoResource = ThaBitMaskNew(mMethodsGet(self)->MaxNumHoPrbsEngine(self));

    return mThis(self)->hoResource;
    }

static uint32 NumLoEngines(ThaModulePrbs self)
    {
    uint32 i;
    uint32 numSetBit = 0;

    for (i = 0; i < mMethodsGet(self)->MaxNumLoPrbsEngine(self); i++)
        {
        if (ThaBitMaskBitVal(LoResourceMask(self), i) == 1)
            numSetBit++;
        }

    return numSetBit;
    }

static uint32 NumHoEngines(ThaModulePrbs self)
    {
    uint32 i;
    uint32 numSetBit = 0;

    for (i = 0; i < mMethodsGet(self)->MaxNumHoPrbsEngine(self); i++)
        {
        if (ThaBitMaskBitVal(HoResourceMask(self), i) == 1)
            numSetBit++;
        }

    return numSetBit;
    }

static eAtRet ResourceCheck(ThaModulePrbs self, AtChannel channel)
    {
    AtUnused(channel);
    return (NumLoEngines(self) == mMethodsGet(self)->MaxNumLoPrbsEngine(self)) ? cAtErrorRsrcNoAvail : cAtOk;
    }

static eAtRet HoVcResourceCheck(ThaModulePrbs self, AtChannel channel)
    {
    AtUnused(channel);
    return (NumHoEngines(self) == mMethodsGet(self)->MaxNumHoPrbsEngine(self)) ? cAtErrorRsrcNoAvail : cAtOk;
    }

static AtPrbsEngine CachePrbsEngine(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine)
    {
    if (m_ThaModulePrbsMethods->CachePrbsEngine(self, engineId, engine) == NULL)
        return NULL;

    if (mMethodsGet(self)->DynamicAllocation(self))
        {
        uint32 freeHwId = ThaBitMaskFirstZeroBit(LoResourceMask(self));
        ThaPrbsEngineHwEngineIdSet((ThaPrbsEngine)engine, freeHwId);
        ThaBitMaskSetBit(LoResourceMask(self), freeHwId);
        }
    else
        ThaPrbsEngineHwEngineIdSet((ThaPrbsEngine)engine, engineId);

    return engine;
    }

static AtPrbsEngine HoVcCachePrbsEngine(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine)
    {
    if (m_ThaModulePrbsMethods->CachePrbsEngine(self, engineId, engine) == NULL)
        return NULL;

    if (mMethodsGet(self)->DynamicAllocation(self))
        {
        uint32 freeHwId = ThaBitMaskFirstZeroBit(HoResourceMask(self));
        ThaPrbsEngineHwEngineIdSet((ThaPrbsEngine)engine, freeHwId);
        ThaBitMaskSetBit(HoResourceMask(self), freeHwId);
        }
    else
        ThaPrbsEngineHwEngineIdSet((ThaPrbsEngine)engine, engineId);

    return engine;
    }

static void DeleteResource(ThaBitMask *resource)
    {
    if (*resource == NULL)
        return;

    AtObjectDelete((AtObject)*resource);
    *resource = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteResource(&mThis(self)->loResource);
    DeleteResource(&mThis(self)->hoResource);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static uint32 HwEngineId(ThaModulePrbs self, uint32 engineId, AtChannel channel)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)AtModulePrbsEngineGet((AtModulePrbs)self, engineId);
    AtUnused(channel);
    return ThaPrbsEngineHwEngineIdGet(engine);
    }

static void ResourceRemove(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    /* Only need to care LO BERT in this context */
    uint32 hwEngineId = ThaPrbsEngineHwEngineIdGet(engine);
    ThaBitMaskClearBit(LoResourceMask(self), hwEngineId);
    }

static uint32 ErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate)
    {
    AtUnused(self);
    if (errorRate == cAtBerRate1E2)
        return 100;
    if (errorRate == cAtBerRate1E3)
        return 1000;
    if (errorRate == cAtBerRate1E4)
        return 10000;
    if (errorRate == cAtBerRate1E5)
        return 100000;
    if (errorRate == cAtBerRate1E6)
        return 1000000;
    if (errorRate == cAtBerRate1E7)
        return 10000000;
    if (errorRate == cAtBerRate1E8)
        return 100000000;
    if (errorRate == cAtBerRate1E9)
        return 1000000000;

    return 0;
    }

static eAtBerRate ErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate)
    {
    AtUnused(self);
    if (errorRate == 100)
        return cAtBerRate1E2;
    if (errorRate == 1000)
        return cAtBerRate1E3;
    if (errorRate == 10000)
        return cAtBerRate1E4;
    if (errorRate == 100000)
        return cAtBerRate1E5;
    if (errorRate == 1000000)
        return cAtBerRate1E6;
    if (errorRate == 10000000)
        return cAtBerRate1E7;
    if (errorRate == 100000000)
        return cAtBerRate1E8;
    if (errorRate == 1000000000)
        return cAtBerRate1E9;

    return cAtBerRateUnknown;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011ModulePrbs *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(loResource);
    mEncodeObject(hoResource);
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtUnused(self);
    return Tha60210011PrbsRegProvider();
    }

static void EngineRegisterInit(ThaModulePrbs self, uint8 partId, uint32 engineId)
    {
    /* Let HO engines be handled by this sub class */
    if (engineId >= mMethodsGet(self)->MaxNumLoPrbsEngine(self))
        return;

    m_ThaModulePrbsMethods->EngineRegisterInit(self, partId, engineId);
    }

static uint32 Restore(AtModule self)
    {
    uint32 engineId;
    uint32 maxNumEngines = AtModulePrbsMaxNumEngines((AtModulePrbs)self);

    /* Initialize all masks to have fresh state */
    ThaBitMaskInit(mThis(self)->loResource);
    ThaBitMaskInit(mThis(self)->hoResource);

    for (engineId = 0; engineId < maxNumEngines; engineId++)
        {
        ThaPrbsEngine engine = (ThaPrbsEngine)AtModulePrbsEngineGet((AtModulePrbs)self, engineId);
        ThaPrbsEngineRestore(engine);
        }

    return 0;
    }

static void HwEngineIdInUseSet(ThaModulePrbs self, ThaPrbsEngine engine, uint32 hwEngineId)
    {
    AtUnused(self);
    ThaPrbsEngineHwEngineIdSet(engine, hwEngineId);

    /* Just care LO resource here. HO resource should be done by itself class */
    ThaBitMaskSetBit(LoResourceMask(self), hwEngineId);
    }

static void OverrideAtObject(AtModulePrbs self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Restore);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, MaxNumEngines);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, m_ThaModulePrbsMethods, sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, PsnChannelId);

        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, AuVcPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, ForceEnableValue);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, ResourceCheck);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcResourceCheck);
        mMethodOverride(m_ThaModulePrbsOverride, CachePrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcCachePrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, HwEngineId);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumHoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumLoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, ResourceRemove);
        mMethodOverride(m_ThaModulePrbsOverride, ErrorRateSw2Hw);
        mMethodOverride(m_ThaModulePrbsOverride, ErrorRateHw2Sw);
        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, De3InvertModeShouldSwap);
        mMethodOverride(m_ThaModulePrbsOverride, EngineRegisterInit);
        mMethodOverride(m_ThaModulePrbsOverride, HwEngineIdInUseSet);
        mMethodOverride(m_ThaModulePrbsOverride, HoPrbsEngineRegisterInit);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs((ThaModulePrbs)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModulePrbs);
    }

AtModulePrbs Tha60210011ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60210011ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModulePrbsObjectInit(newModule, device);
    }

ThaBitMask Tha60210011ModulePrbsHoResourceMask(ThaModulePrbs self)
    {
    if (self)
        return HoResourceMask(self);
    return NULL;
    }

uint32 Tha6021ErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate)
    {
    return ErrorRateSw2Hw(self, errorRate);
    }

eAtBerRate Tha6021ErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate)
    {
    return ErrorRateHw2Sw(self, errorRate);
    }

eBool Tha6021ModulePrbsDe3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs15: return cAtTrue;
        case cAtPrbsModePrbs23: return cAtTrue;
        default: return cAtFalse;
        }
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210011PrbsRegProvider.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PRBSREGPROVIDER_H_
#define _THA60210011PRBSREGPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PrbsRegProvider
    {
    tThaPrbsRegProvider super;
    }tTha60210011PrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPrbsRegProvider Tha60210011PrbsRegProviderObjectInit(ThaPrbsRegProvider self);
uint32 Tha60210011PrbsRegProviderPsnMonIdByPw(ThaModulePrbs prbsModule, uint32 oc48Slice, uint32 oc24Slice, uint32 pwTdmId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PRBSREGPROVIDER_H_ */


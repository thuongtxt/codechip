/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210011AuVcPrbsEngineInternal.h
 * 
 * Created Date: Nov 8, 2016
 *
 * Description : HoVC PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011AUVCPRBSENGINEINTERNAL_H_
#define _THA60210011AUVCPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineAuVcInternal.h"
#include "Tha60210011AuVcPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011AuVcPrbsEngine* Tha60210011AuVcPrbsEngine;

typedef struct tTha60210011AuVcPrbsEngineMethods
    {
    uint32 (*TdmGenControlRegister)(Tha60210011AuVcPrbsEngine self);
    uint32 (*TdmMonControlRegister)(Tha60210011AuVcPrbsEngine self);
    uint32 (*PsnMonControlRegister)(Tha60210011AuVcPrbsEngine self);

    uint32 (*TxTdmGoodBitCounterReg)(Tha60210011AuVcPrbsEngine self);

    uint32 (*RxTdmGoodBitCounterReg)(Tha60210011AuVcPrbsEngine self);
    uint32 (*RxTdmErrorBitCounterReg)(Tha60210011AuVcPrbsEngine self);
    uint32 (*RxTdmLossBitCounterReg)(Tha60210011AuVcPrbsEngine self);

    uint32 (*RxPsnGoodBitCounterReg)(Tha60210011AuVcPrbsEngine self);
    uint32 (*RxPsnErrorBitCounterReg)(Tha60210011AuVcPrbsEngine self);
    uint32 (*RxPsnLossBitCounterReg)(Tha60210011AuVcPrbsEngine self);

    uint32 (*RxTdmLossStickyReg)(Tha60210011AuVcPrbsEngine self);
    uint32 (*RxPsnLossStickyReg)(Tha60210011AuVcPrbsEngine self);
    uint32 (*LossAlarmStickyMask)(Tha60210011AuVcPrbsEngine self);
    }tTha60210011AuVcPrbsEngineMethods;

typedef struct tTha60210011AuVcPrbsEngine
    {
    tThaPrbsEngineAuVc super;
    const tTha60210011AuVcPrbsEngineMethods *methods;

    /* Private data */
    uint32 txFixedPattern;
    uint32 rxTdmFixedPattern;
    uint32 rxPsnFixedPattern;
    }tTha60210011AuVcPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60210011AuVcPrbsEngineObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011AUVCPRBSENGINEINTERNAL_H_ */


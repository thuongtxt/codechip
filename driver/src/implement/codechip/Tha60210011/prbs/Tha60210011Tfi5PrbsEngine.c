/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PRBS
 *
 * File        : Tha60210011Tfi5PrbsEngine.c
 *
 * Created Date: Jun 24, 2015
 *
 * Description : PW CodeChip 60210011: TFI-5 PRBS engine.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../physical/Tha60210011Tfi5SerdesControllerReg.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210011Tfi5PrbsEngine *)self)

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60210011Tfi5PrbsEngine
    {
    tAtPrbsEngine super;

    /* Private data */
    AtSerdesController serdes;
    }tTha60210011Tfi5PrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tAtObjectMethods     m_AtObjectOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    uint32 ret = AtPrbsEngineErrorForce(self, cAtFalse);
    if (ret != cAtOk)
        return ret;

    return AtPrbsEngineEnable(self, cAtFalse);
    }

static uint32 Tfi5LineId(AtPrbsEngine self)
    {
    return AtSerdesControllerHwIdGet(mThis(self)->serdes);
    }

static AtDevice Device(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    if (channel)
        return AtChannelDeviceGet(channel);
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(mThis(self)->serdes));
    }

static AtModule Module(AtPrbsEngine self, eAtModule module)
    {
    return AtDeviceModuleGet(Device(self), module);
    }

static uint32 Read(AtPrbsEngine self, uint32 address, eAtModule module)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    if (channel)
        return m_AtPrbsEngineMethods->Read(self, address, module);

    return mModuleHwRead(Module(self, module), address);
    }

static void Write(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    if (channel)
        {
        m_AtPrbsEngineMethods->Write(self, address, value, module);
        return;
        }

    mModuleHwWrite(Module(self, module), address, value);
    }

static uint32 BaseAddress(AtPrbsEngine self)
    {
    AtDevice device = Device(self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return Tha60210011ModuleSdhSerdesPrbsBaseAddress(sdhModule, mThis(self)->serdes);
    }

static uint32 PrbsCtrlRegister(AtPrbsEngine self)
    {
    return cAf6RegTfi5SerdesCtrl + BaseAddress(self);
    }

static uint32 PrbsCurrentStatusRegister(AtPrbsEngine self)
    {
    return cAf6RegTfi5SerdesPrbsCurrentStatus + BaseAddress(self);
    }

static uint32 PrbsStickyStatusRegister(AtPrbsEngine self)
    {
    return cAf6RegTfi5SerdesPrbsStickyStatus + BaseAddress(self);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 address = PrbsCtrlRegister(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleSdh);
    uint32 lineId = Tfi5LineId(self);

    mFieldIns(&regVal,
              cAf6Tfi5SerdesPrbsErrInstMask(lineId),
              cAf6Tfi5SerdesPrbsErrInstShift(lineId),
              mBoolToBin(force));
    AtPrbsEngineWrite(self, address, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 address = PrbsCtrlRegister(self);
    uint32 regVal  = AtPrbsEngineRead(self, address, cAtModuleSdh);
    return (regVal & cAf6Tfi5SerdesPrbsErrInstMask(Tfi5LineId(self))) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 address = PrbsCtrlRegister(self);
    uint32 regVal  = AtPrbsEngineRead(self, address, cAtModuleSdh);
    uint32 lineId = Tfi5LineId(self);
    mFieldIns(&regVal,
              cAf6Tfi5SerdesPrbsEnableMask(lineId),
              cAf6Tfi5SerdesPrbsEnableShift(lineId),
              mBoolToBin(enable));
    AtPrbsEngineWrite(self, address, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 address = PrbsCtrlRegister(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleSdh);
    return (regVal & cAf6Tfi5SerdesPrbsEnableMask(Tfi5LineId(self))) ? cAtTrue : cAtFalse;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 channelId = Tfi5LineId(self);
    uint32 regValue  = AtPrbsEngineRead(self, PrbsCurrentStatusRegister(self), cAtModuleSdh);
    return (regValue & cAf6RegTfi5SerdesPrbsStatusMask(channelId)) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 channelId = Tfi5LineId(self);
    uint32 regAddr = PrbsStickyStatusRegister(self);
    uint32 regValue  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    if (read2Clear)
        AtPrbsEngineWrite(self, regAddr, cAf6RegTfi5SerdesPrbsStatusMask(channelId), cAtModuleSdh);

    return (regValue & cAf6RegTfi5SerdesPrbsStatusMask(channelId)) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs15;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtTrue : cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011Tfi5PrbsEngine* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(serdes);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Read);
        mMethodOverride(m_AtPrbsEngineOverride, Write);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5PrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    uint32 engineId = AtSerdesControllerIdGet(serdesController);
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, AtSerdesControllerPhysicalPortGet(serdesController), engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->serdes = serdesController;

    return self;
    }

AtPrbsEngine Tha60210011PrbsEngineSerdesTfi5New(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

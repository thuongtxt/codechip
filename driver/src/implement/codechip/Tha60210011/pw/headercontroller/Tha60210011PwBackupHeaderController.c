/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210011PwBackupPsnController.c
 *
 * Created Date: May 9, 2015
 *
 * Description : Backup Psn Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "Tha60210011PwHeaderControllerInternal.h"
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../pwe/Tha60210011ModulePwe.h"
#include "../../pw/Tha60210011ModulePw.h"
#include "../../cla/hbce/Tha60210011Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tAtObjectMethods              *m_AtObjectMethods              = NULL;
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwGroup HsPwGroup(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    AtPw pw = ThaPwAdapterPwGet(adapter);
    return AtPwHsGroupGet(pw);
    }

static eBool NeedProtectOnTxDirection(ThaPwHeaderController self)
    {
    AtPwGroup group = HsPwGroup(self);
    eAtPwGroupLabelSet labelSet;

    /* If PW does not join to HS group, configure backup PSN does not need to be protected */
    if (group == NULL)
        return cAtFalse;

    labelSet = AtPwGroupTxSelectedLabelGet(group);
    return (labelSet == cAtPwGroupLabelSetPrimary) ? cAtFalse : cAtTrue;
    }

static eBool NeedProtectOnRxDirection(ThaPwHeaderController self)
    {
    AtPwGroup group = HsPwGroup(self);
    eAtPwGroupLabelSet labelSet;

    /* If PW does not join to HS group, configure backup PSN does not need to be protected */
    if (group == NULL)
        return cAtFalse;

    labelSet = AtPwGroupRxSelectedLabelGet(group);
    return (labelSet == cAtPwGroupLabelSetPrimary) ? cAtFalse : cAtTrue;
    }

static void Debug(ThaPwHeaderController self)
    {
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(self->hbceMemoryCellContent);

    AtPrintc(cSevNormal, "* Backup HBCE Activated: %s\r\n", self->hbceActivated ? "YES" : "NO");
    AtPrintc(cSevNormal, "* Backup HBCE memory cell %u: %s\r\n",
             ThaHbceMemoryCellIndexGet(cell),
             AtObjectToString((AtObject)cell));
    }

static ThaModulePwe ModulePwe(ThaPwHeaderController self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(mDevice(ThaPwHeaderControllerAdapterGet(self)), cThaModulePwe);
    }

static eAtModulePwRet PsnTypeSet(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtModulePwRet HeaderLengthSet(ThaPwHeaderController self, uint8 hdrLenInByte)
    {
    AtUnused(self);
    AtUnused(hdrLenInByte);

    /* HW provides only one field for both primary and backup header length.
     * Backup header length needs to be configured as SW data-base
     * to not depend on HW field when primary header length changed first */
    ((tTha60210011PwBackupHeaderController *)self)->headerLength = hdrLenInByte;
    return cAtOk;
    }

static uint8 HeaderLengthGet(ThaPwHeaderController self)
    {
    return ((tTha60210011PwBackupHeaderController *)self)->headerLength;
    }

static eAtModulePwRet HeaderRead(ThaPwHeaderController self, uint8 *buffer, uint8 headerLengthInByte)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (ThaPwHeaderControllerIsSimulated(self))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, buffer, ThaPwHeaderControllerSimulationDatabase(self)->psnHeader, headerLengthInByte);
        return cAtOk;
        }

    return Tha60210011ModulePwePwBackupHeaderRead(ModulePwe(self), pw, buffer, headerLengthInByte);
    }

static eAtModulePwRet HeaderWrite(ThaPwHeaderController self, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    AtUnused(writeMode);

    if (ThaPwHeaderControllerIsSimulated(self))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, ThaPwHeaderControllerSimulationDatabase(self)->psnHeader, buffer, headerLenInByte);
        ThaPwHeaderControllerSimulationDatabase(self)->psnHeaderLength = headerLenInByte;
        return cAtOk;
        }

    return Tha60210011ModulePwePwBackupHeaderWrite(ModulePwe(self), pw, buffer, headerLenInByte, psn);
    }

static eBool IsPrimary(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet PsnSet(ThaPwHeaderController self, AtPwPsn psn)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    eAtPwPsnType primaryPsnType = AtPwPsnTypeGet(AtPwPsnGet(ThaPwAdapterPwGet(adapter)));
    eAtRet ret;

    /* TODO: need to consider this condition; Is it necessary?
     * Is it safe enough? Not yet */
    if (AtPwPsnTypeGet(psn) != primaryPsnType)
        return cAtErrorModeNotSupport;

    ret = m_ThaPwHeaderControllerMethods->PsnSet(self, psn);
    if (ret != cAtOk)
        return ret;

    /* Backup psn need to be enabled if PW is enabled */
    /* TODO: need to check why need to control en/dis here */
    return ThaHbcePwHeaderControllerEnable(ThaPwAdapterHbceGet((AtPw)adapter), self, AtChannelIsEnabled((AtChannel)adapter));
    }

static tAtEthVlanTag* StandbyEthCVlanGet(ThaPwHeaderController self, tAtEthVlanTag *cVlan)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwBackupEthCVlanGet(pw, cVlan);
    }

static tAtEthVlanTag* StandbyEthSVlanGet(ThaPwHeaderController self, tAtEthVlanTag *sVlan)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return AtPwBackupEthSVlanGet(pw, sVlan);
    }

static eAtRet NumVlansSet(ThaPwHeaderController self, uint8 numVlans)
    {
    AtUnused(self);
    AtUnused(numVlans);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011PwBackupHeaderController* object = (tTha60210011PwBackupHeaderController*)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(headerLength);
    }

static void OverrideAtObject(ThaPwHeaderController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnTxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnRxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, Debug);
        mMethodOverride(m_ThaPwHeaderControllerOverride, PsnTypeSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderRead);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderWrite);
        mMethodOverride(m_ThaPwHeaderControllerOverride, IsPrimary);
        mMethodOverride(m_ThaPwHeaderControllerOverride, PsnSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, StandbyEthCVlanGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, StandbyEthSVlanGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, NumVlansSet);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideAtObject(self);
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwBackupHeaderController);
    }

static ThaPwHeaderController ObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60210011PwBackupHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProvider, adapter);
    }


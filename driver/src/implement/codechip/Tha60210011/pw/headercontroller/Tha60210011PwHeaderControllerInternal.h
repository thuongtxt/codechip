/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011PwHeaderControllerInternal.h
 * 
 * Created Date: May 11, 2015
 *
 * Description : Header controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWHEADERCONTROLLERINTERNAL_H_
#define _THA60210011PWHEADERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwHeaderController * Tha60210011PwHeaderController;
typedef struct tTha60210011PwHeaderController
    {
    tThaPwHeaderController super;

    /* This product set these parameters into PSN buffer when RTP is enabled,
     * if RTP is disabled, nowhere to save, so they need to be cached in database */
    uint32 txRtpSsrc;
    uint8 txRtpPayloadType;
    eBool rtpPayloadTypeIsCompared;
    eBool rtpSsrcIsCompared;
    }tTha60210011PwHeaderController;

typedef struct tTha60210011PwBackupHeaderController
    {
    tTha60210011PwHeaderController super;

    /* Private data */
    uint8 headerLength;
    }tTha60210011PwBackupHeaderController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController Tha60210011PwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter);
ThaPwHeaderController Tha60210011PwHeaderControllerNew(ThaPwAdapter adapter);
ThaPwHeaderController Tha60210011PwBackupHeaderControllerNew(ThaPwAdapter adapter);

#endif /* _THA60210011PWHEADERCONTROLLERINTERNAL_H_ */


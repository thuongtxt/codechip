/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210011PwHeaderController.c
 *
 * Created Date: May 11, 2015
 *
 * Description : PW header controller for 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../../default/pw/adapters/ThaPwAdapterInternal.h"
#include "../../pwe/Tha60210011ModulePwe.h"
#include "../../cla/hbce/Tha60210011Hbce.h"
#include "Tha60210011PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)
#define mThis(self) ((Tha60210011PwHeaderController)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tAtObjectMethods              *m_AtObjectMethods              = NULL;
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwGroup HsPwGroup(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    AtPw pw = ThaPwAdapterPwGet(adapter);
    return AtPwHsGroupGet(pw);
    }

static eBool NeedProtectOnTxDirection(ThaPwHeaderController self)
    {
    AtPwGroup group = HsPwGroup(self);
    eAtPwGroupLabelSet labelSet;

    /* If PW does not join to HS group, configure primary PSN need to be protected */
    if (group == NULL)
        return cAtTrue;

    labelSet = AtPwGroupTxSelectedLabelGet(group);
    return (labelSet == cAtPwGroupLabelSetBackup) ? cAtFalse : cAtTrue;
    }

static eBool NeedProtectOnRxDirection(ThaPwHeaderController self)
    {
    AtPwGroup group = HsPwGroup(self);
    eAtPwGroupLabelSet labelSet;

    /* If PW does not join to HS group, configure primary PSN need to be protected */
    if (group == NULL)
        return cAtTrue;

    labelSet = AtPwGroupRxSelectedLabelGet(group);
    return (labelSet == cAtPwGroupLabelSetBackup) ? cAtFalse : cAtTrue;
    }

static void Debug(ThaPwHeaderController self)
    {
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(self->hbceMemoryCellContent);

    AtPrintc(cSevNormal, "* Primary HBCE Activated: %s\r\n", self->hbceActivated ? "YES" : "NO");
    AtPrintc(cSevNormal, "* Primary HBCE memory cell %u: %s\r\n",
             ThaHbceMemoryCellIndexGet(cell),
             AtObjectToString((AtObject)cell));
    }

static ThaModulePw ModulePw(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return (ThaModulePw)AtDeviceModuleGet(mDevice(adapter), cAtModulePw);
    }

static eBool IsLogicPw(ThaPwHeaderController self)
    {
    return ThaPwAdapterIsLogicPw(ThaPwHeaderControllerAdapterGet(self));
    }

static eAtModulePwRet RtpTxSsrcSet(ThaPwHeaderController self, uint32 ssrc)
    {
    uint8 hdrLenInByte;
    tThaPwHeaderCache *cache;
    uint8 *sharedBuffer;
    eAtRet ret;
    AtPw adapter;

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return cAtErrorNullPointer;

    /* This product set RTP parameters into PSN buffer when RTP is enabled,
     * if RTP is disabled, nowhere to save, so they need to be saved to database,
     * Moreover, can not save to cache because cache will be deleted when PW is activated */
    mThis(self)->txRtpSsrc = ssrc;

    if (IsLogicPw(self) || ThaPwHeaderControllerIsSimulated(self))
        {
        cache->txRtpSsrc = ssrc;
        return cAtOk;
        }

    adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    if (!AtPwRtpIsEnabled(adapter))
        return cAtOk;

    /* Reconfigure header to update new ssrc */
    hdrLenInByte = mMethodsGet(self)->HeaderLengthGet(self);
    sharedBuffer = ThaModulePwSharedHeaderBuffer(ModulePw(self), NULL);

    ret  = mMethodsGet(self)->HeaderRead(self, sharedBuffer, hdrLenInByte);
    ret |= mMethodsGet(self)->HeaderWrite(self, sharedBuffer, hdrLenInByte, AtPwPsnGet(adapter), cThaPwAdapterHeaderWriteModeDefault);

    return ret;
    }

static uint32 RtpTxSsrcGet(ThaPwHeaderController self)
    {
    return mThis(self)->txRtpSsrc;
    }

static eAtModulePwRet RtpTxPayloadTypeSet(ThaPwHeaderController self, uint8 payloadType)
    {
    uint8 hdrLenInByte;
    tThaPwHeaderCache *cache;
    uint8 *sharedBuffer;
    eAtRet ret;
    AtPw adapter;

    cache = ThaPwHeaderControllerCache(self);
    if (cache == NULL)
        return cAtErrorNullPointer;

    if (!ThaModulePwRtpPayloadTypeIsInRange(payloadType))
        return cAtErrorOutOfRangParm;

    /* This product set RTP parameters into PSN buffer when RTP is enabled,
     * if RTP is disabled, nowhere to save, so they need to be saved to database,
     * Moreover, can not save to cache because cache will be deleted when PW is activated */
    mThis(self)->txRtpPayloadType = payloadType;

    if (IsLogicPw(self) || ThaPwHeaderControllerIsSimulated(self))
        {
        cache->txRtpPayloadType = payloadType;
        return cAtOk;
        }

    adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    if (!AtPwRtpIsEnabled(adapter))
        return cAtOk;

    /* Reconfigure header to update new ssrc */
    hdrLenInByte = mMethodsGet(self)->HeaderLengthGet(self);
    sharedBuffer = ThaModulePwSharedHeaderBuffer(ModulePw(self), NULL);

    ret  = mMethodsGet(self)->HeaderRead(self, sharedBuffer, hdrLenInByte);
    ret |= mMethodsGet(self)->HeaderWrite(self, sharedBuffer, hdrLenInByte, AtPwPsnGet(adapter), cThaPwAdapterHeaderWriteModeDefault);

    return ret;
    }

static uint8 RtpTxPayloadTypeGet(ThaPwHeaderController self)
    {
    return mThis(self)->txRtpPayloadType;
    }

static eAtRet RxEnable(ThaPwHeaderController self, eBool enable)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    return ThaHbcePwHeaderControllerEnable(ThaPwAdapterHbceGet((AtPw)adapter), self, enable);
    }

static eBool RxIsEnable(ThaPwHeaderController self)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(self);
    ThaHbce hbce = ThaPwAdapterHbceGet((AtPw)adapter);

    /* Ask HBCE first */
    if (ThaHbcePwHeaderControllerIsEnabled(hbce, self))
        return cAtTrue;

    /* Get configuration of adapter to know Rx direction is enabled or not */
    return AtChannelIsEnabled((AtChannel)adapter);
    }

static eAtRet ApplyCellToHw(ThaPwHeaderController self)
    {
    ThaHbceMemoryCell cell;
    ThaHbce hbce;
    ThaHbceMemoryPool memPool;
    ThaHbceEntry entry;
    ThaHbceMemoryCellContent content;

    if (ThaPwHeaderControllerInAccessible(self))
        return cAtOk;

    content = ThaPwHeaderControllerHbceMemoryCellContentGet(self);
    if (content == NULL)
        return cAtOk;

    cell = ThaHbceMemoryCellContentCellGet(content);
    hbce = ThaPwAdapterHbceGet((AtPw)ThaPwHeaderControllerAdapterGet(self));
    memPool = ThaHbceMemoryPoolGet(hbce);
    entry = ThaHbceMemoryCellContentHashEntryGet(content);

    Tha60210011HbceMemoryPoolApplyCellToHardware(memPool, ThaHbceMemoryCellIndexGet(cell));
    Tha60210011HbceHashEntryTabCtrlApply(hbce, ThaHbceEntryIndexGet(entry));
    return cAtOk;
    }

static eAtModulePwRet RtpEnable(ThaPwHeaderController self, eBool enable)
    {
    uint8 hdrLenInByte;
    uint8 *sharedBuffer;
    eAtRet ret;
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eThaPwErrorCheckingMode checkMode;
    ThaClaPwController claController;
    eBool compared;

    if (IsLogicPw(self))
        {
        tThaPwConfigCache *cache = ThaPwAdapterCache((ThaPwAdapter)adapter);
        if (cache)
            cache->rtpEnabled = enable;
        return cAtOk;
        }

    if (ThaPwHeaderControllerShouldPreventReconfig(self) && mAdapter(adapter)->rtpEnableIsSet && (AtPwRtpIsEnabled(adapter) == enable))
        return cAtOk;

    /* Get PSN before RTP enabling is changed */
    hdrLenInByte = mMethodsGet(self)->HeaderLengthGet(self);
    sharedBuffer = ThaModulePwSharedHeaderBuffer(ModulePw(self), NULL);
    ret = mMethodsGet(self)->HeaderRead(self, sharedBuffer, hdrLenInByte);
    if (ret != cAtOk)
        return ret;

    ThaPwAdapterStartRemoving(adapter);

    ret = m_ThaPwHeaderControllerMethods->RtpEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    /* Tx SSRC and payload type are cached into database if they are configured while RTP is disabled,
     * it's time to apply them to HW if RTP is enabled or remove them if RTP is disabled */
    ret |= mMethodsGet(self)->HeaderWrite(self, sharedBuffer, hdrLenInByte, AtPwPsnGet(adapter), cThaPwAdapterHeaderWriteModeDefault);
    ret |= mMethodsGet(self)->HeaderLengthSet(self, hdrLenInByte);

    /* Need to disable PT & SSRC comparison in hardware when RTP is disabled.
     * In software view, it still keeps whatever application has configured */
    claController = ThaPwAdapterClaPwController((ThaPwAdapter)adapter);

    /* PayloadType checking */
    compared = (eBool)(enable ? mThis(self)->rtpPayloadTypeIsCompared : cAtFalse);
    checkMode = compared ? cThaPwErrorCheckingModeCheckCountAndDiscard : cThaPwErrorCheckingModeNoCheck;
    ThaClaPwControllerRtpPldTypeMismatchCheckModeSet(claController, adapter, checkMode);

    /* SSRC checking */
    compared = (eBool)(enable ? mThis(self)->rtpSsrcIsCompared : cAtFalse);
    checkMode = compared ? cThaPwErrorCheckingModeCheckCountAndDiscard : cThaPwErrorCheckingModeNoCheck;
    ThaClaPwControllerRtpSsrcMismatchCheckModeSet(claController, adapter, checkMode);

    ThaPwAdapterFinishRemoving(adapter);

    return ret;
    }

static eAtModulePwRet RtpPayloadTypeCompare(ThaPwHeaderController self, eBool enableCompare)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtRet ret = cAtOk;

    if (AtPwRtpIsEnabled(adapter))
        ret = m_ThaPwHeaderControllerMethods->RtpPayloadTypeCompare(self, enableCompare);

    mThis(self)->rtpPayloadTypeIsCompared = enableCompare;
    return ret;
    }

static eBool RtpPayloadTypeIsCompared(ThaPwHeaderController self)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    if (AtPwRtpIsEnabled(adapter))
        return m_ThaPwHeaderControllerMethods->RtpPayloadTypeIsCompared(self);

    return mThis(self)->rtpPayloadTypeIsCompared;
    }

static eAtModulePwRet RtpSsrcCompare(ThaPwHeaderController self, eBool enableCompare)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtRet ret = cAtOk;

    if (AtPwRtpIsEnabled(adapter))
        ret = m_ThaPwHeaderControllerMethods->RtpSsrcCompare(self, enableCompare);

    mThis(self)->rtpSsrcIsCompared = enableCompare;
    return ret;
    }

static eBool RtpSsrcIsCompared(ThaPwHeaderController self)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    if (AtPwRtpIsEnabled(adapter))
        return m_ThaPwHeaderControllerMethods->RtpSsrcIsCompared(self);

    return mThis(self)->rtpSsrcIsCompared;
    }

static eBool NeedDisableTxWhenChangePsn(ThaPwHeaderController self)
    {
    return mMethodsGet(self)->NeedProtectOnTxDirection(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011PwHeaderController object = (Tha60210011PwHeaderController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txRtpSsrc);
    mEncodeUInt(txRtpPayloadType);
    mEncodeUInt(rtpPayloadTypeIsCompared);
    mEncodeUInt(rtpSsrcIsCompared);
    }

static void OverrideAtObject(ThaPwHeaderController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnTxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnRxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, Debug);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpTxSsrcSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpTxSsrcGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpTxPayloadTypeSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpTxPayloadTypeGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RxEnable);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RxIsEnable);
        mMethodOverride(m_ThaPwHeaderControllerOverride, ApplyCellToHw);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpEnable);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpSsrcCompare);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpSsrcIsCompared);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpPayloadTypeCompare);
        mMethodOverride(m_ThaPwHeaderControllerOverride, RtpPayloadTypeIsCompared);
        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedDisableTxWhenChangePsn);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideAtObject(self);
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwHeaderController);
    }

ThaPwHeaderController Tha60210011PwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60210011PwHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PwHeaderControllerObjectInit(newProvider, adapter);
    }

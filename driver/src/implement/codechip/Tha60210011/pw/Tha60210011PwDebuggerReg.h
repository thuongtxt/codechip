/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Debugger
 * 
 * File        : Tha60210011PwDebuggerReg.h
 * 
 * Created Date: Dec 31, 2015
 *
 * Description : PW debugger register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_PW_THA60210011PWDEBUGGERREG_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_PW_THA60210011PWDEBUGGERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW Control
Reg Addr   : 0x00_0010
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure debug PW parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwControl_Base                                                                   0x000010
#define cAf6Reg_DebugPwControl                                                                        0x000010
#define cAf6Reg_DebugPwControl_WidthVal                                                                     32
#define cAf6Reg_DebugPwControl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: GlobalPwID
BitField Type: RW
BitField Desc: Lo TDM PW ID or HO Master STS ID corresponding to global PW ID
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_DebugPwControl_LocalPwID_Bit_Start                                                            20
#define cAf6_DebugPwControl_LocalPwID_Bit_End                                                              29
#define cAf6_DebugPwControl_LocalPwID_Mask                                                          cBit29_20
#define cAf6_DebugPwControl_LocalPwID_Shift                                                                20
#define cAf6_DebugPwControl_LocalPwID_MaxVal                                                            0x3ff
#define cAf6_DebugPwControl_LocalPwID_MinVal                                                              0x0
#define cAf6_DebugPwControl_LocalPwID_RstVal                                                              0x0

/*--------------------------------------
BitField Name: GlobalPwID
BitField Type: RW
BitField Desc: Global PW ID need to debug
BitField Bits: [19:7]
--------------------------------------*/
#define cAf6_DebugPwControl_GlobalPwID_Bit_Start                                                             7
#define cAf6_DebugPwControl_GlobalPwID_Bit_End                                                              19
#define cAf6_DebugPwControl_GlobalPwID_Mask                                                           cBit19_7
#define cAf6_DebugPwControl_GlobalPwID_Shift                                                                 7
#define cAf6_DebugPwControl_GlobalPwID_MaxVal                                                           0x1fff
#define cAf6_DebugPwControl_GlobalPwID_MinVal                                                              0x0
#define cAf6_DebugPwControl_GlobalPwID_RstVal                                                              0x0

/*--------------------------------------
BitField Name: OC48ID
BitField Type: RW
BitField Desc: OC48 ID
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_DebugPwControl_OC48ID_Bit_Start                                                                 4
#define cAf6_DebugPwControl_OC48ID_Bit_End                                                                   6
#define cAf6_DebugPwControl_OC48ID_Mask                                                                cBit6_4
#define cAf6_DebugPwControl_OC48ID_Shift                                                                     4
#define cAf6_DebugPwControl_OC48ID_MaxVal                                                                  0x7
#define cAf6_DebugPwControl_OC48ID_MinVal                                                                  0x0
#define cAf6_DebugPwControl_OC48ID_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: LoOC24Slice
BitField Type: RW
BitField Desc: Low Order OC24 slice if the PW belong to low order path
BitField Bits: [3]
--------------------------------------*/
#define cAf6_DebugPwControl_LoOC24Slice_Bit_Start                                                            3
#define cAf6_DebugPwControl_LoOC24Slice_Bit_End                                                              3
#define cAf6_DebugPwControl_LoOC24Slice_Mask                                                             cBit3
#define cAf6_DebugPwControl_LoOC24Slice_Shift                                                                3
#define cAf6_DebugPwControl_LoOC24Slice_MaxVal                                                             0x1
#define cAf6_DebugPwControl_LoOC24Slice_MinVal                                                             0x0
#define cAf6_DebugPwControl_LoOC24Slice_RstVal                                                             0x0

/*--------------------------------------
BitField Name: HoPwType
BitField Type: RW
BitField Desc: High Order PW type if the PW belong to high order path 0:
STS1/VC3 1: STS3/VC4 2: STS12/VC4-4C Others: STS48/VC4-16C
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_DebugPwControl_HoPwType_Bit_Start                                                               0
#define cAf6_DebugPwControl_HoPwType_Bit_End                                                                 2
#define cAf6_DebugPwControl_HoPwType_Mask                                                              cBit2_0
#define cAf6_DebugPwControl_HoPwType_Shift                                                                   0
#define cAf6_DebugPwControl_HoPwType_MaxVal                                                                0x7
#define cAf6_DebugPwControl_HoPwType_MinVal                                                                0x0
#define cAf6_DebugPwControl_HoPwType_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW Control
Reg Addr   : 0x00_0024
Reg Formula:
    Where  :
Reg Desc   :
This register is used to debug PW modules

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwSticky_Base                                                                    0x000024
#define cAf6Reg_DebugPwSticky                                                                         0x000024
#define cAf6Reg_DebugPwSticky_WidthVal                                                                      32
#define cAf6Reg_DebugPwSticky_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: ClaPrbsErr
BitField Type: WC
BitField Desc: CLA output PRBS error
BitField Bits: [28]
--------------------------------------*/
#define cAf6_DebugPwSticky_ClaPrbsErr_Bit_Start                                                             28
#define cAf6_DebugPwSticky_ClaPrbsErr_Bit_End                                                               28
#define cAf6_DebugPwSticky_ClaPrbsErr_Mask                                                              cBit28
#define cAf6_DebugPwSticky_ClaPrbsErr_Shift                                                                 28
#define cAf6_DebugPwSticky_ClaPrbsErr_MaxVal                                                               0x1
#define cAf6_DebugPwSticky_ClaPrbsErr_MinVal                                                               0x0
#define cAf6_DebugPwSticky_ClaPrbsErr_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PwePrbsErr
BitField Type: RC
BitField Desc: PWE input PRBS error
BitField Bits: [19]
--------------------------------------*/
#define cAf6_DebugPwSticky_PwePrbsErr_Bit_Start                                                             19
#define cAf6_DebugPwSticky_PwePrbsErr_Bit_End                                                               19
#define cAf6_DebugPwSticky_PwePrbsErr_Mask                                                              cBit19
#define cAf6_DebugPwSticky_PwePrbsErr_Shift                                                                 19
#define cAf6_DebugPwSticky_PwePrbsErr_MaxVal                                                               0x1
#define cAf6_DebugPwSticky_PwePrbsErr_MinVal                                                               0x0
#define cAf6_DebugPwSticky_PwePrbsErr_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PlaLoPrbsErr
BitField Type: RC
BitField Desc: Low Order PLA input PRBS error
BitField Bits: [18]
--------------------------------------*/
#define cAf6_DebugPwSticky_PlaLoPrbsErr_Bit_Start                                                           18
#define cAf6_DebugPwSticky_PlaLoPrbsErr_Bit_End                                                             18
#define cAf6_DebugPwSticky_PlaLoPrbsErr_Mask                                                            cBit18
#define cAf6_DebugPwSticky_PlaLoPrbsErr_Shift                                                               18
#define cAf6_DebugPwSticky_PlaLoPrbsErr_MaxVal                                                             0x1
#define cAf6_DebugPwSticky_PlaLoPrbsErr_MinVal                                                             0x0
#define cAf6_DebugPwSticky_PlaLoPrbsErr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PdaHoPrbsErr
BitField Type: RC
BitField Desc: High Order PDA output PRBS error
BitField Bits: [17]
--------------------------------------*/
#define cAf6_DebugPwSticky_PdaHoPrbsErr_Bit_Start                                                           17
#define cAf6_DebugPwSticky_PdaHoPrbsErr_Bit_End                                                             17
#define cAf6_DebugPwSticky_PdaHoPrbsErr_Mask                                                            cBit17
#define cAf6_DebugPwSticky_PdaHoPrbsErr_Shift                                                               17
#define cAf6_DebugPwSticky_PdaHoPrbsErr_MaxVal                                                             0x1
#define cAf6_DebugPwSticky_PdaHoPrbsErr_MinVal                                                             0x0
#define cAf6_DebugPwSticky_PdaHoPrbsErr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PlaHoPrbsErr
BitField Type: RC
BitField Desc: High Order PLA input PRBS error
BitField Bits: [16]
--------------------------------------*/
#define cAf6_DebugPwSticky_PlaHoPrbsErr_Bit_Start                                                           16
#define cAf6_DebugPwSticky_PlaHoPrbsErr_Bit_End                                                             16
#define cAf6_DebugPwSticky_PlaHoPrbsErr_Mask                                                            cBit16
#define cAf6_DebugPwSticky_PlaHoPrbsErr_Shift                                                               16
#define cAf6_DebugPwSticky_PlaHoPrbsErr_MaxVal                                                             0x1
#define cAf6_DebugPwSticky_PlaHoPrbsErr_MinVal                                                             0x0
#define cAf6_DebugPwSticky_PlaHoPrbsErr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: ClaPrbsSyn
BitField Type: WC
BitField Desc: CLA output PRBS sync
BitField Bits: [12]
--------------------------------------*/
#define cAf6_DebugPwSticky_ClaPrbsSyn_Bit_Start                                                             12
#define cAf6_DebugPwSticky_ClaPrbsSyn_Bit_End                                                               12
#define cAf6_DebugPwSticky_ClaPrbsSyn_Mask                                                              cBit12
#define cAf6_DebugPwSticky_ClaPrbsSyn_Shift                                                                 12
#define cAf6_DebugPwSticky_ClaPrbsSyn_MaxVal                                                               0x1
#define cAf6_DebugPwSticky_ClaPrbsSyn_MinVal                                                               0x0
#define cAf6_DebugPwSticky_ClaPrbsSyn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PwePrbsSyn
BitField Type: RC
BitField Desc: PWE input PRBS sync
BitField Bits: [3]
--------------------------------------*/
#define cAf6_DebugPwSticky_PwePrbsSyn_Bit_Start                                                              3
#define cAf6_DebugPwSticky_PwePrbsSyn_Bit_End                                                                3
#define cAf6_DebugPwSticky_PwePrbsSyn_Mask                                                               cBit3
#define cAf6_DebugPwSticky_PwePrbsSyn_Shift                                                                  3
#define cAf6_DebugPwSticky_PwePrbsSyn_MaxVal                                                               0x1
#define cAf6_DebugPwSticky_PwePrbsSyn_MinVal                                                               0x0
#define cAf6_DebugPwSticky_PwePrbsSyn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PlaLoPrbsSyn
BitField Type: RC
BitField Desc: Low Order PLA input PRBS sync
BitField Bits: [2]
--------------------------------------*/
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_Bit_Start                                                            2
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_Bit_End                                                              2
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_Mask                                                             cBit2
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_Shift                                                                2
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_MaxVal                                                             0x1
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_MinVal                                                             0x0
#define cAf6_DebugPwSticky_PlaLoPrbsSyn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PdaHoPrbsSyn
BitField Type: RC
BitField Desc: High Order PDA output PRBS sync
BitField Bits: [1]
--------------------------------------*/
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_Bit_Start                                                            1
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_Bit_End                                                              1
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_Mask                                                             cBit1
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_Shift                                                                1
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_MaxVal                                                             0x1
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_MinVal                                                             0x0
#define cAf6_DebugPwSticky_PdaHoPrbsSyn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PlaHoPrbsSyn
BitField Type: RC
BitField Desc: High Order PLA input PRBS sync
BitField Bits: [0]
--------------------------------------*/
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_Bit_Start                                                            0
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_Bit_End                                                              0
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_Mask                                                             cBit0
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_Shift                                                                0
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_MaxVal                                                             0x1
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_MinVal                                                             0x0
#define cAf6_DebugPwSticky_PlaHoPrbsSyn_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PLA HO Speed
Reg Addr   : 0x00_0030
Reg Formula:
    Where  :
Reg Desc   :
This register is used to show PLA HO PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPlaHoSpeed_Base                                                                0x000030
#define cAf6Reg_DebugPwPlaHoSpeed                                                                     0x000030
#define cAf6Reg_DebugPwPlaHoSpeed_WidthVal                                                                  32
#define cAf6Reg_DebugPwPlaHoSpeed_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaHoPwSpeed
BitField Type: RO
BitField Desc: High order PLA input PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_Bit_Start                                                        0
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_Bit_End                                                         31
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_Mask                                                      cBit31_0
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_Shift                                                            0
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_MaxVal                                                  0xffffffff
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_MinVal                                                         0x0
#define cAf6_DebugPwPlaHoSpeed_PlaHoPwSpeed_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PDA HO Speed
Reg Addr   : 0x00_0031
Reg Formula:
    Where  :
Reg Desc   :
This register is used to show PDA HO PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPdaHoSpeed_Base                                                                0x000031
#define cAf6Reg_DebugPwPdaHoSpeed                                                                     0x000031
#define cAf6Reg_DebugPwPdaHoSpeed_WidthVal                                                                  32
#define cAf6Reg_DebugPwPdaHoSpeed_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PdaHoPwSpeed
BitField Type: RO
BitField Desc: High order PDA output PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_Bit_Start                                                        0
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_Bit_End                                                         31
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_Mask                                                      cBit31_0
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_Shift                                                            0
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_MaxVal                                                  0xffffffff
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_MinVal                                                         0x0
#define cAf6_DebugPwPdaHoSpeed_PdaHoPwSpeed_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PLA LO Speed
Reg Addr   : 0x00_0032
Reg Formula:
    Where  :
Reg Desc   :
This register is used to show PLA LO PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPlaLoSpeed_Base                                                                0x000032
#define cAf6Reg_DebugPwPlaLoSpeed                                                                     0x000032
#define cAf6Reg_DebugPwPlaLoSpeed_WidthVal                                                                  32
#define cAf6Reg_DebugPwPlaLoSpeed_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaLoPwSpeed
BitField Type: RO
BitField Desc: Low order PLA input PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_Bit_Start                                                        0
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_Bit_End                                                         31
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_Mask                                                      cBit31_0
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_Shift                                                            0
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_MaxVal                                                  0xffffffff
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_MinVal                                                         0x0
#define cAf6_DebugPwPlaLoSpeed_PlaLoPwSpeed_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PDA LO Speed
Reg Addr   : 0x00_0033
Reg Formula:
    Where  :
Reg Desc   :
This register is used to show PDA LO PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPdaLoSpeed_Base                                                                0x000033
#define cAf6Reg_DebugPwPdaLoSpeed                                                                     0x000033
#define cAf6Reg_DebugPwPdaLoSpeed_WidthVal                                                                  32
#define cAf6Reg_DebugPwPdaLoSpeed_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PdaLoPwSpeed
BitField Type: RO
BitField Desc: Low order PDA output PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_Bit_Start                                                        0
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_Bit_End                                                         31
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_Mask                                                      cBit31_0
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_Shift                                                            0
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_MaxVal                                                  0xffffffff
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_MinVal                                                         0x0
#define cAf6_DebugPwPdaLoSpeed_PdaLoPwSpeed_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PWE Speed
Reg Addr   : 0x00_0034
Reg Formula:
    Where  :
Reg Desc   :
This register is used to show PWE PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPweSpeed_Base                                                                  0x000034
#define cAf6Reg_DebugPwPweSpeed                                                                       0x000034
#define cAf6Reg_DebugPwPweSpeed_WidthVal                                                                    32
#define cAf6Reg_DebugPwPweSpeed_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PwePwSpeed
BitField Type: RO
BitField Desc: PWE input PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPweSpeed_PwePwSpeed_Bit_Start                                                            0
#define cAf6_DebugPwPweSpeed_PwePwSpeed_Bit_End                                                             31
#define cAf6_DebugPwPweSpeed_PwePwSpeed_Mask                                                          cBit31_0
#define cAf6_DebugPwPweSpeed_PwePwSpeed_Shift                                                                0
#define cAf6_DebugPwPweSpeed_PwePwSpeed_MaxVal                                                      0xffffffff
#define cAf6_DebugPwPweSpeed_PwePwSpeed_MinVal                                                             0x0
#define cAf6_DebugPwPweSpeed_PwePwSpeed_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW CLA Speed
Reg Addr   : 0x00_0035
Reg Formula:
    Where  :
Reg Desc   :
This register is used to show PWE PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwClaLoSpeed_Base                                                                0x000035
#define cAf6Reg_DebugPwClaLoSpeed                                                                     0x000035
#define cAf6Reg_DebugPwClaLoSpeed_WidthVal                                                                  32
#define cAf6Reg_DebugPwClaLoSpeed_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ClaPwSpeed
BitField Type: RO
BitField Desc: CLA output PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_Bit_Start                                                          0
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_Bit_End                                                           31
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_Mask                                                        cBit31_0
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_Shift                                                              0
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_MaxVal                                                    0xffffffff
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_MinVal                                                           0x0
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_RstVal                                                           0x0

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_PW_THA60210011PWDEBUGGERREG_H_ */


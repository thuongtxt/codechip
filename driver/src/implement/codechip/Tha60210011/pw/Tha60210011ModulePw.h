/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011ModulePw.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : 60210011 module PW internal utility declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPW_H_
#define _THA60210011MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "../../../default/pw/ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePw *Tha60210011ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011PwCircuitSliceAndHwIdInSliceGet(AtPw pw, uint8* slice, uint32* hwIdInSlice);
eBool Tha60210011PwCircuitBelongsToLoLine(AtPw pw);
void Tha60210011ModulePwCircuitInfoDisplay(AtPw pwAdapter);
uint32 Tha60210011ModulePwTdmLineIdOfPw(AtPw pwAdapter, uint32 moduleId);
uint32 Tha60210011ModulePwMaxNumPwsPerSts24Slice(Tha60210011ModulePw self);
uint32 Tha60210011ModulePwOriginalNumPwsPerSts24Slice(Tha60210011ModulePw self);

ThaPwDebugger Tha60210011PwDebuggerNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPW_H_ */


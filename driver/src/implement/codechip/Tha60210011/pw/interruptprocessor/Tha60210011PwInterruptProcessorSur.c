/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210011SurPwInterruptProcessor.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PW interrupt processor for 60210011 product: bandwidth of 20G
 *               8192 PWEs PM/FM.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/interruptprocessor/ThaPwInterruptProcessor.h"
#include "../../../../default/pw/interruptprocessor/ThaPwInterruptProcessorInternal.h"
#include "../../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "Tha60210011PwInterruptProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cPw_slice_intr_Mask(sl)     ((cBit0) << (sl))
#define cPw_grp_intr_Mask(grp)      ((cBit0) << (grp))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011PwInterruptProcessorSur
    {
    tThaPwInterruptProcessor super;
    } tTha60210011PwInterruptProcessorSur;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwInterruptProcessorMethods m_ThaPwInterruptProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 BaseAddress(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0x700000U;
    }

static void Process(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    AtModulePw module = (AtModulePw)ThaPwInterruptProcessorModuleGet(self);
    const uint32 baseAddress = mMethodsGet(self)->BaseAddress(self);
    uint32 intrSta = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_PW_Def_Level1_Group_Interrupt_OR);
    uint32 slice;
    static uint32 cNumberPwSlice = 8;
    AtUnused(glbIntr);

    for (slice = 0; slice < cNumberPwSlice; slice++)
        {
        if (intrSta & cPw_slice_intr_Mask(slice))
            {
            uint32 intrStat = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_PW_Def_Level2_Group_Interrupt_OR(slice));
            uint32 grpid;

            for (grpid = 0; grpid < 32; grpid++)
                {
                if (intrStat & cPw_grp_intr_Mask(grpid))
                    {
                    uint32 intrStat32 = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW(slice, grpid));
                    uint32 subid;

                    for (subid = 0; subid < 32; subid++)
                        {
                        if (intrStat32 & cPw_grp_intr_Mask(subid))
                            {
                            uint32 pwid = (slice << 10) + (grpid << 5) + subid;

                            /* Get PW channel */
                            AtPw pwChannel = AtModulePwGetPw(module, pwid);

                            /* Process channel interrupt. */
                            AtPwInterruptProcess(pwChannel, pwid, hal);
                            }
                        }
                    }
                }
            }
        }
    }

static eAtRet Enable(ThaPwInterruptProcessor self, eBool enable)
    {
    const uint32 baseAddress = ThaPwInterruptProcessorBaseAddress(self);
    AtDevice device = AtModuleDeviceGet((AtModule)ThaPwInterruptProcessorModuleGet(self));
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 regAddr = baseAddress + cAf6Reg_FMPM_PW_Def_Interrupt_Mask;
    uint32 regVal  = enable ? cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Mask : 0x0;

    AtHalWrite(hal, regAddr, regVal);

    return cAtOk;
    }

static uint32 CurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_Base;
    }

static uint32 InterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_Base;
    }

static uint32 InterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_Base;
    }

static void OverrideThaPwInterruptProcessor(ThaPwInterruptProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwInterruptProcessorOverride, mMethodsGet(self), sizeof(m_ThaPwInterruptProcessorOverride));

        mMethodOverride(m_ThaPwInterruptProcessorOverride, BaseAddress);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Process);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Enable);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptMaskRegister);
        }

    mMethodsSet(self, &m_ThaPwInterruptProcessorOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwInterruptProcessorSur);
    }

static ThaPwInterruptProcessor ObjectInit(ThaPwInterruptProcessor self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwInterruptProcessorObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    OverrideThaPwInterruptProcessor(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwInterruptProcessor Tha60210011PwInterruptProcessorSurNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwInterruptProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, module);
    }

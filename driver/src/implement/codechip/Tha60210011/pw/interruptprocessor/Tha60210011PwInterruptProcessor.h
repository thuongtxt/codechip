/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011PwInterruptProcessor.h
 * 
 * Created Date: Oct 29, 2015
 *
 * Description : PW interrupt processor interface of 60210011 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWINTERRUPTPROCESSOR_H_
#define _THA60210011PWINTERRUPTPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/interruptprocessor/ThaPwInterruptProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwInterruptProcessorPmc
    {
    tThaPwInterruptProcessor super;
    } tTha60210011PwInterruptProcessorPmc;

/*--------------------------- Forward declarations ---------------------------*/
ThaPwInterruptProcessor Tha60210011PwInterruptProcessorPmcObjectInit(ThaPwInterruptProcessor self, ThaModulePw module);

/* Version 1 of 1xOC48 */
ThaPwInterruptProcessor Tha60210011PwInterruptProcessorPmcNew(ThaModulePw module);

/* Version 2 of SUR PW defects. */
ThaPwInterruptProcessor Tha60210011PwInterruptProcessorSurNew(ThaModulePw module);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PWINTERRUPTPROCESSOR_H_ */


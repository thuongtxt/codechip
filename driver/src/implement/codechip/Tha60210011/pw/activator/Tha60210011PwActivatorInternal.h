/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011PwActivatorInternal.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : 60210011 PW activator representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWACTIVATORINTERNAL_H_
#define _THA60210011PWACTIVATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/activator/ThaPwActivatorInternal.h"
#include "Tha60210011PwActivator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwActivatorMethods
    {
    ThaHwPw (*HwPwObjectCreate)(Tha60210011PwActivator self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLoPw);
    }tTha60210011PwActivatorMethods;

typedef struct tTha60210011PwActivator
    {
    tThaPwDynamicActivator super;
    const tTha60210011PwActivatorMethods *methods;
    }tTha60210011PwActivator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwActivator Tha60210011PwDynamicActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PWACTIVATORINTERNAL_H_ */


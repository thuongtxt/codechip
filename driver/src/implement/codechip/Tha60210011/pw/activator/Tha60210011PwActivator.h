/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011PwActivator.h
 * 
 * Created Date: Sep 9, 2015
 *
 * Description : PW activator for 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWACTIVATOR_H_
#define _THA60210011PWACTIVATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/activator/ThaPwActivator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwActivator * Tha60210011PwActivator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PWACTIVATOR_H_ */


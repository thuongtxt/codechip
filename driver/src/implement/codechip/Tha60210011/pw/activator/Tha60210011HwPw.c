/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210011HwPw.c
 *
 * Created Date: May 16, 2015
 *
 * Description : HW PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/map/ThaModuleAbstractMap.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../map/Tha60210011ModuleMap.h"
#include "Tha60210011HwPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011HwPw)self)

#define mDebugNumberEntryCreate(field) AtDebugEntryNew(cSevNormal, #field, NumberToString(debugger, mThis(self)->field))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tThaHwPwMethods   m_ThaHwPwOverride;

/* Save super's implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tThaHwPwMethods   *m_ThaHwPwMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011HwPw *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(slice);
    mEncodeUInt(tdmPwId);
    mEncodeUInt(isLoLine);
    }

static char *NumberToString(AtDebugger debugger, uint32 number)
    {
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
    AtSnprintf(buffer, bufferSize, "%u", number);
    return buffer;
    }

static void Debug(ThaHwPw self)
    {
    m_ThaHwPwMethods->Debug(self);

    AtPrintc(cSevNormal, "* tdmPwId : %d\r\n", mThis(self)->tdmPwId);
    AtPrintc(cSevNormal, "* slice   : %d\r\n", mThis(self)->slice);
    AtPrintc(cSevNormal, "* isLoLine: %d\r\n", mThis(self)->isLoLine);
    }

static void DebuggerEntriesFill(ThaHwPw self, AtDebugger debugger)
    {
    AtDebuggerEntryAdd(debugger, mDebugNumberEntryCreate(slice));
    AtDebuggerEntryAdd(debugger, mDebugNumberEntryCreate(tdmPwId));
    AtDebuggerEntryAdd(debugger, mDebugNumberEntryCreate(isLoLine));
    }

static void OverrideThaHwPw(ThaHwPw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHwPwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHwPwOverride, m_ThaHwPwMethods, sizeof(m_ThaHwPwOverride));

        mMethodOverride(m_ThaHwPwOverride, Debug);
        mMethodOverride(m_ThaHwPwOverride, DebuggerEntriesFill);
        }

    mMethodsSet(self, &m_ThaHwPwOverride);
    }

static void OverrideAtObject(ThaHwPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaHwPw self)
    {
    OverrideAtObject(self);
    OverrideThaHwPw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011HwPw);
    }

ThaHwPw Tha60210011HwPwObjectInit(ThaHwPw self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHwPwObjectInit(self, adapter, AtChannelIdGet((AtChannel)adapter)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->tdmPwId  = tdmPwId;
    mThis(self)->slice    = slice;
    mThis(self)->isLoLine = isLo;

    return self;
    }

ThaHwPw Tha60210011HwPwNew(ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHwPw newHwPw = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHwPw == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011HwPwObjectInit(newHwPw, adapter, slice, tdmPwId, isLo);
    }

uint32 Tha60210011HwPwTdmIdGet(ThaHwPw self)
    {
    if (self == NULL)
        return 0;

    return mThis(self)->tdmPwId;
    }

void Tha60210011HwPwTdmIdSet(ThaHwPw self, uint32 tdmPwId)
    {
    if (self)
        mThis(self)->tdmPwId = tdmPwId;
    }

uint8 Tha60210011HwPwSliceGet(ThaHwPw self)
    {
    if (self)
        return mThis(self)->slice;

    return 0;
    }

eBool Tha60210011HwPwIsLo(ThaHwPw self)
    {
    if (self)
        return mThis(self)->isLoLine;

    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011HwPw.h
 * 
 * Created Date: May 16, 2015
 *
 * Description : HW PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HWPW_H_
#define _THA60210011HWPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011HwPw * Tha60210011HwPw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHwPw Tha60210011HwPwNew(ThaPwAdapter adapter, uint8 slice, uint32 hwIdInSlice, eBool isLo);
uint32 Tha60210011HwPwTdmIdGet(ThaHwPw self);
void Tha60210011HwPwTdmIdSet(ThaHwPw self, uint32 tdmPwId);
uint8 Tha60210011HwPwSliceGet(ThaHwPw self);
eBool Tha60210011HwPwIsLo(ThaHwPw self);
ThaHwPw Tha60210011HwPwObjectInit(ThaHwPw self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011HWPW_H_ */


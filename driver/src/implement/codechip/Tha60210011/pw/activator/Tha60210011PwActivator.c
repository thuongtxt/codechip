/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210011PwActivator.c
 *
 * Created Date: May 8, 2015
 *
 * Description : PW activator for 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../map/Tha60210011ModuleMap.h"
#include "../../pwe/Tha60210011ModulePwe.h"
#include "../../cla/Tha60210011ModuleCla.h"
#include "../../pda/Tha60210011ModulePda.h"
#include "../Tha60210011PwGroup.h"
#include "../Tha60210011ModulePw.h"
#include "Tha60210011HwPw.h"
#include "Tha60210011PwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011PwActivator)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011PwActivatorMethods m_methods;

/* Override */
static tThaPwDynamicActivatorMethods m_ThaPwDynamicActivatorOverride;
static tThaPwActivatorMethods        m_ThaPwActivatorOverride;

/* Save super implementation */
static const tThaPwDynamicActivatorMethods *m_ThaPwDynamicActivatorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(ThaPwAdapter adapter)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModulePwe);
    }

static Tha60210011ModulePda ModulePda(ThaPwAdapter adapter)
    {
    return (Tha60210011ModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModulePda);
    }

static void DefaultHwConfigure(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    Tha60210011ModulePdaPwDefaultHwConfigure(ModulePda(adapter), (AtPw)adapter);
    Tha60210011ModulePwePwDefaultHwConfigure(ModulePwe(adapter), adapter);
    }

static eAtRet ActivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtPw pw = ThaPwAdapterPwGet(adapter);

    if (AtPwApsGroupGet(pw))
        {
        AtPwGroup apsGroup = AtPwApsGroupGet(pw);
        ret = Tha60210011ModulePweApsGroupPwAdd(ModulePwe(adapter), apsGroup, (AtPw)adapter);
        if (ret != cAtOk)
            return ret;
        }

    if (AtPwHsGroupGet(pw))
        {
        ret = Tha60210011PwHsGroupHwPwAdd(AtPwHsGroupGet(pw), pw);
        if (ret != cAtOk)
            return ret;
        }

    DefaultHwConfigure(self, adapter);
    ret = Tha60210011ModulePwePwHeaderFlush(ModulePwe(adapter), (AtPw)adapter);
    if (ret != cAtOk)
        return ret;

    return m_ThaPwDynamicActivatorMethods->ActivateConfiguration(self, adapter);
    }

static eAtRet DeactivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtPw pw = ThaPwAdapterPwGet(adapter);

    if (AtPwApsGroupGet(pw))
        {
        AtPwGroup apsGroup = AtPwApsGroupGet(pw);
        ret = Tha60210011ModulePweApsGroupPwRemove(ModulePwe(adapter), apsGroup, (AtPw)adapter);
        if (ret != cAtOk)
            return ret;
        }

    if (AtPwHsGroupGet(pw))
        {
        ret = Tha60210011PwHsGroupHwPwRemove(AtPwHsGroupGet(pw), pw);
        if (ret != cAtOk)
            return ret;
        }

    Tha60210011ModulePdaHoLoIndicationReset(ModulePda(adapter), adapter);
    return m_ThaPwDynamicActivatorMethods->DeactivateConfiguration(self, adapter);
    }

static ThaModuleMap ModuleMap(ThaPwAdapter adapter)
    {
    return (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModuleMap);
    }

static ThaHwPw HwPwObjectCreate(Tha60210011PwActivator self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLoPw)
    {
    AtUnused(self);
    return Tha60210011HwPwNew(adapter, slice, tdmPwId, isLoPw);
    }

static ThaHwPw HwPwAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    uint32 tdmPwId;
    uint8 slice;
    ThaModuleMap moduleMap = ModuleMap(adapter);
    eBool isLo = Tha60210011PwCircuitBelongsToLoLine((AtPw)adapter);
    AtUnused(self);

    tdmPwId = Tha60210011ModuleMapPwAllocate(moduleMap, adapter, isLo, &slice);
    if (AtModuleAccessible((AtModule)ThaPwActivatorModuleGet(self)) && (tdmPwId == cThaMapInvalidPwChannelId))
        return NULL;

    return mMethodsGet(mThis(self))->HwPwObjectCreate(mThis(self), adapter, slice, tdmPwId, isLo);
    }

static eAtRet HwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaModuleMap moduleMap;
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    AtUnused(self);

    if (hwPw == NULL)
        return cAtOk;

    moduleMap = ModuleMap(adapter);
    Tha60210011ModuleMapPwDeallocate(moduleMap, Tha60210011HwPwIsLo(hwPw), Tha60210011HwPwSliceGet(hwPw), Tha60210011HwPwTdmIdGet(hwPw));

    AtObjectDelete((AtObject)hwPw);
    return cAtOk;
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;

    ret |= ThaPwDynamicActivatorPwDeactivate(self, adapter);
    ret |= ThaPwActivatorPwCircuitDisconnect(self, adapter);
    ret |= ThaPwDynamicActivatorResourceDeallocate(self, adapter);

    return ret;
    }

static void OverrideThaPwDynamicActivator(ThaPwActivator self)
    {
    ThaPwDynamicActivator activator = (ThaPwDynamicActivator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwDynamicActivatorMethods = mMethodsGet(activator);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDynamicActivatorOverride, m_ThaPwDynamicActivatorMethods, sizeof(m_ThaPwDynamicActivatorOverride));

        mMethodOverride(m_ThaPwDynamicActivatorOverride, ActivateConfiguration);
        mMethodOverride(m_ThaPwDynamicActivatorOverride, DeactivateConfiguration);
        }

    mMethodsSet(activator, &m_ThaPwDynamicActivatorOverride);
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, mMethodsGet(self), sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, HwPwAllocate);
        mMethodOverride(m_ThaPwActivatorOverride, HwPwDeallocate);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitUnbind);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideThaPwDynamicActivator(self);
    OverrideThaPwActivator(self);
    }

static void MethodsInit(Tha60210011PwActivator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, HwPwObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwActivator);
    }

ThaPwActivator Tha60210011PwDynamicActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwDynamicActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha60210011PwDynamicActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PwDynamicActivatorObjectInit(newActivator, pwModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011HwPwInternal.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : 60210011 HW PW internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HWPWINTERNAL_H_
#define _THA60210011HWPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/activator/ThaHwPwInternal.h"
#include "Tha60210011HwPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011HwPw
    {
    tThaHwPw super;

    uint8  slice;
    uint32 tdmPwId;
    eBool isLoLine;
    }tTha60210011HwPw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210011HWPWINTERNAL_H_ */


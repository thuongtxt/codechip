/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210011PwGroupInternal.h
 * 
 * Created Date: May 6, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWGROUPINTERNAL_H_
#define _THA60210011PWGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pw/AtPwGroupInternal.h"
#include "Tha60210011PwGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwApsGroup
    {
    tAtPwApsGroup super;
    }tTha60210011PwApsGroup;

typedef struct tTha60210011PwHsGroup
    {
    tAtPwHsGroup super;
    }tTha60210011PwHsGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THA60210011PWGROUPINTERNAL_H_ */


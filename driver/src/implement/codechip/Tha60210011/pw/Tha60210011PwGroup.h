/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210011PwGroup.h
 * 
 * Created Date: May 6, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWGROUP_H_
#define _THA60210011PWGROUP_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPwGroup Tha60210011PwApsGroupNew(uint32 groupId, AtModulePw module);
AtPwGroup Tha60210011PwHsGroupNew(uint32 groupId, AtModulePw module);
eAtRet Tha60210011PwHsGroupHwPwAdd(AtPwGroup self, AtPw pw);
eAtRet Tha60210011PwHsGroupHwPwRemove(AtPwGroup self, AtPw pw);

#endif /* _THA60210011PWGROUP_H_ */


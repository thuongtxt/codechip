/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Tha60210011PwDefectControllerPmc.c
 *
 * Created Date: Mar 7, 2016
 *
 * Description : Defect controller of 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/ThaModulePw.h"
#include "../../../../default/pda/ThaModulePda.h"
#include "../../../Tha60210021/pw/defectcontroller/Tha60210021PwDefectController.h"
#include "../Tha60210011ModulePw.h"
#include "Tha60210011PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011PwDefectControllerPmc)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtChannelMethods                m_AtChannelOverride;
static tAtPwMethods                     m_AtPwOverride;
static tThaPwDefectControllerMethods    m_ThaPwDefectControllerOverride;

/* Save super's implementation */
static const tAtObjectMethods              *m_AtObjectMethods              = NULL;
static const tAtChannelMethods             *m_AtChannelMethods             = NULL;
static const tAtPwMethods                  *m_AtPwMethods                  = NULL;
static const tThaPwDefectControllerMethods *m_ThaPwDefectControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    eAtModulePwRet ret = m_AtPwMethods->LopsSetThresholdSet(self, numPackets);
    if (ret == cAtOk)
        mThis(self)->lopsSetThresholdInPkt = numPackets;

    return ret;
    }

static uint32 LopsSetThresholdGet(AtPw self)
    {
    /* This product configure LOPS to HW in ms unit, so need to convert from packet unit into ms unit
     * to configure to HW. When getting, calculating make value compute from HW ms unit into packet unit
     * not exact, so need to cache LOPS in packet unit when setting and return cached value when getting
     */
    return mThis(self)->lopsSetThresholdInPkt;
    }

static eBool LopsClearThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPw PwGet(ThaPwDefectController self)
    {
    return (AtPw)ThaPwAdapterPwGet(ThaPwDefectControllerAdapter((AtPw)self));
    }

static eBool PwIsCep(ThaPwDefectController self)
    {
    return (AtPwTypeGet(PwGet(self)) == cAtPwTypeCEP) ? cAtTrue : cAtFalse;
    }

static uint32 LopsMask(ThaPwDefectController self)
    {
    return PwIsCep(self) ? cBit4 : (cBit4 | cBit1);
    }

static uint32 MissingMask(ThaPwDefectController self)
    {
    return PwIsCep(self) ? cBit1 : 0;
    }

static eBool MissingPacketDefectThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    if (alarmType == cAtPwAlarmTypeMissingPacket)
        return PwIsCep((ThaPwDefectController)self) ? cAtTrue : cAtFalse;

    return m_AtChannelMethods->AlarmIsSupported(self, alarmType);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011PwDefectControllerPmc * object = (tTha60210011PwDefectControllerPmc *)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(lopsSetThresholdInPkt);
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwDefectControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, m_ThaPwDefectControllerMethods, sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, LopsMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, MissingMask);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel pw = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(pw, &m_AtChannelOverride);
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdIsSupported);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdIsSupported);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtObject(ThaPwDefectController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideAtPw(self);
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideThaPwDefectController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwDefectControllerPmc);
    }

ThaPwDefectController Tha60210011PwDefectControllerPmcObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021PwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60210011PwDefectControllerPmcNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PwDefectControllerPmcObjectInit(newController, pw);
    }

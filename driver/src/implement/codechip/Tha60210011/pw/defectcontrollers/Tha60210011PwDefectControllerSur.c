/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module PW
 *
 * File        : Tha60210011PwDefectController.c
 *
 * Created Date: Sep 4, 2014
 *
 * Description : PW Defect controller of product 60210011: bandwidth 20G 8192 PWEs FMPM.
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/defectcontrollers/ThaPwDefectControllerInternal.h"
#include "../../../../default/pw/ThaModulePw.h"
#include "../../../../default/pda/ThaModulePda.h"
#include "../../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "../Tha60210011ModulePw.h"
#include "Tha60210011PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011PwDefectControllerSur)((void*)self))

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011PwDefectControllerSur * Tha60210011PwDefectControllerSur;

typedef struct tTha60210011PwDefectControllerSur
    {
    tTha60210011PwDefectControllerPmc super;
    }tTha60210011PwDefectControllerSur;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods             m_AtChannelOverride;
static tAtPwMethods                  m_AtPwOverride;
static tThaPwDefectControllerMethods m_ThaPwDefectControllerOverride;

/* Save super's implementation */
static const tAtPwMethods *m_AtPwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtChannel self)
    {
    return ThaModulePwDefectBaseAddress((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 CurrentStatus(AtChannel self)
    {
    return ThaModulePwDefectCurrentStatusRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 InterruptStatus(AtChannel self)
    {
    return ThaModulePwDefectInterruptStatusRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 InterruptMask(AtChannel self)
    {
    return ThaModulePwDefectInterruptMaskRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 DefectMaskGet(uint32 regVal)
    {
    uint32 defects = 0;
    
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Mask)
        defects |= cAtPwAlarmTypeRBit;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Mask)
        defects |= cAtPwAlarmTypeStrayPacket;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Mask)
        defects |= cAtPwAlarmTypeMalformedPacket;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Mask)
        defects |= cAtPwAlarmTypeLops;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Mask)
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Mask)
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Mask)
        defects |= cAtPwAlarmTypeMissingPacket;
    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Mask)
        defects |= cAtPwAlarmTypeLBit;
        
    return defects;
    }

static uint32 DefectHwGet(AtChannel self)
    {
    const uint32 address = CurrentStatus(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    return DefectMaskGet(regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHwGet(self);
    }

static uint32 DefectHistoryHwGet(AtChannel self, eBool read2Clear)
    {
    const uint32 address = InterruptStatus(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    uint32 defects = DefectMaskGet(regVal);

    if (read2Clear)
        mChannelHwWrite(self, address, regVal, cAtModulePw);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtTrue);
    }

static void InterruptProcess(AtPw self, uint32 hwPw, AtHal hal)
    {
    const uint32 regAddr = CurrentStatus((AtChannel)self) + hwPw;
    uint32 regVal = AtHalRead(hal, regAddr);
    uint32 defects    = 0;
    uint32 state      = 0;

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Mask)
        {
        defects |= cAtPwAlarmTypeRBit;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Mask)
            state |= cAtPwAlarmTypeRBit;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Mask)
        {
        defects |= cAtPwAlarmTypeLops;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Mask)
            state |= cAtPwAlarmTypeLops;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Mask)
        {
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Mask)
            state |= cAtPwAlarmTypeJitterBufferUnderrun;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Mask)
        {
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Mask)
            state |= cAtPwAlarmTypeJitterBufferOverrun;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Mask)
        {
        defects |= cAtPwAlarmTypeMissingPacket;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Mask)
            state |= cAtPwAlarmTypeMissingPacket;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Mask)
        {
        defects |= cAtPwAlarmTypeLBit;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Mask)
            state |= cAtPwAlarmTypeLBit;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Mask)
        {
        defects |= cAtPwAlarmTypeStrayPacket;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Mask)
            state |= cAtPwAlarmTypeStrayPacket;
        }

    if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Mask)
        {
        defects |= cAtPwAlarmTypeMalformedPacket;
        if (regVal & cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Mask)
            state |= cAtPwAlarmTypeMalformedPacket;
        }

    /* Only clear masked bits. */
    regVal = (regVal >> 16) & cBit15_0;

    /* Clear interrupt */
    AtHalWrite(hal, regAddr, regVal);

    AtChannelAllAlarmListenersCall((AtChannel)ThaPwAdapterPwGet(mAdapter(self)), defects, state);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtPwAlarmTypeLBit|cAtPwAlarmTypeRBit|cAtPwAlarmTypeLops|
                                    cAtPwAlarmTypeMissingPacket|cAtPwAlarmTypeJitterBufferUnderrun|
                                    cAtPwAlarmTypeJitterBufferOverrun|cAtPwAlarmTypeStrayPacket|
                                    cAtPwAlarmTypeMalformedPacket);
    AtUnused(self);
    return supportedMasks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    const uint32 address = InterruptMask(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);

    mSetIntrBitMask(cAtPwAlarmTypeLBit,    defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeRBit,    defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeLops,    defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeMissingPacket,    defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferUnderrun, defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferOverrun,  defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeStrayPacket,      defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeMalformedPacket,  defectMask, enableMask, cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Mask);
    mChannelHwWrite(self, address, regVal, cAtModulePw);

    if (regVal)
        AtChannelInterruptEnable(self);
    else
        AtChannelInterruptDisable(self);
    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    const uint32 address = InterruptMask(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    return DefectMaskGet(regVal);
    }

static eAtRet HwInterruptEnable(AtPw self, eBool enable)
    {
    uint32 offset = ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self) >> 5;
    uint32 bitMask = cBit0 << (ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self) & 0x1FU);
    const uint32 regAddr = BaseAddress((AtChannel)self) + cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_Base + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePw);
    regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HwInterruptEnable((AtPw)self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HwInterruptEnable((AtPw)self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint32 offset = ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self) >> 5;
    uint32 bitMask = cBit0 << (ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self) & 0x1FU);
    const uint32 regAddr = BaseAddress(self) + cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_Base + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePw);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 defectType)
    {
    AtUnused(self);
    switch (defectType)
        {
        case cAtPwAlarmTypeLBit:                return cAtTrue;
        case cAtPwAlarmTypeRBit:                return cAtTrue;
        case cAtPwAlarmTypeLops:                return cAtTrue;
        case cAtPwAlarmTypeMissingPacket:       return cAtTrue;
        case cAtPwAlarmTypeJitterBufferUnderrun:return cAtTrue;
        case cAtPwAlarmTypeJitterBufferOverrun: return cAtTrue;
        case cAtPwAlarmTypeMalformedPacket:     return cAtTrue;
        case cAtPwAlarmTypeStrayPacket:         return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 PwDefaultOffset(ThaPwDefectController self)
    {
    uint32 pwId       = AtChannelIdGet((AtChannel)mAdapter(self));
    uint32 pwLevel2Id = (pwId & cBit12_10) >> 10;
    uint32 pwLevel1Id = (pwId & cBit9_5)   >> 5;
    uint32 pwLevel0Id = (pwId & cBit4_0);

    return (pwLevel2Id * 0x400UL) + (pwLevel1Id * 0x20) + pwLevel0Id;
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, mMethodsGet(self), sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, PwDefaultOffset);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, InterruptProcess);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel pw = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(pw), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(pw, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideAtPw(self);
    OverrideAtChannel(self);
    OverrideThaPwDefectController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwDefectControllerSur);
    }

static ThaPwDefectController ObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwDefectControllerPmcObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60210011PwDefectControllerSurNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, pw);
    }

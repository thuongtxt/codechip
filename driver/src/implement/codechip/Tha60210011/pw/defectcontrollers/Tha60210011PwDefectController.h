/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011PwDefectController.h
 * 
 * Created Date: Oct 30, 2015
 *
 * Description : PW defect controller interface for 60210011 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWDEFECTCONTROLLER_H_
#define _THA60210011PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210021/pw/defectcontroller/Tha60210021PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwDefectControllerPmc * Tha60210011PwDefectControllerPmc;

typedef struct tTha60210011PwDefectControllerPmc
    {
    tTha60210021PwDefectController super;

    /* This product configure LOPS threshold in ms unit converted from number of packet */
    uint32 lopsSetThresholdInPkt;
    }tTha60210011PwDefectControllerPmc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60210011PwDefectControllerPmcNew(AtPw pw);
ThaPwDefectController Tha60210011PwDefectControllerPmcObjectInit(ThaPwDefectController self, AtPw pw);
ThaPwDefectController Tha60210011PwDefectControllerSurNew(AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PWDEFECTCONTROLLER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Tha60210011ModulePw.c
 *
 * Created Date: May 6, 2015
 *
 * Description : Module pw of 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "../../Tha60210031/pmc/Tha60210031PmcReg.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../cdr/Tha60210011ModuleCdr.h"
#include "../map/Tha60210011ModuleMap.h"
#include "../man/Tha60210011Device.h"
#include "../common/Tha602100xxCommon.h"
#include "headercontroller/Tha60210011PwHeaderControllerInternal.h"
#include "activator/Tha60210011HwPw.h"
#include "Tha60210011ModulePw.h"
#include "Tha60210011ModulePwInternal.h"
#include "Tha60210011PwGroupInternal.h"
#include "defectcontrollers/Tha60210011PwDefectController.h"
#include "interruptprocessor/Tha60210011PwInterruptProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210011ModulePw *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModulePwMethods m_methods;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtModulePwMethods          m_AtModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;
static tTha60150011ModulePwMethods m_Tha60150011ModulePwOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtModulePwMethods  *m_AtModulePwMethods  = NULL;
static const tThaModulePwMethods *m_ThaModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern ThaPwActivator Tha60210011PwDynamicActivatorNew(AtModulePw pwModule);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxApsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 8192;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 4096;
    }

static AtPwGroup ApsGroupObjectCreate(AtModulePw self, uint32 groupId)
    {
    return Tha60210011PwApsGroupNew(groupId, self);
    }

static AtPwGroup HsGroupObjectCreate(AtModulePw self, uint32 groupId)
    {
    return Tha60210011PwHsGroupNew(groupId, self);
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210011PwDefectControllerSurNew(pw);
    else
        return Tha60210011PwDefectControllerPmcNew(pw);
    }

static ThaPwDefectController DebugDefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210011PwDefectControllerPmcNew(pw);
    else
        return Tha60210011PwDefectControllerSurNew(pw);
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210011PwInterruptProcessorSurNew(self);
    else
        return Tha60210011PwInterruptProcessorPmcNew(self);
    }

static ThaPwInterruptProcessor DebugIntrProcessorCreate(ThaModulePw self)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return Tha60210011PwInterruptProcessorPmcNew(self);
    else
        return Tha60210011PwInterruptProcessorSurNew(self);
    }

static eBool DynamicPwAllocation(ThaModulePw self)
    {
    /* This product will use dynamic activator to activate PW, the reason is: There are some properties of
     * pseudowire that are configured to registers with offset bases on circuit, it means that we only can
     * configure these properties to HW when PW has been bound to circuit, if a pseudowire has not been bound
     * to circuit yet but application configure these properties, we need to save to database and apply to HW
     * after pw has been bound circuit */

    AtUnused(self);
    return cAtTrue;
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha60210011PwDynamicActivatorNew((AtModulePw)self);
    }

static uint32 LocalPwId(ThaModulePw self, AtPw adapter)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet((ThaPwAdapter)adapter);
    AtUnused(self);

    if (hwPw == NULL)
        {
        AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "Hardware PW of PW#%u is NULL\r\n", AtChannelIdGet((AtChannel)adapter));
        return AtChannelIdGet((AtChannel)adapter);
        }

    return Tha60210011HwPwTdmIdGet(ThaPwAdapterHwPwGet((ThaPwAdapter)adapter));
    }

static ThaPwAdapter HwPwId2Adapter(ThaModulePw self, uint32 hwId)
    {
    AtUnused(self);
    AtUnused(hwId);
    return NULL;
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210011PwHeaderControllerNew((ThaPwAdapter)adapter);
    }

static ThaPwHeaderController BackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210011PwBackupHeaderControllerNew((ThaPwAdapter)adapter);
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 8192;
    }

static eAtModulePwRet CepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    AtUnused(khz);
    AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "This product does not support configure Time stamp Frequency CES and CEP separately\r\n");
    return cAtErrorModeNotSupport;
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    return AtModulePwRtpTimestampFrequencyGet(self);
    }

static eAtModulePwRet CesRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    AtUnused(khz);
    AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "This product does not support configure Time stamp Frequency CES and CEP separately\r\n");
    return cAtErrorModeNotSupport;
    }

static uint32 CesRtpTimestampFrequencyGet(AtModulePw self)
    {
    return AtModulePwRtpTimestampFrequencyGet(self);
    }

static ThaModuleCdr CdrModule(AtModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    eAtRet ret = cAtOk;
    ret |= Tha60210011ModuleCdrDcrRtpTimestampFrequencySet(CdrModule(self), khz);
    return ret;
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
    return Tha60210011ModuleCdrDcrRtpTimestampFrequencyGet(CdrModule(self));
    }

static eBool HwStorageIsSupported(Tha60150011ModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RtpIsEnabledAsDefault(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PwAdapterHwId(ThaModulePw self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    AtUnused(self);
    return Tha60210011HwPwTdmIdGet(hwPw);
    }

static uint32 DefaultCounterModule(ThaModulePw self)
    {
    /* This overriding is used to switch counter module to SUR module, but this
     * is still under testing. Return cAtModuleSur to redirect. */
    return m_ThaModulePwMethods->DefaultCounterModule(self);
    }

static ThaPwDebugger PwDebuggerCreate(ThaModulePw self)
    {
    AtUnused(self);
    return Tha60210011PwDebuggerNew();
    }

static eBool TimingModeIsApplicableForEPAR(ThaModulePw self, eAtTimingMode timingMode)
    {
    if ((timingMode == cAtTimingModeExt1Ref) ||
        (timingMode == cAtTimingModeExt2Ref))
        return cAtTrue;
    return m_ThaModulePwMethods->TimingModeIsApplicableForEPAR(self, timingMode);
    }

static eAtRet ResetPw(ThaModulePw self, AtPw pw)
    {
    if (AtPwEthPortGet(pw) == NULL)
        return cAtOk;

    return m_ThaModulePwMethods->ResetPw(self, pw);
    }

static uint32 DefaultLopsSetThreshold(AtModulePw self)
    {
    AtUnused(self);
    return 30;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011ModulePw * object = (tTha60210011ModulePw *)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt8Array(backupHeaderBuffer, cThaMaxPwPsnHdrLen);
    mEncodeUInt8Array(backuppsnBuffer, cThaMaxPwPsnHdrLen);
    }

static eAtRet PwCircuitSliceAndHwIdInSliceGet(Tha60210011ModulePw self, AtPw pw, uint8* slice, uint32* hwIdInSlice)
    {
    eAtRet ret = cAtErrorInvlParm;
    eAtPwType pwType = AtPwTypeGet(pw);
    AtChannel circuit;
    uint8 localSlice = 0;
    uint8 localHwIdInSlice = 0;
    AtUnused(self);

    /*
     * NOTE:
     * - Return OC24 slice space if circuit is lo-order
     * - Return OC48 slice space if circuit is hi-order
     */

    if (slice) *slice = 0;
    if (hwIdInSlice) *hwIdInSlice = 0;

    circuit = ThaModulePwReferenceCircuitToConfigureHardware((ThaModulePw)self, (ThaPwAdapter)pw, AtPwBoundCircuitGet(pw));
    if (circuit == NULL)
        return cAtErrorNullPointer;

    if (pwType == cAtPwTypeSAToP)
        ret = ThaPdhChannelHwIdGet((AtPdhChannel)circuit, cThaModulePwe, &localSlice, &localHwIdInSlice);

    if (pwType == cAtPwTypeCESoP)
        ret = ThaPdhChannelHwIdGet((AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)circuit), cThaModulePwe, &localSlice, &localHwIdInSlice);

    if (pwType == cAtPwTypeCEP)
        ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)circuit, cThaModulePwe, &localSlice, &localHwIdInSlice);

    if (slice)
        *slice = localSlice;

    if (hwIdInSlice)
        *hwIdInSlice = localHwIdInSlice;

    return ret;
    }

static eBool PwCircuitBelongsToLoLine(Tha60210011ModulePw self, AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);
    AtSdhChannel sdhChannel = NULL;
    eAtSdhChannelType type;
    AtUnused(self);

    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        return cAtTrue;

    if (pwType != cAtPwTypeCEP)
        return cAtFalse;

    sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    type = AtSdhChannelTypeGet(sdhChannel);
    if ((type == cAtSdhChannelTypeVc4)     ||
        (type == cAtSdhChannelTypeVc4_64c) ||
        (type == cAtSdhChannelTypeVc4_16c) ||
        (type == cAtSdhChannelTypeVc4_4c)  ||
        (type == cAtSdhChannelTypeVc4_nc))
        return cAtFalse;

    if ((type == cAtSdhChannelTypeVc3) &&
        (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeAu3))
        return cAtFalse;

    return cAtTrue;
    }

static eBool HitlessHeaderChangeShouldBeEnabledByDefault(ThaModulePw self)
    {
    return Tha602100xxModulePwHitlessHeaderChangeShouldBeEnabledByDefault(self);
    }

static uint32 HoPwTdmLineId(Tha60210011ModulePw self, AtPw pwAdapter, uint32 hwIdInSlice, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    AtUnused(moduleId);
    return hwIdInSlice;
    }

static eBool AutoRxMBitShouldEnable(AtModulePw self)
    {
    return Tha602100xxModuleAutoRxMBitShouldEnable(self);
    }

static eBool RtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)CdrModule(self);

    if (Tha60210011ModuleCdrFlexibleFrequency(cdrModule))
        return Tha60210011ModuleCdrTimestampFrequencyInRange(cdrModule, khz);

    return m_AtModulePwMethods->RtpTimestampFrequencyIsSupported(self, khz);
    }

static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210011ModulePw self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static ThaVersionReader VersionReader(Tha60210011ModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static uint32 OriginalNumPwsPerSts24Slice(Tha60210011ModulePw self)
    {
    AtUnused(self);
    return 1024UL;
    }

static uint32 MaxNumPwsPerSts24Slice(Tha60210011ModulePw self)
    {
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionHas1344PwsPerSts24Slice(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? 1344UL : OriginalNumPwsPerSts24Slice(self);
    }

static void OverrideAtObject(AtModulePw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxApsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, ApsGroupObjectCreate);
        mMethodOverride(m_AtModulePwOverride, HsGroupObjectCreate);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, RtpIsEnabledAsDefault);
        mMethodOverride(m_AtModulePwOverride, DefaultLopsSetThreshold);
        mMethodOverride(m_AtModulePwOverride, AutoRxMBitShouldEnable);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyIsSupported);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(pwModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, m_ThaModulePwMethods, sizeof(m_ThaModulePwOverride));
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, HeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, BackupHeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, DynamicPwAllocation);
        mMethodOverride(m_ThaModulePwOverride, LocalPwId);
        mMethodOverride(m_ThaModulePwOverride, PwAdapterHwId);
        mMethodOverride(m_ThaModulePwOverride, HwPwId2Adapter);
        mMethodOverride(m_ThaModulePwOverride, InterruptProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, PwDebuggerCreate);
        mMethodOverride(m_ThaModulePwOverride, TimingModeIsApplicableForEPAR);
        mMethodOverride(m_ThaModulePwOverride, DefaultCounterModule);
        mMethodOverride(m_ThaModulePwOverride, ResetPw);
        mMethodOverride(m_ThaModulePwOverride, DebugDefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, DebugIntrProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, HitlessHeaderChangeShouldBeEnabledByDefault);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideTha60150011ModulePw(AtModulePw self)
    {
    Tha60150011ModulePw pwModule = (Tha60150011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePwOverride, mMethodsGet(pwModule), sizeof(m_Tha60150011ModulePwOverride));
        mMethodOverride(m_Tha60150011ModulePwOverride, HwStorageIsSupported);
        }

    mMethodsSet(pwModule, &m_Tha60150011ModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtObject(self);
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60150011ModulePw(self);
    }

static void MethodsInit(AtModulePw self)
    {
    Tha60210011ModulePw module = (Tha60210011ModulePw)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwCircuitSliceAndHwIdInSliceGet);
        mMethodOverride(m_methods, PwCircuitBelongsToLoLine);
        mMethodOverride(m_methods, HoPwTdmLineId);
        mMethodOverride(m_methods, MaxNumPwsPerSts24Slice);
        mMethodOverride(m_methods, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModulePw);
    }

AtModulePw Tha60210011ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60210011ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModulePwObjectInit(newModule, device);
    }

eAtRet Tha60210011PwCircuitSliceAndHwIdInSliceGet(AtPw pw, uint8* slice, uint32* hwIdInSlice)
    {
    Tha60210011ModulePw modulePw = (Tha60210011ModulePw)AtChannelModuleGet((AtChannel)pw);
    if (modulePw)
        return mMethodsGet(modulePw)->PwCircuitSliceAndHwIdInSliceGet(modulePw, pw, slice, hwIdInSlice);

    return cAtErrorNullPointer;
    }

eBool Tha60210011PwCircuitBelongsToLoLine(AtPw pw)
    {
    Tha60210011ModulePw modulePw = (Tha60210011ModulePw)AtChannelModuleGet((AtChannel)pw);
    if (modulePw)
        return mMethodsGet(modulePw)->PwCircuitBelongsToLoLine(modulePw, pw);

    return cAtErrorNullPointer;
    }

void Tha60210011ModulePwCircuitInfoDisplay(AtPw pwAdapter)
    {
    uint8 slice;
    uint32 hwIdInSlice;

    AtPrintc(cSevInfo, "* Private PW ID: ");
    AtPrintc(cSevNormal, "%u\r\n", AtChannelHwIdGet((AtChannel)pwAdapter));

    AtPrintc(cSevInfo, "* Circuit slice and HW STS:\r\n");
    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pwAdapter, &slice, &hwIdInSlice) != cAtOk)
        {
        AtPrintc(cSevCritical, " converting fail\r\n");
        return;
        }

    AtPrintc(cSevNormal, "        Slice : %u\r\n", slice);
    AtPrintc(cSevNormal, "        HW sts: %u\r\n", hwIdInSlice);
    }

uint32 Tha60210011ModulePwTdmLineIdOfPw(AtPw pwAdapter, uint32 moduleId)
    {
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pwAdapter);
    AtChannel circuit = ThaModulePwReferenceCircuitToConfigureHardware(pwModule, (ThaPwAdapter)pwAdapter, AtPwBoundCircuitGet(pwAdapter));
    eAtPwType pwType = AtPwTypeGet(pwAdapter);
    uint32 channelType;
    uint32 hwIdInSlice;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pwAdapter, NULL, &hwIdInSlice) != cAtOk)
        return 0;

    if (!Tha60210011PwCircuitBelongsToLoLine(pwAdapter))
        return mMethodsGet(mThis(pwModule))->HoPwTdmLineId(mThis(pwModule), pwAdapter, hwIdInSlice, moduleId);

    if (pwType == cAtPwTypeSAToP)
        {
        channelType = AtPdhChannelTypeGet((AtPdhChannel)circuit);
        if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
            return ThaPdhDe1FlatId((ThaPdhDe1)circuit);
        if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
            return (uint32)(hwIdInSlice << 5);
        }

    if (pwType == cAtPwTypeCESoP)
        {
        ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);
        return ThaPdhDe1FlatId(de1);
        }

    /* CEP */
    channelType = AtSdhChannelTypeGet((AtSdhChannel)circuit);
    if ((channelType == cAtSdhChannelTypeVc11) | (channelType == cAtSdhChannelTypeVc12))
        {
        uint8 tug2Id = AtSdhChannelTug2Get((AtSdhChannel)circuit);
        uint8 vtId   = AtSdhChannelTu1xGet((AtSdhChannel)circuit);
        return (uint32)(((uint8)hwIdInSlice << 5UL) + (tug2Id << 2UL) + vtId);
        }

    return (uint32)(hwIdInSlice << 5);
    }

uint32 Tha60210011ModulePwMaxNumPwsPerSts24Slice(Tha60210011ModulePw self)
    {
    if (self)
        return MaxNumPwsPerSts24Slice(self);
    return 0;
    }

uint32 Tha60210011ModulePwOriginalNumPwsPerSts24Slice(Tha60210011ModulePw self)
    {
    if (self)
        return OriginalNumPwsPerSts24Slice(self);
    return 0;
    }

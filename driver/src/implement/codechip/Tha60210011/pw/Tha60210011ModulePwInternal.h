/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011ModulePwInternal.h
 * 
 * Created Date: Mar 8, 2021
 *
 * Description : 60210011 module pw internal declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPWINTERNAL_H_
#define _THA60210011MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/pw/Tha60150011ModulePwInternal.h"
#include "Tha60210011ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePwMethods
    {
    eAtRet (*PwCircuitSliceAndHwIdInSliceGet)(Tha60210011ModulePw self, AtPw pw, uint8* slice, uint32* hwIdInSlice);
    eBool (*PwCircuitBelongsToLoLine)(Tha60210011ModulePw self, AtPw pw);
    uint32 (*HoPwTdmLineId)(Tha60210011ModulePw self, AtPw pwAdapter, uint32 hwIdInSlice, uint32 moduleId);
    uint32 (*MaxNumPwsPerSts24Slice)(Tha60210011ModulePw self);
    uint32 (*StartVersionHas1344PwsPerSts24Slice)(Tha60210011ModulePw self);
    }tTha60210011ModulePwMethods;

typedef struct tTha60210011ModulePw
    {
    tTha60150011ModulePw super;
    const tTha60210011ModulePwMethods *methods;

    /* Private data */
    uint8 backupHeaderBuffer[cThaMaxPwPsnHdrLen];
    uint8 backuppsnBuffer[cThaMaxPwPsnHdrLen];
    }tTha60210011ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60210011ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPWINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210011PwDebuggerInternal.h
 * 
 * Created Date: May 26, 2017
 *
 * Description : PW debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PWDEBUGGERINTERNAL_H_
#define _THA60210011PWDEBUGGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pw/debugger/ThaPwDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PwDebugger
    {
    tThaPwDebugger super;
    }tTha60210011PwDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDebugger Tha60210011PwDebuggerObjectInit(ThaPwDebugger self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PWDEBUGGERINTERNAL_H_ */


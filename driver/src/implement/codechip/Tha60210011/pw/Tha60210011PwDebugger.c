/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210011PwDebugger.c
 *
 * Created Date: Sep 4, 2015
 *
 * Description : PW debugger of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210031/pw/Tha60210031PwDebugger.h"
#include "../pda/Tha60210011ModulePda.h"
#include "Tha60210011ModulePw.h"
#include "Tha60210011PwDebuggerReg.h"
#include "Tha60210011PwDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPdaPrbsStatus      0x80800

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwDebuggerMethods m_ThaPwDebuggerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 HoPwType(uint8 numSts)
    {
    if (numSts == 1)
        return 0;
    if (numSts == 3)
        return 1;
    return 2;
    }

static uint8 LoPwType(AtSdhChannel circuit)
    {
    if (AtSdhChannelTypeGet(circuit) == cAtSdhChannelTypeVc11)
        return 4;
    if (AtSdhChannelTypeGet(circuit) == cAtSdhChannelTypeVc12)
        return 5;
    if (AtSdhChannelTypeGet(circuit) == cAtSdhChannelTypeVc3)
        return 7;
    return 0;
    }

static eAtRet PwSelect(ThaPwDebugger self, AtPw pw)
    {
    eBool belongLoLine;
    uint8 slice, oc48Id;
    uint32 sts;
    AtSdhChannel circuit = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint32 regAddr = cAf6Reg_DebugPwControl_Base;
    uint32 regVal = mChannelHwRead(pw, regAddr, cAtModulePw);

    AtUnused(self);

    mRegFieldSet(regVal, cAf6_DebugPwControl_LocalPwID_, AtChannelHwIdGet((AtChannel)pw));
    mRegFieldSet(regVal, cAf6_DebugPwControl_GlobalPwID_, AtChannelIdGet((AtChannel)pw));

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &sts) != cAtOk)
        return cAtError;

    belongLoLine = Tha60210011PwCircuitBelongsToLoLine(pw);
    oc48Id = (uint8)(belongLoLine ? slice / 2UL : slice);
    mRegFieldSet(regVal, cAf6_DebugPwControl_OC48ID_, oc48Id);

    if (belongLoLine)
        {
        mRegFieldSet(regVal, cAf6_DebugPwControl_LoOC24Slice_, slice % 2);
        if (AtPwTypeGet(pw) == cAtPwTypeCEP)
            mRegFieldSet(regVal, cAf6_DebugPwControl_HoPwType_, LoPwType(circuit));
        }
    else
        mRegFieldSet(regVal, cAf6_DebugPwControl_HoPwType_, HoPwType(AtSdhChannelNumSts(circuit)));

    mChannelHwWrite(pw, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static uint32 PdaPrbsStatusReg(ThaPwDebugger self, AtPw pw)
    {
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)AtChannelModuleGet((AtChannel)pw);
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModulePda);
    AtUnused(self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        return cThaPdaPrbsStatus + ThaModulePdaBaseAddress(modulePda);

    return 0x81000 + ThaModulePdaBaseAddress(modulePda);
    }

static uint32 PlaPrbsCheck(ThaPwDebugger self, AtPw pw, uint32 regVal)
    {
    uint32 result = 0;

    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        if (regVal & cAf6_DebugPwSticky_PlaLoPrbsErr_Mask)
            result |= cThaPwPrbsPlaError;
        if ((regVal & cAf6_DebugPwSticky_PlaLoPrbsSyn_Mask) == 0)
            result |= cThaPwPrbsPlaNotSync;
        }
    else
        {
        if (regVal & cAf6_DebugPwSticky_PlaHoPrbsErr_Mask)
            result |= cThaPwPrbsPlaError;
        if ((regVal & cAf6_DebugPwSticky_PlaHoPrbsSyn_Mask) == 0)
            result |= cThaPwPrbsPlaNotSync;
        }

    return result;
    }

static uint32 PdaPrbsCheck(ThaPwDebugger self, AtPw pw, uint32 regVal)
    {
    uint32 result = 0;

    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return Tha60210031PwPrbsCheck(self, PdaPrbsStatusReg(self, pw), pw, cThaPwPrbsPdaError, cThaPwPrbsPdaNotSync);

    if (regVal & cAf6_DebugPwSticky_PdaHoPrbsErr_Mask)
        result |= cThaPwPrbsPdaError;
    if ((regVal & cAf6_DebugPwSticky_PdaHoPrbsSyn_Mask) == 0)
        result |= cThaPwPrbsPdaNotSync;

    return result;
    }

static uint32 PwPrbsCheck(ThaPwDebugger self, AtPw pw)
    {
    uint32 result = 0;
    uint32 regAddr, regVal;
    const uint32 cDelayTimeBeforePrbsCheckingInMs = 1;

    if (AtPwBoundCircuitGet(pw) == NULL)
        return 0;

    if (mMethodsGet(self)->PwSelect(self, pw) != cAtOk)
        return cBit31_0;

    AtOsalUSleep(cDelayTimeBeforePrbsCheckingInMs * 1000);

    regAddr = cAf6Reg_DebugPwSticky_Base;
    regVal = mChannelHwRead(pw, regAddr, cAtModulePw);
    if (regVal & cAf6_DebugPwSticky_ClaPrbsErr_Mask)
        result |= cThaPwPrbsClaError;
    if (regVal & cAf6_DebugPwSticky_PwePrbsErr_Mask)
        result |= cThaPwPrbsPweError;

    result |= mMethodsGet(self)->PlaPrbsCheck(self, pw, regVal);
    result |= mMethodsGet(self)->PdaPrbsCheck(self, pw, regVal);

    if ((regVal & cAf6_DebugPwSticky_ClaPrbsSyn_Mask) == 0)
        result |= cThaPwPrbsClaNotSync;

    if ((regVal & cAf6_DebugPwSticky_PwePrbsSyn_Mask) == 0)
        result |= cThaPwPrbsPweNotSync;

    /* Clear sticky */
    mChannelHwWrite(pw, regAddr, cBit31_0, cAtModulePw);
    return result;
    }

static void PwSpeedPrint(AtPw pw, const char* title, uint32 reg)
    {
    uint32 regVal = mChannelHwRead(pw, reg, cAtModulePw);
    AtPrintc(cSevNormal, "    %-25s: %u (0x%08x = 0x%08x)\r\n", title, regVal, reg, regVal);
    }

static uint32 PlaSpeedBase(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6Reg_DebugPwPlaLoSpeed_Base;
    return cAf6Reg_DebugPwPlaHoSpeed_Base;
    }

static uint32 PdaSpeedBase(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6Reg_DebugPwPdaLoSpeed_Base;
    return cAf6Reg_DebugPwPdaHoSpeed_Base;
    }

static void PwSpeedHwClear(ThaPwDebugger self, AtPw pw)
    {
    mChannelHwRead(pw, mMethodsGet(self)->PlaSpeedBase(self, pw), cAtModulePw);
    mChannelHwRead(pw, mMethodsGet(self)->PdaSpeedBase(self, pw), cAtModulePw);

    mChannelHwRead(pw, cAf6Reg_DebugPwPweSpeed_Base, cAtModulePw);
    mChannelHwRead(pw, cAf6Reg_DebugPwClaLoSpeed_Base, cAtModulePw);
    }

static void PwSpeedShow(ThaPwDebugger self, AtPw pw)
    {
    const uint32 cDelayTimeBeforeReadPwSpeedInMs = 1;

    if (AtPwBoundCircuitGet(pw) == NULL)
        return;

    AtPrintc(cSevInfo, "* Pseudowire %u speed:\r\n", AtChannelIdGet((AtChannel)pw) + 1);

    if (mMethodsGet(self)->PwSelect(self, pw) != cAtOk)
        {
        AtPrintc(cSevCritical, "Fatal errror: Cannot select pw for checking speed\r\n");
        return;
        }
    AtOsalSleep(1);

    /* As hardware recommend, need do some actions:
     * - Read first time to clear engine
     * - Delay 1s for hardware update engine
     * - Read to show the real values */
    PwSpeedHwClear(self, pw);
    AtOsalUSleep(cDelayTimeBeforeReadPwSpeedInMs * 1000);

    PwSpeedPrint(pw, "PLA" , mMethodsGet(self)->PlaSpeedBase(self, pw));
    PwSpeedPrint(pw, "PDA" , mMethodsGet(self)->PdaSpeedBase(self, pw));

    PwSpeedPrint(pw, "PWE", cAf6Reg_DebugPwPweSpeed_Base);
    PwSpeedPrint(pw, "CLA" , cAf6Reg_DebugPwClaLoSpeed_Base);
    }

static uint32 PwPrbsCheckDefaultOffset(ThaPwDebugger self, AtPw pwAdapter)
    {
    uint8 slice;
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)AtChannelModuleGet((AtChannel)pwAdapter);
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pwAdapter, &slice, NULL) != cAtOk)
        return cBit31_0;

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        return AtChannelHwIdGet((AtChannel)pwAdapter) + ((slice / 2UL) * 8192UL) + ((slice % 2UL) * 4096UL);

    return AtChannelHwIdGet((AtChannel)pwAdapter) + ((slice / 2UL) * 16384UL) + ((slice % 2UL) * 8192UL);
    }

static void PwCepDebug(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* No information so far */
    }

static void OverrideThaPwDebugger(ThaPwDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDebuggerOverride, mMethodsGet(self), sizeof(m_ThaPwDebuggerOverride));
        mMethodOverride(m_ThaPwDebuggerOverride, PwCepDebug);
        mMethodOverride(m_ThaPwDebuggerOverride, PwPrbsCheck);
        mMethodOverride(m_ThaPwDebuggerOverride, PwSpeedShow);
        mMethodOverride(m_ThaPwDebuggerOverride, PwPrbsCheckDefaultOffset);
        mMethodOverride(m_ThaPwDebuggerOverride, PwSelect);
        mMethodOverride(m_ThaPwDebuggerOverride, PlaPrbsCheck);
        mMethodOverride(m_ThaPwDebuggerOverride, PdaPrbsCheck);
        mMethodOverride(m_ThaPwDebuggerOverride, PlaSpeedBase);
        mMethodOverride(m_ThaPwDebuggerOverride, PdaSpeedBase);
        }

    mMethodsSet(self, &m_ThaPwDebuggerOverride);
    }

static void Override(ThaPwDebugger self)
    {
    OverrideThaPwDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwDebugger);
    }

ThaPwDebugger Tha60210011PwDebuggerObjectInit(ThaPwDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDebugger Tha60210011PwDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PwDebuggerObjectInit(newChecker);
    }

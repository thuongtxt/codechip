/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60210011PwApsGroup.c
 *
 * Created Date: May 6, 2015
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/pw/AtModulePwInternal.h"
#include "Tha60210011PwGroupInternal.h"
#include "../pwe/Tha60210011ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwGroupMethods m_AtPwGroupOverride;

/* Save super implementation */
static const tAtPwGroupMethods *m_AtPwGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtPwGroup self)
    {
    AtModule pwModule = (AtModule)AtPwGroupModuleGet(self);
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet(pwModule), cThaModulePwe);
    }

static eAtModulePwRet Enable(AtPwGroup self, eBool enable)
    {
    return Tha60210011ModulePweApsGroupEnable(ModulePwe(self), self, enable);
    }

static eBool IsEnabled(AtPwGroup self)
    {
    return Tha60210011ModulePweApsGroupIsEnabled(ModulePwe(self), self);
    }

static eAtModulePwRet PwAdd(AtPwGroup self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    eAtRet ret = m_AtPwGroupMethods->PwAdd(self, pw);
    if (ret != cAtOk)
        return ret;

    if (ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    return Tha60210011ModulePweApsGroupPwAdd(ModulePwe(self), self, (AtPw)adapter);
    }

static eAtModulePwRet PwRemove(AtPwGroup self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    eAtRet ret;

    ret = m_AtPwGroupMethods->PwRemove(self, pw);
    if (ret != cAtOk)
        return ret;

    if (pw == NULL)
        return cAtOk;

    if (ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    return Tha60210011ModulePweApsGroupPwRemove(ModulePwe(self), self, (AtPw)adapter);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwApsGroup);
    }

static void OverrideAtPwGroup(AtPwGroup self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (!m_methodsInit)
        {
        m_AtPwGroupMethods = mMethodsGet(self);

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwGroupOverride, m_AtPwGroupMethods, sizeof(m_AtPwGroupOverride));
        mMethodOverride(m_AtPwGroupOverride, Enable);
        mMethodOverride(m_AtPwGroupOverride, IsEnabled);
        mMethodOverride(m_AtPwGroupOverride, PwAdd);
        mMethodOverride(m_AtPwGroupOverride, PwRemove);
        }

    mMethodsSet(self, &m_AtPwGroupOverride);
    }

static void Override(AtPwGroup self)
    {
    OverrideAtPwGroup(self);
    }

static AtPwGroup ObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (AtPwApsGroupObjectInit(self, groupId, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwGroup Tha60210011PwApsGroupNew(uint32 groupId, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwGroup newGroup = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGroup == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGroup, groupId, module);
    }

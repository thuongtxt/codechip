/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : ThaPwHsGroup.c
 *
 * Created Date: May 6, 2015
 *
 * Description : HS Group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pw/AtModulePwInternal.h"
#include "Tha60210011PwGroupInternal.h"
#include "../pwe/Tha60210011ModulePwe.h"
#include "../cla/Tha60210011ModuleCla.h"
#include "../cla/hbce/Tha60210011Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwGroupMethods m_AtPwGroupOverride;

/* Save super implementation */
static const tAtPwGroupMethods *m_AtPwGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtPwGroup self)
    {
    AtModule pwModule = (AtModule)AtPwGroupModuleGet(self);
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet(pwModule), cThaModulePwe);
    }

static ThaModuleCla ModuleCla(AtPwGroup self)
    {
    AtModule pwModule = (AtModule)AtPwGroupModuleGet(self);
    return (ThaModuleCla)AtDeviceModuleGet(AtModuleDeviceGet(pwModule), cThaModuleCla);
    }

static eAtRet HwPwAdd(AtPwGroup self, AtPw pw)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);

    ret |= Tha60210011ModulePweHsGroupPwAdd(ModulePwe(self), self, (AtPw)ThaPwAdapterGet(pw));
    ret |= Tha60210011ModuleClaHsGroupPwAdd(ModuleCla(self), self, (AtPw)ThaPwAdapterGet(pw));

    if (ret != cAtOk)
        return ret;

    /* When add PW to HS group, need to enable backup PSN if pw is enabled */
    ret = ThaHbcePwHeaderControllerEnable(ThaPwAdapterHbceGet((AtPw)adapter), ThaPwAdapterBackupHeaderController(adapter), AtChannelIsEnabled((AtChannel)pw));

    /* If back up header controller is not configured for HBCE yet,
     * Above function will return error and it is normal in this case */
    if (ret == cAtErrorNullPointer)
        return cAtOk;

    return ret;
    }

static eAtModulePwRet PwAdd(AtPwGroup self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    eAtRet ret = m_AtPwGroupMethods->PwAdd(self, pw);
    if (ret != cAtOk)
        return ret;

    if (ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    return HwPwAdd(self, pw);
    }

static eAtRet HwPwRemove(AtPwGroup self, AtPw pw)
    {
    eAtRet ret = cAtOk;
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);

    ret |= Tha60210011ModulePweHsGroupPwRemove(ModulePwe(self), self, (AtPw)ThaPwAdapterGet(pw));
    ret |= Tha60210011ModuleClaHsGroupPwRemove(ModuleCla(self), self, (AtPw)ThaPwAdapterGet(pw));

    if (ret != cAtOk)
        return ret;

    /* When add PW to HS group, need to enable backup PSN if pw is enabled */
   ret = ThaHbcePwHeaderControllerEnable(ThaPwAdapterHbceGet((AtPw)adapter), ThaPwAdapterBackupHeaderController(adapter), cAtFalse);

   /* If back up header controller is not configured for HBCE yet,
    * Above function will return error and it is normal in this case */
   if (ret == cAtErrorNullPointer)
       return cAtOk;

   return ret;
    }

static eAtModulePwRet PwRemove(AtPwGroup self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    eAtRet ret;

    ret = m_AtPwGroupMethods->PwRemove(self, pw);
    if (ret != cAtOk)
        return ret;

    if (pw == NULL)
        return cAtOk;

    if (ThaPwAdapterIsLogicPw(adapter))
        return cAtOk;

    return HwPwRemove(self, pw);
    }

static eBool LabelSetIsValid(eAtPwGroupLabelSet labelSet)
    {
    if (labelSet == cAtPwGroupLabelSetPrimary)
        return cAtTrue;
    if (labelSet == cAtPwGroupLabelSetBackup)
        return cAtTrue;
    return cAtFalse;
    }

static eAtModulePwRet TxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    if (!LabelSetIsValid(labelSet))
        return cAtErrorInvlParm;
    return Tha60210011ModulePweHsGroupLabelSetSelect(ModulePwe(self), self, labelSet);
    }

static eAtPwGroupLabelSet TxSelectedLabelGet(AtPwGroup self)
    {
    return Tha60210011ModulePweHsGroupSelectedLabelSetGet(ModulePwe(self), self);
    }

static eAtModulePwRet RxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    if (!LabelSetIsValid(labelSet))
        return cAtErrorInvlParm;
    return Tha60210011ModuleClaHsGroupLabelSetSelect(ModuleCla(self), self, labelSet);
    }

static eAtPwGroupLabelSet RxSelectedLabelGet(AtPwGroup self)
    {
    return Tha60210011ModuleClaHsGroupSelectedLabelSetGet(ModuleCla(self), self);
    }


static eAtModulePwRet Init(AtPwGroup self)
    {
    eAtRet ret = cAtOk;

    ret |= Tha60210011ModulePweHsGroupEnable(ModulePwe(self), self, cAtTrue);
    ret |= Tha60210011ModuleClaHsGroupEnable(ModuleCla(self), self, cAtTrue);

    if (ret != cAtOk)
        return ret;

    return m_AtPwGroupMethods->Init(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwHsGroup);
    }

static void OverrideAtPwGroup(AtPwGroup self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtPwGroupMethods = mMethodsGet(self);

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwGroupOverride, m_AtPwGroupMethods, sizeof(m_AtPwGroupOverride));
        mMethodOverride(m_AtPwGroupOverride, PwAdd);
        mMethodOverride(m_AtPwGroupOverride, PwRemove);
        mMethodOverride(m_AtPwGroupOverride, TxLabelSetSelect);
        mMethodOverride(m_AtPwGroupOverride, TxSelectedLabelGet);
        mMethodOverride(m_AtPwGroupOverride, RxLabelSetSelect);
        mMethodOverride(m_AtPwGroupOverride, RxSelectedLabelGet);
        mMethodOverride(m_AtPwGroupOverride, Init);
        }

    mMethodsSet(self, &m_AtPwGroupOverride);
    }

static void Override(AtPwGroup self)
    {
    OverrideAtPwGroup(self);
    }

static AtPwGroup ObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (AtPwHsGroupObjectInit(self, groupId, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwGroup Tha60210011PwHsGroupNew(uint32 groupId, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwGroup newGroup = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGroup == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGroup, groupId, module);
    }


eAtRet Tha60210011PwHsGroupHwPwAdd(AtPwGroup self, AtPw pw)
    {
    return (self) ? HwPwAdd(self, pw) : cAtErrorNullPointer;
    }

eAtRet Tha60210011PwHsGroupHwPwRemove(AtPwGroup self, AtPw pw)
    {
    return (self) ? HwPwRemove(self, pw) : cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210011PwePwCounterGetter.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PWE Pw counter getter of 60210011 support 4096 PWs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegPmcPwTxPktCnt(ro)     (0x1E10000UL + (ro) * 0x800UL)
#define cRegPmcPwTxPldOctCnt(ro)  (0x1E11000UL + (ro) * 0x800UL)
#define cRegPmcPwTxLbitPktCnt(ro) (0x1E12000UL + (ro) * 0x800UL)
#define cRegPmcPwTxRbitPktCnt(ro) (0x1E13000UL + (ro) * 0x800UL)
#define cRegPmcPwTxMorNbitCnt(ro) (0x1E14000UL + (ro) * 0x800UL)
#define cRegPmcPwTxPbitCnt(ro)    (0x1E15000UL + (ro) * 0x800UL)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011PwePwCounterGetterMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxPktCnt(mRo(clear)) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxBytesGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxPldOctCnt(mRo(clear)) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxLbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxLbitPktCnt(mRo(clear)) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxRbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxRbitPktCnt(mRo(clear)) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxMbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxMorNbitCnt((clear) ? 0 : 1) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwe);
    }

static uint32 TxPbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxPbitCnt((clear) ? 0 : 1) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwe);
    }

static uint32 TxNbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cRegPmcPwTxMorNbitCnt((clear) ? 0 : 1) + mMethodsGet(modulePwe)->CounterOffset(pw), cThaModulePwe);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "pwe_counter_getter";
    }

static void OverrideAtObject(Tha60210011PwePwCounterGetter self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(Tha60210011PwePwCounterGetter self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(Tha60210011PwePwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override methods */
        mMethodOverride(m_methods, TxPacketsGet);
        mMethodOverride(m_methods, TxBytesGet);
        mMethodOverride(m_methods, TxLbitPacketsGet);
        mMethodOverride(m_methods, TxRbitPacketsGet);
        mMethodOverride(m_methods, TxMbitPacketsGet);
        mMethodOverride(m_methods, TxPbitPacketsGet);
        mMethodOverride(m_methods, TxNbitPacketsGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwePwCounterGetter);
    }

Tha60210011PwePwCounterGetter Tha60210011PwePwCounterGetterObjectInit(Tha60210011PwePwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011PwePwCounterGetter Tha60210011PwePwCounterGetterNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011PwePwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PwePwCounterGetterObjectInit(newGetter);
    }

uint32 PwePwCounterGetterTxPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxPacketsGet(self, modulePwe, pw, clear);
    return 0;
    }

uint32 PwePwCounterGetterTxBytesGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxBytesGet(self, modulePwe, pw, clear);
    return 0;
    }

uint32 PwePwCounterGetterTxLbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxLbitPacketsGet(self, modulePwe, pw, clear);
    return 0;
    }

uint32 PwePwCounterGetterTxRbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxRbitPacketsGet(self, modulePwe, pw, clear);
    return 0;
    }

uint32 PwePwCounterGetterTxMbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxMbitPacketsGet(self, modulePwe, pw, clear);
    return 0;
    }

uint32 PwePwCounterGetterTxNbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxNbitPacketsGet(self, modulePwe, pw, clear);
    return 0;
    }

uint32 PwePwCounterGetterTxPbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->TxPbitPacketsGet(self, modulePwe, pw, clear);
    return 0;
    }


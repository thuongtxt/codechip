/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210011ModulePwe.c
 *
 * Created Date: May 5, 2015
 *
 * Description : Module PWE of 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../default/pwe/ThaModulePwe.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../pw/Tha60210011ModulePw.h"
#include "../ram/Tha60210011InternalRam.h"
#include "../man/Tha60210011Device.h"
#include "Tha60210011ModulePlaReg.h"
#include "Tha60210011ModulePweReg.h"
#include "Tha60210011ModulePweInternal.h"
#include "Tha60210011ModulePlaDebugReg.h"

/*--------------------------- Define -----------------------------------------*/

#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_DwIndex 1
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_DwIndex  0

#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_DwIndex 1
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Head_DwIndex 1
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Tail_DwIndex 0

#define cPwStateIdle                             0
#define cPwStateAdding                           1
#define cPwStateAddDone                          2
#define cPwStateRemoving                         3

#define cPayloadSizeNumFrameMask        cBit14_6
#define cPayloadSizeNumFrameShift       6

#define cPayloadSizeNumTimeslotMask     cBit5_0
#define cPayloadSizeNumTimeslotShift    0

#define cLoPayloadTimeStampSourceMask             cBit17
#define cLoPayloadTimeStampSourceShift            17

#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Shift                                                         15

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModulePwe)self)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)
#define mCounterGetter(self) mMethodsGet(mThis(self))->CounterGetter(mThis(self))
#define mPDHPWAddingProtocolToPreventBurst(self, pw) mMethodsGet(mThis(self))->PDHPWAddingProtocolToPreventBurst(mThis(self), pw)
#define mPlaOutUpsrCtrlBase(self) mMethodsGet(mThis(self))->PlaOutUpsrCtrlBase(mThis(self))
#define mPlaOutHspwCtrlBase(self) mMethodsGet(mThis(self))->PlaOutHspwCtrlBase(mThis(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModulePweMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtModuleMethods       m_AtModuleOverride;
static tThaModulePweMethods   m_ThaModulePweOverride;
static tThaModulePweV2Methods m_ThaModulePweV2Override;

/* Save super implementations */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtModuleMethods       *m_AtModuleMethods       = NULL;
static const tThaModulePweMethods   *m_ThaModulePweMethods   = NULL;
static const tThaModulePweV2Methods *m_ThaModulePweV2Methods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ModulePweDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    AtEthPort port = ThaPwAdapterEthPortGet((ThaPwAdapter)pw);
    AtUnused(self);
    return 0x10000 * AtChannelIdGet((AtChannel)port) + Tha60210011ModulePweBaseAddress();
    }

static uint32 PwPweDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw) + ModulePweDefaultOffset(self, pw);
    }

static uint32 ModulePlaDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    AtEthPort port = ThaPwAdapterEthPortGet((ThaPwAdapter)pw);
    AtUnused(self);
    return 65536 * AtChannelIdGet((AtChannel)port) + Tha60210011ModulePlaBaseAddress();
    }

static uint32 PwPlaDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw) + ModulePlaDefaultOffset(self, pw);
    }

static uint32 LoPayloadCtrlOffset(Tha60210011ModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, uint32 loPwid)
    {
    AtUnused(self);
    return (loOc48Slice * 65536UL) + (loOc24Slice * 16384UL) + loPwid;
    }

static uint32 LoPayloadCtrl(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_lo_pld_ctrl_Base;
    }

static uint32 HoPayloadCtrlOffset(Tha60210011ModulePwe self, uint32 hoOc96Slice, uint32 hoOc48Slice, uint32 hoPwid)
    {
    AtUnused(self);
    return (hoOc96Slice * 2048UL) + (hoOc48Slice * 512UL) + hoPwid;
    }

static uint32 HoPayloadCtrl(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_ho_pldd_ctrl_Base;
    }

static uint32 PwPlaLoPldControlBaseAddress(ThaModulePwe self)
    {
    return mMethodsGet(mThis(self))->LoPayloadCtrl(mThis(self));
    }

static uint32 PwPlaLoPldControlOffset(ThaModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, AtPw pw)
    {
    return mMethodsGet(mThis(self))->LoPayloadCtrlOffset(mThis(self), loOc48Slice, loOc24Slice, AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 PwPlaHoPldControlBaseAddress(ThaModulePwe self)
    {
    return mMethodsGet(mThis(self))->HoPayloadCtrl(mThis(self));
    }

static uint32 PwPlaHoPldControlOffset(ThaModulePwe self, uint32 loOc96Slice, uint32 loOc48Slice, AtPw pw)
    {
    return mMethodsGet(mThis(self))->HoPayloadCtrlOffset(mThis(self), loOc96Slice, loOc48Slice, AtChannelHwIdGet((AtChannel)pw));
    }

static eAtRet LoLineCircuitPwPayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    uint32 address, regVal;

    address = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_, payloadSize);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HoLineCircuitPwPayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    uint32 address, regVal;

    address = PwPlaHoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_, payloadSize);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool HasHoBus(ThaModulePwe self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet((AtModule)self);
    return Tha60210011DeviceHasHoBus(device);
    }

static eBool ShouldUseLoLineLogic(ThaModulePwe self, AtPw pw)
    {
    if (HasHoBus(self))
        return Tha60210011PwCircuitBelongsToLoLine(pw);
    return cAtTrue;
    }

static eAtRet PayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    if (ShouldUseLoLineLogic(self, pw))
        return LoLineCircuitPwPayloadSizeSet(self, pw, payloadSize);

    return HoLineCircuitPwPayloadSizeSet(self, pw, payloadSize);
    }

static uint16 LoLineCircuitPwPayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    uint32 address, regVal;
    uint16 payloadSize;

    address = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    payloadSize = (uint16)mRegField(regVal, cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_);

    return payloadSize;
    }

static uint16 HoLineCircuitPwPayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    uint32 address, regVal;
    uint16 payloadSize;

    address = PwPlaHoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    payloadSize = (uint16)mRegField(regVal, cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_);

    return payloadSize;
    }

static uint16 PayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    if (ShouldUseLoLineLogic(self, pw))
        return LoLineCircuitPwPayloadSizeGet(self, pw);

    return HoLineCircuitPwPayloadSizeGet(self, pw);
    }

static eAtRet LoLineCircuitPwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    uint32 address, regVal;

    address = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_, ThaModulePweHwPwType(self, pw, pwType));
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HoLineCircuitPwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    uint32 address, regVal;

    address = PwPlaHoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_, ThaModulePweHwPwType(self, pw, pwType));
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PlaPwTypeSet(Tha60210011ModulePwe self, AtPw pw, eAtPwType pwType)
    {
    if (ShouldUseLoLineLogic((ThaModulePwe)self, pw))
        return LoLineCircuitPwTypeSet((ThaModulePwe)self, pw, pwType);

    return HoLineCircuitPwTypeSet((ThaModulePwe)self, pw, pwType);
    }

static eAtRet PwePwTypeSet(Tha60210011ModulePwe self, AtPw pw, eAtPwType pwType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset((ThaModulePwe)self, pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[1], cAf6_pla_out_pw_ctrl_PlaOutCepCtr_, (pwType == cAtPwTypeCEP) ? 1 : 0);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    eAtRet ret;

    ret  = mMethodsGet(mThis(self))->PlaPwTypeSet(mThis(self), pw, pwType);
    ret |= mMethodsGet(mThis(self))->PwePwTypeSet(mThis(self), pw, pwType);

    return ret;
    }

static uint32 LoAddRemoveProtocolOffset(Tha60210011ModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, uint32 loPwid)
    {
    AtUnused(self);
    return (loOc48Slice * 65536UL) + (loOc24Slice * 16384UL) + loPwid;
    }

static uint32 LoAddRemoveProtocolReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_lo_add_rmv_pw_ctrl_Base;
    }

static uint32 HoAddRemoveProtocolOffset(Tha60210011ModulePwe self, uint32 hoOc96Slice, uint32 hoOc48Slice, uint32 hoPwid)
    {
    AtUnused(self);
    return (hoOc96Slice * 2048UL) + (hoOc48Slice * 512Ul) + hoPwid;
    }

static uint32 HoAddRemoveProtocolReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_ho_add_rmv_pw_ctrl_Base;
    }

static uint32 PwPlaLoAddRmvPwControlBaseAddress(ThaModulePwe self)
    {
    return mMethodsGet(mThis(self))->LoAddRemoveProtocolReg(mThis(self));
    }

static uint32 PwPlaLoAddRmvPwControlOffset(ThaModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, AtPw pw)
    {
    return mMethodsGet(mThis(self))->LoAddRemoveProtocolOffset(mThis(self), loOc48Slice, loOc24Slice, AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 PwPlaHoAddRmvPwControlBaseAddress(ThaModulePwe self)
    {
    return mMethodsGet(mThis(self))->HoAddRemoveProtocolReg(mThis(self));
    }

static uint32 PwPlaHoAddRmvPwControlOffset(ThaModulePwe self, uint32 loOc96Slice, uint32 loOc48Slice, AtPw pw)
    {
    return mMethodsGet(mThis(self))->HoAddRemoveProtocolOffset(mThis(self), loOc96Slice, loOc48Slice, AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 PDHPWAddingProtocolToPreventBurst(Tha60210011ModulePwe pwe, AtPw pw)
    {
    ThaModulePwe self = (ThaModulePwe)pwe;

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return PwPlaLoAddRmvPwControlBaseAddress(self) + Tha60210011ModulePlaAddRemoveProtocolOffset(self, pw);

    return PwPlaHoAddRmvPwControlBaseAddress(self) + Tha60210011ModulePlaAddRemoveProtocolOffset(self, pw);
    }

static eAtRet PwRamStatusClear(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static uint32 PwControl(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6Reg_pla_lo_pw_ctrl_Base;

    return cAf6Reg_pla_ho_pw_ctrl_Base;
    }

static eAtRet PwCwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);

    if (!enable)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static AtModulePw PwModule(Tha60210011ModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 LoPwControlOffset(Tha60210011ModulePwe self, uint8 loOc48Slice, uint8 loOc24Slice, uint32 pwIdInSlice)
    {
    uint32 oc24Offset;
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)PwModule(self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        oc24Offset = loOc24Slice * 1024UL;
    else
        oc24Offset = loOc24Slice * 2048UL;

    return (loOc48Slice * 65536UL) + oc24Offset + pwIdInSlice + Tha60210011ModulePlaBaseAddress();
    }

static uint32 HoPwControlOffset(uint8 hiOc96Slice, uint8 hiOc48Slice, uint32 pwIdInSlice)
    {
    return (hiOc96Slice * 2048UL) + (hiOc48Slice * 64UL) + pwIdInSlice + Tha60210011ModulePlaBaseAddress();
    }

static uint32 PwControlOffset(ThaModulePwe self, AtPw pw)
    {
    uint8 slice;
    uint32 hwIdInSlice;
    uint8 hiOc96Slice, hiOc48Slice;

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        /* Common converting, slice in lo-line will be 0..11 */
        uint8 loOc48Slice = slice / 2;
        uint8 loOc24Slice = slice % 2;

        return LoPwControlOffset((Tha60210011ModulePwe)self, loOc48Slice, loOc24Slice, AtChannelHwIdGet((AtChannel)pw));
        }

    /* Common converting, slice in hi-line will be 0..7 */
    hiOc96Slice = slice / 2;
    hiOc48Slice = slice % 2;

    return HoPwControlOffset(hiOc96Slice, hiOc48Slice, AtChannelHwIdGet((AtChannel)pw));
    }

static eAtRet PwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset(self, pw);
    uint8 hwEnable = (enable) ? 0 : 1;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_, hwEnable);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    return (mRegField(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_) ? cAtFalse : cAtTrue);
    }

static eAtRet PwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    return mMethodsGet(self)->PwCwAutoTxMBitEnable(self, pw, enable);
    }

static eBool PwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
	return mMethodsGet(self)->PwCwAutoTxMBitIsEnabled(self, pw);
    }

static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    /* Offset need to know circuit, if circuit is NULL, can not enable PW */
    if (AtPwBoundCircuitGet(pw) == NULL)
        {
        if (enable)
            {
            AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                        "%s does not have circuit\r\n",
                        AtObjectToString((AtObject)pw));
            }

        return enable ? cAtErrorNullPointer : cAtOk;
        }

    regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    /* Offset need to know circuit, if circuit is NULL, pw is disabled */
    if (AtPwBoundCircuitGet(pw) == NULL)
        return cAtFalse;

    regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxLBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxLBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxRBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base +  ThaModulePwePwPlaDefaultOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_, mBoolToBin(force));

    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxRBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return (mRegField(longRegVal[0], cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_) ? cAtTrue : cAtFalse);
    }

static eAtRet PwTxMBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    uint8 mBitVal = (force == cAtTrue) ? 0x2 : 0x0;

    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_, mBitVal);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxMBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static uint8 NumLoOc48Slices(ThaModulePwe self)
    {
    AtUnused(self);
    return 6;
    }

static uint8 NumLoOc24SlicesInOc48Slice(ThaModulePwe self)
    {
    AtUnused(self);
    return 2;
    }

static uint16 NumLoPwInSlice(ThaModulePwe self)
    {
    AtUnused(self);
    return 1024;
    }

static eAtRet AllLoCircuitPwDisable(ThaModulePwe self)
    {
    uint8 loOc48Slice_i, loOc24Slice_i;
    uint16 loPwid_i;

    for (loOc48Slice_i = 0; loOc48Slice_i < NumLoOc48Slices(self); loOc48Slice_i++)
        {
        for (loOc24Slice_i = 0; loOc24Slice_i < NumLoOc24SlicesInOc48Slice(self); loOc24Slice_i++)
            {
            for (loPwid_i = 0; loPwid_i < NumLoPwInSlice(self); loPwid_i++)
                mModuleHwWrite(self, cAf6Reg_pla_lo_pw_ctrl_Base + LoPwControlOffset(mThis(self), loOc24Slice_i, loOc24Slice_i, loPwid_i), 0);
            }
        }

    return cAtOk;
    }

static uint8 NumHoOc96Slices(ThaModulePwe self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 NumHoOc48SlicesInOc96Slice(ThaModulePwe self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 NumHoPwInSlice(ThaModulePwe self)
    {
    AtUnused(self);
    return 48;
    }

static eAtRet AllHoCircuitPwDisable(ThaModulePwe self)
    {
    uint8 hoOc96Slice_i, hoOc48Slice_i, hoPwid_i;

    for (hoOc96Slice_i = 0; hoOc96Slice_i < NumHoOc96Slices(self); hoOc96Slice_i++)
        {
        for (hoOc48Slice_i = 0; hoOc48Slice_i < NumHoOc48SlicesInOc96Slice(self); hoOc48Slice_i++)
            {
            for (hoPwid_i = 0; hoPwid_i < NumHoPwInSlice(self); hoPwid_i++)
                mModuleHwWrite(self, cAf6Reg_pla_ho_pw_ctrl_Base + HoPwControlOffset(hoOc96Slice_i, hoOc48Slice_i, hoPwid_i), 0);
            }
        }

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePwe self)
    {
    eAtRet ret = cAtOk;

    ret = AllLoCircuitPwDisable(self);
    if (HasHoBus(self))
        ret |= AllHoCircuitPwDisable(self);

    return ret;
    }

static uint32 ByteMask(uint8 byte_i)
    {
    return (cBit7_0 << (byte_i * 8));
    }

static uint8 ByteShift(uint8 byte_i)
    {
    return (uint8)(byte_i * 8);
    }

static uint32 PutByteToDword(uint8 value, uint8 byte_i)
    {
    return (((uint32)value) << ByteShift(byte_i)) & ByteMask(byte_i);
    }

static AtIpCore Core(ThaModulePwe self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    }

/* Return number dword of data buffer has been written to segment */
static uint8 PutDataToFirstSegment(Tha60210011ModulePwe self, AtPw pw, AtPwPsn psn, uint8 *dataBuffer, uint32* firstSegment, eThaPwAdapterHeaderWriteMode writeMode)
    {
    uint8 numWrittenDword = 0;
    int8 dword_i;
    AtUnused(psn);
    AtUnused(self);
    AtUnused(pw);

    for (dword_i = 3; dword_i >= 0; dword_i--)
        {
        if ((writeMode == cThaPwAdapterHeaderWriteModeDefault) && AtPwRtpIsEnabled(pw))
            {
            if (dword_i == 3)
                {
                firstSegment[dword_i]  = ((uint32)AtPwRtpTxPayloadTypeGet(pw) << 25);
                firstSegment[dword_i] |= AtPwRtpTxSsrcGet(pw) >> 7;
                }

            else if (dword_i == 2)
                firstSegment[dword_i] = ((uint32)AtPwRtpTxSsrcGet(pw) << 25);
            }

        firstSegment[dword_i] = ThaPktUtilPutDataToRegister(dataBuffer, numWrittenDword);
        numWrittenDword = (uint8)(numWrittenDword + 1);
        }

    return numWrittenDword;
    }

static uint32 PsnBufferCtrlReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_psnbuf_ctrl_Base;
    }

/*
 * Format of PSN buffer configured to HW: <rtppt[6:0]> <rtpssrc[31:0]> <PSN>
 *                                        |_________8 bytes___________|
 */
static eAtRet WriteHeaderToBuffer(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode)
    {
    uint32 num16BytesSegment, segment_i, writtenNumDword, writtenNumByte;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byteInDword_i;
    int8 dword_i;
    uint32 regAddr = mMethodsGet(mThis(self))->PsnBufferCtrlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    uint32 numByteToWrite = headerLenInByte;

    numByteToWrite += mMethodsGet(mThis(self))->NumberOfAddtionalByte(mThis(self), pw);

    if (numByteToWrite > cMaxSupportedHeaderLengthInByte)
        {
        mChannelLog(pw, cAtLogLevelCritical, "PSN header is too long");
        return cAtErrorModeNotSupport;
        }

    num16BytesSegment = numByteToWrite / 16;
    writtenNumDword = 0;
    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        if (segment_i == 0)
            {
            writtenNumDword += mMethodsGet(mThis(self))->PutDataToFirstSegment(mThis(self), pw, psn, buffer, longRegVal, writeMode);
            mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));
            continue;
            }

        for (dword_i = 3; dword_i >= 0; dword_i--)
            {
            longRegVal[dword_i] = ThaPktUtilPutDataToRegister(buffer, (uint8)writtenNumDword);
            writtenNumDword = writtenNumDword + 1;
            }

        mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));
        }

    /* Maybe there are still some last bytes if headerLenInByte is not multiple of 16 */
    writtenNumByte = writtenNumDword * 4;
    if (writtenNumByte >= headerLenInByte)
        return cAtOk;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    byteInDword_i = 3;
    dword_i = 4;

    while ((writtenNumByte < headerLenInByte) && (dword_i > 0))
        {
        longRegVal[dword_i - 1] |= PutByteToDword(buffer[writtenNumByte], byteInDword_i);

        writtenNumByte++;
        if (byteInDword_i == 0)
            {
            byteInDword_i = 3;
            dword_i--;
            }
        else
            byteInDword_i = (uint8)(byteInDword_i - 1);
        }

    mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));

    return cAtOk;
    }

static uint32 PsnControlReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_psnpro_ctrl_Base;
    }

static uint32 PsnChannelIdMask(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_Mask;
    }

static uint32 PsnChannelIdShift(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_Shift;
    }

static uint32 PsnPwHwId(Tha60210011ModulePwe self, AtPw adapter)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)adapter);
    }

static eAtRet WaitForHwDone(ThaModulePwe self)
    {
    const uint32 cHeaderProcessTimeOutInMs = 100;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    uint32 regVal = 0;
    uint32 regAddr;

	regAddr = mMethodsGet(mThis(self))->PsnControlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    AtOsalCurTimeGet(&startTime);
    while (elapse < cHeaderProcessTimeOutInMs)
        {
        regVal = mModuleHwRead(self, regAddr);
        
		if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self)))
        	return cAtOk;
        
        if ((regVal & cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorIndrAcsTimeOut;
    }

static eBool CanWaitForHwDone(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet WriteHwRequest(ThaModulePwe self, uint32 channelId, uint8 page, uint8 headerLenInByte)
    {
    const uint8 cWriteCommand = 0;
    const uint8 cRequest = 1;
    uint32 regVal = 0;
    uint32 regAddr = mMethodsGet(mThis(self))->PsnControlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    eAtRet ret;
    uint32 fieldMask, fieldShift;

    mFieldIns(&regVal, mMethodsGet(mThis(self))->PsnChannelIdMask(mThis(self)),
                       mMethodsGet(mThis(self))->PsnChannelIdShift(mThis(self)), channelId);

    fieldMask = mMethodsGet(mThis(self))->PlaOutPsnProPageCtrMask(mThis(self));
    fieldShift = mMethodsGet(mThis(self))->PlaOutPsnProPageCtrShift(mThis(self));
    mRegFieldSet(regVal, field, page);

    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_, headerLenInByte);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_, cWriteCommand);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_, cRequest);

    mModuleHwWrite(self, regAddr, regVal);

    if (!mMethodsGet(mThis(self))->CanWaitForHwDone(mThis(self)))
        return cAtOk;

    ret = WaitForHwDone(self);
    if (ret != cAtOk)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Write request fail, header length: %u bytes, page %u (0x%08X = 0x%08X)\r\n",
                     headerLenInByte, page, regAddr, mModuleHwRead(self, regAddr));

    return ret;
    }

static eAtRet WriteRequest(ThaModulePwe self, AtPw adapter, uint8 page, uint8 headerLenInByte)
    {
    return WriteHwRequest(self, mMethodsGet(mThis(self))->PsnPwHwId(mThis(self), adapter), page, headerLenInByte);
    }

static uint8 NumberOfAddtionalByte(Tha60210011ModulePwe self, AtPw adapter)
    {
    eBool rtpIsEnabled = AtPwRtpIsEnabled(adapter);
    AtUnused(self);

    /* If rtp is enabled, RTP information will be stored in FIRST 8 bytes */
    if (rtpIsEnabled)
        return cNumByteOfRtpStorage;

    return 0;
    }

static eAtRet ReadHeaderFromBuffer(ThaModulePwe self, AtPw adapter, uint8 *buffer, uint8 headerLenInByte)
    {
    const uint8 cNumDwordInSegment = 4;
    uint32 num16BytesSegment, segment_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byte_i, byteInDw_i, numAdditionalByte, numIgnoreDword;
    int8 dword_i;
    uint32 regAddr = mMethodsGet(mThis(self))->PsnBufferCtrlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    uint32 numRequestedByte = headerLenInByte;

    numAdditionalByte = mMethodsGet(mThis(self))->NumberOfAddtionalByte(mThis(self), adapter);
    numRequestedByte  = numRequestedByte + numAdditionalByte;
    numIgnoreDword    = (uint8)(numAdditionalByte / 4);
    num16BytesSegment = numRequestedByte / 16;
    byte_i = 0;

    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
        for (dword_i = 3; dword_i >= 0; dword_i--)
            {
            /* Some first dwords of first segment need to be ignored */
            if (segment_i == 0)
                {
                if (dword_i >= (int8)(cNumDwordInSegment - numIgnoreDword))
                    continue;
                }

            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 24);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 16);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 8);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i]);
            }
        }

    if (byte_i >= headerLenInByte)
        return cAtOk;

    /* Last segment */
    mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
    byteInDw_i = 3;
    dword_i = 3;
    while (byte_i < headerLenInByte)
        {
        buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> ByteShift(byteInDw_i));
        if (byteInDw_i == 0)
            {
            byteInDw_i = 3;
            dword_i--;
            }
        else
            byteInDw_i = (uint8)(byteInDw_i - 1);
        }

    return cAtOk;
    }

static eAtRet ReadRequest(ThaModulePwe self, AtPw adapter, uint8 page, uint8 headerLenInByte)
    {
    const uint8 cReadCommand = 1;
    const uint8 cRequest = 1;
    uint32 regVal = 0;
    uint32 regAddr = mMethodsGet(mThis(self))->PsnControlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    eAtRet ret;
    uint32 pwId = mMethodsGet(mThis(self))->PsnPwHwId(mThis(self), adapter);
    uint32 fieldMask, fieldShift;

    mFieldIns(&regVal, mMethodsGet(mThis(self))->PsnChannelIdMask(mThis(self)),
                       mMethodsGet(mThis(self))->PsnChannelIdShift(mThis(self)), pwId);

    fieldMask = mMethodsGet(mThis(self))->PlaOutPsnProPageCtrMask(mThis(self));
    fieldShift = mMethodsGet(mThis(self))->PlaOutPsnProPageCtrShift(mThis(self));
    mRegFieldSet(regVal, field, page);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_, cReadCommand);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_, cRequest);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_, headerLenInByte);

    mModuleHwWrite(self, regAddr, regVal);

    ret = WaitForHwDone(self);
    if (ret != cAtOk)
        AtChannelLog((AtChannel)adapter, cAtLogLevelCritical, AtSourceLocation, "Read request fail, header length: %u bytes, page %u (0x%08X = 0x%08X)\r\n",
                     headerLenInByte, page, regAddr, mModuleHwRead(self, regAddr));
    return ret;
    }

static eAtRet HelperPwHeaderRead(ThaModulePwe self, AtPw adapter, uint8 page, uint8 *buffer, uint8 headerLenInByte)
    {
    uint32 lengthToRequest = headerLenInByte;

    lengthToRequest += mMethodsGet(mThis(self))->NumberOfAddtionalByte(mThis(self), adapter);

	/* It's not superfluous, request 0 byte will make HW hung */
    if (lengthToRequest == 0)
        return cAtOk;

    if (ReadRequest(self, adapter, page, (uint8)lengthToRequest) != cAtOk)
        mChannelReturn(adapter, cAtErrorIndrAcsTimeOut);

    /* But only get header, ignore RTP */
    return ReadHeaderFromBuffer(self, adapter, buffer, headerLenInByte);
    }

static eAtRet HelperPwHeaderWrite(ThaModulePwe self, AtPw adapter, uint8 page, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, uint32 writeMode)
    {
    eAtRet ret;
    uint32 writeRequestNumByte = headerLenInByte;
    if (WriteHeaderToBuffer(self, adapter, buffer, headerLenInByte, psn, writeMode) != cAtOk)
        return cAtErrorModeNotSupport;

    writeRequestNumByte += mMethodsGet(mThis(self))->NumberOfAddtionalByte(mThis(self), adapter);

	/* It's not superfluous, request 0 byte will make HW hung */
    if (writeRequestNumByte == 0)
        return cAtOk;

    ret = WriteRequest(self, adapter, page, (uint8)writeRequestNumByte);
    if (ret != cAtOk)
        mChannelReturn(adapter, ret);

    return ret;
    }

static eAtRet HeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    return HelperPwHeaderRead(self, pw, cWorkingPsnPage, buffer, bufferSize);
    }

static eAtRet HeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
    AtUnused(psn);
    return HelperPwHeaderWrite(self, pw, cWorkingPsnPage, buffer, headerLenInByte, psn, cThaPwAdapterHeaderWriteModeDefault);
    }

static eAtRet RawHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte)
    {
    return HelperPwHeaderWrite(self, pw, cWorkingPsnPage, buffer, headerLenInByte, NULL, cThaPwAdapterHeaderWriteModeRaw);
    }

static void WriteAllZeroToHeader(ThaModulePwe self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = mMethodsGet(mThis(self))->PsnBufferCtrlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    uint8 segment_i;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    for (segment_i = 0; segment_i < cMaxNumSegment; segment_i++)
        mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));
    }

static uint8 HwPsnType(AtPwPsn psn)
    {
    eAtPwPsnType lowerPsnType;
    if (AtPwPsnTypeGet(psn) == cAtPwPsnTypeUdp)
        {
        lowerPsnType = AtPwPsnTypeGet(AtPwPsnLowerPsnGet(psn));
        if (lowerPsnType == cAtPwPsnTypeIPv4)
            return 1;

        if (lowerPsnType == cAtPwPsnTypeIPv6)
            return 2;
        }

    if (AtPwPsnTypeGet(psn) == cAtPwPsnTypeMpls)
        {
        uint8 numOuterLabel = AtPwMplsPsnNumberOfOuterLabelsGet((AtPwMplsPsn)psn);
        lowerPsnType = AtPwPsnTypeGet(AtPwPsnLowerPsnGet(psn));

        if (lowerPsnType == cAtPwPsnTypeIPv4)
            {
            if (numOuterLabel == 0) return 3;
            if (numOuterLabel == 1) return 5;
            if (numOuterLabel == 2) return 7;
            }

        if (lowerPsnType == cAtPwPsnTypeIPv6)
            {
            if (numOuterLabel == 0) return 4;
            if (numOuterLabel == 1) return 6;
            if (numOuterLabel == 2) return 8;
            }
        }

    return 0;
    }

static eAtRet PwPsnTypeSet(ThaModulePwe self, AtPw pw, AtPwPsn psn)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_, HwPsnType(psn));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet NumVlansSet(ThaModulePwe self, AtPw pw, uint8 numVlans)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_, numVlans);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwRtpEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 rtpEnable = enable ? 1 : 0;

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_, rtpEnable);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PlaModuleHsPwGroupDefaultOffset(ThaModulePwe self, AtPwGroup group)
    {
    AtUnused(self);
    return (uint32)(AtPwGroupIdGet(group) + Tha60210011ModulePlaBaseAddress());
    }

static uint32 PlaModuleUpsrPwGroupDefaultOffset(ThaModulePwe self, uint32 hwGroupId)
    {
    uint32 cNumUpsrPerGroupOf16Uprs = mMethodsGet(mThis(self))->NumGroupsPerRegister(mThis(self));
    return hwGroupId / cNumUpsrPerGroupOf16Uprs + Tha60210011ModulePlaBaseAddress();
    }

static eBool PwGroupingShow(Tha60210011ModulePwe self, AtPw pw)
    {
    eBool pwIsInGroup = cAtFalse;
    AtPwGroup pwGroup = AtPwApsGroupGet(pw);
    ThaModulePwe modulePwe = (ThaModulePwe)self;

    if (pwGroup)
        {
        ThaModulePweRegDisplay(pw, "Payload Assembler Output UPSR Control",
                               mPlaOutUpsrCtrlBase(self) + PlaModuleUpsrPwGroupDefaultOffset(modulePwe, AtPwGroupIdGet(pwGroup)));
        pwIsInGroup = cAtTrue;
        }

    pwGroup = AtPwHsGroupGet(pw);
    if (pwGroup)
        {
        ThaModulePweRegDisplay(pw, "Payload Assembler Output HSPW Control",
                               mPlaOutHspwCtrlBase(self) + PlaModuleHsPwGroupDefaultOffset(modulePwe, pwGroup));
        pwIsInGroup = cAtTrue;
        }

    return pwIsInGroup;
    }

static void PwPlaRegsShow(Tha60210011ModulePwe self, AtPw pw)
    {
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    ThaModulePwe modulePwe = (ThaModulePwe)self;

    if (circuit == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound circuit\r\n");
        return;
        }

    Tha60210011ModulePwCircuitInfoDisplay(pwAdapter);
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        ThaModulePweRegDisplay(pw, "Payload Assembler Low-Order Payload Control",
                               PwPlaLoPldControlBaseAddress(modulePwe) + Tha60210011ModulePlaPldControlOffset(modulePwe, pwAdapter));
        ThaModulePweRegDisplay(pw, "Payload Assembler Low-Order Pseudowire Control",
                               cAf6Reg_pla_lo_pw_ctrl_Base + PwControlOffset(modulePwe, pwAdapter));
        ThaModulePweRegDisplay(pw, "Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control",
                               PwPlaLoAddRmvPwControlBaseAddress(modulePwe) + Tha60210011ModulePlaAddRemoveProtocolOffset(modulePwe, pw));
        }
    else
        {
        ThaModulePweRegDisplay(pw, "Payload Assembler High-Order Payload Control",
                               PwPlaHoPldControlBaseAddress(modulePwe) + Tha60210011ModulePlaPldControlOffset(modulePwe, pwAdapter));
        ThaModulePweRegDisplay(pw, "Payload Assembler High-Order Pseudowire Control",
                               cAf6Reg_pla_ho_pw_ctrl_Base + PwControlOffset(modulePwe, pwAdapter));
        ThaModulePweRegDisplay(pw, "Payload Assembler High-Order Add/Remove Pseudowire Protocol Control",
                               PwPlaHoAddRmvPwControlBaseAddress(modulePwe) + Tha60210011ModulePlaAddRemoveProtocolOffset(modulePwe, pw));
        }

    ThaModulePweLongRegDisplay(pw, "Payload Assembler Output Pseudowire Control",
                               cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset(modulePwe, pwAdapter));
    }

static void PwEthPortRegsShow(ThaModulePwe self, AtPw pw)
    {
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);
    AtEthPort port = AtPwEthPortGet(pw);
    if (port == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound ethernet port\r\n");
        return;
        }

    ThaModulePweRegDisplay(pw, "Pseudowire Transmit Ethernet Header Mode Control",
                           cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pwAdapter));
    }

static uint32 PlaDebugOffset(ThaModulePwe self, AtPw pw)
    {
    uint8 slice;
    uint8 hiOc96Slice;
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        /* Common converting, slice in lo-line will be 0..11 */
        uint8 loOc48Slice = slice / 2;
        return (loOc48Slice * 65536UL) + Tha60210011ModulePlaBaseAddress();
        }

    /* Common converting, slice in hi-line will be 0..7 */
    hiOc96Slice = slice / 2;
    return (hiOc96Slice * 2048UL) + Tha60210011ModulePlaBaseAddress();
    }

static void DebugInforLowOrderClock155(Tha60210011ModulePwe self, AtPw pw)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;

    if (!Tha60210011PwCircuitBelongsToLoLine(pw))
        return;

    regAddres = cAf6Reg_pla_lo_clk155_stk_Base + PlaDebugOffset((ThaModulePwe)self, pw);
    regValue  = mModuleHwRead((ThaModulePwe)self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Low-order Clock#155 information", regAddres, regValue);
    Tha6021DebugPrintInfoBit(debugger, "OC24#0 input CEP Neg Pointer", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#1 input CEP Neg Pointer", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#0 input CEP Pos Pointer", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#1 input CEP Pos Pointer", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC24#0 input AIS", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC24#1 input AIS", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#0 input first-timeslot", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#1 input first-timeslot", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#0 input data from demap", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#1 input data from demap", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#0 input of expected pwid", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC24#1 input of expected pwid", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC24#0 convert 155to233 error", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC24#1 convert 155to233 error", regValue, cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_Mask);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite((ThaModulePwe)self, regAddres, regValue);
    }

static void DebugInforforHighOrderClock311(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return;

    regAddres = cAf6Reg_pla_ho_clk311_stk_Base + PlaDebugOffset(self, pw);
    regValue  = mModuleHwRead(self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* High-order Clock#311 information", regAddres, regValue);
    Tha6021DebugPrintInfoBit(debugger, "OC48#0 input CEP Neg Pointer", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#1 input CEP Neg Pointer", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#0 input CEP Pos Pointer", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#1 input CEP Pos Pointer", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC48#0 input AIS", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC48#1 input AIS", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#0 input first-timeslot", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#1 input first-timeslot", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#0 input data from demap", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#1 input data from demap", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#0 input of expected pwid", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48#1 input of expected pwid", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC48#0 convert 311to233 error", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC48#1 convert 311to233 error", regValue, cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_Mask);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddres, regValue);
    }

static void DebugInforForHighOrderProcess(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return;

    regAddres = cAf6Reg_pla_ho_block_pro_stk_Base + PlaDebugOffset(self, pw);
    regValue  = mModuleHwRead(self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* High-order Processing", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, " OC96 block same", regValue, cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC96 block empty", regValue, cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC96 OC48#1 Convert error", regValue, cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC96 OC48#2 Convert Error", regValue, cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC96 max length violation", regValue, cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC96 packet infor Valid", regValue, cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC96 packet infor Ack", regValue, cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC96 packet infor request", regValue, cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_Mask);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddres, regValue);
    }

static void PwPlaDebug(Tha60210011ModulePwe self, AtPw pw)
    {
    ThaModulePwe module = (ThaModulePwe)self;

    mMethodsGet(self)->DebugInforLowOrderClock155(self, pw);
    DebugInforforHighOrderClock311(module, pw);
    DebugInforForHighOrderProcess(module, pw);
    }

static void PwRegsShow(ThaModulePwe self, AtPw pw)
    {
    mMethodsGet(mThis(self))->PwPlaRegsShow(mThis(self), pw);
    PwEthPortRegsShow(self, pw);

    AtPrintc(cSevInfo, "* Pw grouping information:\r\n");
    if (mMethodsGet(mThis(self))->PwGroupingShow(mThis(self), pw) == cAtFalse)
        AtPrintc(cSevNormal, "        Pw does not join to any group\r\n");

    AtPrintc(cSevInfo, "* Debug information:\r\n");
    mMethodsGet(mThis(self))->PwPlaDebug(mThis(self), pw);
    }

static eAtRet PwSupressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_, mBoolToBin(enable));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwSupressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwControl(self, pw) + PwControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    return mBinToBool(mRegField(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_));
    }

static void PlaPwDefaultHwConfigure(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 regAddr;
    uint32 regVal;

    if ((self == NULL) || (!ThaPwTypeIsCesCep((AtPw)pwAdapter)))
        return;

    regAddr = PwControl((ThaModulePwe)self, (AtPw)pwAdapter) + PwControlOffset((ThaModulePwe)self, (AtPw)pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_, AtChannelIdGet((AtChannel)ThaPwAdapterEthPortGet(pwAdapter)));
    mRegFieldSet(regVal, cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_, AtChannelIdGet((AtChannel)pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);
    }

static eBool IsSimulated(ThaModulePwe self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eBool PwStateIsReady(ThaModulePwe self, AtPw pw, uint32 state)
    {
    const uint32 cTimeoutMs = 100;
    uint32 regAddr = mPDHPWAddingProtocolToPreventBurst(self, pw);
    uint32 regVal  = 0;
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
    AtUnused(self);

    if (IsSimulated(self))
        return cAtTrue;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);

    while (elapseTime <= cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
        if (mRegField(regVal, cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_) == state)
            {
            if (elapseTime > mThis(self)->maxPwStateWaitingTimeMs)
                mThis(self)->maxPwStateWaitingTimeMs = elapseTime;
            return cAtTrue;
            }

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "Cannot wait before removing\r\n");
    return cAtFalse;
    }

static eAtRet PwAddingPrepare(ThaModulePwe self, AtPw pw)
    {
    /*
     * Protocol state to add pw:
     * ==================================
     * Step 1: Add initial or pw idle, the protocol state value is 0
     * Step 2: For adding the pw, CPU will Write 1 value for the protocol state to start adding pw.
     *         The protocol state value is 1.
     * Step 3: CPU enables pw at DEMAP to finish the adding pw process.
     *         HW will "automatically" change the protocol state value to 2 to run the pw, and keep this state.
     */
    uint32 regAddr = mPDHPWAddingProtocolToPreventBurst(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    AtOsal osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));

    mRegFieldSet(longRegVal[0], cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_, cPwStateAdding);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    return cAtOk;
    }

static eAtRet PwAddingStart(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* As described above, do nothing */
    return cAtOk;
    }

static eBool PwRemovingIsReady(ThaModulePwe self, AtPw pw)
    {
    return PwStateIsReady(self, pw, cPwStateIdle);
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    /*
     * Protocol state to remove pw:
     * ==================================
     * Step 1: For removing the pw, CPU will write value 3 for the protocol state to start removing pw.
     *         The protocol state value is 3.
     * Step 2: Poll the protocol state until return value 0 value,
     *         "after" that CPU disables pw at DEMAP to finish the removing pw process.
     */

    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtOsal osal;

    if (!ThaPwAdapterHwIsEnabled((ThaPwAdapter)pw))
        return cAtOk;

    osal = AtSharedDriverOsalGet();
    regAddr = mPDHPWAddingProtocolToPreventBurst(self, pw);
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));

    mRegFieldSet(longRegVal[0], cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_, cPwStateRemoving);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    /* Wait for state to become 0 (step 2) */
    return PwRemovingIsReady(self, pw);
    }

static eAtRet PwRemovingFinish(ThaModulePwe self, AtPw pw)
    {
    /* Re-add PW */
    if (!ThaPwAdapterHwIsEnabled((ThaPwAdapter)pw))
        return cAtOk;

    return PwAddingPrepare(self, pw);
    }

static eAtRet PwRemovingPrepare(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* This protocol is not supported in this product */
    return cAtTrue;
    }

static eBool SupportFullPwCounters(Tha60210011ModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210011DeviceStartVersionSupportFullPwCounters(device))
        return cAtTrue;

    return cAtFalse;
    }

static Tha60210011PwePwCounterGetter PwCounterGetterCreate(Tha60210011ModulePwe self)
    {
    if (SupportFullPwCounters(self))
        return Tha60210011PwePwCounterGetterV2New();

    return Tha60210011PwePwCounterGetterNew();
    }

static Tha60210011PwePwCounterGetter CounterGetter(Tha60210011ModulePwe self)
    {
    if (self->counterGetter == NULL)
    	self->counterGetter = PwCounterGetterCreate(self);

    return self->counterGetter;
    }

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxBytesGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxLbitPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxRbitPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxMbitPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxPbitPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return PwePwCounterGetterTxNbitPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static eAtRet PwHeaderCepEbmEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    /* This product does not support CEP fractional */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static void LowOrderBlockProcessDebug(Tha60210011ModulePwe self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_pla_lo_block_pro_stk_Base + Tha60210011ModulePlaBaseAddress();
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* Low-Order Block Processing", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " Low Size Block same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_Mask);
    Tha6021DebugPrintErrorBit(debugger, " High Size Block same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Low Size Block empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_Mask);
    Tha6021DebugPrintErrorBit(debugger, " High Size Block empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache64 Empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache128 Empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache256 Empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Packet Infor Valid", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache64 same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache128 same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache256 same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Output Valid", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#0 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#1 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#2 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#3 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#4 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#5 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#6 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer bank#7 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux Ack full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer#2 Cache full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer#1 Cache full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Status error", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux write full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux read full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Max legth error", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " All Catch empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_Mask);
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void PlaInterfaceDebug(Tha60210011ModulePwe self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_pla_interface_stk_Base + Tha60210011ModulePlaBaseAddress();
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* PLA Interface", regAddress, regValue);
    Tha6021DebugPrintInfoBit(debugger, "PWE Got packet from PLA port#1", regValue, cAf6_pla_interface_stk_PlaP1PWEGet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#1 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP1PWEReq_Mask);
    Tha6021DebugPrintInfoBit(debugger, "PWE Got packet from PLA port#2", regValue, cAf6_pla_interface_stk_PlaP2PWEGet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#2 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP2PWEReq_Mask);
    Tha6021DebugPrintInfoBit(debugger, "PWE Got packet from PLA port#3", regValue, cAf6_pla_interface_stk_PlaP3PWEGet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#3 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP3PWEReq_Mask);
    Tha6021DebugPrintInfoBit(debugger, "PWE Got packet from PLA port#4", regValue, cAf6_pla_interface_stk_PlaP4PWEGet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#4 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP4PWEReq_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Valid for write data request", regValue, cAf6_pla_interface_stk_PlaWrVldDdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Ack for write data request", regValue, cAf6_pla_interface_stk_PlaWrAckDdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Request write data to DDR", regValue, cAf6_pla_interface_stk_PlaWrReqDdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Valid for read data request", regValue, cAf6_pla_interface_stk_PlaRdVldDdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Ack for read data request", regValue, cAf6_pla_interface_stk_PlaRdAckDdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Request read data to DDR", regValue, cAf6_pla_interface_stk_PlaRdReqDdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Valid for pkt info request", regValue, cAf6_pla_interface_stk_PlaPkInfVldQdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Ack for pkt info request", regValue, cAf6_pla_interface_stk_PlaPkInfAckQdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Request Pkt info to QDR", regValue, cAf6_pla_interface_stk_PlaPkInfReqQdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Valid for PSN request", regValue, cAf6_pla_interface_stk_PlaPSNVldQdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Ack for PSN request", regValue, cAf6_pla_interface_stk_PlaPSNAckQdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Request PSN to QDR", regValue, cAf6_pla_interface_stk_PlaPSNReqQdr_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#1 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort1QueAck_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#1 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort1QueRdy_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#2 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort2QueAck_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#2 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort2QueRdy_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#3 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort3QueAck_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#3 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort3QueRdy_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#4 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort4QueAck_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Port#4 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort4QueRdy_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PWE interface end packet error", regValue, cAf6_pla_interface_stk_PlaP1PWEEoPErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Request read pkt info len error", regValue, cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Request write pkt info len error", regValue, cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_Mask);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void LowOrderBlockProcessDebugEntriesFill(AtModule self, AtDebugger debugger)
    {
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_pla_lo_block_pro_stk_Base + Tha60210011ModulePlaBaseAddress();
    regValue   = mModuleHwRead(self, regAddress);

    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Low Size Block same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("High Size Block same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Low Size Block empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("High Size Block empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Cache64 Empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Cache128 Empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Cache256 Empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Packet Infor Valid", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Cache64 same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Cache128 same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Cache256 same", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Output Valid", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#0 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#1 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#2 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#3 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#4 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#5 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#6 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer bank#7 full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Mux Ack full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer#2 Cache full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Buffer#1 Cache full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Status error", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Mux write full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Mux read full", regValue, cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Max legth error", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("All Catch empty", regValue, cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_Mask));

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void PlaInterfaceDebugEntriesFill(AtModule self, AtDebugger debugger)
    {
    uint32 regAddress = cAf6Reg_pla_interface_stk_Base + Tha60210011ModulePlaBaseAddress();
    uint32 regValue = mModuleHwRead(self, regAddress);

    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("PWE Got packet from PLA port#1", regValue, cAf6_pla_interface_stk_PlaP1PWEGet_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#1 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP1PWEReq_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("PWE Got packet from PLA port#2", regValue, cAf6_pla_interface_stk_PlaP2PWEGet_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#2 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP2PWEReq_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("PWE Got packet from PLA port#3", regValue, cAf6_pla_interface_stk_PlaP3PWEGet_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#3 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP3PWEReq_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("PWE Got packet from PLA port#4", regValue, cAf6_pla_interface_stk_PlaP4PWEGet_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#4 PLA ready packet for PWE", regValue, cAf6_pla_interface_stk_PlaP4PWEReq_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Valid for write data request", regValue, cAf6_pla_interface_stk_PlaWrVldDdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Ack for write data request", regValue, cAf6_pla_interface_stk_PlaWrAckDdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Request write data to DDR", regValue, cAf6_pla_interface_stk_PlaWrReqDdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Valid for read data request", regValue, cAf6_pla_interface_stk_PlaRdVldDdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Ack for read data request", regValue, cAf6_pla_interface_stk_PlaRdAckDdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Request read data to DDR", regValue, cAf6_pla_interface_stk_PlaRdReqDdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Valid for pkt info request", regValue, cAf6_pla_interface_stk_PlaPkInfVldQdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Ack for pkt info request", regValue, cAf6_pla_interface_stk_PlaPkInfAckQdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Request Pkt info to QDR", regValue, cAf6_pla_interface_stk_PlaPkInfReqQdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Valid for PSN request", regValue, cAf6_pla_interface_stk_PlaPSNVldQdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Ack for PSN request", regValue, cAf6_pla_interface_stk_PlaPSNAckQdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Request PSN to QDR", regValue, cAf6_pla_interface_stk_PlaPSNReqQdr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#1 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort1QueAck_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#1 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort1QueRdy_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#2 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort2QueAck_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#2 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort2QueRdy_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#3 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort3QueAck_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#3 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort3QueRdy_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#4 output Queue Pkt ack", regValue, cAf6_pla_interface_stk_PlaPort4QueAck_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew("Port#4 output Queue Pkt ready", regValue, cAf6_pla_interface_stk_PlaPort4QueRdy_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("PWE interface end packet error", regValue, cAf6_pla_interface_stk_PlaP1PWEEoPErr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Request read pkt info len error", regValue, cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_Mask));
    AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew("Request write pkt info len error", regValue, cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_Mask));

    /* Clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void ProfileInfoDisplay(ThaModulePwe self)
    {
    m_ThaModulePweMethods->ProfileInfoDisplay(self);

    AtPrintc(cSevNormal, "* Max PW state waiting time: %d (ms)\r\n", mThis(self)->maxPwStateWaitingTimeMs);
    mThis(self)->maxPwStateWaitingTimeMs = 0;
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);

    mMethodsGet(mThis(self))->LowOrderBlockProcessDebug(mThis(self));
    mMethodsGet(mThis(self))->PlaInterfaceDebug(mThis(self));
    AtPrintc(cSevNormal, "\r\n");

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsPlaRegister(AtModuleDeviceGet(self), address) ||
           Tha60210011IsPweRegister(AtModuleDeviceGet(self), address);
    }

static uint16 NumFramesInPacketGet(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (uint16)mRegField(regVal, cPayloadSizeNumFrame);
    }

static eAtRet PwNumDs0TimeslotsSet(ThaModulePwe self, AtPw pw, uint16 numTimeslots)
    {
    uint32 regAddr = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cPayloadSizeNumTimeslot, numTimeslots);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint8 PwNumDs0TimeslotsGet(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (uint8)mRegField(regVal, cPayloadSizeNumTimeslot);
    }

static eBool NumFramesInPacketIsInRange(uint16 numFramesInPacket)
    {
    uint32 maxNumFrames;
    maxNumFrames = cPayloadSizeNumFrameMask >> cPayloadSizeNumFrameShift;
    return (numFramesInPacket <= maxNumFrames) ? cAtTrue : cAtFalse;
    }

static eAtRet PwNumFramesInPacketSet(ThaModulePwe self, AtPw pw, uint16 numFramesInPacket)
    {
    uint32 regAddr, regVal;

    if (!NumFramesInPacketIsInRange(numFramesInPacket))
        return cAtErrorOutOfRangParm;

    regAddr = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cPayloadSizeNumFrame, numFramesInPacket);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if (((localAddress >= 0x0400000) && (localAddress <= 0x04FFFFF)) ||
        ((localAddress >= 0x0070000) && (localAddress <= 0x00AFFFF)))
        return cAtTrue;

    return cAtFalse;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "LO PLA Payload Control Register of OC48#0 OC#24#0",
         "LO PLA Payload Control Register of OC48#0 OC#24#1",
         "LO PWE Control Register of OC48#0",
         "LO PLA Payload Control Register of OC48#1 OC#24#0",
         "LO PLA Payload Control Register of OC48#1 OC#24#1",
         "LO PWE Control Register of OC48#1",
         "LO PLA Payload Control Register of OC48#2 OC#24#0",
         "LO PLA Payload Control Register of OC48#2 OC#24#1",
         "LO PWE Control Register of OC48#2",
         "LO PLA Payload Control Register of OC48#3 OC#24#0",
         "LO PLA Payload Control Register of OC48#3 OC#24#1",
         "LO PWE Control Register of OC48#3",
         "HO PLA Payload Control Register of OC96#0 OC#48#0",
         "HO PLA Payload Control Register of OC96#0 OC#48#1",
         "HO PWE Control Register of OC96#0",
         "HO PLA Payload Control Register of OC96#1 OC#48#0",
         "HO PLA Payload Control Register of OC96#1 OC#48#1",
         "HO PWE Control Register of OC96#1",
         "PLA Output PWE Port1 Control Register",
         "PLA UPSR Control Register",
         "PLA HSPW Control Register",
         sTha60210011ModulePweRamPLAOutputPWEPort2ControlRegister,
         "PLA CRC Error",
         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 0",
         sTha60210011ModulePweRamPWEPseudowireTransmitEthernetHeaderModeControlslice1,
         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 2",
         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 3"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static uint32 StartCrcRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 22;
    }

static uint32 StartHSPWRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 18;
    }

static uint32 StartHoPlaRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 12;
    }

static uint32 StartPweRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 23;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < mMethodsGet(mThis(self))->StartPweRamLocalId(mThis(self)))
        return Tha60210011InternalRamPlaNew(self, ramId, localRamId);

    return Tha60210011InternalRamPweNew(self, ramId, localRamId);
    }

static void PrintDword(uint32 dword)
    {
    AtPrintc(cSevNormal, "  %02x", (uint8)(dword >> 24));
    AtPrintc(cSevNormal, "  %02x", (uint8)(dword >> 16));
    AtPrintc(cSevNormal, "  %02x", (uint8)(dword >> 8));
    AtPrintc(cSevNormal, "  %02x", (uint8)(dword));
    }

static eAtRet ShowHeaderFromBuffer(ThaModulePwe self, AtPw pw, uint8 headerLenInByte)
    {
    uint32 num16BytesSegment, segment_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byte_i, byteInDw_i, numAdditionalByte;
    int8 dword_i;
    uint32 regAddr = mMethodsGet(mThis(self))->PsnBufferCtrlReg(mThis(self)) + Tha60210011ModulePlaBaseAddress();
    uint32 numRequestedByte = headerLenInByte;

    numAdditionalByte = mMethodsGet(mThis(self))->NumberOfAddtionalByte(mThis(self), pw);
    numRequestedByte  = numRequestedByte + numAdditionalByte;
    num16BytesSegment = numRequestedByte / 16;
    byte_i = 0;

    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
        for (dword_i = 3; dword_i >= 0; dword_i--)
            PrintDword(longRegVal[dword_i]);

        AtPrintc(cSevNormal, "\r\n");
        byte_i = (uint8)(byte_i + 16);
        }

    if (byte_i >= numRequestedByte)
        return cAtOk;

    /* Last segment */
    mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
    byteInDw_i = 3;
    dword_i = 3;
    while (byte_i < numRequestedByte)
        {
        AtPrintc(cSevNormal, "  %02x", (uint8)(longRegVal[dword_i] >> ByteShift(byteInDw_i)));
        byte_i++;
        if (byteInDw_i == 0)
            {
            byteInDw_i = 3;
            dword_i--;
            }
        else
            byteInDw_i = (uint8)(byteInDw_i - 1);
        }

    AtPrintc(cSevNormal, "\r\n");
    return cAtOk;
    }

static eAtRet PwHeaderShow(ThaModulePwe self, AtPw adapter, uint8 page, uint8 headerLenInByte)
    {
    uint32 lengthToRequest = headerLenInByte;

    lengthToRequest += mMethodsGet(mThis(self))->NumberOfAddtionalByte(mThis(self), adapter);

    /* It 's not superfluous, request 0 byte will make HW hung */
    if (lengthToRequest == 0)
        return cAtOk;

    if (ReadRequest(self, adapter, page, (uint8)lengthToRequest) != cAtOk)
        mChannelReturn(adapter, cAtErrorIndrAcsTimeOut);

    /* But only get header, ignore RTP */
    return ShowHeaderFromBuffer(self, adapter, headerLenInByte);
    }

static void PwDebug(ThaModulePwe self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    uint8 headerLengthInByte = ThaPwHeaderControllerHeaderLengthGet(ThaPwAdapterHeaderController(adapter));

    AtUnused(self);
    AtPrintc(cSevInfo, "* Header length: ");
    AtPrintc(cSevNormal, "%u bytes (Not include additional bytes)\r\n", headerLengthInByte);
    AtPrintc(cSevInfo, "* Working header buffer:\r\n");
    PwHeaderShow(self, (AtPw)adapter, cWorkingPsnPage, headerLengthInByte);
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Backup header buffer:\r\n");
    PwHeaderShow(self, (AtPw)adapter, cBackupPsnPage, headerLengthInByte);
    }

static uint32 GroupLabelSetSw2Hw(eAtPwGroupLabelSet labelSet)
    {
    return (labelSet == cAtPwGroupLabelSetPrimary) ? 0 : 1;
    }

static eAtPwGroupLabelSet GroupLabelSetHw2Sw(uint32 labelSet)
    {
    return (labelSet == 0) ? cAtPwGroupLabelSetPrimary : cAtPwGroupLabelSetBackup;
    }

static eAtRet ApsGroupPwHwAdd(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset((ThaModulePwe)self, pwAdapter);

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_, 1);
    mRegFieldSet(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_, pwGroupHwId);

    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eBool ApsGroupPwIsInHwGroup(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + mMethodsGet((ThaModulePwe)self)->PwPlaDefaultOffset((ThaModulePwe)self, pwAdapter);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 isInGroup, hwGroupId;

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    isInGroup = mRegField(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_);
    hwGroupId = mRegField(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_);

    if ((hwGroupId == pwGroupHwId) && (isInGroup == 1))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ApsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset((ThaModulePwe)self, pwAdapter);

    AtUnused(pwGroup);

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_, 0);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset((ThaModulePwe)self, pwAdapter);
    uint32 groupId = AtPwGroupIdGet(pwGroup);
    uint32 headGroupId = groupId >> 10;
    uint32 tailGroupId = groupId & cBit9_0;

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_, 1);
    mFieldIns(&longRegVal[cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Head_DwIndex],
              cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Mask_02,
              cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Shift_02,
              headGroupId);
    mFieldIns(&longRegVal[cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Tail_DwIndex],
              cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Mask_01,
              cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Shift_01,
              tailGroupId);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_pla_out_pw_ctrl_Base + ThaModulePwePwPlaDefaultOffset((ThaModulePwe)self, pwAdapter);

    AtUnused(pwGroup);

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);
    mRegFieldSet(longRegVal[cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_DwIndex], cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_, 0);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eAtRet ApsGroupHwEnable(ThaModulePweV2 self, uint32 pwGroupHwId, eBool enable)
    {
    uint32 regAddr = mPlaOutUpsrCtrlBase(self) + PlaModuleUpsrPwGroupDefaultOffset((ThaModulePwe)self, pwGroupHwId);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 cNumUpsrPerGroupOf16Uprs = mMethodsGet(mThis(self))->NumGroupsPerRegister(mThis(self));
    uint32 enableFieldShift = pwGroupHwId % cNumUpsrPerGroupOf16Uprs;
    uint32 enableFieldMask  = cBit0 << enableFieldShift;

    mRegFieldSet(regVal, enableField, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ApsGroupHwIsEnabled(ThaModulePweV2 self, uint32 pwGroupHwId)
    {
    uint32 regAddr = mPlaOutUpsrCtrlBase(self) + PlaModuleUpsrPwGroupDefaultOffset((ThaModulePwe)self, pwGroupHwId);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 cNumUpsrPerGroupOf16Uprs = mMethodsGet(mThis(self))->NumGroupsPerRegister(mThis(self));
    uint32 enableFieldShift = pwGroupHwId % cNumUpsrPerGroupOf16Uprs;
    uint32 enableFieldMask  = cBit0 << enableFieldShift;

    return mBinToBool(mRegField(regVal, enableField));
    }

static eAtRet HsGroupLabelSetSelect(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    uint32 regAddr = mPlaOutHspwCtrlBase(self) + PlaModuleHsPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_, GroupLabelSetSw2Hw(labelSet));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupSelectedLabelSetGet(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = mPlaOutHspwCtrlBase(self) + PlaModuleHsPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    return GroupLabelSetHw2Sw(mRegField(regVal, cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_));
    }

static eAtRet HsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    uint32 regAddr = mPlaOutHspwCtrlBase(self) + PlaModuleHsPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool HsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = mPlaOutHspwCtrlBase(self) + PlaModuleHsPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    return mBinToBool(mRegField(regVal, cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_));
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->counterGetter));
    mThis(self)->counterGetter = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011ModulePwe object = (Tha60210011ModulePwe)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(maxPwStateWaitingTimeMs);
    mEncodeObject(counterGetter);
    }

static uint32 PortCounterOffset(Tha60210011ModulePwe self, AtEthPort port, eBool r2c)
    {
    uint32 pweModuleOffset = Tha60210011ModulePweModuleOffsetByEthernetPort((ThaModulePwe)self, port);
    return ((r2c) ? 0x800 : 0x0) + pweModuleOffset;
    }

static uint32 StartVersionControlJitterAttenuator(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x1072);
    }

static eBool JitterAttenuatorIsSupported(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= Tha60210011ModulePweStartVersionControlJitterAttenuator(self))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LoLinePayloadTimeStampSourceSet(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address, regVal;
    uint32 hwEnableVal = 0;

    /* HW recommends only enable Tx Shaper when run SAToP */
    if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
        hwEnableVal = mBoolToBin(enable);

    address = PwPlaLoPldControlBaseAddress(self) + Tha60210011ModulePlaPldControlOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cLoPayloadTimeStampSource, hwEnableVal);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet PwJitterAttenuatorEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (JitterAttenuatorIsSupported(self) == cAtFalse)
        return cAtOk;

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return LoLinePayloadTimeStampSourceSet(self, pw, enable);

    return cAtOk;
    }

static uint32 MaxChannelHeader(Tha60210011ModulePwe self)
    {
    return AtModulePwMaxPwsGet(PwModule(self));
    }

static eAtRet AllPwsHeaderFlush(ThaModulePwe self)
    {
    eAtRet ret = cAtOk;
    uint32 headerId, numHeaderChannel = mMethodsGet(mThis(self))->MaxChannelHeader(mThis(self));

    WriteAllZeroToHeader(self);
    for (headerId = 0; headerId < numHeaderChannel; headerId++)
        {
        ret = WriteHwRequest(self, headerId, cWorkingPsnPage, (uint8)cMaxSupportedHeaderLengthInByte);
        ret |= WriteHwRequest(self, headerId, cBackupPsnPage, (uint8)cMaxSupportedHeaderLengthInByte);

        if (ret != cAtOk)
            return ret;
        }

    return ret;
    }

static uint32 BaseAddress(ThaModulePwe self)
    {
    AtUnused(self);
    return Tha60210011ModulePweBaseAddress();
    }

static uint32 PlaBaseAddress(ThaModulePwe self)
    {
    AtUnused(self);
    return Tha60210011ModulePlaBaseAddress();
    }

static uint32 PlaPldControlOffset(Tha60210011ModulePwe pwe, AtPw pw)
    {
    uint8 slice;
    uint8 hiOc96Slice, hiOc48Slice;
    uint32 addressOffset;
    ThaModulePwe self = (ThaModulePwe)pwe;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        /* Common converting, slice in lo-line will be 0..11 */
        uint8 loOc48Slice = slice / 2;
        uint8 loOc24Slice = slice % 2;
        addressOffset = PwPlaLoPldControlOffset(self, loOc48Slice, loOc24Slice, pw);
        return addressOffset + Tha60210011ModulePlaBaseAddress();
        }

    /* Common converting, slice in hi-line will be 0..7 */
    hiOc96Slice = slice / 2;
    hiOc48Slice = slice % 2;
    addressOffset = PwPlaHoPldControlOffset(self, hiOc96Slice, hiOc48Slice, pw);
    return addressOffset + Tha60210011ModulePlaBaseAddress();
    }

static uint32 PlaOutPsnProPageCtrMask(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask;
    }

static uint32 PlaOutPsnProPageCtrShift(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift;
    }

static uint32 PlaOutUpsrCtrlBase(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_upsr_ctrl_Base;
    }

static uint32 PlaOutHspwCtrlBase(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_hspw_ctrl_Base;
    }

static void DebuggerEntriesFill(AtModule self, AtDebugger debugger)
    {
    LowOrderBlockProcessDebugEntriesFill(self, debugger);
    PlaInterfaceDebugEntriesFill(self, debugger);
    }

static uint32 AbsolutePwIdGet(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId)
    {
    /* This function may need to override if want to restore absolute PW ID from local PW ID */
    AtUnused(self);
    AtUnused(oc48Slice);
    AtUnused(localPwId);
    return cInvalidUint32;
    }

static uint32 NumGroupsPerRegister(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 16;
    }

static void MethodsInit(Tha60210011ModulePwe self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override methods */
        mMethodOverride(m_methods, NumberOfAddtionalByte);
        mMethodOverride(m_methods, PutDataToFirstSegment);
        mMethodOverride(m_methods, LoPayloadCtrlOffset);
        mMethodOverride(m_methods, LoPayloadCtrl);
        mMethodOverride(m_methods, HoPayloadCtrlOffset);
        mMethodOverride(m_methods, HoPayloadCtrl);
        mMethodOverride(m_methods, AbsolutePwIdGet);

        mMethodOverride(m_methods, LoAddRemoveProtocolOffset);
        mMethodOverride(m_methods, LoAddRemoveProtocolReg);
        mMethodOverride(m_methods, HoAddRemoveProtocolOffset);
        mMethodOverride(m_methods, HoAddRemoveProtocolReg);

        mMethodOverride(m_methods, PsnControlReg);
        mMethodOverride(m_methods, PsnChannelIdMask);
        mMethodOverride(m_methods, PsnChannelIdShift);
        mMethodOverride(m_methods, PsnPwHwId);
        mMethodOverride(m_methods, MaxChannelHeader);

        mMethodOverride(m_methods, PsnBufferCtrlReg);
        mMethodOverride(m_methods, PlaPwDefaultHwConfigure);

        mMethodOverride(m_methods, PwPlaRegsShow);
        mMethodOverride(m_methods, PwGroupingShow);
        mMethodOverride(m_methods, PwPlaDebug);
        mMethodOverride(m_methods, LowOrderBlockProcessDebug);
        mMethodOverride(m_methods, PlaInterfaceDebug);

        mMethodOverride(m_methods, PortCounterOffset);

        mMethodOverride(m_methods, StartCrcRamLocalId);
        mMethodOverride(m_methods, StartHSPWRamLocalId);
        mMethodOverride(m_methods, StartHoPlaRamLocalId);
        mMethodOverride(m_methods, StartPweRamLocalId);

        mMethodOverride(m_methods, StartVersionControlJitterAttenuator);
        mMethodOverride(m_methods, CounterGetter);
        mMethodOverride(m_methods, PwePwTypeSet);
        mMethodOverride(m_methods, PlaPwTypeSet);
        mMethodOverride(m_methods, PlaOutUpsrCtrlBase);
        mMethodOverride(m_methods, PlaOutHspwCtrlBase);
        mMethodOverride(m_methods, PlaOutPsnProPageCtrMask);
        mMethodOverride(m_methods, PlaOutPsnProPageCtrShift);
        mMethodOverride(m_methods, PlaPldControlOffset);
        mMethodOverride(m_methods, PDHPWAddingProtocolToPreventBurst);
        mMethodOverride(m_methods, DebugInforLowOrderClock155);
        mMethodOverride(m_methods, CanWaitForHwDone);
        mMethodOverride(m_methods, NumGroupsPerRegister);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, DebuggerEntriesFill);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PayloadSizeSet);
        mMethodOverride(m_ThaModulePweOverride, PayloadSizeGet);
        mMethodOverride(m_ThaModulePweOverride, PwTypeSet);
        mMethodOverride(m_ThaModulePweOverride, DefaultSet);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingFinish);
        mMethodOverride(m_ThaModulePweOverride, PwRamStatusClear);
        mMethodOverride(m_ThaModulePweOverride, PwCwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, HeaderRead);
        mMethodOverride(m_ThaModulePweOverride, HeaderWrite);
        mMethodOverride(m_ThaModulePweOverride, RawHeaderWrite);
        mMethodOverride(m_ThaModulePweOverride, PwRtpEnable);
        mMethodOverride(m_ThaModulePweOverride, NumVlansSet);
        mMethodOverride(m_ThaModulePweOverride, PwPsnTypeSet);
        mMethodOverride(m_ThaModulePweOverride, PwRegsShow);
        mMethodOverride(m_ThaModulePweOverride, PwSupressEnable);
        mMethodOverride(m_ThaModulePweOverride, PwSupressIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwAddingPrepare);
        mMethodOverride(m_ThaModulePweOverride, PwAddingStart);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingPrepare);
        mMethodOverride(m_ThaModulePweOverride, ProfileInfoDisplay);

        mMethodOverride(m_ThaModulePweOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwHeaderCepEbmEnable);

        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitIsEnabled);

        mMethodOverride(m_ThaModulePweOverride, NumFramesInPacketGet);
        mMethodOverride(m_ThaModulePweOverride, PwNumDs0TimeslotsSet);
        mMethodOverride(m_ThaModulePweOverride, PwNumDs0TimeslotsGet);
        mMethodOverride(m_ThaModulePweOverride, PwNumFramesInPacketSet);
        mMethodOverride(m_ThaModulePweOverride, PwDebug);
        mMethodOverride(m_ThaModulePweOverride, PwJitterAttenuatorEnable);
        mMethodOverride(m_ThaModulePweOverride, BaseAddress);
        mMethodOverride(m_ThaModulePweOverride, PlaBaseAddress);
        mMethodOverride(m_ThaModulePweOverride, PwPlaDefaultOffset);
        mMethodOverride(m_ThaModulePweOverride, PwPweDefaultOffset);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweV2Methods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, m_ThaModulePweV2Methods, sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwIsInHwGroup);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwHwAdd);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwAdd);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupHwEnable);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupHwIsEnabled);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupLabelSetSelect);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupSelectedLabelSetGet);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupEnable);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupIsEnabled);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    OverrideThaModulePweV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModulePwe);
    }

AtModule Tha60210011ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwModulePweV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModulePweObjectInit(newModule, device);
    }

eAtRet Tha60210011ModulePweApsGroupPwAdd(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return ThaModulePweV2ApsGroupPwAdd((ThaModulePweV2)self, pwGroup, pwAdapter);
    }

eAtRet Tha60210011ModulePweApsGroupPwRemove(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return ThaModulePweV2ApsGroupPwRemove((ThaModulePweV2)self, pwGroup, pwAdapter);
    }

eAtRet Tha60210011ModulePweHsGroupPwAdd(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return ThaModulePweV2HsGroupPwAdd((ThaModulePweV2)self, pwGroup, pwAdapter);
    }

eAtRet Tha60210011ModulePweHsGroupPwRemove(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    return ThaModulePweV2HsGroupPwRemove((ThaModulePweV2)self, pwGroup, pwAdapter);
    }

eAtRet Tha60210011ModulePweApsGroupEnable(ThaModulePwe self, AtPwGroup pwGroup, eBool enable)
    {
    return ThaModulePweV2ApsGroupEnable((ThaModulePweV2)self, pwGroup, enable);
    }

eBool Tha60210011ModulePweApsGroupIsEnabled(ThaModulePwe self, AtPwGroup pwGroup)
    {
    return ThaModulePweV2ApsGroupIsEnabled((ThaModulePweV2)self, pwGroup);
    }

eAtRet Tha60210011ModulePweHsGroupEnable(ThaModulePwe self, AtPwGroup pwGroup, eBool enable)
    {
    return ThaModulePweV2HsGroupEnable((ThaModulePweV2)self, pwGroup, enable);
    }

eBool Tha60210011ModulePweHsGroupIsEnabled(ThaModulePwe self, AtPwGroup pwGroup)
    {
    return ThaModulePweV2HsGroupIsEnabled((ThaModulePweV2)self, pwGroup);
    }

eAtRet Tha60210011ModulePweHsGroupLabelSetSelect(ThaModulePwe self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    return ThaModulePweV2HsGroupLabelSetSelect((ThaModulePweV2)self, pwGroup, labelSet);
    }

eAtRet Tha60210011ModulePweHsGroupSelectedLabelSetGet(ThaModulePwe self, AtPwGroup pwGroup)
    {
    return ThaModulePweV2HsGroupSelectedLabelSetGet((ThaModulePweV2)self, pwGroup);
    }

eAtRet Tha60210011ModulePwePwBackupHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn)
    {
    if (self)
        return HelperPwHeaderWrite(self, pw, cBackupPsnPage, buffer, headerLenInByte, psn, cThaPwAdapterHeaderWriteModeDefault);

    return cAtError;
    }

eAtRet Tha60210011ModulePwePwBackupHeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    if (self)
        return HelperPwHeaderRead(self, pw, cBackupPsnPage, buffer, bufferSize);

    return cAtError;
    }

uint32 Tha60210011ModulePweModuleOffsetByEthernetPort(ThaModulePwe self, AtEthPort port)
    {
    AtUnused(self);
    return 0x10000 * AtChannelIdGet((AtChannel)port) + Tha60210011ModulePweBaseAddress();
    }

void Tha60210011ModulePwePwDefaultHwConfigure(ThaModulePwe self, ThaPwAdapter pwAdapter)
    {
    mMethodsGet(mThis(self))->PlaPwDefaultHwConfigure(mThis(self), pwAdapter);
    }

uint32 Tha60210011ModulePlaBaseAddress(void)
    {
    return 0x400000;
    }

uint8 Tha60210011ModulePweNumberOfAddtionalByte(Tha60210011ModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->NumberOfAddtionalByte(self, pw);

    return 0;
    }

eAtRet Tha60210011ModulePwePwHeaderFlush(ThaModulePwe self, AtPw pw)
    {
    eAtRet ret;

    WriteAllZeroToHeader(self);
    ret = WriteRequest(self, pw, cWorkingPsnPage, (uint8)cMaxSupportedHeaderLengthInByte);
    ret |= WriteRequest(self, pw, cBackupPsnPage, (uint8)cMaxSupportedHeaderLengthInByte);
    if (ret != cAtOk)
        mChannelReturn(pw, ret);

    return ret;
    }

eAtRet Tha60210011PlaLoLineCircuitPwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    return LoLineCircuitPwTypeSet(self, pw, pwType);
    }

eAtRet Tha60210011ModulePweAllPwsHeaderFlush(ThaModulePwe self)
    {
    return AllPwsHeaderFlush(self);
    }

uint32 Tha60210011ModulePlaPldControlOffset(ThaModulePwe self, AtPw pw)
    {
    if (self)
        return mMethodsGet(mThis(self))->PlaPldControlOffset(mThis(self), pw);
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePlaAddRemoveProtocolOffset(ThaModulePwe self, AtPw pw)
    {
    uint8 slice;
    uint8 hiOc96Slice, hiOc48Slice;
    uint32 addressOffset;
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        /* Common converting, slice in lo-line will be 0..11 */
        uint8 loOc48Slice = slice / 2;
        uint8 loOc24Slice = slice % 2;
        addressOffset = PwPlaLoAddRmvPwControlOffset(self, loOc48Slice, loOc24Slice, pw);
        return addressOffset + Tha60210011ModulePlaBaseAddress();
        }

    /* Common converting, slice in hi-line will be 0..7 */
    hiOc96Slice = slice / 2;
    hiOc48Slice = slice % 2;
    addressOffset = PwPlaHoAddRmvPwControlOffset(self, hiOc96Slice, hiOc48Slice, pw);
    return addressOffset + Tha60210011ModulePlaBaseAddress();
    }

uint32 Tha60210012ModulePwePortCounterOffset(Tha60210011ModulePwe self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PortCounterOffset(self, port, r2c);
    return 0;
    }

uint32 Tha60210011ModulePweBaseAddress(void)
    {
    return 0x70000;
    }

uint32 Tha60210011ModulePweStartCrcRamLocalId(Tha60210011ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->StartCrcRamLocalId(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePweStartHSPWRamLocalId(Tha60210011ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->StartHSPWRamLocalId(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePweStartHoPlaRamLocalId(Tha60210011ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->StartHoPlaRamLocalId(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePweStartPweRamLocalId(Tha60210011ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->StartPweRamLocalId(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePweStartVersionControlJitterAttenuator(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionControlJitterAttenuator(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePweAbsolutePwIdGet(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId)
    {
    if (self)
        return mMethodsGet(self)->AbsolutePwIdGet(self, oc48Slice, localPwId);
    return cInvalidUint32;
    }

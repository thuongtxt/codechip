/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_PLA_H_
#define _AF6_REG_AF6CCI0011_RD_PLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x0_2000-0x5_63FF
Reg Formula: 0x0_2000 + $LoOc48Slice*65536 + $LoOc24Slice*16384 + $LoPwid
    Where  : 
           + $LoOc48Slice(0-5): LO OC-48 slices, there are total 6xOC-48 slices for LO 15G
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
           + $LoPwid(0-1023): pseudowire channel for each LO OC-24 slice
Reg Desc   : 
This register is used to configure payload in each LO Pseudowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_pld_ctrl_Base                                                                   0x02000
#define cAf6Reg_pla_lo_pld_ctrl(LoOc48Slice, LoOc24Slice, LoPwid)     (0x02000+(LoOc48Slice)*65536+(LoOc24Slice)*16384+(LoPwid))
#define cAf6Reg_pla_lo_pld_ctrl_WidthVal                                                                    32
#define cAf6Reg_pla_lo_pld_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PlaLoPldTypeCtrl
BitField Type: RW
BitField Desc: Payload Type 0: satop 4: ces with cas 5: ces without cas 6: cep
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Bit_Start                                                     15
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Bit_End                                                       17
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Mask                                                   cBit17_15
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_Shift                                                         15
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_MaxVal                                                       0x7
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_MinVal                                                       0x0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldTypeCtrl_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size satop/cep mode: bit[13:0] payload size (ex: value as
0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot
bit[14:6] number of NxDS0 frame
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Bit_Start                                                      0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Bit_End                                                       14
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Mask                                                    cBit14_0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_Shift                                                          0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_MaxVal                                                    0x7fff
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_MinVal                                                       0x0
#define cAf6_pla_lo_pld_ctrl_PlaLoPldSizeCtrl_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Pseudowire Control
Reg Addr   : 0x0_D000-0x5_DBFF
Reg Formula: 0x0_D000 + $LoOc48Slice*65536 + $LoOc24Slice*1024 + $LoPwid
    Where  : 
           + $LoOc48Slice(0-5): LO OC-48 slices, there are total 6xOC-48 slices for LO 15G
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
           + $LoPwid(0-1023): pseudowire channel for each LO OC-24 slice
Reg Desc   : 
This register is used to configure some pseudowire properties in each LO Pseudowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_pw_ctrl_Base                                                                    0x0D000
#define cAf6Reg_pla_lo_pw_ctrl(LoOc48Slice, LoOc24Slice, LoPwid)      (0x0D000+(LoOc48Slice)*65536+(LoOc24Slice)*1024+(LoPwid))
#define cAf6Reg_pla_lo_pw_ctrl_WidthVal                                                                     32
#define cAf6Reg_pla_lo_pw_ctrl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PlaLoPwPidCtrl
BitField Type: RW
BitField Desc: Port ID output
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_Bit_Start                                                        20
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_Bit_End                                                          21
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_Mask                                                      cBit21_20
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_Shift                                                            20
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_MaxVal                                                          0x3
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_MinVal                                                          0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPidCtrl_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaLoPwPWIDCtrl
BitField Type: RW
BitField Desc: real PW id lookup
BitField Bits: [19:07]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_Bit_Start                                                        7
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_Bit_End                                                         19
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_Mask                                                      cBit19_7
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_Shift                                                            7
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_MaxVal                                                      0x1fff
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_MinVal                                                         0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwPWIDCtrl_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaLoPwSuprCtrl
BitField Type: RW
BitField Desc: Suppresion Enable 1: enable 0: disable
BitField Bits: [06]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_Bit_Start                                                        6
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_Bit_End                                                          6
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_Mask                                                         cBit6
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_Shift                                                            6
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_MaxVal                                                         0x1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_MinVal                                                         0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwSuprCtrl_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaLoPwMbitDisCtrl
BitField Type: RW
BitField Desc: M or NP bits disable 1: disable M or NP bits in the Control Word
(assign zero in the packet) 0: normal
BitField Bits: [05]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_Bit_Start                                                     5
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_Bit_End                                                       5
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_Mask                                                      cBit5
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_Shift                                                         5
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaLoPwLbitDisCtrl
BitField Type: RW
BitField Desc: L bit disable 1: disable L bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [04]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_Bit_Start                                                     4
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_Bit_End                                                       4
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_Mask                                                      cBit4
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_Shift                                                         4
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaLoPwMbitCPUCtrl
BitField Type: RW
BitField Desc: M or NP bits value from CPU (low priority than
PlaLoPwMbitDisCtrl)
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_Bit_Start                                                     2
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_Bit_End                                                       3
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_Mask                                                    cBit3_2
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_Shift                                                         2
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_MaxVal                                                      0x3
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwMbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaLoPwLbitCPUCtrl
BitField Type: RW
BitField Desc: L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_Bit_Start                                                     1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_Bit_End                                                       1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_Mask                                                      cBit1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_Shift                                                         1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_MaxVal                                                      0x1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwLbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaLoPwEnCtrl
BitField Type: RW
BitField Desc: LO Pseudowire enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_Bit_Start                                                          0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_Bit_End                                                            0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_Mask                                                           cBit0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_Shift                                                              0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_MaxVal                                                           0x1
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_MinVal                                                           0x0
#define cAf6_pla_lo_pw_ctrl_PlaLoPwEnCtrl_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x0_2800-0x5_6BFF
Reg Formula: 0x0_2800 + $LoOc48Slice*65536 + $LoOc24Slice*16384 + $LoPwid
    Where  : 
           + $LoOc48Slice(0-5): LO OC-48 slices, there are total 6xOC-48 slices for LO 15G
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
           + $LoPwid(0-1023): pseudowire channel for each LO OC-24 slice
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each LO OC-24 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl_Base                                                            0x02800
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl(LoOc48Slice, LoOc24Slice, LoPwid)(0x02800+(LoOc48Slice)*65536+(LoOc24Slice)*16384+(LoPwid))
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl_WidthVal                                                             64
#define cAf6Reg_pla_lo_add_rmv_pw_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: PlaLoStaEngineCtrl
BitField Type: RW
BitField Desc: Status for the engine, need to clear zero when add/remove pw
BitField Bits: [42:2]
--------------------------------------*/
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Bit_Start                                             2
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Bit_End                                              42
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Mask_01                                        cBit31_2
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Shift_01                                              2
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Mask_02                                        cBit10_0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Shift_02                                              0

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_Start                                             0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_End                                               1
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                            cBit1_0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                 0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MaxVal                                              0x3
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MinVal                                              0x0
#define cAf6_pla_lo_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order Payload Control
Reg Addr   : 0x8_0100-0x8_2A2F
Reg Formula: 0x8_0100 + $HoOc96Slice*2048 + $HoOc48Slice*512 + $HoPwid
    Where  : 
           + $HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G
           + $HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice
           + $HoPwid(0-47): pseudowire channel for each HO OC-48 slice
Reg Desc   : 
This register is used to configure payload in each HO Pseudowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_pldd_ctrl_Base                                                                  0x80100
#define cAf6Reg_pla_ho_pldd_ctrl(HoOc96Slice, HoOc48Slice, HoPwid)    (0x80100+(HoOc96Slice)*2048+(HoOc48Slice)*512+(HoPwid))
#define cAf6Reg_pla_ho_pldd_ctrl_WidthVal                                                                   32
#define cAf6Reg_pla_ho_pldd_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: PlaLoPldTypeCtrl
BitField Type: RW
BitField Desc: Payload Type 0: satop 6: cep
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_Bit_Start                                                    15
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_Bit_End                                                      17
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_Mask                                                  cBit17_15
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_Shift                                                        15
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_MaxVal                                                      0x7
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_MinVal                                                      0x0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldTypeCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Bit_Start                                                     0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Bit_End                                                      13
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Mask                                                   cBit13_0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_Shift                                                         0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_MaxVal                                                   0x3fff
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_MinVal                                                      0x0
#define cAf6_pla_ho_pldd_ctrl_PlaLoPldSizeCtrl_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order Pseudowire Control
Reg Addr   : 0x8_0680-0x8_2F2F
Reg Formula: 0x8_0680 + $HoOc96Slice*2048 + $HoOc48Slice*64 + $HoPwid
    Where  : 
           + $HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G
           + $HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice
           + $HoPwid(0-47): pseudowire channel for each HO OC-48 slice
Reg Desc   : 
This register is used to configure some pseudowire properties in each HO Pseudowire channels

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_pw_ctrl_Base                                                                    0x80680
#define cAf6Reg_pla_ho_pw_ctrl(HoOc96Slice, HoOc48Slice, HoPwid)      (0x80680+(HoOc96Slice)*2048+(HoOc48Slice)*64+(HoPwid))
#define cAf6Reg_pla_ho_pw_ctrl_WidthVal                                                                     32
#define cAf6Reg_pla_ho_pw_ctrl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PlaHoPwPidCtrl
BitField Type: RW
BitField Desc: Port ID output
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_Bit_Start                                                        20
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_Bit_End                                                          21
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_Mask                                                      cBit21_20
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_Shift                                                            20
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_MaxVal                                                          0x3
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_MinVal                                                          0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPidCtrl_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaHoPwPWIDCtrl
BitField Type: RW
BitField Desc: real PW id lookup
BitField Bits: [19:07]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_Bit_Start                                                        7
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_Bit_End                                                         19
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_Mask                                                      cBit19_7
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_Shift                                                            7
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_MaxVal                                                      0x1fff
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_MinVal                                                         0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwPWIDCtrl_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaHoPwSuprCtrl
BitField Type: RW
BitField Desc: Suppresion Enable 1: enable 0: disable
BitField Bits: [06]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_Bit_Start                                                        6
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_Bit_End                                                          6
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_Mask                                                         cBit6
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_Shift                                                            6
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_MaxVal                                                         0x1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_MinVal                                                         0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwSuprCtrl_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaHoPwNPbitDisCtrl
BitField Type: RW
BitField Desc: NP bits disable 1: disable NP bits in the Control Word (assign
zero in the packet) 0: normal
BitField Bits: [05]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_Bit_Start                                                    5
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_Bit_End                                                      5
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_Mask                                                     cBit5
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_Shift                                                        5
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_MaxVal                                                     0x1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_MinVal                                                     0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwNPbitDisCtrl_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaHoPwLbitDisCtrl
BitField Type: RW
BitField Desc: L bit disable 1: disable L bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [04]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_Bit_Start                                                     4
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_Bit_End                                                       4
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_Mask                                                      cBit4
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_Shift                                                         4
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaHoPwMbitCPUCtrl
BitField Type: RW
BitField Desc: NP bits value from CPU (low priority than PlaHoPwNPbitDisCtrl)
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_Bit_Start                                                     2
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_Bit_End                                                       3
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_Mask                                                    cBit3_2
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_Shift                                                         2
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_MaxVal                                                      0x3
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwMbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaHoPwLbitCPUCtrl
BitField Type: RW
BitField Desc: L bit value from CPU (low priority than PlaHoPwLbitDisCtrl)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_Bit_Start                                                     1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_Bit_End                                                       1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_Mask                                                      cBit1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_Shift                                                         1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_MaxVal                                                      0x1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwLbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaHoPwEnCtrl
BitField Type: RW
BitField Desc: HO Pseudowire enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_Bit_Start                                                          0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_Bit_End                                                            0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_Mask                                                           cBit0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_Shift                                                              0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_MaxVal                                                           0x1
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_MinVal                                                           0x0
#define cAf6_pla_ho_pw_ctrl_PlaHoPwEnCtrl_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Hig-Order Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x8_0140-0x8_2A6F
Reg Formula: 0x8_0140 + $HoOc96Slice*2048 + $HoOc48Slice*512 + $HoPwid
    Where  : 
           + $HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G
           + $HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice
           + $HoPwid(0-47): pseudowire channel for each HO OC-48 slice
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each HO OC-48 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl_Base                                                            0x80140
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl(HoOc96Slice, HoOc48Slice, HoPwid)(0x80140+(HoOc96Slice)*2048+(HoOc48Slice)*512+(HoPwid))
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl_WidthVal                                                             64
#define cAf6Reg_pla_ho_add_rmv_pw_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: PlaLoStaEngineCtrl
BitField Type: RW
BitField Desc: Status for the engine, need to clear zero when add/remove pw
BitField Bits: [59:2]
--------------------------------------*/
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Bit_Start                                             2
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Bit_End                                              59
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Mask_01                                        cBit31_2
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Shift_01                                              2
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Mask_02                                        cBit27_0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaEngineCtrl_Shift_02                                              0

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_Start                                             0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Bit_End                                               1
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                            cBit1_0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                 0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MaxVal                                              0x3
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_MinVal                                              0x0
#define cAf6_pla_ho_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output Pseudowire Control
Reg Addr   : 0x9_0000-0xC_1FFF
Reg Formula: 0x9_0000 + $pid*65536 + $pwid
    Where  : 
           + $pid(0-3): output port which the PW will be sent out
           + $pwid(0-8191): pseudowire channel
Reg Desc   : 
This register is used to config psedowire at output

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_pw_ctrl_Base                                                                   0x90000
#define cAf6Reg_pla_out_pw_ctrl(pid, pwid)                                        (0x90000+(pid)*65536+(pwid))
#define cAf6Reg_pla_out_pw_ctrl_WidthVal                                                                    64
#define cAf6Reg_pla_out_pw_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PlaOutHspwUsedCtr
BitField Type: RW
BitField Desc: HSPW is used or not 0:not used, all configurations for HSPW will
be not effected 1:used
BitField Bits: [36]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_Bit_Start                                                    36
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_Bit_End                                                      36
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_Mask                                                      cBit4
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_Shift                                                         4
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_MaxVal                                                      0x0
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_MinVal                                                      0x0
#define cAf6_pla_out_pw_ctrl_PlaOutHspwUsedCtr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaOutUpsrUsedCtr
BitField Type: RW
BitField Desc: UPSR is used or not 0:not used, all configurations for UPSR will
be not effected 1:used
BitField Bits: [35]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_Bit_Start                                                    35
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_Bit_End                                                      35
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_Mask                                                      cBit3
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_Shift                                                         3
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_MaxVal                                                      0x0
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_MinVal                                                      0x0
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrUsedCtr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaOutCepCtr
BitField Type: RW
BitField Desc: PW is in a CEP mode
BitField Bits: [34]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_Bit_Start                                                         34
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_Bit_End                                                           34
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_Mask                                                           cBit2
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_Shift                                                              2
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_MaxVal                                                           0x0
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_MinVal                                                           0x0
#define cAf6_pla_out_pw_ctrl_PlaOutCepCtr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PlaOutHspwGrpCtr
BitField Type: RW
BitField Desc: HSPW group
BitField Bits: [33:22]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Bit_Start                                                     22
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Bit_End                                                       33
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Mask_01                                                cBit31_22
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Shift_01                                                      22
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Mask_02                                                  cBit1_0
#define cAf6_pla_out_pw_ctrl_PlaOutHspwGrpCtr_Shift_02                                                       0

/*--------------------------------------
BitField Name: PlaOutUpsrGrpCtr
BitField Type: RW
BitField Desc: UPRS group
BitField Bits: [21:9]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_Bit_Start                                                      9
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_Bit_End                                                       21
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_Mask                                                    cBit21_9
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_Shift                                                          9
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_MaxVal                                                    0x1fff
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_MinVal                                                       0x0
#define cAf6_pla_out_pw_ctrl_PlaOutUpsrGrpCtr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaOutRbitDisCtrl
BitField Type: RW
BitField Desc: R bit disable 1: disable R bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_Bit_Start                                                     8
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_Bit_End                                                       8
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_Mask                                                      cBit8
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_Shift                                                         8
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_MaxVal                                                      0x1
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_MinVal                                                      0x0
#define cAf6_pla_out_pw_ctrl_PlaOutRbitDisCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaOutRbitCPUCtrl
BitField Type: RW
BitField Desc: R bit value from CPU (low priority than PlaOutRbitDisCtrl)
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_Bit_Start                                                     7
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_Bit_End                                                       7
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_Mask                                                      cBit7
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_Shift                                                         7
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_MaxVal                                                      0x1
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_MinVal                                                      0x0
#define cAf6_pla_out_pw_ctrl_PlaOutRbitCPUCtrl_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaOutPSNLenCtrl
BitField Type: RW
BitField Desc: PSN length
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_Bit_Start                                                      0
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_Bit_End                                                        6
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_Mask                                                     cBit6_0
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_Shift                                                          0
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_MaxVal                                                      0x7f
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_MinVal                                                       0x0
#define cAf6_pla_out_pw_ctrl_PlaOutPSNLenCtrl_RstVal                                                       0x0

/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output UPSR Control
Reg Addr   : 0x9_4000-0xC_5FFF
Reg Formula: 0x9_4000 + $upsrgrp
    Where  : 
           + $upsrgrp(0-511): UPSR group address
Reg Desc   : 
This register is used to config UPSR group

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_upsr_ctrl_Base                                                                 0x94000
#define cAf6Reg_pla_out_upsr_ctrl(upsrgrp)                                                 (0x94000+(upsrgrp))
#define cAf6Reg_pla_out_upsr_ctrl_WidthVal                                                                  32
#define cAf6Reg_pla_out_upsr_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaOutUpsrEnCtr
BitField Type: RW
BitField Desc: Total is 8092 upsrID, divide into 512 group address for
configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for
upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Bit_Start                                                     0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Bit_End                                                      15
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask                                                   cBit15_0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift                                                         0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_MaxVal                                                   0xffff
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_MinVal                                                      0x0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output HSPW Control
Reg Addr   : 0x9_8000-0xC_8FFF
Reg Formula: 0x9_8000 + $hspwid
    Where  : 
           + $hspwid(0-4095): HSPW group
Reg Desc   : 
This register is used to config HSPW group

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_hspw_ctrl_Base                                                                 0x98000
#define cAf6Reg_pla_out_hspw_ctrl(hspwid)                                                   (0x98000+(hspwid))
#define cAf6Reg_pla_out_hspw_ctrl_WidthVal                                                                  32
#define cAf6Reg_pla_out_hspw_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaOutHspwPsnCtr
BitField Type: RW
BitField Desc: PSN location for HSPW group
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Bit_Start                                                    1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Bit_End                                                      1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Mask                                                     cBit1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Shift                                                        1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_MaxVal                                                     0x1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_MinVal                                                     0x0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaOutHspwEnCtr
BitField Type: RW
BitField Desc: HSPW group enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Bit_Start                                                     0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Bit_End                                                       0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Mask                                                      cBit0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Shift                                                         0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_MaxVal                                                      0x1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_MinVal                                                      0x0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x8_4000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ctrl_Base                                                               0x84000
#define cAf6Reg_pla_out_psnpro_ctrl                                                                    0x84000
#define cAf6Reg_pla_out_psnpro_ctrl_WidthVal                                                                32
#define cAf6Reg_pla_out_psnpro_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Bit_Start                                               31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Bit_End                                                 31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask                                                cBit31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Shift                                                   31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_MaxVal                                                 0x1
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_MinVal                                                 0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutPsnProRnWCtr
BitField Type: RW
BitField Desc: read or write PSN 1: read PSN 0: write PSN
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Bit_Start                                               30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Bit_End                                                 30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Mask                                                cBit30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Shift                                                   30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_MaxVal                                                 0x1
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_MinVal                                                 0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Bit_Start                                               16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Bit_End                                                 22
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Mask                                             cBit22_16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Shift                                                   16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_MaxVal                                                0x7f
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_MinVal                                                 0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Bit_Start                                              13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Bit_End                                                13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask                                               cBit13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift                                                  13
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_MaxVal                                                0x1
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_MinVal                                                0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaOutPsnProPWIDCtr
BitField Type: RW
BitField Desc: Pseudowire channel
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_Bit_Start                                               0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_Bit_End                                                12
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_Mask                                             cBit12_0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_Shift                                                   0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_MaxVal                                             0x1fff
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_MinVal                                                0x0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPWIDCtr_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Buffer Control
Reg Addr   : 0x8_4010-0x8_4014
Reg Formula: 0x8_4010 + $segid
    Where  : 
           + $segid(0-4): segment 16-byte PSN, total is 5 segments for maximum 80 bytes PSN  %
Reg Desc   : 
This register is used to store PSN data which is before CPU request write or after CPU request read %%
The header format from entry#0 to entry#4 is as follow: %%
{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%
Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%

PSN header is MEF-8: %%
EthType:  0x88D8 %%
4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for  remote  receive side. %%
PSN header is MPLS:  %%
EthType:  0x8847 %%
4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%
Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%
Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%
PSN header is UDP/Ipv4:  %%
EthType:  0x0800 %%
28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
{IP_SrcAdr[31:0]} %%
{IP_DesAdr[31:0]} %%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%
{UDP_Sum[31:0] (optional)} %%
Case: %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%
IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%
{IP_TTL[7:0], IP_Protocol[7:0]} + %%
IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%
IP_DesAdr[31:16] + IP_DesAdr[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Protocol[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
PSN header is UDP/Ipv6:  %%
EthType:  0x86DD%%
48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%
{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%
{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%
{IP_SrcAdr[127:96]}%%
{IP_SrcAdr[95:64]}%%
{IP_SrcAdr[63:32]}%%
{IP_SrcAdr[31:0]}%%
{IP_DesAdr[127:96]}%%
{IP_DesAdr[95:64]}%%
{IP_DesAdr[63:32]}%%
{IP_DesAdr[31:0]}%%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
{UDP_Sum[31:0]}%%
Case:%%
IP_Next_Header[7:0]: 0x11 to signify UDP %%
UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Next_Header[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%
PSN header is MPLS over Ipv4:%%
IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%
After IPv4 header is MPLS labels where inner label is used for PW identification%%
PSN header is MPLS over Ipv6:%%
IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%
After IPv6 header is MPLS labels where inner label is used for PW identification%%
PSN header (all modes) with RTP enable: RTP used for TDM PW, following PSN header, define in RFC3550%%
Case:%%
RTPSSRC[31:0] : This is the SSRC value of RTP header%%
RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnbuf_ctrl_Base                                                               0x84010
#define cAf6Reg_pla_out_psnbuf_ctrl(segid)                                                   (0x84010+(segid))
#define cAf6Reg_pla_out_psnbuf_ctrl_WidthVal                                                               128
#define cAf6Reg_pla_out_psnbuf_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: PSN buffer
BitField Bits: [127:0]
--------------------------------------*/
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Bit_Start                                                0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Bit_End                                                127
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_01                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_01                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_02                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_02                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_03                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_03                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_04                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_04                                                 0

#endif /* _AF6_REG_AF6CCI0011_RD_PLA_H_ */

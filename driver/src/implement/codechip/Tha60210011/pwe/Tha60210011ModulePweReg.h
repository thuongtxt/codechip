/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_PWE_H_
#define _AF6_REG_AF6CCI0011_RD_PWE_H_

/*--------------------------- Define -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet Header Mode Control
Reg Addr   : 0x00000000 - 0x001FFF #The address format for these registers is 0x00000000 + PWID
Reg Formula: 0x00000000 +  PWID
    Where  :
           + $PWID(0-8191): Pseudowire ID
Reg Desc   :
This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_hdr_mode_Base                                                              0x00000000
#define cAf6Reg_pw_txeth_hdr_mode(PWID)                                                    (0x00000000+(PWID))
#define cAf6Reg_pw_txeth_hdr_mode_WidthVal                                                                  32
#define cAf6Reg_pw_txeth_hdr_mode_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: TxEthPadMode
BitField Type: RW
BitField Desc: this is configuration for insertion PAD in short packets 0:
Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to
IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64
bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer
labels(if exist) is less than 64 bytes
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Bit_Start                                                        7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Bit_End                                                          8
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Mask                                                       cBit8_7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Shift                                                            7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_MaxVal                                                         0x3
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_MinVal                                                         0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxEthPwRtpEn
BitField Type: RW
BitField Desc: this is RTP enable for TDM PW packet 1: Enable RTP 0: Disable RTP
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Bit_Start                                                        6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Bit_End                                                          6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Mask                                                         cBit6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Shift                                                            6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_MaxVal                                                         0x1
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_MinVal                                                         0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxEthPwPsnType
BitField Type: RW
BitField Desc: this is Transmit PSN header mode working 1: PW PSN header is
UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW MPLS no outer label over Ipv4 (total
1 MPLS label) 4: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 5: PW
MPLS one outer label over Ipv4 (total 2 MPLS label) 6: PW MPLS one outer label
over Ipv6 (total 2 MPLS label) 7: PW MPLS two outer label over Ipv4 (total 3
MPLS label) 8: PW MPLS two outer label over Ipv6 (total 3 MPLS label) Others:
for other PW PSN header type
BitField Bits: [5:2]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Bit_Start                                                      2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Bit_End                                                        5
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Mask                                                     cBit5_2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Shift                                                          2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_MaxVal                                                       0xf
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_MinVal                                                       0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxEthPwNumVlan
BitField Type: RW
BitField Desc: This is number of vlan in Transmit Ethernet packet 0: no vlan 1:
1 vlan 2: 2 vlan
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Bit_Start                                                      0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Bit_End                                                        1
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Mask                                                     cBit1_0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Shift                                                          0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_MaxVal                                                       0x3
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_MinVal                                                       0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0004000(RO)
Reg Formula: 0x0004000 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_ro_Base                                                                  0x0004000
#define cAf6Reg_Eth_cnt0_64_ro(ethport)                                                  (0x0004000+(ethport))
#define cAf6Reg_Eth_cnt0_64_ro_WidthVal                                                                     32
#define cAf6Reg_Eth_cnt0_64_ro_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Shift                                                              0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0004800(RC)
Reg Formula: 0x0004800 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_rc_Base                                                                  0x0004800
#define cAf6Reg_Eth_cnt0_64_rc(ethport)                                                  (0x0004800+(ethport))
#define cAf6Reg_Eth_cnt0_64_rc_WidthVal                                                                     32
#define cAf6Reg_Eth_cnt0_64_rc_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Shift                                                              0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0004004(RO)
Reg Formula: 0x0004004 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_ro_Base                                                                0x0004002
#define cAf6Reg_Eth_cnt65_127_ro(ethport)                                                (0x0004004+(ethport))
#define cAf6Reg_Eth_cnt65_127_ro_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt65_127_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Shift                                                          0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0004804(RC)
Reg Formula: 0x0004804 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_rc_Base                                                                0x0004802
#define cAf6Reg_Eth_cnt65_127_rc(ethport)                                                (0x0004804+(ethport))
#define cAf6Reg_Eth_cnt65_127_rc_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt65_127_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Shift                                                          0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0004008(RO)
Reg Formula: 0x0004008 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_ro_Base                                                               0x0004004
#define cAf6Reg_Eth_cnt128_255_ro(ethport)                                               (0x0004008+(ethport))
#define cAf6Reg_Eth_cnt128_255_ro_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt128_255_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Shift                                                        0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0004808(RC)
Reg Formula: 0x0004808 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_rc_Base                                                               0x0004804
#define cAf6Reg_Eth_cnt128_255_rc(ethport)                                               (0x0004808+(ethport))
#define cAf6Reg_Eth_cnt128_255_rc_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt128_255_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Shift                                                        0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000400C(RO)
Reg Formula: 0x000400C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_ro_Base                                                               0x0004006
#define cAf6Reg_Eth_cnt256_511_ro(ethport)                                               (0x000400C+(ethport))
#define cAf6Reg_Eth_cnt256_511_ro_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt256_511_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Shift                                                        0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000480C(RC)
Reg Formula: 0x000480C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_rc_Base                                                               0x0004806
#define cAf6Reg_Eth_cnt256_511_rc(ethport)                                               (0x000480C+(ethport))
#define cAf6Reg_Eth_cnt256_511_rc_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt256_511_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Shift                                                        0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0004010(RO)
Reg Formula: 0x0004010 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1023_ro_Base                                                              0x0004008
#define cAf6Reg_Eth_cnt512_1023_ro(ethport)                                              (0x0004010+(ethport))
#define cAf6Reg_Eth_cnt512_1023_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt512_1023_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1023
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Bit_Start                                                  0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Bit_End                                                   31
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Shift                                                      0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_MaxVal                                            0xffffffff
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_MinVal                                                   0x0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0004810(RC)
Reg Formula: 0x0004810 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1023_rc_Base                                                              0x0004808
#define cAf6Reg_Eth_cnt512_1023_rc(ethport)                                              (0x0004810+(ethport))
#define cAf6Reg_Eth_cnt512_1023_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt512_1023_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1023
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Bit_Start                                                  0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Bit_End                                                   31
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Shift                                                      0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_MaxVal                                            0xffffffff
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_MinVal                                                   0x0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x0004014(RO)
Reg Formula: 0x0004014 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1024_1518_ro_Base                                                             0x000400A
#define cAf6Reg_Eth_cnt1024_1518_ro(ethport)                                             (0x0004014+(ethport))
#define cAf6Reg_Eth_cnt1024_1518_ro_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1024_1518_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1024_1518
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Bit_Start                                                0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Bit_End                                                 31
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Mask                                              cBit31_0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Shift                                                    0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_MinVal                                                 0x0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x0004814(RC)
Reg Formula: 0x0004814 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1024_1518_rc_Base                                                             0x000480A
#define cAf6Reg_Eth_cnt1024_1518_rc(ethport)                                             (0x0004814+(ethport))
#define cAf6Reg_Eth_cnt1024_1518_rc_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1024_1518_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1024_1518
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Bit_Start                                                0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Bit_End                                                 31
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Mask                                              cBit31_0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Shift                                                    0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_MinVal                                                 0x0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x0004018(RO)
Reg Formula: 0x0004018 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1519_2047_ro_Base                                                             0x000400C
#define cAf6Reg_Eth_cnt1519_2047_ro(ethport)                                             (0x0004018+(ethport))
#define cAf6Reg_Eth_cnt1519_2047_ro_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1519_2047_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1519_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Bit_Start                                                0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Bit_End                                                 31
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Shift                                                    0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_MinVal                                                 0x0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x0004818(RC)
Reg Formula: 0x0004818 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1519_2047_rc_Base                                                             0x000480C
#define cAf6Reg_Eth_cnt1519_2047_rc(ethport)                                             (0x0004818+(ethport))
#define cAf6Reg_Eth_cnt1519_2047_rc_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1519_2047_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1519_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Bit_Start                                                0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Bit_End                                                 31
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Shift                                                    0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_MinVal                                                 0x0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000401C(RO)
Reg Formula: 0x000401C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_ro_Base                                                                0x000400E
#define cAf6Reg_Eth_cnt_jumbo_ro(ethport)                                                (0x000401C+(ethport))
#define cAf6Reg_Eth_cnt_jumbo_ro_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt_jumbo_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Shift                                                           0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000481C(RC)
Reg Formula: 0x000481C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_rc_Base                                                                0x000480E
#define cAf6Reg_Eth_cnt_jumbo_rc(ethport)                                                (0x000481C+(ethport))
#define cAf6Reg_Eth_cnt_jumbo_rc_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt_jumbo_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Shift                                                           0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Unicast packet
Reg Addr   : 0x0004020(RO)
Reg Formula: 0x0004020 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_ro_Base                                                              0x0004010
#define cAf6Reg_Eth_cnt_Unicast_ro(ethport)                                              (0x0004020+(ethport))
#define cAf6Reg_Eth_cnt_Unicast_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_Unicast_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Shift                                                       0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Unicast packet
Reg Addr   : 0x0004820(RC)
Reg Formula: 0x0004820 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_rc_Base                                                              0x0004810
#define cAf6Reg_Eth_cnt_Unicast_rc(ethport)                                              (0x0004820+(ethport))
#define cAf6Reg_Eth_cnt_Unicast_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_Unicast_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Shift                                                       0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0004024(RO)
Reg Formula: 0x0004024 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_ro_Base                                                               0x0004012
#define cAf6Reg_eth_tx_pkt_cnt_ro(ethport)                                               (0x0004024+(ethport))
#define cAf6Reg_eth_tx_pkt_cnt_ro_WidthVal                                                                  32
#define cAf6Reg_eth_tx_pkt_cnt_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Bit_Start                                                      0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Bit_End                                                       31
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Mask                                                    cBit31_0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Shift                                                          0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_MaxVal                                                0xffffffff
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_MinVal                                                       0x0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0004824(RC)
Reg Formula: 0x0004824 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_rc_Base                                                               0x0004812
#define cAf6Reg_eth_tx_pkt_cnt_rc(ethport)                                               (0x0004824+(ethport))
#define cAf6Reg_eth_tx_pkt_cnt_rc_WidthVal                                                                  32
#define cAf6Reg_eth_tx_pkt_cnt_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Bit_Start                                                      0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Bit_End                                                       31
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Mask                                                    cBit31_0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Shift                                                          0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_MaxVal                                                0xffffffff
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_MinVal                                                       0x0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Broadcast packet
Reg Addr   : 0x0004028(RO)
Reg Formula: 0x0004028 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Broadcast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_Base                                                         0x0004014
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro(ethport)                                         (0x0004028+(ethport))
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_WidthVal                                                            32
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_Start                                            0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_End                                             31
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Mask                                          cBit31_0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Shift                                                0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MinVal                                             0x0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Broadcast packet
Reg Addr   : 0x0004828(RC)
Reg Formula: 0x0004828 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Broadcast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_Base                                                         0x0004814
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc(ethport)                                         (0x0004828+(ethport))
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_WidthVal                                                            32
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_Start                                            0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_End                                             31
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Mask                                          cBit31_0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Shift                                                0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MinVal                                             0x0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Multicast packet
Reg Addr   : 0x000402C(RO)
Reg Formula: 0x000402C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Multicast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_Base                                                         0x0004016
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro(ethport)                                         (0x000402C+(ethport))
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_WidthVal                                                            32
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_Start                                            0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_End                                             31
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Mask                                          cBit31_0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Shift                                                0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_MinVal                                             0x0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Multicast packet
Reg Addr   : 0x000482C(RC)
Reg Formula: 0x000482C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Multicast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_Base                                                         0x0004816
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc(ethport)                                         (0x000482C+(ethport))
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_WidthVal                                                            32
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_Start                                            0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_End                                             31
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Mask                                          cBit31_0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Shift                                                0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_MinVal                                             0x0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000403C(RO)
Reg Formula: 0x000403C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_ro_Base                                                                 0x000401E
#define cAf6Reg_Eth_cnt_byte_ro(ethport)                                                 (0x000403C+(ethport))
#define cAf6Reg_Eth_cnt_byte_ro_WidthVal                                                                    32
#define cAf6Reg_Eth_cnt_byte_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Bit_End                                                          31
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Shift                                                             0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000483C(RC)
Reg Formula: 0x000483C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_rc_Base                                                                 0x000481E
#define cAf6Reg_Eth_cnt_byte_rc(ethport)                                                 (0x000483C+(ethport))
#define cAf6Reg_Eth_cnt_byte_rc_WidthVal                                                                    32
#define cAf6Reg_Eth_cnt_byte_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Bit_End                                                          31
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Shift                                                             0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_RstVal                                                          0x0


#endif /* _AF6_REG_AF6CCI0011_RD_PWE_H_ */

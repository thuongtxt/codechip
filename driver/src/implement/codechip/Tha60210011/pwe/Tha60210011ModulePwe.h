/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210011ModulePwe.h
 * 
 * Created Date: May 6, 2015
 *
 * Description : Module PWE of 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPWE_H_
#define _THA60210011MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwGroup.h"
#include "../../../ThaStmPw/pwe/ThaStmPwModulePweV2.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumByteOfRtpStorage 8
#define cWorkingPsnPage 0
#define cBackupPsnPage  1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePwe * Tha60210011ModulePwe;
typedef struct tTha60210011PwePwCounterGetter * Tha60210011PwePwCounterGetter;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModulePweObjectInit(AtModule self, AtDevice device);

uint32 Tha60210011ModulePweModuleOffsetByEthernetPort(ThaModulePwe self, AtEthPort port);
eAtRet Tha60210011ModulePweApsGroupPwAdd(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210011ModulePweApsGroupPwRemove(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210011ModulePweHsGroupPwAdd(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210011ModulePweHsGroupPwRemove(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210011ModulePweApsGroupEnable(ThaModulePwe self, AtPwGroup pwGroup, eBool enable);
eAtRet Tha60210011ModulePweHsGroupEnable(ThaModulePwe self, AtPwGroup pwGroup, eBool enable);
eBool Tha60210011ModulePweApsGroupIsEnabled(ThaModulePwe self, AtPwGroup pwGroup);
eBool Tha60210011ModulePweHsGroupIsEnabled(ThaModulePwe self, AtPwGroup pwGroup);
eAtRet Tha60210011ModulePweHsGroupLabelSetSelect(ThaModulePwe self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
eAtRet Tha60210011ModulePweHsGroupSelectedLabelSetGet(ThaModulePwe self, AtPwGroup pwGroup);

void Tha60210011ModulePwePwDefaultHwConfigure(ThaModulePwe self, ThaPwAdapter pwAdapter);
eAtRet Tha60210011PlaLoLineCircuitPwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType);

/* PW header helpers */
eAtRet Tha60210011ModulePwePwBackupHeaderWrite(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn);
eAtRet Tha60210011ModulePwePwBackupHeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize);
uint32 Tha60210011ModulePlaBaseAddress(void);
uint32 Tha60210011ModulePweBaseAddress(void);
uint8 Tha60210011ModulePweNumberOfAddtionalByte(Tha60210011ModulePwe self, AtPw pw);
eAtRet Tha60210011ModulePwePwHeaderFlush(ThaModulePwe self, AtPw pw);

uint32 Tha60210012ModulePwePortCounterOffset(Tha60210011ModulePwe self, AtEthPort port, eBool r2c);

uint32 Tha60210011ModulePweStartCrcRamLocalId(Tha60210011ModulePwe self);
uint32 Tha60210011ModulePweStartHSPWRamLocalId(Tha60210011ModulePwe self);
uint32 Tha60210011ModulePweStartHoPlaRamLocalId(Tha60210011ModulePwe self);
uint32 Tha60210011ModulePweStartPweRamLocalId(Tha60210011ModulePwe self);
eAtRet Tha60210011ModulePweAllPwsHeaderFlush(ThaModulePwe self);
uint32 Tha60210011ModulePweAbsolutePwIdGet(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId);

/* Backward compatible */
uint32 Tha60210011ModulePweStartVersionControlJitterAttenuator(ThaModulePwe self);

#endif /* _THA60210011MODULEPWE_H_ */


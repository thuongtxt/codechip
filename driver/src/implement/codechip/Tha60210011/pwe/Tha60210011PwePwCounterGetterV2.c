/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210011PwePwCounterGetterV2.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PWE Pw counter getter of 60210011 V2 support 6144 PWs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegPmcPwTxPktCnt(ro)     (0x1E00000UL + (ro) * 0x2000UL)
#define cRegPmcPwTxPldOctCnt(ro)  (0x1E04000UL + (ro) * 0x2000UL)
#define cRegPmcPwTxLbitPktCnt(ro) (0x1E08000UL + (ro) * 0x2000UL)
#define cRegPmcPwTxRbitPktCnt(ro) (0x1E0C000UL + (ro) * 0x2000UL)
#define cRegPmcPwTxMorNbitCnt(ro) (0x1E10000UL + (ro) * 0x2000UL)
#define cRegPmcPwTxPbitCnt(ro)    (0x1E14000UL + (ro) * 0x2000UL)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011PwePwCounterGetterV2
    {
    tTha60210011PwePwCounterGetter super;
    }tTha60210011PwePwCounterGetterV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011PwePwCounterGetterMethods m_Tha60210011PwePwCounterGetterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(AtPw pw)
    {
    return AtChannelIdGet((AtChannel)pw);
    }

static uint32 TxPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxBytesGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxPldOctCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxLbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxLbitPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxRbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxRbitPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 TxMbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxMorNbitCnt((clear) ? 0 : 1) + CounterOffset(pw), cThaModulePwe);
    }

static uint32 TxPbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxPbitCnt((clear) ? 0 : 1) + CounterOffset(pw), cThaModulePwe);
    }

static uint32 TxNbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return mChannelHwRead(pw, cRegPmcPwTxMorNbitCnt((clear) ? 0 : 1) + CounterOffset(pw), cThaModulePwe);
    }

static void OverrideTha60210011PwePwCounterGetter(Tha60210011PwePwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PwePwCounterGetterOverride, mMethodsGet(self), sizeof(m_Tha60210011PwePwCounterGetterOverride));

        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxBytesGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxLbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxRbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxMbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxPbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxNbitPacketsGet);
        }

    mMethodsSet(self, &m_Tha60210011PwePwCounterGetterOverride);
    }

static void Override(Tha60210011PwePwCounterGetter self)
    {
    OverrideTha60210011PwePwCounterGetter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PwePwCounterGetterV2);
    }

static Tha60210011PwePwCounterGetter ObjectInit(Tha60210011PwePwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwePwCounterGetterObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011PwePwCounterGetter Tha60210011PwePwCounterGetterV2New(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011PwePwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGetter);
    }

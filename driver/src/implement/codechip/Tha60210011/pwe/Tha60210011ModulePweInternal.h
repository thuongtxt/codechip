/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210011ModulePweInternal.h
 * 
 * Created Date: Sep 29, 2015
 *
 * Description : 60210011 module PWE internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPWEINTERNAL_H_
#define _THA60210011MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../ThaStmPw/pwe/ThaStmPwModulePweV2.h"
#include "Tha60210011ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cMaxSupportedHeaderLengthInByte (cMaxNumSegment * 16)
#define cMaxNumSegment 6

#define sTha60210011ModulePweRamPLAOutputPWEPort2ControlRegister                     "PLA Output PWE Port2 Control Register"
#define sTha60210011ModulePweRamPWEPseudowireTransmitEthernetHeaderModeControlslice1 "PWE Pseudowire Transmit Ethernet Header Mode Control slice 1"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePweMethods
    {
    uint8 (*NumberOfAddtionalByte)(Tha60210011ModulePwe self, AtPw adapter);
    eAtRet (*PlaPwTypeSet)(Tha60210011ModulePwe self, AtPw pw, eAtPwType pwType);
    eAtRet (*PwePwTypeSet)(Tha60210011ModulePwe self, AtPw pw, eAtPwType pwType);
    eBool (*CanWaitForHwDone)(Tha60210011ModulePwe self);
    uint32 (*AbsolutePwIdGet)(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId);
    uint32 (*NumGroupsPerRegister)(Tha60210011ModulePwe self);

    /* Return number dword of data buffer has been written to segment */
    uint8 (*PutDataToFirstSegment)(Tha60210011ModulePwe self, AtPw pw, AtPwPsn psn, uint8 *dataBuffer, uint32* firstSegment, eThaPwAdapterHeaderWriteMode writeMode);

    /* register address and fields */
    uint32 (*LoPayloadCtrlOffset)(Tha60210011ModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, uint32 loPwid);
    uint32 (*LoPayloadCtrl)(Tha60210011ModulePwe self);
    uint32 (*HoPayloadCtrlOffset)(Tha60210011ModulePwe self, uint32 hoOc96Slice, uint32 hoOc48Slice, uint32 hoPwid);
    uint32 (*HoPayloadCtrl)(Tha60210011ModulePwe self);

    uint32 (*LoAddRemoveProtocolOffset)(Tha60210011ModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, uint32 loPwid);
    uint32 (*LoAddRemoveProtocolReg)(Tha60210011ModulePwe self);
    uint32 (*HoAddRemoveProtocolOffset)(Tha60210011ModulePwe self, uint32 hoOc96Slice, uint32 hoOc48Slice, uint32 hoPwid);
    uint32 (*HoAddRemoveProtocolReg)(Tha60210011ModulePwe self);

    uint32 (*PsnControlReg)(Tha60210011ModulePwe self);
    uint32 (*PsnChannelIdMask)(Tha60210011ModulePwe self);
    uint32 (*PsnChannelIdShift)(Tha60210011ModulePwe self);
    uint32 (*PsnPwHwId)(Tha60210011ModulePwe self, AtPw adapter);
    uint32 (*PsnBufferCtrlReg)(Tha60210011ModulePwe self);
    void (*PlaPwDefaultHwConfigure)(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter);
    uint32 (*MaxChannelHeader)(Tha60210011ModulePwe self);
    uint32 (*PlaOutUpsrCtrlBase)(Tha60210011ModulePwe self);
    uint32 (*PlaOutHspwCtrlBase)(Tha60210011ModulePwe self);
    uint32 (*PlaPldControlOffset)(Tha60210011ModulePwe self, AtPw pw);
    uint32 (*PwControl)(Tha60210011ModulePwe self, AtPw pw);
    uint32 (*PDHPWAddingProtocolToPreventBurst)(Tha60210011ModulePwe self, AtPw pw);
    uint32 (*PlaOutPsnProPageCtrMask)(Tha60210011ModulePwe self);
    uint32 (*PlaOutPsnProPageCtrShift)(Tha60210011ModulePwe self);

    void (*PwPlaRegsShow)(Tha60210011ModulePwe self, AtPw pw);
    void (*DebugInforLowOrderClock155)(Tha60210011ModulePwe self, AtPw pw);
    eBool (*PwGroupingShow)(Tha60210011ModulePwe self, AtPw pw);
    void (*PwPlaDebug)(Tha60210011ModulePwe self, AtPw pw);
    void (*LowOrderBlockProcessDebug)(Tha60210011ModulePwe self);
    void (*PlaInterfaceDebug)(Tha60210011ModulePwe self);

    uint32 (*PortCounterOffset)(Tha60210011ModulePwe self, AtEthPort port, eBool r2c);

    /* For internal RAMs control */
    uint32 (*StartCrcRamLocalId)(Tha60210011ModulePwe self);
    uint32 (*StartHSPWRamLocalId)(Tha60210011ModulePwe self);
    uint32 (*StartHoPlaRamLocalId)(Tha60210011ModulePwe self);
    uint32 (*StartPweRamLocalId)(Tha60210011ModulePwe self);

    /* For backward compatible */
    uint32 (*StartVersionControlJitterAttenuator)(Tha60210011ModulePwe self);
    Tha60210011PwePwCounterGetter (*CounterGetter)(Tha60210011ModulePwe self);
    }tTha60210011ModulePweMethods;

typedef struct tTha60210011ModulePwe
    {
    tThaStmPwModulePweV2 super;
    const tTha60210011ModulePweMethods* methods;

    /* Debugging */
    uint32 maxPwStateWaitingTimeMs;
    Tha60210011PwePwCounterGetter counterGetter;
    }tTha60210011ModulePwe;

typedef struct tTha60210011PwePwCounterGetterMethods
    {
    /* Counters */
    uint32 (*TxPacketsGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    uint32 (*TxBytesGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    uint32 (*TxLbitPacketsGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    uint32 (*TxRbitPacketsGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    uint32 (*TxMbitPacketsGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    uint32 (*TxNbitPacketsGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    uint32 (*TxPbitPacketsGet)(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
    }tTha60210011PwePwCounterGetterMethods;

typedef struct tTha60210011PwePwCounterGetter
    {
    tAtObject super;
    const tTha60210011PwePwCounterGetterMethods* methods;
    }tTha60210011PwePwCounterGetter;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60210011PwePwCounterGetter Tha60210011PwePwCounterGetterObjectInit(Tha60210011PwePwCounterGetter self);

Tha60210011PwePwCounterGetter Tha60210011PwePwCounterGetterNew(void);
Tha60210011PwePwCounterGetter Tha60210011PwePwCounterGetterV2New(void);

uint32 PwePwCounterGetterTxPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
uint32 PwePwCounterGetterTxBytesGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
uint32 PwePwCounterGetterTxLbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
uint32 PwePwCounterGetterTxRbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
uint32 PwePwCounterGetterTxMbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
uint32 PwePwCounterGetterTxNbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);
uint32 PwePwCounterGetterTxPbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear);

uint32 Tha60210011ModulePlaAddRemoveProtocolOffset(ThaModulePwe self, AtPw pw);
uint32 Tha60210011ModulePlaPldControlOffset(ThaModulePwe self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPWEINTERNAL_H_ */


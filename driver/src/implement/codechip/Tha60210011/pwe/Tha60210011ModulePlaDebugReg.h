/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PLA
 * 
 * File        : Tha60210011ModulePlaDebugReg.h
 * 
 * Created Date: Sep 3, 2015
 *
 * Description : Debug register of PLA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPLADEBUGREG_H_
#define _THA60210011MODULEPLADEBUGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order clk155 Sticky
Reg Addr   : 0x0_8000 + $LoOc48Slice*65536
Reg Formula:
    Where  :
           + $LoOc48Slice(0-5): LO OC-48 slices, there are total 6xOC-48 slices for LO 15G
Reg Desc   :
This register is used to used to sticky some alarms for debug per OC-48 Lo-order @clk155.52 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_clk155_stk_Base                                            0x08000
#define cAf6Reg_pla_lo_clk155_stk(LoOc48Slice)                                    (00x08000 + ((LoOc48Slice)* 65536))
#define cAf6Reg_pla_lo_clk155_stk_WidthVal                                        032
#define cAf6Reg_pla_lo_clk155_stk_WriteMask                                       0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_1ConvErr
BitField Type: W2C
BitField Desc: OC24#1 convert 155to233 Error
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_Bit_Start                                              13
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_Bit_End                                                13
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_Mask                                               cBit13
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_Shift                                                  13
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_MaxVal                                                0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_MinVal                                                0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1ConvErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0ConvErr
BitField Type: W2C
BitField Desc: OC24#0 convert 155to233 Error
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_Bit_Start                                              12
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_Bit_End                                                12
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_Mask                                               cBit12
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_Shift                                                  12
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_MaxVal                                                0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_MinVal                                                0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0ConvErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_1PwVld
BitField Type: W2C
BitField Desc: OC24#1 input of expected pwid
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_Bit_Start                                                11
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_Bit_End                                                  11
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_Mask                                                 cBit11
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_Shift                                                    11
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1PwVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0PwVld
BitField Type: W2C
BitField Desc: OC24#0 input of expected pwid
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_Bit_Start                                                10
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_Bit_End                                                  10
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_Mask                                                 cBit10
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_Shift                                                    10
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0PwVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InVld
BitField Type: W2C
BitField Desc: OC24#0 input data from demap
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_Bit_Start                                                 9
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_Bit_End                                                   9
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_Mask                                                  cBit9
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_Shift                                                     9
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_1InVld
BitField Type: W2C
BitField Desc: OC24#1 input data from demap
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_Bit_Start                                                 8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_Bit_End                                                   8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_Mask                                                  cBit8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_Shift                                                     8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InFst
BitField Type: W2C
BitField Desc: OC24#0 input first-timeslot
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_Bit_Start                                                 7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_Bit_End                                                   7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_Mask                                                  cBit7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_Shift                                                     7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InFst_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_1InFst
BitField Type: W2C
BitField Desc: OC24#1 input first-timeslot
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_Bit_Start                                                 6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_Bit_End                                                   6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_Mask                                                  cBit6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_Shift                                                     6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InFst_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InAis
BitField Type: W2C
BitField Desc: OC24#0 input AIS
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_Bit_Start                                                 5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_Bit_End                                                   5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_Mask                                                  cBit5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_Shift                                                     5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InAis_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_1InAIS
BitField Type: W2C
BitField Desc: OC24#1 input AIS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_Bit_Start                                                 4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_Bit_End                                                   4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_Mask                                                  cBit4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_Shift                                                     4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InAIS_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InPos
BitField Type: W2C
BitField Desc: OC24#0 input CEP Pos Pointer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_Bit_Start                                                 3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_Bit_End                                                   3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_Mask                                                  cBit3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_Shift                                                     3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InPos_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InNeg
BitField Type: W2C
BitField Desc: OC24#0 input CEP Neg Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_Bit_Start                                                 2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_Bit_End                                                   2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_Mask                                                  cBit2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_Shift                                                     2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_0InNeg_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InPos
BitField Type: W2C
BitField Desc: OC24#1 input CEP Pos Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_Bit_Start                                                 1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_Bit_End                                                   1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_Mask                                                  cBit1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_Shift                                                     1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InPos_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24_0InNeg
BitField Type: W2C
BitField Desc: OC24#1 input CEP Neg Pointer
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_Bit_Start                                                 0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_Bit_End                                                   0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_Mask                                                  cBit0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_Shift                                                     0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24_1InNeg_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order clk311 Sticky
Reg Addr   : 0x8_0400-0x8_2C00
Reg Formula: 0x8_0400 + $HoOc96Slice*2048
    Where  :
           + $HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G
Reg Desc   :
This register is used to used to sticky some alarms for debug per OC-96 Hi-order @clk311.04 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_clk311_stk_Base                                                                 0x80400
#define cAf6Reg_pla_ho_clk311_stk(HoOc96Slice)                                    (0x80400+(HoOc96Slice)*2048)
#define cAf6Reg_pla_ho_clk311_stk_WidthVal                                                                  32
#define cAf6Reg_pla_ho_clk311_stk_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_1ConvErr
BitField Type: W2C
BitField Desc: OC48#1 convert 311to233 Error
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_Bit_Start                                              13
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_Bit_End                                                13
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_Mask                                               cBit13
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_Shift                                                  13
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_MaxVal                                                0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_MinVal                                                0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1ConvErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0ConvErr
BitField Type: W2C
BitField Desc: OC48#0 convert 311to233 Error
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_Bit_Start                                              12
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_Bit_End                                                12
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_Mask                                               cBit12
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_Shift                                                  12
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_MaxVal                                                0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_MinVal                                                0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0ConvErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_1PwVld
BitField Type: W2C
BitField Desc: OC48#1 input of expected pwid
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_Bit_Start                                                11
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_Bit_End                                                  11
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_Mask                                                 cBit11
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_Shift                                                    11
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1PwVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0PwVld
BitField Type: W2C
BitField Desc: OC48#0 input of expected pwid
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_Bit_Start                                                10
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_Bit_End                                                  10
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_Mask                                                 cBit10
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_Shift                                                    10
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0PwVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InVld
BitField Type: W2C
BitField Desc: OC48#0 input data from demap
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_Bit_Start                                                 9
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_Bit_End                                                   9
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_Mask                                                  cBit9
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_Shift                                                     9
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_1InVld
BitField Type: W2C
BitField Desc: OC48#1 input data from demap
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_Bit_Start                                                 8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_Bit_End                                                   8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_Mask                                                  cBit8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_Shift                                                     8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InFst
BitField Type: W2C
BitField Desc: OC48#0 input first-timeslot
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_Bit_Start                                                 7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_Bit_End                                                   7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_Mask                                                  cBit7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_Shift                                                     7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InFst_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_1InFst
BitField Type: W2C
BitField Desc: OC48#1 input first-timeslot
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_Bit_Start                                                 6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_Bit_End                                                   6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_Mask                                                  cBit6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_Shift                                                     6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InFst_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InAis
BitField Type: W2C
BitField Desc: OC48#0 input AIS
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_Bit_Start                                                 5
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_Bit_End                                                   5
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_Mask                                                  cBit5
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_Shift                                                     5
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InAis_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_1InAIS
BitField Type: W2C
BitField Desc: OC48#1 input AIS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_Bit_Start                                                 4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_Bit_End                                                   4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_Mask                                                  cBit4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_Shift                                                     4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InAIS_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InPos
BitField Type: W2C
BitField Desc: OC48#0 input CEP Pos Pointer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_Bit_Start                                                 3
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_Bit_End                                                   3
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_Mask                                                  cBit3
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_Shift                                                     3
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InPos_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InNeg
BitField Type: W2C
BitField Desc: OC48#0 input CEP Neg Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_Bit_Start                                                 2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_Bit_End                                                   2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_Mask                                                  cBit2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_Shift                                                     2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_0InNeg_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InPos
BitField Type: W2C
BitField Desc: OC48#1 input CEP Pos Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_Bit_Start                                                 1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_Bit_End                                                   1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_Mask                                                  cBit1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_Shift                                                     1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InPos_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC48_0InNeg
BitField Type: W2C
BitField Desc: OC48#1 input CEP Neg Pointer
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_Bit_Start                                                 0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_Bit_End                                                   0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_Mask                                                  cBit0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_Shift                                                     0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC48_1InNeg_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order Block Process Sticky
Reg Addr   : 0x8_0601-0x8_2E01
Reg Formula: 0x8_0601 + $HoOc96Slice*2048
    Where  :
           + $HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G
Reg Desc   :
This register is used to used to sticky some alarms for debug per OC-96 Hi-order block processing

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_block_pro_stk_Base                                                              0x80601
#define cAf6Reg_pla_ho_block_pro_stk(HoOc96Slice)                                 (0x80601+(HoOc96Slice)*2048)
#define cAf6Reg_pla_ho_block_pro_stk_WidthVal                                                               32
#define cAf6Reg_pla_ho_block_pro_stk_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: PlaHoBlkPkInfoReq
BitField Type: W2C
BitField Desc: OC96 packet infor request
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_Bit_Start                                                7
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_Bit_End                                                  7
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_Mask                                                 cBit7
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_Shift                                                    7
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_MaxVal                                                 0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_MinVal                                                 0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoReq_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaHoBlkPkInfoAck
BitField Type: W2C
BitField Desc: OC96 packet infor Ack
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_Bit_Start                                                6
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_Bit_End                                                  6
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_Mask                                                 cBit6
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_Shift                                                    6
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_MaxVal                                                 0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_MinVal                                                 0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoAck_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaHoBlkPkInfoVld
BitField Type: W2C
BitField Desc: OC96 packet infor Valid
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_Bit_Start                                                5
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_Bit_End                                                  5
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_Mask                                                 cBit5
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_Shift                                                    5
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_MaxVal                                                 0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_MinVal                                                 0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkPkInfoVld_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaHoBlkMaxLength
BitField Type: W2C
BitField Desc: OC96 max length violation
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_Bit_Start                                                4
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_Bit_End                                                  4
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_Mask                                                 cBit4
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_Shift                                                    4
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_MaxVal                                                 0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_MinVal                                                 0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkMaxLength_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaHoPla2ConvErr
BitField Type: W2C
BitField Desc: OC96 OC48#2 Convert Error
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_Bit_Start                                                 3
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_Bit_End                                                   3
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_Mask                                                  cBit3
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_Shift                                                     3
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_MaxVal                                                  0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_MinVal                                                  0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoPla2ConvErr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaHoPla1ConvErr
BitField Type: W2C
BitField Desc: OC96 OC48#1 Convert Error
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_Bit_Start                                                 2
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_Bit_End                                                   2
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_Mask                                                  cBit2
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_Shift                                                     2
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_MaxVal                                                  0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_MinVal                                                  0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoPla1ConvErr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaHoBlkEmp
BitField Type: W2C
BitField Desc: OC96 block empty
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_Bit_Start                                                      1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_Bit_End                                                        1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_Mask                                                       cBit1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_Shift                                                          1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_MaxVal                                                       0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_MinVal                                                       0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkEmp_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaHoBlkSame
BitField Type: W2C
BitField Desc: OC96 block same
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_Bit_Start                                                     0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_Bit_End                                                       0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_Mask                                                      cBit0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_Shift                                                         0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_MaxVal                                                      0x1
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_MinVal                                                      0x0
#define cAf6_pla_ho_block_pro_stk_PlaHoBlkSame_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Block Process Sticky
Reg Addr   : 0x8_8001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some alarms for debug low-order block processing

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_block_pro_stk_Base                                                              0x88001
#define cAf6Reg_pla_lo_block_pro_stk                                                                   0x88001
#define cAf6Reg_pla_lo_block_pro_stk_WidthVal                                                               32
#define cAf6Reg_pla_lo_block_pro_stk_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: PlaLoBlkAllCacheEmp
BitField Type: W2C
BitField Desc: LO all cache empty
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_Bit_Start                                             28
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_Bit_End                                               28
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_Mask                                              cBit28
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_Shift                                                 28
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_MaxVal                                               0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_MinVal                                               0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkAllCacheEmp_RstVal                                               0x0

/*--------------------------------------
BitField Name: PlaLoBlkMaxLengthErr
BitField Type: W2C
BitField Desc: LO Max Length Error
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_Bit_Start                                            26
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_Bit_End                                              26
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_Mask                                             cBit26
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_Shift                                                26
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_MaxVal                                              0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_MinVal                                              0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkMaxLengthErr_RstVal                                              0x0

/*--------------------------------------
BitField Name: PlaLoDDrMuxRdFull
BitField Type: W2C
BitField Desc: LO DDR Mux Read Full
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_Bit_Start                                               25
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_Bit_End                                                 25
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_Mask                                                cBit25
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_Shift                                                   25
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDrMuxRdFull_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoDDRMuxWrFull
BitField Type: W2C
BitField Desc: LO DDR Mux Write Full
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_Bit_Start                                               24
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_Bit_End                                                 24
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_Mask                                                cBit24
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_Shift                                                   24
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxWrFull_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoDDRStaErr
BitField Type: W2C
BitField Desc: LO DDR Status Error
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_Bit_Start                                                  23
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_Bit_End                                                    23
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_Mask                                                   cBit23
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_Shift                                                      23
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_MaxVal                                                    0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_MinVal                                                    0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRStaErr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLoDDRBuf1stCaFul
BitField Type: W2C
BitField Desc: LO DDR Buffer 1st Cache Full
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_Bit_Start                                             22
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_Bit_End                                               22
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_Mask                                              cBit22
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_Shift                                                 22
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_MaxVal                                               0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_MinVal                                               0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf1stCaFul_RstVal                                               0x0

/*--------------------------------------
BitField Name: PlaLoDDRBuf2ndCaFul
BitField Type: W2C
BitField Desc: LO DDR Buffer 2nd Cache Full
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_Bit_Start                                             21
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_Bit_End                                               21
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_Mask                                              cBit21
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_Shift                                                 21
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_MaxVal                                               0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_MinVal                                               0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBuf2ndCaFul_RstVal                                               0x0

/*--------------------------------------
BitField Name: PlaLoDDRMuxAckFul
BitField Type: W2C
BitField Desc: LO DDR Mux Ack Full
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_Bit_Start                                               20
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_Bit_End                                                 20
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_Mask                                                cBit20
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_Shift                                                   20
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRMuxAckFul_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak7Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank7 Full
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_Bit_Start                                                 19
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_Bit_End                                                   19
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_Mask                                                  cBit19
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_Shift                                                     19
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak7Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak6Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank6 Full
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_Bit_Start                                                 18
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_Bit_End                                                   18
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_Mask                                                  cBit18
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_Shift                                                     18
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak6Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak5Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank5 Full
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_Bit_Start                                                 17
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_Bit_End                                                   17
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_Mask                                                  cBit17
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_Shift                                                     17
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak5Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak4Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank4 Full
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_Bit_Start                                                 16
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_Bit_End                                                   16
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_Mask                                                  cBit16
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_Shift                                                     16
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak4Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak3Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank3 Full
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_Bit_Start                                                 15
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_Bit_End                                                   15
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_Mask                                                  cBit15
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_Shift                                                     15
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak3Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak2Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank2 Full
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_Bit_Start                                                 14
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_Bit_End                                                   14
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_Mask                                                  cBit14
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_Shift                                                     14
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak2Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak1Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank1 Full
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_Bit_Start                                                 13
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_Bit_End                                                   13
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_Mask                                                  cBit13
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_Shift                                                     13
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak1Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoDDRBak0Ful
BitField Type: W2C
BitField Desc: LO DDR Buffer Bank0 Full
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_Bit_Start                                                 12
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_Bit_End                                                   12
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_Mask                                                  cBit12
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_Shift                                                     12
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoDDRBak0Ful_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoBlkOutVld
BitField Type: W2C
BitField Desc: LO Block output valid
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_Bit_Start                                                  11
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_Bit_End                                                    11
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_Mask                                                   cBit11
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_Shift                                                      11
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_MaxVal                                                    0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_MinVal                                                    0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkOutVld_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLoBlkCa256Same
BitField Type: W2C
BitField Desc: LO Block Cache256 same
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_Bit_Start                                               10
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_Bit_End                                                 10
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_Mask                                                cBit10
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_Shift                                                   10
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Same_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoBlkCa128Same
BitField Type: W2C
BitField Desc: LO Block Cache128 same
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_Bit_Start                                                9
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_Bit_End                                                  9
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_Mask                                                 cBit9
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_Shift                                                    9
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Same_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoBlkCa64Same
BitField Type: W2C
BitField Desc: LO Block Cache64 same
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_Bit_Start                                                 8
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_Bit_End                                                   8
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_Mask                                                  cBit8
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_Shift                                                     8
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_MaxVal                                                  0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_MinVal                                                  0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Same_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLoBlkPkInfoVld
BitField Type: W2C
BitField Desc: LO Block Pkt Info valid
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_Bit_Start                                                7
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_Bit_End                                                  7
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_Mask                                                 cBit7
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_Shift                                                    7
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkPkInfoVld_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoBlkCa256Emp
BitField Type: W2C
BitField Desc: LO Block Cache256 Empty
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_Bit_Start                                                 6
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_Bit_End                                                   6
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_Mask                                                  cBit6
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_Shift                                                     6
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_MaxVal                                                  0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_MinVal                                                  0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa256Emp_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLoBlkCa128Emp
BitField Type: W2C
BitField Desc: LO Block Cache128 Empty
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_Bit_Start                                                 5
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_Bit_End                                                   5
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_Mask                                                  cBit5
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_Shift                                                     5
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_MaxVal                                                  0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_MinVal                                                  0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa128Emp_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLoBlkCa64Emp
BitField Type: W2C
BitField Desc: LO Block Cache64 Empty
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_Bit_Start                                                  4
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_Bit_End                                                    4
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_Mask                                                   cBit4
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_Shift                                                      4
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_MaxVal                                                   0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_MinVal                                                   0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkCa64Emp_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaLoBlkHiBlkEmp
BitField Type: W2C
BitField Desc: LO Block High Size Block Empty
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_Bit_Start                                                 3
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_Bit_End                                                   3
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_Mask                                                  cBit3
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_Shift                                                     3
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_MaxVal                                                  0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_MinVal                                                  0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkEmp_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLoBlkLoBlkEmp
BitField Type: W2C
BitField Desc: LO Block Low Size Block Empty
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_Bit_Start                                                 2
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_Bit_End                                                   2
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_Mask                                                  cBit2
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_Shift                                                     2
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_MaxVal                                                  0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_MinVal                                                  0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkEmp_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLoBlkHiBlkSame
BitField Type: W2C
BitField Desc: LO Block High Size Block same
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_Bit_Start                                                1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_Bit_End                                                  1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_Mask                                                 cBit1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_Shift                                                    1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkHiBlkSame_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaLoBlkLoBlkSame
BitField Type: W2C
BitField Desc: LO Block Low Size Block same
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_Bit_Start                                                0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_Bit_End                                                  0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_Mask                                                 cBit0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_Shift                                                    0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_MaxVal                                                 0x1
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_MinVal                                                 0x0
#define cAf6_pla_lo_block_pro_stk_PlaLoBlkLoBlkSame_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Interface Sticky
Reg Addr   : 0x8_8002
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some alarms for debug low-order block processing

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_interface_stk_Base                                                                 0x88002
#define cAf6Reg_pla_interface_stk                                                                      0x88002
#define cAf6Reg_pla_interface_stk_WidthVal                                                                  32
#define cAf6Reg_pla_interface_stk_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaPort4QueRdy
BitField Type: W2C
BitField Desc: Port4 output Queue Pkt ready
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort4QueRdy_Bit_Start                                                     31
#define cAf6_pla_interface_stk_PlaPort4QueRdy_Bit_End                                                       31
#define cAf6_pla_interface_stk_PlaPort4QueRdy_Mask                                                      cBit31
#define cAf6_pla_interface_stk_PlaPort4QueRdy_Shift                                                         31
#define cAf6_pla_interface_stk_PlaPort4QueRdy_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort4QueRdy_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort4QueRdy_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort4QueAck
BitField Type: W2C
BitField Desc: Port4 output Queue Pkt ack
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort4QueAck_Bit_Start                                                     30
#define cAf6_pla_interface_stk_PlaPort4QueAck_Bit_End                                                       30
#define cAf6_pla_interface_stk_PlaPort4QueAck_Mask                                                      cBit30
#define cAf6_pla_interface_stk_PlaPort4QueAck_Shift                                                         30
#define cAf6_pla_interface_stk_PlaPort4QueAck_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort4QueAck_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort4QueAck_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort3QueRdy
BitField Type: W2C
BitField Desc: Port3 output Queue Pkt ready
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort3QueRdy_Bit_Start                                                     29
#define cAf6_pla_interface_stk_PlaPort3QueRdy_Bit_End                                                       29
#define cAf6_pla_interface_stk_PlaPort3QueRdy_Mask                                                      cBit29
#define cAf6_pla_interface_stk_PlaPort3QueRdy_Shift                                                         29
#define cAf6_pla_interface_stk_PlaPort3QueRdy_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort3QueRdy_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort3QueRdy_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort3QueAck
BitField Type: W2C
BitField Desc: Port3 output Queue Pkt ack
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort3QueAck_Bit_Start                                                     28
#define cAf6_pla_interface_stk_PlaPort3QueAck_Bit_End                                                       28
#define cAf6_pla_interface_stk_PlaPort3QueAck_Mask                                                      cBit28
#define cAf6_pla_interface_stk_PlaPort3QueAck_Shift                                                         28
#define cAf6_pla_interface_stk_PlaPort3QueAck_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort3QueAck_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort3QueAck_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort2QueRdy
BitField Type: W2C
BitField Desc: Port2 output Queue Pkt ready
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort2QueRdy_Bit_Start                                                     27
#define cAf6_pla_interface_stk_PlaPort2QueRdy_Bit_End                                                       27
#define cAf6_pla_interface_stk_PlaPort2QueRdy_Mask                                                      cBit27
#define cAf6_pla_interface_stk_PlaPort2QueRdy_Shift                                                         27
#define cAf6_pla_interface_stk_PlaPort2QueRdy_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort2QueRdy_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort2QueRdy_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort2QueAck
BitField Type: W2C
BitField Desc: Port2 output Queue Pkt ack
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort2QueAck_Bit_Start                                                     26
#define cAf6_pla_interface_stk_PlaPort2QueAck_Bit_End                                                       26
#define cAf6_pla_interface_stk_PlaPort2QueAck_Mask                                                      cBit26
#define cAf6_pla_interface_stk_PlaPort2QueAck_Shift                                                         26
#define cAf6_pla_interface_stk_PlaPort2QueAck_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort2QueAck_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort2QueAck_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort1QueRdy
BitField Type: W2C
BitField Desc: Port1 output Queue Pkt ready
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort1QueRdy_Bit_Start                                                     25
#define cAf6_pla_interface_stk_PlaPort1QueRdy_Bit_End                                                       25
#define cAf6_pla_interface_stk_PlaPort1QueRdy_Mask                                                      cBit25
#define cAf6_pla_interface_stk_PlaPort1QueRdy_Shift                                                         25
#define cAf6_pla_interface_stk_PlaPort1QueRdy_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort1QueRdy_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort1QueRdy_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPort1QueAck
BitField Type: W2C
BitField Desc: Port1 output Queue Pkt ack
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPort1QueAck_Bit_Start                                                     24
#define cAf6_pla_interface_stk_PlaPort1QueAck_Bit_End                                                       24
#define cAf6_pla_interface_stk_PlaPort1QueAck_Mask                                                      cBit24
#define cAf6_pla_interface_stk_PlaPort1QueAck_Shift                                                         24
#define cAf6_pla_interface_stk_PlaPort1QueAck_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPort1QueAck_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPort1QueAck_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPSNReqQdr
BitField Type: W2C
BitField Desc: Request PSN to QDR
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPSNReqQdr_Bit_Start                                                       22
#define cAf6_pla_interface_stk_PlaPSNReqQdr_Bit_End                                                         22
#define cAf6_pla_interface_stk_PlaPSNReqQdr_Mask                                                        cBit22
#define cAf6_pla_interface_stk_PlaPSNReqQdr_Shift                                                           22
#define cAf6_pla_interface_stk_PlaPSNReqQdr_MaxVal                                                         0x1
#define cAf6_pla_interface_stk_PlaPSNReqQdr_MinVal                                                         0x0
#define cAf6_pla_interface_stk_PlaPSNReqQdr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaPSNAckQdr
BitField Type: W2C
BitField Desc: Ack for PSN request
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPSNAckQdr_Bit_Start                                                       21
#define cAf6_pla_interface_stk_PlaPSNAckQdr_Bit_End                                                         21
#define cAf6_pla_interface_stk_PlaPSNAckQdr_Mask                                                        cBit21
#define cAf6_pla_interface_stk_PlaPSNAckQdr_Shift                                                           21
#define cAf6_pla_interface_stk_PlaPSNAckQdr_MaxVal                                                         0x1
#define cAf6_pla_interface_stk_PlaPSNAckQdr_MinVal                                                         0x0
#define cAf6_pla_interface_stk_PlaPSNAckQdr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaPSNVldQdr
BitField Type: W2C
BitField Desc: Valid for PSN request
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPSNVldQdr_Bit_Start                                                       20
#define cAf6_pla_interface_stk_PlaPSNVldQdr_Bit_End                                                         20
#define cAf6_pla_interface_stk_PlaPSNVldQdr_Mask                                                        cBit20
#define cAf6_pla_interface_stk_PlaPSNVldQdr_Shift                                                           20
#define cAf6_pla_interface_stk_PlaPSNVldQdr_MaxVal                                                         0x1
#define cAf6_pla_interface_stk_PlaPSNVldQdr_MinVal                                                         0x0
#define cAf6_pla_interface_stk_PlaPSNVldQdr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaPkInfReqQdrLenWrErr
BitField Type: W2C
BitField Desc: Request write pkt info len err
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_Bit_Start                                             19
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_Bit_End                                               19
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_Mask                                              cBit19
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_Shift                                                 19
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_MaxVal                                               0x1
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_MinVal                                               0x0
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenWrErr_RstVal                                               0x0

/*--------------------------------------
BitField Name: PlaPkInfReqQdr
BitField Type: W2C
BitField Desc: Request Pkt info to QDR
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_Bit_Start                                                     18
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_Bit_End                                                       18
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_Mask                                                      cBit18
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_Shift                                                         18
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPkInfReqQdr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPkInfAckQdr
BitField Type: W2C
BitField Desc: Ack for pkt info request
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_Bit_Start                                                     17
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_Bit_End                                                       17
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_Mask                                                      cBit17
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_Shift                                                         17
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPkInfAckQdr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPkInfVldQdr
BitField Type: W2C
BitField Desc: Valid for pkt info request
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_Bit_Start                                                     16
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_Bit_End                                                       16
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_Mask                                                      cBit16
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_Shift                                                         16
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaPkInfVldQdr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaPkInfReqQdrLenRdErr
BitField Type: W2C
BitField Desc: Request read pkt info len err
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_Bit_Start                                             15
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_Bit_End                                               15
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_Mask                                              cBit15
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_Shift                                                 15
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_MaxVal                                               0x1
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_MinVal                                               0x0
#define cAf6_pla_interface_stk_PlaPkInfReqQdrLenRdErr_RstVal                                               0x0

/*--------------------------------------
BitField Name: PlaRdReqDdr
BitField Type: W2C
BitField Desc: Request read data to DDR
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaRdReqDdr_Bit_Start                                                        14
#define cAf6_pla_interface_stk_PlaRdReqDdr_Bit_End                                                          14
#define cAf6_pla_interface_stk_PlaRdReqDdr_Mask                                                         cBit14
#define cAf6_pla_interface_stk_PlaRdReqDdr_Shift                                                            14
#define cAf6_pla_interface_stk_PlaRdReqDdr_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaRdReqDdr_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaRdReqDdr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaRdAckDdr
BitField Type: W2C
BitField Desc: Ack for read data request
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaRdAckDdr_Bit_Start                                                        13
#define cAf6_pla_interface_stk_PlaRdAckDdr_Bit_End                                                          13
#define cAf6_pla_interface_stk_PlaRdAckDdr_Mask                                                         cBit13
#define cAf6_pla_interface_stk_PlaRdAckDdr_Shift                                                            13
#define cAf6_pla_interface_stk_PlaRdAckDdr_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaRdAckDdr_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaRdAckDdr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaRdVldDdr
BitField Type: W2C
BitField Desc: Valid for read data request
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaRdVldDdr_Bit_Start                                                        12
#define cAf6_pla_interface_stk_PlaRdVldDdr_Bit_End                                                          12
#define cAf6_pla_interface_stk_PlaRdVldDdr_Mask                                                         cBit12
#define cAf6_pla_interface_stk_PlaRdVldDdr_Shift                                                            12
#define cAf6_pla_interface_stk_PlaRdVldDdr_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaRdVldDdr_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaRdVldDdr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP1PWEEoPErr
BitField Type: W2C
BitField Desc: PWE interface end packet err
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_Bit_Start                                                     11
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_Bit_End                                                       11
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_Mask                                                      cBit11
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_Shift                                                         11
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_MaxVal                                                       0x1
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_MinVal                                                       0x0
#define cAf6_pla_interface_stk_PlaP1PWEEoPErr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaWrReqDdr
BitField Type: W2C
BitField Desc: Request write data to DDR
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaWrReqDdr_Bit_Start                                                        10
#define cAf6_pla_interface_stk_PlaWrReqDdr_Bit_End                                                          10
#define cAf6_pla_interface_stk_PlaWrReqDdr_Mask                                                         cBit10
#define cAf6_pla_interface_stk_PlaWrReqDdr_Shift                                                            10
#define cAf6_pla_interface_stk_PlaWrReqDdr_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaWrReqDdr_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaWrReqDdr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaWrAckDdr
BitField Type: W2C
BitField Desc: Ack for write data request
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaWrAckDdr_Bit_Start                                                         9
#define cAf6_pla_interface_stk_PlaWrAckDdr_Bit_End                                                           9
#define cAf6_pla_interface_stk_PlaWrAckDdr_Mask                                                          cBit9
#define cAf6_pla_interface_stk_PlaWrAckDdr_Shift                                                             9
#define cAf6_pla_interface_stk_PlaWrAckDdr_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaWrAckDdr_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaWrAckDdr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaWrVldDdr
BitField Type: W2C
BitField Desc: Valid for write data request
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaWrVldDdr_Bit_Start                                                         8
#define cAf6_pla_interface_stk_PlaWrVldDdr_Bit_End                                                           8
#define cAf6_pla_interface_stk_PlaWrVldDdr_Mask                                                          cBit8
#define cAf6_pla_interface_stk_PlaWrVldDdr_Shift                                                             8
#define cAf6_pla_interface_stk_PlaWrVldDdr_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaWrVldDdr_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaWrVldDdr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP4PWEReq
BitField Type: W2C
BitField Desc: Port4 PLA ready packet for PWE
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP4PWEReq_Bit_Start                                                         7
#define cAf6_pla_interface_stk_PlaP4PWEReq_Bit_End                                                           7
#define cAf6_pla_interface_stk_PlaP4PWEReq_Mask                                                          cBit7
#define cAf6_pla_interface_stk_PlaP4PWEReq_Shift                                                             7
#define cAf6_pla_interface_stk_PlaP4PWEReq_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP4PWEReq_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP4PWEReq_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP4PWEGet
BitField Type: W2C
BitField Desc: Port4 PWE get packet
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP4PWEGet_Bit_Start                                                         6
#define cAf6_pla_interface_stk_PlaP4PWEGet_Bit_End                                                           6
#define cAf6_pla_interface_stk_PlaP4PWEGet_Mask                                                          cBit6
#define cAf6_pla_interface_stk_PlaP4PWEGet_Shift                                                             6
#define cAf6_pla_interface_stk_PlaP4PWEGet_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP4PWEGet_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP4PWEGet_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP3PWEReq
BitField Type: W2C
BitField Desc: Port3 PLA ready packet for PWE
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP3PWEReq_Bit_Start                                                         5
#define cAf6_pla_interface_stk_PlaP3PWEReq_Bit_End                                                           5
#define cAf6_pla_interface_stk_PlaP3PWEReq_Mask                                                          cBit5
#define cAf6_pla_interface_stk_PlaP3PWEReq_Shift                                                             5
#define cAf6_pla_interface_stk_PlaP3PWEReq_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP3PWEReq_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP3PWEReq_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP3PWEGet
BitField Type: W2C
BitField Desc: Port3 PWE get packet
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP3PWEGet_Bit_Start                                                         4
#define cAf6_pla_interface_stk_PlaP3PWEGet_Bit_End                                                           4
#define cAf6_pla_interface_stk_PlaP3PWEGet_Mask                                                          cBit4
#define cAf6_pla_interface_stk_PlaP3PWEGet_Shift                                                             4
#define cAf6_pla_interface_stk_PlaP3PWEGet_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP3PWEGet_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP3PWEGet_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP2PWEReq
BitField Type: W2C
BitField Desc: Port2 PLA ready packet for PWE
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP2PWEReq_Bit_Start                                                         3
#define cAf6_pla_interface_stk_PlaP2PWEReq_Bit_End                                                           3
#define cAf6_pla_interface_stk_PlaP2PWEReq_Mask                                                          cBit3
#define cAf6_pla_interface_stk_PlaP2PWEReq_Shift                                                             3
#define cAf6_pla_interface_stk_PlaP2PWEReq_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP2PWEReq_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP2PWEReq_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP2PWEGet
BitField Type: W2C
BitField Desc: Port2 PWE get packet
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP2PWEGet_Bit_Start                                                         2
#define cAf6_pla_interface_stk_PlaP2PWEGet_Bit_End                                                           2
#define cAf6_pla_interface_stk_PlaP2PWEGet_Mask                                                          cBit2
#define cAf6_pla_interface_stk_PlaP2PWEGet_Shift                                                             2
#define cAf6_pla_interface_stk_PlaP2PWEGet_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP2PWEGet_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP2PWEGet_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP1PWEReq
BitField Type: W2C
BitField Desc: Port1 PLA ready packet for PWE
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP1PWEReq_Bit_Start                                                         1
#define cAf6_pla_interface_stk_PlaP1PWEReq_Bit_End                                                           1
#define cAf6_pla_interface_stk_PlaP1PWEReq_Mask                                                          cBit1
#define cAf6_pla_interface_stk_PlaP1PWEReq_Shift                                                             1
#define cAf6_pla_interface_stk_PlaP1PWEReq_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP1PWEReq_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP1PWEReq_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaP1PWEGet
BitField Type: W2C
BitField Desc: Port1 PWE get packet
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_interface_stk_PlaP1PWEGet_Bit_Start                                                         0
#define cAf6_pla_interface_stk_PlaP1PWEGet_Bit_End                                                           0
#define cAf6_pla_interface_stk_PlaP1PWEGet_Mask                                                          cBit0
#define cAf6_pla_interface_stk_PlaP1PWEGet_Shift                                                             0
#define cAf6_pla_interface_stk_PlaP1PWEGet_MaxVal                                                          0x1
#define cAf6_pla_interface_stk_PlaP1PWEGet_MinVal                                                          0x0
#define cAf6_pla_interface_stk_PlaP1PWEGet_RstVal                                                          0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _PWE_THA60210011MODULEPLADEBUGREG_H_ */


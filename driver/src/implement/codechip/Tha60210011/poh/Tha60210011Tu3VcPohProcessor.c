/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210011Tu3VcPohProcessor.c
 *
 * Created Date: May 26, 2015
 *
 * Description : TU3 VC POH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "Tha60210011PohReg.h"
#include "Tha60210011PohProcessorInternal.h"
#include "Tha60210011ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModulePoh(channel) ModulePoh((AtChannel)channel)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods                 m_AtSdhChannelOverride;
static tAtSdhPathMethods                    m_AtSdhPathOverride;
static tThaDefaultPohProcessorV2Methods     m_ThaDefaultPohProcessorV2Override;
static tThaAuVcPohProcessorV2Methods        m_ThaAuVcPohProcessorV2Override;
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/* Save super implementation */
static const tAtSdhChannelMethods          *m_AtSdhChannelMethods = NULL;
static const tThaAuVcPohProcessorV2Methods *m_ThaAuVcPohProcessorV2Methods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(AtChannel self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePoh);
    }

static uint32 PohCpeStsTu3DefaultOffset(ThaAuVcPohProcessorV2 self)
    {
    return m_ThaAuVcPohProcessorV2Methods->PohCpeStsTu3DefaultOffset(self) + 1;
    }

static uint32 OverheadByteInsertBufferRegister(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_rtlpohccterbuflo_Base;
    }

static uint32 PohCpeStsTu3PathOhGrabber1(ThaAuVcPohProcessorV2 self)
    {
    return ThaModulePohVtpohgrbBase(mModulePoh(self), mVcGet(self));
    }

static uint32 PohExpectedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgvtexp_Base;
    }

static uint32 PohCapturedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    return ThaModulePohMsgvtcurBase(mModulePoh(self));
    }

static uint32 PohTxTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    return ThaModulePohMsgvtinsBase(mModulePoh(self));
    }

static uint32 InsertBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_rtlpohccterbuflo(mModulePoh(self), hwSts, slice, 0, 0);

    return 0;
    }

static uint32 TerminateInsertControlRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_ter_ctrllo(mModulePoh(self), hwSts, slice, 0);

    return 0;
    }

static uint32 TerminateInsertControlRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_ter_ctrllo_Base;
    }

static uint32 TtiBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_pohmsgvtins(mModulePoh(self), slice, hwSts, 0, 0);

    return 0;
    }

static uint32 ExpTtiBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    return TtiBufferRegOffset(self);
    }

static uint32 PohCpeStsTu3PathOhGrabberRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cAtModuleSdh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohvtpohgrb(mModulePoh(self), slice, hwStsInSlice, 0);

    return 0;
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regAddr;
    uint32 regVal[cThaLongRegMaxSize];
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;
    Tha60210011AuVcPohProcessor vcProcessor = (Tha60210011AuVcPohProcessor)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return 0;

    regAddr = mMethodsGet((ThaAuVcPohProcessorV2)self)->PohCpeStsTu3PathOhGrabber1((ThaAuVcPohProcessorV2)self) +
              mMethodsGet(vcProcessor)->PohCpeStsTu3PathOhGrabberRegOffset(vcProcessor) + 1;
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteF2:
            return (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_F2_);
        case cAtSdhPathOverheadByteF3:
            return (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_F3_);
        case cAtSdhPathOverheadByteH4:
            return (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_H4_);
        case cAtSdhPathOverheadByteK3:
            return (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_K3_);
        default:
            return m_AtSdhChannelMethods->RxOverheadByteGet(self, overheadByte);
        }
    }

static uint32 PohStatusReportRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohAlmStaloBase(mModulePoh(self));
    }

static uint32 PohCounterReportRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohIpmCntloBase(mModulePoh(self));
    }

static uint32 PohInterruptStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohAlmChgloBase(mModulePoh(self));
    }

static uint32 PohInterruptMaskRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_alm_msklo_Base;
    }

static uint32 PohInterruptRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        {
        uint32 offset = Tha60210011ModulePohLoPathInterruptOffset((Tha60210011ModulePoh)mModulePoh(self), slice, hwSts, 0, 0);
        return offset + Tha60210011ModulePohBaseAddress(mModulePoh(self));
        }

    return 0;
    }

static uint32 RfiLomDefectGet(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 UpsrHoldOffRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_upen_xxcfgtmv_Base;
    }

static uint32 UpsrHoldOffRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 lineId = AtSdhChannelLineGet(mVcGet(self));
    uint8 stsId = AtSdhChannelSts1Get(mVcGet(self));
    uint32 sts = Tha60210011ModuleSdhUpsrStsFromTfi5SwStsGet(lineId, stsId);

    return Tha60210011ModulePohBaseAddress(mModulePoh(self)) + sts * 32U;
    }

static uint32 AisDownstreamAtGrabberWord(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 1;
    }

static void InterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    AtChannel vc = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 pohBaseAddress = Tha60210011ModulePohBaseAddress(mModulePoh(self));
    uint32 intrAddress = pohBaseAddress + (uint32)cAf6Reg_alm_chglo(sts, slice, vt);
    uint32 intrMask = AtHalRead(hal, pohBaseAddress + (uint32)cAf6Reg_alm_msklo(sts, slice, vt));
    uint32 intrStatus = AtHalRead(hal, intrAddress);
    uint32 currentStatus = AtHalRead(hal, pohBaseAddress + (uint32)cAf6Reg_alm_stalo(sts, slice, vt));
    uint32 events = 0;
    uint32 defects = 0;

    intrStatus &= intrMask;

    /* TU-AIS */
    if (intrStatus & cAf6_alm_msklo_aismsk_Mask)
        {
        events |= cAtSdhPathAlarmAis;
        if (currentStatus & cAf6_alm_stalo_aissta_Mask)
            defects |= cAtSdhPathAlarmAis;
        }

    /* TU-LOP */
    if (intrStatus & cAf6_alm_msklo_lopmsk_Mask)
        {
        events |= cAtSdhPathAlarmLop;
        if (currentStatus & cAf6_alm_stalo_lopsta_Mask)
            defects |= cAtSdhPathAlarmLop;
        }

    /* LP-RDI/ERDI */
    if (intrStatus & cAf6_alm_msklo_erdimsk_Mask)
        {
        Tha60210011AuVcPohProcessor pohProcessor = (Tha60210011AuVcPohProcessor)self;
        events |= cAtSdhPathAlarmRdi;
        defects |= mMethodsGet(pohProcessor)->ErdiDefectGet(pohProcessor, currentStatus);
        }

    /* LP-TIM */
    if (intrStatus & cAf6_alm_msklo_timmsk_Mask)
        {
        events |= cAtSdhPathAlarmTim;
        if (currentStatus & cAf6_alm_stalo_timsta_Mask)
            defects |= cAtSdhPathAlarmTim;
        }

    /* LP-UNEQ */
    if (intrStatus & cAf6_alm_msklo_uneqmsk_Mask)
        {
        events |= cAtSdhPathAlarmUneq;
        if (currentStatus & cAf6_alm_stalo_uneqsta_Mask)
            defects |= cAtSdhPathAlarmUneq;
        }

    /* LP-PLM */
    if (intrStatus & cAf6_alm_msklo_plmmsk_Mask)
        {
        events |= cAtSdhPathAlarmPlm;
        if (currentStatus & cAf6_alm_stalo_plmsta_Mask)
            defects |= cAtSdhPathAlarmPlm;
        }

    /* LP-RFI (available for VC11) */
    if (intrStatus & cAf6_alm_msklo_rfimsk_Mask)
        {
        events |= cAtSdhPathAlarmRfi;
        if (currentStatus & cAf6_alm_stalo_rfista_Mask)
            defects |= cAtSdhPathAlarmRfi;
        }

    /* LP-BER-TCA */
    if (intrStatus & cAf6_alm_msklo_bertcamsk_Mask)
        {
        events |= cAtSdhPathAlarmBerTca;
        if (currentStatus & cAf6_alm_stalo_bertcasta_Mask)
            defects |= cAtSdhPathAlarmBerTca;
        }

    /* LP-BER-SD */
    if (intrStatus & cAf6_alm_msklo_bersdmsk_Mask)
        {
        events |= cAtSdhPathAlarmBerSd;
        if (currentStatus & cAf6_alm_stalo_bersdsta_Mask)
            defects |= cAtSdhPathAlarmBerSd;
        }

    /* LP-BER-SF */
    if (intrStatus & cAf6_alm_msklo_bersfmsk_Mask)
        {
        events |= cAtSdhPathAlarmBerSf;
        if (currentStatus & cAf6_alm_stalo_bersfsta_Mask)
            defects |= cAtSdhPathAlarmBerSf;
        }

    /* Clear interrupt */
    AtHalWrite(hal, intrAddress, intrStatus);

    AtChannelAllAlarmListenersCall(vc, events, defects);
    }

static uint32 CounterRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_ipm_cntlo(mModulePoh(self), hwSts, slice, 0);

    return 0;
    }

static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0x2C000;
    }

static uint32 PohCpeStatusRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohcpevtsta(mModulePoh(self), slice, hwStsInSlice, 0);

    return 0;
    }

static Tha60210011ModuleOcn OcnModule(Tha60210011AuVcPohProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (Tha60210011ModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eBool HasPayloadUneqDefect(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    uint32 offset = Tha60210011ModuleOcnVtDefaultOffset(vc);
    uint32 regAddr = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAlarmStatus(OcnModule(self), vc) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask)
        return cAtTrue;
    return cAtFalse;
    }

static eBool HasPayloadUneqSticky(Tha60210011AuVcPohProcessor self, eBool r2c)
    {
    eBool hasSticky = cAtFalse;
    AtSdhChannel vc = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    uint32 offset = Tha60210011ModuleOcnVtDefaultOffset(vc);
    uint32 regAddr = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelInterrupt(OcnModule(self), vc) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask)
        hasSticky = cAtTrue;

    if (r2c)
        mChannelHwWrite(self, regAddr, cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask, cThaModuleOcn);
    return hasSticky;
    }

static uint32 VpgramctlBase(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    return Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(OcnModule(self), sdhChannel);
    }

static eAtRet TxUneqForce(Tha60210011AuVcPohProcessor self, eBool enable)
    {
	AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    uint32 regAddr = VpgramctlBase(self) + Tha60210011ModuleOcnVtDefaultOffset(sdhChannel);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_vpgramctl_VtPgUeqFrc_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool TxUneqIsForced(Tha60210011AuVcPohProcessor self)
    {
	AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    uint32 regAddr = VpgramctlBase(self) + Tha60210011ModuleOcnVtDefaultOffset(sdhChannel);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    return mBinToBool(mRegField(regVal, cAf6_vpgramctl_VtPgUeqFrc_));
    }

static void OverrideAtSdhChannel(ThaAuVcPohProcessorV2 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideThaDefaultPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));

        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiAddressGet);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void OverrideThaAuVcPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaAuVcPohProcessorV2Methods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAuVcPohProcessorV2Override, m_ThaAuVcPohProcessorV2Methods, sizeof(m_ThaAuVcPohProcessorV2Override));

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3DefaultOffset);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PathOhGrabber1);
        }

    mMethodsSet(self, &m_ThaAuVcPohProcessorV2Override);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, mMethodsGet(processor), sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, InsertBufferRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TerminateInsertControlRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TerminateInsertControlRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TtiBufferRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, ExpTtiBufferRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, OverheadByteInsertBufferRegister);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStsTu3PathOhGrabberRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohStatusReportRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCounterReportRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohInterruptStatusRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohInterruptMaskRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohInterruptRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, RfiLomDefectGet);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, UpsrHoldOffRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, UpsrHoldOffRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, CounterRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, AisDownstreamAtGrabberWord);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TxUneqForce);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TxUneqIsForced);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HasPayloadUneqDefect);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HasPayloadUneqSticky);
        }

    mMethodsSet(processor, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void OverrideAtSdhPath(ThaAuVcPohProcessorV2 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, InterruptProcess);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideAtSdhChannel(self);
    OverrideThaDefaultPohProcessorV2(self);
    OverrideThaAuVcPohProcessorV2(self);
    OverrideTha60210011AuVcPohProcessor(self);
    OverrideAtSdhPath(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tu3VcPohProcessor);
    }

ThaPohProcessor Tha60210011Tu3VcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaAuVcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210011Tu3VcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210011Tu3VcPohProcessorObjectInit(newProcessor, vc);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210011PohProcessor.h
 * 
 * Created Date: May 26, 2015
 *
 * Description : POH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011POHPROCESSOR_H_
#define _THA60210011POHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "../../../default/poh/ThaPohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011AuVcPohProcessor * Tha60210011AuVcPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha60210011AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210011Tu3VcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210011Vc1xPohProcessorNew(AtSdhVc vc);

uint32 Tha60210011Vc1xPohProcessorVtId(Tha60210011AuVcPohProcessor self);
uint32 Tha60210011AuVcProcessorPohDefectGet(Tha60210011AuVcPohProcessor self);
uint32 Tha60210011AuVcPohProcessorInterruptRead2Clear(Tha60210011AuVcPohProcessor self, eBool clear);

#endif /* _THA60210011POHPROCESSOR_H_ */


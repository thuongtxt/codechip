/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210011AuVcPohProcessor.c
 *
 * Created Date: May 26, 2015
 *
 * Description : POH implementation for AU VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../man/Tha60210011Device.h"
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "Tha60210011PohReg.h"
#include "Tha60210011PohProcessor.h"
#include "Tha60210011PohProcessorInternal.h"
#include "Tha60210011ModulePoh.h"
#include "../../../default/poh/ThaTtiProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* G1 sub fields */
#define cSpareBitMask  cBit0
#define cSpareBitShift 0

#define cJnMsgMask(byteIndex)  ((uint32) cBit7_0 << ((uint32)(cJnMsgShift(byteIndex))))
#define cJnMsgShift(byteIndex) (8 * (3 - (byteIndex)))

#define cTimer1000msUnit          1000UL
#define cTimer200msUnit           200UL
#define cTimer100msUnit           100UL
#define cTimer50msUnit            50UL
#define cTimer10msUnit            10UL
#define cTimer2msUnit             2UL

#define cTimer1000msMode          5
#define cTimer200msMode           4
#define cTimer100msMode           3
#define cTimer50msMode            2
#define cTimer10msMode            1
#define cTimer2msMode             0

#define cAf6_pohcpestsctr_J1MonEnb_Mask     cBit2
#define cAf6_pohcpestsctr_J1MonEnb_Shift    2

#define cAf6_pohcpestssta_JnBadFrame_Mask   cBit24
#define cAf6_pohcpestssta_JnBadFrame_Shift  24


#define cErdiDefectMask     cBit11_9
#define cErdiDefectShift    9

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011AuVcPohProcessor)self)
#define mModulePoh(channel) ModulePoh((AtChannel)channel)
#define mOcnModule(self) OcnModule((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011AuVcPohProcessorMethods m_methods;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tAtChannelMethods                m_AtChannelOverride;
static tAtSdhPathMethods                m_AtSdhPathOverride;
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;
static tThaPohProcessorMethods          m_ThaPohProcessorOverride;
static tThaAuVcPohProcessorV2Methods    m_ThaAuVcPohProcessorV2Override;

/* Save super implementation */
static const tAtObjectMethods          *m_AtObjectMethods  = NULL;
static const tAtChannelMethods         *m_AtChannelMethods = NULL;
static const tAtSdhChannelMethods      *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(AtChannel self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePoh);
    }

static eBool IsOneBitRdi(uint8 erdiValue)
    {
    return (erdiValue & cBit2) ? cAtTrue : cAtFalse;
    }

static uint32 ErdiTypeFromErdiValue(uint8 erdiValue, eBool erdiIsEnable)
    {
    /* E-RDI is not enabled, just only check one-bit RDI */
    if (!erdiIsEnable)
        return IsOneBitRdi(erdiValue) ? cAtSdhPathAlarmRdi : 0;

    /* ERDI Payload defect */
    if (erdiValue == cERDIPayloadValue)
        return cAtSdhPathAlarmErdiP;

    /* ERDI Connectivity defect */
    if (erdiValue == cERDIConnectivityValue)
        return cAtSdhPathAlarmErdiC;

    /* ERDI Server defect */
    if ((erdiValue & cBit2) || (erdiValue == cERDIServerValue))
        return cAtSdhPathAlarmErdiS;

    return 0;
    }

static uint32 ErdiDefectGet(Tha60210011AuVcPohProcessor self, uint32 regVal)
    {
    uint8 erdiVal;
    uint32 erdiType;
    eBool erdiEnabled = AtSdhPathERdiIsEnabled((AtSdhPath)self);

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh((AtChannel)self)))
        {
        uint8 g1Val;
        eBool oneBitRdiRaised = (regVal & cAf6_alm_stahi_erdista_Mask) ? cAtTrue : cAtFalse;

        /* Just simply return RDI if one bit RDI is used */
        if (!erdiEnabled)
            return oneBitRdiRaised ? cAtSdhPathAlarmRdi : 0;

        g1Val = AtSdhChannelRxOverheadByteGet((AtSdhChannel)self, cAtSdhPathOverheadByteG1);
        erdiVal = (g1Val >> 1) & cBit2_0;
        erdiType = ErdiTypeFromErdiValue(erdiVal, erdiEnabled);

        /* If HW returns that RDI is not raised but OH byte from grabber return ERDI-S/ERDI-C,
         * need to mask those alarms */
        if ((oneBitRdiRaised == cAtFalse) && ((erdiType == cAtSdhPathAlarmErdiS) || (erdiType == cAtSdhPathAlarmErdiC)))
            return 0;

        return erdiType;
        }
    else if (!Tha60210011VcPohProcessorInterruptIsSupported(self))
        erdiVal = (uint8)mRegField(regVal, cAf6_ipm_stahi_erdista_);
    else
        erdiVal = (uint8)mRegField(regVal, cErdiDefect);
    	
    return ErdiTypeFromErdiValue(erdiVal, erdiEnabled);
    }

static Tha60210011ModuleOcn OcnModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (Tha60210011ModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eBool HasPayloadUneqDefect(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    uint32 offset = Tha60210011ModuleOcnStsDefaultOffset(vc);
    uint32 regAddr = Tha60210011ModuleOcnStsPointerInterpreterPerChannelAlarmStatus(mOcnModule(self), vc) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask)
        return cAtTrue;

    return cAtFalse;
    }

static eBool HasPayloadUneqSticky(Tha60210011AuVcPohProcessor self, eBool r2c)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    eBool hasSticky = cAtFalse;
    uint32 offset = Tha60210011ModuleOcnStsDefaultOffset(vc);
    uint32 regAddr = Tha60210011ModuleOcnStsPointerInterpreterPerChannelInterrupt(mOcnModule(self), vc) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask)
        hasSticky = cAtTrue;

    if (r2c)
        mChannelHwWrite(self, regAddr, cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask, cThaModuleOcn);

    return hasSticky;
    }

static uint32 RfiLomDefectGet(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmLom;
    }

static uint32 NoInterruptDefectGet(Tha60210011AuVcPohProcessor self)
    {
    uint32 defects = 0;
    uint32 regAddr = mMethodsGet(mThis(self))->PohStatusReportRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->TerminateInsertControlRegOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    defects |= mMethodsGet(mThis(self))->ErdiDefectGet(mThis(self), regVal);
    if (regVal & cAf6_ipm_stahi_sdsta_Mask)
        defects |= cAtSdhPathAlarmBerSd;
    if (regVal & cAf6_ipm_stahi_sfsta_Mask)
        defects |= cAtSdhPathAlarmBerSf;

    if (regVal & cAf6_ipm_stahi_rfista_Mask)
        defects |= mMethodsGet(mThis(self))->RfiLomDefectGet(mThis(self));

    if (regVal & cAf6_ipm_stahi_timsta_Mask)
        defects |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_ipm_stahi_uneqsta_Mask)
        defects |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_ipm_stahi_plmsta_Mask)
        defects |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_ipm_stahi_aissta_Mask)
        defects |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_ipm_stahi_lopsta_Mask)
        defects |= cAtSdhPathAlarmLop;

    if (mMethodsGet(mThis(self))->HasPayloadUneqDefect(mThis(self)))
        defects |= cAtSdhPathAlarmPayloadUneq;

    /* Save stickies when get defects because HW puts defects
     * and stickies in one register with R2C mode */
    mThis(self)->stickyRegVal |= regVal;

    return defects;
    }

static uint32 PohDefectGet(Tha60210011AuVcPohProcessor self)
    {
    uint32 defects = 0;
    uint32 regAddr, regVal;

	if (!Tha60210011VcPohProcessorInterruptIsSupported(mThis(self)))
		return NoInterruptDefectGet(self);
		
	regAddr  = mMethodsGet(mThis(self))->PohStatusReportRegAddr(mThis(self)) +
               mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    regVal   = mChannelHwRead(self, regAddr, cThaModulePoh);
    defects |= mMethodsGet(mThis(self))->ErdiDefectGet(mThis(self), regVal);
    if (regVal & cAf6_alm_stahi_bertcasta_Mask)
        defects |= cAtSdhPathAlarmBerTca;
    if (regVal & cAf6_alm_stahi_rfista_Mask)
        defects |= mMethodsGet(mThis(self))->RfiLomDefectGet(mThis(self));
    if (regVal & cAf6_alm_stahi_timsta_Mask)
        defects |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_alm_stahi_uneqsta_Mask)
        defects |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_alm_stahi_plmsta_Mask)
        defects |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_alm_stahi_aissta_Mask)
        defects |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_alm_stahi_lopsta_Mask)
        defects |= cAtSdhPathAlarmLop;

    if (mMethodsGet(mThis(self))->HasPayloadUneqDefect(mThis(self)))
        defects |= cAtSdhPathAlarmPayloadUneq;

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh((AtChannel)self)) == cAtFalse)
        return defects;

    if (regVal & cAf6_alm_stahi_bersfsta_Mask)
        defects |= cAtSdhPathAlarmBerSf;
    if (regVal & cAf6_alm_stahi_bersdsta_Mask)
        defects |= cAtSdhPathAlarmBerSd;

    return defects;
    }

static uint32 NoInterruptDefectHistoryGet(AtChannel self)
    {
    uint32 stickies = 0;
    uint32 regAddr = mMethodsGet(mThis(self))->PohStatusReportRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->TerminateInsertControlRegOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    mThis(self)->stickyRegVal |= regVal;
    regVal = mThis(self)->stickyRegVal;

    if (regVal & cAf6_ipm_stahi_sdchgstk_Mask)
        stickies |= cAtSdhPathAlarmBerSd;
    if (regVal & cAf6_ipm_stahi_sfchgstk_Mask)
        stickies |= cAtSdhPathAlarmBerSf;
    if (regVal & cAf6_ipm_stahi_rfichgstk_Mask)
        stickies |= mMethodsGet(mThis(self))->RfiLomDefectGet(mThis(self));

    if (regVal & cAf6_ipm_stahi_rdichgstk_Mask)
        stickies |= cAtSdhPathAlarmRdi;
    if (regVal & cAf6_ipm_stahi_timchgstk_Mask)
        stickies |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_ipm_stahi_uneqchgstk_Mask)
        stickies |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_ipm_stahi_plmchgstk_Mask)
        stickies |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_ipm_stahi_aischgstk_Mask)
        stickies |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_ipm_stahi_lopchgstk_Mask)
        stickies |= cAtSdhPathAlarmLop;

    if (mMethodsGet(mThis(self))->HasPayloadUneqSticky(mThis(self), cAtFalse))
        stickies |= cAtSdhPathAlarmPayloadUneq;

    return stickies;
    }

static uint32 NoInterruptDefectHistoryClear(AtChannel self)
    {
    uint32 stickies = mMethodsGet(self)->DefectHistoryGet(self);
    mThis(self)->stickyRegVal = 0;

    if (mMethodsGet(mThis(self))->HasPayloadUneqSticky(mThis(self), cAtTrue))
        stickies |= cAtSdhPathAlarmPayloadUneq;
    return stickies;
    }

static uint32 InterruptRead2Clear(Tha60210011AuVcPohProcessor self, eBool clear)
    {
    uint32 stickies = 0;
    uint32 regAddr, regVal;

    if (!Tha60210011VcPohProcessorInterruptIsSupported(self))
        {
        if (clear)
            return NoInterruptDefectHistoryClear((AtChannel)self);
        else
            return NoInterruptDefectHistoryGet((AtChannel)self);
        }

    regAddr = mMethodsGet(self)->PohInterruptStatusRegAddr(self) +
              mMethodsGet(self)->PohInterruptRegOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_chghi_bertcastachg_Mask)
        stickies |= cAtSdhPathAlarmBerTca;
    if (regVal & cAf6_alm_chghi_rfistachg_Mask)
        stickies |= mMethodsGet(self)->RfiLomDefectGet(self);
    if (regVal & cAf6_alm_chghi_erdistachg_Mask)
        stickies |= cAtSdhPathAlarmRdi;
    if (regVal & cAf6_alm_chghi_timstachg_Mask)
        stickies |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_alm_chghi_uneqstachg_Mask)
        stickies |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_alm_chghi_plmstachg_Mask)
        stickies |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_alm_chghi_aisstachg_Mask)
        stickies |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_alm_chghi_lopstachg_Mask)
        stickies |= cAtSdhPathAlarmLop;

    if (mMethodsGet(self)->HasPayloadUneqSticky(self, clear))
        stickies |= cAtSdhPathAlarmPayloadUneq;

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh((AtChannel)self)))
        {
        if (regVal & cAf6_alm_chghi_bersfstachg_Mask)
            stickies |= cAtSdhPathAlarmBerSf;
        if (regVal & cAf6_alm_chghi_bersdstachg_Mask)
            stickies |= cAtSdhPathAlarmBerSd;
        }

    /* Clear all interrupts. */
    if (clear)
        mChannelHwWrite(self, regAddr, (uint32)-1, cThaModulePoh);

    return stickies;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return mMethodsGet(mThis(self))->InterruptRead2Clear(mThis(self), cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return mMethodsGet(mThis(self))->InterruptRead2Clear(mThis(self), cAtTrue);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
	uint32 mask = (cAtSdhPathAlarmBerTca |
	               cAtSdhPathAlarmRdi    |
	               cAtSdhPathAlarmErdiS  |
	               cAtSdhPathAlarmErdiC  |
	               cAtSdhPathAlarmErdiP  |
	               cAtSdhPathAlarmLom    |
	               cAtSdhPathAlarmTim    |
	               cAtSdhPathAlarmUneq   |
	               cAtSdhPathAlarmPlm    |
	               cAtSdhPathAlarmLop    |
	               cAtSdhPathAlarmAis);

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh(self)))
        {
		mask |= cAtSdhPathAlarmBerSf;
		mask |= cAtSdhPathAlarmBerSd;
        }

    return mask;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(mThis(self))->PohInterruptMaskRegAddr(mThis(self)) +
              mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
		
    if (defectMask & cAtSdhPathAlarmBerTca)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bertcamsk_, (enableMask & cAtSdhPathAlarmBerTca) ? 1 : 0);
    if (defectMask & (cAtSdhPathAlarmRdi | cAtSdhPathAlarmErdiC | cAtSdhPathAlarmErdiS | cAtSdhPathAlarmErdiP))
        mRegFieldSet(regVal, cAf6_alm_mskhi_erdimsk_, (enableMask & cAtSdhPathAlarmRdi) ? 1 : 0);

    if (defectMask & cAtSdhPathAlarmLom)
        mRegFieldSet(regVal, cAf6_alm_mskhi_rfimsk_, (enableMask & cAtSdhPathAlarmLom) ? 1 : 0);
    else if (defectMask & cAtSdhPathAlarmRfi)
        mRegFieldSet(regVal, cAf6_alm_mskhi_rfimsk_, (enableMask & cAtSdhPathAlarmRfi) ? 1 : 0);

    if (defectMask & cAtSdhPathAlarmTim)
        mRegFieldSet(regVal, cAf6_alm_mskhi_timmsk_, (enableMask & cAtSdhPathAlarmTim) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmUneq)
        mRegFieldSet(regVal, cAf6_alm_mskhi_uneqmsk_, (enableMask & cAtSdhPathAlarmUneq) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmPlm)
        mRegFieldSet(regVal, cAf6_alm_mskhi_plmmsk_, (enableMask & cAtSdhPathAlarmPlm) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmLop)
        mRegFieldSet(regVal, cAf6_alm_mskhi_lopmsk_, (enableMask & cAtSdhPathAlarmLop) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_alm_mskhi_aismsk_, (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh(self)))
        {
        if (defectMask & cAtSdhPathAlarmBerSf)
            mRegFieldSet(regVal, cAf6_alm_mskhi_bersfmsk_, (enableMask & cAtSdhPathAlarmBerSf) ? 1 : 0);
        if (defectMask & cAtSdhPathAlarmBerSd)
            mRegFieldSet(regVal, cAf6_alm_mskhi_bersdmsk_, (enableMask & cAtSdhPathAlarmBerSd) ? 1 : 0);
        }
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr, regVal, mask;

	if (!Tha60210011VcPohProcessorInterruptIsSupported(mThis(self)))
		return 0;
		
    regAddr = mMethodsGet(mThis(self))->PohInterruptMaskRegAddr(mThis(self)) +
              mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mask    = 0;

    if (regVal & cAf6_alm_mskhi_bertcamsk_Mask)
        mask |= cAtSdhPathAlarmBerTca;
    if (regVal & cAf6_alm_mskhi_erdimsk_Mask)
        mask |= cAtSdhPathAlarmRdi;
    if (regVal & cAf6_alm_mskhi_rfimsk_Mask)
        mask |= mMethodsGet(mThis(self))->RfiLomDefectGet(mThis(self));
    if (regVal & cAf6_alm_mskhi_timmsk_Mask)
        mask |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_alm_mskhi_uneqmsk_Mask)
        mask |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_alm_mskhi_plmmsk_Mask)
        mask |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_alm_mskhi_lopmsk_Mask)
        mask |= cAtSdhPathAlarmLop;
    if (regVal & cAf6_alm_mskhi_aismsk_Mask)
        mask |= cAtSdhPathAlarmAis;

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh(self)) == cAtFalse)
        return mask;

    if (regVal & cAf6_alm_mskhi_bersfmsk_Mask)
        mask |= cAtSdhPathAlarmBerSf;
    if (regVal & cAf6_alm_mskhi_bersdmsk_Mask)
        mask |= cAtSdhPathAlarmBerSd;

    return mask;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 SpgramctlBase(Tha60210011AuVcPohProcessor self)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    return Tha60210011ModuleOcnStsPointerGeneratorPerChannelControl((Tha60210011ModuleOcn)OcnModule((AtChannel)self), vc);
    }

static eAtRet TxUneqForce(Tha60210011AuVcPohProcessor self, eBool enable)
    {
    uint32 regAddr = SpgramctlBase(self) + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_spgramctl_StsPgUeqFrc_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxSupervisoryUneqForce(AtChannel self, eBool enable)
    {
    return ThaDefaultPohProcessorV2TxUneqForceByPsl((ThaDefaultPohProcessorV2)self, enable);
    }

static eBool TxUneqIsForced(Tha60210011AuVcPohProcessor self)
    {
    uint32 regAddr = SpgramctlBase(self) + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    return mBinToBool(mRegField(regVal, cAf6_spgramctl_StsPgUeqFrc_));
    }

static eAtRet HwTxAlarmForce(Tha60210011AuVcPohProcessor self, uint32 alarmType, eBool enable)
    {
    if (!mMethodsGet((ThaPohProcessor)self)->CanForceTxAlarms((ThaPohProcessor)self, alarmType))
        return cAtErrorModeNotSupport;

    if (alarmType & cAtSdhPathAlarmUneq)
        return mMethodsGet(self)->TxUneqForce(self, enable);

    if (alarmType & cAtSdhPathAlarmPayloadUneq)
        return TxSupervisoryUneqForce((AtChannel)self, enable);

    return cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return mMethodsGet(mThis(self))->HwTxAlarmForce(mThis(self), alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return mMethodsGet(mThis(self))->HwTxAlarmForce(mThis(self), alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarm = 0;

    if (((ThaDefaultPohProcessorV2)self)->pslCached == cAtTrue)
        forcedAlarm |= cAtSdhPathAlarmPayloadUneq;

    if (mMethodsGet(mThis(self))->TxUneqIsForced(mThis(self)))
        forcedAlarm |= cAtSdhPathAlarmUneq;

    return forcedAlarm;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtOk;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathErrorNone;
    }

static uint32 G1J1Index(void)
    {
    return 0;
    }

static uint32 N1C2Index(void)
    {
    return 1;
    }

static uint32 H4F2Index(void)
    {
    return 2;
    }

static uint32 K3F3Index(void)
    {
    return 3;
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OverheadByteInsertBufferRegister(mThis(self)) + N1C2Index() +
                     mMethodsGet(mThis(self))->InsertBufferRegOffset(mThis(self));
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0_, psl);
    mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0msk_, 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OverheadByteInsertBufferRegister(mThis(self)) + N1C2Index() +
                     mMethodsGet(mThis(self))->InsertBufferRegOffset(mThis(self));
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte0_);
    }

static eAtSdhTtiMode RxTtiModeGet(ThaDefaultPohProcessorV2 self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return ThaTtiProcessorTtiModeHw2Sw(mRegField(regVal, cAf6_pohcpestsctr_J1mode_));
    }

static eAtRet RxTtiModeSet(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode ttiMode)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    /* HW removed this field to use for REI block mode */
    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh((AtChannel)self)) == cAtFalse)
        mRegFieldSet(regVal, cAf6_pohcpestsctr_J1MonEnb_, 1);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_J1mode_, ThaTtiProcessorTtiModeSw2Hw(ttiMode));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3PathOhGrabber1(processor) +
            mMethodsGet(mThis(self))->PohCpeStsTu3PathOhGrabberRegOffset(mThis(self));
    uint32 regVal[cThaLongRegMaxSize];
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
    return (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_C2_);
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return (uint8)mRegField(regVal, cAf6_pohcpestsctr_PslExp_);
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_PslExp_, psl);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_TimEnb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpestsctr_TimEnb_));
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
    eAtModuleSdhRet ret;

    ret = ThaPohProcessorERdiMonitorEnable((ThaPohProcessor)self, enable);

    if (Tha60210011ModulePohOnebitRdiGenerationIsSupported((Tha60210011ModulePoh)ModulePoh((AtChannel)self)))
        ret |= ThaPohProcessorTxERdiEnable((ThaPohProcessor)self, enable);

    return ret;
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
    if (ThaPohProcessorERdiMonitorIsEnabled((ThaPohProcessor)self))
        return cAtTrue;

    if (Tha60210011ModulePohOnebitRdiGenerationIsSupported((Tha60210011ModulePoh)ModulePoh((AtChannel)self)))
        return ThaPohProcessorTxERdiIsEnabled((ThaPohProcessor)self);

    return cAtFalse;
    }

static eAtRet ERdiMonitorEnable(ThaPohProcessor self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_ERDIenb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool ERdiMonitorIsEnabled(ThaPohProcessor self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpestsctr_ERDIenb_));
    }

static eAtRet TxERdiEnable(ThaPohProcessor self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TxERdiIsEnabled(ThaPohProcessor self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void InterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    AtChannel au = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    uint32 pohBaseAddress = Tha60210011ModulePohBaseAddress(mModulePoh(self));
    uint32 intrAddress = pohBaseAddress + (uint32)cAf6Reg_alm_chghi(sts, slice);
    uint32 intrMask = AtHalRead(hal, pohBaseAddress + (uint32)cAf6Reg_alm_mskhi(sts, slice));
    uint32 intrStatus = AtHalRead(hal, intrAddress);
    uint32 currentStatus = AtHalRead(hal, pohBaseAddress + (uint32)cAf6Reg_alm_stahi(sts, slice));
    uint32 events = 0;
    uint32 defects = 0;
    AtUnused(vt);

    intrStatus &= intrMask;

    /* AU-AIS */
    if (intrStatus & cAf6_alm_mskhi_aismsk_Mask)
        {
        events |= cAtSdhPathAlarmAis;
        if (currentStatus & cAf6_alm_stahi_aissta_Mask)
            defects |= cAtSdhPathAlarmAis;
        }

    /* AU-LOP */
    if (intrStatus & cAf6_alm_mskhi_lopmsk_Mask)
        {
        events |= cAtSdhPathAlarmLop;
        if (currentStatus & cAf6_alm_stahi_lopsta_Mask)
            defects |= cAtSdhPathAlarmLop;
        }

    /* HP-TIM */
    if (intrStatus & cAf6_alm_mskhi_timmsk_Mask)
        {
        events |= cAtSdhPathAlarmTim;
        if (currentStatus & cAf6_alm_stahi_timsta_Mask)
            defects |= cAtSdhPathAlarmTim;
        }

    /* HP-RDI/ERDI */
    if (intrStatus & cAf6_alm_mskhi_erdimsk_Mask)
        {
        events |= cAtSdhPathAlarmRdi;
        defects |= mMethodsGet(mThis(self))->ErdiDefectGet(mThis(self), currentStatus);
        }

    /* HP-UNEQ */
    if (intrStatus & cAf6_alm_mskhi_uneqmsk_Mask)
        {
        events |= cAtSdhPathAlarmUneq;
        if (currentStatus & cAf6_alm_stahi_uneqsta_Mask)
            defects |= cAtSdhPathAlarmUneq;
        }

    /* HP-PLM */
    if (intrStatus & cAf6_alm_mskhi_plmmsk_Mask)
        {
        events |= cAtSdhPathAlarmPlm;
        if (currentStatus & cAf6_alm_stahi_plmsta_Mask)
            defects |= cAtSdhPathAlarmPlm;
        }

    /* HP-LOM (shared with RFI) */
    if (intrStatus & cAf6_alm_mskhi_rfimsk_Mask)
        {
        events |= cAtSdhPathAlarmLom;
        if (currentStatus & cAf6_alm_stahi_rfista_Mask)
            defects |= cAtSdhPathAlarmLom;
        }

    /* HP-BER-TCA */
    if (intrStatus & cAf6_alm_mskhi_bertcamsk_Mask)
        {
        events |= cAtSdhPathAlarmBerTca;
        if (currentStatus & cAf6_alm_stahi_bertcasta_Mask)
            defects |= cAtSdhPathAlarmBerTca;
        }

    /* HP-BER-SD */
    if (intrStatus & cAf6_alm_mskhi_bersdmsk_Mask)
        {
        events |= cAtSdhPathAlarmBerSd;
        if (currentStatus & cAf6_alm_stahi_bersdsta_Mask)
            defects |= cAtSdhPathAlarmBerSd;
        }

    /* HP-BER-SF */
    if (intrStatus & cAf6_alm_mskhi_bersfmsk_Mask)
        {
        events |= cAtSdhPathAlarmBerSf;
        if (currentStatus & cAf6_alm_stahi_bersfsta_Mask)
            defects |= cAtSdhPathAlarmBerSf;
        }

    /* Clear interrupt */
    AtHalWrite(hal, intrAddress, intrStatus);

    AtChannelAllAlarmListenersCall(au, events, defects);
    }

static eAtRet AutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
    uint8 hwEnable = enable ? 0 : 1;
    uint32 regAddr = mMethodsGet(mThis(self))->TerminateInsertControlRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->TerminateInsertControlRegOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if ((alarmType & cAtSdhPathAlarmLop) || (alarmType & cAtSdhPathAlarmAis))
        mRegFieldSet(regVal, cAf6_ter_ctrlhi_aislopmsk_, hwEnable);

    if (alarmType & cAtSdhPathAlarmTim)
        mRegFieldSet(regVal, cAf6_ter_ctrlhi_timmsk_, hwEnable);

    if (alarmType & cAtSdhPathAlarmPlm)
        mRegFieldSet(regVal, cAf6_ter_ctrlhi_plm_, hwEnable);

    if (alarmType & cAtSdhPathAlarmUneq)
        mRegFieldSet(regVal, cAf6_ter_ctrlhi_uneq_, hwEnable);

    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 AutoRdiAlarms(ThaPohProcessor self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TerminateInsertControlRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->TerminateInsertControlRegOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    uint32 alarms = 0;

    if ((regVal & cAf6_ter_ctrlhi_aislopmsk_Mask) == 0)
        alarms |= (cAtSdhPathAlarmLop | cAtSdhPathAlarmAis);
    if ((regVal & cAf6_ter_ctrlhi_timmsk_Mask) == 0)
        alarms |= cAtSdhPathAlarmTim;
    if ((regVal & cAf6_ter_ctrlhi_plm_Mask) == 0)
        alarms |= cAtSdhPathAlarmPlm;
    if ((regVal & cAf6_ter_ctrlhi_uneq_Mask) == 0)
        alarms |= cAtSdhPathAlarmUneq;

    return alarms;
    }

static eAtModuleSdhRet TxByteG1Insert(AtSdhChannel self, uint8 value)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TerminateInsertControlRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->TerminateInsertControlRegOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_ter_ctrlhi_g1spare_, (value & cSpareBitMask) >> cSpareBitShift);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    uint32 regVal = 0;
    ThaDefaultPohProcessorV2 defaultPohProcessor = (ThaDefaultPohProcessorV2)self;
    uint32 offset = mMethodsGet(mThis(self))->InsertBufferRegOffset(mThis(self));
    uint32 regAddr = mMethodsGet(mThis(self))->OverheadByteInsertBufferRegister(mThis(self)) + offset;

    if (!mMethodsGet(defaultPohProcessor)->OverheadByteIsValid(defaultPohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    if (overheadByte == cAtSdhPathOverheadByteG1)
        return TxByteG1Insert(self, overheadByteValue);

    if (overheadByte == cAtSdhPathOverheadByteC2)
        return AtSdhPathTxPslSet((AtSdhPath)self, overheadByteValue);

    /* Configure overhead byte buffer used for transmit */
    if (overheadByte == cAtSdhPathOverheadByteN1)
        {
        regAddr += N1C2Index();
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1_, overheadByteValue);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1msk_, 1);
        }

    if (overheadByte == cAtSdhPathOverheadByteH4)
        {
        regAddr += H4F2Index();
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1_, overheadByteValue);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1msk_, 1);
        }

    if (overheadByte == cAtSdhPathOverheadByteF2)
        {
        regAddr += H4F2Index();
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0_, overheadByteValue);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0msk_, 1);
        }

    if (overheadByte == cAtSdhPathOverheadByteF3)
        {
        regAddr += K3F3Index();
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0_, overheadByteValue);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0msk_, 1);
        }

    if (overheadByte == cAtSdhPathOverheadByteK3)
        {
        regAddr += K3F3Index();
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1_, overheadByteValue);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1msk_, 1);
        }

    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regVal;
    ThaDefaultPohProcessorV2 defaultPohProcessor = (ThaDefaultPohProcessorV2)self;
    uint32 offset = mMethodsGet(mThis(self))->InsertBufferRegOffset(mThis(self));
    uint32 regAddr = mMethodsGet(mThis(self))->OverheadByteInsertBufferRegister(mThis(self)) + offset;

    if (!mMethodsGet(defaultPohProcessor)->OverheadByteIsValid(defaultPohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    if (overheadByte == cAtSdhPathOverheadByteC2)
        return AtSdhPathTxPslGet((AtSdhPath)self);

    if (overheadByte == cAtSdhPathOverheadByteN1)
        {
        regAddr += N1C2Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte1_);
        }

    if (overheadByte == cAtSdhPathOverheadByteH4)
        {
        regAddr += H4F2Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte1_);
        }

    if (overheadByte == cAtSdhPathOverheadByteF2)
        {
        regAddr += H4F2Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte0_);
        }

    if (overheadByte == cAtSdhPathOverheadByteF3)
        {
        regAddr += K3F3Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte0_);
        }

    if (overheadByte == cAtSdhPathOverheadByteK3)
        {
        regAddr += K3F3Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte1_);
        }

    if (overheadByte == cAtSdhPathOverheadByteG1)
        {
        regAddr += G1J1Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte1_);
        }

    return 0;
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;
    ThaAuVcPohProcessorV2 vcProcessor = (ThaAuVcPohProcessorV2)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return 0;

    regAddr = mMethodsGet(vcProcessor)->PohCpeStsTu3PathOhGrabber1(vcProcessor) +
              mMethodsGet(mThis(self))->PohCpeStsTu3PathOhGrabberRegOffset(mThis(self));
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePoh);

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteC2:
            return AtSdhPathRxPslGet((AtSdhPath)self);
        case cAtSdhPathOverheadByteF2:
            return (uint8)mRegField(longRegVal[1], cAf6_pohstspohgrb_F2_);
        case cAtSdhPathOverheadByteF3:
            return (uint8)mRegField(longRegVal[1], cAf6_pohstspohgrb_F3_);
        case cAtSdhPathOverheadByteG1:
            return (uint8)mRegField(longRegVal[0], cAf6_pohstspohgrb_G1_);
        case cAtSdhPathOverheadByteH4:
            return (uint8)mRegField(longRegVal[1], cAf6_pohstspohgrb_H4_);
        case cAtSdhPathOverheadByteK3:
            return (uint8)mRegField(longRegVal[1], cAf6_pohstspohgrb_K3_);
        case cAtSdhPathOverheadByteN1:
            return (uint8)mRegField(longRegVal[0], cAf6_pohstspohgrb_N1_);
        default:
            return 0;
        }
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmUneq | cAtSdhPathAlarmPayloadUneq;
    }

static eAtRet TxTtiModeSet(Tha60210011AuVcPohProcessor self, eAtSdhTtiMode mode)
    {
    uint32 regAddr = mMethodsGet(self)->TerminateInsertControlRegAddr(self) +
                     mMethodsGet(self)->TerminateInsertControlRegOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_ter_ctrlhi_jnfrmd_, ThaTtiProcessorTtiModeSw2Hw(mode));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtSdhTtiMode TxTtiModeGet(Tha60210011AuVcPohProcessor self)
    {
    uint32 regAddr = mMethodsGet(self)->TerminateInsertControlRegAddr(self) +
                     mMethodsGet(self)->TerminateInsertControlRegOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    return ThaTtiProcessorTtiModeHw2Sw(mRegField(regVal, cAf6_ter_ctrlhi_jnfrmd_));
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    uint32 regAddr;
    uint32 regVal = 0;

    mMethodsGet(mThis(self))->TxTtiModeSet(mThis(self), tti->mode);
    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        regAddr = mMethodsGet(mThis(self))->OverheadByteInsertBufferRegister(mThis(self)) + G1J1Index() +
                  mMethodsGet(mThis(self))->InsertBufferRegOffset(mThis(self));
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0_, tti->message[0]);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte0msk_, 1);
        mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
        return cAtOk;
        }

    regAddr = mMethodsGet((ThaDefaultPohProcessorV2)self)->PohTxTtiAddressGet((ThaDefaultPohProcessorV2)self) +
              mMethodsGet(mThis(self))->TtiBufferRegOffset(mThis(self));

    return ThaTtiProcessorLongWrite(tti, regAddr, (AtChannel)self);
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    uint32 regAddr;
    uint32 regVal;
    AtOsal osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->MemInit(osal, tti->message, 0, cAtSdhChannelMaxTtiLength);
    tti->mode = mMethodsGet(mThis(self))->TxTtiModeGet(mThis(self));

    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        regAddr = mMethodsGet(mThis(self))->OverheadByteInsertBufferRegister(mThis(self)) + G1J1Index() +
                  mMethodsGet(mThis(self))->InsertBufferRegOffset(mThis(self));
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        tti->message[0] = (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte0_);
        return cAtOk;
        }

    regAddr = mMethodsGet((ThaDefaultPohProcessorV2)self)->PohTxTtiAddressGet((ThaDefaultPohProcessorV2)self) +
              mMethodsGet(mThis(self))->TtiBufferRegOffset(mThis(self));
    return ThaTtiProcessorLongRead(tti, regAddr, (AtChannel)self);
    }

static eAtModuleSdhRet RxTtiModeCheck(AtSdhChannel self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    if (!Tha60210011ModulePohShouldCheckTtiOofStatus((Tha60210011ModulePoh)ModulePoh((AtChannel)self)))
        return cAtOk;

    regAddr = mMethodsGet(processor)->PohCpeStatusRegAddr(processor) +
              mMethodsGet(processor)->PohCpeStatusRegOffset(processor);
    regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_pohcpestssta_JnBadFrame_Mask)
        return cAtModuleSdhErrorTtiModeMismatch;

    return cAtOk;
    }

static eBool HasSevereDefects(AtSdhChannel self)
    {
    static const uint32 cVcSevereDefects = (cBit3|cBit1_0); /* AIS-L|LOP-P|AIS-P */
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3PathOhGrabber1(processor) +
            mMethodsGet(mThis(self))->PohCpeStsTu3PathOhGrabberRegOffset(mThis(self));
    uint32 regVal[cThaLongRegMaxSize];
    uint32 wordId = mMethodsGet(mThis(self))->AisDownstreamAtGrabberWord(mThis(self));
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
    return (regVal[wordId] & cVcSevereDefects) ? cAtTrue : cAtFalse;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    eAtModuleSdhRet ret = cAtOk;
    uint32 regAddr;
    uint32 regVal[cThaLongRegMaxSize];
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    ThaAuVcPohProcessorV2 vcProcessor = (ThaAuVcPohProcessorV2)self;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 jn1ByteMask = mMethodsGet(mThis(self))->JnByteMask(mThis(self));
    uint8 jn1ByteShift = mMethodsGet(mThis(self))->JnByteShift(mThis(self));

    mMethodsGet(osal)->MemInit(osal, tti->message, 0, cAtSdhChannelMaxTtiLength);
    tti->mode = mMethodsGet(processor)->RxTtiModeGet(processor);

    /* If VC got severe defects (LOP/AIS), do not get TTI message. */
    if (HasSevereDefects(self))
        return cAtOk;

    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        regAddr = mMethodsGet(vcProcessor)->PohCpeStsTu3PathOhGrabber1(vcProcessor) +
                  mMethodsGet(mThis(self))->PohCpeStsTu3PathOhGrabberRegOffset(mThis(self));
        mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
        tti->message[0] = (uint8)mRegField(regVal[0], jn1Byte);
        return cAtOk;
        }

    ret = RxTtiModeCheck(self);
    if (ret != cAtOk)
        return ret;

    regAddr = mMethodsGet((ThaDefaultPohProcessorV2)self)->PohCapturedTtiAddressGet((ThaDefaultPohProcessorV2)self) +
              mMethodsGet(mThis(self))->ExpTtiBufferRegOffset(mThis(self));
    return ThaTtiProcessorLongRead(tti, regAddr, (AtChannel)self);
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    uint32 regAddr;
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    tti->mode = mMethodsGet(processor)->RxTtiModeGet(processor);

    regAddr = mMethodsGet((ThaDefaultPohProcessorV2)self)->PohExpectedTtiAddressGet((ThaDefaultPohProcessorV2)self) +
              mMethodsGet(mThis(self))->ExpTtiBufferRegOffset(mThis(self));
    return ThaTtiProcessorLongRead(tti, regAddr, (AtChannel)self);
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    uint32 regAddr;
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    mMethodsGet(processor)->RxTtiModeSet(processor, tti->mode);

    regAddr = mMethodsGet((ThaDefaultPohProcessorV2)self)->PohExpectedTtiAddressGet((ThaDefaultPohProcessorV2)self) +
              mMethodsGet(mThis(self))->ExpTtiBufferRegOffset(mThis(self));
    return ThaTtiProcessorLongWrite(tti, regAddr, (AtChannel)self);
    }

static uint32 HwMode2TimerUnitInMs(uint32 timerMode)
    {
    switch (timerMode)
        {
        case cTimer2msMode:     return cTimer2msUnit;
        case cTimer10msMode:    return cTimer10msUnit;
        case cTimer50msMode:    return cTimer50msUnit;
        case cTimer100msMode:   return cTimer100msUnit;
        case cTimer200msMode:   return cTimer200msUnit;
        case cTimer1000msMode:  return cTimer1000msUnit;
        default:                return 0;
        }
    }

static uint32 HoldOffMaxHwValue(uint32 valueMask, uint32 valueShift)
    {
    return (valueMask >> valueShift);
    }

static uint32 HoldOffTimer2Unit(uint32 timerValue, uint32 timerMax)
    {
    if (timerValue <= (timerMax * cTimer2msUnit))
        return cTimer2msUnit;

    if (timerValue <= (timerMax * cTimer10msUnit))
        return cTimer10msUnit;

    if (timerValue <= (timerMax * cTimer50msUnit))
        return cTimer50msUnit;

    if (timerValue <= (timerMax * cTimer100msUnit))
        return cTimer100msUnit;

    if (timerValue <= (timerMax * cTimer200msUnit))
        return cTimer200msUnit;

    if (timerValue <= (timerMax * cTimer1000msUnit))
        return cTimer1000msUnit;

    return cInvalidUint32;
    }

static uint32 HoldOffTimerUnit2HwMode(uint32 resolution)
    {
    switch (resolution)
        {
        case cTimer2msUnit:     return cTimer2msMode;
        case cTimer10msUnit:    return cTimer10msMode;
        case cTimer50msUnit:    return cTimer50msMode;
        case cTimer100msUnit:   return cTimer100msMode;
        case cTimer200msUnit:   return cTimer200msMode;
        case cTimer1000msUnit:  return cTimer1000msMode;
        default:                return 0;
        }
    }

static eAtRet HoldoffSw2Hw(uint32 timerInMs, uint32 *holdoffHwValue, uint32 *holdoffHwMode)
    {
    uint32 maxHwValue;
    uint32 unitInMs;

    maxHwValue = HoldOffMaxHwValue(cAf6_upen_xxcfgtms_AMETmsMax_Mask, cAf6_upen_xxcfgtms_AMETmsMax_Shift);
    unitInMs = HoldOffTimer2Unit(timerInMs, maxHwValue);

    if (unitInMs == cInvalidUint32)
        return cAtErrorOutOfRangParm;

    *holdoffHwValue = 0;
    *holdoffHwMode = HoldOffTimerUnit2HwMode(unitInMs);
    if (unitInMs)
        *holdoffHwValue = timerInMs / unitInMs;

    return cAtOk;
    }

static eAtRet HwHoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    eAtRet ret;
    uint32 regAddr = mMethodsGet(mThis(self))->UpsrHoldOffRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->UpsrHoldOffRegOffset(mThis(self));
    uint32 regVal = 0;
    uint32 timerValue, unitValue;

    ret = HoldoffSw2Hw(timerInMs, &timerValue, &unitValue);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal, cAf6_upen_xxcfgtms_AMETmsMax_, timerValue);
    mRegFieldSet(regVal, cAf6_upen_xxcfgtms_AMETmsUnit_, unitValue);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool HoldOffTimerSupported(AtSdhChannel self)
    {
    return Tha60210011ModulePohUpsrIsSupported((Tha60210011ModulePoh) ModulePoh((AtChannel) self));
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    if (AtSdhChannelHoldOffTimerSupported(self))
        return HwHoldOffTimerSet(self, timerInMs);

    return cAtErrorModeNotSupport;
    }

static uint32 HwHoldOffTimerGet(AtSdhChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->UpsrHoldOffRegAddr(mThis(self)) +
                     mMethodsGet(mThis(self))->UpsrHoldOffRegOffset(mThis(self));
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    uint32 timer = mRegField(regVal, cAf6_upen_xxcfgtms_AMETmsMax_);
    uint32 mode = mRegField(regVal, cAf6_upen_xxcfgtms_AMETmsUnit_);

    return timer * HwMode2TimerUnitInMs(mode);
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    if (Tha60210011ModulePohUpsrIsSupported((Tha60210011ModulePoh) ModulePoh((AtChannel) self)))
        return HwHoldOffTimerGet(self);

    return 0;
    }

static eAtRet TxTtiModeWarmRestore(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 PohCpeStsTu3DefaultOffset(ThaAuVcPohProcessorV2 self)
    {
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohcpestsctr(mModulePoh(self), slice, hwStsInSlice, 0);

    return 0;
    }

static uint32 PohCpeStsTu3PathOhGrabberRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohstspohgrb(mModulePoh(self), mVcGet(self), slice, hwStsInSlice);

    return 0;
    }

static uint32 PohCpeStsTu3Ctrl(ThaAuVcPohProcessorV2 self)
    {
    return ThaModulePohCpestsctrBase(mModulePoh(self));
    }

static uint32 OverheadByteInsertBufferRegister(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_rtlpohccterbufhi_Base;
    }

static uint32 PohCpeStsTu3PathOhGrabber1(ThaAuVcPohProcessorV2 self)
    {
    return ThaModulePohstspohgrbBase(mModulePoh(self), mVcGet(self));
    }

static uint32 PohExpectedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    return ThaModulePohMsgstsexpBase(mModulePoh(self));
    }

static uint32 PohCapturedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return ThaModulePohMsgstscurBase(mModulePoh(self));
    }

static uint32 PohTxTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    return ThaModulePohMsgstsinsBase(mModulePoh(self));
    }

static uint32 InsertBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_rtlpohccterbufhi(mModulePoh(self), hwSts, slice, 0);

    return 0;
    }

static uint32 TerminateInsertControlRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_ter_ctrlhi(mModulePoh(self), hwSts, slice);

    return 0;
    }

static uint32 TerminateInsertControlRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_ter_ctrlhi_Base;
    }

static uint32 TtiBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_pohmsgstsins(mModulePoh(self), slice, hwSts, 0);

    return 0;
    }

static uint32 ExpTtiBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_pohmsgstsexp(mModulePoh(self), slice, hwSts, 0);

    return 0;
    }

static uint32 CounterRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_ipm_cnthi(mModulePoh(self), hwSts, slice);

    return 0;
    }

static uint32 JnByteMask(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6_pohstspohgrb_J1_Mask;
    }

static uint8 JnByteShift(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6_pohstspohgrb_J1_Shift;
    }

static eAtRet PohCounterGet(Tha60210011AuVcPohProcessor self)
	{
	uint32 regAddr = mMethodsGet(mThis(self))->PohCounterReportRegAddr(mThis(self)) +
					 mMethodsGet(mThis(self))->CounterRegOffset(mThis(self));
	uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mThis(self)->reiCounter = mThis(self)->reiCounter + mRegField(regVal, cAf6_ipm_cnthi_reicnt_);
    mThis(self)->bipCounter = mThis(self)->bipCounter + mRegField(regVal, cAf6_ipm_cnthi_bipcnt_);

	return cAtOk;
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
	/* Get counter from HW and save to DB */
	mMethodsGet(mThis(self))->PohCounterGet(mThis(self));

    if (counterType == cAtSdhPathCounterTypeRei)
        return mThis(self)->reiCounter;

    if (counterType == cAtSdhPathCounterTypeBip)
        return mThis(self)->bipCounter;

    return 0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    uint32 counterValue = mMethodsGet(self)->CounterGet(self, counterType);

    /* Counter got from SDH module */
    if (counterType == cAtSdhPathCounterTypeRei)
        mThis(self)->reiCounter = 0;

    if (counterType == cAtSdhPathCounterTypeBip)
        mThis(self)->bipCounter = 0;

    return counterValue;
    }

static eBool CounterHasBlockMode(AtSdhChannel self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtSdhPathCounterTypeRei) || (counterType == cAtSdhPathCounterTypeBip))
        return cAtTrue;
    return cAtFalse;
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    uint32 regAddr, regVal;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint8 blockMode = (mode == cAtSdhChannelCounterModeBlock) ? 1 : 0;
    
    if (blockMode && !CounterHasBlockMode(self, counterType))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    if (counterType == cAtSdhPathCounterTypeBip)
        mRegFieldSet(regVal, cAf6_pohcpestsctr_Blkmden_, blockMode);
    if (counterType == cAtSdhPathCounterTypeRei)
        mRegFieldSet(regVal, cAf6_pohcpestsctr_Reiblkmden_, blockMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 PohStatusReportRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohAlmStahiBase(mModulePoh(self));
    }

static uint32 PohCounterReportRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohIpmCnthiBase(mModulePoh(self));
    }

static eAtRet HwModeSet(Tha60210011AuVcPohProcessor self, eAtSdhChannelMode mode)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint8 hwChannelMode = ThaSdhHwChannelMode(mode);
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_Sdhmode_, hwChannelMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    eAtRet ret;

    ret  = mMethodsGet(mThis(self))->HwModeSet(mThis(self), mode);
    if (ret != cAtOk)
        return ret;

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) +
                     mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return ThaSdhSwChannelMode((uint8)mRegField(regVal, cAf6_pohcpestsctr_Sdhmode_));
    }

static uint32 PohInterruptMaskRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohAlmMskhiBase(mModulePoh(self));
    }

static uint32 PohInterruptStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohAlmChghiBase(mModulePoh(self));
    }

static uint32 PohInterruptRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        {
        uint32 offset = Tha60210011ModulePohHoPathInterruptOffset((Tha60210011ModulePoh)mModulePoh(self), slice, hwSts);
        return offset + Tha60210011ModulePohBaseAddress(mModulePoh(self));
        }

    return 0;
    }

static uint32 UpsrHoldOffRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_upen_xxcfgtms_Base;
    }

static uint32 UpsrHoldOffRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 lineId = AtSdhChannelLineGet(mVcGet(self));
    uint8 stsId = AtSdhChannelSts1Get(mVcGet(self));

    return Tha60210011ModulePohBaseAddress(mModulePoh(self)) + Tha60210011ModuleSdhUpsrStsFromTfi5SwStsGet(lineId, stsId);
    }

static ThaModulePoh PohModule(Tha60210011AuVcPohProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static eBool SoftStickyIsNeeded(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohSoftStickyIsNeeded(PohModule(self));
    }

static uint32 SoftStickyGet(Tha60210011AuVcPohProcessor self, uint32 currentHwDefects)
    {
    uint32 softSticky;
    if (!mMethodsGet(self)->SoftStickyIsNeeded(self))
        return 0;

    softSticky = self->defectRegVal ^ currentHwDefects;
    self->defectRegVal = currentHwDefects;
    return softSticky;
    }

static uint32 AisDownstreamAtGrabberWord(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 Tim2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_TimDstren_Mask;
    }

static uint32 Tim2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_TimDstren_Shift;
    }

static uint32 Uneq2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_UneqDstren_Mask;
    }

static uint32 Uneq2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_UneqDstren_Shift;
    }

static uint32 Plm2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_PlmDstren_Mask;
    }

static uint32 Plm2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_PlmDstren_Shift;
    }

static uint32 VcAis2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_VcaisDstren_Mask;
    }

static uint32 VcAis2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpestsctr_VcaisDstren_Shift;
    }

static uint32 PohCpeStsTu3PohOffset(ThaAuVcPohProcessorV2 self)
    {
    return mMethodsGet(self)->PohCpeStsTu3DefaultOffset(self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtSdhPathPlmMonitorEnable((AtSdhPath)self, cAtTrue);

    if (AtSdhChannelHoldOffTimerSupported((AtSdhChannel)self))
        ret |= AtSdhChannelHoldOffTimerSet((AtSdhChannel)self, 100);

    return ret;
    }

static eAtModuleSdhRet PlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_PlmEnb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool PlmMonitorIsEnabled(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpestsctr_PlmEnb_));
    }

static eBool HasParentAlarmForwarding(AtSdhChannel self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3PathOhGrabber1(processor) +
            mMethodsGet(mThis(self))->PohCpeStsTu3PathOhGrabberRegOffset(mThis(self));
    uint32 regVal[cThaLongRegMaxSize];
    uint32 wordId = mMethodsGet(mThis(self))->AisDownstreamAtGrabberWord(mThis(self));
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
    return (regVal[wordId] & cBit3) ? cAtTrue : cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (AtSdhChannelHasParentAlarmForwarding((AtSdhChannel)self))
        return cAtSdhPathAlarmAis;

    return mMethodsGet(mThis(self))->PohDefectGet(mThis(self));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011AuVcPohProcessor object = (Tha60210011AuVcPohProcessor)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(reiCounter);
    mEncodeUInt(bipCounter);
    mEncodeUInt(stickyRegVal);
    mEncodeUInt(defectRegVal);
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_pohcpestsctr_MonEnb_, (enabled ? 1 : 0));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    return cAtOk;
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return (regVal & cAf6_pohcpestsctr_MonEnb_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0x2D500;
    }

static uint32 PohCpeStatusRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohcpestssta(mModulePoh(self), slice, hwStsInSlice);

    return 0;
    }

static void CounterAccumulate(ThaPohProcessor self, uint16 counterType, uint32 value)
    {
    if (counterType == cAtSdhPathCounterTypeRei)
        mThis(self)->reiCounter += value;

    if (counterType == cAtSdhPathCounterTypeBip)
        mThis(self)->bipCounter += value;
    }

static uint32 RdiMask(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ErdiSMask(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideThaPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    ThaPohProcessor processor = (ThaPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaPohProcessorOverride,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaPohProcessorOverride));

        mMethodOverride(m_ThaPohProcessorOverride, AutoRdiEnable);
        mMethodOverride(m_ThaPohProcessorOverride, AutoRdiAlarms);
        mMethodOverride(m_ThaPohProcessorOverride, CounterAccumulate);
        mMethodOverride(m_ThaPohProcessorOverride, ERdiMonitorEnable);
        mMethodOverride(m_ThaPohProcessorOverride, ERdiMonitorIsEnabled);
        mMethodOverride(m_ThaPohProcessorOverride, TxERdiEnable);
        mMethodOverride(m_ThaPohProcessorOverride, TxERdiIsEnabled);
        }

    mMethodsSet(processor, &m_ThaPohProcessorOverride);
    }

static void OverrideThaDefaultPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));

        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeSet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxTtiModeWarmRestore);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiAddressGet);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void OverrideThaAuVcPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAuVcPohProcessorV2Override, mMethodsGet(self), sizeof(m_ThaAuVcPohProcessorV2Override));

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3DefaultOffset);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3Ctrl);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PathOhGrabber1);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PohOffset);

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Tim2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Tim2AisEnbShift);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Plm2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Plm2AisEnbShift);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Uneq2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Uneq2AisEnbShift);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, VcAis2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, VcAis2AisEnbShift);
        }

    mMethodsSet(self, &m_ThaAuVcPohProcessorV2Override);
    }

static void OverrideAtSdhPath(ThaAuVcPohProcessorV2 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ERdiEnable);
        mMethodOverride(m_AtSdhPathOverride, ERdiIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, InterruptProcess);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(ThaAuVcPohProcessorV2 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);

        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(ThaAuVcPohProcessorV2 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerGet);
        mMethodOverride(m_AtSdhChannelOverride, HasParentAlarmForwarding);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtObject(ThaAuVcPohProcessorV2 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaPohProcessor(self);
    OverrideThaDefaultPohProcessorV2(self);
    OverrideThaAuVcPohProcessorV2(self);
    }

static void MethodsInit(ThaAuVcPohProcessorV2 self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, InsertBufferRegOffset);
        mMethodOverride(m_methods, TerminateInsertControlRegOffset);
        mMethodOverride(m_methods, TerminateInsertControlRegAddr);
        mMethodOverride(m_methods, TtiBufferRegOffset);
        mMethodOverride(m_methods, ExpTtiBufferRegOffset);
        mMethodOverride(m_methods, OverheadByteInsertBufferRegister);
        mMethodOverride(m_methods, PohCpeStsTu3PathOhGrabberRegOffset);
        mMethodOverride(m_methods, TxTtiModeSet);
        mMethodOverride(m_methods, TxTtiModeGet);
        mMethodOverride(m_methods, PohStatusReportRegAddr);
        mMethodOverride(m_methods, PohCounterReportRegAddr);
        mMethodOverride(m_methods, PohInterruptMaskRegAddr);
        mMethodOverride(m_methods, PohInterruptStatusRegAddr);
        mMethodOverride(m_methods, PohInterruptRegOffset);
        mMethodOverride(m_methods, ErdiDefectGet);
        mMethodOverride(m_methods, HasPayloadUneqDefect);
        mMethodOverride(m_methods, HasPayloadUneqSticky);
        mMethodOverride(m_methods, RfiLomDefectGet);
        mMethodOverride(m_methods, UpsrHoldOffRegAddr);
        mMethodOverride(m_methods, UpsrHoldOffRegOffset);
        mMethodOverride(m_methods, CounterRegOffset);
        mMethodOverride(m_methods, InterruptRead2Clear);
        mMethodOverride(m_methods, SoftStickyIsNeeded);
        mMethodOverride(m_methods, TxUneqForce);
        mMethodOverride(m_methods, TxUneqIsForced);
        mMethodOverride(m_methods, HwTxAlarmForce);
        mMethodOverride(m_methods, AisDownstreamAtGrabberWord);
        mMethodOverride(m_methods, PohDefectGet);
        mMethodOverride(m_methods, PohCpeStatusRegAddr);
        mMethodOverride(m_methods, PohCpeStatusRegOffset);
        mMethodOverride(m_methods, PohCounterGet);
        mMethodOverride(m_methods, RdiMask);
        mMethodOverride(m_methods, ErdiSMask);
        mMethodOverride(m_methods, HwModeSet);

        mBitFieldOverride(Tha60210011AuVcPohProcessor, m_methods, JnByte)
        }

    mMethodsSet(processor, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011AuVcPohProcessor);
    }

ThaPohProcessor Tha60210011AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAuVcPohProcessorV2ObjectInit((ThaAuVcPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((ThaAuVcPohProcessorV2)self);
    Override((ThaAuVcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210011AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210011AuVcPohProcessorObjectInit(newProcessor, vc);
    }

eBool Tha60210011VcPohProcessorInterruptIsSupported(Tha60210011AuVcPohProcessor auVcProcessor)
    {
    return Tha60210011ModulePohInterruptIsSupported(mModulePoh(auVcProcessor));
    }

eAtRet Tha60210011AuVcPohProcessorTtiWrite(const tAtSdhTti *tti, uint32 regAddr, AtChannel channel)
    {
    return ThaTtiProcessorLongWrite(tti, regAddr, channel);
    }

eAtRet Tha60210011AuVcPohProcessorTtiRead(tAtSdhTti *tti, uint32 regAddr, AtChannel channel)
    {
    return ThaTtiProcessorLongRead(tti, regAddr, channel);
    }

uint32 Tha60210011AuVcPohProcessorSoftStickyGet(Tha60210011AuVcPohProcessor self, uint32 currentHwDefects)
    {
    if (self)
        return SoftStickyGet(self, currentHwDefects);
    return 0;
    }

uint32 Tha60210011AuVcProcessorPohDefectGet(Tha60210011AuVcPohProcessor self)
    {
    if (self)
        return mMethodsGet(self)->PohDefectGet(self);
    return 0;
    }

uint32 Tha60210011AuVcPohProcessorInterruptRead2Clear(Tha60210011AuVcPohProcessor self, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptRead2Clear(self, clear);
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210011Vc1xPohProcessor.c
 *
 * Created Date: May 26, 2015
 *
 * Description : Vc1x POH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/Tha60210011ModuleSdh.h"
#include "Tha60210011PohReg.h"
#include "Tha60210011PohProcessorInternal.h"
#include "Tha60210011ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_pohvtpohgrb_K4_Mask                              cBit31_24
#define cAf6_pohvtpohgrb_K4_Shift                             24

#define cAf6_pohvtpohgrb_N2_Mask                              cBit23_16
#define cAf6_pohvtpohgrb_N2_Shift                             16

#define cAf6_pohvtpohgrb_J2_Mask                              cBit15_8
#define cAf6_pohvtpohgrb_J2_Shift                             8

#define cAf6_pohvtpohgrb_V5_Mask                              cBit7_0
#define cAf6_pohvtpohgrb_V5_Shift                             0

#define cAf6_pohcpevtctr_ERDIenb_Mask                         cBit7
#define cAf6_pohcpevtctr_ERDIenb_Shift                        7

#define cAf6_pohcpevtctr_VslExp_Mask                          cBit6_4
#define cAf6_pohcpevtctr_VslExp_Shift                         4

#define cAf6_pohcpevtctr_TimMonEnb_Mask                       cBit3
#define cAf6_pohcpevtctr_TimMonEnb_Shift                      3

#define cAf6_pohcpevtctr_J2MonEnb_Mask                        cBit2
#define cAf6_pohcpevtctr_J2MonEnb_Shift                       2

#define cAf6_pohcpevtctr_J2mode_Mask                          cBit1_0
#define cAf6_pohcpevtctr_J2mode_Shift                         0

#define cErdiDefectMask     cBit12_9
#define cErdiDefectShift    9

/*--------------------------- Macros -----------------------------------------*/
#define mModulePoh(channel) ModulePoh((AtChannel)channel)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods                 m_AtSdhChannelOverride;
static tAtSdhPathMethods                    m_AtSdhPathOverride;
static tThaDefaultPohProcessorV2Methods     m_ThaDefaultPohProcessorV2Override;
static tThaAuVcPohProcessorV2Methods        m_ThaAuVcPohProcessorV2Override;
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;
static tThaPohProcessorMethods              m_ThaPohProcessorOverride;

/* Save super implementation */
static const tAtSdhChannelMethods                * m_AtSdhChannelMethods = NULL;
static const tTha60210011AuVcPohProcessorMethods * m_Tha60210011AuVcPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(AtChannel self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePoh);
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regAddr;
    uint32 regVal[cThaLongRegMaxSize];
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;
    ThaAuVcPohProcessorV2 vcProcessor = (ThaAuVcPohProcessorV2)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return 0;

    regAddr = mMethodsGet(vcProcessor)->PohCpeStsTu3PathOhGrabber1(vcProcessor) +
              mMethodsGet((Tha60210011AuVcPohProcessor)vcProcessor)->PohCpeStsTu3PathOhGrabberRegOffset((Tha60210011AuVcPohProcessor)self);
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteN2:
            return (uint8)mRegField(regVal[0], cAf6_pohvtpohgrb_N2_);
        case cAtSdhPathOverheadByteK4:
            return (uint8)mRegField(regVal[0], cAf6_pohvtpohgrb_K4_);
        case cAtSdhPathOverheadByteV5:
            return (uint8)mRegField(regVal[0], cAf6_pohvtpohgrb_V5_);
        default:
            return 0;
        }
    }

static uint32 VtId(Tha60210011AuVcPohProcessor self)
    {
    AtChannel tu1x = (AtChannel)AtSdhChannelParentChannelGet(mVcGet(self));
    AtChannel tug2 = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)tu1x);
    return AtChannelIdGet(tug2) * 4UL + AtChannelIdGet(tu1x);
    }

static uint32 PohCpeStsTu3DefaultOffset(ThaAuVcPohProcessorV2 self)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtId = VtId((Tha60210011AuVcPohProcessor)self);
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePoh);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cAtModuleSdh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohcpevtctr(modulePoh, slice, hwStsInSlice, (uint8)vtId);
    return 0;
    }

static uint32 InsertBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    uint32 vtId = VtId(self);
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePoh);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_rtlpohccterbuflo(modulePoh, hwSts, slice, (uint8)vtId, 0);
    return 0;
    }

static uint32 TerminateInsertControlRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    uint32 vtId = VtId(self);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_ter_ctrllo(mModulePoh(self), hwSts, slice, (uint8)vtId);

    return 0;
    }

static uint32 TtiBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    uint32 vtId = VtId(self);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_pohmsgvtins(mModulePoh(self), slice, hwSts, (uint8)vtId, 0);

    return 0;
    }

static uint32 ExpTtiBufferRegOffset(Tha60210011AuVcPohProcessor self)
    {
    return TtiBufferRegOffset(self);
    }

static uint32 PohCpeStsTu3PathOhGrabberRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtId = VtId(self);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cAtModuleSdh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohvtpohgrb(mModulePoh(self), slice, hwStsInSlice, (uint8)vtId);

    return 0;
    }

static eBool OverheadByteIsValid(ThaDefaultPohProcessorV2 self, uint32 overheadByte)
    {
    AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteV5: return cAtTrue;
        case cAtSdhPathOverheadByteN2: return cAtTrue;
        case cAtSdhPathOverheadByteK4: return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static eAtRet ERdiMonitorEnable(ThaPohProcessor self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpevtctr_ERDIenb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool ERdiMonitorIsEnabled(ThaPohProcessor self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpevtctr_ERDIenb_));
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return (uint8)mRegField(regVal, cAf6_pohcpevtctr_VslExp_);
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpevtctr_VslExp_, psl);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpevtctr_TimMonEnb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpevtctr_TimMonEnb_));
    }

static uint8 TtiModeSw2Hw(eAtSdhTtiMode value)
    {
    if (value == cAtSdhTtiMode1Byte)
        return 0;
    if (value == cAtSdhTtiMode16Byte)
        return 1;
    if (value == cAtSdhTtiMode64Byte)
        return 2;
    return 0;
    }

static eAtRet RxTtiModeSet(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode ttiMode)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    /* HW removed this field to use for REI block mode */
    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh((AtChannel)self)) == cAtFalse)
        mRegFieldSet(regVal, cAf6_pohcpevtctr_J2MonEnb_, 1);

    mRegFieldSet(regVal, cAf6_pohcpevtctr_J2mode_, TtiModeSw2Hw(ttiMode));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    
    return cAtOk;
    }

static eAtSdhTtiMode TtiModeHw2Sw(uint32 value)
    {
    if (value == 0)
        return cAtSdhTtiMode1Byte;
    if (value == 1)
        return cAtSdhTtiMode16Byte;
    if (value == 2)
        return cAtSdhTtiMode64Byte;
    return cAtSdhTtiMode1Byte;
    }

static eAtSdhTtiMode RxTtiModeGet(ThaDefaultPohProcessorV2 self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return TtiModeHw2Sw(mRegField(regVal, cAf6_pohcpevtctr_J2mode_));
    }

static eAtRet TxByteK4Insert(AtSdhChannel self, uint8 overheadByteValue)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor) +
                     mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_ter_ctrllo_k4b0b1_,  (overheadByteValue & cBit7_6) >> 6);
    mRegFieldSet(regVal, cAf6_ter_ctrllo_k4aps_,   (overheadByteValue & cBit5_4) >> 4);
    mRegFieldSet(regVal, cAf6_ter_ctrllo_k4spare_, (overheadByteValue & cBit0));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtRet TxByteV5Insert(AtSdhChannel self, uint8 overheadByteValue)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor) +
                     mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_ter_ctrllo_rfival_, (overheadByteValue & cBit4) >> 4);
    mRegFieldSet(regVal, cAf6_ter_ctrllo_vslval_, (overheadByteValue & cBit3_1) >> 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 J2V5Index(void)
    {
    return 0;
    }

static uint32 N2K4Index(void)
    {
    return 1;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    uint32 regVal = 0;
    ThaDefaultPohProcessorV2 defaultPohProcessor = (ThaDefaultPohProcessorV2)self;
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 offset = mMethodsGet(vcPohProcessor)->InsertBufferRegOffset(vcPohProcessor);
    uint32 regAddr = mMethodsGet(vcPohProcessor)->OverheadByteInsertBufferRegister(vcPohProcessor) + offset;

    if (!mMethodsGet(defaultPohProcessor)->OverheadByteIsValid(defaultPohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    if (overheadByte == cAtSdhPathOverheadByteK4)
        return TxByteK4Insert(self, overheadByteValue);

    if (overheadByte == cAtSdhPathOverheadByteV5)
        return TxByteV5Insert(self, overheadByteValue);

    if (overheadByte == cAtSdhPathOverheadByteN2)
        {
        regAddr += N2K4Index();
        mRegFieldSet(regVal, cAf6_rtlpohccterbuflo_byte0_, overheadByteValue);
        mRegFieldSet(regVal, cAf6_rtlpohccterbuflo_byte0msk_, 1);
        }

    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regVal;
    ThaDefaultPohProcessorV2 defaultPohProcessor = (ThaDefaultPohProcessorV2)self;
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 offset = mMethodsGet(vcPohProcessor)->InsertBufferRegOffset(vcPohProcessor);
    uint32 regAddr = mMethodsGet(vcPohProcessor)->OverheadByteInsertBufferRegister(vcPohProcessor) + offset;

    if (!mMethodsGet(defaultPohProcessor)->OverheadByteIsValid(defaultPohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    if (overheadByte == cAtSdhPathOverheadByteV5)
        {
        regAddr += J2V5Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbuflo_byte0_);
        }

    if (overheadByte == cAtSdhPathOverheadByteN2)
        {
        regAddr += N2K4Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbuflo_byte0_);
        }

    if (overheadByte == cAtSdhPathOverheadByteK4)
        {
        regAddr += N2K4Index();
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        return (uint8)mRegField(regVal, cAf6_rtlpohccterbuflo_byte1_);
        }

    return 0;
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor)+
                     mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_ter_ctrllo_vslval_, psl);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    Tha60210011AuVcPohProcessor vcPohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr = mMethodsGet(vcPohProcessor)->TerminateInsertControlRegAddr(vcPohProcessor)+
                     mMethodsGet(vcPohProcessor)->TerminateInsertControlRegOffset(vcPohProcessor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return (uint8)mRegField(regVal, cAf6_ter_ctrllo_vslval_);
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3PathOhGrabber1(processor) +
                     mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal[cThaLongRegMaxSize];
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);

    return (uint8)((mRegField(regVal[0], cAf6_pohvtpohgrb_V5_) & cBit3_1) >> 1);
    }

static eAtRet TxTtiModeSet(Tha60210011AuVcPohProcessor self, eAtSdhTtiMode mode)
    {
    uint32 regAddr = mMethodsGet(self)->TerminateInsertControlRegAddr(self) +
                     mMethodsGet(self)->TerminateInsertControlRegOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_ter_ctrllo_jnfrmd_, TtiModeSw2Hw(mode));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtSdhTtiMode TxTtiModeGet(Tha60210011AuVcPohProcessor self)
    {
    uint32 regAddr = mMethodsGet(self)->TerminateInsertControlRegAddr(self) +
                     mMethodsGet(self)->TerminateInsertControlRegOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    return TtiModeHw2Sw(mRegField(regVal, cAf6_ter_ctrllo_jnfrmd_));
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
        uint32 regAddr;
        uint32 regVal = 0;

        mMethodsGet(processor)->TxTtiModeSet(processor, tti->mode);
        regAddr = mMethodsGet(processor)->OverheadByteInsertBufferRegister(processor) + J2V5Index() +
                  mMethodsGet(processor)->InsertBufferRegOffset(processor);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1_, tti->message[0]);
        mRegFieldSet(regVal, cAf6_rtlpohccterbufhi_byte1msk_, 1);
        mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
        return cAtOk;
        }

    return m_AtSdhChannelMethods->TxTtiSet(self, tti);
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;

    mMethodsGet(osal)->MemInit(osal, tti->message, 0, cAtSdhChannelMaxTtiLength);
    tti->mode = mMethodsGet(processor)->TxTtiModeGet(processor);
    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        uint32 regAddr;
        uint32 regVal;

        tti->mode = mMethodsGet(processor)->TxTtiModeGet(processor);
        regAddr = mMethodsGet(processor)->OverheadByteInsertBufferRegister(processor) + J2V5Index() +
                  mMethodsGet(processor)->InsertBufferRegOffset(processor);
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        tti->message[0] = (uint8)mRegField(regVal, cAf6_rtlpohccterbufhi_byte1_);
        return cAtOk;
        }

    return m_AtSdhChannelMethods->TxTtiGet(self, tti);
    }

static uint32 PohCpeStsTu3Ctrl(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpevtctr_Base;
    }

static uint32 Tim2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpevtctr_TimDstren_Mask;
    }

static uint32 Tim2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpevtctr_TimDstren_Shift;
    }

static uint32 Uneq2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpevtctr_UneqDstren_Mask;
    }

static uint32 Uneq2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpevtctr_UneqDstren_Shift;
    }

static uint32 Plm2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpevtctr_PlmDstren_Mask;
    }

static uint32 Plm2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cAf6_pohcpevtctr_PlmDstren_Shift;
    }

static uint32 ErdiTypeFromErdiValue(uint8 erdiValue, eBool erdiIsEnable)
    {
    uint8 v5Rdi = (uint8)((erdiValue & cBit3) >> 3);
    uint8 z7Erdi = (uint8)(erdiValue & cBit2_0);

    if (!erdiIsEnable)
        return (v5Rdi == 1) ? cAtSdhPathAlarmRdi : 0;

    if (v5Rdi == 1)
        return (z7Erdi == 6) ? cAtSdhPathAlarmErdiC : cAtSdhPathAlarmErdiS;

    /* V5 == 0 */
    if (z7Erdi == 2)
        return cAtSdhPathAlarmErdiP;

    return 0;
    }

static uint32 ErdiDefectGet(Tha60210011AuVcPohProcessor self, uint32 regVal)
    {
    uint8 erdiVal;
    uint32 erdiType;
    eBool erdiEnabled = AtSdhPathERdiIsEnabled((AtSdhPath)self);

    if (Tha60210011ModulePohSdSfInterruptIsSupported(ModulePoh((AtChannel)self)))
        {
        uint8 v5Val, k4Val;
        eBool oneBitRdiRaised = (regVal & cAf6_alm_stalo_erdista_Mask) ? cAtTrue : cAtFalse;

        /* Just simply return RDI if one bit RDI is used */
        if (!erdiEnabled)
            return oneBitRdiRaised ? cAtSdhPathAlarmRdi : 0;

        v5Val = AtSdhChannelRxOverheadByteGet((AtSdhChannel)self, cAtSdhPathOverheadByteV5);
        k4Val = AtSdhChannelRxOverheadByteGet((AtSdhChannel)self, cAtSdhPathOverheadByteK4);
        erdiVal = (uint8)(((v5Val & cBit0) << 3) | ((k4Val >> 1) & cBit2_0));
        erdiType = ErdiTypeFromErdiValue(erdiVal, erdiEnabled);

        /* If HW returns that RDI is not raised but OH byte from grabber return ERDI-S/ERDI-C,
         * need to mask those alarms */
        if ((oneBitRdiRaised == cAtFalse) && ((erdiType == cAtSdhPathAlarmErdiS) || (erdiType == cAtSdhPathAlarmErdiC)))
            return 0;

        return erdiType;
        }
    else if (!Tha60210011VcPohProcessorInterruptIsSupported(self))
        erdiVal = (uint8)mRegField(regVal, cAf6_ipm_stalo_erdista_);
    else
        erdiVal = (uint8)mRegField(regVal, cErdiDefect);

    return ErdiTypeFromErdiValue(erdiVal, erdiEnabled);
    }

static uint32 PohInterruptRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        {
        AtChannel tu1x = (AtChannel)AtSdhChannelParentChannelGet(mVcGet(self));
        AtChannel tug2 = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)tu1x);
        uint32 offset = Tha60210011ModulePohLoPathInterruptOffset((Tha60210011ModulePoh)mModulePoh(self), slice, hwSts, (uint8)AtChannelIdGet(tug2), (uint8)AtChannelIdGet(tu1x));
        return offset + Tha60210011ModulePohBaseAddress(mModulePoh(self));
        }

    return 0;
    }

static uint32 RfiLomDefectGet(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmRfi; /* Leave it as hardware detected. */
    }

static uint32 UpsrHoldOffRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_upen_xxcfgtmv_Base;
    }

static uint32 UpsrHoldOffRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 lineId = AtSdhChannelLineGet(mVcGet(self));
    uint8 stsId = AtSdhChannelSts1Get(mVcGet(self));
    uint32 sts = Tha60210011ModuleSdhUpsrStsFromTfi5SwStsGet(lineId, stsId);

    return Tha60210011ModulePohBaseAddress(mModulePoh(self)) + sts * 32U + VtId(self);
    }

static eAtRet HwModeSet(Tha60210011AuVcPohProcessor self, eAtSdhChannelMode mode)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint8 hwChannelMode = ThaSdhHwChannelMode(mode);
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpevtctr_VSdhmode_, hwChannelMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) +
                     mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return ThaSdhSwChannelMode((uint8)mRegField(regVal, cAf6_pohcpevtctr_VSdhmode_));
    }

static eAtModuleSdhRet PlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpevtctr_PlmEnb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool PlmMonitorIsEnabled(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpevtctr_PlmEnb_));
    }

static uint32 CounterRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    uint8 vtId = (uint8)VtId(self);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return Tha60210011Reg_ipm_cntlo(mModulePoh(self), hwSts, slice, vtId);

    return 0;
    }

static eBool CounterHasBlockMode(AtSdhChannel self, uint16 counterType)
    {
    AtUnused(self);
    if ((counterType == cAtSdhPathCounterTypeRei) || (counterType == cAtSdhPathCounterTypeBip))
        return cAtTrue;
    return cAtFalse;
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    uint32 regAddr, regVal;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint8 blockMode = (mode == cAtSdhChannelCounterModeBlock) ? 1 : 0;
    
    if (blockMode && !CounterHasBlockMode(self, counterType))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    if (counterType == cAtSdhPathCounterTypeBip)
        mRegFieldSet(regVal, cAf6_pohcpevtctr_VBlkmden_, blockMode);
    if (counterType == cAtSdhPathCounterTypeRei)
        mRegFieldSet(regVal, cAf6_pohcpevtctr_Reiblkmden_, blockMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    
    return cAtOk;
    }

static uint32 JnByteMask(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6_pohvtpohgrb_Byte1_Mask;
    }

static uint8 JnByteShift(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6_pohvtpohgrb_Byte1_Shift;
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_pohcpevtctr_MonEnb_, (enabled ? 1 : 0));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    return cAtOk;
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return (regVal & cAf6_pohcpevtctr_MonEnb_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 PohCpeStatusRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtId = VtId(self);
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohcpevtsta(mModulePoh(self), slice, hwStsInSlice, (uint8)vtId);

    return 0;
    }

static void OverrideAtSdhChannel(ThaAuVcPohProcessorV2 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(ThaAuVcPohProcessorV2 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaDefaultPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDefaultPohProcessorV2Override, mMethodsGet(processor), sizeof(m_ThaDefaultPohProcessorV2Override));
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, OverheadByteIsValid);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeSet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeGet);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void OverrideThaAuVcPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAuVcPohProcessorV2Override, mMethodsGet(self), sizeof(m_ThaAuVcPohProcessorV2Override));

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3DefaultOffset);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3Ctrl);

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Tim2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Tim2AisEnbShift);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Plm2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Plm2AisEnbShift);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Uneq2AisEnbMask);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, Uneq2AisEnbShift);
        }

    mMethodsSet(self, &m_ThaAuVcPohProcessorV2Override);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011AuVcPohProcessorMethods = mMethodsGet(processor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, m_Tha60210011AuVcPohProcessorMethods, sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, InsertBufferRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TerminateInsertControlRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TtiBufferRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, ExpTtiBufferRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TxTtiModeSet);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, TxTtiModeGet);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, ErdiDefectGet);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, RfiLomDefectGet);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohInterruptRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, UpsrHoldOffRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, UpsrHoldOffRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, CounterRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStsTu3PathOhGrabberRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HwModeSet);

        mBitFieldOverride(Tha60210011AuVcPohProcessor, m_Tha60210011AuVcPohProcessorOverride, JnByte)
        }

    mMethodsSet(processor, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void OverrideThaPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    ThaPohProcessor processor = (ThaPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPohProcessorOverride, mMethodsGet(processor), sizeof(m_ThaPohProcessorOverride));

        mMethodOverride(m_ThaPohProcessorOverride, ERdiMonitorEnable);
        mMethodOverride(m_ThaPohProcessorOverride, ERdiMonitorIsEnabled);
        }

    mMethodsSet(processor, &m_ThaPohProcessorOverride);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaDefaultPohProcessorV2(self);
    OverrideThaAuVcPohProcessorV2(self);
    OverrideTha60210011AuVcPohProcessor(self);
    OverrideThaPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Vc1xPohProcessor);
    }

ThaPohProcessor Tha60210011Vc1xPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tu3VcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaAuVcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210011Vc1xPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210011Vc1xPohProcessorObjectInit(newProcessor, vc);
    }

uint32 Tha60210011Vc1xPohProcessorVtId(Tha60210011AuVcPohProcessor self)
    {
    return VtId(self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210011PohProcessorInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : POH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011POHPROCESSORINTERNAL_H_
#define _THA60210011POHPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/util/ThaUtil.h"
#include "../../../default/poh/v2/ThaAuVcPohProcessorV2Internal.h"
#include "Tha60210011PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011AuVcPohProcessorMethods
    {
    uint32 (*InsertBufferRegOffset)(Tha60210011AuVcPohProcessor self);
    uint32 (*TerminateInsertControlRegOffset)(Tha60210011AuVcPohProcessor self);
    uint32 (*TerminateInsertControlRegAddr)(Tha60210011AuVcPohProcessor self);
    uint32 (*TtiBufferRegOffset)(Tha60210011AuVcPohProcessor self);
    uint32 (*ExpTtiBufferRegOffset)(Tha60210011AuVcPohProcessor self);
    uint32 (*OverheadByteInsertBufferRegister)(Tha60210011AuVcPohProcessor self);
    uint32 (*PohCpeStsTu3PathOhGrabberRegOffset)(Tha60210011AuVcPohProcessor self);
    eAtRet (*TxTtiModeSet)(Tha60210011AuVcPohProcessor self, eAtSdhTtiMode mode);
    eAtSdhTtiMode (*TxTtiModeGet)(Tha60210011AuVcPohProcessor self);
    eAtRet (*HwModeSet)(Tha60210011AuVcPohProcessor self, eAtSdhChannelMode mode);

    uint32 (*PohCounterReportRegAddr)(Tha60210011AuVcPohProcessor self); /* Base address for counter status. */
    /* Interrupt registers. */
    uint32 (*PohStatusReportRegAddr)(Tha60210011AuVcPohProcessor self); /* Base address for current status. */
    uint32 (*PohInterruptStatusRegAddr)(Tha60210011AuVcPohProcessor self); /* Base address for interrupt status. */
    uint32 (*PohInterruptMaskRegAddr)(Tha60210011AuVcPohProcessor self); /* Base address for interrupt mask. */
    uint32 (*PohInterruptRegOffset)(Tha60210011AuVcPohProcessor self); /* Offset for sticky, mask and status */
    /* Extension. */
    uint32 (*ErdiDefectGet)(Tha60210011AuVcPohProcessor self, uint32 regVal);
    eBool  (*HasPayloadUneqDefect)(Tha60210011AuVcPohProcessor self);
    eBool  (*HasPayloadUneqSticky)(Tha60210011AuVcPohProcessor self, eBool r2c);
    uint32 (*RfiLomDefectGet)(Tha60210011AuVcPohProcessor self);
    uint32 (*RdiMask)(Tha60210011AuVcPohProcessor self);
    uint32 (*ErdiSMask)(Tha60210011AuVcPohProcessor self);

    uint32 (*UpsrHoldOffRegAddr)(Tha60210011AuVcPohProcessor self);
    uint32 (*UpsrHoldOffRegOffset)(Tha60210011AuVcPohProcessor self);

    uint32 (*CounterRegOffset)(Tha60210011AuVcPohProcessor self);
    uint32 (*InterruptRead2Clear)(Tha60210011AuVcPohProcessor self, eBool clear);

    eBool (*SoftStickyIsNeeded)(Tha60210011AuVcPohProcessor self);

    eAtRet (*TxUneqForce)(Tha60210011AuVcPohProcessor self, eBool enable);
    eBool (*TxUneqIsForced)(Tha60210011AuVcPohProcessor self);
    eAtRet (*HwTxAlarmForce)(Tha60210011AuVcPohProcessor self, uint32 alarmType, eBool enable);

    uint32 (*AisDownstreamAtGrabberWord)(Tha60210011AuVcPohProcessor self);
    uint32 (*PohDefectGet)(Tha60210011AuVcPohProcessor self);

    uint32 (*PohCpeStatusRegAddr)(Tha60210011AuVcPohProcessor self);
    uint32 (*PohCpeStatusRegOffset)(Tha60210011AuVcPohProcessor self);

    eAtRet (*PohCounterGet)(Tha60210011AuVcPohProcessor self);

    /* Bit fields */
    mDefineMaskShiftField(Tha60210011AuVcPohProcessor, JnByte)
    }tTha60210011AuVcPohProcessorMethods;

typedef struct tTha60210011AuVcPohProcessor
    {
    tThaAuVcPohProcessorV2 super;
    const tTha60210011AuVcPohProcessorMethods *methods;

    /* Private data */
    uint32 reiCounter;
    uint32 bipCounter;
    uint32 stickyRegVal;
    uint32 defectRegVal; /* Cache last hardware defects. */
    }tTha60210011AuVcPohProcessor;

typedef struct tTha60210011Tu3VcPohProcessor
    {
    tTha60210011AuVcPohProcessor super;
    }tTha60210011Tu3VcPohProcessor;

typedef struct tTha60210011Vc1xPohProcessor
    {
    tTha60210011Tu3VcPohProcessor super;
    }tTha60210011Vc1xPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha60210011AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
ThaPohProcessor Tha60210011Tu3VcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
ThaPohProcessor Tha60210011Vc1xPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
eBool Tha60210011VcPohProcessorInterruptIsSupported(Tha60210011AuVcPohProcessor auVcProcessor);
eAtRet Tha60210011AuVcPohProcessorTtiWrite(const tAtSdhTti *tti, uint32 regAddr, AtChannel channel);
eAtRet Tha60210011AuVcPohProcessorTtiRead(tAtSdhTti *tti, uint32 regAddr, AtChannel channel);

uint32 Tha60210011AuVcPohProcessorSoftStickyGet(Tha60210011AuVcPohProcessor self, uint32 currentHwDefects);

#endif /* _THA60210011POHPROCESSORINTERNAL_H_ */


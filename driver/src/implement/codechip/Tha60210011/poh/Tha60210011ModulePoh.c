/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210011ModulePoh.c
 *
 * Created Date: Apr 30, 2015
 *
 * Description : POH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "../man/Tha60210011Device.h"
#include "../ram/Tha60210011InternalRam.h"
#include "Tha60210011ModulePohInternal.h"
#include "Tha60210011PohProcessorInternal.h"
#include "Tha60210011PohReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cUpsrAlarmBerSfMask     cBit0
#define cUpsrAlarmBerSdMask     cBit1
#define cUpsrAlarmPlmMask       cBit2
#define cUpsrAlarmLopMask       cBit3
#define cUpsrAlarmAisMask       cBit4
#define cUpsrAlarmUneqMask      cBit5
#define cUpsrAlarmTimMask       cBit6
#define cInvalidAddress         cBit31_0

#define cRegJnRequestDdrMask    cBit31_12
#define cRegJnRequestDdrShift   12

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModulePoh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModulePohMethods m_methods;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;
static tThaModulePohMethods   m_ThaModulePohOverride;
static tThaModulePohV2Methods m_ThaModulePohOverrideV2;

/* Save super implementation */
static const tThaModulePohMethods *m_ThaModulePohMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    if (AtSdhLineHasFramer(line))
        return m_ThaModulePohMethods->LineTtiProcessorCreate(self, line);

    return NULL;
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210011AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210011Tu3VcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210011Vc1xPohProcessorNew(vc);
    }

static eBool SdhChannelIsVc1x(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static void RegDisplay(AtSdhChannel channel, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)channel, address, cThaModulePoh);
    }

static void TtiMsgRegsDisplay(AtSdhChannel sdhChannel, const char* regName, uint32 startAddr)
    {
    uint32 add_i;
    ThaDeviceRegNameDisplay(regName);
    for (add_i = 0; add_i < 8; add_i++)
        {
        uint32 address = startAddr + add_i;
        ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, address, cThaModulePoh);
        }
    }

static void SdhChannelRegShow(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    uint32 address;
    eBool isVcx = AtSdhChannelIsHoVc(sdhChannel);
    eBool isVc1x = SdhChannelIsVc1x(sdhChannel);
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)ThaSdhVcPohProcessorGet((AtSdhVc)sdhChannel);
    ThaDefaultPohProcessorV2 defaultProcessor = (ThaDefaultPohProcessorV2)processor;
    Tha60210011AuVcPohProcessor thisProcessor = (Tha60210011AuVcPohProcessor)processor;

    AtUnused(self);

    if (processor == NULL)
        {
        AtPrintc(cSevInfo, "No POH processor to display\r\n");
        return;
        }

    if (isVcx)
        {
        address = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
        RegDisplay(sdhChannel, "POH CPE STS Control Register", address);

        address = mMethodsGet(defaultProcessor)->PohExpectedTtiAddressGet(defaultProcessor) +
        mMethodsGet(thisProcessor)->ExpTtiBufferRegOffset(thisProcessor);
        TtiMsgRegsDisplay(sdhChannel, "POH CPE J1 STS Expected Message buffer", address);

        address = mMethodsGet(defaultProcessor)->PohCapturedTtiAddressGet(defaultProcessor) +
        mMethodsGet(thisProcessor)->ExpTtiBufferRegOffset(thisProcessor);
        TtiMsgRegsDisplay(sdhChannel, "POH CPE J1 STS Current Message buffer", address);

        address = mMethodsGet(defaultProcessor)->PohTxTtiAddressGet(defaultProcessor) +
        mMethodsGet(thisProcessor)->TtiBufferRegOffset(thisProcessor);
        TtiMsgRegsDisplay(sdhChannel, "POH CPE J1 Insert Message buffer", address);

        address = mMethodsGet(thisProcessor)->TerminateInsertControlRegAddr(thisProcessor) +
        mMethodsGet(thisProcessor)->TerminateInsertControlRegOffset(thisProcessor);
        RegDisplay(sdhChannel, "POH Terminate Insert Control STS", address);
        }

    if (isVc1x)
        {
        address = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) + mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);
        RegDisplay(sdhChannel, "POH CPE VT Control Register", address);

        address = mMethodsGet(defaultProcessor)->PohExpectedTtiAddressGet(defaultProcessor) +
        mMethodsGet(thisProcessor)->TtiBufferRegOffset(thisProcessor);
        TtiMsgRegsDisplay(sdhChannel, "POH CPE J2 Expected Message buffer", address);

        address = mMethodsGet(defaultProcessor)->PohCapturedTtiAddressGet(defaultProcessor) +
        mMethodsGet(thisProcessor)->TtiBufferRegOffset(thisProcessor);
        TtiMsgRegsDisplay(sdhChannel, "POH CPE J2 Current Message buffer", address);

        address = mMethodsGet(defaultProcessor)->PohTxTtiAddressGet(defaultProcessor) +
        mMethodsGet(thisProcessor)->TtiBufferRegOffset(thisProcessor);
        TtiMsgRegsDisplay(sdhChannel, "POH CPE J2 Insert Message buffer", address);

        address = mMethodsGet(thisProcessor)->TerminateInsertControlRegAddr(thisProcessor) +
        mMethodsGet(thisProcessor)->TerminateInsertControlRegOffset(thisProcessor);
        RegDisplay(sdhChannel, "POH Terminate Insert Buffer TU3/VT", address);
        }
    }

static uint32 StartVersionSupportUpsr(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x08, 0x30, 0x23);
    }

static eBool UpsrIsSupported(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule) self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportUpsr(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet UpsrDefaultSet(ThaModulePohV2 self)
    {
    uint32 regAddr = Tha60210011ModulePohBaseAddress((ThaModulePoh)self) + cAf6Reg_upen_amectl;

    /* Activate UPSR monitoring engine */
    mModuleHwWrite(self, regAddr, (cAf6_upen_amectl_AMECtlClrHOff_Mask|cAf6_upen_amectl_AMECtlActive_Mask));

    /* Set alarm-SD/SF mask */
    Tha60210011ModulePohUpsrSdAlarmMaskSet((Tha60210011ModulePoh)self, cAtSdhPathAlarmBerSd);
    Tha60210011ModulePohUpsrSfAlarmMaskSet((Tha60210011ModulePoh)self, cAtSdhPathAlarmBerSf|cAtSdhPathAlarmAis|cAtSdhPathAlarmLop|cAtSdhPathAlarmPlm|cAtSdhPathAlarmUneq);

    return cAtOk;
    }

static uint32 BaseAddress(ThaModulePoh self)
    {
    AtUnused(self);
    return 0x200000;
    }

static void PohInterruptDeboundThresholdDefaultSet(AtModule self)
    {
    uint32 address = cAf6Reg_pcfg_trshglbctr_Base + ThaModulePohBaseAddress((ThaModulePoh)self);

    mModuleHwWrite(self, address, 0x53333354);
    }

static eAtRet DefaultSet(ThaModulePohV2 self)
    {
    PohInterruptDeboundThresholdDefaultSet((AtModule)self);

    if (Tha60210011ModulePohUpsrIsSupported((Tha60210011ModulePoh)self))
        return UpsrDefaultSet(self);

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsPohRegister(AtModuleDeviceGet(self), address);
    }

static uint32 Reg_pohstspohgrb(ThaModulePoh self, AtSdhChannel sdhChannel, uint8 sliceid, uint8 stsid)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return (stsid) * 8UL + (sliceid);
    }

static uint32 Reg_pohvtpohgrb(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    AtUnused(self);
    return (sliceid) * 1344UL + (stsid) * 28UL + (vtid);
    }

static uint32 Reg_pohcpestsctr(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en)
    {
    AtUnused(self);
    return (sliceid) * 96UL + (stsid) * 2UL + (tu3en);
    }

static uint32 Reg_pohramberrateststu3(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en)
    {
    AtUnused(self);
    return stsid * 16UL + sliceid * 2UL + tu3en;
    }

static uint32 Reg_pohcpevtctr(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    AtUnused(self);
    return (sliceid) * 1344UL + (stsid) * 28UL + (vtid);
    }

static uint32 Reg_pohmsgstsexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 384UL + (stsid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgstscur(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 384UL + (stsid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgvtexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 10752UL + (stsid) * 224UL + (vtid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgvtcur(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 10752UL + (stsid) * 224UL + (vtid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgstsins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 512UL + (stsid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgvtins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 10752UL + (stsid) * 224UL + (vtid) * 8UL + (msgid);
    }

static uint32 Reg_ter_ctrlhi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return (sts) * 8UL + (ocid);
    }

static uint32 Reg_ter_ctrllo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return (sts) * 168UL + (ocid) * 28UL + (vt);
    }

static uint32 Reg_rtlpohccterbufhi(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 bgrp)
    {
    AtUnused(self);
    return (ocid) * 256UL + (sts) * 4UL + (bgrp);
    }

static uint32 Reg_rtlpohccterbuflo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt, uint8 bgrp)
    {
    AtUnused(self);
    return (ocid) * 4096UL + (sts) * 64UL + (vt) * 2UL + (bgrp);
    }

static uint32 Reg_ipm_cnthi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return mMethodsGet(mThis(self))->Reg_ter_ctrlhi(self, sts, ocid);
    }

static uint32 Reg_ipm_cntlo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return mMethodsGet(mThis(self))->Reg_ter_ctrllo(self, sts, ocid, vt);
    }

static uint32 Reg_pohcpestssta(ThaModulePoh self, uint8 sliceid, uint8 stsid)
    {
    AtUnused(self);
    return (sliceid) * 48UL + (stsid);
    }

static uint32 Reg_pohcpevtsta(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    return mMethodsGet(mThis(self))->Reg_pohcpevtctr(self, sliceid, stsid, vtid);
    }

static uint32 StartVersionSupportSdSfInterrupt(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    
    if (ThaDeviceIsEp((ThaDevice)device))
        return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x09, 0x28, 0x0);
        
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x09, 0x0);
    }

static uint32 StartVersionSupportBlockModeCounters(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x06, 0x36);
    }

static uint32 NumberStsSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 NumberVtSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 12;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if ((localAddress >= 0x0200000) && (localAddress <= 0x02BFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "POH CPE STS/TU3 Control",
         "POH CPE VT Control",
         "POH Terminate Insert Control STS",
         "POH Terminate Insert Control VT/TU3",
         "POH BER Threshold 2",
         "POH BER Control VT/DSN & STS/TU3"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60210011InternalRamPohNew(self, ramId, localRamId);
    }

static eAtRet AllVtsPohMonEnable(ThaModulePoh self, AtSdhVc vc3, eBool enabled)
    {
    uint8 tug2Id;
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;
    uint32 cpeVtCtrlAddress = Tha60210011ModulePohCpeVtCtrl(pohModule);

    for (tug2Id = 0; tug2Id < 7; tug2Id++)
        {
        uint8 tuId;
        for (tuId = 0; tuId < 4; tuId++)
            {
            uint32 regAddr = cpeVtCtrlAddress + Tha60210011ModulePohCpeVtOffset(pohModule, vc3, tug2Id, tuId);
            uint32 regVal = mModuleHwRead(self, regAddr);
            mRegFieldSet(regVal, cAf6_pohcpevtctr_MonEnb_, (enabled ? 1 : 0));
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static uint32 StartVersionSupportPohMonitorEnabling(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x03, 0x27, 0x63);
    }

static eBool PslTtiInterruptIsSupported(ThaModulePoh self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));

    if (ThaVersionReaderHardwareVersion(versionReader) == ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x6, 0))
        {
        if (ThaVersionReaderHardwareBuiltNumber(versionReader) >= ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x21))
            return cAtTrue;
        return cAtFalse;
        }

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x1009))
            return cAtTrue;

    return cAtFalse;
    }

static uint32 StartVersionSupportNewInterruptMasking(AtDevice device)
    {
    /* From this version, workaround of missed PLM interrupt will be dropped. */
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x19, 0x39);
    }

static uint32 StartBuiltNumberSupportNewInterruptMasking(AtDevice device)
    {
    AtUnused(device);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x07);
    }

static eBool SoftStickyIsNeeded(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceVersionNumber(device) > StartVersionSupportNewInterruptMasking(device))
        return cAtFalse;

    if (AtDeviceVersionNumber(device) == StartVersionSupportNewInterruptMasking(device))
       return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportNewInterruptMasking(device)) ? cAtFalse : cAtTrue;

    return cAtTrue;
    }

static uint32 SdhAlarm2HwMask(uint32 alarms)
    {
    uint32 mask = 0;
    if (alarms & cAtSdhPathAlarmAis)
        mask |= cUpsrAlarmAisMask;
    if (alarms & cAtSdhPathAlarmLop)
        mask |= cUpsrAlarmLopMask;
    if (alarms & cAtSdhPathAlarmPlm)
        mask |= cUpsrAlarmPlmMask;
    if (alarms & cAtSdhPathAlarmBerSf)
        mask |= cUpsrAlarmBerSfMask;
    if (alarms & cAtSdhPathAlarmBerSd)
        mask |= cUpsrAlarmBerSdMask;
    if (alarms & cAtSdhPathAlarmUneq)
        mask |= cUpsrAlarmUneqMask;
    if (alarms & cAtSdhPathAlarmTim)
        mask |= cUpsrAlarmTimMask;

    return mask;
    }

static uint32 HwMask2SdhAlarm(uint32 mask)
    {
    uint32 alarms = 0;
    if (mask & cUpsrAlarmAisMask)
        alarms |= cAtSdhPathAlarmAis;
    if (mask & cUpsrAlarmLopMask)
        alarms |= cAtSdhPathAlarmLop;
    if (mask & cUpsrAlarmPlmMask)
        alarms |= cAtSdhPathAlarmPlm;
    if (mask & cUpsrAlarmBerSfMask)
        alarms |= cAtSdhPathAlarmBerSf;
    if (mask & cUpsrAlarmBerSdMask)
        alarms |= cAtSdhPathAlarmBerSd;
    if (mask & cUpsrAlarmUneqMask)
        alarms |= cAtSdhPathAlarmUneq;
    if (mask & cUpsrAlarmTimMask)
        alarms |= cAtSdhPathAlarmTim;

    return alarms;
    }

static eAtRet UpsrSfAlarmMaskSet(Tha60210011ModulePoh self, uint32 alarmMask)
    {
    uint32 regAddr = Tha60210011ModulePohBaseAddress((ThaModulePoh)self) + cAf6Reg_upen_amemask;
    uint32 regField = SdhAlarm2HwMask(alarmMask);
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_amemask_AMESFAlmMask_, regField);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 UpsrSfAlarmMaskGet(Tha60210011ModulePoh self)
    {
    uint32 regAddr = Tha60210011ModulePohBaseAddress((ThaModulePoh)self) + cAf6Reg_upen_amemask;
    uint32 regField;
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    regField = mRegField(regVal, cAf6_upen_amemask_AMESFAlmMask_);
    return HwMask2SdhAlarm(regField);
    }

static eAtRet UpsrSdAlarmMaskSet(Tha60210011ModulePoh self, uint32 alarmMask)
    {
    uint32 regAddr = Tha60210011ModulePohBaseAddress((ThaModulePoh)self) + cAf6Reg_upen_amemask;
    uint32 regField = SdhAlarm2HwMask(alarmMask);
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_amemask_AMESDAlmMask_, regField);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 UpsrSdAlarmMaskGet(Tha60210011ModulePoh self)
    {
    uint32 regAddr = Tha60210011ModulePohBaseAddress((ThaModulePoh)self) + cAf6Reg_upen_amemask;
    uint32 regField;
    uint32 regVal;

    regVal = mModuleHwRead(self, regAddr);
    regField = mRegField(regVal, cAf6_upen_amemask_AMESDAlmMask_);
    return HwMask2SdhAlarm(regField);
    }

static uint32 JnRequests(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 0xF0FF;
    }

static eAtRet JnRequestDdrEnable(Tha60210011ModulePoh self, eBool enable)
    {
    uint32 regVal = mModuleHwRead(self, 0x200000);
    uint32 jnRequests = mMethodsGet(mThis(self))->JnRequests(mThis(self));
    mRegFieldSet(regVal, cRegJnRequestDdr, (enable) ? jnRequests : 0);
    mModuleHwWrite(self, 0x200000, regVal);
    return cAtOk;
    }

static uint32 StspohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6Reg_pohstspohgrb_Base;
    }

static uint32 CpestsctrBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestsctr_Base;
    }

static uint32 MsgstsexpBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgstsexp_Base;
    }

static uint32 MsgstscurBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgstscur_Base;
    }

static uint32 MsgstsinsBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgstsins_Base;
    }

static uint32 IpmCnthiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cnthi_Base;
    }

static uint32 AlmMskhiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_mskhi_Base;
    }

static eBool InterruptIsSupported(ThaModulePoh self)
    {
    return mMethodsGet(mThis(self))->PohInterruptIsSupported(mThis(self));
    }

static uint32 StartVersionSupportPohInterrupt(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x08, 0x14, 0x00);
    }

static eBool PohInterruptIsSupported(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportPohInterrupt(device))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AlmStahiBase(ThaModulePoh self)
    {
    if (InterruptIsSupported(self))
        return cAf6Reg_alm_stahi_Base;

    return cAf6Reg_ipm_stahi_Base;
    }

static uint32 AlmChghiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_chghi_Base;
    }

static uint32 VtpohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6Reg_pohvtpohgrb_Base;
    }

static uint32 MsgvtinsBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgvtins_Base;
    }

static uint32 IpmCntloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cntlo_Base;
    }

static uint32 AlmStaloBase(ThaModulePoh self)
    {
    if (InterruptIsSupported(self))
        return cAf6Reg_alm_stalo_Base;
    return cAf6Reg_ipm_stalo_Base;
    }

static uint32 AlmChgloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_chglo_Base;
    }

static uint32 MsgvtcurBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgvtcur_Base;
    }

static uint32 HoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    ThaModuleSdhHwSts48ToHwSts24Get(hwSlice, hwSts, &hwSlice, &hwSts);
    return (hwSlice * 128UL) + hwSts;
    }

static uint32 LoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts, uint8 vtgId, uint8 vtId)
    {
    AtUnused(self);
    ThaModuleSdhHwSts48ToHwSts24Get(hwSlice, hwSts, &hwSlice, &hwSts);
    return (hwSlice * 4096UL) + (hwSts * 32UL) + (vtgId * 4UL) + vtId;
    }

static eBool LocalAddressBelongTo155Part(Tha60210011ModulePoh self, uint32 localAddress)
    {
    AtUnused(self);

    if ((localAddress >= 0x24000) && (localAddress <= 0x27FFF))
        return cAtFalse;

    return cAtTrue;
    }

static eBool AddressBelongTo155Part(Tha60210011ModulePoh self, uint32 regAddr)
    {
    uint32 localAddr = regAddr & cBit19_0;
    return mMethodsGet(self)->LocalAddressBelongTo155Part(self, localAddr);
    }

static uint32 StartVersionSupportSeparateRdiErdiS(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool SeparateRdiErdiSIsSupported(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionSupportSeparateRdiErdiS(self);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool OnebitRdiGenerationIsSupported(Tha60210011ModulePoh self)
    {
    /* One-bit RDI generation and separate RDI/ERDI-S are associated together in
     * a single revision update for all projects. */
    return SeparateRdiErdiSIsSupported(self);
    }

static eBool ShouldCheckTtiOofStatus(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return Tha60210011ModulePohPslTtiInterruptIsSupported((ThaModulePoh)self);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Reg_pohstspohgrb);
        mMethodOverride(m_methods, Reg_pohvtpohgrb);
        mMethodOverride(m_methods, Reg_pohcpestsctr);
        mMethodOverride(m_methods, Reg_pohramberrateststu3);
        mMethodOverride(m_methods, Reg_pohcpevtctr);
        mMethodOverride(m_methods, Reg_pohmsgstsexp);
        mMethodOverride(m_methods, Reg_pohmsgstscur);
        mMethodOverride(m_methods, Reg_pohmsgvtexp);
        mMethodOverride(m_methods, Reg_pohmsgvtcur);
        mMethodOverride(m_methods, Reg_pohmsgstsins);
        mMethodOverride(m_methods, Reg_pohmsgvtins);
        mMethodOverride(m_methods, Reg_ter_ctrlhi);
        mMethodOverride(m_methods, Reg_ter_ctrllo);
        mMethodOverride(m_methods, Reg_rtlpohccterbufhi);
        mMethodOverride(m_methods, Reg_rtlpohccterbuflo);
        mMethodOverride(m_methods, Reg_ipm_cnthi);
        mMethodOverride(m_methods, Reg_ipm_cntlo);
        mMethodOverride(m_methods, Reg_pohcpestssta);
        mMethodOverride(m_methods, Reg_pohcpevtsta);
        mMethodOverride(m_methods, StartVersionSupportSdSfInterrupt);
        mMethodOverride(m_methods, NumberStsSlices);
        mMethodOverride(m_methods, NumberVtSlices);
		mMethodOverride(m_methods, StartVersionSupportBlockModeCounters);
        mMethodOverride(m_methods, PslTtiInterruptIsSupported);
        mMethodOverride(m_methods, UpsrIsSupported);
        mMethodOverride(m_methods, JnRequestDdrEnable);
        mMethodOverride(m_methods, JnRequests);
        mMethodOverride(m_methods, HoPathInterruptOffset);
        mMethodOverride(m_methods, LoPathInterruptOffset);
        mMethodOverride(m_methods, LocalAddressBelongTo155Part);
        mMethodOverride(m_methods, StartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_methods, ShouldCheckTtiOofStatus);
        mMethodOverride(m_methods, PohInterruptIsSupported);
        }

    mMethodsSet(pohModule, &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePohMethods = mMethodsGet(pohModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, SdhChannelRegShow);
        mMethodOverride(m_ThaModulePohOverride, AllVtsPohMonEnable);
        mMethodOverride(m_ThaModulePohOverride, StartVersionSupportPohMonitorEnabling);
        mMethodOverride(m_ThaModulePohOverride, SoftStickyIsNeeded);
        mMethodOverride(m_ThaModulePohOverride, StspohgrbBase);
        mMethodOverride(m_ThaModulePohOverride, CpestsctrBase);
        mMethodOverride(m_ThaModulePohOverride, MsgstsexpBase);
        mMethodOverride(m_ThaModulePohOverride, MsgstscurBase);
        mMethodOverride(m_ThaModulePohOverride, MsgstsinsBase);
        mMethodOverride(m_ThaModulePohOverride, IpmCnthiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmMskhiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmStahiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmChghiBase);
        mMethodOverride(m_ThaModulePohOverride, VtpohgrbBase);
        mMethodOverride(m_ThaModulePohOverride, MsgvtinsBase);
        mMethodOverride(m_ThaModulePohOverride, IpmCntloBase);
        mMethodOverride(m_ThaModulePohOverride, AlmStaloBase);
        mMethodOverride(m_ThaModulePohOverride, AlmChgloBase);
        mMethodOverride(m_ThaModulePohOverride, MsgvtcurBase);
        mMethodOverride(m_ThaModulePohOverride, BaseAddress);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideThaModulePohV2(AtModule self)
    {
    ThaModulePohV2 pohModule = (ThaModulePohV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverrideV2, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverrideV2));

        mMethodOverride(m_ThaModulePohOverrideV2, DefaultSet);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverrideV2);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePoh(self);
    OverrideThaModulePohV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModulePoh);
    }

AtModule Tha60210011ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModulePohObjectInit(newModule, device);
    }

uint32 Tha60210011Reg_pohstspohgrb(ThaModulePoh self, AtSdhChannel sdhChannel, uint8 sliceid, uint8 stsid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohstspohgrb(self, sdhChannel, sliceid, stsid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohvtpohgrb(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohvtpohgrb(self, sliceid, stsid, vtid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohcpestsctr(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohcpestsctr(self, sliceid, stsid, tu3en) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohramberrateststu3(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohramberrateststu3(self, sliceid, stsid, tu3en) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohcpevtctr(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohcpevtctr(self, sliceid, stsid, vtid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohmsgstsexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohmsgstsexp(self, sliceid, stsid, msgid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohmsgstscur(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohmsgstscur(self, sliceid, stsid, msgid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohmsgvtexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohmsgvtexp(self, sliceid, stsid, vtid, msgid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohmsgvtcur(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohmsgvtcur(self, sliceid, stsid, vtid, msgid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohmsgstsins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohmsgstsins(self, sliceid, stsid, msgid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohmsgvtins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohmsgvtins(self, sliceid, stsid, vtid, msgid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_ter_ctrlhi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_ter_ctrlhi(self, sts, ocid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_ter_ctrllo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_ter_ctrllo(self, sts, ocid, vt) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_rtlpohccterbufhi(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 bgrp)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_rtlpohccterbufhi(self, sts, ocid, bgrp) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_rtlpohccterbuflo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt, uint8 bgrp)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_rtlpohccterbuflo(self, sts, ocid, vt, bgrp) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_ipm_cnthi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_ipm_cnthi(self, sts, ocid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_ipm_cntlo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_ipm_cntlo(self, sts, ocid, vt) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohcpestssta(ThaModulePoh self, uint8 sliceid, uint8 stsid)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohcpestssta(self, sliceid, stsid) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

uint32 Tha60210011Reg_pohcpevtsta(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vt)
    {
    if (self)
        return mMethodsGet(mThis(self))->Reg_pohcpevtsta(self, sliceid, stsid, vt) + Tha60210011ModulePohBaseAddress(self);
    return cInvalidAddress;
    }

eBool Tha60210011ModulePohSdSfInterruptIsSupported(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    return (AtDeviceVersionNumber(device) > mMethodsGet(mThis(self))->StartVersionSupportSdSfInterrupt(mThis(self))) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210011ModulePohNumberStsSlices(Tha60210011ModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->NumberStsSlices(self);
    return 0;
    }

uint32 Tha60210011ModulePohNumberVtSlices(Tha60210011ModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->NumberVtSlices(self);
    return 0;
    }

eBool Tha60210011ModulePohBlockModeCountersIsSupported(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;
        
    return (AtDeviceVersionNumber(device) >= mMethodsGet(mThis(self))->StartVersionSupportBlockModeCounters(mThis(self))) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210011ModulePohCpeVtCtrl(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpevtctr_Base;
    }

uint32 Tha60210011ModulePohCpeVtOffset(Tha60210011ModulePoh self, AtSdhVc vc, uint8 tug2Id, uint8 tu1xId)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtId = (tug2Id * 4UL) + tu1xId;
    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cAtModuleSdh, &slice, &hwStsInSlice) == cAtOk)
        return Tha60210011Reg_pohcpevtctr((ThaModulePoh)self, slice, hwStsInSlice, (uint8)vtId);
    return 0;
    }

eBool Tha60210011ModulePohPslTtiInterruptIsSupported(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->PslTtiInterruptIsSupported(self);

    return cAtFalse;
    }

eBool Tha60210011ModulePohUpsrIsSupported(Tha60210011ModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->UpsrIsSupported(self);
    return cAtFalse;
    }

eAtRet Tha60210011ModulePohUpsrSfAlarmMaskSet(Tha60210011ModulePoh self, uint32 alarmMask)
    {
    if (self)
        return UpsrSfAlarmMaskSet(self, alarmMask);
    return cAtErrorNullPointer;
    }

uint32 Tha60210011ModulePohUpsrSfAlarmMaskGet(Tha60210011ModulePoh self)
    {
    if (self)
        return UpsrSfAlarmMaskGet(self);
    return 0;
    }

eAtRet Tha60210011ModulePohUpsrSdAlarmMaskSet(Tha60210011ModulePoh self, uint32 alarmMask)
    {
    if (self)
        return UpsrSdAlarmMaskSet(self, alarmMask);
    return cAtErrorNullPointer;
    }

uint32 Tha60210011ModulePohUpsrSdAlarmMaskGet(Tha60210011ModulePoh self)
    {
    if (self)
        return UpsrSdAlarmMaskGet(self);
    return 0;
    }

eAtRet Tha60210011ModulePohJnRequestDdrEnable(Tha60210011ModulePoh self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->JnRequestDdrEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60210011ModulePohInterruptIsSupported(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->PohInterruptIsSupported(mThis(self));
    return cAtFalse;
    }

uint32 Tha60210011ModulePohHoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->HoPathInterruptOffset(self, hwSlice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModulePohLoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts, uint8 vtgId, uint8 vtId)
    {
    if (self)
        return mMethodsGet(self)->LoPathInterruptOffset(self, hwSlice, hwSts, vtgId, vtId);
    return cInvalidUint32;
    }

eBool Tha60210011ModulePohAddressBelongTo155Part(Tha60210011ModulePoh self, uint32 regAddr)
    {
    if (self)
        return AddressBelongTo155Part(self, regAddr);
    return cAtFalse;
    }

eBool Tha60210011ModulePohSeparateRdiErdiSIsSupported(Tha60210011ModulePoh self)
    {
    if (self)
        return SeparateRdiErdiSIsSupported(self);
    return cAtFalse;
    }

eBool Tha60210011ModulePohOnebitRdiGenerationIsSupported(Tha60210011ModulePoh self)
    {
    if (self)
        return OnebitRdiGenerationIsSupported(self);
    return cAtFalse;
    }

eBool Tha60210011ModulePohShouldCheckTtiOofStatus(Tha60210011ModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->ShouldCheckTtiOofStatus(self);
    return cAtFalse;
    }

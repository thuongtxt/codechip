/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210011ModulePohInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : POH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPOHINTERNAL_H_
#define _THA60210011MODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/poh/Tha60150011ModulePoh.h"
#include "Tha60210011ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePohMethods
    {
    eBool (*LocalAddressBelongTo155Part)(Tha60210011ModulePoh self, uint32 localAddress);
    uint32 (*RegPohvtpohgrbSliceidFactor)(ThaModulePoh self);   /* Formula      : Address + $sliceid*1344 + $stsid*28 + $vtid*/
    uint32 (*RegPohcpestsctrSliceidFactor)(ThaModulePoh self);  /* Formula      : {Address + $sliceid*96 + $stsid*2 + $tu3en}*/
    uint32 (*RegPohcpevtctrSliceidFactor)(ThaModulePoh self);   /* Formula      : {Address + $sliceid*1344 + $stsid*28 +$vtid}*/
    uint32 (*RegPohmsgstsexpSliceidFactor)(ThaModulePoh self);  /* Formula      : {Address + $sliceid*384 + $stsid*8 + $msgid}*/
    uint32 (*RegPohmsgstsexpStsidFactor)(ThaModulePoh self);
    uint32 (*JnRequests)(Tha60210011ModulePoh self);

    uint32 (*Reg_pohstspohgrb)              (ThaModulePoh self, AtSdhChannel sdhChannel, uint8 sliceid, uint8 stsid);  /*(0x024000+(stsid)*8+(sliceid))                         */
    uint32 (*Reg_pohvtpohgrb)               (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid);               /*(0x026000+(sliceid)*1344+(stsid)*28+(vtid))            */
    uint32 (*Reg_pohcpestsctr)              (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en);              /*(0x02A000+(sliceid)*96+(stsid)*2+(tu3en))              */
    uint32 (*Reg_pohcpevtctr)               (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid);               /*(0x028000+(sliceid)*1344+(stsid)*28+(vtid))            */
    uint32 (*Reg_pohmsgstsexp)              (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid);              /*(0x0B0000+(sliceid)*384+(stsid)*8+(msgid))             */
    uint32 (*Reg_pohmsgstscur)              (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid);              /*(0x0B1000+(sliceid)*384+(stsid)*8+(msgid))             */
    uint32 (*Reg_pohmsgvtexp)               (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid);  /*(0x080000+(sliceid)*10752+(stsid)*224+(vtid)*8+(msgid))*/
    uint32 (*Reg_pohmsgvtcur)               (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid);  /*(0x090000+(sliceid)*10752+(stsid)*224+(vtid)*8+(msgid))*/
    uint32 (*Reg_pohmsgstsins)              (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid);               /*(0x0B2000+(sliceid)*512+(stsid)*8+(msgid))             */
    uint32 (*Reg_pohmsgvtins)               (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid);   /*(0x0A0000+(sliceid)*10752+(stsid)*224+(vtid)*8+(msgid))*/
    uint32 (*Reg_ter_ctrlhi)                (ThaModulePoh self, uint8 sts, uint8 ocid);                                 /*(0x040400+(STS)*8+(OCID))                              */
    uint32 (*Reg_ter_ctrllo)                (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt);                       /*(0x044000+(STS)*168+(OCID)*28+(VT))                    */
    uint32 (*Reg_rtlpohccterbufhi)          (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 bgrp);                     /*(0x010800+(OCID)*256+(STS)*4+(BGRP))                   */
    uint32 (*Reg_rtlpohccterbuflo)          (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt, uint8 bgrp);           /*(0x018000+(OCID)*4096+(STS)*64+(VT)*2+(BGRP))          */
    uint32 (*Reg_ipm_cnthi)                 (ThaModulePoh self, uint8 sts, uint8 ocid);
    uint32 (*Reg_ipm_cntlo)                 (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt);
    uint32 (*Reg_pohramberrateststu3)       (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en);
    uint32 (*Reg_pohcpestssta)              (ThaModulePoh self, uint8 sliceid, uint8 stsid);                           /*(0x02D500+(stsid)+(sliceid)*48)                         */
    uint32 (*Reg_pohcpevtsta)               (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid);               /*(0x02C000+(sliceid)*1344+(stsid)*28+(vtid))            */
    uint32 (*HoPathInterruptOffset)(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts);
    uint32 (*LoPathInterruptOffset)(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts, uint8 vtgId, uint8 vtId);
    uint32 (*StartVersionSupportSdSfInterrupt)(Tha60210011ModulePoh self);
    uint32 (*StartVersionSupportBlockModeCounters)(Tha60210011ModulePoh self);
    uint32 (*StartVersionSupportSeparateRdiErdiS)(Tha60210011ModulePoh self);
    eAtRet (*JnRequestDdrEnable)(Tha60210011ModulePoh self, eBool enable);
    
    /* Capability */
    eBool (*PslTtiInterruptIsSupported)(ThaModulePoh self);
    eBool (*UpsrIsSupported)(Tha60210011ModulePoh self);
    eBool (*ShouldCheckTtiOofStatus)(Tha60210011ModulePoh self);
    eBool (*PohInterruptIsSupported)(Tha60210011ModulePoh self);

    /* For interrupt process. */
    uint32 (*NumberStsSlices)(Tha60210011ModulePoh self);
    uint32 (*NumberVtSlices)(Tha60210011ModulePoh self);
    }tTha60210011ModulePohMethods;

typedef struct tTha60210011ModulePoh
    {
    tTha60150011ModulePoh super;
    const tTha60210011ModulePohMethods *methods;
    }tTha60210011ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPOHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210011ModulePoh.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : POH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPOH_H_
#define _THA60210011MODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/poh/ThaModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#define Tha60210011ModulePohBaseAddress ThaModulePohBaseAddress

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePoh* Tha60210011ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModulePohObjectInit(AtModule self, AtDevice device);

uint32 Tha60210011Reg_pohstspohgrb      (ThaModulePoh self, AtSdhChannel sdhChannel, uint8 sliceid, uint8 stsid);
uint32 Tha60210011Reg_pohvtpohgrb       (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid);
uint32 Tha60210011Reg_pohcpestsctr      (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en);
uint32 Tha60210011Reg_pohramberrateststu3(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en);
uint32 Tha60210011Reg_pohcpevtctr       (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid);
uint32 Tha60210011Reg_pohmsgstsexp      (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid);
uint32 Tha60210011Reg_pohmsgstscur      (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid);
uint32 Tha60210011Reg_pohmsgvtexp       (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid);
uint32 Tha60210011Reg_pohmsgvtcur       (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid);
uint32 Tha60210011Reg_pohmsgstsins      (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid);
uint32 Tha60210011Reg_pohmsgvtins       (ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid);
uint32 Tha60210011Reg_ter_ctrlhi        (ThaModulePoh self, uint8 sts, uint8 ocid);
uint32 Tha60210011Reg_ter_ctrllo        (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt);
uint32 Tha60210011Reg_rtlpohccterbufhi  (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 bgrp);
uint32 Tha60210011Reg_rtlpohccterbuflo  (ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt, uint8 bgrp);
uint32 Tha60210011Reg_ipm_cnthi(ThaModulePoh self, uint8 sts, uint8 ocid);
uint32 Tha60210011Reg_ipm_cntlo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt);
uint32 Tha60210011Reg_pohcpestssta(ThaModulePoh self, uint8 sliceid, uint8 stsid);
uint32 Tha60210011Reg_pohcpevtsta(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vt);
uint32 Tha60210011ModulePohCpeVtCtrl(Tha60210011ModulePoh self);
uint32 Tha60210011ModulePohCpeVtOffset(Tha60210011ModulePoh self, AtSdhVc vc, uint8 tug2Id, uint8 tu1xId);
eBool Tha60210011ModulePohSdSfInterruptIsSupported(ThaModulePoh self);
uint32 Tha60210011ModulePohNumberStsSlices(Tha60210011ModulePoh self);
uint32 Tha60210011ModulePohNumberVtSlices(Tha60210011ModulePoh self);
eBool Tha60210011ModulePohBlockModeCountersIsSupported(ThaModulePoh self);
eBool Tha60210011ModulePohPslTtiInterruptIsSupported(ThaModulePoh self);
eBool Tha60210011ModulePohUpsrIsSupported(Tha60210011ModulePoh self);
eAtRet Tha60210011ModulePohUpsrSfAlarmMaskSet(Tha60210011ModulePoh self, uint32 alarmMask);
uint32 Tha60210011ModulePohUpsrSfAlarmMaskGet(Tha60210011ModulePoh self);
eAtRet Tha60210011ModulePohUpsrSdAlarmMaskSet(Tha60210011ModulePoh self, uint32 alarmMask);
uint32 Tha60210011ModulePohUpsrSdAlarmMaskGet(Tha60210011ModulePoh self);
eAtRet Tha60210011ModulePohJnRequestDdrEnable(Tha60210011ModulePoh self, eBool enable);
eBool Tha60210011ModulePohInterruptIsSupported(ThaModulePoh self);
uint32 Tha60210011ModulePohHoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts);
uint32 Tha60210011ModulePohLoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts, uint8 vtgId, uint8 vtId);
eBool Tha60210011ModulePohAddressBelongTo155Part(Tha60210011ModulePoh self, uint32 regAddr);
eBool Tha60210011ModulePohSeparateRdiErdiSIsSupported(Tha60210011ModulePoh self);
eBool Tha60210011ModulePohOnebitRdiGenerationIsSupported(Tha60210011ModulePoh self);
uint32 Tha60210011ModulePohCpeStsTu3Ctrl(Tha60210011ModulePoh self);
eBool Tha60210011ModulePohShouldCheckTtiOofStatus(Tha60210011ModulePoh self);

#endif /* _THA60210011MODULEPOH_H_ */


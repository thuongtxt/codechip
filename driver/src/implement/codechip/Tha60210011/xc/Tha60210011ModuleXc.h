/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Tha60210011ModuleXc.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : 60210011XC module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEXC_H_
#define _THA60210011MODULEXC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleXc.h"
#include "../man/Tha60210011Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cHwSliceDisconectVal 14

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha60210011XcConnectDirection
    {
    cTha60210011XcConnectDirectionRx,
    cTha60210011XcConnectDirectionTx
    }eTha60210011XcConnectDirection;

/* Tha60210011ModuleXc 's class */
typedef struct tTha60210011ModuleXc *Tha60210011ModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnect Tha60210011HoVcCrossConnectNew(AtModuleXc module);
uint32 Tha60210011ModuleXcStsHwOffset(AtModuleXc self, uint8 hwSlice, uint8 hwSts);
eBool Tha60210011HoVcCrossConnectIsInXcProgress(AtCrossConnect self);

AtCrossConnect Tha60210011Tfi5VcCrossConnectNew(AtModuleXc module);
AtCrossConnect Tha60210011ModuleXcTfi5VcCrossConnectGet(Tha60210011ModuleXc self);

eAtRet Tha60210011ModuleXcConnectSdhChannelToLoSts(AtSdhChannel channel, uint8 swSts, uint8 loSlice, uint8 loHwSts);
eAtRet Tha60210011ModuleXcDisconnectSdhChannelToLoSts(AtSdhChannel channel, uint8 swSts, uint8 loSlice, uint8 loHwSts);
eAtRet Tha60210011ModuleXcOcnStsFromLoLineStsGet(AtModuleXc self, uint8 loSlice, uint8 loHwSts, uint8* ocnSlice, uint8* ocnSts);
eAtRet Tha60210011ModuleXcTfi5StsFromOcnStsGet(AtModuleXc self, uint8 sliceId, uint8 stsId, uint8* lineId, uint8* tfi5Sts);
eAtRet Tha60210011ModuleXcConnectSdhChannelToHoSts(AtSdhChannel self);
eAtRet Tha60210011ModuleXcDisconnectSdhChannelToHoSts(AtSdhChannel self);
eAtRet Tha60210011ModuleXcSdhChannelLoopRelease(AtSdhChannel self);
eAtRet Tha60210011ModuleXcSdhChannelLoopRemote(AtSdhChannel self);
eAtRet Tha60210011ModuleXcSdhChannelLoopLocal(AtSdhChannel self);
uint8 Tha60210011ModuleXcSdhChannelLoopModeGet(AtSdhChannel self);
uint32 Tha60210011ModuleXcLoopbackValue(Tha60210011ModuleXc self);

uint8 Tha60210011ModuleXcHwSts2SwStsGet(Tha60210011ModuleXc self, uint8 hwSts, uint8 lineId);
eAtRet Tha60210011ModuleXcOcnStsFromTfi5StsGet(AtModuleXc self, AtSdhChannel vc, uint8 tfi5Sts, uint8* sliceId, uint8* hwSts);
uint8 Tha60210011ModuleXcHwSliceIdGet(Tha60210011ModuleXc self, uint8 lineId);

/* Just make it flexible to update convert bus */
eBool Tha60210011ModuleXcNeedConfigurePohInsert(Tha60210011ModuleXc self, AtChannel destVc);
eBool Tha60210011ModuleXcPohInsertShouldEnable(Tha60210011ModuleXc self, AtChannel sourceVc);
#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEXC_H_ */


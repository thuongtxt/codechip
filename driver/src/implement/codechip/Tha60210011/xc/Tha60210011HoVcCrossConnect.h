/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CX
 * 
 * File        : Tha60210011HoVcCrossConnect.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : 60210011XC module header
 * 
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HOVCCROSSCONNECT_H_
#define _THA60210011HOVCCROSSCONNECT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011HoVcCrossConnect * Tha60210011HoVcCrossConnect;
typedef struct tTha60210011Tfi5VcCrossConnect *Tha60210011Tfi5VcCrossConnect;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* _THA60210011HOVCCROSSCONNECT_H_ */

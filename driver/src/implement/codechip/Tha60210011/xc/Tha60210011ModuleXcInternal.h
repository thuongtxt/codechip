/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60210011ModuleXcInternal.h
 *
 * Created Date: Jul 19, 2016
 *
 * Description : 60210011 module XC internal definition
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THA60210011MODULEXCINTERNAL_H_
#define _THA60210011MODULEXCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/xc/ThaModuleXcInternal.h"
#include "Tha60210011ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cStsUnusedValue cAf6_upen_xxcfglkv_AMELkvLine_Mask | cAf6_upen_xxcfglkv_AMELkvSts_Mask

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleXcMethods
    {
    AtCrossConnect (*Tfi5VcCrossConnectObjectCreate)(Tha60210011ModuleXc self);
    uint32 (*RxSxcLineIdMask)(Tha60210011ModuleXc self);
    uint8  (*RxSxcLineIdShift)(Tha60210011ModuleXc self);
    uint8  (*StartHoLineId)(Tha60210011ModuleXc self);
    uint32 (*TxSxcLineIdMask)(Tha60210011ModuleXc self);
    uint8  (*TxSxcLineIdShift)(Tha60210011ModuleXc self);
    uint8 (*HwSts2SwStsGet)(Tha60210011ModuleXc self, uint8 hwSts, uint8 lineId);
    eAtRet (*UpsrLookupHoTableInit)(Tha60210011ModuleXc self);
    uint8 (*HwSliceIdGet)(Tha60210011ModuleXc self, uint8 lineId);
    eAtRet (*HwXcInit)(Tha60210011ModuleXc self);
    eBool (*BridgeAndRollRemoved)(Tha60210011ModuleXc self);

    /* Registers */
    uint32 (*RxBrigdeAndRollSxcReg)(Tha60210011ModuleXc self);
    uint32 (*TxBrigdeAndRollSxcReg)(Tha60210011ModuleXc self);
    }tTha60210011ModuleXcMethods;

typedef struct tTha60210011ModuleXc
    {
    tThaModuleXc super;
    const tTha60210011ModuleXcMethods *methods;
    }tTha60210011ModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXc Tha60210011ModuleXcObjectInit(AtModuleXc self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEXCINTERNAL_H_ */

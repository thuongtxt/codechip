/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : XC Module
 *
 * File        : Tha60210011HoVcCrossConnectInternal.h
 *
 * Created Date: Feb 6, 2017
 *
 * Description : HO VC cross-connect class representation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HOVCCROSSCONNECTINTERNAL_H_
#define _THA60210011HOVCCROSSCONNECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/xc/ThaVcCrossConnectInternal.h"
#include "Tha60210011HoVcCrossConnect.h"
#include "Tha60210011ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011HoVcCrossConnect
    {
    tThaVcCrossConnect super;
    }tTha60210011HoVcCrossConnect;

typedef struct tTha60210011Tfi5VcCrossConnectMethods
    {
    eAtRet (*TxOcnHwConnect)(Tha60210011Tfi5VcCrossConnect self, AtChannel sourceVc, AtChannel destVc);
    }tTha60210011Tfi5VcCrossConnectMethods;

typedef struct tTha60210011Tfi5VcCrossConnect
    {
    tTha60210011HoVcCrossConnect super;
    const tTha60210011Tfi5VcCrossConnectMethods *methods;
    }tTha60210011Tfi5VcCrossConnect;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnect Tha60210011HoVcCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011HOVCCROSSCONNECTINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60210011Tfi5VcCrossConnect.c
 *
 * Created Date: Jul 5, 2015
 *
 * Description : Cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../poh/Tha60210011PohReg.h"
#include "Tha60210011ModuleXcInternal.h"
#include "Tha60210011HoVcCrossConnectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_num_sts_per_lines             48
#define cAf6Reg_max_num_lo_lines              6

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011Tfi5VcCrossConnect *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011Tfi5VcCrossConnectMethods m_methods;

/* Override */
static tAtCrossConnectMethods m_AtCrossConnectOverride;

/* To save super implementation */
static const tAtCrossConnectMethods *m_AtCrossConnectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet OcnHwStsGet(Tha60210011Tfi5VcCrossConnect self, AtChannel channel, uint8 sts1Id, uint8 *hwSlice, uint8 *hwSts)
    {
    AtUnused(self);
    return ThaSdhChannelHwStsGet((AtSdhChannel)channel, cAtModuleXc, sts1Id, hwSlice, hwSts);
    }

static eAtRet Tfi5HwStsGet(Tha60210011Tfi5VcCrossConnect self, AtChannel channel, uint8 sts1Id, uint8 *hwTfi5Line, uint8 *hwTfi5Sts)
    {
    uint8 hwSlice, hwSts;
    AtModuleXc xcModule = (AtModuleXc)AtCrossConnectModuleGet((AtCrossConnect)self);
    eAtRet ret = OcnHwStsGet(self, channel, sts1Id, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return ret;
        
    return Tha60210011ModuleXcTfi5StsFromOcnStsGet(xcModule, hwSlice, hwSts, hwTfi5Line, hwTfi5Sts);
    }

static uint32 StsHwOffset(Tha60210011Tfi5VcCrossConnect self, uint8 hwSlice, uint8 hwSts)
    {
    return Tha60210011ModuleXcStsHwOffset((AtModuleXc)AtCrossConnectModuleGet((AtCrossConnect)self), hwSlice, hwSts);
    }

static uint32 BaseAddress(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    return Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static eAtRet TxOcnHwConnect(Tha60210011Tfi5VcCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    uint32 sts_i;
    uint32 numSts = AtSdhChannelNumSts((AtSdhChannel)destVc);
    uint32 sourceSwSts = AtSdhChannelSts1Get((AtSdhChannel)sourceVc);

    AtUnused(destVc);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr, regVal;
        uint8 srcHwSlice, srcHwSts;
        uint8 desHwSlice, desHwSts;
        uint8 sourceSts1 = (uint8)(sourceSwSts + sts_i);
        eAtRet ret = cAtOk;

        ret |= OcnHwStsGet(self, sourceVc, sourceSts1, &srcHwSlice,  &srcHwSts);
        ret |= Tfi5HwStsGet(self, sourceVc, sourceSts1, &desHwSlice,  &desHwSts);
        if (ret != cAtOk)
            return ret;

        regAddr = cAf6Reg_txbarsxcramctl_Base + BaseAddress(sourceVc) + StsHwOffset(self, desHwSlice, desHwSts);
        regVal  = 0;
        mRegFieldSet(regVal, cAf6_rxbarsxcramctl_RxBarSxcLineId_, srcHwSlice);
        mRegFieldSet(regVal, cAf6_rxbarsxcramctl_RxBarSxcStsId_, srcHwSts);
        mChannelHwWrite(sourceVc, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static const char *TypeString(AtCrossConnect self)
    {
    AtUnused(self);
    return "tfi5vc_xc";
    }

static eAtRet HwConnect(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    AtUnused(self);
    AtUnused(sourceVc);
    AtUnused(destVc);
    return cAtOk;
    }

static eAtRet HwDisconnect(AtCrossConnect self, AtChannel dest)
    {
    AtUnused(self);
    AtUnused(dest);
    return cAtOk;
    }

static void OverrideAtCrossConnect(AtCrossConnect self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtCrossConnectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtCrossConnectOverride, m_AtCrossConnectMethods, sizeof(m_AtCrossConnectOverride));

        mMethodOverride(m_AtCrossConnectOverride, TypeString);
        mMethodOverride(m_AtCrossConnectOverride, HwConnect);
        mMethodOverride(m_AtCrossConnectOverride, HwDisconnect);
        }

    mMethodsSet(self, &m_AtCrossConnectOverride);
    }

static void Override(AtCrossConnect self)
    {
    OverrideAtCrossConnect(self);
    }

static void MethodsInit(AtCrossConnect self)
    {
    Tha60210011Tfi5VcCrossConnect crossConnect = (Tha60210011Tfi5VcCrossConnect)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TxOcnHwConnect);
        }

    mMethodsSet(crossConnect, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Tfi5VcCrossConnect);
    }

static AtCrossConnect ObjectInit(AtCrossConnect self, AtModuleXc module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HoVcCrossConnectObjectInit(self, module) == NULL)
        return NULL;

    /* Over-ride */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtCrossConnect Tha60210011Tfi5VcCrossConnectNew(AtModuleXc module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtCrossConnect newXc = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newXc == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newXc, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60210011ModuleXc.c
 *
 * Created Date: April 23, 2015
 *
 * Description : XC Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../poh/Tha60210011PohReg.h"
#include "../man/Tha60210011Device.h"
#include "Tha60210011ModuleXcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_num_sts_per_lines             48
#define cAf6Reg_rxsxcramctl_num_lines         14
#define cAf6Reg_rxsxcramctl_num_sts_per_line  48
#define cAf6Reg_ho_line_start_id              6
#define cAf6Reg_lo_line_start_id              0

#define cDisconnectVal 14

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleXc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleXcMethods m_methods;

/* Override */
static tAtModuleMethods    m_AtModuleOverride;
static tAtModuleXcMethods  m_AtModuleXcOverride;
static tThaModuleXcMethods m_ThaModuleXcOverride;

/* To save super implementation */
static const tAtModuleMethods   *m_AtModuleMethods   = NULL;
static const tAtModuleXcMethods *m_AtModuleXcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

/* There are two cross-connects that connects three domains:
 * 1. TFI-5 Physical line/STS
 * 2. OCN PP logical slice/STS
 * 3. HO/LO logical slice/STS
 *
 * +---------------+      +-----------------------+      +-----------------------------+
 * |TFI-5 line/STS | <--> |OCN STS PP (slice/STS) | <--> | Hi-line/lo-line (slice/STS) |
 * +---------------+      +-----------------------+      +-----------------------------+
 */

static uint32 BaseAddress(AtModuleXc self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    return Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static uint32 BridgeAndRollSxcBaseAddressForDirection(AtModuleXc self, eTha60210011XcConnectDirection direction)
    {
    uint32 moduleBaseAddress = BaseAddress(self);

    if (direction == cTha60210011XcConnectDirectionRx)
        return mMethodsGet(mThis(self))->RxBrigdeAndRollSxcReg(mThis(self)) + moduleBaseAddress;
    if (direction == cTha60210011XcConnectDirectionTx)
        return mMethodsGet(mThis(self))->TxBrigdeAndRollSxcReg(mThis(self)) + moduleBaseAddress;

    return cBit31_0;
    }

static eAtRet BridgeAndRollStsConnect(AtModuleXc self,
                                      uint8 sourceLineId, uint8 sourceHwSts,
                                      uint8 destLineId, uint8 destHwSts,
                                      eTha60210011XcConnectDirection direction)
    {
    uint32 baseAddress = BridgeAndRollSxcBaseAddressForDirection(self, direction);
    uint32 regAddr = baseAddress + Tha60210011ModuleXcStsHwOffset(self, destLineId, destHwSts);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_rxbarsxcramctl_RxBarSxcLineId_, sourceLineId);
    mRegFieldSet(regVal, cAf6_rxbarsxcramctl_RxBarSxcStsId_, sourceHwSts);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static AtModuleSdh SdhModule(AtModuleXc self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eAtRet UpsrLookupHoTableInit(Tha60210011ModuleXc self)
    {
    uint8 numTfi5Lines = AtModuleSdhNumTfi5Lines(SdhModule((AtModuleXc)self));
    uint8 tfi5_i;
    eAtRet ret = cAtOk;
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    for (tfi5_i = 0; tfi5_i < numTfi5Lines; tfi5_i++)
        {
        uint8 sts_i;

        for (sts_i = 0; sts_i < cAf6Reg_num_sts_per_lines; sts_i++)
            {
            uint32 regAddr = Tha60210011ModulePohBaseAddress(modulePoh) + (uint32)cAf6Reg_upen_xxcfglks(tfi5_i, sts_i);
            uint32 regVal = 0;

            mRegFieldSet(regVal, cAf6_upen_xxcfglks_AMELksLine_, tfi5_i);
            mRegFieldSet(regVal, cAf6_upen_xxcfglks_AMELksSts_, Tha60210011ModuleXcHwSts2SwStsGet(self, sts_i, tfi5_i));
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return ret;
    }
    
static eAtRet UpsrLookupLoTableInit(AtModuleXc self)
    {
    uint8 numLines = Tha60210011ModuleSdhMaxNumLoLines(SdhModule(self));
    uint8 lineId, sts_i;
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    for (lineId = 0; lineId < numLines; lineId++)
        {
        for (sts_i = 0; sts_i < cAf6Reg_rxsxcramctl_num_sts_per_line; sts_i++)
            {
            /* Reset UPSR LO lookup table. */
            uint32 regAddr = Tha60210011ModulePohBaseAddress(modulePoh) + (uint32)cAf6Reg_upen_xxcfglkv(lineId, sts_i);
            mModuleHwWrite(self, regAddr, cStsUnusedValue);
            }
        }

    return cAtOk;
    }

static eAtRet BridgeAndRollSxcInit(AtModuleXc self)
    {
    uint8 numTfi5Lines = AtModuleSdhNumTfi5Lines(SdhModule(self));
    uint8 tfi5_i;
    eAtRet ret = cAtOk;

    for (tfi5_i = 0; tfi5_i < numTfi5Lines; tfi5_i++)
        {
        uint8 sts_i;

        for (sts_i = 0; sts_i < cAf6Reg_num_sts_per_lines; sts_i++)
            {
            BridgeAndRollStsConnect(self, tfi5_i, sts_i, tfi5_i, sts_i, cTha60210011XcConnectDirectionRx);
            BridgeAndRollStsConnect(self, tfi5_i, sts_i, tfi5_i, sts_i, cTha60210011XcConnectDirectionTx);
            }
        }

    return ret;
    }

static ThaModuleOcn ModuleOcn(AtModuleXc self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    }

static eAtRet OcnLineXcInit(AtModuleXc self)
    {
    uint8 line_i;
    uint32 baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ModuleOcn(self)) + BaseAddress(self);
    uint8 numLines = Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)AtModuleDeviceGet((AtModule)self));

    for (line_i = 0; line_i < numLines; line_i++)
        {
        uint8 sts_i;
        for (sts_i = 0; sts_i < cAf6Reg_num_sts_per_lines; sts_i++)
            {
            uint32 regVal = 0;
            uint32 regAddr = baseAddress + Tha60210011ModuleXcStsHwOffset(self, line_i, sts_i);
            mFieldIns(&regVal,
                      mMethodsGet(mThis(self))->TxSxcLineIdMask(mThis(self)),
                      mMethodsGet(mThis(self))->TxSxcLineIdShift(mThis(self)),
                      cDisconnectVal);
            mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, cBit6_0);
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static eAtRet HoLoLineXcInit(AtModuleXc self, uint8 lineId)
    {
    uint32 baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ModuleOcn(self)) + BaseAddress(self);
    uint8 sts_i;
    uint32 rxSxcLineIdMask = mMethodsGet(mThis(self))->RxSxcLineIdMask(mThis(self));
    uint8  rxSxcLineIdShift = mMethodsGet(mThis(self))->RxSxcLineIdShift(mThis(self));

    for (sts_i = 0; sts_i < cAf6Reg_rxsxcramctl_num_sts_per_line; sts_i++)
        {
        uint32 regVal = 0;
        uint32 regAddr = baseAddress + Tha60210011ModuleXcStsHwOffset(self, lineId, sts_i);

        mRegFieldSet(regVal, rxSxcLineId, cDisconnectVal);
        mRegFieldSet(regVal, cAf6_rxsxcramctl_RxSxcStsId_, cBit6_0);
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet HoLineXcInit(AtModuleXc self)
    {
    uint8 numLines = Tha60210011ModuleSdhMaxNumHoLines(SdhModule(self));
    uint8 lineId;
    eAtRet ret = cAtOk;

    for (lineId = 0; lineId < numLines; lineId++)
        ret |= HoLoLineXcInit(self, (uint8)(lineId + mMethodsGet(mThis(self))->StartHoLineId(mThis(self))));

    return ret;
    }

static eAtRet LoLineXcInit(AtModuleXc self)
    {
    uint8 numLines = Tha60210011ModuleSdhMaxNumLoLines(SdhModule(self));
    uint8 lineId;
    eAtRet ret = cAtOk;

    for (lineId = 0; lineId < numLines; lineId++)
        ret |= HoLoLineXcInit(self, lineId + cAf6Reg_lo_line_start_id);

    return ret;
    }

static eAtRet HwXcInit(Tha60210011ModuleXc self)
    {
    AtModuleXc xcModule = (AtModuleXc)self;
    eAtRet ret = cAtOk;

    ret |= OcnLineXcInit(xcModule);
    ret |= HoLineXcInit(xcModule);
    ret |= LoLineXcInit(xcModule);

    return ret;
    }

static eAtRet UpsrTableInit(AtModuleXc self)
    {
    eAtRet ret = mMethodsGet(mThis(self))->UpsrLookupHoTableInit(mThis(self));
    ret |= UpsrLookupLoTableInit(self);

    return ret;
    }

static eBool UpsrIsSupported(AtModule self)
    {
    Tha60210011ModulePoh modulePoh = (Tha60210011ModulePoh)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModulePoh);
    return Tha60210011ModulePohUpsrIsSupported(modulePoh);
    }

static eAtRet Init(AtModule self)
    {
    /* Super setup */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= mMethodsGet(mThis(self))->HwXcInit(mThis(self));

    if (!mMethodsGet(mThis(self))->BridgeAndRollRemoved(mThis(self)))
        ret |= BridgeAndRollSxcInit((AtModuleXc)self);

    if (UpsrIsSupported(self))
        ret |= UpsrTableInit((AtModuleXc)self);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Tfi5MultiRegsDisplay(ThaModuleXc self, AtSdhChannel sdhChannel, uint32 regBaseAddress, const char* regName)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 hwSlice, hwSts;
        uint32 address;

        ThaSdhChannelHwStsGet(sdhChannel, cAtModuleXc, (uint8)(startSts + sts_i), &hwSlice, &hwSts);
        address = regBaseAddress + Tha60210011ModuleXcStsHwOffset((AtModuleXc)self, hwSlice, hwSts);
        ThaDeviceChannelRegValueDisplay(channel, address, cAtModuleXc);
        }
    }

static void HoLoMultiRegsDisplay(ThaModuleXc self, AtSdhChannel sdhChannel, uint32 regBaseAddress, const char* regName)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;
    uint16 *allHwSts;

    ThaDeviceRegNameDisplay(regName);
    allHwSts = AtSdhChannelAllHwSts(sdhChannel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address;
        uint8 hwSlice, hwSts;

        ThaSdhChannelHwStsGet((AtSdhChannel)channel, cAtModuleXc, (uint8)(startSts + sts_i), &hwSlice, &hwSts);
        if (!AtSdhChannelIsChannelizedVc(sdhChannel))
            hwSlice = (uint8)(hwSlice + mMethodsGet(mThis(self))->StartHoLineId(mThis(self)));
        else
            {
            if (allHwSts == NULL)
                return;

            hwSlice = mSliceFromCache(allHwSts[sts_i]);
            hwSts   = mHwStsFromCache(allHwSts[sts_i]);
            }

        address = regBaseAddress + Tha60210011ModuleXcStsHwOffset((AtModuleXc)self, hwSlice, hwSts);
        ThaDeviceChannelRegValueDisplay(channel, address, cAtModuleXc);
        }
    }

static void SdhChannelRegsShow(ThaModuleXc self, AtSdhChannel sdhChannel)
    {
    uint32 regAddr;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    if (!AtSdhChannelIsHoVc(sdhChannel))
        return;

    Tfi5MultiRegsDisplay(self, sdhChannel, ThaSdhVcXcBaseAddressGet((ThaSdhVc)sdhChannel), "OCN Tx SXC Control");
    regAddr = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)self);
    HoLoMultiRegsDisplay(self, sdhChannel, regAddr, "OCN Rx SXC Control");
    }

static AtCrossConnect VcCrossConnectObjectCreate(AtModuleXc self)
    {
    return Tha60210011Tfi5VcCrossConnectNew(self);
    }

static AtCrossConnect Tfi5VcCrossConnectObjectCreate(Tha60210011ModuleXc self)
    {
    return Tha60210011Tfi5VcCrossConnectNew((AtModuleXc)self);
    }

static uint32 RxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_rxsxcramctl_RxSxcLineId_Mask;
    }

static uint8 RxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_rxsxcramctl_RxSxcLineId_Shift;
    }

static uint8 StartHoLineId(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6Reg_ho_line_start_id;
    }

static uint32 TxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_txsxcramctl_TxSxcLineId_Mask;
    }

static uint8 TxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_txsxcramctl_TxSxcLineId_Shift;
    }

static uint32 StsHwOffset(uint8 hwSlice, uint8 hwSts)
    {
    return (256UL * hwSlice) + hwSts;
    }

static uint8 LineIdFromLoLineStsInLoopbackState(Tha60210011ModuleXc self, uint8 loSlice, uint8 loHwSts)
    {
    uint32 baseAddress;
    uint8 lineId, stsId;
    ThaModuleOcn ocnModule = ModuleOcn((AtModuleXc)self);
    AtModuleSdh sdhModule = SdhModule((AtModuleXc)self);

    /* In loop-back state, we need to scan all STSs of XC to find
     * the line containing STS cross-connecting to LO-STS */
    baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)self);
    for (lineId = 0; lineId < AtModuleSdhNumTfi5Lines(sdhModule); lineId++)
        {
        for (stsId = 0; stsId < ThaModuleOcnNumStsInOneSlice(ocnModule); stsId++)
            {
            uint32 regAddr = baseAddress + StsHwOffset(lineId, stsId);
            uint32 regVal  = mModuleHwRead(self, regAddr);
            uint8 txSxcLineId = (uint8)mRegField(regVal, cAf6_txsxcramctl_TxSxcLineId_);
            uint8 txSxcStsId = (uint8)mRegField(regVal, cAf6_txsxcramctl_TxSxcStsId_);

            if ((txSxcLineId == loSlice) && (loHwSts == txSxcStsId))
                return lineId;
            }
        }

    return cInvalidSlice;
    }

static uint8 HwSliceIdGet(Tha60210011ModuleXc self, uint8 lineId)
    {
    AtUnused(self);
    return lineId;
    }

static eBool NeedConfigurePohInsert(Tha60210011ModuleXc self, AtChannel destVc)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet(destVc);
    AtUnused(self);
    return Tha60210011ModuleSdhIsTfi5Line(sdhModule, AtSdhChannelLineGet((AtSdhChannel)destVc));
    }

static uint32 RxBrigdeAndRollSxcReg(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6Reg_rxbarsxcramctl_Base;
    }

static uint32 TxBrigdeAndRollSxcReg(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6Reg_txbarsxcramctl_Base;
    }

static eBool PohInsertShouldEnable(Tha60210011ModuleXc self, AtChannel sourceVc)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet(sourceVc);
    AtUnused(self);
    return  Tha60210011ModuleSdhIsLoLine(sdhModule, AtSdhChannelLineGet((AtSdhChannel)sourceVc));
    }

static uint8 HwSts2SwStsGet(Tha60210011ModuleXc self, uint8 hwSts, uint8 lineId)
    {
    AtUnused(self);
    AtUnused(lineId);
    return (uint8)((hwSts % 16) * 3 + hwSts / 16);
    }
    
static uint32 LoopbackValue(Tha60210011ModuleXc self)
    {
    return mMethodsGet(self)->RxSxcLineIdMask(self) >>
           mMethodsGet(self)->RxSxcLineIdShift(self);
    }

static eAtRet SdhChannelHoStsLoopLocal(AtSdhChannel self)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice, sts_i;
    uint8 hoSlice, hoHwSts;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);
    eAtRet ret;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, swSts, &slice, &hwSts);
        if (ret != cAtOk)
            return ret;

        hoSlice = (uint8)(slice + mMethodsGet(xcModule)->StartHoLineId(xcModule));
        hoHwSts = hwSts;

        baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(hoSlice, hoHwSts);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldIns(&regVal,
                  mMethodsGet(xcModule)->RxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->RxSxcLineIdShift(xcModule),
                  LoopbackValue(xcModule));
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static eAtRet SdhChannelLoStsLoopLocal(AtSdhChannel self)
    {
    uint16 *allHwSts = AtSdhChannelAllHwSts(self);
    uint32 regAddr, regVal, baseAddress;
    uint8 sts_i;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);

    if (allHwSts == NULL)
        return cAtError;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 loSlice = mSliceFromCache(allHwSts[sts_i]);
        uint8 loHwSts = mHwStsFromCache(allHwSts[sts_i]);

        baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress(xcModule);
        regAddr = baseAddress + StsHwOffset(loSlice, loHwSts);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldIns(&regVal,
                  mMethodsGet(mThis(xcModule))->RxSxcLineIdMask(mThis(xcModule)),
                  mMethodsGet(mThis(xcModule))->RxSxcLineIdShift(mThis(xcModule)),
                  LoopbackValue(mThis(xcModule)));
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static eBool SdhChannelIsLoopbackRemote(AtSdhChannel self)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice, sts_i, txSlice;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        eAtRet ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, swSts, &slice, &hwSts);
        if (ret != cAtOk)
            return cAtFalse;

        baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(slice, hwSts);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldGet(regVal,
                  mMethodsGet(xcModule)->TxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->TxSxcLineIdShift(xcModule),
                  uint8, &txSlice);
        if (txSlice == LoopbackValue(xcModule))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool SdhChannelHoStsIsLoopLocal(AtSdhChannel self)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice, sts_i, rxSlice;
    uint8 hoSlice, hoHwSts;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        eAtRet ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, swSts, &slice, &hwSts);
        if (ret != cAtOk)
            return cAtFalse;

        hoSlice = (uint8)(slice + mMethodsGet(xcModule)->StartHoLineId(xcModule));
        hoHwSts = hwSts;

        baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(hoSlice, hoHwSts);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldGet(regVal,
                  mMethodsGet(xcModule)->RxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->RxSxcLineIdShift(xcModule),
                  uint8, &rxSlice);
        if (rxSlice == LoopbackValue(xcModule))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool SdhChannelLoStsIsLoopLocal(AtSdhChannel self)
    {
    uint16 *allHwSts = AtSdhChannelAllHwSts(self);
    uint32 regAddr, regVal, baseAddress;
    uint8 sts_i;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);

    if (allHwSts == NULL)
        return cAtError;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 loSlice = mSliceFromCache(allHwSts[sts_i]);
        uint8 loHwSts = mHwStsFromCache(allHwSts[sts_i]);
        uint8 rxSlice;

        baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress(xcModule);
        regAddr = baseAddress + StsHwOffset(loSlice, loHwSts);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldGet(regVal,
                  mMethodsGet(mThis(xcModule))->RxSxcLineIdMask(mThis(xcModule)),
                  mMethodsGet(mThis(xcModule))->RxSxcLineIdShift(mThis(xcModule)),
                  uint8, &rxSlice);
        if (rxSlice == LoopbackValue(mThis(xcModule)))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool SdhChannelIsLoopbackLocal(AtSdhChannel self)
    {
    if (!AtSdhChannelIsChannelizedVc(self))
        return SdhChannelHoStsIsLoopLocal(self);

    return SdhChannelLoStsIsLoopLocal(self);
    }

static eBool BridgeAndRollRemoved(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleXc(AtModuleXc self)
    {
    ThaModuleXc thaModuleXc = (ThaModuleXc)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleXcOverride, mMethodsGet(thaModuleXc), sizeof(m_ThaModuleXcOverride));

        mMethodOverride(m_ThaModuleXcOverride, SdhChannelRegsShow);
        }

    mMethodsSet(thaModuleXc, &m_ThaModuleXcOverride);
    }

static void OverrideAtModuleXc(AtModuleXc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleXcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleXcOverride, m_AtModuleXcMethods, sizeof(m_AtModuleXcOverride));

        mMethodOverride(m_AtModuleXcOverride, VcCrossConnectObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleXcOverride);
    }

static void OverrideAtModule(AtModuleXc self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleXc self)
    {
    OverrideAtModule(self);
    OverrideAtModuleXc(self);
    OverrideThaModuleXc(self);
    }

static void MethodsInit(AtModuleXc self)
    {
    Tha60210011ModuleXc xcModule = mThis(self);
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Tfi5VcCrossConnectObjectCreate);
        mMethodOverride(m_methods, RxSxcLineIdMask);
        mMethodOverride(m_methods, RxSxcLineIdShift);
        mMethodOverride(m_methods, StartHoLineId);
        mMethodOverride(m_methods, TxSxcLineIdMask);
        mMethodOverride(m_methods, TxSxcLineIdShift);
        mMethodOverride(m_methods, HwSliceIdGet);
        mMethodOverride(m_methods, RxBrigdeAndRollSxcReg);
        mMethodOverride(m_methods, TxBrigdeAndRollSxcReg);
        mMethodOverride(m_methods, HwSts2SwStsGet);
        mMethodOverride(m_methods, UpsrLookupHoTableInit);
        mMethodOverride(m_methods, HwXcInit);
        mMethodOverride(m_methods, BridgeAndRollRemoved);
        }

    mMethodsSet(xcModule, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleXc);
    }

AtModuleXc Tha60210011ModuleXcObjectInit(AtModuleXc self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleXcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXc Tha60210011ModuleXcNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleXc newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleXcObjectInit(newModule, device);
    }

uint32 Tha60210011ModuleXcStsHwOffset(AtModuleXc self, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    return StsHwOffset(hwSlice, hwSts);
    }

/*
 * +---------------+        +-----------------------+        +-----------------------------+
 * |TFI-5 line/STS | <-XC-> |OCN STS PP (slice/STS) | <-XC-> | Hi-line/lo-line (slice/STS) |
 * +---------------+        +-----------------------+        +-----------------------------+
 */

/*
 * +-----------------------+        +-----------------------------+
 * |OCN STS PP (slice/STS) | <-XC-> | Hi-line/lo-line (slice/STS) |
 * +-----------------------+        +-----------------------------+
 */
eAtRet Tha60210011ModuleXcOcnStsFromLoLineStsGet(AtModuleXc self, uint8 loSlice, uint8 loHwSts, uint8* ocnSlice, uint8* ocnSts)
    {
    uint32 regAddr, regVal, baseAddress;

    if (ThaModuleSdhUseStaticXc((ThaModuleSdh)SdhModule(self)))
        {
        *ocnSlice = loSlice;
        *ocnSts = loHwSts;

        return cAtOk;
        }

    baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ModuleOcn(self)) + BaseAddress((AtModuleXc)self);
    regAddr = baseAddress + StsHwOffset(loSlice, loHwSts);

    regVal    = mModuleHwRead(self, regAddr);
    mFieldGet(regVal,
              mMethodsGet(mThis(self))->RxSxcLineIdMask(mThis(self)),
              mMethodsGet(mThis(self))->RxSxcLineIdShift(mThis(self)),
              uint8,
              ocnSlice);
    *ocnSts = (uint8)mRegField(regVal, cAf6_rxsxcramctl_RxSxcStsId_);

    if (*ocnSlice == cHwSliceDisconectVal)
        {
        *ocnSlice = cInvalidSlice;
        *ocnSts = cInvalidHwSts;
        }

    if (*ocnSlice == LoopbackValue(mThis(self)))
        *ocnSlice = LineIdFromLoLineStsInLoopbackState(mThis(self), loSlice, loHwSts);

    return cAtOk;
    }

/*
 * +---------------+        +-----------------------+
 * |TFI-5 line/STS | <-XC-> |OCN STS PP (slice/STS) |
 * +---------------+        +-----------------------+
 */
eAtRet Tha60210011ModuleXcTfi5StsFromOcnStsGet(AtModuleXc self, uint8 sliceId, uint8 stsId, uint8* lineId, uint8* tfi5Sts)
    {
    if (lineId) *lineId = 0;
    if (tfi5Sts) *tfi5Sts = 0;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (lineId) *lineId = sliceId;
    if (tfi5Sts) *tfi5Sts = stsId;
    return cAtOk;
    }

eAtRet Tha60210011ModuleXcConnectSdhChannelToLoSts(AtSdhChannel channel, uint8 swSts, uint8 loSlice, uint8 loHwSts)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);
    uint8 tfi5Line, tfi5Sts, lineId;
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModulePoh);

    eAtRet ret = ThaSdhChannelHwStsGet(channel, cAtModuleXc, swSts, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress(xcModule);
    regAddr = baseAddress + StsHwOffset(slice, hwSts);
    regVal  = 0;
    mFieldIns(&regVal,
              mMethodsGet(mThis(xcModule))->TxSxcLineIdMask(mThis(xcModule)),
              mMethodsGet(mThis(xcModule))->TxSxcLineIdShift(mThis(xcModule)),
              loSlice);
    mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, loHwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress(xcModule);
    regAddr = baseAddress + StsHwOffset(loSlice, loHwSts);
    regVal  = 0;
    mFieldIns(&regVal,
              mMethodsGet(mThis(xcModule))->RxSxcLineIdMask(mThis(xcModule)),
              mMethodsGet(mThis(xcModule))->RxSxcLineIdShift(mThis(xcModule)),
              slice);
    mRegFieldSet(regVal, cAf6_rxsxcramctl_RxSxcStsId_, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    /* Update UPSR lookup table */
    if (!UpsrIsSupported((AtModule)xcModule))
        return cAtOk;

    Tha60210011ModuleXcTfi5StsFromOcnStsGet(xcModule, slice, hwSts, &tfi5Line, &tfi5Sts);

    regAddr = Tha60210011ModulePohBaseAddress(modulePoh) + (uint32)cAf6Reg_upen_xxcfglkv(loSlice, loHwSts);
    regVal  = 0;
    lineId  = AtSdhChannelLineGet(channel);
    mRegFieldSet(regVal, cAf6_upen_xxcfglkv_AMELkvLine_, lineId);
    mRegFieldSet(regVal, cAf6_upen_xxcfglkv_AMELkvSts_, Tha60210011ModuleXcHwSts2SwStsGet(mThis(xcModule), tfi5Sts, lineId));
    mChannelHwWrite(channel, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

eAtRet Tha60210011ModuleXcDisconnectSdhChannelToLoSts(AtSdhChannel channel, uint8 swSts, uint8 loSlice, uint8 loHwSts)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModulePoh);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);
    eAtRet ret;

    if (AtDeviceIsDeleting(AtChannelDeviceGet((AtChannel)channel)))
        return cAtOk;

    ret = ThaSdhChannelHwStsGet(channel, cAtModuleXc, swSts, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
    regAddr = baseAddress + StsHwOffset(slice, hwSts);
    regVal  = 0;
    mFieldIns(&regVal,
              mMethodsGet(mThis(xcModule))->TxSxcLineIdMask(mThis(xcModule)),
              mMethodsGet(mThis(xcModule))->TxSxcLineIdShift(mThis(xcModule)),
              cHwSliceDisconectVal);
    mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
    regAddr = baseAddress + StsHwOffset(loSlice, loHwSts);
    regVal  = 0;
    mFieldIns(&regVal,
              mMethodsGet(xcModule)->RxSxcLineIdMask(xcModule),
              mMethodsGet(xcModule)->RxSxcLineIdShift(xcModule),
              cHwSliceDisconectVal);
    mRegFieldSet(regVal, cAf6_rxsxcramctl_RxSxcStsId_, loHwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    /* Update UPSR lookup table */
    if (!UpsrIsSupported((AtModule)xcModule))
        return cAtOk;

    /* Reset UPSR lookup table */
    regAddr = Tha60210011ModulePohBaseAddress(modulePoh) + (uint32)cAf6Reg_upen_xxcfglkv(loSlice, loHwSts);
    mChannelHwWrite(channel, regAddr, cStsUnusedValue, cThaModulePoh);

    return cAtOk;
    }

eAtRet Tha60210011ModuleXcConnectSdhChannelToHoSts(AtSdhChannel self)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice, sts_i;
    uint8 hoSlice, hoHwSts;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);
    eAtRet ret;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, swSts, &slice, &hwSts);
        if (ret != cAtOk)
            return ret;

        hoSlice = (uint8)(slice + mMethodsGet(xcModule)->StartHoLineId(xcModule));
        hoHwSts = hwSts;

        baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(slice, hwSts);
        regVal  = 0;
        mFieldIns(&regVal,
                  mMethodsGet(xcModule)->TxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->TxSxcLineIdShift(xcModule),
                  hoSlice);
        mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, hoHwSts);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

        baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(hoSlice, hoHwSts);
        regVal  = 0;
        mFieldIns(&regVal,
                  mMethodsGet(xcModule)->RxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->RxSxcLineIdShift(xcModule),
                  slice);
        mRegFieldSet(regVal, cAf6_rxsxcramctl_RxSxcStsId_, hwSts);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

eAtRet Tha60210011ModuleXcDisconnectSdhChannelToHoSts(AtSdhChannel self)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice, sts_i;
    uint8 hoSlice;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);
    eAtRet ret;

    if (AtDeviceIsDeleting(AtChannelDeviceGet((AtChannel)self)))
        return cAtOk;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, swSts, &slice, &hwSts);
        if (ret != cAtOk)
            return ret;

        hoSlice = (uint8)(slice + mMethodsGet(xcModule)->StartHoLineId(xcModule));
        baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(slice, hwSts);
        regVal  = 0;
        mFieldIns(&regVal,
                  mMethodsGet(xcModule)->TxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->TxSxcLineIdShift(xcModule),
                  cHwSliceDisconectVal);
        mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, hwSts);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

        baseAddress = Tha60210011ModuleOcnRxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(hoSlice, hwSts);
        regVal  = 0;
        mFieldIns(&regVal,
                  mMethodsGet(xcModule)->RxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->RxSxcLineIdShift(xcModule),
                  cHwSliceDisconectVal);
        mRegFieldSet(regVal, cAf6_rxsxcramctl_RxSxcStsId_, hwSts);
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

eAtRet Tha60210011ModuleXcSdhChannelLoopRemote(AtSdhChannel self)
    {
    uint32 regAddr, regVal, baseAddress;
    uint8 hwSts, slice, sts_i;
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)self);
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ocnModule), cAtModuleXc);
    eAtRet ret;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        ret = ThaSdhChannelHwStsGet(self, cAtModuleXc, swSts, &slice, &hwSts);
        if (ret != cAtOk)
            return ret;

        baseAddress = Tha60210011ModuleOcnTxSxcControlRegAddr((Tha60210011ModuleOcn)ocnModule) + BaseAddress((AtModuleXc)xcModule);
        regAddr = baseAddress + StsHwOffset(slice, hwSts);
        regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
        mFieldIns(&regVal,
                  mMethodsGet(xcModule)->TxSxcLineIdMask(xcModule),
                  mMethodsGet(xcModule)->TxSxcLineIdShift(xcModule),
                  LoopbackValue(xcModule));
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

eAtRet Tha60210011ModuleXcSdhChannelLoopLocal(AtSdhChannel self)
    {
    if (!AtSdhChannelIsChannelizedVc(self))
        return SdhChannelHoStsLoopLocal(self);

    return SdhChannelLoStsLoopLocal(self);
    }

eAtRet Tha60210011ModuleXcSdhChannelLoopRelease(AtSdhChannel self)
    {
    uint16 *allHwSts;
    uint8 sts_i;
    eAtRet ret = cAtOk;

    if (!AtSdhChannelIsChannelizedVc(self))
        return Tha60210011ModuleXcConnectSdhChannelToHoSts(self);

    allHwSts = AtSdhChannelAllHwSts(self);
    if (allHwSts == NULL)
        return cAtError;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(self); sts_i++)
        {
        uint8 swSts = (uint8)(AtSdhChannelSts1Get(self) + sts_i);
        uint8 loSlice = mSliceFromCache(allHwSts[sts_i]);
        uint8 loHwsts = mHwStsFromCache(allHwSts[sts_i]);

        ret |= Tha60210011ModuleXcConnectSdhChannelToLoSts(self, swSts, loSlice, loHwsts);
        }

    return ret;
    }

uint8 Tha60210011ModuleXcHwSliceIdGet(Tha60210011ModuleXc self, uint8 lineId)
    {
    if (self)
        return mMethodsGet(self)->HwSliceIdGet(self, lineId);
    return 0xFF;
    }

eBool Tha60210011ModuleXcNeedConfigurePohInsert(Tha60210011ModuleXc self, AtChannel destVc)
    {
    if (self)
        return NeedConfigurePohInsert(self, destVc);
    return cAtFalse;
    }

eBool Tha60210011ModuleXcPohInsertShouldEnable(Tha60210011ModuleXc self, AtChannel sourceVc)
    {
    if (self)
        return PohInsertShouldEnable(self, sourceVc);
    return cAtFalse;
    }

eAtRet Tha60210011ModuleXcOcnStsFromTfi5StsGet(AtModuleXc self, AtSdhChannel vc, uint8 tfi5Sts, uint8* sliceId, uint8* hwSts)
    {
    uint8 tfi5HwSts;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);

    if (self == NULL)
        return cAtErrorNullPointer;

    tfi5HwSts = Stm16SdhChannelSwStsId2HwFlatId(ocnModule, vc, tfi5Sts);

    if (sliceId)
        *sliceId = Tha60210011ModuleXcHwSliceIdGet(mThis(self), AtSdhChannelLineGet(vc));
    if (hwSts)
        *hwSts = tfi5HwSts;
    return cAtOk;
    }

uint8 Tha60210011ModuleXcSdhChannelLoopModeGet(AtSdhChannel self)
    {
    if (SdhChannelIsLoopbackRemote(self))
        return cAtLoopbackModeRemote;

    if (SdhChannelIsLoopbackLocal(self))
        return cAtLoopbackModeLocal;

    return cAtLoopbackModeRelease;
    }

uint8 Tha60210011ModuleXcHwSts2SwStsGet(Tha60210011ModuleXc self, uint8 hwSts, uint8 lineId)
    {
    if (self)
        return mMethodsGet(self)->HwSts2SwStsGet(self, hwSts, lineId);
    return cInvalidUint8;
    }

uint32 Tha60210011ModuleXcLoopbackValue(Tha60210011ModuleXc self)
    {
    if (self)
        return LoopbackValue(self);
    return cInvalidUint32;
    }

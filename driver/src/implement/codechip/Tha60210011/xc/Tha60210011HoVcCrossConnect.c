/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60210011HoVcCrossConnect.c
 *
 * Created Date: April, 2015
 *
 * Description : Cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcnReg.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "Tha60210011ModuleXcInternal.h"
#include "Tha60210011HoVcCrossConnectInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011HoVcCrossConnect *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtCrossConnectMethods       m_AtCrossConnectOverride;

/* To save super implementation */
static const tAtCrossConnectMethods     *m_AtCrossConnectMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsSameSide(AtChannel source, AtChannel dest)
	{
    return (ThaSdhVcXcBaseAddressGet((ThaSdhVc)source) == ThaSdhVcXcBaseAddressGet((ThaSdhVc)dest)) ? cAtTrue : cAtFalse;
	}

static uint32 HwSliceOffSet(ThaVcCrossConnect self, AtSdhChannel channel)
    {
    AtUnused(self);
    if (Tha60210011ModuleSdhChannelIsHoPath(channel))
        return 6;
    return 0;
    }

static eAtRet Tha60210011HoVcCrossConnectHwStsGet(ThaVcCrossConnect self, AtSdhChannel channel, uint8 sts1Id, uint8 *hwSlice, uint8 *hwSts)
    {
    uint8 hwRealSlice = 0;
    eAtRet ret;

    ret = ThaVcCrossConnectHwStsGet(self, channel, sts1Id, &hwRealSlice, hwSts);
    if (ret != cAtOk)
    	return ret;

    hwRealSlice = (uint8)(hwRealSlice + HwSliceOffSet(self, channel));
    *hwSlice = hwRealSlice;

    return cAtOk;
    }

static uint32 StsHwOffset(AtCrossConnect self, uint8 hwSlice, uint8 hwSts)
    {
    return Tha60210011ModuleXcStsHwOffset((AtModuleXc)AtCrossConnectModuleGet(self), hwSlice, hwSts);
    }

static Tha60210011ModuleXc tha60210011ModuleXc(AtModule sdhModule)
    {
    return (Tha60210011ModuleXc) AtDeviceModuleGet(AtModuleDeviceGet(sdhModule), cAtModuleXc);
    }

static eBool IsBlockingCrossConnect(AtCrossConnect self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HwIsConnected(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    uint32 sts_i;
    uint32 baseAddress = ThaSdhVcXcBaseAddressGet((ThaSdhVc)destVc);
    uint32 numSts = AtSdhChannelNumSts((AtSdhChannel)destVc);
    uint32 sourceSwSts = cInvalidUint32;
    uint32 destSwSts = AtSdhChannelSts1Get((AtSdhChannel)destVc);
    Tha60210011ModuleXc moduleXc = (Tha60210011ModuleXc)AtCrossConnectModuleGet(self);

    if (sourceVc)
        sourceSwSts = AtSdhChannelSts1Get((AtSdhChannel)sourceVc);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr, regVal;
        uint8 srcHwSlice = 0, srcHwSts = 0;
        uint8 desHwSlice = 0, desHwSts = 0;
        uint8 sourceSts1;
        uint8 destSts1   = (uint8)(destSwSts + sts_i);
        eAtRet ret = cAtOk;
        uint8 actualSourceHwSlice, actualSourceHwSts;

        if (sourceVc)
            {
            sourceSts1 = (uint8)(sourceSwSts + sts_i);
            ret |= Tha60210011HoVcCrossConnectHwStsGet((ThaVcCrossConnect)self, (AtSdhChannel)sourceVc, sourceSts1, &srcHwSlice, &srcHwSts);
            }

        ret |= Tha60210011HoVcCrossConnectHwStsGet((ThaVcCrossConnect)self, (AtSdhChannel)destVc, destSts1, &desHwSlice,  &desHwSts);
        if (ret != cAtOk)
            return cAtFalse;

        regAddr = baseAddress + StsHwOffset(self, desHwSlice, desHwSts);
        regVal  = mChannelHwRead(destVc, regAddr, cAtModuleXc);
        mFieldGet(regVal,
                  mMethodsGet(moduleXc)->TxSxcLineIdMask(moduleXc),
                  mMethodsGet(moduleXc)->TxSxcLineIdShift(moduleXc),
                  uint8,
                  &actualSourceHwSlice);
        actualSourceHwSts = (uint8)mRegField(regVal, cAf6_txsxcramctl_TxSxcStsId_);

        if (sourceVc && ((actualSourceHwSlice != srcHwSlice) || (actualSourceHwSts != srcHwSts)))
            return cAtFalse;

        if ((sourceVc == NULL) && (actualSourceHwSlice == cHwSliceDisconectVal))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet HwConnect(AtCrossConnect self, AtChannel sourceVc, AtChannel destVc)
    {
    uint32 sts_i;
    uint32 baseAddress = ThaSdhVcXcBaseAddressGet((ThaSdhVc)destVc);
    uint32 numSts = AtSdhChannelNumSts((AtSdhChannel)destVc);
    uint32 sourceSwSts = AtSdhChannelSts1Get((AtSdhChannel)sourceVc);
    uint32 destSwSts = AtSdhChannelSts1Get((AtSdhChannel)destVc);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet(sourceVc);
    eBool pohInsert = cAtFalse;
    Tha60210011ModuleXc moduleXc = (Tha60210011ModuleXc)AtCrossConnectModuleGet(self);
    eBool needCfgPohInsert = Tha60210011ModuleXcNeedConfigurePohInsert(tha60210011ModuleXc((AtModule) sdhModule), destVc);
    eBool isBlocking = mMethodsGet(self)->IsBlockingCrossConnect(self);
    if (needCfgPohInsert)
        pohInsert = Tha60210011ModuleXcPohInsertShouldEnable(tha60210011ModuleXc((AtModule) sdhModule), sourceVc);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr, regVal;
        uint8 srcHwSlice = 0, srcHwSts = 0;
        uint8 destHwSlice = 0, destHwSts = 0;
        uint8 sourceSts1 = (uint8)(sourceSwSts + sts_i);
        uint8 destSts1   = (uint8)(destSwSts + sts_i);
        eAtRet ret = cAtOk;
        uint32 loopbackVal = 0;

        ret |= Tha60210011HoVcCrossConnectHwStsGet((ThaVcCrossConnect)self, (AtSdhChannel)sourceVc, sourceSts1, &srcHwSlice, &srcHwSts);
        ret |= Tha60210011HoVcCrossConnectHwStsGet((ThaVcCrossConnect)self, (AtSdhChannel)destVc, destSts1, &destHwSlice,  &destHwSts);
        if (ret != cAtOk)
            return ret;

        regAddr = baseAddress + StsHwOffset(self, destHwSlice, destHwSts);
        regVal  = 0;
        loopbackVal = isBlocking ? Tha60210011ModuleXcLoopbackValue(moduleXc) : srcHwSlice;
        mFieldIns(&regVal, 
        		  mMethodsGet(moduleXc)->TxSxcLineIdMask(moduleXc),
                  mMethodsGet(moduleXc)->TxSxcLineIdShift(moduleXc),
                  IsSameSide(sourceVc, destVc) ?  loopbackVal : srcHwSlice);
        mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, srcHwSts);
        mChannelHwWrite(destVc, regAddr, regVal, cThaModuleOcn);

        if (needCfgPohInsert == cAtFalse)
            continue;

        ret = ThaOcnStsPohInsertEnable((AtSdhChannel)destVc, destSts1, pohInsert);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet HwDisconnect(AtCrossConnect self, AtChannel dest)
    {
    eAtRet ret;
    uint32 sts_i;
    uint32 baseAddress = ThaSdhVcXcBaseAddressGet((ThaSdhVc)dest);
    uint32 numSts = AtSdhChannelNumSts((AtSdhChannel) dest);
    uint32 destSwSts = AtSdhChannelSts1Get((AtSdhChannel)dest);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet(dest);
    eBool needCfgPohInsert = cAtFalse;
    Tha60210011ModuleXc moduleXc = (Tha60210011ModuleXc)AtCrossConnectModuleGet(self);

    if (Tha60210011ModuleSdhIsTfi5Line(sdhModule, AtSdhChannelLineGet((AtSdhChannel)dest)))
        needCfgPohInsert = cAtTrue;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr, regVal = 0;
        uint8 destHwSlice = 0, destHwSts = 0;
        uint8 destSts1 = (uint8)(destSwSts + sts_i);

        Tha60210011HoVcCrossConnectHwStsGet((ThaVcCrossConnect)self, (AtSdhChannel)dest, destSts1, &destHwSlice,  &destHwSts);

        regAddr = baseAddress + StsHwOffset(self, destHwSlice, destHwSts);
        mFieldIns(&regVal,
                  mMethodsGet(moduleXc)->TxSxcLineIdMask(moduleXc),
                  mMethodsGet(moduleXc)->TxSxcLineIdShift(moduleXc),
                  cHwSliceDisconectVal);
        mRegFieldSet(regVal, cAf6_txsxcramctl_TxSxcStsId_, destHwSts);
        mChannelHwWrite(dest, regAddr, regVal, cThaModuleOcn);

        if (needCfgPohInsert == cAtFalse)
            continue;

        ret = ThaOcnStsPohInsertEnable((AtSdhChannel)dest, (uint8)destSts1, cAtTrue);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static void OverrideAtCrossConnect(AtCrossConnect self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtCrossConnectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtCrossConnectOverride, m_AtCrossConnectMethods, sizeof(m_AtCrossConnectOverride));

        mMethodOverride(m_AtCrossConnectOverride, HwConnect);
        mMethodOverride(m_AtCrossConnectOverride, HwDisconnect);
        mMethodOverride(m_AtCrossConnectOverride, HwIsConnected);
        mMethodOverride(m_AtCrossConnectOverride, IsBlockingCrossConnect);
        }

    mMethodsSet(self, &m_AtCrossConnectOverride);
    }

static void Override(AtCrossConnect self)
    {
    OverrideAtCrossConnect(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011HoVcCrossConnect);
    }

AtCrossConnect Tha60210011HoVcCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcCrossConnectObjectInit(self, module) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    m_methodsInit = 1;
    
    return self;
    }

AtCrossConnect Tha60210011HoVcCrossConnectNew(AtModuleXc module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtCrossConnect newXc = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newXc == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011HoVcCrossConnectObjectInit(newXc, module);
    }

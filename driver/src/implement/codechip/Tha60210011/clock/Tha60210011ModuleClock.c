/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60210011ModuleClock.c
 *
 * Created Date: Dec 23, 2015
 *
 * Description : Clock Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumExtractors(AtModuleClock self)
    {
    AtUnused(self);
    return 2;
    }

static eBool ShouldEnableCdrInterrupt(ThaModuleClock self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Init(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return Tha60210011ClockExtractorNew((AtModuleClock)self, extractorId);
    }

static ThaClockMonitor ClockMonitorCreate(ThaModuleClock self)
    {
    AtUnused(self);
    return NULL;
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        mMethodOverride(m_ThaModuleClockOverride, ClockMonitorCreate);
        mMethodOverride(m_ThaModuleClockOverride, ShouldEnableCdrInterrupt);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModule(self);
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleClock);
    }

AtModuleClock Tha60210011ModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60210011ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleClockObjectInit(newModule, device);
    }

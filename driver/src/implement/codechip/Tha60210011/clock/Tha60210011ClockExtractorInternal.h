/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60210011ClockExtractorInternal.h
 * 
 * Created Date: Oct 6, 2016
 *
 * Description : Tha60210011ClockExtractorInternal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011CLOCKEXTRACTORINTERNAL_H_
#define _THA60210011CLOCKEXTRACTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ClockExtractor
    {
    tThaStmPwProductClockExtractor super;
    }tTha60210011ClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor Tha60210011ClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011CLOCKEXTRACTORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60210011ClockExtractor.c
 *
 * Created Date: Dec 28, 2015
 *
 * Description : Clock extractor implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ClockExtractorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegClockExtractSelection         0x11
#define cRegClockSecondRef8KhzDisMask     cBit7
#define cRegClockSecondRef8KhzDisShift    7

#define cRegClockSecondRef8KhzOcnIdMask   cBit6_4
#define cRegClockSecondRef8KhzOcnIdShift  4

#define cRegClockFirstRef8KhzDisMask      cBit3
#define cRegClockFirstRef8KhzDisShift     3

#define cRegClockFirstRef8KhzOcnIdMask    cBit2_0
#define cRegClockFirstRef8KhzOcnIdShift   0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanExtractSdhSystemClock(ThaClockExtractor self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanExtractSdhLineClock(ThaClockExtractor self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtTrue;
    }

static eBool CanExtractExternalClock(ThaClockExtractor self, uint8 externalId)
    {
    AtUnused(externalId);
    AtUnused(self);
    return cAtFalse;
    }

static uint8 ExternalClockGet(AtClockExtractor self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cAtFalse;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 address = cRegClockExtractSelection;
    uint32 regVal  = AtClockExtractorRead(extractor, address);
    uint8 extractorId = AtClockExtractorIdGet(extractor);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)line);

    if (extractorId == 0)
        mRegFieldSet(regVal, cRegClockFirstRef8KhzOcnId, lineId);
    else if (extractorId == 1)
        mRegFieldSet(regVal, cRegClockSecondRef8KhzOcnId, lineId);
    else
        return cAtErrorOutOfRangParm;

    AtClockExtractorWrite(extractor, address, regVal);

    return cAtOk;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    uint8 hwDisableVal = (enable == cAtFalse) ? 1 : 0;
    uint32 address = cRegClockExtractSelection;
    uint32 regVal  = AtClockExtractorRead(self, address);
    uint8 extractorId = AtClockExtractorIdGet(self);

    if (extractorId == 0)
        mRegFieldSet(regVal, cRegClockFirstRef8KhzDis, hwDisableVal);
    else if (extractorId == 1)
        mRegFieldSet(regVal, cRegClockSecondRef8KhzDis, hwDisableVal);
    else
        return cAtErrorOutOfRangParm;

    AtClockExtractorWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    uint32 address = cRegClockExtractSelection;
    uint32 regVal  = AtClockExtractorRead(self, address);
    uint8 extractorId = AtClockExtractorIdGet(self);

    if (extractorId == 0)
        return mRegField(regVal, cRegClockFirstRef8KhzDis) == 0 ? cAtTrue : cAtFalse;
    if (extractorId == 1)
        return mRegField(regVal, cRegClockSecondRef8KhzDis) == 0 ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool SystemClockIsExtracted(AtClockExtractor self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhSystemClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhLineClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractExternalClock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1Clock);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        mMethodOverride(m_AtClockExtractorOverride, SystemClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, ExternalClockGet);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ClockExtractor);
    }

AtClockExtractor Tha60210011ClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60210011ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ClockExtractorObjectInit(newExtractor, clockModule, extractorId);
    }

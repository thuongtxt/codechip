/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock module name
 * 
 * File        : Tha60210011ModuleClockInternal.h
 * 
 * Created Date: Jun 9, 2016
 *
 * Description : Internal data for the clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_CLOCK_THA60210011MODULECLOCKINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_CLOCK_THA60210011MODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/


/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60210011ModuleClock
    {
    tThaStmPwProductModuleClock super;
    }tTha60210011ModuleClock;


/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60210011ModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210011_CLOCK_THA60210011MODULECLOCKINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : Tha60210011LongRegisterAccess.c
 *
 * Created Date: Jun 24, 2015
 *
 * Description : Long register access
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/util/Tha32BitGlobalLongRegisterAccessInternal.h"
#include "./Tha60210011LongRegisterAccess.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha32BitGlobalLongRegisterAccessMethods m_Tha32BitGlobalLongRegisterAccessOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *ReadHoldRegs(Tha32BitGlobalLongRegisterAccess self, uint32 *numRegs)
    {
    static uint32 holdRegs[] = {0, 0x0F00008, 0x0F00009, 0x0F0000A};
    AtUnused(self);
    if (numRegs)
        *numRegs = 4;
    return holdRegs;
    }

static uint32 *WriteHoldRegs(Tha32BitGlobalLongRegisterAccess self, uint32 *numRegs)
    {
    return ReadHoldRegs(self, numRegs);
    }

static void OverrideTha32BitGlobalLongRegisterAccess(AtLongRegisterAccess self)
    {
    Tha32BitGlobalLongRegisterAccess registerAccess = (Tha32BitGlobalLongRegisterAccess)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha32BitGlobalLongRegisterAccessOverride, mMethodsGet(registerAccess), sizeof(m_Tha32BitGlobalLongRegisterAccessOverride));

        mMethodOverride(m_Tha32BitGlobalLongRegisterAccessOverride, ReadHoldRegs);
        mMethodOverride(m_Tha32BitGlobalLongRegisterAccessOverride, WriteHoldRegs);
        }

    mMethodsSet(registerAccess, &m_Tha32BitGlobalLongRegisterAccessOverride);
    }

static void Override(AtLongRegisterAccess self)
    {
    OverrideTha32BitGlobalLongRegisterAccess(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011LongRegisterAccess);
    }

AtLongRegisterAccess Tha60210011LongRegisterAccessObjectInit(AtLongRegisterAccess self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha32BitGlobalLongRegisterAccessObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtLongRegisterAccess Tha60210011LongRegisterAccessNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtLongRegisterAccess newAccess = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAccess == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011LongRegisterAccessObjectInit(newAccess);
    }

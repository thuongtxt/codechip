/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : Tha60210011LongRegisterAccess.h
 * 
 * Created Date: Dec 4, 2015
 *
 * Description : Long register access
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011LONGREGISTERACCESS_H_
#define _THA60210011LONGREGISTERACCESS_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011LongRegisterAccess
    {
    tTha32BitGlobalLongRegisterAccess super;
    }tTha60210011LongRegisterAccess;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLongRegisterAccess Tha60210011LongRegisterAccessObjectInit(AtLongRegisterAccess self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011LONGREGISTERACCESS_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : THA60210012MODULESURINTERNAL.h
 *
 * Created Date: Jul 22, 2016
 *
 * Description : SUR module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULESURINTERNAL_H_
#define _THA60210012MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/sur/AtSurEngineInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/sur/hard/ThaModuleHardSurInternal.h"
#include "../../../default/sur/hard/engine/ThaHardSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Internal RAM names */
#define sTha60210011ModuleSurRamFMPMPWDefInterruptMASKPerTypePerPW "FMPM PW Def Interrupt MASK Per Type Per PW"
#define sTha60210011ModuleSurRamFMPMPWDefFramerInterruptMASKPerPW  "FMPM PW Def Framer Interrupt MASK Per PW"
#define sTha60210011ModuleSurRamFMPMPWFMInterruptMASKPerTypePerPW  "FMPM FM PW Interrupt MASK Per Type Per PW"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleSur
    {
    tThaModuleHardSur super;
    }tTha60210011ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha60210011ModuleSurObjectInit(AtModuleSur self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULESURINTERNAL_H_ */

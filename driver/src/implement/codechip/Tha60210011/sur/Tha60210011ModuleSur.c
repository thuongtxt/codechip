/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210011ModuleSur.c
 *
 * Created Date: Aug 12, 2015
 *
 * Description : SUR Module of product 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../pw/Tha60210011ModulePw.h"
#include "../ram/Tha60210011InternalRam.h"
#include "Tha60210011ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods          m_AtModuleOverride;
static tAtModuleSurMethods       m_AtModuleSurOverride;
static tThaModuleHardSurMethods  m_ThaModuleHardSurOverride;

/* Save super implementation */
static const tAtModuleMethods         *m_AtModuleMethods         = NULL;
static const tThaModuleHardSurMethods *m_ThaModuleHardSurMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 0x700000UL;
    }

static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static eBool IsHoPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    return Tha60210011PwCircuitBelongsToLoLine(pw) ? cAtFalse : cAtTrue;
    }

static eBool IsLoPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    return Tha60210011PwCircuitBelongsToLoLine(pw);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60210011InternalRamSurNew(self, ramId, localRamId);
    }

static eBool HasExpireTimeout(ThaModuleHardSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 versionHasIssue = ThaVersionReaderPwVersionBuild(versionReader, 0x16, 0x06, 0x24, 0x69);
    uint32 buildNumberHasIssue = ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x54);
    uint32 version = AtDeviceVersionNumber(device);
    uint32 buildNumber = ThaVersionReaderHardwareBuiltNumber(versionReader);

    if ((version == versionHasIssue) && (buildNumber == buildNumberHasIssue))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ShouldEnablePwCounters(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->HasExpireTimeout(self);
    }

static uint32 ChangePageTimeoutInMs(ThaModuleHardSur self)
    {
    uint32 timeoutMs = m_ThaModuleHardSurMethods->ChangePageTimeoutInMs(self);

    if (mMethodsGet(self)->HasExpireTimeout(self))
        {
        /* 3s setting is still fine, for the worst case, this timeout can be
         * increased by updating this code block */
        return timeoutMs;
        }

    return timeoutMs;
    }

static uint32 DdrAccessTimeoutInUs(ThaModuleHardSur self)
    {
    if (mMethodsGet(self)->HasExpireTimeout(self))
        {
        /* So far, 100ms timeout is still fine, but if any timeout issue happen,
         * need to increase this setting */
        return m_ThaModuleHardSurMethods->DdrAccessTimeoutInUs(self);
        }

    return m_ThaModuleHardSurMethods->DdrAccessTimeoutInUs(self);
    }

static eBool LineIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x4, 0x1070);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "FMPM K Threshold of PW",
         "FMPM K Threshold of DS1",
         "FMPM K Threshold of DS3",
         "FMPM K Threshold of VT",
         "FMPM K threshold of STS",
         "FMPM Threshold Type of PW",
         "FMPM Threshold Type of DS1",
         "FMPM Threshold Type of DS3",
         "FMPM Threshold Type of VT",
         "FMPM Threshold Type of STS",
         sTha60210011ModuleSurRamFMPMPWFMInterruptMASKPerTypePerPW,
         "FMPM FM PW Framer Interrupt MASK Per PW",
         "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1",
         "FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1",
         "FMPM FM DS3E3 Interrupt MASK Per Type of Per DS3E3",
         "FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3",
         "FMPM FM VTTU Interrupt MASK Per Type Per VTTU",
         "FMPM FM VTTU Interrupt MASK Per VTTU",
         "FMPM FM STS24 Interrupt MASK Per Type Of Per STS1",
         "FMPM FM STS24 Interrupt MASK Per STS1",
         sTha60210011ModuleSurRamFMPMPWDefInterruptMASKPerTypePerPW,
         sTha60210011ModuleSurRamFMPMPWDefFramerInterruptMASKPerPW
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static eBool FailureTimersConfigurable(AtModuleSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x5, 0x1023);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, LineIsSupported);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardSurMethods = mMethodsGet(surModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, m_ThaModuleHardSurMethods, sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, BaseAddress);
        mMethodOverride(m_ThaModuleHardSurOverride, IsHoPw);
        mMethodOverride(m_ThaModuleHardSurOverride, IsLoPw);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLineSurEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, ShouldEnablePwCounters);
        mMethodOverride(m_ThaModuleHardSurOverride, DdrAccessTimeoutInUs);
        mMethodOverride(m_ThaModuleHardSurOverride, ChangePageTimeoutInMs);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        mMethodOverride(m_ThaModuleHardSurOverride, HasExpireTimeout);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule surModule = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(surModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(surModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(surModule, &m_AtModuleOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleSur);
    }

AtModuleSur Tha60210011ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleHardSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60210011ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleSurObjectInit(newModule, device);
    }


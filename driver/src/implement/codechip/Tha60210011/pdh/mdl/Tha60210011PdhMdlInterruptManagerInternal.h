/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210011PdhMdlInterruptManagerInternal.h
 * 
 * Created Date: Sep 21, 2017
 *
 * Description : MDL interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PDHMDLINTERRUPTMANAGERINTERNAL_H_
#define _THA60210011PDHMDLINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pdh/mdl/ThaPdhMdlInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PdhMdlInterruptManager
    {
    tThaPdhMdlInterruptManager super;
    }tTha60210011PdhMdlInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager Tha60210011PdhMdlInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PDHMDLINTERRUPTMANAGERINTERNAL_H_ */


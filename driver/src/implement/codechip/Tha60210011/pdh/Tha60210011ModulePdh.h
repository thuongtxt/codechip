/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210011ModulePdh.h
 * 
 * Created Date: Jun 30, 2015
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPDH_H_
#define _THA60210011MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define sTha60210011ModulePdhRamPDHVTAsyncMapControl "PDH VT Async Map Control"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePdh * Tha60210011ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210011ModulePdhNumPdhRams(Tha60210011ModulePdh self);
uint32 Tha60210011ModulePdhNumPdhRamsInSlice(Tha60210011ModulePdh self);
const char** Tha60210011ModulePdhRxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32* numRams);
const char** Tha60210011ModulePdhTxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32* numRams);

AtPdhDe3 Tha60210011PdhDe3FromSdhLineGet(AtSdhLine sdhLine, uint8 stsInLine);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPDH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210011ModulePdhMdlPrmReg.h
 * 
 * Created Date: Nov 21, 2015
 *
 * Description : MDL - PRM
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPDHMDLPRMREG_H_
#define _THA60210011MODULEPDHMDLPRMREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL TX STUFF 0
Reg Addr   : 0x06000
Reg Formula:
    Where  :
Reg Desc   :
config control Stuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ctrl0_Base                                                                    0x06000
#define cAf6Reg_upen_cfg_ctrl0                                                                         0x06000
#define cAf6Reg_upen_cfg_ctrl0_WidthVal                                                                     32
#define cAf6Reg_upen_cfg_ctrl0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_txcfg_ctrl0
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:08]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Bit_Start                                                        8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Bit_End                                                         31
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Mask                                                      cBit31_8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Shift                                                            8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_MaxVal                                                    0xffffff
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_MinVal                                                         0x0
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_RstVal                                                         0x0

/*--------------------------------------
BitField Name: fcs_err_ins
BitField Type: R/W
BitField Desc: (0): disable,(1): enable insert error
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_Bit_Start                                                            7
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_Bit_End                                                              7
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_Mask                                                             cBit7
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_Shift                                                                7
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_MaxVal                                                             0x1
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_MinVal                                                             0x0
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_RstVal                                                             0x0

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve1
BitField Bits: [06:02]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_reserve1_Bit_Start                                                               2
#define cAf6_upen_cfg_ctrl0_reserve1_Bit_End                                                                 6
#define cAf6_upen_cfg_ctrl0_reserve1_Mask                                                              cBit6_2
#define cAf6_upen_cfg_ctrl0_reserve1_Shift                                                                   2
#define cAf6_upen_cfg_ctrl0_reserve1_MaxVal                                                               0x1f
#define cAf6_upen_cfg_ctrl0_reserve1_MinVal                                                                0x0
#define cAf6_upen_cfg_ctrl0_reserve1_RstVal                                                                0x0

/*--------------------------------------
BitField Name: fcsinscfg
BitField Type: R/W
BitField Desc: (1) enable insert FCS, (0) disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Bit_Start                                                              1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Bit_End                                                                1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Mask                                                               cBit1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Shift                                                                  1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_MaxVal                                                               0x1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_MinVal                                                               0x0
#define cAf6_upen_cfg_ctrl0_fcsinscfg_RstVal                                                               0x1


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE MONITOR FOR DEBUG BOARD
Reg Addr   : 0x06005
Reg Formula: 
    Where  : 
Reg Desc   : 
config enable monitor data before stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_debug_Base                                                             0x06005
#define cAf6Reg_upen_cfg_debug                                                                  0x06005
#define cAf6Reg_upen_cfg_debug_WidthVal                                                              32
#define cAf6Reg_upen_cfg_debug_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: out_cfg_selinfo
BitField Type: R/W
BitField Desc: select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1)
is enable, (0) is disable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_selinfo_Bit_Start                                                 8
#define cAf6_upen_cfg_debug_out_cfg_selinfo_Bit_End                                                   8
#define cAf6_upen_cfg_debug_out_cfg_selinfo_Mask                                                  cBit8
#define cAf6_upen_cfg_debug_out_cfg_selinfo_Shift                                                     8
#define cAf6_upen_cfg_debug_out_cfg_selinfo_MaxVal                                                  0x1
#define cAf6_upen_cfg_debug_out_cfg_selinfo_MinVal                                                  0x0
#define cAf6_upen_cfg_debug_out_cfg_selinfo_RstVal                                                  0x0

/*--------------------------------------
BitField Name: out_info_force
BitField Type: R/W
BitField Desc: 8 bits info force [07:05] : 3'd1 : to set bit G1 3'd2 :  to set
bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :
to set bit G6 [04] : to set bit SE [03] : to set bit FE [02] : to set bit LV
[01] : to set bit SL [00] : to set bit LB
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_info_force_Bit_Start                                                  0
#define cAf6_upen_cfg_debug_out_info_force_Bit_End                                                    7
#define cAf6_upen_cfg_debug_out_info_force_Mask                                                 cBit7_0
#define cAf6_upen_cfg_debug_out_info_force_Shift                                                      0
#define cAf6_upen_cfg_debug_out_info_force_MaxVal                                                  0xff
#define cAf6_upen_cfg_debug_out_info_force_MinVal                                                   0x0
#define cAf6_upen_cfg_debug_out_info_force_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : TX CONFIG FORCE ERROR PARITY
Reg Addr   : 0x06C00
Reg Formula: 
    Where  : 
Reg Desc   : 
config control FORCE ERROR PARITY

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_force_Base                                                           0x06C00
#define cAf6Reg_upen_txcfg_force                                                                0x06C00
#define cAf6Reg_upen_txcfg_force_WidthVal                                                            32
#define cAf6Reg_upen_txcfg_force_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng8
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_Bit_Start                                       25
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_Bit_End                                         25
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_Mask                                        cBit25
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_Shift                                           25
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng8
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_Bit_Start                                       24
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_Bit_End                                         24
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_Mask                                        cBit24
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_Shift                                           24
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng8
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_Bit_Start                                            23
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_Bit_End                                              23
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_Mask                                             cBit23
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_Shift                                                23
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng7
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_Bit_Start                                       22
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_Bit_End                                         22
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_Mask                                        cBit22
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_Shift                                           22
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng7
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_Bit_Start                                       21
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_Bit_End                                         21
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_Mask                                        cBit21
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_Shift                                           21
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng7
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_Bit_Start                                            20
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_Bit_End                                              20
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_Mask                                             cBit20
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_Shift                                                20
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng6
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_Bit_Start                                       19
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_Bit_End                                         19
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_Mask                                        cBit19
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_Shift                                           19
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng6
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_Bit_Start                                       18
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_Bit_End                                         18
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_Mask                                        cBit18
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_Shift                                           18
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng6
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_Bit_Start                                            17
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_Bit_End                                              17
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_Mask                                             cBit17
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_Shift                                                17
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng5
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_Bit_Start                                       16
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_Bit_End                                         16
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_Mask                                        cBit16
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_Shift                                           16
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng5
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_Bit_Start                                       15
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_Bit_End                                         15
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_Mask                                        cBit15
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_Shift                                           15
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng5
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_Bit_Start                                            14
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_Bit_End                                              14
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_Mask                                             cBit14
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_Shift                                                14
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng4
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_Bit_Start                                       13
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_Bit_End                                         13
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_Mask                                        cBit13
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_Shift                                           13
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng4
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_Bit_Start                                       12
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_Bit_End                                         12
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_Mask                                        cBit12
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_Shift                                           12
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng4
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_Bit_Start                                            11
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_Bit_End                                              11
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_Mask                                             cBit11
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_Shift                                                11
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng3
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_Bit_Start                                       10
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_Bit_End                                         10
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_Mask                                        cBit10
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_Shift                                           10
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng3
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_Bit_Start                                        9
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_Bit_End                                          9
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_Mask                                         cBit9
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_Shift                                            9
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng3
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_Bit_Start                                             8
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_Bit_End                                               8
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_Mask                                              cBit8
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_Shift                                                 8
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng2
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_Bit_Start                                        7
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_Bit_End                                          7
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_Mask                                         cBit7
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_Shift                                            7
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng2
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_Bit_Start                                        6
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_Bit_End                                          6
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_Mask                                         cBit6
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_Shift                                            6
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng2
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_Bit_Start                                             5
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_Bit_End                                               5
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_Mask                                              cBit5
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_Shift                                                 5
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng1
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_Bit_Start                                        4
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_Bit_End                                          4
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_Mask                                         cBit4
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_Shift                                            4
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng1
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_Bit_Start                                        3
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_Bit_End                                          3
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_Mask                                         cBit3
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_Shift                                            3
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_MaxVal                                         0x1
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_MinVal                                         0x0
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_RstVal                                         0x0

/*--------------------------------------
BitField Name: cfgforce_mdl_eng1
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_Bit_Start                                             2
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_Bit_End                                               2
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_Mask                                              cBit2
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_Shift                                                 2
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_MaxVal                                              0x1
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_MinVal                                              0x0
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_RstVal                                              0x0

/*--------------------------------------
BitField Name: cfgforce_lbprm
BitField Type: R/W
BitField Desc: force error PRM LB config, (0) : disable, (1) : enable force
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_lbprm_Bit_Start                                                1
#define cAf6_upen_txcfg_force_cfgforce_lbprm_Bit_End                                                  1
#define cAf6_upen_txcfg_force_cfgforce_lbprm_Mask                                                 cBit1
#define cAf6_upen_txcfg_force_cfgforce_lbprm_Shift                                                    1
#define cAf6_upen_txcfg_force_cfgforce_lbprm_MaxVal                                                 0x1
#define cAf6_upen_txcfg_force_cfgforce_lbprm_MinVal                                                 0x0
#define cAf6_upen_txcfg_force_cfgforce_lbprm_RstVal                                                 0x0

/*--------------------------------------
BitField Name: cfgforce_prm
BitField Type: R/W
BitField Desc: force error PRM config, (0) : disable, (1) : enable force
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_prm_Bit_Start                                                  0
#define cAf6_upen_txcfg_force_cfgforce_prm_Bit_End                                                    0
#define cAf6_upen_txcfg_force_cfgforce_prm_Mask                                                   cBit0
#define cAf6_upen_txcfg_force_cfgforce_prm_Shift                                                      0
#define cAf6_upen_txcfg_force_cfgforce_prm_MaxVal                                                   0x1
#define cAf6_upen_txcfg_force_cfgforce_prm_MinVal                                                   0x0
#define cAf6_upen_txcfg_force_cfgforce_prm_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : TX CONFIG DIS PARITY
Reg Addr   : 0x06C01
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DIS PARITY

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_dis_Base                                                             0x06C01
#define cAf6Reg_upen_txcfg_dis                                                                  0x06C01
#define cAf6Reg_upen_txcfg_dis_WidthVal                                                              32
#define cAf6Reg_upen_txcfg_dis_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_Bit_Start                                           25
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_Bit_End                                             25
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_Mask                                            cBit25
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_Shift                                               25
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_Bit_Start                                           24
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_Bit_End                                             24
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_Mask                                            cBit24
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_Shift                                               24
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng8
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_Bit_Start                                                23
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_Bit_End                                                  23
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_Mask                                                 cBit23
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_Shift                                                    23
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_Bit_Start                                           22
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_Bit_End                                             22
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_Mask                                            cBit22
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_Shift                                               22
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_Bit_Start                                           21
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_Bit_End                                             21
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_Mask                                            cBit21
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_Shift                                               21
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng7
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_Bit_Start                                                20
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_Bit_End                                                  20
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_Mask                                                 cBit20
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_Shift                                                    20
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_Bit_Start                                           19
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_Bit_End                                             19
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_Mask                                            cBit19
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_Shift                                               19
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_Bit_Start                                           18
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_Bit_End                                             18
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_Mask                                            cBit18
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_Shift                                               18
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng6
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_Bit_Start                                                17
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_Bit_End                                                  17
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_Mask                                                 cBit17
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_Shift                                                    17
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_Bit_Start                                           16
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_Bit_End                                             16
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_Mask                                            cBit16
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_Shift                                               16
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_Bit_Start                                           15
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_Bit_End                                             15
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_Mask                                            cBit15
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_Shift                                               15
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng5
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_Bit_Start                                                14
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_Bit_End                                                  14
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_Mask                                                 cBit14
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_Shift                                                    14
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_Bit_Start                                           13
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_Bit_End                                             13
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_Mask                                            cBit13
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_Shift                                               13
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_Bit_Start                                           12
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_Bit_End                                             12
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_Mask                                            cBit12
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_Shift                                               12
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng4
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_Bit_Start                                                11
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_Bit_End                                                  11
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_Mask                                                 cBit11
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_Shift                                                    11
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_Bit_Start                                           10
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_Bit_End                                             10
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_Mask                                            cBit10
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_Shift                                               10
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_Bit_Start                                            9
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_Bit_End                                              9
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_Mask                                             cBit9
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_Shift                                                9
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng3
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_Bit_Start                                                 8
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_Bit_End                                                   8
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_Mask                                                  cBit8
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_Shift                                                     8
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_Bit_Start                                            7
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_Bit_End                                              7
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_Mask                                             cBit7
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_Shift                                                7
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_Bit_Start                                            6
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_Bit_End                                              6
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_Mask                                             cBit6
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_Shift                                                6
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng2
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_Bit_Start                                                 5
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_Bit_End                                                   5
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_Mask                                                  cBit5
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_Shift                                                     5
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_Bit_Start                                            4
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_Bit_End                                              4
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_Mask                                             cBit4
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_Shift                                                4
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_Bit_Start                                            3
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_Bit_End                                              3
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_Mask                                             cBit3
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_Shift                                                3
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_MaxVal                                             0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_MinVal                                             0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_RstVal                                             0x0

/*--------------------------------------
BitField Name: cfgdis_mdl_eng1
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_Bit_Start                                                 2
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_Bit_End                                                   2
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_Mask                                                  cBit2
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_Shift                                                     2
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_MaxVal                                                  0x1
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_MinVal                                                  0x0
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_RstVal                                                  0x0

/*--------------------------------------
BitField Name: cfgdis_lbprm
BitField Type: R/W
BitField Desc: dis error PRM LB config, (0) : disable, (1) : enable dis
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_Bit_Start                                                    1
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_Bit_End                                                      1
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_Mask                                                     cBit1
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_Shift                                                        1
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_MaxVal                                                     0x1
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_MinVal                                                     0x0
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfgdis_prm
BitField Type: R/W
BitField Desc: dis error PRM config, (0) : disable, (1) : enable dis
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_prm_Bit_Start                                                      0
#define cAf6_upen_txcfg_dis_cfgdis_prm_Bit_End                                                        0
#define cAf6_upen_txcfg_dis_cfgdis_prm_Mask                                                       cBit0
#define cAf6_upen_txcfg_dis_cfgdis_prm_Shift                                                          0
#define cAf6_upen_txcfg_dis_cfgdis_prm_MaxVal                                                       0x1
#define cAf6_upen_txcfg_dis_cfgdis_prm_MinVal                                                       0x0
#define cAf6_upen_txcfg_dis_cfgdis_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : TX STICKY PARITY
Reg Addr   : 0x06C02
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txsticky_par_Base                                                          0x06C02
#define cAf6Reg_upen_txsticky_par                                                               0x06C02
#define cAf6Reg_upen_txsticky_par_WidthVal                                                           32
#define cAf6Reg_upen_txsticky_par_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_Bit_Start                                           25
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_Bit_End                                             25
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_Mask                                            cBit25
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_Shift                                               25
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_Bit_Start                                           24
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_Bit_End                                             24
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_Mask                                            cBit24
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_Shift                                               24
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng8
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng8_Bit_Start                                                23
#define cAf6_upen_txsticky_par_stk_mdl_eng8_Bit_End                                                  23
#define cAf6_upen_txsticky_par_stk_mdl_eng8_Mask                                                 cBit23
#define cAf6_upen_txsticky_par_stk_mdl_eng8_Shift                                                    23
#define cAf6_upen_txsticky_par_stk_mdl_eng8_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng8_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng8_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_Bit_Start                                           22
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_Bit_End                                             22
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_Mask                                            cBit22
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_Shift                                               22
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_Bit_Start                                           21
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_Bit_End                                             21
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_Mask                                            cBit21
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_Shift                                               21
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng7
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng7_Bit_Start                                                20
#define cAf6_upen_txsticky_par_stk_mdl_eng7_Bit_End                                                  20
#define cAf6_upen_txsticky_par_stk_mdl_eng7_Mask                                                 cBit20
#define cAf6_upen_txsticky_par_stk_mdl_eng7_Shift                                                    20
#define cAf6_upen_txsticky_par_stk_mdl_eng7_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng7_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng7_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_Bit_Start                                           19
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_Bit_End                                             19
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_Mask                                            cBit19
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_Shift                                               19
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_Bit_Start                                           18
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_Bit_End                                             18
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_Mask                                            cBit18
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_Shift                                               18
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng6
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng6_Bit_Start                                                17
#define cAf6_upen_txsticky_par_stk_mdl_eng6_Bit_End                                                  17
#define cAf6_upen_txsticky_par_stk_mdl_eng6_Mask                                                 cBit17
#define cAf6_upen_txsticky_par_stk_mdl_eng6_Shift                                                    17
#define cAf6_upen_txsticky_par_stk_mdl_eng6_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng6_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng6_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_Bit_Start                                           16
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_Bit_End                                             16
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_Mask                                            cBit16
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_Shift                                               16
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_Bit_Start                                           15
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_Bit_End                                             15
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_Mask                                            cBit15
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_Shift                                               15
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng5
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng5_Bit_Start                                                14
#define cAf6_upen_txsticky_par_stk_mdl_eng5_Bit_End                                                  14
#define cAf6_upen_txsticky_par_stk_mdl_eng5_Mask                                                 cBit14
#define cAf6_upen_txsticky_par_stk_mdl_eng5_Shift                                                    14
#define cAf6_upen_txsticky_par_stk_mdl_eng5_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng5_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng5_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_Bit_Start                                           13
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_Bit_End                                             13
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_Mask                                            cBit13
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_Shift                                               13
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_Bit_Start                                           12
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_Bit_End                                             12
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_Mask                                            cBit12
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_Shift                                               12
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng4
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng4_Bit_Start                                                11
#define cAf6_upen_txsticky_par_stk_mdl_eng4_Bit_End                                                  11
#define cAf6_upen_txsticky_par_stk_mdl_eng4_Mask                                                 cBit11
#define cAf6_upen_txsticky_par_stk_mdl_eng4_Shift                                                    11
#define cAf6_upen_txsticky_par_stk_mdl_eng4_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng4_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng4_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_Bit_Start                                           10
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_Bit_End                                             10
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_Mask                                            cBit10
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_Shift                                               10
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_Bit_Start                                            9
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_Bit_End                                              9
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_Mask                                             cBit9
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_Shift                                                9
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng3
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng3_Bit_Start                                                 8
#define cAf6_upen_txsticky_par_stk_mdl_eng3_Bit_End                                                   8
#define cAf6_upen_txsticky_par_stk_mdl_eng3_Mask                                                  cBit8
#define cAf6_upen_txsticky_par_stk_mdl_eng3_Shift                                                     8
#define cAf6_upen_txsticky_par_stk_mdl_eng3_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng3_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng3_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_Bit_Start                                            7
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_Bit_End                                              7
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_Mask                                             cBit7
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_Shift                                                7
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_Bit_Start                                            6
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_Bit_End                                              6
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_Mask                                             cBit6
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_Shift                                                6
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng2
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng2_Bit_Start                                                 5
#define cAf6_upen_txsticky_par_stk_mdl_eng2_Bit_End                                                   5
#define cAf6_upen_txsticky_par_stk_mdl_eng2_Mask                                                  cBit5
#define cAf6_upen_txsticky_par_stk_mdl_eng2_Shift                                                     5
#define cAf6_upen_txsticky_par_stk_mdl_eng2_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng2_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng2_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_Bit_Start                                            4
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_Bit_End                                              4
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_Mask                                             cBit4
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_Shift                                                4
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_Bit_Start                                            3
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_Bit_End                                              3
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_Mask                                             cBit3
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_Shift                                                3
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_MaxVal                                             0x1
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_MinVal                                             0x0
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_RstVal                                             0x0

/*--------------------------------------
BitField Name: stk_mdl_eng1
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng1_Bit_Start                                                 2
#define cAf6_upen_txsticky_par_stk_mdl_eng1_Bit_End                                                   2
#define cAf6_upen_txsticky_par_stk_mdl_eng1_Mask                                                  cBit2
#define cAf6_upen_txsticky_par_stk_mdl_eng1_Shift                                                     2
#define cAf6_upen_txsticky_par_stk_mdl_eng1_MaxVal                                                  0x1
#define cAf6_upen_txsticky_par_stk_mdl_eng1_MinVal                                                  0x0
#define cAf6_upen_txsticky_par_stk_mdl_eng1_RstVal                                                  0x0

/*--------------------------------------
BitField Name: stk_lbprm
BitField Type: R/W
BitField Desc: dis error PRM LB config, (0) : disable, (1) : enable dis
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_lbprm_Bit_Start                                                    1
#define cAf6_upen_txsticky_par_stk_lbprm_Bit_End                                                      1
#define cAf6_upen_txsticky_par_stk_lbprm_Mask                                                     cBit1
#define cAf6_upen_txsticky_par_stk_lbprm_Shift                                                        1
#define cAf6_upen_txsticky_par_stk_lbprm_MaxVal                                                     0x1
#define cAf6_upen_txsticky_par_stk_lbprm_MinVal                                                     0x0
#define cAf6_upen_txsticky_par_stk_lbprm_RstVal                                                     0x0

/*--------------------------------------
BitField Name: stk_prm
BitField Type: R/W
BitField Desc: dis error PRM config, (0) : disable, (1) : enable dis
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_prm_Bit_Start                                                      0
#define cAf6_upen_txsticky_par_stk_prm_Bit_End                                                        0
#define cAf6_upen_txsticky_par_stk_prm_Mask                                                       cBit0
#define cAf6_upen_txsticky_par_stk_prm_Shift                                                          0
#define cAf6_upen_txsticky_par_stk_prm_MaxVal                                                       0x1
#define cAf6_upen_txsticky_par_stk_prm_MinVal                                                       0x0
#define cAf6_upen_txsticky_par_stk_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT C/R & STANDARD Tx
Reg Addr   : 0x08000 - 0x094FF
Reg Formula: 0x08000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   :
Config PRM bit C/R & Standard

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfgcr_Base                                                                  0x08000
#define cAf6Reg_upen_prm_txcfgcr(STSID, VTGID, SLICEID, VTID)         (0x08000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID))
#define cAf6Reg_upen_prm_txcfgcr_WidthVal                                                                   32
#define cAf6Reg_upen_prm_txcfgcr_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_prmen_tx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Bit_Start                                                         3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Bit_End                                                           3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Mask                                                          cBit3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Shift                                                             3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_MaxVal                                                          0x1
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_MinVal                                                          0x0
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_prm_txenstuff
BitField Type: R/W
BitField Desc: config enable stuff message, (1) is disable, (0) is enbale
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Bit_Start                                                    2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Bit_End                                                      2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Mask                                                     cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_Shift                                                        2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_MinVal                                                     0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_txenstuff_RstVal                                                     0x0
/* Hardware change 2016-June-10 */
#define cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_Mask                                                     cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_Shift                                                        2

/*--------------------------------------
BitField Name: cfg_prmstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Bit_Start                                                        1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Bit_End                                                          1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Mask                                                         cBit1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Shift                                                            1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_MaxVal                                                         0x1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_MinVal                                                         0x0
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config bit command/respond PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Bit_Start                                                           0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Bit_End                                                             0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Mask                                                            cBit0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Shift                                                               0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_MinVal                                                            0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT L/B Tx
Reg Addr   : 0x0A000 - 0x0B4FF
Reg Formula: 0x0A000+$SLICEID*1024 + $STSID*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
CONFIG PRM BIT L/B Tx

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfglb_Base                                                                  0x0A000
#define cAf6Reg_upen_prm_txcfglb(STSID, VTGID, SLICEID, VTID)         (0x0A000+(SLICEID)*1024+(STSID)*32+(VTGID)*4+(VTID))
#define cAf6Reg_upen_prm_txcfglb_WidthVal                                                                   32
#define cAf6Reg_upen_prm_txcfglb_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_prm_enlb
BitField Type: R/W
BitField Desc: config enable CPU config LB bit, (0) is disable, (1) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Bit_Start                                                         1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Bit_End                                                           1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Mask                                                          cBit1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Shift                                                             1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_MaxVal                                                          0x1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_MinVal                                                          0x0
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_prm_lb
BitField Type: R/W
BitField Desc: config bit Loopback PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Bit_Start                                                           0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Bit_End                                                             0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Mask                                                            cBit0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Shift                                                               0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_MinVal                                                            0x0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX PRM
Reg Addr   : 0x10000 - 0x134FF
Reg Formula: 0x10000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_byte_Base                                                               0x10000
#define cAf6Reg_upen_txprm_cnt_byte(UPRO, STSID, VTGID, SLICEID, VTID)(0x10000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_txprm_cnt_byte_WidthVal                                                                32
#define cAf6Reg_upen_txprm_cnt_byte_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_byte_prm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Bit_Start                                                      0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Bit_End                                                       31
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Mask                                                    cBit31_0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Shift                                                          0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_MaxVal                                                0xffffffff
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_MinVal                                                       0x0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER PACKET MESSAGE TX PRM
Reg Addr   : 0x14000 - 0x174FF
Reg Formula: 0x14000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter packet message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_pkt_Base                                                                0x14000
#define cAf6Reg_upen_txprm_cnt_pkt(UPRO, STSID, VTGID, SLICEID, VTID) (0x14000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_txprm_cnt_pkt_WidthVal                                                                 32
#define cAf6Reg_upen_txprm_cnt_pkt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pkt_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Bit_Start                                                        0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Bit_End                                                         14
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Mask                                                      cBit14_0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Shift                                                            0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_MaxVal                                                      0x7fff
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_MinVal                                                         0x0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID INFO TX PRM
Reg Addr   : 0x18000 - 0x1B4FF
Reg Formula: 0x18000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter valid info PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_info_Base                                                               0x18000
#define cAf6Reg_upen_txprm_cnt_info(UPRO, STSID, VTGID, SLICEID, VTID)(0x18000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_txprm_cnt_info_WidthVal                                                                32
#define cAf6Reg_upen_txprm_cnt_info_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_vld_prm_info
BitField Type: R/W
BitField Desc: value counter valid info
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Bit_Start                                                  0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Bit_End                                                   14
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Mask                                                cBit14_0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Shift                                                      0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_MaxVal                                                0x7fff
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_MinVal                                                   0x0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message PRM STANDARD
Reg Addr   : 0x20000 - 0x214FF
Reg Formula: 0x20000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
config standard message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cfgstd_Base                                                                 0x20000
#define cAf6Reg_upen_rxprm_cfgstd(STSID, VTGID, SLICEID, VTID)        (0x20000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID))
#define cAf6Reg_upen_rxprm_cfgstd_WidthVal                                                                  32
#define cAf6Reg_upen_rxprm_cfgstd_WriteMask                                                                0x0

/*---------------------------------------
 * BitField Name: cfg_prm_rxen
 */
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_Mask                                                        cBit3
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_Shift                                                       3

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Bit_Start                                                          2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Bit_End                                                            2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Mask                                                           cBit2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Shift                                                              2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_MaxVal                                                           0x1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_MinVal                                                           0x0
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cfg_prm_rxenstuff
BitField Type: R/W
BitField Desc: enable destuff   (0) is disable, (1) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Bit_Start                                                   1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Bit_End                                                     1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Mask                                                    cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_Shift                                                       1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_MaxVal                                                    0x1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_MinVal                                                    0x0
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxenstuff_RstVal                                                    0x0

/* Hardware change 2016-June-10 */
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_Mask                                                    cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_Shift                                                       1


/*--------------------------------------
BitField Name: cfg_std_prm
BitField Type: R/W
BitField Desc: config standard   (0) is ANSI, (1) is AT&T
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Bit_Start                                                         0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Bit_End                                                           0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Mask                                                          cBit0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Shift                                                             0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_MaxVal                                                          0x1
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_MinVal                                                          0x0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL DESTUFF 0
Reg Addr   : 0x23200
Reg Formula:
    Where  :
Reg Desc   :
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_destuff_ctrl0_Base                                                                0x23200
#define cAf6Reg_upen_destuff_ctrl0                                                                     0x23200
#define cAf6Reg_upen_destuff_ctrl0_WidthVal                                                                 32
#define cAf6Reg_upen_destuff_ctrl0_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cfg_crmon
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Bit_Start                                                          5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Bit_End                                                            5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Mask                                                           cBit5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Shift                                                              5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_MaxVal                                                           0x1
#define cAf6_upen_destuff_ctrl0_cfg_crmon_MinVal                                                           0x0
#define cAf6_upen_destuff_ctrl0_cfg_crmon_RstVal                                                           0x0

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_reserve1_Bit_Start                                                           4
#define cAf6_upen_destuff_ctrl0_reserve1_Bit_End                                                             4
#define cAf6_upen_destuff_ctrl0_reserve1_Mask                                                            cBit4
#define cAf6_upen_destuff_ctrl0_reserve1_Shift                                                               4
#define cAf6_upen_destuff_ctrl0_reserve1_MaxVal                                                            0x1
#define cAf6_upen_destuff_ctrl0_reserve1_MinVal                                                            0x0
#define cAf6_upen_destuff_ctrl0_reserve1_RstVal                                                            0x0

/*--------------------------------------
BitField Name: fcsmon
BitField Type: R/W
BitField Desc: (0) : disable monitor, (1) : enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_fcsmon_Bit_Start                                                             3
#define cAf6_upen_destuff_ctrl0_fcsmon_Bit_End                                                               3
#define cAf6_upen_destuff_ctrl0_fcsmon_Mask                                                              cBit3
#define cAf6_upen_destuff_ctrl0_fcsmon_Shift                                                                 3
#define cAf6_upen_destuff_ctrl0_fcsmon_MaxVal                                                              0x1
#define cAf6_upen_destuff_ctrl0_fcsmon_MinVal                                                              0x0
#define cAf6_upen_destuff_ctrl0_fcsmon_RstVal                                                              0x1

/*--------------------------------------
BitField Name: headermon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_headermon_en_Bit_Start                                                       0
#define cAf6_upen_destuff_ctrl0_headermon_en_Bit_End                                                         0
#define cAf6_upen_destuff_ctrl0_headermon_en_Mask                                                        cBit0
#define cAf6_upen_destuff_ctrl0_headermon_en_Shift                                                           0
#define cAf6_upen_destuff_ctrl0_headermon_en_MaxVal                                                        0x1
#define cAf6_upen_destuff_ctrl0_headermon_en_MinVal                                                        0x0
#define cAf6_upen_destuff_ctrl0_headermon_en_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX PRM
Reg Addr   : 0x30000 - 0x334FF
Reg Formula: 0x30000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter good message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_gmess_Base                                                                  0x30000
#define cAf6Reg_upen_rxprm_gmess(UPRO, STSID, VTGID, SLICEID, VTID)   (0x30000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_rxprm_gmess_WidthVal                                                                   32
#define cAf6Reg_upen_rxprm_gmess_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_gmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Bit_Start                                                        0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Bit_End                                                         14
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Shift                                                            0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_MaxVal                                                      0x7fff
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_MinVal                                                         0x0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX PRM
Reg Addr   : 0x34000 - 0x374FF
Reg Formula: 0x34000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter drop message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_drmess_Base                                                                 0x34000
#define cAf6Reg_upen_rxprm_drmess(UPRO, STSID, VTGID, SLICEID, VTID)  (0x34000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_rxprm_drmess_WidthVal                                                                  32
#define cAf6Reg_upen_rxprm_drmess_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_drmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Bit_Start                                                      0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Bit_End                                                       14
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Mask                                                    cBit14_0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Shift                                                          0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_MaxVal                                                    0x7fff
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_MinVal                                                       0x0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER MISS MESSAGE RX PRM
Reg Addr   : 0x38000 - 0x3B4FF
Reg Formula: 0x38000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter miss message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_mmess_Base                                                                  0x38000
#define cAf6Reg_upen_rxprm_mmess(UPRO, STSID, VTGID, SLICEID, VTID)   (0x38000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_rxprm_mmess_WidthVal                                                                   32
#define cAf6Reg_upen_rxprm_mmess_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_mmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Bit_Start                                                        0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Bit_End                                                         14
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Shift                                                            0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_MaxVal                                                      0x7fff
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_MinVal                                                         0x0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX PRM
Reg Addr   : 0x3C000 - 0x3F4FF
Reg Formula: 0x3C000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cnt_byte_Base                                                               0x3C000
#define cAf6Reg_upen_rxprm_cnt_byte(UPRO, STSID, VTGID, SLICEID, VTID)(0x3C000+(STSID)[4:3]*7+(VTGID)*256+(SLICEID)*32+(STSID)[2:0]*4+(VTID)+(UPRO)*8192)
#define cAf6Reg_upen_rxprm_cnt_byte_WidthVal                                                                32
#define cAf6Reg_upen_rxprm_cnt_byte_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_byte_rxprm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Bit_Start                                                    0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Bit_End                                                     31
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Mask                                                  cBit31_0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Shift                                                        0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_MaxVal                                              0xffffffff
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_MinVal                                                     0x0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RX CONFIG CONTROL PARITY
Reg Addr   : 0x23204
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_par_Base                                                                      0x23204
#define cAf6Reg_upen_cfg_par                                                                           0x23204
#define cAf6Reg_upen_cfg_par_WidthVal                                                                       32
#define cAf6Reg_upen_cfg_par_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: icfgparerrdis_prm
BitField Type: R/W
BitField Desc: disable oparerr prm config, (1) : disable, (0) : enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cfg_par_icfgparerrdis_prm_Bit_Start                                                        4
#define cAf6_upen_cfg_par_icfgparerrdis_prm_Bit_End                                                          4
#define cAf6_upen_cfg_par_icfgparerrdis_prm_Mask                                                         cBit4
#define cAf6_upen_cfg_par_icfgparerrdis_prm_Shift                                                            4
#define cAf6_upen_cfg_par_icfgparerrdis_prm_MaxVal                                                         0x1
#define cAf6_upen_cfg_par_icfgparerrdis_prm_MinVal                                                         0x0
#define cAf6_upen_cfg_par_icfgparerrdis_prm_RstVal                                                         0x0

/*--------------------------------------
BitField Name: icfgparforceerr_prm
BitField Type: R/W
BitField Desc: force oparerr prm config, (0) : disable, (1) : enable force
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_par_icfgparforceerr_prm_Bit_Start                                                      3
#define cAf6_upen_cfg_par_icfgparforceerr_prm_Bit_End                                                        3
#define cAf6_upen_cfg_par_icfgparforceerr_prm_Mask                                                       cBit3
#define cAf6_upen_cfg_par_icfgparforceerr_prm_Shift                                                          3
#define cAf6_upen_cfg_par_icfgparforceerr_prm_MaxVal                                                       0x1
#define cAf6_upen_cfg_par_icfgparforceerr_prm_MinVal                                                       0x0
#define cAf6_upen_cfg_par_icfgparforceerr_prm_RstVal                                                       0x0

/*--------------------------------------
BitField Name: icfgparerrdis_mdl
BitField Type: R/W
BitField Desc: disable oparerr mdl config, (1) : disable, (0) : enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_Bit_Start                                                        2
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_Bit_End                                                          2
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_Mask                                                         cBit2
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_Shift                                                            2
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_MaxVal                                                         0x1
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_MinVal                                                         0x0
#define cAf6_upen_cfg_par_icfgparerrdis_mdl_RstVal                                                         0x0

/*--------------------------------------
BitField Name: icfgparforceerr_mdl
BitField Type: R/W
BitField Desc: force oparerr mdl config, (0) : disable, (1) : enable force
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_Bit_Start                                                      1
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_Bit_End                                                        1
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_Mask                                                       cBit1
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_Shift                                                          1
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_MaxVal                                                       0x1
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_MinVal                                                       0x0
#define cAf6_upen_cfg_par_icfgparforceerr_mdl_RstVal                                                       0x0

/*--------------------------------------
BitField Name: reserve_act
BitField Type: R/W
BitField Desc: reserve_act
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_par_reserve_act_Bit_Start                                                              0
#define cAf6_upen_cfg_par_reserve_act_Bit_End                                                                0
#define cAf6_upen_cfg_par_reserve_act_Mask                                                               cBit0
#define cAf6_upen_cfg_par_reserve_act_Shift                                                                  0
#define cAf6_upen_cfg_par_reserve_act_MaxVal                                                               0x1
#define cAf6_upen_cfg_par_reserve_act_MinVal                                                               0x0
#define cAf6_upen_cfg_par_reserve_act_RstVal                                                               0x1


/*------------------------------------------------------------------------------
Reg Name   : RX STICKY PARITY
Reg Addr   : 0x23205
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sticky_par_Base                                                                   0x23205
#define cAf6Reg_upen_sticky_par                                                                        0x23205
#define cAf6Reg_upen_sticky_par_WidthVal                                                                    32
#define cAf6Reg_upen_sticky_par_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: stk_mdl
BitField Type: R/W
BitField Desc: sticky for mdl config error parity
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_sticky_par_stk_mdl_Bit_Start                                                               1
#define cAf6_upen_sticky_par_stk_mdl_Bit_End                                                                 1
#define cAf6_upen_sticky_par_stk_mdl_Mask                                                                cBit1
#define cAf6_upen_sticky_par_stk_mdl_Shift                                                                   1
#define cAf6_upen_sticky_par_stk_mdl_MaxVal                                                                0x1
#define cAf6_upen_sticky_par_stk_mdl_MinVal                                                                0x0
#define cAf6_upen_sticky_par_stk_mdl_RstVal                                                                0x0

/*--------------------------------------
BitField Name: stk_prm
BitField Type: R/W
BitField Desc: sticky for prm config error parity
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sticky_par_stk_prm_Bit_Start                                                               0
#define cAf6_upen_sticky_par_stk_prm_Bit_End                                                                 0
#define cAf6_upen_sticky_par_stk_prm_Mask                                                                cBit0
#define cAf6_upen_sticky_par_stk_prm_Shift                                                                   0
#define cAf6_upen_sticky_par_stk_prm_MaxVal                                                                0x1
#define cAf6_upen_sticky_par_stk_prm_MinVal                                                                0x0
#define cAf6_upen_sticky_par_stk_prm_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB per Channel Interrupt Enable Control
Reg Addr   : 0x28000-0x283FF
Reg Formula: 0x28000+ $STSID*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_cfg_lben_int_Base                                                                  0x28000
#define cAf6Reg_prm_cfg_lben_int(STSID, VTGID, VTID)                     (0x28000+(STSID)*32+(VTGID)*4+(VTID))
#define cAf6Reg_prm_cfg_lben_int_WidthVal                                                                   32
#define cAf6Reg_prm_cfg_lben_int_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: prm_cfglben_int7
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_Bit_Start                                                     7
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_Bit_End                                                       7
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_Mask                                                      cBit7
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_Shift                                                         7
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int7_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int6
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_Bit_Start                                                     6
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_Bit_End                                                       6
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_Mask                                                      cBit6
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_Shift                                                         6
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int6_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int5
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_Bit_Start                                                     5
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_Bit_End                                                       5
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_Mask                                                      cBit5
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_Shift                                                         5
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int5_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int4
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_Bit_Start                                                     4
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_Bit_End                                                       4
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_Mask                                                      cBit4
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_Shift                                                         4
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int4_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int3
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_Bit_Start                                                     3
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_Bit_End                                                       3
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_Mask                                                      cBit3
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_Shift                                                         3
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int3_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int2
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_Bit_Start                                                     2
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_Bit_End                                                       2
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_Mask                                                      cBit2
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_Shift                                                         2
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int2_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int1
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Bit_Start                                                     1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Bit_End                                                       1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Mask                                                      cBit1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_Shift                                                         1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int1_RstVal                                                      0x0

/*--------------------------------------
BitField Name: prm_cfglben_int0
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Bit_Start                                                     0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Bit_End                                                       0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Mask                                                      cBit0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_Shift                                                         0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_MaxVal                                                      0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_MinVal                                                      0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int0_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt Status
Reg Addr   : 0x28400-0x287FF
Reg Formula: 0x28400+ $STSID*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int_sta_Base                                                                    0x28400
#define cAf6Reg_prm_lb_int_sta(STSID, VTGID, VTID)                       (0x28400+(STSID)*32+(VTGID)*4+(VTID))
#define cAf6Reg_prm_lb_int_sta_WidthVal                                                                     32
#define cAf6Reg_prm_lb_int_sta_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: prm_lbint_sta7
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_Bit_Start                                                         7
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_Bit_End                                                           7
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_Mask                                                          cBit7
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_Shift                                                             7
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta7_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta6
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_Bit_Start                                                         6
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_Bit_End                                                           6
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_Mask                                                          cBit6
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_Shift                                                             6
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta6_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta5
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_Bit_Start                                                         5
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_Bit_End                                                           5
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_Mask                                                          cBit5
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_Shift                                                             5
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta5_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta4
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_Bit_Start                                                         4
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_Bit_End                                                           4
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_Mask                                                          cBit4
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_Shift                                                             4
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta4_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta3
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_Bit_Start                                                         3
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_Bit_End                                                           3
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_Mask                                                          cBit3
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_Shift                                                             3
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta3_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta2
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_Bit_Start                                                         2
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_Bit_End                                                           2
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_Mask                                                          cBit2
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_Shift                                                             2
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta2_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta1
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Bit_Start                                                         1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Bit_End                                                           1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Mask                                                          cBit1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_Shift                                                             1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta1_RstVal                                                          0x0

/*--------------------------------------
BitField Name: prm_lbint_sta0
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Bit_Start                                                         0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Bit_End                                                           0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Mask                                                          cBit0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_Shift                                                             0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_MaxVal                                                          0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_MinVal                                                          0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta0_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Current Status
Reg Addr   : 0x28800-0x28BFF
Reg Formula: 0x28800+ $STSID*32 + $VTGID*4 + $VTID
    Where  :
           + $STSID (0-23) : STS ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   :
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int_crrsta_Base                                                                 0x28800
#define cAf6Reg_prm_lb_int_crrsta(STSID, VTGID, VTID)                    (0x28800+(STSID)*32+(VTGID)*4+(VTID))
#define cAf6Reg_prm_lb_int_crrsta_WidthVal                                                                  32
#define cAf6Reg_prm_lb_int_crrsta_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta7
BitField Type: RW
BitField Desc: Current status of event slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_Bit_Start                                                   7
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_Bit_End                                                     7
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_Mask                                                    cBit7
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_Shift                                                       7
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta7_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta6
BitField Type: RW
BitField Desc: Current status of event slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_Bit_Start                                                   6
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_Bit_End                                                     6
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_Mask                                                    cBit6
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_Shift                                                       6
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta6_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta5
BitField Type: RW
BitField Desc: Current status of event slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_Bit_Start                                                   5
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_Bit_End                                                     5
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_Mask                                                    cBit5
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_Shift                                                       5
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta5_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta4
BitField Type: RW
BitField Desc: Current status of event slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_Bit_Start                                                   4
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_Bit_End                                                     4
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_Mask                                                    cBit4
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_Shift                                                       4
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta4_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta3
BitField Type: RW
BitField Desc: Current status of event slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_Bit_Start                                                   3
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_Bit_End                                                     3
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_Mask                                                    cBit3
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_Shift                                                       3
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta3_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta2
BitField Type: RW
BitField Desc: Current status of event slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_Bit_Start                                                   2
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_Bit_End                                                     2
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_Mask                                                    cBit2
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_Shift                                                       2
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta2_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta1
BitField Type: RW
BitField Desc: Current status of event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Bit_Start                                                   1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Bit_End                                                     1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Mask                                                    cBit1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_Shift                                                       1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta1_RstVal                                                    0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta0
BitField Type: RW
BitField Desc: Current status of event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Bit_Start                                                   0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Bit_End                                                     0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Mask                                                    cBit0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_Shift                                                       0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_MaxVal                                                    0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_MinVal                                                    0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta0_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt OR Status
Reg Addr   : 0x28C00-0x28C20
Reg Formula: 0x28C00 +  $GID
    Where  : 
           + $GID (0-31) : group ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_intsta_Base                                                                     0x28C00
#define cAf6Reg_prm_lb_intsta(GID)                                                             (0x28C00+(GID))
#define cAf6Reg_prm_lb_intsta_WidthVal                                                                      32
#define cAf6Reg_prm_lb_intsta_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: prm_lbintsta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_intsta_prm_lbintsta_Bit_Start                                                            0
#define cAf6_prm_lb_intsta_prm_lbintsta_Bit_End                                                             31
#define cAf6_prm_lb_intsta_prm_lbintsta_Mask                                                          cBit31_0
#define cAf6_prm_lb_intsta_prm_lbintsta_Shift                                                                0
#define cAf6_prm_lb_intsta_prm_lbintsta_MaxVal                                                      0xffffffff
#define cAf6_prm_lb_intsta_prm_lbintsta_MinVal                                                             0x0
#define cAf6_prm_lb_intsta_prm_lbintsta_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt OR Status
Reg Addr   : 0x28FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_sta_int_Base                                                                    0x28FFF
#define cAf6Reg_prm_lb_sta_int                                                                         0x28FFF
#define cAf6Reg_prm_lb_sta_int_WidthVal                                                                     32
#define cAf6Reg_prm_lb_sta_int_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: prm_lbsta_int
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Bit_Start                                                          0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Bit_End                                                           31
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Mask                                                        cBit31_0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Shift                                                              0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_MaxVal                                                    0xffffffff
#define cAf6_prm_lb_sta_int_prm_lbsta_int_MinVal                                                           0x0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt Enable Control
Reg Addr   : 0x28FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_en_int_Base                                                                     0x28FFE
#define cAf6Reg_prm_lb_en_int                                                                          0x28FFE
#define cAf6Reg_prm_lb_en_int_WidthVal                                                                      32
#define cAf6Reg_prm_lb_en_int_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: prm_lben_int
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_en_int_prm_lben_int_Bit_Start                                                            0
#define cAf6_prm_lb_en_int_prm_lben_int_Bit_End                                                             31
#define cAf6_prm_lb_en_int_prm_lben_int_Mask                                                          cBit31_0
#define cAf6_prm_lb_en_int_prm_lben_int_Shift                                                                0
#define cAf6_prm_lb_en_int_prm_lben_int_MaxVal                                                      0xffffffff
#define cAf6_prm_lb_en_int_prm_lben_int_MinVal                                                             0x0
#define cAf6_prm_lb_en_int_prm_lben_int_RstVal                                                             0x0

#endif /* _AF6_REG_AF6CCI0011_RD_PDH_PRM_H_ */

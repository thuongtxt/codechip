/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210011PdhDe3.c
 *
 * Created Date: Sep 20, 2016
 *
 * Description : DE3 of 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011PdhDe3
    {
    tTha60210031PdhDe3 super;
    }tTha60210011PdhDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhDe3Methods        m_ThaPdhDe3Override;
static tAtPdhChannelMethods     m_AtPdhChannelOverride;

/* Super implement */
static const tAtPdhChannelMethods   *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, HasLineLayer);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(de3), sizeof(m_ThaPdhDe3Override));

        mMethodOverride(m_ThaPdhDe3Override, IsManagedByModulePdh);
        mMethodOverride(m_ThaPdhDe3Override, ShouldConsiderLofWhenLos);
        }

    mMethodsSet(de3, &m_ThaPdhDe3Override);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtPdhChannel(self);
    OverrideThaPdhDe3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhDe3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210031PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60210011PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }

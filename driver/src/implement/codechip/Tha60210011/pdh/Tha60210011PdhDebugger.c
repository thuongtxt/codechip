/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210011PdhDebugger.c
 *
 * Created Date: Jun 9, 2017
 *
 * Description : PDH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "Tha60210011ModulePdhMdlPrmReg.h"
#include "Tha60210011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

#define cThaPrmPayloadBitLBHwMask    cBit0
#define cThaPrmPayloadBitLBHwShift   0
#define cThaPrmPayloadBitSLHwMask    cBit1
#define cThaPrmPayloadBitSLHwShift   1
#define cThaPrmPayloadBitLVHwMask    cBit2
#define cThaPrmPayloadBitLVHwShift   2
#define cThaPrmPayloadBitFEHwMask    cBit3
#define cThaPrmPayloadBitFEHwShift   3
#define cThaPrmPayloadBitSEHwMask    cBit4
#define cThaPrmPayloadBitSEHwShift   4
#define cThaPrmPayloadBitGHwMask     cBit7_5
#define cThaPrmPayloadBitGHwShift    5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDebuggerMethods m_ThaPdhDebuggerOverride;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DebugRegister(ThaModulePdh pdhModule)
    {
    return cAf6Reg_upen_cfg_debug_Base + ThaModulePdhPrmBaseAddress(pdhModule);
    }

static eAtModulePdhRet PrmMonitorEnable(ThaPdhDebugger self, eBool enable)
    {
	AtUnused(self);
	AtUnused(enable);
    return cAtOk;
    }

static eBool PrmMonitorIsEnabled(ThaPdhDebugger self)
    {
	AtUnused(self);
	return cAtFalse;
    }

static eAtModulePdhRet PrmForce8BitPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = DebugRegister(pdhModule);
    uint32 values = mModuleHwRead(pdhModule, regAddr);
    mRegFieldSet(values, cAf6_upen_cfg_debug_out_cfg_selinfo_, mBoolToBin(enable));
    mModuleHwWrite(pdhModule, regAddr, values);

    return cAtOk;
    }

static eBool PrmIsForced8BitPrmPayload(ThaPdhDebugger self)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regVal = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    return mRegField(regVal, cAf6_upen_cfg_debug_out_cfg_selinfo_) ? cAtTrue : cAtFalse;
    }

static eAtModulePdhRet PrmForcePayLoad(ThaPdhDebugger self, uint32 payloadMap, eBool forced)
    {
    uint8 hwValue = (uint8)mBoolToBin(forced);
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = DebugRegister(pdhModule);
    uint32 regVal = mModuleHwRead(pdhModule, regAddr);

    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG1)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x1 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG2)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x2 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG3)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x3 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG4)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x4 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG5)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x5 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG6)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x6 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitSE)
        mRegFieldSet(regVal, cThaPrmPayloadBitSEHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitFE)
        mRegFieldSet(regVal, cThaPrmPayloadBitFEHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitLV)
        mRegFieldSet(regVal, cThaPrmPayloadBitLVHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitSL)
        mRegFieldSet(regVal, cThaPrmPayloadBitSLHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitLB)
        mRegFieldSet(regVal, cThaPrmPayloadBitLBHw, hwValue);

    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

static uint32 PrmIsForcedPayLoad(ThaPdhDebugger self, uint32 *payLoadValue)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 values, hwPayLoadValue, numberBit;

    numberBit = hwPayLoadValue = 0;
    values = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x1, cThaPdhDebuggerPrmPayloadBitG1)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x2, cThaPdhDebuggerPrmPayloadBitG2)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x3, cThaPdhDebuggerPrmPayloadBitG3)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x4, cThaPdhDebuggerPrmPayloadBitG4)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x5, cThaPdhDebuggerPrmPayloadBitG5)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x6, cThaPdhDebuggerPrmPayloadBitG6)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitSEHw, 0x1, cThaPdhDebuggerPrmPayloadBitSE)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitFEHw, 0x1, cThaPdhDebuggerPrmPayloadBitFE)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitLVHw, 0x1, cThaPdhDebuggerPrmPayloadBitLV)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitSLHw, 0x1, cThaPdhDebuggerPrmPayloadBitSL)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitLBHw, 0x1, cThaPdhDebuggerPrmPayloadBitLB)

    *payLoadValue = hwPayLoadValue;
    return numberBit;
    }

static eAtModulePdhRet PrmForceNmNi(ThaPdhDebugger self, uint32 payloadMap, uint8 swValue)
    {
    AtUnused(self);
    AtUnused(payloadMap);
    AtUnused(swValue);
    return cAtErrorModeNotSupport;
    }

static uint8 PrmUnForceNmNi(ThaPdhDebugger self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet PrmForcedFullPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eAtModulePdhRet PrmChannelSelect(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cAtErrorModeNotSupport;
    }

static void OverrideThaPdhDebugger(ThaPdhDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDebuggerOverride, mMethodsGet(self), sizeof(m_ThaPdhDebuggerOverride));

        mMethodOverride(m_ThaPdhDebuggerOverride, PrmMonitorEnable);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmMonitorIsEnabled);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForce8BitPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmIsForced8BitPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForcePayLoad);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmIsForcedPayLoad);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForceNmNi);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmUnForceNmNi);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForcedFullPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmChannelSelect);
        }

    mMethodsSet(self, &m_ThaPdhDebuggerOverride);
    }

static void Override(ThaPdhDebugger self)
    {
    OverrideThaPdhDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhDebugger);
    }

ThaPdhDebugger Tha60210011PdhDebuggerObjectInit(ThaPdhDebugger self, ThaModulePdh pdhModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDebuggerObjectInit(self, pdhModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPdhDebugger Tha60210011PdhDebuggerNew(ThaModulePdh pdhModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPdhDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PdhDebuggerObjectInit(newChecker, pdhModule);
    }

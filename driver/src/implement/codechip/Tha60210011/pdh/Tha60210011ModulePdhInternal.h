/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210011ModulePdhInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPDHINTERNAL_H_
#define _THA60210011MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/debugger/ThaPdhDebuggerInternal.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "Tha60210011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePdhMethods
    {
    const char** (*RxPrmMdlRamsDescription)(Tha60210011ModulePdh self, uint32 *numRams);
    const char** (*TxPrmMdlRamsDescription)(Tha60210011ModulePdh self, uint32 *numRams);
    eAtRet (*PrmDefaultSet)(Tha60210011ModulePdh self);
    eBool (*HasDataLinkRams)(Tha60210011ModulePdh self);
    }tTha60210011ModulePdhMethods;

typedef struct tTha60210011ModulePdh
    {
    tTha60210031ModulePdh super;
    const tTha60210011ModulePdhMethods* methods;
    }tTha60210011ModulePdh;

typedef struct tTha60210011PdhDebugger
    {
    tThaPdhDebugger super;
    }tTha60210011PdhDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60210011ModulePdhObjectInit(AtModulePdh self, AtDevice device);
ThaPdhDebugger Tha60210011PdhDebuggerObjectInit(ThaPdhDebugger self, ThaModulePdh pdhModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPDHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210011PdhPrmInterruptManagerInternal.h
 * 
 * Created Date: Sep 20, 2017
 *
 * Description : PDH PRM interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011PDHPRMINTERRUPTMANAGERINTERNAL_H_
#define _THA60210011PDHPRMINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pdh/prm/ThaPdhPrmInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PdhPrmInterruptManager
    {
    tThaPdhPrmInterruptManager super;
    }tTha60210011PdhPrmInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager Tha60210011PdhPrmInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210011PDHPRMINTERRUPTMANAGERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210011PdhPrmInterruptManager.c
 *
 * Created Date: Sep 19, 2017
 *
 * Description : PDH PRM interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../man/Tha60210011DeviceReg.h"
#include "../Tha60210011ModulePdhMdlPrmReg.h"
#include "Tha60210011PdhPrmInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtInterruptManager self)
    {
    return (ThaModulePdh)AtInterruptManagerModuleGet(self);
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaInterruptManager manager = (ThaInterruptManager)self;
    ThaModulePdh pdhModule = PdhModule(self);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(manager);
    uint32 numSlices = ThaInterruptManagerNumSlices(manager);
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(manager);
    uint32 intrStatus = AtHalRead(hal, (baseAddress + cAf6Reg_prm_lb_sta_int_Base));
    uint32 intrMask = AtHalRead(hal, (baseAddress + cAf6Reg_prm_lb_en_int_Base));
    uint8 de3;
    AtUnused(glbIntr);
    intrStatus &= intrMask;

    for (de3 = 0; de3 < numStsInSlice; de3++)
        {
        if (intrStatus & cIteratorMask(de3))
            {
            /* Get DE3 channel OR interrupt. */
            uint32 intrDe1Status = AtHalRead(hal, (baseAddress + (uint32)cAf6Reg_prm_lb_intsta_Base + de3));
            uint8 de28 = 0;
            uint8 de2, de1;

            for (de2 = 0; de2 < 7; de2++)
                {
                for (de1 = 0; de1 < 4; de1++)
                    {
                    /* Check for DS1/E1 channel OR interrupt. */
                    if (intrDe1Status & cIteratorMask(de28))
                        {
                        uint8 slice24;
                        for (slice24 = 0; slice24 < numSlices; slice24++)
                            {
                            AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(pdhModule, cAtModulePdh, slice24, de3, de2, de1);
                            AtPdhPrmController controller = AtPdhDe1PrmControllerGet(de1Channel);
                            AtPdhPrmControllerInterruptProcess(controller, slice24, de3, de2, de1, hal);
                            }
                        }
                    de28++;
                    }
                }
            }
        }
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x0380000UL;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_mask_enable_PrmIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal)
        {
        uint32 baseAddress = ThaInterruptManagerBaseAddress(self);
        AtHalWrite(hal, baseAddress + cAf6Reg_prm_lb_en_int_Base, (enable) ? cAf6_prm_lb_en_int_prm_lben_int_Mask : 0x0);
        }
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_lb_int_sta_Base;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_cfg_lben_int_Base;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_lb_int_crrsta_Base;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhPrmInterruptManager);
    }

AtInterruptManager Tha60210011PdhPrmInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPrmInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210011PdhPrmInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PdhPrmInterruptManagerObjectInit(newManager, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210011PdhPrmController.c
 *
 * Created Date: Nov 21, 2015
 *
 * Description : PRM controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../../Tha60210031/pdh/Tha60210031PdhPrmController.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "Tha60210011ModulePdhMdlPrmReg.h"
#include "Tha60210011PdhPrmControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

#define mUpenPrmTxcfgcrAddress(self) UpenPrmTxcfgcrAddress((AtPdhPrmController)self)
#define mUpenPrmTxcfglbAddress(self) UpenPrmTxcfglbAddress((AtPdhPrmController)self)
#define mUpenRxprmCfgstdAddress(self) UpenRxprmCfgstdAddress((AtPdhPrmController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPdhPrmControllerMethods  m_AtPdhPrmControllerOverride;
static tThaPdhPrmControllerMethods m_ThaPdhPrmControllerOverride;

/* Save super implementation */
static const tAtPdhPrmControllerMethods *m_AtPdhPrmControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1Get(AtPdhPrmController self)
    {
    return (ThaPdhDe1)AtPdhPrmControllerDe1Get(self);
    }

static uint32 PrmOffset(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    ThaModulePdh modulePdh = (ThaModulePdh) AtChannelModuleGet((AtChannel) de1);

    return ThaModulePdhDe1PrmOffset(modulePdh, de1);
    }

static AtModulePdh PdhModule(AtPdhPrmController self)
    {
    return (AtModulePdh)AtChannelModuleGet((AtChannel)AtPdhPrmControllerDe1Get(self));
    }

static uint32 UpenPrmTxcfgcrAddress(AtPdhPrmController self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule(self);
    return Tha60210031ModulePdhUpenPrmTxcfgcrBase(pdhModule) + PrmOffset(self);
    }

static uint32 UpenRxprmCfgstdAddress(AtPdhPrmController self)
    {
    return Tha60210031ModulePdhUpenRxprmCfgstdBase((Tha60210031ModulePdh)PdhModule(self)) + PrmOffset(self);
    }

static uint32 UpenPrmTxcfglbAddress(AtPdhPrmController self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule(self);
    return Tha60210031ModulePdhUpenPrmTxcfglbBase(pdhModule) + Tha602PrmControllerLBOffset((AtPdhPrmController)self);
    }

static eBool IsEnabled(AtPdhPrmController self)
    {
    uint32 regAddress = UpenPrmTxcfgcrAddress(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return mRegField(regValue, cAf6_upen_prm_txcfgcr_cfg_prmen_tx_) ? cAtTrue : cAtFalse;
    }

static eAtModulePdhRet HwTxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = mUpenPrmTxcfgcrAddress(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_, standard);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtPdhDe1PrmStandard TxStandardGet(AtPdhPrmController self)
    {
    uint32 regAddr = mUpenPrmTxcfgcrAddress(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);
    return mRegField(regVal, cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_) ? cAtPdhDe1PrmStandardAtt : cAtPdhDe1PrmStandardAnsi;
    }

static eAtModulePdhRet HwRxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = mUpenRxprmCfgstdAddress(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_std_prm_, standard);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtPdhDe1PrmStandard RxStandardGet(AtPdhPrmController self)
    {
    uint32 regAddr = mUpenRxprmCfgstdAddress(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);
    return mRegField(regVal, cAf6_upen_rxprm_cfgstd_cfg_std_prm_) ? cAtPdhDe1PrmStandardAtt : cAtPdhDe1PrmStandardAnsi;
    }
    
static eAtModulePdhRet RxHwEnable(ThaPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr = mUpenRxprmCfgstdAddress(self);
    uint32 regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_, (enable ? 1 : 0));
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }
    
static uint32 TxMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_pkt_Base;
    }

static uint32 TxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_byte_Base;
    }

static uint32 RxGoodMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_gmess_Base;
    }

static uint32 RxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cnt_byte_Base;
    }

static uint32 RxDiscardRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_drmess_Base;
    }

static uint32 RxMissRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_mmess_Base;
    }

static uint8 TxCRBitGet(AtPdhPrmController self)
    {
    uint32 regAddress = mUpenPrmTxcfgcrAddress(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return (uint8)mRegField(regValue, cAf6_upen_prm_txcfgcr_cfg_prm_cr_);
    }

static eAtModulePdhRet HwTxCRBitSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = mUpenPrmTxcfgcrAddress(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_cr_, value);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet TxHwEnable(ThaPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = mUpenPrmTxcfgcrAddress(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfgcr_cfg_prmen_tx_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet HwEnable(ThaPdhPrmController self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= TxHwEnable(self, enable);
    ret |= RxHwEnable(self, enable);

    return ret;
    }

static eAtModulePdhRet HwTxLBBitSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = mUpenPrmTxcfglbAddress(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfglb_cfg_prm_lb_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet TxLBBitEnable(AtPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress, regValue;

    regAddress = mUpenPrmTxcfglbAddress(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfglb_cfg_prm_enlb_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eBool TxLBBitIsEnabled(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress, regValue;

    regAddress = mUpenPrmTxcfglbAddress(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    return mRegField(regValue, cAf6_upen_prm_txcfglb_cfg_prm_enlb_) ? cAtTrue : cAtFalse;
    }

static uint8 TxLBBitGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 regAddress = mUpenPrmTxcfglbAddress(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return (uint8)mRegField(regValue, cAf6_upen_prm_txcfglb_cfg_prm_lb_);
    }

static uint32 CounterOffset(ThaPdhPrmController self, eBool read2Clear)
    {
    return (PrmOffset((AtPdhPrmController)self) + mMethodsGet(self)->Read2ClearOffset(self, read2Clear));
    }

static eAtModulePdhRet HwCRBitExpectSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = mUpenRxprmCfgstdAddress(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_cr_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static uint8 CRBitExpectGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress = mUpenRxprmCfgstdAddress(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return (uint8)mRegField(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_cr_);
    }

static eAtModulePdhRet HwFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddr, regVal;

    /* Check if invalid input */
    if ((order != cAtBitOrderMsb) && (order != cAtBitOrderLsb))
        return cAtErrorInvlParm;

    regAddr = mUpenPrmTxcfgcrAddress(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_, (order == cAtBitOrderLsb) ? 0 : 1);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    regAddr = UpenRxprmCfgstdAddress(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_, (order == cAtBitOrderLsb) ? 0 : 1);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtBitOrder HwFcsBitOrderGet(AtPdhPrmController self)
    {
    uint32 regAddr = mUpenPrmTxcfgcrAddress(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);

    return mRegField(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_) ? cAtBitOrderMsb : cAtBitOrderLsb;
    }

static uint32 Read2ClearOffset(ThaPdhPrmController self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? 0 : 8192;
    }

static uint32 InterruptOffset(ThaPdhPrmController self, uint8 *sliceId)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    return Tha602PrmControllerInterruptOffset(de1, sliceId);
    }

static void OverrideAtPdhPrmController(AtPdhPrmController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhPrmControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhPrmControllerOverride, m_AtPdhPrmControllerMethods, sizeof(m_AtPdhPrmControllerOverride));

        mMethodOverride(m_AtPdhPrmControllerOverride, IsEnabled);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxStandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, RxStandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxCRBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitIsEnabled);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitEnable);
        mMethodOverride(m_AtPdhPrmControllerOverride, CRBitExpectGet);
        }

    mMethodsSet(self, &m_AtPdhPrmControllerOverride);
    }

static void OverrideThaPdhPrmController(AtPdhPrmController self)
    {
    ThaPdhPrmController controller = (ThaPdhPrmController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmControllerOverride, mMethodsGet(controller), sizeof(m_ThaPdhPrmControllerOverride));

        mMethodOverride(m_ThaPdhPrmControllerOverride, TxMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, TxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxGoodMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxDiscardRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxMissRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwEnable);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxStandardSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwRxStandardSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxCRBitSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxLBBitSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, CounterOffset);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwCRBitExpectSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwFcsBitOrderSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwFcsBitOrderGet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, Read2ClearOffset);
        mMethodOverride(m_ThaPdhPrmControllerOverride, InterruptOffset);
        }

    mMethodsSet(controller, &m_ThaPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideAtPdhPrmController(self);
    OverrideThaPdhPrmController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhPrmController);
    }

AtPdhPrmController Tha60210011PdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController Tha60210011PdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PdhPrmControllerObjectInit(newController, de1);
    }

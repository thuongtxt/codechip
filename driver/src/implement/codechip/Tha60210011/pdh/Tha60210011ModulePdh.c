/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210011ModulePdh.c
 *
 * Created Date: May 6, 2015
 *
 * Description : PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhTug.h"
#include "../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/pdh/ThaPdhMdlController.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhPrmController.h"
#include "../../../default/pdh/prm/ThaPdhPrmInterruptManager.h"
#include "../../../default/pdh/mdl/ThaPdhMdlInterruptManager.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdhReg.h"
#include "../../Tha60210031/pdh/Tha60210031PdhMdlController.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../man/Tha60210011Device.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../xc/Tha60210011ModuleXc.h"
#include "../man/Tha60210011DeviceReg.h"
#include "../ram/Tha60210011InternalRam.h"
#include "Tha60210011ModulePdhInternal.h"
#include "Tha60210011ModulePdhMdlPrmReg.h"
#include "Tha60210011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAf6_pdh_slice_intr_Mask(sl)         (1U << (sl))
#define cAf6_pdh_de3_intr_Mask(_de3)         (1U << (_de3))
#define cAf6_pdh_de1_intr_Mask(_de1)         (1U << (_de1))
#define mThis(self) ((Tha60210011ModulePdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModulePdhMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaStmModulePdhMethods      m_ThaStmModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;

/* Save super implementation */
static const tAtModuleMethods        *m_AtModuleMethods     = NULL;
static const tAtModulePdhMethods     *m_AtModulePdhMethods     = NULL;
static const tThaModulePdhMethods    *m_ThaModulePdhMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsPdhRegister(AtModuleDeviceGet(self), address);
    }

static int32 SliceOffset(ThaModulePdh self, uint32 sliceId)
    {
    AtUnused(self);
    return (int32)((0x1000000 + (sliceId * 0x100000)) - 0x700000);
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 12;
    }

static uint32 NumberOfDe3SerialLinesGet(AtModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumberOfDe3sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxDestuffControl0(ThaModulePdh self)
    {
    Tha60210031ModulePdh module = (Tha60210031ModulePdh)self;
    return mMethodsGet(module)->UpenDestuffCtrl0Base(module) + ThaModulePdhPrmBaseAddress(self);
    }

static eAtRet PrmDefaultSet(Tha60210011ModulePdh self)
    {
    uint32 regAddr, regVal;
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!ThaModulePdhPrmIsSupported(pdhModule))
        return cAtOk;

    regAddr = mMethodsGet((Tha60210031ModulePdh)self)->UpenCfgCtrl0Base((Tha60210031ModulePdh)self)  + ThaModulePdhPrmBaseAddress(pdhModule);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfg_ctrl0_fcsinscfg_, 1);    /* Enable FCS monitor */
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = RxDestuffControl0(pdhModule);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_destuff_ctrl0_fcsmon_, 1);    /* Enable FCS insert */
    mRegFieldSet(regVal, cAf6_upen_destuff_ctrl0_cfg_crmon_, 0); /* Disable CR monitort */
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePdh self)
    {
    uint8 numSlices = mMethodsGet(self)->NumSlices(self);
    uint8 slice_i;
    static const uint32 cDefaultPdhGlobalControlVal = 0x200;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        int32 sliceOffset = mMethodsGet(self)->SliceOffset(self, slice_i);
        mModuleHwWrite(self, (uint32)(cThaRegPdhGlbCtrl + sliceOffset), cDefaultPdhGlobalControlVal);
        }

    return mMethodsGet(mThis(self))->PrmDefaultSet(mThis(self));
    }

static ThaModuleSdh ModuleSdh(ThaModulePdh self)
    {
    return (ThaModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    }

static eBool IsTu1x(AtSdhChannel tu)
    {
    eAtSdhChannelType type = AtSdhChannelTypeGet(tu);

    if ((type == cAtSdhChannelTypeTu11) || (type == cAtSdhChannelTypeTu12))
        return cAtTrue;

    return cAtFalse;
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportMdl(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x02, 0x36);
    }

static uint32 StartBuiltNumberSupportMdl(AtDevice device)
    {
    AtUnused(device);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x0, 0x0, 0x26);
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) > StartVersionSupportMdl(device))
       return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartVersionSupportMdl(device))
       return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportMdl(device)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static AtPdhMdlController MdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType)
    {
    AtUnused(self);
    return Tha60210031PdhMdlControllerNew(de3, mdlType);
    }

static uint32 MdlRxBufBaseAddress(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 0x24000UL;
    }

static uint32 MdlRxBufWordOffset(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 256UL;
    }

static uint32 MdlRxStickyBaseAddress(Tha60210031ModulePdh self, uint32 bufferId)
    {
    AtUnused(self);
    AtUnused(bufferId);
    return 0x23210UL;
    }

static uint32 MdlRxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0x27100 : 0x27900;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0x27000 : 0x27800;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0x27200 : 0x27A00;
        default:                                 return 0;
        }
    }

static uint32 MdlRxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0x26100 : 0x26500;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0x26000 : 0x26400;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0x26200 : 0x26600;
        default:                                 return 0;
        }
    }

static uint32 MdlRxDropOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0x27400 : 0x27C00;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0x27300 : 0x27B00;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0x27500 : 0x27D00;
        default:                                 return 0;
        }
    }

static AtPdhDe3 De3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id)
    {
    uint8 loSlice, loHwStsInSlice;
    uint8 tfi5StsInLine;
    ThaModuleSdh sdhModule = (ThaModuleSdh)ModuleSdh(self);
    AtSdhLine sdhLine;
    AtUnused(phyModule);

    /* Convert PDH interrupt ID to Lo-slice ID */
    ThaModuleSdhHwSts24ToHwSts48Get(sdhModule, sliceId, de3Id, &loSlice, &loHwStsInSlice);

    sdhLine = Tha60210011ModuleSdhLineFromLoLineStsGet((AtModuleSdh)sdhModule, loSlice, loHwStsInSlice, &tfi5StsInLine);
    return Tha60210011PdhDe3FromSdhLineGet(sdhLine, tfi5StsInLine);
    }

AtPdhDe3 Tha60210011PdhDe3FromSdhLineGet(AtSdhLine sdhLine, uint8 stsInLine)
    {
    AtSdhChannel au, vc3 = NULL;

    if (sdhLine == NULL)
        return NULL;

    au = Tha60210011ModuleSdhAuFromHwStsGet(sdhLine, stsInLine);
    if (au == NULL)
        return NULL;

    if (AtSdhChannelTypeGet(au) == cAtSdhChannelTypeAu3)
        vc3 = AtSdhChannelSubChannelGet(au, 0);

    else if (AtSdhChannelTypeGet(au) == cAtSdhChannelTypeAu4)
        {
        AtSdhChannel vc4, tug3;

        vc4 = AtSdhChannelSubChannelGet(au, 0);
        if (AtSdhChannelMapTypeGet(vc4) != cAtSdhVcMapTypeVc4Map3xTug3s)
            return NULL;

        tug3 = AtSdhChannelSubChannelGet(vc4, stsInLine / 16);
        if (AtSdhChannelMapTypeGet(tug3) != cAtSdhTugMapTypeTug3MapVc3)
            return NULL;

        vc3 = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug3, 0), 0);
        }

    if (AtSdhChannelMapTypeGet(vc3) == cAtSdhVcMapTypeVc3MapDe3)
        return (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);

    return NULL;
    }

static AtPdhDe1 De1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id)
    {
    AtSdhChannel tu = (AtSdhChannel)ThaModuleSdhTuPathFromHwIdGet(ModuleSdh(self), phyModule, sliceId, de3Id, de2Id, de1Id);

    if (IsTu1x(tu))
        {
        AtSdhChannel vc1x = AtSdhChannelSubChannelGet(tu, 0);
        if (AtSdhChannelMapTypeGet(vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
            return (AtPdhDe1)AtSdhChannelMapChannelGet(vc1x);
        else
            return NULL;
        }
    else
        {
        AtPdhDe3 m13 = ThaModulePdhDe3ChannelFromHwIdGet(self, phyModule, sliceId, de3Id);
        if (m13)
            {
            ThaPdhDe3De1Hw2SwIdGet((ThaPdhDe3)m13, de2Id, de1Id, &de2Id, &de1Id);
            return AtPdhDe3De1Get(m13, de2Id, de1Id);
            }
        else
            return NULL;
        }
    }

static eBool HasDe3Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_global_interrupt_status_DE3IntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static eBool HasDe1Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_global_interrupt_status_DE1IntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptBaseAddress(ThaModulePdh self)
    {
    return Tha60210011DeviceInterruptBaseAddress((Tha60210011Device)AtModuleDeviceGet((AtModule)self));
    }

static uint32 De3InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 intrStatus = AtHalRead(hal, InterruptBaseAddress(self) + cAf6Reg_global_pdh_interrupt_status);
    AtUnused(self);
    AtUnused(glbIntr);
    return (intrStatus >> 12) & cBit11_0;
    }

static uint32 De1InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 intrStatus = AtHalRead(hal, InterruptBaseAddress(self) + cAf6Reg_global_pdh_interrupt_status);
    AtUnused(self);
    AtUnused(glbIntr);
    return intrStatus & cBit11_0;
    }

static uint32 BaseAddress(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x1000000U;
    }

static eAtRet Ds1E1FrmRxMuxSet(ThaStmModulePdh self, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    return cAtOk;
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return Tha60210011PdhDe3New(hwSts, self);
    }

static eBool HasLiu(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0xFF, 0xFF, 0xFF, 0xFF);
    }

static eBool De3TxLiuEnableIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool DetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool DetectDe3LosByAllZeroPattern(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x1, 0x34);
    }

static uint32 StartVersionSupportDe3Ssm(ThaModulePdh self)
    {
    /* Not now */
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    /* Not now */
    AtUnused(self);
    return cBit31_0;
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60210011PdhPrmControllerNew(de1);
    }

static eAtModulePdhRet PrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    uint32 regAddress, regValue;

    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)self))
        return cAtErrorModeNotSupport;

    regAddress = RxDestuffControl0((ThaModulePdh)self);
    regValue = mModuleHwRead(self, regAddress);
    mRegFieldSet(regValue, cAf6_upen_destuff_ctrl0_cfg_crmon_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddress, regValue);

    return cAtOk;
    }

static eBool PrmCrCompareIsEnabled(AtModulePdh self)
    {
    uint32 regAddress, regValue;
    
	if (!ThaModulePdhPrmIsSupported((ThaModulePdh)self))
    	return cAtFalse;
    
    regAddress = RxDestuffControl0((ThaModulePdh)self);
    regValue = mModuleHwRead(self, regAddress);	    
    return mRegField(regValue, cAf6_upen_destuff_ctrl0_cfg_crmon_) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportForcedAisCorrection(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x02, 0x35);
    }

static uint32 StartBuiltNumberSupportForcedAisCorrection(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x17);
    }

static eBool LiuForcedAisIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceVersionNumber(device) > StartVersionSupportForcedAisCorrection(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartVersionSupportForcedAisCorrection(self))
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportForcedAisCorrection(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool LiuDe3OverVc3IsSupported(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x16, 0x02, 0x18, 0x54);
    }

static uint32 NumPdhRams(ThaModulePdh self)
    {
    uint32 numSlices = mMethodsGet(self)->NumSlices(self);
    return Tha60210011ModulePdhNumPdhRamsInSlice(mThis(self)) * numSlices; /* 13x12 */
    }

static const char** TxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32* numRams)
    {
    static const char *txRamsDescription[] =
        {
        "DLK TX PRM configuration",
        "DLK TX PRM LB configuration",
        "DLK TX MDL configuration, slice 0",
        "DLK TX MDL buffer 1, slice 0",
        "DLK TX MDL buffer 2, slice 0",
        "DLK TX MDL configuration, slice 1",
        "DLK TX MDL buffer 1, slice 1",
        "DLK TX MDL buffer 2, slice 1",
        "DLK TX MDL configuration, slice 2",
        "DLK TX MDL buffer 1, slice 2",
        "DLK TX MDL buffer 2, slice 2",
        "DLK TX MDL configuration, slice 3",
        "DLK TX MDL buffer 1, slice 3",
        "DLK TX MDL buffer 2, slice 3",
        "DLK TX MDL configuration, slice 4",
        "DLK TX MDL buffer 1, slice 4",
        "DLK TX MDL buffer 2, slice 4",
        "DLK TX MDL configuration, slice 5",
        "DLK TX MDL buffer 1, slice 5",
        "DLK TX MDL buffer 2, slice 5",
        "DLK TX MDL configuration, slice 6",
        "DLK TX MDL buffer 1, slice 6",
        "DLK TX MDL buffer 2, slice 6",
        "DLK TX MDL configuration, slice 7",
        "DLK TX MDL buffer 1, slice 7",
        "DLK TX MDL buffer 2, slice 7"
        };

    AtUnused(self);
    if (numRams)
        *numRams = mCount(txRamsDescription);

    return txRamsDescription;
    }

static const char** RxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32* numRams)
    {
    static const char *rxRamsDescription[] =
        {
        "DLK RX PRM configuration",
        "DLK RX MDL configuration"
        };

    AtUnused(self);
    if (numRams)
        *numRams = mCount(rxRamsDescription);

    return rxRamsDescription;
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    uint32 numRxMdlPrmRams, numTxMdlPrmRams;
    mMethodsGet(mThis(self))->RxPrmMdlRamsDescription(mThis(self), &numRxMdlPrmRams);
    mMethodsGet(mThis(self))->TxPrmMdlRamsDescription(mThis(self), &numTxMdlPrmRams);
    
    return NumPdhRams((ThaModulePdh)self) + numRxMdlPrmRams + numTxMdlPrmRams;
    }

static eBool IsDatalinkRam(AtModule self, AtInternalRam ram)
    {
    if (AtInternalRamLocalIdGet(ram) < NumPdhRams((ThaModulePdh)self))
        return cAtFalse;
    return cAtTrue;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (IsDatalinkRam(self, ram))
        return mMethodsGet(mThis(self))->HasDataLinkRams(mThis(self)) ? cAtFalse : cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < NumPdhRams((ThaModulePdh)self))
        return Tha60210011InternalRamPdhNew(self, ramId, localRamId);

    return Tha60210011InternalRamDatalinkNew(self, ramId, localRamId);
    }

static uint32 StartVersionFixDs3AisInterrupt(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x03, 0x29, 0x64);
    }

static uint32 StartHardwareVersionSupportsDataLinkFcsLsbBitOrder(ThaModulePdh self)
    {
    /* There are some middle versions hardware do not support, so let handle this
     * in DatalinkFcsBitOrderIsSupported() method */
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool DatalinkFcsBitOrderIsSupported(ThaModulePdh self)
    {
    /* MDL update is available by following firmwares:
     * - From 6.6.1014
     * - From 6.7 to 6.9 that has build < 0x1009: these versions are unofficial
     *   released for XFI tuning. They do not have this update.
     * - From 6.9.1009: it is supported
     * - From 7.0, this update is applied */
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));

    if ((hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x6, 0x1014)) &&
        (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x7, 0)))
        return cAtTrue;

    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x1009))
        return cAtTrue;

    /* As mentioned above, from 7.0, MDL update is ready */
    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x0))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PrmStsAndTug2Offset(ThaModulePdh self)
    {
    /*((STSID[4:3]*7 + VTGID)*256) + (sliceID*32 + STSID[2:0]*4 + VTID)*/
    AtUnused(self);
    return 256UL;
    }

static eBool De1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    /* This product does not have LIU */
    AtUnused(self);
    return cAtFalse;
    }

static eBool De3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    /* This product does not have LIU */
    AtUnused(self);
    return cAtFalse;
    }

static eBool RxHw3BitFeacSignalIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaPdhDebugger DebuggerObjectCreate(ThaModulePdh self)
    {
    return Tha60210011PdhDebuggerNew(self);
    }

static eAtRet De1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    uint32 sliceId, numSlices = ThaModulePdhNumSlices(self);
    uint32 regVal = (enable) ? cBit23_0 : 0x0;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = (uint32)ThaModulePdhSliceBase(self, sliceId);
        mModuleHwWrite(self, sliceBase + cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl, regVal);
        }

    return cAtOk;
    }

static eBool HasDataLinkRams(Tha60210011ModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if ((mInRange(localAddress, 0x1000000, 0x10FFFFF)) ||
        (mInRange(localAddress, 0x1100000, 0x11FFFFF)) ||
        (mInRange(localAddress, 0x1200000, 0x12FFFFF)) ||
        (mInRange(localAddress, 0x1300000, 0x13FFFFF)) ||
        (mInRange(localAddress, 0x1400000, 0x14FFFFF)) ||
        (mInRange(localAddress, 0x1500000, 0x15FFFFF)) ||
        (mInRange(localAddress, 0x1600000, 0x16FFFFF)) ||
        (mInRange(localAddress, 0x1700000, 0x17FFFFF)) ||
        (mInRange(localAddress, 0x1800000, 0x18FFFFF)) ||
        (mInRange(localAddress, 0x1900000, 0x19FFFFF)) ||
        (mInRange(localAddress, 0x1A00000, 0x1AFFFFF)) ||
        (mInRange(localAddress, 0x1B00000, 0x1BFFFFF)))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 UpenCfgCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_ctrl0_Base;
    }

static uint32 UpenDestuffCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_destuff_ctrl0_Base;
    }

static uint32 UpenPrmTxcfgcrBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfgcr_Base;
    }

static uint32 UpenPrmTxcfglbBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfglb_Base;
    }

static uint32 UpenRxprmCfgstdBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cfgstd_Base;
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210011PdhPrmInterruptManagerNew((AtModule)self);
    }

static AtInterruptManager MdlInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210011PdhMdlInterruptManagerNew((AtModule)self);
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static void MethodsInit(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TxPrmMdlRamsDescription);
        mMethodOverride(m_methods, RxPrmMdlRamsDescription);
        mMethodOverride(m_methods, PrmDefaultSet);
        mMethodOverride(m_methods, HasDataLinkRams);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule pdh = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(pdh);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        }

    mMethodsSet(pdh, &m_AtModuleOverride);
    }

static void OverrideTha60210031ModulePdh(AtModulePdh self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, HasLiu);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufBaseAddress);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufWordOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxStickyBaseAddress);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxPktOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxByteOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxDropOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, StartHardwareVersionSupportsDataLinkFcsLsbBitOrder);
        mMethodOverride(m_Tha60210031ModulePdhOverride, DatalinkFcsBitOrderIsSupported);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenDestuffCtrl0Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenCfgCtrl0Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenPrmTxcfgcrBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenPrmTxcfglbBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxprmCfgstdBase);
        }

    mMethodsSet(pdhModule, &m_Tha60210031ModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3SerialLinesGet);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3sGet);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareIsEnabled);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareEnable);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, SliceOffset);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        mMethodOverride(m_ThaModulePdhOverride, DefaultSet);
        mMethodOverride(m_ThaModulePdhOverride, MdlControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, De1ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, HasDe3Interrupt);
        mMethodOverride(m_ThaModulePdhOverride, HasDe1Interrupt);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptStatusGet);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptStatusGet);
        mMethodOverride(m_ThaModulePdhOverride, BaseAddress);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, De3TxLiuEnableIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, DetectDe1LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, DetectDe3LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Ssm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1Ssm);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, LiuForcedAisIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionFixDs3AisInterrupt);
        mMethodOverride(m_ThaModulePdhOverride, PrmStsAndTug2Offset);
        mMethodOverride(m_ThaModulePdhOverride, De1LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, De3LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptEnable);
        mMethodOverride(m_ThaModulePdhOverride, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3TxLiuEnableIsSupported);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh stmPdh = (ThaStmModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(stmPdh), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, Ds1E1FrmRxMuxSet);
        mMethodOverride(m_ThaStmModulePdhOverride, LiuDe3OverVc3IsSupported);
        }

    mMethodsSet(stmPdh, &m_ThaStmModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideThaModulePdh(self);
    OverrideAtModulePdh(self);
    OverrideThaStmModulePdh(self);
    OverrideTha60210031ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModulePdh);
    }

AtModulePdh Tha60210011ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60210011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModulePdhObjectInit(newModule, device);
    }

uint32 Tha60210011ModulePdhNumPdhRamsInSlice(Tha60210011ModulePdh self)
    {
    AtUnused(self);
    return 13;
    }

uint32 Tha60210011ModulePdhNumPdhRams(Tha60210011ModulePdh self)
    {
    if (self)
        return NumPdhRams((ThaModulePdh)self);

    return 0;
    }

const char** Tha60210011ModulePdhRxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32* numRams)
    {
    if (self)
        return mMethodsGet(self)->RxPrmMdlRamsDescription(self, numRams);

    if (numRams)
        *numRams = 0;

    return NULL;
    }

const char** Tha60210011ModulePdhTxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32 *numRams)
    {
    if (self)
        return mMethodsGet(self)->TxPrmMdlRamsDescription(self, numRams);

    if (numRams)
        *numRams = 0;

    return NULL;
    }

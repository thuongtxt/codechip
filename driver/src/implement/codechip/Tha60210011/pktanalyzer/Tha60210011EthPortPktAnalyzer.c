/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Package analyzer
 *
 * File        : Tha60210011EthPortPktAnalyzer.c
 *
 * Created Date: Jul 8, 2015
 *
 * Description : Ethernet port package analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pktanalyzer/ThaEthPort10GbPktAnalyzer.h"
#include "Tha60210011EthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartAddress(ThaPktAnalyzer self)
    {
    AtChannel port = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    AtUnused(self);
    return 0x2F000 + (AtChannelIdGet(port) * 0x10000);
    }

static eBool SkipLastInCompletePacket(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    return Tha10GPktAnalyzerEthPortHwDumpMode(self, pktDumpMode);
    }

static uint32 EthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
    return Tha10GPktAnalyzerEthPortDumpModeHw2Sw(self, pktDumpMode);
    }

static ThaPacketReader TxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static ThaPacketReader RxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderDefaultNew((AtPktAnalyzer)self);
	}

static eBool ShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode)
    {
    return Tha10GbPktAnalyzerShouldRecapture(self, newPktDumpMode, currentDumpMode);
    }

static void Recapture(AtPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    uint32 regVal;
    uint32 regAddr = cThaDiagPktDumpModeCtrl + ThaPktAnalyzerBaseAddress(pktAnalyzer);

    regVal = ThaPktAnalyzerRead(pktAnalyzer, regAddr);
    mFieldIns(&regVal, cThaDebugDumPktCtlRoMask, cThaDebugDumPktCtlRoShift, 0);
    ThaPktAnalyzerWrite(pktAnalyzer, regAddr,  regVal);
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	ThaPktAnalyzerPktDumpModeSet(self, pktDumpMode);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, SkipLastInCompletePacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortHwDumpMode);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortDumpModeHw2Sw);
        mMethodOverride(m_ThaPktAnalyzerOverride, ShouldRecapture);
        mMethodOverride(m_ThaPktAnalyzerOverride, TxPacketReaderCreate);
        mMethodOverride(m_ThaPktAnalyzerOverride, RxPacketReaderCreate);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011EthPortPktAnalyzer);
    }

AtPktAnalyzer Tha60210011EthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60210011EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011EthPortPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

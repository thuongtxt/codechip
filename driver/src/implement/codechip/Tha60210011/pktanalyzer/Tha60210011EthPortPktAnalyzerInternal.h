/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210011EthPortPktAnalyzerInternal.h
 * 
 * Created Date: May 9, 2016
 *
 * Description : Ethernet Port Packet Analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011ETHPORTPKTANALYZERINTERNAL_H_
#define _THA60210011ETHPORTPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011EthPortPktAnalyzer
    {
    tThaStmPwProductEthPortPktAnalyzer super;
    }tTha60210011EthPortPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60210011EthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011ETHPORTPKTANALYZERINTERNAL_H_ */


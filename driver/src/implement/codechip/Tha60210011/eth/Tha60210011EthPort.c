/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210011EthPort.c
 *
 * Created Date: May 7, 2015
 *
 * Description : ETH Port 10Gb
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011EthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
/* ETH Port Register */
#define cEthPortSerdesSelectReg             0x10
#define cEthPortXFISelectMask(portId)       (cBit0 << (portId))
#define cEthPortXFISelectShift(portId)      portId

#define cEthPortLoopBackControl 0x40
#define cEthPortLoopInMask      cBit1
#define cEthPortLoopInShift     1
#define cEthPortLoopOutMask     cBit5
#define cEthPortLoopOutShift    5

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tThaEthPort10GbMethods m_ThaEthPort10GbOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tThaEthPortMethods     m_ThaEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtEthPortMethods      *m_AtEthPortMethods      = NULL;
static const tThaEthPort10GbMethods *m_ThaEthPort10GbMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEthRet RxSerdesHwSelect(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal   = mChannelHwRead(self, cEthPortSerdesSelectReg, cAtModuleEth);
    uint32 portId   = AtChannelIdGet((AtChannel)self);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    mFieldIns(&regVal, cEthPortXFISelectMask(portId), cEthPortXFISelectShift(portId), (portId != serdesId) ? 1 : 0);
    mChannelHwWrite(self, cEthPortSerdesSelectReg, regVal, cAtModuleEth);
    return cAtOk;
    }

static AtModuleEth ModuleEth(AtEthPort self)
    {
    return (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eBool CanSwitchBrigdeSerdes(AtEthPort self, AtSerdesController serdes)
    {
    /* One port is assigned 2 fixed SERDES only, for example port 1 can select SERDES 1 and 5 */
    uint32 portId, serdesId, protectionSerdes;

    /* Can not release bridge */
    if (serdes == NULL)
        return cAtFalse;

    portId   = AtChannelIdGet((AtChannel)self);
    serdesId = AtSerdesControllerIdGet(serdes);
    protectionSerdes = portId + AtModuleEthMaxPortsGet(ModuleEth(self));

    if ((serdesId == portId) || (serdesId == protectionSerdes))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    if (!CanSwitchBrigdeSerdes(self, serdes))
        return cAtErrorModeNotSupport;

    return RxSerdesHwSelect(self, serdes);
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    uint32 regVal   = mChannelHwRead(self, cEthPortSerdesSelectReg, cAtModuleEth);
    uint32 portId   = AtChannelIdGet((AtChannel)self);
    AtModuleEth moduleEth = ModuleEth(self);
    uint32 serdesId;

    mFieldGet(regVal, cEthPortXFISelectMask(portId), cEthPortXFISelectShift(portId), uint32, &serdesId);
    serdesId = (serdesId == 0) ? portId : portId + AtModuleEthMaxPortsGet(moduleEth);
    return AtModuleEthSerdesController(moduleEth, serdesId);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (!CanSwitchBrigdeSerdes(self, serdes))
        return cAtErrorModeNotSupport;

    /* HW bridge as default */
    return cAtOk;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    return CanSwitchBrigdeSerdes(self, serdes);
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtModuleEth moduleEth = ModuleEth(self);
    uint32 portId   = AtChannelIdGet((AtChannel)self);
    uint32 brigdedserdesId = portId + AtModuleEthMaxPortsGet(moduleEth);

    return AtModuleEthSerdesController(moduleEth, brigdedserdesId);
    }

static eBool DebugCounterIsSupported(ThaEthPort10Gb self, uint8 counterType)
    {
    eAtEthPortCounterType typeCounter = counterType;

    AtUnused(self);
    if ((typeCounter == cAtEthPortCounterTxPackets)             ||
        (typeCounter == cAtEthPortCounterTxBytes)               ||
        (typeCounter == cAtEthPortCounterTxPacketsLen0_64)      ||
        (typeCounter == cAtEthPortCounterTxPacketsLen65_127)    ||
        (typeCounter == cAtEthPortCounterTxPacketsLen128_255)   ||
        (typeCounter == cAtEthPortCounterTxPacketsLen256_511)   ||
        (typeCounter == cAtEthPortCounterTxPacketsLen512_1024)  ||
        (typeCounter == cAtEthPortCounterTxPacketsLen1025_1528) ||
        (typeCounter == cAtEthPortCounterTxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterTxPacketsJumbo)        ||
        (typeCounter == cAtEthPortCounterTxBrdCastPackets)      ||
        (typeCounter == cAtEthPortCounterTxMultCastPackets)     ||
        (typeCounter == cAtEthPortCounterTxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxPackets)             ||
        (typeCounter == cAtEthPortCounterRxBytes)               ||
        (typeCounter == cAtEthPortCounterRxPacketsLen0_64)      ||
        (typeCounter == cAtEthPortCounterRxPacketsLen65_127)    ||
        (typeCounter == cAtEthPortCounterRxPacketsLen128_255)   ||
        (typeCounter == cAtEthPortCounterRxPacketsLen256_511)   ||
        (typeCounter == cAtEthPortCounterRxPacketsLen512_1024)  ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1025_1528) ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterRxPacketsJumbo)        ||
        (typeCounter == cAtEthPortCounterRxOversizePackets)     ||
        (typeCounter == cAtEthPortCounterRxUndersizePackets)    ||
        (typeCounter == cAtEthPortCounterRxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxBrdCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxMultCastPackets)     ||
        (typeCounter == cAtEthPortCounterRxPhysicalError))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    return DebugCounterIsSupported((ThaEthPort10Gb)self, (uint8)counterType);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AtEthPortMacCheckingEnable((AtEthPort)self, cAtFalse);
    ret |= AtChannelEnable(self, cAtTrue);
    ret |= AtChannelLoopbackSet(self, cAtLoopbackModeRelease);

    return ret;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    if (interface != cAtEthPortInterfaceXGMii)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceXGMii;
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    AtUnused(self);
    AtUnused(activate);
    return cAtOk;
    }

static eAtModuleEthRet HiGigEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    if (enable)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = cEthPortLoopBackControl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cEthPortLoopIn, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = cEthPortLoopBackControl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cEthPortLoopOut, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LoopInIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = cEthPortLoopBackControl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cEthPortLoopInMask)
        return cAtTrue;

    return cAtFalse;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = cEthPortLoopBackControl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cEthPortLoopOutMask)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaEthPort10Gb(AtEthPort self)
    {
    ThaEthPort10Gb port10gb = (ThaEthPort10Gb)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPort10GbMethods = mMethodsGet(port10gb);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPort10GbOverride, m_ThaEthPort10GbMethods, sizeof(tThaEthPort10GbMethods));

        mMethodOverride(m_ThaEthPort10GbOverride, DebugCounterIsSupported);
        }

    mMethodsSet(port10gb, &m_ThaEthPort10GbOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, HiGigEnable);
        mMethodOverride(m_AtEthPortOverride, HiGigIsEnabled);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(ethPort), sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        mMethodOverride(m_ThaEthPortOverride, LoopInEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutIsEnabled);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideThaEthPort10Gb(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011EthPort);
    }

AtEthPort Tha60210011EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011EthPort10GbObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210011EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011EthPortObjectInit(newPort, portId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha6021ModuleEth.c
 *
 * Created Date: Aug 29, 2015
 *
 * Description : Common functions used for 6021Xxxx products.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "Tha6021ModuleEthReg.h"
#include "Tha6021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool TxSerdesCanBridge(AtEthPort port, AtSerdesController serdes)
    {
    AtSerdesController bridgedSerdes;

    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet((AtChannel)port)))
        return cAtFalse;

    bridgedSerdes = AtEthPortTxBridgedSerdes(port);
    if (bridgedSerdes && (serdes != bridgedSerdes))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort port, AtSerdesController serdes)
    {
    AtSerdesController bridgedSerdes;

    /* To avoid conflicting between normal mode and diagnostic mode. In case of
     * diagnostic mode, bridging would be prevented since it is not necessary */
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet((AtChannel)port)))
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "Not allow bridging in diagnostic mode\r\n");
        return cAtErrorModeNotSupport;
        }

    /* Already bridge */
    bridgedSerdes = AtEthPortTxBridgedSerdes(port);
    if (serdes == bridgedSerdes)
        return cAtOk;

    /* It is being bridged to another interface */
    if (bridgedSerdes && (serdes != bridgedSerdes))
        return cAtErrorChannelBusy;

    return AtSerdesControllerEnable(serdes, cAtTrue);
    }

static eAtModuleEthRet TxSerdesBridgeRelease(AtEthPort port)
    {
    AtSerdesController bridgedSerdes = AtEthPortTxBridgedSerdes(port);
    if (bridgedSerdes == NULL)
        return cAtOk;
    return AtSerdesControllerEnable(bridgedSerdes, cAtFalse);
    }

eAtModuleEthRet Tha6021DsxEthPortRxSerdesSelect(AtEthPort port, AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    uint32 regVal  = mChannelHwRead(port, cEthPortQSGMIIXFIControlReg, cAtModuleEth);
    mRegFieldSet(regVal, cEthPortSGMIILaneSelect, 0);
    mRegFieldSet(regVal, cEthPortQSGMIISerDesIdSelect, serdesId);
    mRegFieldSet(regVal, cEthPortXFISerDesIdSelect, serdesId);
    mChannelHwWrite(port, cEthPortQSGMIIXFIControlReg, regVal, cAtModuleEth);
    return cAtOk;
    }

AtSerdesController Tha6021DsxEthPortRxSelectedSerdes(AtEthPort port)
    {
    uint32 regVal   = mChannelHwRead(port, cEthPortQSGMIIXFIControlReg, cAtModuleEth);
    uint32 serdesId = (regVal & cEthPortQSGMIISerDesIdSelectMask) >> cEthPortQSGMIISerDesIdSelectShift;
    AtModuleEth ethModule = (AtModuleEth) AtChannelModuleGet((AtChannel) port);
    return AtModuleEthSerdesController(ethModule, serdesId);
    }

eAtModuleEthRet Tha6021DsxEthPortTxSerdesBridge(AtEthPort port, AtSerdesController serdes)
    {
    if (serdes)
        return TxSerdesBridge(port, serdes);
    return TxSerdesBridgeRelease(port);
    }

eBool Tha6021DsxEthPortTxSerdesCanBridge(AtEthPort port, AtSerdesController serdes)
    {
    if (serdes)
        return TxSerdesCanBridge(port, serdes);
    return cAtTrue;
    }

AtSerdesController Tha6021DsxEthPortTxBridgedSerdes(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 numSerdes = AtModuleEthNumSerdesControllers(ethModule);
    uint32 serdes_i;

    /* Do not handle bridging in case of diagnostic mode is enabled */
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet((AtChannel)self)))
        return NULL;

    /* NOTE: do not start from first SERDES since it always transmits traffic
     * for the internal MAC */
    for (serdes_i = 1; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = AtModuleEthSerdesController(ethModule, serdes_i);
        if (AtSerdesControllerIsEnabled(serdes))
            return serdes;
        }

    return NULL;
    }

eAtRet Tha6021DsxModuleEthHwDiagnosticModeEnable(AtModule ethModule, eBool enable)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtModuleDeviceGet(ethModule), 0);
    uint32 regVal;
    uint8 diag = enable ? 1 : 0;

    if (!AtModuleEthSerdesApsSupported((AtModuleEth)ethModule))
        return cAtOk;

    regVal = mModuleHwReadByHal(ethModule, cEthPortQSGMIIXFIControlReg, hal);
    mRegFieldSet(regVal, cEthPortQSGMIIDiagEnable, diag);
    mRegFieldSet(regVal, cEthPortXfiDiagEnable, diag);
    mModuleHwWriteByHal(ethModule, cEthPortQSGMIIXFIControlReg, regVal, hal);

    /* Need to control TX enabling properly */
    if (enable)
        regVal = cEthPortSerdesTxEnableAll;
    else
        regVal = cEthPortSerdesTxEnableFirstQsgmii | cEthPortSerdesTxEnableFirstXfi;

    mModuleHwWriteByHal(ethModule, cEthPortSerdesTxEnable, regVal, hal);

    return cAtOk;
    }

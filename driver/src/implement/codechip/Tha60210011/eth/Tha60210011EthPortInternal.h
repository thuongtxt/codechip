/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210011EthPortInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011ETHPORTINTERNAL_H_
#define _THA60210011ETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/eth/Tha60150011EthPort10GbInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011EthPort
    {
    tTha60150011EthPort10Gb super;
    }tTha60210011EthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60210011EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011ETHPORTINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210011ModuleEthInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEETHINTERNAL_H_
#define _THA60210011MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/eth/Tha60150011ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleEth
    {
    tTha60150011ModuleEth super;
    }tTha60210011ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60210011ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEETHINTERNAL_H_ */


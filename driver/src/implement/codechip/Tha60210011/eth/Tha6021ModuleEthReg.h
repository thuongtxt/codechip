/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha6021ModuleEthReg.h
 * 
 * Created Date: Aug 29, 2015
 *
 * Description : Common registers used for 6021Xxxx products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021MODULEETHREG_H_
#define _THA6021MODULEETHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* RX selecting */
#define cEthPortQSGMIIXFIControlReg       0xF00040
#define cEthPortXfiDiagEnableMask         cBit13
#define cEthPortXfiDiagEnableShift        13
#define cEthPortQSGMIIDiagEnableMask      cBit12
#define cEthPortQSGMIIDiagEnableShift     12
#define cEthPortXFISerDesIdSelectMask     cBit9_8
#define cEthPortXFISerDesIdSelectShift    8
#define cEthPortQSGMIISerDesIdSelectMask  cBit7_6
#define cEthPortQSGMIISerDesIdSelectShift 6
#define cEthPortSGMIILaneSelectMask       cBit5_4
#define cEthPortSGMIILaneSelectShift      4

/* TX enabling */
#define cEthPortSerdesTxEnable            0xF00041
#define cEthPortSerdesQSgmiiLaneEnableMask(serdesId, laneId)  (cBit0 << (((serdesId) * 4) + (laneId)))
#define cEthPortSerdesQSgmiiLaneEnableShift(serdesId, laneId) (((serdesId) * 4) + (laneId))
#define cEthPortSerdesXfiEnableMask(serdesId)  (cBit16 << (serdesId))
#define cEthPortSerdesXfiEnableShift(serdesId) (16 + (serdesId))
#define cEthPortSerdesTxEnableAll         cBit19_0
#define cEthPortSerdesTxEnableFirstQsgmii cBit3_0
#define cEthPortSerdesTxEnableFirstXfi    cBit16

/* QSGMII serdes physical parameter registers */
#define cAf6Reg_top_o_control2_txprecursor  0xFE6042
#define cAf6Reg_top_o_control1_txposcursor  0xFE6041
#define cAf6Reg_top_o_control0_txdiffctrl   0xFE6040

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA6021MODULEETHREG_H_ */


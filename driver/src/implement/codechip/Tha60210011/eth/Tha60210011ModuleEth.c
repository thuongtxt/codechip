/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210011ModuleEth.c
 *
 * Created Date: May 7, 2015
 *
 * Description : ETH Module of product 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../man/Tha60210011Device.h"
#include "../prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "Tha60210011ModuleEthInternal.h"
#include "Tha6021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011ModuleEth *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/* Save super implementations */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tAtModuleEthMethods  *m_AtModuleEthMethods  = NULL;
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return Tha60210011EthPortNew(portId, self);
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    AtUnused(self);
    return portId * 0x10000 + 0x21000;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsEthRegister(AtModuleDeviceGet(self), address);
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    /* SERDES manager will manage this */
    AtUnused(self);
    AtUnused(ethPort);
    return NULL;
    }

static eBool PortSerdesPrbsIsSupported(ThaModuleEth self, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(ethPort);
    return cAtTrue;
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
    AtUnused(ethPort);
    return Tha6021XfiSerdesPrbsEngineNew(AtModuleEthSerdesController((AtModuleEth)self, serdesId));
    }

static uint32 DiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return 0xF50000;
    }

static eBool HasSerdesControllers(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SerdesHasMdio(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SerdesMdioBaseAddress(AtModuleEth self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return 0xF47000;
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    return Tha60210011EthSerdesManagerNew((AtModuleEth)self);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return cAtEthPortInterfaceXGMii;
    }

static ThaVersionReader VersionReader(ThaModuleEth self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x09, 0x1, 0x25);
    }

static eBool FullSerdesPrbsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnableTxSerdes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PortHasIpAddress(ThaModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtFalse;
    }

static eAtRet DebugGeSticky(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, HasSerdesControllers);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasMdio);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdioBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleEthOverride, DiagBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionSupportsSerdesAps);
        mMethodOverride(m_ThaModuleEthOverride, FullSerdesPrbsSupported);
        mMethodOverride(m_ThaModuleEthOverride, CanEnableTxSerdes);
        mMethodOverride(m_ThaModuleEthOverride, PortHasIpAddress);
        mMethodOverride(m_ThaModuleEthOverride, DebugGeSticky);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleEth);
    }

AtModuleEth Tha60210011ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60210011ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleEthObjectInit(newModule, device);
    }

uint32 Tha6021ModuleEthSerdesMdioBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF47000;
    }

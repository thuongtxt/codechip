/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha6021ModuleEth.h
 * 
 * Created Date: Jul 18, 2015
 *
 * Description : Common header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021MODULEETH_H_
#define _THA6021MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha6021ModuleEthSerdesMdioBaseAddress(AtModuleEth self);

eAtRet Tha6021DsxModuleEthHwDiagnosticModeEnable(AtModule ethModule, eBool enable);
eBool Tha6021ModuleEthHwDiagnosticModeIsEnabled(AtModule ethModule);

eAtModuleEthRet Tha6021DsxEthPortRxSerdesSelect(AtEthPort port, AtSerdesController serdes);
AtSerdesController Tha6021DsxEthPortRxSelectedSerdes(AtEthPort port);
eAtModuleEthRet Tha6021DsxEthPortTxSerdesBridge(AtEthPort port, AtSerdesController serdes);
AtSerdesController Tha6021DsxEthPortTxBridgedSerdes(AtEthPort self);
eBool Tha6021DsxEthPortTxSerdesCanBridge(AtEthPort port, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021MODULEETH_H_ */


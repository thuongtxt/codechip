/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210011ModuleOcnInternal.h
 * 
 * Created Date: May 27, 2015
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEOCNINTERNAL_H_
#define _THA60210011MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/util/ThaUtil.h"
#include "Tha60210011ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleOcnMethods
    {
    eBool (*UseTfi5)(Tha60210011ModuleOcn self);
    eBool (*NeedScramble)(Tha60210011ModuleOcn self);
    eBool (*NeedMasterTfi5)(Tha60210011ModuleOcn self);
    uint32 (*OcnTerStsPointerGeneratorPerChannelControlRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*HwStsDefaultOffset)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts);
    uint32 (*TxHwStsDefaultOffset)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts);
    eAtRet (*LomCheckingEnable)(Tha60210011ModuleOcn self, AtSdhChannel channel, eBool enable);
    eBool (*LomCheckingIsEnabled)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    eBool (*HoLinePiStsConcatRelease)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts);
    eAtRet (*HoLinePiStsConcatMasterSet)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 masterStsId);
    eAtRet (*HoLinePiStsConcatSlaveSet)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId);
    uint32 (*LoStsPayloadHwFormula)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts);
    uint32 (*TxLoStsPayloadHwFormula)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts);
    eBool  (*ChannelCanBeTerminated)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    eBool  (*ChannelHasAttPointerProcessor)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    eBool  (*ChannelHasPointerProcessor)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    eAtRet (*ShowConcateDebugInformation)(Tha60210011ModuleOcn self);
    uint8 (*StartSts1ToScanConcate)(Tha60210011ModuleOcn self, AtSdhChannel aug);
    uint8  (*NumStsToReleaseConcate)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    eBool  (*HoPathPhyModuleIsDividedToOc24)(Tha60210011ModuleOcn self, eAtModule phyModule);
    eBool (*LoPathPhyModuleIsDividedToBlockOc24)(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule);
    uint32 (*StartVersionNeedHwRxPiPjAdjThreshold)(Tha60210011ModuleOcn self);
    eBool (*ShouldEnableLosPpmDetection)(Tha60210011ModuleOcn self);

    /* HW STS conversion */
    void   (*Oc48HwStsAndSliceConvertToOc24)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8* sliceId, uint8* hwStsInSlice);
    uint8  (*SliceOfChannel)(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule);
    uint8  (*SdhChannelSwStsId2HwFlatId)(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 sts1Id);

    /* Registers */
    uint32 (*OcnVtTuPointerInterpreterPerChannelControlRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnVtTuPointerGeneratorPerChannelControlRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnStsPointerInterpreterPerChannelControlRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnStsPointerGeneratorPerChannelControlRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnRxHoMapConcatenateReg)(Tha60210011ModuleOcn self);
    uint32 (*RxStsPayloadControl)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*TxStsMultiplexingControl)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnStsPointerInterpreterPerChannelInterruptRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel);
    uint32 (*OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel);
    uint32 (*OcnStsPointerGeneratorPerChannelInterruptRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnStsPointerInterpreterPerChannelAdjustCountRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnStsPointerGeneratorPerChannelAdjustCountRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnVtTuPointerInterpreterPerChannelInterruptRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnVtTuPointerGeneratorPerChannelInterruptRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnVtTuPointerGeneratorNdfIntrMask)(Tha60210011ModuleOcn self);
    uint32 (*OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr)(Tha60210011ModuleOcn self, AtSdhChannel channel);
    uint32 (*RxSxcControlRegAddr)(Tha60210011ModuleOcn self);
    uint32 (*TxSxcControlRegAddr)(Tha60210011ModuleOcn self);
    uint32 (*Tfi5GlbtfmBase)(Tha60210011ModuleOcn self);
    uint32 (*Tfi5GlbrfmBase)(Tha60210011ModuleOcn self);
    uint32 (*GlbtpgBase)(Tha60210011ModuleOcn self);
    uint32 (*GlbspiBase)(Tha60210011ModuleOcn self);
    uint32 (*GlbvpiBase)(Tha60210011ModuleOcn self);

    /* Low-order VC-3/VC-1x alarms force */
    uint32 (*TuVcRxForcibleAlarms)(Tha60210011ModuleOcn self, AtChannel tuVc);
    eAtRet (*TuVcHwRxAlarmForce)(Tha60210011ModuleOcn self, AtChannel tuVc, uint32 alarmType, eBool force);
    uint32 (*TuVcRxForcedAlarmGet)(Tha60210011ModuleOcn self, AtChannel tuVc);

    /* Low-order VC-3/VC-1x loopback */
    eBool (*TuVcLoopbackIsSupported)(Tha60210011ModuleOcn self, AtChannel tuVc, uint8 loopbackMode);
    eBool (*TuVcIsLoopbackAtPointerProcessor)(Tha60210011ModuleOcn self, AtChannel tuVc);

    }tTha60210011ModuleOcnMethods;

typedef struct tTha60210011ModuleOcn
    {
    tThaModuleOcn super;
    const tTha60210011ModuleOcnMethods *methods;
    }tTha60210011ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEOCNINTERNAL_H_ */


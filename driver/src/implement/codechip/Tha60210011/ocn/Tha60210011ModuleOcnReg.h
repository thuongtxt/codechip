/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: Apr 23, 2015                                                               
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_OCN_H_
#define _AF6_REG_AF6CCI0011_RD_OCN_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Control
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfm_reg_Base                                                                        0x00000
#define cAf6Reg_glbrfm_reg                                                                             0x00000
#define cAf6Reg_glbrfm_reg_WidthVal                                                                        128
#define cAf6Reg_glbrfm_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RxLineSyncSel
BitField Type: RW
BitField Desc: Select line id for synchronization 8 rx lines. Bit[0] for line 0.
Note that must only 1 bit is set
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxLineSyncSel_Bit_Start                                                             16
#define cAf6_glbrfm_reg_RxLineSyncSel_Bit_End                                                               23
#define cAf6_glbrfm_reg_RxLineSyncSel_Mask                                                           cBit23_16
#define cAf6_glbrfm_reg_RxLineSyncSel_Shift                                                                 16
#define cAf6_glbrfm_reg_RxLineSyncSel_MaxVal                                                              0xff
#define cAf6_glbrfm_reg_RxLineSyncSel_MinVal                                                               0x0
#define cAf6_glbrfm_reg_RxLineSyncSel_RstVal                                                               0x1

/*--------------------------------------
BitField Name: RxFrmLosAisEn
BitField Type: RW
BitField Desc: Enable/disable forwarding P_AIS when LOS detected at Rx Framer.
1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLosAisEn_Bit_Start                                                             14
#define cAf6_glbrfm_reg_RxFrmLosAisEn_Bit_End                                                               14
#define cAf6_glbrfm_reg_RxFrmLosAisEn_Mask                                                              cBit14
#define cAf6_glbrfm_reg_RxFrmLosAisEn_Shift                                                                 14
#define cAf6_glbrfm_reg_RxFrmLosAisEn_MaxVal                                                               0x1
#define cAf6_glbrfm_reg_RxFrmLosAisEn_MinVal                                                               0x0
#define cAf6_glbrfm_reg_RxFrmLosAisEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: RxFrmOofAisEn
BitField Type: RW
BitField Desc: Enable/disable forwarding P_AIS when OOF detected at Rx Framer.
1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmOofAisEn_Bit_Start                                                             13
#define cAf6_glbrfm_reg_RxFrmOofAisEn_Bit_End                                                               13
#define cAf6_glbrfm_reg_RxFrmOofAisEn_Mask                                                              cBit13
#define cAf6_glbrfm_reg_RxFrmOofAisEn_Shift                                                                 13
#define cAf6_glbrfm_reg_RxFrmOofAisEn_MaxVal                                                               0x1
#define cAf6_glbrfm_reg_RxFrmOofAisEn_MinVal                                                               0x0
#define cAf6_glbrfm_reg_RxFrmOofAisEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: RxFrmB1BlockCntMod
BitField Type: RW
BitField Desc: B1 Counter Mode. 1: Block mode 0: Bit-wise mode
BitField Bits: [12]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_Bit_Start                                                        12
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_Bit_End                                                          12
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_Mask                                                         cBit12
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_Shift                                                            12
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_MaxVal                                                          0x1
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_MinVal                                                          0x0
#define cAf6_glbrfm_reg_RxFrmB1BlockCntMod_RstVal                                                          0x1

/*--------------------------------------
BitField Name: RxFrmBadFrmThresh
BitField Type: RW
BitField Desc: Threshold for A1A2 missing counter, that is used to change state
from FRAMED to HUNT.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_Bit_Start                                                          8
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_Bit_End                                                           10
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_Mask                                                        cBit10_8
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_Shift                                                              8
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_MaxVal                                                           0x7
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_MinVal                                                           0x0
#define cAf6_glbrfm_reg_RxFrmBadFrmThresh_RstVal                                                           0x3

/*--------------------------------------
BitField Name: RxFrmB1GoodThresh
BitField Type: RW
BitField Desc: Threshold for B1 good counter, that is used to change state from
CHECK to FRAMED.
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_Bit_Start                                                          4
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_Bit_End                                                            6
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_Mask                                                         cBit6_4
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_Shift                                                              4
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_MaxVal                                                           0x7
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_MinVal                                                           0x0
#define cAf6_glbrfm_reg_RxFrmB1GoodThresh_RstVal                                                           0x4

/*--------------------------------------
BitField Name: RxFrmDescrEn
BitField Type: RW
BitField Desc: Enable/disable de-scrambling of the Rx coming data stream. 1:
Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmDescrEn_Bit_Start                                                               2
#define cAf6_glbrfm_reg_RxFrmDescrEn_Bit_End                                                                 2
#define cAf6_glbrfm_reg_RxFrmDescrEn_Mask                                                                cBit2
#define cAf6_glbrfm_reg_RxFrmDescrEn_Shift                                                                   2
#define cAf6_glbrfm_reg_RxFrmDescrEn_MaxVal                                                                0x1
#define cAf6_glbrfm_reg_RxFrmDescrEn_MinVal                                                                0x0
#define cAf6_glbrfm_reg_RxFrmDescrEn_RstVal                                                                0x1

/*--------------------------------------
BitField Name: RxFrmB1ChkFrmEn
BitField Type: RW
BitField Desc: Enable/disable B1 check option is added to the required framing
algorithm. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_Bit_Start                                                            1
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_Bit_End                                                              1
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_Mask                                                             cBit1
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_Shift                                                                1
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_MaxVal                                                             0x1
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_MinVal                                                             0x0
#define cAf6_glbrfm_reg_RxFrmB1ChkFrmEn_RstVal                                                             0x1

/*--------------------------------------
BitField Name: RxFrmTfi5ModeEn
BitField Type: RW
BitField Desc: TFI-5 mode. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_Bit_Start                                                            0
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_Bit_End                                                              0
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_Mask                                                             cBit0
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_Shift                                                                0
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_MaxVal                                                             0x1
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_MinVal                                                             0x0
#define cAf6_glbrfm_reg_RxFrmTfi5ModeEn_RstVal                                                             0x1


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtfm_reg_Base                                                                        0x00001
#define cAf6Reg_glbtfm_reg                                                                             0x00001
#define cAf6Reg_glbtfm_reg_WidthVal                                                                        128
#define cAf6Reg_glbtfm_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: TxLineSyncSel
BitField Type: RW
BitField Desc: Select line id for synchronization 8 tx lines. Bit[0] for line 0.
Note that must only 1 bit is set
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxLineSyncSel_Bit_Start                                                              4
#define cAf6_glbtfm_reg_TxLineSyncSel_Bit_End                                                               11
#define cAf6_glbtfm_reg_TxLineSyncSel_Mask                                                            cBit11_4
#define cAf6_glbtfm_reg_TxLineSyncSel_Shift                                                                  4
#define cAf6_glbtfm_reg_TxLineSyncSel_MaxVal                                                              0xff
#define cAf6_glbtfm_reg_TxLineSyncSel_MinVal                                                               0x0
#define cAf6_glbtfm_reg_TxLineSyncSel_RstVal                                                               0x1

/*--------------------------------------
BitField Name: TxFrmScrEn
BitField Type: RW
BitField Desc: Enable/disable scrambling of the Tx data stream. 1: Enable 0:
Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmScrEn_Bit_Start                                                                 0
#define cAf6_glbtfm_reg_TxFrmScrEn_Bit_End                                                                   0
#define cAf6_glbtfm_reg_TxFrmScrEn_Mask                                                                  cBit0
#define cAf6_glbtfm_reg_TxFrmScrEn_Shift                                                                     0
#define cAf6_glbtfm_reg_TxFrmScrEn_MaxVal                                                                  0x1
#define cAf6_glbtfm_reg_TxFrmScrEn_MinVal                                                                  0x0
#define cAf6_glbtfm_reg_TxFrmScrEn_RstVal                                                                  0x1


/*------------------------------------------------------------------------------
Reg Name   : OCN Global STS Pointer Interpreter Control
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the STS Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbspi_reg_Base                                                                        0x00002
#define cAf6Reg_glbspi_reg                                                                             0x00002
#define cAf6Reg_glbspi_reg_WidthVal                                                                        128
#define cAf6Reg_glbspi_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgFlowThresh_Bit_Start                                                            24
#define cAf6_glbspi_reg_RxPgFlowThresh_Bit_End                                                              27
#define cAf6_glbspi_reg_RxPgFlowThresh_Mask                                                          cBit27_24
#define cAf6_glbspi_reg_RxPgFlowThresh_Shift                                                                24
#define cAf6_glbspi_reg_RxPgFlowThresh_MaxVal                                                              0xf
#define cAf6_glbspi_reg_RxPgFlowThresh_MinVal                                                              0x0
#define cAf6_glbspi_reg_RxPgFlowThresh_RstVal                                                              0x3

/*--------------------------------------
BitField Name: RxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a pointer increment/decrement.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgAdjThresh_Bit_Start                                                             20
#define cAf6_glbspi_reg_RxPgAdjThresh_Bit_End                                                               23
#define cAf6_glbspi_reg_RxPgAdjThresh_Mask                                                           cBit23_20
#define cAf6_glbspi_reg_RxPgAdjThresh_Shift                                                                 20
#define cAf6_glbspi_reg_RxPgAdjThresh_MaxVal                                                               0xf
#define cAf6_glbspi_reg_RxPgAdjThresh_MinVal                                                               0x0
#define cAf6_glbspi_reg_RxPgAdjThresh_RstVal                                                               0xc

/*--------------------------------------
BitField Name: StsPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when AIS state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAisAisPEn_Bit_Start                                                            18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Bit_End                                                              18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Mask                                                             cBit18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Shift                                                                18
#define cAf6_glbspi_reg_StsPiAisAisPEn_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiAisAisPEn_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiAisAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: StsPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when LOP state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiLopAisPEn_Bit_Start                                                            17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Bit_End                                                              17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Mask                                                             cBit17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Shift                                                                17
#define cAf6_glbspi_reg_StsPiLopAisPEn_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiLopAisPEn_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiLopAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: StsPiMajorMode
BitField Type: RW
BitField Desc: Majority mode for detecting increment/decrement at STS pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiMajorMode_Bit_Start                                                            16
#define cAf6_glbspi_reg_StsPiMajorMode_Bit_End                                                              16
#define cAf6_glbspi_reg_StsPiMajorMode_Mask                                                             cBit16
#define cAf6_glbspi_reg_StsPiMajorMode_Shift                                                                16
#define cAf6_glbspi_reg_StsPiMajorMode_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiMajorMode_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiMajorMode_RstVal                                                              0x1

/*--------------------------------------
BitField Name: StsPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Bit_Start                                                         12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Bit_End                                                           13
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Mask                                                       cBit13_12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Shift                                                             12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_MaxVal                                                           0x3
#define cAf6_glbspi_reg_StsPiNorPtrThresh_MinVal                                                           0x0
#define cAf6_glbspi_reg_StsPiNorPtrThresh_RstVal                                                           0x3

/*--------------------------------------
BitField Name: StsPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Bit_Start                                                          8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Bit_End                                                           11
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Mask                                                        cBit11_8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Shift                                                              8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_MaxVal                                                           0xf
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_MinVal                                                           0x0
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_RstVal                                                           0x8

/*--------------------------------------
BitField Name: StsPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Bit_Start                                                          4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Bit_End                                                            7
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Mask                                                         cBit7_4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Shift                                                              4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_MaxVal                                                           0xf
#define cAf6_glbspi_reg_StsPiBadPtrThresh_MinVal                                                           0x0
#define cAf6_glbspi_reg_StsPiBadPtrThresh_RstVal                                                           0x8

/*--------------------------------------
BitField Name: StsPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable STS POH defect types to downstream AIS in case of
terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM
defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for
PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiPohAisType_Bit_Start                                                            0
#define cAf6_glbspi_reg_StsPiPohAisType_Bit_End                                                              3
#define cAf6_glbspi_reg_StsPiPohAisType_Mask                                                           cBit3_0
#define cAf6_glbspi_reg_StsPiPohAisType_Shift                                                                0
#define cAf6_glbspi_reg_StsPiPohAisType_MaxVal                                                             0xf
#define cAf6_glbspi_reg_StsPiPohAisType_MinVal                                                             0x0
#define cAf6_glbspi_reg_StsPiPohAisType_RstVal                                                             0xf


/*------------------------------------------------------------------------------
Reg Name   : OCN Global VTTU Pointer Interpreter Control
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the VTTU Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbvpi_reg_Base                                                                        0x00003
#define cAf6Reg_glbvpi_reg                                                                             0x00003
#define cAf6Reg_glbvpi_reg_WidthVal                                                                        128
#define cAf6Reg_glbvpi_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: VtPiLomAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Bit_Start                                                             29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Bit_End                                                               29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Mask                                                              cBit29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Shift                                                                 29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiLomAisPEn_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiLomAisPEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiLomInvlCntMod
BitField Type: RW
BitField Desc: H4 monitoring mode. 1: Expected H4 is current frame in the
validated sequence plus one. 0: Expected H4 is the last received value plus one.
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Bit_Start                                                         28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Bit_End                                                           28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Mask                                                          cBit28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Shift                                                             28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_MaxVal                                                           0x1
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_MinVal                                                           0x0
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_RstVal                                                           0x1

/*--------------------------------------
BitField Name: VtPiLomGoodThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with validated sequence
of multi framers in LOM state for condition to entering IM state.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Bit_Start                                                         24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Bit_End                                                           27
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Mask                                                       cBit27_24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Shift                                                             24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_MaxVal                                                           0xf
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_MinVal                                                           0x0
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_RstVal                                                           0xf

/*--------------------------------------
BitField Name: VtPiLomInvlThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with invalidated
sequence of multi framers in IM state  for condition to entering LOM state.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Bit_Start                                                         20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Bit_End                                                           23
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Mask                                                       cBit23_20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Shift                                                             20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_MaxVal                                                           0xf
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_MinVal                                                           0x0
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_RstVal                                                           0x8

/*--------------------------------------
BitField Name: VtPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when AIS state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Bit_Start                                                             18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Bit_End                                                               18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Mask                                                              cBit18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Shift                                                                 18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiAisAisPEn_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiAisAisPEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOP state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Bit_Start                                                             17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Bit_End                                                               17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Mask                                                              cBit17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Shift                                                                 17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiLopAisPEn_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiLopAisPEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiMajorMode
BitField Type: RW
BitField Desc: Majority mode detecting increment/decrement in VTTU pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiMajorMode_Bit_Start                                                             16
#define cAf6_glbvpi_reg_VtPiMajorMode_Bit_End                                                               16
#define cAf6_glbvpi_reg_VtPiMajorMode_Mask                                                              cBit16
#define cAf6_glbvpi_reg_VtPiMajorMode_Shift                                                                 16
#define cAf6_glbvpi_reg_VtPiMajorMode_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiMajorMode_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiMajorMode_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Bit_Start                                                          12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Bit_End                                                            13
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Mask                                                        cBit13_12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Shift                                                              12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_MaxVal                                                            0x3
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_MinVal                                                            0x0
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_RstVal                                                            0x3

/*--------------------------------------
BitField Name: VtPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Bit_Start                                                           8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Bit_End                                                            11
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Mask                                                         cBit11_8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Shift                                                               8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_MaxVal                                                            0xf
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_MinVal                                                            0x0
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_RstVal                                                            0x8

/*--------------------------------------
BitField Name: VtPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Bit_Start                                                           4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Bit_End                                                             7
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Mask                                                          cBit7_4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Shift                                                               4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_MaxVal                                                            0xf
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_MinVal                                                            0x0
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_RstVal                                                            0x8

/*--------------------------------------
BitField Name: VtPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable VTTU POH defect types to downstream AIS in case of
terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-
equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiPohAisType_Bit_Start                                                             0
#define cAf6_glbvpi_reg_VtPiPohAisType_Bit_End                                                               3
#define cAf6_glbvpi_reg_VtPiPohAisType_Mask                                                            cBit3_0
#define cAf6_glbvpi_reg_VtPiPohAisType_Shift                                                                 0
#define cAf6_glbvpi_reg_VtPiPohAisType_MaxVal                                                              0xf
#define cAf6_glbvpi_reg_VtPiPohAisType_MinVal                                                              0x0
#define cAf6_glbvpi_reg_VtPiPohAisType_RstVal                                                              0xf


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Pointer Generator Control
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Pointer Generator

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtpg_reg_Base                                                                        0x00004
#define cAf6Reg_glbtpg_reg                                                                             0x00004
#define cAf6Reg_glbtpg_reg_WidthVal                                                                        128
#define cAf6Reg_glbtpg_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: TxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames to make a condition of pointer adjustments.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Bit_Start                                                           8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Bit_End                                                             9
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Mask                                                          cBit9_8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Shift                                                               8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_MaxVal                                                            0x3
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_MinVal                                                            0x0
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_RstVal                                                            0x3

/*--------------------------------------
BitField Name: TxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer
of TxFiFo.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgFlowThresh_Bit_Start                                                             4
#define cAf6_glbtpg_reg_TxPgFlowThresh_Bit_End                                                               7
#define cAf6_glbtpg_reg_TxPgFlowThresh_Mask                                                            cBit7_4
#define cAf6_glbtpg_reg_TxPgFlowThresh_Shift                                                                 4
#define cAf6_glbtpg_reg_TxPgFlowThresh_MaxVal                                                              0xf
#define cAf6_glbtpg_reg_TxPgFlowThresh_MinVal                                                              0x0
#define cAf6_glbtpg_reg_TxPgFlowThresh_RstVal                                                              0x3

/*--------------------------------------
BitField Name: TxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a condition of pointer
increment/decrement.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgAdjThresh_Bit_Start                                                              0
#define cAf6_glbtpg_reg_TxPgAdjThresh_Bit_End                                                                3
#define cAf6_glbtpg_reg_TxPgAdjThresh_Mask                                                             cBit3_0
#define cAf6_glbtpg_reg_TxPgAdjThresh_Shift                                                                  0
#define cAf6_glbtpg_reg_TxPgAdjThresh_MaxVal                                                               0xf
#define cAf6_glbtpg_reg_TxPgAdjThresh_MinVal                                                               0x0
#define cAf6_glbtpg_reg_TxPgAdjThresh_RstVal                                                               0xc


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Interpreter Per Channel Control
Reg Addr   : 0x22000 - 0x22e2f
Reg Formula: 0x22000 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.

------------------------------------------------------------------------------*/
#define cAf6Reg_spiramctl_Base                                                                         0x22000
#define cAf6Reg_spiramctl(LineId, StsId)                                        (0x22000+512*(LineId)+(StsId))
#define cAf6Reg_spiramctl_WidthVal                                                                         128
#define cAf6Reg_spiramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: StsPiRemoteLoopBack
BitField Type: RW
BitField Desc: Enable/disable Remote Loopback. This bit will be set to 1 send to
CPU a STS-AIS alarm 1: Enable. 0: Disable.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_spiramctl_StsPiRemoteLoopBack_Mask                                                         cBit15
#define cAf6_spiramctl_StsPiRemoteLoopBack_Shift                                                            15

/*--------------------------------------
BitField Name: StsPiChkLom
BitField Type: RW
BitField Desc: Enable/disable LOM checking. This field will be set to 1 when
payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_spiramctl_StsPiChkLom_Bit_Start                                                                14
#define cAf6_spiramctl_StsPiChkLom_Bit_End                                                                  14
#define cAf6_spiramctl_StsPiChkLom_Mask                                                                 cBit14
#define cAf6_spiramctl_StsPiChkLom_Shift                                                                    14
#define cAf6_spiramctl_StsPiChkLom_MaxVal                                                                  0x1
#define cAf6_spiramctl_StsPiChkLom_MinVal                                                                  0x0
#define cAf6_spiramctl_StsPiChkLom_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_spiramctl_StsPiSSDetPatt_Bit_Start                                                             12
#define cAf6_spiramctl_StsPiSSDetPatt_Bit_End                                                               13
#define cAf6_spiramctl_StsPiSSDetPatt_Mask                                                           cBit13_12
#define cAf6_spiramctl_StsPiSSDetPatt_Shift                                                                 12
#define cAf6_spiramctl_StsPiSSDetPatt_MaxVal                                                               0x3
#define cAf6_spiramctl_StsPiSSDetPatt_MinVal                                                               0x0
#define cAf6_spiramctl_StsPiSSDetPatt_RstVal                                                               0x0

/*--------------------------------------
BitField Name: StsPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [11]
--------------------------------------*/
#define cAf6_spiramctl_StsPiAisFrc_Bit_Start                                                                11
#define cAf6_spiramctl_StsPiAisFrc_Bit_End                                                                  11
#define cAf6_spiramctl_StsPiAisFrc_Mask                                                                 cBit11
#define cAf6_spiramctl_StsPiAisFrc_Shift                                                                    11
#define cAf6_spiramctl_StsPiAisFrc_MaxVal                                                                  0x1
#define cAf6_spiramctl_StsPiAisFrc_MinVal                                                                  0x0
#define cAf6_spiramctl_StsPiAisFrc_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in STSPI state machine. 1: Enable
0: Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_spiramctl_StsPiSSDetEn_Bit_Start                                                               10
#define cAf6_spiramctl_StsPiSSDetEn_Bit_End                                                                 10
#define cAf6_spiramctl_StsPiSSDetEn_Mask                                                                cBit10
#define cAf6_spiramctl_StsPiSSDetEn_Shift                                                                   10
#define cAf6_spiramctl_StsPiSSDetEn_MaxVal                                                                 0x1
#define cAf6_spiramctl_StsPiSSDetEn_MinVal                                                                 0x0
#define cAf6_spiramctl_StsPiSSDetEn_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: StsPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_spiramctl_StsPiAdjRule_Bit_Start                                                                9
#define cAf6_spiramctl_StsPiAdjRule_Bit_End                                                                  9
#define cAf6_spiramctl_StsPiAdjRule_Mask                                                                 cBit9
#define cAf6_spiramctl_StsPiAdjRule_Shift                                                                    9
#define cAf6_spiramctl_StsPiAdjRule_MaxVal                                                                 0x1
#define cAf6_spiramctl_StsPiAdjRule_MinVal                                                                 0x0
#define cAf6_spiramctl_StsPiAdjRule_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: StsPiStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_spiramctl_StsPiStsSlvInd_Bit_Start                                                              8
#define cAf6_spiramctl_StsPiStsSlvInd_Bit_End                                                                8
#define cAf6_spiramctl_StsPiStsSlvInd_Mask                                                               cBit8
#define cAf6_spiramctl_StsPiStsSlvInd_Shift                                                                  8
#define cAf6_spiramctl_StsPiStsSlvInd_MaxVal                                                               0x1
#define cAf6_spiramctl_StsPiStsSlvInd_MinVal                                                               0x0
#define cAf6_spiramctl_StsPiStsSlvInd_RstVal                                                               0x0

/*--------------------------------------
BitField Name: StsPiStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_spiramctl_StsPiStsMstId_Bit_Start                                                               0
#define cAf6_spiramctl_StsPiStsMstId_Bit_End                                                                 5
#define cAf6_spiramctl_StsPiStsMstId_Mask                                                              cBit5_0
#define cAf6_spiramctl_StsPiStsMstId_Shift                                                                   0
#define cAf6_spiramctl_StsPiStsMstId_MaxVal                                                               0x3f
#define cAf6_spiramctl_StsPiStsMstId_MinVal                                                                0x0
#define cAf6_spiramctl_StsPiStsMstId_RstVal                                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Generator Per Channel Control
Reg Addr   : 0x23000 - 0x23e2f
Reg Formula: 0x23000 + 512*LineId + StsId
    Where  : 
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer Generator engines.
Backdoor        : itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spgramctl_Base                                                                         0x23000

/*--------------------------------------
BitField Name: StsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_spgramctl_StsPgStsSlvInd_Mask                                                              cBit16
#define cAf6_spgramctl_StsPgStsSlvInd_Shift                                                                 16

/*--------------------------------------
BitField Name: StsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_spgramctl_StsPgStsMstId_Mask                                                             cBit13_8
#define cAf6_spgramctl_StsPgStsMstId_Shift                                                                   8

/*--------------------------------------
BitField Name: StsPgB3BipErrFrc
BitField Type: RW
BitField Desc: Forcing B3 Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_spgramctl_StsPgB3BipErrFrc_Mask                                                             cBit7
#define cAf6_spgramctl_StsPgB3BipErrFrc_Shift                                                                7

/*--------------------------------------
BitField Name: StsPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_spgramctl_StsPgLopFrc_Mask                                                                  cBit6
#define cAf6_spgramctl_StsPgLopFrc_Shift                                                                     6

/*--------------------------------------
BitField Name: StsPgUeqFrc
BitField Type: RW
BitField Desc: Forcing SFM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_spgramctl_StsPgUeqFrc_Mask                                                                  cBit5
#define cAf6_spgramctl_StsPgUeqFrc_Shift                                                                     5

/*--------------------------------------
BitField Name: StsPgAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_spgramctl_StsPgAisFrc_Mask                                                                  cBit4
#define cAf6_spgramctl_StsPgAisFrc_Shift                                                                     4

/*--------------------------------------
BitField Name: StsPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_spgramctl_StsPgSSInsPatt_Mask                                                             cBit3_2
#define cAf6_spgramctl_StsPgSSInsPatt_Shift                                                                  2

/*--------------------------------------
BitField Name: StsPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_spgramctl_StsPgSSInsEn_Mask                                                                 cBit1
#define cAf6_spgramctl_StsPgSSInsEn_Shift                                                                    1

/*--------------------------------------
BitField Name: StsPgPohIns
BitField Type: RW
BitField Desc: Enable/disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_spgramctl_StsPgPohIns_Mask                                                                  cBit0
#define cAf6_spgramctl_StsPgPohIns_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx SXC Control
Reg Addr   : 0x24000 - 0x24d2f
Reg Formula: 0x24000 + 256*LineId + StsId
    Where  : 
           + $LineId(0-13)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS (to PDH) of any line (16 lines - 6 Lo and 8 Ho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines).

------------------------------------------------------------------------------*/
#define cAf6Reg_rxsxcramctl_Base                                                                       0x24000
#define cAf6Reg_rxsxcramctl(LineId, StsId)                                      (0x24000+256*(LineId)+(StsId))
#define cAf6Reg_rxsxcramctl_WidthVal                                                                       128
#define cAf6Reg_rxsxcramctl_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: RxSxcLineId
BitField Type: RW
BitField Desc: Contains the ingoing LineID (0-7) or used to Configure loop back
to PDH (If value LineID is 15). Disconnect (output is all one) if value LineID
is other value (recommend is 14).
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_rxsxcramctl_RxSxcLineId_Bit_Start                                                               8
#define cAf6_rxsxcramctl_RxSxcLineId_Bit_End                                                                11
#define cAf6_rxsxcramctl_RxSxcLineId_Mask                                                             cBit11_8
#define cAf6_rxsxcramctl_RxSxcLineId_Shift                                                                   8
#define cAf6_rxsxcramctl_RxSxcLineId_MaxVal                                                                0xf
#define cAf6_rxsxcramctl_RxSxcLineId_MinVal                                                                0x0
#define cAf6_rxsxcramctl_RxSxcLineId_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxSxcStsId
BitField Type: RW
BitField Desc: Contains the ingoing STSID (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rxsxcramctl_RxSxcStsId_Bit_Start                                                                0
#define cAf6_rxsxcramctl_RxSxcStsId_Bit_End                                                                  5
#define cAf6_rxsxcramctl_RxSxcStsId_Mask                                                               cBit5_0
#define cAf6_rxsxcramctl_RxSxcStsId_Shift                                                                    0
#define cAf6_rxsxcramctl_RxSxcStsId_MaxVal                                                                0x3f
#define cAf6_rxsxcramctl_RxSxcStsId_MinVal                                                                 0x0
#define cAf6_rxsxcramctl_RxSxcStsId_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx SXC Control
Reg Addr   : 0x25000 - 0x2572f
Reg Formula: 0x25000 + 256*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from PDH) (16 lines - 6 Lo and 8 Ho).

------------------------------------------------------------------------------*/
#define cAf6Reg_txsxcramctl_Base                                                                       0x25000
#define cAf6Reg_txsxcramctl(LineId, StsId)                                      (0x25000+256*(LineId)+(StsId))
#define cAf6Reg_txsxcramctl_WidthVal                                                                       128
#define cAf6Reg_txsxcramctl_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: TxSxcLineId
BitField Type: RW
BitField Desc: Contains the ingoing LineID (0-13: 0-5 for Lo) or used to
Configure loop back to TFI-5 (If value LineID is 15). Disconnect (output is all
one) if value LineID is other value (recommend is 14).
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_txsxcramctl_TxSxcLineId_Bit_Start                                                               8
#define cAf6_txsxcramctl_TxSxcLineId_Bit_End                                                                11
#define cAf6_txsxcramctl_TxSxcLineId_Mask                                                             cBit11_8
#define cAf6_txsxcramctl_TxSxcLineId_Shift                                                                   8
#define cAf6_txsxcramctl_TxSxcLineId_MaxVal                                                                0xf
#define cAf6_txsxcramctl_TxSxcLineId_MinVal                                                                0x0
#define cAf6_txsxcramctl_TxSxcLineId_RstVal                                                                0x0

/*--------------------------------------
BitField Name: TxSxcStsId
BitField Type: RW
BitField Desc: Contains the ingoing STSID (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_txsxcramctl_TxSxcStsId_Bit_Start                                                                0
#define cAf6_txsxcramctl_TxSxcStsId_Bit_End                                                                  5
#define cAf6_txsxcramctl_TxSxcStsId_Mask                                                               cBit5_0
#define cAf6_txsxcramctl_TxSxcStsId_Shift                                                                    0
#define cAf6_txsxcramctl_TxSxcStsId_MaxVal                                                                0x3f
#define cAf6_txsxcramctl_TxSxcStsId_MinVal                                                                 0x0
#define cAf6_txsxcramctl_TxSxcStsId_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN RXPP Per STS payload Control
Reg Addr   : 0x40000 - 0x5402f
Reg Formula: 0x40000 + 16384*LineId + StsId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure VT payload mode per STS.

------------------------------------------------------------------------------*/
#define cAf6Reg_demramctl_Base                                                                         0x40000
#define cAf6Reg_demramctl(LineId, StsId)                                      (0x40000+16384*(LineId)+(StsId))
#define cAf6Reg_demramctl_WidthVal                                                                         128
#define cAf6Reg_demramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: PiDemAisDownst
BitField Type: RW
BitField Desc: Enable/disable AIS downstream to Lo Bus. (Used for Remote
loopback function) 1: Enable. 0: Disable.
BitField Bits: [17]
--------------------------------------*/
#define cAf6_demramctl_PiDemAisDownst_Mask                                                              cBit17
#define cAf6_demramctl_PiDemAisDownst_Shift                                                                 17

/*--------------------------------------
BitField Name: PiDemStsTerm
BitField Type: RW
BitField Desc: Enable to terminate the related STS/VC. It means that STS POH
defects related to the STS/VC to generate AIS to downstream. 1: Enable 0:
Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_demramctl_PiDemStsTerm_Bit_Start                                                               16
#define cAf6_demramctl_PiDemStsTerm_Bit_End                                                                 16
#define cAf6_demramctl_PiDemStsTerm_Mask                                                                cBit16
#define cAf6_demramctl_PiDemStsTerm_Shift                                                                   16
#define cAf6_demramctl_PiDemStsTerm_MaxVal                                                                 0x1
#define cAf6_demramctl_PiDemStsTerm_MinVal                                                                 0x0
#define cAf6_demramctl_PiDemStsTerm_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: PiDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_demramctl_PiDemSpeType_Bit_Start                                                               14
#define cAf6_demramctl_PiDemSpeType_Bit_End                                                                 15
#define cAf6_demramctl_PiDemSpeType_Mask                                                             cBit15_14
#define cAf6_demramctl_PiDemSpeType_Shift                                                                   14
#define cAf6_demramctl_PiDemSpeType_MaxVal                                                                 0x3
#define cAf6_demramctl_PiDemSpeType_MinVal                                                                 0x0
#define cAf6_demramctl_PiDemSpeType_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: PiDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug26Type_Bit_Start                                                             12
#define cAf6_demramctl_PiDemTug26Type_Bit_End                                                               13
#define cAf6_demramctl_PiDemTug26Type_Mask                                                           cBit13_12
#define cAf6_demramctl_PiDemTug26Type_Shift                                                                 12
#define cAf6_demramctl_PiDemTug26Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug26Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug26Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug25Type_Bit_Start                                                             10
#define cAf6_demramctl_PiDemTug25Type_Bit_End                                                               11
#define cAf6_demramctl_PiDemTug25Type_Mask                                                           cBit11_10
#define cAf6_demramctl_PiDemTug25Type_Shift                                                                 10
#define cAf6_demramctl_PiDemTug25Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug25Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug25Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug24Type_Bit_Start                                                              8
#define cAf6_demramctl_PiDemTug24Type_Bit_End                                                                9
#define cAf6_demramctl_PiDemTug24Type_Mask                                                             cBit9_8
#define cAf6_demramctl_PiDemTug24Type_Shift                                                                  8
#define cAf6_demramctl_PiDemTug24Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug24Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug24Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug23Type_Bit_Start                                                              6
#define cAf6_demramctl_PiDemTug23Type_Bit_End                                                                7
#define cAf6_demramctl_PiDemTug23Type_Mask                                                             cBit7_6
#define cAf6_demramctl_PiDemTug23Type_Shift                                                                  6
#define cAf6_demramctl_PiDemTug23Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug23Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug23Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug22Type_Bit_Start                                                              4
#define cAf6_demramctl_PiDemTug22Type_Bit_End                                                                5
#define cAf6_demramctl_PiDemTug22Type_Mask                                                             cBit5_4
#define cAf6_demramctl_PiDemTug22Type_Shift                                                                  4
#define cAf6_demramctl_PiDemTug22Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug22Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug22Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug21Type_Bit_Start                                                              2
#define cAf6_demramctl_PiDemTug21Type_Bit_End                                                                3
#define cAf6_demramctl_PiDemTug21Type_Mask                                                             cBit3_2
#define cAf6_demramctl_PiDemTug21Type_Shift                                                                  2
#define cAf6_demramctl_PiDemTug21Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug21Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug21Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug20Type_Bit_Start                                                              0
#define cAf6_demramctl_PiDemTug20Type_Bit_End                                                                1
#define cAf6_demramctl_PiDemTug20Type_Mask                                                             cBit1_0
#define cAf6_demramctl_PiDemTug20Type_Shift                                                                  0
#define cAf6_demramctl_PiDemTug20Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug20Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug20Type_RstVal                                                               0x0

/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Interpreter Per Channel Control
Reg Addr   : 0x40800 - 0x54fff
Reg Formula: 0x40800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer interpreter engines.

------------------------------------------------------------------------------*/
#define cAf6Reg_vpiramctl_Base                                                                         0x40800
#define cAf6Reg_vpiramctl(LineId, StsId, VtgId, VtId)                 (0x40800+16384*(LineId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_vpiramctl_WidthVal                                                                         128
#define cAf6Reg_vpiramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: VtPiRemoteLoopBack
BitField Type: RW
BitField Desc: Enable/disable Remote Loopback. This bit will be set to 1 send to
CPU a VT-AIS alarm 1: Enable. 0: Disable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiRemoteLoopBack_Mask                                                           cBit6
#define cAf6_vpiramctl_VtPiRemoteLoopBack_Shift                                                              6

/*--------------------------------------
BitField Name: VtPiLoTerm
BitField Type: RW
BitField Desc: Enable to terminate the related VTTU. It means that VTTU POH
defects related to the VTTU to generate AIS to downstream.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiLoTerm_Bit_Start                                                                  5
#define cAf6_vpiramctl_VtPiLoTerm_Bit_End                                                                    5
#define cAf6_vpiramctl_VtPiLoTerm_Mask                                                                   cBit5
#define cAf6_vpiramctl_VtPiLoTerm_Shift                                                                      5
#define cAf6_vpiramctl_VtPiLoTerm_MaxVal                                                                   0x1
#define cAf6_vpiramctl_VtPiLoTerm_MinVal                                                                   0x0
#define cAf6_vpiramctl_VtPiLoTerm_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiAisFrc_Bit_Start                                                                  4
#define cAf6_vpiramctl_VtPiAisFrc_Bit_End                                                                    4
#define cAf6_vpiramctl_VtPiAisFrc_Mask                                                                   cBit4
#define cAf6_vpiramctl_VtPiAisFrc_Shift                                                                      4
#define cAf6_vpiramctl_VtPiAisFrc_MaxVal                                                                   0x1
#define cAf6_vpiramctl_VtPiAisFrc_MinVal                                                                   0x0
#define cAf6_vpiramctl_VtPiAisFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiSSDetPatt_Bit_Start                                                               2
#define cAf6_vpiramctl_VtPiSSDetPatt_Bit_End                                                                 3
#define cAf6_vpiramctl_VtPiSSDetPatt_Mask                                                              cBit3_2
#define cAf6_vpiramctl_VtPiSSDetPatt_Shift                                                                   2
#define cAf6_vpiramctl_VtPiSSDetPatt_MaxVal                                                                0x3
#define cAf6_vpiramctl_VtPiSSDetPatt_MinVal                                                                0x0
#define cAf6_vpiramctl_VtPiSSDetPatt_RstVal                                                                0x0

/*--------------------------------------
BitField Name: VtPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in PI State Machine. 1: Enable 0:
Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiSSDetEn_Bit_Start                                                                 1
#define cAf6_vpiramctl_VtPiSSDetEn_Bit_End                                                                   1
#define cAf6_vpiramctl_VtPiSSDetEn_Mask                                                                  cBit1
#define cAf6_vpiramctl_VtPiSSDetEn_Shift                                                                     1
#define cAf6_vpiramctl_VtPiSSDetEn_MaxVal                                                                  0x1
#define cAf6_vpiramctl_VtPiSSDetEn_MinVal                                                                  0x0
#define cAf6_vpiramctl_VtPiSSDetEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: VtPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiAdjRule_Bit_Start                                                                 0
#define cAf6_vpiramctl_VtPiAdjRule_Bit_End                                                                   0
#define cAf6_vpiramctl_VtPiAdjRule_Mask                                                                  cBit0
#define cAf6_vpiramctl_VtPiAdjRule_Shift                                                                     0
#define cAf6_vpiramctl_VtPiAdjRule_MaxVal                                                                  0x1
#define cAf6_vpiramctl_VtPiAdjRule_MinVal                                                                  0x0
#define cAf6_vpiramctl_VtPiAdjRule_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TXPP Per STS Multiplexing Control
Reg Addr   : 0x60000 - 0x7402f
Reg Formula: 0x60000 + 16384*LineId + StsId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure VT payload mode per STS at Tx pointer generator.

------------------------------------------------------------------------------*/
#define cAf6Reg_pgdemramctl_Base                                                                       0x60000
#define cAf6Reg_pgdemramctl(LineId, StsId)                                    (0x60000+16384*(LineId)+(StsId))
#define cAf6Reg_pgdemramctl_WidthVal                                                                       128
#define cAf6Reg_pgdemramctl_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: PgDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemSpeType_Bit_Start                                                             14
#define cAf6_pgdemramctl_PgDemSpeType_Bit_End                                                               15
#define cAf6_pgdemramctl_PgDemSpeType_Mask                                                           cBit15_14
#define cAf6_pgdemramctl_PgDemSpeType_Shift                                                                 14
#define cAf6_pgdemramctl_PgDemSpeType_MaxVal                                                               0x3
#define cAf6_pgdemramctl_PgDemSpeType_MinVal                                                               0x0
#define cAf6_pgdemramctl_PgDemSpeType_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PgDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug26Type_Bit_Start                                                           12
#define cAf6_pgdemramctl_PgDemTug26Type_Bit_End                                                             13
#define cAf6_pgdemramctl_PgDemTug26Type_Mask                                                         cBit13_12
#define cAf6_pgdemramctl_PgDemTug26Type_Shift                                                               12
#define cAf6_pgdemramctl_PgDemTug26Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug26Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug26Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug25Type_Bit_Start                                                           10
#define cAf6_pgdemramctl_PgDemTug25Type_Bit_End                                                             11
#define cAf6_pgdemramctl_PgDemTug25Type_Mask                                                         cBit11_10
#define cAf6_pgdemramctl_PgDemTug25Type_Shift                                                               10
#define cAf6_pgdemramctl_PgDemTug25Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug25Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug25Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug24Type_Bit_Start                                                            8
#define cAf6_pgdemramctl_PgDemTug24Type_Bit_End                                                              9
#define cAf6_pgdemramctl_PgDemTug24Type_Mask                                                           cBit9_8
#define cAf6_pgdemramctl_PgDemTug24Type_Shift                                                                8
#define cAf6_pgdemramctl_PgDemTug24Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug24Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug24Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug23Type_Bit_Start                                                            6
#define cAf6_pgdemramctl_PgDemTug23Type_Bit_End                                                              7
#define cAf6_pgdemramctl_PgDemTug23Type_Mask                                                           cBit7_6
#define cAf6_pgdemramctl_PgDemTug23Type_Shift                                                                6
#define cAf6_pgdemramctl_PgDemTug23Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug23Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug23Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug22Type_Bit_Start                                                            4
#define cAf6_pgdemramctl_PgDemTug22Type_Bit_End                                                              5
#define cAf6_pgdemramctl_PgDemTug22Type_Mask                                                           cBit5_4
#define cAf6_pgdemramctl_PgDemTug22Type_Shift                                                                4
#define cAf6_pgdemramctl_PgDemTug22Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug22Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug22Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug21Type_Bit_Start                                                            2
#define cAf6_pgdemramctl_PgDemTug21Type_Bit_End                                                              3
#define cAf6_pgdemramctl_PgDemTug21Type_Mask                                                           cBit3_2
#define cAf6_pgdemramctl_PgDemTug21Type_Shift                                                                2
#define cAf6_pgdemramctl_PgDemTug21Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug21Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug21Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug20Type_Bit_Start                                                            0
#define cAf6_pgdemramctl_PgDemTug20Type_Bit_End                                                              1
#define cAf6_pgdemramctl_PgDemTug20Type_Mask                                                           cBit1_0
#define cAf6_pgdemramctl_PgDemTug20Type_Shift                                                                0
#define cAf6_pgdemramctl_PgDemTug20Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug20Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug20Type_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Generator Per Channel Control
Reg Addr   : 0x60800 - 0x74fff
Reg Formula: 0x60800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-3)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer Generator engines.
Backdoor        : itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpgramctl_Base                                                                         0x60800

/*--------------------------------------
BitField Name: VtPgBipErrFrc
BitField Type: RW
BitField Desc: Forcing Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgBipErrFrc_Mask                                                                cBit7
#define cAf6_vpgramctl_VtPgBipErrFrc_Shift                                                                   7

/*--------------------------------------
BitField Name: VtPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgLopFrc_Mask                                                                   cBit6
#define cAf6_vpgramctl_VtPgLopFrc_Shift                                                                      6

/*--------------------------------------
BitField Name: VtPgUeqFrc
BitField Type: RW
BitField Desc: Forcing SFM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgUeqFrc_Mask                                                                   cBit5
#define cAf6_vpgramctl_VtPgUeqFrc_Shift                                                                      5

/*--------------------------------------
BitField Name: VtPgAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgAisFrc_Mask                                                                   cBit4
#define cAf6_vpgramctl_VtPgAisFrc_Shift                                                                      4

/*--------------------------------------
BitField Name: VtPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to Pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsPatt_Mask                                                              cBit3_2
#define cAf6_vpgramctl_VtPgSSInsPatt_Shift                                                                   2

/*--------------------------------------
BitField Name: VtPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsEn_Mask                                                                  cBit1
#define cAf6_vpgramctl_VtPgSSInsEn_Shift                                                                     1

/*--------------------------------------
BitField Name: VtPgPohIns
BitField Type: RW
BitField Desc: Enable/ disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgPohIns_Mask                                                                   cBit0
#define cAf6_vpgramctl_VtPgPohIns_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Status
Reg Addr   : 0x20000 - 0x20e00
Reg Formula: 0x20000 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer status

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmsta_Base                                                                          0x20000
#define cAf6Reg_rxfrmsta(LineId)                                                        (0x20000+512*(LineId))
#define cAf6Reg_rxfrmsta_WidthVal                                                                          128
#define cAf6Reg_rxfrmsta_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: StaFFCnvFul
BitField Type: RO
BitField Desc: Status fifo convert clock domain is full
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxfrmsta_StaFFCnvFul_Bit_Start                                                                  2
#define cAf6_rxfrmsta_StaFFCnvFul_Bit_End                                                                    2
#define cAf6_rxfrmsta_StaFFCnvFul_Mask                                                                   cBit2
#define cAf6_rxfrmsta_StaFFCnvFul_Shift                                                                      2
#define cAf6_rxfrmsta_StaFFCnvFul_MaxVal                                                                   0x1
#define cAf6_rxfrmsta_StaFFCnvFul_MinVal                                                                   0x0
#define cAf6_rxfrmsta_StaFFCnvFul_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: StaLos
BitField Type: RO
BitField Desc: Loss of Signal status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxfrmsta_StaLos_Bit_Start                                                                       1
#define cAf6_rxfrmsta_StaLos_Bit_End                                                                         1
#define cAf6_rxfrmsta_StaLos_Mask                                                                        cBit1
#define cAf6_rxfrmsta_StaLos_Shift                                                                           1
#define cAf6_rxfrmsta_StaLos_MaxVal                                                                        0x1
#define cAf6_rxfrmsta_StaLos_MinVal                                                                        0x0
#define cAf6_rxfrmsta_StaLos_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: StaOof
BitField Type: RO
BitField Desc: Out of Frame that is detected at RxFramer
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxfrmsta_StaOof_Bit_Start                                                                       0
#define cAf6_rxfrmsta_StaOof_Bit_End                                                                         0
#define cAf6_rxfrmsta_StaOof_Mask                                                                        cBit0
#define cAf6_rxfrmsta_StaOof_Shift                                                                           0
#define cAf6_rxfrmsta_StaOof_MaxVal                                                                        0x1
#define cAf6_rxfrmsta_StaOof_MinVal                                                                        0x0
#define cAf6_rxfrmsta_StaOof_RstVal                                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Sticky
Reg Addr   : 0x20001 - 0x20e01
Reg Formula: 0x20001 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmstk_Base                                                                          0x20001
#define cAf6Reg_rxfrmstk(LineId)                                                        (0x20001+512*(LineId))
#define cAf6Reg_rxfrmstk_WidthVal                                                                          128
#define cAf6Reg_rxfrmstk_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: StkFFCnvFul
BitField Type: W1C
BitField Desc: Sticky fifo convert clock domain is full
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxfrmstk_StkFFCnvFul_Bit_Start                                                                  2
#define cAf6_rxfrmstk_StkFFCnvFul_Bit_End                                                                    2
#define cAf6_rxfrmstk_StkFFCnvFul_Mask                                                                   cBit2
#define cAf6_rxfrmstk_StkFFCnvFul_Shift                                                                      2
#define cAf6_rxfrmstk_StkFFCnvFul_MaxVal                                                                   0x1
#define cAf6_rxfrmstk_StkFFCnvFul_MinVal                                                                   0x0
#define cAf6_rxfrmstk_StkFFCnvFul_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: StkLos
BitField Type: W1C
BitField Desc: Loss of Signal status sticky
BitField Bits: [1]
--------------------------------------*/
#define cAf6_rxfrmstk_StkLos_Bit_Start                                                                       1
#define cAf6_rxfrmstk_StkLos_Bit_End                                                                         1
#define cAf6_rxfrmstk_StkLos_Mask                                                                        cBit1
#define cAf6_rxfrmstk_StkLos_Shift                                                                           1
#define cAf6_rxfrmstk_StkLos_MaxVal                                                                        0x1
#define cAf6_rxfrmstk_StkLos_MinVal                                                                        0x0
#define cAf6_rxfrmstk_StkLos_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: StkOof
BitField Type: W1C
BitField Desc: Out of Frame sticky
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxfrmstk_StkOof_Bit_Start                                                                       0
#define cAf6_rxfrmstk_StkOof_Bit_End                                                                         0
#define cAf6_rxfrmstk_StkOof_Mask                                                                        cBit0
#define cAf6_rxfrmstk_StkOof_Shift                                                                           0
#define cAf6_rxfrmstk_StkOof_MaxVal                                                                        0x1
#define cAf6_rxfrmstk_StkOof_MinVal                                                                        0x0
#define cAf6_rxfrmstk_StkOof_RstVal                                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read only
Reg Addr   : 0x20002 - 0x20e02
Reg Formula: 0x20002 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer B1 error counter read only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmb1cntro_Base                                                                      0x20002
#define cAf6Reg_rxfrmb1cntro(LineId)                                                    (0x20002+512*(LineId))
#define cAf6Reg_rxfrmb1cntro_WidthVal                                                                      128
#define cAf6Reg_rxfrmb1cntro_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: RxFrmB1ErrRo
BitField Type: RO
BitField Desc: Number of B1 error - read only
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Bit_Start                                                             0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Bit_End                                                              31
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Mask                                                           cBit31_0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_Shift                                                                 0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_MaxVal                                                       0xffffffff
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_MinVal                                                              0x0
#define cAf6_rxfrmb1cntro_RxFrmB1ErrRo_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read to clear
Reg Addr   : 0x20003 - 0x20e03
Reg Formula: 0x20003 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer B1 error counter read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrmb1cntr2c_Base                                                                     0x20003
#define cAf6Reg_rxfrmb1cntr2c(LineId)                                                   (0x20003+512*(LineId))
#define cAf6Reg_rxfrmb1cntr2c_WidthVal                                                                     128
#define cAf6Reg_rxfrmb1cntr2c_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxFrmB1ErrR2C
BitField Type: RC
BitField Desc: Number of B1 error - read to clear
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Bit_Start                                                           0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Bit_End                                                            31
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Mask                                                         cBit31_0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_Shift                                                               0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_MaxVal                                                     0xffffffff
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_MinVal                                                            0x0
#define cAf6_rxfrmb1cntr2c_RxFrmB1ErrR2C_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Interrupt Enable Control
Reg Addr   : 0x22100 - 0x22f2f
Reg Formula: 0x22100 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt enable of STS/VC pointer interpreter.Each register is used to %%
store 3 bits to enable interrupts when the related alarms in related STS/VC happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschcfgram_Base                                                                     0x22100
#define cAf6Reg_upstschcfgram(LineId, StsId)                                    (0x22100+512*(LineId)+(StsId))
#define cAf6Reg_upstschcfgram_WidthVal                                                                     128
#define cAf6Reg_upstschcfgram_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: StsPiStsCepUneqPStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change Un-equip state event from Un-equip to no
Un-equip and vice versa in the related STS/VC to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_Bit_Start                                          2
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_Bit_End                                            2
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_Mask                                           cBit2
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_Shift                                              2
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_MaxVal                                           0x1
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_MinVal                                           0x0
#define cAf6_upstschcfgram_StsPiStsCepUneqPStateChgIntrEn_RstVal                                           0x0

/*--------------------------------------
BitField Name: StsPiStsAisStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS state event from AIS to no AIS and
vice versa in the related STS/VC to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_Bit_Start                                               1
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_Bit_End                                                 1
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_Mask                                                cBit1
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_Shift                                                   1
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_MaxVal                                                0x1
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_MinVal                                                0x0
#define cAf6_upstschcfgram_StsPiStsAisStateChgIntrEn_RstVal                                                0x0

/*--------------------------------------
BitField Name: StsPiStsLopStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOP state event from LOP to no LOP and
vice versa in the related STS/VC to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_Bit_Start                                               0
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_Bit_End                                                 0
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_Mask                                                cBit0
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_Shift                                                   0
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_MaxVal                                                0x1
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_MinVal                                                0x0
#define cAf6_upstschcfgram_StsPiStsLopStateChgIntrEn_RstVal                                                0x0




/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Interrupt Status
Reg Addr   : 0x22140 - 0x22f6f
Reg Formula: 0x22140 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%
Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstkram_Base                                                                     0x22140
#define cAf6Reg_upstschstkram(LineId, StsId)                                    (0x22140+512*(LineId)+(StsId))
#define cAf6Reg_upstschstkram_WidthVal                                                                     128
#define cAf6Reg_upstschstkram_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: StsPiStsConcDetIntr
BitField Type: RW
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsConcDetIntr_Bit_Start                                                     5
#define cAf6_upstschstkram_StsPiStsConcDetIntr_Bit_End                                                       5
#define cAf6_upstschstkram_StsPiStsConcDetIntr_Mask                                                      cBit5
#define cAf6_upstschstkram_StsPiStsConcDetIntr_Shift                                                         5
#define cAf6_upstschstkram_StsPiStsConcDetIntr_MaxVal                                                      0x1
#define cAf6_upstschstkram_StsPiStsConcDetIntr_MinVal                                                      0x0
#define cAf6_upstschstkram_StsPiStsConcDetIntr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: StsPiStsNewDetIntr
BitField Type: RW
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Bit_Start                                                      4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Bit_End                                                        4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Mask                                                       cBit4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Shift                                                          4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_MaxVal                                                       0x1
#define cAf6_upstschstkram_StsPiStsNewDetIntr_MinVal                                                       0x0
#define cAf6_upstschstkram_StsPiStsNewDetIntr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: StsPiStsNdfIntr
BitField Type: RW
BitField Desc: Set to 1 while an NDF event is detected at STS/VC pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsNdfIntr_Bit_Start                                                         3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Bit_End                                                           3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Mask                                                          cBit3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Shift                                                             3
#define cAf6_upstschstkram_StsPiStsNdfIntr_MaxVal                                                          0x1
#define cAf6_upstschstkram_StsPiStsNdfIntr_MinVal                                                          0x0
#define cAf6_upstschstkram_StsPiStsNdfIntr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: StsPiCepUneqStatePChgIntr
BitField Type: RW
BitField Desc: Set to 1  while there is change in CEP Un-equip Path in the
related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm
Current Status register of the related STS/VC to know the STS/VC whether in CEP
Un-equip Path state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Bit_Start                                               2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Bit_End                                                 2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Mask                                                cBit2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Shift                                                   2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_MaxVal                                                0x1
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_MinVal                                                0x0
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_RstVal                                                0x0

/*--------------------------------------
BitField Name: StsPiStsAISStateChgIntr
BitField Type: RW
BitField Desc: Set to 1 while there is change in AIS state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in AIS state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Bit_Start                                                 1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Bit_End                                                   1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Mask                                                  cBit1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Shift                                                     1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_MaxVal                                                  0x1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_MinVal                                                  0x0
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: StsPiStsLopStateChgIntr
BitField Type: RW
BitField Desc: Set 1 to while there is change in LOP state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in LOP state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Bit_Start                                                 0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Bit_End                                                   0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Mask                                                  cBit0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Shift                                                     0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_MaxVal                                                  0x1
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_MinVal                                                  0x0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Current Status
Reg Addr   : 0x22180 - 0x22faf
Reg Formula: 0x22180 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm current status of STS/VC pointer interpreter.  %%
Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstaram_Base                                                                     0x22180
#define cAf6Reg_upstschstaram(LineId, StsId)                                    (0x22180+512*(LineId)+(StsId))
#define cAf6Reg_upstschstaram_WidthVal                                                                     128
#define cAf6Reg_upstschstaram_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: StsPiStsCepUneqPCurStatus
BitField Type: RW
BitField Desc: CEP Un-eqip Path current status in the related STS/VC. When it
changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the
OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Bit_Start                                               2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Bit_End                                                 2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask                                                cBit2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Shift                                                   2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_MaxVal                                                0x1
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_MinVal                                                0x0
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_RstVal                                                0x0

/*--------------------------------------
BitField Name: StsPiStsAisCurStatus
BitField Type: RW
BitField Desc: AIS current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per
Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Bit_Start                                                    1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Bit_End                                                      1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Mask                                                     cBit1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Shift                                                        1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_MaxVal                                                     0x1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_MinVal                                                     0x0
#define cAf6_upstschstaram_StsPiStsAisCurStatus_RstVal                                                     0x0

/*--------------------------------------
BitField Name: StsPiStsLopCurStatus
BitField Type: RW
BitField Desc: LOP current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per
Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Bit_Start                                                    0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Bit_End                                                      0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Mask                                                     cBit0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Shift                                                        0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_MaxVal                                                     0x1
#define cAf6_upstschstaram_StsPiStsLopCurStatus_MinVal                                                     0x0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC Per STS/VC Interrupt Enable Control
Reg Addr   : 0x221fe - 0x22ffe
Reg Formula: 0x221fe + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
The register consists of 48 bits for 48 STS/VCs in the related STS-48 slice to enable interrupts when alarms in related STS/VC happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_intenperstsreg_Base                                                                    0x221fe
#define cAf6Reg_intenperstsreg(LineId)                                                  (0x221fe+512*(LineId))
#define cAf6Reg_intenperstsreg_WidthVal                                                                    128
#define cAf6Reg_intenperstsreg_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: OCNRxStsStsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt. Bit
0 for STS-1/VC-3/TUG-3 #0, respectively.
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_intenperstsreg_OCNRxStsStsIntrEn_Bit_Start                                                      0
#define cAf6_intenperstsreg_OCNRxStsStsIntrEn_Bit_End                                                       47
#define cAf6_intenperstsreg_OCNRxStsStsIntrEn_Mask_01                                                 cBit31_0
#define cAf6_intenperstsreg_OCNRxStsStsIntrEn_Shift_01                                                       0
#define cAf6_intenperstsreg_OCNRxStsStsIntrEn_Mask_02                                                 cBit15_0
#define cAf6_intenperstsreg_OCNRxStsStsIntrEn_Shift_02                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC Per STS/VC Interrupt OR Status
Reg Addr   : 0x221ff - 0x22fff
Reg Formula: 0x221ff + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
The register consists of 48 bits for 48 STS/VC of the related STS-48 slice in the STS/VC Pointer interpreter block.  %%
Each bit is used to store Interrupt OR status of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upintperstsreg_Base                                                                    0x221ff
#define cAf6Reg_upintperstsreg(LineId)                                                  (0x221ff+512*(LineId))
#define cAf6Reg_upintperstsreg_WidthVal                                                                    128
#define cAf6Reg_upintperstsreg_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: OCNRxStsStsIntr
BitField Type: RO
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC to
be set and they are enabled to raise interrupt Bit 0 for STS-1/VC-3/TUG-3 #0,
respectively.
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_upintperstsreg_OCNRxStsStsIntr_Bit_Start                                                        0
#define cAf6_upintperstsreg_OCNRxStsStsIntr_Bit_End                                                         47
#define cAf6_upintperstsreg_OCNRxStsStsIntr_Mask_01                                                   cBit31_0
#define cAf6_upintperstsreg_OCNRxStsStsIntr_Shift_01                                                         0
#define cAf6_upintperstsreg_OCNRxStsStsIntr_Mask_02                                                   cBit15_0
#define cAf6_upintperstsreg_OCNRxStsStsIntr_Shift_02                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Interrupt Enable Control
Reg Addr   : 0x42000 - 0x56fff
Reg Formula: 0x42000 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt enable of VT/TU pointer interpreter.  %%
Each register is used to store 6 bits to enable interrupts when the related alarms in related VT/TU happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchcfgram_Base                                                                      0x42000
#define cAf6Reg_upvtchcfgram(LineId, StsId, VtgId, VtId)              (0x42000+16384*(LineId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_upvtchcfgram_WidthVal                                                                      128
#define cAf6Reg_upvtchcfgram_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: VtPiStsCepUneqPStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change CEP Un-equip VT state event from Un- to no
AIS and vice versa in the related VT/TU to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_Bit_Start                                            2
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_Bit_End                                              2
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_Mask                                             cBit2
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_Shift                                                2
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_MaxVal                                             0x1
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_MinVal                                             0x0
#define cAf6_upvtchcfgram_VtPiStsCepUneqPStateChgIntrEn_RstVal                                             0x0

/*--------------------------------------
BitField Name: VtPiStsAisStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS state event from AIS to no AIS and
vice versa in the related VT/TU to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_Bit_Start                                                 1
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_Bit_End                                                   1
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_Mask                                                  cBit1
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_Shift                                                     1
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_MaxVal                                                  0x1
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_MinVal                                                  0x0
#define cAf6_upvtchcfgram_VtPiStsAisStateChgIntrEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: VtPiStsLopStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOP state event from LOP to no LOP and
vice versa in the related VT/TU to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_Bit_Start                                                 0
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_Bit_End                                                   0
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_Mask                                                  cBit0
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_Shift                                                     0
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_MaxVal                                                  0x1
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_MinVal                                                  0x0
#define cAf6_upvtchcfgram_VtPiStsLopStateChgIntrEn_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Interrupt Status
Reg Addr   : 0x42800 - 0x56fff
Reg Formula: 0x42800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstkram_Base                                                                      0x42800
#define cAf6Reg_upvtchstkram(LineId, StsId, VtgId, VtId)              (0x42800+16384*(LineId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_upvtchstkram_WidthVal                                                                      128
#define cAf6Reg_upvtchstkram_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: VtPiStsNewDetIntr
BitField Type: RW
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
VT/TU pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Bit_Start                                                        4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Bit_End                                                          4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Mask                                                         cBit4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Shift                                                            4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_MaxVal                                                         0x1
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_MinVal                                                         0x0
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: VtPiStsNdfIntr
BitField Type: RW
BitField Desc: Set to 1 while an NDF event is detected at VT/TU pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Bit_Start                                                           3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Bit_End                                                             3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Mask                                                            cBit3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Shift                                                               3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_MaxVal                                                            0x1
#define cAf6_upvtchstkram_VtPiStsNdfIntr_MinVal                                                            0x0
#define cAf6_upvtchstkram_VtPiStsNdfIntr_RstVal                                                            0x0

/*--------------------------------------
BitField Name: VtPiStsCepUneqVStateChgIntr
BitField Type: RW
BitField Desc: Set 1 to while there is change in Unequip state in the related
VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in Uneqip state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Bit_Start                                              2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Bit_End                                                2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Mask                                               cBit2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Shift                                                  2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_MaxVal                                               0x1
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_MinVal                                               0x0
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_RstVal                                               0x0

/*--------------------------------------
BitField Name: VtPiStsAISStateChgIntr
BitField Type: RW
BitField Desc: Set 1 to while there is change in AIS state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in LOP state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Bit_Start                                                   1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Bit_End                                                     1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Mask                                                    cBit1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Shift                                                       1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_MaxVal                                                    0x1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_MinVal                                                    0x0
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: VtPiStsLopStateChgIntr
BitField Type: RW
BitField Desc: Set 1 to while there is change in LOP state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in AIS state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Bit_Start                                                   0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Bit_End                                                     0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Mask                                                    cBit0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Shift                                                       0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_MaxVal                                                    0x1
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_MinVal                                                    0x0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Current Status
Reg Addr   : 0x43000 - 0x57fff
Reg Formula: 0x43000 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstaram_Base                                                                      0x43000
#define cAf6Reg_upvtchstaram(LineId, StsId, VtgId, VtId)              (0x43000+16384*(LineId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_upvtchstaram_WidthVal                                                                      128
#define cAf6Reg_upvtchstaram_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: VtPiStsCepUneqCurStatus
BitField Type: RW
BitField Desc: Unequip current status in the related VT/TU. When it changes for
0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU
per Alarm Interrupt Status register of the related VT/TU is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Bit_Start                                                  2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Bit_End                                                    2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask                                                   cBit2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Shift                                                      2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_MaxVal                                                   0x1
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_MinVal                                                   0x0
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: VtPiStsAisCurStatus
BitField Type: RW
BitField Desc: AIS current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Bit_Start                                                      1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Bit_End                                                        1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Mask                                                       cBit1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Shift                                                          1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_MaxVal                                                       0x1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_MinVal                                                       0x0
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_RstVal                                                       0x0

/*--------------------------------------
BitField Name: VtPiStsLopCurStatus
BitField Type: RW
BitField Desc: LOP current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Bit_Start                                                      0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Bit_End                                                        0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Mask                                                       cBit0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Shift                                                          0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_MaxVal                                                       0x1
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_MinVal                                                       0x0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU Per VT/TU Interrupt OR Status
Reg Addr   : 0x43800 - 0x5782f
Reg Formula: 0x43800 + 16384*LineId + StsId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47)
Reg Desc   : 
The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the VT/TU Pointer interpreter block. Each bit is used to store Interrupt OR status of the related VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upintpervtram_Base                                                                     0x43800
#define cAf6Reg_upintpervtram(LineId, StsId)                                  (0x43800+16384*(LineId)+(StsId))
#define cAf6Reg_upintpervtram_WidthVal                                                                     128
#define cAf6Reg_upintpervtram_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: OCNRxVtVtIntr
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU to be
set and they are enabled to raise interrupt Bit 0 for VT/TU #0, respectively.
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_upintpervtram_OCNRxVtVtIntr_Bit_Start                                                           0
#define cAf6_upintpervtram_OCNRxVtVtIntr_Bit_End                                                            27
#define cAf6_upintpervtram_OCNRxVtVtIntr_Mask                                                         cBit27_0
#define cAf6_upintpervtram_OCNRxVtVtIntr_Shift                                                               0
#define cAf6_upintpervtram_OCNRxVtVtIntr_MaxVal                                                      0xfffffff
#define cAf6_upintpervtram_OCNRxVtVtIntr_MinVal                                                            0x0
#define cAf6_upintpervtram_OCNRxVtVtIntr_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU Per STS/VC Interrupt Enable Control
Reg Addr   : 0x43ffe - 0x57ffe
Reg Formula: 0x43ffe + 16384*LineId
    Where  : 
           + $LineId(0-5)
Reg Desc   : 
The register consists of 48 bits for 48 STS/VCs in the related STS-48 slice to enable interrupts when alarms in related STS/VC happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_intenpervtreg_Base                                                                     0x43ffe
#define cAf6Reg_intenpervtreg(LineId)                                                 (0x43ffe+16384*(LineId))
#define cAf6Reg_intenpervtreg_WidthVal                                                                     128
#define cAf6Reg_intenpervtreg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: OCNRxVtStsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt. Bit
0 for STS-1/VC-3/TUG-3 #0, respectively.
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_intenpervtreg_OCNRxVtStsIntrEn_Bit_Start                                                        0
#define cAf6_intenpervtreg_OCNRxVtStsIntrEn_Bit_End                                                         47
#define cAf6_intenpervtreg_OCNRxVtStsIntrEn_Mask_01                                                   cBit31_0
#define cAf6_intenpervtreg_OCNRxVtStsIntrEn_Shift_01                                                         0
#define cAf6_intenpervtreg_OCNRxVtStsIntrEn_Mask_02                                                   cBit15_0
#define cAf6_intenpervtreg_OCNRxVtStsIntrEn_Shift_02                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU Per STS/VC Interrupt OR Status
Reg Addr   : 0x43fff - 0x57fff
Reg Formula: 0x43fff + 16384*LineId
    Where  : 
           + $LineId(0-5)
Reg Desc   : 
The register consists of 48 bits for 48 STS/VCs of the related STS-48 slice in the VT/TU Pointer interpreter block. Each bit is used to store Interrupt OR status of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upintpervtreg_Base                                                                     0x43fff
#define cAf6Reg_upintpervtreg(LineId)                                                 (0x43fff+16384*(LineId))
#define cAf6Reg_upintpervtreg_WidthVal                                                                     128
#define cAf6Reg_upintpervtreg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: OCNRxVtStsIntr
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx VT/TU Per VT/TU Interrupt OR Status register of the related STS/VC to
be set and they are enabled to raise interrupt Bit 0 for STS-1/VC-3/TUG-3 #0,
respectively.
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_upintpervtreg_OCNRxVtStsIntr_Bit_Start                                                          0
#define cAf6_upintpervtreg_OCNRxVtStsIntr_Bit_End                                                           47
#define cAf6_upintpervtreg_OCNRxVtStsIntr_Mask_01                                                     cBit31_0
#define cAf6_upintpervtreg_OCNRxVtStsIntr_Shift_01                                                           0
#define cAf6_upintpervtreg_OCNRxVtStsIntr_Mask_02                                                     cBit15_0
#define cAf6_upintpervtreg_OCNRxVtStsIntr_Shift_02                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x22080 - 0x22eaf
Reg Formula: 0x22080 + 512*LineId + 64*AdjMode + StsId
    Where  : 
           + $LineId(0-7)
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstsram_Base                                                                   0x22080
#define cAf6Reg_adjcntperstsram(LineId, AdjMode, StsId)               (0x22080+512*(LineId)+64*(AdjMode)+(StsId))
#define cAf6Reg_adjcntperstsram_WidthVal                                                                   128
#define cAf6Reg_adjcntperstsram_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [6] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Bit_Start                                                     0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Bit_End                                                      17
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Mask                                                   cBit17_0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Shift                                                         0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_MaxVal                                                  0x3ffff
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_MinVal                                                      0x0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x41000 - 0x55fff
Reg Formula: 0x41000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $AdjMode(0-1)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstkram_Base                                                                   0x41000
#define cAf6Reg_adjcntperstkram(LineId, AdjMode, StsId, VtgId, VtId)  (0x41000+16384*(LineId)+2048*(AdjMode)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_adjcntperstkram_WidthVal                                                                   128
#define cAf6Reg_adjcntperstkram_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Bit_Start                                                     0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Bit_End                                                      17
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Mask                                                   cBit17_0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Shift                                                         0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_MaxVal                                                  0x3ffff
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_MinVal                                                      0x0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS per Alarm Interrupt Status
Reg Addr   : 0x23140 - 0x23f6f
Reg Formula: 0x23140 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_stspgstkram_Base                                                                       0x23140
#define cAf6Reg_stspgstkram(LineId, StsId)                                      (0x23140+512*(LineId)+(StsId))
#define cAf6Reg_stspgstkram_WidthVal                                                                       128
#define cAf6Reg_stspgstkram_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: StsPgAisIntr
BitField Type: RW
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgAisIntr_Bit_Start                                                              2
#define cAf6_stspgstkram_StsPgAisIntr_Bit_End                                                                2
#define cAf6_stspgstkram_StsPgAisIntr_Mask                                                               cBit2
#define cAf6_stspgstkram_StsPgAisIntr_Shift                                                                  2
#define cAf6_stspgstkram_StsPgAisIntr_MaxVal                                                               0x1
#define cAf6_stspgstkram_StsPgAisIntr_MinVal                                                               0x0
#define cAf6_stspgstkram_StsPgAisIntr_RstVal                                                               0x0

/*--------------------------------------
BitField Name: StsPgFiFoOvfIntr
BitField Type: RW
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx STS
Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Bit_Start                                                          1
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Bit_End                                                            1
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Mask                                                           cBit1
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Shift                                                              1
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_MaxVal                                                           0x1
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_MinVal                                                           0x0
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: StsPgNdfIntr
BitField Type: RW
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgNdfIntr_Bit_Start                                                              0
#define cAf6_stspgstkram_StsPgNdfIntr_Bit_End                                                                0
#define cAf6_stspgstkram_StsPgNdfIntr_Mask                                                               cBit0
#define cAf6_stspgstkram_StsPgNdfIntr_Shift                                                                  0
#define cAf6_stspgstkram_StsPgNdfIntr_MaxVal                                                               0x1
#define cAf6_stspgstkram_StsPgNdfIntr_MinVal                                                               0x0
#define cAf6_stspgstkram_StsPgNdfIntr_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS pointer adjustment per channel counter
Reg Addr   : 0x23080 - 0x23eaf
Reg Formula: 0x23080 + 512*LineId + 64*AdjMode + StsId
    Where  : 
           + $LineId(0-7)
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgperstsram_Base                                                                 0x23080
#define cAf6Reg_adjcntpgperstsram(LineId, AdjMode, StsId)             (0x23080+512*(LineId)+64*(AdjMode)+(StsId))
#define cAf6Reg_adjcntpgperstsram_WidthVal                                                                 128
#define cAf6Reg_adjcntpgperstsram_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: StsPgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Bit_Start                                                       0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Bit_End                                                        17
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Mask                                                     cBit17_0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Shift                                                           0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_MaxVal                                                    0x3ffff
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_MinVal                                                        0x0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU per Alarm Interrupt Status
Reg Addr   : 0x62800 - 0x76fff
Reg Formula: 0x62800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_vtpgstkram_Base                                                                        0x62800
#define cAf6Reg_vtpgstkram(LineId, StsId, VtgId, VtId)                (0x62800+16384*(LineId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_vtpgstkram_WidthVal                                                                        128
#define cAf6Reg_vtpgstkram_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: VtPgAisIntr
BitField Type: RW
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgAisIntr_Bit_Start                                                                2
#define cAf6_vtpgstkram_VtPgAisIntr_Bit_End                                                                  2
#define cAf6_vtpgstkram_VtPgAisIntr_Mask                                                                 cBit2
#define cAf6_vtpgstkram_VtPgAisIntr_Shift                                                                    2
#define cAf6_vtpgstkram_VtPgAisIntr_MaxVal                                                                 0x1
#define cAf6_vtpgstkram_VtPgAisIntr_MinVal                                                                 0x0
#define cAf6_vtpgstkram_VtPgAisIntr_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: VtPgFiFoOvfIntr
BitField Type: RW
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx VTTU
Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Bit_Start                                                            1
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Bit_End                                                              1
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Mask                                                             cBit1
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Shift                                                                1
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_MaxVal                                                             0x1
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_MinVal                                                             0x0
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: VtPgNdfIntr
BitField Type: RW
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgNdfIntr_Bit_Start                                                                0
#define cAf6_vtpgstkram_VtPgNdfIntr_Bit_End                                                                  0
#define cAf6_vtpgstkram_VtPgNdfIntr_Mask                                                                 cBit0
#define cAf6_vtpgstkram_VtPgNdfIntr_Shift                                                                    0
#define cAf6_vtpgstkram_VtPgNdfIntr_MaxVal                                                                 0x1
#define cAf6_vtpgstkram_VtPgNdfIntr_MinVal                                                                 0x0
#define cAf6_vtpgstkram_VtPgNdfIntr_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU pointer adjustment per channel counter
Reg Addr   : 0x61000 - 0x75fff
Reg Formula: 0x61000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-5)
           + $AdjMode(0-1)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgpervtram_Base                                                                  0x61000
#define cAf6Reg_adjcntpgpervtram(LineId, AdjMode, StsId, VtgId, VtId) (0x61000+16384*(LineId)+2048*(AdjMode)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_adjcntpgpervtram_WidthVal                                                                  128
#define cAf6Reg_adjcntpgpervtram_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: VtpgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Bit_Start                                                         0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Bit_End                                                          17
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Mask                                                       cBit17_0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Shift                                                             0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_MaxVal                                                      0x3ffff
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_MinVal                                                          0x0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Bridge and Roll SXC Control
Reg Addr   : 0x26000 - 0x2672f
Reg Formula: 0x26000 + 256*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS (to Rx SPI) of any line (8 lines) can be randomly configured to //connect to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines).

------------------------------------------------------------------------------*/
#define cAf6Reg_rxbarsxcramctl_Base                                                                    0x26000
#define cAf6Reg_rxbarsxcramctl(LineId, StsId)                                   (0x26000+256*(LineId)+(StsId))
#define cAf6Reg_rxbarsxcramctl_WidthVal                                                                    128
#define cAf6Reg_rxbarsxcramctl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: RxBarSxcLineId
BitField Type: RW
BitField Desc: Contains the ingoing LineID (0-7). Disconnect (output is all one)
if value LineID is other value (recommend is 14).
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_Bit_Start                                                         8
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_Bit_End                                                          11
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_Mask                                                       cBit11_8
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_Shift                                                             8
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_MaxVal                                                          0xf
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_MinVal                                                          0x0
#define cAf6_rxbarsxcramctl_RxBarSxcLineId_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxBarSxcStsId
BitField Type: RW
BitField Desc: Contains the ingoing STSID (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_Bit_Start                                                          0
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_Bit_End                                                            5
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_Mask                                                         cBit5_0
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_Shift                                                              0
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_MaxVal                                                          0x3f
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_MinVal                                                           0x0
#define cAf6_rxbarsxcramctl_RxBarSxcStsId_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Bridge and Roll SXC Control
Reg Addr   : 0x27000 - 0x2772f
Reg Formula: 0x27000 + 256*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from Tx SPG) .

------------------------------------------------------------------------------*/
#define cAf6Reg_txbarsxcramctl_Base                                                                    0x27000
#define cAf6Reg_txbarsxcramctl(LineId, StsId)                                   (0x27000+256*(LineId)+(StsId))
#define cAf6Reg_txbarsxcramctl_WidthVal                                                                    128
#define cAf6Reg_txbarsxcramctl_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: TxBarSxcLineId
BitField Type: RW
BitField Desc: Contains the ingoing LineID (0-7). Disconnect (output is all one)
if value LineID is other value (recommend is 14).
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_txbarsxcramctl_TxBarSxcLineId_Bit_Start                                                         8
#define cAf6_txbarsxcramctl_TxBarSxcLineId_Bit_End                                                          11
#define cAf6_txbarsxcramctl_TxBarSxcLineId_Mask                                                       cBit11_8
#define cAf6_txbarsxcramctl_TxBarSxcLineId_Shift                                                             8
#define cAf6_txbarsxcramctl_TxBarSxcLineId_MaxVal                                                          0xf
#define cAf6_txbarsxcramctl_TxBarSxcLineId_MinVal                                                          0x0
#define cAf6_txbarsxcramctl_TxBarSxcLineId_RstVal                                                          0x0

/*--------------------------------------
BitField Name: TxSxcStsId
BitField Type: RW
BitField Desc: Contains the ingoing STSID (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_txbarsxcramctl_TxSxcStsId_Bit_Start                                                             0
#define cAf6_txbarsxcramctl_TxSxcStsId_Bit_End                                                               5
#define cAf6_txbarsxcramctl_TxSxcStsId_Mask                                                            cBit5_0
#define cAf6_txbarsxcramctl_TxSxcStsId_Shift                                                                 0
#define cAf6_txbarsxcramctl_TxSxcStsId_MaxVal                                                             0x3f
#define cAf6_txbarsxcramctl_TxSxcStsId_MinVal                                                              0x0
#define cAf6_txbarsxcramctl_TxSxcStsId_RstVal                                                              0x0

/*------------------------------------------------------------------------------
Reg Name   : OCN Rx High Order Map concatenate configuration
Reg Addr   : 0x28000 - 0x2872f
Reg Formula: 0x28000 + 512*LineId + StsId
    Where  :
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   :
Each register is used to configure concatenation for each STS to High Order Map path.
Backdoor        : irxpp_outho_inst.irxpp_outputho [0].ohoramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_rxhomapramctl_Base                                                                     0x28000
#define cAf6Reg_rxhomapramctl(LineId, StsId)                                    (0x28000+512*(LineId)+(StsId))
#define cAf6Reg_rxhomapramctl_WidthVal                                                                     128
#define cAf6Reg_rxhomapramctl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: HoMapAisDowns
BitField Type: RW
BitField Desc: Enable/disable AIS downstream to Ho Bus. (Used for Remote
loopback function) 1: Enable. 0: Disable.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapAisDowns_Mask                                                            cBit9
#define cAf6_rxhomapramctl_HoMapAisDowns_Shift                                                               9

/*--------------------------------------
BitField Name: HoMapStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Bit_Start                                                          8
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Bit_End                                                            8
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Mask                                                           cBit8
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Shift                                                              8
#define cAf6_rxhomapramctl_HoMapStsSlvInd_MaxVal                                                           0x1
#define cAf6_rxhomapramctl_HoMapStsSlvInd_MinVal                                                           0x0
#define cAf6_rxhomapramctl_HoMapStsSlvInd_RstVal                                                           0x0

/*--------------------------------------
BitField Name: HoMapStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapStsMstId_Bit_Start                                                           0
#define cAf6_rxhomapramctl_HoMapStsMstId_Bit_End                                                             5
#define cAf6_rxhomapramctl_HoMapStsMstId_Mask                                                          cBit5_0
#define cAf6_rxhomapramctl_HoMapStsMstId_Shift                                                               0
#define cAf6_rxhomapramctl_HoMapStsMstId_MaxVal                                                           0x3f
#define cAf6_rxhomapramctl_HoMapStsMstId_MinVal                                                            0x0
#define cAf6_rxhomapramctl_HoMapStsMstId_RstVal                                                            0x0

#endif /* _AF6_REG_AF6CCI0011_RD_OCN_H_ */

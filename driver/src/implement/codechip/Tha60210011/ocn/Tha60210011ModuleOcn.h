/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210011ModuleOcn.h
 * 
 * Created Date: Apr 29, 2015
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEOCN_H_
#define _THA60210011MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cInvalidSlice cBit7_0
#define cInvalidHwSts cBit7_0

/*--------------------------- Macros -----------------------------------------*/
#define mSliceFromCache(cachedVal) ((uint8)((cachedVal >> 8) & cBit7_0))
#define mHwStsFromCache(cachedVal) ((uint8)(cachedVal & cBit7_0))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleOcn * Tha60210011ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011ModuleOcnAisDownstreamEnable(ThaModuleOcn self, eBool enable);
eAtRet Tha60210011ModuleOcnLopDownstreamEnable(ThaModuleOcn self, eBool enable);
eAtRet Tha60210011ModuleOcnLineScrambleEnable(ThaModuleOcn self, eBool enabled);
eBool Tha60210011ModuleOcnLineScrambleIsEnabled(ThaModuleOcn self);

eAtRet Tha60210011ModuleOcnLomCheckingEnable(AtSdhChannel channel, eBool enable);
eBool Tha60210011ModuleOcnLomCheckingIsEnabled(AtSdhChannel channel);

/* For registers */
uint32 Tha60210011ModuleOcnStsDefaultOffset(AtSdhChannel channel);
uint32 Tha60210011ModuleOcnTxStsDefaultOffset(AtSdhChannel channel);
uint32 Tha60210011ModuleOcnRxStsDefaultOffset(AtSdhChannel channel);
uint32 Tha60210011ModuleOcnBaseAddress(ThaModuleOcn self);
uint32 Tha60210011ModuleOcnVtDefaultOffset(AtSdhChannel channel);
uint32 Tha60210011ModuleOcnTxVtDefaultOffset(AtSdhChannel channel);
uint32 Tha60210011ModuleOcnRxVtDefaultOffset(AtSdhChannel channel);
uint32 Tha60210011ModuleOcnLoStsDefaultOffset(AtSdhChannel channel, uint8 sts1);
uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnStsPointerGeneratorPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnTxStsMultiplexingControl(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnRxHighOrderMapConcatenationControl(Tha60210011ModuleOcn self);
uint32 Tha60210011ModuleOcnHwStsOffsetWithBaseAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts);
uint32 Tha60210011ModuleOcnStsPointerGeneratorPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnTfi5GlbtfmBase(ThaModuleOcn self);
uint32 Tha60210011ModuleOcnTfi5GlbrfmBase(ThaModuleOcn self);
uint32 Tha60210011ModuleOcnGlbspiBase(ThaModuleOcn self);
uint32 Tha60210011ModuleOcnGlbvpiBase(ThaModuleOcn self);
uint32 Tha60210011ModuleOcnGlbtpgBase(ThaModuleOcn self);
uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAlarmStatus(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnVtTuPointerGeneratorNdfIntrMask(Tha60210011ModuleOcn self);
uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel);
uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelAlarmStatus(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel);
uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel);
uint32 Tha60210011ModuleOcnStsPointerGeneratorPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel);

eAtRet Tha60210011ModuleOcnLoLineHwSts1PayloadInit(ThaModuleOcn self, AtSdhChannel channel, uint8 slice, uint8 hwStsId);
uint16 Tha60210011ModuleOcnSliceAndStsCache(uint8 slice, uint8 sts);
eAtRet Tha60210011ModuleOcnDebugConcateWithIndent(ThaModuleOcn self, uint32 numSlices, uint32 localBaseAddress, uint32 indent);

uint32 Tha60210011ModuleOcnRxSxcControlRegAddr(Tha60210011ModuleOcn self);
uint32 Tha60210011ModuleOcnTxSxcControlRegAddr(Tha60210011ModuleOcn self);

/* Low-order VC-3/VC-1x alarm force */
uint32 Tha6021OcnTuVcRxForcibleAlarms(AtChannel tuVc);
eAtRet Tha6021OcnTuVcRxAlarmForce(AtChannel tuVc, uint32 alarmType);
eAtRet Tha6021OcnTuVcRxAlarmUnForce(AtChannel tuVc, uint32 alarmType);
uint32 Tha6021OcnTuVcRxForcedAlarmGet(AtChannel tuVc);
uint32 Tha6021OcnTuVcTxForcibleAlarms(AtChannel tuVc);
eAtRet Tha6021OcnTuVcTxAlarmForce(AtChannel tuVc, uint32 alarmType);
eAtRet Tha6021OcnTuVcTxAlarmUnForce(AtChannel tuVc, uint32 alarmType);
uint32 Tha6021OcnTuVcTxForcedAlarmGet(AtChannel tuVc);

/* Low-order VC-3/VC-1x loopback */
eBool Tha6021OcnTuVcLoopbackIsSupported(AtChannel tuVc, uint8 loopbackMode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEOCN_H_ */


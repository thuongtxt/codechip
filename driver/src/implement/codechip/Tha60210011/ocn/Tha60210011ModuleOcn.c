/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60210011ModuleOcn.c
 *
 * Created Date: Apr 23, 2015
 *
 * Description : OCN module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/cdr/ThaModuleCdrInternal.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../../default/sdh/ThaSdhAuInternal.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../sdh/Tha60210011Tfi5LineAuVcInternal.h"
#include "../man/Tha60210011Device.h"
#include "../xc/Tha60210011ModuleXc.h"
#include "../ram/Tha60210011InternalRam.h"
#include "../sdh/Tha60210011Tfi5LineAuVcInternal.h"
#include "Tha60210011ModuleOcnReg.h"
#include "Tha60210011ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_demramctl_TugType_Mask(tug2)  (cBit1_0 << ((tug2) << 1))
#define cAf6_demramctl_TugType_Shift(tug2) ((tug2) << 1)

#define cAf6_glbtfm_reg_TxLineSyncSel_LineMask(lineId)   (cBit4 << (lineId))
#define cAf6_glbtfm_reg_TxLineSyncSel_LineShift(lineId)  (4 + (lineId))

#define cAf6_glbrfm_reg_RxLineSyncSel_LineMask(lineId)   (cBit16 << (lineId))
#define cAf6_glbrfm_reg_RxLineSyncSel_LineShift(lineId)  (16 + (lineId))

#define cAf6_demramctl_TugType_Mask(tug2)  (cBit1_0 << ((tug2) << 1))
#define cAf6_demramctl_TugType_Shift(tug2) ((tug2) << 1)

/* TUVc loopback at PP */
#define cAf6_vpiramctl_LineVtPiLocLb_Mask                                                                cBit6
#define cAf6_vpiramctl_LineVtPiLocLb_Shift                                                                   6
#define cAf6_vpgramctl_LineVtPiRemLb_Mask                                                                cBit8
#define cAf6_vpgramctl_LineVtPiRemLb_Shift                                                                   8

/* TUVc rx AIS force at PI */
#define cAf6_vpiramctl_LineVtPiAisFrcDown_Mask                                                           cBit7
#define cAf6_vpiramctl_LineVtPiAisFrcDown_Shift                                                              7
/* TUVc tx AIS force at PI */
#define cAf6_vpgramctl_LineVtPiAisFrcUp_Mask                                                             cBit9
#define cAf6_vpgramctl_LineVtPiAisFrcUp_Shift                                                                9

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleOcn)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleOcnMethods m_methods;

/* Override */
static tThaModuleOcnMethods m_ThaModuleOcnOverride;
static tAtModuleMethods     m_AtModuleOverride;

/* Supper implementation */
static const tAtModuleMethods * m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UseTfi5(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr  = mMethodsGet(mThis(self))->Tfi5GlbrfmBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    uint32 regValue = mModuleHwRead(self, regAddr);
    uint8 tfi5 = mMethodsGet(mThis(self))->UseTfi5(mThis(self)) ? 1 : 0;

    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmLosAisEn_, 1);
    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmOofAisEn_, 1);
    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmTfi5ModeEn_, tfi5);
    mModuleHwWrite(self, regAddr, regValue);

    return cAtOk;
    }

static eAtRet GlbPointerDefautSet(ThaModuleOcn self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->GlbtpgBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_glbtpg_reg_TxPgNorPtrThresh_, 0x3);
    mRegFieldSet(regVal, cAf6_glbtpg_reg_TxPgAdjThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_glbtpg_reg_TxPgFlowThresh_, 0x3);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 GlbSpiReg(ThaModuleOcn self)
    {
    return mMethodsGet(mThis(self))->GlbspiBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    }

static eAtRet StsPointerDefautSet(ThaModuleOcn self)
    {
    uint32 regAddr = GlbSpiReg(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgAdjThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgFlowThresh_, 0x3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiMajorMode_, 0x1);

    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiNorPtrThresh_, 0x3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiNdfPtrThresh_, 0x8);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiBadPtrThresh_, 0x8);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiPohAisType_, 0xF);

    mModuleHwWrite(self, regAddr, regVal);

    Tha60210011ModuleOcnAisDownstreamEnable(self, cAtTrue);
    Tha60210011ModuleOcnLopDownstreamEnable(self, cAtTrue);

    return cAtOk;
    }

static eAtRet VtTuPointerDefautSet(ThaModuleOcn self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->GlbvpiBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomAisPEn_, 0x1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomInvlCntMod_, 0x1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomGoodThresh_, 0xF);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomInvlThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiAisAisPEn_, 0x1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLopAisPEn_, 0x1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiBadPtrThresh_, 0x1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiMajorMode_, 0x1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiNorPtrThresh_, 0x3);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiNdfPtrThresh_, 0x8);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiBadPtrThresh_, 0x8);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiPohAisType_, 0xF);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet PointerDefautSet(ThaModuleOcn self)
    {
    GlbPointerDefautSet(self);
    StsPointerDefautSet(self);
    VtTuPointerDefautSet(self);
    return cAtOk;
    }

static eAtRet RxMasterLineSet(ThaModuleOcn self, uint8 tfi5Line)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->Tfi5GlbrfmBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 regMask = (uint32)(cAf6_glbrfm_reg_RxLineSyncSel_Mask << tfi5Line);
    uint32 regShift = (uint32)(cAf6_glbrfm_reg_RxLineSyncSel_Shift + tfi5Line);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxLineSyncSel_, 0);
    mFieldIns(&regVal, regMask, regShift, 1);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet TxMasterLineSet(ThaModuleOcn self, uint8 tfi5Line)
    {
    uint32 regAddr = Tha60210011ModuleOcnTfi5GlbtfmBase(self) + ThaModuleOcnBaseAddress(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 regMask = (uint32)(cAf6_glbtfm_reg_TxLineSyncSel_Mask << tfi5Line);
    uint32 regShift = (uint32)(cAf6_glbtfm_reg_TxLineSyncSel_Shift + tfi5Line);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxLineSyncSel_, 0);
    mFieldIns(&regVal, regMask, regShift, 1);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet MasterLineDefaultSet(ThaModuleOcn self)
    {
    eAtRet ret = cAtOk;

    ret |= RxMasterLineSet(self, 0);
    ret |= TxMasterLineSet(self, 0);

    return ret;
    }

static eBool NeedScramble(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedMasterTfi5(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret = cAtOk;

    ret |= RxDefaultSet(self);
    ret |= PointerDefautSet(self);

    if (mMethodsGet(mThis(self))->NeedMasterTfi5(mThis(self)))
        ret |= MasterLineDefaultSet(self);

    /* Let the Physical handle this job */
    ret |= Tha60210011ModuleOcnLineScrambleEnable(self, mMethodsGet(mThis(self))->NeedScramble(mThis(self)));

    return ret;
    }

static uint32 HwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (512UL * hwSlice) + hwSts;
    }

static uint32 HoStsOffsetWithBaseAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    return ThaModuleOcnBaseAddress(self) + mMethodsGet(mThis(self))->HwStsDefaultOffset(mThis(self), channel, hwSlice, hwSts);
    }

static uint32 TxHwHoStsOffsetWithBaseAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    return Tha60210011ModuleOcnBaseAddress(self) + mMethodsGet(mThis(self))->TxHwStsDefaultOffset(mThis(self), channel, hwSlice, hwSts);
    }

static uint32 StsDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 stsId)
    {
    uint8 hwSlice, hwSts;

    if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, stsId, &hwSlice, &hwSts) == cAtOk)
        return HoStsOffsetWithBaseAddress(self, sdhChannel, hwSlice, hwSts);

    return cBit31_0;
    }

static uint32 TxLoStsOffsetWithBaseAddress(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint8 hwSlice, hwSts;

    if (ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts) == cAtOk)
        return ThaModuleOcnBaseAddress((ThaModuleOcn)self) + mMethodsGet(self)->TxLoStsPayloadHwFormula(self, channel, hwSlice, hwSts);

    return cBit31_0;
    }

static uint32 LoStsOffsetWithBaseAddress(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint8 hwSlice, hwSts;

    if (ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts) == cAtOk)
        return ThaModuleOcnBaseAddress((ThaModuleOcn)self) + mMethodsGet(self)->LoStsPayloadHwFormula(self, channel, hwSlice, hwSts);

    return cBit31_0;
    }

static uint32 LoStsPayloadHwFormula(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (16384UL * hwSlice) + hwSts;
    }

static uint32 LoStsDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 sts1)
    {
    return LoStsOffsetWithBaseAddress((Tha60210011ModuleOcn)self, channel, sts1);
    }

static uint32 StsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 hwSlice, hwSts;

    if (ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts) == cAtOk)
        return ThaModuleOcnBaseAddress(self) + (16384UL * hwSlice) + (32UL * hwSts) + (4UL * vtgId) + vtId;

    return cBit31_0;
    }

static uint32 TxVtDefaultOffset(ThaModuleOcn self, AtSdhPath vtTu)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)vtTu;
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId  = AtSdhChannelTu1xGet(sdhChannel);
    
    return ThaModuleOcnTxStsVtDefaultOffset(self, sdhChannel, stsId, vtgId, vtId);
    }

static uint32 VtDefaultOffset(ThaModuleOcn self, AtSdhPath vtTu)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)vtTu;
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId  = AtSdhChannelTu1xGet(sdhChannel);

    return ThaModuleOcnStsVtDefaultOffset(self, sdhChannel, stsId, vtgId, vtId);
    }

static uint32 OcnStsPointerInterpreterPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_spiramctl_Base;
    }

static uint32 OcnRxHoMapConcatenateReg(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_rxhomapramctl_Base;
    }

static eAtRet HoLinePiStsConcatMasterSet(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, mastHwSts;

    if (!mMethodsGet(mThis(self))->ChannelCanBeTerminated(mThis(self), channel))
        return cAtOk;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &mastHwSts);

    /* HO-Line STS */
    regAddr = mMethodsGet(mThis(self))->OcnRxHoMapConcatenateReg(mThis(self)) + HoStsOffsetWithBaseAddress((ThaModuleOcn)self, channel, hwSlice, mastHwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsMstId_, mastHwSts);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsSlvInd_, 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PiStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    if (mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        {
        uint32 regAddr, regVal;
        uint8 hwSlice, mastHwSts;

        ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &mastHwSts);

        /* TFI5 STS */
        regAddr = mMethodsGet(mThis(self))->OcnStsPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
                  HoStsOffsetWithBaseAddress(self, channel, hwSlice, mastHwSts);
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
        mRegFieldSet(regVal, cAf6_spiramctl_StsPiStsMstId_, mastHwSts);
        mRegFieldSet(regVal, cAf6_spiramctl_StsPiStsSlvInd_, 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
        }

    return mMethodsGet(mThis(self))->HoLinePiStsConcatMasterSet(mThis(self), channel, masterStsId);
    }

static eAtRet HoLinePiStsConcatSlaveSet(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    uint32 regAddr, regVal;
    uint8 masterHwStsId, hwSlice, hwSts;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->ChannelCanBeTerminated(mThis(self), channel))
        return cAtOk;

    ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &masterHwStsId);
    ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, slaveStsId, &hwSlice, &hwSts);

    /* HO-Line STS */
    regAddr = mMethodsGet(mThis(self))->OcnRxHoMapConcatenateReg(mThis(self)) + HoStsOffsetWithBaseAddress((ThaModuleOcn)self, channel, hwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsSlvInd_, 1);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsMstId_, masterHwStsId);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PiStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    if (mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        {
        uint32 regAddr, regVal;
        uint8 masterHwStsId, hwSlice, hwSts;
        eAtRet ret = cAtOk;

        ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &masterHwStsId);
        ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, slaveStsId, &hwSlice, &hwSts);
        if (ret != cAtOk)
           return ret;

        /* TFI5 STS */
        regAddr = mMethodsGet(mThis(self))->OcnStsPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
                  HoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
        mRegFieldSet(regVal, cAf6_spiramctl_StsPiStsSlvInd_, 1);
        mRegFieldSet(regVal, cAf6_spiramctl_StsPiStsMstId_, masterHwStsId);
        mRegFieldSet(regVal, cAf6_spiramctl_StsPiSSDetEn_, 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
        }

    return mMethodsGet(mThis(self))->HoLinePiStsConcatSlaveSet(mThis(self), channel, masterStsId, slaveStsId);
    }

static eBool Tfi5StsConcatRelease(ThaModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    uint8 fieldVal;
    eBool isSlaver;
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtTrue;

    regAddr = mMethodsGet(mThis(self))->OcnStsPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
              HoStsOffsetWithBaseAddress(self, channel, stsHwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlaver = (regVal & cAf6_spiramctl_StsPiStsSlvInd_Mask) ? cAtTrue : cAtFalse;
    if (!isSlaver)
        return cAtFalse;

    /* Check if this is slaver of this master STS */
    fieldVal = (uint8)mRegField(regVal, cAf6_spiramctl_StsPiStsMstId_);
    if (fieldVal != masterHwSts)
        return cAtFalse;

    /* Make slaver be master of itself */
    mRegFieldSet(regVal, cAf6_spiramctl_StsPiStsSlvInd_, 0);
    mRegFieldSet(regVal, cAf6_spiramctl_StsPiStsMstId_, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtTrue;
    }

static eBool HoLinePiStsConcatRelease(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    uint8 fieldVal;
    eBool isSlaver;
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ChannelCanBeTerminated(mThis(self), channel))
        return cAtOk;

    regAddr = mMethodsGet(mThis(self))->OcnRxHoMapConcatenateReg(mThis(self)) + HoStsOffsetWithBaseAddress((ThaModuleOcn)self, channel, stsHwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlaver = (regVal & cAf6_rxhomapramctl_HoMapStsSlvInd_Mask) ? cAtTrue : cAtFalse;
    if (!isSlaver)
        return cAtFalse;

    /* Check if this is slaver of this master STS */
    fieldVal = (uint8)mRegField(regVal, cAf6_rxhomapramctl_HoMapStsMstId_);
    if (fieldVal != masterHwSts)
        return cAtFalse;

    /* Make slaver be master of itself */
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsSlvInd_, 0);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsMstId_, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtTrue;
    }

static eBool PiStsConcatRelease(ThaModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    eBool isSlave = Tfi5StsConcatRelease(self, channel, stsHwSlice, masterHwSts, hwSts);
    isSlave |= mMethodsGet(mThis(self))->HoLinePiStsConcatRelease(mThis(self), channel, stsHwSlice, masterHwSts, hwSts);
    return isSlave;
    }

static uint32 OcnStsPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_spgramctl_Base;
    }
static uint32 OcnTerStsPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cInvalidUint32;
    }
static eAtRet PgStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, mastHwSts;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtOk;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &mastHwSts);
    regAddr = mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
              TxHwHoStsOffsetWithBaseAddress(self, channel, hwSlice, mastHwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsMstId_, mastHwSts);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsSlvInd_, 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PgStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    uint32 regAddr, regVal;
    uint8 masterHwStsId, hwSlice, hwSts;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtOk;

    ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &masterHwStsId);
    ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, slaveStsId, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return ret;

    regAddr = mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
              TxHwHoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsSlvInd_, 1);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsMstId_, masterHwStsId);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PgSlaveIndicate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, hwSts;
    eAtRet ret;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtOk;

    ret = ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return ret;

    regAddr = mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
              TxHwHoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);

    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsSlvInd_, mBoolToBin(isSlave));
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool PgStsConcatRelease(ThaModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    uint8 fieldVal;
    eBool isSlaver;
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtTrue;

    regAddr = mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
              TxHwHoStsOffsetWithBaseAddress(self, channel, stsHwSlice, hwSts);

    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlaver = (regVal & cAf6_spgramctl_StsPgStsSlvInd_Mask) ? cAtTrue : cAtFalse;
    if (!isSlaver)
        return cAtFalse;

    /* Check if this is slaver of master STS */
    fieldVal = (uint8)mRegField(regVal, cAf6_spgramctl_StsPgStsMstId_);
    if (fieldVal != masterHwSts)
        return cAtFalse;

    /* It is master of itself */
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsSlvInd_, 0);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgStsMstId_, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtTrue;
    }

static eBool StsIsSlaveInChannel(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint32 regAddr, regVal;
    uint8 masterHwSlice, masterHwStsId, hwSlice, hwSts;
    eBool isSlave;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtFalse;

    /* Convert ID */
    ret |= ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &masterHwSlice, &masterHwStsId);
    ret |= ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return cAtFalse;

    /* This STS can be master */
    regAddr = mMethodsGet(mThis(self))->OcnStsPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
              HoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    isSlave = (regVal & cAf6_spiramctl_StsPiStsSlvInd_Mask) ? cAtTrue : cAtFalse;
    if (!isSlave)
        return cAtFalse;

    /* It must be slave of this master STS */
    if ( mRegField(regVal, cAf6_spiramctl_StsPiStsMstId_) == masterHwStsId)
        return cAtTrue;

    return cAtFalse;
    }

static uint8 MasterSwSts(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    return AtSdhChannelSts1Get(channel);
    }

static eBool SlaveStsIdIsValid(ThaModuleOcn self, AtSdhChannel channel, uint8 swSts)
    {
    uint8 masterSts = MasterSwSts(mThis(self), channel);
    AtUnused(self);
    if ((channel == NULL) || (swSts < masterSts) || (swSts - masterSts >= AtSdhChannelNumSts(channel)))
        return cAtFalse;

    return cAtTrue;
    }

static uint8 IndexOfSts(AtSdhChannel channel, uint8 swSts)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    uint8 masterSts = MasterSwSts(ocnModule, channel);
    return (uint8)(swSts - masterSts);
    }

static void Oc48HwStsAndSliceConvertToOc24(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 *slice, uint8 *hwSts)
    {
    AtUnused(self);
    AtUnused(channel);

    *slice = (uint8)(*slice * 2 + *hwSts % 2);
    *hwSts = (uint8)(*hwSts / 2);
    }

static eAtRet HoPathChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint8 sts_i;
    uint16 *allHwSts;

    allHwSts = AtSdhChannelAllHwSts(channel);
    if (allHwSts == NULL)
        return cAtErrorRsrcNoAvail;

    sts_i = IndexOfSts(channel, swSts);
    if (allHwSts[sts_i] == cBit15_0)
        {
        mMethodsGet(self)->StsIdSw2Hw(self, channel, cThaModuleOcn, swSts, sliceId, hwStsInSlice);
        allHwSts[sts_i] = Tha60210011ModuleOcnSliceAndStsCache(*sliceId, *hwStsInSlice);
        }

    *sliceId      = mSliceFromCache(allHwSts[sts_i]);
    *hwStsInSlice = mHwStsFromCache(allHwSts[sts_i]);

    if (mMethodsGet(mThis(self))->HoPathPhyModuleIsDividedToOc24(mThis(self), phyModule))
        mMethodsGet(mThis(self))->Oc48HwStsAndSliceConvertToOc24(mThis(self), channel, sliceId, hwStsInSlice);

    return cAtOk;
    }

static eBool HoChannelizedVcNeedAccessTfi5Sts(eAtModule phyModule, AtSdhChannel channel)
    {
    uint16 _phyModule = (uint16)phyModule;
    if ((_phyModule == cThaModulePoh) || (_phyModule == cAtModuleXc))
        return cAtTrue;

    if (((_phyModule == cAtModuleSdh) || (_phyModule == cThaModuleOcn)) &&
         (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc4))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LoPathChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint8 sts_i;
    uint16 *allHwSts;

    AtUnused(self);
    allHwSts = AtSdhChannelAllHwSts(channel);
    if (allHwSts == NULL)
        return cAtErrorRsrcNoAvail;

    sts_i = IndexOfSts(channel, swSts);
    if (allHwSts[sts_i] == cBit15_0)
        {
        mMethodsGet(self)->StsIdSw2Hw(self, channel, cThaModuleOcn, swSts, sliceId, hwStsInSlice);
        allHwSts[sts_i] = Tha60210011ModuleOcnSliceAndStsCache(*sliceId, *hwStsInSlice);
        }

    *sliceId      = mSliceFromCache(allHwSts[sts_i]);
    *hwStsInSlice = mHwStsFromCache(allHwSts[sts_i]);

    if (mMethodsGet(mThis(self))->LoPathPhyModuleIsDividedToBlockOc24(mThis(self), channel, phyModule))
        mMethodsGet(mThis(self))->Oc48HwStsAndSliceConvertToOc24(mThis(self), channel, sliceId, hwStsInSlice);

    return cAtOk;
    }

static eAtRet HoChannelizedVcStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint8 sts_i;
    uint16 *allTfi5Sts;

    if (!HoChannelizedVcNeedAccessTfi5Sts(phyModule, channel))
        return LoPathChannelStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);

    /* If need to return HW STS of TFI5 domain */
    sts_i = IndexOfSts(channel, swSts);
    allTfi5Sts = Tha60210011Tfi5LineAuVcAllTfi5Sts(channel);
    if (allTfi5Sts == NULL)
        return cAtErrorInvlParm;

    if (allTfi5Sts[sts_i] == cBit15_0)
        {
        mMethodsGet(self)->StsIdSw2Hw(self, channel, phyModule, swSts, sliceId, hwStsInSlice);
        allTfi5Sts[sts_i] = Tha60210011ModuleOcnSliceAndStsCache(*sliceId, *hwStsInSlice);
        }

    *sliceId      = mSliceFromCache(allTfi5Sts[sts_i]);
    *hwStsInSlice = mHwStsFromCache(allTfi5Sts[sts_i]);
    
    if (mMethodsGet(mThis(self))->HoPathPhyModuleIsDividedToOc24(mThis(self), phyModule))
        mMethodsGet(mThis(self))->Oc48HwStsAndSliceConvertToOc24(mThis(self), channel, sliceId, hwStsInSlice);

    return cAtOk;
    }

static eAtRet ChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    if (!SlaveStsIdIsValid(self, channel, swSts))
        return cAtErrorInvlParm;

    if (Tha60210011ModuleSdhChannelIsHoPath(channel))
        return HoPathChannelStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);

    if (AtSdhChannelIsHoVc(channel))
        return HoChannelizedVcStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);

    return LoPathChannelStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);
    }

static uint8 NumSlices(ThaModuleOcn self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 NumStsInOneSlice(ThaModuleOcn self)
    {
    AtUnused(self);
    return 48;
    }

static uint32 RxStsPayloadControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_demramctl_Base;
    }

static uint32 TxStsMultiplexingControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_pgdemramctl_Base;
    }

static eAtRet SpeTypeSet(ThaModuleOcn self, AtSdhChannel channel, uint32 speType)
    {
    uint32 regAddr, regVal;
    uint32 txOffset = TxLoStsOffsetWithBaseAddress(mThis(self), channel, AtSdhChannelSts1Get(channel));
    uint32 rxOffset = LoStsOffsetWithBaseAddress(mThis(self), channel, AtSdhChannelSts1Get(channel));

    if (!mMethodsGet(mThis(self))->ChannelCanBeTerminated(mThis(self), channel))
        return cAtOk;

    regAddr = ThaModuleOcnRxStsPayloadControl(self, channel) + rxOffset;
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_demramctl_PiDemSpeType_, speType);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    regAddr = ThaModuleOcnTxStsMultiplexingControl(self, channel) + txOffset;
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgDemSpeType_, speType);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 Tug3SpeType(eAtSdhTugMapType tug3MapType)
    {
    if (tug3MapType == cAtSdhTugMapTypeTug3Map7xTug2s)
        return cThaStsTug3ContainVt1x;

    if (tug3MapType == cAtSdhTugMapTypeTug3MapVc3)
        return cThaStsTug3ContainTu3;

    return cThaStsDontCareVtTu;
    }

static eAtRet Tug3HwPayloadTypeSet(ThaModuleOcn self, AtSdhChannel tug3, eAtSdhTugMapType tug3MapType)
    {
    return SpeTypeSet(self, tug3, Tug3SpeType(tug3MapType));
    }

static uint32 Au3VcSpeType(eAtSdhVcMapType mapType)
    {
    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        return cThaStsVc3ContainVt1x;

    return cThaStsDontCareVtTu;
    }

static eAtRet Au3VcHwPayloadTypeSet(ThaModuleOcn self, AtSdhChannel vc3, eAtSdhVcMapType mapType)
    {
    return SpeTypeSet(self, vc3, Au3VcSpeType(mapType));
    }

static eAtRet RxVtgSet(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    uint8 tug2Type = ThaModuleOcnVtTuTypeSw2Hw(vtgTypeCfg);
    uint32 regAddr = ThaModuleOcnRxStsPayloadControl(self, tug2) + LoStsOffsetWithBaseAddress(mThis(self), tug2, stsId);
    uint32 regVal  = mChannelHwRead(tug2, regAddr, cThaModuleOcn);

    mFieldIns(&regVal, cAf6_demramctl_TugType_Mask(vtgId), cAf6_demramctl_TugType_Shift(vtgId), tug2Type);
    mChannelHwWrite(tug2, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxVtgSet(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    uint8 tug2Type = ThaModuleOcnVtTuTypeSw2Hw(vtgTypeCfg);
    uint32 regAddr = ThaModuleOcnTxStsMultiplexingControl(self, tug2) + TxLoStsOffsetWithBaseAddress(mThis(self), tug2, stsId);
    uint32 regVal  = mChannelHwRead(tug2, regAddr, cThaModuleOcn);

    mFieldIns(&regVal, cAf6_demramctl_TugType_Mask(vtgId), cAf6_demramctl_TugType_Shift(vtgId), tug2Type);
    mChannelHwWrite(tug2, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PpSsTxVtSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, uint8 insSsBit, eBool insSsBitEn)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
                     ThaModuleOcnTxStsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_vpgramctl_VtPgSSInsPatt_, insSsBit);
    mRegFieldSet(regVal, cAf6_vpgramctl_VtPgSSInsEn_, insSsBitEn);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 PpSsTxVtGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
                     ThaModuleOcnTxStsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    return (uint8)mRegField(regVal, cAf6_vpgramctl_VtPgSSInsPatt_);
    }

static void VtTerminate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool terminate)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
                     ThaModuleOcnStsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_vpiramctl_VtPiLoTerm_, terminate ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    }

static void VtPohInsertEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eBool enable)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
                     ThaModuleOcnTxStsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_vpgramctl_VtPgPohIns_, enable ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    }

static eBool VtPohInsertIsEnabled(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
                     ThaModuleOcnTxStsVtDefaultOffset(self, channel, stsId, vtgId, vtId);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    return mBinToBool(mRegField(regVal, cAf6_vpgramctl_VtPgPohIns_));
    }

static eAtRet VtTuExpectedSsSet(ThaModuleOcn self, AtSdhPath vtTu, uint8 value)
    {
    uint32 address = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     VtDefaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    mFieldIns(&regVal, cAf6_vpiramctl_VtPiSSDetPatt_Mask, cAf6_vpiramctl_VtPiSSDetPatt_Shift, value);
    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 VtTuExpectedSsGet(ThaModuleOcn self, AtSdhPath vtTu)
    {
    uint32 address = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     VtDefaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    return (uint8)mRegField(regVal, cAf6_vpiramctl_VtPiSSDetPatt_);
    }

static eAtRet VtTuSsCompareEnable(ThaModuleOcn self, AtSdhPath vtTu, eBool enable)
    {
    uint32 address = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     VtDefaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_vpiramctl_VtPiSSDetEn_, mBoolToBin(enable));
    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool VtTuSsCompareIsEnabled(ThaModuleOcn self, AtSdhPath vtTu)
    {
    uint32 address = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     VtDefaultOffset(self, vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    return (regVal & cAf6_vpiramctl_VtPiSSDetEn_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 VtTuTxForcibleAlarms(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static uint32 VtTuTxForcedAlarmGet(ThaModuleOcn self, AtChannel vtTu)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerGeneratorPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     TxVtDefaultOffset(self, (AtSdhPath)vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, regAddr, cThaModuleOcn);
    uint32 forcedAlarms = 0;

    if (regVal & cAf6_vpgramctl_VtPgAisFrc_Mask)
        forcedAlarms |= cAtSdhPathAlarmAis;

    if (regVal & cAf6_vpgramctl_VtPgLopFrc_Mask)
        forcedAlarms |= cAtSdhPathAlarmLop;

    return forcedAlarms;
    }

static eAtRet VtTuHwTxAlarmForce(ThaModuleOcn self, AtChannel vtTu, uint32 alarmType, eBool force)
    {
    uint32 address = mMethodsGet(mThis(self))->OcnVtTuPointerGeneratorPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     TxVtDefaultOffset(self, (AtSdhPath)vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    if (alarmType & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_vpgramctl_VtPgAisFrc_, mBoolToBin(force));

    if (alarmType & cAtSdhPathAlarmLop)
        mRegFieldSet(regVal, cAf6_vpgramctl_VtPgLopFrc_, mBoolToBin(force));

    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 VtTuRxForcibleAlarms(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis;
    }

static uint32 VtTuRxForcedAlarmGet(ThaModuleOcn self, AtChannel vtTu)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     VtDefaultOffset(self, (AtSdhPath)vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, regAddr, cThaModuleOcn);
    uint32 forcedAlarms = 0;

    if (regVal & cAf6_vpiramctl_VtPiAisFrc_Mask)
        forcedAlarms |= cAtSdhPathAlarmAis;

    return forcedAlarms;
    }

static eAtRet VtTuHwRxAlarmForce(ThaModuleOcn self, AtChannel vtTu, uint32 alarmType, eBool force)
    {
    uint32 address = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), (AtSdhChannel)vtTu) +
                     VtDefaultOffset(self, (AtSdhPath)vtTu);
    uint32 regVal  = mChannelHwRead(vtTu, address, cThaModuleOcn);

    if (alarmType & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_vpiramctl_VtPiAisFrc_, mBoolToBin(force));
    mChannelHwWrite(vtTu, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static void Tfi5MultiRegsDisplayAtt(ThaModuleOcn self, AtSdhChannel sdhChannel, uint32 regBaseAddress, const char* regName)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;
    uint32 offset;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 slice, hwSts;
        uint32 address;

        ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, (uint8)(startSts + sts_i), &slice, &hwSts);
        offset = mMethodsGet(self)->TxTerHwHoStsOffsetWithBaseAddress(self, sdhChannel, slice, hwSts);
        address = regBaseAddress + offset;
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleOcn);
        }
    }

static void Tfi5MultiRegsDisplay(ThaModuleOcn self, AtSdhChannel sdhChannel, uint32 regBaseAddress, const char* regName, eBool isTx)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;
    uint32 offset;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 slice, hwSts;
        uint32 address;

        ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, (uint8)(startSts + sts_i), &slice, &hwSts);
        offset = isTx ? TxHwHoStsOffsetWithBaseAddress(self, sdhChannel, slice, hwSts) : HoStsOffsetWithBaseAddress(self, sdhChannel, slice, hwSts);
        address = regBaseAddress + offset;
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleOcn);
        }
    }

static void LoStsMultiRegsDisplay(ThaModuleOcn self, AtSdhChannel sdhChannel, uint32 regBaseAddress, const char* regName, eBool isTx)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 offset = isTx ? TxLoStsOffsetWithBaseAddress(mThis(self), sdhChannel, (uint8)(startSts + sts_i)) : LoStsOffsetWithBaseAddress(mThis(self), sdhChannel, (uint8)(startSts + sts_i));
        uint32 address = regBaseAddress + offset;
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleOcn);
        }
    }

static void LoVtMultiRegsDisplay(ThaModuleOcn self, AtSdhChannel sdhChannel, uint32 regBaseAddress, const char* regName, eBool isTx)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 offset = isTx ? ThaModuleOcnTxStsVtDefaultOffset(self, sdhChannel, (uint8)(startSts + sts_i), AtSdhChannelTug2Get(sdhChannel), AtSdhChannelTu1xGet(sdhChannel))
                             : ThaModuleOcnStsVtDefaultOffset(self, sdhChannel, (uint8)(startSts + sts_i), AtSdhChannelTug2Get(sdhChannel), AtSdhChannelTu1xGet(sdhChannel));
        uint32 address = regBaseAddress + offset;
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleOcn);
        }
    }

static eBool SdhChannelIsLoPath(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType type = AtSdhChannelTypeGet(sdhChannel);

    if ((type == cAtSdhChannelTypeTug3) ||
        (type == cAtSdhChannelTypeTug2) ||
        (type == cAtSdhChannelTypeTu3)  ||
        (type == cAtSdhChannelTypeTu12) ||
        (type == cAtSdhChannelTypeTu11) ||
        (type == cAtSdhChannelTypeVc12) ||
        (type == cAtSdhChannelTypeVc11))
        return cAtTrue;

    if (type != cAtSdhChannelTypeVc3)
        return cAtFalse;

    if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeTu3)
        return cAtTrue;

    return cAtFalse;
    }

static eBool ChannelIsLoPathOrChannelizedVc3(AtSdhChannel sdhChannel)
    {
    if (SdhChannelIsLoPath(sdhChannel))
        return cAtTrue;

    if (AtSdhChannelIsChannelizedVc(sdhChannel) &&
       (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc3))
        return cAtTrue;

    return cAtFalse;
    }

static void SdhChannelRegsShow(ThaModuleOcn self, AtSdhChannel sdhChannel)
    {
    Tha60210011ModuleOcn ocn = (Tha60210011ModuleOcn)self;

    if (ChannelIsLoPathOrChannelizedVc3(sdhChannel))
        {
        LoStsMultiRegsDisplay(self, sdhChannel, ThaModuleOcnRxStsPayloadControl(self, sdhChannel), "OCN RXPP Per STS payload Control", cAtFalse);
        LoStsMultiRegsDisplay(self, sdhChannel, ThaModuleOcnTxStsMultiplexingControl(self, sdhChannel), "OCN TXPP Per STS Multiplexing Control", cAtTrue);
        LoVtMultiRegsDisplay(self,  sdhChannel, Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(ocn, sdhChannel), "OCN VTTU Pointer Interpreter Per Channel Control", cAtFalse);
        LoVtMultiRegsDisplay(self,  sdhChannel, Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(ocn, sdhChannel), "OCN VTTU Pointer Generator Per Channel Control", cAtTrue);
        LoVtMultiRegsDisplay(self,  sdhChannel, Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelInterrupt(ocn, sdhChannel), "OCN Rx VT/TU per Alarm Interrupt Status", cAtFalse);
        LoVtMultiRegsDisplay(self,  sdhChannel, Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAlarmStatus(ocn, sdhChannel), "OCN Rx VT/TU per Alarm Current Status", cAtFalse);
        LoVtMultiRegsDisplay(self,  sdhChannel, Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelInterrupt(ocn, sdhChannel), "OCN TxPg VTTU per Alarm Interrupt Status", cAtTrue);
        return;
        }

    if (mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), sdhChannel))
        {
        Tfi5MultiRegsDisplay(self, sdhChannel, Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ocn, sdhChannel), "OCN STS Pointer Interpreter Per Channel Control", cAtFalse);
        Tfi5MultiRegsDisplay(self, sdhChannel, Tha60210011ModuleOcnStsPointerGeneratorPerChannelControl(ocn, sdhChannel), "OCN STS Pointer Generator Per Channel Control", cAtTrue);
        }

    if (mMethodsGet(mThis(self))->ChannelCanBeTerminated(mThis(self), sdhChannel))
        {
        if (mMethodsGet(mThis(self))->ChannelHasAttPointerProcessor(mThis(self), sdhChannel))
            Tfi5MultiRegsDisplayAtt(self, sdhChannel, mMethodsGet(ocn)->OcnTerStsPointerGeneratorPerChannelControlRegAddr(ocn, sdhChannel), "OCN Registers TxPP Configure per STS For Hi-order Multiplexing from PDH side");
        Tfi5MultiRegsDisplay(self, sdhChannel, Tha60210011ModuleOcnRxHighOrderMapConcatenationControl(ocn), "OCN Rx High Order Map concatenate configuration", cAtFalse);
        }

    if (mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), sdhChannel))
        {
        Tfi5MultiRegsDisplay(self, sdhChannel, Tha60210011ModuleOcnStsPointerInterpreterPerChannelInterrupt(ocn, sdhChannel), "OCN Rx STS/VC per Alarm Interrupt Status", cAtFalse);
        Tfi5MultiRegsDisplay(self, sdhChannel, Tha60210011ModuleOcnStsPointerInterpreterPerChannelAlarmStatus(ocn, sdhChannel), "OCN Rx STS/VC per Alarm Current Status", cAtFalse);
        Tfi5MultiRegsDisplay(self, sdhChannel, Tha60210011ModuleOcnStsPointerGeneratorPerChannelInterrupt(ocn, sdhChannel), "OCN TxPg STS per Alarm Interrupt Status", cAtTrue);
        }
    }

static void ModuleRegShow(AtModule self, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceModuleRegValueDisplay(self, address);
    }

static AtModuleSdh SdhModule(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eAtRet ShowConcateDebugInformation(Tha60210011ModuleOcn self)
    {
    uint32 numLines = AtModuleSdhNumTfi5Lines(SdhModule((AtModule)self));
    uint32 localBaseAddress = mMethodsGet(self)->OcnStsPointerInterpreterPerChannelControlRegAddr(self, NULL);

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "* TFI-5 concatenation\r\n");
    return Tha60210011ModuleOcnDebugConcateWithIndent((ThaModuleOcn)self, numLines, localBaseAddress, 1 /* indent */);
    }

static eAtRet Debug(AtModule self)
    {
    Tha60210011ModuleOcn ocn = (Tha60210011ModuleOcn)self;
    uint32 baseAddress = ThaModuleOcnBaseAddress((ThaModuleOcn)self);

    ModuleRegShow(self, "OCN Global Rx Framer Control", mMethodsGet(ocn)->Tfi5GlbrfmBase(ocn) + baseAddress);
    ModuleRegShow(self, "OCN Global Tx Framer Control", Tha60210011ModuleOcnTfi5GlbtfmBase((ThaModuleOcn)self) + baseAddress);
    ModuleRegShow(self, "OCN Global STS Pointer Interpreter Control", mMethodsGet(ocn)->GlbspiBase(ocn) + baseAddress);
    ModuleRegShow(self, "OCN Global VTTU Pointer Interpreter Control", mMethodsGet(ocn)->GlbvpiBase(ocn) + baseAddress);
    ModuleRegShow(self, "OCN Global Pointer Generator Control", mMethodsGet(ocn)->GlbtpgBase(ocn) + baseAddress);

    mMethodsGet(ocn)->ShowConcateDebugInformation(ocn);

    return cAtOk;
    }

static uint8  NumStsToReleaseConcate(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;
    AtUnused(channel);
    return mMethodsGet(ocnModule)->NumStsInOneSlice(ocnModule);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "OCN Rx Bridge and Roll SXC Control 0",
         "OCN Rx Bridge and Roll SXC Control 1",
         "OCN Rx Bridge and Roll SXC Control 2",
         "OCN Rx Bridge and Roll SXC Control 3",
         "OCN Rx Bridge and Roll SXC Control 4",
         "OCN Rx Bridge and Roll SXC Control 5",
         "OCN Rx Bridge and Roll SXC Control 6",
         "OCN Rx Bridge and Roll SXC Control 7",
         "OCN Tx Bridge and Roll SXC Control 0",
         "OCN Tx Bridge and Roll SXC Control 1",
         "OCN Tx Bridge and Roll SXC Control 2",
         "OCN Tx Bridge and Roll SXC Control 3",
         "OCN Tx Bridge and Roll SXC Control 4",
         "OCN Tx Bridge and Roll SXC Control 5",
         "OCN Tx Bridge and Roll SXC Control 6",
         "OCN Tx Bridge and Roll SXC Control 7",
         "OCN STS Pointer Interpreter Per Channel Control 0",
         "OCN STS Pointer Interpreter Per Channel Control 1",
         "OCN STS Pointer Interpreter Per Channel Control 2",
         "OCN STS Pointer Interpreter Per Channel Control 3",
         "OCN STS Pointer Interpreter Per Channel Control 4",
         "OCN STS Pointer Interpreter Per Channel Control 5",
         "OCN STS Pointer Interpreter Per Channel Control 6",
         "OCN STS Pointer Interpreter Per Channel Control 7",
         "OCN STS Pointer Generator Per Channel Control 0",
         "OCN STS Pointer Generator Per Channel Control 1",
         "OCN STS Pointer Generator Per Channel Control 2",
         "OCN STS Pointer Generator Per Channel Control 3",
         "OCN STS Pointer Generator Per Channel Control 4",
         "OCN STS Pointer Generator Per Channel Control 5",
         "OCN STS Pointer Generator Per Channel Control 6",
         "OCN STS Pointer Generator Per Channel Control 7",
         "OCN Rx SXC Control 0", /* 32 */
         "OCN Rx SXC Control 1",
         "OCN Rx SXC Control 2",
         "OCN Rx SXC Control 3",
         "OCN Rx SXC Control 4", /* 36 */
         "OCN Rx SXC Control 5",
         "OCN Rx SXC Control 6",
         "OCN Rx SXC Control 7",
         "OCN Rx SXC Control 8",
         "OCN Rx SXC Control 9",
         "OCN Rx SXC Control 10", /* 42 */
         "OCN Rx SXC Control 11",
         "OCN Rx SXC Control 12",
         "OCN Rx SXC Control 13",
         "OCN Tx SXC Control 0", /* 46 */
         "OCN Tx SXC Control 1",
         "OCN Tx SXC Control 2",
         "OCN Tx SXC Control 3",
         "OCN Tx SXC Control 4", /* 50 */
         "OCN Tx SXC Control 5",
         "OCN Tx SXC Control 6",
         "OCN Tx SXC Control 7",
         "OCN Rx High Order Map concatenate configuration 0", /* 54 */
         "OCN Rx High Order Map concatenate configuration 1",
         "OCN Rx High Order Map concatenate configuration 2",
         "OCN Rx High Order Map concatenate configuration 3",
         "OCN Rx High Order Map concatenate configuration 4", /* 58 */
         "OCN Rx High Order Map concatenate configuration 5",
         "OCN Rx High Order Map concatenate configuration 6",
         "OCN Rx High Order Map concatenate configuration 7",
         "OCN RXPP Per STS payload Control 0", /* 62 */
         "OCN RXPP Per STS payload Control 1",
         "OCN RXPP Per STS payload Control 2",
         "OCN RXPP Per STS payload Control 3",
         "OCN RXPP Per STS payload Control 4", /* 66 */
         "OCN RXPP Per STS payload Control 5",
         "OCN VTTU Pointer Interpreter Per Channel Control 0", /* 68 */
         "OCN VTTU Pointer Interpreter Per Channel Control 1",
         "OCN VTTU Pointer Interpreter Per Channel Control 2",
         "OCN VTTU Pointer Interpreter Per Channel Control 3",
         "OCN VTTU Pointer Interpreter Per Channel Control 4", /* 72 */
         "OCN VTTU Pointer Interpreter Per Channel Control 5",
         "OCN TXPP Per STS Multiplexing Control 0", /* 74 */
         "OCN TXPP Per STS Multiplexing Control 1",
         "OCN TXPP Per STS Multiplexing Control 2",
         "OCN TXPP Per STS Multiplexing Control 3",
         "OCN TXPP Per STS Multiplexing Control 4", /* 78 */
         "OCN TXPP Per STS Multiplexing Control 5",
         "OCN VTTU Pointer Generator Per Channel Control 0", /* 80 */
         "OCN VTTU Pointer Generator Per Channel Control 1",
         "OCN VTTU Pointer Generator Per Channel Control 2",
         "OCN VTTU Pointer Generator Per Channel Control 3",
         "OCN VTTU Pointer Generator Per Channel Control 4", /* 84 */
         "OCN VTTU Pointer Generator Per Channel Control 5"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60210011InternalRamOcnNew(self, ramId, localRamId);
    }

static uint8 StartSts1ToScanConcate(Tha60210011ModuleOcn self, AtSdhChannel aug)
    {
    AtUnused(self);
    AtUnused(aug);
    return 0;
    }

static eAtRet StsConcatRelease(ThaModuleOcn self, AtSdhChannel aug, uint8 sts1Id)
    {
    uint8 swSts_i;
    eAtRet ret;
    uint8 slice, hwStsMaster;
    uint8 numSts1sToRelease;
    uint8 startSts1 = mMethodsGet(mThis(self))->StartSts1ToScanConcate(mThis(self), aug);

    AtUnused(self);

    ret = ThaSdhChannelHwStsGet(aug, cThaModuleOcn, sts1Id, &slice, &hwStsMaster);
    if (ret != cAtOk)
        return ret;

    numSts1sToRelease = mMethodsGet(mThis(self))->NumStsToReleaseConcate(mThis(self), aug);

    /* This code is applicable when run STM16/OC48 rate */
    for (swSts_i = 0; swSts_i < numSts1sToRelease; swSts_i++)
        {
        uint8 hwSts_i;
        ret = ThaSdhChannelHwStsGet(aug, cThaModuleOcn, (uint8)(startSts1 + swSts_i), &slice, &hwSts_i);
        if (ret != cAtOk)
            return ret;

        PiStsConcatRelease(self, aug, slice, hwStsMaster, hwSts_i);
        PgStsConcatRelease(self, aug, slice, hwStsMaster, hwSts_i);
        mMethodsGet(self)->TerPgStsConcatRelease(self, aug, slice, hwStsMaster, hwSts_i);
        }

    return ret;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsOcnRegister(AtModuleDeviceGet(self), address);
    }

static eBool NeedTerminateAsDefault(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtFalse;
    }

static eAtRet Vc4xTxEnable(ThaModuleOcn self, AtSdhChannel channel, eBool vc4En)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(vc4En);
    return cAtOk;
    }

static eAtRet RxStsTermEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ChannelCanBeTerminated(mThis(self), channel))
        return cAtOk;

    regAddr = ThaModuleOcnRxStsPayloadControl(self, channel) + LoStsOffsetWithBaseAddress(mThis(self), channel, stsId);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

        mRegFieldSet(regVal, cAf6_demramctl_PiDemStsTerm_, enable ? 1 : 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet StsPohInsertEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    uint32 ret;
    uint8 slice, hwSts;
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtOk;

    ret = ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    regAddr = mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
              TxHwHoStsOffsetWithBaseAddress(self, channel, slice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPgPohIns_, mBoolToBin(enable));
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool StsPohInsertIsEnabled(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint32 ret;
    uint8 slice, hwSts;
    uint32 regAddr, regVal;

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtFalse;

    ret = ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &slice, &hwSts);
    if (ret != cAtOk)
        return cAtFalse;

    regAddr = mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelControlRegAddr(mThis(self), channel) +
              TxHwHoStsOffsetWithBaseAddress(self, channel, slice, hwSts);

    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return mBinToBool(mRegField(regVal, cAf6_spgramctl_StsPgPohIns_));
    }

static eAtRet StsTxPayloadTypeSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eThaStsPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    AtUnused(payloadType);
    return cAtOk;
    }

static uint8 StsTxPayloadTypeGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    return 0;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_vpiramctl_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_vpgramctl_Base;
    }

static uint32 BaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0x0100000;
    }

static uint32 Tfi5GlbtfmBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbtfm_reg_Base;
    }

static uint32 Tfi5GlbrfmBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbrfm_reg_Base;
    }

static uint32 GlbtpgBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbtpg_reg_Base;
    }

static uint32 GlbspiBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbspi_reg_Base;
    }

static uint32 GlbvpiBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbvpi_reg_Base;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_upvtchstkram_Base;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_upvtchstaram_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_vtpgstkram_Base;
    }

static uint32 OcnVtTuPointerGeneratorNdfIntrMask(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6_vtpgstkram_VtPgNdfIntr_Mask;
    }

static uint32 OcnStsPointerInterpreterPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6Reg_upstschstkram_Base;
    }

static uint32 OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_upstschstaram_Base;
    }

static eAtRet Vc4xPayloadDefault(ThaModuleOcn self, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    return cAtOk;
    }

static eAtRet LomCheckingEnable(Tha60210011ModuleOcn self, AtSdhChannel channel, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, mastHwSts;
    AtUnused(self);

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return enable ? cAtErrorModeNotSupport : cAtOk;

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &hwSlice, &mastHwSts);
    regAddr = mMethodsGet(mThis(self))->OcnStsPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
              HoStsOffsetWithBaseAddress(ThaModuleOcnFromChannel((AtChannel)channel), channel, hwSlice, mastHwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spiramctl_StsPiChkLom_, mBoolToBin(enable));
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool LomCheckingIsEnabled(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, mastHwSts;
    AtUnused(self);

    if (!mMethodsGet(mThis(self))->ChannelHasPointerProcessor(mThis(self), channel))
        return cAtFalse;

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &hwSlice, &mastHwSts);
    regAddr = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(mThis(self), channel) +
              HoStsOffsetWithBaseAddress(ThaModuleOcnFromChannel((AtChannel)channel), channel, hwSlice, mastHwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return mRegField(regVal, cAf6_spiramctl_StsPiChkLom_) ? cAtTrue : cAtFalse;
    }

static eAtRet DebugSliceConcate(ThaModuleOcn self, uint32 sliceId, uint32 baseLocalAddress, uint32 indent)
    {
    static const uint32 cNumSts1PerSlice = 48;
    uint32 sts1_i;
    uint32 sliceOffset = sliceId * 512UL;
    uint32 baseAddress = ThaModuleOcnBaseAddress(self) + baseLocalAddress;

    for (sts1_i = 0; sts1_i < cNumSts1PerSlice; sts1_i++)
        {
        uint32 regAddr = baseAddress + sliceOffset + sts1_i;
        uint32 regVal = mModuleHwRead(self, regAddr);
        uint32 isSlave = regVal & cAf6_spiramctl_StsPiStsSlvInd_Mask;
        uint32 masterSts1 = mRegField(regVal, cAf6_spiramctl_StsPiStsMstId_);
        uint32 selfMaster = masterSts1;
        uint32 slave_i, numSlaves = 0;

        AtSpaceIndent(indent);
        if (isSlave)
            {
            AtPrintc(cSevWarning, "* Slave  STS-1#%02u: ", sts1_i);
            AtPrintc(cSevNormal, "Master: %u\r\n", masterSts1);
            continue;
            }

        /* This STS-1 is master, scanning all of its possible STSs */
        AtPrintc(cSevInfo, "* Master STS-1#%02u: ", sts1_i);
        AtPrintc(cSevNormal, "Slaves: ");
        for (slave_i = 0; slave_i < cNumSts1PerSlice; slave_i++)
            {
            regAddr = baseAddress + sliceOffset + slave_i;
            regVal = mModuleHwRead(self, regAddr);
            isSlave = regVal & cAf6_spiramctl_StsPiStsSlvInd_Mask;
            masterSts1 = mRegField(regVal, cAf6_spiramctl_StsPiStsMstId_);

            if (isSlave && (masterSts1 == sts1_i))
                {
                if (numSlaves == 0)
                    AtPrintc(cSevNormal, "%u", slave_i);
                else
                    AtPrintc(cSevNormal, ", %u", slave_i);

                numSlaves = numSlaves + 1;
                }
            }

        /* If channel does not belong to any concatenation, it should be master of itself */
        if (numSlaves == 0)
            {
            if (selfMaster == sts1_i)
                AtPrintc(cSevNormal, "None");
            else
                AtPrintc(cSevCritical, "%u", selfMaster);
            }

        AtPrintc(cSevNormal, "\r\n");
        }

    return cAtOk;
    }

static uint32 OcnStsPointerGeneratorPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_stspgstkram_Base;
    }

static uint32 RxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_rxsxcramctl_Base;
    }

static uint32 TxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_txsxcramctl_Base;
    }

static uint8 SdhChannelSwStsId2HwFlatId(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 sts1Id)
    {
    return Stm16SdhChannelSwStsId2HwFlatId((ThaModuleOcn)self, channel, sts1Id);
    }

static AtModuleXc XcModule(Tha60210011ModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static uint8 SliceOfChannel(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule)
    {
    Tha60210011ModuleXc xcModule = (Tha60210011ModuleXc)XcModule(self);
    AtUnused(phyModule);
    return Tha60210011ModuleXcHwSliceIdGet(xcModule, AtSdhChannelLineGet(channel));
    }

static eBool HoPathPhyModuleIsDividedToOc24(Tha60210011ModuleOcn self, eAtModule phyModule)
    {
    AtUnused(self);
    if (phyModule == cAtModuleSur)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DebugConcateWithIndent(ThaModuleOcn self, uint32 numSlices, uint32 localBaseAddress, uint32 indent)
    {
    uint32 slice_i;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        AtSpaceIndent(indent);
        AtPrintc(cSevNormal, "* Slice %u: \r\n", slice_i);
        DebugSliceConcate(self, slice_i, localBaseAddress, indent + 1);
        }

    return cAtOk;
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x4, 0x1036);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= supportedVersion) ? cAtTrue : cAtFalse;
    }

static uint32 SpiramctlAddress(ThaModuleOcn self, AtSdhAu au)
    {
    AtSdhChannel channel = (AtSdhChannel)au;
    return mMethodsGet(mThis(self))->OcnStsPointerInterpreterPerChannelControlRegAddr(mThis(self), channel) +
           Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)au);
    }

static eAtRet PohInterruptNotify(ThaModuleOcn self, AtSdhAu au, eBool notify)
    {
    uint32 regAddr = SpiramctlAddress(self, au);
    uint32 regVal = mChannelHwRead(au, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spiramctl_StsPiRemoteLoopBack_, notify ? 1 : 0);
    mChannelHwWrite(au, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool PohInterruptIsNotified(ThaModuleOcn self, AtSdhAu au)
    {
    uint32 regAddr = SpiramctlAddress(self, au);
    uint32 regVal = mChannelHwRead(au, regAddr, cThaModuleOcn);
    return (regVal & cAf6_spiramctl_StsPiRemoteLoopBack_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 DemramctlAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    uint32 base = ThaModuleOcnRxStsPayloadControl(self, channel);
    return base + LoStsOffsetWithBaseAddress(mThis(self), channel, stsId);
    }

static eAtRet MapLoAisDownstream(ThaModuleOcn self, AtSdhAu au, eBool downstream)
    {
    AtSdhChannel channel = (AtSdhChannel)au;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId = (uint8)(startSts + sts_i);
        uint32 regAddr = DemramctlAddress(self, channel, stsId);
        uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

        mRegFieldSet(regVal, cAf6_demramctl_PiDemAisDownst_, downstream ? 1 : 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static eBool MapLoAisIsDownstream(ThaModuleOcn self, AtSdhAu au)
    {
    AtSdhChannel channel = (AtSdhChannel)au;
    uint8 sts1Id = AtSdhChannelSts1Get(channel);
    uint32 regAddr = DemramctlAddress(self, channel, sts1Id);
    uint32 regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return (regVal & cAf6_demramctl_PiDemAisDownst_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 RxhomapramctlAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    uint32 base = mMethodsGet(mThis(self))->OcnRxHoMapConcatenateReg(mThis(self));
    return base + HoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);
    }

static eAtRet MapHiAisDownstream(ThaModuleOcn self, AtSdhAu au, eBool downstream)
    {
    AtSdhChannel channel = (AtSdhChannel)au;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId = (uint8)(startSts + sts_i);
        uint8 hwSlice;
        uint8 hwSts;
        uint32 regAddr, regVal;

        ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts);
        regAddr = RxhomapramctlAddress(self, channel, hwSlice, hwSts);
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
        mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapAisDowns_, downstream ? 1 : 0);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
        }

    return cAtOk;
    }

static eBool ChannelCanBeTerminated(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtTrue;
    }

static eBool ChannelHasAttPointerProcessor(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtFalse;
    }

static eBool ChannelHasPointerProcessor(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtTrue;
    }

static eBool MapHiAisIsDownstream(ThaModuleOcn self, AtSdhAu au)
    {
    AtSdhChannel channel = (AtSdhChannel)au;
    uint8 sts1 = AtSdhChannelSts1Get(channel);
    uint8 hwSlice, hwSts;
    uint32 regAddr, regVal;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, sts1, &hwSlice, &hwSts);
    regAddr = RxhomapramctlAddress(self, channel, hwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    return (regVal & cAf6_rxhomapramctl_HoMapAisDowns_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PathAisRxForcingPointMove(ThaModuleOcn self, AtSdhAu au, eBool move)
    {
    eAtRet ret = cAtOk;

    ret |= PohInterruptNotify(self, au, move);
    ret |= MapLoAisDownstream(self, au, move);
    ret |= MapHiAisDownstream(self, au, move);

    return ret;
    }

static eBool PathAisRxForcingPointIsMoved(ThaModuleOcn self, AtSdhAu au)
    {
    if (PohInterruptIsNotified(self, au) &&
        MapHiAisIsDownstream(self, au) &&
        MapLoAisIsDownstream(self, au))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 VpiramctlAddress(ThaModuleOcn self, AtSdhVc loVc)
    {
    AtSdhChannel channel = (AtSdhChannel)loVc;
    uint8 sts1Id = AtSdhChannelSts1Get(channel);
    uint8 vtgId = AtSdhChannelTug2Get(channel);
    uint8 vtId = AtSdhChannelTu1xGet(channel);
    uint32 base = mMethodsGet(mThis(self))->OcnVtTuPointerInterpreterPerChannelControlRegAddr(mThis(self), channel);
    uint32 offset = ThaModuleOcnStsVtDefaultOffset(self, channel, sts1Id, vtgId, vtId);

    return base + offset;
    }

static eAtRet NotifyLoVcAisToPoh(ThaModuleOcn self, AtSdhVc loVc, eBool notified)
    {
    uint32 regAddr = VpiramctlAddress(self, loVc);
    uint32 regVal = mChannelHwRead(loVc, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_vpiramctl_VtPiRemoteLoopBack_, notified ? 1 : 0);
    mChannelHwWrite(loVc, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel vc, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8* hwSts)
    {
    *sliceId = mMethodsGet(mThis(self))->SliceOfChannel(mThis(self), vc, phyModule);
    *hwSts = mMethodsGet(mThis(self))->SdhChannelSwStsId2HwFlatId(mThis(self), vc, swSts % 48);
    AtUnused(phyModule);
    return cAtOk;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_adjcntperstkram_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_adjcntpgpervtram_Base;
    }

static uint32 StartVersionNeedHwRxPiPjAdjThreshold(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool TuVcMapCx(AtSdhChannel tuVc)
    {
    uint8 channelType = AtSdhChannelTypeGet(tuVc);

    if ((channelType == cAtSdhChannelTypeVc3) &&
        (AtSdhChannelMapTypeGet(tuVc) == cAtSdhVcMapTypeVc3MapC3))
        return cAtTrue;

    if ((channelType == cAtSdhChannelTypeVc12 ||
         channelType == cAtSdhChannelTypeVc11) &&
        (AtSdhChannelMapTypeGet((AtSdhChannel)tuVc) == cAtSdhVcMapTypeVc1xMapC1x))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PgTuVcTxForcibleAlarms(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    AtUnused(self);
    AtUnused(tuVc);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmUneq;
    }

static eAtRet PgTuVcHwTxAlarmForce(Tha60210011ModuleOcn self, AtChannel tuVc, uint32 alarmType, eBool force)
    {
    uint32 address = Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(self, (AtSdhChannel)tuVc) +
                     VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    uint32 regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    if (alarmType & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_vpgramctl_LineVtPiAisFrcUp_, (force) ? 1 : 0);
    if (alarmType & cAtSdhPathAlarmUneq)
        mRegFieldSet(regVal, cAf6_vpgramctl_VtPgUeqFrc_, (force) ? 1 : 0);
    mChannelHwWrite(tuVc, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 PgTuVcTxForcedAlarmGet(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    uint32 address = Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(self, (AtSdhChannel)tuVc) +
                     VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    uint32 regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    uint32 alarms = 0;

    if (regVal & cAf6_vpgramctl_LineVtPiAisFrcUp_Mask)
        alarms |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_vpgramctl_VtPgUeqFrc_Mask)
        alarms |= cAtSdhPathAlarmUneq;

    return alarms;
    }

static eBool PgTuVcCanForceTxAlarms(Tha60210011ModuleOcn self, AtChannel tuVc, uint32 alarmType)
    {
    return (PgTuVcTxForcibleAlarms(self, tuVc) & alarmType) ? cAtTrue : cAtFalse;
    }

static eAtRet PiTuVcHwRxAlarmForce(Tha60210011ModuleOcn self, AtChannel tuVc, uint32 alarmType, eBool force)
    {
    uint32 address = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(self, (AtSdhChannel)tuVc) +
                     VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    uint32 regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    if (alarmType & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_vpiramctl_LineVtPiAisFrcDown_, (force) ? 1 : 0);
    mChannelHwWrite(tuVc, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 PiTuVcHwRxForcedAlarmGet(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    uint32 address = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(self, (AtSdhChannel)tuVc) +
                     VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    uint32 regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);

    if (regVal & cAf6_vpiramctl_LineVtPiAisFrcDown_Mask)
        return cAtSdhPathAlarmAis;

    return 0;
    }

static uint32 PiTuVcRxForcibleAlarms(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    AtUnused(self);
    AtUnused(tuVc);

    return cAtSdhPathAlarmAis;
    }

static uint32 TuVcRxForcibleAlarms(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    if (mMethodsGet(self)->TuVcIsLoopbackAtPointerProcessor(self, tuVc))
        return PiTuVcRxForcibleAlarms(self, tuVc);

    if (TuVcMapCx((AtSdhChannel)tuVc))
        return cAtSdhPathAlarmAis;
    return 0;
    }

static eAtRet TuVcHwRxAlarmForce(Tha60210011ModuleOcn self, AtChannel tuVc, uint32 alarmType, eBool force)
    {
    if (mMethodsGet(self)->TuVcIsLoopbackAtPointerProcessor(self, tuVc))
        return PiTuVcHwRxAlarmForce(self, tuVc, alarmType, force);

    return Tha6021SdhLoVcRxAlarmForce(tuVc, alarmType, force);
    }

static uint32 TuVcRxForcedAlarmGet(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    if (mMethodsGet(self)->TuVcIsLoopbackAtPointerProcessor(self, tuVc))
        return PiTuVcHwRxForcedAlarmGet(self, tuVc);

    return Tha6021SdhLoVcRxForcedAlarmGet(tuVc);
    }

static eBool TuVcCanForceRxAlarms(Tha60210011ModuleOcn self, AtChannel tuVc, uint32 alarmType)
    {
    return (alarmType & mMethodsGet(self)->TuVcRxForcibleAlarms(self, tuVc)) ? cAtTrue : cAtFalse;
    }

static eBool TuVcIsLoopbackAtPointerProcessor(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    AtUnused(self);
    AtUnused(tuVc);
    return cAtFalse;
    }

static eAtRet PpTuVcLocalLoopbackSet(Tha60210011ModuleOcn self, AtChannel tuVc, eBool enable)
    {
    uint32 address = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(self, (AtSdhChannel)tuVc) +
                     VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    uint32 regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_vpiramctl_LineVtPiLocLb_, (enable) ? 1 : 0);
    mChannelHwWrite(tuVc, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PpTuVcRemoteLoopbackSet(Tha60210011ModuleOcn self, AtChannel tuVc, eBool enable)
    {
    uint32 address = Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(self, (AtSdhChannel)tuVc) +
                     VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    uint32 regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_vpgramctl_LineVtPiRemLb_, (enable) ? 1 : 0);
    mChannelHwWrite(tuVc, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet PpTuVcLoopbackSet(Tha60210011ModuleOcn self, AtChannel tuVc, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            ret |= PpTuVcLocalLoopbackSet(self, tuVc, cAtTrue);
            ret |= PpTuVcRemoteLoopbackSet(self, tuVc, cAtFalse);
            break;

        case cAtLoopbackModeRemote:
            ret |= PpTuVcLocalLoopbackSet(self, tuVc, cAtFalse);
            ret |= PpTuVcRemoteLoopbackSet(self, tuVc, cAtTrue);
            break;

        case cAtLoopbackModeRelease:
            ret |= PpTuVcLocalLoopbackSet(self, tuVc, cAtFalse);
            ret |= PpTuVcRemoteLoopbackSet(self, tuVc, cAtFalse);
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    return ret;
    }

static uint8 PpTuVcLoopbackGet(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    uint32 offset, address, regVal;

    offset = VtDefaultOffset((ThaModuleOcn)self, (AtSdhPath)tuVc);
    address = Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(self, (AtSdhChannel)tuVc) + offset;
    regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    if (regVal & cAf6_vpiramctl_LineVtPiLocLb_Mask)
        return cAtLoopbackModeLocal;

    address = Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(self, (AtSdhChannel)tuVc) + offset;
    regVal  = mChannelHwRead(tuVc, address, cThaModuleOcn);
    if (regVal & cAf6_vpgramctl_LineVtPiRemLb_Mask)
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static eBool PpTuVcLoopbackIsSupported(Tha60210011ModuleOcn self, AtChannel tuVc, uint8 loopbackMode)
    {
    AtUnused(self);
    AtUnused(tuVc);
    if ((loopbackMode == cAtLoopbackModeLocal)  ||
        (loopbackMode == cAtLoopbackModeRemote) ||
        (loopbackMode == cAtLoopbackModeRelease))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet VtTuLoopbackSet(ThaModuleOcn self, AtSdhChannel tuVc, uint8 loopbackMode)
    {
    if (mMethodsGet(mThis(self))->TuVcIsLoopbackAtPointerProcessor(mThis(self), (AtChannel)tuVc))
        return PpTuVcLoopbackSet(mThis(self), (AtChannel)tuVc, loopbackMode);

    return Tha60210011SdhVcPdhLoopbackSet((AtChannel)tuVc, loopbackMode);
    }

static uint8 VtTuLoopbackGet(ThaModuleOcn self, AtSdhChannel tuVc)
    {
    if (mMethodsGet(mThis(self))->TuVcIsLoopbackAtPointerProcessor(mThis(self), (AtChannel)tuVc))
        return PpTuVcLoopbackGet(mThis(self), (AtChannel)tuVc);

    if (!TuVcMapCx(tuVc))
        return cAtLoopbackModeRelease;

    return Tha60210011SdhVcPdhLoopbackGet((AtChannel)tuVc);
    }

static eBool TuVcLoopbackIsSupported(Tha60210011ModuleOcn self, AtChannel tuVc, uint8 loopbackMode)
    {
    if (mMethodsGet(self)->TuVcIsLoopbackAtPointerProcessor(self, tuVc))
        return PpTuVcLoopbackIsSupported(self, tuVc, loopbackMode);

    if (!TuVcMapCx((AtSdhChannel)tuVc))
        return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;

    if ((loopbackMode == cAtLoopbackModeLocal)  ||
        (loopbackMode == cAtLoopbackModeRemote) ||
        (loopbackMode == cAtLoopbackModeRelease))
        return Tha60210011SdhVcPdhLoopbackIsApplicable(tuVc, loopbackMode);

    return cAtFalse;
    }

static eBool LoPathPhyModuleIsDividedToBlockOc24(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule)
    {
    uint16 _phyModule = (uint16)phyModule;

    AtUnused(self);
    AtUnused(channel);
    if ((_phyModule == cAtModuleSdh)  ||
        (_phyModule == cThaModuleOcn) ||
        (_phyModule == cThaModulePoh) ||
        (_phyModule == cAtModuleXc)   ||
        (_phyModule == cAtModulePrbs))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 TxHwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    return mMethodsGet(self)->HwStsDefaultOffset(self, channel, hwSlice, hwSts);
    }

static uint32 TxLoStsPayloadHwFormula(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    return mMethodsGet(self)->LoStsPayloadHwFormula(self, channel, hwSlice, hwSts);
    }

static eBool ShouldEnableLosPpmDetection(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 OcnStsPointerInterpreterPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_adjcntperstsram_Base;
    }

static uint32 OcnStsPointerGeneratorPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_adjcntpgperstsram_Base;
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UseTfi5);
        mMethodOverride(m_methods, NeedScramble);
        mMethodOverride(m_methods, NeedMasterTfi5);
        mMethodOverride(m_methods, HwStsDefaultOffset);
        mMethodOverride(m_methods, TxHwStsDefaultOffset);
        mMethodOverride(m_methods, OcnVtTuPointerInterpreterPerChannelControlRegAddr);
        mMethodOverride(m_methods, OcnVtTuPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_methods, OcnStsPointerInterpreterPerChannelControlRegAddr);
        mMethodOverride(m_methods, OcnStsPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_methods, OcnTerStsPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_methods, LomCheckingEnable);
        mMethodOverride(m_methods, LomCheckingIsEnabled);
        mMethodOverride(m_methods, HoLinePiStsConcatRelease);
        mMethodOverride(m_methods, HoLinePiStsConcatMasterSet);
        mMethodOverride(m_methods, HoLinePiStsConcatSlaveSet);
        mMethodOverride(m_methods, OcnStsPointerGeneratorPerChannelInterruptRegAddr);
        mMethodOverride(m_methods, RxSxcControlRegAddr);
        mMethodOverride(m_methods, TxSxcControlRegAddr);
        mMethodOverride(m_methods, LoStsPayloadHwFormula);
        mMethodOverride(m_methods, TxLoStsPayloadHwFormula);
        mMethodOverride(m_methods, OcnRxHoMapConcatenateReg);
        mMethodOverride(m_methods, SdhChannelSwStsId2HwFlatId);
        mMethodOverride(m_methods, SliceOfChannel);
        mMethodOverride(m_methods, HoPathPhyModuleIsDividedToOc24);
        mMethodOverride(m_methods, ShowConcateDebugInformation);
        mMethodOverride(m_methods, StartSts1ToScanConcate);
        mMethodOverride(m_methods, Tfi5GlbtfmBase);
        mMethodOverride(m_methods, Tfi5GlbrfmBase);
        mMethodOverride(m_methods, GlbtpgBase);
        mMethodOverride(m_methods, GlbspiBase);
        mMethodOverride(m_methods, GlbvpiBase);
        mMethodOverride(m_methods, OcnVtTuPointerInterpreterPerChannelInterruptRegAddr);
        mMethodOverride(m_methods, OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr);
        mMethodOverride(m_methods, OcnVtTuPointerGeneratorPerChannelInterruptRegAddr);
        mMethodOverride(m_methods, OcnVtTuPointerGeneratorNdfIntrMask);
        mMethodOverride(m_methods, OcnStsPointerInterpreterPerChannelInterruptRegAddr);
        mMethodOverride(m_methods, OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr);
        mMethodOverride(m_methods, ChannelCanBeTerminated);
        mMethodOverride(m_methods, ChannelHasPointerProcessor);
        mMethodOverride(m_methods, ChannelHasAttPointerProcessor);
        mMethodOverride(m_methods, NumStsToReleaseConcate);
        mMethodOverride(m_methods, Oc48HwStsAndSliceConvertToOc24);
        mMethodOverride(m_methods, OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr);
        mMethodOverride(m_methods, OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr);
        mMethodOverride(m_methods, StartVersionNeedHwRxPiPjAdjThreshold);
        mMethodOverride(m_methods, LoPathPhyModuleIsDividedToBlockOc24);
        mMethodOverride(m_methods, TuVcRxForcibleAlarms);
        mMethodOverride(m_methods, TuVcHwRxAlarmForce);
        mMethodOverride(m_methods, TuVcRxForcedAlarmGet);
        mMethodOverride(m_methods, TuVcLoopbackIsSupported);
        mMethodOverride(m_methods, TuVcIsLoopbackAtPointerProcessor);
        mMethodOverride(m_methods, ShouldEnableLosPpmDetection);
        mMethodOverride(m_methods, OcnStsPointerInterpreterPerChannelAdjustCountRegAddr);
        mMethodOverride(m_methods, OcnStsPointerGeneratorPerChannelAdjustCountRegAddr);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, PiStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, PiStsConcatSlaveSet);
        mMethodOverride(m_ThaModuleOcnOverride, PgStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, PgStsConcatSlaveSet);
        mMethodOverride(m_ThaModuleOcnOverride, StsIsSlaveInChannel);
        mMethodOverride(m_ThaModuleOcnOverride, Tug3HwPayloadTypeSet);
        mMethodOverride(m_ThaModuleOcnOverride, Au3VcHwPayloadTypeSet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTerminate);
        mMethodOverride(m_ThaModuleOcnOverride, VtPohInsertEnable);
        mMethodOverride(m_ThaModuleOcnOverride, VtPohInsertIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, StsPohInsertEnable);
        mMethodOverride(m_ThaModuleOcnOverride, StsPohInsertIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, RxVtgSet);
        mMethodOverride(m_ThaModuleOcnOverride, TxVtgSet);
        mMethodOverride(m_ThaModuleOcnOverride, PpSsTxVtSet);
        mMethodOverride(m_ThaModuleOcnOverride, PpSsTxVtGet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuExpectedSsSet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuExpectedSsGet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuSsCompareEnable);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuSsCompareIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuTxForcibleAlarms);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuHwTxAlarmForce);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuTxForcedAlarmGet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuRxForcibleAlarms);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuRxForcedAlarmGet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuHwRxAlarmForce);
        mMethodOverride(m_ThaModuleOcnOverride, NumSlices);
        mMethodOverride(m_ThaModuleOcnOverride, NumStsInOneSlice);
        mMethodOverride(m_ThaModuleOcnOverride, ChannelStsIdSw2HwGet);
        mMethodOverride(m_ThaModuleOcnOverride, SdhChannelRegsShow);
        mMethodOverride(m_ThaModuleOcnOverride, StsConcatRelease);
        mMethodOverride(m_ThaModuleOcnOverride, NeedTerminateAsDefault);
        mMethodOverride(m_ThaModuleOcnOverride, Vc4xTxEnable);
        mMethodOverride(m_ThaModuleOcnOverride, RxStsTermEnable);
        mMethodOverride(m_ThaModuleOcnOverride, PgSlaveIndicate);
        mMethodOverride(m_ThaModuleOcnOverride, StsTxPayloadTypeSet);
        mMethodOverride(m_ThaModuleOcnOverride, StsTxPayloadTypeGet);
        mMethodOverride(m_ThaModuleOcnOverride, BaseAddress);
        mMethodOverride(m_ThaModuleOcnOverride, StsIdSw2Hw);
        mMethodOverride(m_ThaModuleOcnOverride, StsDefaultOffset);
        mMethodOverride(m_ThaModuleOcnOverride, StsVtDefaultOffset);
        mMethodOverride(m_ThaModuleOcnOverride, Vc4xPayloadDefault);
        mMethodOverride(m_ThaModuleOcnOverride, CanMovePathAisRxForcingPoint);
        mMethodOverride(m_ThaModuleOcnOverride, PathAisRxForcingPointMove);
        mMethodOverride(m_ThaModuleOcnOverride, PathAisRxForcingPointIsMoved);
        mMethodOverride(m_ThaModuleOcnOverride, NotifyLoVcAisToPoh);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, RxStsPayloadControl);
        mMethodOverride(m_ThaModuleOcnOverride, TxStsMultiplexingControl);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuLoopbackSet);
        mMethodOverride(m_ThaModuleOcnOverride, VtTuLoopbackGet);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleOcn);
    }

AtModule Tha60210011ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210011ModuleOcnObjectInit(newModule, device);
    }

eAtRet Tha60210011ModuleOcnAisDownstreamEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 address = GlbSpiReg(self);
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiAisAisPEn_, enable ? 1 : 0);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

eAtRet Tha60210011ModuleOcnLopDownstreamEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 address = GlbSpiReg(self);
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiLopAisPEn_, enable ? 1 : 0);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

eAtRet Tha60210011ModuleOcnLineScrambleEnable(ThaModuleOcn self, eBool enabled)
    {
    uint32 regValue, address;

    /* RX */
    address  = mMethodsGet(mThis(self))->Tfi5GlbrfmBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_glbrfm_reg_RxFrmDescrEn_, enabled ? 1 : 0);
    mModuleHwWrite(self, address, regValue);

    /* TX */
    address  = mMethodsGet(mThis(self))->Tfi5GlbtfmBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_glbtfm_reg_TxFrmScrEn_, enabled ? 1 : 0);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

eBool Tha60210011ModuleOcnLineScrambleIsEnabled(ThaModuleOcn self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->Tfi5GlbrfmBase(mThis(self)) + ThaModuleOcnBaseAddress(self);
    uint32 regValue = mModuleHwRead(self, regAddr);
    return (regValue & cAf6_glbrfm_reg_RxFrmDescrEn_Mask) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210011ModuleOcnTxStsDefaultOffset(AtSdhChannel channel)
    {
    uint8 hwSlice, hwSts;
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &hwSlice, &hwSts) == cAtOk)
        return TxHwHoStsOffsetWithBaseAddress(ThaModuleOcnFromChannel((AtChannel)channel),channel, hwSlice, hwSts);
    return cBit31_0;
    }

uint32 Tha60210011ModuleOcnStsDefaultOffset(AtSdhChannel channel)
    {
    return ThaModuleOcnStsDefaultOffset(ThaModuleOcnFromChannel((AtChannel)channel), channel, AtSdhChannelSts1Get(channel));
    }

uint32 Tha60210011ModuleOcnBaseAddress(ThaModuleOcn self)
    {
    return ThaModuleOcnBaseAddress(self);
    }

uint32 Tha60210011ModuleOcnTxVtDefaultOffset(AtSdhChannel channel)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
    	return TxVtDefaultOffset(ocnModule, (AtSdhPath)channel);
    return cBit31_0;
    }

uint32 Tha60210011ModuleOcnVtDefaultOffset(AtSdhChannel channel)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
    	return VtDefaultOffset(ocnModule, (AtSdhPath)channel);
    return cBit31_0;
    }

uint32 Tha60210011ModuleOcnLoStsDefaultOffset(AtSdhChannel channel, uint8 sts1)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)channel);
    if (ocnModule)
        return LoStsDefaultOffset(ocnModule, channel, sts1);
    return cBit31_0;
    }

eAtRet Tha60210011ModuleOcnLomCheckingEnable(AtSdhChannel channel, eBool enable)
    {
    Tha60210011ModuleOcn ocnModule = mThis(ThaModuleOcnFromChannel((AtChannel)channel));
    if (ocnModule)
        return mMethodsGet(ocnModule)->LomCheckingEnable(ocnModule, channel, enable);
    
    return cAtErrorNullPointer;
    }

eBool Tha60210011ModuleOcnLomCheckingIsEnabled(AtSdhChannel channel)
    {
    Tha60210011ModuleOcn ocnModule = mThis(ThaModuleOcnFromChannel((AtChannel)channel));
    if (ocnModule)
        return mMethodsGet(ocnModule)->LomCheckingIsEnabled(ocnModule, channel);
    return cAtFalse;
    }

eAtRet Tha60210011ModuleOcnLoLineHwSts1PayloadInit(ThaModuleOcn self, AtSdhChannel channel, uint8 slice, uint8 hwStsId)
    {
    uint32 regVal;
    uint32 offset = ThaModuleOcnBaseAddress(self) + mMethodsGet(mThis(self))->LoStsPayloadHwFormula(mThis(self), channel, slice, hwStsId);
    uint32 regAddr = ThaModuleOcnRxStsPayloadControl(self, channel) + offset;

    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_demramctl_PiDemSpeType_, cThaStsDontCareVtTu);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    regAddr = ThaModuleOcnTxStsMultiplexingControl(self, channel) + offset;
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgDemSpeType_, cThaStsDontCareVtTu);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

uint16 Tha60210011ModuleOcnSliceAndStsCache(uint8 slice, uint8 sts)
    {
    uint16 cache = 0;
    cache = (uint16)(slice << 8 | sts);
    return cache;
    }

uint32 Tha60210011ModuleOcnTfi5GlbtfmBase(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->Tfi5GlbtfmBase(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnTfi5GlbrfmBase(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->Tfi5GlbrfmBase(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerInterpreterPerChannelInterruptRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAlarmStatus(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerGeneratorPerChannelInterruptRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerGeneratorNdfIntrMask(Tha60210011ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerGeneratorNdfIntrMask(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(self)->OcnStsPointerInterpreterPerChannelInterruptRegAddr(self, sdhChannel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelAlarmStatus(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnGlbspiBase(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->GlbspiBase(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnGlbvpiBase(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->GlbvpiBase(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnStsPointerInterpreterPerChannelControlRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerGeneratorPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnStsPointerGeneratorPerChannelControlRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerInterpreterPerChannelControlRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelControl(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerGeneratorPerChannelControlRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnRxHighOrderMapConcatenationControl(Tha60210011ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->OcnRxHoMapConcatenateReg(self);
    return cBit31_0;
    }

uint32 Tha60210011ModuleOcnHwStsOffsetWithBaseAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    if (self)
        return HoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);
    return 0;
    }

uint32 Tha60210011ModuleOcnGlbtpgBase(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->GlbtpgBase(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerGeneratorPerChannelInterrupt(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(mThis(self))->OcnStsPointerGeneratorPerChannelInterruptRegAddr(mThis(self), sdhChannel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnRxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->RxSxcControlRegAddr(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnTxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->TxSxcControlRegAddr(self);
    return cInvalidUint32;
    }

eAtRet Tha60210011ModuleOcnDebugConcateWithIndent(ThaModuleOcn self, uint32 numSlices, uint32 localBaseAddress, uint32 indent)
    {
    if (self)
        return DebugConcateWithIndent(self, numSlices, localBaseAddress, indent);
    return cAtErrorNullPointer;
    }

uint32 Tha60210011ModuleOcnVtTuPointerInterpreterPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnVtTuPointerGeneratorPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerInterpreterPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnStsPointerInterpreterPerChannelAdjustCountRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleOcnStsPointerGeneratorPerChannelAdjustCount(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->OcnStsPointerGeneratorPerChannelAdjustCountRegAddr(self, channel);
    return cInvalidUint32;
    }

uint32 Tha6021OcnTuVcRxForcibleAlarms(AtChannel tuVc)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);
    if (ocnModule)
        return mMethodsGet(ocnModule)->TuVcRxForcibleAlarms(ocnModule, tuVc);
    return 0;
    }

eAtRet Tha6021OcnTuVcRxAlarmForce(AtChannel tuVc, uint32 alarmType)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (TuVcCanForceRxAlarms(ocnModule, tuVc, alarmType))
        return mMethodsGet(ocnModule)->TuVcHwRxAlarmForce(ocnModule, tuVc, alarmType, cAtTrue);

    return cAtErrorModeNotSupport;
    }

eAtRet Tha6021OcnTuVcRxAlarmUnForce(AtChannel tuVc, uint32 alarmType)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (TuVcCanForceRxAlarms(ocnModule, tuVc, alarmType))
        return mMethodsGet(ocnModule)->TuVcHwRxAlarmForce(ocnModule, tuVc, alarmType, cAtFalse);

    return cAtErrorModeNotSupport;
    }

uint32 Tha6021OcnTuVcRxForcedAlarmGet(AtChannel tuVc)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);
    if (ocnModule)
        return mMethodsGet(ocnModule)->TuVcRxForcedAlarmGet(ocnModule, tuVc);
    return 0x0;
    }

eBool Tha6021OcnTuVcLoopbackIsSupported(AtChannel tuVc, uint8 loopbackMode)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);
    if (ocnModule)
        return mMethodsGet(ocnModule)->TuVcLoopbackIsSupported(ocnModule, tuVc, loopbackMode);
    return cAtFalse;
    }

uint32 Tha6021OcnTuVcTxForcibleAlarms(AtChannel tuVc)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);
    if (ocnModule)
        return PgTuVcTxForcibleAlarms(ocnModule, tuVc);
    return 0;
    }

eAtRet Tha6021OcnTuVcTxAlarmForce(AtChannel tuVc, uint32 alarmType)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (PgTuVcCanForceTxAlarms(ocnModule, tuVc, alarmType))
        return PgTuVcHwTxAlarmForce(ocnModule, tuVc, alarmType, cAtTrue);

    return cAtErrorModeNotSupport;
    }

eAtRet Tha6021OcnTuVcTxAlarmUnForce(AtChannel tuVc, uint32 alarmType)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);

    if (ocnModule == NULL)
        return cAtErrorNullPointer;
    if (PgTuVcCanForceTxAlarms(ocnModule, tuVc, alarmType))
        return PgTuVcHwTxAlarmForce(ocnModule, tuVc, alarmType, cAtFalse);

    return cAtErrorModeNotSupport;
    }

uint32 Tha6021OcnTuVcTxForcedAlarmGet(AtChannel tuVc)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)ThaModuleOcnFromChannel(tuVc);
    if (ocnModule)
        return PgTuVcTxForcedAlarmGet(ocnModule, tuVc);
    return 0x0;
    }

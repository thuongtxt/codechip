/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210011ModuleBer.h
 * 
 * Created Date: Jun 10, 2015
 *
 * Description : 60210011 BER declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEBER_H_
#define _THA60210011MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ber/ThaModuleHardBerInternal.h"
#include "../../../default/ber/ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Threshold 1 rates */
#define cSts1Threshold1Rate           0
#define cSts3Threshold1Rate           1
#define cSts6Threshold1Rate           2
#define cSts9Threshold1Rate           3
#define cSts12Threshold1Rate          4
#define cSts15Threshold1Rate          5
#define cSts18Threshold1Rate          6
#define cSts21Threshold1Rate          7
#define cSts24Threshold1Rate          8
#define cSts27Threshold1Rate          9
#define cSts30Threshold1Rate          10
#define cSts33Threshold1Rate          11
#define cSts36Threshold1Rate          12
#define cSts39Threshold1Rate          13
#define cSts42Threshold1Rate          14
#define cSts45Threshold1Rate          15
#define cSts48Threshold1Rate          16
#define cDs3Threshold1Rate            17
#define cE3G832Threshold1Rate         18
#define cE3G751Threshold1Rate         19
#define cLineDs3Threshold1Rate        20
#define cLineE3Threshold1Rate         21
#define cLineEc1Threshold1Rate        22
#define cSts192Threshold1Rate         47
#define cOc1Threshold1Rate            22
#define cOc3Threshold1Rate            48
#define cOc12Threshold1Rate           49
#define cOc48Threshold1Rate           50
#define cOc96Threshold1Rate           51
#define cOc192Threshold1Rate          52
#define cVt15Threshold1Rate           65
#define cVt2Threshold1Rate            67
#define cDs1Threshold1Rate            69
#define cE1Threshold1Rate             71

/* Threshold 2 rates */
#define cSts1Threshold2Rate            0
#define cSts3Threshold2Rate            1
#define cSts6Threshold2Rate            2
#define cSts9Threshold2Rate            3
#define cSts12Threshold2Rate           4
#define cSts15Threshold2Rate           5
#define cSts18Threshold2Rate           6
#define cSts21Threshold2Rate           7
#define cSts24Threshold2Rate           8
#define cSts27Threshold2Rate           9
#define cSts30Threshold2Rate           10
#define cSts33Threshold2Rate           11
#define cSts36Threshold2Rate           12
#define cSts39Threshold2Rate           13
#define cSts42Threshold2Rate           14
#define cSts45Threshold2Rate           15
#define cSts48Threshold2Rate           16
#define cDs3Threshold2Rate             17
#define cE3G832Threshold2Rate          18
#define cE3G751Threshold2Rate          19
#define cLineDs3Threshold2Rate         20
#define cLineE3Threshold2Rate          21
#define cLineEC1Threshold2Rate         22
#define cSts192Threshold2Rate          47
#define cOc3Threshold2Rate             48
#define cOc12Threshold2Rate            49
#define cOc48Threshold2Rate            50
#define cOc96Threshold2Rate            51
#define cOc192Threshold2Rate           52
#define cVt15Threshold2Rate            60
#define cVt2Threshold2Rate             61
#define cDs1Threshold2Rate             62
#define cE1Threshold2Rate              63

/* Standard clear time threshold */
#define cLineEc1Threshold1             0x3C71
#define cLineEC1Threshold2_0           0x440022
#define cLineEC1Threshold2_1           0x440022
#define cLineEC1Threshold2_2           0x24
#define cLineEC1Threshold2_3           0x3120038
#define cLineEC1Threshold2_4           0x34C0039
#define cLineEC1Threshold2_5           0x3520039
#define cLineEC1Threshold2_6           0x3540039
#define cLineEC1Threshold2_7           0x3540039

#define cOc3Threshold1                 0x540166
#define cOc3Threshold2_0               0xEC0076
#define cOc3Threshold2_1               0xEC0076
#define cOc3Threshold2_2               0x7B
#define cOc3Threshold2_3               0x98C009C
#define cOc3Threshold2_4               0xA40009D
#define cOc3Threshold2_5               0xA52009D
#define cOc3Threshold2_6               0xA54009D
#define cOc3Threshold2_7               0xA54009D

#define cOc12Threshold1                0x13605CB
#define cOc12Threshold2_0              0x3FC0202
#define cOc12Threshold2_1              0x4040202
#define cOc12Threshold2_2              0x212
#define cOc12Threshold2_3              0x2716024B
#define cOc12Threshold2_4              0x29F6024F
#define cOc12Threshold2_5              0x2A420250
#define cOc12Threshold2_6              0x2A4A0250
#define cOc12Threshold2_7              0x2A4C0250

#define cOc48Threshold1                0x4A51792
#define cOc48Threshold2_0              0x108E0857
#define cOc48Threshold2_1              0x10B00858
#define cOc48Threshold2_2              0x896
#define cOc48Threshold2_3              0x9E1C08E1
#define cOc48Threshold2_4              0xA9BC08F2
#define cOc48Threshold2_5              0xAAF608F3
#define cOc48Threshold2_6              0xAB1608F3
#define cOc48Threshold2_7              0xAB1A08F4

#define cLineDs3Threshold1             0x38c7
#define cLineDs3Threshold2_0           0x3A001D
#define cLineDs3Threshold2_1           0x3A001D
#define cLineDs3Threshold2_2           0x21
#define cLineDs3Threshold2_3           0x2E20032
#define cLineDs3Threshold2_4           0x2E20032
#define cLineDs3Threshold2_5           0x2E20032
#define cLineDs3Threshold2_6           0x2E20032
#define cLineDs3Threshold2_7           0x2E20032

#define cLineE3Threshold1              0x2E96
#define cLineE3Threshold2_0            0x2A0015
#define cLineE3Threshold2_1            0x2A0015
#define cLineE3Threshold2_2            0x18
#define cLineE3Threshold2_3            0x2300028
#define cLineE3Threshold2_4            0x2300028
#define cLineE3Threshold2_5            0x2300028
#define cLineE3Threshold2_6            0x2300028
#define cLineE3Threshold2_7            0x2300028

#define cLineDs1Threshold1             0x0E1E0815
#define cLineDs1Threshold2_0           0x20001
#define cLineDs1Threshold2_1           0x20001
#define cLineDs1Threshold2_2           0x2
#define cLineDs1Threshold2_3           0x560009
#define cLineDs1Threshold2_4           0x560009
#define cLineDs1Threshold2_5           0x560009
#define cLineDs1Threshold2_6           0x560009
#define cLineDs1Threshold2_7           0x560009

#define cLineE1Threshold1              0x0E1E0815
#define cLineE1Threshold2_0            0x40002
#define cLineE1Threshold2_1            0x40002
#define cLineE1Threshold2_2            0x3
#define cLineE1Threshold2_3            0x76000B
#define cLineE1Threshold2_4            0x76000B
#define cLineE1Threshold2_5            0x76000B
#define cLineE1Threshold2_6            0x76000B
#define cLineE1Threshold2_7            0x76000B


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleBer *Tha60210011ModuleBer;

typedef struct tTha60210011ModuleBerMethods
    {
    eAtRet   (*BerEnable)(Tha60210011ModuleBer self, eBool enable);
    void   (*DefaultSet)(Tha60210011ModuleBer self);
    uint32 (*Threshold1AddressByRate)(Tha60210011ModuleBer self, uint32 rate);
    uint32 (*Threshold2AddressByRate)(Tha60210011ModuleBer self, uint32 rate);
    uint32 (*BerStsTu3ReportBaseAddress)(Tha60210011ModuleBer self);
    uint32 (*Imemrwptrsh1Base)(Tha60210011ModuleBer self);
    uint32 (*Imemrwpctrl2Base)(Tha60210011ModuleBer self);
    uint32 (*Imemrwpctrl1Base)(Tha60210011ModuleBer self);
    uint32 (*Threshold1Offset)(Tha60210011ModuleBer self, uint32 rate);
    uint32 (*Vc1xBerControlOffset)(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts);
    uint32 (*Vc1xCurrentBerOffset)(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts);
    uint32 (*De3BerControlOffset)(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts);
    uint32 (*De3CurrentBerOffset)(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts);
    uint32 (*De1BerControlOffset)(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id);
    uint32 (*De1CurrentBerOffset)(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id, uint8 tu1xId);
    eAtRet (*LinePohHwId)(Tha60210011ModuleBer self, AtSdhLine line, uint8 *slice, uint8 *hwSts);
    uint32 (*LineBerControlOffset)(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts);
    uint32 (*LineCurrentBerOffset)(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts);
    uint32 (*StsBerControlOffset)(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts);
    uint32 (*StsCurrentBerOffset)(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts);
    eBool  (*HasStandardClearTime)(Tha60210011ModuleBer self);
    uint32 (*BerMeasureStsChannelOffset)(Tha60210011ModuleBer self);
    }tTha60210011ModuleBerMethods;

typedef struct tTha60210011ModuleBer
    {
    tThaModuleHardBer super;
    const tTha60210011ModuleBerMethods *methods;
    }tTha60210011ModuleBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60210011SdhVc1xBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011SdhAuVcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011SdhTu3VcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011PdhDe3BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

AtModuleBer Tha60210011ModuleBerObjectInit(AtModuleBer self, AtDevice device);
uint32 Tha60210011RegThreshold1AddressByRate(Tha60210011ModuleBer self, uint32 rate);
uint32 Tha60210011RegThreshold2AddressByRate(Tha60210011ModuleBer self, uint32 rate);
uint32 Tha60210011RegBerStsTu3ReportBaseAddress(Tha60210011ModuleBer self);
uint32 Tha60210011ModuleBerImemrwpctrl2Base(Tha60210011ModuleBer self);
eAtRet Tha60210011ModuleBerLinePohHwId(Tha60210011ModuleBer self, AtSdhLine line, uint8 *slice, uint8 *hwSts);
uint32 Tha60210011ModuleBerVc1xBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerVc1xCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerDe3BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerDe3CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerLineBerControlOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerLineCurrentBerOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerStsBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerStsCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts);
uint32 Tha60210011ModuleBerImemrwpctrl1Base(Tha60210011ModuleBer self);
uint32 Tha60210011ModuleBerDe1BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id);
uint32 Tha60210011ModuleBerDe1CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id, uint8 tu1xId);
eAtRet Tha60210011ModuleBerEnable(Tha60210011ModuleBer self, eBool enable);
uint32 Tha60210011BerMeasureStsChannelOffset(Tha60210011ModuleBer self);

uint8 Tha60210011BerRate2HwValue(eAtBerRate rate);
eAtBerRate Tha60210011HwValue2BerRate(uint8 value);
void Tha60210011ModuleBerSts1ThresholdInit(Tha60210011ModuleBer self);
void Tha60210011ModuleBerVc1xThresholdInit(Tha60210011ModuleBer self);
void Tha60210011ModuleBerE1ThresholdInit(Tha60210011ModuleBer self);
void Tha60210011ModuleBerDs1ThresholdInit(Tha60210011ModuleBer self);
void Tha60210011ModuleBerDs3ThresholdInit(Tha60210011ModuleBer self);
void Tha60210011ModuleBerE3ThresholdInit(Tha60210011ModuleBer self);

void Tha60210011ModuleBerThresholdPrint(Tha60210011ModuleBer self, const char* rate, uint32 thresh1, uint32 thresh2);
void Tha60210011ModuleBerIsEnabledPrint(Tha60210011ModuleBer self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEBER_H_ */


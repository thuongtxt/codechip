/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210011SdhAuVcBerControllerInternal.h
 * 
 * Created Date: Jun 9, 2015
 *
 * Description : BER controller of AU-VC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011SDHAUVCCONTROLLERINTERNAL_H_
#define _THA60210011SDHAUVCCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/ber/AtBerControllerInternal.h"
#include "../../../../generic/ber/AtModuleBerInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "Tha60210011ModuleBer.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011SdhAuVcBerController * Tha60210011SdhAuVcBerController;
typedef struct tTha60210011SdhVc1xBerController * Tha60210011SdhVc1xBerController;

typedef struct tTha60210011SdhAuVcBerControllerMethods
    {
    uint32 (*HwChannelTypeValue)(AtBerController self);
    uint32 (*BerControlOffset)(Tha60210011SdhAuVcBerController self);
    uint32 (*CurrentBerOffset)(Tha60210011SdhAuVcBerController self);
    eBool (*ThresholdIsSupported)(Tha60210011SdhAuVcBerController self, eAtBerRate threshold);
    }tTha60210011SdhAuVcBerControllerMethods;

typedef struct tTha60210011SdhAuVcBerController
    {
    tAtBerController super;
    const tTha60210011SdhAuVcBerControllerMethods *methods;
    }tTha60210011SdhAuVcBerController;

typedef struct tTha60210011SdhVc1xBerControllerMethods
    {
    uint8 (*ChannelId)(Tha60210011SdhVc1xBerController self);
    uint32 (*BerReportRegisterAddress)(Tha60210011SdhVc1xBerController self);
    uint32 (*BerControlRegisterAddress)(Tha60210011SdhVc1xBerController self);
    }tTha60210011SdhVc1xBerControllerMethods;

typedef struct tTha60210011SdhVc1xBerController
    {
    tTha60210011SdhAuVcBerController super;
    const tTha60210011SdhVc1xBerControllerMethods *methods;
    }tTha60210011SdhVc1xBerController;

typedef struct tTha60210011PdhDe1BerController
    {
    tTha60210011SdhVc1xBerController super;
    }tTha60210011PdhDe1BerController;

typedef struct tTha60210011SdhTu3VcBerController
    {
    tTha60210011SdhAuVcBerController super;
    }tTha60210011SdhTu3VcBerController;

typedef struct tTha60210011PdhDe3BerController
    {
    tTha60210011SdhTu3VcBerController super;
    }tTha60210011PdhDe3BerController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60210011SdhAuVcBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011SdhVc1xBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011PdhDe1BerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011SdhTu3VcBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210011PdhDe3BerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

uint32 Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(AtBerController self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011SDHAUVCCONTROLLERINTERNAL_H_ */


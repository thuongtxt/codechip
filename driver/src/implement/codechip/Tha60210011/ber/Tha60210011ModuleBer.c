/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210011ModuleBer.c
 *
 * Created Date: Jun 9, 2015
 *
 * Description : Module BER of 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/ber/ThaBerMeasureTime.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../poh/Tha60210011PohReg.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../man/Tha60210011Device.h"
#include "Tha60210011ModuleBer.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleBer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleBerMethods m_methods;

/* Override */
static tThaModuleHardBerMethods     m_ThaModuleHardBerOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleBerMethods          m_AtModuleBerOverride;

/* Cache super */
static const tAtModuleMethods      *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtBerController AuVcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel auVc)
    {
    return Tha60210011SdhAuVcBerControllerNew(AtChannelIdGet((AtChannel)auVc), (AtChannel)auVc, (AtModuleBer)self);
    }

static AtBerController Tu3VcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel tu3Vc)
    {
    return Tha60210011SdhTu3VcBerControllerNew(AtChannelIdGet((AtChannel)tu3Vc), (AtChannel)tu3Vc, (AtModuleBer)self);
    }

static AtBerController Vc1xBerControllerCreate(ThaModuleHardBer self, AtSdhChannel vc1x)
    {
    return Tha60210011SdhVc1xBerControllerNew(AtChannelIdGet((AtChannel)vc1x), (AtChannel)vc1x, (AtModuleBer)self);
    }

static eAtRet BerEnable(Tha60210011ModuleBer self, eBool enable)
    {
    uint32 address = cAf6Reg_pcfg_glbenb_Base + AtModuleBerBaseAddress((AtModuleBer)self);
    uint32 regVal = mModuleHwRead(self, address);
    uint8 enBit = (uint8) mBoolToBin(enable);

    /* Enable ber for DSn, VT, STS */
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_timerenb_, enBit);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_dsnsenb_, enBit);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_vtenb_,   enBit);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_stsenb_,  enBit);

    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static uint32 Threshold1AddressByRate(Tha60210011ModuleBer self, uint32 rate)
    {
    uint32 base = mMethodsGet(self)->Imemrwptrsh1Base(self);
    uint32 rateOffset = mMethodsGet(self)->Threshold1Offset(self, rate);
    return base + rateOffset + AtModuleBerBaseAddress((AtModuleBer)self);
    }

static uint32 Threshold2AddressByRate(Tha60210011ModuleBer self, uint32 rate)
    {
    return cAf6Reg_imemrwptrsh2_Base + rate * 8 + AtModuleBerBaseAddress((AtModuleBer)self);
    }

static void BerSts1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts1Threshold1Rate),     0x3A70);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate),     0x420022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 1, 0x420022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 2, 0x23);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 3, 0x3020037);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 4, 0x3380038);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 5, 0x33E0038);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 6, 0x3400038);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 7, 0x3400038);
    }

static void BerSts3cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts3Threshold1Rate),     0x8A8F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate),     0xE20073);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 1, 0xE80074);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 2, 0x67);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 3, 0x80C0096);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 4, 0x9DC009A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 5, 0xA12009A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 6, 0xA18009A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 7, 0xA18009A);
    }

static void BerSts6cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts6Threshold1Rate),     0xDC91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate),     0x1D000F2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 1, 0x1E600F3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 2, 0xAF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 3, 0xD42011C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 4, 0x138A0128);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 5, 0x14600129);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 6, 0x14760129);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 7, 0x14780129);
    }

static void BerSts9cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts9Threshold1Rate),     0x10C91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate),     0x2B60172);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 1, 0x2E80174);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 2, 0xDD);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 3, 0x10880199);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 4, 0x1CD001B3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 5, 0x1EAC01B6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 6, 0x1EDE01B7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 7, 0x1EE201B7);
    }

static void BerVc1xThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    /* VC11 */
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cVt15Threshold1Rate),     0x80F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 2, 0x2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 3, 0x5A0009);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 4, 0x5C000A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 5, 0x5E000A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 6, 0x5E000A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 7, 0x5E000A);

    /* VC12 */
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cVt2Threshold1Rate),     0xE14);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate),     0x60003);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 1, 0x60003);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 2, 0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 3, 0x7C000C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 4, 0x82000C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 5, 0x82000C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 6, 0x82000C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 7, 0x82000C);
    }

static void BerSts12cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts12Threshold1Rate),     0x12891);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate),     0x39401F2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 1, 0x3EC01F6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 2, 0xFA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 3, 0x12940226);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 4, 0x25AE023E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 5, 0x28F20242);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 6, 0x294C0243);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 7, 0x29540243);
    }

static void BerSts15cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts15Threshold1Rate),     0x13A91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate),     0x4680272);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 1, 0x4F00279);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 2, 0x10C  );
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 3, 0x13DC0281);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 4, 0x2E2802C6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 5, 0x333002CE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 6, 0x33BC02CF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 7, 0x33CA02CF);
    }

static void BerSts18cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts18Threshold1Rate),     0x14691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate),     0x53402F2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 1, 0x5F602FC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 2, 0x118);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 3, 0x14AA02EC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 4, 0x3640034E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 5, 0x3D640359);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 6, 0x3E2C035A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 7, 0x3E40035A);
    }

static void BerSts21cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts21Threshold1Rate),     0x14E91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate),     0x5F60371);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 1, 0x6FE0380);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 2, 0x11F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 3, 0x152C0352);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 4, 0x3DFC03D5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 5, 0x478E03E4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 6, 0x489E03E5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 7, 0x48BA03E5);
    }

static void BerSts24cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts24Threshold1Rate),     0x15291);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate),     0x6B003F1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 1, 0x8040404);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 2, 0x123);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 3, 0x157C03B3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 4, 0x455E045B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 5, 0x51AE046E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 6, 0x53120470);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 7, 0x53360470);
    }

static void BerSts27cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts27Threshold1Rate),     0x15491);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate),     0x7620470);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 1, 0x90A0488);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 2, 0x126);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 3, 0x15AE0410);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 4, 0x4C6A04E0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 5, 0x5BC204F8);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 6, 0x5D8404FA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 7, 0x5DB204FB);
    }

static void BerSts30cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts30Threshold1Rate),     0x15691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate),     0x80C04EE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 1, 0xA12050C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 2, 0x128);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 3, 0x15CE0468);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 4, 0x53240564);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 5, 0x65CC0582);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 6, 0x67F60585);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 7, 0x682E0585);
    }

static void BerSts33cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts33Threshold1Rate),      0x15891);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate),      0x8B0056C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 1,  0xB1A0590);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 2,  0x129);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 3,  0x15E204BC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 4,  0x599005E8);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 5,  0x6FCA060B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 6,  0x7268060F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 7,  0x72AC060F);
    }

static void BerSts36cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts36Threshold1Rate),     0x15891);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate),     0x94A05EA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 1, 0xC200614);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 3, 0x15EE050C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 4, 0x5FB0066B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 5, 0x79BE0695);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 6, 0x7CDA0699);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 7, 0x7D2C0699);
    }

static void BerSts39cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts39Threshold1Rate),     0x15891);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate),     0x9DE0667);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 1, 0xD280699);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 3, 0x15F60558);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 4, 0x658A06ED);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 5, 0x83A6071E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 6, 0x874C0723);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 7, 0x87AA0723);
    }

static void BerSts42cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts42Threshold1Rate),     0x15891);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate),     0xA6C06E3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 1, 0xE30071E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 3, 0x15FA05A1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 4, 0x6B1E076E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 5, 0x8D8207A7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 6, 0x91BC07AD);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 7, 0x922A07AD);
    }

static void BerSts45cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts45Threshold1Rate),     0x15A91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate),     0xAF40760);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 1, 0xF3807A2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 3, 0x15FE05E6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 4, 0x707207EF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 5, 0x97540830);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 6, 0x9C2C0836);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 7, 0x9CAA0837);
    }

static void BerSts48cThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts48Threshold1Rate),     0x15A91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate),     0xB7407DB);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 1, 0x10400827);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 2, 0x12B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 3, 0x16000628);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 4, 0x7588087F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 5, 0xA11A08B8);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 6, 0xA69C08C0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 7, 0xA72C08C1);
    }

static void BerDs1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cDs1Threshold1Rate),     0x805);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 2, 0x1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 3, 0x460009);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 4, 0x520009);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 5, 0x540009);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 6, 0x540009);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 7, 0x540009);
    }

static void BerE1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE1Threshold1Rate),     0xA0D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate),     0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 1, 0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 2, 0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 3, 0x64000B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 4, 0x6C000B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 5, 0x6E000B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 6, 0x6E000B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 7, 0x6E000B);
    }

static void BerE3G832ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE3G832Threshold1Rate),     0x2A5B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate),     0x2A0015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 1, 0x2A0015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 2, 0x17);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 3, 0x2120027);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 4, 0x22C0028);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 5, 0x2300028);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 6, 0x2300028);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 7, 0x2300028);
    }

static void BerDs3ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cDs3Threshold1Rate),     0x2664);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate),     0x38001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 1, 0x3A001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 2, 0x15);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 3, 0x1D6002F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 4, 0x2B80031);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 5, 0x2D60032);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 6, 0x2D80032);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 7, 0x2DA0032);
    }

static void BerE3G751ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE3G751Threshold1Rate),     0x2A68);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate),     0x1C000E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 1, 0x1C000E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 2, 0x17);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 3, 0x21A001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 4, 0x22E001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 5, 0x230001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 6, 0x230001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 7, 0x230001D);
    }

static void BerSts1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts1Threshold1Rate),     0x1D070);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate),     0x420022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 1, 0x420022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 2, 0x23);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 3, 0x30201CC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 4, 0x33801E3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 5, 0x33E01E6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 6, 0x34001E6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts1Threshold2Rate) + 7, 0x34001E6);
    }

static void BerSts3cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts3cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts3Threshold1Rate),     0x2508F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate),     0xE20073);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 1, 0xE80074);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 2, 0x67);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 3, 0x9740550);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 4, 0x9DC0565);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 5, 0xA120583);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 6, 0xA180586);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts3Threshold2Rate) + 7, 0xA180586);
    }

static void BerSts6cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts6cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts6Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate),     0x1D000F2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 1, 0x1E600F3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 2, 0xAF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 3, 0xD420715);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 4, 0x147C0AE6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 5, 0x14600ADB);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 6, 0x14760AE7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts6Threshold2Rate) + 7, 0x14780AE8);
    }

static void BerSts9cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts9cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts9Threshold1Rate),     0x2508F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate),     0x2B60172);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 1, 0x2E80174);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 2, 0xDD);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 3, 0x108808BE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 4, 0x1EEA1041);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 5, 0x1EAC1027);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 6, 0x1EDE1041);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts9Threshold2Rate) + 7, 0x1EE21044);
    }

static void BerVc1xThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerVc1xThresholdStandardClearTimeInit(self);
        return;
        }

    /* VC11 */
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cVt15Threshold1Rate),     0x460F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 2, 0x2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 3, 0x5A0046);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 4, 0x5C0049);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 5, 0x5E0049);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 6, 0x5E0049);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt15Threshold2Rate) + 7, 0x5E0049);

    /* VC12 */
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cVt2Threshold1Rate),     0x5814);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate),     0x60003);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 1, 0x60003);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 2, 0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 3, 0x7C005B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 4, 0x82005F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 5, 0x820060);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 6, 0x820060);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cVt2Threshold2Rate) + 7, 0x820060);
    }

static void BerSts12cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts12cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts12Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate),     0x39401F2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 1, 0x3EC01F6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 2, 0xFA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 3, 0x129409C6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 4, 0x25AE1554);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 5, 0x28F2156A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 6, 0x294C1598);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts12Threshold2Rate) + 7, 0x2954159D);
    }

static void BerSts15cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts15cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts15Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate),     0x4680272);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 1, 0x4F00279);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 2, 0x10C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 3, 0x13DC0A6C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 4, 0x33D81AEE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 5, 0x33301AA5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 6, 0x33BC1AED);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts15Threshold2Rate) + 7, 0x33CA1AF4);
    }

static void BerSts18cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts18cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts18Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate),     0x53402F2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 1, 0x5F602FC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 2, 0x118);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 3, 0x14AA0AD3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 4, 0x3E562041);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 5, 0x3D641FD7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 6, 0x3E2C203F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts18Threshold2Rate) + 7, 0x3E40204A);
    }

static void BerSts21cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts21cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts21Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate),     0x5F60371);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 1, 0x6FE0380);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 2, 0x11F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 3, 0x152C0B13);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 4, 0x48D42594);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 5, 0x478E2503);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 6, 0x489E2590);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts21Threshold2Rate) + 7, 0x48BA259E);
    }

static void BerSts24cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts24cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts24Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate),     0x6B003F1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 1, 0x8040404);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 2, 0x123);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 3, 0x157C0B3C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 4, 0x53542AE6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 5, 0x533C2AF2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 6, 0x53122ADF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts24Threshold2Rate) + 7, 0x53362AF2);
    }

static void BerSts27cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts27cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts27Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate),     0x7620470);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 1, 0x90A0488);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 2, 0x126);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 3, 0x15AE0B55);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 4, 0x5DD63037);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 5, 0x5DBA3045);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 6, 0x5D84302D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts27Threshold2Rate) + 7, 0x5DB23045);
    }

static void BerSts30cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts30cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts30Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate),     0x80C04EE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 1, 0xA12050C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 2, 0x128);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 3, 0x15CE0B65);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 4, 0x685A3587);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 5, 0x68383598);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 6, 0x67F6357A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts30Threshold2Rate) + 7, 0x682E3597);
    }

static void BerSts33cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts33cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts33Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate),     0x8B0056C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 1, 0xB1A0590);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 2, 0x129);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 3, 0x15E20B6F);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 4, 0x72DE3AD7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 5, 0x72B83AEA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 6, 0x72683AC6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts33Threshold2Rate) + 7, 0x72AC3AE9);
    }

static void BerSts36cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts36cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts36Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate),     0x94A05EA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 1, 0xC200614);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 3, 0x15EE0B75);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 4, 0x7D644026);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 5, 0x7D3A403B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 6, 0x7CDA4010);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts36Threshold2Rate) + 7, 0x7D2C403A);
    }

static void BerSts39cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts39cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts39Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate),     0x9DE0667);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 1, 0xD280699);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 3, 0x15F60B79);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 4, 0x87EA4575);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 5, 0x87BC458C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 6, 0x874C455A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts39Threshold2Rate) + 7, 0x87AA458B);
    }

static void BerSts42cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts42cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts42Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate),     0xA6C06E3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 1, 0xE30071E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 3, 0x15FA0B7B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 4, 0x92704AC4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 5, 0x923E4ADD);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 6, 0x91BC4AA2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts42Threshold2Rate) + 7, 0x922A4ADB);
    }


static void BerSts45cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts45cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts45Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate),     0xAF40760);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 1, 0xF3807A2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 2, 0x12A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 3, 0x15FE0B7C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 4, 0x9CF85012);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 5, 0x9CC0502E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 6, 0x9C2C4FEA);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts45Threshold2Rate) + 7, 0x9CAA502B);
    }

static void BerSts48cThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerSts48cThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts48Threshold1Rate),     0x25691);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate),     0xB7407DB);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 1, 0x10400827);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 2, 0x12B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 3, 0x16000B7D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 4, 0xA7805560);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 5, 0xA744557E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 6, 0xA69C5531);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts48Threshold2Rate) + 7, 0xA72C557B);
    }

static void BerDs1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerDs1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cDs1Threshold1Rate),     0x1805);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 2, 0x1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 3, 0x460040);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 4, 0x520042);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 5, 0x540043);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 6, 0x540043);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 7, 0x540043);
    }

static void BerE1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerE1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE1Threshold1Rate),     0x380D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate),     0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 1, 0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 2, 0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 3, 0x64004b);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 4, 0x6C0052);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 5, 0x6E0053);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 6, 0x6E0053);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 7, 0x6E0053);
    }

static void BerDs3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerDs3ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cDs3Threshold1Rate),     0x19064);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate),     0x38001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 1, 0x3A001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 2, 0x15);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 3, 0x2B8019C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 4, 0x2B8019C);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 5, 0x2D601AD);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 6, 0x2D801AF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs3Threshold2Rate) + 7, 0x2DA01AF);
    }

static void BerE3G832ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerE3G832ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE3G832Threshold1Rate),     0x17E5B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate),     0x2A0015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 1, 0x2A0015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 2, 0x17);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 3, 0x2120141);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 4, 0x22C0151);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 5, 0x2300153);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 6, 0x2300153);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G832Threshold2Rate) + 7, 0x2300153);
    }

static void BerE3G751ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerE3G751ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE3G751Threshold1Rate),     0x1B468);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate),     0x1C000E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 1, 0x1C000E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 2, 0x17);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 3, 0x21A0146);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 4, 0x22E0151);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 5, 0x2300153);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 6, 0x2300153);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE3G751Threshold2Rate) + 7, 0x2300153);
    }

static void DefaultSet(Tha60210011ModuleBer self)
    {
    BerSts1ThresholdInit(self);
    BerSts3cThresholdInit(self);
    BerSts6cThresholdInit(self);
    BerSts9cThresholdInit(self);
    BerVc1xThresholdInit(self);
    BerSts12cThresholdInit(self);
    BerSts15cThresholdInit(self);
    BerSts18cThresholdInit(self);
    BerSts21cThresholdInit(self);
    BerSts24cThresholdInit(self);
    BerSts27cThresholdInit(self);
    BerSts30cThresholdInit(self);
    BerSts33cThresholdInit(self);
    BerSts36cThresholdInit(self);
    BerSts39cThresholdInit(self);
    BerSts42cThresholdInit(self);
    BerSts45cThresholdInit(self);
    BerSts48cThresholdInit(self);
    BerDs1ThresholdInit(self);
    BerE1ThresholdInit(self);
    BerDs3ThresholdInit(self);
    BerE3G832ThresholdInit(self);
    BerE3G751ThresholdInit(self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = mMethodsGet(mThis(self))->BerEnable(mThis(self), cAtTrue);
    mMethodsGet(mThis(self))->DefaultSet(mThis(self));

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void IsEnabledPrint(Tha60210011ModuleBer self)
    {
    uint32 address = cAf6Reg_pcfg_glbenb_Base + AtModuleBerBaseAddress((AtModuleBer)self);
    uint32 regVal = mModuleHwRead(self, address);

    AtPrintc(cSevNormal, "- BER enable status: ");
    AtPrintc(cSevInfo, "STS %s", (regVal & cAf6_pcfg_glbenb_dsnsenb_Mask) ? "enabled" : "disabled");
    AtPrintc(cSevInfo, ", VT %s", (regVal & cAf6_pcfg_glbenb_vtenb_Mask) ? "enabled" : "disabled");
    AtPrintc(cSevInfo, ", DSnEn %s\r\n", (regVal & cAf6_pcfg_glbenb_stsenb_Mask) ? "enabled" : "disabled");
    }

static void ThresholdPrintByAddress(Tha60210011ModuleBer self, uint32 address)
    {
    uint32 value = mModuleHwRead(self, address);
    AtPrintc(cSevNormal, "      0x%08x: 0x%08x\r\n", address, value);
    }

static void ThresholdPrint(Tha60210011ModuleBer self, const char* rate, uint32 thresh1, uint32 thresh2)
    {
    AtPrintc(cSevInfo, "\r\n- %s threshold:\r\n", rate);
    AtPrintc(cSevInfo, "  + Threshold 1:\r\n");
    ThresholdPrintByAddress(self, Tha60210011RegThreshold1AddressByRate(self, thresh1));

    AtPrintc(cSevInfo, "  + Threshold 2:\r\n");
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2));
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 1);
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 2);
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 3);
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 4);
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 5);
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 6);
    ThresholdPrintByAddress(self, Tha60210011RegThreshold2AddressByRate(self, thresh2) + 7);
    }

static eAtRet Debug(AtModule self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    m_AtModuleMethods->Debug(self);

    Tha60210011ModuleBerIsEnabledPrint(module);
    Tha60210011ModuleBerThresholdPrint(module, "VT-15",  cVt15Threshold1Rate , cVt15Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "VT-2",   cVt2Threshold1Rate  , cVt2Threshold2Rate  );
    Tha60210011ModuleBerThresholdPrint(module, "E1",     cE1Threshold1Rate   , cE1Threshold2Rate   );
    Tha60210011ModuleBerThresholdPrint(module, "DS1",    cDs1Threshold1Rate  , cDs1Threshold2Rate  );
    Tha60210011ModuleBerThresholdPrint(module, "STS-1",  cSts1Threshold1Rate , cSts1Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "STS-3",  cSts3Threshold1Rate , cSts3Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "STS-6",  cSts6Threshold1Rate , cSts6Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "STS-9",  cSts9Threshold1Rate , cSts9Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "STS-12", cSts12Threshold1Rate, cSts12Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-15", cSts15Threshold1Rate, cSts15Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-18", cSts18Threshold1Rate, cSts18Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-21", cSts21Threshold1Rate, cSts21Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-24", cSts24Threshold1Rate, cSts24Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-27", cSts27Threshold1Rate, cSts27Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-30", cSts30Threshold1Rate, cSts30Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-33", cSts33Threshold1Rate, cSts33Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-36", cSts36Threshold1Rate, cSts36Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-39", cSts39Threshold1Rate, cSts39Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-42", cSts42Threshold1Rate, cSts42Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-45", cSts45Threshold1Rate, cSts45Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "STS-48", cSts48Threshold1Rate, cSts48Threshold2Rate);
    Tha60210011ModuleBerThresholdPrint(module, "DS3",    cDs3Threshold1Rate  , cDs3Threshold2Rate  );

    return cAtOk;
    }

static AtBerController De1PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    return Tha60210011PdhDe1BerControllerNew(AtChannelIdGet((AtChannel)de1), (AtChannel)de1, (AtModuleBer)self);
    }

static AtBerController De3PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    return Tha60210011PdhDe3BerControllerNew(AtChannelIdGet((AtChannel)de3), (AtChannel)de3, (AtModuleBer)self);
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsBerRegister(AtModuleDeviceGet(self), address);
    }

static uint32 BerStsTu3ReportBaseAddress(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_ramberrateststu3_Base;
    }

static eBool ChannelBerShouldBeEnabledByDefault(AtModuleBer self)
    {
    /* To enforce LAB testing */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 Imemrwpctrl1Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwpctrl1_Base;
    }

static uint32 Imemrwptrsh1Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwptrsh1_Base;
    }

static uint32 Imemrwpctrl2Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwpctrl2_Base;
    }

static uint32 Threshold1Offset(Tha60210011ModuleBer self, uint32 rate)
    {
    AtUnused(self);
    return (rate & cBit2_0) * 8 + ((rate & cBit6_3) >> 3) * 128;
    }

static uint32 Vc1xBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    return (hwSts * 128UL + slice * 8UL + AtSdhChannelTug2Get(vc1x));
    }

static uint32 De3BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(de3);
    return (hwSts * 128UL) + (slice + 4UL) * 8UL;
    }

static uint32 LineBerControlOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(slice);
    AtUnused(hwSts);
    return cInvalidUint32;
    }

static uint32 LineCurrentBerOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(slice);
    AtUnused(hwSts);
    return cInvalidUint32;
    }

static eAtRet LinePohHwId(Tha60210011ModuleBer self, AtSdhLine line, uint8 *slice, uint8 *hwSts)
    {
    AtUnused(self);
    AtUnused(line);
    *slice = cInvalidUint8;
    *hwSts = cInvalidUint8;
    return cAtErrorNotImplemented;
    }

static uint32 StsBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return hwSts * 128UL + slice * 8UL;
    }

static uint32 Vc1xCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    return hwSts * 336UL + slice * 28UL + AtSdhChannelTug2Get(vc1x) * 4UL + AtSdhChannelTu1xGet(vc1x);
    }

static uint32 De3CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(de3);
    return (hwSts * 16UL) +  ((slice + 4UL) * 2UL) + 1;
    }

static uint32 StsCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (hwSts * 16UL + slice * 2UL);
    }

static uint32 De1BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id)
    {
    AtUnused(self);
    AtUnused(de1);
    return (hwSts * 128UL + (slice + 6UL) * 8UL + tug2Id);
    }

static uint32 De1CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id, uint8 tu1xId)
    {
    AtUnused(self);
    AtUnused(de1);
    return hwSts * 336UL + (slice + 6UL) * 28UL + tug2Id * 4UL + tu1xId;
    }

static eBool HasStandardClearTime(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 BerMeasureStsChannelOffset(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return 5632UL;
    }

static uint32 PeriodProcess(AtModuleBer self, uint32 periodInMs)
    {
    tAtOsalCurTime startTime, endTime;
    uint32 elapseTime;
    AtBerMeasureTimeEngine engine = AtModuleBerMeasureTimeEngineGet(self);

    AtUnused(periodInMs);

    if (engine == NULL)
        return 0;

    AtModuleLock((AtModule)self);

    AtOsalCurTimeGet(&startTime);
    AtBerMeasureTimeEnginePeriodProcess(engine);
    AtOsalCurTimeGet(&endTime);
    elapseTime = mTimeIntervalInMsGet(startTime, endTime);

    AtModuleUnLock((AtModule)self);
    return elapseTime;
    }

static AtBerMeasureTimeEngine MeasureTimeHardEngineObjectCreate(AtModuleBer self, AtBerController controller)
    {
    AtUnused(self);
    return ThaBerMeasureTimeHardEngineNew(controller);
    }

static AtBerMeasureTimeEngine MeasureTimeSoftEngineObjectCreate(AtModuleBer self, AtBerController controller)
    {
    AtUnused(self);
    return ThaBerMeasureTimeSoftEngineNew(controller);
    }

static eBool MeasureTimeEngineIsSupported(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    return mMethodsGet(module)->HasStandardClearTime(module);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, ChannelBerShouldBeEnabledByDefault);
        mMethodOverride(m_AtModuleBerOverride, PeriodProcess);
        mMethodOverride(m_AtModuleBerOverride, MeasureTimeHardEngineObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, MeasureTimeSoftEngineObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, MeasureTimeEngineIsSupported);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void OverrideThaModuleHardBer(AtModuleBer self)
    {
    ThaModuleHardBer module = (ThaModuleHardBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardBerOverride, mMethodsGet(module), sizeof(m_ThaModuleHardBerOverride));

        mMethodOverride(m_ThaModuleHardBerOverride, AuVcBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, Tu3VcBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, Vc1xBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De1PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3PathBerControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleHardBerOverride);
    }

static void OverrideAtModule(AtModuleBer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideThaModuleHardBer(self);
    OverrideAtModule(self);
    OverrideAtModuleBer(self);
    }

static void MethodsInit(AtModuleBer self)
    {
    Tha60210011ModuleBer module = mThis(self);
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, BerEnable);
        mMethodOverride(m_methods, Threshold1AddressByRate);
        mMethodOverride(m_methods, Threshold2AddressByRate);
        mMethodOverride(m_methods, BerStsTu3ReportBaseAddress);
        mMethodOverride(m_methods, Imemrwptrsh1Base);
        mMethodOverride(m_methods, Imemrwpctrl2Base);
        mMethodOverride(m_methods, Imemrwpctrl1Base);
        mMethodOverride(m_methods, Threshold1Offset);
        mMethodOverride(m_methods, Vc1xBerControlOffset);
        mMethodOverride(m_methods, De3BerControlOffset);
        mMethodOverride(m_methods, LineBerControlOffset);
        mMethodOverride(m_methods, LineCurrentBerOffset);
        mMethodOverride(m_methods, LinePohHwId);
        mMethodOverride(m_methods, StsBerControlOffset);
        mMethodOverride(m_methods, Vc1xCurrentBerOffset);
        mMethodOverride(m_methods, De3CurrentBerOffset);
        mMethodOverride(m_methods, StsCurrentBerOffset);
        mMethodOverride(m_methods, De1BerControlOffset);
        mMethodOverride(m_methods, De1CurrentBerOffset);
        mMethodOverride(m_methods, HasStandardClearTime);
        mMethodOverride(m_methods, BerMeasureStsChannelOffset);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleBer);
    }

AtModuleBer Tha60210011ModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleHardBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60210011ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleBerObjectInit(newBerModule, device);
    }

uint8 Tha60210011BerRate2HwValue(eAtBerRate rate)
    {
    switch ((uint32) rate)
        {
        case cAtBerRate1E3: return 1;
        case cAtBerRate1E4: return 2;
        case cAtBerRate1E5: return 3;
        case cAtBerRate1E6: return 4;
        case cAtBerRate1E7: return 5;
        case cAtBerRate1E8: return 6;
        case cAtBerRate1E9: return 7;

        case cAtBerRateUnknown:
        case cAtBerRate1E10:
        default:
            return 0;
        }
    }

eAtBerRate Tha60210011HwValue2BerRate(uint8 value)
    {
    switch (value)
        {
        case 1:  return cAtBerRate1E3;
        case 2:  return cAtBerRate1E4;
        case 3:  return cAtBerRate1E5;
        case 4:  return cAtBerRate1E6;
        case 5:  return cAtBerRate1E7;
        case 6:  return cAtBerRate1E8;
        case 7:  return cAtBerRate1E9;
        default: return cAtBerRate1E10;
        }
    }

void Tha60210011ModuleBerSts1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerSts1ThresholdInit(self);
    }

void Tha60210011ModuleBerVc1xThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerVc1xThresholdInit(self);
    }

void Tha60210011ModuleBerE1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerE1ThresholdInit(self);
    }

void Tha60210011ModuleBerDs1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerDs1ThresholdInit(self);
    }

void Tha60210011ModuleBerDs3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerDs3ThresholdInit(self);
    }

void Tha60210011ModuleBerE3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        {
        BerE3G832ThresholdInit(self);
        BerE3G751ThresholdInit(self);
        }
    }

uint32 Tha60210011RegThreshold1AddressByRate(Tha60210011ModuleBer self, uint32 rate)
    {
    if (self)
        return mMethodsGet(self)->Threshold1AddressByRate(self, rate);
    return cBit31_0;
    }

uint32 Tha60210011RegThreshold2AddressByRate(Tha60210011ModuleBer self, uint32 rate)
    {
    if (self)
        return mMethodsGet(self)->Threshold2AddressByRate(self, rate);
    return cBit31_0;
    }

uint32 Tha60210011RegBerStsTu3ReportBaseAddress(Tha60210011ModuleBer self)
    {
    if (self)
        return mMethodsGet(self)->BerStsTu3ReportBaseAddress(self);
    return cBit31_0;
    }

void Tha60210011ModuleBerIsEnabledPrint(Tha60210011ModuleBer self)
    {
    if (self)
        IsEnabledPrint(self);
    }

void Tha60210011ModuleBerThresholdPrint(Tha60210011ModuleBer self, const char* rate, uint32 thresh1, uint32 thresh2)
    {
    if (self)
        ThresholdPrint(self, rate, thresh1, thresh2);
    }

uint32 Tha60210011ModuleBerVc1xBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->Vc1xBerControlOffset(self, vc1x, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerDe3BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->De3BerControlOffset(self, de3, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerLineBerControlOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->LineBerControlOffset(self, line, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerImemrwpctrl2Base(Tha60210011ModuleBer self)
    {
    if (self)
        return mMethodsGet(self)->Imemrwpctrl2Base(self);
    return cInvalidUint32;
    }

eAtRet Tha60210011ModuleBerLinePohHwId(Tha60210011ModuleBer self, AtSdhLine line, uint8 *slice, uint8 *hwSts)
    {
    if (self)
        return mMethodsGet(self)->LinePohHwId(self, line, slice, hwSts);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210011ModuleBerStsBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->StsBerControlOffset(self, channel, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerVc1xCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->Vc1xCurrentBerOffset(self, vc1x, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerDe3CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->De3CurrentBerOffset(self, de3, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerStsCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->StsCurrentBerOffset(self, channel, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerLineCurrentBerOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    if (self)
        return mMethodsGet(self)->LineCurrentBerOffset(self, line, slice, hwSts);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerImemrwpctrl1Base(Tha60210011ModuleBer self)
    {
    if (self)
        return mMethodsGet(self)->Imemrwpctrl1Base(self);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerDe1BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id)
    {
    if (self)
        return mMethodsGet(self)->De1BerControlOffset(self, de1, slice, hwSts, tug2Id);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleBerDe1CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id, uint8 tu1xId)
    {
    if (self)
        return mMethodsGet(self)->De1CurrentBerOffset(self, de1, slice, hwSts, tug2Id, tu1xId);
    return cInvalidUint32;
    }

eAtRet Tha60210011ModuleBerEnable(Tha60210011ModuleBer self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->BerEnable(self, enable);

    return cAtErrorNullPointer;
    }

uint32 Tha60210011BerMeasureStsChannelOffset(Tha60210011ModuleBer self)
    {
    if (self)
        return mMethodsGet(self)->BerMeasureStsChannelOffset(self);
    return cBit31_0;
    }

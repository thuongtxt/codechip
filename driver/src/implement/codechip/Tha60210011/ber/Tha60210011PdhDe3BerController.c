/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210011SdhTu3VcBerController.c
 *
 * Created Date: Jun 9, 2015
 *
 * Description : BER controller of TU3-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60210011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                  m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods  m_Tha60210011SdhAuVcBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleBer BerModule(Tha60210011SdhAuVcBerController self)
    {
    return (Tha60210011ModuleBer)AtBerControllerModuleGet((AtBerController)self);
    }

static AtPdhChannel De3(Tha60210011SdhAuVcBerController self)
    {
    return (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtPdhChannel pdhChannel = De3(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerDe3BerControlOffset(BerModule(self), pdhChannel, slice, hwSts);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtPdhChannel pdhChannel = De3(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerDe3CurrentBerOffset(BerModule(self), pdhChannel, slice, hwSts);
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtChannel pdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)pdhChannel);
    switch (frameType)
        {
        case cAtPdhE3Frmg832: return 18;
        case cAtPdhE3FrmG751: return 19;
        default: return 17;
        }
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    AtChannel sdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return (hwSts * 2UL + (slice + 4UL) * 128UL + Tha60210011BerMeasureStsChannelOffset(module) + 1);
    }

static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmSdBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmSfBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride,  mMethodsGet(controller), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));
        
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(controller, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhDe3BerController);
    }

AtBerController Tha60210011PdhDe3BerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhTu3VcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210011PdhDe3BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PdhDe3BerControllerObjectInit(newController, controllerId, channel, berModule);
    }

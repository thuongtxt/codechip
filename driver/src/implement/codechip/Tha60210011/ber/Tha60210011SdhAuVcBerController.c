/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210011SdhAuVcBerController.c
 *
 * Created Date: Jun 9, 2015
 *
 * Description : SDH line BER controller of 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../poh/Tha60210011ModulePoh.h"
#include "AtChannel.h"
#include "Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60210011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011SdhAuVcBerController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011SdhAuVcBerControllerMethods m_methods;

/* Override */
static tAtBerControllerMethods m_AtBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ExpectedDetectionTimeInMs(AtBerController self, eAtBerRate rate)
    {
    AtUnused(self);
    switch ((uint32) rate)
        {
        case cAtBerRate1E3: return 10;
        case cAtBerRate1E4: return 100;
        case cAtBerRate1E5: return 1000;
        case cAtBerRate1E6: return 10000;
        case cAtBerRate1E7: return 100000;
        case cAtBerRate1E8: return 1000000;
        case cAtBerRate1E9: return 10000000;
        default:            return 0;
        }
    }

static uint32 ExpectedClearingTimeInMs(AtBerController self, eAtBerRate rate)
    {
    return ExpectedDetectionTimeInMs(self, rate);
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtBerControllerMonitoredChannel(self);

    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc4_64c)
        return cSts192Threshold1Rate;

    return AtSdhChannelNumSts(channel) / 3;
    }

static eBool IsRateOverSts12c(AtBerController self)
    {
    return (HwChannelTypeValue(self) > 4) ? cAtTrue : cAtFalse;
    }

static eBool ThresholdIsSupported(Tha60210011SdhAuVcBerController self, eAtBerRate threshold)
    {
    /* All rate is support if rate lower than STS-12c */
    if (!IsRateOverSts12c((AtBerController)self))
        return cAtTrue;

    /* Prevent user configure 10-3 if rate  from STS-15c to STS-48c */
    return (threshold == cAtBerRate1E3) ? cAtFalse : cAtTrue;
    }

static eAtRet Init(AtBerController self)
    {
    eAtRet ret = cAtOk;

    ret |= AtBerControllerSfThresholdSet(self, cAtBerRate1E10);
    ret |= AtBerControllerSdThresholdSet(self, cAtBerRate1E10);
    ret |= AtBerControllerTcaThresholdSet(self, cAtBerRate1E10);

    return ret;
    }

static uint32 Imemrwpctrl2Address(AtBerController self)
    {
    AtModuleBer berModule = (AtModuleBer)AtBerControllerModuleGet(self);
    uint32 baseAddress = AtModuleBerBaseAddress(berModule);
    uint32 regBase = Tha60210011ModuleBerImemrwpctrl2Base((Tha60210011ModuleBer)berModule);
    return regBase + mMethodsGet(mThis(self))->BerControlOffset(mThis(self)) + baseAddress;
    }

static eAtRet HwChannelTypeUpdate(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];
    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_rate1_, mMethodsGet(mThis(self))->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);
    return cAtOk;
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];
    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_ena1_, mBoolToBin(enable));
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_rate1_, mMethodsGet(mThis(self))->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return ((mRegField(regVal[0], cAf6_imemrwpctrl2_ena1_) == 1) ? cAtTrue : cAtFalse);
    }

static eAtRet SdThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    /* Check Input Threshold */
    if (!mMethodsGet(mThis(self))->ThresholdIsSupported(mThis(self), threshold))
        return cAtErrorModeNotSupport;

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_sdtrsh1_, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate SdThresholdGet(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[0], cAf6_imemrwpctrl2_sdtrsh1_));
    }

static eAtRet SfThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    /* Check Input Threshold */
    if (!mMethodsGet(mThis(self))->ThresholdIsSupported(mThis(self), threshold))
        return cAtErrorModeNotSupport;

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_sftrsh1_, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate SfThresholdGet(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[0], cAf6_imemrwpctrl2_sftrsh1_));
    }

static eAtRet TcaThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    /* Check Input Threshold */
    if (!mMethodsGet(mThis(self))->ThresholdIsSupported(mThis(self), threshold))
        return cAtErrorModeNotSupport;

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[1], cAf6_imemrwpctrl2_tcatrsh1_, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate TcaThresholdGet(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[1], cAf6_imemrwpctrl2_tcatrsh1_));
    }

static eAtBerRate CurBerGet(AtBerController self)
    {
    AtModuleBer module = (AtModuleBer)AtBerControllerModuleGet(self);
    uint32 baseAddress = AtModuleBerBaseAddress(module);
    uint32 address = Tha60210011RegBerStsTu3ReportBaseAddress((Tha60210011ModuleBer)module) +
                     mMethodsGet(mThis(self))->CurrentBerOffset(mThis(self)) + baseAddress;
    uint32 regVal = AtBerControllerRead(self, address);

    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal, cAf6_ramberrateststu3_rate_));
    }

static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhPathAlarmBerSd)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhPathAlarmBerSf)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhPathAlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SdHistoryGet(AtBerController self, eBool clear)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    uint32 channelInterrupt;

    if (clear)
        channelInterrupt = AtChannelAlarmInterruptClear(channel);
    else
        channelInterrupt = AtChannelAlarmInterruptGet(channel);

    if (channelInterrupt & cAtSdhPathAlarmBerSd)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SfHistoryGet(AtBerController self, eBool clear)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    uint32 channelInterrupt;

    if (clear)
        channelInterrupt = AtChannelAlarmInterruptClear(channel);
    else
        channelInterrupt = AtChannelAlarmInterruptGet(channel);

    if (channelInterrupt & cAtSdhPathAlarmBerSf)
        return cAtTrue;

    return cAtFalse;
    }

static Tha60210011ModuleBer BerModule(Tha60210011SdhAuVcBerController self)
    {
    return (Tha60210011ModuleBer)AtBerControllerModuleGet((AtBerController)self);
    }

static AtSdhChannel SdhChannel(Tha60210011SdhAuVcBerController self)
    {
    return (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtSdhChannel sdhChannel = SdhChannel(self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerStsBerControlOffset(BerModule(self), sdhChannel, slice, hwSts);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtSdhChannel sdhChannel = SdhChannel(self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerStsCurrentBerOffset(BerModule(self), sdhChannel, slice, hwSts);
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    AtChannel sdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return (hwSts * 2UL + slice * 128UL + Tha60210011BerMeasureStsChannelOffset(module));
    }

static void MethodsInit(AtBerController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BerControlOffset);
        mMethodOverride(m_methods, CurrentBerOffset);
        mMethodOverride(m_methods, HwChannelTypeValue);
        mMethodOverride(m_methods, ThresholdIsSupported);
        }

    mMethodsSet((Tha60210011SdhAuVcBerController)self, &m_methods);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Init);
        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, CurBerGet);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        mMethodOverride(m_AtBerControllerOverride, SdHistoryGet);
        mMethodOverride(m_AtBerControllerOverride, SfHistoryGet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, HwChannelTypeUpdate);
        mMethodOverride(m_AtBerControllerOverride, ExpectedDetectionTimeInMs);
        mMethodOverride(m_AtBerControllerOverride, ExpectedClearingTimeInMs);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011SdhAuVcBerController);
    }

AtBerController Tha60210011SdhAuVcBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210011SdhAuVcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011SdhAuVcBerControllerObjectInit(newController, controllerId, channel, berModule);
    }

uint32 Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(AtBerController self)
    {
    if (self)
        return Imemrwpctrl2Address(self);
    return cInvalidUint32;
    }

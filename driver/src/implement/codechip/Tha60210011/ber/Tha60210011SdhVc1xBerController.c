/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210011SdhVc1xBerController.c
 *
 * Created Date: Jun 9, 2015
 *
 * Description : BER controller of 60210011 SDH VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60210011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011SdhVc1xBerController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011SdhVc1xBerControllerMethods m_methods;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleBer BerModule(Tha60210011SdhAuVcBerController self)
    {
    return (Tha60210011ModuleBer)AtBerControllerModuleGet((AtBerController)self);
    }

static uint32 ExpectedDetectionTimeInMs(AtBerController self, eAtBerRate rate)
    {
    AtUnused(self);
    switch ((uint32) rate)
        {
        case cAtBerRate1E3: return 40;
        case cAtBerRate1E4: return 400;
        case cAtBerRate1E5: return 4000;
        case cAtBerRate1E6: return 40000;
        case cAtBerRate1E7: return 400000;
        case cAtBerRate1E8: return 4000000;
        case cAtBerRate1E9: return 40000000;
        default:            return 0;
        }
    }

static uint32 ExpectedClearingTimeInMs(AtBerController self, eAtBerRate rate)
    {
    return ExpectedDetectionTimeInMs(self, rate);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerVc1xBerControlOffset(BerModule(self), sdhChannel, slice, hwSts);
    }

static uint8 ChannelId(Tha60210011SdhVc1xBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    return AtSdhChannelTu1xGet(sdhChannel);
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    return (AtSdhChannelTypeGet((AtSdhChannel)AtBerControllerMonitoredChannel(self)) == cAtSdhChannelTypeVc12) ? 1 : 0;
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) +
                     mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 enFieldMask = cAf6_imemrwpctrl1_ena1_Mask << (8 * vt);
    uint8 enFieldShift = (uint8)(cAf6_imemrwpctrl1_ena1_Shift + (8 * vt));
    uint32 channelTypeMask = cAf6_imemrwpctrl1_etype1_Mask << (8 * vt);
    uint8 channelTypeShift = (uint8)(cAf6_imemrwpctrl1_etype1_Shift + (8 * vt));
    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], enField, mBoolToBin(enable));
    mRegFieldSet(regVal[0], channelType, mMethodsGet(controller)->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 enFieldMask = cAf6_imemrwpctrl1_ena1_Mask << (8 * vt);
    uint8 enFieldShift = (uint8)(cAf6_imemrwpctrl1_ena1_Shift + (8 * vt));
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return ((mRegField(regVal[0], enField) == 1) ? cAtTrue : cAtFalse);
    }

static eAtRet SdThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 sdFieldMask = cAf6_imemrwpctrl1_sdtrsh1_Mask << (8 * vt);
    uint8 sdFieldShift = (uint8)(cAf6_imemrwpctrl1_sdtrsh1_Shift + (8 * vt));

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], sdField, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate SdThresholdGet(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 sdFieldMask = cAf6_imemrwpctrl1_sdtrsh1_Mask << (8 * vt);
    uint8 sdFieldShift = (uint8)(cAf6_imemrwpctrl1_sdtrsh1_Shift + (8 * vt));

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[0], sdField));
    }

static eAtRet SfThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 sfFieldMask = cAf6_imemrwpctrl1_sftrsh1_Mask << (8 * vt);
    uint8 sfFieldShift = (uint8)(cAf6_imemrwpctrl1_sftrsh1_Shift + (8 * vt));

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], sfField, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate SfThresholdGet(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 sfFieldMask = cAf6_imemrwpctrl1_sftrsh1_Mask << (8 * vt);
    uint8 sfFieldShift = (uint8)(cAf6_imemrwpctrl1_sftrsh1_Shift + (8 * vt));

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)(mRegField(regVal[0], sfField)));
    }

static eAtRet TcaThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 tcaFieldMask = cAf6_imemrwpctrl1_tcatrsh1_Mask << (3 * vt);
    uint8 tcaFieldShift = (uint8)(cAf6_imemrwpctrl1_tcatrsh1_Shift + (3 * vt));

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[1], tcaField, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate TcaThresholdGet(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerControlRegisterAddress(mThis(self)) + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 tcaFieldMask = cAf6_imemrwpctrl1_tcatrsh1_Mask << (3 * vt);
    uint8 tcaFieldShift = (uint8)(cAf6_imemrwpctrl1_tcatrsh1_Shift + (3 * vt));

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)(mRegField(regVal[1], tcaField)));
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerVc1xCurrentBerOffset(BerModule(self), sdhChannel, slice, hwSts);
    }

static uint32 BerReportRegisterAddress(Tha60210011SdhVc1xBerController self)
    {
    AtUnused(self);
    return cAf6Reg_ramberratevtds_Base;
    }

static uint32 BerControlRegisterAddress(Tha60210011SdhVc1xBerController self)
    {
    return Tha60210011ModuleBerImemrwpctrl1Base(BerModule((Tha60210011SdhAuVcBerController)self));
    }

static eAtBerRate CurBerGet(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = mMethodsGet(mThis(self))->BerReportRegisterAddress(mThis(self)) +
                     mMethodsGet(controller)->CurrentBerOffset(controller) + baseAddress;
    uint32 regVal = AtBerControllerRead(self, address);

    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal, cAf6_ramberrateststu3_rate_));
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 28UL + slice * 1344UL + AtSdhChannelTug2Get(sdhChannel) * 4UL + AtSdhChannelTu1xGet(sdhChannel);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, CurBerGet);
        mMethodOverride(m_AtBerControllerOverride, ExpectedDetectionTimeInMs);
        mMethodOverride(m_AtBerControllerOverride, ExpectedClearingTimeInMs);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static void MethodsInit(AtBerController self)
    {
    Tha60210011SdhVc1xBerController controller = (Tha60210011SdhVc1xBerController)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ChannelId);
        mMethodOverride(m_methods, BerReportRegisterAddress);
        mMethodOverride(m_methods, BerControlRegisterAddress);
        }

    mMethodsSet(controller, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011SdhVc1xBerController);
    }

AtBerController Tha60210011SdhVc1xBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhAuVcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210011SdhVc1xBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011SdhVc1xBerControllerObjectInit(newController, controllerId, channel, berModule);
    }

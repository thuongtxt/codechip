/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210011SdhTu3VcBerController.c
 *
 * Created Date: Jun 9, 2015
 *
 * Description : BER controller of TU3-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60210011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_ena2_, mBoolToBin(enable));
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_rate2_, mMethodsGet(controller)->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return ((mRegField(regVal[0], cAf6_imemrwpctrl2_ena2_) == 1) ? cAtTrue : cAtFalse);
    }

static eAtRet SdThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_sdtrsh2_, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate SdThresholdGet(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[0], cAf6_imemrwpctrl2_sdtrsh2_));
    }

static eAtRet SfThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_sftrsh2_, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate SfThresholdGet(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[0], cAf6_imemrwpctrl2_sftrsh2_));
    }

static eAtRet TcaThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[1], cAf6_imemrwpctrl2_tcatrsh2_, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate TcaThresholdGet(AtBerController self)
    {
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];

    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)mRegField(regVal[1], cAf6_imemrwpctrl2_tcatrsh2_));
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    Tha60210011ModuleBer berModule = (Tha60210011ModuleBer)AtBerControllerModuleGet((AtBerController)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerStsCurrentBerOffset(berModule, sdhChannel, slice, hwSts) + 1;
    }

static eAtRet HwChannelTypeUpdate(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 address = Tha60210011SdhAuVcBerControllerImemrwpctrl2Address(self);
    uint32 regVal[cThaLongRegMaxSize];
    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], cAf6_imemrwpctrl2_rate2_, mMethodsGet(controller)->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);
    return cAtOk;
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    AtChannel sdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return (hwSts * 2UL + slice * 128UL + Tha60210011BerMeasureStsChannelOffset(module) + 1);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, HwChannelTypeUpdate);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011SdhTu3VcBerController);
    }

AtBerController Tha60210011SdhTu3VcBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhAuVcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210011SdhTu3VcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011SdhTu3VcBerControllerObjectInit(newController, controllerId, channel, berModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210011PdhDe1BerController.c
 *
 * Created Date: Aug 4, 2015
 *
 * Description : DS1/E1 BER controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60210011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;
static tTha60210011SdhVc1xBerControllerMethods m_Tha60210011SdhVc1xBerControllerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleBer BerModule(Tha60210011SdhAuVcBerController self)
    {
    return (Tha60210011ModuleBer)AtBerControllerModuleGet((AtBerController)self);
    }

static AtPdhChannel De1(Tha60210011SdhAuVcBerController self)
    {
    return (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    uint8 tug2Id;
    uint8 slice = 0, hwSts = 0;
    AtPdhChannel de1 = De1(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);

    if (sdhChannel)
        tug2Id = AtSdhChannelTug2Get(sdhChannel);
    else
        tug2Id = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)de1);

    ThaPdhChannelHwIdGet(de1, cThaModulePoh, &slice, &hwSts);
    return Tha60210011ModuleBerDe1BerControlOffset(BerModule(self), de1, slice, hwSts, tug2Id);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtPdhChannel de1 = De1(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint8 slice = 0, hwSts = 0;
    uint8 tug2Id;
    uint8 tuId;

    ThaPdhChannelHwIdGet(de1, cThaModulePoh, &slice, &hwSts);

    if (sdhChannel)
        {
        tug2Id = AtSdhChannelTug2Get(sdhChannel);
        tuId = AtSdhChannelTu1xGet(sdhChannel);
        }
    else
        {
        tug2Id = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)de1);
        tuId = ThaPdhDe2De1HwDe1IdGet((ThaPdhDe1)de1);
        }

    return Tha60210011ModuleBerDe1CurrentBerOffset(BerModule(self), de1, slice, hwSts, tug2Id, tuId);
    }

static uint8 ChannelId(Tha60210011SdhVc1xBerController self)
    {
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    return (uint8)((sdhChannel == NULL) ? ThaPdhDe2De1HwDe1IdGet((ThaPdhDe1)de1) : AtSdhChannelTu1xGet(sdhChannel));
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    return AtPdhDe1IsE1((AtPdhDe1)AtBerControllerMonitoredChannel(self)) ? 1 : 0;
    }

static eAtRet HwChannelTypeUpdate(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    Tha60210011SdhVc1xBerController vc1xController = (Tha60210011SdhVc1xBerController)self;
    
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address =  mMethodsGet(vc1xController)->BerControlRegisterAddress(vc1xController) +
                      mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
                      
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet(vc1xController)->ChannelId(vc1xController);
    uint32 channelTypeMask = cAf6_imemrwpctrl1_etype1_Mask << (8 * vt);
    uint8 channelTypeShift = (uint8)(cAf6_imemrwpctrl1_etype1_Shift + (8 * vt));
    
    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], channelType, mMethodsGet(controller)->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);
    
    return cAtOk;
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address =  mMethodsGet((Tha60210011SdhVc1xBerController)self)->BerControlRegisterAddress((Tha60210011SdhVc1xBerController)self) +
                      mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 enFieldMask = cAf6_imemrwpctrl1_ena1_Mask << (8 * vt);
    uint8 enFieldShift = (uint8)(cAf6_imemrwpctrl1_ena1_Shift + (8 * vt));
    uint32 channelTypeMask = cAf6_imemrwpctrl1_etype1_Mask << (8 * vt);
    uint8 channelTypeShift = (uint8)(cAf6_imemrwpctrl1_etype1_Shift + (8 * vt));

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], enField, mBoolToBin(enable));
    mRegFieldSet(regVal[0], channelType, mMethodsGet(controller)->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 28UL + (slice + 6UL) * 1344UL + AtSdhChannelTug2Get(sdhChannel) * 4UL + AtSdhChannelTu1xGet(sdhChannel);
    }

static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe1AlarmSdBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe1AlarmSfBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe1AlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, HwChannelTypeUpdate);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void OverrideTha60210011SdhVc1xBerController(AtBerController self)
    {
    Tha60210011SdhVc1xBerController controller = (Tha60210011SdhVc1xBerController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhVc1xBerControllerOverride, mMethodsGet(controller), sizeof(m_Tha60210011SdhVc1xBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhVc1xBerControllerOverride, ChannelId);
        }

    mMethodsSet(controller, &m_Tha60210011SdhVc1xBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    OverrideTha60210011SdhVc1xBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhDe1BerController);
    }

AtBerController Tha60210011PdhDe1BerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhVc1xBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210011PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PdhDe1BerControllerObjectInit(newController, controllerId, channel, berModule);
    }

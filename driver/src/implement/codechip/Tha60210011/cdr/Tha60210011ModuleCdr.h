/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210011ModuleCdr.h
 * 
 * Created Date: May 1, 2015
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60210011MODULECDR_H_
#define THA60210011MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/interrupt/AtInterruptManager.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60210011ModuleCdrLoInterruptManagerIndex 0
#define cTha60210011ModuleCdrHoInterruptManagerIndex 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleCdr * Tha60210011ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011ModuleCdrDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz);
uint32 Tha60210011ModuleCdrDcrRtpTimestampFrequencyGet(ThaModuleCdr self);
uint32 Tha60210011ModuleCdrHoSliceOffset(ThaModuleCdr self);
uint32 Tha60210011ModuleCdrLoSliceOffset(ThaModuleCdr self, uint8 slice);
uint32 Tha60210011ModuleCdrMaxNumLoSlices(ThaModuleCdr self);
uint32 Tha60210011ModuleCdrStartLoRamLocalId(Tha60210011ModuleCdr self);
uint32 Tha60210011ModuleCdrLoBaseAddress(Tha60210011ModuleCdr self);
uint32 Tha60210011ModuleCdrHoBaseAddress(Tha60210011ModuleCdr self);
eBool Tha60210011ModuleCdrFlexibleFrequency(ThaModuleCdr self);
eBool Tha60210011ModuleCdrTimestampFrequencyInRange(ThaModuleCdr self, uint32 khz);
AtInterruptManager Tha60210011ModuleCdrVcCdrControllerInterruptManager(Tha60210011ModuleCdr self, ThaCdrController controller);
uint32 Tha60210011ModuleCdrLoCdrStsTimingCtrl(Tha60210011ModuleCdr self);
AtSdhChannel Tha60210011CdrControllerSdhChannelGet(ThaCdrController controller);

void Tha60210011ModuleCdrPktAnalyzerCla2CdrDataFlush(Tha60210011ModuleCdr self);
eAtRet Tha60210011ModuleCdrPktAnalyzerCla2CdrTrigger(Tha60210011ModuleCdr self, uint32 cdrId);
eBool Tha60210011ModuleCdrPktAnalyzerCla2CdrHoIsEnabled(Tha60210011ModuleCdr self, uint16 packetId);
eBool Tha60210011ModuleCdrPktAnalyzerCla2CdrPacketIsError(Tha60210011ModuleCdr self, uint16 packetId);
uint32 Tha60210011ModuleCdrPktAnalyzerCla2CdrRtpTimeStampGet(Tha60210011ModuleCdr self, uint16 packetId);
uint32 Tha60210011ModuleCdrPktAnalyzerCla2CdrRtpSequenceNumberGet(Tha60210011ModuleCdr self, uint16 packetId);
uint32 Tha60210011ModuleCdrPktAnalyzerCla2CdrControlWordSequenceNumberGet(Tha60210011ModuleCdr self, uint16 packetId);

#ifdef __cplusplus
}
#endif
#endif /* THA60210011MODULECDR_H_ */

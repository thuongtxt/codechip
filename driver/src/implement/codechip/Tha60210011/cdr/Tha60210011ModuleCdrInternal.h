/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210011ModuleCdrInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULECDRINTERNAL_H_
#define _THA60210011MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/cdr/Tha60150011ModuleCdrInternal.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "Tha60210011ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cHoBaseAddress             0x300000UL
#define cLoBaseAddress             0xC00000UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleCdrMethods
    {
    uint8 (*MaxNumLoSlices)(Tha60210011ModuleCdr self);
    uint32 (*StartLoRamLocalId)(Tha60210011ModuleCdr self);
    uint32 (*LoCdrStsTimingControl)(Tha60210011ModuleCdr self);
    uint32 (*LoDcrPrcSrcSelCfgAddress)(Tha60210011ModuleCdr self, uint8 sliceId);
    uint32 (*LoDcrPrcFreqCfgAddress)(Tha60210011ModuleCdr self, uint8 sliceId);
    uint32 (*LoDcrRtpFreqCfgAddress)(Tha60210011ModuleCdr self, uint8 sliceId);
    }tTha60210011ModuleCdrMethods;

typedef struct tTha60210011ModuleCdr
    {
    tTha60150011ModuleCdr super;
    const tTha60210011ModuleCdrMethods *methods;
    }tTha60210011ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULECDRINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210011CdrInterruptManager.c
 *
 * Created Date: Dec 18, 2015
 *
 * Description : CDR module LO interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"
#include "../man/Tha60210011DeviceReg.h"
#include "../man/Tha60210011Device.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "Tha60210011ModuleCdrInternal.h"
#include "Tha60210011CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mManager(self) ((ThaInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods   m_ThaCdrInterruptManagerOverride;
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;

/* Save super implementation. */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0xC00000;
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 InterruptBaseAddress(ThaInterruptManager self)
    {
    return Tha60210011DeviceInterruptBaseAddress((Tha60210011Device)AtModuleDeviceGet(AtInterruptManagerModuleGet((AtInterruptManager)self)));
    }

static uint32 SlicesInterruptGet(ThaInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return AtHalRead(hal, InterruptBaseAddress(self) + cAf6Reg_global_locdr_interrupt_status);
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_status_LoCdrIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtDevice device = AtModuleDeviceGet(cdrModule);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhChannel au;
    AtSdhLine line;
    uint8 hwStsInLine;

    ThaModuleSdhHwSts24ToHwSts48Get((ThaModuleSdh)sdhModule, slice, sts, &slice, &sts);

    line = Tha60210011ModuleSdhLineFromLoLineStsGet(sdhModule, slice, sts, &hwStsInLine);
    if (line == NULL)
        return NULL;

    au = Tha60210011ModuleSdhAuFromHwStsGet(line, hwStsInLine);
    return Tha6021CdrControllerFromAuPathGet((AtSdhPath)au, hwStsInLine, vt);
    }

ThaCdrController Tha6021CdrControllerFromAuPathGet(AtSdhPath au, uint8 hwStsInLine, uint8 vt)
    {
    AtPdhDe3 de3 = NULL;
    uint8 vtgId, vtId;

    vtgId = vt >> 2;
    vtId  = vt & 0x3;

    if (au)
        {
        AtSdhChannel vc = AtSdhChannelSubChannelGet((AtSdhChannel)au, 0);
        uint8 mapType = AtSdhChannelMapTypeGet(vc);

        if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
            {
            AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc, hwStsInLine / 16);
            uint8 tug3MapType = AtSdhChannelMapTypeGet(tug3);

            if (tug3MapType == cAtSdhTugMapTypeTug3MapVc3)
                {
                AtSdhChannel vc3 = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug3, 0), 0);
                if (AtSdhChannelMapTypeGet(vc3) == cAtSdhVcMapTypeVc3MapC3)
                    return ThaSdhVcCdrControllerGet((AtSdhVc)vc3);

                de3 = (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);
                }
            else if (tug3MapType == cAtSdhTugMapTypeTug3Map7xTug2s)
                {
                AtSdhChannel tug2 = AtSdhChannelSubChannelGet(tug3, vtgId);
                AtSdhChannel vc1x = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug2, vtId), 0);

                if (AtSdhChannelMapTypeGet(vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
                    return ThaPdhDe1CdrControllerGet((ThaPdhDe1)AtSdhChannelMapChannelGet(vc1x));
                return ThaSdhVcCdrControllerGet((AtSdhVc)vc1x);
                }
            }
        else if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
            {
            AtSdhChannel tug2 = AtSdhChannelSubChannelGet(vc, vtgId);
            AtSdhChannel vc1x = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug2, vtId), 0);

            if (AtSdhChannelMapTypeGet(vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
                return ThaPdhDe1CdrControllerGet((ThaPdhDe1)AtSdhChannelMapChannelGet(vc1x));
            return ThaSdhVcCdrControllerGet((AtSdhVc)vc1x);
            }

        if (mapType == cAtSdhVcMapTypeVc3MapDe3)
            de3 = (AtPdhDe3)AtSdhChannelMapChannelGet(vc);
        }

    if (de3 == NULL)
        return NULL;

    /* There is case DE3 channelized but it is bound pw */
    if (AtChannelBoundPwGet((AtChannel)de3) || !AtPdhDe3IsChannelized(de3))
        return ThaPdhDe3CdrControllerGet((ThaPdhDe3)de3);

    ThaPdhDe3De1Hw2SwIdGet((ThaPdhDe3)de3, vtgId, vtId, &vtgId, &vtId);
    return ThaPdhDe1CdrControllerGet((ThaPdhDe1)AtPdhDe3De1Get(de3, vtgId, vtId));
    }

static uint32 InterruptOffset(ThaCdrInterruptManager self, ThaCdrController controller)
    {
    return mMethodsGet(controller)->DefaultOffset(controller) - mMethodsGet(mManager(self))->BaseAddress(mManager(self));
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, SlicesInterruptGet);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, AcrDcrControllerFromHwIdGet);
        mMethodOverride(m_ThaCdrInterruptManagerOverride, InterruptOffset);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011LoCdrInterruptManager);
    }

AtInterruptManager Tha60210011LoCdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031CdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210011LoCdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011LoCdrInterruptManagerObjectInit(newManager, module);
    }

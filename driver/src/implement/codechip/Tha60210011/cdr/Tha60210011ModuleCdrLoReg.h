/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210011ModuleCdrLoReg.h
 * 
 * Created Date: May 14, 2015
 *
 * Description : CDR registers of LO path
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULECDRLOREG_H_
#define _THA60210011MODULECDRLOREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : CDR line mode control
Reg Addr   : 0x0000000- 0x0000001
Reg Formula: 0x0000000 + Slide
    Where  :
           + $Slide(0-1): Slide = 0 for STM4_0, Slide = 1 for STM4_1
Reg Desc   :
This register is used to configure line mode for CDR engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_line_mode_ctrl_Base                                                              0x0000000
#define cAf6Reg_locdr_line_mode_ctrl(Slide)                                                  (0x0000000+(Slide))
#define cAf6Reg_locdr_line_mode_ctrl_WidthVal                                                                128
#define cAf6Reg_locdr_line_mode_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: VC3Mode
BitField Type: RW
BitField Desc: VC3 or TU3 mode, each bit is used configured for each STS (only
valid in TU3 CEP) 1: TU3 CEP 0: Other mode
BitField Bits: [123:112]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_VC3Mode_Bit_Start                                                          112
#define cAf6_locdr_line_mode_ctrl_VC3Mode_Bit_End                                                            123
#define cAf6_locdr_line_mode_ctrl_VC3Mode_Mask                                                         cBit27_16
#define cAf6_locdr_line_mode_ctrl_VC3Mode_Shift                                                               16
#define cAf6_locdr_line_mode_ctrl_VC3Mode_MaxVal                                                             0x0
#define cAf6_locdr_line_mode_ctrl_VC3Mode_MinVal                                                             0x0
#define cAf6_locdr_line_mode_ctrl_VC3Mode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: DE3Mode
BitField Type: RW
BitField Desc: DS3 or E3 mode, each bit is used configured for each STS. 1: DS3
mode 0: E3 mod
BitField Bits: [107:96]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_DE3Mode_Bit_Start                                                           96
#define cAf6_locdr_line_mode_ctrl_DE3Mode_Bit_End                                                            107
#define cAf6_locdr_line_mode_ctrl_DE3Mode_Mask                                                          cBit11_0
#define cAf6_locdr_line_mode_ctrl_DE3Mode_Shift                                                                0
#define cAf6_locdr_line_mode_ctrl_DE3Mode_MaxVal                                                             0x0
#define cAf6_locdr_line_mode_ctrl_DE3Mode_MinVal                                                             0x0
#define cAf6_locdr_line_mode_ctrl_DE3Mode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: STS12STSMode
BitField Type: RW
BitField Desc: STS mode of STS12 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [95]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_Bit_Start                                                      95
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_Bit_End                                                        95
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_Mask                                                       cBit31
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_Shift                                                          31
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_MaxVal                                                        0x0
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_MinVal                                                        0x0
#define cAf6_locdr_line_mode_ctrl_STS12STSMode_RstVal                                                        0x0

/*--------------------------------------
BitField Name: STS12VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS12, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [94:88]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS12VTType_Bit_Start                                                       88
#define cAf6_locdr_line_mode_ctrl_STS12VTType_Bit_End                                                         94
#define cAf6_locdr_line_mode_ctrl_STS12VTType_Mask                                                     cBit30_24
#define cAf6_locdr_line_mode_ctrl_STS12VTType_Shift                                                           24
#define cAf6_locdr_line_mode_ctrl_STS12VTType_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS12VTType_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS12VTType_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS11STSMode
BitField Type: RW
BitField Desc: STS mode of STS11 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [87]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_Bit_Start                                                      87
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_Bit_End                                                        87
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_Mask                                                       cBit23
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_Shift                                                          23
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_MaxVal                                                        0x0
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_MinVal                                                        0x0
#define cAf6_locdr_line_mode_ctrl_STS11STSMode_RstVal                                                        0x0

/*--------------------------------------
BitField Name: STS11VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS11, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [86:80]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS11VTType_Bit_Start                                                       80
#define cAf6_locdr_line_mode_ctrl_STS11VTType_Bit_End                                                         86
#define cAf6_locdr_line_mode_ctrl_STS11VTType_Mask                                                     cBit22_16
#define cAf6_locdr_line_mode_ctrl_STS11VTType_Shift                                                           16
#define cAf6_locdr_line_mode_ctrl_STS11VTType_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS11VTType_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS11VTType_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS10STSMode
BitField Type: RW
BitField Desc: STS mode of STS10 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [79]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_Bit_Start                                                      79
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_Bit_End                                                        79
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_Mask                                                       cBit15
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_Shift                                                          15
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_MaxVal                                                        0x0
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_MinVal                                                        0x0
#define cAf6_locdr_line_mode_ctrl_STS10STSMode_RstVal                                                        0x0

/*--------------------------------------
BitField Name: STS10VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS10, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [78:72]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS10VTType_Bit_Start                                                       72
#define cAf6_locdr_line_mode_ctrl_STS10VTType_Bit_End                                                         78
#define cAf6_locdr_line_mode_ctrl_STS10VTType_Mask                                                      cBit14_8
#define cAf6_locdr_line_mode_ctrl_STS10VTType_Shift                                                            8
#define cAf6_locdr_line_mode_ctrl_STS10VTType_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS10VTType_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS10VTType_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS9STSMode
BitField Type: RW
BitField Desc: STS mode of STS9 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [71]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_Bit_Start                                                       71
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_Bit_End                                                         71
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_Mask                                                         cBit7
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_Shift                                                            7
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS9STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS9VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS9, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [70:64]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS9VTType_Bit_Start                                                        64
#define cAf6_locdr_line_mode_ctrl_STS9VTType_Bit_End                                                          70
#define cAf6_locdr_line_mode_ctrl_STS9VTType_Mask                                                        cBit6_0
#define cAf6_locdr_line_mode_ctrl_STS9VTType_Shift                                                             0
#define cAf6_locdr_line_mode_ctrl_STS9VTType_MaxVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS9VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS9VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS8STSMode
BitField Type: RW
BitField Desc: STS mode of STS8 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [63]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_Bit_Start                                                       63
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_Bit_End                                                         63
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_Mask                                                        cBit31
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_Shift                                                           31
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS8STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS8VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS8, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [62:56]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS8VTType_Bit_Start                                                        56
#define cAf6_locdr_line_mode_ctrl_STS8VTType_Bit_End                                                          62
#define cAf6_locdr_line_mode_ctrl_STS8VTType_Mask                                                      cBit30_24
#define cAf6_locdr_line_mode_ctrl_STS8VTType_Shift                                                            24
#define cAf6_locdr_line_mode_ctrl_STS8VTType_MaxVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS8VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS8VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS7STSMode
BitField Type: RW
BitField Desc: STS mode of STS7 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [55]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_Bit_Start                                                       55
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_Bit_End                                                         55
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_Mask                                                        cBit23
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_Shift                                                           23
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS7STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS7VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS7, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [54:48]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS7VTType_Bit_Start                                                        48
#define cAf6_locdr_line_mode_ctrl_STS7VTType_Bit_End                                                          54
#define cAf6_locdr_line_mode_ctrl_STS7VTType_Mask                                                      cBit22_16
#define cAf6_locdr_line_mode_ctrl_STS7VTType_Shift                                                            16
#define cAf6_locdr_line_mode_ctrl_STS7VTType_MaxVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS7VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS7VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS6STSMode
BitField Type: RW
BitField Desc: STS mode of STS6 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [47]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_Bit_Start                                                       47
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_Bit_End                                                         47
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_Mask                                                        cBit15
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_Shift                                                           15
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS6STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS6VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS6, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [46:40]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS6VTType_Bit_Start                                                        40
#define cAf6_locdr_line_mode_ctrl_STS6VTType_Bit_End                                                          46
#define cAf6_locdr_line_mode_ctrl_STS6VTType_Mask                                                       cBit14_8
#define cAf6_locdr_line_mode_ctrl_STS6VTType_Shift                                                             8
#define cAf6_locdr_line_mode_ctrl_STS6VTType_MaxVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS6VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS6VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS5STSMode
BitField Type: RW
BitField Desc: STS mode of STS5 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [39]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_Bit_Start                                                       39
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_Bit_End                                                         39
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_Mask                                                         cBit7
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_Shift                                                            7
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_MaxVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS5STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS5VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS5, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [38:32]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS5VTType_Bit_Start                                                        32
#define cAf6_locdr_line_mode_ctrl_STS5VTType_Bit_End                                                          38
#define cAf6_locdr_line_mode_ctrl_STS5VTType_Mask                                                        cBit6_0
#define cAf6_locdr_line_mode_ctrl_STS5VTType_Shift                                                             0
#define cAf6_locdr_line_mode_ctrl_STS5VTType_MaxVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS5VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS5VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS4STSMode
BitField Type: RW
BitField Desc: STS mode of STS4 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [31]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_Bit_Start                                                       31
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_Bit_End                                                         31
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_Mask                                                        cBit31
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_Shift                                                           31
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_MaxVal                                                         0x1
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS4STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS4VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS4, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [30:24]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS4VTType_Bit_Start                                                        24
#define cAf6_locdr_line_mode_ctrl_STS4VTType_Bit_End                                                          30
#define cAf6_locdr_line_mode_ctrl_STS4VTType_Mask                                                      cBit30_24
#define cAf6_locdr_line_mode_ctrl_STS4VTType_Shift                                                            24
#define cAf6_locdr_line_mode_ctrl_STS4VTType_MaxVal                                                         0x7f
#define cAf6_locdr_line_mode_ctrl_STS4VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS4VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS3STSMode
BitField Type: RW
BitField Desc: STS mode of STS3 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [23]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_Bit_Start                                                       23
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_Bit_End                                                         23
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_Mask                                                        cBit23
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_Shift                                                           23
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_MaxVal                                                         0x1
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS3STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS3VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS3, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS3VTType_Bit_Start                                                        16
#define cAf6_locdr_line_mode_ctrl_STS3VTType_Bit_End                                                          22
#define cAf6_locdr_line_mode_ctrl_STS3VTType_Mask                                                      cBit22_16
#define cAf6_locdr_line_mode_ctrl_STS3VTType_Shift                                                            16
#define cAf6_locdr_line_mode_ctrl_STS3VTType_MaxVal                                                         0x7f
#define cAf6_locdr_line_mode_ctrl_STS3VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS3VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS2STSMode
BitField Type: RW
BitField Desc: STS mode of STS2 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [15]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_Bit_Start                                                       15
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_Bit_End                                                         15
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_Mask                                                        cBit15
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_Shift                                                           15
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_MaxVal                                                         0x1
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS2STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS2VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS2, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [14:8]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS2VTType_Bit_Start                                                         8
#define cAf6_locdr_line_mode_ctrl_STS2VTType_Bit_End                                                          14
#define cAf6_locdr_line_mode_ctrl_STS2VTType_Mask                                                       cBit14_8
#define cAf6_locdr_line_mode_ctrl_STS2VTType_Shift                                                             8
#define cAf6_locdr_line_mode_ctrl_STS2VTType_MaxVal                                                         0x7f
#define cAf6_locdr_line_mode_ctrl_STS2VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS2VTType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: STS1STSMode
BitField Type: RW
BitField Desc: STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_Bit_Start                                                        7
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_Bit_End                                                          7
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_Mask                                                         cBit7
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_Shift                                                            7
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_MaxVal                                                         0x1
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_MinVal                                                         0x0
#define cAf6_locdr_line_mode_ctrl_STS1STSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: STS1VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS1, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_locdr_line_mode_ctrl_STS1VTType_Bit_Start                                                         0
#define cAf6_locdr_line_mode_ctrl_STS1VTType_Bit_End                                                           6
#define cAf6_locdr_line_mode_ctrl_STS1VTType_Mask                                                        cBit6_0
#define cAf6_locdr_line_mode_ctrl_STS1VTType_Shift                                                             0
#define cAf6_locdr_line_mode_ctrl_STS1VTType_MaxVal                                                         0x7f
#define cAf6_locdr_line_mode_ctrl_STS1VTType_MinVal                                                          0x0
#define cAf6_locdr_line_mode_ctrl_STS1VTType_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Timing External reference control
Reg Addr   : 0x0000003
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_timing_extref_ctrl_Base                                                          0x0000003
#define cAf6Reg_locdr_timing_extref_ctrl                                                               0x0000003
#define cAf6Reg_locdr_timing_extref_ctrl_WidthVal                                                             64
#define cAf6Reg_locdr_timing_extref_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: Ext3N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the third external reference signal
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_Bit_Start                                                       32
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_Bit_End                                                         47
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_Mask                                                      cBit15_0
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_Shift                                                            0
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_MaxVal                                                         0x0
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_MinVal                                                         0x0
#define cAf6_locdr_timing_extref_ctrl_Ext3N2k_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Ext2N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the second external reference signal
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_Bit_Start                                                       16
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_Bit_End                                                         31
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_Mask                                                     cBit31_16
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_Shift                                                           16
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_MaxVal                                                      0xffff
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_MinVal                                                         0x0
#define cAf6_locdr_timing_extref_ctrl_Ext2N2k_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Ext1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the first external reference signal
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_Bit_Start                                                        0
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_Bit_End                                                         15
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_Mask                                                      cBit15_0
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_Shift                                                            0
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_MaxVal                                                      0xffff
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_MinVal                                                         0x0
#define cAf6_locdr_timing_extref_ctrl_Ext1N2k_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR STS Timing control
Reg Addr   : 0x0000800-0x000081F
Reg Formula: 0x0000800+STS
    Where  :
           + $STS(0-23):
Reg Desc   :
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_sts_timing_ctrl_Base                                                             0x0000800
#define cAf6Reg_locdr_sts_timing_ctrl(STS)                                                     (0x0000800+(STS))
#define cAf6Reg_locdr_sts_timing_ctrl_WidthVal                                                                32
#define cAf6Reg_locdr_sts_timing_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CDRChid
BitField Type: RW
BitField Desc: CDR channel ID in CDR mode.
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_locdr_sts_timing_ctrl_CDRChid_Bit_Start                                                          20
#define cAf6_locdr_sts_timing_ctrl_CDRChid_Bit_End                                                            29
#define cAf6_locdr_sts_timing_ctrl_CDRChid_Mask                                                        cBit29_20
#define cAf6_locdr_sts_timing_ctrl_CDRChid_Shift                                                              20
#define cAf6_locdr_sts_timing_ctrl_CDRChid_MaxVal                                                          0x3ff
#define cAf6_locdr_sts_timing_ctrl_CDRChid_MinVal                                                            0x0
#define cAf6_locdr_sts_timing_ctrl_CDRChid_RstVal                                                            0x0

/*--------------------------------------
BitField Name: VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_Bit_Start                                                        17
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_Bit_End                                                          17
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_Mask                                                         cBit17
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_Shift                                                            17
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_MaxVal                                                          0x1
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_MinVal                                                          0x0
#define cAf6_locdr_sts_timing_ctrl_VC3EParEn_RstVal                                                          0x0

/*--------------------------------------
BitField Name: MapSTSMode
BitField Type: RW
BitField Desc: Map STS mode 0: Payload STS mode 1: Payload DE3 mode
BitField Bits: [16]
--------------------------------------*/
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_Bit_Start                                                       16
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_Bit_End                                                         16
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_Mask                                                        cBit16
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_Shift                                                           16
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_MaxVal                                                         0x1
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_MinVal                                                         0x0
#define cAf6_locdr_sts_timing_ctrl_MapSTSMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: DE3TimeMode
BitField Type: RW
BitField Desc: DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode 9: OCN System timing mode 10: DCR timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_Bit_Start                                                       4
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_Bit_End                                                         7
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_Mask                                                      cBit7_4
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_Shift                                                           4
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_MaxVal                                                        0xf
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_MinVal                                                        0x0
#define cAf6_locdr_sts_timing_ctrl_DE3TimeMode_RstVal                                                        0x0

/*--------------------------------------
BitField Name: VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_Bit_Start                                                       0
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_Bit_End                                                         3
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_Mask                                                      cBit3_0
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_Shift                                                           0
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_MaxVal                                                        0xf
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_MinVal                                                        0x0
#define cAf6_locdr_sts_timing_ctrl_VC3TimeMode_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR VT Timing control
Reg Addr   : 0x0000C00-0x0000FFF
Reg Formula: 0x0000C00 + STS*32 + TUG*4 + VT
    Where  :
           + $STS(0-23):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1
Reg Desc   :
This register is used to configure timing mode for per VT

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_vt_timing_ctrl_Base                                                              0x0000C00
#define cAf6Reg_locdr_vt_timing_ctrl(STS, TUG, VT)                             (0x0000C00+(STS)*32+(TUG)*4+(VT))
#define cAf6Reg_locdr_vt_timing_ctrl_WidthVal                                                                 32
#define cAf6Reg_locdr_vt_timing_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CDRChid
BitField Type: RW
BitField Desc: CDR channel ID in CDR mode
BitField Bits: [21:12]
--------------------------------------*/
#define cAf6_locdr_vt_timing_ctrl_CDRChid_Bit_Start                                                           12
#define cAf6_locdr_vt_timing_ctrl_CDRChid_Bit_End                                                             21
#define cAf6_locdr_vt_timing_ctrl_CDRChid_Mask                                                         cBit21_12
#define cAf6_locdr_vt_timing_ctrl_CDRChid_Shift                                                               12
#define cAf6_locdr_vt_timing_ctrl_CDRChid_MaxVal                                                           0x3ff
#define cAf6_locdr_vt_timing_ctrl_CDRChid_MinVal                                                             0x0
#define cAf6_locdr_vt_timing_ctrl_CDRChid_RstVal                                                             0x0

/*--------------------------------------
BitField Name: VTEParEn
BitField Type: RW
BitField Desc: VT EPAR mode 0: Disable 1: Enable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_Bit_Start                                                           9
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_Bit_End                                                             9
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_Mask                                                            cBit9
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_Shift                                                               9
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_MaxVal                                                            0x1
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_MinVal                                                            0x0
#define cAf6_locdr_vt_timing_ctrl_VTEParEn_RstVal                                                            0x0

/*--------------------------------------
BitField Name: MapVTMode
BitField Type: RW
BitField Desc: Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_Bit_Start                                                          8
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_Bit_End                                                            8
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_Mask                                                           cBit8
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_Shift                                                              8
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_MaxVal                                                           0x1
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_MinVal                                                           0x0
#define cAf6_locdr_vt_timing_ctrl_MapVTMode_RstVal                                                           0x0

/*--------------------------------------
BitField Name: DE1TimeMode
BitField Type: RW
BitField Desc: DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode 9: OCN System timing mode 10: DCR timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_Bit_Start                                                        4
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_Bit_End                                                          7
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_Mask                                                       cBit7_4
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_Shift                                                            4
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_MaxVal                                                         0xf
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_MinVal                                                         0x0
#define cAf6_locdr_vt_timing_ctrl_DE1TimeMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: VTTimeMode
BitField Type: RW
BitField Desc: VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_Bit_Start                                                         0
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_Bit_End                                                           3
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_Mask                                                        cBit3_0
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_Shift                                                             0
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_MaxVal                                                          0xf
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_MinVal                                                          0x0
#define cAf6_locdr_vt_timing_ctrl_VTTimeMode_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR ToP Timing Frame sync 8K control
Reg Addr   : 0x0000820
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure timing source to generate the 125us signal frame sync for ToP

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_top_timing_frmsync_ctrl_Base                                                     0x0000820
#define cAf6Reg_locdr_top_timing_frmsync_ctrl                                                          0x0000820
#define cAf6Reg_locdr_top_timing_frmsync_ctrl_WidthVal                                                        32
#define cAf6Reg_locdr_top_timing_frmsync_ctrl_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: ToPPDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3:
VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_Bit_Start                                           16
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_Bit_End                                             18
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_Mask                                         cBit18_16
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_Shift                                               16
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_MaxVal                                             0x7
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_MinVal                                             0x0
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineType_RstVal                                             0x0

/*--------------------------------------
BitField Name: ToPPDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH/CEP CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_Bit_Start                                              4
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_Bit_End                                               13
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_Mask                                            cBit13_4
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_Shift                                                  4
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_MaxVal                                             0x3ff
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_MinVal                                               0x0
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPPDHLineID_RstVal                                               0x0

/*--------------------------------------
BitField Name: ToPTimeMode
BitField Type: RW
BitField Desc: Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: OCN System timing mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_Bit_Start                                               0
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_Bit_End                                                 3
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_Mask                                              cBit3_0
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_Shift                                                   0
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_MaxVal                                                0xf
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_MinVal                                                0x0
#define cAf6_locdr_top_timing_frmsync_ctrl_ToPTimeMode_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Reference Sync 8K Master Output control
Reg Addr   : 0x0000821
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure timing source to generate the reference sync master output signal

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_refsync_master_octrl_Base                                                        0x0000821
#define cAf6Reg_locdr_refsync_master_octrl                                                             0x0000821
#define cAf6Reg_locdr_refsync_master_octrl_WidthVal                                                           32
#define cAf6Reg_locdr_refsync_master_octrl_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RefOut1PDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2
3: VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_Bit_Start                                          16
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_Bit_End                                            18
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_Mask                                        cBit18_16
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_Shift                                              16
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_MaxVal                                            0x7
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_MinVal                                            0x0
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineType_RstVal                                            0x0

/*--------------------------------------
BitField Name: RefOut1PDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_Bit_Start                                             4
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_Bit_End                                              13
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_Mask                                           cBit13_4
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_Shift                                                 4
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_MaxVal                                            0x3ff
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_MinVal                                              0x0
#define cAf6_locdr_refsync_master_octrl_RefOut1PDHLineID_RstVal                                              0x0

/*--------------------------------------
BitField Name: RefOut1TimeMode
BitField Type: RW
BitField Desc: Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: External Clock sync
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_Bit_Start                                              0
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_Bit_End                                                3
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_Mask                                             cBit3_0
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_Shift                                                  0
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_MaxVal                                               0xf
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_MinVal                                               0x0
#define cAf6_locdr_refsync_master_octrl_RefOut1TimeMode_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Reference Sync 8k Slaver Output control
Reg Addr   : 0x0000822
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure timing source to generate the reference sync slaver output signal

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_refsync_slaver_octrl_Base                                                        0x0000822
#define cAf6Reg_locdr_refsync_slaver_octrl                                                             0x0000822
#define cAf6Reg_locdr_refsync_slaver_octrl_WidthVal                                                           32
#define cAf6Reg_locdr_refsync_slaver_octrl_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RefOut2PDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2
3: VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_Bit_Start                                          16
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_Bit_End                                            18
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_Mask                                        cBit18_16
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_Shift                                              16
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_MaxVal                                            0x7
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_MinVal                                            0x0
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineType_RstVal                                            0x0

/*--------------------------------------
BitField Name: RefOut2PDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_Bit_Start                                             4
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_Bit_End                                              13
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_Mask                                           cBit13_4
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_Shift                                                 4
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_MaxVal                                            0x3ff
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_MinVal                                              0x0
#define cAf6_locdr_refsync_slaver_octrl_RefOut2PDHLineID_RstVal                                              0x0

/*--------------------------------------
BitField Name: RefOut2TimeMode
BitField Type: RW
BitField Desc: Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: External Clock sync
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_Bit_Start                                              0
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_Bit_End                                                3
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_Mask                                             cBit3_0
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_Shift                                                  0
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_MaxVal                                               0xf
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_MinVal                                               0x0
#define cAf6_locdr_refsync_slaver_octrl_RefOut2TimeMode_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR Timing External reference  and PRC control
Reg Addr   : 0x0020000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure for the 2Khz coefficient of external reference signal, PRC clock in order to generate the sync 125us timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_acr_timing_extref_prc_ctrl_Base                                                  0x0020000
#define cAf6Reg_locdr_acr_timing_extref_prc_ctrl                                                       0x0020000
#define cAf6Reg_locdr_acr_timing_extref_prc_ctrl_WidthVal                                                     64
#define cAf6Reg_locdr_acr_timing_extref_prc_ctrl_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: TxOCNN2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the Ethernet clock signal, default 125Mhz
input clock
BitField Bits: [63:48]
--------------------------------------*/
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_Bit_Start                                              48
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_Bit_End                                                63
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_Mask                                            cBit31_16
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_Shift                                                  16
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_MaxVal                                                0x0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_MinVal                                                0x0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_TxOCNN2k_RstVal                                                0x0

/*--------------------------------------
BitField Name: Ext2N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the second external reference signal,
default 8khz input signal
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_Bit_Start                                               32
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_Bit_End                                                 47
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_Mask                                              cBit15_0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_Shift                                                    0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_MaxVal                                                 0x0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_MinVal                                                 0x0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext2N2k_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Ext1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the first external reference signal,
default 8khz input signal
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_Bit_Start                                               16
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_Bit_End                                                 31
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_Mask                                             cBit31_16
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_Shift                                                   16
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_MaxVal                                              0xffff
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_MinVal                                                 0x0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ext1N2k_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Ocn1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the SONET interface signal, default 8khz
input signal
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_Bit_Start                                                0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_Bit_End                                                 15
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_Mask                                              cBit15_0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_Shift                                                    0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_MaxVal                                              0xffff
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_MinVal                                                 0x0
#define cAf6_locdr_acr_timing_extref_prc_ctrl_Ocn1N2k_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR Engine Timing control
Reg Addr   : 0x0020800-0x0020BFF
Reg Formula: 0x0020800 + CHID
    Where  :
           + $CHID(0-768):
Reg Desc   :
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_acr_eng_timing_ctrl_Base                                                         0x0020800
#define cAf6Reg_locdr_acr_eng_timing_ctrl(CHID)                                               (0x0020800+(CHID))
#define cAf6Reg_locdr_acr_eng_timing_ctrl_WidthVal                                                            32
#define cAf6Reg_locdr_acr_eng_timing_ctrl_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: VC4_4c
BitField Type: RW
BitField Desc: VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c
BitField Bits: [26]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_Bit_Start                                                       26
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_Bit_End                                                         26
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_Mask                                                        cBit26
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_Shift                                                           26
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_MaxVal                                                         0x1
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_MinVal                                                         0x0
#define cAf6_locdr_acr_eng_timing_ctrl_VC4_4c_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PktLen_ind
BitField Type: RW
BitField Desc: The payload packet  length for jumbo frame
BitField Bits: [25]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_Bit_Start                                                   25
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_Bit_End                                                     25
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_Mask                                                    cBit25
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_Shift                                                       25
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_MaxVal                                                     0x1
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_MinVal                                                     0x0
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_ind_RstVal                                                     0x0

/*--------------------------------------
BitField Name: HoldValMode
BitField Type: RW
BitField Desc: Hold value mode of NCO, default value 0 0: Hardware calculated
and auto update 1: Software calculated and update, hardware is disabled
BitField Bits: [24]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_Bit_Start                                                  24
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_Bit_End                                                    24
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_Mask                                                   cBit24
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_Shift                                                      24
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_MaxVal                                                    0x1
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_MinVal                                                    0x0
#define cAf6_locdr_acr_eng_timing_ctrl_HoldValMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: SeqMode
BitField Type: RW
BitField Desc: Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero
BitField Bits: [23]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_Bit_Start                                                      23
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_Bit_End                                                        23
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_Mask                                                       cBit23
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_Shift                                                          23
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_MaxVal                                                        0x1
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_MinVal                                                        0x0
#define cAf6_locdr_acr_eng_timing_ctrl_SeqMode_RstVal                                                        0x0

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6:
STS1/TU3 7: VC4/VC4-4c
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_Bit_Start                                                     20
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_Bit_End                                                       22
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_Mask                                                   cBit22_20
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_Shift                                                         20
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_MaxVal                                                       0x7
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_MinVal                                                       0x0
#define cAf6_locdr_acr_eng_timing_ctrl_LineType_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PktLen
BitField Type: RW
BitField Desc: The payload packet  length parameter to create a packet. SAToP
mode: The number payload of bit. CESoPSN mode: The number of bit which converted
to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0
with M frame, the value configured to this register is Mx256 bits for E1 mode,
Mx193 bits for DS1 mode.
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_Bit_Start                                                        4
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_Bit_End                                                         19
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_Mask                                                      cBit19_4
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_Shift                                                            4
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_MaxVal                                                      0xffff
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_MinVal                                                         0x0
#define cAf6_locdr_acr_eng_timing_ctrl_PktLen_RstVal                                                         0x0

/*--------------------------------------
BitField Name: CDRTimeMode
BitField Type: RW
BitField Desc: CDR time mode 0: System mode 1: Loop timing mode, transparency
service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock
for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock
for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1
clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using
Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode,
using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free
timing mode, using system clock for CDR source to generate service Tx clock 8:
ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_Bit_Start                                                   0
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_Bit_End                                                     3
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_Mask                                                  cBit3_0
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_Shift                                                       0
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_MaxVal                                                    0xf
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_MinVal                                                    0x0
#define cAf6_locdr_acr_eng_timing_ctrl_CDRTimeMode_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust State status
Reg Addr   : 0x0021000-0x00213FF
Reg Formula: 0x0021000 + CHID
    Where  :
           + $CHID(0-768):
Reg Desc   :
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_adj_state_stat_Base                                                              0x0021000
#define cAf6Reg_locdr_adj_state_stat(CHID)                                                    (0x0021000+(CHID))
#define cAf6Reg_locdr_adj_state_stat_WidthVal                                                                 32
#define cAf6Reg_locdr_adj_state_stat_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Adjstate
BitField Type: RW
BitField Desc: Adjust state 0: Load state 1: Holdover state 2: Initialization
state 3: Learn State 4: Rapid State 5: Lock State
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_locdr_adj_state_stat_Adjstate_Bit_Start                                                           0
#define cAf6_locdr_adj_state_stat_Adjstate_Bit_End                                                             2
#define cAf6_locdr_adj_state_stat_Adjstate_Mask                                                          cBit2_0
#define cAf6_locdr_adj_state_stat_Adjstate_Shift                                                               0
#define cAf6_locdr_adj_state_stat_Adjstate_MaxVal                                                            0x7
#define cAf6_locdr_adj_state_stat_Adjstate_MinVal                                                            0x0
#define cAf6_locdr_adj_state_stat_Adjstate_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust Holdover value status
Reg Addr   : 0x0021800-0x0021BFF
Reg Formula: 0x0021800 + CHID
    Where  :
           + $CHID(0-768):
Reg Desc   :
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_locdr_adj_holdover_value_stat_Base                                                     0x0021800
#define cAf6Reg_locdr_adj_holdover_value_stat(CHID)                                           (0x0021800+(CHID))
#define cAf6Reg_locdr_adj_holdover_value_stat_WidthVal                                                        32
#define cAf6Reg_locdr_adj_holdover_value_stat_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: HoldVal
BitField Type: RW
BitField Desc: NCO Holdover value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_Bit_Start                                                   0
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_Bit_End                                                    31
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_Mask                                                 cBit31_0
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_Shift                                                       0
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_MaxVal                                             0xffffffff
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_MinVal                                                    0x0
#define cAf6_locdr_adj_holdover_value_stat_HoldVal_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR TX Engine Active Control
Reg Addr   : 0x0010000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to activate the DCR TX Engine. Active high.

------------------------------------------------------------------------------*/
#define cAf6Reg_lodcr_tx_eng_active_ctrl_Base                                                          0x0010000
#define cAf6Reg_lodcr_tx_eng_active_ctrl                                                               0x0010000
#define cAf6Reg_lodcr_tx_eng_active_ctrl_WidthVal                                                             32
#define cAf6Reg_lodcr_tx_eng_active_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: data
BitField Type: RW
BitField Desc: DCR TX Engine Active Control
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_lodcr_tx_eng_active_ctrl_data_Bit_Start                                                           0
#define cAf6_lodcr_tx_eng_active_ctrl_data_Bit_End                                                            31
#define cAf6_lodcr_tx_eng_active_ctrl_data_Mask                                                         cBit31_0
#define cAf6_lodcr_tx_eng_active_ctrl_data_Shift                                                               0
#define cAf6_lodcr_tx_eng_active_ctrl_data_MaxVal                                                     0xffffffff
#define cAf6_lodcr_tx_eng_active_ctrl_data_MinVal                                                            0x0
#define cAf6_lodcr_tx_eng_active_ctrl_data_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Source Select Configuration
Reg Addr   : 0x0010001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.

------------------------------------------------------------------------------*/
#define cAf6Reg_lodcr_prc_src_sel_cfg_Base                                                             0x0010001
#define cAf6Reg_lodcr_prc_src_sel_cfg                                                                  0x0010001
#define cAf6Reg_lodcr_prc_src_sel_cfg_WidthVal                                                                32
#define cAf6Reg_lodcr_prc_src_sel_cfg_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: DcrPrcDirectMode
BitField Type: RW
BitField Desc: 0: Select RTP frequency lower than 65535 and use value DCRRTPFreq
as RTP frequency 1: Select RTP frequency value 155.52Mhz divide by
DcrRtpFreqDivide. In this mode, below DcrPrcSourceSel[2:0] must set to 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_Bit_Start                                                  3
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_Bit_End                                                    3
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_Mask                                                   cBit3
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_Shift                                                      3
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_MaxVal                                                   0x1
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_MinVal                                                   0x0
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_RstVal                                                   0x0

/*--------------------------------------
BitField Name: DcrPrcSourceSel
BitField Type: RW
BitField Desc: PRC source selection. 0: PRC Reference Clock 1: System Clock
19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line
Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock
Port 4
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_Bit_Start                                                   0
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_Bit_End                                                     2
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_Mask                                                  cBit2_0
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_Shift                                                       0
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_MaxVal                                                    0x7
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_MinVal                                                    0x0
#define cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Frequency Configuration
Reg Addr   : 0x0010002
Reg Formula:
    Where  :
Reg Desc   :
This register is used to select RTP frequency 77.76Mhz or 155.52Mhz

------------------------------------------------------------------------------*/
#define cAf6Reg_lodcr_prd_freq_cfg_Base                                                                0x0010002
#define cAf6Reg_lodcr_prd_freq_cfg                                                                     0x0010002
#define cAf6Reg_lodcr_prd_freq_cfg_WidthVal                                                                   32
#define cAf6Reg_lodcr_prd_freq_cfg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DcrRtpFreqDivide
BitField Type: RW
BitField Desc: Dcr RTP Frequency = 155.52Mhz/ DcrRtpFreqDivide
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_Bit_Start                                                     0
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_Bit_End                                                      15
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_Mask                                                   cBit15_0
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_Shift                                                         0
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_MaxVal                                                   0xffff
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_MinVal                                                      0x0
#define cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Frequency Configuration
Reg Addr   : 0x001000b
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_lodcr_prc_freq_cfg_Base                                                                0x001000b
#define cAf6Reg_lodcr_prc_freq_cfg                                                                     0x001000b
#define cAf6Reg_lodcr_prc_freq_cfg_WidthVal                                                                   32
#define cAf6Reg_lodcr_prc_freq_cfg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DCRPrcFrequency
BitField Type: RW
BitField Desc: Frequency of PRC clock. This is used to configure the frequency
of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Mask                                                    cBit17_0
#define cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Shift                                                          0

/*------------------------------------------------------------------------------
Reg Name   : DCR RTP Frequency Configuration
Reg Addr   : 0x001000C
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_lodcr_rtp_freq_cfg_Base                                                                0x001000C
#define cAf6Reg_lodcr_rtp_freq_cfg                                                                     0x001000C
#define cAf6Reg_lodcr_rtp_freq_cfg_WidthVal                                                                   32
#define cAf6Reg_lodcr_rtp_freq_cfg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DCRRTPFreq
BitField Type: RW
BitField Desc: Frequency of RTP clock. This is used to configure the frequency
of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_lodcr_rtp_freq_cfg_DCRRTPFreq_Mask                                                         cBit17_0
#define cAf6_lodcr_rtp_freq_cfg_DCRRTPFreq_Shift                                                               0

/*------------------------------------------------------------------------------
Reg Name   : DCR Tx Engine Timing control
Reg Addr   : 0x0010400
Reg Formula: 0x0010400 + CHID
    Where  :
           + $CHID(0-768):
Reg Desc   :
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_tx_eng_timing_ctrl_Base                                                          0x0010400
#define cAf6Reg_dcr_tx_eng_timing_ctrl(CHID)                                                (0x0010400+(CHID))
#define cAf6Reg_dcr_tx_eng_timing_ctrl_WidthVal                                                             32
#define cAf6Reg_dcr_tx_eng_timing_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: PktLen_bit
BitField Type: RW
BitField Desc: The payload packet length BIT
BitField Bits: [21:19]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Bit_Start                                                    19
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Bit_End                                                      21
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Mask                                                  cBit21_19
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Shift                                                        19
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_MaxVal                                                      0x7
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_MinVal                                                      0x0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_RstVal                                                      0x0

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6:
STS1/TU3 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Bit_Start                                                      16
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Bit_End                                                        18
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Mask                                                    cBit18_16
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Shift                                                          16
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_MaxVal                                                        0x7
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_MinVal                                                        0x0
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PktLen_byte
BitField Type: RW
BitField Desc: The payload packet length Byte
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Bit_Start                                                    0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Bit_End                                                     15
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Mask                                                  cBit15_0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Shift                                                        0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_MaxVal                                                  0xffff
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_MinVal                                                     0x0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_RstVal                                                     0x0


#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULECDRLOREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210011ModuleCdr.c
 *
 * Created Date: Apr 21, 2015
 *
 * Description : CDR module of product 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrReg.h"
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../man/Tha60210011Device.h"
#include "../ram/Tha60210011InternalRam.h"
#include "../pwe/Tha60210011ModulePwe.h"
#include "Tha60210011ModuleCdrInternal.h"
#include "Tha60210011ModuleCdrHoReg.h"
#include "Tha60210011ModuleCdrLoReg.h"
#include "Tha60210011CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidAddress            cBit31_0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleCdr)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleCdrMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleCdrMethods         m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods      m_ThaModuleCdrStmOverride;
static tAtModuleMethods             m_AtModuleOverride;

/* Save super implementation */
static const tThaModuleCdrMethods         *m_ThaModuleCdrMethods         = NULL;
static const tThaModuleCdrStmMethods      *m_ThaModuleCdrStmMethods      = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EngineIdOfHoVc(AtSdhVc vc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModuleCdr, &slice, &hwSts);

    return hwSts;
    }

static eBool HasHoCdr(ThaModuleCdr self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet((AtModule)self);
    return Tha60210011DeviceHasHoBus(device);
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    if (HasHoCdr(self))
        return Tha60210011HoLineVcCdrControllerNew(EngineIdOfHoVc(vc), (AtChannel)vc);
    return m_ThaModuleCdrMethods->HoVcCdrControllerCreate(self, vc);
    }

static uint32 CDRLineModeControlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_locdr_line_mode_ctrl_Base;
    }

static uint32 VtTimingControlReg(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6Reg_locdr_vt_timing_ctrl_Base;
    }

static uint8 MaxNumLoSlices(Tha60210011ModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    uint8 numLoLines = Tha60210011ModuleSdhMaxNumLoLines(sdhModule);
    return (uint8)mMin((Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)device) * 2), (numLoLines * 2));
    }

static void LowPartDcrClockFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        uint32 regAddr  = mMethodsGet(mThis(self))->LoDcrPrcFreqCfgAddress(mThis(self), slice_i);
        uint32 regValue = mModuleHwRead(self, regAddr);
        mRegFieldSet(regValue, cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_, khz);
        mModuleHwWrite(self, regAddr, regValue);
        }
    }

static uint32 LowPartDcrClockFrequencyGet(ThaModuleCdr self)
    {
    uint32 regAddr  = mMethodsGet(mThis(self))->LoDcrPrcFreqCfgAddress(mThis(self), 0);
    uint32 regValue = mModuleHwRead(self, regAddr);
    return mRegField(regValue, cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_);
    }

static void HoPartDcrClockeFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    uint32 regAddr  = cAf6Reg_dcr_prc_freq_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue = mModuleHwRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_, khz);
    mModuleHwWrite(self, regAddr, regValue);
    }

static uint32 HoPartDcrClockeFrequencyGet(ThaModuleCdr self)
    {
    uint32 regVal = mModuleHwRead(self, cAf6Reg_dcr_prc_freq_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self));
    return mRegField(regVal, cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_);
    }

static eAtRet PartDcrClockFrequencySet(ThaModuleCdr self, uint32 khz, uint8 partId)
    {
    AtUnused(partId);
    LowPartDcrClockFrequencySet(self, khz);

    if (HasHoCdr(self))
        HoPartDcrClockeFrequencySet(self, khz);

    return cAtOk;
    }

static uint32 DcrClockFrequencyGet(ThaModuleCdr self)
    {
    if (HasHoCdr(self))
        return HoPartDcrClockeFrequencyGet(self);
    return LowPartDcrClockFrequencyGet(self);
    }

static void LoPartDcrClockSourceSet(ThaModuleCdr self, uint8 hwClockSource)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        uint32 regAddr  = mMethodsGet(mThis(self))->LoDcrPrcSrcSelCfgAddress(mThis(self), slice_i);
        uint32 regValue = mModuleHwRead(self, regAddr);
        mRegFieldSet(regValue, cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_, hwClockSource);
        mModuleHwWrite(self, regAddr, regValue);
        }
    }

static void HoPartDcrClockSourceSet(ThaModuleCdr self, uint8 hwClockSource)
    {
    uint32 regAddr  = cAf6Reg_dcr_prc_src_sel_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue = mModuleHwRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_, hwClockSource);
    mModuleHwWrite(self, regAddr, regValue);
    }

static eAtRet PartDcrClockSourceSet(ThaModuleCdr self, uint8 hwClockSource, uint8 partId)
    {
    AtUnused(partId);
    LoPartDcrClockSourceSet(self, hwClockSource);

    if (HasHoCdr(self))
        HoPartDcrClockSourceSet(self, hwClockSource);

    return cAtOk;
    }

static uint8 LoPartDcrClockSourceGet(ThaModuleCdr self)
    {
    uint32 regAddr  = mMethodsGet(mThis(self))->LoDcrPrcSrcSelCfgAddress(mThis(self), 0);
    uint32 regValue = mModuleHwRead(self, regAddr);
    return (uint8)mRegField(regValue, cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_);
    }

static uint8 HoPartDcrClockSourceGet(ThaModuleCdr self)
    {
    uint32 regAddr  = cAf6Reg_dcr_prc_src_sel_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue = mModuleHwRead(self, regAddr);
    return (uint8)mRegField(regValue, cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_);
    }

static uint8 HwDcrClockSourceGet(ThaModuleCdr self)
    {
    if (HasHoCdr(self))
        return HoPartDcrClockSourceGet(self);
    return LoPartDcrClockSourceGet(self);
    }

static void LoDcrRtpTimestampFrequencyLessThan16BitsSelect(ThaModuleCdr self)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        uint32 address  = mMethodsGet(mThis(self))->LoDcrPrcSrcSelCfgAddress(mThis(self), slice_i);
        uint32 regValue = mModuleHwRead(self, address);

        if (mRegField(regValue, cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_) == 0x0) /* PRC mode */
            mRegFieldSet(regValue, cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_, cRtpTimestampFrequencyLessThan16BitsMode);
        mModuleHwWrite(self, address, regValue);
        }
    }

static void HoDcrRtpTimestampFrequencyLessThan16BitsSelect(ThaModuleCdr self)
    {
    uint32 address  = cAf6Reg_dcr_prc_src_sel_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue = mModuleHwRead(self, address);

    if (mRegField(regValue, cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_) == 0x0) /* PRC mode */
        mRegFieldSet(regValue, cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_, cRtpTimestampFrequencyLessThan16BitsMode);
    mModuleHwWrite(self, address, regValue);
    }

static eAtRet DcrRtpTimestampFrequencyLessThan16BitsSelect(ThaModuleCdr self)
    {
    LoDcrRtpTimestampFrequencyLessThan16BitsSelect(self);

    if (HasHoCdr(self))
        HoDcrRtpTimestampFrequencyLessThan16BitsSelect(self);

    return cAtOk;
    }

static void LoDcrRtpTimestampFrequencyLessThan16BitsSet(ThaModuleCdr self, uint32 khz)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        uint32 regAddr = mMethodsGet(mThis(self))->LoDcrRtpFreqCfgAddress(mThis(self), slice_i);
        uint32 regVal  = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cAf6_lodcr_rtp_freq_cfg_DCRRTPFreq_, khz);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static uint32 LoDcrRtpTimestampFrequencyLessThan16BitsGet(ThaModuleCdr self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->LoDcrRtpFreqCfgAddress(mThis(self), 0);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_lodcr_rtp_freq_cfg_DCRRTPFreq_);
    }

static void HoDcrRtpTimestampFrequencyLessThan16BitsSet(ThaModuleCdr self, uint32 khz)
    {
    uint32 regAddr = cAf6Reg_dcr_rtp_freq_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_, khz);
    mModuleHwWrite(self, regAddr, regVal);
    }

static uint32 HoDcrRtpTimestampFrequencyLessThan16BitsGet(ThaModuleCdr self)
    {
    uint32 regAddr = cAf6Reg_dcr_rtp_freq_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_);
    }

static eAtRet DcrRtpTimestampFrequencyLessThan16BitsSet(ThaModuleCdr self, uint32 khz)
    {
    LoDcrRtpTimestampFrequencyLessThan16BitsSet(self, khz);

    if (HasHoCdr(self))
        HoDcrRtpTimestampFrequencyLessThan16BitsSet(self, khz);

    return cAtOk;
    }

static void LoDcrRtpTimestampFrequencyLargerThan16BitsSelect(ThaModuleCdr self)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        uint32 regAddr = mMethodsGet(mThis(self))->LoDcrPrcSrcSelCfgAddress(mThis(self), slice_i);
        uint32 regValue  = mModuleHwRead(self, regAddr);

        if (mRegField(regValue, cAf6_lodcr_prc_src_sel_cfg_DcrPrcSourceSel_) == 0x1) /* System clock mode */
            mRegFieldSet(regValue, cAf6_lodcr_prc_src_sel_cfg_DcrPrcDirectMode_, cRtpTimestampFrequencyLargerThan16BitsMode);
        mModuleHwWrite(self, regAddr, regValue);
        }
    }

static void HoDcrRtpTimestampFrequencyLargerThan16BitsSelect(ThaModuleCdr self)
    {
    uint32 regAddr = cAf6Reg_dcr_prc_src_sel_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue  = mModuleHwRead(self, regAddr);

    if (mRegField(regValue, cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_) == 0x1) /* System clock mode */
        mRegFieldSet(regValue, cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_, cRtpTimestampFrequencyLargerThan16BitsMode);
    mModuleHwWrite(self, regAddr, regValue);
    }

static eAtRet DcrRtpTimestampFrequencyLargerThan16BitsSelect(ThaModuleCdr self)
    {
    LoDcrRtpTimestampFrequencyLargerThan16BitsSelect(self);

    if (HasHoCdr(self))
        HoDcrRtpTimestampFrequencyLargerThan16BitsSelect(self);

    return cAtOk;
    }

static void LoDcrRtpTimestampFrequencyLargerThan16BitsSet(ThaModuleCdr self, uint32 khz)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        uint32 address  = cAf6Reg_lodcr_prd_freq_cfg_Base + Tha60210011ModuleCdrLoSliceOffset(self, slice_i);
        uint32 regValue = mModuleHwRead(self, address);
        mRegFieldSet(regValue, cAf6_lodcr_prd_freq_cfg_DcrRtpFreqDivide_, cDcrRtpFrequency155Mhz / khz);
        mModuleHwWrite(self, address, regValue);
        }
    }

static void HoDcrRtpTimestampFrequencyLargerThan16BitsSet(ThaModuleCdr self, uint32 khz)
    {
    uint32 address  = cAf6Reg_dcr_prd_freq_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_, cDcrRtpFrequency155Mhz / khz);
    mModuleHwWrite(self, address, regValue);
    }

static eAtRet DcrRtpTimestampFrequencyLargerThan16BitsSet(ThaModuleCdr self, uint32 khz)
    {
    LoDcrRtpTimestampFrequencyLargerThan16BitsSet(self, khz);

    if (HasHoCdr(self))
        HoDcrRtpTimestampFrequencyLargerThan16BitsSet(self, khz);

    return cAtOk;
    }

static eBool DcrRtpTimestampFrequencyIsLargerThan16Bits(ThaModuleCdr self)
    {
    uint32 regAddr = cAf6Reg_dcr_prc_src_sel_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    return (mRegField(regVal, cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_) == 0x1) ? cAtTrue : cAtFalse;
    }

static uint32 DcrRtpTimestampFrequencyLessThan16BitsGet(ThaModuleCdr self)
    {
    if (HasHoCdr(self))
        return HoDcrRtpTimestampFrequencyLessThan16BitsGet(self);
    return LoDcrRtpTimestampFrequencyLessThan16BitsGet(self);
    }

static uint32 DcrRtpTimestampFrequencyLargerThan16BitsGet(ThaModuleCdr self)
    {
    uint32 address  = cAf6Reg_dcr_prd_freq_cfg_Base + Tha60210011ModuleCdrHoSliceOffset(self);
    uint32 regValue = mModuleHwRead(self, address);

    return cDcrRtpFrequency155Mhz / mRegField(regValue, cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_);
    }

static uint32 HoStsTimingControlReg(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_sts_timing_ctrl_Base;
    }

static uint32 LoCdrStsTimingControl(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_locdr_sts_timing_ctrl_Base;
    }

static eBool ShouldOperateOnHoBus(ThaModuleCdr self, AtSdhChannel channel)
    {
    if (HasHoCdr((ThaModuleCdr)self) && Tha60210011ModuleSdhChannelIsHoPath(channel))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 StsTimingControlReg(ThaModuleCdrStm self, AtSdhChannel channel)
    {
    if (ShouldOperateOnHoBus((ThaModuleCdr)self, channel))
        return HoStsTimingControlReg(self);

    return mMethodsGet(mThis(self))->LoCdrStsTimingControl(mThis(self));
    }

static AtSdhChannel SdhChannelFromController(ThaCdrController controller)
    {
    AtSdhChannel channel = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)controller, ThaCdrControllerChannelGet(controller));
    if (channel == NULL)
        {
        /* May be DE2-DE1 controller, try getting de3 */
        AtPdhChannel de1 = (AtPdhChannel)ThaCdrControllerChannelGet(controller);
        AtPdhChannel de2 = AtPdhChannelParentChannelGet(de1);
        if (de2)
            {
            AtPdhChannel de3 = AtPdhChannelParentChannelGet(de2);
            return (AtSdhChannel)AtPdhChannelVcInternalGet(de3);
            }

        return NULL;
        }

    return channel;
    }

static uint32 AdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller)
    {
    AtSdhChannel channel = SdhChannelFromController(controller);
    AtUnused(self);
    if (ShouldOperateOnHoBus(self, channel))
        return cAf6Reg_cdr_adj_state_stat_Base;

    return cAf6Reg_locdr_adj_state_stat_Base;
    }

static uint32 SliceOffset(ThaModuleCdr self, AtSdhChannel sdhChannel, uint8 slice)
    {
    if (ShouldOperateOnHoBus(self, sdhChannel))
        return Tha60210011ModuleCdrHoSliceOffset(self);

    return Tha60210011ModuleCdrLoSliceOffset(self, slice);
    }

static uint32 EngineTimingCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    /* Cannot input controller into this method, because this method is used in some case
     * when controller is not available, so just return address of module LO CDR, if address of HO-CDR is different, the
     * difference will be handled in method calculate offset EngineTimingOffsetByHwSts
     */
    return cAf6Reg_locdr_acr_eng_timing_ctrl_Base;
    }

static eBool OnlySupportPrcForDcr(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultDcrClockFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 19440;
    }

static uint32 EngineTimingOffsetByHwSts(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    uint32 cdrId, sliceOffset;

    cdrId = ThaModuleCdrStmSdhVcCdrIdCalculate(self, channel, slice, hwStsId, vtgId, vtId);
    sliceOffset = ThaModuleCdrStmSliceOffset(self, channel, slice);

    if (ShouldOperateOnHoBus(self, channel))
        return sliceOffset + hwStsId + slice * 64UL;

    return sliceOffset + cdrId;
    }

static eAtRet StsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    if (ShouldOperateOnHoBus(self, channel))
        return ThaModuleCdrStmHoEngineLineTypeSet(self, channel, slice, hwStsId, payloadType);

    return m_ThaModuleCdrStmMethods->StsPldSet(self, channel, slice, hwStsId, payloadType);
    }

static eAtRet VtgPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    if (ShouldOperateOnHoBus(self, channel))
        return cAtOk;

    return m_ThaModuleCdrStmMethods->VtgPldSet(self, channel, slice, hwSts, vtgId, payloadType);
    }

static eAtRet Vc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    if (ShouldOperateOnHoBus(self, channel))
        return cAtOk;

    return m_ThaModuleCdrStmMethods->Vc3CepModeEnable(self, channel, slice, hwSts, enable);
    }

static uint32 ControllerDefaultOffsetBySdhChannel(ThaModuleCdr self, ThaCdrController controller, AtSdhChannel channel)
    {
    uint8 slice, sts;
    ThaSdhChannel2HwMasterStsId(channel, cThaModuleCdr, &slice, &sts);

    if (ShouldOperateOnHoBus(self, channel))
        return ThaModuleCdrStmSliceOffset(self, channel, slice) + slice * 64UL + ThaCdrControllerIdGet(controller);

    return ThaModuleCdrStmSliceOffset(self, channel, slice) + ThaCdrControllerIdGet(controller);
    }

static uint32 ControllerDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtSdhChannel sdhChannel = Tha60210011CdrControllerSdhChannelGet(controller);
    AtUnused(self);
    return ControllerDefaultOffsetBySdhChannel(self, controller, sdhChannel);
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsCdrRegister(AtModuleDeviceGet(self), address);
    }

static void LoActivate(ThaModuleCdr self, eBool activateTx)
    {
    uint8 slice_i;

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        if (activateTx)
            mModuleHwWrite(self, cAf6Reg_lodcr_tx_eng_active_ctrl_Base + Tha60210011ModuleCdrLoSliceOffset(self, slice_i), cAtTrue);
        }
    }

static void HoActivate(ThaModuleCdr self, eBool activateTx)
    {
    if (activateTx)
        mModuleHwWrite(self, cAf6Reg_lodcr_tx_eng_active_ctrl_Base + Tha60210011ModuleCdrHoSliceOffset(self), cAtTrue);
    }

static void Activate(ThaModuleCdr self)
    {
    eBool activateTx = mMethodsGet(self)->NeedActivateTxEngine(self);

    LoActivate(self, activateTx);
    if (HasHoCdr(self))
        HoActivate(self, activateTx);
    }

static uint8 MaxNumStsInLoSlice(ThaModuleCdr self)
    {
    AtUnused(self);
    return 24;
    }

static uint8 MaxNumStsInHoSlice(ThaModuleCdr self)
    {
    AtUnused(self);
    return 48;
    }

static void HoDefaultSet(ThaModuleCdr self)
    {
    uint8 stsId;

    for (stsId = 0; stsId < MaxNumStsInHoSlice(self); stsId++)
        {
        uint32 offset = Tha60210011ModuleCdrHoSliceOffset(self) + stsId;
        mModuleHwWrite(self, HoStsTimingControlReg((ThaModuleCdrStm)self) + offset, 0);
        }
    }

static void LoDefaultSet(ThaModuleCdr self)
    {
    uint8 stsId, slice_i, vtgId, vtId;
    uint32 regBase = mMethodsGet(mThis(self))->LoCdrStsTimingControl(mThis(self));

    for (slice_i = 0; slice_i < mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self)); slice_i++)
        {
        for (stsId = 0; stsId < MaxNumStsInLoSlice(self); stsId++)
            {
            uint32 offset = Tha60210011ModuleCdrLoSliceOffset(self, slice_i) + stsId;

            /* Reset CDR STS Timing control */
            mModuleHwWrite(self, regBase + offset, 0);

            for (vtgId = 0; vtgId < 7; vtgId++)
                {
                for (vtId = 0; vtId < 4; vtId++)
                    {
                    uint32 regAddr = mMethodsGet((ThaModuleCdrStm)self)->VtTimingControlReg((ThaModuleCdrStm)self);

                    /* Reset CDR VT Timing control */
                    offset = (uint32)(stsId << 5) + (uint32)(vtgId << 2) + vtId;
                    mModuleHwWrite(self, regAddr + Tha60210011ModuleCdrLoSliceOffset(self, slice_i) + offset, 0);
                    }
                }
            }
        }
    }

static eAtRet DefaultSet(ThaModuleCdr self)
    {
    Activate(self);
    if (HasHoCdr(self))
        HoDefaultSet(self);
    LoDefaultSet(self);

    return cAtOk;
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    /* This product has not supported this feature */
    AtUnused(self);
    return NULL;
    }

static uint32 StartVersionSupportDcrShapper(ThaModuleCdr self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    return Tha60210011ModulePweStartVersionControlJitterAttenuator(pweModule);
    }

static eBool DcrShaperIsSupported(ThaModuleCdr self, ThaCdrController controller)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion < StartVersionSupportDcrShapper(self))
    return cAtFalse;

    if (ShouldOperateOnHoBus(self, SdhChannelFromController(controller)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if (((localAddress >= 0x0300000) && (localAddress <= 0x033FFFF)) || /* ho part */
        ((localAddress >= 0x0C00000) && (localAddress <= 0x0EFFFFF)))   /* lo part */
        return cAtTrue;

    return cAtFalse;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "HO CDR STS Timing control",
         "HO CDR ACR Engine Timing control, slice 0",
         "HO CDR ACR Engine Timing control, slice 1",
         "HO CDR ACR Engine Timing control, slice 2",
         "HO CDR ACR Engine Timing control, slice 3",
         "HO CDR ACR Engine Timing control, slice 4",
         "HO CDR ACR Engine Timing control, slice 5",
         "HO CDR ACR Engine Timing control, slice 6",
         "HO CDR ACR Engine Timing control, slice 7",
         "LO CDR STS Timing control, slice 0",
         "LO CDR VT Timing control, slice 0",
         "LO CDR ACR Engine Timing control, slice 0",
         "LO CDR STS Timing control, slice 1",
         "LO CDR VT Timing control, slice 1",
         "LO CDR ACR Engine Timing control, slice 1",
         "LO CDR STS Timing control, slice 2",
         "LO CDR VT Timing control, slice 2",
         "LO CDR ACR Engine Timing control, slice 2",
         "LO CDR STS Timing control, slice 3",
         "LO CDR VT Timing control, slice 3",
         "LO CDR ACR Engine Timing control, slice 3",
         "LO CDR STS Timing control, slice 4",
         "LO CDR VT Timing control, slice 4",
         "LO CDR ACR Engine Timing control, slice 4",
         "LO CDR STS Timing control, slice 5",
         "LO CDR VT Timing control, slice 5",
         "LO CDR ACR Engine Timing control, slice 5",
         "LO CDR STS Timing control, slice 6",
         "LO CDR VT Timing control, slice 6",
         "LO CDR ACR Engine Timing control, slice 6",
         "LO CDR STS Timing control, slice 7",
         "LO CDR VT Timing control, slice 7",
         "LO CDR ACR Engine Timing control, slice 7",
         "LO CDR STS Timing control, slice 8",
         "LO CDR VT Timing control, slice 8",
         "LO CDR ACR Engine Timing control, slice 8",
         "LO CDR STS Timing control, slice 9",
         "LO CDR VT Timing control, slice 9",
         "LO CDR ACR Engine Timing control, slice 9",
         "LO CDR STS Timing control, slice 10",
         "LO CDR VT Timing control, slice 10",
         "LO CDR ACR Engine Timing control, slice 10",
         "LO CDR STS Timing control, slice 11",
         "LO CDR VT Timing control, slice 11",
         "LO CDR ACR Engine Timing control, slice 11"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < mMethodsGet(mThis(self))->StartLoRamLocalId(mThis(self)))
        return Tha60210011InternalRamCdrHoNew(self, ramId, localRamId);

    return Tha60210011InternalRamCdrNew(self, ramId, localRamId);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    uint32 localRamId = AtInternalRamLocalIdGet(ram);

    if (localRamId < mMethodsGet(mThis(self))->StartLoRamLocalId(mThis(self)))
        {
        if (HasHoCdr((ThaModuleCdr)self))
            return (localRamId < 5) ? cAtFalse : cAtTrue;
        return cAtTrue;
        }

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static uint32 StartLoRamLocalId(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return 9;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (!ThaModuleCdrInterruptIsSupported((ThaModuleCdr)self))
        return NULL;

    if (managerIndex == 0)
        return Tha60210011LoCdrInterruptManagerNew(self);

    if (managerIndex == cTha60210011ModuleCdrHoInterruptManagerIndex)
        return Tha60210011HoCdrInterruptManagerNew(self);

    return NULL;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x24, 0x40);
    }

static eAtClockState ClockStateFromHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    AtUnused(self);

    if (hwState == 2)
        return cAtClockStateInit;

    if ((hwState <= 1) || (hwState == 7))
        return cAtClockStateHoldOver;

    if (hwState == 5)
        return cAtClockStateLocked;

    return cAtClockStateLearning;
    }

static uint32 PktAnalyzerCla2CdrBaseAddress(Tha60210011ModuleCdr self)
    {
    return Tha60210011ModuleCdrHoSliceOffset((ThaModuleCdr)self);
    }

static AtIpCore IpCore(Tha60210011ModuleCdr self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    }

static uint32 DcrTxEngineTimingControlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_dcr_tx_eng_timing_ctrl_Base;
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedJaVersionCem = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x3, 0x00);
    uint32 supportedVersionCemStop = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x8, 0x0, 0x00);
    uint32 supportedJaVersionEoS = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x8, 0x2, 0x00);

    if ((hwVersion >= supportedJaVersionCem) && (hwVersion < supportedVersionCemStop))
        return cAtTrue;

    if (hwVersion >= supportedJaVersionEoS)
        return cAtTrue;

    return cAtFalse;
    }

static AtInterruptManager VcCdrControllerInterruptManager(Tha60210011ModuleCdr self, ThaCdrController controller)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaCdrControllerChannelGet(controller);
    uint32 channelType = AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc));
    AtInterruptManager loManager = AtModuleInterruptManagerAtIndexGet((AtModule)self, cTha60210011ModuleCdrLoInterruptManagerIndex);
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (channelType == cAtSdhChannelTypeTu3)
        return loManager;

    if (HasHoCdr(cdrModule))
        return AtModuleInterruptManagerAtIndexGet((AtModule)self, cTha60210011ModuleCdrHoInterruptManagerIndex);

    return loManager;
    }

static uint32 MaxDcrPrcFrequency(ThaModuleCdr self)
    {
    if (Tha60210011ModuleCdrFlexibleFrequency(self))
        return cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Mask >> cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Shift;
    return m_ThaModuleCdrMethods->MaxDcrPrcFrequency(self);
    }

static uint32 LoDcrRtpFreqCfgAddress(Tha60210011ModuleCdr self, uint8 sliceId)
    {
    return cAf6Reg_lodcr_rtp_freq_cfg_Base + Tha60210011ModuleCdrLoSliceOffset((ThaModuleCdr)self, sliceId);
    }

static uint32 LoDcrPrcSrcSelCfgAddress(Tha60210011ModuleCdr self, uint8 sliceId)
    {
    return cAf6Reg_lodcr_prc_src_sel_cfg_Base + Tha60210011ModuleCdrLoSliceOffset((ThaModuleCdr)self, sliceId);
    }

static uint32 LoDcrPrcFreqCfgAddress(Tha60210011ModuleCdr self, uint8 sliceId)
    {
    return cAf6Reg_lodcr_prc_freq_cfg_Base + Tha60210011ModuleCdrLoSliceOffset((ThaModuleCdr)self, sliceId);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, PartDcrClockFrequencySet);
        mMethodOverride(m_ThaModuleCdrOverride, PartDcrClockSourceSet);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, AdjustStateStatusRegister);
        mMethodOverride(m_ThaModuleCdrOverride, EngineTimingCtrlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, CDRLineModeControlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, OnlySupportPrcForDcr);
        mMethodOverride(m_ThaModuleCdrOverride, ControllerDefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultSet);
        mMethodOverride(m_ThaModuleCdrOverride, DcrClockFrequencyGet);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultDcrClockFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModuleCdrOverride, HwDcrClockSourceGet);
        mMethodOverride(m_ThaModuleCdrOverride, DcrShaperIsSupported);
        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, ClockStateFromHwStateGet);
        mMethodOverride(m_ThaModuleCdrOverride, DcrTxEngineTimingControlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, ShouldEnableJitterAttenuator);
        mMethodOverride(m_ThaModuleCdrOverride, MaxDcrPrcFrequency);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm cdrModule = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, mMethodsGet(cdrModule), sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, VtTimingControlReg);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsTimingControlReg);
        mMethodOverride(m_ThaModuleCdrStmOverride, SliceOffset);
        mMethodOverride(m_ThaModuleCdrStmOverride, EngineTimingOffsetByHwSts);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsPldSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtgPldSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, Vc3CepModeEnable);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrStmOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MaxNumLoSlices);
        mMethodOverride(m_methods, StartLoRamLocalId);
        mMethodOverride(m_methods, LoCdrStsTimingControl);
        mMethodOverride(m_methods, LoDcrPrcSrcSelCfgAddress);
        mMethodOverride(m_methods, LoDcrRtpFreqCfgAddress);
        mMethodOverride(m_methods, LoDcrPrcFreqCfgAddress);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleCdr);
    }

AtModule Tha60210011ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleCdrObjectInit(newModule, device);
    }

eAtRet Tha60210011ModuleCdrDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    eAtRet ret = cAtOk;

    if (!ThaModuleCdrHasDcrTimingMode(self))
        return cAtOk;

    if ((khz <= cBit15_0) || Tha60210011ModuleCdrFlexibleFrequency(self))
        {
        ret |= DcrRtpTimestampFrequencyLessThan16BitsSet(self, khz);
        ret |= DcrRtpTimestampFrequencyLessThan16BitsSelect(self);
        return ret;
        }

    if ((khz != cDcrRtpFrequency155Mhz) && (khz != cDcrRtpFrequency77Mhz))
        return cAtErrorModeNotSupport;

    ret |= DcrRtpTimestampFrequencyLargerThan16BitsSet(self, khz);
    ret |= DcrRtpTimestampFrequencyLargerThan16BitsSelect(self);

    return ret;
    }

uint32 Tha60210011ModuleCdrDcrRtpTimestampFrequencyGet(ThaModuleCdr self)
    {
    if (!ThaModuleCdrHasDcrTimingMode(self))
        return 0x0;

    if (Tha60210011ModuleCdrFlexibleFrequency(self))
        return DcrRtpTimestampFrequencyLessThan16BitsGet(self);

    if (DcrRtpTimestampFrequencyIsLargerThan16Bits(self))
        return DcrRtpTimestampFrequencyLargerThan16BitsGet(self);

    return DcrRtpTimestampFrequencyLessThan16BitsGet(self);
    }

uint32 Tha60210011ModuleCdrLoSliceOffset(ThaModuleCdr self, uint8 slice)
    {
    AtUnused(self);
    return (cLoBaseAddress + ((slice / 4) * 0x100000UL) + ((slice % 4) * 0x40000UL));
    }

uint32 Tha60210011ModuleCdrHoSliceOffset(ThaModuleCdr self)
    {
    AtUnused(self);
    return cHoBaseAddress;
    }

uint32 Tha60210011ModuleCdrMaxNumLoSlices(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(mThis(self))->MaxNumLoSlices(mThis(self));
    
    return 0;
    }

uint32 Tha60210011ModuleCdrStartLoRamLocalId(Tha60210011ModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->StartLoRamLocalId(self);
        
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleCdrLoBaseAddress(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return cLoBaseAddress;
    }

uint32 Tha60210011ModuleCdrHoBaseAddress(Tha60210011ModuleCdr self)
    {
    AtUnused(self);		
    return cHoBaseAddress;
    }

eAtRet Tha60210011ModuleCdrPktAnalyzerCla2CdrTrigger(Tha60210011ModuleCdr self, uint32 cdrId)
    {
    uint32 regVal;
    uint32 regAddr = cReg_cdr_income_packet_info_trigger_Base + PktAnalyzerCla2CdrBaseAddress(self);

    /* Set trigger CDR ID */
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, c_cdr_income_packet_info_trigger_CDRID_, cdrId);
    mModuleHwWrite(self, regAddr, regVal);

    /* Trigger */
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, c_cdr_income_packet_info_trigger_Trigger_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, c_cdr_income_packet_info_trigger_Trigger_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

void Tha60210011ModuleCdrPktAnalyzerCla2CdrDataFlush(Tha60210011ModuleCdr self)
    {
    const uint16 cMaxPacketNum = 256;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 packetId;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    for (packetId = 0; packetId < cMaxPacketNum; packetId++)
        {
        uint32 regAddr = cReg_cdr_income_packet_info_data_Base + PktAnalyzerCla2CdrBaseAddress(self) + packetId;
        mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
        }
    }

eBool Tha60210011ModuleCdrPktAnalyzerCla2CdrHoIsEnabled(Tha60210011ModuleCdr self, uint16 packetId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cReg_cdr_income_packet_info_data_Base + PktAnalyzerCla2CdrBaseAddress(self) + packetId;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    return mRegField(longRegVal[2], c_cdr_income_packet_info_trigger_HOEnb_) ? cAtTrue : cAtFalse;
    }

eBool Tha60210011ModuleCdrPktAnalyzerCla2CdrPacketIsError(Tha60210011ModuleCdr self, uint16 packetId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cReg_cdr_income_packet_info_data_Base + PktAnalyzerCla2CdrBaseAddress(self) + packetId;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    return mRegField(longRegVal[2], c_cdr_income_packet_info_trigger_PktError_) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210011ModuleCdrPktAnalyzerCla2CdrRtpTimeStampGet(Tha60210011ModuleCdr self, uint16 packetId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cReg_cdr_income_packet_info_data_Base + PktAnalyzerCla2CdrBaseAddress(self) + packetId;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    return mRegField(longRegVal[1], c_cdr_income_packet_info_trigger_RTPTimeStamp_);
    }

uint32 Tha60210011ModuleCdrPktAnalyzerCla2CdrRtpSequenceNumberGet(Tha60210011ModuleCdr self, uint16 packetId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cReg_cdr_income_packet_info_data_Base + PktAnalyzerCla2CdrBaseAddress(self) + packetId;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    return mRegField(longRegVal[0], c_cdr_income_packet_info_trigger_RTPSeqNum_);
    }

uint32 Tha60210011ModuleCdrPktAnalyzerCla2CdrControlWordSequenceNumberGet(Tha60210011ModuleCdr self, uint16 packetId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cReg_cdr_income_packet_info_data_Base + PktAnalyzerCla2CdrBaseAddress(self) + packetId;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, IpCore(self));
    return mRegField(longRegVal[0], c_cdr_income_packet_info_trigger_CWSeqNum_);
    }

AtInterruptManager Tha60210011ModuleCdrVcCdrControllerInterruptManager(Tha60210011ModuleCdr self, ThaCdrController controller)
    {
    if (self)
        return VcCdrControllerInterruptManager(self, controller);
    return NULL;
    }

eBool Tha60210011ModuleCdrFlexibleFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha60210011ModuleCdrTimestampFrequencyInRange(ThaModuleCdr self, uint32 khz)
    {
    uint32 maxVal = cAf6_lodcr_rtp_freq_cfg_DCRRTPFreq_Mask >> cAf6_lodcr_rtp_freq_cfg_DCRRTPFreq_Shift;
    AtUnused(self);
    return (khz <= maxVal) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210011ModuleCdrLoCdrStsTimingCtrl(Tha60210011ModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->LoCdrStsTimingControl(self);
    return cInvalidUint32;
    }

AtSdhChannel Tha60210011CdrControllerSdhChannelGet(ThaCdrController controller)
    {
    AtPdhChannel de1;
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)controller;
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(sdhCdrController, ThaCdrControllerChannelGet(controller));

    if (sdhChannel)
        return sdhChannel;

    /* Channel is DE2-DE1 */
    de1 = (AtPdhChannel)ThaCdrControllerChannelGet(controller);
    return (AtSdhChannel)AtPdhChannelVcInternalGet(AtPdhChannelParentChannelGet(AtPdhChannelParentChannelGet(de1)));
    }

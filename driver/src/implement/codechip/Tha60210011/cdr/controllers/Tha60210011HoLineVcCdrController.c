/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210011HoVcCdrController.c
 *
 * Created Date: Apr 21, 2015
 *
 * Description : CDR Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../sdh/Tha60210011ModuleSdh.h"
#include "../Tha60210011ModuleCdr.h"
#include "Tha60210011HoLineVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegCDRStsTimeCtrl             0x0000100UL
#define cThaVc3TimingModeMask(slice)   (cBit3_0 << (uint16)((slice) * 4))
#define cThaVc3TimingModeShift(slice)  ((slice) * 4)
#define cThaVc3EparEnMask(slice)       (cBit0 << (uint8)(slice))
#define cThaVc3EparEnShift(slice)      (slice)
#define cThaVc3EparEnDwordIndex        1
#define cStsAcrDcrHwMode               8

#define cThaLoLineVc3TimingModeMask   cBit3_0
#define cThaLoLineVc3TimingModeShift  0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;
static tThaHoVcCdrControllerMethods       m_ThaHoVcCdrControllerOverride;

/* Super implement */
static const tThaCdrControllerMethods           *m_ThaCdrControllerMethods = NULL;
static const tThaSdhChannelCdrControllerMethods *m_ThaSdhChannelCdrControllerMethods = NULL;
static const tThaHoVcCdrControllerMethods       *m_ThaHoVcCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 TimingModeSw2Hw(ThaCdrController self, eAtTimingMode timingMode)
    {
    AtUnused(self);

    if (timingMode == cAtTimingModeSys)     return 0;
    if (timingMode == cAtTimingModeLoop)    return 1;
    if (timingMode == cAtTimingModeAcr)     return cStsAcrDcrHwMode;
    if (timingMode == cAtTimingModeDcr)     return cStsAcrDcrHwMode;
    if (timingMode == cAtTimingModeSlave)   return 1;
    if (timingMode == cAtTimingModeExt1Ref) return 6;
    if (timingMode == cAtTimingModeExt2Ref) return 7;
    if (mMethodsGet(self)->FreeRunningIsSupported(self))
        {
        if (timingMode == cAtTimingModeFreeRunning)
            return cStsAcrDcrHwMode; /* CDR Mode*/
        }

    return 0;
    }

static uint8 HwStsTimingModeGet(ThaCdrController self)
    {
    uint8 slice, sts, hwTimingMode;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(self);
    uint32 regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);

    if (Tha60210011ModuleSdhVcBelongsToLoLine(sdhChannel))
        {

        hwTimingMode = (uint8)mRegField(regVal, cThaLoLineVc3TimingMode);
        }
    else
        {
        mFieldGet(regVal,
                  cThaVc3TimingModeMask(slice),
                  cThaVc3TimingModeShift(slice),
                  uint8, &hwTimingMode);
        }

    return hwTimingMode;
    }

static uint8 CdrHwTimingModeGet(ThaCdrController self)
    {
    uint32 regAddr = ThaModuleCdrEngineTimingCtrlRegister(ThaCdrControllerModuleGet(self)) + mMethodsGet(self)->TimingCtrlOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);

    return (uint8)(mRegField(regVal, cThaCDRTimeMode));
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
    uint8 stsTimingMode = HwStsTimingModeGet(self);
    if (stsTimingMode != cStsAcrDcrHwMode)
        return stsTimingMode;

    return CdrHwTimingModeGet(self);
    }

static eAtRet CdrIdAssign(ThaCdrController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(self);

    /* Just LO has this configuration */
    if (Tha60210011ModuleSdhVcBelongsToLoLine(sdhChannel))
        return m_ThaCdrControllerMethods->CdrIdAssign(self);

    return cAtOk;
    }

static AtModule CdrModule(ThaCdrController self)
    {
    return (AtModule)ThaCdrControllerModuleGet(self);
    }

static AtInterruptManager InterruptManager(ThaCdrController self)
    {
    return Tha60210011ModuleCdrVcCdrControllerInterruptManager((Tha60210011ModuleCdr)CdrModule(self), self);
    }

static eAtRet StsTimingModeSet(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    uint8 slice, sts;
    ThaCdrController controller = (ThaCdrController)self;
    uint32 regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(controller)+ mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);
    AtUnused(timingSource);

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);

    if (Tha60210011ModuleSdhVcBelongsToLoLine(sdhChannel))
        mRegFieldSet(regVal, cThaLoLineVc3TimingMode, TimingModeSw2Hw(controller, timingMode));
    else
        {
        mFieldIns(&regVal,
                  cThaVc3TimingModeMask(slice),
                  cThaVc3TimingModeShift(slice),
                  TimingModeSw2Hw(controller, timingMode));
        }

    ThaCdrControllerWrite(controller, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet HoEparEnable(ThaSdhChannelCdrController self, eBool enable)
    {
    uint8 slice, sts;
    uint32 longReg[cThaLongRegMaxSize];
    ThaCdrController controller = (ThaCdrController)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);
    uint32 regAddr;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);
    regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(controller) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    ThaCdrControllerLongRead(controller, regAddr, longReg, cThaLongRegMaxSize, cThaModuleCdr);
    mFieldIns(&longReg[cThaVc3EparEnDwordIndex],
              cThaVc3EparEnMask(slice),
              cThaVc3EparEnShift(slice),
              mBoolToBin(enable));
    ThaCdrControllerLongWrite(controller, regAddr, longReg, cThaLongRegMaxSize, cThaModuleCdr);
    return cAtOk;
    }

static eAtRet EparEnable(ThaSdhChannelCdrController self, eBool enable)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet((ThaCdrController)self);
    if (Tha60210011ModuleSdhVcBelongsToLoLine(sdhChannel))
        return m_ThaSdhChannelCdrControllerMethods->EparEnable(self, enable);

    return HoEparEnable(self, enable);
    }

static eBool HoEparIsEnabled(ThaSdhChannelCdrController self)
    {
    uint8 slice, sts, hwEparEnable;
    uint32 longReg[cThaLongRegMaxSize];
    ThaCdrController controller = (ThaCdrController)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);
    uint32 regAddr;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);
    regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(controller) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    ThaCdrControllerLongRead(controller, regAddr, longReg, cThaLongRegMaxSize, cThaModuleCdr);
    mFieldGet(longReg[cThaVc3EparEnDwordIndex],
              cThaVc3EparEnMask(slice),
              cThaVc3EparEnShift(slice),
              uint8,
              &hwEparEnable);
    return hwEparEnable ? cAtTrue : cAtFalse;
    }

static eBool EparIsEnabled(ThaSdhChannelCdrController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet((ThaCdrController)self);
    if (Tha60210011ModuleSdhVcBelongsToLoLine(sdhChannel))
        return m_ThaSdhChannelCdrControllerMethods->EparIsEnabled(self);

    return HoEparIsEnabled(self);
    }

static uint32 HoTimingSourceId(ThaHoVcCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaCdrController controller = (ThaCdrController)self;
    AtChannel source = mMethodsGet(controller)->TimingSourceForConfiguration(controller, timingMode, timingSource);
    return mMethodsGet(controller)->ChannelIdFlat(controller, source);
    }

static uint32 TimingSourceId(ThaHoVcCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet((ThaCdrController)self);
    if (Tha60210011ModuleSdhVcBelongsToLoLine(sdhChannel))
        return m_ThaHoVcCdrControllerMethods->TimingSourceId(self, timingMode, timingSource);

    return HoTimingSourceId(self, timingMode, timingSource);
    }

static eBool CanSpecifyFixedTimingMode(ThaCdrController self, eAtTimingMode timingMode)
    {
    AtUnused(self);

    /* This product use these two timing sources as system timing, so it is valid
     * when application apply one of them */
    if ((timingMode == cAtTimingModeExt1Ref) ||
        (timingMode == cAtTimingModeSys))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlVcOffset(self);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, HwTimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, CdrIdAssign);
        mMethodOverride(m_ThaCdrControllerOverride, InterruptManager);
        mMethodOverride(m_ThaCdrControllerOverride, CanSpecifyFixedTimingMode);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void OverrideThaSdhChannelCdrController(ThaCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhChannelCdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, m_ThaSdhChannelCdrControllerMethods, sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, StsTimingModeSet);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, EparEnable);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, EparIsEnabled);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void OverrideThaHoVcCdrController(ThaCdrController self)
    {
    ThaHoVcCdrController controller = (ThaHoVcCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHoVcCdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHoVcCdrControllerOverride, m_ThaHoVcCdrControllerMethods, sizeof(m_ThaHoVcCdrControllerOverride));

        mMethodOverride(m_ThaHoVcCdrControllerOverride, TimingSourceId);
        }

    mMethodsSet(controller, &m_ThaHoVcCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController(self);
    OverrideThaHoVcCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011HoLineVcCdrController);
    }

ThaCdrController Tha60210011HoLineVcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHoVcCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60210011HoLineVcCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011HoLineVcCdrControllerObjectInit(newController, engineId, channel);
    }

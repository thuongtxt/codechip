/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210011HoVcCdrControllerInternal.h
 * 
 * Created Date: Aug 21, 2017
 *
 * Description : HO VC CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HOVCCDRCONTROLLERINTERNAL_H_
#define _THA60210011HOVCCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cdr/controllers/ThaHoVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011HoLineVcCdrController
    {
    tThaHoVcCdrController super;
    }tTha60210011HoLineVcCdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha60210011HoLineVcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011HOVCCDRCONTROLLERINTERNAL_H_ */


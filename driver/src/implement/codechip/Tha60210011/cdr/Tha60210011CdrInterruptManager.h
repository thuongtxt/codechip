/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210011CdrInterruptManager.h
 * 
 * Created Date: Dec 21, 2015
 *
 * Description : CDR module HO interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011CDRINTERRUPTMANAGER_H_
#define _THA60210011CDRINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011LoCdrInterruptManager
    {
    tTha60210031CdrInterruptManager super;
    }tTha60210011LoCdrInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60210011LoCdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

AtInterruptManager Tha60210011LoCdrInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210011HoCdrInterruptManagerNew(AtModule module);

ThaCdrController Tha6021CdrControllerFromAuPathGet(AtSdhPath au, uint8 tfi5HwStsInLine, uint8 vt);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011CDRINTERRUPTMANAGER_H_ */


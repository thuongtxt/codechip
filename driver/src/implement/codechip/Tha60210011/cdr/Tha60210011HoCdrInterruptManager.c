/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210011HoCdrInterruptManager.c
 *
 * Created Date: Dec 21, 2015
 *
 * Description : CDR module HO interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"
#include "../man/Tha60210011DeviceReg.h"
#include "Tha60210011ModuleCdrHoReg.h"
#include "Tha60210011CdrInterruptManager.h"
#include "Tha60210011ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mManager(self) ((ThaInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011HoCdrInterruptManager
    {
    tTha60210011LoCdrInterruptManager super;
    }tTha60210011HoCdrInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods   m_ThaCdrInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    uint32 numSlices = mMethodsGet(mManager(self))->NumSlices(mManager(self));
    uint32 intrSliceOR = mMethodsGet(mManager(self))->SlicesInterruptGet(mManager(self), glbIntr, hal);
    uint8 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrSliceOR & cIteratorMask(slice))
            {
            uint32 intrSlice24 = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_chn_intr_or_stat_Base + slice);
            uint32 stsInSlice;

            for (stsInSlice = 0; stsInSlice < 32; stsInSlice++)
                {
                if (intrSlice24 & cIteratorMask(stsInSlice))
                    {
                    uint32 offset = (uint32)(slice << 5) + stsInSlice;
                    uint8 sts48 = (offset & 0x3F);
                    ThaCdrController controller;

                    if (sts48 >= 48)
                        break;

                    controller = ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet((ThaCdrInterruptManager)self, slice, (uint8)stsInSlice, 0);

                    ThaCdrControllerInterruptProcess(controller, offset);
                    }
                }
            }
        }
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mManager(self));
    AtHalWrite(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base, (enable) ? cBit15_0 : 0);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x300000;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x25400;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x25200;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x25000;
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 SlicesInterruptGet(ThaInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    uint32 baseAddress = ThaInterruptManagerBaseAddress(self);
    uint32 intrSliceEn = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_en_ctrl);
    uint32 intrSliceOR = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_or_stat);
    AtUnused(glbIntr);
    return (intrSliceEn & intrSliceOR);
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_status_HoCdrIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet(cdrModule), cAtModuleSdh);
    AtSdhPath au;

    AtUnused(vt);

    au = ThaModuleSdhAuPathFromHwIdGet(sdhModule, cThaModuleCdr, slice, sts);
    if (au)
        {
        AtSdhChannel vc = AtSdhChannelSubChannelGet((AtSdhChannel)au, 0);
        return ThaSdhVcCdrControllerGet((AtSdhVc)vc);
        }

    return NULL;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, SlicesInterruptGet);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, AcrDcrControllerFromHwIdGet);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011HoCdrInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011LoCdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210011HoCdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

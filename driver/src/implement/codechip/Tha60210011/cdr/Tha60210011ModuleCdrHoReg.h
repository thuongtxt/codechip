/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210011ModuleCdrHoReg.h
 * 
 * Created Date: May 14, 2015
 *
 * Description : Registers of HO part
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULECDRHOREG_H_
#define _THA60210011MODULECDRHOREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : CDR CPU  Reg Hold Control
Reg Addr   : 0x030000-0x030002
Reg Formula: 0x030000 + HoldId
    Where  :
           + $HoldId(0-2): Hold register
Reg Desc   :
The register provides hold register for three word 32-bits MSB when CPU access to engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_cpu_hold_ctrl_Base                                                                0x030000
#define cAf6Reg_cdr_cpu_hold_ctrl(HoldId)                                                  (0x030000+(HoldId))
#define cAf6Reg_cdr_cpu_hold_ctrl_WidthVal                                                                  32
#define cAf6Reg_cdr_cpu_hold_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: HoldReg0
BitField Type: RW
BitField Desc: Hold 32 bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_Bit_Start                                                            0
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_Bit_End                                                             31
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_Mask                                                          cBit31_0
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_Shift                                                                0
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_MaxVal                                                      0xffffffff
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_MinVal                                                             0x0
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Timing External reference control
Reg Addr   : 0x0000003
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_timing_extref_ctrl_Base                                                          0x0000003
#define cAf6Reg_cdr_timing_extref_ctrl                                                               0x0000003
#define cAf6Reg_cdr_timing_extref_ctrl_WidthVal                                                             64
#define cAf6Reg_cdr_timing_extref_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: Ext3N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the third external reference signal
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Bit_Start                                                       32
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Bit_End                                                         47
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Mask                                                      cBit15_0
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Shift                                                            0
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_MaxVal                                                         0x0
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_MinVal                                                         0x0
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Ext2N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the second external reference signal
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Bit_Start                                                       16
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Bit_End                                                         31
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Mask                                                     cBit31_16
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Shift                                                           16
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_MaxVal                                                      0xffff
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_MinVal                                                         0x0
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Ext1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the first external reference signal
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Bit_Start                                                        0
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Bit_End                                                         15
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Mask                                                      cBit15_0
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Shift                                                            0
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_MaxVal                                                      0xffff
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_MinVal                                                         0x0
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR STS Timing control
Reg Addr   : 0x0000100-0x000011F
Reg Formula: 0x0000100+STS
    Where  :
           + $STS(0-47):
Reg Desc   :
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_sts_timing_ctrl_Base                                                             0x0000100
#define cAf6Reg_cdr_sts_timing_ctrl(STS)                                                     (0x0000100+(STS))
#define cAf6Reg_cdr_sts_timing_ctrl_WidthVal                                                                64
#define cAf6Reg_cdr_sts_timing_ctrl_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: slc7VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [39]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_Bit_Start                                                    39
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_Bit_End                                                      39
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_Mask                                                      cBit7
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_Shift                                                         7
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc7VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc6VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [38]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_Bit_Start                                                    38
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_Bit_End                                                      38
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_Mask                                                      cBit6
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_Shift                                                         6
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc6VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc5VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [37]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_Bit_Start                                                    37
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_Bit_End                                                      37
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_Mask                                                      cBit5
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_Shift                                                         5
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc5VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc4VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [36]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_Bit_Start                                                    36
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_Bit_End                                                      36
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_Mask                                                      cBit4
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_Shift                                                         4
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc4VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc3VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [35]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_Bit_Start                                                    35
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_Bit_End                                                      35
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_Mask                                                      cBit3
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_Shift                                                         3
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc3VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc2VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [34]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_Bit_Start                                                    34
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_Bit_End                                                      34
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_Mask                                                      cBit2
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_Shift                                                         2
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc2VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc1VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [33]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_Bit_Start                                                    33
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_Bit_End                                                      33
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_Mask                                                      cBit1
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_Shift                                                         1
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc1VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc0VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [32]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_Bit_Start                                                    32
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_Bit_End                                                      32
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_Mask                                                   cBit32_0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_Shift                                                         0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_MaxVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_MinVal                                                      0x0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3EParEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: slc7VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_Bit_Start                                                  28
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_Bit_End                                                    31
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_Mask                                                cBit31_28
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_Shift                                                      28
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc7VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc6VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_Bit_Start                                                  24
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_Bit_End                                                    27
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_Mask                                                cBit27_24
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_Shift                                                      24
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc6VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc5VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_Bit_Start                                                  20
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_Bit_End                                                    23
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_Mask                                                cBit23_20
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_Shift                                                      20
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc5VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc4VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_Bit_Start                                                  16
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_Bit_End                                                    19
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_Mask                                                cBit19_16
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_Shift                                                      16
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc4VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc3VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_Bit_Start                                                  12
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_Bit_End                                                    15
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_Mask                                                cBit15_12
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_Shift                                                      12
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc3VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc2VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_Bit_Start                                                   8
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_Bit_End                                                    11
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_Mask                                                 cBit11_8
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_Shift                                                       8
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc2VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc1VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_Bit_Start                                                   4
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_Bit_End                                                     7
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_Mask                                                  cBit7_4
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_Shift                                                       4
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc1VC3TimeMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: slc0VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR
timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_Bit_Start                                                   0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_Bit_End                                                     3
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_Mask                                                  cBit3_0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_Shift                                                       0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_MaxVal                                                    0xf
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_MinVal                                                    0x0
#define cAf6_cdr_sts_timing_ctrl_slc0VC3TimeMode_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR Engine Timing control
Reg Addr   : 0x0020800-0x0020BFF
Reg Formula: 0x0020800 + 64*slice + stsid
    Where  :
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   :
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_acr_eng_timing_ctrl_Base                                                         0x0020800
#define cAf6Reg_cdr_acr_eng_timing_ctrl(slice, stsid)                           (0x0020800+64*(slice)+(stsid))
#define cAf6Reg_cdr_acr_eng_timing_ctrl_WidthVal                                                            32
#define cAf6Reg_cdr_acr_eng_timing_ctrl_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: VC4_4c
BitField Type: RW
BitField Desc: VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c
BitField Bits: [26]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Bit_Start                                                       26
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Bit_End                                                         26
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Mask                                                        cBit26
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Shift                                                           26
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_MaxVal                                                         0x1
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_MinVal                                                         0x0
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PktLen_ind
BitField Type: RW
BitField Desc: The payload packet  length for jumbo frame
BitField Bits: [25]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Bit_Start                                                   25
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Bit_End                                                     25
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Mask                                                    cBit25
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Shift                                                       25
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_MaxVal                                                     0x1
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_MinVal                                                     0x0
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_RstVal                                                     0x0

/*--------------------------------------
BitField Name: HoldValMode
BitField Type: RW
BitField Desc: Hold value mode of NCO, default value 0 0: Hardware calculated
and auto update 1: Software calculated and update, hardware is disabled
BitField Bits: [24]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Bit_Start                                                  24
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Bit_End                                                    24
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Mask                                                   cBit24
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Shift                                                      24
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_MaxVal                                                    0x1
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_MinVal                                                    0x0
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_RstVal                                                    0x0

/*--------------------------------------
BitField Name: SeqMode
BitField Type: RW
BitField Desc: Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero
BitField Bits: [23]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Bit_Start                                                      23
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Bit_End                                                        23
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Mask                                                       cBit23
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Shift                                                          23
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_MaxVal                                                        0x1
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_MinVal                                                        0x0
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_RstVal                                                        0x0

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0-5: unused 6: STS1/TU3 7: VC4/VC4-4c
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Bit_Start                                                     20
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Bit_End                                                       22
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Mask                                                   cBit22_20
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Shift                                                         20
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_MaxVal                                                       0x7
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_MinVal                                                       0x0
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PktLen
BitField Type: RW
BitField Desc: The payload packet  length parameter to create a packet. SAToP
mode: The number payload of bit. CESoPSN mode: The number of bit which converted
to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0
with M frame, the value configured to this register is Mx256 bits for E1 mode,
Mx193 bits for DS1 mode.
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Bit_Start                                                        4
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Bit_End                                                         19
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Mask                                                      cBit19_4
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Shift                                                            4
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_MaxVal                                                      0xffff
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_MinVal                                                         0x0
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_RstVal                                                         0x0

/*--------------------------------------
BitField Name: CDRTimeMode
BitField Type: RW
BitField Desc: CDR time mode 0-7: unused 8: ACR timing mode 9: unused 10: DCR
timing mode 11: unused
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Bit_Start                                                   0
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Bit_End                                                     3
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Mask                                                  cBit3_0
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Shift                                                       0
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_MaxVal                                                    0xf
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_MinVal                                                    0x0
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust State status
Reg Addr   : 0x0021000-0x00213FF
Reg Formula: 0x0021000 + 64*slice + stsid
    Where  :
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   :
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_adj_state_stat_Base                                                              0x0021000
#define cAf6Reg_cdr_adj_state_stat(slice, stsid)                                (0x0021000+64*(slice)+(stsid))
#define cAf6Reg_cdr_adj_state_stat_WidthVal                                                                 32
#define cAf6Reg_cdr_adj_state_stat_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Adjstate
BitField Type: RW
BitField Desc: Adjust state 0: Load state 1: Wait state 2: Initialization state
3: Learn State 4: Rapid State 5: Lock State 6: Freeze State 7: Holdover State
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_cdr_adj_state_stat_Adjstate_Bit_Start                                                           0
#define cAf6_cdr_adj_state_stat_Adjstate_Bit_End                                                             2
#define cAf6_cdr_adj_state_stat_Adjstate_Mask                                                          cBit2_0
#define cAf6_cdr_adj_state_stat_Adjstate_Shift                                                               0
#define cAf6_cdr_adj_state_stat_Adjstate_MaxVal                                                            0x7
#define cAf6_cdr_adj_state_stat_Adjstate_MinVal                                                            0x0
#define cAf6_cdr_adj_state_stat_Adjstate_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust Holdover value status
Reg Addr   : 0x0021800-0x0021BFF
Reg Formula: 0x0021800 + 64*slice + stsid
    Where  :
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   :
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_adj_holdover_value_stat_Base                                                     0x0021800
#define cAf6Reg_cdr_adj_holdover_value_stat(slice, stsid)                       (0x0021800+64*(slice)+(stsid))
#define cAf6Reg_cdr_adj_holdover_value_stat_WidthVal                                                        32
#define cAf6Reg_cdr_adj_holdover_value_stat_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: HoldVal
BitField Type: RW
BitField Desc: NCO Holdover value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Bit_Start                                                   0
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Bit_End                                                    31
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Mask                                                 cBit31_0
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Shift                                                       0
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_MaxVal                                             0xffffffff
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_MinVal                                                    0x0
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR TX Engine Active Control
Reg Addr   : 0x010000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to activate the DCR TX Engine. Active high.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_tx_eng_active_ctrl_Base                                                           0x010000
#define cAf6Reg_dcr_tx_eng_active_ctrl                                                                0x010000
#define cAf6Reg_dcr_tx_eng_active_ctrl_WidthVal                                                             32
#define cAf6Reg_dcr_tx_eng_active_ctrl_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: data
BitField Type: RW
BitField Desc: DCR TX Engine Active Control
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dcr_tx_eng_active_ctrl_data_Bit_Start                                                           0
#define cAf6_dcr_tx_eng_active_ctrl_data_Bit_End                                                            31
#define cAf6_dcr_tx_eng_active_ctrl_data_Mask                                                         cBit31_0
#define cAf6_dcr_tx_eng_active_ctrl_data_Shift                                                               0
#define cAf6_dcr_tx_eng_active_ctrl_data_MaxVal                                                     0xffffffff
#define cAf6_dcr_tx_eng_active_ctrl_data_MinVal                                                            0x0
#define cAf6_dcr_tx_eng_active_ctrl_data_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Source Select Configuration
Reg Addr   : 0x0010001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prc_src_sel_cfg_Base                                                             0x0010001
#define cAf6Reg_dcr_prc_src_sel_cfg                                                                  0x0010001
#define cAf6Reg_dcr_prc_src_sel_cfg_WidthVal                                                                32
#define cAf6Reg_dcr_prc_src_sel_cfg_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: DcrPrcDirectMode
BitField Type: RW
BitField Desc: 0: Select RTP frequency lower than 65535 and use value DCRRTPFreq
as RTP frequency 1: Select RTP frequency value 155.52Mhz divide by
DcrRtpFreqDivide. In this mode, below DcrPrcSourceSel[2:0] must set to 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_Bit_Start                                                  3
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_Bit_End                                                    3
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_Mask                                                   cBit3
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_Shift                                                      3
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_MaxVal                                                   0x1
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_MinVal                                                   0x0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcDirectMode_RstVal                                                   0x0

/*--------------------------------------
BitField Name: DcrPrcSourceSel
BitField Type: RW
BitField Desc: PRC source selection. 0: PRC Reference Clock 1: System Clock
19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line
Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock
Port 4
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Bit_Start                                                   0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Bit_End                                                     2
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Mask                                                  cBit2_0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Shift                                                       0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_MaxVal                                                    0x7
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_MinVal                                                    0x0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Frequency Configuration
Reg Addr   : 0x0010002
Reg Formula:
    Where  :
Reg Desc   :
This register is used to select RTP frequency 77.76Mhz or 155.52Mhz

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prd_freq_cfg_Base                                                                0x0010002
#define cAf6Reg_dcr_prd_freq_cfg                                                                     0x0010002
#define cAf6Reg_dcr_prd_freq_cfg_WidthVal                                                                   32
#define cAf6Reg_dcr_prd_freq_cfg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DcrRtpFreqDivide
BitField Type: RW
BitField Desc: Dcr RTP Frequency = 155.52Mhz/ DcrRtpFreqDivide
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_Bit_Start                                                     0
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_Bit_End                                                      15
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_Mask                                                   cBit15_0
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_Shift                                                         0
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_MaxVal                                                   0xffff
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_MinVal                                                      0x0
#define cAf6_dcr_prd_freq_cfg_DcrRtpFreqDivide_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Frequency Configuration
Reg Addr   : 0x001000b
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prc_freq_cfg_Base                                                                0x001000b
#define cAf6Reg_dcr_prc_freq_cfg                                                                     0x001000b
#define cAf6Reg_dcr_prc_freq_cfg_WidthVal                                                                   32
#define cAf6Reg_dcr_prc_freq_cfg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DCRPrcFrequency
BitField Type: RW
BitField Desc: Frequency of PRC clock. This is used to configure the frequency
of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_Mask                                                    cBit17_0
#define cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_Shift                                                          0

/*------------------------------------------------------------------------------
Reg Name   : DCR RTP Frequency Configuration
Reg Addr   : 0x001000C
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_rtp_freq_cfg_Base                                                                0x001000C
#define cAf6Reg_dcr_rtp_freq_cfg                                                                     0x001000C
#define cAf6Reg_dcr_rtp_freq_cfg_WidthVal                                                                   32
#define cAf6Reg_dcr_rtp_freq_cfg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DCRRTPFreq
BitField Type: RW
BitField Desc: Frequency of RTP clock. This is used to configure the frequency
of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_Mask                                                         cBit17_0
#define cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_Shift                                                               0

/*------------------------------------------------------------------------------
Reg Name   : RAM TimingGen Parity Force Control
Reg Addr   : 0x188
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_TimingGen_Parity_Force_Control_Base                                                  0x188
#define cAf6Reg_RAM_TimingGen_Parity_Force_Control                                                       0x188
#define cAf6Reg_RAM_TimingGen_Parity_Force_Control_WidthVal                                                 32
#define cAf6Reg_RAM_TimingGen_Parity_Force_Control_WriteMask                                               0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR STS Timing control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Bit_Start                                       0
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Bit_End                                       0
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Mask                                   cBit0
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Shift                                       0
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM TimingGen Parity Disable Control
Reg Addr   : 0x189
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_TimingGen_Parity_Diable_Control_Base                                                 0x189
#define cAf6Reg_RAM_TimingGen_Parity_Diable_Control                                                      0x189
#define cAf6Reg_RAM_TimingGen_Parity_Diable_Control_WidthVal                                                32
#define cAf6Reg_RAM_TimingGen_Parity_Diable_Control_WriteMask                                              0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR STS Timing control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_Bit_Start                                       0
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_Bit_End                                       0
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_Shift                                       0
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_TimingGen_Parity_Diable_Control_CDRSTSTimingCtrl_ParErrDis_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM TimingGen parity Error Sticky
Reg Addr   : 0x18a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_TimingGen_Parity_Error_Sticky_Base                                                   0x18a
#define cAf6Reg_RAM_TimingGen_Parity_Error_Sticky                                                        0x18a
#define cAf6Reg_RAM_TimingGen_Parity_Error_Sticky_WidthVal                                                  32
#define cAf6Reg_RAM_TimingGen_Parity_Error_Sticky_WriteMask                                                0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR STS Timing control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_Bit_Start                                       0
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_Bit_End                                       0
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_Mask                                   cBit0
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_Shift                                       0
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM ACR Parity Force Control
Reg Addr   : 0x2000c
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_ACR_Parity_Force_Control_Base                                                      0x2000c
#define cAf6Reg_RAM_ACR_Parity_Force_Control                                                           0x2000c
#define cAf6Reg_RAM_ACR_Parity_Force_Control_WidthVal                                                       32
#define cAf6Reg_RAM_ACR_Parity_Force_Control_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc7_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_Bit_Start                                       7
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_Bit_End                                       7
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_Mask                                   cBit7
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_Shift                                       7
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc7_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc6_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_Bit_Start                                       6
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_Bit_End                                       6
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_Mask                                   cBit6
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_Shift                                       6
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc6_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc5_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_Bit_Start                                       5
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_Bit_End                                       5
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_Mask                                   cBit5
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_Shift                                       5
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc5_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc4_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_Bit_Start                                       4
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_Bit_End                                       4
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_Mask                                   cBit4
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_Shift                                       4
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc4_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc3_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_Bit_Start                                       3
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_Bit_End                                       3
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_Mask                                   cBit3
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_Shift                                       3
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc3_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc2_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_Bit_Start                                       2
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_Bit_End                                       2
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_Mask                                   cBit2
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_Shift                                       2
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc2_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_Bit_Start                                       1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_Bit_End                                       1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_Mask                                   cBit1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_Shift                                       1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc1_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
Slice0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_Bit_Start                                       0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_Bit_End                                       0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_Mask                                   cBit0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_Shift                                       0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrlSlc0_ParErrFrc_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM ACR Parity Disable Control
Reg Addr   : 0x2000d
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_ACR_Parity_Disable_Control_Base                                                    0x2000d
#define cAf6Reg_RAM_ACR_Parity_Disable_Control                                                         0x2000d
#define cAf6Reg_RAM_ACR_Parity_Disable_Control_WidthVal                                                     32
#define cAf6Reg_RAM_ACR_Parity_Disable_Control_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc7_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_Bit_Start                                       7
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_Bit_End                                       7
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_Mask                                   cBit7
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_Shift                                       7
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc7_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc6_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_Bit_Start                                       6
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_Bit_End                                       6
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_Mask                                   cBit6
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_Shift                                       6
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc6_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc5_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_Bit_Start                                       5
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_Bit_End                                       5
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_Mask                                   cBit5
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_Shift                                       5
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc5_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc4_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_Bit_Start                                       4
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_Bit_End                                       4
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_Mask                                   cBit4
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_Shift                                       4
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc4_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc3_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_Bit_Start                                       3
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_Bit_End                                       3
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_Mask                                   cBit3
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_Shift                                       3
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc3_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc2_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_Bit_Start                                       2
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_Bit_End                                       2
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_Mask                                   cBit2
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_Shift                                       2
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc2_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc1_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_Bit_Start                                       1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_Bit_End                                       1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_Shift                                       1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
Slice0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_Bit_Start                                       0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_Bit_End                                       0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_Shift                                       0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParErrDis_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM ACR parity Error Sticky
Reg Addr   : 0x2000e
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_ACR_Parity_Error_Sticky_Base                                                       0x2000e
#define cAf6Reg_RAM_ACR_Parity_Error_Sticky                                                            0x2000e
#define cAf6Reg_RAM_ACR_Parity_Error_Sticky_WidthVal                                                        32
#define cAf6Reg_RAM_ACR_Parity_Error_Sticky_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc7_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_Bit_Start                                       7
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_Bit_End                                       7
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_Mask                                   cBit7
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_Shift                                       7
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc7_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc6_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_Bit_Start                                       6
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_Bit_End                                       6
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_Mask                                   cBit6
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_Shift                                       6
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc6_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc5_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_Bit_Start                                       5
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_Bit_End                                       5
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_Mask                                   cBit5
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_Shift                                       5
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc5_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc4_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_Bit_Start                                       4
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_Bit_End                                       4
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_Mask                                   cBit4
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_Shift                                       4
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc4_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc3_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_Bit_Start                                       3
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_Bit_End                                       3
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_Mask                                   cBit3
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_Shift                                       3
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc3_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc2_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_Bit_Start                                       2
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_Bit_End                                       2
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_Mask                                   cBit2
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_Shift                                       2
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc2_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc1_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Bit_Start                                       1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Bit_End                                       1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Mask                                   cBit1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Shift                                       1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"
Slice0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Bit_Start                                       0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Bit_End                                       0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Mask                                   cBit0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Shift                                       0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_RstVal                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Enable Control
Reg Addr   : 0x00025000-0x000251FF
Reg Formula: 0x00025000 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt enable of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_Base                                                       0x00025000
#define cAf6Reg_cdr_per_chn_intr_en_ctrl(StsID, VtnID)                         (0x00025000+(StsID)*32+(VtnID))
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_WidthVal                                                           32
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CDRUnlokcedIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change UnLocked te event to generate an
interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Bit_Start                                            0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Bit_End                                              0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Mask                                             cBit0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Shift                                                0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_MaxVal                                             0x1
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_MinVal                                             0x0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Status
Reg Addr   : 0x00025200-0x000253FF
Reg Formula: 0x00025200 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_stat_Base                                                          0x00025200
#define cAf6Reg_cdr_per_chn_intr_stat(StsID, VtnID)                            (0x00025200+(StsID)*32+(VtnID))
#define cAf6Reg_cdr_per_chn_intr_stat_WidthVal                                                              32
#define cAf6Reg_cdr_per_chn_intr_stat_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: CDRUnLockedIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in UnLocked the event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Bit_Start                                                 0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Bit_End                                                   0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Mask                                                  cBit0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Shift                                                     0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_MaxVal                                                  0x1
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_MinVal                                                  0x0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Current Status
Reg Addr   : 0x00025400-0x000255FF
Reg Formula: 0x00025400 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel Current tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_curr_stat_Base                                                          0x00025400
#define cAf6Reg_cdr_per_chn_curr_stat(StsID, VtnID)                            (0x00025400+(StsID)*32+(VtnID))
#define cAf6Reg_cdr_per_chn_curr_stat_WidthVal                                                              32
#define cAf6Reg_cdr_per_chn_curr_stat_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: CDRUnLockedCurrSta
BitField Type: RW
BitField Desc: Current tus of UnLocked event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Bit_Start                                              0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Bit_End                                                0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Mask                                               cBit0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Shift                                                  0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_MaxVal                                               0x1
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_MinVal                                               0x0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt OR Status
Reg Addr   : 0x00025600-0x0002560F
Reg Formula: 0x00025600 +  StsID
    Where  : 
           + $StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_or_stat_Base                                                       0x00025600
#define cAf6Reg_cdr_per_chn_intr_or_stat(StsID)                                           (0x00025600+(StsID))
#define cAf6Reg_cdr_per_chn_intr_or_stat_WidthVal                                                           32
#define cAf6Reg_cdr_per_chn_intr_or_stat_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CDRVtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding DS1/E1 is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Bit_Start                                               0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Bit_End                                                31
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Mask                                             cBit31_0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Shift                                                   0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_MaxVal                                         0xffffffff
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_MinVal                                                0x0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt OR Status
Reg Addr   : 0x000257FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 16 bits for 16 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_Base                                                     0x000257FF
#define cAf6Reg_cdr_per_stsvc_intr_or_stat                                                          0x000257FF
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_WidthVal                                                         32
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: CDRStsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding STS/VC is
set and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Bit_Start                                            0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Bit_End                                             31
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Mask                                          cBit31_0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Shift                                                0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_MaxVal                                      0xffffffff
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_MinVal                                             0x0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt Enable Control
Reg Addr   : 0x000257FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base                                                     0x000257FE
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl                                                          0x000257FE
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_WidthVal                                                         32
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: CDRStsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Bit_Start                                               0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Bit_End                                                31
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Mask                                             cBit31_0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Shift                                                   0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_MaxVal                                         0xffffffff
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_MinVal                                                0x0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_RstVal                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : CDR InCome Packet Information Capture Trigger
Reg Addr   : 0x0023007
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 16 interrupt enable bits for 16 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cReg_cdr_income_packet_info_trigger_Base                                                     0x0023007
#define cReg_cdr_income_packet_info_trigger                                                          0x0023007
#define cReg_cdr_income_packet_info_trigger_WidthVal                                                        32
#define cReg_cdr_income_packet_info_trigger_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: Trigger
BitField Type: RW
BitField Desc: Dump trigger, write 1 and write 0.
BitField Bits: [16]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_Trigger_Bit_Start                                                  16
#define c_cdr_income_packet_info_trigger_Trigger_Bit_End                                                    16
#define c_cdr_income_packet_info_trigger_Trigger_Mask                                                   cBit16
#define c_cdr_income_packet_info_trigger_Trigger_Shift                                                      16
#define c_cdr_income_packet_info_trigger_Trigger_MaxVal                                                    0x1
#define c_cdr_income_packet_info_trigger_Trigger_MinVal                                                    0x0
#define c_cdr_income_packet_info_trigger_Trigger_RstVal                                                    0x0

/*--------------------------------------
BitField Name: CDRID
BitField Type: RW
BitField Desc: Set CDR ID, same value bit[14:0] of Classify Per Pseudowire
Identification to CDR Control.
BitField Bits: [15:0]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_CDRID_Bit_Start                                                     0
#define c_cdr_income_packet_info_trigger_CDRID_Bit_End                                                      15
#define c_cdr_income_packet_info_trigger_CDRID_Mask                                                   cBit15_0
#define c_cdr_income_packet_info_trigger_CDRID_Shift                                                         0
#define c_cdr_income_packet_info_trigger_CDRID_MaxVal                                                   0xffff
#define c_cdr_income_packet_info_trigger_CDRID_MinVal                                                      0x0
#define c_cdr_income_packet_info_trigger_CDRID_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR InCome Packet Information Data
Reg Addr   : 0x0023100
Reg Formula: 0x0023100 +  entry
    Where  :
           + $entry(0-255): 256 packet continuous
Reg Desc   :
The register consists of 16 interrupt enable bits for 16 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cReg_cdr_income_packet_info_data_Base                                                     0x0023100
#define cReg_cdr_income_packet_info_data(entry)                                         (0x0023100+(entry))
#define cReg_cdr_income_packet_info_data_WidthVal                                                       128
#define cReg_cdr_income_packet_info_data_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: HOEnb
BitField Type: RW
BitField Desc: High Order path enable
BitField Bits: [65]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_HOEnb_Bit_Start                                                    65
#define c_cdr_income_packet_info_trigger_HOEnb_Bit_End                                                      65
#define c_cdr_income_packet_info_trigger_HOEnb_Mask                                                      cBit1
#define c_cdr_income_packet_info_trigger_HOEnb_Shift                                                         1
#define c_cdr_income_packet_info_trigger_HOEnb_MaxVal                                                      0x0
#define c_cdr_income_packet_info_trigger_HOEnb_MinVal                                                      0x0
#define c_cdr_income_packet_info_trigger_HOEnb_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PktError
BitField Type: RW
BitField Desc: packet error, set 1 incase of Lbit
BitField Bits: [64]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_PktError_Bit_Start                                                 64
#define c_cdr_income_packet_info_trigger_PktError_Bit_End                                                   64
#define c_cdr_income_packet_info_trigger_PktError_Mask                                                   cBit0
#define c_cdr_income_packet_info_trigger_PktError_Shift                                                      0
#define c_cdr_income_packet_info_trigger_PktError_MaxVal                                                   0x0
#define c_cdr_income_packet_info_trigger_PktError_MinVal                                                   0x0
#define c_cdr_income_packet_info_trigger_PktError_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RTPTimeStamp
BitField Type: RW
BitField Desc: RTP timestamp
BitField Bits: [63:32]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_Bit_Start                                             32
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_Bit_End                                               63
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_Mask                                            cBit31_0
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_Shift                                                  0
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_MaxVal                                               0x0
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_MinVal                                               0x0
#define c_cdr_income_packet_info_trigger_RTPTimeStamp_RstVal                                               0x0

/*--------------------------------------
BitField Name: RTPSeqNum
BitField Type: RW
BitField Desc: RTP Sequence Number
BitField Bits: [31:16]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_RTPSeqNum_Bit_Start                                                16
#define c_cdr_income_packet_info_trigger_RTPSeqNum_Bit_End                                                  31
#define c_cdr_income_packet_info_trigger_RTPSeqNum_Mask                                              cBit31_16
#define c_cdr_income_packet_info_trigger_RTPSeqNum_Shift                                                    16
#define c_cdr_income_packet_info_trigger_RTPSeqNum_MaxVal                                               0xffff
#define c_cdr_income_packet_info_trigger_RTPSeqNum_MinVal                                                  0x0
#define c_cdr_income_packet_info_trigger_RTPSeqNum_RstVal                                                  0x0

/*--------------------------------------
BitField Name: CWSeqNum
BitField Type: RW
BitField Desc: Control Word Sequence Number
BitField Bits: [15:0]
--------------------------------------*/
#define c_cdr_income_packet_info_trigger_CWSeqNum_Bit_Start                                                  0
#define c_cdr_income_packet_info_trigger_CWSeqNum_Bit_End                                                   15
#define c_cdr_income_packet_info_trigger_CWSeqNum_Mask                                                cBit15_0
#define c_cdr_income_packet_info_trigger_CWSeqNum_Shift                                                      0
#define c_cdr_income_packet_info_trigger_CWSeqNum_MaxVal                                                0xffff
#define c_cdr_income_packet_info_trigger_CWSeqNum_MinVal                                                   0x0
#define c_cdr_income_packet_info_trigger_CWSeqNum_RstVal                                                   0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULECDRHOREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : Tha602100xxCommon.h
 * 
 * Created Date: Aug 11, 2016
 *
 * Description : Common declarations used for all of 602100xx products.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA602100XXCOMMON_H_
#define _THA602100XXCOMMON_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/pw/ThaModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha602100xxModulePdhShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self);
eBool Tha602100xxModulePdhDe1LiuShouldReportLofWhenLos(ThaModulePdh self);
eBool Tha602100xxModulePdhDe3LiuShouldReportLofWhenLos(ThaModulePdh self);
eBool Tha602100xxModuleAutoRxMBitShouldEnable(AtModulePw self);
eBool Tha602100xxModulePwHitlessHeaderChangeShouldBeEnabledByDefault(ThaModulePw self);
eBool Tha602100xxHwAccessOptimized(void);

#ifdef __cplusplus
}
#endif
#endif /* _THA602100XXCOMMON_H_ */


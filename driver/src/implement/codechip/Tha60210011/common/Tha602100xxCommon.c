/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : Tha602100xxCommon.c
 *
 * Created Date: Aug 11, 2016
 *
 * Description : Common implementation for all of 602100xx products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha602100xxCommon.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool Tha602100xxModulePdhShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self)
    {
    /* Now this is OFF only for LAB test, once customer is agree, this has to be
     * applied on main branch */
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha602100xxModulePwHitlessHeaderChangeShouldBeEnabledByDefault(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha602100xxHwAccessOptimized(void)
    {
    return cAtTrue;
    }

eBool Tha602100xxModulePdhDe1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha602100xxModulePdhDe3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    /* Until DS3 LIU also has the same issue, then this has to be cAtTrue */
    AtUnused(self);
    return cAtFalse;
    }

eBool Tha602100xxModuleAutoRxMBitShouldEnable(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210011ModuleDemap.c
 *
 * Created Date: May 06, 2015
 *
 * Description : PWCodechip @60210011 TDM-to-PW mapping.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../man/Tha60210011Device.h"
#include "../common/Tha602100xxCommon.h"
#include "Tha60210011ModuleMapInternal.h"
#include "Tha60210011ModuleMapHoReg.h"
#include "Tha60210011ModuleMapLoReg.h"
#include "Tha60210011ModuleMapAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleDemap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleDemapMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tThaModuleDemapMethods       m_ThaModuleDemapOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods = NULL;
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleDemapMethods       *m_ThaModuleDemapMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    return Tha60210011ModuleMapDemapSliceOffset(self, sdhChannel, slice);
    }

static uint32 VcDmapChnCtrl(ThaModuleDemap self, AtSdhVc vc)
    {
    if (mMethodsGet(mThis(self))->VcIsHoCep(mThis(self), vc))
        return cAf6Reg_demap_channel_ctrl_Base;

    return cAf6Reg_lodemap_channel_ctrl_Base;
    }

static eBool IsLongRegDemapCtrl(Tha60210011ModuleDemap self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 De1DmapChnCtrl(ThaModuleDemap self, AtPdhDe1 de1)
    {
    return VcDmapChnCtrl(self, (AtSdhVc)AtPdhChannelVcInternalGet((AtPdhChannel)de1));
    }

static eAtRet BindHoStsToPseudowire(Tha60210011ModuleDemap self, AtSdhVc vc, uint8 hwSlice, uint8 hwSts, AtPw pw)
    {
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eBool enable = mMethodsGet(abstractMap)->NeedEnableAfterBindToPw(abstractMap, pw);
    uint32 regAddr = ThaModuleDemapVcDmapChnCtrl((ThaModuleDemap)self, vc) + ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
    uint32 regVal  = mChannelHwRead(vc, regAddr, cThaModuleDemap);
    uint32 enableMask = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnMask(self);
    uint32 enableShift = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnShift(self);
    mRegFieldSet(regVal, enable, pw ? enable : 0);
    mChannelHwWrite(vc, regAddr, regVal, cThaModuleDemap);
    return cAtOk;
    }

static eAtRet BindHoLineVcToPseudowire(Tha60210011ModuleDemap self, AtSdhVc vc, AtPw pw)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        mMethodsGet(self)->BindHoStsToPseudowire(self, vc, hwSlice, hwSts, pw);
        }
    return cAtOk;
    }

static eBool IsVc4_64c(AtSdhVc vc)
    {
    return (AtSdhChannelTypeGet((AtSdhChannel)vc) == cAtSdhChannelTypeVc4_64c) ? cAtTrue : cAtFalse;
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    if (IsVc4_64c(vc))
        return cAtOk;

    if (mMethodsGet(mThis(self))->VcIsHoCep(mThis(self), vc))
        return mMethodsGet(mThis(self))->BindHoLineVcToPseudowire(mThis(self), vc, pw);

    return m_ThaModuleAbstractMapMethods->BindAuVcToPseudowire(self, vc, pw);
    }

static eBool VcIsHoCep(Tha60210011ModuleDemap self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210011ModuleSdhChannelIsHoPath((AtSdhChannel)vc);
    }

static uint32 AuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode)
    {
    if (cepMode)
        *cepMode = cAtPwCepModeBasic;

    if (mMethodsGet(mThis(self))->VcIsHoCep(mThis(self), vc))
        {
        uint8 slice, hwSts;
        if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModuleDemap, &slice, &hwSts) != cAtOk)
            return cInvalidUint32;

        return hwSts;
        }

    return m_ThaModuleAbstractMapMethods->AuVcBoundPwHwIdGet(self, vc, cepMode);
    }

static eAtRet HoLineVcxEncapConnectionEnable(Tha60210011ModuleDemap self, AtSdhVc vcx, eBool enable)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 demapChnCtrl = mMethodsGet((ThaModuleDemap)self)->VcDmapChnCtrl((ThaModuleDemap)self, vcx);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint32 enableMask = mMethodsGet(mThis(self))->HoVcDemapChannelCtrlDemapChEnMask(mThis(self));
    uint32 enableShift = mMethodsGet(mThis(self))->HoVcDemapChannelCtrlDemapChEnShift(mThis(self));
    ThaModuleAbstractMap demapModule = (ThaModuleAbstractMap)self;

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = demapChnCtrl + mMethodsGet(demapModule)->AuVcStsDefaultOffset(demapModule, vcx, (uint8)(stsId + sts_i));
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);
        mRegFieldSet(regVal, enable, mBoolToBin(enable));
        mChannelHwWrite(vcx, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsDemapRegister(AtModuleDeviceGet(self), address);
    }

static eAtRet VcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    if (IsVc4_64c(vcx))
        return cAtOk;

    if(mMethodsGet(mThis(self))->VcIsHoCep(mThis(self), vcx))
    return mMethodsGet(mThis(self))->HoLineVcxEncapConnectionEnable(mThis(self), vcx, enable);

    return m_ThaModuleAbstractMapMethods->VcxEncapConnectionEnable(self, vcx, enable);
    }

static eBool NeedEnableAfterBindToPw(ThaModuleAbstractMap self, AtPw pw)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)pw;/*This pw is input as pw adater */
    AtUnused(self);

    if (pw && !ThaPwAdapterIsLogicPw(adapter) && AtChannelIsEnabled((AtChannel)pw))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    if (mMethodsGet(mThis(self))->VcIsHoCep(mThis(self), vc))
        {
        AtDevice device = AtModuleDeviceGet((AtModule)self);
        ThaModuleStmMap mapModule = (ThaModuleStmMap)AtDeviceModuleGet(device, cThaModuleMap);
        return ThaModuleStmMapSdhChannelMapLineOffset(mapModule, (AtSdhChannel)vc, stsId, 0, 0);
        }

    return m_ThaModuleAbstractMapMethods->AuVcStsDefaultOffset(self, vc, stsId);
    }

static eAtRet LoSliceReset(AtModule self, uint8 sliceId, uint32 channelControlRegister)
    {
    static const uint32 cNumStsInSlice = 24;
    static const uint32 cNumVtgInSts = 7;
    static const uint32 cNumVtInVtg = 4;
    static const uint32 cNumTimeslots = 32;
    uint32 address, baseAddress = Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, sliceId) + channelControlRegister;
    uint32 stsId, vtgId, vtId, timeslotId;
    uint32 longReg[cThaLongRegMaxSize];
    Tha60210011ModuleDemap module = (Tha60210011ModuleDemap)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleDemap);

    AtOsalMemInit(longReg, 0, sizeof(longReg));
    for (stsId = 0; stsId < cNumStsInSlice; stsId++)
        {
        for (vtgId = 0; vtgId < cNumVtgInSts; vtgId++)
            {
            for (vtId = 0; vtId < cNumVtInVtg; vtId++)
                {
                for (timeslotId = 0; timeslotId < cNumTimeslots; timeslotId++)
                    {
                    uint32 offset = (672 * stsId) + (96 * vtgId) + (32 * vtId) + timeslotId;
                    address = baseAddress + offset;
                    if (mMethodsGet(module)->IsLongRegDemapCtrl(module))
                        {
                        mModuleHwLongWrite(module, address, longReg, cThaLongRegMaxSize, AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), 0));
                        }
                    else
                        {
                        mModuleHwWrite(module, address, 0);
                        }
                    }
                }
            }
        }

    /* As the HW Flush originally flush the flowing registers to 0, just keep
     * flushing them */
    if (Tha602100xxHwAccessOptimized())
        {
        uint32 startOffset = ((672 * 23) + (96 * 6) + (32 * 3) + 31) + 1;
        uint32 stopOffset = 0x3FFF;
        uint32 offset_i;
        for (offset_i = startOffset; offset_i <= stopOffset; offset_i++)
            {
            address = baseAddress + offset_i;
            if (mMethodsGet(module)->IsLongRegDemapCtrl(module))
                {
                mModuleHwLongWrite(module, address, longReg, cThaLongRegMaxSize, AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), 0));
                }
            else
                {
                mModuleHwWrite(module, address, 0);
                }
            }
        }

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= Tha60210011ModuleMapDemapAllLoSlicesReset(self, cAf6Reg_lodemap_channel_ctrl_Base);
    ret |= Tha60210011MapDemapHoInit(self, cAf6Reg_demap_channel_ctrl_Base);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Tha60210011ModuleMapAsyncInit(self, Tha60210011ModuleDeMapSupperAsyncInit,
                                         cAf6Reg_lodemap_channel_ctrl_Base, cAf6Reg_demap_channel_ctrl_Base);
    }

static eBool HoLineVcxEncapConnectionIsEnabled(Tha60210011ModuleDemap self, AtSdhVc vcx)
    {
    ThaModuleAbstractMap demapModule = (ThaModuleAbstractMap)self;
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 demapChnCtrl = mMethodsGet((ThaModuleDemap)demapModule)->VcDmapChnCtrl((ThaModuleDemap)demapModule, vcx);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint32 enableMask = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnMask(self);

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = demapChnCtrl + mMethodsGet(demapModule)->AuVcStsDefaultOffset(demapModule, vcx, (uint8)(stsId + sts_i));
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);

        if ((regVal & enableMask) == 0)
            return cAtFalse;
        }

    return (numSts != 0) ? cAtTrue : cAtFalse;
    }

static eBool VcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    if(mMethodsGet(mThis(self))->VcIsHoCep(mThis(self), vcx))
    return mMethodsGet(mThis(self))->HoLineVcxEncapConnectionIsEnabled(mThis(self), vcx);

    return m_ThaModuleAbstractMapMethods->VcxEncapConnectionIsEnabled(self, vcx);
    }

static uint32 HoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    return Tha60210011ModuleMapDemapHoSliceOffset(self, slice);
    }

static uint32 HoVcDemapChannelCtrlDemapChEnMask(Tha60210011ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChEn_Mask;
    }

static uint32 HoVcDemapChannelCtrlDemapChEnShift(Tha60210011ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChEn_Shift;
    }

static eBool HasMapHo(AtModule self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet(self);
    return Tha60210011DeviceHasHoBus(device);
    }

static uint32 DmapFirstTsMask(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit15;
    return m_ThaModuleDemapMethods->DmapFirstTsMask(self);
    }

static uint8 DmapFirstTsShift(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 15;
    return m_ThaModuleDemapMethods->DmapFirstTsShift(self);
    }

static uint32 DmapChnTypeMask(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit14_12;
    return m_ThaModuleDemapMethods->DmapChnTypeMask(self);
    }

static uint8 DmapChnTypeShift(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 12;
    return m_ThaModuleDemapMethods->DmapChnTypeShift(self);
    }

static uint32 DmapTsEnMask(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit11;
    return m_ThaModuleDemapMethods->DmapTsEnMask(self);
    }

static uint8 DmapTsEnShift(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 11;
    return m_ThaModuleDemapMethods->DmapTsEnShift(self);
    }

static uint32 DmapPwIdMask(ThaModuleDemap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit10_0;
    return m_ThaModuleDemapMethods->DmapPwIdMask(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BindHoLineVcToPseudowire);
        mMethodOverride(m_methods, HoVcDemapChannelCtrlDemapChEnMask);
        mMethodOverride(m_methods, HoVcDemapChannelCtrlDemapChEnShift);
        mMethodOverride(m_methods, BindHoStsToPseudowire);
        mMethodOverride(m_methods, HoLineVcxEncapConnectionEnable);
        mMethodOverride(m_methods, HoLineVcxEncapConnectionIsEnabled);
        mMethodOverride(m_methods, VcIsHoCep);
        mMethodOverride(m_methods, IsLongRegDemapCtrl);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, m_ThaModuleDemapMethods, sizeof(m_ThaModuleDemapOverride));

        /* Override register address */
        mMethodOverride(m_ThaModuleDemapOverride, De1DmapChnCtrl);
        mMethodOverride(m_ThaModuleDemapOverride, VcDmapChnCtrl);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapPwIdMask);
        }

    mMethodsSet(demapModule, &m_ThaModuleDemapOverride);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhSliceOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NeedEnableAfterBindToPw);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcStsDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcBoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HoSliceOffset);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleDemap);
    }

AtModule Tha60210011ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleDemapObjectInit(newModule, device);
    }

eAtRet Tha60210011ModuleMapDemapAllLoSlicesReset(AtModule self, uint32 channelControlRegister)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet((AtDevice)device, cThaModuleMap);
    uint8 numSlices = mMethodsGet(mapModule)->NumLoSlices(mapModule);
    uint8 slice_i;
    eAtRet ret = cAtOk;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        ret |= LoSliceReset(self, slice_i, channelControlRegister);

    return ret;
    }

eAtRet Tha60210011MapDemapHoInit(AtModule self, uint32 regAddress)
    {
    const uint32 cMaxNumStsPerSlice = 48;
    uint32 numSlices = Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)AtModuleDeviceGet(self));
    uint32 sliceId, stsId;
    uint32 regAddr = regAddress;

    if (!HasMapHo(self))
        return cAtOk;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        for (stsId = 0; stsId < cMaxNumStsPerSlice; stsId++)
            {
            uint32 offset = (256 * sliceId) + stsId + cHoBaseAddress;
            mModuleHwWrite(self, regAddr + offset, 0);
            }
        }

    return cAtOk;
    }

eAtRet Tha60210011ModuleMapDemapLoSliceReset(AtModule self, uint8 sliceId, uint32 channelControlRegister)
    {
    if (self)
        return LoSliceReset(self, sliceId, channelControlRegister);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210011ModuleDeMapSupperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

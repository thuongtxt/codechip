/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : MAP
 *
 * File        : Tha60210011ModuleMap.h
 *
 * Created Date: May 6, 2015
 *
 * Description : PWCodechip @60210011 TDM-from/to-PW mapping.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEMAP_H_
#define _THA60210011MODULEMAP_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../../../default/map/ThaModuleMap.h"
#include "../../../default/map/ThaModuleDemap.h"
#include "../../../default/util/ThaBitMask.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleMap * Tha60210011ModuleMap;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
uint32 Tha60210011ModuleMapDemapLoSliceOffset(ThaModuleAbstractMap self, uint8 slice);
uint32 Tha60210011ModuleMapDemapHoSliceOffset(ThaModuleAbstractMap self, uint8 slice);
uint32 Tha60210011ModuleMapDemapSliceOffset(ThaModuleAbstractMap self, AtSdhChannel channel, uint8 slice);
eThaMapChannelType Tha60210011ModuleMapChannelType(AtChannel circuit, AtPw pw);

uint32 Tha60210011MapDemapHoLineVcDefaultOffset(ThaModuleAbstractMap self, uint8 slice, uint8 hwSts);
void Tha60210011MapHoBertHwDefault(ThaModuleMap self, AtSdhChannel channel);
ThaBitMask Tha60210011ModuleMapPoolBySlice(ThaModuleMap self, uint8 slice);

uint32 Tha60210011ModuleMapPwAllocate(ThaModuleMap self, ThaPwAdapter pwAdapter, eBool isLo, uint8 *slice);
eAtRet Tha60210011ModuleMapPwDeallocate(ThaModuleMap self, eBool isLo, uint8 slice, uint32 tdmPwId);

uint32 Tha60210011ModuleMapHoLineVcEncapHwIdAllocate(ThaModuleMap self, AtSdhVc vcx);
uint32 Tha60210011ModuleMapLoLineVcEncapHwIdAllocate(ThaModuleMap self, AtSdhVc vc1x);
uint32 Tha60210011ModuleMapDe1EncapHwIdAllocate(ThaModuleMap self, AtPdhDe1 de1);
uint32 Tha60210011ModuleMapDe3EncapHwIdAllocate(ThaModuleMap self, AtPdhDe3 de3);
uint32 Tha60210011ModuleMapNxDs0EncapHwIdAllocate(ThaModuleMap self, AtPdhNxDS0 nxds0);
eAtRet Tha60210011ModuleMapEncapHwIdDeallocate(ThaModuleMap self, eBool isLo, uint8 slice, uint32 encapHwId);

uint32 Tha60210011ModuleMapHoBaseAddress(ThaModuleMap self);
uint32 Tha60210011ModuleMapLoBaseAddress(ThaModuleMap self);
uint32 Tha60210011ModuleMapStartLoRamId(ThaModuleMap self);
uint32 Tha60210011ModuleMapLomapChannelCtrlBase(Tha60210011ModuleMap self);

eAtRet Tha60210011ModuleMapDemapLoSliceReset(AtModule self, uint8 sliceId, uint32 channelControlRegister);

uint32 Tha60210011ModuleMapHoBaseAddress(ThaModuleMap self);
uint32 Tha60210011ModuleMapLoBaseAddress(ThaModuleMap self);
uint32 Tha60210011ModuleDemapHoBaseAddress(ThaModuleDemap self);
uint32 Tha60210011ModuleDemapLoBaseAddress(ThaModuleDemap self);

eBool Tha60210011ModuleMapDemapBitFieldsNeedShiftUp(ThaModuleAbstractMap self);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210011MODULEMAP_H_ */

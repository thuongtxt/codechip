/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60210011ModuleMapInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : MAP/DEMAP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEMAPINTERNAL_H_
#define _THA60210011MODULEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleStmMapInternal.h"
#include "../../Tha60150011/map/Tha60150011ModuleMapDemap.h"
#include "Tha60210011ModuleMap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cInvalidAddress cBit31_0
#define cLoBaseAddress 0x800000UL
#define cHoBaseAddress 0x60000UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleDemap * Tha60210011ModuleDemap;

typedef struct tTha60210011ModuleDemapMethods
    {
    eAtRet (*BindHoLineVcToPseudowire)(Tha60210011ModuleDemap self, AtSdhVc vc, AtPw pw);
    eAtRet (*BindHoStsToPseudowire)(Tha60210011ModuleDemap self, AtSdhVc vc, uint8 hwSlice, uint8 hwSts, AtPw pw);
    eAtRet (*HoLineVcxEncapConnectionEnable)(Tha60210011ModuleDemap self, AtSdhVc vcx, eBool enable);
    eBool (*HoLineVcxEncapConnectionIsEnabled)(Tha60210011ModuleDemap self, AtSdhVc vcx);
    uint32 (*HoVcDemapChannelCtrlDemapChEnMask)(Tha60210011ModuleDemap self);
    uint32 (*HoVcDemapChannelCtrlDemapChEnShift)(Tha60210011ModuleDemap self);
    eBool (*VcIsHoCep)(Tha60210011ModuleDemap self, AtSdhVc vc);
    eBool (*IsLongRegDemapCtrl)(Tha60210011ModuleDemap self);
    }tTha60210011ModuleDemapMethods;

typedef struct tTha60210011ModuleDemap
    {
    tTha60150011ModuleDemap super;
    const tTha60210011ModuleDemapMethods* methods;

    }tTha60210011ModuleDemap;

typedef struct tTha60210011ModuleMapMethods
    {
    eAtRet (*BindHoLineVcToPseudowire)(Tha60210011ModuleMap self, AtSdhVc vc, AtPw pw);
    uint32 (*LoSliceLocalPwIdRestore)(Tha60210011ModuleMap self, ThaPwAdapter pwAdapter);
    uint8 (*MaxNumStsInLoSlice)(Tha60210011ModuleMap self);
    uint32 (*NumberOfMapLoPwPerSlice)(Tha60210011ModuleMap self);
    uint32 (*HoLineVcPwAllocate)(Tha60210011ModuleMap self, ThaPwAdapter adapter, uint8 *slice);
    uint32 (*AllPwLocalIdRestore)(Tha60210011ModuleMap self);
    uint32 (*LomapChannelCtrlBase)(Tha60210011ModuleMap self);
    uint32 (*StartMapLocalRamId)(Tha60210011ModuleMap self);
    }tTha60210011ModuleMapMethods;

typedef struct tTha60210011ModuleMap
    {
    tTha60150011ModuleMap super;
    const tTha60210011ModuleMapMethods* methods;

    /* Private data */
    AtOsalMutex loMutex;
    ThaBitMask *loSlicePools;
    uint8 numLoPool;
    }tTha60210011ModuleMap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModuleDemapObjectInit(AtModule self, AtDevice device);
AtModule Tha60210011ModuleMapObjectInit(AtModule self, AtDevice device);

eAtRet Tha60210011ModuleMapDemapAllLoSlicesReset(AtModule self, uint32 channelControlRegister);
eAtRet Tha60210011MapDemapHoInit(AtModule self, uint32 regAddress);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEMAPINTERNAL_H_ */


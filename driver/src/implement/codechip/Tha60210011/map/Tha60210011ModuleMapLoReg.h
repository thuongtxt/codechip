/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60210011ModuleMapLoReg.h
 * 
 * Created Date: May 11, 2015
 *
 * Description : LO-MAP registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEMAPLOREG_H_
#define _THA60210011MODULEMAPLOREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Demap Channel Control
Reg Addr   : 0x000000-0x003FFF
Reg Formula: 0x000000 + 672*stsid + 96*vtgid + 24*vtid + slotid
    Where  :
           + $stsid(0-23): is STS identification number)
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
           + $slotid(0-31): is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1
Reg Desc   :
The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.

------------------------------------------------------------------------------*/
#define cAf6Reg_lodemap_channel_ctrl_Base                                                               0x000000
#define cAf6Reg_lodemap_channel_ctrl(stsid, vtgid, vtid, slotid)        (0x000000+672*(stsid)+96*(vtgid)+24*(vtid)+(slotid))
#define cAf6Reg_lodemap_channel_ctrl_WidthVal                                                                 32
#define cAf6Reg_lodemap_channel_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DemapFirstTs
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only
BitField Bits: [14]
--------------------------------------*/
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_Bit_Start                                                      14
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_Bit_End                                                        14
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_Mask                                                       cBit14
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_Shift                                                          14
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_MaxVal                                                        0x1
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_MinVal                                                        0x0
#define cAf6_lodemap_channel_ctrl_DemapFirstTs_RstVal                                                        0x0

/*--------------------------------------
BitField Name: DemapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP
encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over
DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5:
MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: PLCP
encapsulation from DS3/E3(unused)
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_lodemap_channel_ctrl_DemapChannelType_Bit_Start                                                  11
#define cAf6_lodemap_channel_ctrl_DemapChannelType_Bit_End                                                    13
#define cAf6_lodemap_channel_ctrl_DemapChannelType_Mask                                                cBit13_11
#define cAf6_lodemap_channel_ctrl_DemapChannelType_Shift                                                      11
#define cAf6_lodemap_channel_ctrl_DemapChannelType_MaxVal                                                    0x7
#define cAf6_lodemap_channel_ctrl_DemapChannelType_MinVal                                                    0x0
#define cAf6_lodemap_channel_ctrl_DemapChannelType_RstVal                                                    0x0

/*--------------------------------------
BitField Name: DemapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_lodemap_channel_ctrl_DemapChEn_Bit_Start                                                         10
#define cAf6_lodemap_channel_ctrl_DemapChEn_Bit_End                                                           10
#define cAf6_lodemap_channel_ctrl_DemapChEn_Mask                                                          cBit10
#define cAf6_lodemap_channel_ctrl_DemapChEn_Shift                                                             10
#define cAf6_lodemap_channel_ctrl_DemapChEn_MaxVal                                                           0x1
#define cAf6_lodemap_channel_ctrl_DemapChEn_MinVal                                                           0x0
#define cAf6_lodemap_channel_ctrl_DemapChEn_RstVal                                                           0x0

/*--------------------------------------
BitField Name: DemapPWIdField
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_Bit_Start                                                     0
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_Bit_End                                                       9
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_Mask                                                    cBit9_0
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_Shift                                                         0
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_MaxVal                                                    0x3ff
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_MinVal                                                      0x0
#define cAf6_lodemap_channel_ctrl_DemapPWIdField_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Map Channel Control
Reg Addr   : 0x014000-0x017FFF
Reg Formula: 0x014000 + 672*stsid + 96*vtgid + 24*vtid + slotid
    Where  :
           + $stsid(0-23): is STS identification number)
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
           + $slotid(0-31): is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1
Reg Desc   :
The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.

------------------------------------------------------------------------------*/
#define cAf6Reg_lomap_channel_ctrl_Base                                                                 0x014000
#define cAf6Reg_lomap_channel_ctrl(stsid, vtgid, vtid, slotid)          (0x014000+672*(stsid)+96*(vtgid)+24*(vtid)+(slotid))
#define cAf6Reg_lomap_channel_ctrl_WidthVal                                                                   32
#define cAf6Reg_lomap_channel_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: MapFirstTs
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only
BitField Bits: [14]
--------------------------------------*/
#define cAf6_lomap_channel_ctrl_MapFirstTs_Bit_Start                                                          14
#define cAf6_lomap_channel_ctrl_MapFirstTs_Bit_End                                                            14
#define cAf6_lomap_channel_ctrl_MapFirstTs_Mask                                                           cBit14
#define cAf6_lomap_channel_ctrl_MapFirstTs_Shift                                                              14
#define cAf6_lomap_channel_ctrl_MapFirstTs_MaxVal                                                            0x1
#define cAf6_lomap_channel_ctrl_MapFirstTs_MinVal                                                            0x0
#define cAf6_lomap_channel_ctrl_MapFirstTs_RstVal                                                            0x0

/*--------------------------------------
BitField Name: MapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP
encapsulation from DS1/E1 2: CESoP with CAS 3: ATM(unused) 4: HDLC/PPP/LAPS over
DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP
encapsulation from SONET/SDH 7: PLCP encapsulation from DS3/E3(unused)
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_lomap_channel_ctrl_MapChannelType_Bit_Start                                                      11
#define cAf6_lomap_channel_ctrl_MapChannelType_Bit_End                                                        13
#define cAf6_lomap_channel_ctrl_MapChannelType_Mask                                                    cBit13_11
#define cAf6_lomap_channel_ctrl_MapChannelType_Shift                                                          11
#define cAf6_lomap_channel_ctrl_MapChannelType_MaxVal                                                        0x7
#define cAf6_lomap_channel_ctrl_MapChannelType_MinVal                                                        0x0
#define cAf6_lomap_channel_ctrl_MapChannelType_RstVal                                                        0x0

/*--------------------------------------
BitField Name: MapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_lomap_channel_ctrl_MapChEn_Bit_Start                                                             10
#define cAf6_lomap_channel_ctrl_MapChEn_Bit_End                                                               10
#define cAf6_lomap_channel_ctrl_MapChEn_Mask                                                              cBit10
#define cAf6_lomap_channel_ctrl_MapChEn_Shift                                                                 10
#define cAf6_lomap_channel_ctrl_MapChEn_MaxVal                                                               0x1
#define cAf6_lomap_channel_ctrl_MapChEn_MinVal                                                               0x0
#define cAf6_lomap_channel_ctrl_MapChEn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: MapPWIdField
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation.
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_lomap_channel_ctrl_MapPWIdField_Bit_Start                                                         0
#define cAf6_lomap_channel_ctrl_MapPWIdField_Bit_End                                                           9
#define cAf6_lomap_channel_ctrl_MapPWIdField_Mask                                                        cBit9_0
#define cAf6_lomap_channel_ctrl_MapPWIdField_Shift                                                             0
#define cAf6_lomap_channel_ctrl_MapPWIdField_MaxVal                                                        0x3ff
#define cAf6_lomap_channel_ctrl_MapPWIdField_MinVal                                                          0x0
#define cAf6_lomap_channel_ctrl_MapPWIdField_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Map Line Control
Reg Addr   : 0x010000-0x0103FF
Reg Formula: 0x010000 +0x010000 + 32*stsid + 4*vtgid + vtid
    Where  :
           + $stsid(0-23): is STS identification number
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
Reg Desc   :
The registers provide the per line configurations for STS/VT/DS1/E1 line.

------------------------------------------------------------------------------*/
#define cAf6Reg_lomap_line_ctrl_Base                                                                    0x010000
#define cAf6Reg_lomap_line_ctrl(stsid, vtgid, vtid)                     (0x010000+0x010000+32*(stsid)+4*(vtgid)+(vtid))
#define cAf6Reg_lomap_line_ctrl_WidthVal                                                                      32
#define cAf6Reg_lomap_line_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: MapDs1LcEn
BitField Type: RW
BitField Desc: DS1E1 Loop code insert enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_lomap_line_ctrl_MapDs1LcEn_Bit_Start                                                             17
#define cAf6_lomap_line_ctrl_MapDs1LcEn_Bit_End                                                               17
#define cAf6_lomap_line_ctrl_MapDs1LcEn_Mask                                                              cBit17
#define cAf6_lomap_line_ctrl_MapDs1LcEn_Shift                                                                 17
#define cAf6_lomap_line_ctrl_MapDs1LcEn_MaxVal                                                               0x1
#define cAf6_lomap_line_ctrl_MapDs1LcEn_MinVal                                                               0x0
#define cAf6_lomap_line_ctrl_MapDs1LcEn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: MapFrameType
BitField Type: RW
BitField Desc: depend on MapSigType[3:0], the MapFrameType[1:0] bit field can
have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3
framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3
G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP
basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode
BitField Bits: [16:15]
--------------------------------------*/
#define cAf6_lomap_line_ctrl_MapFrameType_Bit_Start                                                           15
#define cAf6_lomap_line_ctrl_MapFrameType_Bit_End                                                             16
#define cAf6_lomap_line_ctrl_MapFrameType_Mask                                                         cBit16_15
#define cAf6_lomap_line_ctrl_MapFrameType_Shift                                                               15
#define cAf6_lomap_line_ctrl_MapFrameType_MaxVal                                                             0x3
#define cAf6_lomap_line_ctrl_MapFrameType_MinVal                                                             0x0
#define cAf6_lomap_line_ctrl_MapFrameType_RstVal                                                             0x0

/*--------------------------------------
BitField Name: MapSigType
BitField Type: RW
BitField Desc: 0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6:
unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c
BitField Bits: [14:11]
--------------------------------------*/
#define cAf6_lomap_line_ctrl_MapSigType_Bit_Start                                                             11
#define cAf6_lomap_line_ctrl_MapSigType_Bit_End                                                               14
#define cAf6_lomap_line_ctrl_MapSigType_Mask                                                           cBit14_11
#define cAf6_lomap_line_ctrl_MapSigType_Shift                                                                 11
#define cAf6_lomap_line_ctrl_MapSigType_MaxVal                                                               0xf
#define cAf6_lomap_line_ctrl_MapSigType_MinVal                                                               0x0
#define cAf6_lomap_line_ctrl_MapSigType_RstVal                                                               0x0

/*--------------------------------------
BitField Name: MapTimeSrcMaster
BitField Type: RW
BitField Desc: This bit is used to indicate the master timing or the master VC3
in VC4/VC4-Xc
BitField Bits: [10]
--------------------------------------*/
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_Bit_Start                                                       10
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_Bit_End                                                         10
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_Mask                                                        cBit10
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_Shift                                                           10
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_MaxVal                                                         0x1
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_MinVal                                                         0x0
#define cAf6_lomap_line_ctrl_MapTimeSrcMaster_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MapTimeSrcId
BitField Type: RW
BitField Desc: The reference line ID used for timing reference or the master VC3
ID in VC4/VC4-Xc
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_lomap_line_ctrl_MapTimeSrcId_Bit_Start                                                            0
#define cAf6_lomap_line_ctrl_MapTimeSrcId_Bit_End                                                              9
#define cAf6_lomap_line_ctrl_MapTimeSrcId_Mask                                                           cBit9_0
#define cAf6_lomap_line_ctrl_MapTimeSrcId_Shift                                                                0
#define cAf6_lomap_line_ctrl_MapTimeSrcId_MaxVal                                                           0x3ff
#define cAf6_lomap_line_ctrl_MapTimeSrcId_MinVal                                                             0x0
#define cAf6_lomap_line_ctrl_MapTimeSrcId_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Map IDLE Code
Reg Addr   : 0x018200
Reg Formula:
    Where  :
Reg Desc   :
The registers provide the idle pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_lomap_idle_code_Base                                                                    0x018200
#define cAf6Reg_lomap_idle_code                                                                         0x018200
#define cAf6Reg_lomap_idle_code_WidthVal                                                                      32
#define cAf6Reg_lomap_idle_code_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: IdleCode
BitField Type: RW
BitField Desc: Idle code
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_lomap_idle_code_IdleCode_Bit_Start                                                                0
#define cAf6_lomap_idle_code_IdleCode_Bit_End                                                                  7
#define cAf6_lomap_idle_code_IdleCode_Mask                                                               cBit7_0
#define cAf6_lomap_idle_code_IdleCode_Shift                                                                    0
#define cAf6_lomap_idle_code_IdleCode_MaxVal                                                                0xff
#define cAf6_lomap_idle_code_IdleCode_MinVal                                                                 0x0
#define cAf6_lomap_idle_code_IdleCode_RstVal                                                                 0x0


#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEMAPLOREG_H_ */


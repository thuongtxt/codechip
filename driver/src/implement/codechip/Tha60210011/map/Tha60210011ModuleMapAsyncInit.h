/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60210011ModuleMapSyncInit.h
 * 
 * Created Date: Aug 19, 2016
 *
 * Description : async init interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEMAPSYNCINIT_H_
#define _THA60210011MODULEMAPSYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef eAtRet (*tSupperAsyncInitFunc)(AtModule self);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011ModuleMapSupperAsyncInit(AtModule self);
eAtRet Tha60210011ModuleDeMapSupperAsyncInit(AtModule self);

eAtRet Tha60210011ModuleMapAsyncInit(AtModule self, tSupperAsyncInitFunc supperAsyncInitFunc, uint32 lo_channel_ctr_base, uint32 line_ctr_base);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEMAPSYNCINIT_H_ */


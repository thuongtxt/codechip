/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60210011ModuleMapHoReg.h
 * 
 * Created Date: May 11, 2015
 *
 * Description : HO-MAP registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEMAPHOREG_H_
#define _THA60210011MODULEMAPHOREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Demap Channel Control
Reg Addr   : 0x0000-0x007FF
Reg Formula: 0x0000 + 256*slice + stsid
    Where  :
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   :
The registers are used by the hardware to configure PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_demap_channel_ctrl_Base                                                                 0x0000
#define cAf6Reg_demap_channel_ctrl(slice, stsid)                                  (0x0000+256*(slice)+(stsid))
#define cAf6Reg_demap_channel_ctrl_WidthVal                                                                 32
#define cAf6Reg_demap_channel_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Demapsr_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [2]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Bit_Start                                                     2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Bit_End                                                       2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Mask                                                      cBit2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Shift                                                         2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_MaxVal                                                      0x1
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_MinVal                                                      0x0
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Demapsrctype
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [1]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_Demapsrctype_Bit_Start                                                       1
#define cAf6_demap_channel_ctrl_Demapsrctype_Bit_End                                                         1
#define cAf6_demap_channel_ctrl_Demapsrctype_Mask                                                        cBit1
#define cAf6_demap_channel_ctrl_Demapsrctype_Shift                                                           1
#define cAf6_demap_channel_ctrl_Demapsrctype_MaxVal                                                        0x1
#define cAf6_demap_channel_ctrl_Demapsrctype_MinVal                                                        0x0
#define cAf6_demap_channel_ctrl_Demapsrctype_RstVal                                                        0x0

/*--------------------------------------
BitField Name: DemapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChEn_Bit_Start                                                          0
#define cAf6_demap_channel_ctrl_DemapChEn_Bit_End                                                            0
#define cAf6_demap_channel_ctrl_DemapChEn_Mask                                                           cBit0
#define cAf6_demap_channel_ctrl_DemapChEn_Shift                                                              0
#define cAf6_demap_channel_ctrl_DemapChEn_MaxVal                                                           0x1
#define cAf6_demap_channel_ctrl_DemapChEn_MinVal                                                           0x0
#define cAf6_demap_channel_ctrl_DemapChEn_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Map Line Control
Reg Addr   : 0x4000-0x47FF
Reg Formula: 0x4000 + 256*slice + stsid
    Where  :
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   :
The registers provide the per line configurations for STS

------------------------------------------------------------------------------*/
#define cAf6Reg_map_line_ctrl_Base                                                                      0x4000
#define cAf6Reg_map_line_ctrl(slice, stsid)                                       (0x4000+256*(slice)+(stsid))
#define cAf6Reg_map_line_ctrl_WidthVal                                                                      32
#define cAf6Reg_map_line_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Mapsrc_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [9]
--------------------------------------*/
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Bit_Start                                                           9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Bit_End                                                             9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Mask                                                            cBit9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Shift                                                               9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_MaxVal                                                            0x1
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_MinVal                                                            0x0
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Mapsrc_type
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_line_ctrl_Mapsrc_type_Bit_Start                                                             8
#define cAf6_map_line_ctrl_Mapsrc_type_Bit_End                                                               8
#define cAf6_map_line_ctrl_Mapsrc_type_Mask                                                              cBit8
#define cAf6_map_line_ctrl_Mapsrc_type_Shift                                                                 8
#define cAf6_map_line_ctrl_Mapsrc_type_MaxVal                                                              0x1
#define cAf6_map_line_ctrl_Mapsrc_type_MinVal                                                              0x0
#define cAf6_map_line_ctrl_Mapsrc_type_RstVal                                                              0x0

/*--------------------------------------
BitField Name: MapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapChEn_Bit_Start                                                                 7
#define cAf6_map_line_ctrl_MapChEn_Bit_End                                                                   7
#define cAf6_map_line_ctrl_MapChEn_Mask                                                                  cBit7
#define cAf6_map_line_ctrl_MapChEn_Shift                                                                     7
#define cAf6_map_line_ctrl_MapChEn_MaxVal                                                                  0x1
#define cAf6_map_line_ctrl_MapChEn_MinVal                                                                  0x0
#define cAf6_map_line_ctrl_MapChEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: MapTimeSrcMaster
BitField Type: RW
BitField Desc: This bit is used to indicate the master timing or the master VC3
in VC4/VC4-Xc
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Bit_Start                                                        6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Bit_End                                                          6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Mask                                                         cBit6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Shift                                                            6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_MaxVal                                                         0x1
#define cAf6_map_line_ctrl_MapTimeSrcMaster_MinVal                                                         0x0
#define cAf6_map_line_ctrl_MapTimeSrcMaster_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MapTimeSrcId
BitField Type: RW
BitField Desc: The reference line ID used for timing reference or the master VC3
ID in VC4/VC4-Xc
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcId_Bit_Start                                                            0
#define cAf6_map_line_ctrl_MapTimeSrcId_Bit_End                                                              5
#define cAf6_map_line_ctrl_MapTimeSrcId_Mask                                                           cBit5_0
#define cAf6_map_line_ctrl_MapTimeSrcId_Shift                                                                0
#define cAf6_map_line_ctrl_MapTimeSrcId_MaxVal                                                            0x3f
#define cAf6_map_line_ctrl_MapTimeSrcId_MinVal                                                             0x0
#define cAf6_map_line_ctrl_MapTimeSrcId_RstVal                                                             0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEMAPHOREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210011ModuleMapAsyncInit.c
 *
 * Created Date: Aug 18, 2016
 *
 * Description : Async intialization
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/util/ThaBitMask.h"
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../man/Tha60210011Device.h"
#include "../pw/Tha60210011ModulePw.h"
#include "../pw/activator/Tha60210011HwPw.h"
#include "../ram/Tha60210011InternalRam.h"
#include "../pda/Tha60210011ModulePda.h"
#include "Tha60210011ModuleMapHoReg.h"
#include "Tha60210011ModuleMapLoReg.h"
#include "Tha60210011ModuleMapInternal.h"
#include "Tha60210011ModuleMapAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaNumberOfMapHoPwPerSlice 48UL
#define cThaNumberOfMapLoPwPerSlice 1024UL

/*--------------------------------------
 BitField Name: cThaMapTmDs1LoopcodeMask
 BitField Type: R/W
 BitField Desc: bit [19:17] MapDs1Loopcode[2:0]:
 BitField Bits: 19:17
 --------------------------------------*/
#define cThaMapTmDs1LoopcodeMask                     cBit19_17
#define cThaMapTmDs1LoopcodeShift                    17

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011ModuleMap*)self)

/*--------------------------- Local typedefs ---------------------------------*/

typedef enum eTha60210011ModuleMapAsyncState
    {
    cTha60210011ModuleMapAsyncStateSupperInit = 0,
    cTha60210011ModuleMapAsyncStateLoSliceInit,
    cTha60210011ModuleMapAsyncStateHoSliceInit
    }eTha60210011ModuleMapAsyncState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncInitStateSet(AtModule self, uint32 state)
    {
    ThaModuleAbstractAsyncInitStateSet(self, state);
    }

static uint32 AsyncInitStateGet(AtModule self)
    {
    return ThaModuleAbstractAsyncInitStateGet(self);
    }

static void AsyncInitLoSliceStateSet(AtModule self, uint32 state)
    {
    ThaModuleAbstractAsyncInitLoSliceStateSet(self, state);
    }

static uint32 AsyncInitLoSliceStateGet(AtModule self)
    {
    return ThaModuleAbstractAsyncInitLoSliceStateGet(self);
    }

static eAtRet Tha60210011ModuleMapDemapAllLoSlicesAsyncReset(AtModule self, uint32 channelControlRegister)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet((AtDevice)device, cThaModuleMap);
    uint32 numSlices = (uint32)mMethodsGet(mapModule)->NumLoSlices(mapModule);
    uint32 slice_i = AsyncInitLoSliceStateGet(self);
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;
    char stateString[32];

    AtOsalCurTimeGet(&profileTime);
    AtSprintf(stateString, "state: %d", slice_i);

    ret = Tha60210011ModuleMapDemapLoSliceReset(self, (uint8)slice_i, channelControlRegister);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);
    if (ret == cAtOk)
        {
        slice_i += 1;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        slice_i = 0;

    if (slice_i == numSlices)
        {
        slice_i = 0;
        ret = cAtOk;
        }

    AsyncInitLoSliceStateSet(self, slice_i);
    return ret;
    }

eAtRet Tha60210011ModuleMapAsyncInit(AtModule self, tSupperAsyncInitFunc supperAsyncInitFunc, uint32 lo_channel_ctr_base, uint32 line_ctr_base)
    {
    eAtRet ret;
    uint32 state = AsyncInitStateGet(self);
    tAtOsalCurTime profileTime;
    char stateString[32];

    AtOsalCurTimeGet(&profileTime);
    AtSprintf(stateString, "state: %d", state);

    switch (state)
        {
        case cTha60210011ModuleMapAsyncStateSupperInit:
            ret = supperAsyncInitFunc(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);
            if (ret == cAtOk)
                {
                state = cTha60210011ModuleMapAsyncStateLoSliceInit;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cTha60210011ModuleMapAsyncStateSupperInit;

            break;
        case cTha60210011ModuleMapAsyncStateLoSliceInit:
            ret = Tha60210011ModuleMapDemapAllLoSlicesAsyncReset(self, lo_channel_ctr_base);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);
            if (ret == cAtOk)
                {
                state = cTha60210011ModuleMapAsyncStateHoSliceInit;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cTha60210011ModuleMapAsyncStateSupperInit;

            break;
        case cTha60210011ModuleMapAsyncStateHoSliceInit:
            ret = Tha60210011MapDemapHoInit(self, line_ctr_base);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);
            state = cTha60210011ModuleMapAsyncStateSupperInit;
            break;
        default:
            state = cTha60210011ModuleMapAsyncStateSupperInit;
            ret   = cAtErrorDevFail;
        }

    AsyncInitStateSet(self, state);
    return ret;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210011ModuleMap.c
 *
 * Created Date: May 06, 2015
 *
 * Description : PWCodechip @60210011 TDM-from-PW mapping.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../common/Tha602100xxCommon.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../man/Tha60210011Device.h"
#include "../pw/Tha60210011ModulePw.h"
#include "../pw/activator/Tha60210011HwPw.h"
#include "../ram/Tha60210011InternalRam.h"
#include "../pda/Tha60210011ModulePda.h"
#include "Tha60210011ModuleMapHoReg.h"
#include "Tha60210011ModuleMapLoReg.h"
#include "Tha60210011ModuleMapInternal.h"
#include "Tha60210011ModuleMapAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
/*--------------------------------------
 BitField Name: cThaMapTmDs1LoopcodeMask
 BitField Type: R/W
 BitField Desc: bit [19:17] MapDs1Loopcode[2:0]:
 BitField Bits: 19:17
 --------------------------------------*/
#define cThaMapTmDs1LoopcodeMask                     cBit19_17
#define cThaMapTmDs1LoopcodeShift                    17

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleMap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModuleMapMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleMapMethods         m_ThaModuleMapOverride;
static tThaModuleStmMapMethods      m_ThaModuleStmMapOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Save super implementation */
static const tAtObjectMethods             *m_AtObjectMethods = NULL;
static const tAtModuleMethods             *m_AtModuleMethods = NULL;
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleMapMethods         *m_ThaModuleMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapTmDs1Loopcode)

static AtModulePw PwModule(ThaModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static AtChannel HoVcToAllocatePwHwId(Tha60210011ModuleMap self, AtPw adapter)
    {
    AtChannel circuit = AtPwBoundCircuitGet(adapter);
    return ThaModulePwReferenceCircuitToConfigureHardware((ThaModulePw)PwModule((ThaModuleMap)self), (ThaPwAdapter)adapter, circuit);
    }

static uint32 HoLineVcPwAllocate(Tha60210011ModuleMap self, ThaPwAdapter adapter, uint8 *slice)
    {
    /* Use hwSts as TDM PW */
    uint8 hwSts;
    AtSdhChannel channel = (AtSdhChannel)HoVcToAllocatePwHwId(mThis(self), (AtPw)adapter);
    AtUnused(self);
    ThaSdhChannel2HwMasterStsId(channel, cThaModuleMap, slice, &hwSts);
    return hwSts;
    }

static uint32 LoLineVcFlatId(AtChannel channel)
    {
    uint8 hwSlice, hwSts;
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;
    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleMap, &hwSlice, &hwSts) == cAtOk)
        return (hwSts * 28UL) + (AtSdhChannelTug2Get(sdhChannel) * 4UL) + AtSdhChannelTu1xGet(sdhChannel);

    return cThaMapInvalidPwChannelId;
    }

static uint8 NumOC24Slice(ThaModuleMap self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    return (uint8)(Tha60210011ModuleSdhMaxNumLoLines(moduleSdh) * 2);
    }

static ThaBitMask* AllPools(ThaModuleMap self)
    {
    uint32 memSize;
    uint8 numLoPool;

    if (mThis(self)->loSlicePools)
        return mThis(self)->loSlicePools;

    numLoPool = NumOC24Slice(self);
    memSize = numLoPool * sizeof(ThaBitMask);
    mThis(self)->loSlicePools = AtOsalMemAlloc(memSize);
    if (mThis(self)->loSlicePools == NULL)
        return NULL;

    AtOsalMemInit(mThis(self)->loSlicePools, 0, memSize);
    /* Cache number of lo pools to use later because module SDH can be deleted anytime */
    mThis(self)->numLoPool = numLoPool;
    return mThis(self)->loSlicePools;
    }

static eBool SliceIsValid(ThaModuleMap self, uint8 slice)
    {
    return (slice < mThis(self)->numLoPool) ? cAtTrue : cAtFalse;
    }

static ThaBitMask PoolBySlice(ThaModuleMap self, uint8 slice)
    {
    ThaBitMask* allPools = AllPools(self);

    if ((allPools == NULL) || (!SliceIsValid(self, slice)))
        return NULL;

    if (allPools[slice] == NULL)
        allPools[slice] = ThaBitMaskNew(mMethodsGet(mThis(self))->NumberOfMapLoPwPerSlice(mThis(self)));

    return allPools[slice];
    }

static eBool IsDe2(AtPdhChannel channel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(channel);
    if ((channelType == cAtPdhChannelTypeE2) || (channelType == cAtPdhChannelTypeDs2))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PdhChannelFlatId(AtChannel channel)
    {
    uint8 slice, hwId, hwDe2Id, hwDe1Id;
    AtPdhChannel pdhChannel = (AtPdhChannel)channel;
    AtChannel sdhChannel = (AtChannel)AtPdhChannelVcInternalGet(pdhChannel);

    if (sdhChannel)
        return LoLineVcFlatId(sdhChannel);

    /* Should be DE2 DE1 */
    if (!IsDe2(AtPdhChannelParentChannelGet(pdhChannel)))
        return cThaMapInvalidPwChannelId;

    if (ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwId, &hwDe2Id, &hwDe1Id, cThaModuleMap) == cAtOk)
        return ((hwId * 28UL) + (hwDe2Id * 4UL) + hwDe1Id);

    return cThaMapInvalidPwChannelId;
    }

static uint32 SelectedValueByCircuit(ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtPwType pwType = AtPwTypeGet((AtPw)adapter);

    if (pwType == cAtPwTypeCESoP)
        return cThaMapInvalidPwChannelId;

    if (pwType == cAtPwTypeSAToP)
       return PdhChannelFlatId(circuit);

    if (pwType == cAtPwTypeCEP)
       return LoLineVcFlatId(circuit);

    return cThaMapInvalidPwChannelId;
    }

static Tha60210011ModulePda ModulePda(ThaModuleMap self)
    {
    return (Tha60210011ModulePda)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePda);
    }

static uint32 LoLinePwAllocate(ThaModuleMap self, ThaPwAdapter adapter, uint8 *slice)
    {
    ThaBitMask pool;
    uint8 loSlice;
    uint32 hwIdInSlice;
    uint32 freePw = cThaMapInvalidPwChannelId;
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)adapter);
    Tha60210011ModulePda modulePda = ModulePda(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet((AtPw)adapter, &loSlice, &hwIdInSlice) != cAtOk)
        {
        mChannelLog(adapter, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");
        return cThaMapInvalidPwChannelId;
        }

    *slice = loSlice;
    pool = PoolBySlice(self, loSlice);
    if (pool == NULL)
        return cThaMapInvalidPwChannelId;

    if (AtModuleInAccessible((AtModule)self))
        return cThaMapInvalidPwChannelId;

    /* Active driver or standby driver get invalid HW id  */
    AtOsalMutexLock(mThis(self)->loMutex);
    if (freePw == cThaMapInvalidPwChannelId)
        {
        uint32 selectedValue = SelectedValueByCircuit(adapter, circuit);
        if ((selectedValue != cThaMapInvalidPwChannelId) && (ThaBitMaskBitVal(pool, selectedValue) == 0))
            freePw = selectedValue;
        else
            freePw = ThaBitMaskFirstZeroBit(pool);
        }

    /* Use a valid one */
    if (ThaBitMaskBitPositionIsValid(pool, freePw))
        {
        ThaBitMaskSetBit(pool, freePw);
        Tha60210011ModulePdaLoSliceHwPwIdSet(modulePda, adapter, loSlice, freePw);
        }

    AtOsalMutexUnLock(mThis(self)->loMutex);
    return freePw;
    }

static uint32 SdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    return Tha60210011ModuleMapDemapSliceOffset(self, sdhChannel, slice);
    }

static void AllPoolsDelete(ThaModuleMap self)
    {
    uint8 slice_i;
    if (mThis(self)->loSlicePools == NULL)
        return;

    for (slice_i = 0; slice_i < mThis(self)->numLoPool; slice_i++)
        {
        if (mThis(self)->loSlicePools[slice_i])
            {
            AtObjectDelete((AtObject)mThis(self)->loSlicePools[slice_i]);
            mThis(self)->loSlicePools[slice_i] = NULL;
            }
        }

    AtOsalMemFree(mThis(self)->loSlicePools);
    mThis(self)->loSlicePools = NULL;
    }

static void Delete(AtObject self)
    {
    AtOsalMutexDestroy(mThis(self)->loMutex);
    mThis(self)->loMutex = NULL;
    AllPoolsDelete((ThaModuleMap)self);
    m_AtObjectMethods->Delete(self);
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsMapRegister(AtModuleDeviceGet(self), address);
    }

static eThaMapChannelType PdhChannelMapChannelTypeGet(AtPdhChannel circuit)
    {
    eAtPdhChannelType pdhChannelType = AtPdhChannelTypeGet(circuit);
    if ((pdhChannelType == cAtPdhChannelTypeDs1) || (pdhChannelType == cAtPdhChannelTypeE1))
        return cThaMapChannelDe1;

    if ((pdhChannelType == cAtPdhChannelTypeDs3) || (pdhChannelType == cAtPdhChannelTypeE3))
        return cThaMapChannelDe3;

    return cThaMapChannelTypeUnknown;
    }

static eBool HasMapHo(AtModule self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet(self);
    return Tha60210011DeviceHasHoBus(device);
    }

static eBool ShouldOperateOnHoBus(AtModule self, AtSdhChannel circuit)
    {
    if (HasMapHo(self) && Tha60210011ModuleSdhChannelIsHoPath(circuit))
        return cAtTrue;
    return cAtFalse;
    }

static eThaMapChannelType SdhChannelMapChannelTypeGet(AtSdhChannel circuit)
    {
    if (Tha60210011ModuleSdhChannelIsHoPath(circuit))
        return cThaMapChannelHoLineVc;

    return cThaMapChannelLoLineVc;
    }

static uint32 VcMapChnCtrl(ThaModuleMap self, AtSdhVc vc)
    {
    AtUnused(self);
    if (Tha60210011ModuleSdhChannelIsHoPath((AtSdhChannel)vc))
        return cBit31_0; /* HW removed this register in HO part */

    return mMethodsGet(mThis(self))->LomapChannelCtrlBase(mThis(self));
    }

static uint32 De1MapChnCtrl(ThaModuleMap self, AtPdhDe1 de1)
    {
    return mMethodsGet(self)->VcMapChnCtrl(self, (AtSdhVc)AtPdhChannelVcInternalGet((AtPdhChannel)de1));
    }

static uint32 MapFrmTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);

    return cAf6_lomap_line_ctrl_MapFrameType_Mask;
    }

static uint8 MapFrmTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_lomap_line_ctrl_MapFrameType_Shift;
    }

static uint32 MapSigTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_lomap_line_ctrl_MapSigType_Mask;
    }

static uint8 MapSigTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_lomap_line_ctrl_MapSigType_Shift;
    }

static uint32 MapTimeSrcMastLoHoMask(ThaModuleMap self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
        return cAf6_map_line_ctrl_MapTimeSrcMaster_Mask;

    return cAf6_lomap_line_ctrl_MapTimeSrcMaster_Mask;
    }

static uint32 MapTimeSrcMastMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    return MapTimeSrcMastLoHoMask(self, Tha60210011ModuleSdhChannelIsHoPath(sdhChannel));
    }

static uint8 MapTimeSrcMastLoHoShift(ThaModuleMap self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
        return cAf6_map_line_ctrl_MapTimeSrcMaster_Shift;

    return cAf6_lomap_line_ctrl_MapTimeSrcMaster_Shift;
    }

static uint8 MapTimeSrcMastShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    return MapTimeSrcMastLoHoShift(self, Tha60210011ModuleSdhChannelIsHoPath(sdhChannel));
    }

static uint32 MapTimeSrcIdLoHoMask(ThaModuleMap self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
        return cAf6_map_line_ctrl_MapTimeSrcId_Mask;

    return cAf6_lomap_line_ctrl_MapTimeSrcId_Mask;
    }

static uint32 MapTimeSrcIdMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    return MapTimeSrcIdLoHoMask(self, Tha60210011ModuleSdhChannelIsHoPath(sdhChannel));
    }

static uint8 MapTimeSrcIdShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return 0;
    }

static uint32 SdhChannelMapLineOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 slice, hwSts;

    ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, stsId, &slice, &hwSts);

    if (ShouldOperateOnHoBus((AtModule)self, sdhChannel))
        return Tha60210011MapDemapHoLineVcDefaultOffset((ThaModuleAbstractMap)self, slice, hwSts);

    return (uint32)(hwSts << 5) + (uint32)(vtgId << 2) + vtId + ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, sdhChannel, slice);
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    if (ShouldOperateOnHoBus((AtModule)self, (AtSdhChannel)vc))
        return ThaModuleStmMapSdhChannelMapLineOffset((ThaModuleStmMap)self, (AtSdhChannel)vc, stsId, 0, 0);

    return m_ThaModuleAbstractMapMethods->AuVcStsDefaultOffset(self, vc, stsId);
    }

static uint8 MaxNumStsInHoSlice(ThaModuleMap self)
    {
    AtUnused(self);
    return 48;
    }

static uint8 NumHoSlices(ThaModuleAbstractMap self)
    {
    return Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)AtModuleDeviceGet((AtModule)self));
    }

static uint32 MapLineCtrl(ThaModuleStmMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);

    if (ShouldOperateOnHoBus((AtModule)self, sdhChannel))
        return cAf6Reg_map_line_ctrl_Base;

    return cAf6Reg_lomap_line_ctrl_Base;
    }

static eAtRet HoDefaultSet(ThaModuleMap self)
    {
    uint8 sts_i, slice_i;
    uint32 regAddr, regVal, offset;
    ThaModuleAbstractMap module = (ThaModuleAbstractMap)self;
    uint8 numSlices = mMethodsGet(module)->NumHoSlices(module);

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        for (sts_i = 0; sts_i < MaxNumStsInHoSlice(self); sts_i++)
            {
            offset  = sts_i + Tha60210011ModuleMapDemapHoSliceOffset((ThaModuleAbstractMap)self, slice_i);
            regAddr = cAf6Reg_map_line_ctrl_Base + offset;
            regVal = 0;

            mFieldIns(&regVal, MapTimeSrcMastLoHoMask(self, cAtTrue), MapTimeSrcMastLoHoShift(self, cAtTrue), 1);
            mFieldIns(&regVal, MapTimeSrcIdLoHoMask(self, cAtTrue), MapTimeSrcIdShift(self, NULL), sts_i);
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static uint8 MaxNumStsInLoSlice(Tha60210011ModuleMap self)
    {
    AtUnused(self);
    return 24;
    }

static uint8 NumLoSlices(ThaModuleAbstractMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    uint8 numLoLines = Tha60210011ModuleSdhMaxNumLoLines(sdhModule);
    return (uint8)mMin((Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)device) * 2), (numLoLines * 2));
    }

static eAtRet LoDefaultSet(ThaModuleMap self)
    {
    ThaModuleAbstractMap module = (ThaModuleAbstractMap)self;
    uint8 numSlices = mMethodsGet(module)->NumLoSlices(module);
    uint8 sts_i, vtg_i, vt_i, slice_i;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        for (sts_i = 0; sts_i < mMethodsGet(mThis(self))->MaxNumStsInLoSlice(mThis(self)); sts_i++)
            {
            uint32 stsOffset = (uint32)(sts_i << 5);

            for (vtg_i = 0; vtg_i < 7; vtg_i++)
                {
                for (vt_i = 0; vt_i < 4; vt_i++)
                    {
                    uint32 offset  = stsOffset + (uint32)(vtg_i << 2) + vt_i + Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, slice_i);
                    uint32 regAddr = cAf6Reg_lomap_line_ctrl_Base + offset;
                    uint32 regVal = 0;
                    mFieldIns(&regVal, MapTimeSrcMastLoHoMask(self, cAtFalse), MapTimeSrcMastLoHoShift(self, cAtFalse), 1);
                    mFieldIns(&regVal, MapTimeSrcIdLoHoMask(self, cAtFalse), MapTimeSrcIdShift(self, NULL), offset);
                    mModuleHwWrite(self, regAddr, regVal);
                    }
                }

            /* As the HW Flush originally flush the flowing registers to 0, just keep
             * flushing them */
            if (Tha602100xxHwAccessOptimized())
                {
                for (vt_i = 28; vt_i < 32; vt_i++)
                    {
                    uint32 offset  = stsOffset + vt_i + Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, slice_i);
                    uint32 regAddr = cAf6Reg_lomap_line_ctrl_Base + offset;
                    mModuleHwWrite(self, regAddr, 0);
                    }
                }
            }

        /* As the HW Flush originally flush the flowing registers to 0, just keep
         * flushing them */
        if (Tha602100xxHwAccessOptimized())
            {
            uint32 startOffset = ((32 * 23) + (4 * 6) + 3) + 1; /* 32*stsid + 4*vtgid + vtid */
            uint32 stopOffset  = 0x3FF;
            uint32 offset_i;

            for (offset_i = startOffset; offset_i <= stopOffset; offset_i++)
                {
                uint32 offset  = offset_i + Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, slice_i);
                uint32 regAddr = cAf6Reg_lomap_line_ctrl_Base + offset;
                mModuleHwWrite(self, regAddr, 0);
                }
            }
        }

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleStmMap self)
    {
    eAtRet ret = cAtOk;
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (HasMapHo((AtModule)self))
        ret |= HoDefaultSet(mapModule);
    ret |= LoDefaultSet(mapModule);

    return ret;
    }

static eAtRet BindHoLineVcToPseudowire(Tha60210011ModuleMap self, AtSdhVc vc, AtPw pw)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eBool enable = mMethodsGet(abstractMap)->NeedEnableAfterBindToPw(abstractMap, pw);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;
        uint32 regAddr, regVal, offset;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        offset  = ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regAddr = cAf6Reg_map_line_ctrl_Base + offset;
        regVal  = mChannelHwRead(vc, regAddr, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_map_line_ctrl_MapChEn_, pw ? mBoolToBin(enable) : 0);
        mChannelHwWrite(vc, regAddr, regVal, cThaModuleMap);
        }
    return cAtOk;
    }

static eBool IsVc4_64c(AtSdhVc vc)
    {
    return (AtSdhChannelTypeGet((AtSdhChannel)vc) == cAtSdhChannelTypeVc4_64c) ? cAtTrue : cAtFalse;
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    if (IsVc4_64c(vc))
        return cAtOk;

    if (ShouldOperateOnHoBus((AtModule)self, (AtSdhChannel)vc))
        return mMethodsGet(mThis(self))->BindHoLineVcToPseudowire(mThis(self), vc, pw);

    return m_ThaModuleAbstractMapMethods->BindAuVcToPseudowire(self, vc, pw);
    }

static uint32 AuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode)
    {
    if (cepMode)
        *cepMode = cAtPwCepModeBasic;

    if (ShouldOperateOnHoBus((AtModule)self, (AtSdhChannel)vc))
        {
        uint8 slice, hwSts;
        if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModuleMap, &slice, &hwSts) != cAtOk)
            return cInvalidUint32;

        return hwSts;
        }

    return m_ThaModuleAbstractMapMethods->AuVcBoundPwHwIdGet(self, vc, cepMode);
    }

static eAtRet HoLineVcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 mapLineCtrl = mMethodsGet((ThaModuleStmMap)self)->MapLineCtrl((ThaModuleStmMap)self, sdhChannel);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = mapLineCtrl + mMethodsGet(self)->AuVcStsDefaultOffset(self, vcx, (uint8)(stsId + sts_i));
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);
        mRegFieldSet(regVal, cAf6_map_line_ctrl_MapChEn_, mBoolToBin(enable));
        mChannelHwWrite(vcx, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet VcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    if (IsVc4_64c(vcx))
        return cAtOk;

    if (!ShouldOperateOnHoBus((AtModule)self, (AtSdhChannel)vcx))
        return m_ThaModuleAbstractMapMethods->VcxEncapConnectionEnable(self, vcx, enable);

    return HoLineVcxEncapConnectionEnable(self, vcx, enable);
    }

static eBool NeedEnableAfterBindToPw(ThaModuleAbstractMap self, AtPw pw)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)pw;/*This pw is input as pw adater */
    AtUnused(self);

    if (pw && !ThaPwAdapterIsLogicPw(adapter) && AtChannelIsEnabled((AtChannel)pw))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet AuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (IsVc4_64c(vc))
        return cAtOk;

    if (ShouldOperateOnHoBus((AtModule)self, (AtSdhChannel)vc))
        return cAtOk;

    return m_ThaModuleAbstractMapMethods->AuVcFrameModeSet(self, vc);
    }

static void Vc4_4cConfigurationClear(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 stsId)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    AtUnused(stsId);
    }

static eAtRet Debug(AtModule self)
    {
    uint8 slice_i;
    AtPrintc(cSevNormal, "- Module is: ");
    AtPrintc(self->locked ? cSevCritical : cSevInfo, "%s\r\n", self->locked ? "Locked" : "Unlocked");

    if (mThis(self)->loSlicePools == NULL)
        return cAtOk;

    for (slice_i = 0; slice_i < mThis(self)->numLoPool; slice_i++)
        {
        AtPrintc(cSevNormal, "\r\n- LO private PWs pools[%d]: \r\n", slice_i);
        ThaBitMaskDebug(mThis(self)->loSlicePools[slice_i]);
        }

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= Tha60210011ModuleMapDemapAllLoSlicesReset(self, mMethodsGet(mThis(self))->LomapChannelCtrlBase(mThis(self)));
    ret |= Tha60210011MapDemapHoInit(self, cAf6Reg_map_line_ctrl_Base);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    uint32 regBase = mMethodsGet(mThis(self))->LomapChannelCtrlBase(mThis(self));
    return Tha60210011ModuleMapAsyncInit(self, Tha60210011ModuleMapSupperAsyncInit,
                                         regBase, cAf6Reg_map_line_ctrl_Base);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "MAPHO Thalassa Map Line Control Slice 0",
         "MAPHO Thalassa Map Line Control Slice 1",
         "MAPHO Thalassa Map Line Control Slice 2",
         "MAPHO Thalassa Map Line Control Slice 3",
         "MAPHO Thalassa Map Line Control Slice 4",
         "MAPHO Thalassa Map Line Control Slice 5",
         "MAPHO Thalassa Map Line Control Slice 6",
         "MAPHO Thalassa Map Line Control Slice 7",
         "MAPHO Thalassa Demap Line Control Slice 0",
         "MAPHO Thalassa Demap Line Control Slice 1",
         "MAPHO Thalassa Demap Line Control Slice 2",
         "MAPHO Thalassa Demap Line Control Slice 3",
         "MAPHO Thalassa Demap Line Control Slice 4",
         "MAPHO Thalassa Demap Line Control Slice 5",
         "MAPHO Thalassa Demap Line Control Slice 6",
         "MAPHO Thalassa Demap Line Control Slice 7",
         "MAPLO Thalassa Map Channel Control slice 0",
         "MAPLO Thalassa Map Line Control slice 0",
         "MAPLO Thalassa Demap Channel Control slice 0",
         "MAPLO Thalassa Map Channel Control slice 1",
         "MAPLO Thalassa Map Line Control slice 1",
         "MAPLO Thalassa Demap Channel Control slice 1",
         "MAPLO Thalassa Map Channel Control slice 2",
         "MAPLO Thalassa Map Line Control slice 2",
         "MAPLO Thalassa Demap Channel Control slice 2",
         "MAPLO Thalassa Map Channel Control slice 3",
         "MAPLO Thalassa Map Line Control slice 3",
         "MAPLO Thalassa Demap Channel Control slice 3",
         "MAPLO Thalassa Map Channel Control slice 4",
         "MAPLO Thalassa Map Line Control slice 4",
         "MAPLO Thalassa Demap Channel Control slice 4",
         "MAPLO Thalassa Map Channel Control slice 5",
         "MAPLO Thalassa Map Line Control slice 5",
         "MAPLO Thalassa Demap Channel Control slice 5",
         "MAPLO Thalassa Map Channel Control slice 6",
         "MAPLO Thalassa Map Line Control slice 6",
         "MAPLO Thalassa Demap Channel Control slice 6",
         "MAPLO Thalassa Map Channel Control slice 7",
         "MAPLO Thalassa Map Line Control slice 7",
         "MAPLO Thalassa Demap Channel Control slice 7",
         "MAPLO Thalassa Map Channel Control slice 8",
         "MAPLO Thalassa Map Line Control slice 8",
         "MAPLO Thalassa Demap Channel Control slice 8",
         "MAPLO Thalassa Map Channel Control slice 9",
         "MAPLO Thalassa Map Line Control slice 9",
         "MAPLO Thalassa Demap Channel Control slice 9",
         "MAPLO Thalassa Map Channel Control slice 10",
         "MAPLO Thalassa Map Line Control slice 10",
         "MAPLO Thalassa Demap Channel Control slice 10",
         "MAPLO Thalassa Map Channel Control slice 11",
         "MAPLO Thalassa Map Line Control slice 11",
         "MAPLO Thalassa Demap Channel Control slice 11"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static uint32 StartMapLocalRamId(Tha60210011ModuleMap self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    return (uint32)(Tha60210011ModuleSdhMaxNumHoLines(moduleSdh) * 2);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if ((localRamId < mMethodsGet(mThis(self))->StartMapLocalRamId(mThis(self))) && HasMapHo(self))
        return Tha60210011InternalRamMapHoNew(self, ramId, localRamId);

    return Tha60210011InternalRamMapNew(self, ramId, localRamId);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (HasMapHo(self))
        return m_AtModuleMethods->InternalRamIsReserved(self, ram);

    if (AtInternalRamLocalIdGet(ram) < mMethodsGet(mThis(self))->StartMapLocalRamId(mThis(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HoLineVcPwDeallocate(ThaModuleMap self, uint8 slice, uint32 tdmPwId)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(tdmPwId);
    return cAtOk;
    }

static eAtRet LoLinePwDeallocate(ThaModuleMap self, uint8 slice, uint32 tdmPwId)
    {
    if (!SliceIsValid(self, slice) || (mThis(self)->loSlicePools == NULL) || (mThis(self)->loSlicePools[slice] == NULL))
        return cAtErrorRsrcNoAvail;

    ThaBitMaskClearBit(mThis(self)->loSlicePools[slice], tdmPwId);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011ModuleMap *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(loSlicePools, object->numLoPool);
    mEncodeNone(loMutex);
    }

static eAtRet MapLineControlVcDefaultSet(ThaModuleMap self, AtSdhChannel vc)
    {
    if (Tha60210011ModuleSdhVcBelongsToLoLine(vc) == cAtFalse)
        return cAtOk;

    return m_ThaModuleMapMethods->MapLineControlVcDefaultSet(self, vc);
    }

static eAtRet HoVcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
    /* Products that have MAP HO do not need to set signal type */
    if (HasMapHo((AtModule)self))
        return cAtOk;

    return m_ThaModuleAbstractMapMethods->HoVcSignalTypeSet(self, sdhVc);
    }

static AtModulePw ModulePw(AtModule self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static uint32 LoSliceLocalPwIdRestore(Tha60210011ModuleMap self, ThaPwAdapter pwAdapter)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(pwAdapter);
    uint8 loSlice = Tha60210011HwPwSliceGet(hwPw);
    uint32 localId = Tha60210011ModulePdaLoSliceHwPwIdGet(ModulePda((ThaModuleMap)self), pwAdapter, loSlice);
    ThaBitMask pool = PoolBySlice((ThaModuleMap)self, loSlice);

    Tha60210011HwPwTdmIdSet(hwPw, localId);
    ThaBitMaskSetBit(pool, localId);

        return 0;
    }

static uint32 PwLocalIdRestore(ThaModuleMap self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    if (hwPw == NULL)
        return 0;

    if (!Tha60210011HwPwIsLo(hwPw))
    return 0;

    return mMethodsGet(mThis(self))->LoSliceLocalPwIdRestore(mThis(self), adapter);
    }

static uint32 Restore(AtModule self)
    {
    uint32 remained = 0;

    remained = remained + m_AtModuleMethods->Restore(self);
    remained = remained + mMethodsGet(mThis(self))->AllPwLocalIdRestore(mThis(self));

    return remained;
    }

static eBool HoLineVcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 mapLineCtrl = mMethodsGet((ThaModuleStmMap)self)->MapLineCtrl((ThaModuleStmMap)self, sdhChannel);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = mapLineCtrl + mMethodsGet(self)->AuVcStsDefaultOffset(self, vcx, (uint8)(stsId + sts_i));
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);

        if ((regVal & cAf6_map_line_ctrl_MapChEn_Mask) == 0)
            return cAtFalse;
        }

    return (numSts != 0) ? cAtTrue : cAtFalse;
    }

static eBool VcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    if (!ShouldOperateOnHoBus((AtModule)self, (AtSdhChannel)vcx))
        return m_ThaModuleAbstractMapMethods->VcxEncapConnectionIsEnabled(self, vcx);

    return HoLineVcxEncapConnectionIsEnabled(self, vcx);
    }

static uint32 HoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    return Tha60210011ModuleMapDemapHoSliceOffset(self, slice);
    }

static uint32 NumberOfMapLoPwPerSlice(Tha60210011ModuleMap self)
    {
    return Tha60210011ModulePwMaxNumPwsPerSts24Slice((Tha60210011ModulePw)PwModule((ThaModuleMap)self));
    }

static eBool BitFieldsNeedShiftUp(ThaModuleAbstractMap self)
    {
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)PwModule((ThaModuleMap)self);
    uint32 maxPws = Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule);
    uint32 originalMaxPws = Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule);
    return (maxPws > originalMaxPws) ? cAtTrue : cAtFalse;
    }

static uint32 MapChnTypeMask(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit14_12;
    return m_ThaModuleMapMethods->MapChnTypeMask(self);
    }

static uint8  MapChnTypeShift(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 12;
    return m_ThaModuleMapMethods->MapChnTypeShift(self);
    }

static uint32 MapFirstTsMask(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit15;
    return m_ThaModuleMapMethods->MapFirstTsMask(self);
    }

static uint8  MapFirstTsShift(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 15;
    return m_ThaModuleMapMethods->MapFirstTsShift(self);
    }

static uint32 MapTsEnMask(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit11;
    return m_ThaModuleMapMethods->MapTsEnMask(self);
    }

static uint8  MapTsEnShift(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 11;
    return m_ThaModuleMapMethods->MapTsEnShift(self);
    }

static uint32 MapPWIdFieldMask(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit10_0;
    return m_ThaModuleMapMethods->MapPWIdFieldMask(self);
    }

static uint8 MapPWIdFieldShift(ThaModuleMap self)
    {
    if (Tha60210011ModuleMapDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 0;
    return m_ThaModuleMapMethods->MapPWIdFieldShift(self);
    }

static uint32 LomapChannelCtrlBase(Tha60210011ModuleMap self)
    {
    AtUnused(self);
    return cAf6Reg_lomap_channel_ctrl_Base;
    }

static uint32 AllPwLocalIdRestore(Tha60210011ModuleMap self)
    {
    uint32 remained = 0;
    AtIterator pwIterator = AtModulePwIteratorCreate(ModulePw((AtModule)self));
    AtPw pw;

    while ((pw = (AtPw)AtIteratorNext(pwIterator)) != NULL)
        remained = remained + PwLocalIdRestore((ThaModuleMap)self, pw);
    AtObjectDelete((AtObject)pwIterator);

    return remained;
    }

static eAtRet HoBertHwDefault(ThaModuleMap self, AtSdhChannel channel)
    {
    uint8 sts_i;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    ThaModuleDemap demapModule = (ThaModuleDemap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleDemap);
    ThaModuleStmMap stmMapModule = (ThaModuleStmMap)self;
    uint8 srcType = (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc3) ? 0 : 1;
    uint32 demapRegAddress = mMethodsGet(demapModule)->VcDmapChnCtrl(demapModule, (AtSdhVc)channel);
    uint32 mapRegAddress = mMethodsGet(stmMapModule)->MapLineCtrl(stmMapModule, channel);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
       {
       uint8 sts1Id = (uint8)(startSts + sts_i);
       uint8 hwSlice = 0, hwSts = 0;
       uint32 regAddr, regVal;

       if (ThaSdhChannelHwStsGet(channel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
           mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

       regAddr = demapRegAddress + mMethodsGet(abstractMap)->AuVcStsDefaultOffset(abstractMap, (AtSdhVc)channel, sts1Id);
       regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);

       if ((channelType == cAtSdhChannelTypeVc4_4c) || (numSts == 12))
           mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i < 4) ? 1 : 0);
       else if ((channelType == cAtSdhChannelTypeVc4_16c) || (numSts == 48))
           mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i < 16) ? 1 : 0);
       else if (channelType == cAtSdhChannelTypeVc4_nc)
           mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i % 3 == 0) ? 1 : 0);
       else
           mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i == 0) ? 1 : 0);

       mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsrctype_, srcType);
       mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);

       /* Need to set master for first 4 STSs in VC4-4c */
       regAddr = mapRegAddress + mMethodsGet(abstractMap)->AuVcStsDefaultOffset(abstractMap, (AtSdhVc)channel, sts1Id);
       regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);

       if ((channelType == cAtSdhChannelTypeVc4_4c) || (numSts == 12))
           mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i < 4) ? 1 : 0);
       else if ((channelType == cAtSdhChannelTypeVc4_16c) || (numSts == 48))
           mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i < 16) ? 1 : 0);
       else if (channelType == cAtSdhChannelTypeVc4_nc)
           mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i % 3 == 0) ? 1 : 0);
       else
           mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i == 0) ? 1 : 0);

       mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_type_, srcType);
       mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);
       }
    return cAtOk;
    }

static uint32 EncapHwIdAllocate(ThaModuleMap self,
                                AtChannel circuit,
                                uint8 slice,
                                uint32 (*HwIdAllocate)(AtChannel circuit))
    {
    ThaBitMask pool;
    uint32 freeHwId = cThaMapInvalidEncapHwId;
    Tha60210011ModuleMap moduleMap = (Tha60210011ModuleMap)self;

    pool = PoolBySlice(self, slice);
    if (pool == NULL)
        return cThaMapInvalidEncapHwId;

    if (AtModuleInAccessible((AtModule)self))
        return cThaMapInvalidEncapHwId;

    /* Active driver or standby driver get invalid HW id  */
    AtOsalMutexLock(moduleMap->loMutex);
    if (freeHwId == cThaMapInvalidEncapHwId)
        {
        uint32 allocatedHwId = cThaMapInvalidEncapHwId;

        if (HwIdAllocate)
            allocatedHwId = HwIdAllocate(circuit);

        if ((allocatedHwId != cThaMapInvalidEncapHwId) && (ThaBitMaskBitVal(pool, allocatedHwId) == 0))
            freeHwId = allocatedHwId;
        else
            freeHwId = ThaBitMaskFirstZeroBit(pool);
        }

    /* Use a valid one */
    if (ThaBitMaskBitPositionIsValid(pool, freeHwId))
        ThaBitMaskSetBit(pool, freeHwId);

    AtOsalMutexUnLock(moduleMap->loMutex);
    return freeHwId;
    }

static uint32 LoLineVcEncapHwIdAllocate(ThaModuleMap self, AtSdhVc vc)
    {
    uint8 loSlice, hwIdInSlice;

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModuleMap, &loSlice, &hwIdInSlice) != cAtOk)
        {
        mChannelLog(vc, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");
        return cThaMapInvalidEncapHwId;
        }

    return EncapHwIdAllocate(self, (AtChannel)vc, loSlice, LoLineVcFlatId);
    }

static uint32 HoLineVcEncapHwIdAllocate(ThaModuleMap self, AtSdhVc vcx)
    {
    uint8 slice, hwSts;
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)vcx, cThaModuleMap, &slice, &hwSts) != cAtOk)
        {
        mChannelLog(vcx, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");
        return cThaMapInvalidEncapHwId;
        }

    return hwSts;
    }

static uint32 PdhChannelEncapHwIdAllocate(ThaModuleMap self,
                                          AtPdhChannel channel,
                                          uint32 (*HwIdAllocate)(AtChannel circuit))
    {
    uint8 loSlice, hwIdInSlice;

    if (ThaPdhChannelHwIdGet(channel, cThaModuleMap, &loSlice, &hwIdInSlice) != cAtOk)
        {
        mChannelLog(channel, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");
        return cThaMapInvalidEncapHwId;
        }

    return EncapHwIdAllocate(self, (AtChannel)channel, loSlice, HwIdAllocate);
    }

static uint32 De1EncapHwIdAllocate(ThaModuleMap self, AtPdhDe1 de1)
    {
    return PdhChannelEncapHwIdAllocate(self, (AtPdhChannel)de1, PdhChannelFlatId);
    }

static uint32 De3EncapHwIdAllocate(ThaModuleMap self, AtPdhDe3 de3)
    {
    return PdhChannelEncapHwIdAllocate(self, (AtPdhChannel)de3, PdhChannelFlatId);
    }

static uint32 NxDs0EncapHwIdAllocate(ThaModuleMap self, AtPdhNxDS0 nxds0)
    {
    AtPdhChannel de1 = (AtPdhChannel)AtPdhNxDS0De1Get(nxds0);
    return PdhChannelEncapHwIdAllocate(self, (AtPdhChannel)de1, NULL);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BindHoLineVcToPseudowire);
        mMethodOverride(m_methods, LoSliceLocalPwIdRestore);
        mMethodOverride(m_methods, MaxNumStsInLoSlice);
        mMethodOverride(m_methods, NumberOfMapLoPwPerSlice);
        mMethodOverride(m_methods, HoLineVcPwAllocate);
        mMethodOverride(m_methods, LomapChannelCtrlBase);
        mMethodOverride(m_methods, AllPwLocalIdRestore);
        mMethodOverride(m_methods, StartMapLocalRamId);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, Restore);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleStmMap(AtModule self)
    {
    ThaModuleStmMap mapModule = (ThaModuleStmMap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleStmMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleStmMapOverride));

        /* Override register address */
        mMethodOverride(m_ThaModuleStmMapOverride, MapLineCtrl);
        mMethodOverride(m_ThaModuleStmMapOverride, SdhChannelMapLineOffset);
        mMethodOverride(m_ThaModuleStmMapOverride, DefaultSet);
        mMethodOverride(m_ThaModuleStmMapOverride, Vc4_4cConfigurationClear);
        }

    mMethodsSet(mapModule, &m_ThaModuleStmMapOverride);
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, m_ThaModuleMapMethods, sizeof(m_ThaModuleMapOverride));

        /* Override register address */
        mMethodOverride(m_ThaModuleMapOverride, De1MapChnCtrl);
        mMethodOverride(m_ThaModuleMapOverride, VcMapChnCtrl);
        mMethodOverride(m_ThaModuleMapOverride, MapFrmTypeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapFrmTypeShift);
        mMethodOverride(m_ThaModuleMapOverride, MapSigTypeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapSigTypeShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcMastMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcMastShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcIdMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcIdShift);
        mMethodOverride(m_ThaModuleMapOverride, MapLineControlVcDefaultSet);
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTmDs1Loopcode)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapPWIdField)
        mMethodOverride(m_ThaModuleMapOverride, HoBertHwDefault);
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhSliceOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NumHoSlices);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NumLoSlices);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NeedEnableAfterBindToPw);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcFrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcStsDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcBoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HoVcSignalTypeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HoSliceOffset);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap(self);
    OverrideThaModuleStmMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleMap);
    }

AtModule Tha60210011ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Setup some private data. */
    mThis(self)->loMutex = AtOsalMutexCreate();

    return self;
    }

AtModule Tha60210011ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleMapObjectInit(newModule, device);
    }

uint32 Tha60210011ModuleMapDemapLoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    AtUnused(self);
    return (((slice / 4UL) * 0x100000UL) + ((slice % 4UL) * 0x40000UL) + cLoBaseAddress);
    }

uint32 Tha60210011ModuleMapDemapHoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    AtUnused(self);
    return cHoBaseAddress + (slice * 256UL);
    }

uint32 Tha60210011ModuleMapDemapSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    if (ShouldOperateOnHoBus((AtModule)self, sdhChannel))
        return Tha60210011ModuleMapDemapHoSliceOffset(self, slice);

    return Tha60210011ModuleMapDemapLoSliceOffset(self, slice);
    }

eThaMapChannelType Tha60210011ModuleMapChannelType(AtChannel circuit, AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeCESoP)
        return cThaMapChannelNxDs0;

    if (pwType == cAtPwTypeSAToP)
        return PdhChannelMapChannelTypeGet((AtPdhChannel)circuit);

    if (pwType == cAtPwTypeCEP)
        return SdhChannelMapChannelTypeGet((AtSdhChannel)circuit);

    return cThaMapChannelTypeUnknown;
    }

uint32 Tha60210011MapDemapHoLineVcDefaultOffset(ThaModuleAbstractMap self, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    return ((256UL * slice) + hwSts + cHoBaseAddress);
    }

uint32 Tha60210011ModuleMapPwAllocate(ThaModuleMap self, ThaPwAdapter pwAdapter, eBool isLo, uint8 *slice)
    {
    if (self == NULL)
        return cInvalidUint32;

    if (AtPwBoundCircuitGet((AtPw)pwAdapter) == NULL)
        {
        mChannelLog(pwAdapter, cAtLogLevelCritical, "PW circuit is NULL");
        return cThaMapInvalidPwChannelId;
        }

    if (isLo)
        return LoLinePwAllocate(self, pwAdapter, slice);

    if (HasMapHo((AtModule)self))
        return mMethodsGet(mThis(self))->HoLineVcPwAllocate(mThis(self), pwAdapter, slice);

    return LoLinePwAllocate(self, pwAdapter, slice);
    }

eAtRet Tha60210011ModuleMapPwDeallocate(ThaModuleMap self, eBool isLo, uint8 slice, uint32 tdmPwId)
    {
    if (isLo)
        return LoLinePwDeallocate(self, slice, tdmPwId);

    if (HasMapHo((AtModule)self))
        return HoLineVcPwDeallocate(self, slice, tdmPwId);

    return LoLinePwDeallocate(self, slice, tdmPwId);
    }

void Tha60210011MapHoBertHwDefault(ThaModuleMap self, AtSdhChannel channel)
    {
    ThaModuleMapHoBertHwDefault(self, channel);
    }

uint32 Tha60210011ModuleMapHoBaseAddress(ThaModuleMap self)
    {
    AtUnused(self);
    return cHoBaseAddress;
    }

uint32 Tha60210011ModuleMapLoBaseAddress(ThaModuleMap self)
    {
    AtUnused(self);
    return cLoBaseAddress;
    }

uint32 Tha60210011ModuleMapStartLoRamId(ThaModuleMap self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartMapLocalRamId(mThis(self));

    return cInvalidUint32;
    }

eAtRet Tha60210011ModuleMapSupperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

eBool Tha60210011ModuleMapDemapBitFieldsNeedShiftUp(ThaModuleAbstractMap self)
    {
    if (self)
        return BitFieldsNeedShiftUp(self);
    return cAtFalse;
    }

uint32 Tha60210011ModuleMapLomapChannelCtrlBase(Tha60210011ModuleMap self)
    {
    if (self)
        return mMethodsGet(self)->LomapChannelCtrlBase(self);
    return cInvalidUint32;
    }

ThaBitMask Tha60210011ModuleMapPoolBySlice(ThaModuleMap self, uint8 slice)
    {
    if (self)
        return PoolBySlice(self, slice);
    return NULL;
    }

uint32 Tha60210011ModuleMapHoLineVcEncapHwIdAllocate(ThaModuleMap self, AtSdhVc vcx)
    {
    if (self == NULL)
        return cInvalidUint32;

    if (HasMapHo((AtModule)self))
        return HoLineVcEncapHwIdAllocate(self, vcx);

    return LoLineVcEncapHwIdAllocate(self, vcx);
    }

uint32 Tha60210011ModuleMapLoLineVcEncapHwIdAllocate(ThaModuleMap self, AtSdhVc vc1x)
    {
    if (self)
        return LoLineVcEncapHwIdAllocate(self, vc1x);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleMapDe1EncapHwIdAllocate(ThaModuleMap self, AtPdhDe1 de1)
    {
    if (self)
        return De1EncapHwIdAllocate(self, de1);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleMapDe3EncapHwIdAllocate(ThaModuleMap self, AtPdhDe3 de3)
    {
    if (self)
        return De3EncapHwIdAllocate(self, de3);
    return cInvalidUint32;
    }

uint32 Tha60210011ModuleMapNxDs0EncapHwIdAllocate(ThaModuleMap self, AtPdhNxDS0 nxds0)
    {
    if (self)
        return NxDs0EncapHwIdAllocate(self, nxds0);
    return cInvalidUint32;
    }

eAtRet Tha60210011ModuleMapEncapHwIdDeallocate(ThaModuleMap self, eBool isLo, uint8 slice, uint32 encapHwId)
    {
    return Tha60210011ModuleMapPwDeallocate(self, isLo, slice, encapHwId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamCdrHo.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : HO CDR internal RAM for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../cdr/Tha60210011ModuleCdr.h"
#include "Tha60210011InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011InternalRamCdrHo
    {
    tThaInternalRam super;
    }tTha60210011InternalRamCdrHo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleCdr ModuleCdr(ThaInternalRam self)
    {
    return (Tha60210011ModuleCdr)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) == 0)
        return Tha60210011ModuleCdrHoBaseAddress(ModuleCdr(self)) + 0x188;

    return Tha60210011ModuleCdrHoBaseAddress(ModuleCdr(self)) + 0x2000c;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    if (localId == 0)
        return cBit0;

    return cBit0 << (localId - 1);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);
    uint32 offset;

    if (AtInternalRamIsReserved(internalRam))
        return cInvalidUint32;

    if (localId == 0)
        offset = 0x100;
    else
        offset = (uint32)(0x20800 + 64 * (localId - 1));

    return (uint32)(Tha60210011ModuleCdrHoBaseAddress(ModuleCdr(self)) + offset);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamCdrHo);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamCdrHoNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamPdaEcc.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : PDA internal RAM ECC implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../man/Tha60210011Device.h"
#include "Tha60210011InternalRam.h"
#include "diag/Tha6021001RamErrorGenerator.h"
#include "Tha60210011RamErrorGeneratorReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cPdaBaseAddress 0x500000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011InternalRamPdaEcc
    {
    tThaInternalRamEcc super;
    }tTha60210011InternalRamPdaEcc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInternalRamMethods      m_AtInternalRamOverride;
static tThaInternalRamMethods     m_ThaInternalRamOverride;
static tThaInternalRamEccMethods  m_ThaInternalRamEccOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return cPdaBaseAddress + 0x8;
    }

static uint32 CorrectableMask(ThaInternalRamEcc self)
    {
    AtUnused(self);
    return cBit6;
    }

static uint32 UncorrectableMask(ThaInternalRamEcc self)
    {
    AtUnused(self);
    return cBit5;
    }

static eBool ErrorGeneratorIsSupported(ThaInternalRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtInternalRamModuleRamGet((AtInternalRam)self));
    return Tha60210011RamErrorGeneratorIsSupported(device);
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    switch (counterType)
        {
        case cAtRamCounterTypeEccCorrectableErrors   : return mMethodsGet(ram)->ErrorGeneratorIsSupported(ram);
        case cAtRamCounterTypeEccUncorrectableErrors : return mMethodsGet(ram)->ErrorGeneratorIsSupported(ram);
        default:
            return cAtFalse;
        }
    }

static uint32 CounterTypeRegister(AtInternalRam self,
                                  uint16 counterType,
                                  eBool r2c)
    {
    AtUnused(self);
    
    switch (counterType)
        {
        case cAtRamCounterTypeEccCorrectableErrors  : return cAf6Reg_ddrno1_ecc_correctable_counter(r2c ? 1 : 0);
        case cAtRamCounterTypeEccUncorrectableErrors: return cAf6Reg_ddrno1_ecc_noncorrectable_counter(r2c ? 1 : 0);
        default: return cBit31_0;
        }
    }

static uint32 HwCounterGet(AtInternalRam self, uint16 counterType, eBool r2c)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = CounterTypeRegister(self, counterType, r2c);
    return mModuleHwRead(module, regAddr);
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    if (AtInternalRamCounterTypeIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtFalse);
    return 0x0;
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    if (AtInternalRamCounterTypeIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtTrue);
    return 0x0;
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaInternalRam self)
    {
    AtModule moduleRam = (AtModule)AtInternalRamModuleRamGet((AtInternalRam)self);
    return Tha60210011PdaRamEccErrorGeneratorNew(moduleRam);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));
        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CounterGet);
        mMethodOverride(m_AtInternalRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorObjectCreate);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorIsSupported);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideThaInternalRamEcc(AtInternalRam self)
    {
    ThaInternalRamEcc ram = (ThaInternalRamEcc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamEccOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamEccOverride));

        mMethodOverride(m_ThaInternalRamEccOverride, CorrectableMask);
        mMethodOverride(m_ThaInternalRamEccOverride, UncorrectableMask);
        }

    mMethodsSet(ram, &m_ThaInternalRamEccOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    OverrideThaInternalRamEcc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamPdaEcc);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamEccObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamPdaEccNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamOcn.c
 *
 * Created Date: Dec 23, 2015
 *
 * Description : OCN Internal RAM implementation for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60210011ModuleOcn.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cStsPpMaxRamLocalId     32
#define cSxcMaxRamLocalId       62

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtInternalRam self)
    {
    return Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)AtInternalRamPhyModuleGet(self));
    }

static eBool IsReserved(AtInternalRam self)
    {
    static const uint32 reserved[] = {4, 5, 6, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31,
                                      36, 37, 42, 43, 44, 45, 50, 51, 52, 53, 58, 59, 60, 61, 66,
                                      67, 72, 73, 78, 79, 84, 85};
    uint32 numReserved = mCount(reserved);
    uint32 localId = AtInternalRamLocalIdGet(self);
    uint32 ram_i;

    for (ram_i = 0; ram_i < numReserved; ram_i++)
        if (reserved[ram_i] == localId)
            return cAtTrue;

    return cAtFalse;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);

    if (localId < cStsPpMaxRamLocalId)
        return BaseAddress(internalRam) + 0x10;

    if (localId < cSxcMaxRamLocalId)
        return BaseAddress(internalRam) + 0x14;

    return BaseAddress(internalRam) + 0x18;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (localId < cStsPpMaxRamLocalId)
        return cBit0 << localId;

    if (localId < cSxcMaxRamLocalId)
        return cBit0 << (localId - cStsPpMaxRamLocalId);

    return cBit0 << (localId - cSxcMaxRamLocalId);
    }

static uint32 HoSliceIdGet(ThaInternalRam self, uint32 offset)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)((localId - offset) % 8);
    }

static uint32 LoSliceIdGet(ThaInternalRam self, uint32 offset)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)((localId - offset) % 6);
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (IsReserved((AtInternalRam)self))
        return cInvalidUint32;

    /* OCN Rx Bridge and Roll SXC Control */
    if (localId < 8)
        return (uint32)(HoSliceIdGet(self, 0) * 0x100 + 0x26000);

    /* OCN Tx Bridge and Roll SXC Control */
    if (localId < 16)
        return (uint32)(HoSliceIdGet(self, 8) * 0x100 + 0x27000);

    /* OCN STS Pointer Interpreter Per Channel Control */
    if (localId < 24)
        return (uint32)(HoSliceIdGet(self, 16) * 0x200 + 0x22000);

    /* OCN STS Pointer Generator Per Channel Control */
    if (localId < 32)
        return (uint32)(HoSliceIdGet(self, 32) * 0x200 + 0x23000);

    /* OCN Rx SXC Control */
    if (localId < 46)
        return (uint32)(((localId - 32) % 14) * 0x100 + 0x24000);

    /* OCN Tx SXC Control */
    if (localId < 54)
        return (uint32)(HoSliceIdGet(self, 46) * 0x100 + 0x25000);

    /* OCN Rx High Order Map concatenate configuration */
    if (localId < 62)
        return (uint32)(HoSliceIdGet(self, 54) * 0x200 + 0x28000);

    /* OCN RXPP Per STS payload Control */
    if (localId < 68)
        return (uint32)(LoSliceIdGet(self, 62) * 0x4000 + 0x40000);

    /* OCN VTTU Pointer Interpreter Per Channel Control */
    if (localId < 74)
        return (uint32)(LoSliceIdGet(self, 68) * 0x4000 + 0x40800);

    /* OCN TXPP Per STS Multiplexing Control */
    if (localId < 80)
        return (uint32)(LoSliceIdGet(self, 74) * 0x4000 + 0x60000);

    /* OCN VTTU Pointer Generator Per Channel Control */
    if (localId < 86)
        return (uint32)(LoSliceIdGet(self, 80) * 0x4000 + 0x60800);

    return cInvalidUint32;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);

    if (offset == cInvalidUint32)
        return cInvalidUint32;

    return (uint32)(BaseAddress((AtInternalRam)self) + offset);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamOcn);
    }

AtInternalRam Tha60210011InternalRamOcnObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamOcnObjectInit(newRam, phyModule, ramId, localId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamPdh.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : PDH Internal RAM implementation for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pdh/Tha60210011ModulePdh.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePdh ModulePdh(AtInternalRam self)
    {
    return (Tha60210011ModulePdh)AtInternalRamPhyModuleGet(self);
    }

static uint32 NumRamsInSlice(AtInternalRam self)
	{
    return Tha60210011ModulePdhNumPdhRamsInSlice(ModulePdh(self));
	}
	
static uint32 SliceGet(AtInternalRam self)
    {
    return AtInternalRamLocalIdGet(self) / NumRamsInSlice(self);
    }

static const char* Name(AtInternalRam self)
    {
    static char fineDescription[128] = {0};
    static const char * description[] =
        {
         "PDH DS1/E1/J1 Rx Framer Mux Control"       ,
         "PDH STS/VT Demap Control"                  ,
         "PDH RxM23E23 Control"                      ,
         "PDH RxDS3E3 OH Pro Control"                ,
         "PDH RxM12E12 Control"                      ,
         "PDH DS1/E1/J1 Rx Framer Control"           ,
         "PDH DS1/E1/J1 Tx Framer Control"           ,
         "PDH DS1/E1/J1 Tx Framer Signalling Control",
         "PDH TxM12E12 Control"                      ,
         "PDH TxM23E23 Control"                      ,
         "PDH TxM23E23 E3g832 Trace Byte"            ,
         "PDH STS/VT Map Control"                    ,
         sTha60210011ModulePdhRamPDHVTAsyncMapControl
        };

    AtSprintf(fineDescription, "%s %u", description[self->localId % NumRamsInSlice(self)], SliceGet(self));
    return (const char*) fineDescription;
    }

static const char* Description(AtInternalRam self)
    {
    return AtInternalRamName(self);
    }

static uint32 LocalIdInSlice(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    return AtInternalRamLocalIdGet(internalRam) % NumRamsInSlice(internalRam);
    }

static eBool IsReserved(AtInternalRam self)
    {
    const uint8 cInUsedSlice = 8;

    if (AtModuleInternalRamIsReserved(AtInternalRamPhyModuleGet(self), self))
        return cAtTrue;

    /* PDH DS1/E1/J1 Rx Framer Mux Control is reserved */
    return (SliceGet(self) < cInUsedSlice && (LocalIdInSlice((ThaInternalRam)self) > 0)) ? cAtFalse : cAtTrue;
    }

static uint32 SliceBaseAddress(ThaInternalRam self)
    {
    return 0x1000000 + (SliceGet((AtInternalRam)self)) * 0x100000;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    return SliceBaseAddress(self) + 0x10;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    return cBit0 << LocalIdInSlice(self);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    const uint32 cCellAddressInSlice[] = {0x30000,
                                          0x24000,
                                          0x40000,
                                          0x40420,
                                          0x41000,
                                          0x54000,
                                          0x94000,
                                          0x94800,
                                          0x81000,
                                          0x80000,
                                          0x80200,
                                          0x76000,
                                          0x76800};
    uint32 localId;

    if (AtInternalRamIsReserved((AtInternalRam)self))
        return cInvalidUint32;

    localId = LocalIdInSlice(self);
    if (localId >= mCount(cCellAddressInSlice))
        return cInvalidUint32;

    return (uint32)(SliceBaseAddress(self) + cCellAddressInSlice[localId]);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, Description);
        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        mMethodOverride(m_AtInternalRamOverride, Name);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamPdh);
    }

AtInternalRam Tha60210011InternalRamPdhObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamPdhNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamPdhObjectInit(newRam, phyModule, ramId, localId);
    }

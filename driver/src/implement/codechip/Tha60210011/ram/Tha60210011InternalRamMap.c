/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamMap.c
 *
 * Created Date: Dec 15, 2015
 *
 * Description : MAP internal RAM for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../map/Tha60210011ModuleMap.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumInternalRamInSlice   3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((Tha60210011InternalRamMap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011InternalRamMapMethods m_methods;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartLocalId(AtInternalRam self)
    {
    return Tha60210011ModuleMapStartLoRamId((ThaModuleMap)AtInternalRamPhyModuleGet(self));
    }

static uint32 SliceIdGet(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    return (AtInternalRamLocalIdGet(internalRam) - StartLocalId(internalRam)) / cNumInternalRamInSlice;
    }

static eBool IsReserved(AtInternalRam self)
    {
    if (SliceIdGet((ThaInternalRam)self) < 8)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 LocalIdInSlice(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    return (AtInternalRamLocalIdGet(internalRam) - StartLocalId(internalRam)) % cNumInternalRamInSlice;
    }

static uint32 SliceOffset(ThaInternalRam self)
    {
    return SliceIdGet(self) * 0x40000;
    }

static uint32 LoBaseAddress(ThaInternalRam self)
    {
    return Tha60210011ModuleMapLoBaseAddress((ThaModuleMap)AtInternalRamPhyModuleGet((AtInternalRam)(self)));
    }

static uint32 MapErrorForceRegister(Tha60210011InternalRamMap self)
    {
    AtUnused(self);
    return 0x18208;
    }

static uint32 DemapErrorForceRegister(Tha60210011InternalRamMap self)
    {
    AtUnused(self);
    return 0x04218;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    uint32 baseAddress = LoBaseAddress(self) + SliceOffset(self);
    if (LocalIdInSlice(self) < 2)
        return baseAddress + mMethodsGet(mThis(self))->MapErrorForceRegister(mThis(self));

    return baseAddress + mMethodsGet(mThis(self))->DemapErrorForceRegister(mThis(self));
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localIdInSlice;
    if ((localIdInSlice = LocalIdInSlice(self)) < 2)
        return cBit0 << localIdInSlice;

    return cBit0;
    }

static Tha60210011ModuleMap MapModule(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    AtDevice device = AtModuleDeviceGet(module);
    return (Tha60210011ModuleMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 cellAddressOffsetInSlice[] = {cInvalidUint32, 0x10000, 0x0};
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localRamId = AtInternalRamLocalIdGet(internalRam);
    uint32 localIdInSlice;

    cellAddressOffsetInSlice[0] = Tha60210011ModuleMapLomapChannelCtrlBase(MapModule(self));

    if (AtInternalRamIsReserved(internalRam))
        return cInvalidUint32;

    if (localRamId < StartLocalId(internalRam))
        return cInvalidUint32;

    localIdInSlice = LocalIdInSlice(self);
    if (localIdInSlice >= mCount(cellAddressOffsetInSlice))
        return cInvalidUint32;

    return (uint32)(LoBaseAddress(self) + cellAddressOffsetInSlice[localIdInSlice] + SliceOffset(self));
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamMap);
    }

static void MethodsInit(AtInternalRam self)
    {
    Tha60210011InternalRamMap ram = (Tha60210011InternalRamMap)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Common */
        mMethodOverride(m_methods, MapErrorForceRegister);
        mMethodOverride(m_methods, DemapErrorForceRegister);
        }

    mMethodsSet(ram, &m_methods);
    }

 AtInternalRam Tha60210011InternalRamMapObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamMapNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamMapObjectInit(newRam, phyModule, ramId, localId);
    }

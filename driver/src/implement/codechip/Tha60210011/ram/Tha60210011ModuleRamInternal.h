/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210011ModuleRamInternal.h
 * 
 * Created Date: Aug 12, 2016
 *
 * Description : RAM module of product 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60210011MODULERAMINTERNAL_H_
#define THA60210011MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6021ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleRam
    {
    tTha6021ModuleRam super;
    }tTha60210011ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60210011ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* THA60210011MODULERAMINTERNAL_H_ */


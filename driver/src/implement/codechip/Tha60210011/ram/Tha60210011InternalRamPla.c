/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamPla.c
 *
 * Created Date: Dec 15, 2015
 *
 * Description : PLA Internal RAM implementation for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60210011Device.h"
#include "../pwe/Tha60210011ModulePwe.h"
#include "Tha60210011InternalRam.h"
#include "diag/Tha6021001RamErrorGenerator.h"
#include "Tha60210011RamErrorGeneratorReg.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/* Save super implementation */
static const tThaInternalRamMethods *m_ThaInternalRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePwe ModulePwe(ThaInternalRam self)
    {
    return (Tha60210011ModulePwe)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static uint32 CrcRamLocalId(ThaInternalRam self)
    {
    return Tha60210011ModulePweStartCrcRamLocalId(ModulePwe(self));
    }

static uint32 HSPWRamLocalStartId(ThaInternalRam self)
    {
    return Tha60210011ModulePweStartHSPWRamLocalId(ModulePwe(self));
    }

static uint32 HoPlaRamLocalStartId(ThaInternalRam self)
    {
    return Tha60210011ModulePweStartHoPlaRamLocalId(ModulePwe(self));
    }

static eBool RamIsParity(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) != CrcRamLocalId(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    return RamIsParity((ThaInternalRam)self) ? cAtTrue : cAtFalse;
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    return RamIsParity((ThaInternalRam)self) ? cAtRamAlarmParityError : cAtRamAlarmCrcError;
    }

static eBool RamIsLoPla(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < HoPlaRamLocalStartId(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsHoPla(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    if ((localId >= HoPlaRamLocalStartId(self)) && (localId < HSPWRamLocalStartId(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsPlaProtection(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < CrcRamLocalId(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    if (RamIsLoPla(self) || RamIsHoPla(self))
        return Tha60210011ModulePlaBaseAddress() + 0x88040;

    if (RamIsPlaProtection(self))
        return Tha60210011ModulePlaBaseAddress() + 0x88044;

    /* CRC PLA */
    return Tha60210011ModulePlaBaseAddress() + 0x88048;
    }

static uint32 ErrorMask(ThaInternalRam self)
    {
    return RamIsParity(self) ? cAtRamAlarmParityError : cAtRamAlarmCrcError;
    }

static uint32 LoPlaLocalIdMask(uint32 localId)
    {
    return cBit0 << (((localId / 3) * 4) + (localId % 3)) ;
    }

static uint32 HoPlaLocalIdMask(uint32 localId)
    {
    return cBit24 << (((localId / 3) * 4) + (localId % 3)) ;
    }

static uint32 HspwUpsrLocalIdMask(uint32 localId)
    {
    uint32 shift = (((localId / 3) * 4) + (localId % 3));

    /* PLA Output PWE Port1 and Port2 Control Register */
    if ((localId % 3) == 0)
        return cBit16 << shift;

    /* PLA UPSR Port1 OR Port2 Control Register */
    /* PLA HSPW Port1 OR Port2 Control Register */
    return ((cBit20 | cBit16) << (shift % 3));
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    if (RamIsLoPla(self))
        return LoPlaLocalIdMask(localId);

    if (RamIsHoPla(self))
        return HoPlaLocalIdMask(localId - HoPlaRamLocalStartId(self));

    if (RamIsPlaProtection(self))
        return HspwUpsrLocalIdMask(localId - HSPWRamLocalStartId(self));

    return cBit0;
    }

static eBool ErrorGeneratorIsSupported(ThaInternalRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtInternalRamModuleRamGet((AtInternalRam)self));
    if (RamIsParity(self))
        return cAtFalse;

    return Tha60210011RamErrorGeneratorIsSupported(device);
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    switch (counterType)
        {
        case cAtRamCounterTypeCrcErrors : return mMethodsGet(ram)->ErrorGeneratorIsSupported(ram);
        default: return cAtFalse;
        }
    }

static uint32 CounterTypeRegister(ThaInternalRam self,  uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtRamCounterTypeCrcErrors:   return cAf6Reg_ddrno1_crc_counter(r2c ? 1 : 0);
        default: return cBit31_0;
        }
    }

static uint32 HwCounterGet(AtInternalRam self, uint16 counterType, eBool r2c)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(ram)->CounterTypeRegister(ram, counterType, r2c);
    return mModuleHwRead(module, regAddr);
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    if (AtInternalRamCounterTypeIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtFalse);
    return 0x0;
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    if (AtInternalRamCounterTypeIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtTrue);
    return 0x0;
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaInternalRam self)
    {
    AtModule moduleRam = (AtModule)AtInternalRamModuleRamGet((AtInternalRam)self);
    return Tha60210011PlaRamCrcErrorGeneratorNew(moduleRam);
    }

static uint32 LoSliceOffsetGet(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)(0x10000 * (localId / 3));
    }

static uint32 HoSliceOffsetGet(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)(0x800 * ((localId - HoPlaRamLocalStartId(self)) / 3));
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 ramId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (RamIsLoPla(self))
        {
        static const uint32 cellAddressOfLoPla[] = {0x2000, 0x6000, 0xD000};
        return cellAddressOfLoPla[ramId % 3] + LoSliceOffsetGet(self);
        }

    if (RamIsHoPla(self))
        {
        static const uint32 cellAddressOfHoPla[] = {0x80100, 0x80300, 0x80680};
        return cellAddressOfHoPla[ramId % 3] + HoSliceOffsetGet(self);
        }

    if (RamIsPlaProtection(self))
        {
        static const uint32 cellAddressOfHspwUpsr[] = {0x90000, 0x94000, 0x98000, 0xA0000, 0x94000, 0x98000};
        uint32 localId = ramId - HSPWRamLocalStartId(self);
        if (localId >= mCount(cellAddressOfHspwUpsr))
            return cInvalidUint32;

        return cellAddressOfHspwUpsr[localId];
        }

    return cInvalidUint32;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);
    if (offset == cInvalidUint32)
        return cInvalidUint32;

    return (uint32)(Tha60210011ModulePlaBaseAddress() + offset);
    }

static eAtRet ParityVerifyingTrigger(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr;
    uint32 ramId = AtInternalRamLocalIdGet((AtInternalRam)self);
    const uint32 cHSPWRamLocalStartIdPort2 = HSPWRamLocalStartId(self) + 3;

    if (ramId < cHSPWRamLocalStartIdPort2)
        return m_ThaInternalRamMethods->ParityVerifyingTrigger(self);

    if (ramId < CrcRamLocalId(self))
        {
        const uint32 cellAddressOfHspwUpsrPort2[] = {0xA0000, 0xA4000, 0xA8000};
        uint32 localId = ramId - cHSPWRamLocalStartIdPort2;
        if (localId >= mCount(cellAddressOfHspwUpsrPort2))
            return cAtErrorInvalidAddress;

        /* Read a cell of internal RAM to produce parity error sticky. */
        regAddr = Tha60210011ModulePlaBaseAddress() + cellAddressOfHspwUpsrPort2[localId];
        mModuleHwRead(module, regAddr);
        }

    return cAtOk;
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, ForcableErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CounterGet);
        mMethodOverride(m_AtInternalRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaInternalRamMethods = mMethodsGet(ram);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, CounterTypeRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMask);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorObjectCreate);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        mMethodOverride(m_ThaInternalRamOverride, ParityVerifyingTrigger);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamPla);
    }

AtInternalRam Tha60210011InternalRamPlaObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamPlaObjectInit(newRam, phyModule, ramId, localId);
    }

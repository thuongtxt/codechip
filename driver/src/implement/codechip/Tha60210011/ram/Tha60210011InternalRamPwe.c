/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamPwe.c
 *
 * Created Date: Dec 15, 2015
 *
 * Description : PWE internal RAM for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pwe/Tha60210011ModulePwe.h"
#include "Tha60210011InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011InternalRamPwe
    {
    tThaInternalRam super;
    }tTha60210011InternalRamPwe;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePwe ModulePwe(AtInternalRam self)
    {
    return (Tha60210011ModulePwe)AtInternalRamPhyModuleGet(self);
    }

static uint32 StartLocalId(AtInternalRam self)
    {
    return Tha60210011ModulePweStartPweRamLocalId(ModulePwe(self));
    }

static uint32 SliceIdGet(AtInternalRam self)
    {
    return (AtInternalRamLocalIdGet(self) - StartLocalId(self));
    }

static eBool IsReserved(AtInternalRam self)
    {
    if (AtModuleInternalRamIsReserved(AtInternalRamPhyModuleGet(self), self))
        return cAtTrue;

    return (SliceIdGet(self) < 2) ? cAtFalse : cAtTrue;
    }

static uint32 SliceOffset(ThaInternalRam self)
    {
    return (uint32)(SliceIdGet((AtInternalRam)self) * 0x10000);
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    return Tha60210011ModulePweBaseAddress() + SliceOffset(self) + 0xF000;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    AtUnused(self);
    return cBit0;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 localRamId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (IsReserved((AtInternalRam)self))
        return cInvalidUint32;

    if (localRamId < StartLocalId((AtInternalRam)self))
        return cInvalidUint32;

    return (uint32)(Tha60210011ModulePweBaseAddress() + SliceOffset(self));
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamPwe);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamPweNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

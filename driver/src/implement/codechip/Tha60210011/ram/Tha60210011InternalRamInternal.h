/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210011InternalRamInternal.h
 * 
 * Created Date: Jul 27, 2016
 *
 * Description : Internal RAM declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011INTERNALRAMINTERNAL_H_
#define _THA60210011INTERNALRAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "Tha60210011InternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011InternalRamMap * Tha60210011InternalRamMap;
typedef struct tTha60210011InternalRamCdr * Tha60210011InternalRamCdr;

typedef struct tTha60210011InternalRamCla
    {
    tThaInternalRam super;
    }tTha60210011InternalRamCla;

typedef struct tTha60210011InternalRamOcn
    {
    tThaInternalRam super;
    }tTha60210011InternalRamOcn;

typedef struct tTha60210011InternalRamPla
    {
    tThaInternalRam super;
    }tTha60210011InternalRamPla;

typedef struct tTha60210011InternalRamPdh
    {
    tThaInternalRam super;
    }tTha60210011InternalRamPdh;

typedef struct tTha60210011InternalRamSur
    {
    tThaInternalRam super;
    }tTha60210011InternalRamSur;

typedef struct tTha60210011InternalRamMapMethods
    {
    uint32 (*DemapErrorForceRegister)(Tha60210011InternalRamMap self);
    uint32 (*MapErrorForceRegister)(Tha60210011InternalRamMap self);
    }tTha60210011InternalRamMapMethods;

typedef struct tTha60210011InternalRamMap
    {
    tThaInternalRam super;
    const tTha60210011InternalRamMapMethods *methods;
    }tTha60210011InternalRamMap;

typedef struct tTha60210011InternalRamCdrMethods
    {
    uint32 (*TimingGenParityForceRegister)(Tha60210011InternalRamCdr self);
    uint32 (*ACREngineTimingParityForceRegister)(Tha60210011InternalRamCdr self);
    }tTha60210011InternalRamCdrMethods;

typedef struct tTha60210011InternalRamCdr
    {
    tThaInternalRam super;
    const tTha60210011InternalRamCdrMethods *methods;
    }tTha60210011InternalRamCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam Tha60210011InternalRamClaObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamOcnObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPlaObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPdhObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamSurObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamMapObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamCdrObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011INTERNALRAMINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_PARITY_ECC_CRC_H_
#define _AF6_REG_AF6CCI0011_RD_PARITY_ECC_CRC_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : QDR ECC Force Error Control
Reg Addr   : 0xF26000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures QDR ECC force error.

------------------------------------------------------------------------------*/
#define cAf6Reg_qdr_ecc_force_error_control_Base                                                      0xF26000
#define cAf6Reg_qdr_ecc_force_error_control                                                           0xF26000
#define cAf6Reg_qdr_ecc_force_error_control_WidthVal                                                        32
#define cAf6Reg_qdr_ecc_force_error_control_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: QDREccNonCorrectCfg
BitField Type: RW
BitField Desc: Ecc error mode  1: Non-correctable  0: Correctable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_Bit_Start                                      29
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_Bit_End                                        29
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_Mask                                       cBit29
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_Shift                                          29
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_MaxVal                                        0x1
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_MinVal                                        0x0
#define cAf6_qdr_ecc_force_error_control_QDREccNonCorrectCfg_RstVal                                        0x0

/*--------------------------------------
BitField Name: QDREccErrForeverCfg
BitField Type: RW
BitField Desc: Force ecc error mode  1: forever when field QDREccErrNumber
differ zero 0: burst
BitField Bits: [28]
--------------------------------------*/
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_Bit_Start                                      28
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_Bit_End                                        28
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_Mask                                       cBit28
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_Shift                                          28
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_MaxVal                                        0x1
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_MinVal                                        0x0
#define cAf6_qdr_ecc_force_error_control_QDREccErrForeverCfg_RstVal                                        0x0

/*--------------------------------------
BitField Name: QDREccErrNumberCfg
BitField Type: RW
BitField Desc: number of ECC error inserted to QDR
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_Bit_Start                                        0
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_Bit_End                                         27
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_Mask                                      cBit27_0
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_Shift                                            0
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_MaxVal                                   0xfffffff
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_MinVal                                         0x0
#define cAf6_qdr_ecc_force_error_control_QDREccErrNumberCfg_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : QDR ECC Error Sticky
Reg Addr   : 0xF26001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report sticky of QDR ECC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_qdr_ecc_sticky_Base                                                                   0xF26001
#define cAf6Reg_qdr_ecc_sticky                                                                        0xF26001
#define cAf6Reg_qdr_ecc_sticky_WidthVal                                                                     32
#define cAf6Reg_qdr_ecc_sticky_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: QDREccNonCorrectError
BitField Type: WC
BitField Desc: QDR non-correctable error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_Bit_Start                                                  1
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_Bit_End                                                    1
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_Mask                                                   cBit1
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_Shift                                                      1
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_MaxVal                                                   0x1
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_MinVal                                                   0x0
#define cAf6_qdr_ecc_sticky_QDREccNonCorrectError_RstVal                                                   0x0

/*--------------------------------------
BitField Name: QDREccCorrectError
BitField Type: QDR correctable error
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_Bit_Start                                                     0
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_Bit_End                                                       0
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_Mask                                                      cBit0
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_Shift                                                         0
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_MaxVal                                                      0x1
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_MinVal                                                      0x0
#define cAf6_qdr_ecc_sticky_QDREccCorrectError_RstVal                                                       WC


/*------------------------------------------------------------------------------
Reg Name   : QDR ECC Correctable Error Counter
Reg Addr   : 0xF26002
Reg Formula: 0xF26002 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts QDR ECC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_qdr_ecc_correctable_counter_Base                                                      0xF26002
#define cAf6Reg_qdr_ecc_correctable_counter(R2C)                                              (0xF26002+(R2C))
#define cAf6Reg_qdr_ecc_correctable_counter_WidthVal                                                        32
#define cAf6Reg_qdr_ecc_correctable_counter_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: QDREccErrorCounter
BitField Type: QDR Ecc error counter
BitField Desc:
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_Bit_Start                                        0
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_Bit_End                                         27
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_Mask                                      cBit27_0
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_Shift                                            0
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_MaxVal                                   0xfffffff
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_MinVal                                         0x0
#define cAf6_qdr_ecc_correctable_counter_QDREccErrorCounter_RstVal                                          RO


/*------------------------------------------------------------------------------
Reg Name   : QDR ECC NonCorrectable Error Counter
Reg Addr   : 0xF26004
Reg Formula: 0xF26004 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts QDR ECC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_qdr_ecc_noncorrectable_counter_Base                                                   0xF26004
#define cAf6Reg_qdr_ecc_noncorrectable_counter(R2C)                                           (0xF26004+(R2C))
#define cAf6Reg_qdr_ecc_noncorrectable_counter_WidthVal                                                     32
#define cAf6Reg_qdr_ecc_noncorrectable_counter_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: QDREccErrorCounter
BitField Type: QDR Ecc error counter
BitField Desc:
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_Bit_Start                                       0
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_Bit_End                                      27
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_Mask                                   cBit27_0
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_Shift                                         0
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_MaxVal                                0xfffffff
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_MinVal                                      0x0
#define cAf6_qdr_ecc_noncorrectable_counter_QDREccErrorCounter_RstVal                                       RO


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 PDA ECC Force Error Control
Reg Addr   : 0x500020
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures DDRno1 ECC force error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_ecc_force_error_control_Base                                                   0x500020
#define cAf6Reg_ddrno1_ecc_force_error_control                                                        0x500020
#define cAf6Reg_ddrno1_ecc_force_error_control_WidthVal                                                     32
#define cAf6Reg_ddrno1_ecc_force_error_control_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: DDRno1EccNonCorrectCfg
BitField Type: RW
BitField Desc: Ecc error mode  1: Non-correctable  0: Correctable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_Bit_Start                                      29
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_Bit_End                                      29
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_Mask                                  cBit29
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_Shift                                      29
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_MaxVal                                     0x1
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_MinVal                                     0x0
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccNonCorrectCfg_RstVal                                     0x0

/*--------------------------------------
BitField Name: DDRno1EccErrForeverCfg
BitField Type: RW
BitField Desc: Force ecc error mode  1: forever when field DDRno1EccErrNumber
differ zero 0: burst
BitField Bits: [28]
--------------------------------------*/
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_Bit_Start                                      28
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_Bit_End                                      28
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_Mask                                  cBit28
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_Shift                                      28
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_MaxVal                                     0x1
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_MinVal                                     0x0
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrForeverCfg_RstVal                                     0x0

/*--------------------------------------
BitField Name: DDRno1EccErrNumberCfg
BitField Type: RW
BitField Desc: number of ECC error inserted to DDRno1
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_Bit_Start                                       0
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_Bit_End                                      27
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_Mask                                cBit27_0
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_Shift                                       0
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_MaxVal                               0xfffffff
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_MinVal                                     0x0
#define cAf6_ddrno1_ecc_force_error_control_DDRno1EccErrNumberCfg_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 PDA ECC Error Sticky
Reg Addr   : 0x500021
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report sticky of DDRno1 ECC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_ecc_sticky_Base                                                                0x500021
#define cAf6Reg_ddrno1_ecc_sticky                                                                     0x500021
#define cAf6Reg_ddrno1_ecc_sticky_WidthVal                                                                  32
#define cAf6Reg_ddrno1_ecc_sticky_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: DDRno1EccNonCorrectError
BitField Type: WC
BitField Desc: DDRno1 non-correctable error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_Bit_Start                                            1
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_Bit_End                                              1
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_Mask                                             cBit1
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_Shift                                                1
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_MaxVal                                             0x1
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_MinVal                                             0x0
#define cAf6_ddrno1_ecc_sticky_DDRno1EccNonCorrectError_RstVal                                             0x0

/*--------------------------------------
BitField Name: DDRno1EccCorrectError
BitField Type: DDRno1 correctable error
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_Bit_Start                                               0
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_Bit_End                                                 0
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_Mask                                                cBit0
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_Shift                                                   0
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_MaxVal                                                0x1
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_MinVal                                                0x0
#define cAf6_ddrno1_ecc_sticky_DDRno1EccCorrectError_RstVal                                                 WC


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 PDA ECC Correctable Error Counter
Reg Addr   : 0x500022
Reg Formula: 0x500022 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts DDRno1 ECC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_ecc_correctable_counter_Base                                                   0x500022
#define cAf6Reg_ddrno1_ecc_correctable_counter(R2C)                                           (0x500022+(R2C))
#define cAf6Reg_ddrno1_ecc_correctable_counter_WidthVal                                                     32
#define cAf6Reg_ddrno1_ecc_correctable_counter_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: DDRno1EccErrorCounter
BitField Type: DDRno1 Ecc error counter
BitField Desc:
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_Bit_Start                                       0
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_Bit_End                                      27
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_Mask                                cBit27_0
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_Shift                                       0
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_MaxVal                               0xfffffff
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_MinVal                                     0x0
#define cAf6_ddrno1_ecc_correctable_counter_DDRno1EccErrorCounter_RstVal                                      RO


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 PDA ECC NonCorrectable Error Counter
Reg Addr   : 0x500024
Reg Formula: 0x500024 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts DDRno1 ECC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_ecc_noncorrectable_counter_Base                                                0x500024
#define cAf6Reg_ddrno1_ecc_noncorrectable_counter(R2C)                                        (0x500024+(R2C))
#define cAf6Reg_ddrno1_ecc_noncorrectable_counter_WidthVal                                                  32
#define cAf6Reg_ddrno1_ecc_noncorrectable_counter_WriteMask                                                0x0

/*--------------------------------------
BitField Name: DDRno1EccErrorCounter
BitField Type: DDRno1 Ecc error counter
BitField Desc:
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_Bit_Start                                       0
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_Bit_End                                      27
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_Mask                                cBit27_0
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_Shift                                       0
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_MaxVal                               0xfffffff
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_MinVal                                     0x0
#define cAf6_ddrno1_ecc_noncorrectable_counter_DDRno1EccErrorCounter_RstVal                                      RO


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 PLA CRC Force Error Control
Reg Addr   : 0x488050
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures DDRno1 PLA CRC force error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_crc_force_error_control_Base                                                   0x488050
#define cAf6Reg_ddrno1_crc_force_error_control                                                        0x488050
#define cAf6Reg_ddrno1_crc_force_error_control_WidthVal                                                     32
#define cAf6Reg_ddrno1_crc_force_error_control_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: DDRno1CrcErrForeverCfg
BitField Type: RW
BitField Desc: Force crc error mode  1: forever when field DDRno1CrcErrNumber
differ zero 0: burst
BitField Bits: [28]
--------------------------------------*/
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_Bit_Start                                      28
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_Bit_End                                      28
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_Mask                                  cBit28
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_Shift                                      28
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_MaxVal                                     0x1
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_MinVal                                     0x0
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrForeverCfg_RstVal                                     0x0

/*--------------------------------------
BitField Name: DDRno1CrcErrNumberCfg
BitField Type: RW
BitField Desc: number of CRC error inserted to DDRno1
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_Bit_Start                                       0
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_Bit_End                                      27
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_Mask                                cBit27_0
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_Shift                                       0
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_MaxVal                               0xfffffff
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_MinVal                                     0x0
#define cAf6_ddrno1_crc_force_error_control_DDRno1CrcErrNumberCfg_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 PLA CRC Error Sticky
Reg Addr   : 0x488051
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report sticky of DDRno1 CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_crc_sticky_Base                                                                0x488051
#define cAf6Reg_ddrno1_crc_sticky                                                                     0x488051
#define cAf6Reg_ddrno1_crc_sticky_WidthVal                                                                  32
#define cAf6Reg_ddrno1_crc_sticky_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: DDRno1CrcError
BitField Type: DDRno1 CRC error
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_Bit_Start                                                      0
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_Bit_End                                                        0
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_Mask                                                       cBit0
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_Shift                                                          0
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_MaxVal                                                       0x1
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_MinVal                                                       0x0
#define cAf6_ddrno1_crc_sticky_DDRno1CrcError_RstVal                                                        WC


/*------------------------------------------------------------------------------
Reg Name   : DDRno1 CRC Error Counter
Reg Addr   : 0x488052
Reg Formula: 0x488052 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts DDRno1 CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno1_crc_counter_Base                                                               0x488052
#define cAf6Reg_ddrno1_crc_counter(R2C)                                                       (0x488052+(R2C))
#define cAf6Reg_ddrno1_crc_counter_WidthVal                                                                 32
#define cAf6Reg_ddrno1_crc_counter_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DDRno1CrcErrorCounter
BitField Type: DDRno1 Crc error counter
BitField Desc:
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_Bit_Start                                              0
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_Bit_End                                               27
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_Mask                                            cBit27_0
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_Shift                                                  0
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_MaxVal                                         0xfffffff
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_MinVal                                               0x0
#define cAf6_ddrno1_crc_counter_DDRno1CrcErrorCounter_RstVal                                                RO


/*------------------------------------------------------------------------------
Reg Name   : DDRno2 PDA CRC Force Error Control
Reg Addr   : 0x500030
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures DDRno2 PDA CRC force error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno2_crc_force_error_control_Base                                                   0x500030
#define cAf6Reg_ddrno2_crc_force_error_control                                                        0x500030
#define cAf6Reg_ddrno2_crc_force_error_control_WidthVal                                                     32
#define cAf6Reg_ddrno2_crc_force_error_control_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: DDRno2CrcErrForeverCfg
BitField Type: RW
BitField Desc: Force crc error mode  1: forever when field DDRno2CrcErrNumber
differ zero 0: burst
BitField Bits: [28]
--------------------------------------*/
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_Bit_Start                                      28
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_Bit_End                                      28
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_Mask                                  cBit28
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_Shift                                      28
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_MaxVal                                     0x1
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_MinVal                                     0x0
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrForeverCfg_RstVal                                     0x0

/*--------------------------------------
BitField Name: DDRno2CrcErrNumberCfg
BitField Type: RW
BitField Desc: number of CRC error inserted to DDRno2
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_Bit_Start                                       0
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_Bit_End                                      27
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_Mask                                cBit27_0
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_Shift                                       0
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_MaxVal                               0xfffffff
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_MinVal                                     0x0
#define cAf6_ddrno2_crc_force_error_control_DDRno2CrcErrNumberCfg_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DDRno2 PDA CRC Error Sticky
Reg Addr   : 0x500031
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report sticky of DDRno2 CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno2_crc_sticky_Base                                                                0x500031
#define cAf6Reg_ddrno2_crc_sticky                                                                     0x500031
#define cAf6Reg_ddrno2_crc_sticky_WidthVal                                                                  32
#define cAf6Reg_ddrno2_crc_sticky_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: DDRno2CrcError
BitField Type: DDRno2 CRC error
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_Bit_Start                                                      0
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_Bit_End                                                        0
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_Mask                                                       cBit0
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_Shift                                                          0
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_MaxVal                                                       0x1
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_MinVal                                                       0x0
#define cAf6_ddrno2_crc_sticky_DDRno2CrcError_RstVal                                                        WC


/*------------------------------------------------------------------------------
Reg Name   : DDRno2 CRC Error Counter
Reg Addr   : 0x500032
Reg Formula: 0x500032 + R2C
    Where  : 
           + $R2C(0-1): value zero for read only,value 1 for read to clear
Reg Desc   : 
This register counts DDRno2 CRC error.

------------------------------------------------------------------------------*/
#define cAf6Reg_ddrno2_crc_counter_Base                                                               0x500032
#define cAf6Reg_ddrno2_crc_counter(R2C)                                                       (0x500032+(R2C))
#define cAf6Reg_ddrno2_crc_counter_WidthVal                                                                 32
#define cAf6Reg_ddrno2_crc_counter_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DDRno2CrcErrorCounter
BitField Type: DDRno2 Crc error counter
BitField Desc:
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_Bit_Start                                              0
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_Bit_End                                               27
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_Mask                                            cBit27_0
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_Shift                                                  0
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_MaxVal                                         0xfffffff
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_MinVal                                               0x0
#define cAf6_ddrno2_crc_counter_DDRno2CrcErrorCounter_RstVal                                                RO

#endif /* _AF6_REG_AF6CCI0011_RD_PARITY_ECC_CRC_H_ */

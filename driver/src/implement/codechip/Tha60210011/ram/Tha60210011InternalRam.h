/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210011InternalRam.h
 * 
 * Created Date: Dec 7, 2015
 *
 * Description : Internal RAM inteface for product 60210011.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011INTERNALRAM_H_
#define _THA60210011INTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtInternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam Tha60210011InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPohNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPdhNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamMapNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPdaNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPdaEccNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamClaNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamCdrHoNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamCdrNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamMapHoNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamPweNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamDatalinkNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamSurNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210011InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011INTERNALRAM_H_ */


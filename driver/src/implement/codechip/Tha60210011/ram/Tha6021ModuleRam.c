/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6021ModuleRam.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : RAM module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRam.h"
#include "../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "../../Tha60150011/ram/Tha60150011Qdr.h"
#include "Tha6021ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidAddress 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6021ModuleRam)self)

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6021ModuleRamMethods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
    return Tha6021DdrNew(self, core, ddrId);
    }

static AtRam QdrCreate(AtModuleRam self, AtIpCore core, uint8 qdrId)
    {
    return Tha60210011QdrNew(self, core, qdrId);
    }

static uint32 Offset(AtRam self)
    {
    return ThaDdrDefaultOffset((ThaDdr) self);
    }

static uint32 DdrTestControlRegister(Tha6021ModuleRam self, AtRam ram)
    {
    AtUnused(self);
    return (0xF20051 + Offset(ram));
    }

static uint32 DdrTestModeRegister(Tha6021ModuleRam self, AtRam ram)
    {
    AtUnused(self);
    return (0xF20050 + Offset(ram));
    }

static uint32 DdrTestStatusRegister(Tha6021ModuleRam self, AtRam ram)
    {
    AtUnused(self);
    return (0xF20052 + Offset(ram));
    }

static uint32 DdrTestErrorCountersRegister(Tha6021ModuleRam self, AtRam ram, eTha6021RamCounterType readingMode)
    {
    uint32 offset = Offset(ram);
    AtUnused(self);
    if (readingMode == cTha6021RamCounterTypeReadonly)
        return (0xF20053 + offset);
    return 0xF20054 + offset;
    }

static uint32 DdrTestWriteCounterRegister(Tha6021ModuleRam self, AtRam ram, eTha6021RamCounterType readingMode)
    {
    AtUnused(self);
    AtUnused(readingMode);
    return (0xF20055 + Offset(ram));
    }

static uint32 DdrTestReadCounterRegister(Tha6021ModuleRam self, AtRam ram, eTha6021RamCounterType readingMode)
    {
    AtUnused(self);
    AtUnused(readingMode);
    return (0xF20056 + Offset(ram));
    }

static uint32 QDRUserClockValueStatusRegister(ThaModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    return (0xF00068UL + qdrId);
    }

static uint32 QdrUserClock(AtModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(qdrId);
    return 250000000;
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    AtUnused(self);
    AtUnused(ramId);
    AtUnused(localRamId);
    return NULL;
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, QDRUserClockValueStatusRegister);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        mMethodOverride(m_AtModuleRamOverride, QdrCreate);
        mMethodOverride(m_AtModuleRamOverride, QdrUserClock);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule ramModule = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(ramModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(ramModule, &m_AtModuleOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule(self);
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

static void MethodsInit(Tha6021ModuleRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Register */
        mMethodOverride(m_methods, DdrTestControlRegister);
        mMethodOverride(m_methods, DdrTestModeRegister);
        mMethodOverride(m_methods, DdrTestStatusRegister);
        mMethodOverride(m_methods, DdrTestErrorCountersRegister);
        mMethodOverride(m_methods, DdrTestReadCounterRegister);
        mMethodOverride(m_methods, DdrTestWriteCounterRegister);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021ModuleRam);
    }

AtModuleRam Tha6021ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

uint32 Tha6021ModuleRamDdrTestControlRegister(AtRam ram)
    {
    Tha6021ModuleRam self = mThis(AtRamModuleGet(ram));
    if (self)
        return mMethodsGet(self)->DdrTestControlRegister(self, ram);
    return cInvalidAddress;
    }

uint32 Tha6021ModuleRamDdrTestModeRegister(AtRam ram)
    {
    Tha6021ModuleRam self = mThis(AtRamModuleGet(ram));
    if (self)
        return mMethodsGet(self)->DdrTestModeRegister(self, ram);
    return cInvalidAddress;
    }

uint32 Tha6021ModuleRamDdrTestStatusRegister(AtRam ram)
    {
    Tha6021ModuleRam self = mThis(AtRamModuleGet(ram));
    if (self)
        return mMethodsGet(self)->DdrTestStatusRegister(self, ram);
    return cInvalidAddress;
    }

uint32 Tha6021ModuleRamDdrTestErrorCountersRegister(AtRam ram, eTha6021RamCounterType readingMode)
    {
    Tha6021ModuleRam self = mThis(AtRamModuleGet(ram));
    if (self)
        return mMethodsGet(self)->DdrTestErrorCountersRegister(self, ram, readingMode);
    return cInvalidAddress;
    }

uint32 Tha6021ModuleRamDdrTestReadCounterRegister(AtRam ram, eTha6021RamCounterType readingMode)
    {
    Tha6021ModuleRam self = mThis(AtRamModuleGet(ram));
    if (self)
        return mMethodsGet(self)->DdrTestReadCounterRegister(self, ram, readingMode);
    return cInvalidAddress;
    }

uint32 Tha6021ModuleRamDdrTestWriteCounterRegister(AtRam ram, eTha6021RamCounterType readingMode)
    {
    Tha6021ModuleRam self = mThis(AtRamModuleGet(ram));
    if (self)
        return mMethodsGet(self)->DdrTestWriteCounterRegister(self, ram, readingMode);
    return cInvalidAddress;
    }

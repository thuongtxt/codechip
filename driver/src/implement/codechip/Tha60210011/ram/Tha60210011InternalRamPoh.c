/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamPoh.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : POH Internal RAM implementation for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210011InternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#define cPohBaseAddress 0x200000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011InternalRamPoh
    {
    tThaInternalRam super;
    }tTha60210011InternalRamPoh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return cPohBaseAddress + 0x10;
    }

static ThaModulePoh PohModule(ThaInternalRam self)
    {
    AtModule phyModule = AtInternalRamPhyModuleGet((AtInternalRam)self);
    AtDevice device = AtModuleDeviceGet(phyModule);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 cellAddress[] = {cInvalidUint32, 0x28000, 0x40400, 0x44000, 0x60400, 0x62000};
    uint32 localRamId = AtInternalRamLocalIdGet((AtInternalRam)self);

    cellAddress[0] = ThaModulePohCpestsctrBase(PohModule(self));
    if (localRamId >= mCount(cellAddress))
        return cInvalidUint32;

    return (uint32)(cPohBaseAddress + cellAddress[localRamId]);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamPoh);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamPohNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

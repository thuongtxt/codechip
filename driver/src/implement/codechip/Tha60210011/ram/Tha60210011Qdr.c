/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011Qdr.c
 *
 * Created Date: Jul 11, 2015
 *
 * Description : QDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60150011/ram/Tha60150011Qdr.h"
#include "../../Tha60150011/man/Tha60150011DeviceReg.h"
#include "../man/Tha60210011Device.h"
#include "Tha6021ModuleRam.h"
#include "Tha60210011RamErrorGeneratorReg.h"
#include "Tha60210011QdrInternal.h"
#include "diag/Tha6021001RamErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
/* Test status */
#define cCalibFailMask cBit0
#define cPrbsErrorMask cBit4

/* Test control */
#define cHwTestEnableMask   cBit8
#define cHwTestEnableShift  8
#define cForceErrorMask     cBit4
#define cForceErrorShift    4
#define cHwTestModeMask     cBit1_0
#define cHwTestModeShift    0
#define cHwTestModeFixed    0
#define cHwTestModeIncrease 1
#define cHwTestModeRandom2  2
#define cHwTestModeRandom3  3
#define cHwAssistAddressIncreaseMask  cBit7
#define cHwAssistAddressIncreaseShift 7

/* Hardware test counters */
#define cHwTestReadCounterMask       cBit7_0
#define cHwTestReadCounterShift      0
#define cHwTestWriteCounterMask      cBit15_8
#define cHwTestWriteCounterShift     8
#define cHwTestReadValidCounterMask  cBit24_16
#define cHwTestReadValidCounterShift 16

/* Hardware test error counter */
#define cHwTestPrbsErrorCounterMask  cBit7_0
#define cHwTestPrbsErrorCounterShift 0

/* Error counter clear */
#define cErrorCounterClearMask  cBit0
#define cErrorCounterClearShift 0

/* Cell access */
#define cAddressMask cBit18_0

/* Read/write control */
#define cAccessCommandMask         cBit1_0
#define cAccessCommandShift        0
#define cAssistCommandRequestMask  cBit2
#define cAssistCommandRequestShift 2
#define cAccessCommandWrite        1
#define cAccessCommandRead         2
#define cAccessCommandDone         0

/* Hardware assist command */
#define cAssistCommandMask           cBit6_5
#define cAssistCommandShift          5
#define cAssistCommandFill           0
#define cAssistCommandReadWriteCheck 1
#define cAssistCommandReadCheck      2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011Qdr *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011QdrMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtRamMethods    m_AtRamOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtRamMethods    *m_AtRamMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(Tha60210011Qdr self)
    {
    AtUnused(self);
    return 0xF24000;
    }

static uint32 RegisterWithOffset(AtRam self, uint32 offset)
    {
    return mMethodsGet(mThis(self))->BaseAddress(mThis(self)) + offset;
    }

static uint32 StickyRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x0);
    }

static uint32 TestControlRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x1);
    }

static uint32 CountersRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x7);
    }

static uint32 PrbsErrorCounterRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x8);
    }

static uint32 ErrorCounterClearRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x3);
    }

static uint32 CellAddressRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x4);
    }

static uint32 CellReadDataRegister(AtRam self, uint32 dwordIndex)
    {
    return RegisterWithOffset(self, (dwordIndex * 0x10) + 0x6);
    }

static uint32 CellWriteDataRegister(AtRam self, uint32 dwordIndex)
    {
    return RegisterWithOffset(self, (dwordIndex * 0x10) + 0x5);
    }

static uint32 CellExpectedDataRegister(AtRam self, uint32 dwordIndex)
    {
    return RegisterWithOffset(self, (dwordIndex * 0x10) + 0x9);
    }

static uint32 AccessCommandRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0x2);
    }

static uint32 EndAddressRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0xF5);
    }

static uint32 FirstErrorAddressRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0xF6);
    }

static uint32 ErrorCountRegister(AtRam self)
    {
    return RegisterWithOffset(self, 0xF7);
    }

static void StartAddressSet(AtRam self, uint32 startAddress)
    {
    AtRamWrite(self, CellAddressRegister(self), startAddress);
    }

static void StopAddressSet(AtRam self, uint32 endAddress)
    {
    AtRamWrite(self, EndAddressRegister(self), endAddress);
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    uint32 regVal;
    uint32 regAddr = StickyRegister(self);

    AtRamWrite(self, regAddr, cCalibFailMask);
    AtOsalUSleep(1000);
    regVal = AtRamRead(self, regAddr);

    if (AtRamIsSimulated(self))
        return cAtOk;

    return (regVal & cCalibFailMask) ? cAtErrorQdrCalibFail : cAtOk;
    }

static eAtRet HwTestErrorForce(AtRam self, eBool forced)
    {
    uint32 regAddr = TestControlRegister(self);
    uint32 regVal  = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cForceError, forced ? 1 : 0);
    AtRamWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool HwTestErrorIsForced(AtRam self)
    {
    uint32 regAddr = TestControlRegister(self);
    uint32 regVal  = AtRamRead(self, regAddr);
    return (regVal & cForceErrorMask) ? cAtTrue : cAtFalse;
    }

static eAtRet DefaultHwModeSetup(AtRam self)
    {
    uint32 regAddr = TestControlRegister(self);
    uint32 regVal  = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cHwTestMode, cHwTestModeRandom2);
    AtRamWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet HwTestSetup(AtRam self)
    {
    eAtRet ret = cAtOk;

    ret |= AtRamHwTestErrorForce(self, cAtFalse);
    ret |= DefaultHwModeSetup(self);

    return ret;
    }

static eAtRet HelperHwTestStart(AtRam self, eBool start)
    {
    uint32 regAddr = TestControlRegister(self);
    uint32 regVal  = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cHwTestEnable, start ? 1 : 0);
    AtRamWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet HwTestStart(AtRam self)
    {
    if (AtRamHwTestIsStarted(self))
        return cAtOk;

    HwTestSetup(self);
    return HelperHwTestStart(self, cAtTrue);
    }

static eAtRet HwTestStop(AtRam self)
    {
    eAtRet ret = cAtOk;

    if (!AtRamHwTestIsStarted(self))
        return cAtOk;

    ret |= HelperHwTestStart(self, cAtFalse);
    ret |= AtRamHwTestErrorForce(self, cAtFalse);

    return ret;
    }

static eBool HwTestIsStarted(AtRam self)
    {
    uint32 regAddr = TestControlRegister(self);
    uint32 regVal  = AtRamRead(self, regAddr);
    return (regVal & cHwTestEnableMask) ? cAtTrue : cAtFalse;
    }

static eBool EccCounterIsSupported(AtRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    return Tha60210011RamErrorGeneratorIsSupported(device);
    }

static eBool CounterIsSupported(AtRam self, uint32 counterType)
    {
    switch (counterType)
        {
        case cAtRamCounterTypeRead   : return cAtTrue;
        case cAtRamCounterTypeWrite  : return cAtTrue;
        case cAtRamCounterTypeErrors : return cAtTrue;
        case cAtRamCounterTypeEccCorrectableErrors :   return EccCounterIsSupported(self);
        case cAtRamCounterTypeEccUncorrectableErrors : return EccCounterIsSupported(self);
        default:
            return cAtFalse;
        }
    }

static void ErrorCounterClear(AtRam self)
    {
    uint32 regAddr = ErrorCounterClearRegister(self);
    uint32 regVal;

    regVal  = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cErrorCounterClear, 1);
    AtRamWrite(self, regAddr, regVal);

    regVal  = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cErrorCounterClear, 0);
    AtRamWrite(self, regAddr, regVal);
    }

static uint32 EccCorrectableErrorsCounterRead2Clear(AtRam self, eBool clear)
    {
    uint32 regAddr = (uint32)cAf6Reg_qdr_ecc_correctable_counter(clear);
    return AtRamRead(self, regAddr);
    }

static uint32 EccUncorrectableErrorsCounterRead2Clear(AtRam self, eBool clear)
    {
    uint32 regAddr = (uint32)cAf6Reg_qdr_ecc_noncorrectable_counter(clear);
    return AtRamRead(self, regAddr);
    }

static uint32 CounterRead2Clear(AtRam self, uint32 counterType, eBool clear)
    {
    uint32 regVal = AtRamRead(self, CountersRegister(self));

    if (counterType == cAtRamCounterTypeRead)
        return mRegField(regVal, cHwTestReadCounter);
    if (counterType == cAtRamCounterTypeWrite)
        return mRegField(regVal, cHwTestWriteCounter);

    if (counterType == cAtRamCounterTypeErrors)
        {
        regVal = AtRamRead(self, PrbsErrorCounterRegister(self));
        if (clear)
            ErrorCounterClear(self);
        return mRegField(regVal, cHwTestPrbsErrorCounter);
        }

    if (counterType == cAtRamCounterTypeEccCorrectableErrors)
        return EccCorrectableErrorsCounterRead2Clear(self, clear);

    if (counterType == cAtRamCounterTypeEccUncorrectableErrors)
        return EccUncorrectableErrorsCounterRead2Clear(self, clear);

    return 0x0;
    }

static uint32 CounterGet(AtRam self, uint32 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtRam self, uint32 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static void DebugCountersDisplay(AtRam self)
    {
    uint32 regVal = AtRamRead(self, CountersRegister(self));
    uint32 counter = mRegField(regVal, cHwTestReadValidCounter);
    AtPrintc(cSevNormal, "* ReadValid: ");
    AtPrintc(counter ? cSevInfo : cSevCritical, "%u\r\n", counter);
    }

static void Debug(AtRam self)
    {
    m_AtRamMethods->Debug(self);
    DebugCountersDisplay(self);
    }

static eAtModuleRamRet AccessEnable(AtRam self, eBool enable)
    {
    /* If hardware mode is not running, memory can be accessed by SW at anytime */
    if (!AtRamHwTestIsStarted(self))
        {
        mThis(self)->accessEnabled = enable;
        return cAtOk;
        }

    if (enable)
        {
        AtDriverLog(AtDriverSharedDriverGet(),
                    cAtLogLevelCritical,
                    "Cannot access memory when hardware testing is running.\r\n");
        return cAtErrorModeNotSupport;
        }

    mThis(self)->accessEnabled = enable;
    return cAtOk;
    }

static eBool AccessIsEnabled(AtRam self)
    {
    return mThis(self)->accessEnabled;
    }

static uint32 CellSizeGet(AtRam self)
    {
    /* Note, just 144 bits are used */
    AtUnused(self);
    return 5;
    }

static uint32 CellSizeInBits(AtRam self)
    {
    return AtRamDataBusSizeGet(self) * 4;
    }

static eBool HwIsBusy(AtRam self)
    {
    uint32 regVal = AtRamRead(self, AccessCommandRegister(self));
    if (AtRamIsSimulated(self))
        return cAtFalse;
    return (mRegField(regVal, cAccessCommand) == 0) ? cAtFalse : cAtTrue;
    }

static AtDevice Device(AtRam self)
    {
    return AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    }

static uint32 TimeoutMs(AtRam self)
    {
    if (AtDeviceTestbenchIsEnabled(Device(self)))
        return 10 * 60 * 1000; /* 10 minutes */

    AtUnused(self);
    return 10;
    }

static eBool HwFinish(AtRam self)
    {
    uint32 timeoutMs = TimeoutMs(self);
    tAtOsalCurTime startTime, currentTime;
    uint32 elapseTime = 0;

    AtOsalCurTimeGet(&startTime);

    while (elapseTime < timeoutMs)
        {
        if (!HwIsBusy(self))
            return cAtTrue;

        if (AtRamIsSimulated(self))
            return cAtTrue;

        AtOsalCurTimeGet(&currentTime);
        elapseTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    return cAtFalse;
    }

static eAtRet ClearRequestIfNeed(AtRam self)
    {
    uint32 regAddr, regVal;

    if (!AtRamShouldClearStateMachineBeforeAccessing(self))
        return cAtOk;

    regAddr = AccessCommandRegister(self);
    regVal = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cAccessCommand, 0);
    AtRamWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet CellRead(AtRam self, uint32 address, uint32 *value)
    {
    uint32 numDwords, dword_i;

    ClearRequestIfNeed(self);

    if (HwIsBusy(self))
        return cAtErrorDevBusy;

    /* Make request */
    AtRamWrite(self, CellAddressRegister(self), address);
    AtRamWrite(self, AccessCommandRegister(self), cAccessCommandRead);
    if (!HwFinish(self))
        return cAtErrorRamAccessTimeout;

    /* Read data */
    numDwords = AtRamCellSizeGet(self);
    for (dword_i = 0; dword_i < numDwords; dword_i++)
        value[dword_i] = AtRamRead(self, CellReadDataRegister(self, dword_i));

    return cAtOk;
    }

static eAtRet CellWrite(AtRam self, uint32 address, const uint32 *value)
    {
    uint32 numDwords, dword_i;

    ClearRequestIfNeed(self);

    if (HwIsBusy(self))
        return cAtErrorDevBusy;

    if (!AtRamCellValueIsValid(self, value))
        return cAtErrorOutOfRangParm;

    /* Write data */
    numDwords = AtRamCellSizeGet(self);
    for (dword_i = 0; dword_i < numDwords; dword_i++)
        AtRamWrite(self, CellWriteDataRegister(self, dword_i), value[dword_i]);

    /* Make request */
    AtRamWrite(self, CellAddressRegister(self), address);
    AtRamWrite(self, AccessCommandRegister(self), cAccessCommandWrite);
    if (HwFinish(self))
        return cAtOk;

    return cAtErrorRamAccessTimeout;
    }

static uint32 AddressBusSizeGet(AtRam self)
    {
    AtUnused(self);
    return 19;
    }

static uint32 DataBusSizeGet(AtRam self)
    {
    AtUnused(self);
    return 36;
    }

static eBool HwTestIsGood(AtRam self)
    {
    uint32 regAddr = StickyRegister(self);
    uint32 regVal  = AtRamRead(self, regAddr);
    AtRamWrite(self, regAddr, cPrbsErrorMask);
    return (regVal & cPrbsErrorMask) ? cAtFalse : cAtTrue;
    }

static eBool IsGood(AtRam self)
    {
    if (AtRamHwTestIsStarted(self))
        return HwTestIsGood(self);
    return m_AtRamMethods->IsGood(self);
    }

static const char *TypeString(AtRam self)
    {
    AtUnused(self);
    return "QDR";
    }

static eAtRet HwCommandFinish(AtRam self)
    {
    static const uint32 cTimeoutMs = 5000;
    tAtOsalCurTime startTime, currentTime;
    uint32 elapseTime = 0;
    uint32 regAddr = AccessCommandRegister(self);

    AtOsalCurTimeGet(&startTime);

    while (elapseTime < cTimeoutMs)
        {
        if ((AtRamRead(self, regAddr) & cAssistCommandRequestMask) == 0)
            return cAtOk;

        if (AtRamIsSimulated(self))
            return cAtOk;

        AtOsalCurTimeGet(&currentTime);
        elapseTime = mTimeIntervalInMsGet(startTime, currentTime);
        }

    return cAtErrorRamAccessTimeout;
    }

static eAtRet AssistCommandApplyWithAddressIncrease(AtRam self, uint8 command, eBool addressIsIncreased)
    {
    uint32 regAddr = TestControlRegister(self);
    uint32 regVal = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cAssistCommand, command);
    mRegFieldSet(regVal, cHwAssistAddressIncrease, addressIsIncreased ? 1 : 0);
    AtRamWrite(self, regAddr, regVal);

    /* Start requesting */
    regAddr = AccessCommandRegister(self);
    regVal  = AtRamRead(self, regAddr);
    mRegFieldSet(regVal, cAssistCommandRequest, 1);
    AtRamWrite(self, regAddr, regVal);

    return HwCommandFinish(self);
    }

static eAtRet HwRequest(AtRam self, eBool addressIsIncreased, uint32 startAddress, uint32 endAddress, uint8 command)
    {
    uint32 start = addressIsIncreased ? startAddress : endAddress;
    uint32 stop  = addressIsIncreased ? endAddress : startAddress;

    StartAddressSet(self, start);
    StopAddressSet(self, stop);

    return AssistCommandApplyWithAddressIncrease(self, command, addressIsIncreased);
    }

static void FillDataSet(AtRam self, uint32 data)
    {
    uint32 dword_i;
    uint32 cellSize = AtRamCellSizeGet(self);

    for (dword_i = 0; dword_i < cellSize; dword_i++)
        AtRamWrite(self, CellWriteDataRegister(self, dword_i), data);
    }

static void ExpectedDataSet(AtRam self, uint32 data)
    {
    uint32 dword_i;
    uint32 cellSize = AtRamCellSizeGet(self);

    for (dword_i = 0; dword_i < cellSize; dword_i++)
        AtRamWrite(self, CellExpectedDataRegister(self, dword_i), data);
    }

static eAtModuleRamRet MemoryFill(AtRam self, eBool addressIsIncreased, uint32 data, uint32 startAddress, uint32 endAddress)
    {
    if (!AtRamTestHwAssistIsEnabled(self))
        return m_AtRamMethods->MemoryFill(self, addressIsIncreased, data, startAddress, endAddress);

    FillDataSet(self, data);

    return HwRequest(self, addressIsIncreased, startAddress, endAddress, cAssistCommandFill);
    }

static uint32 ErrorCounterRead(AtRam self)
    {
    return AtRamRead(self, ErrorCountRegister(self)) & cBit19_0;
    }

static eAtModuleRamRet MemoryReadWriteCheck(AtRam self,
                                            eBool increaseAddress,
                                            uint32 expectedReadData,
                                            uint32 writeData,
                                            uint32 startAddress,
                                            uint32 endAddress,
                                            uint32 *firstErrorAddress,
                                            uint32 *errorCount)
    {
    eAtRet ret;

    if (!AtRamTestHwAssistIsEnabled(self))
        return m_AtRamMethods->MemoryReadWriteCheck(self,
                                                    increaseAddress,
                                                    expectedReadData,
                                                    writeData,
                                                    startAddress,
                                                    endAddress,
                                                    firstErrorAddress,
                                                    errorCount);

    FillDataSet(self, writeData);
    ExpectedDataSet(self, expectedReadData);
    ErrorCounterClear(self);

    ret = HwRequest(self, increaseAddress, startAddress, endAddress, cAssistCommandReadWriteCheck);
    if (ret != cAtOk)
        return ret;

    *firstErrorAddress = AtRamRead(self, FirstErrorAddressRegister(self));
    *errorCount = ErrorCounterRead(self);

    return cAtOk;
    }

static eAtModuleRamRet MemoryReadCheck(AtRam self,
                                       eBool addressIsIncreased,
                                       uint32 expectedData,
                                       uint32 startAddress,
                                       uint32 endAddress,
                                       uint32 *firstErrorAddress,
                                       uint32 *errorCount)
    {
    eAtRet ret;

    if (!AtRamTestHwAssistIsEnabled(self))
        return m_AtRamMethods->MemoryReadCheck(self,
                                               addressIsIncreased,
                                               expectedData,
                                               startAddress,
                                               endAddress,
                                               firstErrorAddress,
                                               errorCount);

    FillDataSet(self, 0);
    ExpectedDataSet(self, expectedData);
    ErrorCounterClear(self);

    ret = HwRequest(self, addressIsIncreased, startAddress, endAddress, cAssistCommandReadCheck);
    if (ret != cAtOk)
        return ret;

    *firstErrorAddress = AtRamRead(self, FirstErrorAddressRegister(self));
    *errorCount = ErrorCounterRead(self);

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011Qdr * object = (tTha60210011Qdr *)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(accessEnabled);
    mEncodeObject(errorGen);
    }

static AtErrorGenerator ErrorGeneratorGet(AtRam self)
    {
    if (!EccCounterIsSupported(self))
        return NULL;

    if (mThis(self)->errorGen == NULL)
        mThis(self)->errorGen = Tha60210011QdrRamEccErrorGeneratorNew((AtModule)AtRamModuleGet(self));

    return mThis(self)->errorGen;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->errorGen);
    mThis(self)->errorGen = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, InitStatusGet);
        mMethodOverride(m_AtRamOverride, HwTestStart);
        mMethodOverride(m_AtRamOverride, HwTestStop);
        mMethodOverride(m_AtRamOverride, HwTestIsStarted);
        mMethodOverride(m_AtRamOverride, HwTestErrorForce);
        mMethodOverride(m_AtRamOverride, HwTestErrorIsForced);
        mMethodOverride(m_AtRamOverride, Debug);
        mMethodOverride(m_AtRamOverride, CounterGet);
        mMethodOverride(m_AtRamOverride, CounterClear);
        mMethodOverride(m_AtRamOverride, CounterIsSupported);
        mMethodOverride(m_AtRamOverride, AccessEnable);
        mMethodOverride(m_AtRamOverride, AccessIsEnabled);
        mMethodOverride(m_AtRamOverride, AddressBusSizeGet);
        mMethodOverride(m_AtRamOverride, CellRead);
        mMethodOverride(m_AtRamOverride, CellWrite);
        mMethodOverride(m_AtRamOverride, CellSizeGet);
        mMethodOverride(m_AtRamOverride, CellSizeInBits);
        mMethodOverride(m_AtRamOverride, IsGood);
        mMethodOverride(m_AtRamOverride, DataBusSizeGet);
        mMethodOverride(m_AtRamOverride, TypeString);
        mMethodOverride(m_AtRamOverride, MemoryFill);
        mMethodOverride(m_AtRamOverride, MemoryReadWriteCheck);
        mMethodOverride(m_AtRamOverride, MemoryReadCheck);
        mMethodOverride(m_AtRamOverride, ErrorGeneratorGet);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtObject(self);
    OverrideAtRam(self);
    }

static void MethodsInit(AtRam self)
    {
    Tha60210011Qdr qdr = (Tha60210011Qdr)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        }

    mMethodsSet(qdr, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Qdr);
    }

AtRam Tha60210011QdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtRamObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtRam Tha60210011QdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011QdrObjectInit(newDdr, ramModule, core, ramId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6021DdrInternal.h
 * 
 * Created Date: Jun 29, 2015
 *
 * Description : RAM testing for 6021xxx
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021DDRINTERNAL_H_
#define _THA6021DDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaDdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021Ddr
    {
    tThaDdr super;
    }tTha6021Ddr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha6021DdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#endif /* _THA6021DDRINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamMapHo.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : HO MAP internal RAM for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/Tha60210011ModuleSdh.h"
#include "../map/Tha60210011ModuleMap.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011InternalRamMapHo
    {
    tThaInternalRam super;
    }tTha60210011InternalRamMapHo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumOc48Slices(AtInternalRam self)
    {
    AtDevice device = AtModuleDeviceGet(AtInternalRamPhyModuleGet(self));
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return Tha60210011ModuleSdhMaxNumHoLines(moduleSdh);
    }

static uint32 SliceIdGet(AtInternalRam self)
    {
    return (AtInternalRamLocalIdGet(self) % NumOc48Slices(self));
    }

static eBool IsReserved(AtInternalRam self)
    {
    return (SliceIdGet(self) < 4) ? cAtFalse : cAtTrue;
    }

static eBool IsMapRam(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    return (AtInternalRamLocalIdGet(internalRam) < NumOc48Slices(internalRam)) ? cAtTrue : cAtFalse;
    }

static uint32 HobaseAddress(ThaInternalRam self)
    {
    return Tha60210011ModuleMapHoBaseAddress((ThaModuleMap)AtInternalRamPhyModuleGet((AtInternalRam)(self)));
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    if (IsMapRam(self))
        return HobaseAddress(self) + 0x4808;
        
    /* Demap ram */
	return HobaseAddress(self) + 0x808;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);

    return cBit0 << (localId & (uint32)(NumOc48Slices(internalRam) - 1));
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset;

    if (AtInternalRamIsReserved((AtInternalRam)self))
        return cInvalidUint32;

    if (IsMapRam(self))
        offset = 0x4000;
    else
        offset = 0x0;

    return (uint32)(HobaseAddress(self) + offset + 0x100 * SliceIdGet((AtInternalRam)self));
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamMapHo);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamMapHoNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

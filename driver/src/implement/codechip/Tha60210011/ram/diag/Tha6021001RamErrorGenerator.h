/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210011QdrErrorGenerator.h
 * 
 * Created Date: Apr 11, 2016
 *
 * Description : Error Generator for QDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011QDRERRORGENERATOR_H_
#define _THA60210011QDRERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60210011QdrRamEccErrorGeneratorNew(AtModule module);
AtErrorGenerator Tha60210011PdaRamEccErrorGeneratorNew(AtModule module);
AtErrorGenerator Tha60210011PdaRamCrcErrorGeneratorNew(AtModule module);
AtErrorGenerator Tha60210011PlaRamCrcErrorGeneratorNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011QDRERRORGENERATOR_H_ */


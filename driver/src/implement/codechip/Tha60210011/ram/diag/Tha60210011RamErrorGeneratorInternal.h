/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210011RamErrorGeneratorInternal.h
 * 
 * Created Date: Apr 11, 2016
 *
 * Description : Ram Error Generator Internal
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011RAMERRORGENERATORINTERNAL_H_
#define _THA60210011RAMERRORGENERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210031/ram/diag/Tha60210031RamErrorGeneratorInternal.h"
#include "Tha6021001RamErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011QdrRamEccErrorGenerator *Tha60210011QdrRamEccErrorGenerator;

typedef struct tTha60210011QdrRamEccErrorGenerator
    {
    tTha60210031RamTxDdrEccErrorGenerator super;
    } tTha60210011QdrRamEccErrorGenerator;

typedef struct tTha60210011PdaRamEccErrorGenerator
    {
    tTha60210031RamTxDdrEccErrorGenerator super;
    } tTha60210011PdaRamEccErrorGenerator;

typedef struct tTha60210011PlaRamCrcErrorGenerator
    {
    tTha60210031RamCrcErrorGenerator super;
    } tTha60210011PlaRamCrcErrorGenerator;

typedef struct tTha60210011PdaRamCrcErrorGenerator
    {
    tTha60210031RamCrcErrorGenerator super;
    } tTha60210011PdaRamCrcErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60210011PlaRamCrcErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011RAMERRORGENERATORINTERNAL_H_ */


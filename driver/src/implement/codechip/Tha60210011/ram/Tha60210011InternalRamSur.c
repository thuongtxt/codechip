/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamSur.c
 *
 * Created Date: Dec 16, 2015
 *
 * Description : PM/FM Internal RAM implementation for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "Tha60210011InternalRam.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPmFmBaseAddress 0x700000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return cPmFmBaseAddress + 0x60;
    }

static ThaModuleHardSur SurModule(ThaInternalRam self)
    {
    return (ThaModuleHardSur)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    ThaModuleHardSur surModule = SurModule(self);
    uint32 cellAddress[] = {0xC0,
                            0xB0,
                            0xa0,
                            0x90,
                            0x80,
                            0xC000,
                            0x8000,
                            0x400,
                            0x4000,
                            0x200,

                            /* Some compiler will complain if assign function return value */
                            cInvalidUint32, /* 10 */
                            cInvalidUint32, /* 11 */

                            0x34000, 0x1B200,
                            0x1A800, 0x180B0,
                            0x28000, 0x1AE00,
                            0x1A200, 0x18090,
                            0x9E000, 0x9B500};
    uint32 localRamId;

    cellAddress[10] = ThaModuleHardSurFmPWInterruptMASKPerTypePerPWBase(surModule);
    cellAddress[11] = ThaModuleHardSurPWFramerInterruptMASKPerPWBase(surModule);

    localRamId = AtInternalRamLocalIdGet((AtInternalRam)self);
    if (localRamId >= mCount(cellAddress))
        return cInvalidUint32;

    return (uint32)(cPmFmBaseAddress + cellAddress[localRamId]);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamSur);
    }

AtInternalRam Tha60210011InternalRamSurObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamSurNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamSurObjectInit(newRam, phyModule, ramId, localId);
    }

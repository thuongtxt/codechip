/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210011QdrInternal.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : QDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011QDRINTERNAL_H_
#define _THA60210011QDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/ram/AtRamInternal.h"
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Qdr * Tha60210011Qdr;

typedef struct tTha60210011QdrMethods
    {
    uint32 (*BaseAddress)(Tha60210011Qdr self);
    }tTha60210011QdrMethods;

typedef struct tTha60210011Qdr
    {
    tAtRam super;
    const tTha60210011QdrMethods *methods;

    /* Private data */
    eBool accessEnabled;
    AtErrorGenerator errorGen;
    }tTha60210011Qdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha60210011QdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011QDRINTERNAL_H_ */


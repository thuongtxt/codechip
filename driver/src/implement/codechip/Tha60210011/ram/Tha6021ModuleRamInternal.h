/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6021ModuleRamInternal.h
 * 
 * Created Date: Jun 29, 2015
 *
 * Description : Module Ram
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021MODULERAMINTERNAL_H_
#define _THA6021MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaModuleRamInternal.h"
#include "Tha6021ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021ModuleRamMethods
    {
    uint32 (*DdrTestControlRegister)(Tha6021ModuleRam self, AtRam ram);
    uint32 (*DdrTestModeRegister)(Tha6021ModuleRam self, AtRam ram);
    uint32 (*DdrTestStatusRegister)(Tha6021ModuleRam self, AtRam ram);
    uint32 (*DdrTestErrorCountersRegister)(Tha6021ModuleRam self, AtRam ram, eTha6021RamCounterType readingMode);
    uint32 (*DdrTestReadCounterRegister)(Tha6021ModuleRam self, AtRam ram, eTha6021RamCounterType readingMode);
    uint32 (*DdrTestWriteCounterRegister)(Tha6021ModuleRam self, AtRam ram, eTha6021RamCounterType readingMode);
    }tTha6021ModuleRamMethods;

typedef struct tTha6021ModuleRam
    {
    tThaModuleRam super;
    const tTha6021ModuleRamMethods *methods;
    }tTha6021ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha6021ModuleRamObjectInit(AtModuleRam self, AtDevice device);
AtModuleRam Tha60210011ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#endif /* _THA6021MODULERAMINTERNAL_H_ */


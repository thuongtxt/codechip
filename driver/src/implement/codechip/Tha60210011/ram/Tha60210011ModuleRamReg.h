/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210011ModuleRamReg.h
 * 
 * Created Date: Nov 27, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULERAMREG_H_
#define _THA60210011MODULERAMREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cHoCdrBaseAddress             0x300000UL
#define cLoCdrBaseAddress             0xC00000UL
#define cLoCdrOffset(slice)             (cLoCdrBaseAddress + ((slice / 4) * 0x100000UL) + ((slice % 4) * 0x40000UL))
#define cAf6Reg_Cdr_LoTimingGen_Force_Pariry_Ctrl_Base                     0x825
#define cAf6Reg_Cdr_LoTimingGen_Check_Pariry_Ctrl_Base                     0x825

#define cAf6_Cdr_LoTimingGen_Pariry_Control_Vt_ParErrDis_Mask              cBit17
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Vt_ParErrDis_Shift             17
                                                                           
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Vt_ParErrFrc_Mask              cBit1
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Vt_ParErrFrc_Shift             1
                                                                           
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Sts_ParErrDis_Mask             cBit16
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Sts_ParErrDis_Shift            16
                                                                           
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Sts_ParErrFrc_Mask             cBit0
#define cAf6_Cdr_LoTimingGen_Pariry_Control_Sts_ParErrFrc_Shift            0

#define cAf6Reg_Cdr_LoTimingGen_Pariry_Sticky_Base                         0x82a
#define cAf6_Cdr_LoTimingGen_Pariry_Sticky_Vt_Mask                         cBit1
#define cAf6_Cdr_LoTimingGen_Pariry_Sticky_Vt_Shift                        1
                                                                           
#define cAf6_Cdr_LoTimingGen_Pariry_Sticky_Sts_Mask                        cBit0
#define cAf6_Cdr_LoTimingGen_Pariry_Sticky_Sts_Shift                       0

#define cAf6Reg_Cdr_LoTimingCtrl_Force_Pariry_Ctrl_Base                    0x2000c
#define cAf6Reg_Cdr_LoTimingCtrl_Check_Pariry_Ctrl_Base                    0x2000c


#define cAf6_Cdr_LoTimingCtrl_Pariry_Control_Engine_ParErrDis_Mask         cBit1
#define cAf6_Cdr_LoTimingCtrl_Pariry_Control_Engine_ParErrDis_Shift        1

#define cAf6_Cdr_LoTimingCtrl_Pariry_Control_Engine_ParErrFrc_Mask         cBit0
#define cAf6_Cdr_LoTimingCtrl_Pariry_Control_Engine_ParErrFrc_Shift        0

#define cAf6Reg_Cdr_LoTimingCtrl_Pariry_Sticky_Base                        0x2000d
#define cAf6_Cdr_LoTimingCtrl_Pariry_Sticky_Engine_Mask                    cBit0
#define cAf6_Cdr_LoTimingCtrl_Pariry_Sticky_Engine_Shift                   0

#define cAf6Reg_Cdr_HoTimingGen_Force_Pariry_Ctrl_Base                         0x189
#define cAf6Reg_Cdr_HoTimingGen_Check_Pariry_Ctrl_Base                         0x189
#define cAf6_Cdr_HoTimingGen_Pariry_Control_Sts_ParErrDis_Mask             cBit16
#define cAf6_Cdr_HoTimingGen_Pariry_Control_Sts_ParErrDis_Shift            16
                                                                           
#define cAf6_Cdr_HoTimingGen_Pariry_Control_Sts_ParErrFrc_Mask             cBit0
#define cAf6_Cdr_HoTimingGen_Pariry_Control_Sts_ParErrFrc_Shift            0

#define cAf6Reg_Cdr_HoTimingGen_Pariry_Sticky_Base           0x188
#define cAf6_Cdr_HoTimingGen_Pariry_Sticky_Sts_Mask          cBit0
#define cAf6_Cdr_HoTimingGen_Pariry_Sticky_Sts_Shift         0

#define cAf6Reg_Cdr_HoTimingCtrl_Force_Pariry_Ctrl_Base                               0x20006
#define cAf6Reg_Cdr_HoTimingCtrl_Check_Pariry_Ctrl_Base                               0x20006
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc7_ParErrDis_Mask            cBit23
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc6_ParErrDis_Mask            cBit22
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc5_ParErrDis_Mask            cBit21
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc4_ParErrDis_Mask            cBit20
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc3_ParErrDis_Mask            cBit19
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc2_ParErrDis_Mask            cBit18
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc1_ParErrDis_Mask            cBit17
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc0_ParErrDis_Mask            cBit16
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc7_ParErrDis_Shift           23
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc6_ParErrDis_Shift           22
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc5_ParErrDis_Shift           21
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc4_ParErrDis_Shift           20
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc3_ParErrDis_Shift           19
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc2_ParErrDis_Shift           18
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc1_ParErrDis_Shift           17
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc0_ParErrDis_Shift           16

#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc7_ParErrFrc_Mask            cBit7
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc6_ParErrFrc_Mask            cBit6
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc5_ParErrFrc_Mask            cBit5
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc4_ParErrFrc_Mask            cBit4
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc3_ParErrFrc_Mask            cBit3
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc2_ParErrFrc_Mask            cBit2
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc1_ParErrFrc_Mask            cBit1
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc0_ParErrFrc_Mask            cBit0
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc7_ParErrFrc_Shift           7
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc6_ParErrFrc_Shift           6
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc5_ParErrFrc_Shift           5
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc4_ParErrFrc_Shift           4
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc3_ParErrFrc_Shift           3
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc2_ParErrFrc_Shift           2
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc1_ParErrFrc_Shift           1
#define cAf6_Cdr_HoTimingCtrl_Pariry_Control_EngineSlc0_ParErrFrc_Shift           0

#define cAf6Reg_Cdr_HoTimingCtrl_Pariry_Sticky_Base                 0x20007
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc7_Mask         cBit7
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc6_Mask         cBit6
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc5_Mask         cBit5
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc4_Mask         cBit4
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc3_Mask         cBit3
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc2_Mask         cBit2
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc1_Mask         cBit1
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc0_Mask         cBit0
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc7_Shift        7
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc6_Shift        6
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc5_Shift        5
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc4_Shift        4
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc3_Shift        3
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc2_Shift        2
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc1_Shift        1
#define cAf6_Cdr_HoTimingCtrl_Pariry_Sticky_EngineSlc0_Shift        0

#define cClaBaseAddress 0x0600000
#define cAf6Reg_Cla_1_Force_Pariry_Ctrl_Base                      0x00010000
#define cAf6Reg_Cla_1_Check_Pariry_Ctrl_Base                      0x00010000
#define cAf6Reg_Cla_2_Force_Pariry_Ctrl_Base                     0x000F0000
#define cAf6Reg_Cla_2_Check_Pariry_Ctrl_Base                     0x000F0000

#define cAf6_Cla_1_Pariry_Control_HcbeHashTabCtrl_ParErrFrc_Mask       cBit31
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp8_ParErrFrc_Mask  cBit30
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp7_ParErrFrc_Mask  cBit29
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp6_ParErrFrc_Mask  cBit28
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp5_ParErrFrc_Mask  cBit27
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp4_ParErrFrc_Mask  cBit26
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp3_ParErrFrc_Mask  cBit25
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp2_ParErrFrc_Mask  cBit24
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp1_ParErrFrc_Mask  cBit23
#define cAf6_Cla_1_Pariry_Control_PerGrpEnCtrl_ParErrFrc_Mask          cBit22
#define cAf6_Cla_1_Pariry_Control_PerPwMefOverMplsCtrl_ParErrFrc_Mask  cBit21
#define cAf6_Cla_1_Pariry_Control_PerPwTypeCtrl_ParErrFrc_Mask         cBit20
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp4_ParErrFrc_Mask  cBit19
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp3_ParErrFrc_Mask  cBit18
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp2_ParErrFrc_Mask  cBit17
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp1_ParErrFrc_Mask  cBit16
#define cAf6_Cla_2_Pariry_Control_PerPwIdToCdrCtrl_ParErrFrc_Mask      cBit1

#define cAf6_Cla_1_Pariry_Control_HcbeHashTabCtrl_ParErrDis_Mask       cBit15
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp8_ParErrDis_Mask  cBit14
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp7_ParErrDis_Mask  cBit13
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp6_ParErrDis_Mask  cBit12
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp5_ParErrDis_Mask  cBit11
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp4_ParErrDis_Mask  cBit10
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp3_ParErrDis_Mask  cBit9
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp2_ParErrDis_Mask  cBit8
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp1_ParErrDis_Mask  cBit7
#define cAf6_Cla_1_Pariry_Control_PerGrpEnCtrl_ParErrDis_Mask          cBit6
#define cAf6_Cla_1_Pariry_Control_PerPwMefOverMplsCtrl_ParErrDis_Mask  cBit5
#define cAf6_Cla_1_Pariry_Control_PerPwTypeCtrl_ParErrDis_Mask         cBit4
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp4_ParErrDis_Mask  cBit3
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp3_ParErrDis_Mask  cBit2
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp2_ParErrDis_Mask  cBit1
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp1_ParErrDis_Mask  cBit0
#define cAf6_Cla_2_Pariry_Control_PerPwIdToCdrCtrl_ParErrDis_Mask      cBit0

#define cAf6_Cla_1_Pariry_Control_HcbeHashTabCtrl_ParErrFrc_Shift       31
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp8_ParErrFrc_Shift  30
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp7_ParErrFrc_Shift  29
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp6_ParErrFrc_Shift  28
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp5_ParErrFrc_Shift  27
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp4_ParErrFrc_Shift  26
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp3_ParErrFrc_Shift  25
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp2_ParErrFrc_Shift  24
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp1_ParErrFrc_Shift  23
#define cAf6_Cla_1_Pariry_Control_PerGrpEnCtrl_ParErrFrc_Shift          22
#define cAf6_Cla_1_Pariry_Control_PerPwMefOverMplsCtrl_ParErrFrc_Shift  21
#define cAf6_Cla_1_Pariry_Control_PerPwTypeCtrl_ParErrFrc_Shift         20
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp4_ParErrFrc_Shift  19
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp3_ParErrFrc_Shift  18
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp2_ParErrFrc_Shift  17
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp1_ParErrFrc_Shift  16
#define cAf6_Cla_2_Pariry_Control_PerPwIdToCdrCtrl_ParErrFrc_Shift      1

#define cAf6_Cla_1_Pariry_Control_HcbeHashTabCtrl_ParErrDis_Shift       15
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp8_ParErrDis_Shift  14
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp7_ParErrDis_Shift  13
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp6_ParErrDis_Shift  12
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp5_ParErrDis_Shift  11
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp4_ParErrDis_Shift  10
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp3_ParErrDis_Shift  9
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp2_ParErrDis_Shift  8
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl2Grp1_ParErrDis_Shift  7
#define cAf6_Cla_1_Pariry_Control_PerGrpEnCtrl_ParErrDis_Shift          6
#define cAf6_Cla_1_Pariry_Control_PerPwMefOverMplsCtrl_ParErrDis_Shift  5
#define cAf6_Cla_1_Pariry_Control_PerPwTypeCtrl_ParErrDis_Shift         4
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp4_ParErrDis_Shift  3
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp3_ParErrDis_Shift  2
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp2_ParErrDis_Shift  1
#define cAf6_Cla_1_Pariry_Control_HcbeLkUpInfoCrl1Grp1_ParErrDis_Shift  0
#define cAf6_Cla_2_Pariry_Control_PerPwIdToCdrCtrl_ParErrDis_Shift      0

#define cAf6Reg_Cla_1_Pariry_Sticky_Base           0x000F8000
#define cAf6Reg_Cla_2_Pariry_Sticky_Base           0x000F8000
#define cAf6_Cla_2_Pariry_Sticky_PerPwIdToCdrCtrl_Mask      cBit16
#define cAf6_Cla_1_Pariry_Sticky_HcbeHashTabCtrl_Mask       cBit15
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp8_Mask  cBit14
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp7_Mask  cBit13
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp6_Mask  cBit12
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp5_Mask  cBit11
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp4_Mask  cBit10
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp3_Mask  cBit9
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp2_Mask  cBit8
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp1_Mask  cBit7
#define cAf6_Cla_1_Pariry_Sticky_PerGrpEnCtrl_Mask          cBit6
#define cAf6_Cla_1_Pariry_Sticky_PerPwMefOverMplsCtrl_Mask  cBit5
#define cAf6_Cla_1_Pariry_Sticky_PerPwTypeCtrl_Mask         cBit4
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp4_Mask  cBit3
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp3_Mask  cBit2
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp2_Mask  cBit1
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp1_Mask  cBit0

#define cAf6_Cla_2_Pariry_Sticky_PerPwIdToCdrCtrl_Shift      16
#define cAf6_Cla_1_Pariry_Sticky_HcbeHashTabCtrl_Shift       15
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp8_Shift  14
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp7_Shift  13
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp6_Shift  12
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp5_Shift  11
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp4_Shift  10
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp3_Shift  9
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp2_Shift  8
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl2Grp1_Shift  7
#define cAf6_Cla_1_Pariry_Sticky_PerGrpEnCtrl_Shift          6
#define cAf6_Cla_1_Pariry_Sticky_PerPwMefOverMplsCtrl_Shift  5
#define cAf6_Cla_1_Pariry_Sticky_PerPwTypeCtrl_Shift         4
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp4_Shift  3
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp3_Shift  2
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp2_Shift  1
#define cAf6_Cla_1_Pariry_Sticky_HcbeLkUpInfoCrl1Grp1_Shift  0

#define cMapBaseAddress 0x0800000
#define cDeMapBaseAddress 0x0800000
#define cMapOffset(slice)             (cMapBaseAddress + (slice) * 0x40000UL)
#define cAf6Reg_Map_Ctrl_Force_Pariry_Ctrl_Base                        0x18202
#define cAf6Reg_Map_Ctrl_Check_Pariry_Ctrl_Base                        0x18202
#define cAf6_Map_Ctrl_Pariry_Control_Line_ParErrDis_Mask            cBit17
#define cAf6_Map_Ctrl_Pariry_Control_Line_ParErrDis_Shift           17
#define cAf6_Map_Ctrl_Pariry_Control_Channel_ParErrDis_Mask         cBit16
#define cAf6_Map_Ctrl_Pariry_Control_Channel_ParErrDis_Shift        16

#define cAf6_Map_Ctrl_Pariry_Control_Line_ParErrFrc_Mask            cBit1
#define cAf6_Map_Ctrl_Pariry_Control_Line_ParErrFrc_Shift           1
#define cAf6_Map_Ctrl_Pariry_Control_Channel_ParErrFrc_Mask         cBit0
#define cAf6_Map_Ctrl_Pariry_Control_Channel_ParErrFrc_Shift        0

#define cAf6Reg_Map_Ctrl_Pariry_Sticky_Base                   0x18201
#define cAf6_Map_Ctrl_Pariry_Sticky_Line_Mask                 cBit1
#define cAf6_Map_Ctrl_Pariry_Sticky_Line_Shift                1
#define cAf6_Map_Ctrl_Pariry_Sticky_Channel_Mask              cBit0
#define cAf6_Map_Ctrl_Pariry_Sticky_Channel_Shift             0

#define cAf6Reg_DeMap_Ctrl_Force_Pariry_Ctrl_Base                        0x14212
#define cAf6Reg_DeMap_Ctrl_Check_Pariry_Ctrl_Base                        0x14212
#define cAf6_DeMap_Ctrl_Pariry_Control_Channel_ParErrDis_Mask         cBit16
#define cAf6_DeMap_Ctrl_Pariry_Control_Channel_ParErrDis_Shift        16

#define cAf6_DeMap_Ctrl_Pariry_Control_Line_ParErrFrc_Mask            cBit1
#define cAf6_DeMap_Ctrl_Pariry_Control_Line_ParErrFrc_Shift           1
#define cAf6_DeMap_Ctrl_Pariry_Control_Channel_ParErrFrc_Mask         cBit0
#define cAf6_DeMap_Ctrl_Pariry_Control_Channel_ParErrFrc_Shift        0

#define cAf6Reg_DeMap_Ctrl_Pariry_Sticky_Base                   0x14211
#define cAf6_DeMap_Ctrl_Pariry_Sticky_Channel_Mask              cBit0
#define cAf6_DeMap_Ctrl_Pariry_Sticky_Channel_Shift             0

#define cOcnBaseAddress 0x0100000
#define cAf6Reg_Ocn_Cfg1_Force_Pariry_Ctrl_Base                           0x00010
#define cAf6Reg_Ocn_Cfg1_Check_Pariry_Ctrl_Base                           0x00010
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine8_ParErrDis_Mask         cBit31
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine8_ParErrDis_Shift        31
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine7_ParErrDis_Mask         cBit30
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine7_ParErrDis_Shift        30
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine6_ParErrDis_Mask         cBit29
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine6_ParErrDis_Shift        29
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine5_ParErrDis_Mask         cBit28
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine5_ParErrDis_Shift        28
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine4_ParErrDis_Mask         cBit27
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine4_ParErrDis_Shift        27
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine3_ParErrDis_Mask         cBit26
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine3_ParErrDis_Shift        26
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine2_ParErrDis_Mask         cBit25
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine2_ParErrDis_Shift        25
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine1_ParErrDis_Mask         cBit24
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine1_ParErrDis_Shift        24

/* */
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll8_ParErrDis_Mask    cBit23
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll8_ParErrDis_Shift   22
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll7_ParErrDis_Mask    cBit22
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll7_ParErrDis_Shift   22
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll6_ParErrDis_Mask    cBit21
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll6_ParErrDis_Shift   21
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll5_ParErrDis_Mask    cBit20
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll5_ParErrDis_Shift   20
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll4_ParErrDis_Mask    cBit19
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll4_ParErrDis_Shift   19
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll3_ParErrDis_Mask    cBit18
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll3_ParErrDis_Shift   18
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll2_ParErrDis_Mask    cBit17
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll2_ParErrDis_Shift   17
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll1_ParErrDis_Mask    cBit16
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll1_ParErrDis_Shift   16

/* */
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine8_ParErrFrc_Mask         cBit15
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine8_ParErrFrc_Shift        15
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine7_ParErrFrc_Mask         cBit14
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine7_ParErrFrc_Shift        14
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine6_ParErrFrc_Mask         cBit13
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine6_ParErrFrc_Shift        13
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine5_ParErrFrc_Mask         cBit12
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine5_ParErrFrc_Shift        12
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine4_ParErrFrc_Mask         cBit11
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine4_ParErrFrc_Shift        11
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine3_ParErrFrc_Mask         cBit10
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine3_ParErrFrc_Shift        10
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine2_ParErrFrc_Mask         cBit9
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine2_ParErrFrc_Shift        9
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine1_ParErrFrc_Mask         cBit8
#define cAf6_Ocn_Cfg1_Pariry_Control_StsPiLine1_ParErrFrc_Shift        8
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll8_ParErrFrc_Mask    cBit7
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll8_ParErrFrc_Shift   7
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll7_ParErrFrc_Mask    cBit6
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll7_ParErrFrc_Shift   6
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll6_ParErrFrc_Mask    cBit5
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll6_ParErrFrc_Shift   5
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll5_ParErrFrc_Mask    cBit4
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll5_ParErrFrc_Shift   4
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll4_ParErrFrc_Mask    cBit3
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll4_ParErrFrc_Shift   3
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll3_ParErrFrc_Mask    cBit2
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll3_ParErrFrc_Shift   2
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll2_ParErrFrc_Mask    cBit1
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll2_ParErrFrc_Shift   1
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll1_ParErrFrc_Mask    cBit0
#define cAf6_Ocn_Cfg1_Pariry_Control_RxBrideAndRoll1_ParErrFrc_Shift   0

/* */
#define cAf6Reg_Ocn_Cfg1_Pariry_Sticky_Base                           0x00018
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine8_Mask                   cBit15
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine8_Shift                  15
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine7_Mask                   cBit14
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine7_Shift                  14
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine6_Mask                   cBit13
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine6_Shift                  13
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine5_Mask                   cBit12
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine5_Shift                  12
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine4_Mask                   cBit11
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine4_Shift                  11
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine3_Mask                   cBit10
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine3_Shift                  10
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine2_Mask                   cBit9
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine2_Shift                  9
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine1_Mask                   cBit8
#define cAf6_Ocn_Cfg1_Pariry_Sticky_StsPiLine1_Shift                  8
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll8_Shift             cBit7
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll8_Mask              7
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll7_Shift             cBit6
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll7_Mask              6
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll6_Shift             cBit5
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll6_Mask              5
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll5_Shift             cBit4
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll5_Mask              4
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll4_Shift             cBit3
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll4_Mask              3
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll3_Shift             cBit2
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll3_Mask              2
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll2_Shift             cBit1
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll2_Mask              1
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll1_Shift             cBit0
#define cAf6_Ocn_Cfg1_Pariry_Sticky_RxBrideAndRoll1_Mask              0

#define cAf6Reg_Ocn_Cfg2_Force_Pariry_Ctrl_Base                        0x00011
#define cAf6Reg_Ocn_Cfg2_Check_Pariry_Ctrl_Base                        0x00011
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc14_ParErrDis_Mask         cBit29
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc14_ParErrDis_Shift        29
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc13_ParErrDis_Mask         cBit28
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc13_ParErrDis_Shift        28
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc12_ParErrDis_Mask         cBit27
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc12_ParErrDis_Shift        27
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc11_ParErrDis_Mask         cBit26
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc11_ParErrDis_Shift        26
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc10_ParErrDis_Mask         cBit25
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc10_ParErrDis_Shift        25
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc9_ParErrDis_Mask          cBit24
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc9_ParErrDis_Shift         24
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc8_ParErrDis_Mask          cBit23
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc8_ParErrDis_Shift         22
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc7_ParErrDis_Mask          cBit22
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc7_ParErrDis_Shift         22
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc6_ParErrDis_Mask          cBit21
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc6_ParErrDis_Shift         21
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc5_ParErrDis_Mask          cBit20
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc5_ParErrDis_Shift         20
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc4_ParErrDis_Mask          cBit19
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc4_ParErrDis_Shift         19
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc3_ParErrDis_Mask          cBit18
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc3_ParErrDis_Shift         18
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc2_ParErrDis_Mask          cBit17
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc2_ParErrDis_Shift         17
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc1_ParErrDis_Mask          cBit16
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc1_ParErrDis_Shift         16


#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc14_ParErrFrc_Mask         cBit13
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc14_ParErrFrc_Shift        13
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc13_ParErrFrc_Mask         cBit12
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc13_ParErrFrc_Shift        12
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc12_ParErrFrc_Mask         cBit11
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc12_ParErrFrc_Shift        11
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc11_ParErrFrc_Mask         cBit10
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc11_ParErrFrc_Shift        10
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc10_ParErrFrc_Mask         cBit9
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc10_ParErrFrc_Shift        9
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc9_ParErrFrc_Mask          cBit8
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc9_ParErrFrc_Shift         8
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc8_ParErrFrc_Mask          cBit7
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc8_ParErrFrc_Shift         7
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc7_ParErrFrc_Mask          cBit6
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc7_ParErrFrc_Shift         6
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc6_ParErrFrc_Mask          cBit5
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc6_ParErrFrc_Shift         5
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc5_ParErrFrc_Mask          cBit4
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc5_ParErrFrc_Shift         4
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc4_ParErrFrc_Mask          cBit3
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc4_ParErrFrc_Shift         3
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc3_ParErrFrc_Mask          cBit2
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc3_ParErrFrc_Shift         2
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc2_ParErrFrc_Mask          cBit1
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc2_ParErrFrc_Shift         1
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc1_ParErrFrc_Mask          cBit0
#define cAf6_Ocn_Cfg2_Pariry_Control_RxSxc1_ParErrFrc_Shift         0



#define cAf6Reg_Ocn_Cfg2_Pariry_Sticky_Base                 0x00019
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc14_Mask            cBit13
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc14_Shift           13
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc13_Mask            cBit12
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc13_Shift           12
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc12_Mask            cBit11
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc12_Shift           11
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc11_Mask            cBit10
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc11_Shift           10
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc10_Mask            cBit9
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc10_Shift           9
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc9_Mask             cBit8
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc9_Shift            8
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc8_Shift            cBit7
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc8_Mask             7
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc7_Shift            cBit6
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc7_Mask             6
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc6_Shift            cBit5
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc6_Mask             5
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc5_Shift            cBit4
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc5_Mask             4
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc4_Shift            cBit3
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc4_Mask             3
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc3_Shift            cBit2
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc3_Mask             2
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc2_Shift            cBit1
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc2_Mask             1
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc1_Shift            cBit0
#define cAf6_Ocn_Cfg2_Pariry_Sticky_RxSxc1_Mask             0


#define cAf6Reg_Ocn_Cfg3_Force_Pariry_Ctrl_Base                      0x00012
#define cAf6Reg_Ocn_Cfg3_Check_Pariry_Ctrl_Base                      0x00012
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine8_ParErrDis_Mask     cBit31
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine8_ParErrDis_Shift    31
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine7_ParErrDis_Mask     cBit30
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine7_ParErrDis_Shift    30
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine6_ParErrDis_Mask     cBit29
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine6_ParErrDis_Shift    29
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine5_ParErrDis_Mask     cBit28
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine5_ParErrDis_Shift    28
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine4_ParErrDis_Mask     cBit27
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine4_ParErrDis_Shift    27
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine3_ParErrDis_Mask     cBit26
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine3_ParErrDis_Shift    26
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine2_ParErrDis_Mask     cBit25
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine2_ParErrDis_Shift    25
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine1_ParErrDis_Mask     cBit24
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine1_ParErrDis_Shift    24

/* */
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine8_ParErrDis_Mask    cBit23
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine8_ParErrDis_Shift   22
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine7_ParErrDis_Mask    cBit22
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine7_ParErrDis_Shift   22
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine6_ParErrDis_Mask    cBit21
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine6_ParErrDis_Shift   21
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine5_ParErrDis_Mask    cBit20
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine5_ParErrDis_Shift   20
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine4_ParErrDis_Mask    cBit19
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine4_ParErrDis_Shift   19
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine3_ParErrDis_Mask    cBit18
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine3_ParErrDis_Shift   18
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine2_ParErrDis_Mask    cBit17
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine2_ParErrDis_Shift   17
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine1_ParErrDis_Mask    cBit16
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine1_ParErrDis_Shift   16

/* */
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine8_ParErrFrc_Mask     cBit15
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine8_ParErrFrc_Shift    15
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine7_ParErrFrc_Mask     cBit14
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine7_ParErrFrc_Shift    14
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine6_ParErrFrc_Mask     cBit13
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine6_ParErrFrc_Shift    13
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine5_ParErrFrc_Mask     cBit12
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine5_ParErrFrc_Shift    12
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine4_ParErrFrc_Mask     cBit11
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine4_ParErrFrc_Shift    11
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine3_ParErrFrc_Mask     cBit10
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine3_ParErrFrc_Shift    10
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine2_ParErrFrc_Mask     cBit9
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine2_ParErrFrc_Shift    9
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine1_ParErrFrc_Mask     cBit8
#define cAf6_Ocn_Cfg3_Pariry_Control_RxHoLine1_ParErrFrc_Shift    8
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine8_ParErrFrc_Mask    cBit7
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine8_ParErrFrc_Shift   7
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine7_ParErrFrc_Mask    cBit6
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine7_ParErrFrc_Shift   6
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine6_ParErrFrc_Mask    cBit5
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine6_ParErrFrc_Shift   5
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine5_ParErrFrc_Mask    cBit4
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine5_ParErrFrc_Shift   4
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine4_ParErrFrc_Mask    cBit3
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine4_ParErrFrc_Shift   3
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine3_ParErrFrc_Mask    cBit2
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine3_ParErrFrc_Shift   2
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine2_ParErrFrc_Mask    cBit1
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine2_ParErrFrc_Shift   1
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine1_ParErrFrc_Mask    cBit0
#define cAf6_Ocn_Cfg3_Pariry_Control_TxSxcLine1_ParErrFrc_Shift   0

/* */
#define cAf6Reg_Ocn_Cfg3_Pariry_Sticky_Base                      0x00019
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine8_Mask               cBit31
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine8_Shift              31
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine7_Mask               cBit30
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine7_Shift              30
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine6_Mask               cBit29
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine6_Shift              29
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine5_Mask               cBit28
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine5_Shift              28
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine4_Mask               cBit27
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine4_Shift              27
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine3_Mask               cBit26
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine3_Shift              26
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine2_Mask               cBit25
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine2_Shift              25
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine1_Mask               cBit24
#define cAf6_Ocn_Cfg3_Pariry_Sticky_RxHoLine1_Shift              24

#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine8_Shift             cBit23
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine8_Mask              22
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine7_Shift             cBit22
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine7_Mask              22
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine6_Shift             cBit21
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine6_Mask              21
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine5_Shift             cBit20
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine5_Mask              20
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine4_Shift             cBit19
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine4_Mask              19
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine3_Shift             cBit18
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine3_Mask              18
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine2_Shift             cBit17
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine2_Mask              17
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine1_Shift             cBit16
#define cAf6_Ocn_Cfg3_Pariry_Sticky_TxSxcLine1_Mask              16

#define cAf6Reg_Ocn_Cfg4_Force_Pariry_Ctrl_Base                      0x00013
#define cAf6Reg_Ocn_Cfg4_Check_Pariry_Ctrl_Base                      0x00013
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice6_ParErrDis_Mask     cBit29
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice6_ParErrDis_Shift    29
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice5_ParErrDis_Mask     cBit28
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice5_ParErrDis_Shift    28
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice4_ParErrDis_Mask     cBit27
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice4_ParErrDis_Shift    27
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice3_ParErrDis_Mask     cBit26
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice3_ParErrDis_Shift    26
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice2_ParErrDis_Mask     cBit25
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice2_ParErrDis_Shift    25
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice1_ParErrDis_Mask     cBit24
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice1_ParErrDis_Shift    24



#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice6_ParErrDis_Mask    cBit21
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice6_ParErrDis_Shift   21
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice5_ParErrDis_Mask    cBit20
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice5_ParErrDis_Shift   20
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice4_ParErrDis_Mask    cBit19
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice4_ParErrDis_Shift   19
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice3_ParErrDis_Mask    cBit18
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice3_ParErrDis_Shift   18
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice2_ParErrDis_Mask    cBit17
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice2_ParErrDis_Shift   17
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice1_ParErrDis_Mask    cBit16
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice1_ParErrDis_Shift   16



#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice6_ParErrFrc_Mask     cBit13
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice6_ParErrFrc_Shift    13
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice5_ParErrFrc_Mask     cBit12
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice5_ParErrFrc_Shift    12
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice4_ParErrFrc_Mask     cBit11
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice4_ParErrFrc_Shift    11
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice3_ParErrFrc_Mask     cBit10
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice3_ParErrFrc_Shift    10
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice2_ParErrFrc_Mask     cBit9
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice2_ParErrFrc_Shift    9
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice1_ParErrFrc_Mask     cBit8
#define cAf6_Ocn_Cfg4_Pariry_Control_VtPiSlice1_ParErrFrc_Shift    8



#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice6_ParErrFrc_Mask    cBit5
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice6_ParErrFrc_Shift   5
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice5_ParErrFrc_Mask    cBit4
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice5_ParErrFrc_Shift   4
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice4_ParErrFrc_Mask    cBit3
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice4_ParErrFrc_Shift   3
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice3_ParErrFrc_Mask    cBit2
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice3_ParErrFrc_Shift   2
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice2_ParErrFrc_Mask    cBit1
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice2_ParErrFrc_Shift   1
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice1_ParErrFrc_Mask    cBit0
#define cAf6_Ocn_Cfg4_Pariry_Control_VtDemapSlice1_ParErrFrc_Shift   0

/* */
#define cAf6Reg_Ocn_Cfg4_Pariry_Sticky_Base                      0x0001a

#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice6_Mask               cBit13
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice6_Shift              13
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice5_Mask               cBit12
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice5_Shift              12
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice4_Mask               cBit11
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice4_Shift              11
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice3_Mask               cBit10
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice3_Shift              10
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice2_Mask               cBit9
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice2_Shift              9
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice1_Mask               cBit8
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtPiSlice1_Shift              8


#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice6_Shift             cBit5
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice6_Mask              5
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice5_Shift             cBit4
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice5_Mask              4
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice4_Shift             cBit3
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice4_Mask              3
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice3_Shift             cBit2
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice3_Mask              2
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice2_Shift             cBit1
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice2_Mask              1
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice1_Shift             cBit0
#define cAf6_Ocn_Cfg4_Pariry_Sticky_VtDemapSlice1_Mask              0

#define cAf6Reg_Ocn_Cfg5_Force_Pariry_Ctrl_Base                      0x00013
#define cAf6Reg_Ocn_Cfg5_Check_Pariry_Ctrl_Base                      0x00013
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice6_ParErrDis_Mask     cBit29
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice6_ParErrDis_Shift    29
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice5_ParErrDis_Mask     cBit28
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice5_ParErrDis_Shift    28
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice4_ParErrDis_Mask     cBit27
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice4_ParErrDis_Shift    27
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice3_ParErrDis_Mask     cBit26
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice3_ParErrDis_Shift    26
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice2_ParErrDis_Mask     cBit25
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice2_ParErrDis_Shift    25
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice1_ParErrDis_Mask     cBit24
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice1_ParErrDis_Shift    24



#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice6_ParErrDis_Mask    cBit21
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice6_ParErrDis_Shift   21
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice5_ParErrDis_Mask    cBit20
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice5_ParErrDis_Shift   20
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice4_ParErrDis_Mask    cBit19
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice4_ParErrDis_Shift   19
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice3_ParErrDis_Mask    cBit18
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice3_ParErrDis_Shift   18
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice2_ParErrDis_Mask    cBit17
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice2_ParErrDis_Shift   17
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice1_ParErrDis_Mask    cBit16
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice1_ParErrDis_Shift   16



#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice6_ParErrFrc_Mask     cBit13
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice6_ParErrFrc_Shift    13
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice5_ParErrFrc_Mask     cBit12
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice5_ParErrFrc_Shift    12
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice4_ParErrFrc_Mask     cBit11
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice4_ParErrFrc_Shift    11
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice3_ParErrFrc_Mask     cBit10
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice3_ParErrFrc_Shift    10
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice2_ParErrFrc_Mask     cBit9
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice2_ParErrFrc_Shift    9
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice1_ParErrFrc_Mask     cBit8
#define cAf6_Ocn_Cfg5_Pariry_Control_VtPgSlice1_ParErrFrc_Shift    8



#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice6_ParErrFrc_Mask    cBit5
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice6_ParErrFrc_Shift   5
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice5_ParErrFrc_Mask    cBit4
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice5_ParErrFrc_Shift   4
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice4_ParErrFrc_Mask    cBit3
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice4_ParErrFrc_Shift   3
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice3_ParErrFrc_Mask    cBit2
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice3_ParErrFrc_Shift   2
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice2_ParErrFrc_Mask    cBit1
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice2_ParErrFrc_Shift   1
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice1_ParErrFrc_Mask    cBit0
#define cAf6_Ocn_Cfg5_Pariry_Control_VtMapSlice1_ParErrFrc_Shift   0

/* */
#define cAf6Reg_Ocn_Cfg5_Pariry_Sticky_Base                      0x0001a

#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice6_Mask               cBit29
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice6_Shift              29
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice5_Mask               cBit28
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice5_Shift              28
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice4_Mask               cBit27
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice4_Shift              27
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice3_Mask               cBit26
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice3_Shift              26
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice2_Mask               cBit25
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice2_Shift              25
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice1_Mask               cBit24
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtPgSlice1_Shift              24


#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice6_Shift             cBit21
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice6_Mask              21
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice5_Shift             cBit20
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice5_Mask              20
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice4_Shift             cBit19
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice4_Mask              19
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice3_Shift             cBit18
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice3_Mask              18
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice2_Shift             cBit17
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice2_Mask              17
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice1_Shift             cBit16
#define cAf6_Ocn_Cfg5_Pariry_Sticky_VtMapSlice1_Mask              16

#define cAf6Reg_Ocn_Cfg6_Force_Pariry_Ctrl_Base                           0x00010
#define cAf6Reg_Ocn_Cfg6_Check_Pariry_Ctrl_Base                           0x00010
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine8_ParErrDis_Mask         cBit31
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine8_ParErrDis_Shift        31
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine7_ParErrDis_Mask         cBit30
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine7_ParErrDis_Shift        30
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine6_ParErrDis_Mask         cBit29
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine6_ParErrDis_Shift        29
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine5_ParErrDis_Mask         cBit28
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine5_ParErrDis_Shift        28
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine4_ParErrDis_Mask         cBit27
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine4_ParErrDis_Shift        27
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine3_ParErrDis_Mask         cBit26
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine3_ParErrDis_Shift        26
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine2_ParErrDis_Mask         cBit25
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine2_ParErrDis_Shift        25
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine1_ParErrDis_Mask         cBit24
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine1_ParErrDis_Shift        24

/* */
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll8_ParErrDis_Mask    cBit23
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll8_ParErrDis_Shift   22
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll7_ParErrDis_Mask    cBit22
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll7_ParErrDis_Shift   22
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll6_ParErrDis_Mask    cBit21
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll6_ParErrDis_Shift   21
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll5_ParErrDis_Mask    cBit20
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll5_ParErrDis_Shift   20
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll4_ParErrDis_Mask    cBit19
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll4_ParErrDis_Shift   19
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll3_ParErrDis_Mask    cBit18
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll3_ParErrDis_Shift   18
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll2_ParErrDis_Mask    cBit17
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll2_ParErrDis_Shift   17
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll1_ParErrDis_Mask    cBit16
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll1_ParErrDis_Shift   16

/* */
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine8_ParErrFrc_Mask         cBit15
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine8_ParErrFrc_Shift        15
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine7_ParErrFrc_Mask         cBit14
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine7_ParErrFrc_Shift        14
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine6_ParErrFrc_Mask         cBit13
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine6_ParErrFrc_Shift        13
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine5_ParErrFrc_Mask         cBit12
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine5_ParErrFrc_Shift        12
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine4_ParErrFrc_Mask         cBit11
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine4_ParErrFrc_Shift        11
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine3_ParErrFrc_Mask         cBit10
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine3_ParErrFrc_Shift        10
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine2_ParErrFrc_Mask         cBit9
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine2_ParErrFrc_Shift        9
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine1_ParErrFrc_Mask         cBit8
#define cAf6_Ocn_Cfg6_Pariry_Control_StsPgLine1_ParErrFrc_Shift        8
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll8_ParErrFrc_Mask    cBit7
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll8_ParErrFrc_Shift   7
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll7_ParErrFrc_Mask    cBit6
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll7_ParErrFrc_Shift   6
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll6_ParErrFrc_Mask    cBit5
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll6_ParErrFrc_Shift   5
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll5_ParErrFrc_Mask    cBit4
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll5_ParErrFrc_Shift   4
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll4_ParErrFrc_Mask    cBit3
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll4_ParErrFrc_Shift   3
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll3_ParErrFrc_Mask    cBit2
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll3_ParErrFrc_Shift   2
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll2_ParErrFrc_Mask    cBit1
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll2_ParErrFrc_Shift   1
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll1_ParErrFrc_Mask    cBit0
#define cAf6_Ocn_Cfg6_Pariry_Control_TxBrideAndRoll1_ParErrFrc_Shift   0

/* */
#define cAf6Reg_Ocn_Cfg6_Pariry_Sticky_Base                           0x00018
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine8_Mask                   cBit31
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine8_Shift                  31
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine7_Mask                   cBit30
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine7_Shift                  30
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine6_Mask                   cBit29
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine6_Shift                  29
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine5_Mask                   cBit28
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine5_Shift                  28
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine4_Mask                   cBit27
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine4_Shift                  27
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine3_Mask                   cBit26
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine3_Shift                  26
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine2_Mask                   cBit25
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine2_Shift                  25
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine1_Mask                   cBit24
#define cAf6_Ocn_Cfg6_Pariry_Sticky_StsPgLine1_Shift                  24
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll8_Shift             cBit23
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll8_Mask              22
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll7_Shift             cBit22
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll7_Mask              22
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll6_Shift             cBit21
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll6_Mask              21
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll5_Shift             cBit20
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll5_Mask              20
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll4_Shift             cBit19
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll4_Mask              19
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll3_Shift             cBit18
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll3_Mask              18
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll2_Shift             cBit17
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll2_Mask              17
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll1_Shift             cBit16
#define cAf6_Ocn_Cfg6_Pariry_Sticky_TxBrideAndRoll1_Mask              16


#define cPdhBaseAddress 0x1000000
#define cPdhOffset(slice)             (cPdhBaseAddress + (slice) * 0x100000)
#define cAf6Reg_Pdh_Cfg1_Force_Pariry_Ctrl_Base                           0x08
#define cAf6Reg_Pdh_Cfg1_Check_Pariry_Ctrl_Base                           0x08
#define cAf6_Pdh_Cfg1_Pariry_Control_VTAsyncMap_ParErrDis_Mask         cBit28
#define cAf6_Pdh_Cfg1_Pariry_Control_VTAsyncMap_ParErrDis_Shift        28
#define cAf6_Pdh_Cfg1_Pariry_Control_StsMapCtrl_ParErrDis_Mask         cBit27
#define cAf6_Pdh_Cfg1_Pariry_Control_StsMapCtrl_ParErrDis_Shift        27
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Trace_ParErrDis_Mask         cBit26
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Trace_ParErrDis_Shift        26
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Ctrl_ParErrDis_Mask         cBit25
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Ctrl_ParErrDis_Shift        25
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM12E12Ctrl_ParErrDis_Mask         cBit24
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM12E12Ctrl_ParErrDis_Shift        24
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1SigCtrl_ParErrDis_Mask         cBit23
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1SigCtrl_ParErrDis_Shift        22
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1FrmCtrl_ParErrDis_Mask         cBit22
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1FrmCtrl_ParErrDis_Shift        22
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDE1FrmCtrl_ParErrDis_Mask         cBit21
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDE1FrmCtrl_ParErrDis_Shift        21
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM12E12Ctrl_ParErrDis_Mask         cBit20
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM12E12Ctrl_ParErrDis_Shift        20
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDS3E3OHCtrl_ParErrDis_Mask        cBit19
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDS3E3OHCtrl_ParErrDis_Shift       19
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM23E23Ctrr_ParErrDis_Mask         cBit18
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM23E23Ctrr_ParErrDis_Shift        18
#define cAf6_Pdh_Cfg1_Pariry_Control_STSVTDeMapCtrl_ParErrDis_Mask       cBit17
#define cAf6_Pdh_Cfg1_Pariry_Control_STSVTDeMapCtrl_ParErrDis_Shift      17
#define cAf6_Pdh_Cfg1_Pariry_Control_MuxCtrl_ParErrDis_Mask              cBit16
#define cAf6_Pdh_Cfg1_Pariry_Control_MuxCtrl_ParErrDis_Shift             16

/* */
#define cAf6_Pdh_Cfg1_Pariry_Control_VTAsyncMap_ParErrFrc_Mask         cBit12
#define cAf6_Pdh_Cfg1_Pariry_Control_VTAsyncMap_ParErrFrc_Shift        12
#define cAf6_Pdh_Cfg1_Pariry_Control_StsMapCtrl_ParErrFrc_Mask         cBit11
#define cAf6_Pdh_Cfg1_Pariry_Control_StsMapCtrl_ParErrFrc_Shift        11
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Trace_ParErrFrc_Mask         cBit10
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Trace_ParErrFrc_Shift        10
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Ctrl_ParErrFrc_Mask         cBit9
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM23E23Ctrl_ParErrFrc_Shift        9
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM12E12Ctrl_ParErrFrc_Mask         cBit8
#define cAf6_Pdh_Cfg1_Pariry_Control_TxM12E12Ctrl_ParErrFrc_Shift        8
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1SigCtrl_ParErrFrc_Mask         cBit7
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1SigCtrl_ParErrFrc_Shift        7
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1FrmCtrl_ParErrFrc_Mask         cBit6
#define cAf6_Pdh_Cfg1_Pariry_Control_TxDE1FrmCtrl_ParErrFrc_Shift        6
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDE1FrmCtrl_ParErrFrc_Mask         cBit5
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDE1FrmCtrl_ParErrFrc_Shift        5
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM12E12Ctrl_ParErrFrc_Mask         cBit4
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM12E12Ctrl_ParErrFrc_Shift        4
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDS3E3OHCtrl_ParErrFrc_Mask         cBit3
#define cAf6_Pdh_Cfg1_Pariry_Control_RxDS3E3OHCtrl_ParErrFrc_Shift        3
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM23E23Ctrr_ParErrFrc_Mask         cBit2
#define cAf6_Pdh_Cfg1_Pariry_Control_RxM23E23Ctrr_ParErrFrc_Shift        2
#define cAf6_Pdh_Cfg1_Pariry_Control_STSVTDeMapCtrl_ParErrFrc_Mask         cBit1
#define cAf6_Pdh_Cfg1_Pariry_Control_STSVTDeMapCtrl_ParErrFrc_Shift        1
#define cAf6_Pdh_Cfg1_Pariry_Control_MuxCtrl_ParErrFrc_Mask         cBit0
#define cAf6_Pdh_Cfg1_Pariry_Control_MuxCtrl_ParErrFrc_Shift        0

/* */
#define cAf6Reg_Pdh_Cfg1_Pariry_Sticky_Base                      0x09
#define cAf6_Pdh_Cfg1_Pariry_Sticky_VTAsyncMap_Mask              cBit12
#define cAf6_Pdh_Cfg1_Pariry_Sticky_VTAsyncMap_Shift             12
#define cAf6_Pdh_Cfg1_Pariry_Sticky_StsMapCtrl_Mask              cBit11
#define cAf6_Pdh_Cfg1_Pariry_Sticky_StsMapCtrl_Shift             11
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxM23E23Trace_Mask           cBit10
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxM23E23Trace_Shift          10
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxM23E23Ctrl_Mask            cBit9
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxM23E23Ctrl_Shift           9
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxM12E12Ctrl_Mask            cBit8
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxM12E12Ctrl_Shift           8
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxDE1SigCtrl_Mask           cBit7
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxDE1SigCtrl_Shift          7
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxDE1FrmCtrl_Mask           cBit6
#define cAf6_Pdh_Cfg1_Pariry_Sticky_TxDE1FrmCtrl_Shift          6
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxDE1FrmCtrl_Mask           cBit5
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxDE1FrmCtrl_Shift            5
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxM12E12Ctrl_Mask           cBit4
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxM12E12Ctrl_Shift            4
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxDS3E3OHCtrl_Mask          cBit3
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxDS3E3OHCtrl_Shift           3
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxM23E23Ctrr_Mask           cBit2
#define cAf6_Pdh_Cfg1_Pariry_Sticky_RxM23E23Ctrr_Shift            2
#define cAf6_Pdh_Cfg1_Pariry_Sticky_STSVTDeMapCtrl_Mask         cBit1
#define cAf6_Pdh_Cfg1_Pariry_Sticky_STSVTDeMapCtrl_Shift          1
#define cAf6_Pdh_Cfg1_Pariry_Sticky_MuxCtrl_Mask                cBit0
#define cAf6_Pdh_Cfg1_Pariry_Sticky_MuxCtrl_Shift                 0


#define cPlaBaseAddress 0x0400000
#define cAf6Reg_Pla_Cfg1_Pariry_Sticky_Base                      0x8800f
#define cAf6_Pla_Cfg1_Pariry_Sticky_CRCCheckP2_Mask              cBit65
#define cAf6_Pla_Cfg1_Pariry_Sticky_CRCCheckP2_Shift             65
#define cAf6_Pla_Cfg1_Pariry_Sticky_CRCCheckP1_Mask              cBit64
#define cAf6_Pla_Cfg1_Pariry_Sticky_CRCCheckP1_Shift             64
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParHSPWP2Ctr_Mask            cBit54
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParHSPWP2Ctr_Shift           54
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParUPSRP2Ctr_Mask            cBit53
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParUPSRP2Ctr_Shift           53
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParOutPWEP2Ctr_Mask          cBit52
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParOutPWEP2Ctr_Shift         52
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParHSPWP1Ctr_Mask            cBit50
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParHSPWP1Ctr_Shift           50
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParUPSRP1Ctr_Mask            cBit49
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParUPSRP1Ctr_Shift           49

#define cAf6_Pla_Cfg1_Pariry_Sticky_ParOutPWEP1Ctr_Mask          cBit48
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParOutPWEP1Ctr_Shift         48
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC96_1_Mask    cBit30
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC96_1_Shift   30
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_1_OC48_1_Mask    cBit29
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_1_OC48_1_Shift   29
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_1_OC48_0_Mask    cBit28
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_1_OC48_0_Shift   28
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC96_0_Mask           cBit26
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC96_0_Shift          26
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_0_OC48_1_Mask    cBit25
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_0_OC48_1_Shift   25
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_0_OC48_0_Mask    cBit24
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC96_0_OC48_0_Shift   24


#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_3_Mask           cBit14
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_3_Shift          14
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_3_OC24_1_Mask    cBit13
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_3_OC24_1_Shift   13
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_3_OC24_0_Mask    cBit12
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_3_OC24_0_Shift   12

#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_2_Mask           cBit10
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_2_Shift          10
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_2_OC24_1_Mask    cBit9
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_2_OC24_1_Shift   9
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_2_OC24_0_Mask    cBit8
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_2_OC24_0_Shift   8


#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_1_Mask           cBit6
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_1_Shift          6
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_1_OC24_1_Mask    cBit5
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_1_OC24_1_Shift   5
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_1_OC24_0_Mask    cBit4
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_1_OC24_0_Shift   4

#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_0_Mask           cBit2
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPWECtrRegOC48_0_Shift          2
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_0_OC24_1_Mask    cBit1
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_0_OC24_1_Shift   1
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_0_OC24_0_Mask    cBit0
#define cAf6_Pla_Cfg1_Pariry_Sticky_ParLOPldCtrRegOC48_0_OC24_0_Shift   0


#define cAf6Reg_Pla_Cfg1_Force_Pariry_Ctrl_Base                      0x8802f
#define cAf6Reg_Pla_Cfg1_Check_Pariry_Ctrl_Base                      0x8802e

#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP2_ParErrDis_Mask              cBit65
#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP2_ParErrDis_Shift             65
#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP1_ParErrDis_Mask              cBit64
#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP1_ParErrDis_Shift             64
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP2Ctr_ParErrDis_Mask            cBit54
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP2Ctr_ParErrDis_Shift           54
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP2Ctr_ParErrDis_Mask            cBit53
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP2Ctr_ParErrDis_Shift           53
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP2Ctr_ParErrDis_Mask          cBit52
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP2Ctr_ParErrDis_Shift         52
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP1Ctr_ParErrDis_Mask            cBit50
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP1Ctr_ParErrDis_Shift           50
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP1Ctr_ParErrDis_Mask            cBit49
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP1Ctr_ParErrDis_Shift           49
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP1Ctr_ParErrDis_Mask          cBit48
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP1Ctr_ParErrDis_Shift         48
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_1_ParErrDis_Mask    cBit30
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_1_ParErrDis_Shift   30
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_1_ParErrDis_Mask    cBit29
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_1_ParErrDis_Shift   29
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_0_ParErrDis_Mask    cBit28
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_0_ParErrDis_Shift   28
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_0_ParErrDis_Mask           cBit26
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_0_ParErrDis_Shift          26
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_1_ParErrDis_Mask    cBit25
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_1_ParErrDis_Shift   25
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_0_ParErrDis_Mask    cBit24
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_0_ParErrDis_Shift   24


#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_3_ParErrDis_Mask           cBit14
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_3_ParErrDis_Shift          14
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_1_ParErrDis_Mask    cBit13
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_1_ParErrDis_Shift   13
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_0_ParErrDis_Mask    cBit12
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_0_ParErrDis_Shift   12

#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_2_ParErrDis_Mask           cBit10
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_2_ParErrDis_Shift          10
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_1_ParErrDis_Mask    cBit9
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_1_ParErrDis_Shift   9
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_0_ParErrDis_Mask    cBit8
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_0_ParErrDis_Shift   8


#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_1_ParErrDis_Mask           cBit6
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_1_ParErrDis_Shift          6
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_1_ParErrDis_Mask    cBit5
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_1_ParErrDis_Shift   5
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_0_ParErrDis_Mask    cBit4
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_0_ParErrDis_Shift   4

#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_0_ParErrDis_Mask           cBit2
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_0_ParErrDis_Shift          2
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_1_ParErrDis_Mask    cBit1
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_1_ParErrDis_Shift   1
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_0_ParErrDis_Mask    cBit0
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_0_ParErrDis_Shift   0



#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP2_ParErrFrc_Mask              cBit65
#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP2_ParErrFrc_Shift             65
#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP1_ParErrFrc_Mask              cBit64
#define cAf6_Pla_Cfg1_Pariry_Control_CRCCheckP1_ParErrFrc_Shift             64
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP2Ctr_ParErrFrc_Mask            cBit54
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP2Ctr_ParErrFrc_Shift           54
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP2Ctr_ParErrFrc_Mask            cBit53
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP2Ctr_ParErrFrc_Shift           53
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP2Ctr_ParErrFrc_Mask          cBit52
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP2Ctr_ParErrFrc_Shift         52
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP1Ctr_ParErrFrc_Mask            cBit50
#define cAf6_Pla_Cfg1_Pariry_Control_ParHSPWP1Ctr_ParErrFrc_Shift           50
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP1Ctr_ParErrFrc_Mask            cBit49
#define cAf6_Pla_Cfg1_Pariry_Control_ParUPSRP1Ctr_ParErrFrc_Shift           49
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP1Ctr_ParErrFrc_Mask          cBit48
#define cAf6_Pla_Cfg1_Pariry_Control_ParOutPWEP1Ctr_ParErrFrc_Shift         48
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_1_ParErrFrc_Mask    cBit30
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_1_ParErrFrc_Shift   30
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_1_ParErrFrc_Mask    cBit29
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_1_ParErrFrc_Shift   29
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_0_ParErrFrc_Mask    cBit28
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_1_OC48_0_ParErrFrc_Shift   28
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_0_ParErrFrc_Mask           cBit26
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC96_0_ParErrFrc_Shift          26
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_1_ParErrFrc_Mask    cBit25
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_1_ParErrFrc_Shift   25
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_0_ParErrFrc_Mask    cBit24
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC96_0_OC48_0_ParErrFrc_Shift   24


#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_3_ParErrFrc_Mask           cBit14
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_3_ParErrFrc_Shift          14
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_1_ParErrFrc_Mask    cBit13
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_1_ParErrFrc_Shift   13
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_0_ParErrFrc_Mask    cBit12
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_3_OC24_0_ParErrFrc_Shift   12

#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_2_ParErrFrc_Mask           cBit10
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_2_ParErrFrc_Shift          10
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_1_ParErrFrc_Mask    cBit9
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_1_ParErrFrc_Shift   9
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_0_ParErrFrc_Mask    cBit8
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_2_OC24_0_ParErrFrc_Shift   8


#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_1_ParErrFrc_Mask           cBit6
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_1_ParErrFrc_Shift          6
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_1_ParErrFrc_Mask    cBit5
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_1_ParErrFrc_Shift   5
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_0_ParErrFrc_Mask    cBit4
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_1_OC24_0_ParErrFrc_Shift   4

#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_0_ParErrFrc_Mask           cBit2
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPWECtrRegOC48_0_ParErrFrc_Shift          2
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_1_ParErrFrc_Mask    cBit1
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_1_ParErrFrc_Shift   1
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_0_ParErrFrc_Mask    cBit0
#define cAf6_Pla_Cfg1_Pariry_Control_ParLOPldCtrRegOC48_0_OC24_0_ParErrFrc_Shift   0

#define cMdlBaseAddress 0x0400000
#define cMdlSliceOffset(slice)             (cMdlBaseAddress +(slice)*0x800UL)
#define cAf6Reg_Mdl_Cfg1_Force_Pariry_Ctrl_Base                      0x0480
#define cAf6Reg_Mdl_Cfg1_Check_Pariry_Ctrl_Base                      0x0480
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_2_ParErrDis_Mask              cBit5
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_2_ParErrDis_Shift             5
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_1_ParErrDis_Mask              cBit4
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_1_ParErrDis_Shift             4
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_confgure_ParErrDis_Mask              cBit3
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_confgure_ParErrDis_Shift             3

#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_2_ParErrFrc_Mask              cBit2
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_2_ParErrFrc_Shift             2
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_1_ParErrFrc_Mask              cBit1
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_buffer_1_ParErrFrc_Shift             1
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_confgure_ParErrFrc_Mask              cBit0
#define cAf6_Mdl_Cfg1_Pariry_Control_mdl_confgure_ParErrFrc_Shift             0

#define cAf6Reg_Mdl_Cfg1_Pariry_Sticky_Base                        0x0481
#define cAf6_Mdl_Cfg1_Pariry_Sticky_mdl_buffer_2_Mask              cBit2
#define cAf6_Mdl_Cfg1_Pariry_Sticky_mdl_buffer_2_Shift             2
#define cAf6_Mdl_Cfg1_Pariry_Sticky_mdl_buffer_1_Mask              cBit1
#define cAf6_Mdl_Cfg1_Pariry_Sticky_mdl_buffer_1_Shift             1
#define cAf6_Mdl_Cfg1_Pariry_Sticky_mdl_confgure_Mask              cBit0
#define cAf6_Mdl_Cfg1_Pariry_Sticky_mdl_confgure_Shift             0
 
 
 #define cPdaBaseAddress 0x0400000
#define cAf6Reg_Pda_Cfg1_Force_Pariry_Ctrl_Base                      0x8
#define cAf6Reg_Pda_Cfg1_Check_Pariry_Ctrl_Base                      0x9
#define cAf6_Pda_Cfg1_Pariry_Control_EccCor_ParErrDis_Shift           6 
#define cAf6_Pda_Cfg1_Pariry_Control_EccErr_ParErrDis_Shift           5 
#define cAf6_Pda_Cfg1_Pariry_Control_CrcErr_ParErrDis_Shift           4 
#define cAf6_Pda_Cfg1_Pariry_Control_LoTdmParErr_ParErrDis_Shift      3 
#define cAf6_Pda_Cfg1_Pariry_Control_TdmLkParErr_ParErrDis_Shift      2 
#define cAf6_Pda_Cfg1_Pariry_Control_JitBufParErr_ParErrDis_Shift     1 
#define cAf6_Pda_Cfg1_Pariry_Control_ReorderParErr_ParErrDis_Shift    0 

#define cAf6_Pda_Cfg1_Pariry_Control_EccCor_ParErrFrc_Shift             6
#define cAf6_Pda_Cfg1_Pariry_Control_EccErr_ParErrFrc_Shift             5
#define cAf6_Pda_Cfg1_Pariry_Control_CrcErr_ParErrFrc_Shift             4
#define cAf6_Pda_Cfg1_Pariry_Control_LoTdmParErr_ParErrFrc_Shift        3
#define cAf6_Pda_Cfg1_Pariry_Control_TdmLkParErr_ParErrFrc_Shift        2
#define cAf6_Pda_Cfg1_Pariry_Control_JitBufParErr_ParErrFrc_Shift       1
#define cAf6_Pda_Cfg1_Pariry_Control_ReorderParErr_ParErrFrc_Shift      0
 
#define cAf6Reg_Pda_Cfg1_Pariry_Sticky_Base                        0x5
#define cAf6_Pda_Cfg1_Pariry_Sticky_EccCor_Shift                  30
#define cAf6_Pda_Cfg1_Pariry_Sticky_EccErr_Shift                  29
#define cAf6_Pda_Cfg1_Pariry_Sticky_CrcErr_Shift                  28
#define cAf6_Pda_Cfg1_Pariry_Sticky_LoTdmParErr_Shift             27
#define cAf6_Pda_Cfg1_Pariry_Sticky_TdmLkParErr_Shift             26
#define cAf6_Pda_Cfg1_Pariry_Sticky_JitBufParErr_Shift            25
#define cAf6_Pda_Cfg1_Pariry_Sticky_ReorderParErr_Shift           24


 

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULERAMREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6021ModuleRam.h
 *
 * Created Date: Jun 29, 2015
 *
 * Description : RAM module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA6021MODULERAM_H_
#define _THA6021MODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha6021RamCounterType
    {
    cTha6021RamCounterTypeReadonly,
    cTha6021RamCounterTypeRead2Clear
    }eTha6021RamCounterType;

typedef struct tTha6021ModuleRam *Tha6021ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha6021DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60210011QdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60210031DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

uint32 Tha6021ModuleRamDdrTestControlRegister(AtRam ram);
uint32 Tha6021ModuleRamDdrTestModeRegister(AtRam ram);
uint32 Tha6021ModuleRamDdrTestStatusRegister(AtRam ram);
uint32 Tha6021ModuleRamDdrTestErrorCountersRegister(AtRam ram, eTha6021RamCounterType readingMode);
uint32 Tha6021ModuleRamDdrTestReadCounterRegister(AtRam ram, eTha6021RamCounterType readingMode);
uint32 Tha6021ModuleRamDdrTestWriteCounterRegister(AtRam ram, eTha6021RamCounterType readingMode);

#endif /* _THA6021MODULERAM_H_ */


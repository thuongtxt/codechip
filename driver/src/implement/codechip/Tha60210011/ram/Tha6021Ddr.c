/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6021Ram.c
 *
 * Created Date: Jun 29, 2015
 *
 * Description : RAM testing for 6021Xxxx
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ram/AtModuleRamInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60210011DeviceReg.h"
#include "Tha6021DdrInternal.h"
#include "Tha6021ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
/* Register status  */
#define cAf6DdrTestInitFailMask  cBit1
#define cAf6DdrTestInitFailShift 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods m_AtRamOverride;

/* Save super implementation */
static const tAtRamMethods *m_AtRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldGetOnCore(AtRam self)
    {
    /* It is recommended that this status is retrieved from Core */
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    uint32 mask;
    eBool isGood;
    static const uint32 cTimeoutMs = 5000;
    AtDevice device = AtModuleDeviceGet((AtModule)AtRamModuleGet(self));

    if (ShouldGetOnCore(self))
        return m_AtRamMethods->InitStatusGet(self);

    mask = (AtRamIdGet(self) == 0) ? cDdr1CalibFailMask : cDdr2CalibFailMask;
    isGood = ThaDeviceStickyIsGood(device, cFpgaStickyRegister, mask, cTimeoutMs);

    return isGood ? cAtOk : cAtErrorDdrCalibFail;
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, m_AtRamMethods, sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, InitStatusGet);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021Ddr);
    }

AtRam Tha6021DdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam Tha6021DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6021DdrObjectInit(newRam, ramModule, core, ramId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamCla.c
 *
 * Created Date: Dec 9, 2015
 *
 * Description : CLA internal RAM implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../cla/Tha60210011ModuleCla.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaInternalRam self)
    {
    return Tha60210011ModuleClaBaseAddress((ThaModuleCla)AtInternalRamPhyModuleGet((AtInternalRam)self));
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return BaseAddress(self) + 0xF8000;
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    /* Classify HBCE Looking Up Information Control 1 */
    if (localId < 4)
        return (uint32)(0x80000 + localId * 0x10000);

    /* Classify Per Pseudowire Type Control,
     * Classify Per Pseudowire MEF Over MPLS Control,
     * Classify Per Group Enable Control */
    if (localId < 7)
        return (uint32)(0x50000 + (localId - 4) * 0x10000);

    /* Classify HBCE Looking Up Information Control 2 */
    if (localId < 15)
        return (uint32)(0x40000 + (localId - 7) * 0x1000);

    /* Classify HBCE Hashing Table Control */
    if (localId == 15)
        return 0x20000;

    /* Classify Per Pseudowire Identification to CDR Control */
    return 0xC0000;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    if (AtInternalRamIsReserved((AtInternalRam)self))
        return cInvalidUint32;

    return (uint32)(BaseAddress(self) + FirstCellOffset(self));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamCla);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

AtInternalRam Tha60210011InternalRamClaObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamClaNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamClaObjectInit(newRam, phyModule, ramId, localId);
    }

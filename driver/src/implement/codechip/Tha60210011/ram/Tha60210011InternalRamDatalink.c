/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamDatalink.c
 *
 * Created Date: Dec 16, 2015
 *
 * Description : Data link Internal RAM implementation for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha60210011ModulePdh.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cDlkBaseAddress 0x380000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011InternalRamDatalink
    {
    tThaInternalRam super;
    }tTha60210011InternalRamDatalink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePdh ModulePdh(AtInternalRam self)
    {
    return (Tha60210011ModulePdh)AtInternalRamPhyModuleGet(self);
    }

static uint32 StartLocalId(AtInternalRam self)
    {
    return Tha60210011ModulePdhNumPdhRams(ModulePdh(self));
    }

static const char** TxRamsDescription(AtInternalRam self)
    {
    return Tha60210011ModulePdhTxPrmMdlRamsDescription(ModulePdh(self), NULL);
    }

static uint32 NumTxRams(AtInternalRam self)
    {
    uint32 numRams;
    Tha60210011ModulePdhTxPrmMdlRamsDescription(ModulePdh(self), &numRams);

    return numRams;
    }

static const char** RxRamsDescription(AtInternalRam self)
    {
    return Tha60210011ModulePdhRxPrmMdlRamsDescription(ModulePdh(self), NULL);
    }

static uint32 NumRxRams(AtInternalRam self)
    {
    uint32 numRams;
    Tha60210011ModulePdhRxPrmMdlRamsDescription(ModulePdh(self), &numRams);

    return numRams;
    }

static const char* Description(AtInternalRam self)
    {
    uint32 desc_i = AtInternalRamLocalIdGet(self) - StartLocalId(self);
    uint32 numTxRams = NumTxRams(self);
    const char** description = TxRamsDescription(self);

    if ((desc_i < numTxRams) && (description))
    	return description[desc_i];
    	
    desc_i = desc_i - numTxRams;
    description = RxRamsDescription(self);

    if ((desc_i < NumRxRams(self)) && description)
        return description[desc_i];

    return "Unknown";
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam) - StartLocalId(internalRam);
    if (localId < NumTxRams(internalRam))
        return cDlkBaseAddress + 0x6C00;

    return cDlkBaseAddress + 0x23600;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam) - StartLocalId(internalRam);
    uint32 numTxRams = NumTxRams(internalRam);

    if (localId < numTxRams)
        return cBit0 << localId;

    return cBit0 << (localId - numTxRams);
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam) - StartLocalId(internalRam);
    uint32 numTxRams = NumTxRams(internalRam);

    /* DLK TX PRM configuration */
    if (localId == 0)
        return 0x8000;

    /* DLK TX PRM LB configuration */
    if (localId == 1)
        return 0xA000;

    /* DLK TX MDL configuration
     * DLK TX MDL buffer 1
     * DLK TX MDL buffer 2 */
    if (localId < numTxRams)
        {
        uint32 addressInSlice = (localId - 2) % 3;
        uint32 slice = (localId - 2) / 3;
        static const uint32 cellAddressofTxMdl[] = {0x460, 0x0, 0x200};

        if (addressInSlice >= mCount(cellAddressofTxMdl))
            return cInvalidUint32;

        return (uint32)(cellAddressofTxMdl[addressInSlice] + slice * 0x800);
        }

    /* DLK RX PRM configuration */
    if (localId == numTxRams)
        return 0x20000;

    /* DLK RX MDL configuration */
    return 0x23000;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    return (uint32)(cDlkBaseAddress + FirstCellOffset(self));
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, Description);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamDatalink);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamDatalinkNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

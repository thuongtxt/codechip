/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011InternalRamCdr.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : CDR internal RAM for product 60210011.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../cdr/Tha60210011ModuleCdr.h"
#include "Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumInternalRamInSlice   3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((Tha60210011InternalRamCdr)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011InternalRamCdrMethods m_methods;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleCdr ModuleCdr(ThaInternalRam self)
    {
    return (Tha60210011ModuleCdr)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static uint32 StartLocalId(ThaInternalRam self)
    {
    return Tha60210011ModuleCdrStartLoRamLocalId(ModuleCdr(self));
    }

static uint32 SliceIdGet(ThaInternalRam self)
    {
    return (AtInternalRamLocalIdGet((AtInternalRam)self) - StartLocalId(self)) / cNumInternalRamInSlice;
    }

static eBool IsReserved(AtInternalRam self)
    {
    return (SliceIdGet((ThaInternalRam)self) < 8) ? cAtFalse : cAtTrue;
    }

static uint32 LocalIdInSlice(ThaInternalRam self)
    {
    return (AtInternalRamLocalIdGet((AtInternalRam)self) - StartLocalId(self)) % cNumInternalRamInSlice;
    }

static uint32 SliceOffset(ThaInternalRam self)
    {
    return SliceIdGet(self) * 0x40000;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    uint32 baseAddress = Tha60210011ModuleCdrLoBaseAddress(ModuleCdr(self)) + SliceOffset(self);

    if (LocalIdInSlice(self) < 2)
        return baseAddress + mMethodsGet(mThis(self))->TimingGenParityForceRegister(mThis(self));

    return baseAddress + mMethodsGet(mThis(self))->ACREngineTimingParityForceRegister(mThis(self));
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localIdInSlice;
    if ((localIdInSlice = LocalIdInSlice(self)) < 2)
        return cBit0 << localIdInSlice;

    return cBit0;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 cellAddressOffsetInSlice[] = {cInvalidUint32, cInvalidUint32, cInvalidUint32};
    uint32 localRamId = AtInternalRamLocalIdGet(internalRam);
    uint32 localIdInSlice;
    Tha60210011ModuleCdr cdrModule = ModuleCdr(self);

    cellAddressOffsetInSlice[0] = Tha60210011ModuleCdrLoCdrStsTimingCtrl(cdrModule);
    cellAddressOffsetInSlice[1] = ThaModuleCdrStmVtTimingControlReg((ThaModuleCdrStm)cdrModule);
    cellAddressOffsetInSlice[2] = ThaModuleCdrEngineTimingCtrlRegister((ThaModuleCdr)cdrModule);

    if (AtInternalRamIsReserved(internalRam))
        return cInvalidUint32;

    if (localRamId < StartLocalId(self))
        return cInvalidUint32;

    localIdInSlice = LocalIdInSlice(self);
    if (localIdInSlice >= mCount(cellAddressOffsetInSlice))
        return cInvalidUint32;

    return (uint32)(Tha60210011ModuleCdrLoBaseAddress(cdrModule) + cellAddressOffsetInSlice[localIdInSlice] + SliceOffset(self));
    }

static uint32 TimingGenParityForceRegister(Tha60210011InternalRamCdr self)
    {
    AtUnused(self);
    return 0x82c;
    }

static uint32 ACREngineTimingParityForceRegister(Tha60210011InternalRamCdr self)
    {
    AtUnused(self);
    return 0x2000c;
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static void MethodsInit(AtInternalRam self)
    {
    Tha60210011InternalRamCdr ram = (Tha60210011InternalRamCdr)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Common */
        mMethodOverride(m_methods, TimingGenParityForceRegister);
        mMethodOverride(m_methods, ACREngineTimingParityForceRegister);
        }

    mMethodsSet(ram, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InternalRamCdr);
    }

AtInternalRam Tha60210011InternalRamCdrObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210011InternalRamCdrNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210011InternalRamCdrObjectInit(newRam, phyModule, ramId, localId);
    }

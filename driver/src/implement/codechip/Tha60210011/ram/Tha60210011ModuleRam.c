/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210011ModuleRam.c
 *
 * Created Date: Dec 9, 2014
 *
 * Description : RAM module of product 60210011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/ram/ThaModuleRam.h"
#include "../man/Tha60210011DeviceReg.h"
#include "Tha6021ModuleRamInternal.h"
#include "Tha60210011ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mParityMask(name) (cAf6_global_parity_interrupt_status_##name##ParIntStatus_Mask)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumDdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 NumQdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 DdrUserClock(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 300000000;
    }

static const char *DdrTypeString(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return "DDR4";
    }

static uint32 DDRUserClockValueStatusRegister(ThaModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    return (0xF00065UL + ddrId);
    }

static uint32 DdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 32;
    }

static eBool NeedClearWholeMemoryBeforeTesting(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ClockDdrSwingRangeInPpm(ThaModuleRam self)
    {
    AtUnused(self);
    return 100;
    }

static uint32 ClockQdrSwingRangeInPpm(ThaModuleRam self)
    {
    AtUnused(self);
    return 100;
    }

static eBool DdrHasSpecificValue(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportInternalRamMonitoring(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x14, 0x38);
    }

static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInternalRamMonitoring(device))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 IntrBaseAddress(void)
    {
    return 0x1E80000;
    }

static uint32 InterruptStatusGet(ThaModuleRam self)
    {
    return mModuleHwRead(self, IntrBaseAddress() + 0x8);
    }

static eBool PhyModuleHasRamInterrupt(ThaModuleRam self, uint32 moduleId, uint32 ramIntr)
    {
    AtUnused(self);
    switch (moduleId)
        {
        case cThaModuleOcn:
            return (ramIntr & mParityMask(Ocn)) ? cAtTrue : cAtFalse;
        case cThaModulePoh:
            return (ramIntr & (mParityMask(Poh) | mParityMask(Ame))) ? cAtTrue : cAtFalse; /* TODO: Do not find AME parity address! */
        case cThaModuleMap:
            return (ramIntr & (mParityMask(MapHo) | mParityMask(Map1) | mParityMask(Map2) | mParityMask(Map3) | mParityMask(Map4) | mParityMask(Map5) | mParityMask(Map6))) ? cAtTrue : cAtFalse;
        case cThaModuleCdr:
            return (ramIntr & (mParityMask(CdrHo) | mParityMask(Cdr1) | mParityMask(Cdr2) | mParityMask(Cdr3) | mParityMask(Cdr4) | mParityMask(Cdr5) | mParityMask(Cdr6))) ? cAtTrue : cAtFalse;
        case cAtModulePdh:
            return (ramIntr & (mParityMask(Pdh1) | mParityMask(Pdh2) | mParityMask(Pdh3) | mParityMask(Pdh4) | mParityMask(Pdh5) | mParityMask(Pdh6) | mParityMask(Prm))) ? cAtTrue : cAtFalse;
        case cThaModulePda:
            return (ramIntr & mParityMask(Pda)) ? cAtTrue : cAtFalse;
        case cThaModulePwe:
            return (ramIntr & (mParityMask(Pwe0) | mParityMask(Pwe1) | mParityMask(Pwe2) | mParityMask(Pwe3) | mParityMask(Pla) | mParityMask(Pmc))) ? cAtTrue : cAtFalse;
        case cThaModuleCla:
            return (ramIntr & mParityMask(Cla)) ? cAtTrue : cAtFalse;
        case cAtModuleSur:
            return (ramIntr & mParityMask(Pmc)) ? cAtTrue : cAtFalse;
        default: return cAtFalse;
        }
    return cAtFalse;
    }

static const char ** ModuleRamsDescription(ThaModuleRam self, uint32 moduleId, uint32 *numRams)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModule module = AtDeviceModuleGet(device, moduleId);

    return AtModuleAllInternalRamsDescription(module, numRams);
    }

static const char ** PhyModuleRamDescription(ThaModuleRam self, uint32 moduleId, uint32 *numRams)
    {
    switch (moduleId)
        {
        case cThaModuleOcn: return ModuleRamsDescription(self, cThaModuleOcn, numRams);
        case cThaModulePoh: return ModuleRamsDescription(self, cThaModulePoh, numRams);
        case cThaModulePda: return ModuleRamsDescription(self, cThaModulePda, numRams);
        case cThaModuleCla: return ModuleRamsDescription(self, cThaModuleCla, numRams);
        case cThaModuleMap: return ModuleRamsDescription(self, cThaModuleMap, numRams);
        case cThaModuleCdr: return ModuleRamsDescription(self, cThaModuleCdr, numRams);
        case cAtModulePdh : return ModuleRamsDescription(self, cAtModulePdh, numRams);
        case cThaModulePwe: return ModuleRamsDescription(self, cThaModulePwe, numRams);
        case cAtModuleSur : return ModuleRamsDescription(self, cAtModuleSur, numRams);
        default:            return NULL;
        }

    return NULL;
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, NumQdrGet);
        mMethodOverride(m_AtModuleRamOverride, DdrTypeString);
        mMethodOverride(m_AtModuleRamOverride, DdrUserClock);
        mMethodOverride(m_AtModuleRamOverride, DdrDataBusSizeGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, DDRUserClockValueStatusRegister);
        mMethodOverride(m_ThaModuleRamOverride, NeedClearWholeMemoryBeforeTesting);
        mMethodOverride(m_ThaModuleRamOverride, ClockDdrSwingRangeInPpm);
        mMethodOverride(m_ThaModuleRamOverride, ClockQdrSwingRangeInPpm);
        mMethodOverride(m_ThaModuleRamOverride, DdrHasSpecificValue);
        mMethodOverride(m_ThaModuleRamOverride, InternalRamMonitoringIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, InterruptStatusGet);
        mMethodOverride(m_ThaModuleRamOverride, PhyModuleHasRamInterrupt);
        mMethodOverride(m_ThaModuleRamOverride, PhyModuleRamDescription);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleRam);
    }

AtModuleRam Tha60210011ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60210011ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleRamObjectInit(newModule, device);
    }

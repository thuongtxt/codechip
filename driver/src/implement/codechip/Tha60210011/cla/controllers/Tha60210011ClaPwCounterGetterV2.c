/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011ClaPwCounterGetterV2.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : CLA counter getter version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mRo(clear) ((clear) ? 0 : 1)
#define cPwRxPktCnt(ro)        (0x1E20000UL + (ro) * 0x2000UL)
#define cPwRxMalformPktCnt(ro) (0x1E50000UL + (ro) * 0x2000UL)
#define cPwRxStrayPktCnt(ro)   (0x1E54000UL + (ro) * 0x2000UL)
#define cPwRxLbitPktCnt(ro)    (0x1E2C000UL + (ro) * 0x2000UL)
#define cPwRxRbitPktCnt(ro)    (0x1E38000UL + (ro) * 0x2000UL)
#define cPwRxMbitOrNbitCnt(ro) (0x1E30000UL + (ro) * 0x2000UL)
#define cPwRxPbitPktCnt(ro)    (0x1E34000UL + (ro) * 0x2000UL)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011ClaPwCounterGetterV2
    {
    tTha60210011ClaPwCounterGetter super;
    }tTha60210011ClaPwCounterGetterV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011ClaPwCounterGetterMethods m_Tha60210011ClaPwCounterGetterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(AtPw pw)
    {
    return AtChannelIdGet((AtChannel)pw);
    }

static uint32 RxPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxMalformedPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxMalformPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxStrayPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxStrayPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxLbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxLbitPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxRbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxRbitPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxMbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw,  cPwRxMbitOrNbitCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxNbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxMbitOrNbitCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxPbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(controller);
    return mChannelHwRead(pw, cPwRxPbitPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static void OverrideTha60210011ClaPwCounterGetter(Tha60210011ClaPwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ClaPwCounterGetterOverride, mMethodsGet(self), sizeof(m_Tha60210011ClaPwCounterGetterOverride));

        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxMalformedPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxStrayPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxLbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxRbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxMbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxNbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxPbitPacketsGet);
        }

    mMethodsSet(self, &m_Tha60210011ClaPwCounterGetterOverride);
    }

static void Override(Tha60210011ClaPwCounterGetter self)
    {
    OverrideTha60210011ClaPwCounterGetter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ClaPwCounterGetterV2);
    }

static Tha60210011ClaPwCounterGetter ObjectInit(Tha60210011ClaPwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ClaPwCounterGetterObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011ClaPwCounterGetter Tha60210011ClaPwCounterGetterV2New(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011ClaPwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGetter);
    }

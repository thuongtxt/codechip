/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210011ClaPwControllerInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : PW controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011CLAPWCONTROLLERINTERNAL_H_
#define _THA60210011CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60150011/cla/Tha60150011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ClaPwCounterGetter * Tha60210011ClaPwCounterGetter;
typedef struct tTha60210011ClaPwController * Tha60210011ClaPwController;

typedef struct tTha60210011ClaPwControllerMethods
    {
    eBool (*SupportFullPwCounters)(Tha60210011ClaPwController self);
    Tha60210011ClaPwCounterGetter (*CounterGetter)(Tha60210011ClaPwController self);
    uint32 (*PwCdrIdGet)(Tha60210011ClaPwController self, AtPw pw);
    }tTha60210011ClaPwControllerMethods;

typedef struct tTha60210011ClaPwController
    {
    tTha60150011ClaPwController super;
    const tTha60210011ClaPwControllerMethods * methods;

    Tha60210011ClaPwCounterGetter counterGetter;
    }tTha60210011ClaPwController;

typedef struct tTha60210011ClaPwCounterGetterMethods
    {
    uint32 (*RxPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxMalformedPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxStrayPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxLbitPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxRbitPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxMbitPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxNbitPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    uint32 (*RxPbitPacketsGet)(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
    }tTha60210011ClaPwCounterGetterMethods;

typedef struct tTha60210011ClaPwCounterGetter
    {
    tAtObject super;
    const tTha60210011ClaPwCounterGetterMethods* methods;
    }tTha60210011ClaPwCounterGetter;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60210011ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);
Tha60210011ClaPwCounterGetter Tha60210011ClaPwCounterGetterObjectInit(Tha60210011ClaPwCounterGetter self);

Tha60210011ClaPwCounterGetter Tha60210011ClaPwCounterGetterNew(void);
Tha60210011ClaPwCounterGetter Tha60210011ClaPwCounterGetterV2New(void);

uint32 ClaPwCounterGetterRxPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxMalformedPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxStrayPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxLbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxRbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxMbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxNbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);
uint32 ClaPwCounterGetterRxPbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011CLAPWCONTROLLERINTERNAL_H_ */


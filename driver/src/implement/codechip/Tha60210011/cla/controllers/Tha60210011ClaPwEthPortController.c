/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011ClaPwEthPortController.c
 *
 * Created Date: May 6, 2015
 *
 * Description : CLA ETH Port controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../Tha60210011ModuleClaReg.h"
#include "../Tha60210011ModuleCla.h"
#include "Tha60210011ClaPwEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;
static tThaClaControllerMethods        m_ThaClaControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ClaModule(ThaClaEthPortController self)
    {
    return (ThaModuleCla)ThaClaControllerModuleGet((ThaClaController)self);
    }

static uint32 CounterOffset(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return Tha60210011ModuleClaPortCounterOffset((Tha60210011ModuleCla)ClaModule(self), port, r2c);
    }

static uint32 IncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_rx_port_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 IncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_byte_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_rx_port_over_size_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_rx_port_under_size_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt0_64_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt65_127_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt128_255_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt256_511_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt512_1023_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt1024_1518_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom1529to2047bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt1519_2047_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 EthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_jumbo_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 UnicastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_Unicast_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 BcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_rx_port_bcast_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 McastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_rx_port_mcast_pkt_cnt_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 PhysicalErrorRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_phy_err_ro_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 PortOffset(AtEthPort port)
    {
    return AtChannelIdGet((AtChannel)port);
    }

static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    uint32 remain = 0;

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mFieldIns(&longRegVal[0], cAf6_cla_glb_psn_RxPsnDAExp_Mask_01, cAf6_cla_glb_psn_RxPsnDAExp_Shift_01, macAddr[5] & cBit1_0);

    longRegVal[1]  = (uint32)(macAddr[1] << 30);
    longRegVal[1] |= (uint32)(macAddr[2] << 22);
    longRegVal[1] |= (uint32)(macAddr[3] << 14);
    longRegVal[1] |= (uint32)(macAddr[4] << 6);
    longRegVal[1] |= (uint32)(macAddr[5] >> 2);

    remain = (uint32)((macAddr[0] << 6) | (macAddr[1] >> 2));
    mFieldIns(&longRegVal[2], cAf6_cla_glb_psn_RxPsnDAExp_Mask_02, cAf6_cla_glb_psn_RxPsnDAExp_Shift_02, remain);

    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    uint32 bitField;

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldGet(longRegVal[2], cAf6_cla_glb_psn_RxPsnDAExp_Mask_02, cAf6_cla_glb_psn_RxPsnDAExp_Shift_02, uint32, &bitField);
    macAddr[0] = (uint8)(bitField >> 6);
    macAddr[1] = (uint8)((bitField << 2) | (longRegVal[1] >> 30));
    macAddr[2] = (uint8)(longRegVal[1] >> 22);
    macAddr[3] = (uint8)(longRegVal[1] >> 14);
    macAddr[4] = (uint8)(longRegVal[1] >> 6);

    mFieldGet(longRegVal[0], cAf6_cla_glb_psn_RxPsnDAExp_Mask_01, cAf6_cla_glb_psn_RxPsnDAExp_Shift_01, uint32, &bitField);
    macAddr[5] = (uint8)((longRegVal[1] << 2) | bitField);

    return cAtOk;
    }

static eAtRet IpV4AddressSet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtRet IpV4AddressGet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtRet IpV6AddressSet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtRet IpV6AddressGet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static uint32 ClaGlbPsnCtrl(ThaClaController self)
    {
    return Tha60210011ClaPwClaGlbPsnCtrl(self);
    }

static eAtRet EthPortMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    AtUnused(self);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_cla_glb_psn_RxMacCheckDis_, (enable) ? 0 : 1);
    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool EthPortMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    AtUnused(self);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (mRegField(longRegVal[0], cAf6_cla_glb_psn_RxMacCheckDis_) == 1) ? cAtFalse : cAtTrue;
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    uint32 regAddr, regVal;
    AtEthPort port = (AtEthPort)channel;
    uint32 portEnMask, portEnShift;
    AtUnused(self);

    portEnMask  = cAf6_cla_per_eth_enb_CLAEthPort1En_Mask << AtChannelIdGet((AtChannel)port);
    portEnShift = cAf6_cla_per_eth_enb_CLAEthPort1En_Shift + AtChannelIdGet((AtChannel)port);

    regAddr = cAf6Reg_cla_per_eth_enb_Base + Tha60210011ModuleClaBaseAddress(ClaModule((ThaClaEthPortController)self));
    regVal = mChannelHwRead(port, regAddr, cThaModuleCla);

    mRegFieldSet(regVal, portEn, enable ? 1 : 0);
    mChannelHwWrite(port, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    uint32 regAddr, regVal;
    AtEthPort port = (AtEthPort)channel;
    uint32 portEnMask, portEnShift;
    AtUnused(self);

    portEnMask  = cAf6_cla_per_eth_enb_CLAEthPort1En_Mask << AtChannelIdGet((AtChannel)port);
    portEnShift = cAf6_cla_per_eth_enb_CLAEthPort1En_Shift + AtChannelIdGet((AtChannel)port);

    regAddr = cAf6Reg_cla_per_eth_enb_Base + Tha60210011ModuleClaBaseAddress(ClaModule((ThaClaEthPortController)self));
    regVal = mChannelHwRead(port, regAddr, cThaModuleCla);

    return (mRegField(regVal, portEn) == 1) ? cAtTrue : cAtFalse;
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        /* Counters */
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncomPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncombyteRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktoversizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktundersizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom1529to2047bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktJumboLenRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, BcastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, McastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UnicastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PhysicalErrorRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV4AddressSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV4AddressGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV6AddressSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV6AddressGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckEnable);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckIsEnabled);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void OverrideThaClaController(ThaClaEthPortController self)
    {
    ThaClaController claController = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(claController), sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, ClaGlbPsnCtrl);
        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        mMethodOverride(m_ThaClaControllerOverride, ChannelIsEnabled);
        }

    mMethodsSet(claController, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    OverrideThaClaController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ClaPwEthPortController);
    }

ThaClaEthPortController Tha60210011ClaPwEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClaPwEthPortControllerV2ObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60210011ClaPwEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ClaPwEthPortControllerObjectInit(newController, cla);
    }

uint32 Tha60210011ClaPwClaGlbPsnCtrl(ThaClaController self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaClaControllerModuleGet(self);
    return cAf6Reg_cla_glb_psn_Base + Tha60210011ModuleClaBaseAddress(claModule);
    }

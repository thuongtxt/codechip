/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011ClaPwController.c
 *
 * Created Date: May 7, 2015
 *
 * Description : 60210011 PW CLA controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../pw/Tha60210011ModulePw.h"
#include "../../man/Tha60210011Device.h"
#include "../hbce/Tha60210011Hbce.h"
#include "../Tha60210011ModuleClaReg.h"
#include "../Tha60210011ModuleCla.h"
#include "Tha60210011ClaPwControllerInternal.h"
#include "../../cdr/Tha60210011ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegCLAHBCEGlbCtrl                   cAf6Reg_cla_hbce_glb_Base + cThaRegClaBaseAddress

#define cThaRegCLAHBCEHashingTabCtrl            cAf6Reg_cla_hbce_hash_table_Base + cThaRegClaBaseAddress

#define cThaClaHbceCodingSelectedModeMask              cBit0
#define cThaClaHbceCodingSelectedModeShift             0

#define cThaClaHbceHashingTabSelectedPageMask          cBit1
#define cThaClaHbceHashingTabSelectedPageShift         1

#define cThaClaHbceTimeoutValMask                      cBit15_2
#define cThaClaHbceTimeoutValShift                     2

#define cThaCLAHbceFlowNumMask                         cAf6_cla_hbce_hash_table_CLAHbceLink_Mask
#define cThaCLAHbceFlowNumShift                        cAf6_cla_hbce_hash_table_CLAHbceLink_Shift
#define cThaCLAHbceMemoryStartPtrMask                  cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Mask
#define cThaCLAHbceMemoryStartPtrShift                 cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Shift

/*--------------------------- Macros -----------------------------------------*/
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)
#define mThis(self) ((Tha60210011ClaPwController)self)
#define mCounterGetter(self) mMethodsGet(mThis(self))->CounterGetter(mThis(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ClaPwControllerMethods m_methods;

/* Override */
static tAtObjectMethods                   m_AtObjectOverride;
static tThaClaControllerMethods           m_ThaClaControllerOverride;
static tThaClaPwControllerMethods         m_ThaClaPwControllerOverride;
static tTha60150011ClaPwControllerMethods m_Tha60150011ClaPwControllerOverride;

/* Save super implementation */
static const tAtObjectMethods             *m_AtObjectMethods           = NULL;
static const tThaClaControllerMethods     *m_ThaClaControllerMethods   = NULL;
static const tThaClaPwControllerMethods   *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineRegAdress(ThaClaPwController, CLAHBCEHashingTabCtrl)
mDefineMaskShift(ThaClaPwController, ClaHbceCodingSelectedMode)
mDefineMaskShift(ThaClaPwController, ClaHbceHashingTabSelectedPage)
mDefineMaskShift(ThaClaPwController, ClaHbceTimeoutVal)

mDefineMaskShift(ThaClaPwController, CLAHbceFlowNum)
mDefineMaskShift(ThaClaPwController, CLAHbceMemoryStartPtr)

static uint8 HbceMaxFlowsPerEntry(ThaClaPwController self)
    {
    AtUnused(self);
    return 12;
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60210011HbceNew(hbceId, (AtModule)mClaModule(self), core);
    }

static uint32 PwMefOverMplsControl(Tha60150011ClaPwController self)
    {
    AtUnused(self);
    return cAf6Reg_cla_per_pw_mefompls_Base + Tha60210011ModuleClaBaseAddress(mClaModule(self));
    }

static eAtRet CdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable)
    {
    uint32 address = cAf6Reg_cla_2_cdr_cfg_Base + Tha60210011ModuleClaBaseAddress(mClaModule(self)) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);
    AtUnused(cdrId);

    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwCdrEn_, (enable) ? 1 : 0);
    mChannelHwWrite(pw, address, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool CdrIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 address = cAf6Reg_cla_2_cdr_cfg_Base + Tha60210011ModuleClaBaseAddress(mClaModule(self)) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    return mBinToBool(mRegField(regVal, cAf6_cla_2_cdr_cfg_PwCdrEn_));
    }

static ThaModuleHardSur SurModule(ThaClaPwController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)mClaModule(self));
    return (ThaModuleHardSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static eBool NeedUseInternalCounter(ThaClaPwController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SupportFullPwCounters(Tha60210011ClaPwController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)mClaModule(self));

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210011DeviceStartVersionSupportFullPwCounters(device))
        return cAtTrue;

    return cAtFalse;
    }

static Tha60210011ClaPwCounterGetter PwCounterGetterCreate(ThaClaPwController self)
    {
    if (mMethodsGet(mThis(self))->SupportFullPwCounters(mThis(self)))
        return Tha60210011ClaPwCounterGetterV2New();

    return Tha60210011ClaPwCounterGetterNew();
    }

static Tha60210011ClaPwCounterGetter CounterGetter(Tha60210011ClaPwController self)
    {
    if (self->counterGetter == NULL)
    	self->counterGetter = PwCounterGetterCreate((ThaClaPwController)self);
    return self->counterGetter;
    }

static uint32 RxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxMalformedPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxMalformedPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxStrayPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxStrayPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxLbitPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxLbitPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxRbitPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxRbitPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxMbitPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxMbitPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxNbitPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxNbitPacketsGet(SurModule(self), pw, clear);
    }

static uint32 RxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (NeedUseInternalCounter(self))
        return ClaPwCounterGetterRxPbitPacketsGet(mCounterGetter(self), self, pw, clear);
    return ThaModuleHardSurPwRxPbitPacketsGet(SurModule(self), pw, clear);
    }

static eAtRet PartPwCdrDisable(ThaClaPwController self, uint32 localPwId, uint8 partId, AtIpCore core)
    {
    AtUnused(self);
    AtUnused(localPwId);
    AtUnused(partId);
    AtUnused(core);
    return cAtOk;
    }

/* In case of lo-line, slice will be 0..11 */
static eAtRet CdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice)
    {
    eBool isLoCircuit = Tha60210011PwCircuitBelongsToLoLine(pw);
    uint32 address = cAf6Reg_cla_2_cdr_cfg_Base + Tha60210011ModuleClaBaseAddress(mClaModule(self)) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);
    uint32 tdmLineId = Tha60210011ModulePwTdmLineIdOfPw(pw, cThaModuleCla);

    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwHoEn_, (isLoCircuit) ? 0 : 1);
    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_, (isLoCircuit) ? slice / 2 : slice);
    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_, (isLoCircuit) ? slice % 2 : 0);
    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwTdmLineId_, tdmLineId);

    mChannelHwWrite(pw, address, regVal, cThaModuleCla);
    return cAtOk;
    }

static uint32 PwCdrIdGet(Tha60210011ClaPwController self, AtPw pw)
    {
    uint32 address = cAf6Reg_cla_2_cdr_cfg_Base + Tha60210011ModuleClaBaseAddress(mClaModule(self)) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal & cBit14_0;
    }

static eAtRet PwLookupRemove(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware)
    {
    ThaHbce hbce;
    ThaPwHeaderController controller;
    eAtRet ret;

    ret = m_ThaClaPwControllerMethods->PwLookupRemove(self, pwAdapter, applyHardware);
    if (ret != cAtOk)
        return ret;

    if (pwAdapter == NULL)
        return cAtOk;

    hbce = ThaClaPwControllerHbceGet(self, pwAdapter);
    if (hbce == NULL)
        return cAtOk;

    controller = ThaPwAdapterBackupHeaderController((ThaPwAdapter)pwAdapter);
    if (ThaPwHeaderControllerHbceMemoryCellContentGet(controller))
        ret |= ThaHbcePwRemove(hbce, controller);

    return ret;
    }

static eAtRet PwLookupEnable(ThaClaPwController self, AtPw pwAdapter, eBool enable)
    {
    ThaPwHeaderController controller;
    eAtRet ret = m_ThaClaPwControllerMethods->PwLookupEnable(self, pwAdapter, enable);

    if (ret != cAtOk)
        return ret;

    if (pwAdapter == NULL)
        return cAtOk;

    /* Do not enable backup PSN if PW does not belong to HS group */
    if ((AtPwHsGroupGet(ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter)) == NULL) && (enable == cAtTrue))
        return cAtOk;

    controller = ThaPwAdapterBackupHeaderController((ThaPwAdapter)pwAdapter);
    if (ThaPwHeaderControllerHbceMemoryCellContentGet(controller))
        {
        ThaHbce hbce = ThaClaPwControllerHbceGet(self, pwAdapter);
        ret |= ThaHbcePwHeaderControllerEnable(hbce, controller, enable);
        }

    return ret;
    }

static uint32 PwDefaultOffset(ThaClaPwController self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw);
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    AtUnused(self);
    ThaPwAdapterHbceEnable((AtPw)channel, enable);
    return cAtOk;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    AtPw pw = (AtPw)channel;

    if (ThaClaPwControllerHbceGet((ThaClaPwController)self, pw) && ThaPwAdapterHbceIsEnabled(pw))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ClaGlbPsnCtrl(ThaClaController self)
    {
    return Tha60210011ClaPwClaGlbPsnCtrl(self);
    }

static eAtRet DefaultSet(ThaClaController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PwLookupModeSet(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    AtUnused(pw);
    AtUnused(self);
    if (lookupMode == cThaPwLookupModePsn)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eThaPwLookupMode PwLookupModeGet(ThaClaPwController self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaPwLookupModePsn;
    }

static void Delete(AtObject self)
    {
    Tha60210011ClaPwController thisController = (Tha60210011ClaPwController)self;
    AtObjectDelete((AtObject)(thisController->counterGetter));
    thisController->counterGetter = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011ClaPwController object = (Tha60210011ClaPwController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(counterGetter);
    }

static eAtRet MefOverMplsEnable(ThaClaController self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet PwSuppressEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    ThaModuleClaPwV2 moduleCla = (ThaModuleClaPwV2)ThaClaControllerModuleGet((ThaClaController)self);
    uint32 regAddr = ThaModuleClaPwV2ClaPwTypeCtrlReg(moduleCla) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_cla_per_pw_ctrl_RxEthSupEn_, mBoolToBin(enable));
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwSuppressIsEnabled(ThaClaPwController self, AtPw pw)
    {
    ThaModuleClaPwV2 moduleCla = (ThaModuleClaPwV2)ThaClaControllerModuleGet((ThaClaController)self);
    uint32 regAddr = ThaModuleClaPwV2ClaPwTypeCtrlReg(moduleCla) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return mRegField(longRegVal[1], cAf6_cla_per_pw_ctrl_RxEthSupEn_) ? cAtTrue : cAtFalse;
    }

static uint16 CdrIdGet(ThaClaPwController self, AtPw pw)
    {
    eBool isLoCircuit = Tha60210011PwCircuitBelongsToLoLine(pw);
    uint32 tdmLineId = Tha60210011ModulePwTdmLineIdOfPw(pw, cThaModuleCla);
    uint8 slice = ThaPwAdapterCdrCircuitSlice(ThaPwAdapterGet(pw), AtPwBoundCircuitGet(pw));
    uint32 cdrId = 0;

    AtUnused(self);
    mRegFieldSet(cdrId, cAf6_cla_2_cdr_cfg_PwHoEn_, (isLoCircuit) ? 0 : 1);
    mRegFieldSet(cdrId, cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_, (isLoCircuit) ? slice / 2 : slice);
    mRegFieldSet(cdrId, cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_, (isLoCircuit) ? slice % 2 : 0);
    mRegFieldSet(cdrId, cAf6_cla_2_cdr_cfg_PwTdmLineId_, tdmLineId);

    return (uint16)cdrId;
    }

static Tha60210011ModuleCdr ModuleCdr(AtPw pw)
    {
    return (Tha60210011ModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModuleCdr);
    }

static void PktAnalyzerCla2CdrDataFlush(ThaClaPwController self, AtPw pw)
    {
    AtUnused(self);
    Tha60210011ModuleCdrPktAnalyzerCla2CdrDataFlush(ModuleCdr(pw));
    }

static eAtRet PktAnalyzerCla2CdrTrigger(ThaClaPwController self, AtPw pw)
    {
    return Tha60210011ModuleCdrPktAnalyzerCla2CdrTrigger(ModuleCdr(pw), CdrIdGet(self, pw));
    }

static eBool PktAnalyzerCla2CdrHoIsEnabled(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    return Tha60210011ModuleCdrPktAnalyzerCla2CdrHoIsEnabled(ModuleCdr(pw), packetId);
    }

static eBool PktAnalyzerCla2CdrPacketIsError(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    return Tha60210011ModuleCdrPktAnalyzerCla2CdrPacketIsError(ModuleCdr(pw), packetId);
    }

static uint32 PktAnalyzerCla2CdrRtpTimeStampGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    return Tha60210011ModuleCdrPktAnalyzerCla2CdrRtpTimeStampGet(ModuleCdr(pw), packetId);
    }

static uint32 PktAnalyzerCla2CdrRtpTimeStampOffset(ThaClaPwController self, AtPw pw, uint16 previousPktId, uint16 nextPktId)
    {
    return ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampGet(self, pw, nextPktId) - ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampGet(self, pw, previousPktId);
    }

static uint32 PktAnalyzerCla2CdrRtpSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    return Tha60210011ModuleCdrPktAnalyzerCla2CdrRtpSequenceNumberGet(ModuleCdr(pw), packetId);
    }

static uint32 PktAnalyzerCla2CdrControlWordSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    return Tha60210011ModuleCdrPktAnalyzerCla2CdrControlWordSequenceNumberGet(ModuleCdr(pw), packetId);
    }

static void OverrideAtObject(ThaClaPwController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        mMethodOverride(m_ThaClaControllerOverride, ChannelIsEnabled);
        mMethodOverride(m_ThaClaControllerOverride, ClaGlbPsnCtrl);
        mMethodOverride(m_ThaClaControllerOverride, DefaultSet);
        mMethodOverride(m_ThaClaControllerOverride, MefOverMplsEnable);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, CdrEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, CLAHBCEHashingTabCtrl);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceMaxFlowsPerEntry);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, RxLbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxRbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMalformedPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxNbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxStrayPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PartPwCdrDisable);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrCircuitSliceSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupRemove);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwDefaultOffset);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwSuppressEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwSuppressIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrDataFlush);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrTrigger);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrHoIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrPacketIsError);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrRtpTimeStampGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrRtpTimeStampOffset);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrRtpSequenceNumberGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PktAnalyzerCla2CdrControlWordSequenceNumberGet);

        /* Bit field override */
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceCodingSelectedMode)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceHashingTabSelectedPage)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceTimeoutVal)

        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceMemoryStartPtr)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceFlowNum)
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void OverrideTha60150011ClaPwController(ThaClaPwController self)
    {
    Tha60150011ClaPwController controller = (Tha60150011ClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ClaPwControllerOverride, mMethodsGet(controller), sizeof(m_Tha60150011ClaPwControllerOverride));

        mMethodOverride(m_Tha60150011ClaPwControllerOverride, PwMefOverMplsControl);
        }

    mMethodsSet(controller, &m_Tha60150011ClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideAtObject(self);
    OverrideThaClaController(self);
    OverrideThaClaPwController(self);
    OverrideTha60150011ClaPwController(self);
    }

static void MethodsInit(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SupportFullPwCounters);
        mMethodOverride(m_methods, CounterGetter);
        mMethodOverride(m_methods, PwCdrIdGet);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ClaPwController);
    }

ThaClaPwController Tha60210011ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60210011ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ClaPwControllerObjectInit(newController, cla);
    }

uint32 Tha60210011ClaPwControllerPwCdrIdGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(mThis(self))->PwCdrIdGet(mThis(self), pw);
    return 0;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210011ClaPwEthPortControllerInternal.h
 * 
 * Created Date: Oct 23, 2015
 *
 * Description : CLA Ethernet port controller of 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011CLAPWETHPORTCONTROLLERINTERNAL_H_
#define _THA60210011CLAPWETHPORTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cla/controllers/ThaClaEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mClaController(self) ((ThaClaController)self)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ClaPwEthPortController
    {
    tThaStmPwProductClaPwEthPortControllerV2 super;
    }tTha60210011ClaPwEthPortController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController Tha60210011ClaPwEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011CLAPWETHPORTCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011ClaPwCounterGetter.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : CLA counter getter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mRo(clear) ((clear) ? 0 : 1)
#define cPwRxPktCnt(ro)        (0x1E20000UL + (ro) * 0x800UL)
#define cPwRxMalformPktCnt(ro) (0x1E2C000UL + (ro) * 0x800UL)
#define cPwRxStrayPktCnt(ro)   (0x1E2D000UL + (ro) * 0x800UL)
#define cPwRxLbitPktCnt(ro)    (0x1E23000UL + (ro) * 0x800UL)
#define cPwRxRbitPktCnt(ro)    (0x1E26000UL + (ro) * 0x800UL)
#define cPwRxMbitOrNbitCnt(ro) (0x1E24000UL + (ro) * 0x800UL)
#define cPwRxPbitPktCnt(ro)    (0x1E25000UL + (ro) * 0x800UL)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ClaPwCounterGetterMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(ThaClaPwController self, AtPw pw)
    {
    ThaModuleClaPw claModule = (ThaModuleClaPw)ThaClaControllerModuleGet((ThaClaController)self);
    return ThaModuleClaPwCounterOffset(claModule, pw);
    }

static uint32 RxPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxPktCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxMalformedPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxMalformPktCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxStrayPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxStrayPktCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxLbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxLbitPktCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxRbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxRbitPktCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxMbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw,  cPwRxMbitOrNbitCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxNbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxMbitOrNbitCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static uint32 RxPbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxPbitPktCnt(mRo(clear)) + CounterOffset(controller, pw), cThaModulePwPmc);
    }

static void MethodsInit(Tha60210011ClaPwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override methods */
        mMethodOverride(m_methods, RxPacketsGet);
        mMethodOverride(m_methods, RxMalformedPacketsGet);
        mMethodOverride(m_methods, RxStrayPacketsGet);
        mMethodOverride(m_methods, RxLbitPacketsGet);
        mMethodOverride(m_methods, RxRbitPacketsGet);
        mMethodOverride(m_methods, RxMbitPacketsGet);
        mMethodOverride(m_methods, RxNbitPacketsGet);
        mMethodOverride(m_methods, RxPbitPacketsGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ClaPwCounterGetter);
    }

Tha60210011ClaPwCounterGetter Tha60210011ClaPwCounterGetterObjectInit(Tha60210011ClaPwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011ClaPwCounterGetter Tha60210011ClaPwCounterGetterNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011ClaPwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ClaPwCounterGetterObjectInit(newGetter);
    }

uint32 ClaPwCounterGetterRxPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxMalformedPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxMalformedPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxStrayPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxStrayPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxLbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxLbitPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxRbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxRbitPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxMbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxMbitPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxNbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxNbitPacketsGet(self, controller, pw, clear);
    return 0;
    }

uint32 ClaPwCounterGetterRxPbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxPbitPacketsGet(self, controller, pw, clear);
    return 0;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210011ModuleCla.h
 * 
 * Created Date: May 12, 2015
 *
 * Description : CLA
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULECLA_H_
#define _THA60210011MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwGroup.h"
#include "../../../default/cla/ThaModuleCla.h"
#include "../../../default/pw/adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegClaBaseAddress                   0x0600000

#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_DwIndex  1
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_DwIndex 1
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Head_DwIndex 1
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Tail_DwIndex 0

#define cThaRegClaHbceLookingUpInformationCtrl  cAf6Reg_cla_hbce_lkup_info_Base + cThaRegClaBaseAddress
#define cThaRegClaHbceLookingUpInformationCtr2  cAf6Reg_cla_hbce_lkup_info_extra_Base + cThaRegClaBaseAddress

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleCla * Tha60210011ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011ModuleClaHsGroupPwAdd(ThaModuleCla self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210011ModuleClaHsGroupPwRemove(ThaModuleCla self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210011ModuleClaHsGroupEnable(ThaModuleCla self, AtPwGroup pwGroup, eBool enable);
eAtRet Tha60210011ModuleClaHsGroupIsEnabled(ThaModuleCla self, AtPwGroup pwGroup);
eAtRet Tha60210011ModuleClaHsGroupLabelSetSelect(ThaModuleCla self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
eAtRet Tha60210011ModuleClaHsGroupSelectedLabelSetGet(ThaModuleCla self, AtPwGroup pwGroup);
uint32 Tha60210011ModuleClaBaseAddress(ThaModuleCla self);
uint32 Tha60210011ClaPwClaGlbPsnCtrl(ThaClaController self);
uint32 Tha60210011ClaPwControllerPwCdrIdGet(ThaClaPwController self, AtPw pw);

uint32 Tha60210011ModuleClaHbceFlowDirectMask(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceFlowDirectShift(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceGroupWorkingMask(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceGroupWorkingShift(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceGroupIdFlowMask1(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceGroupIdFlowShift1(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceGroupIdFlowMask2(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceGroupIdFlowShift2(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceFlowIdMask(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceFlowIdShift(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceFlowEnableMask(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceFlowEnableShift(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceStoreIdMask(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaHbceStoreIdShift(Tha60210011ModuleCla self);
uint32 Tha60210011ModuleClaPortCounterOffset(Tha60210011ModuleCla self, AtEthPort port, eBool r2c);
uint32 Tha60210011ModuleClaHbceFlowIdFromPw(Tha60210011ModuleCla self, ThaPwAdapter pw);

#endif /* _THA60210011MODULECLA_H_ */


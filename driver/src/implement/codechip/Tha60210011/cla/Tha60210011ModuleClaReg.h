/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_CLA_H_
#define _AF6_REG_AF6CCI0011_RD_CLA_H_

/*--------------------------- Define -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Classify Global PSN Control
Reg Addr   : 0x00000000 - 0x00000003
Reg Formula: 0x00000000 +  eth_port
    Where  : 
           + $eth_port(0-3): Ethernet port ID
Reg Desc   : 
This register configures identification per Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_glb_psn_Base                                                                    0x00000000
#define cAf6Reg_cla_glb_psn(ethport)                                                    (0x00000000+(ethport))
#define cAf6Reg_cla_glb_psn_WidthVal                                                                        96
#define cAf6Reg_cla_glb_psn_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: RxPsnDAExp
BitField Type: RW
BitField Desc: Mac Address expected at Rx Ethernet port 1: Enable checking 0:
Disable checking
BitField Bits: [77:30]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnDAExp_Bit_Start                                                               30
#define cAf6_cla_glb_psn_RxPsnDAExp_Bit_End                                                                 77
#define cAf6_cla_glb_psn_RxPsnDAExp_Mask_01                                                          cBit31_30
#define cAf6_cla_glb_psn_RxPsnDAExp_Shift_01                                                                30
#define cAf6_cla_glb_psn_RxPsnDAExp_Mask_02                                                           cBit13_0
#define cAf6_cla_glb_psn_RxPsnDAExp_Shift_02                                                                 0

/*--------------------------------------
BitField Name: RxSendLbit2CdrEn
BitField Type: RW
BitField Desc: Enable to send Lbit to CDR engine 1: Enable to send Lbit 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_Bit_Start                                                         29
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_Bit_End                                                           29
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_Mask                                                          cBit29
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_Shift                                                             29
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_MaxVal                                                           0x1
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_MinVal                                                           0x0
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_RstVal                                                           0x1

/*--------------------------------------
BitField Name: RxPsnMplsOutLabelCheckEn
BitField Type: RW
BitField Desc: Enable to check MPLS outer 1: Enable checking 0: Disable checking
BitField Bits: [28]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_Bit_Start                                                 28
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_Bit_End                                                   28
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_Mask                                                  cBit28
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_Shift                                                     28
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_MaxVal                                                   0x1
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_MinVal                                                   0x0
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_RstVal                                                   0x0

/*--------------------------------------
BitField Name: RxPsnCpuBfdCtlEn
BitField Type: RW
BitField Desc: Enable VCCV BFD control packet sending to CPU for processing 1:
Enable sending 0: Discard
BitField Bits: [27]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_Bit_Start                                                         27
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_Bit_End                                                           27
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_Mask                                                          cBit27
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_Shift                                                             27
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_MaxVal                                                           0x1
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_MinVal                                                           0x0
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxMacCheckDis
BitField Type: RW
BitField Desc: Disable to check MAC address at Ethernet port receive direction
1: Disable checking 0: Enable checking
BitField Bits: [26]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxMacCheckDis_Bit_Start                                                            26
#define cAf6_cla_glb_psn_RxMacCheckDis_Bit_End                                                              26
#define cAf6_cla_glb_psn_RxMacCheckDis_Mask                                                             cBit26
#define cAf6_cla_glb_psn_RxMacCheckDis_Shift                                                                26
#define cAf6_cla_glb_psn_RxMacCheckDis_MaxVal                                                              0x1
#define cAf6_cla_glb_psn_RxMacCheckDis_MinVal                                                              0x0
#define cAf6_cla_glb_psn_RxMacCheckDis_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxPsnCpuIcmpEn
BitField Type: RW
BitField Desc: Enable ICMP control packet sending to CPU for processing 1:
Enable sending 0: Discard
BitField Bits: [25]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_Bit_Start                                                           25
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_Bit_End                                                             25
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_Mask                                                            cBit25
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_Shift                                                               25
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_MaxVal                                                             0x1
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_MinVal                                                             0x0
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxPsnCpuArpEn
BitField Type: RW
BitField Desc: Enable ARP control packet sending to CPU for processing 1: Enable
sending 0: Discard
BitField Bits: [24]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_Bit_Start                                                            24
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_Bit_End                                                              24
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_Mask                                                             cBit24
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_Shift                                                                24
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_MaxVal                                                              0x1
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_MinVal                                                              0x0
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PweLoopClaEn
BitField Type: RW
BitField Desc: Enable Loop back traffic from PW Encapsulation to Classification
1: Enable Loop back mode 0: Normal, not loop back
BitField Bits: [23]
--------------------------------------*/
#define cAf6_cla_glb_psn_PweLoopClaEn_Bit_Start                                                             23
#define cAf6_cla_glb_psn_PweLoopClaEn_Bit_End                                                               23
#define cAf6_cla_glb_psn_PweLoopClaEn_Mask                                                              cBit23
#define cAf6_cla_glb_psn_PweLoopClaEn_Shift                                                                 23
#define cAf6_cla_glb_psn_PweLoopClaEn_MaxVal                                                               0x1
#define cAf6_cla_glb_psn_PweLoopClaEn_MinVal                                                               0x0
#define cAf6_cla_glb_psn_PweLoopClaEn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: RxPsnIpUdpMode
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1:
Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or
Destination) is used to identify pseudowire packet 0: Classify engine will
automatically search for value 0x85E in source or destination UDP port. The
remaining UDP port is used to identify pseudowire packet
BitField Bits: [22]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Bit_Start                                                           22
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Bit_End                                                             22
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Mask                                                            cBit22
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Shift                                                               22
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_MaxVal                                                             0x1
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_MinVal                                                             0x0
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxPsnIpUdpSel
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 using to select Source or
Destination to identify pseudowire packet from Ethernet side. It is not use when
RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify
pseudowire packet 0: Classify engine selects destination UDP port to identify
pseudowire packet
BitField Bits: [21]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Bit_Start                                                            21
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Bit_End                                                              21
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Mask                                                             cBit21
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Shift                                                                21
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_MaxVal                                                              0x1
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_MinVal                                                              0x0
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxPsnIpTtlChkEn
BitField Type: RW
BitField Desc: Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1:
Enable checking 0: Disable checking
BitField Bits: [20]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_Bit_Start                                                          20
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_Bit_End                                                            20
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_Mask                                                           cBit20
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_Shift                                                              20
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_MaxVal                                                            0x1
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_MinVal                                                            0x0
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxPsnMplsOutLabel
BitField Type: RW
BitField Desc: Received 2-label MPLS packet from PSN side will be discarded when
it's outer label is different than RxPsnMplsOutLabel
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_Bit_Start                                                         0
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_Bit_End                                                          19
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_Mask                                                       cBit19_0
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_Shift                                                             0
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_MaxVal                                                      0xfffff
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_MinVal                                                          0x0
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Type Control
Reg Addr   : 0x00050000 - 0x000527FF #The address format for these registers is 0x00050000 + PWID
Reg Formula: 0x00050000 +  PWID
    Where  : 
           + $PWID(0-10239): Pseudowire ID
Reg Desc   : 
This register configures identification types per pseudowire

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_pw_ctrl_Base                                                                0x00050000
#define cAf6Reg_cla_per_pw_ctrl(PWID)                                                      (0x00050000+(PWID))
#define cAf6Reg_cla_per_pw_ctrl_WidthVal                                                                    64
#define cAf6Reg_cla_per_pw_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxEthPwOnflyVC34
BitField Type: RW
BitField Desc: CEP EBM on-fly fractional, length changed
BitField Bits: [60]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_Bit_Start                                                     60
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_Bit_End                                                       60
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_Mask                                                      cBit28
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_Shift                                                         28
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_MaxVal                                                       0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_MinVal                                                       0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_RstVal                                                       0x0

/*--------------------------------------
BitField Name: RxEthPwLen
BitField Type: RW
BitField Desc: length of packet to check malform
BitField Bits: [59:46]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Bit_Start                                                           46
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Bit_End                                                             59
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Mask                                                         cBit27_14
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Shift                                                               14
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_MaxVal                                                             0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_MinVal                                                             0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxEthSupEn
BitField Type: RW
BitField Desc: Suppress enable
fractional
BitField Bits: [45]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Bit_Start                                                         45
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Bit_End                                                           45
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Mask                                                          cBit13
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Shift                                                             13
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_MaxVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_MinVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxEthCepMode
BitField Type: RW
BitField Desc: CEP mode working 0,1: CEP basic
fractional
BitField Bits: [44]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Bit_Start                                                         44
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Bit_End                                                           44
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Mask                                                          cBit12
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Shift                                                             12
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_MaxVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_MinVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxEthRtpSsrcValue
BitField Type: RW
BitField Desc: This value is used to compare with SSRC value in RTP header of
received TDM PW packets
BitField Bits: [43:12]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Bit_Start                                                    12
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Bit_End                                                      43
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Mask_01                                               cBit31_12
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Shift_01                                                     12
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Mask_02                                                cBit11_0
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Shift_02                                                      0

/*--------------------------------------
BitField Name: RxEthRtpPtValue
BitField Type: RW
BitField Desc: This value is used to compare with PT value in RTP header of
received TDM PW packets
BitField Bits: [11:5]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Bit_Start                                                       5
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Bit_End                                                        11
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Mask                                                     cBit11_5
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Shift                                                           5
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_MaxVal                                                       0x7f
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_MinVal                                                        0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxEthRtpEn
BitField Type: RW
BitField Desc: Enable RTP 1: Enable RTP 0: Disable RTP
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Bit_Start                                                            4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Bit_End                                                              4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Mask                                                             cBit4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Shift                                                                4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_MaxVal                                                             0x1
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_MinVal                                                             0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxEthRtpSsrcChkEn
BitField Type: RW
BitField Desc: Enable checking SSRC field of RTP header in received TDM PW
packet 1: Enable checking 0: Disable checking
BitField Bits: [3]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Bit_Start                                                     3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Bit_End                                                       3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Mask                                                      cBit3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Shift                                                         3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_MaxVal                                                      0x1
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_MinVal                                                      0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RxEthRtpPtChkEn
BitField Type: RW
BitField Desc: Enable checking PT field of RTP header in received TDM PW packet
1: Enable checking 0: Disable checking
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Bit_Start                                                       2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Bit_End                                                         2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Mask                                                        cBit2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Shift                                                           2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_MaxVal                                                        0x1
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_MinVal                                                        0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxEthPwType
BitField Type: RW
BitField Desc: this is PW type working 0: CES mode without CAS 1: CES mode with
CAS 2: CEP mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Bit_Start                                                           0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Bit_End                                                             1
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Mask                                                          cBit1_0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Shift                                                               0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_MaxVal                                                            0x3
#define cAf6_cla_per_pw_ctrl_RxEthPwType_MinVal                                                            0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire MEF Over MPLS Control
Reg Addr   : 0x00060000 - 0x000627FF #The address format for these registers is 0x00060000 + PWID
Reg Formula: 0x00060000 +  PWID
    Where  : 
           + $PWID(0-10239): Pseudowire ID
Reg Desc   : 
This register configures identification MEF over MPLS

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_pw_mefompls_Base                                                            0x00060000
#define cAf6Reg_cla_per_pw_mefompls(PWID)                                                  (0x00060000+(PWID))
#define cAf6Reg_cla_per_pw_mefompls_WidthVal                                                                96
#define cAf6Reg_cla_per_pw_mefompls_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSEcidValue
BitField Type: RW
BitField Desc: ECID value used to compare with ECID in Ethernet MEF packet
BitField Bits: [70:51]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_Bit_Start                                            51
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_Bit_End                                              70
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_Mask_01                                       cBit31_19
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_Shift_01                                             19
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_Mask_02                                         cBit6_0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_Shift_02                                              0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSDaValue
BitField Type: RW
BitField Desc: DA value used to compare with DA in Ethernet MEF packet
BitField Bits: [50:3]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_Bit_Start                                               3
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_Bit_End                                                50
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_Mask_01                                          cBit31_3
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_Shift_01                                                3
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_Mask_02                                          cBit18_0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_Shift_02                                                0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSDaCheckEn
BitField Type: RW
BitField Desc: Enable checking DA in Ethernet MEF packet with MPLS PW label 1:
Enable checking 0: Disable checking
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_Bit_Start                                             2
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_Bit_End                                               2
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_Mask                                              cBit2
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_Shift                                                 2
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_MaxVal                                              0x1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_MinVal                                              0x0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_RstVal                                              0x0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSEcidCheckEn
BitField Type: RW
BitField Desc: Enable checking ECID in Ethernet MEF packet with MPLS PW label 1:
Enable checking 0: Disable checking
BitField Bits: [1]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_Bit_Start                                           1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_Bit_End                                             1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_Mask                                            cBit1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_Shift                                               1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_MaxVal                                            0x1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_MinVal                                            0x0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidCheckEn_RstVal                                            0x0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSEn
BitField Type: RW
BitField Desc: Enable MEF over MPLS PW mode 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_Bit_Start                                                    0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_Bit_End                                                      0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_Mask                                                     cBit0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_Shift                                                        0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_MaxVal                                                     0x1
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_MinVal                                                     0x0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEn_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Global Control
Reg Addr   : 0x00010000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure global for HBCE module

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_glb_Base                                                                   0x00010000
#define cAf6Reg_cla_hbce_glb                                                                        0x00010000
#define cAf6Reg_cla_hbce_glb_WidthVal                                                                       32
#define cAf6Reg_cla_hbce_glb_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: CLAHbceTimeoutValue
BitField Type: RW
BitField Desc: this is time out value for HBCE module, packing be discarded
BitField Bits: [15:2]
--------------------------------------*/
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_Bit_Start                                                      2
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_Bit_End                                                       15
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_Mask                                                    cBit15_2
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_Shift                                                          2
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_MaxVal                                                    0x3fff
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_MinVal                                                       0x0
#define cAf6_cla_hbce_glb_CLAHbceTimeoutValue_RstVal                                                    0x00FF

/*--------------------------------------
BitField Name: CLAHbceHashingTableSelectedPage
BitField Type: RW
BitField Desc: this is to select hashing table page to access looking up
information buffer 1: PageID 1 0: PageID 0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_Bit_Start                                          1
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_Bit_End                                            1
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_Mask                                           cBit1
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_Shift                                              1
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_MaxVal                                           0x1
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_MinVal                                           0x0
#define cAf6_cla_hbce_glb_CLAHbceHashingTableSelectedPage_RstVal                                           0x1

/*--------------------------------------
BitField Name: CLAHbceCodingSelectedMode
BitField Type: RW
BitField Desc: this is to select mode to code original information that needed
to lookup 1: Code level 1 (Looking the Classify HBCE Hashing Table Control for
Hashid detail) 0: Non XOR
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_Bit_Start                                                0
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_Bit_End                                                  0
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_Mask                                                 cBit0
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_Shift                                                    0
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_MaxVal                                                 0x1
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_MinVal                                                 0x0
#define cAf6_cla_hbce_glb_CLAHbceCodingSelectedMode_RstVal                                                 0x1


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Hashing Table Control
Reg Addr   : 0x00020000 - 0x00037FFF The address format for these registers is 0x00020000 + HashID
Reg Formula: 0x00020000 + HashID
    Where  : 
           + $HashID(0-16383): HashID
Reg Desc   : 
HBCE module uses 14 bits for tab-index. Tab-index is generated by hashing function applied to the original id. %%
Hashing function applies an XOR function to all bits of tab-index. %%
The indexes to the tab-index are generated by hashing function and therefore collisions may occur. %%
There are maximum fours (4) entries for every hash to identify flow traffic whether match or not. %%
If the collisions are more than fours (4), they are handled by pointer to link another memory. %%
The formula of hash pattern is {label ID(20bits), PSN mode (2bits), eth_port(2bits)}, call HashPattern (24bits)%%
The HashID formula has two case depend on CLAHbceCodingSelectedMode%%
CLAHbceCodingSelectedMode = 1: HashID = HashPattern[13:0] XOR {4'd0,HashPattern[23:14]}, CLAHbceStoreID = HashPattern[23:14]%%
CLAHbceCodingSelectedMode = 0: HashID = HashPattern[13:0], CLAHbceStoreID = HashPattern[23:14]

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_hash_table_Base                                                            0x00020000
#define cAf6Reg_cla_hbce_hash_table(HashID)                                              (0x00020000+(HashID))
#define cAf6Reg_cla_hbce_hash_table_WidthVal                                                                32
#define cAf6Reg_cla_hbce_hash_table_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAHbceLink
BitField Type: RW
BitField Desc: this is pointer to link extra location for conflict hash more
than fours
BitField Bits: [12]
--------------------------------------*/
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Bit_Start                                                      12
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Bit_End                                                        12
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Mask                                                       cBit12
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Shift                                                          12
#define cAf6_cla_hbce_hash_table_CLAHbceLink_MaxVal                                                        0x1
#define cAf6_cla_hbce_hash_table_CLAHbceLink_MinVal                                                        0x0
#define cAf6_cla_hbce_hash_table_CLAHbceLink_RstVal                                                        0x0

/*--------------------------------------
BitField Name: CLAHbceMemoryExtraStartPointer
BitField Type: RW
BitField Desc: this is a MemExtraPtr to read the Classify HBCE Looking Up
Information Extra Control in case the number of collisions are more than 4
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Bit_Start                                       0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Bit_End                                      11
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Mask                                  cBit11_0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Shift                                        0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_MaxVal                                   0xfff
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_MinVal                                     0x0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_RstVal                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control
Reg Addr   : 0x00080000 - 0x00083FFF The address format for these registers is 0x00080000 + CollisHashID*0x10000 + HashID
Reg Formula: 0x00080000 + HashID
    Where  : 
           + $HashID(0-16383): HashID
           + $CollisHashID(0-3): Collision
Reg Desc   : 
This memory contain 4 entries (collisions) to examine one Pseudowire label whether match or not.%%
In general, One hashing(HashID) contain 4 entries (collisions). If the collisions are over 4, it will jump to the another extra memory. %%

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_Base                                                             0x00080000
#define cAf6Reg_cla_hbce_lkup_info(HashID, CollisHashID)                                 (0x00080000+(HashID))
#define cAf6Reg_cla_hbce_lkup_info_WidthVal                                                                 64
#define cAf6Reg_cla_hbce_lkup_info_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [37]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Bit_Start                                                 37
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Bit_End                                                   37
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Mask                                                   cBit5
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Shift                                                      5
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_MaxVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_MinVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_RstVal                                                   0x0

/*--------------------------------------
BitField Name: CLAHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [36]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Bit_Start                                                 36
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Bit_End                                                   36
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Mask                                                   cBit4
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Shift                                                      4
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_MaxVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_MinVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_RstVal                                                   0x0

/*--------------------------------------
BitField Name: CLAHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [35:24]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Bit_Start                                                  24
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Bit_End                                                    35
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_01                                             cBit31_24
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_01                                                   24
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_02                                               cBit3_0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_02                                                    0

/*--------------------------------------
BitField Name: CLAHbceFlowID
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [23:11]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Bit_Start                                                     11
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Bit_End                                                       23
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask                                                   cBit23_11
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift                                                         11
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_MaxVal                                                    0x1fff
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_MinVal                                                       0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_RstVal                                                       0x0

/*--------------------------------------
BitField Name: CLAHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Bit_Start                                                    10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Bit_End                                                      10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask                                                     cBit10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift                                                        10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_MaxVal                                                      0x1
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_MinVal                                                      0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_RstVal                                                      0x0

/*--------------------------------------
BitField Name: CLAHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Bit_Start                                                     0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Bit_End                                                       9
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask                                                    cBit9_0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift                                                         0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_MaxVal                                                    0x3ff
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_MinVal                                                      0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Group Enable Control
Reg Addr   : 0x00070000 - 0x00071FFF
Reg Formula: 0x00070000 + Working_ID*0x1000 + Grp_ID
    Where  : 
           + $Working_ID(0-1): CLAHbceGrpWorking
           + $Grp_ID(0-4095): CLAHbceGrpIDFlow
Reg Desc   : 
This register configures Group that Flowid (or Pseudowire ID) is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_grp_enb_Base                                                                0x00070000
#define cAf6Reg_cla_per_grp_enb(WorkingID, GrpID)                      (0x00070000+(WorkingID)*0x1000+(GrpID))
#define cAf6Reg_cla_per_grp_enb_WidthVal                                                                    32
#define cAf6Reg_cla_per_grp_enb_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAGrpPWEn
BitField Type: RW
BitField Desc: This indicate the FlowID (or Pseudowire ID) is enable or not 1:
FlowID enable 0: FlowID disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Bit_Start                                                            0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Bit_End                                                              0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Mask                                                             cBit0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Shift                                                                0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_MaxVal                                                             0x1
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_MinVal                                                             0x0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Ethernet port Enable Control
Reg Addr   : 0x000D0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures specific Ethernet port is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_eth_enb_Base                                                                0x000D0000
#define cAf6Reg_cla_per_eth_enb                                                                     0x000D0000
#define cAf6Reg_cla_per_eth_enb_WidthVal                                                                    32
#define cAf6Reg_cla_per_eth_enb_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthPort4En
BitField Type: RW
BitField Desc: This indicate Ethernet port 4 is enable or not 1: enable 0:
disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort4En_Bit_Start                                                         3
#define cAf6_cla_per_eth_enb_CLAEthPort4En_Bit_End                                                           3
#define cAf6_cla_per_eth_enb_CLAEthPort4En_Mask                                                          cBit3
#define cAf6_cla_per_eth_enb_CLAEthPort4En_Shift                                                             3
#define cAf6_cla_per_eth_enb_CLAEthPort4En_MaxVal                                                          0x1
#define cAf6_cla_per_eth_enb_CLAEthPort4En_MinVal                                                          0x0
#define cAf6_cla_per_eth_enb_CLAEthPort4En_RstVal                                                          0x0

/*--------------------------------------
BitField Name: CLAEthPort3En
BitField Type: RW
BitField Desc: This indicate Ethernet port 3 is enable or not 1: enable 0:
disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort3En_Bit_Start                                                         2
#define cAf6_cla_per_eth_enb_CLAEthPort3En_Bit_End                                                           2
#define cAf6_cla_per_eth_enb_CLAEthPort3En_Mask                                                          cBit2
#define cAf6_cla_per_eth_enb_CLAEthPort3En_Shift                                                             2
#define cAf6_cla_per_eth_enb_CLAEthPort3En_MaxVal                                                          0x1
#define cAf6_cla_per_eth_enb_CLAEthPort3En_MinVal                                                          0x0
#define cAf6_cla_per_eth_enb_CLAEthPort3En_RstVal                                                          0x0

/*--------------------------------------
BitField Name: CLAEthPort2En
BitField Type: RW
BitField Desc: This indicate Ethernet port 2 is enable or not 1: enable 0:
disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort2En_Bit_Start                                                         1
#define cAf6_cla_per_eth_enb_CLAEthPort2En_Bit_End                                                           1
#define cAf6_cla_per_eth_enb_CLAEthPort2En_Mask                                                          cBit1
#define cAf6_cla_per_eth_enb_CLAEthPort2En_Shift                                                             1
#define cAf6_cla_per_eth_enb_CLAEthPort2En_MaxVal                                                          0x1
#define cAf6_cla_per_eth_enb_CLAEthPort2En_MinVal                                                          0x0
#define cAf6_cla_per_eth_enb_CLAEthPort2En_RstVal                                                          0x0

/*--------------------------------------
BitField Name: CLAEthPort1En
BitField Type: RW
BitField Desc: This indicate Ethernet port 1 is enable or not 1: enable 0:
disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Bit_Start                                                         0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Bit_End                                                           0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Mask                                                          cBit0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Shift                                                             0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_MaxVal                                                          0x1
#define cAf6_cla_per_eth_enb_CLAEthPort1En_MinVal                                                          0x0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count0_64 bytes packet
Reg Addr   : 0x000D1000(RO)
Reg Formula: 0x000D1000 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_ro_Base                                                                 0x000D1000
#define cAf6Reg_Eth_cnt0_64_ro(ethport)                                                 (0x000D1000+(ethport))
#define cAf6Reg_Eth_cnt0_64_ro_WidthVal                                                                     32
#define cAf6Reg_Eth_cnt0_64_ro_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Shift                                                              0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count0_64 bytes packet
Reg Addr   : 0x000D1800(RC)
Reg Formula: 0x000D1800 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_rc_Base                                                                 0x000D1800
#define cAf6Reg_Eth_cnt0_64_rc(ethport)                                                 (0x000D1800+(ethport))
#define cAf6Reg_Eth_cnt0_64_rc_WidthVal                                                                     32
#define cAf6Reg_Eth_cnt0_64_rc_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Shift                                                              0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count65_127 bytes packet
Reg Addr   : 0x000D1004(RO)
Reg Formula: 0x000D1004 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_ro_Base                                                               0x000D1004
#define cAf6Reg_Eth_cnt65_127_ro(ethport)                                               (0x000D1004+(ethport))
#define cAf6Reg_Eth_cnt65_127_ro_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt65_127_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Shift                                                          0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count65_127 bytes packet
Reg Addr   : 0x000D1804(RC)
Reg Formula: 0x000D1804 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_rc_Base                                                               0x000D1804
#define cAf6Reg_Eth_cnt65_127_rc(ethport)                                               (0x000D1804+(ethport))
#define cAf6Reg_Eth_cnt65_127_rc_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt65_127_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Shift                                                          0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count128_255 bytes packet
Reg Addr   : 0x000D1008(RO)
Reg Formula: 0x000D1008 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_ro_Base                                                              0x000D1008
#define cAf6Reg_Eth_cnt128_255_ro(ethport)                                              (0x000D1008+(ethport))
#define cAf6Reg_Eth_cnt128_255_ro_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt128_255_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Shift                                                        0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count128_255 bytes packet
Reg Addr   : 0x000D1808(RC)
Reg Formula: 0x000D1808 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_rc_Base                                                              0x000D1808
#define cAf6Reg_Eth_cnt128_255_rc(ethport)                                              (0x000D1808+(ethport))
#define cAf6Reg_Eth_cnt128_255_rc_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt128_255_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Shift                                                        0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000D100C(RO)
Reg Formula: 0x000D100C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_ro_Base                                                              0x000D100C
#define cAf6Reg_Eth_cnt256_511_ro(ethport)                                              (0x000D100C+(ethport))
#define cAf6Reg_Eth_cnt256_511_ro_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt256_511_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Shift                                                        0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000D180C(RC)
Reg Formula: 0x000D180C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_rc_Base                                                              0x000D180C
#define cAf6Reg_Eth_cnt256_511_rc(ethport)                                              (0x000D180C+(ethport))
#define cAf6Reg_Eth_cnt256_511_rc_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt256_511_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Shift                                                        0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x000D1010(RO)
Reg Formula: 0x000D1010 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1023_ro_Base                                                             0x000D1010
#define cAf6Reg_Eth_cnt512_1023_ro(ethport)                                             (0x000D1010+(ethport))
#define cAf6Reg_Eth_cnt512_1023_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt512_1023_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1023
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Bit_Start                                                  0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Bit_End                                                   31
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_Shift                                                      0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_MaxVal                                            0xffffffff
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_MinVal                                                   0x0
#define cAf6_Eth_cnt512_1023_ro_CLAEthCnt512_1023_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x000D1810(RC)
Reg Formula: 0x000D1810 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1023_rc_Base                                                             0x000D1810
#define cAf6Reg_Eth_cnt512_1023_rc(ethport)                                             (0x000D1810+(ethport))
#define cAf6Reg_Eth_cnt512_1023_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt512_1023_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1023
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Bit_Start                                                  0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Bit_End                                                   31
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_Shift                                                      0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_MaxVal                                            0xffffffff
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_MinVal                                                   0x0
#define cAf6_Eth_cnt512_1023_rc_CLAEthCnt512_1023_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000D1014(RO)
Reg Formula: 0x000D1014 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1024_1518_ro_Base                                                            0x000D1014
#define cAf6Reg_Eth_cnt1024_1518_ro(ethport)                                            (0x000D1014+(ethport))
#define cAf6Reg_Eth_cnt1024_1518_ro_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1024_1518_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1024_1518
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Bit_Start                                                0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Bit_End                                                 31
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Mask                                              cBit31_0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_Shift                                                    0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_MinVal                                                 0x0
#define cAf6_Eth_cnt1024_1518_ro_CLAEthCnt1024_1518_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000D1814(RC)
Reg Formula: 0x000D1814 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1024_1518_rc_Base                                                            0x000D1814
#define cAf6Reg_Eth_cnt1024_1518_rc(ethport)                                            (0x000D1814+(ethport))
#define cAf6Reg_Eth_cnt1024_1518_rc_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1024_1518_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1024_1518
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Bit_Start                                                0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Bit_End                                                 31
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Mask                                              cBit31_0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_Shift                                                    0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_MinVal                                                 0x0
#define cAf6_Eth_cnt1024_1518_rc_CLAEthCnt1024_1518_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000D1018(RO)
Reg Formula: 0x000D1018 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1519_2047_ro_Base                                                            0x000D1018
#define cAf6Reg_Eth_cnt1519_2047_ro(ethport)                                            (0x000D1018+(ethport))
#define cAf6Reg_Eth_cnt1519_2047_ro_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1519_2047_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1519_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Bit_Start                                                0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Bit_End                                                 31
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_Shift                                                    0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_MinVal                                                 0x0
#define cAf6_Eth_cnt1519_2047_ro_CLAEthCnt1519_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000D1818(RC)
Reg Formula: 0x000D1818 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1519_2047_rc_Base                                                            0x000D1818
#define cAf6Reg_Eth_cnt1519_2047_rc(ethport)                                            (0x000D1818+(ethport))
#define cAf6Reg_Eth_cnt1519_2047_rc_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1519_2047_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1519_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Bit_Start                                                0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Bit_End                                                 31
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_Shift                                                    0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_MinVal                                                 0x0
#define cAf6_Eth_cnt1519_2047_rc_CLAEthCnt1519_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Jumbo packet
Reg Addr   : 0x000D101C(RO)
Reg Formula: 0x000D101C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_ro_Base                                                               0x000D101C
#define cAf6Reg_Eth_cnt_jumbo_ro(ethport)                                               (0x000D101C+(ethport))
#define cAf6Reg_Eth_cnt_jumbo_ro_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt_jumbo_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Shift                                                           0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Jumbo packet
Reg Addr   : 0x000D181C(RC)
Reg Formula: 0x000D181C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_rc_Base                                                               0x000D181C
#define cAf6Reg_Eth_cnt_jumbo_rc(ethport)                                               (0x000D181C+(ethport))
#define cAf6Reg_Eth_cnt_jumbo_rc_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt_jumbo_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Shift                                                           0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Unicast packet
Reg Addr   : 0x000D1020(RO)
Reg Formula: 0x000D1020 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_ro_Base                                                             0x000D1020
#define cAf6Reg_Eth_cnt_Unicast_ro(ethport)                                             (0x000D1020+(ethport))
#define cAf6Reg_Eth_cnt_Unicast_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_Unicast_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Shift                                                       0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Unicast packet
Reg Addr   : 0x000D1820(RC)
Reg Formula: 0x000D1820 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_rc_Base                                                             0x000D1820
#define cAf6Reg_Eth_cnt_Unicast_rc(ethport)                                             (0x000D1820+(ethport))
#define cAf6Reg_Eth_cnt_Unicast_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_Unicast_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Shift                                                       0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Total packet
Reg Addr   : 0x000D1024(RO)
Reg Formula: 0x000D1024 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_pkt_cnt_ro_Base                                                             0x000D1024
#define cAf6Reg_rx_port_pkt_cnt_ro(ethport)                                             (0x000D1024+(ethport))
#define cAf6Reg_rx_port_pkt_cnt_ro_WidthVal                                                                 32
#define cAf6Reg_rx_port_pkt_cnt_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Bit_Start                                                     0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Bit_End                                                      31
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Mask                                                   cBit31_0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Shift                                                         0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_MaxVal                                               0xffffffff
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_MinVal                                                      0x0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Total packet
Reg Addr   : 0x000D1824(RC)
Reg Formula: 0x000D1824 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_pkt_cnt_rc_Base                                                             0x000D1824
#define cAf6Reg_rx_port_pkt_cnt_rc(ethport)                                             (0x000D1824+(ethport))
#define cAf6Reg_rx_port_pkt_cnt_rc_WidthVal                                                                 32
#define cAf6Reg_rx_port_pkt_cnt_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Bit_Start                                                     0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Bit_End                                                      31
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Mask                                                   cBit31_0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Shift                                                         0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_MaxVal                                               0xffffffff
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_MinVal                                                      0x0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Broadcast packet
Reg Addr   : 0x000D1028(RO)
Reg Formula: 0x000D1028 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Broadcast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_Base                                                       0x000D1028
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro(ethport)                                       (0x000D1028+(ethport))
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_WidthVal                                                           32
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_Start                                           0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_End                                            31
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Mask                                         cBit31_0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Shift                                               0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MaxVal                                     0xffffffff
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MinVal                                            0x0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Broadcast packet
Reg Addr   : 0x000D1828(RC)
Reg Formula: 0x000D1828 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Broadcast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_Base                                                       0x000D1828
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc(ethport)                                       (0x000D1828+(ethport))
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_WidthVal                                                           32
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_Start                                           0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_End                                            31
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Mask                                         cBit31_0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Shift                                               0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MaxVal                                     0xffffffff
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MinVal                                            0x0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Multicast packet
Reg Addr   : 0x000D102C(RO)
Reg Formula: 0x000D102C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Multicast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_Base                                                       0x000D102C
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro(ethport)                                       (0x000D102C+(ethport))
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_WidthVal                                                           32
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_Start                                           0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_End                                            31
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Mask                                         cBit31_0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Shift                                               0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_MaxVal                                     0xffffffff
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_MinVal                                            0x0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Multicast packet
Reg Addr   : 0x000D182C(RC)
Reg Formula: 0x000D182C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Multicast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_Base                                                       0x000D182C
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc(ethport)                                       (0x000D182C+(ethport))
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_WidthVal                                                           32
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_Start                                           0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_End                                            31
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Mask                                         cBit31_0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Shift                                               0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_MaxVal                                     0xffffffff
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_MinVal                                            0x0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Under size packet
Reg Addr   : 0x000D1030(RO)
Reg Formula: 0x000D1030 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the under size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_Base                                                  0x000D1030
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro(ethport)                                  (0x000D1030+(ethport))
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_WidthVal                                                      32
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: CLAEthCntUnderSize
BitField Type: RO
BitField Desc: This is statistic counter under size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Bit_Start                                       0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Bit_End                                       31
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Mask                                    cBit31_0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Shift                                          0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_MaxVal                                0xffffffff
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_MinVal                                       0x0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Under size packet
Reg Addr   : 0x000D1830(RC)
Reg Formula: 0x000D1830 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the under size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_Base                                                  0x000D1830
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc(ethport)                                  (0x000D1830+(ethport))
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_WidthVal                                                      32
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: CLAEthCntUnderSize
BitField Type: RO
BitField Desc: This is statistic counter under size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Bit_Start                                       0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Bit_End                                       31
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Mask                                    cBit31_0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Shift                                          0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_MaxVal                                0xffffffff
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_MinVal                                       0x0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Over size packet
Reg Addr   : 0x000D1034(RO)
Reg Formula: 0x000D1034 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_Base                                                   0x000D1034
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro(ethport)                                   (0x000D1034+(ethport))
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_WidthVal                                                       32
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: CLAEthCntOverSize
BitField Type: RO
BitField Desc: This is statistic counter over size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Bit_Start                                        0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Bit_End                                         31
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Mask                                      cBit31_0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Shift                                            0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_MaxVal                                  0xffffffff
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_MinVal                                         0x0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Over size packet
Reg Addr   : 0x000D1834(RC)
Reg Formula: 0x000D1834 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_Base                                                   0x000D1834
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc(ethport)                                   (0x000D1834+(ethport))
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_WidthVal                                                       32
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: CLAEthCntOverSize
BitField Type: RO
BitField Desc: This is statistic counter over size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Bit_Start                                        0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Bit_End                                         31
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Mask                                      cBit31_0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Shift                                            0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_MaxVal                                  0xffffffff
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_MinVal                                         0x0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count physical error packet
Reg Addr   : 0x000D1038(RO)
Reg Formula: 0x000D1038 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count physical error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_phy_err_ro_Base                                                             0x000D1038
#define cAf6Reg_Eth_cnt_phy_err_ro(ethport)                                             (0x000D1038+(ethport))
#define cAf6Reg_Eth_cnt_phy_err_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_phy_err_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter physical error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Bit_Start                                                    0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Bit_End                                                     31
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Shift                                                        0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_MinVal                                                     0x0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count physical error packet
Reg Addr   : 0x000D1838(RC)
Reg Formula: 0x000D1838 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count physical error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_phy_err_rc_Base                                                             0x000D1838
#define cAf6Reg_Eth_cnt_phy_err_rc(ethport)                                             (0x000D1838+(ethport))
#define cAf6Reg_Eth_cnt_phy_err_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_phy_err_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter physical error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Bit_Start                                                    0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Bit_End                                                     31
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Shift                                                        0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_MinVal                                                     0x0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x000D103C(RO)
Reg Formula: 0x000D103C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_ro_Base                                                                0x000D103C
#define cAf6Reg_Eth_cnt_byte_ro(ethport)                                                (0x000D103C+(ethport))
#define cAf6Reg_Eth_cnt_byte_ro_WidthVal                                                                    32
#define cAf6Reg_Eth_cnt_byte_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Bit_End                                                          31
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Shift                                                             0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x000D183C(RC)
Reg Formula: 0x000D183C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_rc_Base                                                                0x000D183C
#define cAf6Reg_Eth_cnt_byte_rc(ethport)                                                (0x000D183C+(ethport))
#define cAf6Reg_Eth_cnt_byte_rc_WidthVal                                                                    32
#define cAf6Reg_Eth_cnt_byte_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Bit_End                                                          31
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Shift                                                             0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control2
Reg Addr   : 0x0040000 - 0x0047FFF The address format for these registers is 0x0040000 + CollisHashID*0x1000  + MemExtraPtr
Reg Formula: 0x0040000 + MemExtraPtr
    Where  : 
           + $MemExtraPtr(0-4095): MemExtraPtr
           + $CollisHashID (0-7): Collision
Reg Desc   : 
This memory contain maximum 8 entries extra to examine one Pseudowire label whether match or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_extra_Base                                                        0x0040000
#define cAf6Reg_cla_hbce_lkup_info_extra(MemExtraPtr, CollisHashID)                  (0x0040000+(MemExtraPtr))
#define cAf6Reg_cla_hbce_lkup_info_extra_WidthVal                                                           64
#define cAf6Reg_cla_hbce_lkup_info_extra_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [37]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Bit_Start                                      37
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Bit_End                                        37
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Mask                                        cBit5
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Shift                                           5
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_MaxVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_MinVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_RstVal                                        0x0

/*--------------------------------------
BitField Name: CLAExtraHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [36]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Bit_Start                                      36
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Bit_End                                        36
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Mask                                        cBit4
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Shift                                           4
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_MaxVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_MinVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_RstVal                                        0x0

/*--------------------------------------
BitField Name: CLAExtraHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [35:24]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Bit_Start                                       24
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Bit_End                                         35
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Mask_01                                  cBit31_24
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Shift_01                                        24
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Mask_02                                    cBit3_0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Shift_02                                         0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowID
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [23:11]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Bit_Start                                          11
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Bit_End                                            23
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Mask                                        cBit23_11
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Shift                                              11
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_MaxVal                                         0x1fff
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_MinVal                                            0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_RstVal                                            0x0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Bit_Start                                         10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Bit_End                                           10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Mask                                          cBit10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Shift                                             10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_MaxVal                                           0x1
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_MinVal                                           0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_RstVal                                           0x0

/*--------------------------------------
BitField Name: CLAExtraHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Bit_Start                                          0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Bit_End                                            9
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Mask                                         cBit9_0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Shift                                              0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_MaxVal                                         0x3ff
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_MinVal                                           0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_RstVal                                           0x0

/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Slice to CDR Control
Reg Addr   : 0x00C0000 - 0x0041FFF
Reg Formula: 0x00C0000 +  PWID
    Where  :
           + $PWID(0-8191): Pseudowire ID
Reg Desc   :
This register configures Slice ID to CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_2_cdr_cfg_Base                                                                   0x00C0000
#define cAf6Reg_cla_2_cdr_cfg(PWID)                                                         (0x00C0000+(PWID))
#define cAf6Reg_cla_2_cdr_cfg_WidthVal                                                                      32
#define cAf6Reg_cla_2_cdr_cfg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: PwCdrEn
BitField Type: RW
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Bit_Start                                                                15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Bit_End                                                                  15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Mask                                                                 cBit15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Shift                                                                    15

/*--------------------------------------
BitField Name: PwHoEn
BitField Type: RW
BitField Desc: Indicate 8x Hi order OC48 enable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwHoEn_Bit_Start                                                                 14
#define cAf6_cla_2_cdr_cfg_PwHoEn_Bit_End                                                                   14
#define cAf6_cla_2_cdr_cfg_PwHoEn_Mask                                                                  cBit14
#define cAf6_cla_2_cdr_cfg_PwHoEn_Shift                                                                     14
#define cAf6_cla_2_cdr_cfg_PwHoEn_MaxVal                                                                   0x1
#define cAf6_cla_2_cdr_cfg_PwHoEn_MinVal                                                                   0x0
#define cAf6_cla_2_cdr_cfg_PwHoEn_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: PwHoLoOc48Id
BitField Type: RW
BitField Desc: Indicate 8x Hi order OC48 or 6x Low order OC48 slice
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Bit_Start                                                           11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Bit_End                                                             13
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Mask                                                         cBit13_11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Shift                                                               11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_MaxVal                                                             0x7
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_MinVal                                                             0x0
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwLoSlice24Sel
BitField Type: RW
BitField Desc: Pseudo-wire Low order OC24 slice selection, only valid in Lo
Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within
a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a
Lo OC48
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Bit_Start                                                         10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Bit_End                                                           10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Mask                                                          cBit10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Shift                                                             10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_MaxVal                                                           0x1
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_MinVal                                                           0x0
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to
Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and
MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Bit_Start                                                             0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Bit_End                                                               9
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Mask                                                            cBit9_0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Shift                                                                 0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_MaxVal                                                            0x3ff
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_MinVal                                                              0x0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_RstVal                                                              0x0

/* Dword 0 */
#define cEthPortDataEthFullMask(port)  (cBit0 << (port * 8))
#define cEthPortDataEthFullShift(port) (port * 8)

#define cEthPortGrayEthFullMask(port)  (cBit1 << (port * 8))
#define cEthPortGrayEthFullShift(port) (1 + (port * 8))

#define cEthPortLostSopMask(port)      (cBit2 << (port * 8))
#define cEthPortLostSopShift(port)     (2 + (port * 8))

#define cEthPortLostEopMask(port)      (cBit3 << (port * 8))
#define cEthPortLostEopShift(port)     (3 + (port * 8))

#define cEthPortPktShortMask(port)     (cBit4 << (port * 8))
#define cEthPortPktShortShift(port)    (4 + (port * 8))

#define cEthPortPktOverMask(port)      (cBit5 << (port * 8))
#define cEthPortPktOverShift(port)     (5 + (port * 8))

/* Dword 1 */
#define cEthPwLkGoodMask               cBit16
#define cEthPwLkGoodShift              16

#define cEthPwLkFailMask               cBit17
#define cEthPwLkFailShift              17

#define cEth2ClaMask(port)             (cBit20 << (port))
#define cEth2ClaShift(port)            (20 + (port))

#define cCla2PdaMask(port)             (cBit24 << (port))
#define cCla2PdaShift(port)            (24 + (port))

#define cCla2PdaErrMask(port)          (cBit28 << (port))
#define cCla2PdaErrShift(port)         (28 + (port))

#define cCla2pdaSopPort(port)          (cBit0 << (port))
#define cCla2pdaEopPort(port)          (cBit4 << (port))
#define cCla2pdaMissSopPort(port)      (cBit8 << (port))
#define cCla2pdaMissEopPort(port)      (cBit10 << (port))

/* Dword 2 */
#define cClaLengthErrorMask            cBit0
#define cCwErrorMask                   cBit2
#define cPwDisable                     cBit5
#define cEthTypeError                  cBit6
#define cMacError                      cBit7
#define cPhyError                      cBit8

/* Dword 2 */
#define cRtpSsrcErrMask     cBit1 /* [65] */
#define cUdpCheckSumErrMask cBit3 /* [67] */
#define cIpCheckSumErrMask  cBit4 /* [68] */
#define cRtpPtErrMask       cBit10 /* [74] */
#define cPwCfgDisable       cBit11 /* [75] */

#endif /* _AF6_REG_AF6CCI0011_RD_CLA_H_ */

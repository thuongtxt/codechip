/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011HbceMemoryPool.c
 *
 * Created Date: May 12, 2015
 *
 * Description : Memory pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/cla/hbce/ThaHbceEntityInternal.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../Tha60210011ModuleClaReg.h"
#include "../Tha60210011ModuleCla.h"
#include "Tha60210011HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cLongRegSize 5

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011HbceMemoryPool)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011HbceMemoryPoolMethods m_methods;

/* Override  */
static tAtObjectMethods      m_AtObjectOverride;
static tThaHbceEntityMethods m_ThaHbceEntityOverride;
static tThaHbceMemoryPoolMethods m_ThaHbceMemoryPoolOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;
static const tThaHbceMemoryPoolMethods *m_ThaHbceMemoryPoolMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    AtUnused(self);
    return 96 * 1024;
    }

static uint32 ExtraCellLookupInformationCtrl(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return cThaRegClaHbceLookingUpInformationCtr2 + cellIndex - Tha60210011HbceMaxNumHashCells(self->hbce);
    }

static eBool IsStandardCellIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return cellIndex < Tha60210011HbceMaxNumHashCells(self->hbce);
    }

static uint32 ClaHbceLookingUpInformationCtrlPerCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    if (Tha60210011HbceMemoryPoolIsStandardCellIndex(self, cellIndex))
        {
    	uint32 maxNumHashIdsPerColision = ThaHbceMaxNumEntries(self->hbce);
        uint32 blockIndex   = cellIndex / maxNumHashIdsPerColision;
        uint32 indexInBlock = cellIndex % maxNumHashIdsPerColision;
        uint32 offset = mMethodsGet(mThis(self))->ClaHbceLookingUpInformationCtrlPerCellOffset(mThis(self), cellIndex, blockIndex, indexInBlock);
        return cThaRegClaHbceLookingUpInformationCtrl + offset;
        }

    return mMethodsGet(mThis(self))->ExtraCellLookupInformationCtrl(self, cellIndex);
    }

static ThaHbceMemoryCell UseCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryCell cell = ThaHbceMemoryPoolCellAtIndex(self, cellIndex);
    if (cell == NULL)
        return NULL;

    /* Clean it */
    ThaHbceEntityInit((ThaHbceEntity)cell);
    self->numUsedCells = self->numUsedCells + 1;
    Tha60210011HbceMemoryCellUse(cell, cAtTrue);

    return cell;
    }

static uint32 HwMemoryPoolCellIndexFromSwCellIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return cellIndex - Tha60210011HbceMaxNumHashCells(self->hbce);
    }

static uint32 ExtraPointerFromCellIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return HwMemoryPoolCellIndexFromSwCellIndex(self, cellIndex) % Tha60210011HbceNumExtraMemoryPointer(self->hbce);
    }

static eBool CanFreeExtraPointer(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint8 i;
    uint32 extraPointer;
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);

    if (cellIndex < Tha60210011HbceMaxNumHashCells(hbce))
        return cAtFalse;

    extraPointer = ExtraPointerFromCellIndex(self, cellIndex);
    for (i = 0; i < Tha60210011HbceNumHashCollisionInOneExtraPointer((Tha60210011Hbce)hbce); i++)
        {
        cellIndex = extraPointer + (uint32)(i * Tha60210011HbceNumExtraMemoryPointer(self->hbce)) + Tha60210011HbceMaxNumHashCells(hbce);
        if (Tha60210011HbceMemoryCellIsUsed(ThaHbceMemoryPoolCellAtIndex(self, cellIndex)) == cAtTrue)
            return cAtFalse;
        }

    return cAtTrue;
    }

static AtDevice Device(ThaHbceMemoryPool self)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return AtModuleDeviceGet(claModule);
    }

static eAtRet UnUseCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint32 numUsedCells = ThaHbceMemoryPoolNumUsedCells(self);
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);

    if (cell == NULL)
        return cAtErrorNullPointer;

    /* Clean this cell */
    ThaHbceEntityInit((ThaHbceEntity)cell);
    ThaHbceMemoryCellContentRemainedBitsInvalidate(ThaHbceMemoryCellContentGet(cell));

    self->numUsedCells = numUsedCells - 1;
    Tha60210011HbceMemoryCellUse(cell, cAtFalse);

    if (CanFreeExtraPointer(self, cell))
        Tha60210011HbceMemoryPoolFreeExtraPointerListPush(self, cellIndex - Tha60210011HbceMaxNumHashCells(hbce));

    if (!AtDeviceIsDeleting(Device(self)))
        Tha60210011HbceMemoryPoolApplyCellToHardware(self, ThaHbceMemoryCellIndexGet(cell));

    return cAtOk;
    }

static eAtRet Init(ThaHbceEntity self)
    {
    eAtRet ret = m_ThaHbceEntityMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Set up free extra pointer list */
    if (mThis(self)->freeExtraPointerMask != NULL)
        ThaBitMaskInit(mThis(self)->freeExtraPointerMask);
    else
        mThis(self)->freeExtraPointerMask = ThaBitMaskNew(Tha60210011HbceNumExtraMemoryPointer(((ThaHbceMemoryPool)self)->hbce));

    return cAtOk;
    }

static AtIpCore Core(ThaHbceMemoryPool self)
    {
    return ThaHbceCoreGet(self->hbce);
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    uint32 address = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, ThaHbceMemoryCellIndexGet(cell));
    uint32 longRegVal[cLongRegSize];
    ThaHbceMemoryCellContent cellContent;
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    /* Update hardware */
    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSize, Core(self));
    mFieldIns(&longRegVal[0],
              Tha60210011ModuleClaHbceFlowEnableMask(claModule),
              Tha60210011ModuleClaHbceFlowEnableShift(claModule),
              mBoolToBin(enable));
    mModuleHwLongWrite(claModule, address, longRegVal, cLongRegSize, Core(self));

    /* Update software database */
    cellContent = ThaHbceMemoryCellContentGet(cell);
    ThaHbceMemoryCellContentEnable(cellContent, enable);

    return cAtOk;
    }

static eAtRet CellInfoGet(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;
    AtHal hal = AtDeviceIpCoreHalGet(Device(pool), 0);
    Tha60210011ModuleCla moduleCla = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 address = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(pool, cellIndex);
    uint32 regVal = AtHalRead(hal, address);

    if (flowId)
        mFieldGet(regVal, Tha60210011ModuleClaHbceFlowIdMask(moduleCla), Tha60210011ModuleClaHbceFlowIdShift(moduleCla), uint32, flowId);

    if (enable)
        *enable = (regVal & Tha60210011ModuleClaHbceFlowEnableMask(moduleCla)) ? cAtTrue : cAtFalse;

    if (storeId)
        mFieldGet(regVal, Tha60210011ModuleClaHbceStoreIdMask(moduleCla), Tha60210011ModuleClaHbceStoreIdShift(moduleCla), uint32, storeId);

    return cAtOk;
    }

static eBool CellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    eBool enabled;
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);
    mMethodsGet(mThis(self))->CellInfoGet((Tha60210011HbceMemoryPool)self, cellIndex, NULL, &enabled, NULL);
    return enabled;
    }

static eBool ShouldOptimizeInit(ThaHbceEntity self)
    {
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet((ThaHbceMemoryPool)self);
    return ThaHbceEntityShouldOptimizeInit((ThaHbceEntity)hbce);
    }

static eAtRet CellInvalidate(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    /* Note: the cell is invalidated by writing maximum store ID. Without doing
     * this, all of fields will be 0 and restoring will be wrong in case of PWs
     * are disabled, cells may be restored and associated to any disabled PWs.
     * See the example commit message for detail example. */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 address = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, cellIndex);
    uint32 longRegVal[cLongRegSize];
    uint32 fieldMask = Tha60210011ModuleClaHbceStoreIdMask((Tha60210011ModuleCla)claModule);
    uint32 fieldShift = Tha60210011ModuleClaHbceStoreIdShift((Tha60210011ModuleCla)claModule);

    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&(longRegVal[0]), fieldMask, fieldShift, fieldMask >> fieldShift);

    if (ThaHbceEntityShouldOptimizeInit((ThaHbceEntity)self))
        {
        mModuleHwWrite(claModule, address, longRegVal[0]);
        }
    else
        {
        mModuleHwLongWrite(claModule, address, longRegVal, cLongRegSize, Core(self));
        }

    return cAtOk;
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return CellInvalidate(self, cellIndex);
    }

static ThaHbceMemoryCell CellCreate(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return Tha60210011HbceMemoryCellNew(self, cellIndex);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->freeExtraPointerMask);
    mThis(self)->freeExtraPointerMask = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011HbceMemoryPool* object = (tTha60210011HbceMemoryPool*)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(freeExtraPointerMask);
    }

static eBool CellIsPrimary(ThaHbceMemoryPool self, uint32 absoluteCellIndex, uint32* groupId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    Tha60210011ModuleCla moduleCla = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    AtDevice device = AtModuleDeviceGet((AtModule)moduleCla);
    uint32 fieldVal;

    uint32 address = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, absoluteCellIndex);
    AtDeviceLongReadOnCore(device, address, longRegVal, cThaLongRegMaxSize, 0);

    mFieldGet(longRegVal[cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Tail_DwIndex],
              Tha60210011ModuleClaHbceGroupIdFlowMask1(moduleCla),
              Tha60210011ModuleClaHbceGroupIdFlowShift1(moduleCla),
              uint32,
              &fieldVal);
    *groupId = fieldVal;

    if (Tha60210011ModuleClaHbceGroupIdFlowMask2(moduleCla) != 0)
        {
        mFieldGet(longRegVal[cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Head_DwIndex],
                  Tha60210011ModuleClaHbceGroupIdFlowMask2(moduleCla),
                  Tha60210011ModuleClaHbceGroupIdFlowShift2(moduleCla),
                  uint32,
                  &fieldVal);
        *groupId |= (fieldVal << (32 - Tha60210011ModuleClaHbceGroupIdFlowShift1(moduleCla)));
        }

    if (longRegVal[cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_DwIndex] & Tha60210011ModuleClaHbceGroupWorkingMask(moduleCla))
        return cAtTrue;

    return cAtFalse;
    }

static ThaPwAdapter StandbyCellRestore(ThaHbceMemoryPool self, uint32 entryIndex, uint32 absoluteCellIndex)
    {
    ThaHbceMemoryCellContent cellContent;
    ThaHbceMemoryCell cell;
    ThaPwAdapter pwAdapter;
    ThaPwHeaderController headerController;
    uint32 flowId, storeId;
    eBool enabled;
    uint32 remaingingBits[2];
    AtPw pw;
    AtPwGroup pwGroup;
    ThaModuleCla moduleCla = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    /* Check if this cell belongs to any PW */
    mMethodsGet(mThis(self))->CellInfoGet(mThis(self), absoluteCellIndex, &flowId, &enabled, &storeId);
    pwAdapter = ThaModuleClaHbcePwFromFlowId(moduleCla, flowId);
    if (pwAdapter == NULL)
        return NULL;

    /* Enable should match */
    if (enabled != ThaPwAdapterIsEnabled(pwAdapter))
        return NULL;

    /* If entry is specified, it must match with the hash value. */
    pw = ThaPwAdapterPwGet(pwAdapter);
    pwGroup = AtPwHsGroupGet(pw);
    AtOsalMemInit(remaingingBits, 0, sizeof(remaingingBits));
    if (entryIndex != cInvalidUint32)
        {
        uint32 hashIndex = ThaHbcePwPsnHash(ThaHbceMemoryPoolHbceGet(self), pwAdapter, AtPwPsnGet((AtPw)pwAdapter), remaingingBits);

        /* This may not be the primary header controller, and it can be backup */
        if ((hashIndex != entryIndex) && (pwGroup != NULL))
            hashIndex = ThaHbcePwPsnHash(ThaHbceMemoryPoolHbceGet(self), pwAdapter, AtPwBackupPsnGet((AtPw)pwAdapter), remaingingBits);

        /* No header controller match this */
        if (hashIndex != entryIndex)
            return NULL;
        }

    /* Remaining bits would also match */
    if (remaingingBits[0] != storeId)
        return NULL;

    /* This cell belong to backup header controller */
    headerController = ThaPwAdapterHeaderController(pwAdapter);
    if (pwGroup)
        {
        uint32 groupId;
        eBool cellIsPrimary = mMethodsGet(mThis(self))->CellIsPrimary(self, absoluteCellIndex, &groupId);

        if (groupId != AtPwGroupIdGet(pwGroup))
            AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation, "Hardware group ID and SW group ID are mismatched\r\n");

        if (cellIsPrimary)
            headerController = ThaPwAdapterHeaderController(pwAdapter);
        else
            headerController = ThaPwAdapterBackupHeaderController(pwAdapter);
        }

    if (!ThaPwHeaderControllerHbceIsActivated(headerController))
        return NULL;

    /* Then restore this cell */
    ThaHbceMemoryPoolUseCell(self, absoluteCellIndex);
    cell = ThaHbceMemoryPoolCellAtIndex(self, absoluteCellIndex);
    cellContent = ThaHbceMemoryCellContentGet(cell);
    if (cellContent == NULL)
        return NULL;

    ThaHbceMemoryCellContentPwHeaderControllerSet(cellContent, headerController);
    ThaPwHeaderControllerHbceMemoryCellContentSet(headerController, cellContent);

    ThaHbceMemoryCellContentEnable(cellContent, enabled);
    ThaHbceMemoryCellContentRemainedBitsSet(cellContent, remaingingBits, mCount(remaingingBits));

    return pwAdapter;
    }

static uint32 ClaHbceLookingUpInformationCtrlPerCellOffset(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 blockIndex, uint32 indexInBlock)
    {
    AtUnused(self);
    AtUnused(cellIndex);
    return (blockIndex * 0x10000) + indexInBlock;
    }

static void ApplyCell(Tha60210011HbceMemoryPool self, uint32 cellIndex)
    {
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaHbceMemoryCellContent cellContent;
    AtPwGroup group;
    uint32 groupId;
    uint32 tailGroupId;
    uint32 flowId;
    ThaPwAdapter pwAdapter;
    ThaPwHeaderController headerController;
    AtDevice device;
    uint32 storeId;
    eBool hwEnable;
    Tha60210011ModuleCla claModule;

    if (self == NULL)
        return;

    cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex((ThaHbceMemoryPool)self, cellIndex));
    if (cellContent == NULL)
        return;

    if (ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL) == NULL) /* TODO: why need that */
        return;

    address = Tha60210011ClaHbceLookingUpInformationCtrlPerCell((ThaHbceMemoryPool)self, cellIndex);
    headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
    pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    device = AtModuleDeviceGet((AtModule)claModule);
    if (pwAdapter == NULL)
        {
        hwEnable = 0;
        group = NULL;
        flowId = 0;
        }
    else
        {
        hwEnable = AtChannelIsEnabled((AtChannel)pwAdapter) ? 1 : 0;
        group = AtPwHsGroupGet(ThaPwAdapterPwGet(pwAdapter));
        flowId = Tha60210011ModuleClaHbceFlowIdFromPw(claModule, pwAdapter);
        }

    AtDeviceLongReadOnCore(device, address, longRegVal, cThaLongRegMaxSize, 0);
    mFieldIns(&longRegVal[0],
              Tha60210011ModuleClaHbceFlowIdMask(claModule),
              Tha60210011ModuleClaHbceFlowIdShift(claModule),
              flowId);

    /* Disable cell of backup PSN if PW does not belong to HW group */
    if ((group == NULL) && (ThaPwHeaderControllerIsPrimary(headerController) == cAtFalse))
        mFieldIns(&longRegVal[0],
                  Tha60210011ModuleClaHbceFlowEnableMask(claModule),
                  Tha60210011ModuleClaHbceFlowEnableShift(claModule),
                  0);
    else
        mFieldIns(&longRegVal[0],
                  Tha60210011ModuleClaHbceFlowEnableMask(claModule),
                  Tha60210011ModuleClaHbceFlowEnableShift(claModule),
                  hwEnable);

    storeId = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
    mFieldIns(&longRegVal[0],
              Tha60210011ModuleClaHbceStoreIdMask(claModule),
              Tha60210011ModuleClaHbceStoreIdShift(claModule),
              storeId);

    if (group != NULL)
        {
        groupId = AtPwGroupIdGet(group);
        tailGroupId = groupId & (Tha60210011ModuleClaHbceGroupIdFlowMask1(claModule) >> Tha60210011ModuleClaHbceGroupIdFlowShift1(claModule));

        mFieldIns(&longRegVal[1],
                  Tha60210011ModuleClaHbceFlowDirectMask(claModule),
                  Tha60210011ModuleClaHbceFlowDirectShift(claModule),
                  0);
        if (Tha60210011ModuleClaHbceGroupIdFlowMask2(claModule) != 0)
            {
            uint32 headGroupId = groupId >> (32 - Tha60210011ModuleClaHbceGroupIdFlowShift1(claModule));
            mFieldIns(&longRegVal[1],
                      Tha60210011ModuleClaHbceGroupIdFlowMask2(claModule),
                      Tha60210011ModuleClaHbceGroupIdFlowShift2(claModule),
                      headGroupId);
            }

        mFieldIns(&longRegVal[0],
                  Tha60210011ModuleClaHbceGroupIdFlowMask1(claModule),
                  Tha60210011ModuleClaHbceGroupIdFlowShift1(claModule),
                  tailGroupId);
        mFieldIns(&longRegVal[1],
                  Tha60210011ModuleClaHbceGroupWorkingMask(claModule),
                  Tha60210011ModuleClaHbceGroupWorkingShift(claModule),
                  mBoolToBin(ThaPwHeaderControllerIsPrimary(headerController)));
        }
    else
        mFieldIns(&longRegVal[1],
                  Tha60210011ModuleClaHbceFlowDirectMask(claModule),
                  Tha60210011ModuleClaHbceFlowDirectShift(claModule),
                  1);

    AtDeviceLongWriteOnCore(device, address, longRegVal, cThaLongRegMaxSize, 0);
    }

static void MethodsInit(ThaHbceMemoryPool self)
    {
    Tha60210011HbceMemoryPool module = (Tha60210011HbceMemoryPool)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CellIsPrimary);
        mMethodOverride(m_methods, ExtraCellLookupInformationCtrl);
        mMethodOverride(m_methods, ClaHbceLookingUpInformationCtrlPerCellOffset);
        mMethodOverride(m_methods, ClaHbceLookingUpInformationCtrlPerCell);
        mMethodOverride(m_methods, CellInfoGet);
        mMethodOverride(m_methods, ApplyCell);
        }

    mMethodsSet(module, &m_methods);
    }

static void OverrideAtObject(ThaHbceMemoryPool self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHbceEntity(ThaHbceMemoryPool self)
    {
    ThaHbceEntity hbceObject = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(hbceObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Init);
        mMethodOverride(m_ThaHbceEntityOverride, ShouldOptimizeInit);
        }

    mMethodsSet(hbceObject, &m_ThaHbceEntityOverride);
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMemoryPoolMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, m_ThaHbceMemoryPoolMethods, sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, UseCell);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, UnUseCell);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellIsEnabled);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellInit);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellCreate);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideAtObject(self);
    OverrideThaHbceEntity(self);
    OverrideThaHbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011HbceMemoryPool);
    }

ThaHbceMemoryPool Tha60210011HbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60210011HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011HbceMemoryPoolObjectInit(newPool, hbce);
    }

uint32 Tha60210011HbceMemoryPoolFreeExtraPointerListPop(ThaHbceMemoryPool self)
    {
    uint32 firstZero = ThaBitMaskFirstZeroBit(mThis(self)->freeExtraPointerMask);
    ThaBitMaskSetBit(mThis(self)->freeExtraPointerMask, firstZero);

    return firstZero;
    }

void Tha60210011HbceMemoryPoolFreeExtraPointerListPush(ThaHbceMemoryPool self, uint32 extraPointer)
    {
    ThaBitMaskClearBit(mThis(self)->freeExtraPointerMask, extraPointer);
    }

void Tha60210011HbceMemoryPoolFreeExtraPointerInUsedSet(ThaHbceMemoryPool self, uint32 pointer)
    {
    ThaBitMaskSetBit(mThis(self)->freeExtraPointerMask, pointer);
    }

uint32 Tha60210011HbceMemoryPoolCellHwFlowIdGet(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    uint32 flowId;
    mMethodsGet(mThis(self))->CellInfoGet(mThis(self), cellIndex, &flowId, NULL, NULL);
    return flowId;
    }

void Tha60210011HbceMemoryPoolApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    if (self)
        mMethodsGet(mThis(self))->ApplyCell(mThis(self), cellIndex);
    }

ThaPwAdapter Tha60210011HbceMemoryPoolStandbyCellRestore(ThaHbceMemoryPool self, uint32 entryIndex, uint32 absoluteCellIndex)
    {
    if (self)
        return StandbyCellRestore(self, entryIndex, absoluteCellIndex);
    return NULL;
    }

uint32 Tha60210011ClaHbceLookingUpInformationCtrlPerCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClaHbceLookingUpInformationCtrlPerCell(self, cellIndex);
    return cInvalidUint32;
    }

eBool Tha60210011HbceMemoryPoolIsStandardCellIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    if (self)
        return IsStandardCellIndex(self, cellIndex);
    return cAtFalse;
    }

eAtRet Tha60210011HbceMemoryPoolCellInfoGet(ThaHbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    if (self)
        return mMethodsGet(mThis(self))->CellInfoGet(mThis(self), cellIndex, flowId, enable, storeId);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60290011HbceMemoryPoolClaHbceLookingUpInformationCtrlPerCellOffset(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 blockIndex, uint32 indexInBlock)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClaHbceLookingUpInformationCtrlPerCellOffset(self, cellIndex, blockIndex, indexInBlock);

    return cInvalidUint32;
    }

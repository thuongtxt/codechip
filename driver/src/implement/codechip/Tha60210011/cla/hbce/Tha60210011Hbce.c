/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011Hbce.c
 *
 * Created Date: May 7, 2015
 *
 * Description : HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011Hbce.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../common/Tha602100xxCommon.h"
#include "../Tha60210011ModuleClaReg.h"
#include "../Tha60210011ModuleCla.h"
#include "Tha60210011HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaHbcePatern23_14Mask                        cBit23_14
#define cThaHbcePatern23_14Shift                       14
#define cThaHbcePatern23_14DwIndex                     0

#define cThaHbcePatern13_0Mask                         cBit13_0
#define cThaHbcePatern13_0Shift                        0
#define cThaHbcePatern13_0DwIndex                      0

#define cThaHbcePwLabelMask                            cBit23_4
#define cThaHbcePwLabelShift                           4

#define cThaHbcePsnTypeMask                            cBit3_2
#define cThaHbcePsnTypeShift                           2

#define cThaHbceGePortMask                             cBit1_0
#define cThaHbceGePortShift                            0


/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011Hbce)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011HbceMethods m_methods;

/* Override */
static tThaHbceMethods       m_ThaHbceOverride;
static tThaHbceEntityMethods m_ThaHbceEntityOverride;

/* Save super implementation */
static const tThaHbceMethods       *m_ThaHbceMethods       = NULL;
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(ThaHbce self)
    {
    AtUnused(self);
    return 16384;
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60210011HbceMemoryPoolNew(self);
    }

static uint8 NumHashCollisionInOneHashIndex(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 4;
    }

static uint8  NumHashCollisionInOneExtraPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 8;
    }

/* Cell pool is divided into 4 blocks, each block contains 16384 cells */
static uint32 FreeCellIndexInHashCollisionPool(ThaHbce self, ThaHbceMemoryPool memPool, uint32 hashIndex)
    {
    uint8 i;
    for (i = 0; i < Tha60210011HbceNumHashCollisionInOneHashIndex(mThis(self)); i++)
        {
        uint32 cellIndex = Tha60210011HbceHashCellIndex(self, hashIndex, i);
        if (Tha60210011HbceMemoryCellIsUsed(ThaHbceMemoryPoolCellAtIndex(memPool, cellIndex)) == cAtFalse)
            return cellIndex;
        }

    return cInvalidUint32;
    }

static uint32 FreeCellIndexInExtraMemoryPool(ThaHbce self, ThaHbceMemoryPool memPool, uint32 freeExtraPointer)
    {
    uint32 i;

    for (i = 0; i < Tha60210011HbceNumHashCollisionInOneExtraPointer(mThis(self)); i++)
        {
        uint32 cellIndex = Tha60210011HbceExtraPointerCellIndex(self, freeExtraPointer, i);
        if (Tha60210011HbceMemoryCellIsUsed(ThaHbceMemoryPoolCellAtIndex(memPool, cellIndex)) == cAtFalse)
            return cellIndex;
        }

    return cInvalidUint32;
    }

static uint32 ExtraStartPointerOfCell(ThaHbce self, uint32 extraPollCellIndex)
    {
    return (extraPollCellIndex - Tha60210011HbceMaxNumHashCells(self)) % Tha60210011HbceNumExtraMemoryPointer(self);
    }

static uint32 Read(ThaHbce self, uint32 address)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return mModuleHwReadByHal(claModule, address, AtIpCoreHalGet(self->core));
    }

static void Write(ThaHbce self, uint32 address, uint32 value)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    mModuleHwWriteByHal(claModule, address, value, AtIpCoreHalGet(self->core));
    }

/*
 * The SW memory pool contains two sub pools inside. First pool (called hash collision pool) contains cells controlled
 * by hash index and hash collision Id. The second pool (called extra memory pool)contains
 * cells controlled by hash collision Id and memory extra pointer which allocate dynamically
 * if a hash index has over 4 collisions. The second pool start from index 65536 which is
 * the maximum number of cells in first pool.
 *
 * To allocate cell from SW memory pool, we will run on three cases as the following:
 * case 1: hash entry has less than 4 collisions. We will allocate the cell of hash collision pool.
 * case 2: hash entry has 4 collisions. We will find a free extra memory pointer in HW memory
 * pool for this collisions and allocate cell corresponding with this pointer.
 * case 3: hash entry has more than 4 collisions. We will allocate next cell in HW memory pool
 * base on first cell allocated in case 2
 *
 * */
static ThaHbceMemoryCell AllocateCellForPw(ThaHbce self, ThaPwHeaderController controller, uint32 hashIndex)
    {
    uint32 cellIndex;
    ThaHbceEntry hashEntry = ThaHbceEntryAtIndex(self, hashIndex);
    ThaHbceMemoryPool memoryPool = ThaHbceMemoryPoolGet(self);
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);
    uint32 numPoolCollisionFlows = Tha60210011HbceEntryNumPoolCollisionFlowsGet(hashEntry);
    uint32 numHashCollisionFlows = Tha60210011HbceEntryNumHashCollisionFlowsGet(hashEntry);

    /* Check to see if there is existing pw that has same label as adding pw, if yes, return NULL */
    if (ThaHbceNewPwLabelExistInHashEntry(self, controller, psn, hashEntry))
        return NULL;

    /* One hash entry can support 4 collisions, if hash collision is full, extra will be used */
    if (numHashCollisionFlows < Tha60210011HbceNumHashCollisionInOneHashIndex(mThis(self)))
        cellIndex = FreeCellIndexInHashCollisionPool(self, memoryPool, hashIndex);

    /* It is first time need to use extra pool, get free extra pointer */
    else if (numPoolCollisionFlows == 0)
        cellIndex = Tha60210011HbceMemoryPoolFreeExtraPointerListPop(memoryPool) + Tha60210011HbceMaxNumHashCells(self);

    /* Extra pool has been allocate before, just find free cell index in that extra pointer pool */
    else
        cellIndex = FreeCellIndexInExtraMemoryPool(self, memoryPool, Tha60210011HbceHashEntryExtraPointerGet(self, hashEntry));

    if (cellIndex == cInvalidUint32)
        return NULL;

    return ThaHbceMemoryPoolUseCell(memoryPool, cellIndex);
    }

static ThaHbceMemoryCell SearchCellForPw(ThaHbce self, ThaPwAdapter pwAdapter, ThaHbceEntry hashEntry)
    {
    ThaHbceMemoryPool memoryPool = ThaHbceMemoryPoolGet(self);
    ThaHbceMemoryCellContent content = NULL;
    uint32 cellIndex, flow_i;
    uint32 numFlows = ThaHbceEntryNumFlowsGet(hashEntry);

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        content = ThaHbceEntryCellContentAtIndex(hashEntry, flow_i);
        cellIndex = ThaHbceMemoryCellIndexGet(ThaHbceMemoryCellContentCellGet(content));
        if (Tha60210011HbceMemoryPoolCellHwFlowIdGet(memoryPool, cellIndex) == AtChannelIdGet((AtChannel)pwAdapter))
            break;
        }

    return ThaHbceMemoryCellContentCellGet(content);
    }

static eAtRet Apply2Hardware(ThaHbce self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 EntryOffset(ThaHbce self, uint32 entryIndex, uint8 pageId)
    {
    AtUnused(self);
    AtUnused(pageId);
    return entryIndex;
    }

static uint32 HashIndexGet(Tha60210011Hbce self, uint32 hwLabel)
    {
    uint32 swHbcePatern23_14 = mRegField(hwLabel, cThaHbcePatern23_14);
    uint32 swHbcePatern13_0  = mRegField(hwLabel, cThaHbcePatern13_0);
    AtUnused(self);

    /* Calculate hash index */
    return swHbcePatern13_0 ^ swHbcePatern23_14;
    }

static uint8 HbcePktType(eAtPwPsnType psnType)
    {
    if (psnType == cAtPwPsnTypeUdp)  return 1;
    if (psnType == cAtPwPsnTypeMpls) return 2;
    if (psnType == cAtPwPsnTypeMef)  return 0;

    return 0;
    }

static uint32 HwHbceLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType)
    {
    uint32 hwLabel = 0;
    uint8 ethPortId = (uint8)(ethPort ? (uint8)AtChannelIdGet((AtChannel)ethPort) : 0);
    AtUnused(self);

    mRegFieldSet(hwLabel, cThaHbcePwLabel, label);
    mRegFieldSet(hwLabel, cThaHbcePsnType, HbcePktType(psnType));
    mRegFieldSet(hwLabel, cThaHbceGePort, ethPortId);
    return hwLabel;
    }

static eAtRet RemainBitsGet(Tha60210011Hbce self, uint32 hwLabel, uint32 *remainBits)
    {
    AtUnused(self);
    remainBits[0] = mRegField(hwLabel, cThaHbcePatern23_14);
    return cAtOk;
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel = HwHbceLabel(self, ethPort, label, psnType);
    uint32 hashIndex = mMethodsGet(mThis(self))->HashIndexGet(mThis(self), hwLabel);

    AtUnused(partId);
    if (remainBits)
        mMethodsGet(mThis(self))->RemainBitsGet(mThis(self), hwLabel, remainBits);

    return hashIndex;
    }

static ThaHbceEntry EntryObjectCreate(ThaHbce self, uint32 entryIndex)
    {
    return Tha60210011HbceEntryNew(self, entryIndex);
    }

static uint32 NumExtraMemoryPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 4096;
    }

static uint32 MaxNumHashCells(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 65536;
    }

static uint32 HashEntryExtraPointerGet(Tha60210011Hbce self, ThaHbceEntry hashEntry)
    {
    uint32 extraPointer;
    uint32 regVal, address;
    ThaHbce hbce = (ThaHbce)self;
    ThaClaPwController pwController = ThaModuleClaPwControllerGet((ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self));

    address = ThaHbceCLAHBCEHashingTabCtrl(hbce) + mMethodsGet(hbce)->EntryOffset(hbce, ThaHbceEntryIndexGet(hashEntry), 0);
    regVal  = Read(hbce, address);
    mFieldGet(regVal,
              ThaClaPwControllerCLAHbceMemoryStartPtrMask(pwController),
              ThaClaPwControllerCLAHbceMemoryStartPtrShift(pwController),
              uint32,
              &extraPointer);
    return extraPointer;
    }

static eAtRet HashEntryTabCtrlApply(Tha60210011Hbce self, uint32 entryIndex)
    {
    uint32 regVal, address;
    uint32 startMemoryCell;
    uint32 swCellIndex;
    ThaHbceMemoryCell cell;
    ThaHbceMemoryCellContent cellContent;
    ThaHbceEntry hashEntry;
    uint32 numPoolCollisions;
    ThaClaPwController pwController;
    ThaHbce hbce = (ThaHbce)self;

    hashEntry = ThaHbceEntryAtIndex(hbce, entryIndex);
    numPoolCollisions = Tha60210011HbceEntryNumPoolCollisionFlowsGet(hashEntry);
    pwController = ThaModuleClaPwControllerGet((ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self));

    /* If there is no flow belong to extra pointer pool in this entry, reset it */
    address = ThaHbceCLAHBCEHashingTabCtrl(hbce) + mMethodsGet(hbce)->EntryOffset(hbce, entryIndex, 0);
    if (numPoolCollisions == 0)
        {
        Write(hbce, address, 0);
        return cAtOk;
        }

    /* Do not need to update extra pointer if there is at least 1 flow in extra pointer pool */
    if (numPoolCollisions > 1)
        return cAtOk;

    cellContent = (ThaHbceMemoryCellContent)AtListObjectGet(Tha60210011HbceEntryPoolCollisionFlowListGet(hashEntry), 0);
    cell = (ThaHbceMemoryCell)ThaHbceMemoryCellContentCellGet(cellContent);
    swCellIndex = ThaHbceMemoryCellIndexGet(cell);
    startMemoryCell = ExtraStartPointerOfCell(hbce, swCellIndex);

    regVal = 0;
    mFieldIns(&regVal,
              ThaClaPwControllerCLAHbceFlowNumMask(pwController),
              ThaClaPwControllerCLAHbceFlowNumShift(pwController),
              1);
    mFieldIns(&regVal,
              ThaClaPwControllerCLAHbceMemoryStartPtrMask(pwController),
              ThaClaPwControllerCLAHbceMemoryStartPtrShift(pwController),
              startMemoryCell);
    Write(hbce, address, regVal);

    return cAtOk;
    }

static uint32 ExtraPointerCellIndex(Tha60210011Hbce self, uint32 extraPointerIndex, uint32 localCellIndex)
    {
    ThaHbce hbce = (ThaHbce)self;
    return extraPointerIndex + (localCellIndex * Tha60210011HbceNumExtraMemoryPointer(hbce)) + Tha60210011HbceMaxNumHashCells(hbce);
    }

static uint32 HashCellIndex(Tha60210011Hbce self, uint32 hashEntryIndex, uint32 localCellIndex)
    {
    ThaHbce hbce = (ThaHbce)self;
    return hashEntryIndex + (uint32)(localCellIndex * ThaHbceMaxNumEntries(hbce));
    }

static eBool HasPages(ThaHbce self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldOptimizeInit(ThaHbceEntity self)
    {
    AtUnused(self);
    return Tha602100xxHwAccessOptimized();
    }

static uint32 Restore(ThaHbceEntity self, uint8 page)
    {
    /* The current database might be already updated by previous restore
     * operations, so it is necessary to cleanup before this current restore
     * operation. Otherwise, duplicated entries will exist in database */
    if (AtDriverKeepDatabaseAfterRestore())
        ThaHbceMemoryPoolNumUsedCellsReset(ThaHbceMemoryPoolGet((ThaHbce)self));

    return m_ThaHbceEntityMethods->Restore(self, page);
    }

static void OverrideThaHbceEntity(ThaHbce self)
    {
    ThaHbceEntity hbceObject = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(hbceObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, ShouldOptimizeInit);
        mMethodOverride(m_ThaHbceEntityOverride, Restore);
        }

    mMethodsSet(hbceObject, &m_ThaHbceEntityOverride);
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, m_ThaHbceMethods, sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, AllocateCellForPw);
        mMethodOverride(m_ThaHbceOverride, Apply2Hardware);
        mMethodOverride(m_ThaHbceOverride, EntryOffset);
        mMethodOverride(m_ThaHbceOverride, SearchCellForPw);
        mMethodOverride(m_ThaHbceOverride, PsnHash);
        mMethodOverride(m_ThaHbceOverride, EntryObjectCreate);
        mMethodOverride(m_ThaHbceOverride, HasPages);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbceEntity(self);
    OverrideThaHbce(self);
    }

static void MethodsInit(ThaHbce self)
    {
    Tha60210011Hbce hbce = mThis(self);
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, NumExtraMemoryPointer);
        mMethodOverride(m_methods, MaxNumHashCells);
        mMethodOverride(m_methods, HashIndexGet);
        mMethodOverride(m_methods, RemainBitsGet);
        mMethodOverride(m_methods, NumHashCollisionInOneHashIndex);
        mMethodOverride(m_methods, NumHashCollisionInOneExtraPointer);
        mMethodOverride(m_methods, HashCellIndex);
        mMethodOverride(m_methods, ExtraPointerCellIndex);
        mMethodOverride(m_methods, HashEntryExtraPointerGet);
        mMethodOverride(m_methods, HashEntryTabCtrlApply);
        }

    mMethodsSet(hbce, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Hbce);
    }

ThaHbce Tha60210011HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductHbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60210011HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011HbceObjectInit(newHbce, hbceId, claModule, core);
    }

uint32 Tha60210011HbceMaxNumHashCells(ThaHbce self)
    {
    return (self) ? mMethodsGet(mThis(self))->MaxNumHashCells(mThis(self)) : 0;
    }

/* There are 4096 extra pointer, each extra pointer has maximum 8 collisions */
uint32 Tha60210011HbceNumExtraMemoryPointer(ThaHbce self)
    {
    return (self) ? mMethodsGet(mThis(self))->NumExtraMemoryPointer(mThis(self)) : 0;
    }

eAtRet Tha60210011HbceHashEntryTabCtrlApply(ThaHbce self, uint32 entryIndex)
    {
    if (self)
        return mMethodsGet(mThis(self))->HashEntryTabCtrlApply(mThis(self), entryIndex);
    return cAtErrorNullPointer;
    }

uint32 Tha60210011HbceExtraPointerCellIndex(ThaHbce self, uint32 extraPointerIndex, uint32 localCellIndex)
    {
    if (self)
        return mMethodsGet(mThis(self))->ExtraPointerCellIndex(mThis(self), extraPointerIndex, localCellIndex);

    return cInvalidUint32;
    }

uint32 Tha60210011HbceHashCellIndex(ThaHbce self, uint32 hashEntryIndex, uint32 localCellIndex)
    {
    if (self)
        return mMethodsGet(mThis(self))->HashCellIndex(mThis(self), hashEntryIndex, localCellIndex);

    return cInvalidUint32;
    }

uint32 Tha60210011HbceHashEntryExtraPointerGet(ThaHbce self, ThaHbceEntry hashEntry)
    {
    if (self)
        return mMethodsGet(mThis(self))->HashEntryExtraPointerGet(mThis(self), hashEntry);

    return cInvalidUint32;
    }

uint8 Tha60210011HbceNumHashCollisionInOneHashIndex(Tha60210011Hbce self)
    {
    if (self)
        return mMethodsGet(self)->NumHashCollisionInOneHashIndex(self);

    return 0;
    }

uint8 Tha60210011HbceNumHashCollisionInOneExtraPointer(Tha60210011Hbce self)
    {
    if (self)
        return mMethodsGet(self)->NumHashCollisionInOneExtraPointer(self);
    return 0;
    }

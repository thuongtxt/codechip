/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210011Hbce.h
 * 
 * Created Date: May 7, 2015
 *
 * Description : HBCE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HBCE_H_
#define _THA60210011HBCE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cla/hbce/ThaHbceEntityInternal.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Hbce * Tha60210011Hbce;
typedef struct tTha60210011HbceMemoryPool * Tha60210011HbceMemoryPool;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbce Tha60210011HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbceMemoryPool Tha60210011HbceMemoryPoolNew(ThaHbce hbce);
ThaHbceMemoryCell Tha60210011HbceMemoryCellNew(ThaHbceMemoryPool memoryPool, uint32 cellIndex);
ThaHbceEntry Tha60210011HbceEntryNew(ThaHbce hbce, uint32 entryIndex);
ThaHbce Tha60210011HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core);

uint32 Tha60210011HbceMaxNumHashCells(ThaHbce self);
uint32 Tha60210011HbceMaxNumHwMemPoolCells(ThaHbce self);
uint32 Tha60210011HbceNumExtraMemoryPointer(ThaHbce self);
eAtRet Tha60210011HbceHashEntryTabCtrlApply(ThaHbce self, uint32 entryIndex);

uint32 Tha60210011HbceMemoryPoolFreeExtraPointerListPop(ThaHbceMemoryPool self);
void Tha60210011HbceMemoryPoolFreeExtraPointerListPush(ThaHbceMemoryPool self, uint32 extraPointer);
void Tha60210011HbceMemoryPoolFreeExtraPointerInUsedSet(ThaHbceMemoryPool self, uint32 pointer);

uint32 Tha60210011HbceMemoryPoolCellHwFlowIdGet(ThaHbceMemoryPool self, uint32 cellIndex);
void Tha60210011HbceMemoryCellUse(ThaHbceMemoryCell self, eBool use);
eBool Tha60210011HbceMemoryCellIsUsed(ThaHbceMemoryCell self);
void Tha60210011HbceMemoryPoolApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex);

uint32 Tha60210011HbceHashCellIndex(ThaHbce self, uint32 hashEntryIndex, uint32 localCellIndex);
uint32 Tha60210011HbceExtraPointerCellIndex(ThaHbce self, uint32 extraPointerIndex, uint32 localCellIndex);
uint32 Tha60210011HbceHashEntryExtraPointerGet(ThaHbce self, ThaHbceEntry hashEntry);

uint32 Tha60210011HbceEntryNumHashCollisionFlowsGet(ThaHbceEntry self);
uint32 Tha60210011HbceEntryNumPoolCollisionFlowsGet(ThaHbceEntry self);
AtList Tha60210011HbceEntryHashCollisionFlowListGet(ThaHbceEntry self);
AtList Tha60210011HbceEntryPoolCollisionFlowListGet(ThaHbceEntry self);
ThaPwAdapter Tha60210011HbceMemoryPoolStandbyCellRestore(ThaHbceMemoryPool self, uint32 entryIndex, uint32 absoluteCellIndex);
uint32 Tha60210011ClaHbceLookingUpInformationCtrlPerCell(ThaHbceMemoryPool self, uint32 cellIndex);
eBool Tha60210011HbceMemoryPoolIsStandardCellIndex(ThaHbceMemoryPool self, uint32 cellIndex);
eAtRet Tha60210011HbceMemoryPoolCellInfoGet(ThaHbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId);

uint32 Tha60290011HbceMemoryPoolClaHbceLookingUpInformationCtrlPerCellOffset(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 blockIndex, uint32 indexInBlock);
uint8 Tha60210011HbceNumHashCollisionInOneHashIndex(Tha60210011Hbce self);
uint8 Tha60210011HbceNumHashCollisionInOneExtraPointer(Tha60210011Hbce self);

#endif /* _THA60210011HBCE_H_ */


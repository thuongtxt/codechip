/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210011HbceInternal.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : HBCE header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011HBCEINTERNAL_H_
#define _THA60210011HBCEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/util/ThaBitMask.h"
#include "Tha60210011Hbce.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/


typedef struct tTha60210011HbceMethods
    {
    uint32 (*NumExtraMemoryPointer)(Tha60210011Hbce self);
    uint32 (*MaxNumHashCells)(Tha60210011Hbce self);
    uint32 (*HashIndexGet)(Tha60210011Hbce self, uint32 hwLabel);
    eAtRet (*RemainBitsGet)(Tha60210011Hbce self, uint32 hwLabel, uint32 *remainBits);
    uint8  (*NumHashCollisionInOneHashIndex)(Tha60210011Hbce self);
    uint8  (*NumHashCollisionInOneExtraPointer)(Tha60210011Hbce self);
    uint32 (*HashEntryExtraPointerGet)(Tha60210011Hbce self, ThaHbceEntry hashEntry);
    uint32 (*ExtraPointerCellIndex)(Tha60210011Hbce self, uint32 extraPointerIndex, uint32 localCellIndex);
    uint32 (*HashCellIndex)(Tha60210011Hbce self, uint32 hashEntryIndex, uint32 localCellIndex);
    eAtRet (*HashEntryTabCtrlApply)(Tha60210011Hbce self, uint32 entryIndex);
    }tTha60210011HbceMethods;

typedef struct tTha60210011Hbce
    {
    tThaStmPwProductHbce super;
    const tTha60210011HbceMethods *methods;
    }tTha60210011Hbce;

typedef struct tTha60210011HbceMemoryPoolMethods
    {
    eBool (*CellIsPrimary)(ThaHbceMemoryPool self, uint32 absoluteCellIndex, uint32* groupId);
    uint32 (*ExtraCellLookupInformationCtrl)(ThaHbceMemoryPool self, uint32 cellIndex);
    uint32 (*ClaHbceLookingUpInformationCtrlPerCellOffset)(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 blockIndex, uint32 indexInBlock);
    uint32 (*ClaHbceLookingUpInformationCtrlPerCell)(ThaHbceMemoryPool self, uint32 cellIndex);
    eAtRet (*CellInfoGet)(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId);
    void (*ApplyCell)(Tha60210011HbceMemoryPool self, uint32 cellIndex);
    }tTha60210011HbceMemoryPoolMethods;

typedef struct tTha60210011HbceMemoryPool
    {
    tThaHbceMemoryPool super;
    const tTha60210011HbceMemoryPoolMethods *methods;

    /* Private data */
    ThaBitMask freeExtraPointerMask;
    }tTha60210011HbceMemoryPool;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceMemoryPool Tha60210011HbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011HBCEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011HbceMemoryCell.c
 *
 * Created Date: May 21, 2015
 *
 * Description : 60210011 HBCE Memory cell
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cla/hbce/ThaHbceEntityInternal.h"
#include "../../../../../util/coder/AtCoderUtil.h"
#include "Tha60210011Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011HbceMemoryCell *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011HbceMemoryCell
    {
    tThaHbceMemoryCell super;

    /* Private data */
    eBool isUsed;
    }tTha60210011HbceMemoryCell;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011HbceMemoryCell *object = (tTha60210011HbceMemoryCell *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(isUsed);
    }

static void OverrideAtObject(ThaHbceMemoryCell self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaHbceMemoryCell self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011HbceMemoryCell);
    }

static ThaHbceMemoryCell Tha60210011HbceMemoryCellObjectInit(ThaHbceMemoryCell self, ThaHbceMemoryPool memoryPool, uint32 cellIndex)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceMemoryCellObjectInit(self, memoryPool, cellIndex) == NULL)
        return NULL;

    /* Setup private data */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryCell Tha60210011HbceMemoryCellNew(ThaHbceMemoryPool memoryPool, uint32 cellIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryCell newCell = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCell == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011HbceMemoryCellObjectInit(newCell, memoryPool, cellIndex);
    }

void Tha60210011HbceMemoryCellUse(ThaHbceMemoryCell self, eBool use)
    {
    if (self)
        mThis(self)->isUsed = use;
    }

eBool Tha60210011HbceMemoryCellIsUsed(ThaHbceMemoryCell self)
    {
    return (eBool)(self ? mThis(self)->isUsed : cAtFalse);
    }


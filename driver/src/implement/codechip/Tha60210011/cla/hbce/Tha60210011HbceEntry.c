/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011HbceEntry.c
 *
 * Created Date: Feb 24, 2021
 *
 * Description : HBCE entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011Hbce.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../Tha60210011ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidUint32 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210011HbceEntry *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011HbceEntry
    {
    tThaHbceEntry super;

    /* Private data */
    AtList hashCollisionFlows;
    AtList poolCollisionFlows;
    }tTha60210011HbceEntry;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tThaHbceEntryMethods  m_ThaHbceEntryOverride;
static tThaHbceEntityMethods m_ThaHbceEntityOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;
static const tThaHbceEntryMethods  *m_ThaHbceEntryMethods  = NULL;
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumFlowsGet(ThaHbceEntry self)
	{
	return Tha60210011HbceEntryNumHashCollisionFlowsGet(self) + Tha60210011HbceEntryNumPoolCollisionFlowsGet(self);
	}

static ThaHbceMemoryCellContent CellContentAtIndex(ThaHbceEntry self, uint32 cellIndex)
    {
	uint32 numHashCollisions = Tha60210011HbceEntryNumHashCollisionFlowsGet(self);
	if (cellIndex < numHashCollisions)
		return (ThaHbceMemoryCellContent)AtListObjectGet(Tha60210011HbceEntryHashCollisionFlowListGet(self), cellIndex);

	return (ThaHbceMemoryCellContent)AtListObjectGet(Tha60210011HbceEntryPoolCollisionFlowListGet(self), cellIndex - numHashCollisions);
    }

static ThaHbce Hbce(ThaHbceEntry self)
	{
	ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
	return ThaClaPwControllerHbceForPart(ThaModuleClaPwControllerGet(claModule), 0);
	}

static eAtRet CellContentObjectAdd(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent)
	{
	ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(cellContent);
	uint32 swCellIndex = ThaHbceMemoryCellIndexGet(cell);

	if (swCellIndex < Tha60210011HbceMaxNumHashCells(Hbce(self)))
		return AtListObjectAdd(Tha60210011HbceEntryHashCollisionFlowListGet(self), (AtObject)cellContent);

	return AtListObjectAdd(Tha60210011HbceEntryPoolCollisionFlowListGet(self), (AtObject)cellContent);
	}

static eAtRet CellContentObjectRemove(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent)
	{
	ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(cellContent);
	uint32 swCellIndex = ThaHbceMemoryCellIndexGet(cell);

	if (swCellIndex < Tha60210011HbceMaxNumHashCells(Hbce(self)))
		return AtListObjectRemove(Tha60210011HbceEntryHashCollisionFlowListGet(self), (AtObject)cellContent);

	return AtListObjectRemove(Tha60210011HbceEntryPoolCollisionFlowListGet(self), (AtObject)cellContent);
	}

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->hashCollisionFlows));
    mThis(self)->hashCollisionFlows = NULL;
    AtObjectDelete((AtObject)(mThis(self)->poolCollisionFlows));
    mThis(self)->poolCollisionFlows = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210011HbceEntry * object = (tTha60210011HbceEntry *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    ThaHbceEntryFlowListSerialize((ThaHbceEntry)self, object->hashCollisionFlows, encoder);
    ThaHbceEntryFlowListSerialize((ThaHbceEntry)self, object->poolCollisionFlows, encoder);
    }

static ThaPwAdapter StandbyCellRestore(ThaHbceEntry self, uint32 entryIndex, uint32 absoluteCellIndex)
    {
    ThaHbceMemoryCell cell;
    ThaHbceMemoryCellContent cellContent;
    ThaPwAdapter adapter;
    eAtRet ret;
    ThaHbceMemoryPool pool = ThaHbceMemoryPoolGet(self->hbce);

    adapter = Tha60210011HbceMemoryPoolStandbyCellRestore(pool, entryIndex, absoluteCellIndex);
    if (adapter == NULL)
        return NULL;

    cell = ThaHbceMemoryPoolCellAtIndex(pool, absoluteCellIndex);
    cellContent = ThaHbceMemoryCellContentGet(cell);
    ret = ThaHbceEntryCellContentAdd(self, cellContent);
    if (ret != cAtOk)
        {
        AtModule module = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
        AtModuleLog(module, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot add cell %d to entry %d, ret = %s\r\n",
                    absoluteCellIndex + 1, entryIndex + 1, AtRet2String(ret));
        }

    return adapter;
    }

static uint32 StandbyCollisionPoolRestore(ThaHbceEntity self, uint32 entryIndex)
    {
    uint8 localCellIndex;
    ThaHbceEntry entry = (ThaHbceEntry)self;
    Tha60210011Hbce hbce = (Tha60210011Hbce)entry->hbce;

    for (localCellIndex = 0; localCellIndex < Tha60210011HbceNumHashCollisionInOneHashIndex(hbce); localCellIndex++)
        {
        uint32 cellIndex = Tha60210011HbceHashCellIndex(entry->hbce, entryIndex, localCellIndex);
        if (StandbyCellRestore(entry, entryIndex, cellIndex) == NULL)
            continue;
        }

    return 0;
    }

static uint32 StandbyExtraPointerRestore(ThaHbceEntity self, uint32 entryIndex, uint32 extraPointer)
    {
    ThaHbceEntry entry = (ThaHbceEntry)self;
    ThaHbce hbce = entry->hbce;
    uint32 numCells = Tha60210011HbceNumHashCollisionInOneExtraPointer((Tha60210011Hbce)hbce);
    uint32 cell_i;
    eBool removed = cAtFalse;
    ThaHbceMemoryPool memoryPool = ThaHbceMemoryPoolGet(hbce);

    for (cell_i = 0; cell_i < numCells; cell_i++)
        {
        uint32 cellIndex = Tha60210011HbceExtraPointerCellIndex(hbce, extraPointer, cell_i);

        if (StandbyCellRestore(entry, entryIndex, cellIndex) == NULL)
            continue;

        if (removed)
            continue;

        Tha60210011HbceMemoryPoolFreeExtraPointerInUsedSet(memoryPool, extraPointer);
        removed = cAtTrue;
        }

    return 0;
    }

static void RestoredDatabaseReset(ThaHbceEntity self)
    {
    if (mThis(self)->hashCollisionFlows)
        AtListFlush(mThis(self)->hashCollisionFlows);
    if (mThis(self)->poolCollisionFlows)
        AtListFlush(mThis(self)->poolCollisionFlows);
    }

static uint32 Restore(ThaHbceEntity self, uint8 page)
    {
    ThaHbceEntry entry = (ThaHbceEntry)self;
    uint32 entryIndex = ThaHbceEntryIndexGet(entry);
    uint32 extraPointerIndex;
    uint32 remain;
    Tha60210011Hbce hbce = (Tha60210011Hbce)entry->hbce;

    AtUnused(page);

    /* The current database might be already updated by previous restore
     * operations (only with autotest), so it is necessary to cleanup before this
     * current restore operation. Otherwise, duplicated entries will exist in
     * database */
    if (AtDriverKeepDatabaseAfterRestore())
        RestoredDatabaseReset(self);

    remain = StandbyCollisionPoolRestore(self, entryIndex);
    if (entry->standby->hitCounts <= Tha60210011HbceNumHashCollisionInOneHashIndex(hbce))
        return remain;

    extraPointerIndex = Tha60210011HbceHashEntryExtraPointerGet(entry->hbce, entry);
    remain = remain + StandbyExtraPointerRestore(self, entryIndex, extraPointerIndex);
    return remain;
    }

static void OverrideAtObject(ThaHbceEntry self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHbceEntry(ThaHbceEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntryMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntryOverride, mMethodsGet(self), sizeof(m_ThaHbceEntryOverride));

        mMethodOverride(m_ThaHbceEntryOverride, NumFlowsGet);
        mMethodOverride(m_ThaHbceEntryOverride, CellContentAtIndex);
        mMethodOverride(m_ThaHbceEntryOverride, CellContentObjectAdd);
        mMethodOverride(m_ThaHbceEntryOverride, CellContentObjectRemove);
        }

    mMethodsSet(self, &m_ThaHbceEntryOverride);
    }

static void OverrideThaHbceEntity(ThaHbceEntry self)
    {
    ThaHbceEntity hbceObject = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(hbceObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Restore);
        }

    mMethodsSet(hbceObject, &m_ThaHbceEntityOverride);
    }

static void Override(ThaHbceEntry self)
    {
	OverrideAtObject(self);
    OverrideThaHbceEntry(self);
    OverrideThaHbceEntity(self);
    }

static uint32 ObjectSize(void)
	{
	return sizeof(tTha60210011HbceEntry);
	}

static ThaHbceEntry ObjectInit(ThaHbceEntry self, ThaHbce hbce, uint32 entryIndex)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceEntryObjectInit(self, hbce, entryIndex) == NULL)
        return NULL;

    /* Set up class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceEntry Tha60210011HbceEntryNew(ThaHbce hbce, uint32 entryIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceEntry newEntry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEntry == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEntry, hbce, entryIndex);
    }

static AtList HashCollisionFlowListGet(ThaHbceEntry self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self->hbce;
    if (mThis(self)->hashCollisionFlows == NULL)
    	mThis(self)->hashCollisionFlows = AtListCreate(Tha60210011HbceNumHashCollisionInOneHashIndex(hbce));

    return mThis(self)->hashCollisionFlows;
    }

static AtList PoolCollisionFlowListGet(ThaHbceEntry self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self->hbce;
    if (mThis(self)->poolCollisionFlows == NULL)
    	mThis(self)->poolCollisionFlows = AtListCreate(Tha60210011HbceNumHashCollisionInOneExtraPointer(hbce));

    return mThis(self)->poolCollisionFlows;
    }

uint32 Tha60210011HbceEntryNumHashCollisionFlowsGet(ThaHbceEntry self)
    {
	return (self) ? AtListLengthGet(HashCollisionFlowListGet(self)) : 0;
    }

uint32 Tha60210011HbceEntryNumPoolCollisionFlowsGet(ThaHbceEntry self)
    {
	return (self) ? AtListLengthGet(PoolCollisionFlowListGet(self)) : 0;
    }

AtList Tha60210011HbceEntryHashCollisionFlowListGet(ThaHbceEntry self)
	{
	return (self) ? HashCollisionFlowListGet(self) : NULL;
	}

AtList Tha60210011HbceEntryPoolCollisionFlowListGet(ThaHbceEntry self)
	{
	return (self) ? PoolCollisionFlowListGet(self) : NULL;
	}

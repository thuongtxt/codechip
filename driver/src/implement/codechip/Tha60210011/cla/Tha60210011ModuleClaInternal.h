/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210011ModuleClaInternal.h
 * 
 * Created Date: May 28, 2015
 *
 * Description : CLA
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULECLAINTERNAL_H_
#define _THA60210011MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/cla/Tha60150011ModuleClaInternal.h"
#include "Tha60210011ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Debug sticky */
#define cEthernetSticky 0x60000F

#define sTha60210011ModuleClaRamClassifyPerGroupEnableControl "Classify Per Group Enable Control"
#define sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 "Classify HBCE Looking Up Information Control 2"
#define sTha60210011ModuleClaRamClassifyHBCEHashingTableControl "Classify HBCE Hashing Table Control"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModuleClaMethods
    {
    uint32 (*HbceFlowDirectMask)(Tha60210011ModuleCla self);
    uint32 (*HbceFlowDirectShift)(Tha60210011ModuleCla self);
    uint32 (*HbceGroupWorkingMask)(Tha60210011ModuleCla self);
    uint32 (*HbceGroupWorkingShift)(Tha60210011ModuleCla self);
    uint32 (*HbceGroupIdFlowMask1)(Tha60210011ModuleCla self);
    uint32 (*HbceGroupIdFlowShift1)(Tha60210011ModuleCla self);
    uint32 (*HbceGroupIdFlowMask2)(Tha60210011ModuleCla self);
    uint32 (*HbceGroupIdFlowShift2)(Tha60210011ModuleCla self);
    uint32 (*HbceFlowIdMask)(Tha60210011ModuleCla self);
    uint32 (*HbceFlowIdShift)(Tha60210011ModuleCla self);
    uint32 (*HbceFlowEnableMask)(Tha60210011ModuleCla self);
    uint32 (*HbceFlowEnableShift)(Tha60210011ModuleCla self);
    uint32 (*HbceStoreIdMask)(Tha60210011ModuleCla self);
    uint32 (*HbceStoreIdShift)(Tha60210011ModuleCla self);
    uint32 (*GroupEnableControlWorkingOffset)(Tha60210011ModuleCla self);
    uint32 (*EthPortCounterBase)(Tha60210011ModuleCla self, AtEthPort port);
    uint32 (*HbceFlowIdFromPw)(Tha60210011ModuleCla self, ThaPwAdapter pw);
    void (*PwClaRegShow)(Tha60210011ModuleCla self, AtPw pw);
    uint32 (*PortCounterOffset)(Tha60210011ModuleCla self, AtEthPort port, eBool r2c);
    eBool (*EthPortDebugShouldDisplay)(Tha60210011ModuleCla self, uint32 portId);
    eBool (*HasPwGroup)(Tha60210011ModuleCla self);

    /* Backward compatible */
    uint32 (*StartHwVersionControlSuppression)(Tha60210011ModuleCla self);

    /* Debug register */
    void (*DebugShowClaEthTypeSticky)(Tha60210011ModuleCla self);
    uint32 (*ClaStickyMacErrorMask)(Tha60210011ModuleCla self);
    uint32 (*ClaStickyPhyErrorMask)(Tha60210011ModuleCla self);
    uint32 (*ClaStickyRtpPtErrorMask)(Tha60210011ModuleCla self);
    }tTha60210011ModuleClaMethods;

typedef struct tTha60210011ModuleCla
    {
    tTha60150011ModuleCla super;
    const tTha60210011ModuleClaMethods *methods;

    /*async init*/
    uint32 asyncInitState;
    uint32 asyncRegResetState;
    }tTha60210011ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModuleClaObjectInit(AtModule self, AtDevice device);
uint32 Tha60210011ModuleClaSupperAsyncInit(AtModule self);
eAtRet Tha60210011ModuleClaAsyncInit(AtModule self);
uint32 Tha60210011ModuleClaNumPwGroup(AtModule self);
void Tha60210011ModuleClaOnePwGroupReset(AtModule self, uint32 group_i);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULECLAINTERNAL_H_ */


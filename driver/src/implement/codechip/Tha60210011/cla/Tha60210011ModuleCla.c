/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011ModuleCla.c
 *
 * Created Date: May 6, 2015
 *
 * Description : CLA Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../default/cla/hbce/ThaHbce.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../default/cla/hbce/ThaHbceEntry.h"
#include "../man/Tha60210011Device.h"
#include "../ram/Tha60210011InternalRam.h"
#include "Tha60210011ModuleClaReg.h"
#include "Tha60210011ModuleClaInternal.h"
#include "hbce/Tha60210011Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleCla)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tTha60210011ModuleClaMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tThaModuleClaMethods     m_ThaModuleClaOverride;
static tThaModuleClaPwMethods   m_ThaModuleClaPwOverride;
static tThaModuleClaPwV2Methods m_ThaModuleClaPwV2Override;
static tAtModuleMethods         m_AtModuleOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ModuleClaBaseAddress(void)
    {
    return cThaRegClaBaseAddress;
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return Tha60210011ClaPwEthPortControllerNew(self);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60210011ClaPwControllerNew(self);
    }

static uint32 ClaPwTypeCtrlReg(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6Reg_cla_per_pw_ctrl_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsClaRegister(AtModuleDeviceGet(self), address);
    }

static uint32 ClaPwTypePwLenMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6_cla_per_pw_ctrl_RxEthPwLen_Mask;
    }

static uint8 ClaPwTypePwLenShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6_cla_per_pw_ctrl_RxEthPwLen_Shift;
    }

static uint32 ClaPwTypeCepModeMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6_cla_per_pw_ctrl_RxEthCepMode_Mask;
    }

static uint8 ClaPwTypeCepModeShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6_cla_per_pw_ctrl_RxEthCepMode_Shift;
    }

static uint32 ClaPwEbmCpuModeMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_Mask;
    }

static uint8  ClaPwEbmCpuModeShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6_cla_per_pw_ctrl_RxEthPwOnflyVC34_Shift;
    }

static void OneRegShow(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModuleCla);
    }

static uint32 GroupEnableControlWorkingOffset(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return 0x1000;
    }

static eBool PwGroupingShow(ThaModuleCla self, AtPw pw)
    {
    AtPwGroup pwGroup = AtPwHsGroupGet(pw);
    AtUnused(self);
    if (pwGroup)
        {
        OneRegShow(pw, "Classify Per Group Enable Control (primary)",
                   cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) + mMethodsGet(mThis(self))->GroupEnableControlWorkingOffset(mThis(self)) + ModuleClaBaseAddress());
        OneRegShow(pw, "Classify Per Group Enable Control (backup)",
                   cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) + ModuleClaBaseAddress());
        return cAtTrue;
        }

    return cAtFalse;
    }

static void OneLongRegShow(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegLongValueDisplay((AtChannel)pw, address, cThaModuleCla);
    }

static void HbceShow(ThaModuleCla self, ThaPwHeaderController headerController)
    {
    ThaPwAdapter pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    ThaClaPwController pwController = ThaPwAdapterClaPwController(pwAdapter);
    ThaHbce hbce;
    ThaHbceMemoryPool memPool;
    ThaHbceMemoryCellContent cellContent;
    ThaHbceMemoryCell cell;
    ThaHbceEntry hbceEntry;
    uint32 hashIndex;
    uint32 cellIndex;

    AtUnused(self);

    cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(headerController);
    if (cellContent == NULL)
        return;

    if (ThaPwHeaderControllerIsPrimary(headerController))
        AtPrintc(cSevInfo, "* Pw HBCE information:\r\n");
    else
        AtPrintc(cSevInfo, "* Pw Backup HBCE information:\r\n");

    hbceEntry = ThaHbceMemoryCellContentHashEntryGet(cellContent);
    hashIndex = ThaHbceEntryIndexGet(hbceEntry);
    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    hbce = ThaPwAdapterHbceGet((AtPw)pwAdapter);
    memPool = ThaHbceMemoryPoolGet(hbce);
    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    AtPrintc(cSevInfo, "   - Hash Index: %d\r\n", hashIndex);
    AtPrintc(cSevInfo, "   - SW cell Index: %d\r\n", cellIndex);

    if (ThaModuleClaCLAHBCEHashingTabCtrlIsUsed(self))
        OneRegShow((AtPw)pwAdapter, "Classify HBCE Hashing Table Control",
               ThaClaPwControllerCLAHBCEHashingTabCtrl(pwController) + hashIndex);

    OneLongRegShow((AtPw)pwAdapter, "Classify HBCE Looking Up Information Control",
                   Tha60210011ClaHbceLookingUpInformationCtrlPerCell(memPool, cellIndex));
    }

static void PwClaRegShow(Tha60210011ModuleCla self, AtPw pw)
    {
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    OneLongRegShow(pw, "Classify Per Pseudowire Type Control",
                   ThaModuleClaPwV2ClaPwTypeCtrlReg((ThaModuleClaPwV2)self) + pwId);

    OneRegShow(pw, "Classify Per Pseudowire Slice to CDR Control",
               cAf6Reg_cla_2_cdr_cfg_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self) + pwId);
    }

static void PwRegsShow(ThaModuleCla self, AtPw pw)
    {
    ThaPwAdapter pwAdapter = ThaPwAdapterGet(pw);

    mMethodsGet(mThis(self))->PwClaRegShow(mThis(self), pw);
    HbceShow(self, ThaPwAdapterHeaderController(pwAdapter));
    HbceShow(self, ThaPwAdapterBackupHeaderController(pwAdapter));

    AtPrintc(cSevInfo, "* Pw grouping information:\r\n");
    if (PwGroupingShow(self, pw) == cAtFalse)
        AtPrintc(cSevNormal, "        Pw does not join to any group\r\n");
    }

static uint32 NumPwGroup(AtModule self)
    {
    AtUnused(self);
    return 4096;
    }

static void OnePwGroupReset(AtModule self, uint32 group_i)
    {
    uint32 address;
        address = cAf6Reg_cla_per_grp_enb_Base + group_i + mMethodsGet(mThis(self))->GroupEnableControlWorkingOffset(mThis(self)) + ModuleClaBaseAddress();
        mModuleHwWrite(self, address, 0x0);
        address = cAf6Reg_cla_per_grp_enb_Base + group_i + ModuleClaBaseAddress();
        mModuleHwWrite(self, address, 0x0);
        }

static void PwGroupReset(AtModule self)
    {
    uint32 group_i;
    for (group_i = 0; group_i < NumPwGroup(self); group_i++)
        {
        OnePwGroupReset(self, group_i);
        }
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);

    if (mMethodsGet(mThis(self))->HasPwGroup(mThis(self)))
        PwGroupReset(self);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Tha60210011ModuleClaAsyncInit(self);
    }

static void DebugPrint(uint32 stickyRaised, const char *debugString)
    {
    eAtSevLevel color = stickyRaised ? cSevCritical : cSevInfo;
    AtPrintc(cSevNormal, "%-30s: ", debugString);
    AtPrintc(color, "%s\r\n", stickyRaised ? "SET" : "CLEAR");
    }

static void DebugOkPrint(uint32 stickyRaised, const char *debugString)
    {
    eAtSevLevel color = stickyRaised ? cSevDebug : cSevNormal;
    AtPrintc(cSevNormal, "%-30s: ", debugString);
    AtPrintc(color, "%s\r\n", stickyRaised ? "OK" : "CLEAR");
    }

static eAtRet Debug(AtModule self)
    {
    uint8 i;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet(self), 0);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleEth);

    mModuleHwLongRead(self, cEthernetSticky, longRegVal, cThaLongRegMaxSize, core);
    AtPrintc(cSevNormal, "\n0x60000F: 0x%08x.%08x.%08x.%08x\r\n", longRegVal[3], longRegVal[2], longRegVal[1], longRegVal[0]);

    for (i = 0; i < AtModuleEthMaxPortsGet(ethModule); i++)
        {
        if (!mMethodsGet(mThis(self))->EthPortDebugShouldDisplay(mThis(self), i))
            continue;

        AtPrintc(cSevNormal, "\r\n");
        AtPrintc(cSevInfo, "port %d\r\n", i);
        DebugPrint((longRegVal[0] & cEthPortDataEthFullMask(i)), "eth port data eth full");
        DebugPrint((longRegVal[0] & cEthPortGrayEthFullMask(i)), "eth port gray eth full");
        DebugPrint((longRegVal[0] & cEthPortLostSopMask(i)), "eth port lost sop");
        DebugPrint((longRegVal[0] & cEthPortLostEopMask(i)), "eth port lost eop");
        DebugPrint((longRegVal[0] & cEthPortPktShortMask(i)), "eth port pkt short");
        DebugPrint((longRegVal[0] & cEthPortPktOverMask(i)), "eth port pkt over");
        DebugOkPrint((longRegVal[1] & cEth2ClaMask(i)), "eth2cla");
        DebugOkPrint((longRegVal[1] & cCla2PdaMask(i)), "cla2pda");
        DebugPrint((longRegVal[1] & cCla2PdaErrMask(i)), "cla2pda error");
        DebugOkPrint((longRegVal[1] & cCla2pdaSopPort(i)), "cla2pda_sop");
        DebugOkPrint((longRegVal[1] & cCla2pdaEopPort(i)), "cla2pda_eop");
        DebugPrint((longRegVal[1] & cCla2pdaMissSopPort(i)), "miss sop");
        DebugPrint((longRegVal[1] & cCla2pdaMissEopPort(i)), "miss eop");
        }

    AtPrintc(cSevNormal, "\r\n");

    AtPrintc(cSevInfo, "Global\r\n");
    DebugOkPrint((longRegVal[1] & cEthPwLkGoodMask), "pw lookup good");
    DebugPrint((longRegVal[1] & cEthPwLkFailMask), "pw lookup fail");
    DebugPrint((longRegVal[2] & cClaLengthErrorMask), "CLA length error");
    DebugPrint((longRegVal[2] & cCwErrorMask), "CW error");
    DebugPrint((longRegVal[2] & cPwDisable), "pw disable");
    mMethodsGet(mThis(self))->DebugShowClaEthTypeSticky(mThis(self));
    DebugPrint((longRegVal[2] & mMethodsGet(mThis(self))->ClaStickyMacErrorMask(mThis(self))), "MAC error");
    DebugPrint((longRegVal[2] & mMethodsGet(mThis(self))->ClaStickyPhyErrorMask(mThis(self))), "PHY error");

    DebugPrint((longRegVal[2] & cRtpSsrcErrMask), "RTP_ssrc err");
    DebugPrint((longRegVal[2] & cUdpCheckSumErrMask), "UDP Checksum err");
    DebugPrint((longRegVal[2] & cIpCheckSumErrMask), "IP checksum err");
    DebugPrint((longRegVal[2] & mMethodsGet(mThis(self))->ClaStickyRtpPtErrorMask(mThis(self))), "RTP_PT err ");
    DebugPrint((longRegVal[2] & cPwCfgDisable), "pw cfg disable");

    AtPrintc(cSevNormal, "\r\n");

    AtOsalMemInit(longRegVal, 0xFF, sizeof(longRegVal));
    mModuleHwLongWrite(self, cEthernetSticky, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if ((localAddress >= 0x0600000) && (localAddress <= 0x06FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60210011InternalRamClaNew(self, ramId, localRamId);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    AtUnused(self);
    return (AtInternalRamLocalIdGet(ram) == 5) ? cAtTrue : cAtFalse;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "Classify HBCE Looking Up Information Control 1, Group 0",
         "Classify HBCE Looking Up Information Control 1, Group 1",
         "Classify HBCE Looking Up Information Control 1, Group 2",
         "Classify HBCE Looking Up Information Control 1, Group 3",
         "Classify Per Pseudowire Type Control",
         "Classify Per Pseudowire MEF Over MPLS Control",
         sTha60210011ModuleClaRamClassifyPerGroupEnableControl,
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 0",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 1",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 2",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 3",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 4",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 5",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 6",
         sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2 ", Group 7",
         sTha60210011ModuleClaRamClassifyHBCEHashingTableControl,
         "Classify Per Pseudowire Identification to CDR Control"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static uint32 HbceFlowDirectMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Mask;
    }

static uint32 HbceFlowDirectShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Shift;
    }

static uint32 HbceGroupWorkingMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Mask;
    }

static uint32 HbceGroupWorkingShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Shift;
    }

static uint32 HbceGroupIdFlowMask1(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_01;
    }

static uint32 HbceGroupIdFlowShift1(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_01;
    }

static uint32 HbceGroupIdFlowMask2(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_02;
    }

static uint32 HbceGroupIdFlowShift2(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_02;
    }

static uint32 HbceFlowIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask;
    }

static uint32 HbceFlowIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift;
    }

static uint32 HbceFlowEnableMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask;
    }

static uint32 HbceFlowEnableShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift;
    }

static uint32 HbceStoreIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask;
    }

static uint32 HbceStoreIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift;
    }

static void DebugShowClaEthTypeSticky(Tha60210011ModuleCla self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);

    mModuleHwLongRead(self, cEthernetSticky, longRegVal, cThaLongRegMaxSize, core);
    DebugPrint((longRegVal[2] & cEthTypeError), "ETH_TYPE error");
    }

static uint32 ClaStickyMacErrorMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cMacError;
    }

static uint32 ClaStickyPhyErrorMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cPhyError;
    }

static uint32 ClaStickyRtpPtErrorMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cRtpPtErrMask;
    }

static uint32 PortCounterOffset(Tha60210011ModuleCla self, AtEthPort port, eBool r2c)
    {
    return (AtChannelIdGet((AtChannel)port) + ((r2c) ? 0x800 : 0x0) + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self));
    }

static uint32 HbceFlowIdFromPw(Tha60210011ModuleCla self, ThaPwAdapter pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw);
    }

static AtModulePw PwModule(Tha60210011ModuleCla self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static ThaPwAdapter HbcePwFromFlowId(ThaModuleCla self, uint32 flowId)
    {
    return ThaPwAdapterGet(AtModulePwGetPw(PwModule((Tha60210011ModuleCla)self), flowId));
    }

static uint32 StartHwVersionControlSuppression(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x0);
    }

static eBool CanControlPwSuppression(ThaModuleClaPw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= mMethodsGet(mThis(self))->StartHwVersionControlSuppression(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool EthPortDebugShouldDisplay(Tha60210011ModuleCla self, uint32 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtTrue;
    }

static eBool HasPwGroup(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011ModuleCla object = (Tha60210011ModuleCla)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncInitState);
    mEncodeUInt(asyncRegResetState);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, PwRegsShow);
        mMethodOverride(m_ThaModuleClaOverride, HbcePwFromFlowId);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideThaModuleClaPw(AtModule self)
    {
    ThaModuleClaPw claModule = (ThaModuleClaPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwOverride));

        mMethodOverride(m_ThaModuleClaPwOverride, CanControlPwSuppression);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwOverride);
    }

static void OverrideThaModuleClaPwV2(AtModule self)
    {
    ThaModuleClaPwV2 claModuleV2 = (ThaModuleClaPwV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, mMethodsGet(claModuleV2), sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypeCtrlReg);
        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypePwLenMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypePwLenShift);
        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypeCepModeMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypeCepModeShift);
        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwEbmCpuModeMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwEbmCpuModeShift);
        }

    mMethodsSet(claModuleV2, &m_ThaModuleClaPwV2Override);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPw(self);
    OverrideThaModuleClaPwV2(self);
    OverrideAtModule(self);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210011ModuleCla module = (Tha60210011ModuleCla)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HbceFlowDirectMask);
        mMethodOverride(m_methods, HbceFlowDirectShift);
        mMethodOverride(m_methods, HbceGroupWorkingMask);
        mMethodOverride(m_methods, HbceGroupWorkingShift);
        mMethodOverride(m_methods, HbceGroupIdFlowMask1);
        mMethodOverride(m_methods, HbceGroupIdFlowShift1);
        mMethodOverride(m_methods, HbceGroupIdFlowMask2);
        mMethodOverride(m_methods, HbceGroupIdFlowShift2);
        mMethodOverride(m_methods, HbceFlowIdMask);
        mMethodOverride(m_methods, HbceFlowIdShift);
        mMethodOverride(m_methods, HbceFlowEnableMask);
        mMethodOverride(m_methods, HbceFlowEnableShift);
        mMethodOverride(m_methods, HbceStoreIdMask);
        mMethodOverride(m_methods, HbceStoreIdShift);
        mMethodOverride(m_methods, PwClaRegShow);
        mMethodOverride(m_methods, GroupEnableControlWorkingOffset);
        mMethodOverride(m_methods, PortCounterOffset);
        mMethodOverride(m_methods, HbceFlowIdFromPw);
        mMethodOverride(m_methods, EthPortDebugShouldDisplay);
        mMethodOverride(m_methods, StartHwVersionControlSuppression);
        mMethodOverride(m_methods, DebugShowClaEthTypeSticky);
        mMethodOverride(m_methods, ClaStickyMacErrorMask);
        mMethodOverride(m_methods, ClaStickyPhyErrorMask);
        mMethodOverride(m_methods, ClaStickyRtpPtErrorMask);
        mMethodOverride(m_methods, HasPwGroup);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModuleCla);
    }

AtModule Tha60210011ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModuleClaObjectInit(newModule, device);
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaPwHeaderController controller)
    {
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(controller);
    ThaHbce hbce = ThaPwAdapterHbceGet((AtPw)adapter);
    ThaHbceMemoryCellContent cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(controller);
    ThaHbceMemoryCell cell;
    uint32 cellIndex;
    ThaHbceMemoryPool memPool;

    if (cellContent == NULL)
        return cInvalidUint32;

    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    memPool = ThaHbceMemoryPoolGet(hbce);

    return Tha60210011ClaHbceLookingUpInformationCtrlPerCell(memPool, cellIndex);
    }

static void HsGroupPwAdd(ThaModuleCla self, AtPw pwAdapter, AtPwGroup pwGroup, uint32 regAddr, eBool isPrimary)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 groupId = AtPwGroupIdGet(pwGroup);
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)self;
    uint32 tailGroupId = groupId & (Tha60210011ModuleClaHbceGroupIdFlowMask1(claModule) >> Tha60210011ModuleClaHbceGroupIdFlowShift1(claModule));

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[1],
              Tha60210011ModuleClaHbceFlowDirectMask(claModule),
              Tha60210011ModuleClaHbceFlowDirectShift(claModule),
              0);

    if (Tha60210011ModuleClaHbceGroupIdFlowMask2(claModule) != 0)
        {
        uint32 headGroupId = groupId >> (32 - Tha60210011ModuleClaHbceGroupIdFlowShift1(claModule));
        mFieldIns(&longRegVal[1], Tha60210011ModuleClaHbceGroupIdFlowMask2(claModule), Tha60210011ModuleClaHbceGroupIdFlowShift2(claModule), headGroupId);
        }

    mFieldIns(&longRegVal[0],
              Tha60210011ModuleClaHbceGroupIdFlowMask1(claModule),
              Tha60210011ModuleClaHbceGroupIdFlowShift1(claModule),
              tailGroupId);

    mFieldIns(&longRegVal[1],
              Tha60210011ModuleClaHbceGroupWorkingMask(claModule),
              Tha60210011ModuleClaHbceGroupWorkingShift(claModule),
              mBoolToBin(isPrimary));

    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

eAtRet Tha60210011ModuleClaHsGroupPwAdd(ThaModuleCla self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    ThaPwHeaderController headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pwAdapter);
    ThaPwHeaderController backupHeaderController = ThaPwAdapterBackupHeaderController((ThaPwAdapter)pwAdapter);
    uint32 regAddress;
    AtUnused(self);

    regAddress = ClaHbceLookingUpInformationCtrl(headerController);
    if (regAddress != cInvalidUint32)
        HsGroupPwAdd(self, pwAdapter, pwGroup, regAddress, cAtTrue);

    regAddress = ClaHbceLookingUpInformationCtrl(backupHeaderController);
    if (regAddress != cInvalidUint32)
        HsGroupPwAdd(self, pwAdapter, pwGroup, regAddress, cAtFalse);

    return cAtOk;
    }

static void HsGroupPwRemove(ThaModuleCla self, AtPw pwAdapter, uint32 regAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)self;

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[1],
              Tha60210011ModuleClaHbceFlowDirectMask(claModule),
              Tha60210011ModuleClaHbceFlowDirectShift(claModule),
              1);
    mFieldIns(&longRegVal[1],
              Tha60210011ModuleClaHbceGroupWorkingMask(claModule),
              Tha60210011ModuleClaHbceGroupWorkingShift(claModule),
              0);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

eAtRet Tha60210011ModuleClaHsGroupPwRemove(ThaModuleCla self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    ThaPwHeaderController headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pwAdapter);
    ThaPwHeaderController backupHeaderController = ThaPwAdapterBackupHeaderController((ThaPwAdapter)pwAdapter);
    uint32 regAddress;

    AtUnused(self);
    AtUnused(pwGroup);

    regAddress = ClaHbceLookingUpInformationCtrl(headerController);
    if (regAddress != cInvalidUint32)
        HsGroupPwRemove(self, pwAdapter, regAddress);

    regAddress = ClaHbceLookingUpInformationCtrl(backupHeaderController);
    if (regAddress != cInvalidUint32)
        HsGroupPwRemove(self, pwAdapter, regAddress);

    return cAtOk;
    }

eAtRet Tha60210011ModuleClaHsGroupEnable(ThaModuleCla self, AtPwGroup pwGroup, eBool enable)
    {
    eAtPwGroupLabelSet labelSet = AtPwGroupRxSelectedLabelGet(pwGroup);
    uint32 workingOffset = (labelSet == cAtPwGroupLabelSetPrimary) ?  mMethodsGet(mThis(self))->GroupEnableControlWorkingOffset(mThis(self)) : 0;
    uint32 regAddr = cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) + workingOffset + ModuleClaBaseAddress();

    mModuleHwWrite(self, regAddr, mBoolToBin(enable));
    return cAtOk;
    }

eAtRet Tha60210011ModuleClaHsGroupIsEnabled(ThaModuleCla self, AtPwGroup pwGroup)
    {
    eAtPwGroupLabelSet labelSet = AtPwGroupRxSelectedLabelGet(pwGroup);
    uint32 workingOffset = (labelSet == cAtPwGroupLabelSetPrimary) ?  mMethodsGet(mThis(self))->GroupEnableControlWorkingOffset(mThis(self)) : 0;
    uint32 regAddr = cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) + workingOffset + ModuleClaBaseAddress();
    uint32 regVal  = mModuleHwRead(self, regAddr);

    return mBinToBool(mRegField(regVal, cAf6_cla_per_grp_enb_CLAGrpPWEn_));
    }

eAtRet Tha60210011ModuleClaHsGroupLabelSetSelect(ThaModuleCla self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    uint32 regAddr = cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) + ModuleClaBaseAddress();
    mModuleHwWrite(self, regAddr, (labelSet == cAtPwGroupLabelSetPrimary) ? 0 : 1);

    regAddr = cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) +  mMethodsGet(mThis(self))->GroupEnableControlWorkingOffset(mThis(self)) + ModuleClaBaseAddress();
    mModuleHwWrite(self, regAddr, (labelSet == cAtPwGroupLabelSetPrimary) ? 1 : 0);

    return cAtOk;
    }

eAtRet Tha60210011ModuleClaHsGroupSelectedLabelSetGet(ThaModuleCla self, AtPwGroup pwGroup)
    {
    uint32 regAddr = cAf6Reg_cla_per_grp_enb_Base + AtPwGroupIdGet(pwGroup) + ModuleClaBaseAddress();
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal == 0) ? cAtPwGroupLabelSetPrimary : cAtPwGroupLabelSetBackup;
    }

uint32 Tha60210011ModuleClaBaseAddress(ThaModuleCla self)
    {
    return (self) ? ModuleClaBaseAddress() : 0;
    }

uint32 Tha60210011ModuleClaHbceFlowDirectMask(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceFlowDirectMask(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceFlowDirectShift(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceFlowDirectShift(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceGroupWorkingMask(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceGroupWorkingMask(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceGroupWorkingShift(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceGroupWorkingShift(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceGroupIdFlowMask1(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceGroupIdFlowMask1(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceGroupIdFlowShift1(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceGroupIdFlowShift1(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceGroupIdFlowMask2(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceGroupIdFlowMask2(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceGroupIdFlowShift2(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceGroupIdFlowShift2(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceFlowIdMask(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceFlowIdMask(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceFlowIdShift(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceFlowIdShift(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceFlowEnableMask(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceFlowEnableMask(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceFlowEnableShift(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceFlowEnableShift(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceStoreIdMask(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceStoreIdMask(self) : 0;
    }

uint32 Tha60210011ModuleClaHbceStoreIdShift(Tha60210011ModuleCla self)
    {
    return (self) ? mMethodsGet(self)->HbceStoreIdShift(self) : 0;
    }

uint32 Tha60210011ModuleClaPortCounterOffset(Tha60210011ModuleCla self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PortCounterOffset(self, port, r2c);
    return 0;
    }

uint32 Tha60210011ModuleClaHbceFlowIdFromPw(Tha60210011ModuleCla self, ThaPwAdapter pw)
    {
    if (self)
        return mMethodsGet(self)->HbceFlowIdFromPw(self, pw);
    return 0;
    }

uint32 Tha60210011ModuleClaSupperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

uint32 Tha60210011ModuleClaNumPwGroup(AtModule self)
    {
    return NumPwGroup(self);
    }

void Tha60210011ModuleClaOnePwGroupReset(AtModule self, uint32 group_i)
    {
    OnePwGroupReset(self, group_i);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210011ModuleClaAsyncInit.c
 *
 * Created Date: Aug 18, 2016
 *
 * Description : Async init implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../default/cla/hbce/ThaHbce.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../default/cla/hbce/ThaHbceEntry.h"
#include "../man/Tha60210011Device.h"
#include "../ram/Tha60210011InternalRam.h"
#include "Tha60210011ModuleClaReg.h"
#include "Tha60210011ModuleClaInternal.h"
#include "hbce/Tha60210011Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011ModuleCla)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eTha60210011ModuleClaState
    {
    cTha60210011ModuleClaStateSupperInit = 0,
    cTha60210011ModuleClaStateRegReset
    }eTha60210011ModuleClaState;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 MaxEntryPerAsyncInitGet(AtModule self, uint32 state)
    {
    uint32 max0 = (state + 1)*1000;
    uint32 max1 = Tha60210011ModuleClaNumPwGroup(self);

    if (max0 < max1)
        return max0;
    return max1;
    }

static uint32 StartEntryPerAsyncInitGet(AtModule self, uint32 state)
    {
    AtUnused(self);
    return state * 1000;
    }

static void AsyncInitRegResetStateSet(AtModule self, uint32 state)
    {
    mThis(self)->asyncRegResetState = state;
    }

static uint32 AsyncInitRegResetStateGet(AtModule self)
    {
    return mThis(self)->asyncRegResetState;
    }

static eAtRet Tha60210011ModuleClaPwGroupAsyncReset(AtModule self)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 state = AsyncInitRegResetStateGet(self);
    uint32 maxEntry = MaxEntryPerAsyncInitGet(self, state);
    tAtOsalCurTime profileTime;
    char stateString[64];

    AtSprintf(stateString, "state: %d", state);
    AtOsalCurTimeGet(&profileTime);

    for (i = StartEntryPerAsyncInitGet(self, state); i < maxEntry; i++)
        Tha60210011ModuleClaOnePwGroupReset(self, i);

    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);

    state += 1;
    if (maxEntry == Tha60210011ModuleClaNumPwGroup(self))
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncInitRegResetStateSet(self, state);
    return ret;
    }

static void AsyncInitStateSet(AtModule self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

static uint32 AsyncInitStateGet(AtModule self)
    {
    return mThis(self)->asyncInitState;
    }

eAtRet Tha60210011ModuleClaAsyncInit(AtModule self)
    {
    uint32 state = AsyncInitStateGet(self);
    char stateString[32];
    tAtOsalCurTime profileTime;
    eAtRet ret = cAtOk;

    AtSprintf(stateString, "state: %d", state);
    AtOsalCurTimeGet(&profileTime);

    switch(state)
        {
        case cTha60210011ModuleClaStateSupperInit:
            ret = Tha60210011ModuleClaSupperAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);
            if (ret == cAtOk)
                {
                state = cTha60210011ModuleClaStateRegReset;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cTha60210011ModuleClaStateSupperInit;
            break;
        case cTha60210011ModuleClaStateRegReset:
            ret = Tha60210011ModuleClaPwGroupAsyncReset(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, AtSourceLocation, stateString);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, AtSourceLocation, stateString);
            if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
                state = cTha60210011ModuleClaStateSupperInit;

            break;
        default:
            state = cTha60210011ModuleClaStateSupperInit;
            ret = cAtErrorDevFail;
        }

    AsyncInitStateSet(self, state);
    return ret;
    }

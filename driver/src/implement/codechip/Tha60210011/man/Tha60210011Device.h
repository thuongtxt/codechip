/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210011Device.h
 * 
 * Created Date: May 11, 2015
 *
 * Description : Product 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011DEVICE_H_
#define _THA60210011DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60150011/man/Tha60150011Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011Device * Tha60210011Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210011DeviceHasHoBus(Tha60210011Device self);

eBool Tha60210011IsCdrHoRegister(AtDevice self, uint32 address);
eBool Tha60210011IsCdrLoRegister(AtDevice self, uint32 address);
eBool Tha60210011IsCdrRegister(AtDevice self, uint32 address);
eBool Tha60210011IsClaRegister(AtDevice self, uint32 address);
eBool Tha60210011IsCosRegister(AtDevice self, uint32 address);
eBool Tha60210011IsEthRegister(AtDevice self, uint32 address);
eBool Tha60210011IsMapHoRegister(AtDevice self, uint32 address);
eBool Tha60210011IsMapLoRegister(AtDevice self, uint32 address);
eBool Tha60210011IsMapRegister(AtDevice self, uint32 address);
eBool Tha60210011IsDemapRegister(AtDevice self, uint32 address);
eBool Tha60210011IsOcnRegister(AtDevice self, uint32 address);
eBool Tha60210011IsOcnTfi5SideRegister(AtDevice self, uint32 address);
eBool Tha60210011IsOcnTfi5SerdesRegister(AtDevice self, uint32 address);
eBool Tha60210011IsOcnTerminatedSideRegister(AtDevice self, uint32 address);
eBool Tha60210011IsPdhRegister(AtDevice self, uint32 address);
eBool Tha60210011IsPmcRegister(AtDevice self, uint32 address);
eBool Tha60210011IsBerRegister(AtDevice self, uint32 address);
eBool Tha60210011IsPdaRegister(AtDevice self, uint32 address);
eBool Tha60210011IsPweRegister(AtDevice self, uint32 address);
eBool Tha60210011IsPlaRegister(AtDevice self, uint32 address);
eBool Tha60210011IsPohRegister(AtDevice self, uint32 address);

uint32 Tha60210011DeviceStartVersionSupportUpsr(AtDevice self);
uint32 Tha60210011DeviceStartVersionSupportFullPwCounters(AtDevice self);
eAtRet Tha60210011DeviceXfiDiagPerGroupEnable(AtDevice self, uint8 groupId, eBool diag);

/* Capacity of sub modules */
uint8 Tha60210011DeviceNumUsedOc48Slices(Tha60210011Device self);

uint32 Tha60210011DeviceInterruptBaseAddress(Tha60210011Device self);
eBool Tha6021ShouldDisableHwAccessBeforeDeleting(AtDevice self);
eBool Tha60210011RamErrorGeneratorIsSupported(AtDevice self);

/* New HwReseet with SEU waiting for idle before hwreset */
eAtRet Tha60210011DeviceHwResetWithHandler(AtDevice self, eBool shouldApplyNewReset, eAtRet (*handler)(AtDevice dev));
eAtRet Tha60210011DeviceMoveAllSemControllersToState(AtDevice self, eAtSemControllerAlarmType state);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011DEVICE_H_ */


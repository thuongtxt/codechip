/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210011DeviceRegister.c
 *
 * Created Date: May 11, 2015
 *
 * Description : Register assignment
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../../../../generic/physical/AtSerdesManagerInternal.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../ocn/Tha60210011ModuleOcn.h"
#include "../physical/Tha60210011Tfi5SerdesControllerReg.h"
#include "Tha60210011Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsEthSerdesRegister(AtDevice self, uint32 address)
    {
    AtSerdesManager manager = AtDeviceSerdesManagerGet(self);
    if (manager)
        return AtSerdesManagerCanAccessRegister(manager, 0, address, cAtModuleEth);
    return cAtTrue;
    }

eBool Tha60210011IsCdrHoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0300000, 0x033FFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsCdrLoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if (mInRange(address, 0x0C00000, 0x0C3FFFF) ||
        mInRange(address, 0x0C40000, 0x0C7FFFF) ||
        mInRange(address, 0x0C80000, 0x0CBFFFF) ||
        mInRange(address, 0x0CC0000, 0x0CFFFFF) ||
        mInRange(address, 0x0D00000, 0x0D3FFFF) ||
        mInRange(address, 0x0D40000, 0x0D7FFFF) ||
        mInRange(address, 0x0D80000, 0x0DBFFFF) ||
        mInRange(address, 0x0DC0000, 0x0DFFFFF) ||
        mInRange(address, 0x0E00000, 0x0E3FFFF) ||
        mInRange(address, 0x0E40000, 0x0E7FFFF) ||
        mInRange(address, 0x0E80000, 0x0EBFFFF) ||
        mInRange(address, 0x0EC0000, 0x0EFFFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210011IsCdrRegister(AtDevice self, uint32 address)
    {
    return Tha60210011IsCdrHoRegister(self, address) || Tha60210011IsCdrLoRegister(self, address);
    }

eBool Tha60210011IsClaRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0600000, 0x06FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsCosRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (mInRange(address, 0x0070000, 0x007FFFF) ||
        mInRange(address, 0x0080000, 0x008FFFF) ||
        mInRange(address, 0x0090000, 0x009FFFF) ||
        mInRange(address, 0x00A0000, 0x00AFFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210011IsEthRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if (address == 0x10) /* Register to control serdes switch/bridge */
        return cAtTrue;

    if (mInRange(address, 0x0020000, 0x002FFFF) ||
        mInRange(address, 0x0030000, 0x003FFFF) ||
        mInRange(address, 0x0040000, 0x004FFFF) ||
        mInRange(address, 0x0050000, 0x005FFFF) ||
        mInRange(address, 0xF00040, 0xF00046)   ||
        mInRange(address, 0xF45040, 0xF45044))
        return cAtTrue;

    return IsEthSerdesRegister(self, address);
    }

eBool Tha60210011IsMapHoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0060000, 0x006FFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsMapLoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (mInRange(address, 0x0800000, 0x083FFFF) ||
        mInRange(address, 0x0840000, 0x087FFFF) ||
        mInRange(address, 0x0880000, 0x08BFFFF) ||
        mInRange(address, 0x08C0000, 0x08FFFFF) ||
        mInRange(address, 0x0900000, 0x093FFFF) ||
        mInRange(address, 0x0940000, 0x097FFFF) ||
        mInRange(address, 0x0980000, 0x09BFFFF) ||
        mInRange(address, 0x09C0000, 0x09FFFFF) ||
        mInRange(address, 0x0A00000, 0x0A3FFFF) ||
        mInRange(address, 0x0A40000, 0x0A7FFFF) ||
        mInRange(address, 0x0A80000, 0x0ABFFFF) ||
        mInRange(address, 0x0AC0000, 0x0AFFFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210011IsMapRegister(AtDevice self, uint32 address)
    {
    return Tha60210011IsMapHoRegister(self, address) || Tha60210011IsMapLoRegister(self, address);
    }

eBool Tha60210011IsDemapRegister(AtDevice self, uint32 address)
    {
    return Tha60210011IsMapRegister(self, address);
    }

eBool Tha60210011IsOcnRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0100000, 0x01FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsPdhRegister(AtDevice self, uint32 address)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(self, cAtModulePdh);
    uint32 localAddress = address - ThaModulePdhBaseAddress(pdhModule);

    if (mInRange(localAddress, 0x000000, 0x0FFFFF) ||
        mInRange(localAddress, 0x100000, 0x1FFFFF) ||
        mInRange(localAddress, 0x200000, 0x2FFFFF) ||
        mInRange(localAddress, 0x300000, 0x3FFFFF) ||
        mInRange(localAddress, 0x400000, 0x4FFFFF) ||
        mInRange(localAddress, 0x500000, 0x5FFFFF) ||
        mInRange(localAddress, 0x600000, 0x6FFFFF) ||
        mInRange(localAddress, 0x700000, 0x7FFFFF) ||
        mInRange(localAddress, 0x800000, 0x8FFFFF) ||
        mInRange(localAddress, 0x900000, 0x9FFFFF) ||
        mInRange(localAddress, 0xA00000, 0xAFFFFF) ||
        mInRange(localAddress, 0xB00000, 0xBFFFFF))
        return cAtTrue;

    /* MDL */
    localAddress = address - ThaModulePdhMdlBaseAddress(pdhModule);
    if (mInRange(localAddress, 0x00000, 0x3FFFF))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011IsPohRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0200000, 0x02FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsPlaRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0400000, 0x04FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsPweRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (mInRange(address, 0x0070000, 0x007FFFF) ||
        mInRange(address, 0x0080000, 0x008FFFF) ||
        mInRange(address, 0x0090000, 0x009FFFF) ||
        mInRange(address, 0x00A0000, 0x00AFFFF))
        return cAtTrue;

    /* Internal counter registers, see INTPMC doc */
    if (mInRange(address, 0x1E10000UL, 0x1E10000UL + 0x87FF) ||
        mInRange(address, 0x1E11000UL, 0x1E11000UL + 0x87FF) ||
        mInRange(address, 0x1E12000UL, 0x1E12000UL + 0x87FF) ||
        mInRange(address, 0x1E13000UL, 0x1E13000UL + 0x87FF) ||
        mInRange(address, 0x1E14000UL, 0x1E14000UL + 0x87FF) ||
        mInRange(address, 0x1E15000UL, 0x1E15000UL + 0x87FF))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011IsPdaRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if (mInRange(address, 0x0500000, 0x05FFFFF))
        return cAtTrue;

    /* Internal counter registers, see INTPMC doc */
    if (mInRange(address, 0x1E21000UL, 0x1E21000UL + 0x87FF) ||
        mInRange(address, 0x1E27000UL, 0x1E27000UL + 0x87FF) ||
        mInRange(address, 0x1E28000UL, 0x1E28000UL + 0x87FF) ||
        mInRange(address, 0x1E22000UL, 0x1E22000UL + 0x87FF) ||
        mInRange(address, 0x1E2A000UL, 0x1E2A000UL + 0x87FF) ||
        mInRange(address, 0x1E2B000UL, 0x1E2B000UL + 0x87FF) ||
        mInRange(address, 0x1E29000UL, 0x1E29000UL + 0x87FF))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011IsBerRegister(AtDevice self, uint32 address)
    {
    return Tha60210011IsPohRegister(self, address);
    }

eBool Tha60210011IsPmcRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0700000, 0x07FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210011IsOcnTfi5SerdesRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if ((address == cAf6RegTfi5LoopbackControl) ||
        (address == (cAf6RegTfi5SerdesCtrl + cAf6RegTfi5DiagBaseAddress)))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011IsOcnTfi5SideRegister(AtDevice self, uint32 address)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 localAddress = address - baseAddress;

    if (mInRange(localAddress, 0x22000, 0x22e2f) ||
        mInRange(localAddress, 0x23000, 0x23e2f) ||
        mInRange(localAddress, 0x25000, 0x2572f) ||
        mInRange(localAddress, 0x20000, 0x20e00) ||
        mInRange(localAddress, 0x20001, 0x20e01) ||
        mInRange(localAddress, 0x20002, 0x20e02) ||
        mInRange(localAddress, 0x20003, 0x20e03) ||
        mInRange(localAddress, 0x22140, 0x22f6f) ||
        mInRange(localAddress, 0x22180, 0x22faf) ||
        mInRange(localAddress, 0x22080, 0x22eaf) ||
        mInRange(localAddress, 0x23140, 0x23f6f) ||
        mInRange(localAddress, 0x23080, 0x23eaf) ||
        mInRange(localAddress, 0x26000, 0x2672f) ||
        mInRange(localAddress, 0x27000, 0x2772f))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210011IsOcnTerminatedSideRegister(AtDevice self, uint32 address)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 localAddress = address - baseAddress;

    if (mInRange(localAddress, 0x24000, 0x24d2f) ||
        mInRange(localAddress, 0x40000, 0x5402f) ||
        mInRange(localAddress, 0x40800, 0x54fff) ||
        mInRange(localAddress, 0x60000, 0x7402f) ||
        mInRange(localAddress, 0x60800, 0x74fff) ||
        mInRange(localAddress, 0x42800, 0x56fff) ||
        mInRange(localAddress, 0x43000, 0x57fff) ||
        mInRange(localAddress, 0x41000, 0x55fff) ||
        mInRange(localAddress, 0x62800, 0x76fff) ||
        mInRange(localAddress, 0x61000, 0x75fff) ||
        mInRange(localAddress, 0x28000, 0x2872f))
        return cAtTrue;

    return cAtFalse;
    }

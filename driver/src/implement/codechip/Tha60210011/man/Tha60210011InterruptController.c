/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha60210011InterruptController.c
 *
 * Created Date: Jul 14, 2015
 *
 * Description : PW CodeChip Interrupt Controller.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaIpCoreInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdhReg.h"
#include "../../Tha60210031/pmc/Tha60210031PmcReg.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../poh/Tha60210011PohReg.h"
#include "../pw/Tha60210011ModulePw.h"
#include "../sdh/Tha60210011ModuleSdh.h"
#include "Tha60210011DeviceReg.h"
#include "Tha60210011Device.h"
#include "Tha60210011InterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtIpCoreMethods  m_AtIpCoreOverride;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaIpCore self)
    {
    return Tha60210011DeviceInterruptBaseAddress((Tha60210011Device)AtIpCoreDeviceGet((AtIpCore)self));
    }

static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    AtHalNoLockWrite(hal, BaseAddress((ThaIpCore)self) + cAf6Reg_global_interrupt_pin_disable, enable ? 0 : 1);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    return (AtHalRead(hal, BaseAddress((ThaIpCore)self) + cAf6Reg_global_interrupt_pin_disable) & cBit0) ? cAtFalse : cAtTrue;
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 interruptEnable;

    if (mMethodsGet(self)->CanEnableHwInterruptPin(self))
        {
        AtIpCoreInterruptEnable((AtIpCore)self, cAtFalse);
        return 0;
        }

    interruptEnable = AtHalRead(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable);
    AtHalWrite(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable, 0);
    return interruptEnable;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    return AtHalRead(hal, BaseAddress(self) + cAf6Reg_global_interrupt_status);
    }

static uint32 StartVersionSupportInterruptRestore(AtDevice device)
    {
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x02, 0x18, 0x54);
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(mIpCore(self));

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInterruptRestore(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet InterruptRestore(ThaIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 regAddr;
    uint32 interruptEnable;

    if (!mMethodsGet(self)->InterruptRestoreIsSupported(self))
        return cAtErrorModeNotSupport;

    interruptEnable = AtHalNoLockRead(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable);

    /* Produce trigger to restore interrupt. */
    regAddr = BaseAddress(self) + cAf6Reg_global_interrupt_mask_restore;
    AtHalNoLockWrite(hal, regAddr, 0);
    AtHalNoLockWrite(hal, regAddr, interruptEnable);

    return cAtOk;
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrMask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (mMethodsGet(self)->CanEnableHwInterruptPin(self))
        AtIpCoreInterruptEnable((AtIpCore)self, cAtTrue);
    else
        AtHalWrite(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable, intrMask);
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_TFI5IntStatus_Mask |
                          cAf6_global_interrupt_status_STSIntStatus_Mask  |
                          cAf6_global_interrupt_status_VTIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_MdlIntStatus_Mask |
                          cAf6_global_interrupt_status_PrmIntStatus_Mask |
                          cAf6_global_interrupt_status_DE3IntStatus_Mask |
                          cAf6_global_interrupt_status_DE1IntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool PwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_global_interrupt_status_PWEIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static eBool SurCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_PmIntStatus_Mask |
                          cAf6_global_interrupt_status_FmIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool ClockCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_LoCdrIntStatus_Mask |
                          cAf6_global_interrupt_status_HoCdrIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static void HwInterruptEnable(ThaIpCore self, uint32 mask, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 interruptEnReg = AtHalRead(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable);

    if (enable)
        interruptEnReg |= mask;
    else
        interruptEnReg &= ~mask;

    AtHalWrite(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable, interruptEnReg);
    }

static eBool InterruptHwIsEnabled(ThaIpCore self, uint32 mask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 regVal = AtHalRead(hal, BaseAddress(self) + cAf6Reg_global_interrupt_mask_enable);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eAtRet SemInterruptEnable(AtIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable((ThaIpCore)self, cAf6_global_interrupt_status_SemIntStatus_Mask, enable);
    return cAtOk;
    }

static eBool SemInterruptIsEnabled(AtIpCore self)
    {
    return InterruptHwIsEnabled((ThaIpCore)self, cAf6_global_interrupt_status_SemIntStatus_Mask);
    }

static eAtRet SysmonInterruptEnable(AtIpCore self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool SysmonInterruptIsEnabled(AtIpCore self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtIpCoreDeviceGet((AtIpCore)self), cAtModulePdh);

    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_mask_enable_MdlIntEnable_Mask|
                                     cAf6_global_interrupt_mask_enable_PrmIntEnable_Mask|
                                     cAf6_global_interrupt_mask_enable_DE3IntEnable_Mask|
                                     cAf6_global_interrupt_mask_enable_DE1IntEnable_Mask, enable);

    /* Enable slice interrupt */
    ThaModulePdhDe3InterruptEnable(pdhModule, enable);
    ThaModulePdhDe1InterruptEnable(pdhModule, enable);
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)AtDeviceModuleGet(AtIpCoreDeviceGet((AtIpCore)self), cAtModuleSdh);

    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_mask_enable_TFI5IntEnable_Mask|
                                     cAf6_global_interrupt_mask_enable_STSIntEnable_Mask |
                                     cAf6_global_interrupt_mask_enable_VTIntEnable_Mask, enable);

    ThaModuleSdhLineInterruptEnable(moduleSdh, enable);
    ThaModuleSdhStsInterruptEnable(moduleSdh, enable);
    ThaModuleSdhVtInterruptEnable(moduleSdh, enable);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(AtIpCoreDeviceGet((AtIpCore)self), cAtModulePw);
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_mask_enable_PWEIntEnable_Mask, enable);
    ThaModulePwDefectSliceInterruptEnable(pwModule, enable);
    }

static void SurHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_PmIntStatus_Mask |
                                     cAf6_global_interrupt_status_FmIntStatus_Mask, enable);
    }

static void ClockHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_LoCdrIntStatus_Mask|cAf6_global_interrupt_status_HoCdrIntStatus_Mask, enable);
    }

static void PhysicalHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_SemIntStatus_Mask, enable);
    }

static eBool SemCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_global_interrupt_status_SemIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportInterruptPinRegister(AtDevice device)
    {
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x22, 0x41);
    }

static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(mIpCore(self));

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInterruptPinRegister(device))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtIpCore(ThaIntrController self)
    {
    AtIpCore core = (AtIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, mMethodsGet(core), sizeof(m_AtIpCoreOverride));

        mMethodOverride(m_AtIpCoreOverride, InterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, InterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, SemInterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, SemInterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, SysmonInterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, SysmonInterruptIsEnabled);
        }
    mMethodsSet(core, &m_AtIpCoreOverride);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, SdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PwCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SurCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ClockCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SurHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ClockHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestore);
        mMethodOverride(m_ThaIpCoreOverride, CanEnableHwInterruptPin);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        mMethodOverride(m_ThaIpCoreOverride, PhysicalHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SemCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, HwInterruptEnable);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideAtIpCore(self);
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011InterruptController);
    }

ThaIntrController Tha60210011IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60210011IntrControllerNew(AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011IntrControllerObjectInit(newController, core);
    }

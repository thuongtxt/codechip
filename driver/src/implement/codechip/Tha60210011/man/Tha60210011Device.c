/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210011Device.c
 *
 * Created Date: Apr 23, 2015
 *
 * Description : Product 60030041
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/ram/AtModuleRamInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/physical/ThaSemControllerV3.h"
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "../xc/Tha60210011ModuleXc.h"
#include "../poh/Tha60210011PohReg.h"
#include "../physical/Tha6021Physical.h"
#include "../pwe/Tha60210011ModulePwe.h"
#include "../poh/Tha60210011ModulePoh.h"
#include "../ber/Tha60210011ModuleBer.h"
#include "Tha60210011DeviceInternal.h"
#include "Tha60210011DeviceReg.h"
#include "Tha60210011InterruptControllerInternal.h"
#include "Tha60210011DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cDiagModeEnableRegister  0xf00043
#define cDiagDdr1DiagEnableMask  cBit4
#define cDiagDdr1DiagEnableShift 4
#define cDiagDdr2DiagEnableMask  cBit5
#define cDiagDdr2DiagEnableShift 5
#define cDiagQdrDiagEnableMask   cBit6
#define cDiagQdrDiagEnableShift  6
#define cDiagXfiDiagEnableMask   cBit8
#define cDiagXfiDiagEnableShift  8
#define cDiagOcnDiagEnableMask   cBit0
#define cDiagOcnDiagEnableShift  0

#define cDiagXfiGroup1DiagEnableMask   cBit8
#define cDiagXfiGroup1DiagEnableShift  8

#define cDiagXfiGroup2DiagEnableMask   cBit9
#define cDiagXfiGroup2DiagEnableShift  9

#define cThaFPGAOCNClock155_52MhzValueStatus 0xF00063

#define cTimeToWaitAllSemsToObservationInMs 10000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011Device)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011DeviceMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;

/* Super implementation */
static const tAtObjectMethods          *m_AtObjectMethods = NULL;
static const tThaDeviceMethods         *m_ThaDeviceMethods = NULL;
static const tAtDeviceMethods          *m_AtDeviceMethods = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleSdh)    return (AtModule)Tha60210011ModuleSdhNew(self);
    if (moduleId  == cAtModuleXc)     return (AtModule)Tha60210011ModuleXcNew(self);
    if (moduleId  == cAtModulePw)     return (AtModule)Tha60210011ModulePwNew(self);
    if (moduleId  == cAtModuleEth)    return (AtModule)Tha60210011ModuleEthNew(self);
    if (moduleId  == cAtModuleSur)    return (AtModule)Tha60210011ModuleSurNew(self);
    if (moduleId  == cAtModulePdh)    return (AtModule)Tha60210011ModulePdhNew(self);
    if (moduleId  == cAtModuleRam)    return (AtModule)Tha60210011ModuleRamNew(self);
    if (moduleId  == cAtModuleBer)    return (AtModule)Tha60210011ModuleBerNew(self);
    if (moduleId  == cAtModulePrbs)   return (AtModule)Tha60210011ModulePrbsNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60210011ModulePktAnalyzerNew(self);
    if (moduleId  == cAtModuleClock)  return (AtModule)Tha60210011ModuleClockNew(self);

    /* Private modules */
    if (phyModule == cThaModuleOcn)   return Tha60210011ModuleOcnNew(self);
    if (phyModule == cThaModuleCdr)   return Tha60210011ModuleCdrNew(self);
    if (phyModule == cThaModuleMap)   return Tha60210011ModuleMapNew(self);
    if (phyModule == cThaModuleDemap) return Tha60210011ModuleDemapNew(self);
    if (phyModule == cThaModulePda)   return Tha60210011ModulePdaNew(self);
    if (phyModule == cThaModulePoh)   return Tha60210011ModulePohNew(self);
    if (phyModule == cThaModulePwe)   return Tha60210011ModulePweNew(self);
    if (phyModule == cThaModuleCla)   return Tha60210011ModuleClaNew(self);
    if (phyModule == cThaModuleCos)   return Tha60210011ModuleCosNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModuleSur,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    switch (moduleValue)
        {
        case cAtModuleXc   : return cAtTrue;
        case cAtModulePrbs : return cAtTrue;
        case cAtModuleBer  : return cAtTrue;
        case cAtModuleClock: return cAtTrue;
        case cAtModuleSur:   return cAtTrue;
        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static void UnbindAllPws(AtDevice self)
    {
    uint32 pwId;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(self, cAtModulePw);
    if (pwModule)
       {
       for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
           {
           AtPw pw = AtModulePwGetPw(pwModule, pwId);
           if (pw != NULL)
               AtPwCircuitUnbind(pw);
           }
       }
    }

static uint32 StartVersionSupportDiagnosticCheck(Tha60210011Device self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x15, 0x08, 0x30, 0x00);
    }

static eBool CanRunDiagnostic(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceVersionNumber(self) >= mMethodsGet(mThis(self))->StartVersionSupportDiagnosticCheck(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ShouldReActivate(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Reactivate(AtDevice self)
    {
    eAtRet ret = cAtOk;
    eBool activated;

    if (!mMethodsGet(mThis(self))->ShouldReActivate(mThis(self)))
        return cAtOk;

    activated = AtDeviceAllCoresAreActivated(self);
    if (!activated)
        return cAtOk;

    /* As discussed with hardware, all modules need to be reactivated if they were
     * activated before. The purpose of doing this is also to cleanup hardware
     * resources and reset internal state machines */
    ret |= AtDeviceAllCoresActivate(self, cAtFalse);
    AtOsalSleep(1);
    ret |= AtDeviceAllCoresActivate(self, cAtTrue);

    return ret;
    }

static ThaEthSerdesManager SerdesManager(AtDevice self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    if (ethModule)
        return ThaModuleEthSerdesManager(ethModule);
    return NULL;
    }

static eAtRet AllSerdesReset(Tha60210011Device self)
    {
    return ThaEthSerdesManagerAllSerdesReset(SerdesManager((AtDevice)self));
    }

static eBool AllSerdesGood(AtDevice self)
    {
    ThaEthSerdesManager manager = SerdesManager(self);
    uint32 numSerdes = ThaEthSerdesManagerNumSerdesController(manager);
    uint32 serdes_i;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = ThaEthSerdesManagerSerdesController(manager, serdes_i);
        if (!AtSerdesControllerIsGood(serdes))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet BerRequestDdrDisable(Tha60210011Device self)
    {
    AtHal hal = AtDeviceIpCoreHalGet((AtDevice)self, 0);
    uint32 regVal = AtHalRead(hal, 0x260000);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_dsnsenb_, 0);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_vtenb_,   0);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_stsenb_,  0);
    AtHalWrite(hal, 0x260000, regVal);
    return cAtOk;
    }

static uint32 StartVersionSupportDisableJnRequestDdr(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x16, 0x01, 0x27, 0x54);
    }

static eBool NeedToDisableJnRequestDdr(AtDevice self)
    {
    return (AtDeviceVersionNumber(self) >= StartVersionSupportDisableJnRequestDdr(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet PlaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PdaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet ClaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PweHwFlush(Tha60210011Device self)
    {
    ThaModulePwe pwe = (ThaModulePwe)AtDeviceModuleGet((AtDevice)self, cThaModulePwe);
    if (pwe)
	    return Tha60210011ModulePweAllPwsHeaderFlush(pwe);
    return cAtOk;	    
    }

static eAtRet PrbsHwFlush(Tha60210011Device self)
	{
	AtUnused(self);
	return cAtOk;
	}

static eAtRet PdhHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet MapHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AttHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static Tha60210011ModulePoh ModulePoh(AtDevice self)
    {
    return (Tha60210011ModulePoh)AtDeviceModuleGet(self, cThaModulePoh);
    }

static Tha60210011ModuleBer ModuleBer(AtDevice self)
    {
    return (Tha60210011ModuleBer)AtDeviceModuleGet(self, cAtModuleBer);
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = cAtOk;
    eBool supportDisableJnRequestDdr = mMethodsGet(mThis(self))->NeedToDisableJnRequestDdr(self);
    Tha60210011ModulePoh modulePoh = ModulePoh(self);
    Tha60210011ModuleBer moduleBer = ModuleBer(self);

    mMethodsGet(mThis(self))->UnbindAllPws(self);

    ret = Tha60210011ModuleBerEnable(moduleBer, cAtFalse);
    if (ret != cAtOk)
        return ret;

    if (supportDisableJnRequestDdr)
        {
        ret = Tha60210011ModulePohJnRequestDdrEnable(modulePoh, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    ret |= Reactivate(self);
    ret |= AtDeviceDiagnosticCheckEnable(self, mMethodsGet(mThis(self))->CanRunDiagnostic(self));
    ret |= m_AtDeviceMethods->Init(self);

    /* To avoid QDR ECC error raise*/
    ret |= mMethodsGet(mThis(self))->PweHwFlush(mThis(self));

    if (supportDisableJnRequestDdr)
        Tha60210011ModulePohJnRequestDdrEnable(modulePoh, cAtTrue);

    mMethodsGet(mThis(self))->AllSerdesReset(mThis(self));
    ret |= AtDeviceAllModulesHwResourceCheck(self);

    return ret;
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
    AtUnused(self);
    return Tha60210011LongRegisterAccessNew();
    }

static eBool WarmRestoreReadOperationDisabled(AtDevice self)
    {
    /* TODO: disable read action when warm restore for TB testing */
    AtUnused(self);
    return cAtTrue;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60210011IntrControllerNew(core);
    }

static void StickyDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static void StatusDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static void RamMarginDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static eBool OcnClockIsGood(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 masks = cTfi5FailMask;
    eBool isGood = ThaDeviceStickyIsGood(self, cFpgaStickyRegister, masks, cTimeoutMs);

    if (isGood)
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: TFI-5 ok\r\n", AtFunction);
    else
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: TFI-5 fail\r\n", AtFunction);

    return isGood;
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
    /* This product does not have SSKey */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportXfiDiagPerGroup(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x9, 0x18, 0x0);
    }

static eBool XfiDiagPerGroupIsSupported(AtDevice self)
    {
    uint32 version = AtDeviceVersionNumber(self);
    uint32 startSupportedVersion = StartVersionSupportXfiDiagPerGroup(self) ;
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eAtRet HwDiagnosticModeEnable(AtDevice self, eBool enable)
    {
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    uint32 regAddr = cDiagModeEnableRegister;
    uint32 regVal = AtIpCoreRead(core, regAddr);
    uint8 diag = enable ? 1 : 0;

    mRegFieldSet(regVal, cDiagDdr1DiagEnable, diag);
    mRegFieldSet(regVal, cDiagDdr2DiagEnable, diag);
    mRegFieldSet(regVal, cDiagQdrDiagEnable, diag);
    mRegFieldSet(regVal, cDiagOcnDiagEnable, diag);

    if (mMethodsGet(mThis(self))->XfiDiagPerGroupIsSupported(self))
        {
        mRegFieldSet(regVal, cDiagXfiGroup1DiagEnable, diag);
        mRegFieldSet(regVal, cDiagXfiGroup2DiagEnable, diag);
        }
    else
        mRegFieldSet(regVal, cDiagXfiDiagEnable, diag);

    AtIpCoreWrite(core, regAddr, regVal);

    return cAtOk;
    }

static eBool XfiGroupIsValid(uint8 groupId)
    {
    return (groupId <= 1) ? cAtTrue : cAtFalse;
    }

static void UnbindAndDeleteAllPws(AtDevice self)
    {
    uint32 pwId;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(self, cAtModulePw);

    if (!pwModule)
        return;

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
        {
        for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
    	    {
        	AtPw pw = AtModulePwGetPw(pwModule, pwId);
	        if (pw != NULL)
    	        {
        	    AtPwCircuitUnbind(pw);
            	AtModulePwDeletePw(pwModule, pwId);
            	}
        	}
        }
    }

static ThaModuleHardSur ModuleSur(AtDevice self)
    {
    return (ThaModuleHardSur)AtDeviceModuleGet(self, cAtModuleSur);
    }

static eAtRet DiagnosticModeEnablePrepare(AtDevice self)
    {
    uint32 cDelayTimeInMs = 500;
    eBool isSimulated = AtDeviceIsSimulated(self);

    if (AtModuleHwIsReady((AtModule)ModuleSur(self)))
        AtModuleDeactivate((AtModule)ModuleSur(self));

    if (!isSimulated)
    AtOsalUSleep(cDelayTimeInMs * 1000);

    UnbindAndDeleteAllPws(self);
    ThaIpCorePohDeactivate((ThaIpCore)AtDeviceIpCoreGet(self, 0));

    if (!isSimulated)
        AtOsalUSleep(cDelayTimeInMs * 1000);

    return cAtOk;
    }

static eAtRet DiagnosticModeEnable(AtDevice self, eBool enable)
    {
    eAtRet ret = cAtOk;

    if (enable)
        DiagnosticModeEnablePrepare(self);

    ret |= m_AtDeviceMethods->DiagnosticModeEnable(self, enable);
    ret |= mMethodsGet(mThis(self))->HwDiagnosticModeEnable(self, enable);

    if (mMethodsGet(mThis(self))->ShouldResetSerdesWhenEnablingDiagnosticMode(mThis(self), enable))
        mMethodsGet(mThis(self))->AllSerdesReset(mThis(self));

    return ret;
    }

static uint32 StartVersionSupportUart(Tha60150011Device self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0);
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    uint32 startVersionSupported = StartVersionSupportUart(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader((AtDevice)self));
    return (currentVersion >= startVersionSupported) ? cAtTrue : cAtFalse;
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceIsSimulated(self))
        return cAtTrue;

    if (Tha60150011DeviceSemUartIsSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    AtUnused(self);
    return 3;
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
        return Tha60210011SemControllerNew(self, semId);
    }

static eBool ThermalSensorIsSupported(AtDevice self)
    {
    AtUnused(self);	
    return cAtFalse;
    }

static uint8 NumXGMIIClock156_25Mhz(Tha60150011Device self)
    {
    AtUnused(self);
    return 2;
    }

static eBool PllIsLocked(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    return ThaDeviceStickyIsGood(self, cFpgaStickyRegister, cPllNotLockMask, cTimeoutMs);
    }

static eBool ShouldEnforceEpLogic(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60210011VersionReaderNew((AtDevice)self);
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
    if (AtDeviceIsSimulated((AtDevice)self))
        return 0;
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x15, 0x11, 0x05, 0x32);
    }

static uint8 NumCoreClocks(Tha60150011Device self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 CoreClockValueInHz(Tha60150011Device self, uint8 core)
    {
    AtUnused(self);
    AtUnused(core);
    return 155520000;
    }

static eBool NewStartupSequenceIsSupported(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldShowCommonDebugInfo(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet QdrDiagnostic(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 masks = cQdrCalibFailMask;
    eBool isGood = ThaDeviceStickyIsGood(self, cFpgaStickyRegister, masks, cTimeoutMs);

    if (isGood)
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: QDR calib done\r\n", AtFunction);
    else
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: QDR calib fail\r\n", AtFunction);

    return isGood ? cAtOk : cAtErrorQdrCalibFail;
    }

static eBool NeedDebugDdrCalib(void)
    {
    return cAtFalse;
    }

static uint32 DdrCalibTimeoutMs(Tha60210011Device self)
    {
    AtUnused(self);
    if (NeedDebugDdrCalib())
        return 5 * 60 * 1000; /* 5 minutes */

    /* Current timeout so far */
    return 20000;
    }

static eBool DdrCalibIsGood(AtDevice self)
    {
    uint32 cTimeoutMs = mMethodsGet(mThis(self))->DdrCalibTimeoutMs(mThis(self));
    uint32 regVal;
    uint32 masks = cDdr1CalibFailMask | cDdr2CalibFailMask;
    eBool isGood;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    tAtOsalCurTime startTime, stopTime;

    AtOsalCurTimeGet(&startTime);
    isGood = ThaDeviceStickyIsGood(self, cFpgaStickyRegister, masks, cTimeoutMs);
    AtOsalCurTimeGet(&stopTime);

    if (isGood)
        {
        uint32 elapseTimeMs = mTimeIntervalInMsGet(startTime, stopTime);
        if (elapseTimeMs > mThis(self)->maxDdrCalibTimeMs)
            mThis(self)->maxDdrCalibTimeMs = elapseTimeMs;

        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All DDRs calib done\r\n", AtFunction);
        return isGood;
        }

    regVal = AtIpCoreRead(core, cFpgaStickyRegister);
    if (regVal & cDdr1CalibFailMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#1 calib fail\r\n", AtFunction);
    if (regVal & cDdr2CalibFailMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#2 calib fail\r\n", AtFunction);

    return isGood;
    }

static uint32 XGMIIClock156_25MhzValueStatusRegister(Tha60150011Device self, uint8 clockId)
    {
    if (clockId == 1)
        return 0xF00062;
    return m_Tha60150011DeviceMethods->XGMIIClock156_25MhzValueStatusRegister(self, clockId);
    }

static void OcnClockStatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtDebugger debugger = AtDeviceDebuggerGet(device);
    ThaDeviceClockResultPrint(device, debugger, "OCN Clock 155.52Mhz Value Status", cThaFPGAOCNClock155_52MhzValueStatus, 155520000, 5);
    }

static const char *XGMIIClockName(Tha60150011Device self, uint8 clockId)
    {
    if (clockId == 0)
        return "XGMII(port 1-4)";
    if (clockId == 1)
        return "XGMII(port 5-8)";

    return m_Tha60150011DeviceMethods->XGMIIClockName(self, clockId);
    }

static eBool EthIntfXgmiiClockIsGood(AtDevice self)
    {
    uint32 regVal;
    static const uint32 cTimeoutMs = 5000;
    eBool isGood =  ThaDeviceStickyIsGood(self, cFpgaStickyRegister, cXgmii2_rst_out155Mask | cXgmii1_rst_out155Mask, cTimeoutMs);
    AtIpCore core = AtDeviceIpCoreGet(self, 0);

    if (isGood)
       {
       AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All XGMII clock out 155 ok\r\n", AtFunction);
       return isGood;
       }

    regVal = AtIpCoreRead(core, cFpgaStickyRegister);
    if (regVal & cXgmii1_rst_out155Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XGMII#1 clock out 155 fail\r\n", AtFunction);
    if (regVal & cXgmii1_rst_out155Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XGMII#2 clock out 155 fail\r\n", AtFunction);

    return isGood;
    }

static eBool EthIntfCheckingIsSupported(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthIntfClockIsGood(AtDevice self)
    {
    if (!mMethodsGet(mThis(self))->EthIntfCheckingIsSupported(mThis(self)))
        return cAtTrue;

    return EthIntfXgmiiClockIsGood(self);
    }

static uint32 ClockPcieSwingRangeInPpm(Tha60150011Device self)
    {
    AtUnused(self);
    return 100;
    }

static eAtRet HalTest(AtDevice self)
    {
    /* Disable hal test until HW provide registers used for testing */
    AtUnused(self);
    return cAtOk;
    }

static eBool NeedCoreResetWhenDiagCheck(Tha60150011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumUsedOc48Slice(Tha60210011Device self)
    {
    /* There should be 8 for this product. But at the current module, only 1
     * are being used */
    AtUnused(self);
    return 1;
    }

static void Delete(AtObject self)
    {
    /* Delete over HAL TCP is slow */
    if (ThaDeviceShouldDeprovisionHwOnDeleting((ThaDevice)self))
        mMethodsGet(mThis(self))->UnbindAllPws((AtDevice)self);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static uint32 InterruptBaseAddress(Tha60210011Device self)
    {
    AtUnused(self);
    return 0x1E80000;
    }

static eBool ShouldDisableHwAccessBeforeDeleting(AtDevice self)
    {
    return Tha6021ShouldDisableHwAccessBeforeDeleting(self);
    }

static uint32 StartVersionSupportFullPwCounters(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x10, 0x30, 0x30);
    }

static eBool RamErrorGeneratorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldDeprovisionHwOnDeleting(ThaDevice self)
    {
    if (AtDeviceInAccessible((AtDevice)self))
        return cAtFalse;

    if (AtHalIsSlow(AtDeviceIpCoreHalGet((AtDevice)self, 0)))
        return cAtFalse;

    AtUnused(self);
    return cAtTrue;
    }

static char *Buffer(AtDebugger debugger, uint32 *bufferSize)
    {
    if (debugger)
        return AtDebuggerCharBuffer(debugger, bufferSize);
    return AtSharedDriverSharedBuffer(bufferSize);
    }

static eAtRet PutString(AtDebugger debugger, eAtSevLevel severity, const char *name, const char *value)
    {
    if (debugger)
        return AtDebuggerEntryAdd(debugger, AtDebugEntryNew(severity, name, value));

    AtPrintc(severity, "* %s: %s\r\n", name, value);
    return cAtOk;
    }

static void DebuggerFill(AtDevice self, AtDebugger debugger)
    {
    uint32 bufferSize;
    char *buffer = Buffer(debugger, &bufferSize);

    AtSnprintf(buffer, bufferSize, "%d (ms)", mThis(self)->maxDdrCalibTimeMs);
    PutString(debugger, cSevNormal, "DDR calib done max time", buffer);

    if (mMethodsGet(mThis(self))->ShouldApplyNewReset(mThis(self)))
        {
        AtSnprintf(buffer, bufferSize, "%d (ms)", mThis(self)->timeToObservation);
        PutString(debugger, cSevNormal, "SEM to Observation max time", buffer);
        }
    }

static void Debug(AtDevice self)
    {
    m_AtDeviceMethods->Debug(self);
    DebuggerFill(self, AtDeviceDebuggerGet(self));
    }

static eBool ShouldApplyNewReset(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ResetRetryTime(ThaDevice self)
    {
    if (NeedDebugDdrCalib())
        return 1;

    if (mMethodsGet(mThis(self))->ShouldApplyNewReset(mThis(self)))
        return 2;

    return m_ThaDeviceMethods->ResetRetryTime(self);
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return Tha60210011DeviceAsyncInit(self);
    }

static eAtRet HwAsyncFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011Device object = (Tha60210011Device)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(maxDdrCalibTimeMs);
    mEncodeUInt(asyncInitState);
    mEncodeUInt(asyncReactivateState);
    mEncodeNone(reactivaatePrevStateTime);
    mEncodeNone(movingSemId);
    mEncodeNone(asyncResetState);
    mEncodeNone(isWaiting);
    mEncodeNone(timeToObservation);
    }

static eAtRet MoveAllSemControllersToState(AtDevice self, eAtSemControllerAlarmType state)
    {
    uint8 numSemControllers = AtDeviceNumSemControllersGet(self);
    uint8 controller_i;

    for (controller_i = 0; controller_i < numSemControllers; controller_i++)
        {
        AtSemController controller = AtDeviceSemControllerGetByIndex(self, controller_i);
        eAtRet ret = ThaSemControllerMoveToStateByUart(controller, state);
        if (ret != cAtOk)
            {
            AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "Can not set SEM#%u to state %u\r\n", controller_i, state);
            return ret;
            }
        }

    return cAtOk;
    }

static eAtRet AsyncMoveAllSemControllersToState(AtDevice self, eAtSemControllerAlarmType state)
    {
    uint8 controller_i = mThis(self)->movingSemId;
    AtSemController controller = AtDeviceSemControllerGetByIndex(self, controller_i);
    eAtRet ret = ThaSemControllerAsyncMoveToStateByUart(controller, state);

    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    if (ret != cAtOk)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "Can not set SEM#%u to state %u\r\n", controller_i, state);
        mThis(self)->movingSemId = 0;
        return ret;
        }

    mThis(self)->movingSemId++;
    if (mThis(self)->movingSemId == AtDeviceNumSemControllersGet(self))
        {
        mThis(self)->movingSemId = 0;
        return cAtOk;
        }

    return cAtErrorAgain;
    }

static eAtRet FailStickiesLog(AtDevice self, eBool checkEthIntf)
    {
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    uint32 regVal = AtIpCoreRead(core, cFpgaStickyRegister);
    eAtRet ret = cAtOk;

    if (regVal & cPllNotLockMask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: PLL is not locked\r\n", AtFunction);
        ret |= cAtErrorDevicePllNotLocked;
        }

    if (regVal & cQdrCalibFailMask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: QDR calib fail\r\n", AtFunction);
        ret |= cAtErrorQdrCalibFail;
        }

    if (regVal & cDdr1CalibFailMask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#1 calib fail\r\n", AtFunction);
        ret |= cAtErrorDdrCalibFail;
        }

    if (regVal & cDdr2CalibFailMask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#2 calib fail\r\n", AtFunction);
        ret |= cAtErrorDdrCalibFail;
        }

    if (regVal & cTfi5FailMask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: TFI5 fail\r\n", AtFunction);
        ret |= cAtErrorDevFail;
        }

    if (!checkEthIntf)
        return ret;

    if (regVal & cXgmii1_rst_out155Mask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XGMII#1 clock out 155 fail\r\n", AtFunction);
        ret |= cAtErrorXfiPcsNotLocked;
        }

    if (regVal & cXgmii1_rst_out155Mask)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XGMII#2 clock out 155 fail\r\n", AtFunction);
        ret |= cAtErrorXfiPcsNotLocked;
        }

    return ret;
    }

static eAtRet AllStickiesCheck(AtDevice self)
    {
    static const uint32 cTimeoutMs = 10000;
    eBool checkEthIntf = mMethodsGet(mThis(self))->EthIntfCheckingIsSupported(mThis(self));
    uint32 allStickies = cPllNotLockMask | cQdrCalibFailMask | cDdr1CalibFailMask | cDdr2CalibFailMask | cTfi5FailMask;
    eBool isGood;

    if (checkEthIntf)
        allStickies |= cXgmii2_rst_out155Mask | cXgmii1_rst_out155Mask;

    isGood = ThaDeviceStickyIsGood(self, cFpgaStickyRegister, allStickies, cTimeoutMs);
    if (isGood)
        return cAtOk;

    return FailStickiesLog(self, checkEthIntf);
    }

static eAtRet AsyncAllStickiesCheck(AtDevice self)
    {
    static const uint32 cTimeoutMs = 10000;
    eAtRet ret;
    eBool checkEthIntf = mMethodsGet(mThis(self))->EthIntfCheckingIsSupported(mThis(self));
    uint32 allStickies = cPllNotLockMask | cQdrCalibFailMask | cDdr1CalibFailMask | cDdr2CalibFailMask | cTfi5FailMask;

    if (checkEthIntf)
        allStickies |= cXgmii2_rst_out155Mask | cXgmii1_rst_out155Mask;

    ret = ThaDeviceStickyAsyncCheck(self, cFpgaStickyRegister, allStickies, cTimeoutMs);
    if ((ret == cAtOk) || AtDeviceAsyncRetValIsInState(ret))
        return ret;

    /* Log the raised sticky */
    return FailStickiesLog(self, checkEthIntf);
    }

static eAtRet PllAsyncCheck(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    return ThaDeviceStickyAsyncCheck(self, cFpgaStickyRegister, cPllNotLockMask, cTimeoutMs);
    }

static eAtRet QdrCalibAsyncCheck(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 masks = cQdrCalibFailMask;
    eAtRet ret = ThaDeviceStickyAsyncCheck(self, cFpgaStickyRegister, masks, cTimeoutMs);

    if (ret == cAtOk)
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: QDR calib done\r\n", AtFunction);
    else if (!AtDeviceAsyncRetValIsInState(ret))
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: QDR calib fail\r\n", AtFunction);

    return ret;
    }

static eAtRet DdrCalibAsyncCheck(AtDevice self)
    {
    uint32 cTimeoutMs = mMethodsGet(mThis(self))->DdrCalibTimeoutMs(mThis(self));
    uint32 regVal;
    uint32 masks = cDdr1CalibFailMask | cDdr2CalibFailMask;
    eAtRet ret;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);

    ret = ThaDeviceStickyAsyncCheck(self, cFpgaStickyRegister, masks, cTimeoutMs);
    if (ret == cAtOk)
        {
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All DDRs calib done\r\n", AtFunction);
        return ret;
        }

    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    regVal = AtIpCoreRead(core, cFpgaStickyRegister);
    if (regVal & cDdr1CalibFailMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#1 calib fail\r\n", AtFunction);
    if (regVal & cDdr2CalibFailMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#2 calib fail\r\n", AtFunction);

    return ret;
    }

static eAtRet OcnClockAsyncCheck(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 masks = cTfi5FailMask;
    eAtRet ret = ThaDeviceStickyAsyncCheck(self, cFpgaStickyRegister, masks, cTimeoutMs);

    if (ret == cAtOk)
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: TFI-5 ok\r\n", AtFunction);
    else if (!AtDeviceAsyncRetValIsInState(ret))
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: TFI-5 fail\r\n", AtFunction);

    return ret;
    }

static eAtRet EthIntfXgmiiClockAsyncCheck(AtDevice self)
    {
    uint32 regVal;
    static const uint32 cTimeoutMs = 5000;
    eAtRet ret;
    AtIpCore core;

    if (!mMethodsGet(mThis(self))->EthIntfCheckingIsSupported(mThis(self)))
        return cAtOk;

    ret =  ThaDeviceStickyAsyncCheck(self, cFpgaStickyRegister, cXgmii2_rst_out155Mask | cXgmii1_rst_out155Mask, cTimeoutMs);
    core = AtDeviceIpCoreGet(self, 0);
    if (ret == cAtOk)
        {
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All XGMII clock out 155 ok\r\n", AtFunction);
        return ret;
        }

    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    regVal = AtIpCoreRead(core, cFpgaStickyRegister);
    if (regVal & cXgmii1_rst_out155Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XGMII#1 clock out 155 fail\r\n", AtFunction);
    if (regVal & cXgmii1_rst_out155Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XGMII#2 clock out 155 fail\r\n", AtFunction);

    return ret;
    }

static tAtAsyncOperationFunc NextOperation(AtDevice self, uint32 state)
    {
    static tAtAsyncOperationFunc allOps[] = {(tAtAsyncOperationFunc)PllAsyncCheck,
                                             (tAtAsyncOperationFunc)QdrCalibAsyncCheck,
                                             (tAtAsyncOperationFunc)DdrCalibAsyncCheck,
                                             (tAtAsyncOperationFunc)OcnClockAsyncCheck,
                                             (tAtAsyncOperationFunc)EthIntfXgmiiClockAsyncCheck};
    AtUnused(self);
    if (state >= mCount(allOps))
        return NULL;

    return allOps[state];
    }

static eAtRet AsyncOneByOneStickyCheck(AtDevice self)
    {
    return AtDeviceAsyncProcess(self, &mThis(self)->asyncInitState, NextOperation);
    }

static eAtRet ResetStatusCheck(Tha60150011Device self)
    {
    if (mMethodsGet(mThis(self))->ShouldApplyNewReset(mThis(self)))
        return AllStickiesCheck((AtDevice)self);

    return m_Tha60150011DeviceMethods->ResetStatusCheck(self);
    }

static eAtRet AsyncResetStatusCheck(Tha60150011Device self)
    {
    if (mMethodsGet(mThis(self))->ShouldApplyNewReset(mThis(self)))
        return AsyncAllStickiesCheck((AtDevice)self);

    return AsyncOneByOneStickyCheck((AtDevice)self);
    }

static eBool AllSemControllersAreInObservation(AtDevice self)
    {
    uint8 numSemControllers = AtDeviceNumSemControllersGet(self);
    uint8 controller_i;

    for (controller_i = 0; controller_i < numSemControllers; controller_i++)
        {
        AtSemController controller = AtDeviceSemControllerGetByIndex(self, controller_i);
        if ((AtSemControllerAlarmGet(controller) & cAtSemControllerAlarmObservation) == 0)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet AllSemControllersToObservationWait(AtDevice self)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    eBool isSimulated = AtDeviceIsSimulated(self);

    AtOsalCurTimeGet(&startTime);
    while (elapse < (cTimeToWaitAllSemsToObservationInMs * 3))
        {
        if (AllSemControllersAreInObservation(self))
            {
            if (mThis(self)->timeToObservation < elapse)
                mThis(self)->timeToObservation = elapse;
            return cAtOk;
            }

        /* Calculate elapse time */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        if (isSimulated)
            return cAtOk;
        }

    AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "TIMEOUT: Wait for OBSERVATION timeout (ms): %d\r\n", elapse);
    return cAtErrorSemTimeOut;
    }

static eAtRet AsyncAllSemControllersToObservationWait(AtDevice self)
    {
    tAtOsalCurTime curTime;
    uint32 elapse = 0;
    eBool isSimulated = AtDeviceIsSimulated(self);
    eBool success = cAtFalse;

    if (!mThis(self)->isWaiting)
        {
        AtOsalCurTimeGet(&mThis(self)->reactivaatePrevStateTime);
        mThis(self)->isWaiting = cAtTrue;
        }

    if ((AllSemControllersAreInObservation(self)) || isSimulated)
        success = cAtTrue;

    /* Calculate elapse time */
    AtOsalCurTimeGet(&curTime);
    elapse = mTimeIntervalInMsGet(mThis(self)->reactivaatePrevStateTime, curTime);

    if (success)
        {
        if (mThis(self)->timeToObservation < elapse)
            mThis(self)->timeToObservation = elapse;

        mThis(self)->isWaiting = cAtFalse;
        return cAtOk;
        }

    if (elapse < cTimeToWaitAllSemsToObservationInMs)
        return cAtErrorAgain;

    mThis(self)->isWaiting = cAtFalse;
    return cAtErrorSemTimeOut;
    }

static eBool ShouldMoveSemToObservationDuringHwReset(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet HwResetWithHandler(AtDevice self, eBool shouldApplyNewReset, eAtRet (*handler)(AtDevice dev))
    {
    eAtRet ret;

    if (shouldApplyNewReset)
        {
        ret = Tha60150011DeviceSemClockProvide(self);
        if (ret != cAtOk)
            return ret;

        /* Wait all SEMs to become Observation */
        ret = AllSemControllersToObservationWait(self);
        if (ret != cAtOk)
            return ret;

        /* Set SEM to IDLE */
        ret = MoveAllSemControllersToState(self, cAtSemControllerAlarmIdle);
        if (ret != cAtOk)
            return ret;
        }

    ret = handler(self);

    /* Get all SEM back to work */
    if (shouldApplyNewReset && mMethodsGet(mThis(self))->ShouldMoveSemToObservationDuringHwReset(mThis(self)))
        ret |= MoveAllSemControllersToState(self, cAtSemControllerAlarmObservation);

    return ret;
    }

static eAtRet HwReset(AtDevice self)
    {
    eBool shouldApplyNewReset = mMethodsGet(mThis(self))->ShouldApplyNewReset(mThis(self));
    return HwResetWithHandler(self, shouldApplyNewReset, m_AtDeviceMethods->HwReset);
    }

static eAtRet AsyncMoveAllSemsToIdle(AtDevice self)
    {
    return AsyncMoveAllSemControllersToState(self, cAtSemControllerAlarmIdle);
    }

static eAtRet SuperAsyncHwReset(AtDevice self)
    {
    return m_AtDeviceMethods->AsyncHwReset(self);
    }

static eAtRet AsyncMoveAllSemsToObservation(AtDevice self)
    {
    return AsyncMoveAllSemControllersToState(self, cAtSemControllerAlarmObservation);
    }

static tAtAsyncOperationFunc NextAsyncResetOperation(AtDevice self, uint32 state)
    {
    static tAtAsyncOperationFunc func[] = {(tAtAsyncOperationFunc)AsyncAllSemControllersToObservationWait,
                                           (tAtAsyncOperationFunc)AsyncMoveAllSemsToIdle,
                                           (tAtAsyncOperationFunc)SuperAsyncHwReset,
                                           (tAtAsyncOperationFunc)AsyncMoveAllSemsToObservation};
    AtUnused(self);
    if (state >= mCount(func))
        return NULL;

    return func[state];
    }

static eAtRet AsyncHwReset(AtDevice self)
    {
    eBool shouldApplyNewReset = mMethodsGet(mThis(self))->ShouldApplyNewReset(mThis(self));
    if (shouldApplyNewReset)
        return AtDeviceAsyncProcess(self, &mThis(self)->asyncResetState,  NextAsyncResetOperation);

    return m_AtDeviceMethods->AsyncHwReset(self);
    }

static eAtRet OcnHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool HasHoBus(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldResetSerdesWhenEnablingDiagnosticMode(Tha60210011Device self, eBool enable)
    {
    if (enable && !AllSerdesGood((AtDevice)self))
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject device = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(device, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        mMethodOverride(m_AtDeviceOverride, GlobalLongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, WarmRestoreReadOperationDisabled);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeEnable);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, NumSemControllersGet);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, ShouldDisableHwAccessBeforeDeleting);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, DebuggerFill);
        mMethodOverride(m_AtDeviceOverride, HwReset);
        mMethodOverride(m_AtDeviceOverride, AsyncHwReset);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, ShouldEnforceEpLogic);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, StartVersionSupportSurveillance);
        mMethodOverride(m_ThaDeviceOverride, ShouldDeprovisionHwOnDeleting);
        mMethodOverride(m_ThaDeviceOverride, ResetRetryTime);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha6015011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, NumCoreClocks);
        mMethodOverride(m_Tha60150011DeviceOverride, CoreClockValueInHz);
        mMethodOverride(m_Tha60150011DeviceOverride, StickyDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, StatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, RamMarginDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, OcnClockIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, NewStartupSequenceIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, ShouldShowCommonDebugInfo);
        mMethodOverride(m_Tha60150011DeviceOverride, DdrCalibIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, NumXGMIIClock156_25Mhz);
        mMethodOverride(m_Tha60150011DeviceOverride, XGMIIClock156_25MhzValueStatusRegister);
        mMethodOverride(m_Tha60150011DeviceOverride, OcnClockStatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, XGMIIClockName);
        mMethodOverride(m_Tha60150011DeviceOverride, QdrDiagnostic);
        mMethodOverride(m_Tha60150011DeviceOverride, PllIsLocked);
        mMethodOverride(m_Tha60150011DeviceOverride, EthIntfClockIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, ClockPcieSwingRangeInPpm);
        mMethodOverride(m_Tha60150011DeviceOverride, HalTest);
        mMethodOverride(m_Tha60150011DeviceOverride, NeedCoreResetWhenDiagCheck);
        mMethodOverride(m_Tha60150011DeviceOverride, SemUartIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, ResetStatusCheck);
        mMethodOverride(m_Tha60150011DeviceOverride, AsyncResetStatusCheck);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideThaDevice(self);
    OverrideAtDevice(self);
    OverrideTha6015011Device(self);
    }

static void MethodsInit(Tha60210011Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PweHwFlush);
        mMethodOverride(m_methods, HwAsyncFlush);
        mMethodOverride(m_methods, ShouldReActivate);
        mMethodOverride(m_methods, NumUsedOc48Slice);
        mMethodOverride(m_methods, StartVersionSupportDiagnosticCheck);
        mMethodOverride(m_methods, StartVersionSupportFullPwCounters);
        mMethodOverride(m_methods, CanRunDiagnostic);
        mMethodOverride(m_methods, NeedToDisableJnRequestDdr);
        mMethodOverride(m_methods, XfiDiagPerGroupIsSupported);
        mMethodOverride(m_methods, RamErrorGeneratorIsSupported);
        mMethodOverride(m_methods, HwDiagnosticModeEnable);
        mMethodOverride(m_methods, DdrCalibTimeoutMs);
        mMethodOverride(m_methods, ShouldApplyNewReset);
        mMethodOverride(m_methods, EthIntfCheckingIsSupported);
        mMethodOverride(m_methods, BerRequestDdrDisable);
        mMethodOverride(m_methods, UnbindAllPws);
        mMethodOverride(m_methods, PrbsHwFlush);
        mMethodOverride(m_methods, PlaHwFlush);
        mMethodOverride(m_methods, PdaHwFlush);
        mMethodOverride(m_methods, ClaHwFlush);
        mMethodOverride(m_methods, OcnHwFlush);
        mMethodOverride(m_methods, AttHwFlush);
        mMethodOverride(m_methods, MapHwFlush);
        mMethodOverride(m_methods, PdhHwFlush);
        mMethodOverride(m_methods, HasHoBus);
        mMethodOverride(m_methods, ShouldResetSerdesWhenEnablingDiagnosticMode);
        mMethodOverride(m_methods, AllSerdesReset);
        mMethodOverride(m_methods, ShouldMoveSemToObservationDuringHwReset);
        mMethodOverride(m_methods, InterruptBaseAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Device);
    }

AtDevice Tha60210011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60210011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011DeviceObjectInit(newDevice, driver, productCode);
    }

uint32 Tha60210011DeviceStartVersionSupportFullPwCounters(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSupportFullPwCounters(self);
    return cBit31_0;
    }

eAtRet Tha60210011DeviceXfiDiagPerGroupEnable(AtDevice self, uint8 groupId, eBool diag)
    {
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    uint32 regAddr = cDiagModeEnableRegister;
    uint32 regVal;

    if (!mMethodsGet(mThis(self))->XfiDiagPerGroupIsSupported(self))
        return cAtErrorModeNotSupport;

    if (!XfiGroupIsValid(groupId))
        return cAtErrorOutOfRangParm;

    regVal = AtIpCoreRead(core, regAddr);
    if (groupId == 0)
        mRegFieldSet(regVal, cDiagXfiGroup1DiagEnable, diag);
    if (groupId == 1)
        mRegFieldSet(regVal, cDiagXfiGroup2DiagEnable, diag);
    AtIpCoreWrite(core, regAddr, regVal);

    return cAtOk;
    }

uint8 Tha60210011DeviceNumUsedOc48Slices(Tha60210011Device self)
    {
    if (self)
        return mMethodsGet(self)->NumUsedOc48Slice(self);
    return 0;
    }

uint32 Tha60210011DeviceInterruptBaseAddress(Tha60210011Device self)
    {
    if (self)
        return mMethodsGet(self)->InterruptBaseAddress(self);
    return cBit31_0;
    }

eBool Tha6021ShouldDisableHwAccessBeforeDeleting(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse; /* Note: just for lab testing, it is enabled at main released branch */
    }

eBool Tha60210011RamErrorGeneratorIsSupported(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->RamErrorGeneratorIsSupported(self);
    return cAtFalse;
    }

eAtRet Tha60210011DeviceSuperAsyncInit(AtDevice self)
    {
    return m_AtDeviceMethods->AsyncInit(self);
    }

eBool Tha60210011DeviceSupportDisableJnRequestDdr(AtDevice self)
    {
    return mMethodsGet(mThis(self))->NeedToDisableJnRequestDdr(self);
    }

void Tha60210011DeviceUnbindAllPws(AtDevice self)
    {
	mMethodsGet(mThis(self))->UnbindAllPws(self);
    }

void Tha60210011DeviceBerRequestDdrDisable(AtDevice self)
    {
	if (self)
		mMethodsGet(mThis(self))->BerRequestDdrDisable(mThis(self));
    }

void Tha60210011DeviceJnRequestDdrEnable(AtDevice self, eBool enable)
    {
    Tha60210011ModulePohJnRequestDdrEnable(ModulePoh(self), enable);
    }

eBool Tha60210011DeviceCanRunDiagnostic(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->CanRunDiagnostic(self);
    return cAtFalse;
    }

eAtRet Tha60210011DeviceAllSerdesReset(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->AllSerdesReset(mThis(self));
    return cAtErrorNullPointer;
    }

eAtRet Tha60210011DevicePwePsnFlush(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->PweHwFlush(mThis(self));
    return cAtErrorNullPointer;
    }

eBool Tha60210011DeviceHasHoBus(Tha60210011Device self)
    {
    if (self)
        return mMethodsGet(self)->HasHoBus(self);
    return cAtFalse;
    }

eAtRet Tha60210011DeviceHwResetWithHandler(AtDevice self, eBool shouldApplyNewReset, eAtRet (*handler)(AtDevice dev))
    {
    if (self)
        return HwResetWithHandler(self, shouldApplyNewReset, handler);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210011DeviceMoveAllSemControllersToState(AtDevice self, eAtSemControllerAlarmType state)
    {
    return MoveAllSemControllersToState(self, state);
    }

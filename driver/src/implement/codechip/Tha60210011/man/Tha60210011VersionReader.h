/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210011VersionReader.h
 * 
 * Created Date: Dec 3, 2015
 *
 * Description : 60210011 Version reader interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011VERSIONREADER_H_
#define _THA60210011VERSIONREADER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011VersionReader * Tha60210011VersionReader;

typedef struct tTha60210011VersionReaderMethods
    {
    uint32 (*StartVersionIgnoreDate)(ThaVersionReader self);
    uint32 (*StartBuiltNumberIgnoreDate)(ThaVersionReader self);
    char *(*BuilNumberStringMake)(ThaVersionReader self, uint8 firstIndex, uint8 secondIndex, uint8 thirdIndex);
    }tTha60210011VersionReaderMethods;

typedef struct tTha60210011VersionReader
    {
    tThaVersionReaderPw super;
    const tTha60210011VersionReaderMethods *methods;
    }tTha60210011VersionReader;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210011VERSIONREADER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210011DeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011DEVICEASYNCINIT_H_
#define _THA60210011DEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210011DeviceAsyncInit(AtDevice self);
eAtRet Tha60210011DeviceSuperAsyncInit(AtDevice self);
eBool Tha60210011DeviceSupportDisableJnRequestDdr(AtDevice self);
void Tha60210011DeviceUnbindAllPws(AtDevice self);
void Tha60210011DeviceBerRequestDdrDisable(AtDevice self);
void Tha60210011DeviceJnRequestDdrEnable(AtDevice self, eBool enable);
eBool Tha60210011DeviceCanRunDiagnostic(AtDevice self);
eAtRet Tha60210011DeviceAllSerdesReset(AtDevice self);
eAtRet Tha60210011DevicePwePsnFlush(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011DEVICEASYNCINIT_H_ */


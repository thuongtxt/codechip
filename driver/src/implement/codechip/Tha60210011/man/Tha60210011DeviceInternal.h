/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210011DeviceInternal.h
 * 
 * Created Date: May 27, 2015
 *
 * Description : Product 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011DEVICEINTERNAL_H_
#define _THA60210011DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/man/Tha60150011DeviceInternal.h"
#include "Tha60210011Device.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011DeviceMethods
    {
    eBool (*ShouldReActivate)(Tha60210011Device self);
    eBool (*RamErrorGeneratorIsSupported)(AtDevice self);
    eAtRet (*PweHwFlush)(Tha60210011Device self);
    eAtRet (*PlaHwFlush)(Tha60210011Device self);
    eAtRet (*PdaHwFlush)(Tha60210011Device self);
    eAtRet (*ClaHwFlush)(Tha60210011Device self);
    eAtRet (*PrbsHwFlush)(Tha60210011Device self);
    eAtRet (*AttHwFlush)(Tha60210011Device self);
    eAtRet (*MapHwFlush)(Tha60210011Device self);
    eAtRet (*PdhHwFlush)(Tha60210011Device self);
    void  (*UnbindAllPws)(AtDevice self);
    eAtRet (*BerRequestDdrDisable)(Tha60210011Device self);
    uint32 (*StartVersionSupportDiagnosticCheck)(Tha60210011Device self);
    eAtRet (*HwAsyncFlush)(Tha60210011Device self);
    eBool (*ShouldApplyNewReset)(Tha60210011Device self);
    eAtRet (*OcnHwFlush)(Tha60210011Device self);
    eBool (*HasHoBus)(Tha60210011Device self);
    eBool (*ShouldResetSerdesWhenEnablingDiagnosticMode)(Tha60210011Device self, eBool enable);
    eAtRet (*AllSerdesReset)(Tha60210011Device self);

    /* Capacity of sub modules */
    uint8 (*NumUsedOc48Slice)(Tha60210011Device self);

    uint32 (*StartVersionSupportFullPwCounters)(AtDevice self);
    eBool (*CanRunDiagnostic)(AtDevice self);
    eBool (*NeedToDisableJnRequestDdr)(AtDevice self);
    eBool (*XfiDiagPerGroupIsSupported)(AtDevice self);
    eAtRet (*HwDiagnosticModeEnable)(AtDevice self, eBool enable);
    uint32 (*DdrCalibTimeoutMs)(Tha60210011Device self);
    eBool (*EthIntfCheckingIsSupported)(Tha60210011Device self);

    /* SEM control to overcome device init */
    eBool (*ShouldMoveSemToObservationDuringHwReset)(Tha60210011Device self);

    uint32 (*InterruptBaseAddress)(Tha60210011Device self);
    }tTha60210011DeviceMethods;

typedef struct tTha60210011Device
    {
    tTha60150011Device super;
    const tTha60210011DeviceMethods *methods;

    /* Private data */
    uint32 maxDdrCalibTimeMs;

    /* Async int state */
    uint32 asyncInitState;
    uint32 asyncReactivateState;
    tAtOsalCurTime reactivaatePrevStateTime;

    /* Async reset state */
    uint8 movingSemId;
    uint32 asyncResetState;
    eBool isWaiting;

    /* SEM profiles */
    uint32 timeToObservation;
    }tTha60210011Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60210011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011DEVICEINTERNAL_H_ */


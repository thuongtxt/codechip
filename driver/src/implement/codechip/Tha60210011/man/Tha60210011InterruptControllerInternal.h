/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha60210011InterruptControllerInternal.h
 *
 * Created Date: Jul 27, 2015
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011INTERRUPTCONTROLLERINTERNAL_H_
#define _THA60210011INTERRUPTCONTROLLERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/intrcontroller/ThaIntrControllerInternal.h"


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011InterruptController
    {
    tThaIntrController super;
    } tTha60210011InterruptController;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha60210011IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);
ThaIntrController Tha60210011IntrControllerNew(AtIpCore core);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210011INTERRUPTCONTROLLERINTERNAL_H_ */

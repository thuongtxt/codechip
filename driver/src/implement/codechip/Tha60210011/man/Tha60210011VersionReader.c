/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60210011VersionReader.c
 *
 * Created Date: Sep 4, 2015
 *
 * Description : Tha60210011 version reader
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210011VersionReader.h"

/*--------------------------- Define -----------------------------------------*/
#define cInternalBuildIdFirstIndexMask  cBit15_12
#define cInternalBuildIdFirstIndexShift 12

#define cInternalBuildIdSecondIndexMask  cBit11_8
#define cInternalBuildIdSecondIndexShift 8

#define cInternalBuildIdThirdIndexMask  cBit7_0
#define cInternalBuildIdThirdIndexShift 0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011VersionReader)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011VersionReaderMethods m_methods;

/* Override */
static tThaVersionReaderMethods   m_ThaVersionReaderOverride;
static tThaVersionReaderPwMethods m_ThaVersionReaderPwOverride;

/* Save super implementation */
static const tThaVersionReaderMethods *m_ThaVersionReaderMethods = NULL;
static const tThaVersionReaderPwMethods *m_ThaVersionReaderPwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasInternalBuiltNumber(ThaVersionReader self)
    {
    if (ThaVersionReaderReleasedDate(self) >= ThaVersionReaderReleasedDateBuild(0x15, 0x9, 0x1))
        return cAtTrue;

    return cAtFalse;
    }

static void VersionAndBuiltNumber32BitsFromHwGet(ThaVersionReader self, uint32 *version, uint32 *built)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtDevice device = ThaVersionReaderDeviceGet(self);

    *version = 0;
    *built = 0;
    AtOsalMemInit(longRegVal, 0, cThaLongRegMaxSize);
    if (AtDeviceIpCoreHalGet(device, 0))
        {
        static const uint32 cVersionRegister = 0;

        AtDeviceLongReadOnCore(device, cVersionRegister, longRegVal, cThaLongRegMaxSize, 0);
        *version = longRegVal[1];
        *built = longRegVal[2];
        }
    }

static uint32 BuiltNumberRead(ThaVersionReader self)
    {
    uint32 version, built;
    VersionAndBuiltNumber32BitsFromHwGet(self, &version, &built);

    return (built & cBit15_0);
    }

static uint32 VersionNumberFromHwGet(ThaVersionReader self)
    {
    uint32 version, built;
    VersionAndBuiltNumber32BitsFromHwGet(self, &version, &built);
    return version;
    }

static uint32 BuiltNumberFromHwGet(ThaVersionReader self)
    {
    uint32 version, built;
    VersionAndBuiltNumber32BitsFromHwGet(self, &version, &built);
    return built & cBit15_0;
    }

static char *VersionDescriptionWithoutBuiltNumber(ThaVersionReader self, char *buffer, uint32 bufferLen)
    {
    uint32 versionValue;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 version, day, month, year;

    /* Get version value from hardware */
    versionValue = m_ThaVersionReaderMethods->VersionNumber(self);

    /* Make its description */
    if (bufferLen < 20)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, buffer, 0, bufferLen);
    version  = (uint8)mRegField(versionValue, cVersionHH);
    day      = (uint8)mRegField(versionValue, cVersionDD);
    month    = (uint8)mRegField(versionValue, cVersionMM);
    year     = (uint8)mRegField(versionValue, cVersionYY);

    AtSprintf(buffer, "%d.%d (on 20%d%d/%d%d/%d%d)",
              mLastNible(version) , mFirstNible(version),
              mLastNible(year)    , mFirstNible(year),
              mLastNible(month)   , mFirstNible(month),
              mLastNible(day)     , mFirstNible(day));

    return buffer;
    }

static char *BuilNumberStringMake(ThaVersionReader self, uint8 firstIndex, uint8 secondIndex, uint8 thirdIndex)
    {
    static char buf[16];
    AtUnused(self);

    AtSprintf(buf, "%X%X.%2X", firstIndex, secondIndex, thirdIndex);
    return buf;
    }

static char *VersionDescription(ThaVersionReader self, char *buffer, uint32 bufferLen)
    {
    char* builtNumString;
    uint32 builtNumber;
    uint8 firstIndex, secondIndex, thirdIndex;

    /* Reuse description of super */
    VersionDescriptionWithoutBuiltNumber(self, buffer, bufferLen);
    if (!mMethodsGet(self)->HasInternalBuiltNumber(self))
        return buffer;

    /* Add internal build ID to version description */
    builtNumber = ThaVersionReaderHardwareBuiltNumber(self);
    firstIndex  = (uint8)mRegField(builtNumber, cInternalBuildIdFirstIndex);
    secondIndex = (uint8)mRegField(builtNumber, cInternalBuildIdSecondIndex);
    thirdIndex  = (uint8)mRegField(builtNumber, cInternalBuildIdThirdIndex);

    AtStrcat(buffer, ", built: ");
    builtNumString = mMethodsGet((Tha60210011VersionReader)self)->BuilNumberStringMake(self, firstIndex, secondIndex, thirdIndex);
    AtStrcat(buffer, builtNumString);

    return buffer;
    }


static uint32 VersionNumber(ThaVersionReader self)
    {
    uint32 versionWithDate = m_ThaVersionReaderMethods->VersionNumber(self);
    uint32 builtNumber = ThaVersionReaderHardwareBuiltNumber(self);

    if ((versionWithDate >= mMethodsGet(mThis(self))->StartVersionIgnoreDate(self)) &&
        (builtNumber >= mMethodsGet(mThis(self))->StartBuiltNumberIgnoreDate(self)))
        return versionWithDate & cBit7_0; /* Version only */

    return versionWithDate;
    }

static uint32 VersionBuild(ThaVersionReader self, uint8 _year, uint8 _month, uint8 _day, uint8 _version)
    {
    uint32 versionWithDate = m_ThaVersionReaderMethods->VersionNumber(self);
    uint32 builtNumber = ThaVersionReaderHardwareBuiltNumber(self);

    if ((versionWithDate >= mMethodsGet(mThis(self))->StartVersionIgnoreDate(self)) &&
        (builtNumber >= mMethodsGet(mThis(self))->StartBuiltNumberIgnoreDate(self)))
        return _version;

    return m_ThaVersionReaderPwMethods->VersionBuild(self, _year, _month, _day, _version);
    }

static uint32 StartVersionIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartBuiltNumberIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 ReleasedDate(ThaVersionReader self)
    {
    uint32 versionNumber = m_ThaVersionReaderMethods->VersionNumber(self);
    return mRegField(versionNumber, cVersionDate);
    }


static void MethodsInit(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionIgnoreDate);
        mMethodOverride(m_methods, StartBuiltNumberIgnoreDate);
        mMethodOverride(m_methods, BuilNumberStringMake);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideThaVersionReader(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVersionReaderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderOverride, m_ThaVersionReaderMethods, sizeof(m_ThaVersionReaderOverride));

        mMethodOverride(m_ThaVersionReaderOverride, VersionDescription);
        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberRead);
        mMethodOverride(m_ThaVersionReaderOverride, HasInternalBuiltNumber);
        mMethodOverride(m_ThaVersionReaderOverride, VersionNumber);
        mMethodOverride(m_ThaVersionReaderOverride, ReleasedDate);
        mMethodOverride(m_ThaVersionReaderOverride, VersionNumberFromHwGet);
        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberFromHwGet);
        }

    mMethodsSet(self, &m_ThaVersionReaderOverride);
    }

static void OverrideThaVersionReaderPw(ThaVersionReader self)
    {
    ThaVersionReaderPw pwVerReader = (ThaVersionReaderPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVersionReaderPwMethods = mMethodsGet(pwVerReader);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderPwOverride, m_ThaVersionReaderPwMethods, sizeof(m_ThaVersionReaderPwOverride));
        mMethodOverride(m_ThaVersionReaderPwOverride, VersionBuild);
        }

    mMethodsSet(pwVerReader, &m_ThaVersionReaderPwOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideThaVersionReader(self);
    OverrideThaVersionReaderPw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011VersionReader);
    }

ThaVersionReader Tha60210011VersionReaderObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (Tha60150011VersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60210011VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011VersionReaderObjectInit(newReader, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210011DeviceReg.h
 * 
 * Created Date: Jul 28, 2015
 *
 * Description : Device registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011DEVICEREG_H_
#define _THA60210011DEVICEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cFpgaStickyRegister 0xF00052

#define cXfiFail1_4Mask        cBit11_8
#define cXfiFail5_8Mask        cBit15_12
#define cXfiFailMask(xfi)      (cBit8 << (xfi))
#define cTfi5FailMask          cBit4
#define cDdr1CalibFailMask     cBit3
#define cDdr2CalibFailMask     cBit2
#define cQdrCalibFailMask      cBit1
#define cPllNotLockMask        cBit0
#define cXgmii2_rst_out155Mask cBit17
#define cXgmii1_rst_out155Mask cBit16

/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Status
Reg Addr   : 0x00_0002
Reg Formula:
    Where  :
Reg Desc   :
This register indicate global interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_status_Base                                                          0x000002
#define cAf6Reg_global_interrupt_status                                                               0x000002
#define cAf6Reg_global_interrupt_status_WidthVal                                                            32
#define cAf6Reg_global_interrupt_status_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: ExtPinIntStatus
BitField Type: RO
BitField Desc: External Pin Interrupt Status  1: Set  0: Clear
BitField Bits: [31]
--------------------------------------*/
#define cAf6_global_interrupt_status_ExtPinIntStatus_Bit_Start                                              31
#define cAf6_global_interrupt_status_ExtPinIntStatus_Bit_End                                                31
#define cAf6_global_interrupt_status_ExtPinIntStatus_Mask                                               cBit31
#define cAf6_global_interrupt_status_ExtPinIntStatus_Shift                                                  31
#define cAf6_global_interrupt_status_ExtPinIntStatus_MaxVal                                                0x1
#define cAf6_global_interrupt_status_ExtPinIntStatus_MinVal                                                0x0
#define cAf6_global_interrupt_status_ExtPinIntStatus_RstVal                                                0x0

/*--------------------------------------
BitField Name: ExtOrIntStatus
BitField Type: RO
BitField Desc: External OR Interrupt Status  1: Set  0: Clear
BitField Bits: [30]
--------------------------------------*/
#define cAf6_global_interrupt_status_ExtOrIntStatus_Bit_Start                                               30
#define cAf6_global_interrupt_status_ExtOrIntStatus_Bit_End                                                 30
#define cAf6_global_interrupt_status_ExtOrIntStatus_Mask                                                cBit30
#define cAf6_global_interrupt_status_ExtOrIntStatus_Shift                                                   30
#define cAf6_global_interrupt_status_ExtOrIntStatus_MaxVal                                                 0x1
#define cAf6_global_interrupt_status_ExtOrIntStatus_MinVal                                                 0x0
#define cAf6_global_interrupt_status_ExtOrIntStatus_RstVal                                                 0x0

/*--------------------------------------
BitField Name: ReserveIntStatus
BitField Type: RO
BitField Desc: Reserve Interrupt Status  1: Set  0: Clear
BitField Bits: [29:13]
--------------------------------------*/
#define cAf6_global_interrupt_status_ReserveIntStatus_Bit_Start                                             13
#define cAf6_global_interrupt_status_ReserveIntStatus_Bit_End                                               29
#define cAf6_global_interrupt_status_ReserveIntStatus_Mask                                           cBit29_13
#define cAf6_global_interrupt_status_ReserveIntStatus_Shift                                                 13
#define cAf6_global_interrupt_status_ReserveIntStatus_MaxVal                                           0x1ffff
#define cAf6_global_interrupt_status_ReserveIntStatus_MinVal                                               0x0
#define cAf6_global_interrupt_status_ReserveIntStatus_RstVal                                               0x0

/*--------------------------------------
BitField Name: HoCdrIntStatus
BitField Type: RO
BitField Desc: High Order CDR Interrupt Status  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_interrupt_status_SemIntStatus_Bit_Start                                               12
#define cAf6_global_interrupt_status_SemIntStatus_Bit_End                                                 12
#define cAf6_global_interrupt_status_SemIntStatus_Mask                                                cBit12
#define cAf6_global_interrupt_status_SemIntStatus_Shift                                                   12
#define cAf6_global_interrupt_status_SemIntStatus_MaxVal                                                 0x1
#define cAf6_global_interrupt_status_SemIntStatus_MinVal                                                 0x0
#define cAf6_global_interrupt_status_SemIntStatus_RstVal                                                 0x0

/*--------------------------------------
BitField Name: HoCdrIntStatus
BitField Type: RO
BitField Desc: High Order CDR Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_interrupt_status_HoCdrIntStatus_Bit_Start                                               11
#define cAf6_global_interrupt_status_HoCdrIntStatus_Bit_End                                                 11
#define cAf6_global_interrupt_status_HoCdrIntStatus_Mask                                                cBit11
#define cAf6_global_interrupt_status_HoCdrIntStatus_Shift                                                   11
#define cAf6_global_interrupt_status_HoCdrIntStatus_MaxVal                                                 0x1
#define cAf6_global_interrupt_status_HoCdrIntStatus_MinVal                                                 0x0
#define cAf6_global_interrupt_status_HoCdrIntStatus_RstVal                                                 0x0

/*--------------------------------------
BitField Name: LoCdrIntStatus
BitField Type: RO
BitField Desc: Low Order CDR Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_interrupt_status_LoCdrIntStatus_Bit_Start                                               10
#define cAf6_global_interrupt_status_LoCdrIntStatus_Bit_End                                                 10
#define cAf6_global_interrupt_status_LoCdrIntStatus_Mask                                                cBit10
#define cAf6_global_interrupt_status_LoCdrIntStatus_Shift                                                   10
#define cAf6_global_interrupt_status_LoCdrIntStatus_MaxVal                                                 0x1
#define cAf6_global_interrupt_status_LoCdrIntStatus_MinVal                                                 0x0
#define cAf6_global_interrupt_status_LoCdrIntStatus_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MdlIntStatus
BitField Type: RO
BitField Desc: MDL Interrupt Status  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_interrupt_status_MdlIntStatus_Bit_Start                                                  9
#define cAf6_global_interrupt_status_MdlIntStatus_Bit_End                                                    9
#define cAf6_global_interrupt_status_MdlIntStatus_Mask                                                   cBit9
#define cAf6_global_interrupt_status_MdlIntStatus_Shift                                                      9
#define cAf6_global_interrupt_status_MdlIntStatus_MaxVal                                                   0x1
#define cAf6_global_interrupt_status_MdlIntStatus_MinVal                                                   0x0
#define cAf6_global_interrupt_status_MdlIntStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PrmIntStatus
BitField Type: RO
BitField Desc: PRM Interrupt Status  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_interrupt_status_PrmIntStatus_Bit_Start                                                  8
#define cAf6_global_interrupt_status_PrmIntStatus_Bit_End                                                    8
#define cAf6_global_interrupt_status_PrmIntStatus_Mask                                                   cBit8
#define cAf6_global_interrupt_status_PrmIntStatus_Shift                                                      8
#define cAf6_global_interrupt_status_PrmIntStatus_MaxVal                                                   0x1
#define cAf6_global_interrupt_status_PrmIntStatus_MinVal                                                   0x0
#define cAf6_global_interrupt_status_PrmIntStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PmIntStatus
BitField Type: RO
BitField Desc: PM Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_interrupt_status_PmIntStatus_Bit_Start                                                   7
#define cAf6_global_interrupt_status_PmIntStatus_Bit_End                                                     7
#define cAf6_global_interrupt_status_PmIntStatus_Mask                                                    cBit7
#define cAf6_global_interrupt_status_PmIntStatus_Shift                                                       7
#define cAf6_global_interrupt_status_PmIntStatus_MaxVal                                                    0x1
#define cAf6_global_interrupt_status_PmIntStatus_MinVal                                                    0x0
#define cAf6_global_interrupt_status_PmIntStatus_RstVal                                                    0x0

/*--------------------------------------
BitField Name: FmIntStatus
BitField Type: RO
BitField Desc: FM Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_interrupt_status_FmIntStatus_Bit_Start                                                   6
#define cAf6_global_interrupt_status_FmIntStatus_Bit_End                                                     6
#define cAf6_global_interrupt_status_FmIntStatus_Mask                                                    cBit6
#define cAf6_global_interrupt_status_FmIntStatus_Shift                                                       6
#define cAf6_global_interrupt_status_FmIntStatus_MaxVal                                                    0x1
#define cAf6_global_interrupt_status_FmIntStatus_MinVal                                                    0x0
#define cAf6_global_interrupt_status_FmIntStatus_RstVal                                                    0x0

/*--------------------------------------
BitField Name: TFI5IntStatus
BitField Type: RO
BitField Desc: TFI5 Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_interrupt_status_TFI5IntStatus_Bit_Start                                                 5
#define cAf6_global_interrupt_status_TFI5IntStatus_Bit_End                                                   5
#define cAf6_global_interrupt_status_TFI5IntStatus_Mask                                                  cBit5
#define cAf6_global_interrupt_status_TFI5IntStatus_Shift                                                     5
#define cAf6_global_interrupt_status_TFI5IntStatus_MaxVal                                                  0x1
#define cAf6_global_interrupt_status_TFI5IntStatus_MinVal                                                  0x0
#define cAf6_global_interrupt_status_TFI5IntStatus_RstVal                                                  0x0

/*--------------------------------------
BitField Name: STSIntStatus
BitField Type: RO
BitField Desc: STS Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_interrupt_status_STSIntStatus_Bit_Start                                                  4
#define cAf6_global_interrupt_status_STSIntStatus_Bit_End                                                    4
#define cAf6_global_interrupt_status_STSIntStatus_Mask                                                   cBit4
#define cAf6_global_interrupt_status_STSIntStatus_Shift                                                      4
#define cAf6_global_interrupt_status_STSIntStatus_MaxVal                                                   0x1
#define cAf6_global_interrupt_status_STSIntStatus_MinVal                                                   0x0
#define cAf6_global_interrupt_status_STSIntStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: VTIntStatus
BitField Type: RO
BitField Desc: VT Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_interrupt_status_VTIntStatus_Bit_Start                                                   3
#define cAf6_global_interrupt_status_VTIntStatus_Bit_End                                                     3
#define cAf6_global_interrupt_status_VTIntStatus_Mask                                                    cBit3
#define cAf6_global_interrupt_status_VTIntStatus_Shift                                                       3
#define cAf6_global_interrupt_status_VTIntStatus_MaxVal                                                    0x1
#define cAf6_global_interrupt_status_VTIntStatus_MinVal                                                    0x0
#define cAf6_global_interrupt_status_VTIntStatus_RstVal                                                    0x0

/*--------------------------------------
BitField Name: DE3IntStatus
BitField Type: RO
BitField Desc: DS3/E3 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_interrupt_status_DE3IntStatus_Bit_Start                                                  2
#define cAf6_global_interrupt_status_DE3IntStatus_Bit_End                                                    2
#define cAf6_global_interrupt_status_DE3IntStatus_Mask                                                   cBit2
#define cAf6_global_interrupt_status_DE3IntStatus_Shift                                                      2
#define cAf6_global_interrupt_status_DE3IntStatus_MaxVal                                                   0x1
#define cAf6_global_interrupt_status_DE3IntStatus_MinVal                                                   0x0
#define cAf6_global_interrupt_status_DE3IntStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: DE1IntStatus
BitField Type: RO
BitField Desc: DS1/E1 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_interrupt_status_DE1IntStatus_Bit_Start                                                  1
#define cAf6_global_interrupt_status_DE1IntStatus_Bit_End                                                    1
#define cAf6_global_interrupt_status_DE1IntStatus_Mask                                                   cBit1
#define cAf6_global_interrupt_status_DE1IntStatus_Shift                                                      1
#define cAf6_global_interrupt_status_DE1IntStatus_MaxVal                                                   0x1
#define cAf6_global_interrupt_status_DE1IntStatus_MinVal                                                   0x0
#define cAf6_global_interrupt_status_DE1IntStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PWEIntStatus
BitField Type: RO
BitField Desc: PWE Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_status_PWEIntStatus_Bit_Start                                                  0
#define cAf6_global_interrupt_status_PWEIntStatus_Bit_End                                                    0
#define cAf6_global_interrupt_status_PWEIntStatus_Mask                                                   cBit0
#define cAf6_global_interrupt_status_PWEIntStatus_Shift                                                      0
#define cAf6_global_interrupt_status_PWEIntStatus_MaxVal                                                   0x1
#define cAf6_global_interrupt_status_PWEIntStatus_MinVal                                                   0x0
#define cAf6_global_interrupt_status_PWEIntStatus_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Mask Enable
Reg Addr   : 0x00_0003
Reg Formula:
    Where  :
Reg Desc   :
This register configures global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_mask_enable_Base                                                     0x000003
#define cAf6Reg_global_interrupt_mask_enable                                                          0x000003
#define cAf6Reg_global_interrupt_mask_enable_WidthVal                                                       32
#define cAf6Reg_global_interrupt_mask_enable_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: HoCdrIntEnable
BitField Type: RW
BitField Desc: High Order CDR Interrupt Enable  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_Bit_Start                                          11
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_Bit_End                                            11
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_Mask                                           cBit11
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_Shift                                              11
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_MinVal                                            0x0
#define cAf6_global_interrupt_mask_enable_HoCdrIntEnable_RstVal                                            0x0

/*--------------------------------------
BitField Name: LoCdrIntEnable
BitField Type: RW
BitField Desc: Low Order CDR Interrupt Enable  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_Bit_Start                                          10
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_Bit_End                                            10
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_Mask                                           cBit10
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_Shift                                              10
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_MinVal                                            0x0
#define cAf6_global_interrupt_mask_enable_LoCdrIntEnable_RstVal                                            0x0

/*--------------------------------------
BitField Name: MdlIntEnable
BitField Type: RW
BitField Desc: MDL Interrupt Enable  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_Bit_Start                                             9
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_Bit_End                                               9
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_Mask                                              cBit9
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_Shift                                                 9
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_MaxVal                                              0x1
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_MinVal                                              0x0
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_RstVal                                              0x0

/*--------------------------------------
BitField Name: PrmIntEnable
BitField Type: RW
BitField Desc: PRM Interrupt Enable  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_Bit_Start                                             8
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_Bit_End                                               8
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_Mask                                              cBit8
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_Shift                                                 8
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_MaxVal                                              0x1
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_MinVal                                              0x0
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_RstVal                                              0x0

/*--------------------------------------
BitField Name: PmIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Enable  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PmIntEnable_Bit_Start                                              7
#define cAf6_global_interrupt_mask_enable_PmIntEnable_Bit_End                                                7
#define cAf6_global_interrupt_mask_enable_PmIntEnable_Mask                                               cBit7
#define cAf6_global_interrupt_mask_enable_PmIntEnable_Shift                                                  7
#define cAf6_global_interrupt_mask_enable_PmIntEnable_MaxVal                                               0x1
#define cAf6_global_interrupt_mask_enable_PmIntEnable_MinVal                                               0x0
#define cAf6_global_interrupt_mask_enable_PmIntEnable_RstVal                                               0x0

/*--------------------------------------
BitField Name: FmIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Enable  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_FmIntEnable_Bit_Start                                              6
#define cAf6_global_interrupt_mask_enable_FmIntEnable_Bit_End                                                6
#define cAf6_global_interrupt_mask_enable_FmIntEnable_Mask                                               cBit6
#define cAf6_global_interrupt_mask_enable_FmIntEnable_Shift                                                  6
#define cAf6_global_interrupt_mask_enable_FmIntEnable_MaxVal                                               0x1
#define cAf6_global_interrupt_mask_enable_FmIntEnable_MinVal                                               0x0
#define cAf6_global_interrupt_mask_enable_FmIntEnable_RstVal                                               0x0

/*--------------------------------------
BitField Name: TFI5IntEnable
BitField Type: RW
BitField Desc: TFI5 Interrupt Enable  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_Bit_Start                                            5
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_Bit_End                                              5
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_Mask                                             cBit5
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_Shift                                                5
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_MaxVal                                             0x1
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_MinVal                                             0x0
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_RstVal                                             0x0

/*--------------------------------------
BitField Name: STSIntEnable
BitField Type: RW
BitField Desc: STS Interrupt Enable  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_STSIntEnable_Bit_Start                                             4
#define cAf6_global_interrupt_mask_enable_STSIntEnable_Bit_End                                               4
#define cAf6_global_interrupt_mask_enable_STSIntEnable_Mask                                              cBit4
#define cAf6_global_interrupt_mask_enable_STSIntEnable_Shift                                                 4
#define cAf6_global_interrupt_mask_enable_STSIntEnable_MaxVal                                              0x1
#define cAf6_global_interrupt_mask_enable_STSIntEnable_MinVal                                              0x0
#define cAf6_global_interrupt_mask_enable_STSIntEnable_RstVal                                              0x0

/*--------------------------------------
BitField Name: VTIntEnable
BitField Type: RW
BitField Desc: VT Interrupt Enable  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_VTIntEnable_Bit_Start                                              3
#define cAf6_global_interrupt_mask_enable_VTIntEnable_Bit_End                                                3
#define cAf6_global_interrupt_mask_enable_VTIntEnable_Mask                                               cBit3
#define cAf6_global_interrupt_mask_enable_VTIntEnable_Shift                                                  3
#define cAf6_global_interrupt_mask_enable_VTIntEnable_MaxVal                                               0x1
#define cAf6_global_interrupt_mask_enable_VTIntEnable_MinVal                                               0x0
#define cAf6_global_interrupt_mask_enable_VTIntEnable_RstVal                                               0x0

/*--------------------------------------
BitField Name: DE3IntEnable
BitField Type: RW
BitField Desc: DS3/E3 Interrupt Enable  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_Bit_Start                                             2
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_Bit_End                                               2
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_Mask                                              cBit2
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_Shift                                                 2
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_MaxVal                                              0x1
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_MinVal                                              0x0
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_RstVal                                              0x0

/*--------------------------------------
BitField Name: DE1IntEnable
BitField Type: RW
BitField Desc: DS1/E1 Interrupt Enable  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_Bit_Start                                             1
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_Bit_End                                               1
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_Mask                                              cBit1
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_Shift                                                 1
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_MaxVal                                              0x1
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_MinVal                                              0x0
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_RstVal                                              0x0

/*--------------------------------------
BitField Name: PWEIntEnable
BitField Type: RW
BitField Desc: PWE Interrupt Enable  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_Bit_Start                                             0
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_Bit_End                                               0
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_Mask                                              cBit0
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_Shift                                                 0
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_MaxVal                                              0x1
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_MinVal                                              0x0
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Global PDH Interrupt Status
Reg Addr   : 0x00_0004
Reg Formula:
    Where  :
Reg Desc   :
This register indicate global pdh interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_pdh_interrupt_status_Base                                                      0x000004
#define cAf6Reg_global_pdh_interrupt_status                                                           0x000004
#define cAf6Reg_global_pdh_interrupt_status_WidthVal                                                        32
#define cAf6Reg_global_pdh_interrupt_status_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: DE3Slice12IntStatus
BitField Type: RO
BitField Desc: DE3 Slice12 Interrupt Status  1: Set  0: Clear
BitField Bits: [23]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_Bit_Start                                      23
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_Bit_End                                        23
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_Mask                                       cBit23
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_Shift                                          23
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_MaxVal                                        0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_MinVal                                        0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice12IntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE3Slice11IntStatus
BitField Type: RO
BitField Desc: DE3 Slice11 Interrupt Status  1: Set  0: Clear
BitField Bits: [22]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_Bit_Start                                      22
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_Bit_End                                        22
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_Mask                                       cBit22
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_Shift                                          22
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_MaxVal                                        0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_MinVal                                        0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice11IntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE3Slice10IntStatus
BitField Type: RO
BitField Desc: DE3 Slice10 Interrupt Status  1: Set  0: Clear
BitField Bits: [21]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_Bit_Start                                      21
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_Bit_End                                        21
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_Mask                                       cBit21
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_Shift                                          21
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_MaxVal                                        0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_MinVal                                        0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice10IntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE3Slice9IntStatus
BitField Type: RO
BitField Desc: DE3 Slice9 Interrupt Status  1: Set  0: Clear
BitField Bits: [20]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_Bit_Start                                       20
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_Bit_End                                         20
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_Mask                                        cBit20
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_Shift                                           20
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice9IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice8IntStatus
BitField Type: RO
BitField Desc: DE3 Slice8 Interrupt Status  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_Bit_Start                                       19
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_Bit_End                                         19
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_Mask                                        cBit19
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_Shift                                           19
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice7IntStatus
BitField Type: RO
BitField Desc: DE3 Slice7 Interrupt Status  1: Set  0: Clear
BitField Bits: [18]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_Bit_Start                                       18
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_Bit_End                                         18
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_Mask                                        cBit18
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_Shift                                           18
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice6IntStatus
BitField Type: RO
BitField Desc: DE3 Slice6 Interrupt Status  1: Set  0: Clear
BitField Bits: [17]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_Bit_Start                                       17
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_Bit_End                                         17
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_Mask                                        cBit17
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_Shift                                           17
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice5IntStatus
BitField Type: RO
BitField Desc: DE3 Slice5 Interrupt Status  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_Bit_Start                                       16
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_Bit_End                                         16
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_Mask                                        cBit16
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_Shift                                           16
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice4IntStatus
BitField Type: RO
BitField Desc: DE3 Slice4 Interrupt Status  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_Bit_Start                                       15
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_Bit_End                                         15
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_Mask                                        cBit15
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_Shift                                           15
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice3IntStatus
BitField Type: RO
BitField Desc: DE3 Slice3 Interrupt Status  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_Bit_Start                                       14
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_Bit_End                                         14
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_Mask                                        cBit14
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_Shift                                           14
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice2IntStatus
BitField Type: RO
BitField Desc: DE3 Slice2 Interrupt Status  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_Bit_Start                                       13
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_Bit_End                                         13
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_Mask                                        cBit13
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_Shift                                           13
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE3Slice1IntStatus
BitField Type: RO
BitField Desc: DE3 Slice1 Interrupt Status  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_Bit_Start                                       12
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_Bit_End                                         12
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_Mask                                        cBit12
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_Shift                                           12
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice12IntStatus
BitField Type: RO
BitField Desc: DE1 Slice12 Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_Bit_Start                                      11
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_Bit_End                                        11
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_Mask                                       cBit11
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_Shift                                          11
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_MaxVal                                        0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_MinVal                                        0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice12IntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1Slice11IntStatus
BitField Type: RO
BitField Desc: DE1 Slice11 Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_Bit_Start                                      10
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_Bit_End                                        10
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_Mask                                       cBit10
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_Shift                                          10
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_MaxVal                                        0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_MinVal                                        0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice11IntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1Slice10IntStatus
BitField Type: RO
BitField Desc: DE1 Slice10 Interrupt Status  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_Bit_Start                                       9
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_Bit_End                                         9
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_Mask                                        cBit9
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_Shift                                           9
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_MaxVal                                        0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_MinVal                                        0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice10IntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1Slice9IntStatus
BitField Type: RO
BitField Desc: DE1 Slice9 Interrupt Status  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_Bit_Start                                        8
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_Bit_End                                          8
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_Mask                                         cBit8
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_Shift                                            8
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice9IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice8IntStatus
BitField Type: RO
BitField Desc: DE1 Slice8 Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_Bit_Start                                        7
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_Bit_End                                          7
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_Mask                                         cBit7
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_Shift                                            7
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice7IntStatus
BitField Type: RO
BitField Desc: DE1 Slice7 Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_Bit_Start                                        6
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_Bit_End                                          6
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_Mask                                         cBit6
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_Shift                                            6
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice6IntStatus
BitField Type: RO
BitField Desc: DE1 Slice6 Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_Bit_Start                                        5
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_Bit_End                                          5
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_Mask                                         cBit5
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_Shift                                            5
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice5IntStatus
BitField Type: RO
BitField Desc: DE1 Slice5 Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_Bit_Start                                        4
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_Bit_End                                          4
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_Mask                                         cBit4
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_Shift                                            4
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice4IntStatus
BitField Type: RO
BitField Desc: DE1 Slice4 Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_Bit_Start                                        3
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_Bit_End                                          3
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_Mask                                         cBit3
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_Shift                                            3
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice3IntStatus
BitField Type: RO
BitField Desc: DE1 Slice3 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_Bit_Start                                        2
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_Bit_End                                          2
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_Mask                                         cBit2
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_Shift                                            2
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice2IntStatus
BitField Type: RO
BitField Desc: DE1 Slice2 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_Bit_Start                                        1
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_Bit_End                                          1
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_Mask                                         cBit1
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_Shift                                            1
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1Slice1IntStatus
BitField Type: RO
BitField Desc: DE1 Slice1 Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_Bit_Start                                        0
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_Bit_End                                          0
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_Mask                                         cBit0
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_Shift                                            0
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_MaxVal                                         0x1
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_MinVal                                         0x0
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Pin Disable
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures global interrupt pin disable

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_pin_disable_Base                                                     0x000005
#define cAf6Reg_global_interrupt_pin_disable                                                          0x000005
#define cAf6Reg_global_interrupt_pin_disable_WidthVal                                                       32
#define cAf6Reg_global_interrupt_pin_disable_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: GlbPinIntDisable
BitField Type: RW
BitField Desc: Global Interrupt Pin Disable  1: Disable  0: Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_Bit_Start                                         0
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_Bit_End                                           0
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_Mask                                          cBit0
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_Shift                                             0
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_MaxVal                                          0x1
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_MinVal                                          0x0
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Mask Restore
Reg Addr   : 0x00_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_mask_restore_Base                                                    0x000006
#define cAf6Reg_global_interrupt_mask_restore                                                         0x000006
#define cAf6Reg_global_interrupt_mask_restore_WidthVal                                                      32
#define cAf6Reg_global_interrupt_mask_restore_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: MdlIntRestore
BitField Type: RW
BitField Desc: MDL Interrupt Restore  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_Bit_Start                                           9
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_Bit_End                                             9
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_Mask                                            cBit9
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_Shift                                               9
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_MinVal                                            0x0
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_RstVal                                            0x0

/*--------------------------------------
BitField Name: PrmIntRestore
BitField Type: RW
BitField Desc: PRM Interrupt Restore  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_Bit_Start                                           8
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_Bit_End                                             8
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_Mask                                            cBit8
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_Shift                                               8
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_MinVal                                            0x0
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_RstVal                                            0x0

/*--------------------------------------
BitField Name: PmIntRestore
BitField Type: RW
BitField Desc: PM Interrupt Restore  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_PmIntRestore_Bit_Start                                            7
#define cAf6_global_interrupt_mask_restore_PmIntRestore_Bit_End                                              7
#define cAf6_global_interrupt_mask_restore_PmIntRestore_Mask                                             cBit7
#define cAf6_global_interrupt_mask_restore_PmIntRestore_Shift                                                7
#define cAf6_global_interrupt_mask_restore_PmIntRestore_MaxVal                                             0x1
#define cAf6_global_interrupt_mask_restore_PmIntRestore_MinVal                                             0x0
#define cAf6_global_interrupt_mask_restore_PmIntRestore_RstVal                                             0x0

/*--------------------------------------
BitField Name: FmIntRestore
BitField Type: RW
BitField Desc: FM Interrupt Restore  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_FmIntRestore_Bit_Start                                            6
#define cAf6_global_interrupt_mask_restore_FmIntRestore_Bit_End                                              6
#define cAf6_global_interrupt_mask_restore_FmIntRestore_Mask                                             cBit6
#define cAf6_global_interrupt_mask_restore_FmIntRestore_Shift                                                6
#define cAf6_global_interrupt_mask_restore_FmIntRestore_MaxVal                                             0x1
#define cAf6_global_interrupt_mask_restore_FmIntRestore_MinVal                                             0x0
#define cAf6_global_interrupt_mask_restore_FmIntRestore_RstVal                                             0x0

/*--------------------------------------
BitField Name: TFI5IntRestore
BitField Type: RW
BitField Desc: TFI5 Interrupt Restore  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_Bit_Start                                          5
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_Bit_End                                            5
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_Mask                                           cBit5
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_Shift                                              5
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_MaxVal                                           0x1
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_MinVal                                           0x0
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_RstVal                                           0x0

/*--------------------------------------
BitField Name: STSIntRestore
BitField Type: RW
BitField Desc: STS Interrupt Restore  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_STSIntRestore_Bit_Start                                           4
#define cAf6_global_interrupt_mask_restore_STSIntRestore_Bit_End                                             4
#define cAf6_global_interrupt_mask_restore_STSIntRestore_Mask                                            cBit4
#define cAf6_global_interrupt_mask_restore_STSIntRestore_Shift                                               4
#define cAf6_global_interrupt_mask_restore_STSIntRestore_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_restore_STSIntRestore_MinVal                                            0x0
#define cAf6_global_interrupt_mask_restore_STSIntRestore_RstVal                                            0x0

/*--------------------------------------
BitField Name: VTIntRestore
BitField Type: RW
BitField Desc: VT Interrupt Restore  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_VTIntRestore_Bit_Start                                            3
#define cAf6_global_interrupt_mask_restore_VTIntRestore_Bit_End                                              3
#define cAf6_global_interrupt_mask_restore_VTIntRestore_Mask                                             cBit3
#define cAf6_global_interrupt_mask_restore_VTIntRestore_Shift                                                3
#define cAf6_global_interrupt_mask_restore_VTIntRestore_MaxVal                                             0x1
#define cAf6_global_interrupt_mask_restore_VTIntRestore_MinVal                                             0x0
#define cAf6_global_interrupt_mask_restore_VTIntRestore_RstVal                                             0x0

/*--------------------------------------
BitField Name: DE3IntRestore
BitField Type: RW
BitField Desc: DS3/E3 Interrupt Restore  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_Bit_Start                                           2
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_Bit_End                                             2
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_Mask                                            cBit2
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_Shift                                               2
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_MinVal                                            0x0
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1IntRestore
BitField Type: RW
BitField Desc: DS1/E1 Interrupt Restore  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_Bit_Start                                           1
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_Bit_End                                             1
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_Mask                                            cBit1
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_Shift                                               1
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_MinVal                                            0x0
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_RstVal                                            0x0

/*--------------------------------------
BitField Name: PWEIntRestore
BitField Type: RW
BitField Desc: PWE Interrupt Restore  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_Bit_Start                                           0
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_Bit_End                                             0
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_Mask                                            cBit0
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_Shift                                               0
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_MaxVal                                            0x1
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_MinVal                                            0x0
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Global  Configuration RAM Parity Interrupt Mask Enable
Reg Addr   : 0x00_0007
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures parity global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_parity_interrupt_mask_enable_Base                                              0x000007
#define cAf6Reg_global_parity_interrupt_mask_enable                                                   0x000007
#define cAf6Reg_global_parity_interrupt_mask_enable_WidthVal                                                32
#define cAf6Reg_global_parity_interrupt_mask_enable_WriteMask                                              0x0

/*--------------------------------------
BitField Name: PrmParIntEnable
BitField Type: RW
BitField Desc: PRM Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [31]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_Bit_Start                                      31
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_Bit_End                                      31
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_Mask                                   cBit31
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_Shift                                      31
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: AmeParIntEnable
BitField Type: RW
BitField Desc: AME Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [30]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_Bit_Start                                      30
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_Bit_End                                      30
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_Mask                                   cBit30
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_Shift                                      30
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_AmeParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: PmcParIntEnable
BitField Type: RW
BitField Desc: PMC Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [29]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_Bit_Start                                      29
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_Bit_End                                      29
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_Mask                                   cBit29
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_Shift                                      29
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: ClaParIntEnable
BitField Type: RW
BitField Desc: CLA Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [28]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_Bit_Start                                      28
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_Bit_End                                      28
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_Mask                                   cBit28
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_Shift                                      28
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pwe3ParIntEnable
BitField Type: RW
BitField Desc: PWE ETH Port3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [27]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_Bit_Start                                      27
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_Bit_End                                      27
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_Mask                                  cBit27
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_Shift                                      27
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pwe3ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pwe2ParIntEnable
BitField Type: RW
BitField Desc: PWE ETH Port2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [26]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_Bit_Start                                      26
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_Bit_End                                      26
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_Mask                                  cBit26
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_Shift                                      26
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pwe2ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pwe1ParIntEnable
BitField Type: RW
BitField Desc: PWE ETH Port1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [25]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_Bit_Start                                      25
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_Bit_End                                      25
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_Mask                                  cBit25
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_Shift                                      25
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pwe1ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pwe0ParIntEnable
BitField Type: RW
BitField Desc: PWE ETH Port0 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [24]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_Bit_Start                                      24
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_Bit_End                                      24
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_Mask                                  cBit24
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_Shift                                      24
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pwe0ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: PdaParIntEnable
BitField Type: RW
BitField Desc: PDA Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [23]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_Bit_Start                                      23
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_Bit_End                                      23
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_Mask                                   cBit23
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_Shift                                      23
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: PlaParIntEnable
BitField Type: RW
BitField Desc: PLA Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [22]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_Bit_Start                                      22
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_Bit_End                                      22
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_Mask                                   cBit22
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_Shift                                      22
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Cdr6ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [21]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_Bit_Start                                      21
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_Bit_End                                      21
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_Mask                                  cBit21
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_Shift                                      21
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Map6ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [20]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_Bit_Start                                      20
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_Bit_End                                      20
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_Mask                                  cBit20
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_Shift                                      20
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pdh6ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_Bit_Start                                      19
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_Bit_End                                      19
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_Mask                                  cBit19
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_Shift                                      19
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Cdr5ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice5 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [18]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_Bit_Start                                      18
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_Bit_End                                      18
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_Mask                                  cBit18
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_Shift                                      18
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Map5ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice5 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [17]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_Bit_Start                                      17
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_Bit_End                                      17
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_Mask                                  cBit17
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_Shift                                      17
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pdh5ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice5 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_Bit_Start                                      16
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_Bit_End                                      16
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_Mask                                  cBit16
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_Shift                                      16
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Cdr4ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice4 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_Bit_Start                                      15
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_Bit_End                                      15
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_Mask                                  cBit15
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_Shift                                      15
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Map4ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice4 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_Bit_Start                                      14
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_Bit_End                                      14
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_Mask                                  cBit14
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_Shift                                      14
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pdh4ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice4 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_Bit_Start                                      13
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_Bit_End                                      13
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_Mask                                  cBit13
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_Shift                                      13
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Cdr3ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_Bit_Start                                      12
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_Bit_End                                      12
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_Mask                                  cBit12
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_Shift                                      12
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Map3ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_Bit_Start                                      11
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_Bit_End                                      11
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_Mask                                  cBit11
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_Shift                                      11
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pdh3ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_Bit_Start                                      10
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_Bit_End                                      10
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_Mask                                  cBit10
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_Shift                                      10
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Cdr2ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_Bit_Start                                       9
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_Bit_End                                       9
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_Mask                                   cBit9
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_Shift                                       9
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Map2ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_Bit_Start                                       8
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_Bit_End                                       8
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_Mask                                   cBit8
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_Shift                                       8
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pdh2ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_Bit_Start                                       7
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_Bit_End                                       7
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_Mask                                   cBit7
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_Shift                                       7
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Cdr1ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_Bit_Start                                       6
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_Bit_End                                       6
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_Mask                                   cBit6
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_Shift                                       6
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Map1ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_Bit_Start                                       5
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_Bit_End                                       5
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_Mask                                   cBit5
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_Shift                                       5
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: Pdh1ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_Bit_Start                                       4
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_Bit_End                                       4
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_Mask                                   cBit4
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_Shift                                       4
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: CdrHoParIntEnable
BitField Type: RW
BitField Desc: CDRHO Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_Bit_Start                                       3
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_Bit_End                                       3
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_Mask                                   cBit3
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_Shift                                       3
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_CdrHoParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: MapHoParIntEnable
BitField Type: RW
BitField Desc: MAPHO Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_Bit_Start                                       2
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_Bit_End                                       2
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_Mask                                   cBit2
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_Shift                                       2
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_MapHoParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: PohParIntEnable
BitField Type: RW
BitField Desc: POH Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_Bit_Start                                       1
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_Bit_End                                       1
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_Mask                                    cBit1
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_Shift                                       1
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_RstVal                                     0x0

/*--------------------------------------
BitField Name: OcnParIntEnable
BitField Type: RW
BitField Desc: OCN Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_Bit_Start                                       0
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_Bit_End                                       0
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_Mask                                    cBit0
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_Shift                                       0
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_MaxVal                                     0x1
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_MinVal                                     0x0
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Global  Configuration RAM Parity Interrupt Status
Reg Addr   : 0x00_0008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show global parity interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_parity_interrupt_status_Base                                                   0x000008
#define cAf6Reg_global_parity_interrupt_status                                                        0x000008
#define cAf6Reg_global_parity_interrupt_status_WidthVal                                                     32
#define cAf6Reg_global_parity_interrupt_status_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PrmParIntStatus
BitField Type: RO
BitField Desc: PRM Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [31]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_Bit_Start                                       31
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_Bit_End                                         31
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_Mask                                        cBit31
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_Shift                                           31
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: AmeParIntStatus
BitField Type: RO
BitField Desc: AME Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [30]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_Bit_Start                                       30
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_Bit_End                                         30
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_Mask                                        cBit30
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_Shift                                           30
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_AmeParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: PmcParIntStatus
BitField Type: RO
BitField Desc: PMC Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [29]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_Bit_Start                                       29
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_Bit_End                                         29
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_Mask                                        cBit29
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_Shift                                           29
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: ClaParIntStatus
BitField Type: RO
BitField Desc: CLA Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [28]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_Bit_Start                                       28
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_Bit_End                                         28
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_Mask                                        cBit28
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_Shift                                           28
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: Pwe3ParIntStatus
BitField Type: RO
BitField Desc: PWE ETH Port3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [27]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_Bit_Start                                      27
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_Bit_End                                        27
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_Mask                                       cBit27
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_Shift                                          27
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pwe3ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pwe2ParIntStatus
BitField Type: RO
BitField Desc: PWE ETH Port2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [26]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_Bit_Start                                      26
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_Bit_End                                        26
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_Mask                                       cBit26
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_Shift                                          26
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pwe2ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pwe1ParIntStatus
BitField Type: RO
BitField Desc: PWE ETH Port1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [25]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_Bit_Start                                      25
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_Bit_End                                        25
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_Mask                                       cBit25
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_Shift                                          25
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pwe1ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pwe0ParIntStatus
BitField Type: RO
BitField Desc: PWE ETH Port0 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [24]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_Bit_Start                                      24
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_Bit_End                                        24
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_Mask                                       cBit24
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_Shift                                          24
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pwe0ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: PdaParIntStatus
BitField Type: RO
BitField Desc: PDA Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [23]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_Bit_Start                                       23
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_Bit_End                                         23
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_Mask                                        cBit23
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_Shift                                           23
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: PlaParIntStatus
BitField Type: RO
BitField Desc: PLA Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [22]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_Bit_Start                                       22
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_Bit_End                                         22
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_Mask                                        cBit22
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_Shift                                           22
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: Cdr6ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [21]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_Bit_Start                                      21
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_Bit_End                                        21
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_Mask                                       cBit21
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_Shift                                          21
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Map6ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [20]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_Bit_Start                                      20
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_Bit_End                                        20
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_Mask                                       cBit20
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_Shift                                          20
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pdh6ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_Bit_Start                                      19
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_Bit_End                                        19
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_Mask                                       cBit19
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_Shift                                          19
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Cdr5ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice5 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [18]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_Bit_Start                                      18
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_Bit_End                                        18
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_Mask                                       cBit18
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_Shift                                          18
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Map5ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice5 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [17]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_Bit_Start                                      17
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_Bit_End                                        17
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_Mask                                       cBit17
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_Shift                                          17
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pdh5ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice5 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_Bit_Start                                      16
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_Bit_End                                        16
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_Mask                                       cBit16
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_Shift                                          16
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Cdr4ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice4 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_Bit_Start                                      15
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_Bit_End                                        15
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_Mask                                       cBit15
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_Shift                                          15
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Map4ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice4 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_Bit_Start                                      14
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_Bit_End                                        14
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_Mask                                       cBit14
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_Shift                                          14
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pdh4ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice4 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_Bit_Start                                      13
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_Bit_End                                        13
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_Mask                                       cBit13
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_Shift                                          13
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Cdr3ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_Bit_Start                                      12
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_Bit_End                                        12
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_Mask                                       cBit12
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_Shift                                          12
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Map3ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_Bit_Start                                      11
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_Bit_End                                        11
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_Mask                                       cBit11
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_Shift                                          11
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pdh3ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_Bit_Start                                      10
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_Bit_End                                        10
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_Mask                                       cBit10
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_Shift                                          10
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Cdr2ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_Bit_Start                                       9
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_Bit_End                                         9
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_Mask                                        cBit9
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_Shift                                           9
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Map2ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_Bit_Start                                       8
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_Bit_End                                         8
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_Mask                                        cBit8
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_Shift                                           8
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pdh2ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_Bit_Start                                       7
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_Bit_End                                         7
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_Mask                                        cBit7
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_Shift                                           7
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Cdr1ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_Bit_Start                                       6
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_Bit_End                                         6
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_Mask                                        cBit6
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_Shift                                           6
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Map1ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_Bit_Start                                       5
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_Bit_End                                         5
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_Mask                                        cBit5
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_Shift                                           5
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: Pdh1ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_Bit_Start                                       4
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_Bit_End                                         4
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_Mask                                        cBit4
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_Shift                                           4
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_MaxVal                                        0x1
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_MinVal                                        0x0
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: CdrHoParIntStatus
BitField Type: RO
BitField Desc: CDRHO Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_Bit_Start                                       3
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_Bit_End                                        3
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_Mask                                       cBit3
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_Shift                                          3
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_MaxVal                                       0x1
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_MinVal                                       0x0
#define cAf6_global_parity_interrupt_status_CdrHoParIntStatus_RstVal                                       0x0

/*--------------------------------------
BitField Name: MapHoParIntStatus
BitField Type: RO
BitField Desc: MAPHO Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_Bit_Start                                       2
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_Bit_End                                        2
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_Mask                                       cBit2
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_Shift                                          2
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_MaxVal                                       0x1
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_MinVal                                       0x0
#define cAf6_global_parity_interrupt_status_MapHoParIntStatus_RstVal                                       0x0

/*--------------------------------------
BitField Name: PohParIntStatus
BitField Type: RO
BitField Desc: POH Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PohParIntStatus_Bit_Start                                        1
#define cAf6_global_parity_interrupt_status_PohParIntStatus_Bit_End                                          1
#define cAf6_global_parity_interrupt_status_PohParIntStatus_Mask                                         cBit1
#define cAf6_global_parity_interrupt_status_PohParIntStatus_Shift                                            1
#define cAf6_global_parity_interrupt_status_PohParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_PohParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_PohParIntStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: OcnParIntStatus
BitField Type: RO
BitField Desc: OCN Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_Bit_Start                                        0
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_Bit_End                                          0
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_Mask                                         cBit0
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_Shift                                            0
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_MaxVal                                         0x1
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_MinVal                                         0x0
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Global LoCDR Interrupt Status
Reg Addr   : 0x00_0009
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global low order CDR interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_locdr_interrupt_status_Base                                                    0x000009
#define cAf6Reg_global_locdr_interrupt_status                                                         0x000009
#define cAf6Reg_global_locdr_interrupt_status_WidthVal                                                      32
#define cAf6Reg_global_locdr_interrupt_status_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: LoCDRSlice12IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice12 Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_Bit_Start                                      11
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_Bit_End                                      11
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_Mask                                   cBit11
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_Shift                                      11
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice12IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice11IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice11 Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_Bit_Start                                      10
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_Bit_End                                      10
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_Mask                                   cBit10
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_Shift                                      10
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice11IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice10IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice10 Interrupt Status  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_Bit_Start                                       9
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_Bit_End                                       9
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_Mask                                    cBit9
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_Shift                                       9
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice10IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice9IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice9 Interrupt Status  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_Bit_Start                                       8
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_Bit_End                                       8
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_Mask                                     cBit8
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_Shift                                        8
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice9IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice8IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice8 Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_Bit_Start                                       7
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_Bit_End                                       7
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_Mask                                     cBit7
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_Shift                                        7
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice7IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice7 Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_Bit_Start                                       6
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_Bit_End                                       6
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_Mask                                     cBit6
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_Shift                                        6
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice6IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice6 Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_Bit_Start                                       5
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_Bit_End                                       5
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_Mask                                     cBit5
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_Shift                                        5
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice5IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice5 Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_Bit_Start                                       4
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_Bit_End                                       4
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_Mask                                     cBit4
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_Shift                                        4
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice4IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice4 Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_Bit_Start                                       3
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_Bit_End                                       3
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_Mask                                     cBit3
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_Shift                                        3
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice3IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice3 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_Bit_Start                                       2
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_Bit_End                                       2
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_Mask                                     cBit2
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_Shift                                        2
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice2IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice2 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_Bit_Start                                       1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_Bit_End                                       1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_Mask                                     cBit1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_Shift                                        1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LoCDRSlice1IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice1 Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_Bit_Start                                       0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_Bit_End                                       0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_Mask                                     cBit0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_Shift                                        0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_MaxVal                                     0x1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_MinVal                                     0x0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_RstVal                                     0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011DEVICEREG_H_ */


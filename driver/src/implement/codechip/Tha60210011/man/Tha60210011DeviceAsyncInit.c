/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210011DeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : To handle device async init
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011DeviceInternal.h"
#include "Tha60210011DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumUsPerMs 1000
#define cFixedTimeWaitAfterReactiveInMs 1000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210011Device)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eTha60210021DeviceInitState
    {
    cDeviceInitStateUnbindAllPw = 0,
    cDeviceInitStateHwFlush,
    cDeviceInitStateReactivated,
    cDeviceInitStateSupperInit,
    cDeviceInitStatePweAllPsnFlush,
    cDeviceInitStateAllSerdesReset,
    cDeviceInitStateAllModulesHwResourceCheck,
    }eTha60210021DeviceInitState;

typedef enum eAsyncReactivateStates
    {
    cTha60210011AsyncActivateOff,
    cTha60210011AsyncActivateWait,
    cTha60210011AsyncActivateOn
    } eAsyncReactivateStates;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncReactivateStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncReactivateState = state;
    }

static void AsyncReactivateStateReset(AtDevice self)
    {
    mThis(self)->asyncReactivateState = cTha60210011AsyncActivateOff;
    }

static uint32 AsyncReactivateStateGet(AtDevice self)
    {
    return mThis(self)->asyncReactivateState;
    }

static eAtRet AsyncActivateOff(AtDevice self)
    {
    eAtRet ret = cAtOk;
    eBool activated;
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);

    if (!mMethodsGet(mThis(self))->ShouldReActivate(mThis(self)))
        return cAtOk;

    activated = AtDeviceAllCoresAreActivated(self);
    if (!activated)
        return cAtOk;

    /* As discussed with hardware, all modules need to be reactivated if they were
     * activated before. The purpose of doing this is also to cleanup hardware
     * resources and reset internal state machines */
    ret = AtDeviceAllCoresActivate(self, cAtFalse);
    if (ret == cAtOk)
        {
        AtOsalCurTimeGet(&mThis(self)->reactivaatePrevStateTime);
        AtDeviceAsyncInitNeedFixDelayTimeSet(self, cFixedTimeWaitAfterReactiveInMs * cNumUsPerMs);
        AsyncReactivateStateSet(self, cTha60210011AsyncActivateWait);
        ret = cAtRetNeedDelay;
        }

    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AsyncActivateOff");
    return ret;
    }

static eAtRet AsyncActivateWait(AtDevice self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);
    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &mThis(self)->reactivaatePrevStateTime);
    if (diffTimeInMs >= cFixedTimeWaitAfterReactiveInMs)
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(self, 0);
        AsyncReactivateStateSet(self, cTha60210011AsyncActivateOn);
        ret = cAtErrorAgain;
        }
    else
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(self, (cFixedTimeWaitAfterReactiveInMs - diffTimeInMs)*cNumUsPerMs);
        ret = cAtRetNeedDelay;
        }

    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AsyncActivateWait");
    return ret;
    }

static eAtRet AsyncActivateOn(AtDevice self)
    {
    tAtOsalCurTime profileTime;
    eAtRet ret;

    AtOsalCurTimeGet(&profileTime);

    ret = AtDeviceAllCoresActivate(self, cAtTrue);
    AsyncReactivateStateReset(self);

    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AsyncActivateOn");
    return ret;
    }

static eAtRet AsyncReactivate(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncReactivateStateGet(self);
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);
    switch (state)
        {
        case cTha60210011AsyncActivateOff:
            ret = AsyncActivateOff(self);
            break;
        case cTha60210011AsyncActivateWait:
            ret = AsyncActivateWait(self);
            break;
        case cTha60210011AsyncActivateOn:
            ret = AsyncActivateOn(self);
            break;
        default:
            AsyncReactivateStateReset(self);
            ret = cAtErrorDevFail;
        }

    return ret;
    }

static void AsyncInitStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

static void AsyncInitStateReset(AtDevice self)
    {
    mThis(self)->asyncInitState = cDeviceInitStateUnbindAllPw;
    }

static uint32 AsyncInitStateGet(AtDevice self)
    {
    return mThis(self)->asyncInitState;
    }

static eAtRet StateUnbindAllPw(AtDevice self)
    {
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);

    Tha60210011DeviceUnbindAllPws(self);
    Tha60210011DeviceBerRequestDdrDisable(self);
    if (Tha60210011DeviceSupportDisableJnRequestDdr(self))
        Tha60210011DeviceJnRequestDdrEnable(self, cAtFalse);

    AsyncInitStateSet(self, cDeviceInitStateHwFlush);

    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "StateUnbindAllPw");
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "StateUnbindAllPw");
    return cAtErrorAgain;
    }

static eAtRet StateHwFlush(AtDevice self)
    {
    tAtOsalCurTime profileTime;
    eAtRet ret;

    AtOsalCurTimeGet(&profileTime);

    ret = mMethodsGet(mThis(self))->HwAsyncFlush(mThis(self));
    if (ret == cAtOk)
        {
	    AsyncInitStateSet(self, cDeviceInitStateReactivated);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
	    AsyncInitStateReset(self);

    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "StateHwFlush");
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "StateHwFlush");
    return ret;
    }

static eAtRet StateReactivated(AtDevice self)
    {
    tAtOsalCurTime profileTime;
    eAtRet ret;

    AtOsalCurTimeGet(&profileTime);

    ret = AsyncReactivate(self);
    AtDeviceDiagnosticCheckEnable(self, Tha60210011DeviceCanRunDiagnostic(self));
    if (ret == cAtOk)
        {
	    AsyncInitStateSet(self, cDeviceInitStateSupperInit);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
	    AsyncInitStateReset(self);

    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "StateReactivated");
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "StateReactivated");
    return ret;
    }

static eAtRet StateSupperInit(AtDevice self)
    {
    eAtRet ret;
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);

    ret = Tha60210011DeviceSuperAsyncInit(self);
    if (ret == cAtOk)
        {
	    AsyncInitStateSet(self, cDeviceInitStatePweAllPsnFlush);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
	    AsyncInitStateReset(self);

    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "StateSupperInit");
    return ret;
    }

static eAtRet StatePweAllPsnFlush(AtDevice self)
    {
    eAtRet ret;
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);

    ret = Tha60210011DevicePwePsnFlush(self);
    if (ret == cAtOk)
        {
	    AsyncInitStateSet(self, cDeviceInitStateAllSerdesReset);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
	    AsyncInitStateReset(self);

    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "StatePweAllPsnFlush");
    return ret;
    }

static eAtRet StateAllSerdesReset(AtDevice self)
    {
    tAtOsalCurTime profileTime;
    eAtRet ret;

    AtOsalCurTimeGet(&profileTime);

    if (Tha60210011DeviceSupportDisableJnRequestDdr(self))
        Tha60210011DeviceJnRequestDdrEnable(self, cAtTrue);

    ret = Tha60210011DeviceAllSerdesReset(self);
    if (ret == cAtOk)
        {
        AsyncInitStateSet(self, cDeviceInitStateAllModulesHwResourceCheck);
        ret = cAtErrorAgain;
        }

    else if (!AtDeviceAsyncRetValIsInState(ret))
        AsyncInitStateReset(self);

    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "StateAllSerdesReset");
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "StateAllSerdesReset");

    return ret;
    }

static eAtRet StateAllModulesHwResourceCheck(AtDevice self)
    {
    tAtOsalCurTime profileTime;
    eAtRet ret;

    AtOsalCurTimeGet(&profileTime);

    ret = AtDeviceAllModulesHwResourceCheck(self);
    AsyncInitStateReset(self);

    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AllModulesHwResourceCheck");
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AllModulesHwResourceCheck");

    return ret;
    }

eAtRet Tha60210011DeviceAsyncInit(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncInitStateGet(self);

    switch(state)
        {
        case cDeviceInitStateUnbindAllPw:
            ret = StateUnbindAllPw(self);
            break;
        case cDeviceInitStateHwFlush:
            ret = StateHwFlush(self);
            break;
        case cDeviceInitStateReactivated:
            ret = StateReactivated(self);
            break;
        case cDeviceInitStateSupperInit:
            ret = StateSupperInit(self);
            break;
        case cDeviceInitStatePweAllPsnFlush:
            ret = StatePweAllPsnFlush(self);
            break;
        case cDeviceInitStateAllSerdesReset:
            ret = StateAllSerdesReset(self);
            break;
        case cDeviceInitStateAllModulesHwResourceCheck:
            ret = StateAllModulesHwResourceCheck(self);
            break;
        default:
            AsyncInitStateReset(self);
            ret = cAtErrorDevFail;
        }

    return ret;
    }

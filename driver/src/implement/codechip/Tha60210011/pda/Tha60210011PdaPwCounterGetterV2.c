/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210011PdaPwCounterGetterV2.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PDA pw counter getter V2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwRxPldOctCnt(ro)          (0x1E24000UL + (ro) * 0x2000UL)
#define cPwRxReorderDropPktCnt(ro)  (0x1E3C000UL + (ro) * 0x2000UL)
#define cPwRxReorderedPktCnt(ro)    (0x1E40000UL + (ro) * 0x2000UL)
#define cPwRxLofsTransCnt(ro)       (0x1E28000UL + (ro) * 0x2000UL)
#define cPwRxJitBufOverrunPktCnt(ro)(0x1E48000UL + (ro) * 0x2000UL)
#define cPwJitBufUnderrunEvtCnt(ro) (0x1E4C000UL + (ro) * 0x2000UL)
#define cPwRxReorderLostPktCnt(ro)  (0x1E44000UL + (ro) * 0x2000UL)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210011PdaPwCounterGetterV2
    {
    tTha60210011PdaPwCounterGetter super;
    }tTha60210011PdaPwCounterGetterV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tTha60210011PdaPwCounterGetterMethods m_Tha60210011PdaPwCounterGetterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(AtPw pw)
    {
    return AtChannelIdGet((AtChannel)pw);
    }

static uint32 RxBytesGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwRxPldOctCnt((clear) ? 0 : 1) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxReorderedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwRxReorderedPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxLostPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwRxReorderLostPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxOutOfSeqDropedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwRxReorderDropPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxJitBufOverrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwRxJitBufOverrunPktCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxJitBufUnderrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwJitBufUnderrunEvtCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static uint32 RxLopsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(modulePda);
    return mChannelHwRead(pw, cPwRxLofsTransCnt(mRo(clear)) + CounterOffset(pw), cThaModulePwPmc);
    }

static void OverrideTha60210011PdaPwCounterGetter(Tha60210011PdaPwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PdaPwCounterGetterOverride, mMethodsGet(self), sizeof(m_Tha60210011PdaPwCounterGetterOverride));

        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxBytesGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxReorderedPacketsGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxLostPacketsGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxJitBufOverrunGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxJitBufUnderrunGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxLopsGet);
        }

    mMethodsSet(self, &m_Tha60210011PdaPwCounterGetterOverride);
    }

static void Override(Tha60210011PdaPwCounterGetter self)
    {
    OverrideTha60210011PdaPwCounterGetter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdaPwCounterGetterV2);
    }

static Tha60210011PdaPwCounterGetter ObjectInit(Tha60210011PdaPwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdaPwCounterGetterObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011PdaPwCounterGetter Tha60210011PdaPwCounterGetterV2New(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011PdaPwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGetter);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210011ModulePda.c
 *
 * Created Date: Jun 25, 2014
 *
 * Description : PDA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pw/ThaPwUtil.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../pw/Tha60210011ModulePw.h"
#include "../man/Tha60210011Device.h"
#include "../ram/Tha60210011InternalRam.h"
#include "Tha60210011ModulePdaReg.h"
#include "Tha60210011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPdaTdmLookupHwResetValue 0x17FF

/*--------------------------- Macros -----------------------------------------*/
#define mUs2Ms(us) (((us) / 1000) + (((us) % 1000) ? 1 : 0))
#define mMs2Us(us) ((us) * 1000)
#define mModuleV2(self) ((ThaModulePdaV2)self)
#define mThis(self) ((Tha60210011ModulePda)self)
#define mCounterGetter(self) mMethodsGet(mThis(self))->CounterGetter(mThis(self))
#define mBaseAddress(self) ThaModulePdaBaseAddress((ThaModulePda)self)

#define cRegJitterBufferCenter 0x518000UL

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011ModulePdaMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tThaModulePdaV2Methods       m_ThaModulePdaV2Override;
static tTha60150011ModulePdaMethods m_Tha60150011ModulePdaOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModulePdaMethods *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaModulePda self)
    {
    AtUnused(self);
    return 0x500000;
    }

static uint32 PwDefaultOffset(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw);
    }

static eBool JitterBufferCenterIsSupported(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PwLopsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
    uint32 lopsInUs = ThaModulePdaCalculateUsByNumberOfPacket(self, pw, numPackets);
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regVal, cAf6_ramreorcfg_PwSetLopsInMsec_, mUs2Ms(lopsInUs));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);
    return cAtOk;
    }

static uint32 PwLopsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
    uint32 lopsInMs;

    uint32 regVal = mChannelHwRead(pw, address, cThaModulePda);
    lopsInMs = mRegField(regVal, cAf6_ramreorcfg_PwSetLopsInMsec_);

    return ThaModulePdaCalculateNumPacketsByUs(self, pw, mMs2Us(lopsInMs));
    }

static eAtRet PwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    eAtPwType pwType = AtPwTypeGet(pw);
    uint32 address = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
    uint32 regVal;

    regVal = mChannelHwRead(pw, address, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramreorcfg_PwSetLofsInPk_, numPackets);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    /* In case SAToP and CESoP, field LOPS also need to configure corresponding */
    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        return PwLopsSetThresholdSet(self, pw, numPackets);

    return cAtOk;
    }

static uint32 PwLofsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
    uint32 regVal;

    regVal = mChannelHwRead(pw, address, cThaModulePda);

    return mRegField(regVal, cAf6_ramreorcfg_PwSetLofsInPk_);
    }

static uint32 ReoderTimeoutCalculate(ThaModulePda self, AtPw pw)
    {
    uint16 jitterDelayInPackets;
    uint32 payloadInBytes;
    uint32 speedInKbps;
    AtChannel circuit = AtPwBoundCircuitGet(pw);

    jitterDelayInPackets = ThaModulePdaPwJitterBufferDelayInPacketGet(self, pw);
    if (jitterDelayInPackets > 31)
        jitterDelayInPackets = 31;

    speedInKbps = mSpeedInKbps(ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit));
    if (speedInKbps == 0)
        return cDefaultJitterTimeout;

    payloadInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, AtPwPayloadSizeGet(pw));
    return (jitterDelayInPackets * payloadInBytes * 16) / speedInKbps;
    }

static eAtRet PwReorderingEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 reorderTimeout;

    if (!enable)
        {
        regAddr = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
        regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
        mRegFieldSet(regVal, cAf6_ramreorcfg_PwReorEn_, 0);
        mRegFieldSet(regVal, cAf6_ramreorcfg_PwReorTimeout_, cDefaultJitterTimeout);
        mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

        return cAtOk;
        }

    regAddr = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);

    /* HW recommended, reorder time out must >= 2 */
    reorderTimeout = ReoderTimeoutCalculate(self, pw);
    if (reorderTimeout < 2)
        reorderTimeout = 2;

    mRegFieldSet(regVal, cAf6_ramreorcfg_PwReorTimeout_, reorderTimeout);
    mRegFieldSet(regVal, cAf6_ramreorcfg_PwReorEn_, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool PwReorderingIsEnable(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    return (regVal & cAf6_ramreorcfg_PwReorEn_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 HwLimitNumPktForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    eAtSdhChannelType channelType;
    AtUnused(self);
    AtUnused(payloadSize);

    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return 2048;

    channelType = AtSdhChannelTypeGet((AtSdhChannel)AtPwBoundCircuitGet(pw));
    if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        return 2048;

    return 8191;
    }

static uint32 PWPdaJitBufCtrl(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6Reg_ramjitbufcfg_Base + mBaseAddress(self);
    }

static uint32 JitterBufferSizePktMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Mask;
    }

static uint8 JitterBufferSizePktShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Shift;
    }

static uint32 JitterBufferDelayPktMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PdvSizeInPkUnit_Mask;
    }

static uint8 JitterBufferDelayPktShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PdvSizeInPkUnit_Shift;
    }

static uint32 PDATdmJitBufStat(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6Reg_ramjitbufsta_Base + mBaseAddress(self);
    }

static uint32 BufferNumPktMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufsta_JitBufNumPk_Mask;
    }

static uint8 BufferNumPktShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufsta_JitBufNumPk_Shift;
    }

static uint32 BufferNumAdditionalBytesMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufsta_JitBufExtraByte_Mask;
    }

static uint8  BufferNumAdditionalBytesShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufsta_JitBufExtraByte_Shift;
    }

static const char *JitterBufferStateString(ThaModulePda self, uint32 hwState)
    {
    AtUnused(self);
    if (hwState == cThaPdaJitterBufferStateStart)   return "start";
    if (hwState == cThaPdaJitterBufferStateFilling) return "filling";
    if (hwState == 2) return "ready";
    if (hwState == 3) return "read";
    if (hwState == 4) return "lost";
    if (hwState == 5) return "near_empty";

    return "xxx";
    }

static AtModulePw PwModule(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 LoTdmModeOffset(Tha60210011ModulePda self, uint32 oc48Slice, uint32 oc24Slice, uint32 channelHwId)
    {
    uint32 oc24SliceOffset;
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)PwModule((ThaModulePda)self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        oc24SliceOffset = oc24Slice * 0x400UL;
    else
        oc24SliceOffset = oc24Slice * 2048UL;

    return (oc48Slice * 0x1000UL) + oc24SliceOffset + channelHwId;
    }

static uint32 PwTdmOffset(ThaModulePda self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 hwIdInSlice = 0;

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        uint32 oc48Id = slice / 2;
        uint32 oc24SliceId = slice % 2;

        return mMethodsGet(mThis(self))->LoTdmModeOffset(mThis(self), oc48Id, oc24SliceId, AtChannelHwIdGet((AtChannel)pw));
        }

    return (slice * 0x100UL) + hwIdInSlice;
    }

static eBool HasHoBus(AtModule self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet(self);
    return Tha60210011DeviceHasHoBus(device);
    }

static eBool ShouldUseLoRegisters(ThaModulePda self, AtPw pw)
    {
    if (HasHoBus((AtModule)self))
        return Tha60210011PwCircuitBelongsToLoLine(pw);

    return cAtTrue;
    }

static uint32 PWPdaTdmModeCtrl(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6Reg_ramlotdmmodecfg_Base + mBaseAddress(self);

    return cAf6Reg_ramhotdmmodecfg_Base + mBaseAddress(self);
    }

static uint32 PdaNumNxDs0Mask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramlotdmmodecfg_PDANxDS0_Mask;
    }

static uint8 PdaNumNxDs0Shift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramlotdmmodecfg_PDANxDS0_Shift;
    }

static uint32 PktReplaceModeMask(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6_ramlotdmmodecfg_PDARepMode_Mask;

    return cAf6_ramhotdmmodecfg_PDAHoRepMode_Mask;
    }

static uint8 PktReplaceModeShift(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6_ramlotdmmodecfg_PDARepMode_Shift;

    return cAf6_ramhotdmmodecfg_PDAHoRepMode_Shift;
    }

static uint32 PdaIdleCodeMask(ThaModulePdaV2 self, AtPw pw)
    {
    if (ShouldUseLoRegisters((ThaModulePda)self, pw))
        return cAf6_ramlotdmmodecfg_PDAIdleCode_Mask;

    return cAf6_ramhotdmmodecfg_PDAHoIdleCode_Mask;
    }

static uint8 PdaIdleCodeShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6_ramlotdmmodecfg_PDAIdleCode_Shift;

    return cAf6_ramhotdmmodecfg_PDAHoIdleCode_Shift;
    }

static uint32 AisRdiUneqOffMask(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_Mask;

    return cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Mask;
    }

static uint8 AisRdiUneqOffShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_Shift;

    return cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Shift;
    }

static eBool CircuitIsLoVc3(AtPw pwAdapter)
	{
	AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pwAdapter);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
	return  (channelType == cAtSdhChannelTypeVc3) ? cAtTrue : cAtFalse;
	}

static uint8 LoHwPdaMode(AtPw pwAdapter)
    {
    eAtPwType pwType = AtPwTypeGet(pwAdapter);

    if (pwType == cAtPwTypeSAToP)
        {
        AtPdhChannel pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pwAdapter);
        eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);
        if ((channelType == cAtPdhChannelTypeDs1) || (channelType == cAtPdhChannelTypeE1))
            return 0;
        if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
            return 15;
        }

    if (pwType == cAtPwTypeCEP)
        return CircuitIsLoVc3(pwAdapter) ? 8 : 12;

    if (pwType == cAtPwTypeCESoP)
        return 2;

    return 0;
    }

static uint8 HoHwPdaMode(AtPw pwAdapter)
    {
    AtSdhChannel sdhChannel;
    uint8 numSts;

    if (AtPwTypeGet(pwAdapter) != cAtPwTypeCEP)
        return 0;

    sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pwAdapter);
    numSts = AtSdhChannelNumSts(sdhChannel);

    if ((numSts == 1) || ((numSts % 3) != 0))
        return 0;

    if ((numSts == 3) || (numSts == 6))
        return 1;

    if ((numSts == 9) || (numSts == 12) || (numSts == 15))
        return 2;

    if (numSts <= 48)
        return 3;

    return 0;
    }

static eAtRet LoPwPdaModeSet(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ramlotdmmodecfg_Base + mBaseAddress(self) + mTdmOffset(self, pwAdapter);
    regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramlotdmmodecfg_PDAMode_, LoHwPdaMode(pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet HoPwPdaModeSet(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_ramhotdmmodecfg_Base + mBaseAddress(self) + mTdmOffset(self, pwAdapter);
    regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramhotdmmodecfg_PDAHoMode_, HoHwPdaMode(pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 StartVersionSupportLowRateCesop(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x0);
    }

static eBool CesopLowRateControlIsSupported(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersion = mMethodsGet(mThis(self))->StartVersionSupportLowRateCesop(mThis(self));

    if (hwVersion >= startVersion)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PwTdmLowRateOffset(Tha60210011ModulePda self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 hwIdInSlice = 0;
    uint32 oc48Id, oc24SliceId;
    uint32 oc48Offset, oc24Offset;
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)AtChannelModuleGet((AtChannel)pw);

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    oc48Id = slice / 2;
    oc24SliceId = slice % 2;

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        {
        oc48Offset = oc48Id * 0x2000UL;
        oc24Offset = oc24SliceId * 0x1000UL;
        }
    else
        {
        oc48Offset = oc48Id * 16384UL;
        oc24Offset = oc24SliceId * 8192UL;
        }

    return oc48Offset + oc24Offset + AtChannelHwIdGet((AtChannel)pw);
    }

static eAtRet PWCESoPLowRateEnable(ThaModulePda self, AtPw pwAdapter)
    {
    uint8 hwEnable = 0;
    uint32 regAddr, regValue;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 fieldMask, fieldShift, offset;

    if (!CesopLowRateControlIsSupported(self))
        return cAtOk;

    if (AtPwTypeGet(pwAdapter) == cAtPwTypeCESoP)
        {
        AtPdhNxDS0 circuit = (AtPdhNxDS0)AtPwBoundCircuitGet(pwAdapter);
        uint8 numTs = AtPdhNxDs0NumTimeslotsGet(circuit);

        hwEnable = (numTs <= 3) ? 1 : 0;
        }

    /* Small timeslot enabling is required to be configured for other kinds of
     * PW as well. So far, this is misunderstood that only CESoP then HW will
     * care this */
    offset = mMethodsGet(mThis(self))->PwTdmLowRateOffset(mThis(self), pwAdapter);
    regAddr = mMethodsGet(mThis(self))->LoTdmSmallDS0Control(mThis(self)) + mBaseAddress(self) + offset;
    regValue = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    mRegFieldSet(regValue, cAf6_ramlotdmsmallds0control_PwLowDs0Mode_, hwEnable);
    mChannelHwWrite(pwAdapter, regAddr, regValue, cThaModulePda);

    regAddr = PWPdaJitBufCtrl((ThaModulePdaV2)self) + mPwOffset(self, pwAdapter);
    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    fieldMask = mMethodsGet(mThis(self))->PwLowDs0ModeMask(mThis(self));
    fieldShift = mMethodsGet(mThis(self))->PwLowDs0ModeShift(mThis(self));
    mRegFieldSet(longRegVal[1], field, hwEnable);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwPdaModeSet(ThaModulePda self, AtPw pwAdapter, uint8 mode)
    {
    AtUnused(mode);

    if (Tha60210011PwCircuitBelongsToLoLine(pwAdapter))
        {
        Tha60210011ModulePdaPWCESoPLowRateEnable(self, pwAdapter);
        return LoPwPdaModeSet(self, pwAdapter);
        }

    return HoPwPdaModeSet(self, pwAdapter);
    }

static eBool IsLoTu3Vc3Cep(AtPw pw)
	{
	if ((AtPwTypeGet(pw) == cAtPwTypeCEP) && CircuitIsLoVc3(pw))
		return cAtTrue;

	return cAtFalse;
	}

static eAtRet HwStsIdSet(ThaModulePda self, AtPw pw)
    {
    uint8 hwSlice, hwSts;
    uint32 regAddr, regVal;
    AtPdhChannel pdhChannel;
    eAtPdhChannelType channelType;
    eBool isTu3Vc3CepPw = IsLoTu3Vc3Cep(pw);
    eBool needToSetHwStsId;

    /* SAToP and TU-3's VC-3 needs to configure hardware STS */
    needToSetHwStsId = ((AtPwTypeGet(pw) == cAtPwTypeSAToP) || isTu3Vc3CepPw) ? cAtTrue : cAtFalse;
    if (!needToSetHwStsId)
        return cAtOk;

    if (isTu3Vc3CepPw)
    	{
        ThaSdhChannel2HwMasterStsId((AtSdhChannel)AtPwBoundCircuitGet(pw), cThaModulePda, &hwSlice, &hwSts);
    	}
    else
		{
    pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
    channelType = AtPdhChannelTypeGet(pdhChannel);
    if ((channelType != cAtPdhChannelTypeDs3) && (channelType != cAtPdhChannelTypeE3))
        return cAtOk;

    ThaPdhChannelHwIdGet(pdhChannel, cThaModulePda, &hwSlice, &hwSts);
		}

    regAddr = cAf6Reg_ramlotdmmodecfg_Base + mBaseAddress(self) + mTdmOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramlotdmmodecfg_PDAStsId_, hwSts);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 LoTdmLookupOffset(Tha60210011ModulePda self, uint32 oc48Slice, uint32 oc24Slice, uint32 channelHwId)
    {
    uint32 oc48Offset, oc24Offset;
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)PwModule((ThaModulePda)self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        {
        oc48Offset = oc48Slice * 0x800UL;
        oc24Offset = oc24Slice * 0x400UL;
        }
    else
        {
        oc48Offset = oc48Slice * 4096UL;
        oc24Offset = oc24Slice * 2048UL;
        }

    return oc48Offset + oc24Offset + channelHwId;
    }

static uint32 PwLoTdmLookupOffset(Tha60210011ModulePda self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 oc48Id, oc24SliceId;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    oc48Id = slice / 2;
    oc24SliceId = slice % 2;
    return mMethodsGet(self)->LoTdmLookupOffset(self, oc48Id, oc24SliceId, AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 LoTdmLookupControl(Tha60210011ModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->LoTdmLookupCtrl(self, pw) + mBaseAddress(self) + mMethodsGet(self)->PwLoTdmLookupOffset(self, pw);
    }

static eAtRet HwPwTdmLookupSet(Tha60210011ModulePda self, uint32 address, AtPw pw)
    {
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePda);
    uint32 lookupPwIdMask = mMethodsGet(self)->PwLookupPwIDMask(self);
    uint8 lookupPwIdShift = mMethodsGet(self)->PwLookupPwIDShift(self);
    mRegFieldSet(regVal, lookupPwId, AtChannelIdGet((AtChannel)pw));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwLoTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    return HwPwTdmLookupSet(self, LoTdmLookupControl(self, pw), pw);
    }

static eAtRet PwLoTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    mChannelHwWrite(pw, LoTdmLookupControl(self, pw), cPdaTdmLookupHwResetValue, cThaModulePda);
    return cAtOk;
    }

static uint32 PwHoTdmLookupOffset(Tha60210011ModulePda self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 hwIdInSlice = 0;
    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return  slice * 0x40UL + hwIdInSlice;
    }

static eBool RamhotdmlkupcfgAddressChanged(Tha60210011ModulePda self)
    {
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)PwModule((ThaModulePda)self);
    uint32 maxPws = Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule);
    uint32 originalMaxPws = Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule);
    return (maxPws > originalMaxPws) ? cAtTrue : cAtFalse;
    }

static uint32 HoTdmLookupCtrl(Tha60210011ModulePda self)
    {
    if (RamhotdmlkupcfgAddressChanged(self))
        return 0x00048000;

    return cAf6Reg_ramhotdmlkupcfg_Base;
    }

static uint32 PwLookupPwIDMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramhotdmlkupcfg_PwID_Mask;
    }

static uint8 PwLookupPwIDShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramhotdmlkupcfg_PwID_Shift;
    }

static uint32 HoTdmLookupControl(Tha60210011ModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->HoTdmLookupCtrl(self) + mBaseAddress(self) + mMethodsGet(self)->PwHoTdmLookupOffset(self, pw);
    }

static eAtRet PwHoTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    return HwPwTdmLookupSet(self, HoTdmLookupControl(self, pw), pw);
    }

static eAtRet PwHoTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    mChannelHwWrite(pw, HoTdmLookupControl(self, pw), cPdaTdmLookupHwResetValue, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwCircuitIdSet(ThaModulePda self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        eAtRet ret = HwStsIdSet(self, pw);
        ret |= mMethodsGet(mThis(self))->PwLoTdmLookupSet(mThis(self), pw);
        return ret;
        }

    return mMethodsGet(mThis(self))->PwHoTdmLookupSet(mThis(self), pw);
    }

static eAtRet PwCircuitIdReset(ThaModulePda self, AtPw pw)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return mMethodsGet(mThis(self))->PwLoTdmLookupReset(mThis(self), pw);

    return mMethodsGet(mThis(self))->PwHoTdmLookupReset(mThis(self), pw);
    }

static eAtRet VcDeAssemblerSet(ThaModulePda self, AtSdhChannel vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return cAtOk;
    }

static eAtRet PwCepEquip(ThaModulePda self, AtPw pw, AtChannel subChannel, eBool equip)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(subChannel);
    AtUnused(equip);
    return cAtOk;
    }

static eAtRet PwCwSequenceModeSet(ThaModulePda self, AtPw pw, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(sequenceMode);
    return cAtOk;
    }

static eAtRet PwCircuitSliceSet(ThaModulePda self, AtPw pw, uint8 slice)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(slice);
    return cAtOk;
    }

static void PwBufferRegsShow(Tha60210011ModulePda self, AtPw pw)
    {
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);
    ThaModulePdaLongRegDisplay(pw, "Pseudowire PDA Jitter Buffer Control",
                               mMethodsGet(mModuleV2(self))->PWPdaJitBufCtrl(mModuleV2(self)) + mPwOffset(self, pwAdapter));
    ThaModulePdaLongRegDisplay(pw, "Pseudowire PDA Jitter Buffer Status",
                               mMethodsGet(mModuleV2(self))->PDATdmJitBufStat(mModuleV2(self)) + mPwOffset(self, pwAdapter));
    ThaModulePdaRegDisplay(pw, "Pseudowire PDA Reorder Control",
                           cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + mPwOffset(self, pwAdapter));
    }

static void PwCircuitRegsShow(Tha60210011ModulePda self, AtPw pw)
    {
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    ThaModulePda modulePda = (ThaModulePda)self;

    if (circuit == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound circuit\r\n");
        return;
        }

    Tha60210011ModulePwCircuitInfoDisplay(pwAdapter);
    if (ShouldUseLoRegisters(modulePda, pw))
        {
        uint32 offset;
        ThaModulePdaRegDisplay(pw, "Pseudowire PDA Low Order TDM mode Control",
                               mMethodsGet(modulePda)->PWPdaTdmModeCtrl(modulePda, pwAdapter) + mTdmOffset(self, pwAdapter));
        ThaModulePdaRegDisplay(pw, "Pseudowire PDA Low Order TDM Look Up Control", LoTdmLookupControl(mThis(self), pw));

        offset = mMethodsGet(self)->PwTdmLowRateOffset(self, pw);
        ThaModulePdaRegDisplay(pw, "Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control",
                               mMethodsGet(self)->LoTdmSmallDS0Control(self) + mBaseAddress(self) + offset);
        return;
        }

    ThaModulePdaRegDisplay(pw, "Pseudowire PDA High Order TDM mode Control",
                           mMethodsGet(modulePda)->PWPdaTdmModeCtrl(modulePda, pwAdapter) + mTdmOffset(self, pwAdapter));
    ThaModulePdaRegDisplay(pw, "Pseudowire PDA High Order TDM Look Up Control",
                           HoTdmLookupControl(mThis(self), pw));
    }

static void PwRegsShow(ThaModulePda self, AtPw pw)
    {
    mMethodsGet(mThis(self))->PwBufferRegsShow(mThis(self), pw);
    mMethodsGet(mThis(self))->PwCircuitRegsShow(mThis(self), pw);
    }

static eAtRet PwEparEnable(ThaModulePda self, AtPw pwAdapter, eBool enable)
    {
    uint32 address = mMethodsGet(mModuleV2(self))->PWPdaJitBufCtrl(mModuleV2(self)) + mPwOffset(self, pwAdapter);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cAf6_ramjitbufcfg_PwEparEn_, mBoolToBin(enable));
    mChannelHwLongWrite(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static eBool PwEparIsEnabled(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 address = mMethodsGet(mModuleV2(self))->PWPdaJitBufCtrl(mModuleV2(self)) + mPwOffset(self, pwAdapter);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    return mBinToBool(mRegField(longRegVal[1], cAf6_ramjitbufcfg_PwEparEn_));
    }

static void PwJitterPayloadLengthSet(ThaModulePda self, AtPw pw, uint32 payloadSizeInBytes)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    regAddr = mMethodsGet(mModuleV2(self))->PWPdaJitBufCtrl(mModuleV2(self)) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mFieldIns(&longRegVal[1],
              cAf6_ramjitbufcfg_PwPayloadLen_Mask_02,
              cAf6_ramjitbufcfg_PwPayloadLen_Shift_02,
              payloadSizeInBytes >> 6);

    mFieldIns(&longRegVal[0],
              cAf6_ramjitbufcfg_PwPayloadLen_Mask_01,
              cAf6_ramjitbufcfg_PwPayloadLen_Shift_01,
              payloadSizeInBytes & cBit5_0);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    }

static ThaModuleHardSur SurModule(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleHardSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static eBool SupportFullPwCounters(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210011DeviceStartVersionSupportFullPwCounters(device))
        return cAtTrue;

    return cAtFalse;
    }

static Tha60210011PdaPwCounterGetter PwCounterGetterCreate(ThaModulePda self)
    {
    if (SupportFullPwCounters(self))
        return Tha60210011PdaPwCounterGetterV2New();

    return Tha60210011PdaPwCounterGetterNew();
    }

static Tha60210011PdaPwCounterGetter CounterGetter(Tha60210011ModulePda self)
    {
    if (self->counterGetter == NULL)
    	self->counterGetter = PwCounterGetterCreate((ThaModulePda)self);
    return self->counterGetter;
    }

static uint32 PwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxBytesGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxReorderedPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxLostPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxOutOfSeqDropedPacketsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxJitBufOverrunGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxJitBufUnderrunGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return PdaPwCounterGetterRxLopsGet(mCounterGetter(self), self, pw, clear);
    }

static uint32 PwRxPacketsSentToTdmGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return ThaModuleHardSurPwRxPacketsSentToTdmGet(SurModule(self), pw, clear);
    }

static eBool ShouldStartJitterCenter(ThaModulePda self, AtPw adapter)
    {
	AtUnused(self);	
    return ThaPwAdapterHwIsEnabled((ThaPwAdapter)adapter);
    }

static eAtRet PwCenterJitterStart(ThaModulePda self, AtPw adapter)
    {
    ThaModulePdaV2 pdaModule = (ThaModulePdaV2)self;
    uint32 regAddr, longRegVal[cThaLongRegMaxSize];

    if (!ShouldStartJitterCenter(self, adapter))
        return cAtOk;

    mThis(self)->jitterCenterStarted = cAtTrue;
    if (mMethodsGet(self)->JitterBufferCenterIsSupported(self))
        return m_ThaModulePdaMethods->PwCenterJitterStart(self, adapter);

    /* IMPORTANT: do not remove the following line codes, its purpose is to
     * start hardware jitter buffer centering. Hardware already reviewed this */
    regAddr = mMethodsGet(pdaModule)->PWPdaJitBufCtrl(pdaModule) + mPwOffset(self, adapter);
    mChannelHwLongRead(adapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mChannelHwLongWrite(adapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwCenterJitterStop(ThaModulePda self, AtPw pw)
    {
    eAtRet ret = cAtOk;
    if (mMethodsGet(self)->JitterBufferCenterIsSupported(self) && (mThis(self)->jitterCenterStarted))
        ret = m_ThaModulePdaMethods->PwCenterJitterStop(self, pw);

    mThis(self)->jitterCenterStarted = cAtFalse;
    return ret;
    }

static eAtRet ReorderPdvSet(Tha60150011ModulePda self, AtPw pw)
    {
    uint32 regAddr = cAf6Reg_ramreorcfg_Base + mBaseAddress(self) + PwDefaultOffset((ThaModulePda)self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    uint32 reorderTimeout = ReoderTimeoutCalculate((ThaModulePda)self, pw);
    if (reorderTimeout < 2)
        reorderTimeout = 2;

    mRegFieldSet(regVal, cAf6_ramreorcfg_PwReorTimeout_, reorderTimeout);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210011IsPdaRegister(AtModuleDeviceGet(self), address);
    }

static eBool NeedProtectWhenSetLopsThreshold(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if ((localAddress >= 0x0500000) && (localAddress <= 0x05FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 NumOc48s(AtModule self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet(self);
    return Tha60210011DeviceNumUsedOc48Slices(device);
    }

static uint32 TdmLookupResetValue(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cPdaTdmLookupHwResetValue;
    }

static eAtRet HoLookupReset(AtModule self)
    {
    static const uint8 cNumStsInOc48 = 48;
    uint8 numOc48 = NumOc48s(self);
    uint8 oc48Id, stsId;
    uint32 regAddr = mMethodsGet(mThis(self))->HoTdmLookupCtrl(mThis(self)) + mBaseAddress(self);
    uint32 hwReset = mMethodsGet(mThis(self))->TdmLookupResetValue(mThis(self));

    for (oc48Id = 0; oc48Id < numOc48; oc48Id++)
        {
        for (stsId = 0; stsId < cNumStsInOc48; stsId++)
            {
            uint32 offset = (oc48Id * 0x40UL) + stsId;
            mModuleHwWrite(self, regAddr + offset, hwReset);
            }
        }

    return cAtOk;
    }

static eAtRet LoLookupReset(AtModule self)
    {
    static const uint8 cNumSlicesPerOc48 = 2;
    static const uint32 cNumPwsPerSlice = 1024;
    uint8 numOc48 = NumOc48s(self);
    uint8 oc48Id, sliceId;
    uint32 pwId;
    uint32 regAddr = cAf6Reg_ramlotdmlkupcfg_Base + mBaseAddress(self);
    uint32 hwReset = mMethodsGet(mThis(self))->TdmLookupResetValue(mThis(self));

    for (oc48Id = 0; oc48Id < numOc48; oc48Id++)
        {
        for (sliceId = 0; sliceId < cNumSlicesPerOc48; sliceId++)
            {
            for (pwId = 0; pwId < cNumPwsPerSlice; pwId++)
                {
                uint32 offset = (oc48Id * 0x800UL) + (sliceId * 0x400UL) + pwId;
                mModuleHwWrite(self, regAddr + offset, hwReset);
                }
            }
        }

    return cAtOk;
    }

static eAtRet LookupReset(Tha60210011ModulePda self_)
    {
    eAtRet ret = cAtOk;
    AtModule self = (AtModule)self_;

    ret |= HoLookupReset(self);
    ret |= LoLookupReset(self);

    return ret;
    }

static ThaVersionReader VersionReader(AtModule self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet(self));
    }

static uint32 JitterBufferCenterDefaultValue(AtModule self)
    {
    uint32 versionNumber = AtDeviceVersionNumber(AtModuleDeviceGet(self));
    if (versionNumber == ThaVersionReaderPwVersionBuild(VersionReader(self), 0x16, 0x01, 0x07, 0x41))
        return 0x702;

    return 0x302;
    }

static void JitterBufferCenterDefaultValueSet(AtModule self)
    {
    uint32 resetValue = JitterBufferCenterDefaultValue(self);
    mModuleHwWrite(self, cRegJitterBufferCenter, resetValue);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;


    /* Super must be done first */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Set default HW value for Jitter Buffer centering */
    JitterBufferCenterDefaultValueSet(self);
    return mMethodsGet(mThis(self))->LookupReset(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eBool PwJitterBufferIsEmpty(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    AtOsalUSleep(20000);
    return cAtTrue;
    }

static uint32 CorrectNumAdditionalBytes(ThaModulePda self, AtPw pw, uint32 additionalBytes)
    {
    AtUnused(self);

    /* OCN LO: unit 4byte
       OCN HO: unit 32byte */
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return additionalBytes * 4;

    return additionalBytes * 32;
    }

static uint32 NumCurrentPacketsInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;
    uint32 numPackets, additionalBytes;
    uint32 payloadSizeInBytes = ThaPwAdapterPayloadSizeGet((ThaPwAdapter)pwAdapter);

    if (payloadSizeInBytes == 0)
        return 0;

    regAddr = PDATdmJitBufStat((ThaModulePdaV2)self) + mPwOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);

    /* Hw value is unit 2 packets */
    numPackets      = mRegField(regVal, cAf6_ramjitbufsta_JitBufNumPk_) * 2;
    additionalBytes = CorrectNumAdditionalBytes(self, pwAdapter, mRegField(regVal, cAf6_ramjitbufsta_JitBufExtraByte_));

    return numPackets + (additionalBytes / payloadSizeInBytes);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;
    uint32 additionalBytes;
    uint32 payloadSizeInBytes = ThaPwAdapterPayloadSizeGet((ThaPwAdapter)pwAdapter);

    if (payloadSizeInBytes == 0)
        return 0;

    regAddr = PDATdmJitBufStat((ThaModulePdaV2)self) + mPwOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    additionalBytes = CorrectNumAdditionalBytes(self, pwAdapter, mRegField(regVal, cAf6_ramjitbufsta_JitBufExtraByte_));
    return additionalBytes % payloadSizeInBytes;
    }

static void ShowDebugSticky1(AtModule self)
    {
    uint32 regAddress, regValue, longRegValue[cThaLongRegMaxSize], numOfDword;
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    AtDebugger debugger = NULL;

    regAddress = cPdaDebugSticky1;
    numOfDword = mModuleHwLongRead(self, regAddress, longRegValue, sizeof(longRegValue), core);
    regValue = longRegValue[0];

    Tha6021DebugPrintStart(debugger);
    ThaDeviceRegLongValueDisplayOnly("* PDA debug sticky 1", regAddress, longRegValue, numOfDword);
    Tha6021DebugPrintInfoBit(debugger, "STICKY_STATUS_PDA_wrblken_disabled", regValue, cPDA_wrblken_disabled);
    Tha6021DebugPrintInfoBit(debugger, "STICKY_STATUS_PDA_wrblkrdy_disabled", regValue, cPDA_wrblkrdy_disabled);
    Tha6021DebugPrintInfoBit(debugger, "CLA2PDA VALID ETH#2", regValue, cCLA2PDA_VALID_ETH_2);
    Tha6021DebugPrintInfoBit(debugger, "CLA2PDA VALID ETH#1", regValue, cCLA2PDA_VALID_ETH_1);
    Tha6021DebugPrintInfoBit(debugger, "CLA2PDA VALID ETH#2", regValue, cCLA2PDA_VALID_ETH_2);
    Tha6021DebugPrintErrorBit(debugger, "CLA2PDA ERR ETH#1", regValue, cCLA2PDA_ERR_ETH_1);
    Tha6021DebugPrintErrorBit(debugger, "CLA2PDA ERR ETH#2", regValue, cCLA2PDA_ERR_ETH_2);
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwLongWrite(self, regAddress, longRegValue, sizeof(longRegValue), core);
    }

static void ShowDebugSticky2(AtModule self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cPdaDebugSticky2;
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* PDA debug sticky 2", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, "LinkList ECC err", regValue, cLinkList_ECC_err);
    Tha6021DebugPrintErrorBit(debugger, "Read Free err", regValue, cRead_Free_err);
    Tha6021DebugPrintErrorBit(debugger, "HO Block err", regValue, cHO_Block_err);
    Tha6021DebugPrintErrorBit(debugger, "LinkList request err", regValue, cLinkList_request_err);
    Tha6021DebugPrintErrorBit(debugger, "JB valid err (bit17) ", regValue, cJB_valid_err_bit17   );
    Tha6021DebugPrintErrorBit(debugger, "TDM CRC err          ", regValue, cTDM_CRC_err          );
    Tha6021DebugPrintErrorBit(debugger, "Deque HO err         ", regValue, cDeque_HO_err         );
    Tha6021DebugPrintErrorBit(debugger, "LinkList Block same  ", regValue, cLinkList_Block_same  );
    Tha6021DebugPrintErrorBit(debugger, "LinkList Block empty ", regValue, cLinkList_Block_empty );
    Tha6021DebugPrintErrorBit(debugger, "JB_valid err (bit12) ", regValue, cJB_valid_err_bit12   );
    Tha6021DebugPrintErrorBit(debugger, "LinkList valid err   ", regValue, cLinkList_valid_err   );
    Tha6021DebugPrintErrorBit(debugger, "JB Par err           ", regValue, cJB_Par_err           );
    Tha6021DebugPrintErrorBit(debugger, "Reorder err          ", regValue, cReorder_err          );
    Tha6021DebugPrintErrorBit(debugger, "Reorder Page same    ", regValue, cReorder_Page_same    );
    Tha6021DebugPrintErrorBit(debugger, "Reorder Page Empty   ", regValue, cReorder_Page_Empty   );
    Tha6021DebugPrintErrorBit(debugger, "Reorder Seq err      ", regValue, cReorder_Seq_err      );
    Tha6021DebugPrintErrorBit(debugger, "Write cache full     ", regValue, cWrite_cache_full     );
    Tha6021DebugPrintErrorBit(debugger, "Write cache over size", regValue, cWrite_cache_over_size);
    Tha6021DebugPrintErrorBit(debugger, "Write cache Empty    ", regValue, cWrite_cache_Empty    );
    Tha6021DebugPrintErrorBit(debugger, "Write cache Block dis", regValue, cWrite_cache_Block_dis);
    Tha6021DebugPrintErrorBit(debugger, "Block Empty          ", regValue, cBlock_Empty          );
    Tha6021DebugPrintErrorBit(debugger, "Block Same           ", regValue, cBlock_Same           );
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);

    ShowDebugSticky1(self);
    ShowDebugSticky2(self);
    AtPrintc(cSevNormal, "\r\n");

    return cAtOk;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "PDA Reorder Control",
         "PDA Jitter Buffer Control",
         "PDA Lo and Ho TDM Look Up Control",
         "PDA Low Order TDM mode Control",
         "PDA data CRC error",
         "PDA link list ECC"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < 5)
        return Tha60210011InternalRamPdaNew(self, ramId, localRamId);

    return Tha60210011InternalRamPdaEccNew(self, ramId, localRamId);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->counterGetter));
    mThis(self)->counterGetter = NULL;
    m_AtObjectMethods->Delete(self);
    }

static eBool LopsThresholdHasLimitOneSecond(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210011ModulePda object = (Tha60210011ModulePda)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(jitterCenterStarted);
    mEncodeObject(counterGetter);
    }

static uint32 LoSliceHwPwIdGet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice)
    {
    /* This product do not support means to get HW id */
    AtUnused(adapter);
    AtUnused(self);
    AtUnused(loSlice);
    return cThaMapInvalidPwChannelId;
    }

static void LoSliceHwPwIdSet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice, uint32 hwId)
    {
    AtUnused(adapter);
    AtUnused(self);
    AtUnused(loSlice);
    AtUnused(hwId);
    }

/* This product configure LOPS in ms unit, so LOPS in packet unit need to be converted to ms to configure to HW
 * Because there is offset in calculation, LOPS in packet unit need to be cached to return in method GET.
 * This cache make reconfiguration checking always pass, so need to disable reconfiguration checking
 */
static eBool NeedPreventReconfigure(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PwCwAutoRxMBitEnable(ThaModulePda self, AtPw pwAdapter, eBool enable)
    {
    uint32 regAddr = cAf6Reg_ramlotdmmodecfg_Base + mBaseAddress(self) + mTdmOffset(self, pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramlotdmmodecfg_PDARdiOff_, enable ? 0 : 1);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool PwCwAutoRxMBitIsEnabled(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr = cAf6Reg_ramlotdmmodecfg_Base + mBaseAddress(self) + mTdmOffset(self, pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    return (regVal & cAf6_ramlotdmmodecfg_PDARdiOff_Mask) ? cAtFalse : cAtTrue;
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x5, 0x1004);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionSupport) ? cAtTrue : cAtFalse;
    }

static eAtRet PwDefaultHwConfigure(Tha60210011ModulePda self, AtPw pwAdapter)
    {
    uint8 slice = 0;
    uint32 oc48Id;
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 tdmLineId;
    eBool loPw;
    uint32 fieldMask, fieldShift;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pwAdapter, &slice, NULL) != cAtOk)
        mChannelLog(pwAdapter, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    address = mMethodsGet(mModuleV2(self))->PWPdaJitBufCtrl(mModuleV2(self)) + mPwOffset(self, pwAdapter);
    mChannelHwLongRead(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    tdmLineId = Tha60210011ModulePwTdmLineIdOfPw(pwAdapter, cThaModulePda);
    oc48Id = mMethodsGet(self)->Oc48IdFromSlice(self, pwAdapter, slice);
    loPw = Tha60210011PwCircuitBelongsToLoLine(pwAdapter);

    if (loPw)
        {
        uint32 oc24SliceId = slice % 2;
        fieldMask = mMethodsGet(self)->PwLoSlice24SelMask(self);
        fieldShift = mMethodsGet(self)->PwLoSlice24SelShift(self);
        mRegFieldSet(longRegVal[1], field, oc24SliceId);
        }

    mRegFieldSet(longRegVal[1], cAf6_ramjitbufcfg_PwHiLoPathInd_, loPw ? 0 : 1);

    fieldMask = mMethodsGet(self)->TdmLineIdMask(self);
    fieldShift = mMethodsGet(self)->TdmLineIdShift(self);
    mRegFieldSet(longRegVal[1], field, tdmLineId);

    fieldMask = mMethodsGet(self)->HoLoOc48IdMask(self);
    fieldShift = mMethodsGet(self)->HoLoOc48IdShift(self);
    mRegFieldSet(longRegVal[1], field, oc48Id);

    fieldMask = mMethodsGet(self)->PwCEPModeMask(self);
    fieldShift = mMethodsGet(self)->PwCEPModeShift(self);
    mRegFieldSet(longRegVal[1], field, (AtPwTypeGet(pwAdapter) == cAtPwTypeCEP) ? 1 : 0);
    mChannelHwLongWrite(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 LoTdmSmallDS0Control(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramlotdmsmallds0control_Base;
    }

static uint8 Oc48IdFromSlice(Tha60210011ModulePda self, AtPw pwAdapter, uint8 sliceId)
    {
    AtUnused(self);
    if (Tha60210011PwCircuitBelongsToLoLine(pwAdapter))
        return sliceId / 2;
    return sliceId;
    }

static uint32 HoLoOc48IdMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwHoLoOc48Id_Mask;
    }

static uint32 HoLoOc48IdShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwHoLoOc48Id_Shift;
    }

static uint32 TdmLineIdMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwTdmLineId_Mask;
    }

static uint32 TdmLineIdShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwTdmLineId_Shift;
    }

static uint32 PwLoSlice24SelMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLoSlice24Sel_Mask;
    }

static uint32 PwLoSlice24SelShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLoSlice24Sel_Shift;
    }

static uint32 PwCEPModeMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwCEPMode_Mask;
    }

static uint32 PwCEPModeShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwCEPMode_Shift;
    }

static uint32 PwLowDs0ModeMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLowDs0Mode_Mask;
    }

static uint32 PwLowDs0ModeShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLowDs0Mode_Shift;
    }

static uint32 LoTdmLookupCtrl(Tha60210011ModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6Reg_ramlotdmlkupcfg_Base;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsEnabled);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferCenterIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsSetThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwReorderingEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwReorderingIsEnable);
        mMethodOverride(m_ThaModulePdaOverride, HwLimitNumPktForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferStateString);
        mMethodOverride(m_ThaModulePdaOverride, PwTdmOffset);
        mMethodOverride(m_ThaModulePdaOverride, PWPdaTdmModeCtrl);
        mMethodOverride(m_ThaModulePdaOverride, PwPdaModeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitIdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitIdReset);
        mMethodOverride(m_ThaModulePdaOverride, VcDeAssemblerSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCepEquip);
        mMethodOverride(m_ThaModulePdaOverride, PwCwSequenceModeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitSliceSet);
        mMethodOverride(m_ThaModulePdaOverride, PwRegsShow);
        mMethodOverride(m_ThaModulePdaOverride, PwDefaultOffset);
        mMethodOverride(m_ThaModulePdaOverride, PwEparEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwEparIsEnabled);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterPayloadLengthSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCenterJitterStop);
        mMethodOverride(m_ThaModulePdaOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLopsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxPacketsSentToTdmGet);
        mMethodOverride(m_ThaModulePdaOverride, NeedProtectWhenSetLopsThreshold);
        mMethodOverride(m_ThaModulePdaOverride, PwCenterJitterStart);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferIsEmpty);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, LopsThresholdHasLimitOneSecond);
        mMethodOverride(m_ThaModulePdaOverride, NeedPreventReconfigure);
        mMethodOverride(m_ThaModulePdaOverride, BaseAddress);

        mBitFieldOverride(ThaModulePda, m_ThaModulePdaOverride, PktReplaceMode)
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideThaModulePdaV2(AtModule self)
    {
    ThaModulePdaV2 pdaModuleV2 = (ThaModulePdaV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaV2Override, mMethodsGet(pdaModuleV2), sizeof(m_ThaModulePdaV2Override));

        mMethodOverride(m_ThaModulePdaV2Override, PWPdaJitBufCtrl);
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, JitterBufferSizePkt)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, JitterBufferDelayPkt)

        mMethodOverride(m_ThaModulePdaV2Override, PDATdmJitBufStat);
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, BufferNumPkt)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, BufferNumAdditionalBytes)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, PdaIdleCode);
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, PdaNumNxDs0)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, AisRdiUneqOff)
        }

    mMethodsSet(pdaModuleV2, &m_ThaModulePdaV2Override);
    }

static void OverrideTha60150011ModulePda(AtModule self)
    {
    Tha60150011ModulePda pdaModule = (Tha60150011ModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_Tha60150011ModulePdaOverride));

        mMethodOverride(m_Tha60150011ModulePdaOverride, ReorderPdvSet);
        }

    mMethodsSet(pdaModule, &m_Tha60150011ModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModulePdaV2(self);
    OverrideThaModulePda(self);
    OverrideTha60150011ModulePda(self);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210011ModulePda modulePda = (Tha60210011ModulePda)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwLoTdmLookupSet);
        mMethodOverride(m_methods, PwHoTdmLookupSet);
        mMethodOverride(m_methods, PwLoTdmLookupOffset);
        mMethodOverride(m_methods, PwHoTdmLookupOffset);
        mMethodOverride(m_methods, TdmLookupResetValue);
        mMethodOverride(m_methods, PwLoTdmLookupReset);
        mMethodOverride(m_methods, PwHoTdmLookupReset);
        mMethodOverride(m_methods, HoTdmLookupCtrl);
        mMethodOverride(m_methods, PwDefaultHwConfigure);
        mMethodOverride(m_methods, PwBufferRegsShow);
        mMethodOverride(m_methods, PwCircuitRegsShow);
        mMethodOverride(m_methods, LoSliceHwPwIdGet);
        mMethodOverride(m_methods, LoSliceHwPwIdSet);
        mMethodOverride(m_methods, StartVersionSupportLowRateCesop);
        mMethodOverride(m_methods, CounterGetter);
        mMethodOverride(m_methods, Oc48IdFromSlice);
        mMethodOverride(m_methods, HoLoOc48IdMask);
        mMethodOverride(m_methods, HoLoOc48IdShift);
        mMethodOverride(m_methods, TdmLineIdMask);
        mMethodOverride(m_methods, TdmLineIdShift);
        mMethodOverride(m_methods, PwLoSlice24SelMask);
        mMethodOverride(m_methods, PwLoSlice24SelShift);
        mMethodOverride(m_methods, PwCEPModeMask);
        mMethodOverride(m_methods, PwCEPModeShift);
        mMethodOverride(m_methods, PwLowDs0ModeMask);
        mMethodOverride(m_methods, PwLowDs0ModeShift);
        mMethodOverride(m_methods, LookupReset);
        mMethodOverride(m_methods, LoTdmSmallDS0Control);
        mMethodOverride(m_methods, PwTdmLowRateOffset);
        mMethodOverride(m_methods, LoTdmModeOffset);
        mMethodOverride(m_methods, LoTdmLookupOffset);
        mMethodOverride(m_methods, LoTdmLookupCtrl);
        mMethodOverride(m_methods, PwLookupPwIDMask);
        mMethodOverride(m_methods, PwLookupPwIDShift);
        }

    mMethodsSet(modulePda, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011ModulePda);
    }

AtModule Tha60210011ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210011ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011ModulePdaObjectInit(newModule, device);
    }

eAtRet Tha60210011ModulePdaPwDefaultHwConfigure(Tha60210011ModulePda self, AtPw pwAdapter)
    {
    if (self)
        return mMethodsGet(self)->PwDefaultHwConfigure(self, pwAdapter);
    return cAtErrorModeNotSupport;
    }

void Tha60210011ModulePdaHoLoIndicationReset(Tha60210011ModulePda self, ThaPwAdapter pwAdapter)
    {
    uint8 slice = 0;
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];

    if ((self == NULL) || (!ThaPwTypeIsCesCep((AtPw)pwAdapter)))
        return;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet((AtPw)pwAdapter, &slice, NULL) != cAtOk)
        mChannelLog(pwAdapter, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    address = mMethodsGet(mModuleV2(self))->PWPdaJitBufCtrl(mModuleV2(self)) + mPwOffset(self, pwAdapter);
    mChannelHwLongRead(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cAf6_ramjitbufcfg_PwHiLoPathInd_, 0);
    mChannelHwLongWrite(pwAdapter, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    }

uint32 Tha60210011ModulePdaLoSliceHwPwIdGet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice)
    {
    if (self)
        return mMethodsGet(self)->LoSliceHwPwIdGet(self, adapter, loSlice);

    return cThaMapInvalidPwChannelId;
    }

void Tha60210011ModulePdaLoSliceHwPwIdSet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice, uint32 hwId)
    {
    if (self)
        mMethodsGet(self)->LoSliceHwPwIdSet(self, adapter, loSlice, hwId);
    }

eAtRet Tha60210011ModulePdaPWCESoPLowRateEnable(ThaModulePda self, AtPw pwAdapter)
    {
    if (self)
        return PWCESoPLowRateEnable(self, pwAdapter);
    return cAtErrorNullPointer;
    }


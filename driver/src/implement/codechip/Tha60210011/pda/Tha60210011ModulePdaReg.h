/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : PDA
 *                                                                              
 * File        : Tha60210011ModulePdaReg.h
 *                                                                              
 * Created Date: PDA registers
 *                                                                              
 * Description : This file contain all constant definitions of block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_PDA_H_
#define _AF6_REG_AF6CCI0011_RD_PDA_H_

/*--------------------------- Define -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Control
Reg Addr   : 0x00010000 - 0x00011FFF #The address format for these registers is 0x00010000 + PWID
Reg Formula: 0x00010000 +  PWID
    Where  : 
           + $PWID(0-8191): Pseudowire ID
Reg Desc   : 
This register configures jitter buffer parameters per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufcfg_Base                                                                   0x00010000
#define cAf6Reg_ramjitbufcfg(PWID)                                                         (0x00010000+(PWID))
#define cAf6Reg_ramjitbufcfg_WidthVal                                                                       64
#define cAf6Reg_ramjitbufcfg_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: PwLowDs0Mode
BitField Type: RW
BitField Desc: Pseudo-wire CES Low DS0 mode
               1: CESoP Pseodo-wire with NxDS0 <= 3
               0: Other Pseodo-wire mode
BitField Bits: [56]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwLowDs0Mode_Bit_Start                                                            56
#define cAf6_ramjitbufcfg_PwLowDs0Mode_Bit_End                                                              56
#define cAf6_ramjitbufcfg_PwLowDs0Mode_Mask                                                             cBit24
#define cAf6_ramjitbufcfg_PwLowDs0Mode_Shift                                                                24
#define cAf6_ramjitbufcfg_PwLowDs0Mode_MaxVal                                                              0x0
#define cAf6_ramjitbufcfg_PwLowDs0Mode_MinVal                                                              0x0
#define cAf6_ramjitbufcfg_PwLowDs0Mode_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PwCEPMode
BitField Type: RW
BitField Desc: Pseudo-wire CEP mode indication
BitField Bits: [55]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwCEPMode_Bit_Start                                                               55
#define cAf6_ramjitbufcfg_PwCEPMode_Bit_End                                                                 55
#define cAf6_ramjitbufcfg_PwCEPMode_Mask                                                                cBit23
#define cAf6_ramjitbufcfg_PwCEPMode_Shift                                                                   23
#define cAf6_ramjitbufcfg_PwCEPMode_MaxVal                                                                 0x0
#define cAf6_ramjitbufcfg_PwCEPMode_MinVal                                                                 0x0
#define cAf6_ramjitbufcfg_PwCEPMode_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: PwHoLoOc48Id
BitField Type: RW
BitField Desc: Indicate 4x Hi order OC48 or 4x Low order OC48 slice
BitField Bits: [54:53]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Bit_Start                                                            53
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Bit_End                                                              54
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Mask                                                          cBit22_21
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Shift                                                                21
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_MaxVal                                                              0x0
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_MinVal                                                              0x0
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PwLoSlice24Sel
BitField Type: RW
BitField Desc: Pseudo-wire Low order OC24 slice selection, only valid in Lo
Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within
a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a
Lo OC48
BitField Bits: [52]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Bit_Start                                                          52
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Bit_End                                                            52
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Mask                                                           cBit20
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Shift                                                              20
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_MaxVal                                                            0x0
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_MinVal                                                            0x0
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to
Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and
MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID
BitField Bits: [51:42]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwTdmLineId_Bit_Start                                                             42
#define cAf6_ramjitbufcfg_PwTdmLineId_Bit_End                                                               51
#define cAf6_ramjitbufcfg_PwTdmLineId_Mask                                                           cBit19_10
#define cAf6_ramjitbufcfg_PwTdmLineId_Shift                                                                 10
#define cAf6_ramjitbufcfg_PwTdmLineId_MaxVal                                                               0x0
#define cAf6_ramjitbufcfg_PwTdmLineId_MinVal                                                               0x0
#define cAf6_ramjitbufcfg_PwTdmLineId_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PwEparEn
BitField Type: RW
BitField Desc: Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0:
Disable EPAR timing mode
BitField Bits: [41]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwEparEn_Bit_Start                                                                41
#define cAf6_ramjitbufcfg_PwEparEn_Bit_End                                                                  41
#define cAf6_ramjitbufcfg_PwEparEn_Mask                                                                  cBit9
#define cAf6_ramjitbufcfg_PwEparEn_Shift                                                                     9
#define cAf6_ramjitbufcfg_PwEparEn_MaxVal                                                                  0x0
#define cAf6_ramjitbufcfg_PwEparEn_MinVal                                                                  0x0
#define cAf6_ramjitbufcfg_PwEparEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: PwHiLoPathInd
BitField Type: RW
BitField Desc: Pseudo-wire belong to Hi-order or Lo-order path 1: Pseudo-wire
belong to Hi-order path 0: Pseudo-wire belong to Lo-order path
BitField Bits: [40]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Bit_Start                                                           40
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Bit_End                                                             40
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Mask                                                             cBit8
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Shift                                                                8
#define cAf6_ramjitbufcfg_PwHiLoPathInd_MaxVal                                                             0x0
#define cAf6_ramjitbufcfg_PwHiLoPathInd_MinVal                                                             0x0
#define cAf6_ramjitbufcfg_PwHiLoPathInd_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwPayloadLen
BitField Type: RW
BitField Desc: TDM Payload of pseudo-wire in byte unit
BitField Bits: [39:26]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwPayloadLen_Bit_Start                                                            26
#define cAf6_ramjitbufcfg_PwPayloadLen_Bit_End                                                              39
#define cAf6_ramjitbufcfg_PwPayloadLen_Mask_01                                                       cBit31_26
#define cAf6_ramjitbufcfg_PwPayloadLen_Shift_01                                                             26
#define cAf6_ramjitbufcfg_PwPayloadLen_Mask_02                                                         cBit7_0
#define cAf6_ramjitbufcfg_PwPayloadLen_Shift_02                                                              0

/*--------------------------------------
BitField Name: PdvSizeInPkUnit
BitField Type: RW
BitField Desc: Pdv size in packet unit. This parameter is to prevent packet
delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in
packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps *
PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps *
PdvInUsus)%(8000*PwPayloadLen) !=0)
BitField Bits: [25:13]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Bit_Start                                                         13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Bit_End                                                           25
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Mask                                                       cBit25_13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Shift                                                             13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_MaxVal                                                        0x1fff
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_MinVal                                                           0x0
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_RstVal                                                           0x0

/*--------------------------------------
BitField Name: JitBufSizeInPkUnit
BitField Type: RW
BitField Desc: Jitter buffer size in packet unit. This parameter is mostly
double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit =
(PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps *
JitBufInUsus)%(8000*PwPayloadLen) !=0)
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Bit_Start                                                       0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Bit_End                                                        12
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Mask                                                     cBit12_0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Shift                                                           0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_MaxVal                                                     0x1fff
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_MinVal                                                        0x0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Status
Reg Addr   : 0x00018000 - 0x00019FFF #The address format for these registers is 0x00018000 + PWID
Reg Formula: 0x00018000 +  PWID
    Where  : 
           + $PWID(0-8191): Pseudowire ID
Reg Desc   : 
This register shows jitter buffer status per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufsta_Base                                                                   0x00014000
#define cAf6Reg_ramjitbufsta(PWID)                                                         (0x00014000+(PWID))
#define cAf6Reg_ramjitbufsta_WidthVal                                                                      128
#define cAf6Reg_ramjitbufsta_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: JitBufHwComSta
BitField Type: RW
BitField Desc: Harware debug only status
BitField Bits: [127:25]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufHwComSta_Bit_Start                                                          25
#define cAf6_ramjitbufsta_JitBufHwComSta_Bit_End                                                           127
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_01                                                     cBit31_25
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_01                                                           25
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_02                                                      cBit31_0
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_02                                                            0
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_03                                                      cBit31_0
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_03                                                            0
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_04                                                      cBit31_0
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_04                                                            0

/*--------------------------------------
BitField Name: JitBufExtraByte
BitField Type: RW
BitField Desc: Extra byte in jitter buffer beside packets
BitField Bits: [24:17]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufExtraByte_Bit_Start                                                         17
#define cAf6_ramjitbufsta_JitBufExtraByte_Bit_End                                                           24
#define cAf6_ramjitbufsta_JitBufExtraByte_Mask                                                       cBit23_16
#define cAf6_ramjitbufsta_JitBufExtraByte_Shift                                                             16
#define cAf6_ramjitbufsta_JitBufExtraByte_MaxVal                                                          0xff
#define cAf6_ramjitbufsta_JitBufExtraByte_MinVal                                                           0x0
#define cAf6_ramjitbufsta_JitBufExtraByte_RstVal                                                           0x0

/*--------------------------------------
BitField Name: JitBufNumPk
BitField Type: RW
BitField Desc: Current number of packet in jitter buffer
BitField Bits: [16:4]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufNumPk_Bit_Start                                                              4
#define cAf6_ramjitbufsta_JitBufNumPk_Bit_End                                                               16
#define cAf6_ramjitbufsta_JitBufNumPk_Mask                                                            cBit15_4
#define cAf6_ramjitbufsta_JitBufNumPk_Shift                                                                  4
#define cAf6_ramjitbufsta_JitBufNumPk_MaxVal                                                            0x1fff
#define cAf6_ramjitbufsta_JitBufNumPk_MinVal                                                               0x0
#define cAf6_ramjitbufsta_JitBufNumPk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: JitBufFull
BitField Type: RW
BitField Desc: Jitter Buffer full status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufFull_Bit_Start                                                               3
#define cAf6_ramjitbufsta_JitBufFull_Bit_End                                                                 3
#define cAf6_ramjitbufsta_JitBufFull_Mask                                                                cBit3
#define cAf6_ramjitbufsta_JitBufFull_Shift                                                                   3
#define cAf6_ramjitbufsta_JitBufFull_MaxVal                                                                0x1
#define cAf6_ramjitbufsta_JitBufFull_MinVal                                                                0x0
#define cAf6_ramjitbufsta_JitBufFull_RstVal                                                                0x0

/*--------------------------------------
BitField Name: JitBufState
BitField Type: RW
BitField Desc: Jitter buffer state machine status 0: START 1: FILL 2: READY 3:
READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufState_Bit_Start                                                              0
#define cAf6_ramjitbufsta_JitBufState_Bit_End                                                                2
#define cAf6_ramjitbufsta_JitBufState_Mask                                                             cBit2_0
#define cAf6_ramjitbufsta_JitBufState_Shift                                                                  0
#define cAf6_ramjitbufsta_JitBufState_MaxVal                                                               0x7
#define cAf6_ramjitbufsta_JitBufState_MinVal                                                               0x0
#define cAf6_ramjitbufsta_JitBufState_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Reorder Control
Reg Addr   : 0x00020000 - 0x00021FFF #The address format for these registers is 0x00020000 + PWID
Reg Formula: 0x00020000 +  PWID
    Where  : 
           + $PWID(0-8191): Pseudowire ID
Reg Desc   : 
This register configures reorder parameters per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramreorcfg_Base                                                                     0x00020000
#define cAf6Reg_ramreorcfg(PWID)                                                           (0x00020000+(PWID))
#define cAf6Reg_ramreorcfg_WidthVal                                                                         32
#define cAf6Reg_ramreorcfg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: PwSetLofsInPk
BitField Type: RW
BitField Desc: Number of consecutive lost packet to declare lost of packet state
BitField Bits: [25:17]
--------------------------------------*/
#define cAf6_ramreorcfg_PwSetLofsInPk_Bit_Start                                                             17
#define cAf6_ramreorcfg_PwSetLofsInPk_Bit_End                                                               25
#define cAf6_ramreorcfg_PwSetLofsInPk_Mask                                                           cBit25_17
#define cAf6_ramreorcfg_PwSetLofsInPk_Shift                                                                 17
#define cAf6_ramreorcfg_PwSetLofsInPk_MaxVal                                                             0x1ff
#define cAf6_ramreorcfg_PwSetLofsInPk_MinVal                                                               0x0
#define cAf6_ramreorcfg_PwSetLofsInPk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PwSetLopsInMsec
BitField Type: RW
BitField Desc: Number of empty time to declare lost of packet synchronization
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_ramreorcfg_PwSetLopsInMsec_Bit_Start                                                            9
#define cAf6_ramreorcfg_PwSetLopsInMsec_Bit_End                                                             16
#define cAf6_ramreorcfg_PwSetLopsInMsec_Mask                                                          cBit16_9
#define cAf6_ramreorcfg_PwSetLopsInMsec_Shift                                                                9
#define cAf6_ramreorcfg_PwSetLopsInMsec_MaxVal                                                            0xff
#define cAf6_ramreorcfg_PwSetLopsInMsec_MinVal                                                             0x0
#define cAf6_ramreorcfg_PwSetLopsInMsec_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwReorEn
BitField Type: RW
BitField Desc: Set 1 to enable reorder
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ramreorcfg_PwReorEn_Bit_Start                                                                   8
#define cAf6_ramreorcfg_PwReorEn_Bit_End                                                                     8
#define cAf6_ramreorcfg_PwReorEn_Mask                                                                    cBit8
#define cAf6_ramreorcfg_PwReorEn_Shift                                                                       8
#define cAf6_ramreorcfg_PwReorEn_MaxVal                                                                    0x1
#define cAf6_ramreorcfg_PwReorEn_MinVal                                                                    0x0
#define cAf6_ramreorcfg_PwReorEn_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PwReorTimeout
BitField Type: RW
BitField Desc: Reorder timeout in 512us unit to detect lost packets. The formula
is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen *
16)/PwSpeedInKbps
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ramreorcfg_PwReorTimeout_Bit_Start                                                              0
#define cAf6_ramreorcfg_PwReorTimeout_Bit_End                                                                7
#define cAf6_ramreorcfg_PwReorTimeout_Mask                                                             cBit7_0
#define cAf6_ramreorcfg_PwReorTimeout_Shift                                                                  0
#define cAf6_ramreorcfg_PwReorTimeout_MaxVal                                                              0xff
#define cAf6_ramreorcfg_PwReorTimeout_MinVal                                                               0x0
#define cAf6_ramreorcfg_PwReorTimeout_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Low Order TDM mode Control
Reg Addr   : 0x00030000 - 0x0003FFFF #The address format for low order path is 0x00030000 +  LoOc48ID*0x1000 + LoOrderoc24Slice*0x400 + TdmOc24Pwid
Reg Formula: 0x00030000 + $LoOc48ID*0x1000 + $LoOrderoc24Slice*0x400 + $TdmOc24Pwid
    Where  : 
           + $LoOc48ID(0-5): LoOC48 ID
           + $LoOrderoc24Slice(0-1): Lo-order slice oc24
           + $TdmOc24Pwid(0-1023): TDM OC24 slice PWID
Reg Desc   : 
This register configure TDM mode for interworking between Pseudowire and Lo TDM

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmmodecfg_Base                                                                0x00030000
#define cAf6Reg_ramlotdmmodecfg(LoOc48ID, LoOrderoc24Slice, TdmOc24Pwid)(0x00030000+(LoOc48ID)*0x1000+(LoOrderoc24Slice)*0x400+(TdmOc24Pwid))
#define cAf6Reg_ramlotdmmodecfg_WidthVal                                                                    32
#define cAf6Reg_ramlotdmmodecfg_WriteMask                                                                  0x0


#define cAf6_ramlotdmmodecfg_PDARdiOff_Mask                                                           cBit28
#define cAf6_ramlotdmmodecfg_PDARdiOff_Shift                                                              28

/*--------------------------------------
BitField Name: PDANxDS0
BitField Type: RW
BitField Desc: Number of DS0 allocated for CESoP PW
BitField Bits: [27:22]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDANxDS0_Bit_Start                                                             22
#define cAf6_ramlotdmmodecfg_PDANxDS0_Bit_End                                                               27
#define cAf6_ramlotdmmodecfg_PDANxDS0_Mask                                                           cBit27_22
#define cAf6_ramlotdmmodecfg_PDANxDS0_Shift                                                                 22
#define cAf6_ramlotdmmodecfg_PDANxDS0_MaxVal                                                              0x3f
#define cAf6_ramlotdmmodecfg_PDANxDS0_MinVal                                                               0x0
#define cAf6_ramlotdmmodecfg_PDANxDS0_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PDAIdleCode
BitField Type: RW
BitField Desc: Idle pattern to replace data in case of Lost/Lbit packet
BitField Bits: [21:14]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Bit_Start                                                          14
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Bit_End                                                            21
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Mask                                                        cBit21_14
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Shift                                                              14
#define cAf6_ramlotdmmodecfg_PDAIdleCode_MaxVal                                                           0xff
#define cAf6_ramlotdmmodecfg_PDAIdleCode_MinVal                                                            0x0
#define cAf6_ramlotdmmodecfg_PDAIdleCode_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PDAAisUneqRdiOff
BitField Type: RW
BitField Desc: Set 1 to disable sending AIS/Unequip/RDI from PW to TDM
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_Bit_Start                                                     13
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_Bit_End                                                       13
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_Mask                                                      cBit13
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_Shift                                                         13
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_MaxVal                                                       0x1
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_MinVal                                                       0x0
#define cAf6_ramlotdmmodecfg_PDAAisUneqRdiOff_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PDARepMode
BitField Type: RW
BitField Desc: Mode to replace lost and Lbit packet 0: Replace by replaying
previous good packet (often for voice or video application) 1: Replace by AIS
(default) 2: Replace by a configuration idle code Replace by a configuration
idle code
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDARepMode_Bit_Start                                                           11
#define cAf6_ramlotdmmodecfg_PDARepMode_Bit_End                                                             12
#define cAf6_ramlotdmmodecfg_PDARepMode_Mask                                                         cBit12_11
#define cAf6_ramlotdmmodecfg_PDARepMode_Shift                                                               11
#define cAf6_ramlotdmmodecfg_PDARepMode_MaxVal                                                             0x3
#define cAf6_ramlotdmmodecfg_PDARepMode_MinVal                                                             0x0
#define cAf6_ramlotdmmodecfg_PDARepMode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PDAStsId
BitField Type: RW
BitField Desc: Only used in DS3/E3 SAToP, corresponding Slice OC24 STS ID of PW
circuit
BitField Bits: [9:5]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAStsId_Bit_Start                                                              5
#define cAf6_ramlotdmmodecfg_PDAStsId_Bit_End                                                                9
#define cAf6_ramlotdmmodecfg_PDAStsId_Mask                                                             cBit9_5
#define cAf6_ramlotdmmodecfg_PDAStsId_Shift                                                                  5
#define cAf6_ramlotdmmodecfg_PDAStsId_MaxVal                                                              0x1f
#define cAf6_ramlotdmmodecfg_PDAStsId_MinVal                                                               0x0
#define cAf6_ramlotdmmodecfg_PDAStsId_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PDAMode
BitField Type: RW
BitField Desc: TDM Payload De-Assembler modes 0: DS1/E1 SAToP 2: CESoP without
CAS 12: CEP basic 15: DS3/E3 SAToP
BitField Bits: [4:1]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAMode_Bit_Start                                                               1
#define cAf6_ramlotdmmodecfg_PDAMode_Bit_End                                                                 4
#define cAf6_ramlotdmmodecfg_PDAMode_Mask                                                              cBit4_1
#define cAf6_ramlotdmmodecfg_PDAMode_Shift                                                                   1
#define cAf6_ramlotdmmodecfg_PDAMode_MaxVal                                                                0xf
#define cAf6_ramlotdmmodecfg_PDAMode_MinVal                                                                0x0
#define cAf6_ramlotdmmodecfg_PDAMode_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PDAFracEbmEn
BitField Type: RW
BitField Desc: EBM enable in CEP fractional mode 0: EBM disable 1: EBM enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Bit_Start                                                          0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Bit_End                                                            0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Mask                                                           cBit0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Shift                                                              0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_MaxVal                                                           0x1
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_MinVal                                                           0x0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Low Order TDM Look Up Control
Reg Addr   : 0x00040000 - 0x00043FFF #The address format for low order path is 0x00040000 +  LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24Pwid
Reg Formula: 0x00040000 + LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24PwID
    Where  : 
           + $LoOc48ID(0-5): LoOC48 ID
           + $LoOc24Slice(0-1): Lo-order slice oc24
           + TdmOc24PwID(0-1023): TDM OC24 slice PWID
Reg Desc   : 
This register configure lookup from TDM PWID to global 8192 PWID

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmlkupcfg_Base                                                                0x00040000
#define cAf6Reg_ramlotdmlkupcfg(LoOc48ID, LoOc24Slice, TdmOc24PwID)   (0x00040000+(LoOc48ID)*0x800+(LoOc24Slice)*0x400+(TdmOc24PwID))
#define cAf6Reg_ramlotdmlkupcfg_WidthVal                                                                    32
#define cAf6Reg_ramlotdmlkupcfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PwID
BitField Type: RW
BitField Desc: Flat 8192 PWID
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwID_Bit_Start                                                                  0
#define cAf6_ramlotdmlkupcfg_PwID_Bit_End                                                                   12
#define cAf6_ramlotdmlkupcfg_PwID_Mask                                                                cBit12_0
#define cAf6_ramlotdmlkupcfg_PwID_Shift                                                                      0
#define cAf6_ramlotdmlkupcfg_PwID_MaxVal                                                                0x1fff
#define cAf6_ramlotdmlkupcfg_PwID_MinVal                                                                   0x0
#define cAf6_ramlotdmlkupcfg_PwID_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA High Order TDM Look Up Control
Reg Addr   : 0x00044000 - 0x000441FF #The address format for high order path is 0x00044000 +  HoOc48ID*0x40 + HoMasSTS
Reg Formula: 0x00044000 + HoOc48ID*0x40 + HoMasSTS
    Where  :
           + $HoOc48ID(0-7): HoOC48 ID
           + $HoMasSTS(0-47): HO TDM per OC48 master STS ID
Reg Desc   :
This register configure lookup from HO TDM master STSIS to global 8192 PWID

------------------------------------------------------------------------------*/
#define cAf6Reg_ramhotdmlkupcfg_Base                                                                0x00044000
#define cAf6Reg_ramhotdmlkupcfg(HoOc48ID, HoMasSTS)                    (0x00044000+(HoOc48ID)*0x40+(HoMasSTS))
#define cAf6Reg_ramhotdmlkupcfg_WidthVal                                                                    32
#define cAf6Reg_ramhotdmlkupcfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PwID
BitField Type: RW
BitField Desc: Flat 8192 PWID
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_ramhotdmlkupcfg_PwID_Bit_Start                                                                  0
#define cAf6_ramhotdmlkupcfg_PwID_Bit_End                                                                   12
#define cAf6_ramhotdmlkupcfg_PwID_Mask                                                                cBit12_0
#define cAf6_ramhotdmlkupcfg_PwID_Shift                                                                      0
#define cAf6_ramhotdmlkupcfg_PwID_MaxVal                                                                0x1fff
#define cAf6_ramhotdmlkupcfg_PwID_MinVal                                                                   0x0
#define cAf6_ramhotdmlkupcfg_PwID_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA High Order TDM mode Control
Reg Addr   : 0x00050000 - 0x000501FF #The address format is 0x00050000 +  HoOc48ID*0x100 + HoMasSTS
Reg Formula: 0x00050000 + HoOc48ID*0x100 + HoMasSTS
    Where  :
           + $HoOc48ID(0-7): HoOC48 ID
           + $HoMasSTS(0-47): HO TDM per OC48 master STS ID
Reg Desc   :
This register configure TDM mode for interworking between Pseudowire and Ho TDM

------------------------------------------------------------------------------*/
#define cAf6Reg_ramhotdmmodecfg_Base                                                                0x00050000
#define cAf6Reg_ramhotdmmodecfg(HoOc48ID, HoMasSTS)                   (0x00050000+(HoOc48ID)*0x100+(HoMasSTS))
#define cAf6Reg_ramhotdmmodecfg_WidthVal                                                                    32
#define cAf6Reg_ramhotdmmodecfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PDAHoIdleCode
BitField Type: RW
BitField Desc: Idle pattern to replace data in case of Lost/Lbit packet
BitField Bits: [12:5]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Bit_Start                                                         5
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Bit_End                                                          12
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Mask                                                       cBit12_5
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Shift                                                             5
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_MaxVal                                                         0xff
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_MinVal                                                          0x0
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PDAHoAisUneqOff
BitField Type: RW
BitField Desc: Set 1 to disable sending AIS/Unequip from PW to Ho TDM
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Bit_Start                                                       4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Bit_End                                                         4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Mask                                                        cBit4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Shift                                                           4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_MaxVal                                                        0x1
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_MinVal                                                        0x0
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PDAHoRepMode
BitField Type: RW
BitField Desc: HO Mode to replace lost and Lbit packet 0: Replace by replaying
previous good packet (often for voice or video application) 1: Replace by AIS
(default) 2: Replace by a configuration idle code Replace by a configuration
idle code
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Bit_Start                                                          2
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Bit_End                                                            3
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Mask                                                         cBit3_2
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Shift                                                              2
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_MaxVal                                                           0x3
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_MinVal                                                           0x0
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PDAHoMode
BitField Type: RW
BitField Desc: TDM HO TDM mode 0: STS1 CEP basic 1: STS3 CEP basic 2: STS12 CEP
basic 3: STS48 CEP basic
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoMode_Bit_Start                                                             0
#define cAf6_ramhotdmmodecfg_PDAHoMode_Bit_End                                                               1
#define cAf6_ramhotdmmodecfg_PDAHoMode_Mask                                                            cBit1_0
#define cAf6_ramhotdmmodecfg_PDAHoMode_Shift                                                                 0
#define cAf6_ramhotdmmodecfg_PDAHoMode_MaxVal                                                              0x3
#define cAf6_ramhotdmmodecfg_PDAHoMode_MinVal                                                              0x0
#define cAf6_ramhotdmmodecfg_PDAHoMode_RstVal                                                              0x0

/* Debug sticky */
#define cPdaDebugSticky1 0x500006 /* 128bits */
#define cPDA_wrblken_disabled cBit31
#define cPDA_wrblkrdy_disabled cBit30
#define cCLA2PDA_VALID_ETH_2 cBit13
#define cCLA2PDA_VALID_ETH_1 cBit12

#define cCLA2PDA_ERR_ETH_2   cBit9
#define cCLA2PDA_ERR_ETH_1   cBit8

#define cPdaDebugSticky2 0x500005 /* 32 bit */
#define cLinkList_ECC_err      cBit21
#define cRead_Free_err         cBit20

#define cHO_Block_err          cBit19
#define cLinkList_request_err  cBit18
#define cJB_valid_err_bit17    cBit17
#define cTDM_CRC_err           cBit16

#define cDeque_HO_err          cBit15
#define cLinkList_Block_same   cBit14
#define cLinkList_Block_empty  cBit13
#define cJB_valid_err_bit12    cBit12

#define cLinkList_valid_err    cBit11
#define cJB_Par_err            cBit10
#define cReorder_err           cBit9
#define cReorder_Page_same     cBit8

#define cReorder_Page_Empty    cBit7
#define cReorder_Seq_err       cBit6
#define cWrite_cache_full      cBit5
#define cWrite_cache_over_size cBit4

#define cWrite_cache_Empty     cBit3
#define cWrite_cache_Block_dis cBit2
#define cBlock_Empty           cBit1
#define cBlock_Same            cBit0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control
Reg Addr   : Address: 0x00080000 - 0x0008FFFF
Reg Formula: Address + LoOc48ID*0x2000 + LoOc24Slice*0x1000 + TdmOc24PwID
    Where  :
           + {$LoOc48ID(0-3): LoOC48 ID} %% {$LoOc24Slice(0-1): Lo-order slice oc24} %% {TdmOc24PwID(0-1023): TDM OC24 slice PWID}
Reg Desc   : This register configures jitter buffer parameters per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmsmallds0control_Base                                                                   0x00080000
#define cAf6Reg_ramlotdmsmallds0control(PWID)                                                         (0x00080000+(PWID))
#define cAf6Reg_ramlotdmsmallds0control_WidthVal                                                                       64
#define cAf6Reg_ramlotdmsmallds0control_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: PDASmallDs0En
BitField Type: RW
BitField Desc: PDA CESoP small DS0 enable
               1: CESoP Pseodo-wire with NxDS0 <= 3
               0: Other Pseodo-wire mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_Bit_Start                                                             0
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_Bit_End                                                               0
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_Mask                                                              cBit0
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_Shift                                                                 0
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_MaxVal                                                              0x0
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_MinVal                                                              0x0
#define cAf6_ramlotdmsmallds0control_PwLowDs0Mode_RstVal                                                              0x0


#endif /* _AF6_REG_AF6CCI0011_RD_PDA_H_ */

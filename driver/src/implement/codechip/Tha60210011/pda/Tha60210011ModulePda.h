/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210011ModulePda.h
 * 
 * Created Date: May 26, 2015
 *
 * Description : PDA
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPDA_H_
#define _THA60210011MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pda/ThaModulePda.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011ModulePda * Tha60210011ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210011ModulePdaObjectInit(AtModule self, AtDevice device);
eAtRet Tha60210011ModulePdaPwDefaultHwConfigure(Tha60210011ModulePda self, AtPw pwAdapter);
void Tha60210011ModulePdaHoLoIndicationReset(Tha60210011ModulePda self, ThaPwAdapter pwAdapter);
uint32 Tha60210011ModulePdaLoSliceHwPwIdGet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice);
void Tha60210011ModulePdaLoSliceHwPwIdSet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice, uint32 hwId);
eAtRet Tha60210011ModulePdaPWCESoPLowRateEnable(ThaModulePda self, AtPw pwAdapter);

#endif /* _THA60210011MODULEPDA_H_ */


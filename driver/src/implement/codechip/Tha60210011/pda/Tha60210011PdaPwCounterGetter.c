/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210011PdaPwCounterGetter.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PDA pw counter getter V2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwRxPldOctCnt(ro)          (0x1E21000UL + (ro) * 0x800UL)
#define cPwRxReorderDropPktCnt(ro)  (0x1E27000UL + (ro) * 0x800UL)
#define cPwRxReorderedPktCnt(ro)    (0x1E28000UL + (ro) * 0x800UL)
#define cPwRxLofsTransCnt(ro)       (0x1E22000UL + (ro) * 0x800UL)
#define cPwRxJitBufOverrunPktCnt(ro)(0x1E2A000UL + (ro) * 0x800UL)
#define cPwJitBufUnderrunEvtCnt(ro) (0x1E2B000UL + (ro) * 0x800UL)
#define cPwRxReorderLostPktCnt(ro)  (0x1E29000UL + (ro) * 0x800UL)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210011PdaPwCounterGetterMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RxBytesGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxPldOctCnt((clear) ? 0 : 1) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static uint32 RxReorderedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxReorderedPktCnt(mRo(clear)) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static uint32 RxLostPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxReorderLostPktCnt(mRo(clear)) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static uint32 RxOutOfSeqDropedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxReorderDropPktCnt(mRo(clear)) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static uint32 RxJitBufOverrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxJitBufOverrunPktCnt(mRo(clear)) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static uint32 RxJitBufUnderrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwJitBufUnderrunEvtCnt(mRo(clear)) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static uint32 RxLopsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    AtUnused(self);
    return mChannelHwRead(pw, cPwRxLofsTransCnt(mRo(clear)) + mMethodsGet(modulePda)->CounterOffset(modulePda, pw), cThaModulePwPmc);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "pda_counter_getter";
    }

static void OverrideAtObject(Tha60210011PdaPwCounterGetter self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(Tha60210011PdaPwCounterGetter self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(Tha60210011PdaPwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override methods */
        mMethodOverride(m_methods, RxBytesGet);
        mMethodOverride(m_methods, RxReorderedPacketsGet);
        mMethodOverride(m_methods, RxLostPacketsGet);
        mMethodOverride(m_methods, RxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_methods, RxJitBufOverrunGet);
        mMethodOverride(m_methods, RxJitBufUnderrunGet);
        mMethodOverride(m_methods, RxLopsGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdaPwCounterGetter);
    }

Tha60210011PdaPwCounterGetter Tha60210011PdaPwCounterGetterObjectInit(Tha60210011PdaPwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011PdaPwCounterGetter Tha60210011PdaPwCounterGetterNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011PdaPwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return Tha60210011PdaPwCounterGetterObjectInit(newGetter);
    }

uint32 PdaPwCounterGetterRxBytesGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxBytesGet(self, modulePda, pw, clear);
    return 0;
    }

uint32 PdaPwCounterGetterRxReorderedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxReorderedPacketsGet(self, modulePda, pw, clear);
    return 0;
    }

uint32 PdaPwCounterGetterRxLostPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxLostPacketsGet(self, modulePda, pw, clear);
    return 0;
    }

uint32 PdaPwCounterGetterRxOutOfSeqDropedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxOutOfSeqDropedPacketsGet(self, modulePda, pw, clear);
    return 0;
    }

uint32 PdaPwCounterGetterRxJitBufOverrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxJitBufOverrunGet(self, modulePda, pw, clear);
    return 0;
    }

uint32 PdaPwCounterGetterRxJitBufUnderrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxJitBufUnderrunGet(self, modulePda, pw, clear);
    return 0;
    }

uint32 PdaPwCounterGetterRxLopsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxLopsGet(self, modulePda, pw, clear);
    return 0;
    }


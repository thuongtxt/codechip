/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210011ModulePdaInternal.h
 * 
 * Created Date: Sep 29, 2015
 *
 * Description : 60210011 PDA internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPDAINTERNAL_H_
#define _THA60210011MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/pda/Tha60150011ModulePdaInternal.h"
#include "Tha60210011ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210011PdaPwCounterGetter * Tha60210011PdaPwCounterGetter;

typedef struct tTha60210011ModulePdaMethods
    {
    uint32 (*HoTdmLookupCtrl)(Tha60210011ModulePda self);
    uint32 (*LoTdmLookupCtrl)(Tha60210011ModulePda self, AtPw pw);
    uint32 (*PwTdmLowRateOffset)(Tha60210011ModulePda self, AtPw pw);
    uint32 (*PwHoTdmLookupOffset)(Tha60210011ModulePda self, AtPw pw);
    uint32 (*PwLoTdmLookupOffset)(Tha60210011ModulePda self, AtPw pw);
    uint32 (*TdmLookupResetValue)(Tha60210011ModulePda self);
    eAtRet (*PwLoTdmLookupSet)(Tha60210011ModulePda self, AtPw pw);
    eAtRet (*PwHoTdmLookupSet)(Tha60210011ModulePda self, AtPw pw);
    eAtRet (*PwLoTdmLookupReset)(Tha60210011ModulePda self, AtPw pw);
    eAtRet (*PwHoTdmLookupReset)(Tha60210011ModulePda self, AtPw pw);
    eAtRet (*PwDefaultHwConfigure)(Tha60210011ModulePda self, AtPw pw);
    void   (*PwBufferRegsShow)(Tha60210011ModulePda self, AtPw pw);
    void   (*PwCircuitRegsShow)(Tha60210011ModulePda self, AtPw pw);
    uint32 (*LoSliceHwPwIdGet)(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice);
    void (*LoSliceHwPwIdSet)(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice, uint32 hwId);
    uint32 (*StartVersionSupportLowRateCesop)(Tha60210011ModulePda self);
    Tha60210011PdaPwCounterGetter (*CounterGetter)(Tha60210011ModulePda self);
    uint8 (*Oc48IdFromSlice)(Tha60210011ModulePda self, AtPw pwAdapter, uint8 sliceId);
    eAtRet (*LookupReset)(Tha60210011ModulePda self);

    /* Register fields */
    uint32 (*HoLoOc48IdMask)(Tha60210011ModulePda self);
    uint32 (*HoLoOc48IdShift)(Tha60210011ModulePda self);
    uint32 (*TdmLineIdMask)(Tha60210011ModulePda self);
    uint32 (*TdmLineIdShift)(Tha60210011ModulePda self);
    uint32 (*PwLoSlice24SelMask)(Tha60210011ModulePda self);
    uint32 (*PwLoSlice24SelShift)(Tha60210011ModulePda self);
    uint32 (*PwCEPModeMask)(Tha60210011ModulePda self);
    uint32 (*PwCEPModeShift)(Tha60210011ModulePda self);
    uint32 (*PwLowDs0ModeMask)(Tha60210011ModulePda self);
    uint32 (*PwLowDs0ModeShift)(Tha60210011ModulePda self);
    uint32 (*PwLookupPwIDMask)(Tha60210011ModulePda self);
    uint8 (*PwLookupPwIDShift)(Tha60210011ModulePda self);

    /* Register bases */
    uint32 (*LoTdmSmallDS0Control)(Tha60210011ModulePda self);
    eBool  (*RxMbitCesopIsSupported)(Tha60210011ModulePda self);
    uint32 (*LoTdmModeOffset)(Tha60210011ModulePda self, uint32 oc48Slice, uint32 oc24Slice, uint32 channelHwId);
    uint32 (*LoTdmLookupOffset)(Tha60210011ModulePda self, uint32 oc48Slice, uint32 oc24Slice, uint32 channelHwId);
    }tTha60210011ModulePdaMethods;

typedef struct tTha60210011ModulePda
    {
    tTha60150011ModulePda super;
    const tTha60210011ModulePdaMethods *methods;

    /* Private data */
    Tha60210011PdaPwCounterGetter counterGetter;
    eBool jitterCenterStarted;
    }tTha60210011ModulePda;

typedef struct tTha60210011PdaPwCounterGetterMethods
    {
    uint32 (*RxBytesGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    uint32 (*RxReorderedPacketsGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    uint32 (*RxLostPacketsGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    uint32 (*RxOutOfSeqDropedPacketsGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    uint32 (*RxJitBufOverrunGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    uint32 (*RxJitBufUnderrunGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    uint32 (*RxLopsGet)(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
    }tTha60210011PdaPwCounterGetterMethods;

typedef struct tTha60210011PdaPwCounterGetter
    {
    tAtObject super;
    const tTha60210011PdaPwCounterGetterMethods* methods;
    }tTha60210011PdaPwCounterGetter;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60210011PdaPwCounterGetter Tha60210011PdaPwCounterGetterObjectInit(Tha60210011PdaPwCounterGetter self);

Tha60210011PdaPwCounterGetter Tha60210011PdaPwCounterGetterNew(void);
Tha60210011PdaPwCounterGetter Tha60210011PdaPwCounterGetterV2New(void);

uint32 PdaPwCounterGetterRxBytesGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
uint32 PdaPwCounterGetterRxReorderedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
uint32 PdaPwCounterGetterRxLostPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
uint32 PdaPwCounterGetterRxOutOfSeqDropedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
uint32 PdaPwCounterGetterRxJitBufOverrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
uint32 PdaPwCounterGetterRxJitBufUnderrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);
uint32 PdaPwCounterGetterRxLopsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPDAINTERNAL_H_ */


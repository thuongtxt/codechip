/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha62290011PdhDe3.c
 *
 * Created Date: Nov 21, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011PdhDe3Internal.h"
#include "AtPdhSerialLine.h"
#include "Tha62290011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha62290011PdhDe3
    {
    tTha60290011PdhDe3 super;
    }tTha62290011PdhDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;

/* Save Super Implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SerialLineIsEC1(AtPdhChannel self)
    {
    uint32 de3Id = AtChannelIdGet((AtChannel)self);
    AtModule module = AtChannelModuleGet((AtChannel)self);
    AtPdhSerialLine serLine = AtModulePdhDe3SerialLineGet((AtModulePdh)module, de3Id);

    return (AtPdhSerialLineModeGet(serLine) == cAtPdhDe3SerialLineModeEc1) ? cAtTrue : cAtFalse;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (ret != cAtOk)
        return ret;

    if (SerialLineIsEC1((AtPdhChannel)self))
        return cAtOk;

    Tha62290011PdhChannelDimLiuTimingModeSet((AtPdhChannel)self, timingMode);

    return cAtOk;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = m_AtChannelMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (SerialLineIsEC1((AtPdhChannel)self))
        return cAtOk;

    Tha62290011PdhChannelDimLiuEnable((AtPdhChannel)self, enable);

    return cAtOk;
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel(self);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha62290011PdhDe3));

    /* Supper constructor */
    if (Tha60290011PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha62290011PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha62290011PdhDe3));
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }

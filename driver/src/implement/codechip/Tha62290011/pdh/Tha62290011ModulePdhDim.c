/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha62290011ModulePdhDim.c
 *
 * Created Date: Dec 26, 2016
 *
 * Description : Implementation of DIM
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "Tha62290011ModulePdh.h"
#include "AtPdhSerialLine.h"
#include "Tha62290011DimReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaDimRegLiuModeEc1        (4)
#define cThaDimRegLiuModeE3         (3)
#define cThaDimRegLiuModeDs3        (2)
#define cThaDimRegLiuModeE1         (1)
#define cThaDimRegLiuModeDs1        (0)

#define cDimMacSubtypeEc1Ds3E3      0x10
#define cDimMacSubtypeDs1E1         0x00
#define cMaxLiuDs1E1                (84UL)
#define cMaxLiuEc1De3               (24UL)


/*--------------------------- Macros -----------------------------------------*/
/* 28-51: E3/DS3/EC1 */
#define mDe3ToLiuId(de3Id)              (de3Id + 28UL)
/* 0-83: E1/DE1 */
#define mDe1ToLiuId(de1Id)              (de1Id)

#define mDimLiuChannelCtrlReg(liuId)    (cAf6Reg_upen_c + liuId*4UL)
#define mDimLiuTxOverrunCntReg(liuId)   (cAf6Reg_upen_overruncnt + liuId*4UL)
#define mDimLiuTxUnderrunCntReg(liuId)  (cAf6Reg_upen_underruncnt + liuId*4UL)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegAddressWithBase(ThaModulePdh self, uint32 regAddr)
    {
    return regAddr + Tha62290011ModulePdhDimBaseAddress(self);
    }

static eBool LiuInterfaceIsDs3(ThaModulePdh self)
    {
    return (Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self) == cTha60290011PdhInterfaceTypeDe3) ? cAtTrue : cAtFalse;
    }

static uint32 PdhChannelToLiuId(AtPdhChannel channel)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)channel);
    uint8 channelType = AtPdhChannelTypeGet(channel);

    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return mDe1ToLiuId(channelId);

    return mDe3ToLiuId(channelId);
    }

static uint32 ChannelToHwDimMode(AtPdhChannel pdhChannel)
    {
    uint8 channelType = AtPdhChannelTypeGet(pdhChannel);
    uint32 mode;

    if (channelType == cAtPdhChannelTypeE1)
        return cThaDimRegLiuModeE1;

    if (channelType == cAtPdhChannelTypeDs1)
        return cThaDimRegLiuModeDs1;

    mode = AtPdhSerialLineModeGet((AtPdhSerialLine)pdhChannel);
    if (mode == cAtPdhDe3SerialLineModeEc1)
        return cThaDimRegLiuModeEc1;

    if (mode == cAtPdhDe3SerialLineModeE3)
        return cThaDimRegLiuModeE3;

    return cThaDimRegLiuModeDs3;
    }

static uint8 TimingModeToHwMode(eAtTimingMode timingMode)
    {
    if (timingMode == cAtTimingModeExt1Ref)
        return 0x2;

    if (timingMode == cAtTimingModeLoop)
        return 0x1;

    return 0x0;
    }

static eAtRet DimActive(ThaModulePdh self)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen0);

    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen0_Active_, 0);
    mModuleHwWrite(self, regAddr, regVal);
    mRegFieldSet(regVal, cAf6_upen0_Active_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DimFlush(ThaModulePdh self)
    {
    uint32 dimBase = Tha62290011ModulePdhDimBaseAddress(self);
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    ThaDeviceMemoryFlush(device, dimBase + cAf6Reg_upen_c,  dimBase + 0x54C, 0);

    return cAtOk;
    }

static eAtRet LiuEnable(AtPdhChannel channel, eBool enable)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    uint32 liuId = PdhChannelToLiuId(channel);
    uint32 regAddr = RegAddressWithBase(module, mDimLiuChannelCtrlReg(liuId));
    uint32 regVal  = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_LIU_RX_Enable_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_upen_c_LIU_TX_Enable_, mBoolToBin(enable));
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eAtRet LiuModeSet(AtPdhChannel channel)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    uint32 liuId = PdhChannelToLiuId(channel);
    uint32 regAddr = RegAddressWithBase(module, mDimLiuChannelCtrlReg(liuId));
    uint32 regVal  = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_LIU_TX_Mode_, ChannelToHwDimMode(channel));
    mRegFieldSet(regVal, cAf6_upen_c_LIU_RX_Mode_, ChannelToHwDimMode(channel));
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eAtRet LiuTimingModeSet(AtPdhChannel channel, eAtTimingMode timingMode)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    uint32 liuId = PdhChannelToLiuId(channel);
    uint32 regAddr = RegAddressWithBase(module, mDimLiuChannelCtrlReg(liuId));
    uint32 regVal  = mModuleHwRead(module, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_LIU_Tx_Timing_Mode_, TimingModeToHwMode(timingMode));
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eAtRet EthTypeSet(ThaModulePdh self, uint16 type)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen6);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen6_Ether_Type_, type);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint16 EthTypeGet(ThaModulePdh self)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen6);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint16)mRegField(regVal, cAf6_upen6_Ether_Type_);
    }

static eAtRet EthSubtypeSet(ThaModulePdh self, uint8 subtype)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen_subtype);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_subtype_Ether_Subtype_, subtype);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint8 EthSubtypeGet(ThaModulePdh self)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen_subtype);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint8)mRegField(regVal, cAf6_upen_subtype_Ether_Subtype_);
    }

static eAtRet EthLengthSet(ThaModulePdh self, uint16 length)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen6);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen6_Ether_Len_, length);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint16 EthLengthGet(ThaModulePdh self)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen6);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint16)mRegField(regVal, cAf6_upen6_Ether_Len_);
    }

static void HwRegToMac(uint32 msb16b, uint32 lsb32b, uint8 *address)
    {
    address[0] = (uint8)((msb16b & cBit15_8) >> 8);
    address[1] = (uint8)(msb16b & cBit7_0);

    address[2] = (uint8)((lsb32b & cBit31_24) >> 24);
    address[3] = (uint8)((lsb32b & cBit23_16) >> 16);
    address[4] = (uint8)((lsb32b & cBit15_8) >> 8);
    address[5] = (uint8)(lsb32b & cBit7_0);
    }

static void MacToHwReg(uint32 * msb16b, uint32 * lsb32b, uint8 *address)
    {
    mFieldIns(msb16b, cBit15_8, 8, address[0]);
    mFieldIns(msb16b, cBit7_0, 0, address[1]);

    mFieldIns(lsb32b, cBit31_24, 24, address[2]);
    mFieldIns(lsb32b, cBit23_16, 16, address[3]);
    mFieldIns(lsb32b, cBit15_8, 8, address[4]);
    mFieldIns(lsb32b, cBit7_0, 0, address[5]);
    }

static eAtModuleEthRet DestMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    uint32 lsb32Mac = 0;
    uint32 msb16Mac = mModuleHwRead((ThaEthPort)self, RegAddressWithBase(self, cAf6Reg_upen2));
    MacToHwReg(&msb16Mac, &lsb32Mac, address);
    mModuleHwWrite(self, RegAddressWithBase(self, cAf6Reg_upen2), msb16Mac);
    mModuleHwWrite(self, RegAddressWithBase(self, cAf6Reg_upen3), lsb32Mac);

    return cAtOk;
    }

static eAtModuleEthRet DestMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    uint32 msb16Mac = mModuleHwRead(self, RegAddressWithBase(self, cAf6Reg_upen2));
    uint32 lsb32Mac = mModuleHwRead(self, RegAddressWithBase(self, cAf6Reg_upen3));
    HwRegToMac(msb16Mac, lsb32Mac, address);

    return cAtOk;
    }

static eAtModuleEthRet SourceMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    uint32 lsb32Mac = 0;
    uint32 msb16Mac = mModuleHwRead(self, RegAddressWithBase(self, cAf6Reg_upen4));
    MacToHwReg(&msb16Mac, &lsb32Mac, address);
    mModuleHwWrite(self, RegAddressWithBase(self, cAf6Reg_upen4), msb16Mac);
    mModuleHwWrite(self, RegAddressWithBase(self, cAf6Reg_upen5), lsb32Mac);

    return cAtOk;
    }

static eAtModuleEthRet SourceMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    uint32 msb16Mac = mModuleHwRead(self, RegAddressWithBase(self, cAf6Reg_upen4));
    uint32 lsb32Mac = mModuleHwRead(self, RegAddressWithBase(self, cAf6Reg_upen5));
    HwRegToMac(msb16Mac, lsb32Mac, address);

    return cAtOk;
    }

static eAtRet DimLiuLoopback(ThaModulePdh self, uint32 pdhChannelId, uint8 loopMode)
    {
    uint32 regVal, regAddr, liuId;
    if (LiuInterfaceIsDs3(self))
        liuId = mDe3ToLiuId(pdhChannelId);
    else
        liuId = mDe1ToLiuId(pdhChannelId);

    regAddr = RegAddressWithBase(self, mDimLiuChannelCtrlReg(liuId));
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_LIU_LoopOut_, (loopMode == cAtLoopbackModeRemote) ? 0x1 : 0x0);
    mRegFieldSet(regVal, cAf6_upen_c_LIU_LoopIn_, (loopMode == cAtLoopbackModeLocal) ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet DimAxi4Loopback(ThaModulePdh self, uint8 loopMode)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_upen0);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen0_AXI4_Stream_loopout_, (loopMode == cAtLoopbackModeRemote) ? 0x1 : 0x0);
    mRegFieldSet(regVal, cAf6_upen0_AXI4_Stream_loopin_, (loopMode == cAtLoopbackModeLocal) ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static void DebugPrintVal(const char *debugString, uint32 fieldVal)
    {
    AtPrintc(cSevNormal, "        %-30s: 0x%X\r\n", debugString, fieldVal);
    }

static void DebugPrintStr(const char * desc, const char *debugString)
    {
    AtPrintc(cSevNormal, "        %-30s: %s\r\n", desc, debugString);
    }

static uint32 De1DaErrorCounter(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_x_cnt04_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 De1SaErrorCounter(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_de1saerr_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 De3Ec1DaErrorCounter(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_x_cnt06_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 De3Ec1SaErrorCounter(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_de3saerr_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 OutComingPackets(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_x_cnt00_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 OutComingBytes(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_x_cnt01_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 InComPackets(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_x_cnt02_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 InComBytes(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = RegAddressWithBase(self, cAf6Reg_x_cnt03_pen);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 TxLiuFifoOverrunCnt(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 pdhChannelId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 liuId = LiuInterfaceIsDs3(self) ? mDe3ToLiuId(pdhChannelId) : mDe1ToLiuId(pdhChannelId);
    uint32 regAddr = RegAddressWithBase(self, mDimLiuTxOverrunCntReg(liuId));
    uint32 regVal = mModuleHwRead(self, regAddr);
    mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 TxLiuFifoUnderrunCnt(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 pdhChannelId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 liuId = LiuInterfaceIsDs3(self) ? mDe3ToLiuId(pdhChannelId) : mDe1ToLiuId(pdhChannelId);
    uint32 regAddr = RegAddressWithBase(self, mDimLiuTxUnderrunCntReg(liuId));
    uint32 regVal = mModuleHwRead(self, regAddr);
    mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static eBool RxLiuFifoChannelIsFull(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 dimBase = Tha62290011ModulePdhDimBaseAddress((ThaModulePdh)self);
    uint32 channelId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 channelMask = cBit0 << (channelId % 32);
    uint32 regAddr = cAf6Reg_stk0_pen + dimBase + (channelId/32UL) * 4;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mModuleHwWrite(self, regAddr, channelMask);

    return (regVal & channelMask) ? cAtTrue : cAtFalse;
    }

static eBool TxLiuFifoIsOverrun(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 dimBase = Tha62290011ModulePdhDimBaseAddress((ThaModulePdh)self);
    uint32 channelId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 channelMask = cBit0 << (channelId % 32);
    uint32 regAddr = cAf6Reg_stk3_pen + dimBase + (channelId/32UL) * 4;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mModuleHwWrite(self, regAddr, channelMask);

    return (regVal & channelMask) ? cAtTrue : cAtFalse;
    }

static eBool TxLiuFifoIsUnderrun(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 dimBase = Tha62290011ModulePdhDimBaseAddress((ThaModulePdh)self);
    uint32 channelId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 channelMask = cBit0 << (channelId % 32);
    uint32 regAddr = cAf6Reg_stk6_pen + dimBase + (channelId/32UL) * 4;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mModuleHwWrite(self, regAddr, channelMask);

    return (regVal & channelMask) ? cAtTrue : cAtFalse;
    }

static uint32 De3LCVCnt(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 dimBase = Tha62290011ModulePdhDimBaseAddress((ThaModulePdh)self);
    uint32 de3Id = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 regAddr = dimBase + cAf6Reg_x_lcvcnt_pen + de3Id * 4;
    uint32 regVal =  mModuleHwRead(self, regAddr);
    mModuleHwWrite(self, regAddr, 0x0);

    return regVal;
    }

static uint32 RegsDisplay(ThaModulePdh self, AtPdhChannel pdhChannel, const char* regName, uint32 regBaseAddress)
    {
    uint32 regOffset = Tha62290011ModulePdhDimBaseAddress(self);
    uint32 regAddr = regBaseAddress + regOffset;

    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pdhChannel, regAddr, cAtModulePdh);
    return mModuleHwRead(self, regAddr);
    }

static const char *SubTypeMode2String(uint32 subType)
    {
    if (subType == 0x0)
        return "Ds1/E1";

    if (subType == 0x10)
        return "DS3/E3/EC-1 Frames";

    if ((subType >= 0x80) && (subType <= 0xFF))
        return "Control frames";

    return "Reserved";
    }

static const char *DimLiuMode2String(uint32 liuMode)
    {
    switch (liuMode)
        {
        case 0x0: return "DS1";
        case 0x1: return "E1";
        case 0x2: return "DS3";
        case 0x3: return "E3";
        case 0x4: return "EC1";
        default:  return "Invalid";
        }
    }

static const char *TimingMode2String(uint32 timingMode)
    {
    switch (timingMode)
        {
        case 0x0: return "Recovery from ETH payload";
        case 0x1: return "Looptime";
        case 0x2: return "External";
        case 0x3: return "System timing mode.";
        default:  return "Reserve";
        }
    }

static void DimRegsDebugShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 pdhChannelId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 liuId = LiuInterfaceIsDs3(self) ? mDe3ToLiuId(pdhChannelId) : mDe1ToLiuId(pdhChannelId);
    uint32 regVal = RegsDisplay(self, pdhChannel, "LIU RX/Tx Channel control", mDimLiuChannelCtrlReg(liuId));
    DebugPrintVal("TX Enable", mRegField(regVal, cAf6_upen_c_LIU_TX_Enable_));
    DebugPrintVal("RX Enable", mRegField(regVal, cAf6_upen_c_LIU_RX_Enable_));
    DebugPrintVal("Liu Loopout", mRegField(regVal, cAf6_upen_c_LIU_LoopOut_));
    DebugPrintVal("Liu Loopin", mRegField(regVal, cAf6_upen_c_LIU_LoopIn_));
    DebugPrintStr("TX Mode", DimLiuMode2String(mRegField(regVal, cAf6_upen_c_LIU_TX_Mode_)));
    DebugPrintStr("RX Mode", DimLiuMode2String(mRegField(regVal, cAf6_upen_c_LIU_RX_Mode_)));
    DebugPrintStr("Tx Timing Mode", TimingMode2String(mRegField(regVal, cAf6_upen_c_LIU_Tx_Timing_Mode_)));

    regVal = RegsDisplay(self, pdhChannel, "Ethernet Encapsulation EtheType and EtherLength  Configuration", cAf6Reg_upen6);
    DebugPrintVal("EtherType", mRegField(regVal, cAf6_upen6_Ether_Type_));
    DebugPrintVal("Ether Len", mRegField(regVal, cAf6_upen6_Ether_Len_));

    regVal = RegsDisplay(self, pdhChannel, "Ethernet Encapsulation SA bit47_32 Configuration", cAf6Reg_upen_subtype);
    DebugPrintStr("Sub Type", SubTypeMode2String(mRegField(regVal, cAf6_upen_subtype_Ether_Subtype_)));
    }

static eAtRet DebugChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    AtPrintc(cSevInfo, "   - Dim Info:\r\n");
    DimRegsDebugShow(self, pdhChannel);

    AtPrintc(cSevInfo, "   - Dim Counter:\r\n");
    DebugPrintVal("TX LIU FIFO Overrun Counter",    TxLiuFifoOverrunCnt(self, pdhChannel));
    DebugPrintVal("TX LIU FIFO Underrun Counter",   TxLiuFifoUnderrunCnt(self, pdhChannel));
    DebugPrintStr("RX LIU FIFO Full channel",       RxLiuFifoChannelIsFull(self, pdhChannel) ? "Set" : "Clear");
    DebugPrintStr("TX LIU FIFO Overrun channel",    TxLiuFifoIsOverrun(self, pdhChannel) ? "Set" : "Clear");
    DebugPrintStr("TX LIU FIFO Underrun channel",   TxLiuFifoIsUnderrun(self, pdhChannel) ? "Set" : "Clear");
    if (LiuInterfaceIsDs3(self))
        DebugPrintVal("DE3 LCV Counter",    De3LCVCnt(self, pdhChannel));
    return cAtOk;
    }

static eBool StickyIsSet(uint32 regVal, uint32 mask)
    {
    return (mask & regVal) ? cAtTrue : cAtFalse;
    }

static void ErrStickyPrint(const char * str, eBool isSet)
    {
    uint8 color = isSet ? cSevCritical : cSevInfo;
    AtPrintc(cSevNormal, "%s", str);
    AtPrintc(color, "%s\n", isSet ? "set" : "clear");
    }

static eBool IsDecTdmDataLost(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_lost_tdm_dat_Mask);
    }

static eBool IsDecapBufferFull(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_buf_ful_Mask);
    }

static eBool IsDe3DaMacError(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_de3_da_err_Mask);
    }

static eBool IsDe3SaMacError(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_de3_sa_err_Mask);
    }

static eBool IsDe1DaMacError(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_de1_da_err_Mask);
    }

static eBool IsDe1SaMacError(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_de1_sa_err_Mask);
    }

static eBool IsDe3SeqenceError(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_de3_seq_err_Shift);
    }

static eBool IsDe1SeqenceError(uint32 regVal)
    {
    return StickyIsSet(regVal, cAf6_stk9_pen_dec_de1_seq_err_Mask);
    }

static eAtRet DebugCounterShow(ThaModulePdh self)
    {
    AtPrintc(cSevNormal, "\nCounters\n");
    AtPrintc(cSevNormal, "  Dim Tx Packet Counter            : %d\n", OutComingPackets(self, cAtTrue));
    AtPrintc(cSevNormal, "  Dim Tx Byte Counter              : %d\n", OutComingBytes(self, cAtTrue));
    AtPrintc(cSevNormal, "  Dim Rx Packet Counter            : %d\n", InComPackets(self, cAtTrue));
    AtPrintc(cSevNormal, "  Dim Rx Byte Counter              : %d\n", InComBytes(self, cAtTrue));
    AtPrintc(cSevNormal, "  DE1 DA Error Counter             : %d\n", De1DaErrorCounter(self, cAtTrue));
    AtPrintc(cSevNormal, "  DE1 SA Error Counter             : %d\n", De1SaErrorCounter(self, cAtTrue));
    AtPrintc(cSevNormal, "  DE3/EC1 DA Error Counter         : %d\n", De3Ec1DaErrorCounter(self, cAtTrue));
    AtPrintc(cSevNormal, "  DE3/EC1 SA Error Counter         : %d\n", De3Ec1SaErrorCounter(self, cAtTrue));
    return cAtOk;
    }

static void DebugPdhDimStickyStatusShow(ThaModulePdh self)
    {
    uint32 regVal = mModuleHwRead(self, RegAddressWithBase(self, cAf6Reg_stk9_pen));

    AtPrintc(cSevNormal, "\nStickies\n");
    ErrStickyPrint("  DEC TDM lost data                : ", IsDecTdmDataLost(regVal));
    ErrStickyPrint("  DEC Buffer Full                  : ", IsDecapBufferFull(regVal));
    ErrStickyPrint("  DEC DE3 DA error                 : ", IsDe3DaMacError(regVal));
    ErrStickyPrint("  DEC DE3 SA error                 : ", IsDe3SaMacError(regVal));
    ErrStickyPrint("  DEC DE3 DA error                 : ", IsDe1DaMacError(regVal));
    ErrStickyPrint("  DEC DE1 SA error                 : ", IsDe1SaMacError(regVal));
    ErrStickyPrint("  DEC DE3 Sequence Number error    : ", IsDe3SeqenceError(regVal));
    ErrStickyPrint("  DEC DE1 Sequence Number error    : ", IsDe1SeqenceError(regVal));
    mModuleHwWrite(self, RegAddressWithBase(self, cAf6Reg_stk9_pen), 0xFFFFFFFF);
    }

static eAtRet DebugAxi4StreamShow(ThaModulePdh self)
    {
    uint8 daMacAddr[6], saMacAddr[6];
    SourceMacAddressGet(self, saMacAddr);
    DestMacAddressGet(self, daMacAddr);

    AtPrintc(cSevInfo, "\nDIM Info\n");
    AtPrintc(cSevNormal, "  Da Mac                           : %s\n", AtBytes2String(daMacAddr, cAtMacAddressLen, 16));
    AtPrintc(cSevNormal, "  Sa Mac                           : %s\n", AtBytes2String(saMacAddr, cAtMacAddressLen, 16));
    AtPrintc(cSevNormal, "  Eth Type                         : 0x%X\n", EthTypeGet(self));
    AtPrintc(cSevNormal, "  Subtype                          : 0x%X\n", EthSubtypeGet(self));
    AtPrintc(cSevNormal, "  Eth Length                       : %d\n", EthLengthGet(self));

    DebugPdhDimStickyStatusShow(self);

    return DebugCounterShow(self);
    }

uint32 Tha62290011ModulePdhDimBaseAddress(ThaModulePdh self)
    {
    if (self)
        return 0xFE2000;

    return 0xCAFECAFE;
    }

eAtRet Tha62290011ModulePdhDimActive(ThaModulePdh self)
    {
    if (self)
        return DimActive(self);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimFlush(ThaModulePdh self)
    {
    if (self)
        return DimFlush(self);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011PdhChannelDimLiuEnable(AtPdhChannel channel, eBool enable)
    {
    if (channel)
        return LiuEnable(channel, enable);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011PdhChannelDimLiuModeSet(AtPdhChannel channel)
    {
    if (channel)
        return LiuModeSet(channel);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011PdhChannelDimLiuTimingModeSet(AtPdhChannel channel, eAtTimingMode timingMode)
    {
    if (channel)
        return LiuTimingModeSet(channel, timingMode);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimLiuLoopback(ThaModulePdh self, uint32 pdhChannelId, uint8 loopMode)
    {
    if (self)
        return DimLiuLoopback(self, pdhChannelId, loopMode);
    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimAxi4Loopback(ThaModulePdh self, uint8 loopMode)
    {
    if (self)
        return DimAxi4Loopback(self, loopMode);
    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimEthSubtypeSet(ThaModulePdh self, uint8 subtype)
    {
    if (self)
        return EthSubtypeSet(self, subtype);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimEthTypeSet(ThaModulePdh self, uint16 type)
    {
    if (self)
        return EthTypeSet(self, type);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimEthLengthSet(ThaModulePdh self, uint16 length)
    {
    if (self)
        return EthLengthSet(self, length);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimDebugChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    if (self || pdhChannel)
        return DebugChannelRegsShow(self, pdhChannel);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimSourceMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return SourceMacAddressSet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimDestMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return DestMacAddressSet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimSourceMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return SourceMacAddressGet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimDestMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return DestMacAddressGet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha62290011ModulePdhDimDebugAxi4StreamShow(ThaModulePdh self)
    {
    if (self)
        return DebugAxi4StreamShow(self);

    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha62290011PdhSerialLine.c
 *
 * Created Date: Nov 25, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011PdhSerialLineInternal.h"
#include "Tha62290011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha62290011PdhSerialLine
    {
    tTha60290011PdhSerialLine super;
    }tTha62290011PdhSerialLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPdhSerialLineMethods m_AtPdhSerialLineOverride;

/* Save super implementation */
static const tAtPdhSerialLineMethods *m_AtPdhSerialLineMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ModeSet(AtPdhSerialLine self, uint32 mode)
    {
    eAtRet ret = m_AtPdhSerialLineMethods->ModeSet((AtPdhSerialLine)self, mode);
    if (ret != cAtOk)
        return ret;

    return Tha62290011PdhChannelDimLiuModeSet((AtPdhChannel)self);
    }

static void OverrideAtPdhSerialLine(AtPdhSerialLine self)
    {
    AtPdhSerialLine serialLine = (AtPdhSerialLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhSerialLineMethods = mMethodsGet(serialLine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhSerialLineOverride, m_AtPdhSerialLineMethods, sizeof(tAtPdhSerialLineMethods));

        mMethodOverride(m_AtPdhSerialLineOverride, ModeSet);
        }

    mMethodsSet(serialLine, &m_AtPdhSerialLineOverride);
    }

static void Override(AtPdhSerialLine self)
    {
    OverrideAtPdhSerialLine(self);
    }

static AtPdhSerialLine ObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha62290011PdhSerialLine));

    /* Supper constructor */
    if (Tha60290011PdhSerialLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhSerialLine Tha62290011PdhSerialLineNew(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhSerialLine newSerialLine = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha62290011PdhSerialLine));
    if (newSerialLine == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newSerialLine, channelId, module);
    }

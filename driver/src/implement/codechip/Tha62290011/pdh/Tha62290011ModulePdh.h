/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha62290011ModulePdh.h
 *
 * Created Date: Nov 22, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA62290011MODULEPDH_H_
#define _THA62290011MODULEPDH_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha62290011ModulePdhDimBaseAddress(ThaModulePdh self);
eAtRet Tha62290011ModulePdhDimLiuLoopback(ThaModulePdh self, uint32 pdhId, uint8 loopMode);
eAtRet Tha62290011ModulePdhDimAxi4Loopback(ThaModulePdh self, uint8 loopMode);
uint32 Tha62290011ModulePdhDimBaseAddress(ThaModulePdh self);
eAtRet Tha62290011ModulePdhDimActive(ThaModulePdh self);
eAtRet Tha62290011ModulePdhDimFlush(ThaModulePdh self);
eAtRet Tha62290011ModulePdhDimLiuLoopback(ThaModulePdh self, uint32 pdhChannelId, uint8 loopMode);
eAtRet Tha62290011ModulePdhDimAxi4Loopback(ThaModulePdh self, uint8 loopMode);
eAtRet Tha62290011ModulePdhDimEthSubtypeSet(ThaModulePdh self, uint8 subtype);
eAtRet Tha62290011ModulePdhDimEthTypeSet(ThaModulePdh self, uint16 type);
eAtRet Tha62290011ModulePdhDimEthLengthSet(ThaModulePdh self, uint16 length);
eAtRet Tha62290011ModulePdhDimDebugChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel);
eAtRet Tha62290011ModulePdhDimSourceMacAddressSet(ThaModulePdh self, uint8 *address);
eAtRet Tha62290011ModulePdhDimDestMacAddressSet(ThaModulePdh self, uint8 *address);
eAtRet Tha62290011ModulePdhDimSourceMacAddressGet(ThaModulePdh self, uint8 *address);
eAtRet Tha62290011ModulePdhDimDestMacAddressGet(ThaModulePdh self, uint8 *address);
eAtRet Tha62290011ModulePdhDimDebugAxi4StreamShow(ThaModulePdh self);

eAtRet Tha62290011PdhChannelDimLiuEnable(AtPdhChannel channel, eBool enable);
eAtRet Tha62290011PdhChannelDimLiuModeSet(AtPdhChannel channel);
eAtRet Tha62290011PdhChannelDimLiuTimingModeSet(AtPdhChannel channel, eAtTimingMode timingMode);

AtPdhSerialLine Tha62290011PdhSerialLineNew(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA62290011MODULEPDH_H_ */

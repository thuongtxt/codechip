/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha62290011ModulePdh.c
 *
 * Created Date: Nov 21, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011ModulePdhInternal.h"
#include "Tha62290011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha62290011ModulePdh
    {
    tTha60290011ModulePdh super;
    }tTha62290011ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tAtModulePdhMethods  *m_AtModulePdhMethods  = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
    return Tha62290011PdhDe1New(de1Id, self);
    }

static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    return Tha62290011PdhDe3New(de3Id, self);
    }

static AtPdhSerialLine De3SerialCreate(AtModulePdh self, uint32 serialLineId)
    {
    return Tha62290011PdhSerialLineNew(serialLineId, self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;
    ThaModulePdh module = (ThaModulePdh)self;
    eTha60290011PdhInterfaceType interface = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self);
    uint8 subtype = (interface == cTha60290011PdhInterfaceTypeDe1) ? 0x0 : 0x10;
    uint16 ethLen = Tha60290011ModulePdhLengthDefaultGet((ThaModulePdh)self);

    Tha62290011ModulePdhDimFlush(module);
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    Tha62290011ModulePdhDimEthSubtypeSet(module, subtype);
    Tha62290011ModulePdhDimEthLengthSet(module, ethLen);

    return Tha62290011ModulePdhDimActive(module);
    }

static void PdhChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    m_ThaModulePdhMethods->PdhChannelRegsShow(self, pdhChannel);
    Tha62290011ModulePdhDimDebugChannelRegsShow(self, pdhChannel);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, De3SerialCreate);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, PdhChannelRegsShow);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha62290011ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha62290011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

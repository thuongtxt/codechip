/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha62290011PdhDe1.c
 *
 * Created Date: Nov 21, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "../../Tha60290011/pdh/Tha60290011PdhDe1Internal.h"
#include "Tha62290011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha62290011PdhDe1
    {
    tTha60290011PdhDe1 super;
    }tTha62290011PdhDe1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool LiuInterfaceIsE1Ds1(AtPdhChannel self)
    {
    AtModule module = AtChannelModuleGet((AtChannel)self);
    eTha60290011PdhInterfaceType curInterface = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)module);

    return (curInterface == cTha60290011PdhInterfaceTypeDe1) ? cAtTrue : cAtFalse;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {

    eAtRet ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (ret != cAtOk)
        return ret;

    if (!LiuInterfaceIsE1Ds1((AtPdhChannel)self))
        return cAtOk;

    Tha62290011PdhChannelDimLiuTimingModeSet((AtPdhChannel)self, timingMode);
    return cAtOk;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = m_AtChannelMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (!LiuInterfaceIsE1Ds1((AtPdhChannel)self))
        return cAtOk;

    Tha62290011PdhChannelDimLiuEnable((AtPdhChannel)self, enable);

    return cAtOk;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    if (ret != cAtOk)
        return ret;

    if (!LiuInterfaceIsE1Ds1(self))
        return cAtOk;

    return Tha62290011PdhChannelDimLiuModeSet(self);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(tAtPdhChannelMethods));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha62290011PdhDe1);
    }

static AtPdhDe1 ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290011PdhDe1ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 Tha62290011PdhDe1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDe1, channelId, module);
    }

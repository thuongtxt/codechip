/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_DIM_H_
#define _AF6_REG_AF6CNC0011_RD_DIM_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Active Regiter
Reg Addr   : 0x0000_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures Active for all Arrive modules

------------------------------------------------------------------------------*/
#define cAf6Reg_upen0                                                                               0x00000000

/*--------------------------------------
BitField Name: Liuid_debug
BitField Type: R/W
BitField Desc: LIU ID use to select the LIU ID for TX/RX rate calculation
BitField Bits: [09:03]
--------------------------------------*/
#define cAf6_upen0_Liuid_debug_Mask                                                                    cBit9_3
#define cAf6_upen0_Liuid_debug_Shift                                                                         3

/*--------------------------------------
BitField Name: AXI4_Stream_loopout
BitField Type: R/W
BitField Desc: AXI4 Stream loop_out - Set 1: Loopback from rx DIM Arrive to tx
DIM Arrive - Set 0: Don't loopback
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen0_AXI4_Stream_loopout_Mask                                                              cBit2
#define cAf6_upen0_AXI4_Stream_loopout_Shift                                                                 2

/*--------------------------------------
BitField Name: AXI4_Stream_loopin
BitField Type: R/W
BitField Desc: AXI4 Stream loop_in - Set 1: Loopback from tx DIM Arrive to rx
DIM Arrive - Set 0: Don't loopback
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen0_AXI4_Stream_loopin_Mask                                                               cBit1
#define cAf6_upen0_AXI4_Stream_loopin_Shift                                                                  1

/*--------------------------------------
BitField Name: Active
BitField Type: RW
BitField Desc: Module Active - Set 1: Active all modules - Set 0: De-active all
modules + Flush all FIFOs + Reset all Status and Control Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen0_Active_Mask                                                                           cBit0
#define cAf6_upen0_Active_Shift                                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Version ID
Reg Addr   : 0x0000_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register shows Version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen1                                                                               0x00000004

/*--------------------------------------
BitField Name: Verion_Year
BitField Type: RO
BitField Desc: Version Year
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen1_Verion_Year_Mask                                                                  cBit31_24
#define cAf6_upen1_Verion_Year_Shift                                                                        24

/*--------------------------------------
BitField Name: Verion_Month
BitField Type: RO
BitField Desc: Version Month
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen1_Verion_Month_Mask                                                                 cBit23_16
#define cAf6_upen1_Verion_Month_Shift                                                                       16

/*--------------------------------------
BitField Name: Verion_Date
BitField Type: RO
BitField Desc: Version Date
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen1_Verion_Date_Mask                                                                   cBit15_8
#define cAf6_upen1_Verion_Date_Shift                                                                         8

/*--------------------------------------
BitField Name: Verion_Number
BitField Type: RO
BitField Desc: Version Number
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen1_Verion_Number_Mask                                                                  cBit7_0
#define cAf6_upen1_Verion_Number_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : LIU RX/Tx Channel control
Reg Addr   : 0x0000_0400 - 0x0000_054C
Reg Formula: 0x0000_0400 + liuid*4
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
The Control is use to configure the channel type and timing control per channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_c                                                                              0x00000400

/*--------------------------------------
BitField Name: LIU_TX_AIS_Insert
BitField Type: R/W
BitField Desc: LIU TX AIS Insert - Set 1: AIS Insert Enable - Set 0: AIS Insert
Disable
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_c_LIU_TX_AIS_Insert_Mask                                                              cBit15
#define cAf6_upen_c_LIU_TX_AIS_Insert_Shift                                                                 15

/*--------------------------------------
BitField Name: LIU_TX_Enable
BitField Type: R/W
BitField Desc: LIU TX Enable - Set 1: LIU Enable - Set 0: LIU Disable and flush
FIFO
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_c_LIU_TX_Enable_Mask                                                                  cBit14
#define cAf6_upen_c_LIU_TX_Enable_Shift                                                                     14

/*--------------------------------------
BitField Name: LIU_RX_Enable
BitField Type: R/W
BitField Desc: LIU RX Enable - Set 1: LIU Enable - Set 0: LIU Disable and flush
FIFO
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_c_LIU_RX_Enable_Mask                                                                  cBit13
#define cAf6_upen_c_LIU_RX_Enable_Shift                                                                     13

/*--------------------------------------
BitField Name: LIU_LoopOut
BitField Type: R/W
BitField Desc: LIU Loop from RX side to TX side - Set 1: Loop Enable - Set 0:
Loop Disable
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_c_LIU_LoopOut_Mask                                                                    cBit12
#define cAf6_upen_c_LIU_LoopOut_Shift                                                                       12

/*--------------------------------------
BitField Name: LIU_LoopIn
BitField Type: R/W
BitField Desc: LIU Loop from TX side to RX side - Set 1: Loop Enable - Set 0:
Loop Disable
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_c_LIU_LoopIn_Mask                                                                     cBit11
#define cAf6_upen_c_LIU_LoopIn_Shift                                                                        11

/*--------------------------------------
BitField Name: LIU_TX_Invert
BitField Type: R/W
BitField Desc: LIU TX Clock Invert - Set 1: Clock Invert Enable - Set 0: Clock
Invert Disable
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_c_LIU_TX_Invert_Mask                                                                  cBit10
#define cAf6_upen_c_LIU_TX_Invert_Shift                                                                     10

/*--------------------------------------
BitField Name: LIU_RX_Sample
BitField Type: R/W
BitField Desc: LIU RX Sampling - Set 1: Negative Edge Detect - Set 0: Positive
Edge Detect
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_c_LIU_RX_Sample_Mask                                                                   cBit9
#define cAf6_upen_c_LIU_RX_Sample_Shift                                                                      9

/*--------------------------------------
BitField Name: LIU_TX_Mode
BitField Type: R/W
BitField Desc: LIU TX Mode - Set 4: EC1. - Set 3: E3. - Set 2: DS3. - Set 1: E1.
- Set 0: DS1
BitField Bits: [08:06]
--------------------------------------*/
#define cAf6_upen_c_LIU_TX_Mode_Mask                                                                   cBit8_6
#define cAf6_upen_c_LIU_TX_Mode_Shift                                                                        6

/*--------------------------------------
BitField Name: LIU_RX_Mode
BitField Type: R/W
BitField Desc: LIU RX Mode - Set 4: EC1. - Set 3: E3. - Set 2: DS3. - Set 1: E1.
- Set 0: DS1
BitField Bits: [05:03]
--------------------------------------*/
#define cAf6_upen_c_LIU_RX_Mode_Mask                                                                   cBit5_3
#define cAf6_upen_c_LIU_RX_Mode_Shift                                                                        3

/*--------------------------------------
BitField Name: LIU_Tx_Timing_Mode
BitField Type: R/W
BitField Desc: Tx LIU Timing mode - Set (3-7): Reserve. - Set 3: System timing
mode. - Set 2: External. - Set 1: Looptime. - Set 0: Recovery from ETH payload
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_c_LIU_Tx_Timing_Mode_Mask                                                            cBit2_0
#define cAf6_upen_c_LIU_Tx_Timing_Mode_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation DA bit47_32 Configuration
Reg Addr   : 0x0000_0008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit47_32

------------------------------------------------------------------------------*/
#define cAf6Reg_upen2                                                                               0x00000008

/*--------------------------------------
BitField Name: DA_bit47_32
BitField Type: R/W
BitField Desc: DA bit47_32
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen2_DA_bit47_32_Mask                                                                   cBit15_0
#define cAf6_upen2_DA_bit47_32_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation DA bit32_00 Configuration
Reg Addr   : 0x0000_000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit32_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen3                                                                               0x0000000C

/*--------------------------------------
BitField Name: DA_bit31_00
BitField Type: R/W
BitField Desc: DA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen3_DA_bit31_00_Mask                                                                   cBit31_0
#define cAf6_upen3_DA_bit31_00_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation SA bit47_32 Configuration
Reg Addr   : 0x0000_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the SA bit47_32

------------------------------------------------------------------------------*/
#define cAf6Reg_upen4                                                                               0x00000010

/*--------------------------------------
BitField Name: SA_bit47_32
BitField Type: R/W
BitField Desc: SA bit47_32
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen4_SA_bit47_32_Mask                                                                   cBit15_0
#define cAf6_upen4_SA_bit47_32_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation SA bit32_00 Configuration
Reg Addr   : 0x0000_0014
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the SA bit32_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen5                                                                               0x00000014

/*--------------------------------------
BitField Name: SA_bit31_00
BitField Type: R/W
BitField Desc: SA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen5_SA_bit31_00_Mask                                                                   cBit31_0
#define cAf6_upen5_SA_bit31_00_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation EtheType and EtherLength  Configuration
Reg Addr   : 0x0000_0018
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the EtheType and EtherLength

------------------------------------------------------------------------------*/
#define cAf6Reg_upen6                                                                               0x00000018

/*--------------------------------------
BitField Name: Ether_Type
BitField Type: R/W
BitField Desc: Ethernet Type
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen6_Ether_Type_Mask                                                                   cBit31_16
#define cAf6_upen6_Ether_Type_Shift                                                                         16

/*--------------------------------------
BitField Name: Ether_Len
BitField Type: R/W
BitField Desc: Ethernet Length - DS1/E1: number of DS1/E1 * 2 bytes + 2 (seq
num) - DS3/E3/EC-1: number of DS3/E3/EC-1 * 6 bytes + 2 - Control frames: length
of control frame payload
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen6_Ether_Len_Mask                                                                     cBit15_0
#define cAf6_upen6_Ether_Len_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation SA bit47_32 Configuration
Reg Addr   : 0x0000_001C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure Sub_Type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_subtype                                                                        0x0000001C

/*--------------------------------------
BitField Name: Ether_Subtype
BitField Type: R/W
BitField Desc: Ethernet SubType - 0x0           : DS1/E1 Frames - 0x1-0xF
: reserved - 0x10               : DS3/E3/EC-1 Frames - 0x11-0x7F        :
reserved - 0x80-0xFF  : control frames
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_subtype_Ether_Subtype_Mask                                                           cBit7_0
#define cAf6_upen_subtype_Ether_Subtype_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Overrun Counter
Reg Addr   : 0x0000_0800 - 0x0000_094C
Reg Formula: 0x0000_0800 + liuid*4
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
TX LIU overrun counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_overruncnt                                                                     0x00000800

/*--------------------------------------
BitField Name: txliuoverrun_cnt
BitField Type: R/W
BitField Desc: Tx_LUI_overrun_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_overruncnt_txliuoverrun_cnt_Mask                                                    cBit31_0
#define cAf6_upen_overruncnt_txliuoverrun_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Underrun Counter
Reg Addr   : 0x0000_0A00 - 0x0000_0B4C
Reg Formula: 0x0000_0A00 + liuid*4
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
TX LIU Underrun counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_underruncnt                                                                    0x00000A00

/*--------------------------------------
BitField Name: txliuunderrun_cnt
BitField Type: R/W
BitField Desc: Tx_LUI_underrun_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_underruncnt_txliuunderrun_cnt_Mask                                                  cBit31_0
#define cAf6_upen_underruncnt_txliuunderrun_cnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (Arrive to Ciena) Packet Counter
Reg Addr   : 0x0000_0080
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Packet Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_cnt00_pen                                                                         0x00000080

/*--------------------------------------
BitField Name: axispktcnt
BitField Type: R/W
BitField Desc: axis_packect_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_cnt00_pen_axispktcnt_Mask                                                              cBit31_0
#define cAf6_x_cnt00_pen_axispktcnt_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (Arrive to Ciena) Byte Counter
Reg Addr   : 0x0000_0084
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Byte Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_cnt01_pen                                                                         0x00000084

/*--------------------------------------
BitField Name: axisbytecnt
BitField Type: R/W
BitField Desc: axis_byte_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_cnt01_pen_axisbytecnt_Mask                                                             cBit31_0
#define cAf6_x_cnt01_pen_axisbytecnt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (Ciena to Arrive) Packet Counter
Reg Addr   : 0x0000_0088
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Packet Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_cnt02_pen                                                                         0x00000088

/*--------------------------------------
BitField Name: axispktcnt
BitField Type: R/W
BitField Desc: axis_packect_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_cnt02_pen_axispktcnt_Mask                                                              cBit31_0
#define cAf6_x_cnt02_pen_axispktcnt_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (Ciena to Arrive) Byte Counter
Reg Addr   : 0x0000_008C
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Byte Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_cnt03_pen                                                                         0x0000008C

/*--------------------------------------
BitField Name: axisbytecnt
BitField Type: R/W
BitField Desc: axis_byte_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_cnt03_pen_axisbytecnt_Mask                                                             cBit31_0
#define cAf6_x_cnt03_pen_axisbytecnt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DE1 DA Error  Counter
Reg Addr   : 0x0000_0090
Reg Formula: 
    Where  : 
Reg Desc   : 
DE1 DA Error Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_cnt04_pen                                                                         0x00000090

/*--------------------------------------
BitField Name: de1daerr_cnt
BitField Type: R/W
BitField Desc: de1_da_err_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_cnt04_pen_de1daerr_cnt_Mask                                                            cBit31_0
#define cAf6_x_cnt04_pen_de1daerr_cnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : DE1 SA Error  Counter
Reg Addr   : 0x0000_0094
Reg Formula: 
    Where  : 
Reg Desc   : 
DE1 SA Error Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_de1saerr_pen                                                                        0x00000094

/*--------------------------------------
BitField Name: de1saerr_cnt
BitField Type: R/W
BitField Desc: de1_sa_err_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_de1saerr_pen_de1saerr_cnt_Mask                                                           cBit31_0
#define cAf6_de1saerr_pen_de1saerr_cnt_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DE3/EC1 DA Error  Counter
Reg Addr   : 0x0000_0098
Reg Formula: 
    Where  : 
Reg Desc   : 
DE3/EC1 DA Error Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_cnt06_pen                                                                         0x00000098

/*--------------------------------------
BitField Name: de3daerr_cnt
BitField Type: R/W
BitField Desc: de3_da_err_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_cnt06_pen_de3daerr_cnt_Mask                                                            cBit31_0
#define cAf6_x_cnt06_pen_de3daerr_cnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : DE3/EC1 SA Error  Counter
Reg Addr   : 0x0000_009C
Reg Formula: 
    Where  : 
Reg Desc   : 
DE3/EC1 SA Error Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_de3saerr_pen                                                                        0x0000009C

/*--------------------------------------
BitField Name: de3saerr_cnt
BitField Type: R/W
BitField Desc: de3sa_err_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_de3saerr_pen_de3saerr_cnt_Mask                                                           cBit31_0
#define cAf6_de3saerr_pen_de3saerr_cnt_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DE3 LCV (Line Code Violation) Counter
Reg Addr   : 0x0000_00A0-0x0000_00FC
Reg Formula: 0x0000_00A0 + de3liuid*4
    Where  : 
           + $de3liuid(0-23):
Reg Desc   : 
DE3 LCV (Line Code Violation) Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_x_lcvcnt_pen                                                                        0x000000A0

/*--------------------------------------
BitField Name: de3lcvcnt
BitField Type: R/W
BitField Desc: de3_lcv_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_x_lcvcnt_pen_de3lcvcnt_Mask                                                              cBit31_0
#define cAf6_x_lcvcnt_pen_de3lcvcnt_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RX LIU FIFO Full channel ID 0-31 sticky
Reg Addr   : 0x0000_0158
Reg Formula: 
    Where  : 
Reg Desc   : 
RX LIU FIFO full sticky. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk0_pen                                                                            0x00000158

/*--------------------------------------
BitField Name: rxliufull
BitField Type: R/W
BitField Desc: Rx_Liu_Fifo_Full - Bit#0 for LIU ID#0 - Bit#1 for LIU ID#1 -
Bit#31 for LIU ID#31 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk0_pen_rxliufull_Mask                                                                  cBit31_0
#define cAf6_stk0_pen_rxliufull_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RX LIU FIFO Full channel ID 32-63 sticky
Reg Addr   : 0x0000_015C
Reg Formula: 
    Where  : 
Reg Desc   : 
RX LIU FIFO full sticky. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk1_pen                                                                            0x0000015C

/*--------------------------------------
BitField Name: rxliufull
BitField Type: R/W
BitField Desc: Rx_Liu_Fifo_Full - Bit#0 for LIU ID#32 - Bit#1 for LIU ID#33
.................. - Bit#31 for LIU ID#63 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk1_pen_rxliufull_Mask                                                                  cBit31_0
#define cAf6_stk1_pen_rxliufull_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RX LIU FIFO Full channel ID 64-83 sticky
Reg Addr   : 0x0000_0160
Reg Formula: 
    Where  : 
Reg Desc   : 
RX LIU FIFO full sticky. Write 0x000F_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk2_pen                                                                            0x00000160

/*--------------------------------------
BitField Name: rxliufull
BitField Type: R/W
BitField Desc: Rx_Liu_Fifo_Full - Bit#0 for LIU ID#64 - Bit#1 for LIU ID#65
.................. - Bit#19 for LIU ID#83 - Set 1: shown FIFO Full
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_stk2_pen_rxliufull_Mask                                                                  cBit19_0
#define cAf6_stk2_pen_rxliufull_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Overrun channel ID 0-31 sticky
Reg Addr   : 0x0000_0164
Reg Formula: 
    Where  : 
Reg Desc   : 
TX LIU FIFO overrun sticky. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk3_pen                                                                            0x00000164

/*--------------------------------------
BitField Name: txliuover
BitField Type: R/W
BitField Desc: Tx_Liu_Fifo_over - Bit#0 for LIU ID#0 - Bit#1 for LIU ID#1 -
Bit#31 for LIU ID#31 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk3_pen_txliuover_Mask                                                                  cBit31_0
#define cAf6_stk3_pen_txliuover_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Overrun channel ID 32-63 sticky
Reg Addr   : 0x0000_0168
Reg Formula: 
    Where  : 
Reg Desc   : 
TX LIU FIFO overrun sticky. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk4_pen                                                                            0x00000168

/*--------------------------------------
BitField Name: txliuover
BitField Type: R/W
BitField Desc: Tx_Liu_Fifo_over - Bit#0 for LIU ID#32 - Bit#1 for LIU ID#33
.................. - Bit#31 for LIU ID#63 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk4_pen_txliuover_Mask                                                                  cBit31_0
#define cAf6_stk4_pen_txliuover_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Overrun channel ID 64-83 sticky
Reg Addr   : 0x0000_016C
Reg Formula: 
    Where  : 
Reg Desc   : 
TX LIU FIFO overrun sticky. Write 0x000F_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk5_pen                                                                            0x0000016C

/*--------------------------------------
BitField Name: txliuover
BitField Type: R/W
BitField Desc: Tx_Liu_Fifo_over - Bit#0 for LIU ID#64 - Bit#1 for LIU ID#65
.................. - Bit#19 for LIU ID#83 - Set 1: shown FIFO Full
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_stk5_pen_txliuover_Mask                                                                  cBit19_0
#define cAf6_stk5_pen_txliuover_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Underrun channel ID 0-31 sticky
Reg Addr   : 0x0000_0170
Reg Formula: 
    Where  : 
Reg Desc   : 
TX LIU FIFO Underrun sticky. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk6_pen                                                                            0x00000170

/*--------------------------------------
BitField Name: txliuunder
BitField Type: R/W
BitField Desc: Tx_Liu_Fifo_under - Bit#0 for LIU ID#0 - Bit#1 for LIU ID#1 -
Bit#31 for LIU ID#31 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk6_pen_txliuunder_Mask                                                                 cBit31_0
#define cAf6_stk6_pen_txliuunder_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Underrun channel ID 32-63 sticky
Reg Addr   : 0x0000_0174
Reg Formula: 
    Where  : 
Reg Desc   : 
TX LIU FIFO Underrun sticky. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk7_pen                                                                            0x00000174

/*--------------------------------------
BitField Name: txliuunder
BitField Type: R/W
BitField Desc: Tx_Liu_Fifo_under - Bit#0 for LIU ID#32 - Bit#1 for LIU ID#33
.................. - Bit#31 for LIU ID#63 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk7_pen_txliuunder_Mask                                                                 cBit31_0
#define cAf6_stk7_pen_txliuunder_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Underrun channel ID 64-83 sticky
Reg Addr   : 0x0000_0178
Reg Formula: 
    Where  : 
Reg Desc   : 
TX LIU FIFO Underrun sticky. Write 0x000F_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk8_pen                                                                            0x00000178

/*--------------------------------------
BitField Name: txliuunder
BitField Type: R/W
BitField Desc: Tx_Liu_Fifo_under - Bit#0 for LIU ID#64 - Bit#1 for LIU ID#65
.................. - Bit#19 for LIU ID#83 - Set 1: shown FIFO Full
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_stk8_pen_txliuunder_Mask                                                                 cBit19_0
#define cAf6_stk8_pen_txliuunder_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Underrun channel ID 64-83 Sticky
Reg Addr   : 0x0000_017C
Reg Formula: 
    Where  : 
Reg Desc   : 
Global Sticky. Write 0x000F_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_stk9_pen                                                                            0x0000017C

/*--------------------------------------
BitField Name: dec_lost_tdm_dat
BitField Type: R/W
BitField Desc: DEC TDM lost data
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk9_pen_dec_lost_tdm_dat_Mask                                                              cBit7
#define cAf6_stk9_pen_dec_lost_tdm_dat_Shift                                                                 7

/*--------------------------------------
BitField Name: dec_buf_ful
BitField Type: R/W
BitField Desc: DEC Buffer Full
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk9_pen_dec_buf_ful_Mask                                                                   cBit6
#define cAf6_stk9_pen_dec_buf_ful_Shift                                                                      6

/*--------------------------------------
BitField Name: dec_de3_da_err
BitField Type: R/W
BitField Desc: DEC DE3 DA error
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk9_pen_dec_de3_da_err_Mask                                                                cBit5
#define cAf6_stk9_pen_dec_de3_da_err_Shift                                                                   5

/*--------------------------------------
BitField Name: dec_de3_sa_err
BitField Type: R/W
BitField Desc: DEC DE3 SA error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk9_pen_dec_de3_sa_err_Mask                                                                cBit4
#define cAf6_stk9_pen_dec_de3_sa_err_Shift                                                                   4

/*--------------------------------------
BitField Name: dec_de1_da_err
BitField Type: R/W
BitField Desc: DEC DE1 DA error
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk9_pen_dec_de1_da_err_Mask                                                                cBit3
#define cAf6_stk9_pen_dec_de1_da_err_Shift                                                                   3

/*--------------------------------------
BitField Name: dec_de1_sa_err
BitField Type: R/W
BitField Desc: DEC DE1 SA error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk9_pen_dec_de1_sa_err_Mask                                                                cBit2
#define cAf6_stk9_pen_dec_de1_sa_err_Shift                                                                   2

/*--------------------------------------
BitField Name: dec_de3_seq_err
BitField Type: R/W
BitField Desc: DEC DE3 Sequence Number error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk9_pen_dec_de3_seq_err_Mask                                                               cBit1
#define cAf6_stk9_pen_dec_de3_seq_err_Shift                                                                  1

/*--------------------------------------
BitField Name: dec_de1_seq_err
BitField Type: R/W
BitField Desc: DEC DE1 Sequence Number error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk9_pen_dec_de1_seq_err_Mask                                                               cBit0
#define cAf6_stk9_pen_dec_de1_seq_err_Shift                                                                  0

#endif /* _AF6_REG_AF6CNC0011_RD_DIM_H_ */

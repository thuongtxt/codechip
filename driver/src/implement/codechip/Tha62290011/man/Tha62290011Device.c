/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha62290011Device.c
 *
 * Created Date: Nov 9, 2016 
 *
 * Description : Tha62290011 Device implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha61290011/man/Tha61290011Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha62290011Device
    {
    tTha61290011Device super;
    }tTha62290011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tTha60210031DeviceMethods m_Tha60210031DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60210031DeviceMethods *m_Tha60210031DeviceMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId  == cAtModulePdh)    return (AtModule)Tha62290011ModulePdhNew(self);
    if (moduleId  == cAtModuleEth)    return (AtModule)Tha62290011ModuleEthNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool DdrCalibCurrentStatusIsDone(Tha60210031Device self, uint32 regVal, uint32 mask)
    {
    return Tha60210031DeviceDdrCalibCurrentStatusIsDone(self, regVal, mask);
    }

static void OverrideTha60210031Device(AtDevice self)
    {
    Tha60210031Device device = (Tha60210031Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031DeviceOverride, m_Tha60210031DeviceMethods, sizeof(m_Tha60210031DeviceOverride));

        mMethodOverride(m_Tha60210031DeviceOverride, DdrCalibCurrentStatusIsDone);
        }

    mMethodsSet(device, &m_Tha60210031DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideTha60210031Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha62290011Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61290011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha62290011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }


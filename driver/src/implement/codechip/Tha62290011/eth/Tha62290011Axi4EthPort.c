/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha62290011Axi4EthPort.c
 *
 * Created Date: Nov 22, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPortReg.h"
#include "../../Tha60290011/eth/Tha60290011ClsInterfaceEthPortInternal.h"
#include "../pdh/Tha62290011ModulePdh.h"
#include "../pdh/Tha62290011DimReg.h"
#include "../../../default/eth/ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha62290011Axi4EthPort *) self)

/*--------------------------- Macros -----------------------------------------*/
#define mPdhModule(_port) ((ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)_port), cAtModulePdh))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha62290011Axi4EthPort
    {
    tTha60290011ClsInterfaceEthPort super;
    }tTha62290011Axi4EthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;
static tAtEthPortMethods                m_AtEthPortOverride;
static tTha60290011ClsInterfaceEthPortMethods   m_Tha60290011ClsInterfaceEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods          *m_AtChannelMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    return Tha62290011ModulePdhDimSourceMacAddressSet(mPdhModule(self), address);
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    return Tha62290011ModulePdhDimSourceMacAddressSet(mPdhModule(self), address);
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    return Tha62290011ModulePdhDimSourceMacAddressSet(mPdhModule(self), address);
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    return Tha62290011ModulePdhDimSourceMacAddressSet(mPdhModule(self), address);
    }

static eAtRet EthTypeSet(ThaEthPort self, uint16 type)
    {
    return Tha62290011ModulePdhDimEthTypeSet(mPdhModule(self), type);
    }

static eAtRet Debug(AtChannel self)
    {
    return Tha62290011ModulePdhDimDebugAxi4StreamShow(mPdhModule(self));
    }

static eAtRet DefaultSet(AtChannel self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdGlbCtrl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    mChannelHwWrite(self, regAddr, 0x253, cAtModuleEth);

    regAddr = cThaRegIPEthernetTripleSpdGlbIntfCtrl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    mChannelHwWrite(self, regAddr, 0x82, cAtModuleEth);

    return cAtOk;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290011ClsInterfaceEthPort(AtEthPort self)
    {
    Tha60290011ClsInterfaceEthPort ethPort = (Tha60290011ClsInterfaceEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ClsInterfaceEthPortOverride, mMethodsGet(ethPort), sizeof(m_Tha60290011ClsInterfaceEthPortOverride));

        mMethodOverride(m_Tha60290011ClsInterfaceEthPortOverride, EthTypeSet);
        }

    mMethodsSet(ethPort, &m_Tha60290011ClsInterfaceEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290011ClsInterfaceEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha62290011Axi4EthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ClsInterfaceEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha62290011Axi4EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha61150011ModulePdh.c
 *
 * Created Date: Apr 20, 2015
 *
 * Description : PDH module of 61150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60150011/pdh/Tha60150011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61150011ModulePdh
    {
    tTha60150011ModulePdh super;
    }tTha61150011ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdhMethods m_ThaModulePdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePrbs PrbsModule(ThaModulePdh self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePrbs);
    }

static AtPrbsEngine De1PrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    return AtModulePrbsDe1PrbsEngineCreate(PrbsModule(self), 0, (AtPdhDe1)de1);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(ThaModulePdh self, AtPdhNxDS0 nxds0)
    {
    return AtModulePrbsNxDs0PrbsEngineCreate(PrbsModule(self), 0, (AtPdhNxDS0)nxds0);
    }

static AtPrbsEngine De3PrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3)
    {
    return AtModulePrbsDe3PrbsEngineCreate(PrbsModule(self), 0, (AtPdhDe3)de3);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61150011ModulePdh);
    }

static void OverrideThaModulePdh(AtModule self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, De1PrbsEngineCreate);
        mMethodOverride(m_ThaModulePdhOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3PrbsEngineCreate);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePdh(self);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override((AtModule)self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha61150011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

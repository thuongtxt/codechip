/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha61150011Device.c
 *
 * Created Date: Mar 27, 2015
 *
 * Description : 61150011 device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60150011/man/Tha60150011DeviceInternal.h"
#include "../../Tha61031031/prbs/Tha61031031ModulePrbs.h"
#include "../sdh/Tha61150011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61150011Device * Tha61150011Device;

typedef struct tTha61150011Device
    {
    tTha60150011Device super;

    }tTha61150011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tTha60150011DeviceMethods  m_Tha60150011DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId  == cAtModulePrbs) return (AtModule)Tha61150011ModulePrbsNew(self);
    if (moduleId  == cAtModuleEth)  return (AtModule)Tha61150011ModuleEthNew(self);
    if (moduleId == cAtModulePdh)   return (AtModule)Tha61150011ModulePdhNew(self);
    if (moduleId == cAtModuleSdh)   return (AtModule)Tha61150011ModuleSdhNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModulePrbs};
    AtUnused(self);

    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    AtUnused(self);
    switch (moduleValue)
        {
        case cAtModulePrbs:
        	return cAtTrue;

        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static eBool NewStartupSequenceIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha6015011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, NewStartupSequenceIsSupported);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }
    
static eAtRet SdhSerdesReset(AtDevice self)
    {
    /* This is for testing on board K7 and it only has one port */
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh), 0);
    AtSerdesController serdesController = AtSdhLineSerdesController(line);
    if (serdesController)
        return AtSerdesControllerReset(serdesController);
    return cAtOk;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!ThaDeviceIsEp((ThaDevice)self))
        return cAtOk;

    return SdhSerdesReset(self);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, Init);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideTha6015011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61150011Device);
    }

static AtDevice Tha61150011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha61150011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha61150011DeviceObjectInit(newDevice, driver, productCode);
    }


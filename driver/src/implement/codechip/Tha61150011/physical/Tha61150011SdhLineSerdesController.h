/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha61150011SdhLineSerdesController.h
 * 
 * Created Date: Jul 24, 2015
 *
 * Description : 61150011 SDH serdes controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA61150011_PHYSICAL_THA61150011SDHLINESERDESCONTROLLER_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA61150011_PHYSICAL_THA61150011SDHLINESERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/physical/Tha60150011SdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha61150011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA61150011_PHYSICAL_THA61150011SDHLINESERDESCONTROLLER_H_ */


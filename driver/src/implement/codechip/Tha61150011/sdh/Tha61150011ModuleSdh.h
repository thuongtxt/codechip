/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha61150011ModuleSdh.h
 * 
 * Created Date: Jul 24, 2015
 *
 * Description : 61150011 module sdh
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA61150011_SDH_THA61150011MODULESDH_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA61150011_SDH_THA61150011MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/sdh/Tha60150011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61150011ModuleSdh
    {
    tTha60150011ModuleSdh super;
    }tTha61150011ModuleSdh;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha61150011ModuleSdhNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA61150011_SDH_THA61150011MODULESDH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha61150011PrbsReg.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : PRBS register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011PRBSREG_H_
#define _THA60150011PRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* PW PRBS engine enable control */
#define cThaRegPrbsEnable           0x246400

/* PW PRBS engine status */
#define cThaRegPrbsStatus           0x246800

#define cThaRegPrbsStatusErrorMask  cBit17_16
#define cThaRegPrbsStatusErrorShift 16

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011PRBSREG_H_ */


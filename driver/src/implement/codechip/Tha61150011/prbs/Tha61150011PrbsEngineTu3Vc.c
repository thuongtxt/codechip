/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61150011PrbsEngineTu3Vc.c
 *
 * Created Date: Jul 24, 2015
 *
 * Description : TU3 VC engine of 61150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha61031031/prbs/Tha61031031PrbsEngine.h"
#include "Tha61150011PrbsEngine.h"
#include "AtSdhChannel.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61150011PrbsEngineTu3Vc
    {
    tTha61031031PrbsEngineTu3Vc super;
    }tTha61150011PrbsEngineTu3Vc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha61031031PrbsEngineTu3VcMethods m_Tha61031031PrbsEngineTu3VcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine De3PrbsEngineCreate(AtPrbsEngine self)
    {
    AtSdhChannel vc3 = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    if (AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3) != cAtOk)
        return NULL;

    return Tha61150011PrbsEngineDe3New((AtPdhDe3)AtSdhChannelMapChannelGet(vc3));
    }

static void OverrideTha61031031PrbsEngineTu3Vc(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTu3Vc engine = (Tha61031031PrbsEngineTu3Vc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTu3VcOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTu3VcOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTu3VcOverride, De3PrbsEngineCreate);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTu3VcOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha61031031PrbsEngineTu3Vc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61150011PrbsEngineTu3Vc);
    }

static AtPrbsEngine Tha61150011PrbsEngineTu3VcObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTu3VcObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61150011PrbsEngineTu3VcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61150011PrbsEngineTu3VcObjectInit(newEngine, vc);
    }

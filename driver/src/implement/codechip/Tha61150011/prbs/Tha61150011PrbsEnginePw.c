/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61150011PrbsEngine.c
 *
 * Created Date: Mar 26, 2015
 *
 * Description : Module PRBS of 61150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "Tha61150011PrbsReg.h"
#include "Tha61150011PrbsEngine.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha61031031/prbs/Tha61031031PrbsEngine.h"
#include "AtPw.h"
#include "AtPdhChannel.h"
#include "AtPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha61150011PrbsEnginePw)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha61150011PrbsEnginePwMethods m_methods;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61150011PrbsEnginePw);
    }

static AtSdhChannel SdhChannelFromPdhChannel(AtPdhChannel pdhChannel)
    {
    AtPdhChannel parentChannel;
    AtSdhChannel sdhChannel;

    parentChannel = AtPdhChannelParentChannelGet(pdhChannel);
    if (parentChannel == NULL)
        return NULL;

    sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)parentChannel);
    if (sdhChannel)
        return sdhChannel;

    return SdhChannelFromPdhChannel(parentChannel);
    }

static uint8 SliceId(AtPrbsEngine self)
    {
    uint8 slice, sts;
    AtPw pw = (AtPw)AtPrbsEngineChannelGet(self);
    AtSdhChannel sdhChannel = NULL;

    if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
        {
        AtPdhChannel cirCuit = (AtPdhChannel)AtPwBoundCircuitGet(pw);
        sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(cirCuit);
        if (sdhChannel == NULL)
            sdhChannel = SdhChannelFromPdhChannel(cirCuit);
        }

    else if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)AtPwBoundCircuitGet(pw)));
    else if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);

    if (sdhChannel == NULL)
        return cBit7_0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModulePrbs, &slice, &sts);
    return slice;
    }

static uint32 DefaultOffset(AtPrbsEngine self)
    {
    uint32 sliceOffset;
    AtPw pw = (AtPw)AtPrbsEngineChannelGet(self);
    uint8 slice = SliceId(self);

    if (slice == cBit7_0)
        return 0;

    sliceOffset = mMethodsGet(mThis(self))->PrbsSliceOffset(mThis(self), slice);
    return sliceOffset + AtChannelIdGet((AtChannel)pw);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 address = mMethodsGet(mThis(self))->PrbsEnableRegister(mThis(self));
    AtPrbsEngineWrite(self, address + DefaultOffset(self), enable ? 1 : 0, cThaModulePwe);
    mThis(self)->isEnabled = enable;
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    /* Hardware has an issue when reading enable register will affect to datapath monitoring.
     * So software need to save enable status to database */
    return mThis(self)->isEnabled;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtUnused(self);
    return force ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PrbsEngineIsRunning(AtPrbsEngine self)
    {
    uint32 checkTime;
    uint32 address = mMethodsGet(mThis(self))->PrbsStatusRegister(mThis(self)) + DefaultOffset(self);
    uint32 firstTimeVal = AtPrbsEngineRead(self, address, cThaModulePwe);

    for (checkTime = 0; checkTime < 10; checkTime++)
        {
        uint32 secondTimeVal;

        AtOsalUSleep(100);
        secondTimeVal = AtPrbsEngineRead(self, address, cThaModulePwe);

        if (firstTimeVal != secondTimeVal)
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 address = mMethodsGet(mThis(self))->PrbsStatusRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, address + DefaultOffset(self), cThaModulePwe);

    if (!PrbsEngineIsRunning(self))
        return cAtPrbsEngineAlarmTypeError;

    return (mRegField(regVal, cThaRegPrbsStatusError) != 1) ? cAtPrbsEngineAlarmTypeError : 0x0;
    }

static uint32 PrbsEnableRegister(Tha61150011PrbsEnginePw self)
    {
    AtUnused(self);
    return cThaRegPrbsEnable;
    }

static uint32 PrbsStatusRegister(Tha61150011PrbsEnginePw self)
    {
    AtUnused(self);
    return cThaRegPrbsStatus;
    }

static uint32 PrbsSliceOffset(Tha61150011PrbsEnginePw self, uint8 slice)
    {
    AtUnused(self);
    return slice * 0x1000UL;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static void MethodsInit(Tha61150011PrbsEnginePw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PrbsEnableRegister);
        mMethodOverride(m_methods, PrbsStatusRegister);
        mMethodOverride(m_methods, PrbsSliceOffset);
        }

    mMethodsSet(self, &m_methods);
    }

AtPrbsEngine Tha61150011PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEnginePwObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    mThis(self)->isEnabled = cAtFalse;
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61150011PrbsEnginePwNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61150011PrbsEnginePwObjectInit(newEngine, pw);
    }

uint32 Tha61150011PrbsEnginePwDefaultOffset(AtPrbsEngine self)
    {
    return (self) ? DefaultOffset(self) : cInvalidUint32;
    }

uint8 Tha61150011PrbsEnginePwSliceId(AtPrbsEngine self)
    {
    return (uint8)((self) ? SliceId(self) : cBit7_0);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61150011PrbsEngine.h
 *
 * Created Date: Apr 21, 2015
 *
 * Description : PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA61150011PRBSENGINE_H_
#define _THA61150011PRBSENGINE_H_
/*--------------------------- Include files ----------------------------------*/
#include "../../Tha61031031/prbs/Tha61031031PrbsEngine.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha61150011PrbsEnginePw * Tha61150011PrbsEnginePw;
typedef struct tTha61150011PrbsEnginePwMethods
    {
    uint32 (*PrbsEnableRegister)(Tha61150011PrbsEnginePw self);
    uint32 (*PrbsStatusRegister)(Tha61150011PrbsEnginePw self);
    uint32 (*PrbsSliceOffset)(Tha61150011PrbsEnginePw self, uint8 slice);
    }tTha61150011PrbsEnginePwMethods;

typedef struct tTha61150011PrbsEnginePw
    {
    tTha61031031PrbsEnginePw super;
    const tTha61150011PrbsEnginePwMethods *methods;

    /* Private data */
    eBool isEnabled;
    }tTha61150011PrbsEnginePw;

typedef struct tTha61150011PrbsEngineDe3
    {
    tTha61031031PrbsEngineDe3 super;
    }tTha61150011PrbsEngineDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtPrbsEngine Tha61150011PrbsEngineDe3New(AtPdhDe3 de3);
AtPrbsEngine Tha61150011PrbsEnginePwNew(AtPw pw);
AtPrbsEngine Tha61150011PrbsEngineTu3VcNew(AtSdhChannel vc);
AtPrbsEngine Tha61210031PrbsEnginePwNew(AtPw pw);

AtPrbsEngine Tha61150011PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3);
AtPrbsEngine Tha61150011PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw);
uint32 Tha61150011PrbsEnginePwDefaultOffset(AtPrbsEngine self);
uint8 Tha61150011PrbsEnginePwSliceId(AtPrbsEngine self);
#endif /* _THA61150011PRBSENGINE_H_ */

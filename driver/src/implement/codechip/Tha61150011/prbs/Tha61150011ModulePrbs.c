/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210021PrbsEngineDe1.c
 *
 * Created Date: Mar 26, 2015
 *
 * Description : PRBS implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha61031031/prbs/Tha61031031PrbsEngine.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha61150011PrbsReg.h"
#include "Tha61150011PrbsEngine.h"
#include "Tha61150011ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods              m_AtModuleOverride;
static tAtModulePrbsMethods          m_AtModulePrbsOverride;
static tTha61031031ModulePrbsMethods m_Tha61031031ModulePrbsOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods = NULL;
static const tAtModulePrbsMethods *m_AtModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtModule self)
	{
    return AtModuleDeviceGet(self);
	}

static void DefaultSet(AtModule self)
    {
	const uint8 cNumSlice = 2;
	uint32 numPw = AtModulePwMaxPwsGet((AtModulePw)AtDeviceModuleGet(Device(self), cAtModulePw));
	uint32 pw_i;
	uint8 slice_i;
	uint32 address;

	/* This product supports two set of prbs registers according two OCN slice */
	for (slice_i = 0; slice_i < cNumSlice; slice_i++)
		{
		for (pw_i = 0; pw_i < numPw; pw_i++)
			{
			address  = cThaRegPrbsEnable + pw_i + slice_i * 0x1000UL;
			mModuleHwWrite(self, address, 0x0);
			}
		}
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha61150011PrbsEngineDe3New(de3);
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
            return Tha61150011PrbsEngineTu3VcNew(sdhVc);
        }

    return m_AtModulePrbsMethods->SdhVcPrbsEngineCreate(self, engineId, sdhVc);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(Tha61031031ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return Tha61150011PrbsEnginePwNew(pw);
    }

static eBool AllChannelsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DefaultJitterBuffer(Tha61031031ModulePrbs self)
    {
    AtUnused(self);
    return 64000;/* 64ms */
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, AllChannelsSupported);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha61031031ModulePrbs(AtModulePrbs self)
    {
    Tha61031031ModulePrbs prbsModule = (Tha61031031ModulePrbs)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031ModulePrbsOverride, mMethodsGet(prbsModule), sizeof(m_Tha61031031ModulePrbsOverride));

        mMethodOverride(m_Tha61031031ModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_Tha61031031ModulePrbsOverride, DefaultJitterBuffer);
        }

    mMethodsSet(prbsModule, &m_Tha61031031ModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideTha61031031ModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61150011ModulePrbs);
    }

AtModulePrbs Tha61150011ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha61150011ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha61150011ModulePrbsObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61150011PrbsEngineDe3.c
 *
 * Created Date: Apr 21, 2015
 *
 * Description : PRBS DE3 of product 61150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61150011PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha61031031PrbsEngineTdmPwMethods  m_Tha61031031PrbsEngineTdmPwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DefaultPayloadSizeSet(Tha61031031PrbsEngineTdmPw self, AtPw pw)
    {
    /* NOTE: Current FPGA only support configure payload size smaller 1000 */
    AtUnused(self);
    return AtPwPayloadSizeSet(pw, 700);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61150011PrbsEngineDe3);
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, DefaultPayloadSizeSet);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

AtPrbsEngine Tha61150011PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineDe3ObjectInit(self, de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61150011PrbsEngineDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha61150011PrbsEngineDe3ObjectInit(newEngine, de3);
    }

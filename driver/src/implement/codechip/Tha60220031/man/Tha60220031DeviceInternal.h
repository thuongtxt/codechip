/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60220031DeviceInternal.h
 * 
 * Created Date: Apr 3, 2015
 *
 * Description : Tha60220031 device declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60220031DEVICEINTERNAL_H_
#define _THA60220031DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60220031Device
    {
    tThaPdhPwProductDevice super;
    }tTha60220031Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60220031DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60220031DEVICEINTERNAL_H_ */


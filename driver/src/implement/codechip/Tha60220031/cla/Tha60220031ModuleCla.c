/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60220031ModuleCla.c
 *
 * Created Date: Jun 15, 2015
 *
 * Description : Tha60220031 module CLA implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/cla/ThaPdhPwProductModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPwProductModuleClaMethods     m_ThaStmPwProductModuleClaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eThaPwLookupMode DefaultPwLookupModeGet(ThaStmPwProductModuleCla self)
    {
    AtUnused(self);
    return cThaPwLookupModePsn;
    }

static void OverrideThaStmPwProductModuleCla(AtModule self)
    {
    ThaStmPwProductModuleCla claModule = (ThaStmPwProductModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaStmPwProductModuleClaOverride));

        mMethodOverride(m_ThaStmPwProductModuleClaOverride, DefaultPwLookupModeGet);
        }

    mMethodsSet(claModule, &m_ThaStmPwProductModuleClaOverride);
    }


static void Override(AtModule self)
    {
    OverrideThaStmPwProductModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductModuleCla);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60220031ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETHERNET
 * 
 * File        : Tha60220031EthPortSerdesController.h
 * 
 * Created Date: Jul 27, 2015
 *
 * Description : Ethernet port controller of 60220031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60220031_PHYSICAL_THA60220031ETHPORTSERDESCONTROLLER_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60220031_PHYSICAL_THA60220031ETHPORTSERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60220031EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60220031_PHYSICAL_THA60220031ETHPORTSERDESCONTROLLER_H_ */


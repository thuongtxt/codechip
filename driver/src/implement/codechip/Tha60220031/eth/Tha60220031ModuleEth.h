/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETHERNET
 * 
 * File        : Tha60220031ModuleEth.h
 * 
 * Created Date: Jul 27, 2015
 *
 * Description : Ethernet port of 60220031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60220031_ETH_THA60220031MODULEETH_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60220031_ETH_THA60220031MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60220031EthPortNew(uint8 portId, AtModuleEth module);
AtModuleEth Tha60220031ModuleEthNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60220031_ETH_THA60220031MODULEETH_H_ */


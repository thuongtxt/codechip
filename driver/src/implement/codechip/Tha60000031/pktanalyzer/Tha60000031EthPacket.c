/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60000031EthPacket.c
 *
 * Created Date: Jul 10, 2014
 *
 * Description : Packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPacket.h"

/*--------------------------- Define -----------------------------------------*/
#define cMacLength  6
#define cZteEthTypeLength 2
#define cZteTagLength 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60000031EthPacket
    {
    tThaStmPwProductEthPacket super;
    }tTha60000031EthPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPacketMethods m_AtEthPacketOverride;
static tThaStmPwProductEthPacketMethods m_ThaStmPwProductEthPacketOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtVlanTag* FirstVlanAtOffset(AtEthPacket self, uint32 offset, tAtVlanTag *vlanTag)
    {
    uint32 length, remainingBytes;
    uint8* buffer = AtPacketDataBuffer((AtPacket)self, &length);

    remainingBytes = length - offset;
    if (remainingBytes < cZteEthTypeLength)
        return NULL;

    vlanTag->vlanId = buffer[offset];
    vlanTag->vlanId = (uint16)(vlanTag->vlanId << 8);
    vlanTag->vlanId = (uint16)(vlanTag->vlanId | buffer[offset + 1]);

    return vlanTag;
    }

static tAtVlanTag* SecondVlanAtOffset(AtEthPacket self, uint32 offset, tAtVlanTag *vlanTag)
    {
    uint16 vlanId;
    uint32 length, remainingBytes;
    uint8* pktBuffer = AtPacketDataBuffer((AtPacket)self, &length);
    uint8* buffer;
    remainingBytes = length - offset;
    if (remainingBytes < cZteTagLength)
        return NULL;

    buffer = pktBuffer + offset;
    vlanTag->pcp = *buffer >> 3;
    vlanTag->cfi = (uint8)((*buffer << 5) | (*(buffer + 1) >> 3));

    vlanId = *(buffer + 2);
    vlanId = (uint16)(vlanId << 8);
    vlanId = (uint16)(vlanId | *(buffer + 3));

    vlanTag->vlanId = vlanId;

    return vlanTag;
    }

static void DisplayFirstVlanTag(ThaStmPwProductEthPacket self, tAtVlanTag *pVlanTag, uint8 level)
    {
	AtUnused(self);
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Ethernet Type: 0x%04x\r\n", pVlanTag->vlanId);
    }

static const char* EncapTypeString(uint8 encapType)
    {
    static char encapTypeStr[8];
    const char* encapString;

    if (encapType == 0x1)  encapString = "PPP";
    else if (encapType == 0x2)  encapString = "MLPPP";
    else if (encapType == 0x3)  encapString = "HDLC";
    else if (encapType == 0x4)  encapString = "FR";
    else if (encapType == 0x5)  encapString = "LAPB";
    else if (encapType == 0x6)  encapString = "X.25";
    else if (encapType == 0x8)  encapString = "ATM SAR AAL5 PDU";
    else if (encapType == 0x9)  encapString = "ATM OAM";
    else if (encapType == 0xA)  encapString = "ATM unmatch";
    else encapString = "Unknown";

    AtSprintf(encapTypeStr, "0x%02x (%s)", encapType, encapString);
    return encapTypeStr;
    }

static void DisplaySecondVlanTag(ThaStmPwProductEthPacket self, tAtVlanTag *pVlanTag, uint8 level)
    {
    uint8 cpuPktIndicator;
    uint8 portNumber;
    uint8 encapType;
    uint8 slotNumber;
    uint16 channelNumber;
    uint8 pktLen;
	AtUnused(self);

    cpuPktIndicator = (pVlanTag->pcp & cBit4) ? 1 : 0;
    portNumber = pVlanTag->pcp & cBit3_0;
    encapType = (pVlanTag->cfi >> 4) & cBit3_0;
    slotNumber = pVlanTag->cfi & cBit3_0;

    mFieldGet(pVlanTag->vlanId, cBit15_6, 6, uint16, &channelNumber);
    mFieldGet(pVlanTag->vlanId, cBit5_0, 0, uint8, &pktLen);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal,
             "* Tag: CPU_indicator = %d, PORT_NUMBER = %d, ENCAP_TYPE = %s, SLOT_NUMBER = %d, CHANNEL_NO = %d, PKT_LEN = %d\r\n",
             cpuPktIndicator,
             portNumber,
             EncapTypeString(encapType),
             slotNumber,
             channelNumber,
             pktLen);
    }

static uint32 FirstVlanOffset(AtEthPacket self)
    {
    return AtEthPacketPreambleLen(self) + (cMacLength * 2UL);
    }

static uint32 SecondVlanOffset(AtEthPacket self)
    {
    return FirstVlanOffset(self) + cZteEthTypeLength;
    }

static void DisplayVlans(AtEthPacket self, uint8 level)
    {
    tAtVlanTag vlanTag, *pVlanTag;
    ThaStmPwProductEthPacket packet = (ThaStmPwProductEthPacket)self;

    /* The first VLAN */
    pVlanTag = FirstVlanAtOffset(self, FirstVlanOffset(self), &vlanTag);
    if (pVlanTag == NULL)
        return;
    mMethodsGet(packet)->DisplayFirstVlanTag(packet, pVlanTag, level);

    /* And the second one */
    pVlanTag = SecondVlanAtOffset(self, SecondVlanOffset(self), &vlanTag);
    if (pVlanTag == NULL)
        return;
    mMethodsGet(packet)->DisplaySecondVlanTag(packet, pVlanTag, level);
    return;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60000031EthPacket);
    }

static void OverrideThaStmPwProductEthPacket(AtPacket self)
    {
    ThaStmPwProductEthPacket packet = (ThaStmPwProductEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductEthPacketOverride, mMethodsGet(packet), sizeof(m_ThaStmPwProductEthPacketOverride));

        mMethodOverride(m_ThaStmPwProductEthPacketOverride, DisplayFirstVlanTag);
        mMethodOverride(m_ThaStmPwProductEthPacketOverride, DisplaySecondVlanTag);
        }

    mMethodsSet(packet, &m_ThaStmPwProductEthPacketOverride);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket packet = (AtEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, mMethodsGet(packet), sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, DisplayVlans);
        }

    mMethodsSet(packet, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideThaStmPwProductEthPacket(self);
    OverrideAtEthPacket(self);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket Tha60000031EthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

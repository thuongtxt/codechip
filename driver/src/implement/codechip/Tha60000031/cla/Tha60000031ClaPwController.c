/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60000031ClaPwController.c
 *
 * Created Date: Jul 9, 2014
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/cla/ThaPdhPwProductClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60000031ClaPwController
    {
    tThaPdhPwProductClaPwController super;
    }tTha60000031ClaPwController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPwProductClaPwControllerMethods m_ThaStmPwProductClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 LocalChannelIdFromCVlan(ThaStmPwProductClaPwController self, tAtEthVlanTag *tag)
    {
	AtUnused(self);
    return ((tag->vlanId >> 6) & cBit9_0);
    }

static void OverrideThaStmPwProductClaPwController(ThaClaPwController self)
    {
    ThaStmPwProductClaPwController controller = (ThaStmPwProductClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductClaPwControllerOverride, mMethodsGet(controller), sizeof(m_ThaStmPwProductClaPwControllerOverride));

        mMethodOverride(m_ThaStmPwProductClaPwControllerOverride, LocalChannelIdFromCVlan);
        }

    mMethodsSet(controller, &m_ThaStmPwProductClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaStmPwProductClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60000031ClaPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60000031ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, cla);
    }

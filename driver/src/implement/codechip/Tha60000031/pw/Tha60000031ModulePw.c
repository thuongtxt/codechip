/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60000031ModulePw.c
 *
 * Created Date: Jul 9, 2014
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/pw/ThaPdhPwProductModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60000031ModulePw
    {
    tThaPdhPwProductModulePw super;
    }tTha60000031ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPwHeaderProvider PwHeaderProviderCreate(ThaModulePw self)
    {
	AtUnused(self);
    return Tha60000031PwHeaderProviderNew();
    }

static eBool PwIdleCodeIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePw(ThaModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(self), sizeof(m_ThaModulePwOverride));
        mMethodOverride(m_ThaModulePwOverride, PwHeaderProviderCreate);
        mMethodOverride(m_ThaModulePwOverride, PwIdleCodeIsSupported);
        }

    mMethodsSet(self, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw((ThaModulePw)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60000031ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModulePwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60000031ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

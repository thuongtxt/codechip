/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60000031PwHeaderProvider.c
 *
 * Created Date: Jul 9, 2014
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/headerprovider/ThaPwHeaderProviderInternal.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/
#define cZteRouterTagInBytes 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60000031PwHeaderProvider
    {
    tThaPwHeaderProvider super;
    }tTha60000031PwHeaderProvider;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderProviderMethods m_ThaPwHeaderProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CVlanExist(uint8 headerLength)
    {
    if (headerLength < (cMacSizeInBytes + cEthTypeSizeInBytes + cZteRouterTagInBytes))
        return cAtFalse;

    return cAtTrue;
    }

static eBool SVlanExist(uint8 headerLength)
    {
    if (headerLength < (cMacSizeInBytes + cEthTypeSizeInBytes))
        return cAtFalse;

    return cAtTrue;
    }

static tAtEthVlanTag *CVlanTagFromBuffer(uint8 *buffer, tAtEthVlanTag *vlanTag)
    {
    uint16 vlanId;

    vlanTag->priority = *buffer >> 3;
    vlanTag->cfi      = (uint8)((*buffer << 5) | (*(buffer + 1) >> 3));

    vlanId = *(buffer + 2);
    vlanId = (uint16)(vlanId << 8);
    vlanId = (uint16)(vlanId | *(buffer + 3));

    vlanTag->vlanId = vlanId;

    return vlanTag;
    }

static ThaModulePw ModulePw(ThaPwAdapter pw)
    {
    return (ThaModulePw)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cAtModulePw);
    }

static tAtEthVlanTag* CVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *cVlan)
    {
    /* Get current header buffer */
    uint8 currentHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(controller, &currentHeaderLength);
    ThaPwAdapter adapter = ThaPwHeaderControllerAdapterGet(controller);
	AtUnused(self);

    if (!CVlanExist(currentHeaderLength))
        return NULL;

    headerBuffer = headerBuffer + cMacSizeInBytes + cEthTypeSizeInBytes;

    /* This product only support lookup 2 vlans */
    if (ThaModulePwLookupModeGet(ModulePw(adapter), (AtPw)adapter) != cThaPwLookupMode2Vlans)
        return NULL;

    return CVlanTagFromBuffer(headerBuffer, cVlan);
    }

static tAtEthVlanTag* SVlanGet(ThaPwHeaderProvider self, ThaPwHeaderController controller, tAtEthVlanTag *sVlan)
    {
    /* Get current header buffer */
    uint8 currentHeaderLength;
    uint8 *headerBuffer = ThaPwHeaderControllerHwHeaderArrayGet(controller, &currentHeaderLength);
	AtUnused(self);

    if (!SVlanExist(currentHeaderLength))
        return NULL;

    headerBuffer = headerBuffer + cMacSizeInBytes;

    sVlan->vlanId = *headerBuffer;
    sVlan->vlanId = (uint16)(sVlan->vlanId << 8);
    sVlan->vlanId = (uint16)(sVlan->vlanId | *(headerBuffer + 1));

    return sVlan;
    }

static uint8 PutCVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
	AtUnused(vlanType);
	AtUnused(self);
    buffer[currentIndex] = (uint8)((vlanTag->priority << 3) | (vlanTag->cfi >> 5));
    currentIndex = (uint8)(currentIndex + 1);
    buffer[currentIndex] = (uint8)((uint8)(vlanTag->cfi << 3) & cBit7_3);
    currentIndex = (uint8)(currentIndex + 1);
    buffer[currentIndex] = (uint8)(vlanTag->vlanId >> 8);
    currentIndex = (uint8)(currentIndex + 1);
    buffer[currentIndex] = (uint8)vlanTag->vlanId;
    currentIndex = (uint8)(currentIndex + 1);

    return currentIndex;
    }

static uint8 PutSVlanToBuffer(ThaPwHeaderProvider self, uint8 *buffer, uint8 currentIndex, uint16 vlanType, const tAtEthVlanTag *vlanTag)
    {
	AtUnused(vlanType);
	AtUnused(self);
    buffer[currentIndex] = (uint8)(vlanTag->vlanId >> 8);
    currentIndex = (uint8)(currentIndex + 1);
    buffer[currentIndex] = (uint8)vlanTag->vlanId;
    currentIndex = (uint8)(currentIndex + 1);

    return currentIndex;
    }

static void OverrideThaPwHeaderProvider(ThaPwHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderProviderOverride, mMethodsGet(self), sizeof(m_ThaPwHeaderProviderOverride));

        mMethodOverride(m_ThaPwHeaderProviderOverride, CVlanGet);
        mMethodOverride(m_ThaPwHeaderProviderOverride, SVlanGet);
        mMethodOverride(m_ThaPwHeaderProviderOverride, PutCVlanToBuffer);
        mMethodOverride(m_ThaPwHeaderProviderOverride, PutSVlanToBuffer);
        }

    mMethodsSet(self, &m_ThaPwHeaderProviderOverride);
    }

static void Override(ThaPwHeaderProvider self)
    {
    OverrideThaPwHeaderProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60000031PwHeaderProvider);
    }

static ThaPwHeaderProvider ObjectInit(ThaPwHeaderProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHeaderProviderObjectInit(self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderProvider Tha60000031PwHeaderProviderNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProvider);
    }

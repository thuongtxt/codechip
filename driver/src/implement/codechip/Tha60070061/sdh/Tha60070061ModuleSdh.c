/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60070061ModuleSdh.c
 *
 * Created Date: Nov 4, 2014
 *
 * Description : SDH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60030080/sdh/Tha60030080ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070061ModuleSdh
    {
    tTha60030080ModuleSdh super;
    }tTha60070061ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save supper implement */
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    return 4;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
	AtUnused(self);
    return Tha60070061SdhLineSerdesControllerNew(line, serdesId);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(self);
    return (uint8)AtChannelIdGet((AtChannel)line);
    }

static uint8 SerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(self);
    return (uint8)AtChannelIdGet((AtChannel)line);
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha60070061SdhLineNew(channelId, (AtModuleSdh)self, version);

    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static eAtSerdesTimingMode DefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(self);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm4)
        return cAtSerdesTimingModeAuto;

    return cAtSerdesTimingModeLockToData;
    }

static eBool BlockErrorCountersSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        mMethodOverride(m_ThaModuleSdhOverride, DefaultSerdesTimingModeForLine);
        mMethodOverride(m_ThaModuleSdhOverride, BlockErrorCountersSupported);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070061ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030080ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60070061ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

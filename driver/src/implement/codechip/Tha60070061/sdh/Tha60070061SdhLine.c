/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60070061SdhLine.c
 *
 * Created Date: Nov 18, 2014
 *
 * Description : This file contains implementations of SDH line for AF6LTT0061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/sdh/ThaStmPwProductSdhLineInternal.h"
#include "../man/Tha60070061DeviceReg.h"
#include "../../Tha60070041/man/Tha60070041Device.h"

/*--------------------------- Define -----------------------------------------*/
/* TOP 4 status */
#define cRegTop4Status                           0xF00064
#define cRegTop4StatusSfpIsExistedMask(lineId)   (cBit0 << lineId)
#define cRegTop4StatusSfpIsExistedShift(lineId)  (lineId)

/*--------------------------- Macros -----------------------------------------*/
#define mLedStateGet(self, color, fieldName, ledState)                                                                                                      \
    {                                                                                                                                                       \
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);                                                                                                               \
    uint32 regVal = mChannelHwRead(self, cRegTopControl4, cAtModuleSdh);                                                                                           \
    mFieldGet(regVal, cRegTopControl4##color##Led##fieldName##Mask(lineId), cRegTopControl4##color##Led##fieldName##Shift(lineId), eAtLedState, &ledState); \
    }

#define mLedStateSet(self, color, fieldName)                                                                                                                       \
    {                                                                                                                                                                     \
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);                                                                                                                             \
    uint32 regVal = mChannelHwRead(self, cRegTopControl4, cAtModuleSdh);                                                                                                         \
    mFieldIns(&regVal, cRegTopControl4##color##Led##fieldName##Mask(lineId), cRegTopControl4##color##Led##fieldName##Shift(lineId), (ledState == cAtLedStateOn) ? 1 : 0); \
    mChannelHwWrite(self, cRegTopControl4, regVal, cAtModuleSdh);              \
    }

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070061SdhLine
    {
    tThaStmPwProductSdhLine super;
    }tTha60070061SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineMethods m_ThaSdhLineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FpgaLoopOutIsApplicable(ThaSdhLine self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopOutIsApplicable);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideThaSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070061SdhLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60070061SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module, version);
    }

eAtRet Tha60070061SdhLineGreenLedStateSet(AtSdhLine self, eAtLedState ledState)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateSet(self, Green, PortState);
    return cAtOk;
    }

eAtRet Tha60070061SdhLineRedLedStateSet(AtSdhLine self, eAtLedState ledState)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateSet(self, Red, PortState);
    return cAtOk;
    }

eAtLedState Tha60070061SdhLineRedLedStateGet(AtSdhLine self)
    {
    eAtLedState ledState;

    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateGet(self, Red, PortState, ledState);
    return ledState;
    }

eAtLedState Tha60070061SdhLineGreenLedStateGet(AtSdhLine self)
    {
    eAtLedState ledState;

    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateGet(self, Green, PortState, ledState);
    return ledState;
    }

eBool Tha60070061SdhLineSfpIsExisted(AtSdhLine self)
    {
    eBool isExisted = cAtFalse;
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 regVal = mChannelHwRead(self, cRegTop4Status, cAtModuleSdh);

    mFieldGet(regVal, cRegTop4StatusSfpIsExistedMask(lineId), cRegTop4StatusSfpIsExistedShift(lineId), eBool, &isExisted);
    return isExisted;
    }

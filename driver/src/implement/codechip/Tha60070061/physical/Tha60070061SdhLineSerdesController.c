/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60070061SdhLineSerdesController.c
 *
 * Created Date: Nov 10, 2014
 *
 * Description : This file contains implementations of SDH line serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/physical/ThaStmPwProductSdhLineSerdesController.h"
#include "../man/Tha60070061DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cPhyBaseAddress         0xF30800
#define cSerdesLoopInOffset     0x61
#define cSerdesLockToRefOffset  0x64
#define cSerdesLockToDataOffset 0x65

#define cAtSdhLineSerdesSubLoopin 0xF00042
#define cAtSdhLineSerdesSubLoopinMask(lineId)  (cBit0 << lineId)
#define cAtSdhLineSerdesSubLoopinShift(lineId) (lineId)

#define cAtSdhLineSerdesDisable 0xF00041
#define cAtSdhLineSerdesDisableMask(lineId)  (cBit0 << lineId)
#define cAtSdhLineSerdesDisableShift(lineId) (lineId)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070061SdhLineSerdesController * Tha60070061SdhLineSerdesController;

typedef struct tTha60070061SdhLineSerdesController
    {
    tThaStmPwProductSdhLineSerdesController super;

    /* Private data */
    }tTha60070061SdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSerdesControllerMethods          m_ThaSerdesControllerOverride;
static tThaSdhLineSerdesControllerMethods   m_ThaSdhLineSerdesControllerOverride;
static tAtSerdesControllerMethods           m_AtSerdesControllerOverride;

/* Supper implementation */
static const tThaSerdesControllerMethods    *m_ThaSerdesControllerMethods = NULL;
static const tAtSerdesControllerMethods     *m_AtSerdesControllerMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xF30000;
    }

static uint32 PartOffset(ThaSerdesController self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return cPhyBaseAddress + cSerdesLoopInOffset;
    }

static eAtRet LoopInEnable(ThaSerdesController self, eBool enable)
    {
    uint32 regVal = 0;
    uint32 lineId = 0;
    eAtRet ret = m_ThaSerdesControllerMethods->LoopInEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    lineId = AtSerdesControllerIdGet((AtSerdesController)self);
    regVal = AtSerdesControllerRead((AtSerdesController)self, cAtSdhLineSerdesSubLoopin, cAtModuleSdh);
    mFieldIns(&regVal, cAtSdhLineSerdesSubLoopinMask(lineId), cAtSdhLineSerdesSubLoopinShift(lineId), enable);
    AtSerdesControllerWrite((AtSerdesController)self, cAtSdhLineSerdesSubLoopin, regVal, cAtModuleSdh);
    return ret;
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSerdesControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, m_ThaSerdesControllerMethods , sizeof(m_ThaSerdesControllerOverride));
        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, PartOffset);
		mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
		mMethodOverride(m_ThaSerdesControllerOverride, LoopInEnable);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    return cPhyBaseAddress + cSerdesLockToRefOffset;
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    return cPhyBaseAddress + cSerdesLockToDataOffset;
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController controller = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride,  mMethodsGet(controller), sizeof(m_ThaSdhLineSerdesControllerOverride));
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegAddress);
        }

    mMethodsSet(controller, &m_ThaSdhLineSerdesControllerOverride);
    }

static eAtRet SfpEnable(AtSerdesController self, eBool enable)
    {
    uint32 lineId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cRegTopControl4, cAtModuleSdh);

    mFieldIns(&regVal, cRegTopControl4SfpDisMask(lineId), cRegTopControl4SfpDisShift(lineId), enable ? 0 : 1);
    AtSerdesControllerWrite(self, cRegTopControl4, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eBool SfpIsEnabled(AtSerdesController self)
    {
    uint32 lineId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cRegTopControl4, cAtModuleSdh);

    return (regVal & cRegTopControl4SfpDisMask(lineId)) ? cAtFalse : cAtTrue;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    uint32 lineId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAtSdhLineSerdesDisable, cAtModuleSdh);

    mFieldIns(&regVal, cAtSdhLineSerdesDisableMask(lineId), cAtSdhLineSerdesDisableShift(lineId), enable ? 0 : 1);
    AtSerdesControllerWrite(self, cAtSdhLineSerdesDisable, regVal, cAtModuleSdh);

    SfpEnable(self, enable);

    return cAtOk;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    uint32 lineId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cAtSdhLineSerdesDisable, cAtModuleSdh);
    return (regVal & cAtSdhLineSerdesDisableMask(lineId)) ? cAtFalse : cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static void Debug(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->Debug(self);
    AtPrintc(cSevNormal, "* SFP: %s\r\n", SfpIsEnabled(self) ? "enable" : "disable");
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideThaSerdesController(self);
    OverrideThaSdhLineSerdesController(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070061SdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60070061SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

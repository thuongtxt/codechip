/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60070061Ddr.c
 *
 * Created Date: Nov 20, 2014
 *
 * Description : This file contains implementations of DDR for AF6LTT0061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/ram/ThaStmPwProductDdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070061Ddr * Tha60070061Ddr;

typedef struct tTha60070061Ddr
    {
    tThaStmPwProductDdr super;
    }tTha60070061Ddr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaDdrMethods m_ThaDdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070061Ddr);
    }

static uint32 BaseAddress(ThaDdr self)
    {
	AtUnused(self);
    return 0xF10000;
    }

static void OverrideThaDdr(AtRam self)
    {
    ThaDdr ddr = (ThaDdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDdrOverride, mMethodsGet(ddr), sizeof(m_ThaDdrOverride));

        mMethodOverride(m_ThaDdrOverride, BaseAddress);
        }

    mMethodsSet(ddr, &m_ThaDdrOverride);
    }

static void Override(AtRam self)
    {
    OverrideThaDdr(self);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam Tha60070061DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDdr, ramModule, core, ramId);
    }


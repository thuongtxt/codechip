/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60070061Device.c
 *
 * Created Date: Nov 4, 2014
 *
 * Description : Product 60070061
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60070061Device.h"
#include "Tha60070061DeviceReg.h"
#include "../../Tha60070041/man/Tha60070041Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mLedStateSet(color, fieldName)                                                                                                              \
    {                                                                                                                                                     \
    regVal = Read(self, cRegTopControl4);                                                                                                                 \
    mFieldIns(&regVal, cRegTopControl4##color##Led##fieldName##Mask, cRegTopControl4##color##Led##fieldName##Shift, (ledState == cAtLedStateOn) ? 1 : 0); \
    Write(self, cRegTopControl4, regVal);                                                                                                                 \
    }

#define mLedStateGet(color, fieldName, ledState)                                                                                      \
    {                                                                                                                                       \
    regVal = Read(self, cRegTopControl4);                                                                                                   \
    mFieldGet(regVal, cRegTopControl4##color##Led##fieldName##Mask, cRegTopControl4##color##Led##fieldName##Shift, eAtLedState, &ledState); \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaDeviceMethods m_ThaDeviceOverride;
static tAtDeviceMethods m_AtDeviceOverride;

/* Save super implement */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId  == cAtModuleSdh)   return (AtModule)Tha60070061ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)    return (AtModule)Tha60070061ModulePwNew(self);
    if (moduleId  == cAtModuleEth)   return (AtModule)Tha60070061ModuleEthNew(self);
    if (moduleId  == cAtModuleRam)   return (AtModule)Tha60070061ModuleRamNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 1;
    }

/* Currently no need to check SSKEY */
static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60070061SSKeyCheckerNew(self);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModulePrbs)
        return cAtTrue;

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock,
                                                 cAtModulePrbs};
    AtUnused(self);

    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070061Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030080DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60070061DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

static uint32 Read(AtDevice self, uint32 address)
    {
    return AtHalRead(AtDeviceIpCoreHalGet(self, 0), address);
    }

static void Write(AtDevice self, uint32 address, uint32 value)
    {
    AtHalWrite(AtDeviceIpCoreHalGet(self, 0), address, value);
    }

eAtRet Tha60070061DeviceGreenLedStateSet(AtDevice self, eAtLedState ledState)
    {
    uint32 regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateSet(Green, CardState);
    return cAtOk;
    }

eAtRet Tha60070061DeviceRedLedStateSet(AtDevice self, eAtLedState ledState)
    {
    uint32 regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateSet(Red, CardState);
    return cAtOk;
    }

eAtRet Tha60070061DeviceGreenLedActivationSet(AtDevice self, eAtLedState ledState)
    {
    uint32 regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateSet(Green, CardActive);
    return cAtOk;
    }

eAtRet Tha60070061DeviceRedLedActivationSet(AtDevice self, eAtLedState ledState)
    {
    uint32 regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    mLedStateSet(Red, CardActive);
    return cAtOk;
    }

eAtLedState Tha60070061DeviceGreenLedStateGet(AtDevice self)
    {
    uint32 regVal;
    eAtLedState ledState;

    if (self == NULL)
        return cAtLedStateOff;

    mLedStateGet(Green, CardState, ledState);
    return ledState;
    }

eAtLedState Tha60070061DeviceRedLedStateGet(AtDevice self)
    {
    uint32 regVal;
    eAtLedState ledState;

    if (self == NULL)
        return cAtLedStateOff;

    mLedStateGet(Red, CardState, ledState);
    return ledState;
    }

eAtLedState Tha60070061DeviceGreenLedActivationGet(AtDevice self)
    {
    uint32 regVal;
    eAtLedState ledState;

    if (self == NULL)
        return cAtLedStateOff;

    mLedStateGet(Green, CardActive, ledState);
    return ledState;
    }

eAtLedState Tha60070061DeviceRedLedActivationGet(AtDevice self)
    {
    uint32 regVal;
    eAtLedState ledState;

    if (self == NULL)
        return cAtLedStateOff;

    mLedStateGet(Red, CardActive, ledState);
    return ledState;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60070061Device.h
 * 
 * Created Date: Apr 26, 2015
 *
 * Description : Product 60070061
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070061DEVICE_H_
#define _THA60070061DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60030080/man/Tha60030080Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60070061Device
    {
    tTha60030080Device super;
    }tTha60070061Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60070061DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070061DEVICE_H_ */


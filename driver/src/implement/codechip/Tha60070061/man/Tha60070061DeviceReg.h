/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60070061DeviceReg.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : Tha60070061 device registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070061DEVICEREG_H_
#define _THA60070061DEVICEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Top control 4 register */
#define cRegTopControl4  0xF00044

#define cRegTopControl4SfpDisMask(lineId)   (cBit12 << lineId)
#define cRegTopControl4SfpDisShift(lineId)  12

#define cRegTopControl4GreenLedPortStateMask(lineId)   (cBit5 << (lineId * 2))
#define cRegTopControl4GreenLedPortStateShift(lineId)  (5 + (lineId * 2))

#define cRegTopControl4RedLedPortStateMask(lineId)     (cBit4 << (lineId * 2))
#define cRegTopControl4RedLedPortStateShift(lineId)    (4 + (lineId * 2))

/* Card active LEDs */
#define cRegTopControl4RedLedCardActiveMask     cBit3
#define cRegTopControl4RedLedCardActiveShift    3
#define cRegTopControl4GreenLedCardActiveMask   cBit2
#define cRegTopControl4GreenLedCardActiveShift  2

/* Card status LEDs */
#define cRegTopControl4RedLedCardStateMask      cBit1
#define cRegTopControl4RedLedCardStateShift     1
#define cRegTopControl4GreenLedCardStateMask    cBit0
#define cRegTopControl4GreenLedCardStateShift   0

#ifdef __cplusplus
}
#endif
#endif /* _THA60070061DEVICEREG_H_ */


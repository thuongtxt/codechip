/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha60030023ModulePpp.c
 *
 * Created Date: Jun 24, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaPdhMlppp/ppp/ThaPdhMlpppModulePppInternal.h"
#include "../../Tha60030022/ppp/Tha60030022ModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030023ModulePpp
    {
    tTha60030022ModulePpp super;
    }tTha60030023ModulePpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePppMethods m_AtModulePppOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    /* This product does not support MLPPP */
    return 16;
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, mMethodsGet(self), sizeof(m_AtModulePppOverride));

        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        }

    mMethodsSet(self, &m_AtModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030023ModulePpp);
    }

static AtModulePpp ObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030022ModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60030023ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

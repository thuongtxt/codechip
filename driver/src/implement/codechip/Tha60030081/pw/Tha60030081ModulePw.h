/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60030081ModulePw.h
 * 
 * Created Date: Mar 14, 2014
 *
 * Description : PW module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030081MODULEPW_H_
#define _THA60030081MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pw/ThaStmPwProductModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030081ModulePw
    {
    tThaStmPwProductModulePw super;
    }tTha60030081ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60030081ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60030081MODULEPW_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60030081ModulePw.c
 *
 * Created Date: Dec 13, 2013
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60030081ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PwIdleCodeIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool PwMbitCounterIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool UseLbitPacketsForAcrDcr(ThaModulePw self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwIdleCodeIsSupported);
        mMethodOverride(m_ThaModulePwOverride, PwMbitCounterIsSupported);
        mMethodOverride(m_ThaModulePwOverride, UseLbitPacketsForAcrDcr);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081ModulePw);
    }

AtModulePw Tha60030081ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60030081ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60030081ModulePwObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60031031ModuleClock.h
 * 
 * Created Date: Sep 3, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030081MODULECLOCK_H_
#define _THA60030081MODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtClockExtractor.h"
#include "../../../default/clock/ThaModuleClockInternal.h"
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030081ModuleClock * Tha60030081ModuleClock;

typedef struct tTha60030081ModuleClock
    {
    tThaStmPwProductModuleClock super;

    /* This module clock wrap-up 60031031 module clock */
    AtModuleClock localClockModule;
    }tTha60030081ModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60030081ModuleClockLocalModule(AtModuleClock self);
AtModuleClock Tha60030081ModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60030081MODULECLOCK_H_ */


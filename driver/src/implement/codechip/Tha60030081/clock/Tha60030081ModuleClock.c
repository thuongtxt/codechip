/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60030081ModuleClock.c
 *
 * Created Date: Nov 16, 2013
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSdh.h"
#include "Tha60030081ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60030081ModuleClock)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods        m_AtModuleOverride;
static tAtModuleClockMethods   m_AtModuleClockOverride;
static tThaModuleClockMethods  m_ThaModuleClockOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081ModuleClock);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
	AtUnused(self);
    return 2;
    }

static AtModuleSdh SdhModule(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eAtRet ClockDefaultExtract(AtModule self)
    {
    AtModuleSdh sdhModule     = SdhModule(self);
    AtModuleClock clockModule = (AtModuleClock)self;
    eAtRet ret = cAtOk;

    ret |= AtClockExtractorExtract(AtModuleClockExtractorGet(clockModule, 0),
                                   cAtTimingModeSdhLineRef,
                                   (AtChannel)AtModuleSdhLineGet(sdhModule, 0));
    ret |= AtClockExtractorExtract(AtModuleClockExtractorGet(clockModule, 1),
                                   cAtTimingModeSdhLineRef,
                                   (AtChannel)AtModuleSdhLineGet(sdhModule, 1));

    return ret;
    }

static eAtRet Setup(AtModule self)
    {
    Tha60030081ModuleClock thisModule = (Tha60030081ModuleClock)self;
    AtDevice device = AtModuleDeviceGet(self);
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Setup 60031031 module clock */
    if (thisModule->localClockModule == NULL)
        thisModule->localClockModule = ThaStmPwProductModuleClockNew(device);

    if (thisModule->localClockModule == NULL)
        return cAtError;

    return AtModuleSetup((AtModule)(thisModule->localClockModule));
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        {
        AtDriverLog(AtDeviceDriverGet(AtModuleDeviceGet(self)), cAtLogLevelWarning, "Module %s super init fail\r\n", AtObjectToString((AtObject)self));
        return ret;
        }

    return ClockDefaultExtract(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    if (mThis(self)->localClockModule)
        AtObjectDelete((AtObject)mThis(self)->localClockModule);
    mThis(self)->localClockModule = NULL;

    m_AtObjectMethods->Delete(self);
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return Tha60030081ClockExtractorNew((AtModuleClock)self, extractorId);
    }

static void OverrideAtObject(AtModuleClock self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModule(self);
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    OverrideAtObject(self);
    }

AtModuleClock Tha60030081ModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60030081ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60030081ModuleClockObjectInit(newModule, device);
    }

AtModuleClock Tha60030081ModuleClockLocalModule(AtModuleClock self)
    {
    return self ? mThis(self)->localClockModule : NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60030081ClockExtractor.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/clock/ThaClockExtractorInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockExtractor.h"
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductModuleClock.h"
#include "Tha60030081ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#define cSelectRegister 0xF00041
#define cSelectMask(outputId)  (cBit1_0 << ((outputId) * 2))
#define cSelectShift(outputId) ((outputId) * 2)

#define cPart1FirstExtractor 0
#define cPart2FirstExtractor 2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60030081ClockExtractor)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030081ClockExtractor * Tha60030081ClockExtractor;
typedef struct tTha60030081ClockExtractor
    {
    tThaStmPwProductClockExtractor super;

    /* Reference to clock extractor that it wraps */
    AtClockExtractor localExtractor;
    }tTha60030081ClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081ClockExtractor);
    }

static eAtModuleClockRet ExtractFromLocalExtractor(AtClockExtractor self, AtClockExtractor localExtractor)
    {
    uint32 regVal;
    uint32 outputId = AtClockExtractorIdGet((AtClockExtractor)self);

    regVal = AtClockExtractorRead((AtClockExtractor)self, cSelectRegister);
    mFieldIns(&regVal, cSelectMask(outputId), cSelectShift(outputId), AtClockExtractorIdGet(localExtractor));
    AtClockExtractorWrite((AtClockExtractor)self, cSelectRegister, regVal);

    mThis(self)->localExtractor = localExtractor;

    return cAtOk;
    }

static AtModuleClock LocalClockModule(AtClockExtractor self)
    {
    return Tha60030081ModuleClockLocalModule(AtClockExtractorModuleGet(self));
    }

static eBool TwoExtractorsOnTheSamePart(AtClockExtractor self, uint8 partId, uint8 *oppositeLocalExtractorId)
    {
    uint32 regVal;
    uint8 oppositePart;
    uint8 oppositeLocalId;
    uint8 thisExtractorId     = AtClockExtractorIdGet(self);
    uint8 oppositeExtractorId = (thisExtractorId == 0) ? 1 : 0;
    AtClockExtractor oppositeExtractor = AtModuleClockExtractorGet(AtClockExtractorModuleGet(self), oppositeExtractorId);

    /* If opposite extractor has not been allocated local extractor, so they are not on same part */
    if ((oppositeExtractor == NULL) || (mThis(oppositeExtractor)->localExtractor == NULL))
        return cAtFalse;

    /* Get opposite local extractor id */
    regVal = AtClockExtractorRead((AtClockExtractor)self, cSelectRegister);
    mFieldGet(regVal, cSelectMask(oppositeExtractorId), cSelectShift(oppositeExtractorId), uint8, &oppositeLocalId);
    oppositePart = (oppositeLocalId <= 1) ? 0 : 1;

    if (oppositeLocalExtractorId)
        *oppositeLocalExtractorId = oppositeLocalId;

    return (oppositePart == partId) ? cAtTrue : cAtFalse;
    }

static AtClockExtractor FindLocalClockExtractorOnPart(AtClockExtractor self, uint8 partId)
    {
    uint8 oppositeLocalExtractorId = 0;

    /* Not in the same part, just need to use the first one of other part */
    if (!TwoExtractorsOnTheSamePart(self, partId, &oppositeLocalExtractorId))
        return AtModuleClockExtractorGet(LocalClockModule(self), (partId == 0) ? cPart1FirstExtractor : cPart2FirstExtractor);

    /* Same part, need to use other local extractor */
    if (partId == 0)
        return AtModuleClockExtractorGet(LocalClockModule(self), (oppositeLocalExtractorId == 0) ? 1 : 0);

    return AtModuleClockExtractorGet(LocalClockModule(self), (oppositeLocalExtractorId == 2) ? 3 : 2);
    }

static eAtModuleClockRet HwSystemClockExtract(ThaClockExtractor self)
    {
    AtClockExtractor me = (AtClockExtractor)self;
    AtClockExtractor localExtractor = mThis(self)->localExtractor;

    if (localExtractor == NULL)
        localExtractor = FindLocalClockExtractorOnPart(me, 0);

    if (localExtractor == NULL)
        return cAtError;

    if (AtClockExtractorSystemClockExtract(localExtractor) != cAtOk)
        return cAtError;

    return ExtractFromLocalExtractor(me, localExtractor);
    }

static eBool SystemClockIsExtracted(AtClockExtractor self)
    {
    return AtClockExtractorSdhSystemClockIsExtracted(mThis(self)->localExtractor);
    }

static eBool CanExtractSdhLineClock(ThaClockExtractor self, AtSdhLine line)
    {
	AtUnused(line);
	AtUnused(self);
    return cAtTrue;
    }

static eAtModuleClockRet HwExternalClockExtract(ThaClockExtractor self, uint8 externalId)
    {
    AtClockExtractor thisExtractor = (AtClockExtractor)self;
    AtClockExtractor localExtractor = mThis(self)->localExtractor;
    if (localExtractor == NULL)
        localExtractor = FindLocalClockExtractorOnPart(thisExtractor, 0);

    if (localExtractor == NULL)
        return cAtError;

    if (AtClockExtractorExternalClockExtract(localExtractor, externalId) != cAtOk)
        return cAtError;

    return ExtractFromLocalExtractor(thisExtractor, localExtractor);
    }

static uint8 ExternalClockGet(AtClockExtractor self)
    {
    if (mThis(self)->localExtractor)
        return AtClockExtractorExternalClockGet(mThis(self)->localExtractor);

    return ThaModuleClockMaxNumExternalClockSources((ThaModuleClock)AtClockExtractorModuleGet(self));
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtTrue;
    }

static AtClockExtractor FindLocalClockExtractorForDe1(AtClockExtractor self, AtPdhDe1 de1)
    {
    AtClockExtractor localExtractor = mThis(self)->localExtractor;
    uint8 partOfde1 = ThaPdhDe1PartId((ThaPdhDe1)de1);

    if (localExtractor == NULL)
        localExtractor = FindLocalClockExtractorOnPart(self, partOfde1);

    else if (ThaClockExtractorPartId((ThaClockExtractor)localExtractor) != partOfde1)
        localExtractor = FindLocalClockExtractorOnPart(self, partOfde1);

    if (localExtractor == NULL)
        return NULL;

    if (AtClockExtractorPdhDe1ClockExtract(localExtractor, de1) != cAtOk)
        return NULL;

    return localExtractor;
    }

static eAtModuleClockRet HwPdhDe1ClockExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtClockExtractor localExtractor = FindLocalClockExtractorForDe1((AtClockExtractor)self, de1);

    if (localExtractor == NULL)
        return cAtError;

    return ExtractFromLocalExtractor((AtClockExtractor)self, localExtractor);
    }

static AtClockExtractor FindLocalClockExtractorForLine(AtClockExtractor self, AtSdhLine line)
    {
    AtClockExtractor localExtractor = mThis(self)->localExtractor;
    uint8 partOfLine = ThaModuleSdhPartOfChannel((AtSdhChannel)line);

    if (localExtractor == NULL)
        localExtractor = FindLocalClockExtractorOnPart(self, partOfLine);

    else if (ThaClockExtractorPartId((ThaClockExtractor)localExtractor) != partOfLine)
        localExtractor = FindLocalClockExtractorOnPart(self, partOfLine);

    if (localExtractor == NULL)
        return NULL;

    if (AtClockExtractorSdhLineClockExtract(localExtractor, line) != cAtOk)
        return NULL;

    return localExtractor;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    AtClockExtractor localExtractor = FindLocalClockExtractorForLine((AtClockExtractor)self, line);

    if (localExtractor == NULL)
        return cAtError;

    return ExtractFromLocalExtractor((AtClockExtractor)self, localExtractor);
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    if (mThis(self)->localExtractor)
        return AtClockExtractorEnable(mThis(self)->localExtractor, enable);

    if (enable == cAtFalse)
        return cAtOk;

    return cAtError;
    }

/* + 2 core have 4 pin refout (0,1 for core1 and 2,3 for core2),
 *   bit[1:0] of register 0xF00044 used to select refout to measure
 *
 * + read 0xF00064 to return value 0x1F40
 */

static void RefOutSelect(AtClockExtractor self)
    {
    static const uint8 checkingTime = 5;
    uint8 time_i;

    AtClockExtractorWrite((AtClockExtractor)self, 0xF00044, AtClockExtractorIdGet(mThis(self)->localExtractor));
    for (time_i = 0; time_i < checkingTime; time_i++)
        {
        AtClockExtractorRead((AtClockExtractor)self, 0xF00064);
        AtOsalUSleep(500000);
        }
    }

static void PrintExtractClockStatus(AtClockExtractor self)
    {
    static const uint8 checkingTime = 3;
    uint8 time_i;
    eBool clockGood = cAtTrue;

    if (mThis(self)->localExtractor == NULL)
        {
        AtPrintc(cSevNormal, "Unknown\r\n");
        return;
        }

    RefOutSelect(self);
    for (time_i = 0; time_i < checkingTime; time_i++)
        {
        if (AtClockExtractorRead((AtClockExtractor)self, 0xF00064) != 0x1F40)
            {
            clockGood = cAtFalse;
            break;
            }

        AtOsalUSleep(100000);
        }

    if (clockGood)
        AtPrintc(cSevInfo, "Good\r\n");
    else
        AtPrintc(cSevCritical, "Not good\r\n");
    }

static void Debug(AtClockExtractor self)
    {
    AtPrintc(cSevInfo, "Extractor: %d\r\n", AtClockExtractorIdGet(self));
    AtPrintc(cSevNormal, "    Local extractor ID: ");
    if (mThis(self)->localExtractor == NULL)
        AtPrintc(cSevWarning, "none\r\n");
    else
        AtPrintc(cSevNormal, "%d\r\n", AtClockExtractorIdGet(mThis(self)->localExtractor));

    AtPrintc(cSevNormal, "    Status            : ");
    PrintExtractClockStatus(self);
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    return AtClockExtractorIsEnabled(mThis(self)->localExtractor);
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhLineClock);
        mMethodOverride(m_ThaClockExtractorOverride, HwExternalClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1Clock);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSystemClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwPdhDe1ClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        mMethodOverride(m_AtClockExtractorOverride, SystemClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, ExternalClockGet);
        mMethodOverride(m_AtClockExtractorOverride, Debug);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60030081ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60030081ModuleSdh.c
 *
 * Created Date: Jun 10, 2013
 *
 * Description : SDH module for 60030081 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/physical/ThaSerdesController.h"
#include "Tha60030081ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081ModuleSdh);
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha60030081SdhLineNew(channelId, (AtModuleSdh)self, version);

    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static eAtRet Debug(AtModule self)
    {
	AtUnused(self);
    /* TODO: Need to check if super implementation need to be used */
    return cAtOk;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    if (ThaDeviceIsEp((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return ThaSdhLineSerdesControllerCyclone4V2New(line, serdesId);

    return Tha60030081SdhLineSerdesControllerNew(line, serdesId);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)line);
	AtUnused(self);

    if (lineId == 0) return 0;
    if (lineId == 1) return 1;
    if (lineId == 2) return 2;
    if (lineId == 3) return 3;
    if (lineId == 4) return 0x8;
    if (lineId == 5) return 0x9;
    if (lineId == 6) return 0xA;
    if (lineId == 7) return 0xB;

    /* Return invalid port */
    return 255;
    }

static uint8 SerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    return mMethodsGet(self)->SerdesCoreIdOfLine(self, line);
    }

static eBool BerHardwareInterruptIsSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool BlockErrorCountersSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanDisablePwPacketsToPsn(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, BerHardwareInterruptIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, BlockErrorCountersSupported);
        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        mMethodOverride(m_ThaModuleSdhOverride, CanDisablePwPacketsToPsn);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModule(AtModuleSdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModule(self);
    OverrideThaModuleSdh(self);
    }

AtModuleSdh Tha60030081ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60030081ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60030081ModuleSdhObjectInit(newModule, device);
    }

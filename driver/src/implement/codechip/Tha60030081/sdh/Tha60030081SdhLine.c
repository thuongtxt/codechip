/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60030081SdhLine.c
 *
 * Created Date: Jul 17, 2013
 *
 * Description : SDH line of product 60030081
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaSdhLineInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cSdhLineOhBusEnable 0x40A27
#define cOhBusEnableMask(lineId) (cBit0 << (lineId))
#define cOhBusEnableShift(lineId) (lineId)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030081SdhLine
    {
    tThaSdhLine super;
    }tTha60030081SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineMethods m_ThaSdhLineOverride;
static tAtSdhLineMethods  m_AtSdhLineOverride;
static tAtChannelMethods  m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FpgaLoopOutIsApplicable(ThaSdhLine self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleSdhRet OverheadBytesInsertEnable(AtSdhLine self, eBool enable)
    {
    uint32 address = cSdhLineOhBusEnable + ThaSdhLinePartOffset((ThaSdhLine)self);
    uint32 regValue = mChannelHwRead(self, address, cAtModuleSdh);
    uint32 lineLocalId = ThaModuleSdhLineLocalId(self);

    /* Enable insert means disable OH bus and vice versa */
    uint8 ohBusEnable = (enable == cAtTrue) ? 0 : 1;

    mFieldIns(&regValue, cOhBusEnableMask(lineLocalId), cOhBusEnableShift(lineLocalId), ohBusEnable);
    mChannelHwWrite(self, address, regValue, cAtModuleSdh);
    return cAtOk;
    }

static eBool OverheadBytesInsertIsEnabled(AtSdhLine self)
    {
    uint32 address = cSdhLineOhBusEnable + ThaSdhLinePartOffset((ThaSdhLine)self);
    uint32 regValue = mChannelHwRead(self, address, cAtModuleSdh);
    uint32 lineLocalId = ThaModuleSdhLineLocalId(self);
    uint8 ohBusIsEnabled;

    mFieldGet(regValue, cOhBusEnableMask(lineLocalId), cOhBusEnableShift(lineLocalId), uint8, &ohBusIsEnabled);
    return (ohBusIsEnabled) ? cAtFalse : cAtTrue;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Enable insert Overhead bytes */
    return AtSdhLineOverheadBytesInsertEnable((AtSdhLine)self, cAtTrue);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, OverheadBytesInsertEnable);
        mMethodOverride(m_AtSdhLineOverride, OverheadBytesInsertIsEnabled);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopOutIsApplicable);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideThaSdhLine(self);
    OverrideAtSdhLine(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081SdhLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60030081SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module, version);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60030081ModuleSdh.h
 * 
 * Created Date: Dec 13, 2013
 *
 * Description : SDH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030081MODULESDH_H_
#define _THA60030081MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/sdh/ThaStmPwProductModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030081ModuleSdh
    {
    tThaStmPwProductModuleSdh super;
    }tTha60030081ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60030081ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60030081MODULESDH_H_ */


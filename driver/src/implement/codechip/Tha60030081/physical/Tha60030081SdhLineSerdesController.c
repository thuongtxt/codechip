/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60030081SdhLineSerdesController.c
 *
 * Created Date: Aug 17, 2013
 *
 * Description : SDH Line SERDES controller for product 60030081
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/physical/ThaStmPwProductSdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030081SdhLineSerdesController
    {
    tThaStmPwProductSdhLineSerdesController super;
    }tTha60030081SdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSerdesControllerMethods         m_ThaSerdesControllerOverride;
static tAtSerdesControllerMethods          m_AtSerdesControllerOverride;
static tThaSdhLineSerdesControllerMethods  m_ThaSdhLineSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PartOffset(ThaSerdesController self)
    {
	AtUnused(self);
    return 0x0;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
	AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eBool OnFirstPart(ThaSerdesController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return ((ThaModuleSdhPartOfChannel(channel) == 0) ? cAtTrue : cAtFalse);
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
    return OnFirstPart(self) ? 0xf40461 : 0xf40861;
    }

static uint32 LocalLineId(ThaSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return ThaModuleSdhLineLocalId(line);
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
    return cBit0 << LocalLineId(self);
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
    return LocalLineId(self);
    }

static eBool PllIsLocked(AtSerdesController self)
    {
	AtUnused(self);
    /* Hardware needs to give SW other registers for this checking */
    return cAtTrue;
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
    return OnFirstPart((ThaSerdesController)self) ? 0xf40464 : 0xf40864;
    }

static uint32 LockToDataRegMask(ThaSdhLineSerdesController self)
    {
    return cBit0 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToDataRegShift(ThaSdhLineSerdesController self)
    {
    return LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
    return OnFirstPart((ThaSerdesController)self) ? 0xf40465 : 0xf40865;
    }

static uint32 LockToRefRegMask(ThaSdhLineSerdesController self)
    {
    return cBit0 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegShift(ThaSdhLineSerdesController self)
    {
    return LocalLineId((ThaSerdesController)self);
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
    AtChannel physicalPort = AtSerdesControllerPhysicalPortGet(self);
    ThaDevice device = (ThaDevice)AtChannelDeviceGet(physicalPort);
    return ThaDeviceIsEp(device) ? cAtFalse : cAtTrue;
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController sdhLineSerdes = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride, mMethodsGet(sdhLineSerdes), sizeof(m_ThaSdhLineSerdesControllerOverride));

        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegShift);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegShift);
        }

    mMethodsSet(sdhLineSerdes, &m_ThaSdhLineSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInMask);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInShift);
        mMethodOverride(m_ThaSerdesControllerOverride, PartOffset);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    OverrideThaSdhLineSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081SdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60030081SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60030081ModulePoh.c
 *
 * Created Date: Apr 8, 2014
 *
 * Description : POH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/poh/ThaModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030081ModulePoh
    {
    tThaModulePohV2 super;

    /* Private data */
    }tTha60030081ModulePoh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohV2Methods m_ThaModulePohV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RaiseUNEQwhenRxAndExpectedPslAreZero(ThaModulePohV2 self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePohV2(AtModule self)
    {
    ThaModulePohV2 pohModuleV2 = (ThaModulePohV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohV2Override, mMethodsGet(pohModuleV2), sizeof(m_ThaModulePohV2Override));

        mMethodOverride(m_ThaModulePohV2Override, RaiseUNEQwhenRxAndExpectedPslAreZero);
        }

    mMethodsSet(pohModuleV2, &m_ThaModulePohV2Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePohV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081ModulePoh);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePohV2ObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60030081ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

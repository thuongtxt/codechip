/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60030081SSKeyChecker.h
 * 
 * Created Date: Dec 19, 2013
 *
 * Description : SSKey checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030081SSKEYCHECKER_H_
#define _THA60030081SSKEYCHECKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductSSKeyChecker.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030081SSKeyChecker
    {
    tThaStmPwProductSSKeyChecker super;
    }tTha60030081SSKeyChecker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSSKeyChecker Tha60030081SSKeyCheckerObjectInit(AtSSKeyChecker self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60030081SSKEYCHECKER_H_ */


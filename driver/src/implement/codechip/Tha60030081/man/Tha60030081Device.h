/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60030081Device.h
 * 
 * Created Date: Dec 9, 2013
 *
 * Description : Product 60030081
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030081DEVICE_H_
#define _THA60030081DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60030081Device
    {
    tThaStmPwProductDevice super;
    }tTha60030081Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60030081DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60030081DEVICE_H_ */


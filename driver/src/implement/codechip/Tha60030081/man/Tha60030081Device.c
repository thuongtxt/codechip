/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030081Device.c
 *
 * Created Date: May 6, 2013
 *
 * Description : Product 60030131 (STM SAToP/CESoP)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60030081Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleSdh)   return (AtModule)Tha60030081ModuleSdhNew(self);
    if (moduleId  == cAtModuleClock) return (AtModule)Tha60030081ModuleClockNew(self);
    if (moduleId  == cAtModulePw)    return (AtModule)Tha60030081ModulePwNew(self);
    if (moduleId  == cAtModulePdh)   return (AtModule)Tha60030081ModulePdhNew(self);
    if (phyModule == cThaModuleCdr)  return ThaModuleCdrStmNew(self);
    if (phyModule == cThaModulePoh)  return Tha60030081ModulePohNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    /* FIXME: SSKey checking must be open for production */
    return cAtFalse;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60030081SSKeyCheckerNew(self);
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    /* If hardware use new OCN version that fix interrupt issue, this function
     * must return cAtTrue and the StartVersionWithNewOcn must be overridden correctly */
    return cAtFalse;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
	AtUnused(self);
    /* Need to override correctly if CanHaveNewOcn function return cAtTrue */
    return 0x0;
    }

static eBool PrbsIsSupported(AtDevice self)
    {
    return (ThaDeviceIsEp((ThaDevice)self) || AtDeviceAllFeaturesAvailableInSimulation(self)) ? cAtTrue : cAtFalse;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModulePrbs)
        return PrbsIsSupported(self);
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *EpAllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock,
                                                 cAtModulePrbs};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);
    return supportedModules;
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    if (PrbsIsSupported(self))
        return EpAllSupportedModulesGet(self, numModules);
    return m_AtDeviceMethods->AllSupportedModulesGet(self, numModules);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, CanHaveNewOcn);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }


static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030081Device);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

AtDevice Tha60030081DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030081DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60030081DeviceObjectInit(newDevice, driver, productCode);
    }

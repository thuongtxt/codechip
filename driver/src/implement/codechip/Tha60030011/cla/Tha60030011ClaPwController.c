/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60030011ClaPwController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030011ClaPwController
    {
    tThaClaPwControllerV2 super;
    }tTha60030011ClaPwController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030011ClaPwController);
    }

static ThaHbce HbceCreate(ThaClaPwController self_, uint8 hbceId, AtIpCore core)
    {
	AtUnused(core);
	AtUnused(hbceId);
	AtUnused(self_);
    /* This product does not support HBCE */
    return NULL;
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    ThaClaPwController claController = (ThaClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(claController), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        }

    mMethodsSet(claController, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaPwControllerV2ObjectInit((ThaClaPwController)self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60030011ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, cla);
    }

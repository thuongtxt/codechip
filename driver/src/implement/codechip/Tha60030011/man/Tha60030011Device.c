/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030011Device.c
 *
 * Created Date: May 6, 2013
 *
 * Description : Product 0x2400 (E1 PW)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaPdhPw/man/ThaPdhPwDeviceInternal.h"
#include "../../../default/util/ThaUtil.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030011Device
    {
    tThaPdhPwProductDevice super;
    }tTha60030011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030011Device);
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
	AtUnused(self);
    return 1;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtDevice self, AtModule module)
    {
	AtUnused(module);
	AtUnused(self);
    return NULL;
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
	AtUnused(self);
    return Tha16BitGlobalLongRegisterAccessNew();
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (phyModule == cThaModuleCla) return Tha60030011ModuleClaNew(self);
    if (moduleId == cAtModulePdh)   return (AtModule)ThaModulePdhNew(self);
    if (moduleId == cAtModuleEth)   return (AtModule)Tha60030011ModuleEthNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, NumIpCoresGet);
        mMethodOverride(m_AtDeviceOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, GlobalLongRegisterAccessCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

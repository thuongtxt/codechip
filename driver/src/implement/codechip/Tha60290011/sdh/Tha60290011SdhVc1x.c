/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290011SdhAbstractVc1x.c
 *
 * Created Date: Oct 5, 2016
 *
 * Description : Tha60290011SdhAbstractVc1x implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60290011ModuleOcn.h"
#include "Tha60290011SdhVc1xInternal.h"
#include "Tha60290011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60290011SdhVc1x *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModule(AtChannel self)
    {
    return (AtModule)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhTuVc1xCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhTuVc1xCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn(self)))
        return Tha6021OcnTuVcTxAlarmForce(self, alarmType);

    return m_AtChannelMethods->TxAlarmForce(self, alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn(self)))
        return Tha6021OcnTuVcTxAlarmUnForce(self, alarmType);

    return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn(self)))
        return Tha6021OcnTuVcTxForcedAlarmGet(self);

    return m_AtChannelMethods->TxForcedAlarmGet(self);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn(self)))
        return Tha6021OcnTuVcTxForcibleAlarms(self);

    return m_AtChannelMethods->TxForcibleAlarmsGet(self);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
	OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SdhVc1x);
    }

AtSdhVc Tha60290011SdhVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhVc1xObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60290011SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011SdhVc1xObjectInit(self, channelId, channelType, module);
    }

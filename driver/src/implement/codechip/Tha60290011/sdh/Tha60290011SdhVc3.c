/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290011SdhVc3.c
 *
 * Created Date: Dec 14, 2016
 *
 * Description : SDH VC concrete class for 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../ocn/Tha60290011ModuleOcn.h"
#include "Tha60290011ModuleSdh.h"
#include "Tha60290011SdhVc3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtSdhVcMethods    m_AtSdhVcOverride;
static tThaSdhVcMethods   m_ThaSdhVcOverride;

/* Super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModuleGet(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
	{
	if (!ThaSdhChannelShouldGetCounterFromPmc(self))
	    return m_AtChannelMethods->CounterGet(self, counterType);

	if (AtSdhPathHasPointerProcessor((AtSdhPath)self) && AtSdhPathCounterIsPointerAdjust(counterType))
	    return ThaModulePmcSdhAuPointerCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtFalse);

	return ThaModulePmcSdhAuVcPohCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtFalse);
	}

static uint32 CounterClear(AtChannel self, uint16 counterType)
	{
	if (!ThaSdhChannelShouldGetCounterFromPmc(self))
		    return m_AtChannelMethods->CounterClear(self, counterType);

	if (AtSdhPathHasPointerProcessor((AtSdhPath)self) && AtSdhPathCounterIsPointerAdjust(counterType))
		return ThaModulePmcSdhAuPointerCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtTrue);

	return ThaModulePmcSdhAuVcPohCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtTrue);
	}

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static eBool NewLoopbackIsSupported(AtChannel self)
    {
    return Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn(self));
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (!NewLoopbackIsSupported(self))
        return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);

    if ((loopbackMode == cAtLoopbackModeLocal)
        || (loopbackMode == cAtLoopbackModeRemote)
        || (loopbackMode == cAtLoopbackModeRelease))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (NewLoopbackIsSupported(self) && (alarmType & cAtSdhPathAlarmAis))
        return Tha60290011ModuleOcnFpgaAuVcTxAisForce(self, cAtTrue);

    return m_AtChannelMethods->TxAlarmForce(self, alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (NewLoopbackIsSupported(self) && (alarmType & cAtSdhPathAlarmAis))
        return Tha60290011ModuleOcnFpgaAuVcTxAisForce(self, cAtFalse);

    return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarm = 0;

    if (NewLoopbackIsSupported(self) && Tha60290011ModuleOcnFpgaAuVcTxAisIsForced(self))
            forcedAlarm |= cAtSdhPathAlarmAis;

    forcedAlarm |= m_AtChannelMethods->TxForcedAlarmGet(self);

    return forcedAlarm;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (!NewLoopbackIsSupported(self))
        return m_AtChannelMethods->RxAlarmForce(self, alarmType);

    if (alarmType & cAtSdhPathAlarmAis)
        return Tha60290011ModuleOcnFpgaAuVcRxAisForce(self, cAtTrue);

    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (!NewLoopbackIsSupported(self))
        return m_AtChannelMethods->RxAlarmUnForce(self, alarmType);

    if (alarmType & cAtSdhPathAlarmAis)
        return Tha60290011ModuleOcnFpgaAuVcRxAisForce(self, cAtFalse);

    return cAtOk;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    if (!NewLoopbackIsSupported(self))
        return m_AtChannelMethods->RxForcedAlarmGet(self);

    if (Tha60290011ModuleOcnFpgaAuVcRxAisIsForced(self))
        return cAtSdhPathAlarmAis;

    return 0;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    if (!NewLoopbackIsSupported(self))
        return m_AtChannelMethods->RxForcibleAlarmsGet(self);
    return cAtSdhPathAlarmAis;
    }

static eBool HasOcnStuffing(ThaSdhVc self)
    {
    /* VC3 stuffing comes at the same version of loopback. */
    return Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn((AtChannel)self));
    }

static eAtRet CanChangeStuffing(AtSdhVc self, eBool enable)
    {
    uint16 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)self);

    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        return enable ? cAtOk : cAtErrorModeNotSupport;

    if ((mapType == cAtSdhVcMapTypeVc3MapDe3) && !ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        return enable ? cAtOk : cAtErrorModeNotSupport;

    return cAtOk;
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, mMethodsGet(self), sizeof(m_AtSdhVcOverride));
        mMethodOverride(m_AtSdhVcOverride, CanChangeStuffing);
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));
        mMethodOverride(m_ThaSdhVcOverride, HasOcnStuffing);
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhVc(self);
    OverrideThaSdhVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SdhVc3);
    }

AtSdhVc Tha60290011SdhVc3ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60290011SdhVc3New(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011SdhVc3ObjectInit(self, channelId, channelType, module);
    }

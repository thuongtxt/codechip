/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290011ModuleSdh.c
 *
 * Created Date: Jun 10, 2016
 *
 * Description : SDH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../pdh/Tha60290011ModulePdh.h"
#include "../encap/Tha60290011HdlcChannel.h"
#include "Tha60290011ModuleSdh.h"
#include "Tha60290011ModuleSdhInternal.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha60290011SdhLineNew(channelId, (AtModuleSdh)self);

    if (channelType == cAtSdhChannelTypeAu3)
        return (AtSdhChannel)Tha60290011LineAuNew(channelId, channelType, self);

    if (channelType == cAtSdhChannelTypeVc3)
		return (AtSdhChannel)Tha60290011SdhVc3New(channelId, channelType, (AtModuleSdh)self);

    if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
		return (AtSdhChannel)Tha60290011SdhVc1xNew(channelId, channelType, (AtModuleSdh)self);

    return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
    }

static uint8 NumLinesPerPart(ThaModuleSdh self)
    {
    AtUnused(self);
    return 24;
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    AtUnused(self);
    return 24;
    }

static uint32 MaxNumDccHdlcChannels(ThaModuleSdh self)
    {
    AtUnused(self);
    return 48;
    }

static uint32 DccIdOffset(eAtSdhLineDccLayer layers)
    {
    return (layers & cAtSdhLineDccLayerSection) ? 0 : 1;
    }

static uint32 DccIdProvision(AtSdhLine self, eAtSdhLineDccLayer layers)
    {
    return (uint32)(AtChannelIdGet((AtChannel)self) * 2 + DccIdOffset(layers));
    }

static AtHdlcChannel DccChannelCreate(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layers)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    uint32 dccId = DccIdProvision(line, layers);
    if (dccId >= ThaModuleSdhMaxNumDccHdlcChannels((ThaModuleSdh) self))
        return NULL;

    if (Tha60290011DeviceOcnTxDccRsMsInsControlIsSupported(device))
        return Tha60290011HdlcChannelV4New(dccId, line, layers, self);

    if (Tha60290011DeviceDccHdlcFrameTransmissioniBitOderPerChannelIsSupported(device))
        return Tha60290011HdlcChannelV3New(dccId, line, layers, self);

    if (Tha60290011DeviceDccTxFcsMsbIsSupported(device))
        return Tha60290011HdlcChannelV2New(dccId, line, layers, self);

    return Tha60290011HdlcChannelNew(dccId, line, layers, self);
    }

static eBool DccLayerIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layers)
    {
    AtUnused(self);
    AtUnused(line);

    switch ((uint32) layers)
        {
        case cAtSdhLineDccLayerSection: return cAtTrue;
        case cAtSdhLineDccLayerLine   : return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool ShouldCompareTuSsBit(AtModuleSdh self, AtSdhPath tu)
    {
    AtUnused(self);
    AtUnused(tu);
    return cAtTrue;
    }

static eBool NeedClearanceNotifyLogic(AtModuleSdh self)
    {
    AtUnused(self);
    return Tha602900xxNeedClearanceNotifyLogic();
    }

static eBool ShouldUpdateTimRdiBackwardWhenAisDownstreamChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldUpdatePlmMonitorWhenExpectedPslChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtSdhLine LineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 localLineInSlice)
    {
    uint8 lineId;

    AtUnused(sliceId);
    if ((phyModule == cAtModuleSdh) || (phyModule == cAtModuleSur))
        {
        lineId = (uint8)(localLineInSlice);
        return AtModuleSdhLineGet((AtModuleSdh)self, lineId);
        }

    return NULL;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, NumLinesPerPart);
        mMethodOverride(m_ThaModuleSdhOverride, MaxNumDccHdlcChannels);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldUpdateTimRdiBackwardWhenAisDownstreamChange);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldUpdatePlmMonitorWhenExpectedPslChange);
        mMethodOverride(m_ThaModuleSdhOverride, LineFromHwIdGet);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        mMethodOverride(m_AtModuleSdhOverride, DccChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, DccLayerIsSupported);
        mMethodOverride(m_AtModuleSdhOverride, ShouldCompareTuSsBit);
        mMethodOverride(m_AtModuleSdhOverride, NeedClearanceNotifyLogic);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleSdh);
    }

AtModuleSdh Tha60290011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60290011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleSdhObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290011SdhLineInternal.h
 * 
 * Created Date: Sep 9, 2017
 *
 * Description : SDH Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SDHLINEINTERNAL_H_
#define _THA60290011SDHLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031SdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011SdhLine
    {
    tTha60210031SdhLine super;
    }tTha60290011SdhLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine Tha60290011SdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SDHLINEINTERNAL_H_ */


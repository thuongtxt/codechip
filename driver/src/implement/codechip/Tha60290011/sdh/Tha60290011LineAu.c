/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290011LineAu.c
 *
 * Created Date: Nov 14, 2017
 *
 * Description : AU implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290011SdhAuInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60290011ModuleOcnReg.h"
#include "Tha60290011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods              *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    uint32 offset = Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    return localAddress + offset;
    }

static uint32 PiPointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_upstschstkram_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 events = 0;

    if (regVal & cAf6_upstschstkram_StsPiStsNewDetIntr_Mask)
        events |= cAtSdhPathAlarmNewPointer;

    if (regVal & cAf6_upstschstkram_StsPiStsNdfIntr_Mask)
        events |= cAtSdhPathAlarmPiNdf;

    if (read2Clear)
        {
        regVal = cAf6_upstschstkram_StsPiStsNewDetIntr_Mask |
                 cAf6_upstschstkram_StsPiStsNdfIntr_Mask;
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return events;
    }

static uint32 PgPointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_stspgstkram_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 events = 0;

    if (regVal & cAf6_stspgstkram_StsPgNdfIntr_Mask)
        events |= cAtSdhPathAlarmPgNdf;

    if (read2Clear)
        mChannelHwWrite(self, regAddr, cAf6_stspgstkram_StsPgNdfIntr_Mask, cThaModuleOcn);

    return events;
    }

static uint32 PointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 piEvents = PiPointerEventRead2Clear(self, read2Clear);
    uint32 pgEvents = PgPointerEventRead2Clear(self, read2Clear);
    return piEvents | pgEvents;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 defects = 0;

    defects |= m_AtChannelMethods->DefectHistoryGet(self);
    defects |= PointerEventRead2Clear(self, cAtFalse);

    return defects;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 defects = 0;

    defects |= m_AtChannelMethods->DefectHistoryClear(self);
    defects |= PointerEventRead2Clear(self, cAtTrue);

    return defects;
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }


static void Override(AtSdhAu self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011LineAu);
    }

AtSdhAu Tha60290011SdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha60290011LineAuNew(uint32 auId, uint8 auType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011SdhAuObjectInit(self, auId, auType, module);
    }

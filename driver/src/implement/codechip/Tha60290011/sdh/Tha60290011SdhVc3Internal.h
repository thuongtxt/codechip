/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290011SdhVc3Internal.h
 * 
 * Created Date: Aug 14, 2017
 *
 * Description : SDH VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SDHVC3INTERNAL_H_
#define _THA60290011SDHVC3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031SdhVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011SdhVc3
    {
    tTha60210031SdhVc super;
    }tTha60290011SdhVc3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60290011SdhVc3ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SDHVC3INTERNAL_H_ */


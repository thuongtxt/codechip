/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290011ModulePdhInternal.h
 *
 * Created Date: Aug 02, 2017
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULESDHINTERNAL_H_
#define _THA60290011MODULESDHINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031ModuleSdhInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleSdh
    {
    tTha60210031ModuleSdh super;
    } tTha60290011ModuleSdh;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60290011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULESDHINTERNAL_H_ */

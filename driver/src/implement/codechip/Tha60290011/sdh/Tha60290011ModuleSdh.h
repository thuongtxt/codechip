/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290011ModuleSdh.h
 * 
 * Created Date: Oct 5, 2016
 *
 * Description : Tha60290011ModuleSdh declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULESDH_H_
#define _THA60290011MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha60290011LineAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhVc Tha60290011SdhTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60290011SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60290011SerialDe1Vc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60290011SdhVc3New(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULESDH_H_ */


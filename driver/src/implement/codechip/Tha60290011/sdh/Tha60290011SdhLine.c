/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290011SdhLine.c
 *
 * Created Date: Oct 6, 2016 
 *
 * Description : Implementation of Sdh Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011SdhLineInternal.h"
#include "AtPdhSerialLine.h"
#include "../../Tha60210031/ocn/Tha60210031ModuleOcn.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../pdh/Tha60290011ModulePdhMuxReg.h"
#include "../pdh/Tha60290011ModulePdh.h"
#include "../ocn/Tha60290011ModuleOcn.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mLineToHwId(_lineId) (_lineId * 32UL)
#define mThaModulePdh(line) ((ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh))
#define cAf6_tfmramctl_OCNRxAisLFrc_Mask  cBit9
#define cAf6_tfmramctl_OCNRxAisLFrc_Shift 9

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods          m_AtChannelOverride;
static tAtSdhChannelMethods       m_AtSdhChannelOverride;
static tAtSdhLineMethods          m_AtSdhLineOverride;
static tThaSdhLineMethods         m_ThaSdhLineOverride;
static tTha60210031SdhLineMethods m_Tha60210031SdhLineOverride;

/* Save Supper Implementation */
static const tAtChannelMethods          *m_AtChannelMethods          = NULL;
static const tAtSdhChannelMethods       *m_AtSdhChannelMethods       = NULL;
static const tAtSdhLineMethods          *m_AtSdhLineMethods          = NULL;
static const tTha60210031SdhLineMethods *m_Tha60210031SdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint32 lineId = AtChannelIdGet(self);
    ThaModulePdh modulePdh = mThaModulePdh(self);
    return Tha60290011ModulePdhTxHwLineEnable(modulePdh, mLineToHwId(lineId), enable);
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 lineId = AtChannelIdGet(self);
    return Tha60290011ModulePdhTxHwLineIsEnabled(mThaModulePdh(self), mLineToHwId(lineId));
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 lineId = AtChannelIdGet(self);
    ThaModulePdh modulePdh = mThaModulePdh(self);

    return Tha60290011ModulePdhRxHwLineEnable(modulePdh, lineId, enable);
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 lineId = AtChannelIdGet(self);
    return Tha60290011ModulePdhRxHwLineIsEnabled(mThaModulePdh(self), lineId);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret;
    ThaModulePdh modulePdh = mThaModulePdh(self);
    uint32 lineId = AtChannelIdGet(self);
    AtPdhSerialLine serLine = AtModulePdhDe3SerialLineGet((AtModulePdh)modulePdh, lineId);

    if (AtPdhSerialLineModeGet(serLine) != cAtPdhDe3SerialLineModeEc1)
        return cAtOk;

    ret = m_AtChannelMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    AtUnused(self);
    if ((timingMode == cAtTimingModeSys)  ||
        (timingMode == cAtTimingModeLoop) ||
        (timingMode == cAtTimingModeExt1Ref))
        return cAtTrue;

    return cAtFalse;
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    return Tha60210031OcnLineTimingModeGet(self);
    }

static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
	if (ThaSdhChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhLineCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtFalse);

	return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
	if (ThaSdhChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhLineCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtTrue);
	return m_AtChannelMethods->CounterClear(self, counterType);
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
	{
	if (ThaSdhChannelShouldGetCounterFromPmc((AtChannel)self))
        return ThaModulePmcSdhLineBlockErrorCounterGet(PmcModule((AtChannel)self), self, counterType, cAtFalse);
	return m_AtSdhChannelMethods->BlockErrorCounterGet(self, counterType);
	}

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
	{
	if (ThaSdhChannelShouldGetCounterFromPmc((AtChannel)self))
        return ThaModulePmcSdhLineBlockErrorCounterGet(PmcModule((AtChannel)self), self, counterType, cAtTrue);
	return m_AtSdhChannelMethods->BlockErrorCounterClear(self, counterType);
	}

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(loopbackMode);
    return Tha60290011ModuleOcnNewLoopbackIsSupported(ModuleOcn(self));
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhLineAlarmAis;
    }

static eBool CanForceRxAlarms(ThaSdhLine self, uint32 alarmType)
    {
    return (alarmType & AtChannelRxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static uint32 TohMonitoringChannelControlRegister(AtChannel self)
    {
    Tha60210031SdhLine sdhLine = (Tha60210031SdhLine)self;
    return mMethodsGet(sdhLine)->TohMonitoringChannelControlRegAddr(sdhLine) +
           mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    }

static eAtRet HelperRxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    uint32 regVal, address;
    eBool canForce = CanForceRxAlarms((ThaSdhLine)self, alarmType);

    if (canForce == cAtFalse)
        return (force) ? cAtErrorModeNotSupport : cAtOk;

    address = TohMonitoringChannelControlRegister(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (alarmType & cAtSdhLineAlarmAis)
        mRegFieldSet(regVal, cAf6_tfmramctl_OCNRxAisLFrc_, mBoolToBin(force));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr = TohMonitoringChannelControlRegister(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 forcedAlarms = 0;

    if (regVal & cAf6_tfmramctl_OCNRxAisLFrc_Mask)
        forcedAlarms |= cAtSdhLineAlarmAis;

    return forcedAlarms;
    }

static eBool ShouldHideLofWhenLos(Tha60210031SdhLine self)
    {
    return Tha602900xxSdhLineShouldHideLofWhenLos((AtSdhLine)self);
    }

static eBool TxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    return Tha60290011ModuleOcnTohByteIsSupported(self, overheadByte);
    }

static eBool RxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    return Tha60290011ModuleOcnTohByteIsSupported(self, overheadByte);
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 value)
    {
    if (TxOverheadByteIsSupported(self, overheadByte))
        return Tha60290011ModuleOcnFpgaLineTxOhByteSet(self, overheadByte, value);

    return cAtErrorModeNotSupport;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    if (TxOverheadByteIsSupported(self, overheadByte))
        return Tha60290011ModuleOcnFpgaLineTxOhByteGet(self, overheadByte);

    return 0;
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    if (RxOverheadByteIsSupported(self, overheadByte))
        return Tha60290011ModuleOcnFpgaLineRxOhByteGet(self, overheadByte);

    return 0;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    if (Tha60290011DeviceTohSonetSdhModeIsSupported(device))
        {
        eAtRet ret = Tha60290011ModuleOcnEc1LineModeSet(ModuleOcn((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self), mode);
        if (ret != cAtOk)
            return ret;
        }

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    if (Tha60290011DeviceTohSonetSdhModeIsSupported(device))
        return Tha60290011ModuleOcnEc1LineModeGet(ModuleOcn((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self));

    return m_AtSdhChannelMethods->ModeGet(self);
    }

static void OverrideTha60210031SdhLine(AtSdhLine self)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031SdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineOverride, m_Tha60210031SdhLineMethods, sizeof(m_Tha60210031SdhLineOverride));

        mMethodOverride(m_Tha60210031SdhLineOverride, ShouldHideLofWhenLos);
        }

    mMethodsSet(line, &m_Tha60210031SdhLineOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
		mMethodOverride(m_AtChannelOverride, CounterClear);
		mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    AtSdhLine line = (AtSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, ScrambleEnable);
        mMethodOverride(m_AtSdhLineOverride, ScrambleIsEnabled);
        }

    mMethodsSet(line, &m_AtSdhLineOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhLine(self);
    OverrideThaSdhLine(self);
    OverrideTha60210031SdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SdhLine);
    }

AtSdhLine Tha60290011SdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60290011SdhLineNew(uint32 channelId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011SdhLineObjectInit(newLine, channelId, module);
    }

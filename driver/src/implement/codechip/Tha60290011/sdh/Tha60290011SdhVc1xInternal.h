/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290011SdhVc1xInternal.h
 * 
 * Created Date: Sep 09, 2017
 *
 * Description : SDH VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SDHVC1XINTERNAL_H_
#define _THA60290011SDHVC1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031SdhVc1xInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011SdhVc1x
    {
    tTha60210031SdhVc1x super;
    }tTha60290011SdhVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60290011SdhVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SDHVC1XINTERNAL_H_ */


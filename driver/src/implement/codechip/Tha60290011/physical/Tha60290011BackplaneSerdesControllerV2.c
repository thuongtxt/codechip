/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290011BackplaneSerdesControllerV2.c
 *
 * Created Date: Nov 9, 2017
 *
 * Description : Backplane serdes interface version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../Tha60290021/prbs/Tha6029SerdesPrbsEngine.h"
#include "../../Tha60290021/physical/Tha6029BackplaneSerdesControllerInternal.h"
#include "../physical/Tha60290011Physical.h"
#include "../eth/Tha60290011SerdesBackplaneEthPortReg.h"
#include "../man/Tha60290011DeviceReg.h"
#include "../prbs/Tha60290011XfiSerdesPrbsEngine.h"
#include "Tha60290011SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011BackplaneSerdesControllerV2
    {
    tTha6029BackplaneSerdesController super;
    }tTha60290011BackplaneSerdesControllerV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                    m_AtObjectOverride;
static tAtSerdesControllerMethods          m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth10G;
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth10G) ? cAtTrue : cAtFalse;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth10G;
    }

static uint32 NumLanes(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static AtSerdesController LaneObjectCreate(AtSerdesController self, uint32 laneIdx)
    {
    AtUnused(self);
    AtUnused(laneIdx);
    return NULL;
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static const char *IdString(AtSerdesController self)
    {
    return AtSerdesControllerDefaultIdString(self);
    }

static uint32 BaseAddress(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 localId = Tha60290011SerdesManagerBackplaneSerdesLocalId(manager, serdesId);

    if (localId == 0)
        return cXfiActSerdesTuningBaseAddress;

    if (localId == 1)
        return cXfiStbSerdesTuningBaseAddress;

    return cInvalidUint32;
    }

static uint32 HwIdGet(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);

    if (serdesId == cTha60290011XfiSerdesId0)       return 0;
    if (serdesId == cTha60290011XfiSerdesId1)       return 1;

    return cInvalidUint32;
    }

static uint32 RegisterWithLocalAddress(AtSerdesController self, uint32 localAddress)
    {
    return AtSerdesControllerBaseAddress(self) + localAddress;
    }

static uint32 DrpBaseAddress(AtSerdesController self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_OETH_10G_DRP_Base);
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    AtDrp drp = Tha60290011DrpNew(DrpBaseAddress(self), hal, self);
    AtDrpNeedSelectPortSet(drp, cAtFalse);
    return drp;
    }

static uint8 LoopbackSw2Hw(uint8 swVal)
    {
    if (swVal == cAtLoopbackModeRelease)
        return 0;
    if (swVal == cAtLoopbackModeLocal)
        return 2;
    if (swVal == cAtLoopbackModeRemote)
        return 4;
    return 0;
    }

static uint8 LoopbackHw2Sw(uint8 hwVal)
    {
    if (hwVal == 0)
        return cAtLoopbackModeRelease;
    if (hwVal == 2)
        return cAtLoopbackModeLocal;
    if (hwVal == 4)
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static eAtRet LoopbackSet(AtSerdesController self, uint8 loopbackMode)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, cAf6Reg_ETH_10G_LoopBack_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_10G_LoopBack_lpback_lane0_, LoopbackSw2Hw(loopbackMode));
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 LoopbackGet(AtSerdesController self)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, cAf6Reg_ETH_10G_LoopBack_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint8 hwLoopVal = (uint8)mRegField(regVal, cAf6_ETH_10G_LoopBack_lpback_lane0_);

    return LoopbackHw2Sw(hwLoopVal);
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    eAtRet ret = cAtOk;
    eBool currentEnabled = AtSerdesControllerLoopbackIsEnabled(self, loopbackMode);

    if (enable == currentEnabled)
        return cAtOk;

    if (!enable && (loopbackMode != (uint16)LoopbackGet(self)))
        return cAtOk;

    if (enable)
        ret = LoopbackSet(self, loopbackMode);
    else
        ret = LoopbackSet(self, cAtLoopbackModeRelease);

    /* XILINX guideline that SERDES reset needs to be done on loopback operation.
     * Only reset SERDES when it is powerup, otherwise, timeout error code will be returned */
    if (!AtSerdesControllerPowerIsDown(self))
        ret |= AtSerdesControllerReset(self);

    return ret;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    uint16 currentLoopMode = (uint16)LoopbackGet(self);

    if (loopbackMode == currentLoopMode)
        return cAtTrue;
    return cAtFalse;
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    AtDevice dev = AtChannelDeviceGet(channel);
    static char description[64];

    uint8 localIdx = (uint8)(AtSerdesControllerIdGet((AtSerdesController)self));
    AtSnprintf(description, sizeof(description), "%sbackplane_serdes.%d", AtDeviceIdToString(dev), localIdx + 1);
    return description;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60290011BackplaneSerdesPrbsEngineV2New(self);
    }

static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    return (AtChannel)AtModuleEthPortGet(ModuleEth(self), 0);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerGthE3New(self, drpBaseAddress);
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    return (float)(4 * 3.125);
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);

        /* For EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);

        mMethodOverride(m_AtSerdesControllerOverride, NumLanes);
        mMethodOverride(m_AtSerdesControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, IdString);
        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, DrpObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011BackplaneSerdesControllerV2));

    /* Super constructor */
    if (Tha6029BackplaneSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290011BackplaneSerdesControllerV2New(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60290011BackplaneSerdesControllerV2));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, physicalPort, serdesId, manager);
    }

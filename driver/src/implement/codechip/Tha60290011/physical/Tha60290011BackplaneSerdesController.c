/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes controller
 *
 * File        : Tha60290011BackplaneSerdesController.c
 *
 * Created Date: Aug 12, 2016
 *
 * Description : Backplane Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../Tha60290021/prbs/Tha6029SerdesPrbsEngine.h"
#include "../man/Tha60290011DeviceReg.h"
#include "Tha60290011SerdesControllerInternal.h"
#include "Tha60290011SerdesManager.h"
#include "../prbs/Tha60290011XfiSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011BackplaneSerdesController
    {
    tTha60290011SerdesController super;
    }tTha60290011BackplaneSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                    m_AtObjectOverride;
static tAtSerdesControllerMethods          m_AtSerdesControllerOverride;
static tTha60290011SerdesControllerMethods m_Tha60290011SerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth10G) ? cAtTrue : cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    return ModeIsSupported(self, mode) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth10G;
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    AtDevice dev = AtChannelDeviceGet(channel);
    static char description[64];

    uint8 localIdx = (uint8)(AtSerdesControllerIdGet((AtSerdesController)self));
    AtSnprintf(description, sizeof(description), "%sbackplane_serdes.%d", AtDeviceIdToString(dev), localIdx + 1);
    return description;
    }

static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    return (AtChannel)AtModuleEthPortGet(ModuleEth(self), 0);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerGthE3New(self, drpBaseAddress);
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    return (float)(4 * 3.125);
    }

static uint32 TuningBaseAddress(Tha60290011SerdesController self)
    {
    AtSerdesController serdes = (AtSerdesController)self;
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    uint32 localId = Tha60290011SerdesManagerBackplaneSerdesLocalId(manager, serdesId);

    if (localId == 0)
        return cXfiActSerdesTuningBaseAddress;

    if (localId == 1)
        return cXfiStbSerdesTuningBaseAddress;

    return cInvalidUint32;
    }

static uint32 DrpLocalBaseAddress(Tha60290011SerdesController self)
    {
    AtUnused(self);
    return 0x400; /* See Xilinx_Serdes_Turning_1x.atreg */
    }

static uint32 PrbsBaseAddress(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 localId = Tha60290011SerdesManagerBackplaneSerdesLocalId(manager, serdesId);

    if (localId == 0)
        return cXfiActDiagBaseAddress;

    if (localId == 1)
        return cXfiStbDiagBaseAddress;

    return cInvalidUint32;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60290011XfiSerdesPrbsEngineNew(self, PrbsBaseAddress(self));
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static void OverrideTha60290011SerdesController(AtSerdesController self)
    {
    Tha60290011SerdesController serdes = (Tha60290011SerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011SerdesControllerOverride, mMethodsGet(serdes), sizeof(m_Tha60290011SerdesControllerOverride));

        mMethodOverride(m_Tha60290011SerdesControllerOverride, TuningBaseAddress);
        mMethodOverride(m_Tha60290011SerdesControllerOverride, DrpLocalBaseAddress);
        }

    mMethodsSet(serdes, &m_Tha60290011SerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);

        /* For EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha60290011SerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011BackplaneSerdesController));

    /* Super constructor */
    if (Tha60290011SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290011BackplaneSerdesControllerNew(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60290011BackplaneSerdesController));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, physicalPort, serdesId, manager);
    }

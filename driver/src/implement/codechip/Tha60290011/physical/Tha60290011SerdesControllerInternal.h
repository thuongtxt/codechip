/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290011SerdesControllerInternal.h
 * 
 * Created Date: Aug 18, 2016
 *
 * Description : Serdes controller internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SERDESCONTROLLERINTERNAL_H_
#define _THA60290011SERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/physical/Tha6029FaceplateSerdesControllerInternal.h"
#include "../../Tha60290021/physical/Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011SerdesController * Tha60290011SerdesController;

typedef struct tTha60290011SerdesControllerMethods
    {
    uint32 (*TuningBaseAddress)(Tha60290011SerdesController self);
    uint32 (*DrpLocalBaseAddress)(Tha60290011SerdesController self);
    }tTha60290011SerdesControllerMethods;

typedef struct tTha60290011SerdesController
    {
    tTha6029SerdesController super;
    const tTha60290011SerdesControllerMethods *methods;
    }tTha60290011SerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60290011SerdesControllerObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SERDESCONTROLLERINTERNAL_H_ */

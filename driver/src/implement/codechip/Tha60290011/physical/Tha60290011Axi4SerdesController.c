/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes controller
 *
 * File        : Tha60290011Axi4SerdesController.c
 *
 * Created Date: Aug 12, 2016
 *
 * Description : Axi4 Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/physical/Tha6029Physical.h"
#include "../prbs/Tha60290011SgmiiSerdesPrbsEngine.h"
#include "../man/Tha60290011DeviceReg.h"
#include "../man/Tha60290011DeviceInternal.h"
#include "Tha60290011SerdesControllerInternal.h"
#include "Tha60290011SerdesManager.h"
#include "AtModuleEth.h"
#include "AtEyeScanController.h"

/*--------------------------- Define -----------------------------------------*/
#define cAxi4SerdesPrbsEngineId 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011Axi4SerdesController
    {
    tTha60290011SerdesController super;
    }tTha60290011Axi4SerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                    m_AtObjectOverride;
static tAtSerdesControllerMethods          m_AtSerdesControllerOverride;
static tTha60290011SerdesControllerMethods m_Tha60290011SerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth2500M) ? cAtTrue : cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    return ModeIsSupported(self, mode) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth2500M;
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[64];

    AtSprintf(str, "%s2500BaseX", AtDeviceIdToString(dev));
    return str;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);

    if (AtSerdesControllerModeGet(self) != cAtSerdesModeEth2500M)
        return NULL;

    if (Tha60290011Device2Dot5GitSerdesPrbsIsRemoved(device))
        return NULL;

    return Tha60290011SgmiiSerdesPrbsEngineWithHwIdNew(self, cAxi4SerdesPrbsEngineId);
    }

static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    return (AtChannel)AtModuleEthPortGet(ModuleEth(self), 1);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    return AtSerdesControllerBaseAddress(self) + 0x200UL;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerGthE3New(self, drpBaseAddress);
    }

static uint32 TuningBaseAddress(Tha60290011SerdesController self)
    {
    AtUnused(self);
    return cSgmii2500MSerdesTuningBaseAddress;
    }

static uint32 DrpLocalBaseAddress(Tha60290011SerdesController self)
    {
    AtUnused(self);
    return 0x200UL; /* See Xilinx_Serdes_Turning_1x.atreg */
    }

static uint32 AlarmGet(AtSerdesController self)
	{
	return Tha60290011SgmiiSerdesLinkAlarmGet(self);
	}

static uint32 AlarmHistoryGet(AtSerdesController self)
	{
	return Tha60290011SgmiiSerdesLinkAlarmHistoryGet(self, cAtFalse);
	}

static uint32 AlarmHistoryClear(AtSerdesController self)
	{
	return Tha60290011SgmiiSerdesLinkAlarmHistoryGet(self, cAtTrue);
	}

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
	{
	return (Tha60290011SgmiiSerdesLinkAlarmGet(self) == 0) ? cAtSerdesLinkStatusUp: cAtSerdesLinkStatusDown;
	}

static eAtRet Reset(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtSerdesControllerMethods->Reset(self);
    ret |= Tha6029SgmiiElectricallyIsolatePHYFromGMII(self, cAtFalse);

    return ret;
    }

static void OverrideTha60290011SerdesController(AtSerdesController self)
    {
    Tha60290011SerdesController serdes = (Tha60290011SerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011SerdesControllerOverride, mMethodsGet(serdes), sizeof(m_Tha60290011SerdesControllerOverride));

        mMethodOverride(m_Tha60290011SerdesControllerOverride, TuningBaseAddress);
        mMethodOverride(m_Tha60290011SerdesControllerOverride, DrpLocalBaseAddress);
        }

    mMethodsSet(serdes, &m_Tha60290011SerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha60290011SerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011Axi4SerdesController));

    /* Super constructor */
    if (Tha60290011SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290011Axi4SerdesControllerNew(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60290011Axi4SerdesController));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, physicalPort, serdesId, manager);
    }

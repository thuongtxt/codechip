/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : Tha60290011SemController.h
 * 
 * Created Date: Oct 21, 2016
 *
 * Description : Tha60290011SemController declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SEMCONTROLLER_H_
#define _THA60290011SEMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60290011SemControllerNew(AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SEMCONTROLLER_H_ */


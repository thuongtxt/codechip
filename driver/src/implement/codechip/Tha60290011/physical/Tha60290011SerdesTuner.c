/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290011SerdesTuner.c
 *
 * Created Date: Oct 22, 2016
 *
 * Description : SERDES tuner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/physical/Tha6029SerdesTunerInternal.h"
#include "Tha60290011Physical.h"
#include "../../../../util/coder/AtCoderUtil.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011SerdesTuner *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011SerdesTuner
    {
    tTha6029SerdesTuner super;

    /* As HW assigns each tuner different base address */
    uint32 baseAddress;
    }tTha60290011SerdesTuner;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6029SerdesTunerMethods m_Tha6029SerdesTunerOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LocalId(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AbsoluteOffset(Tha6029SerdesTuner self)
    {
    return mThis(self)->baseAddress;
    }

static uint32 TxPowerDownMask(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cBit3_2;
    }

static uint32 TxPowerDownShift(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return 2;
    }

static AtDrp DrpObjectCreate(Tha6029SerdesTuner self, uint32 baseAddress, AtHal hal)
    {
    AtSerdesController serdes = Tha6029SerdesTunerSerdesGet(self);
    return Tha60290011DrpNew(baseAddress, hal, serdes);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290011SerdesTuner* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(baseAddress);
    }

static void OverrideAtObject(Tha6029SerdesTuner self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha6029SerdesTuner(Tha6029SerdesTuner self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesTunerOverride, mMethodsGet(self), sizeof(m_Tha6029SerdesTunerOverride));

        mMethodOverride(m_Tha6029SerdesTunerOverride, AbsoluteOffset);
        mMethodOverride(m_Tha6029SerdesTunerOverride, LocalId);
        mMethodOverride(m_Tha6029SerdesTunerOverride, TxPowerDownMask);
        mMethodOverride(m_Tha6029SerdesTunerOverride, TxPowerDownShift);
        mMethodOverride(m_Tha6029SerdesTunerOverride, DrpObjectCreate);
        }

    mMethodsSet(self, &m_Tha6029SerdesTunerOverride);
    }

static void Override(Tha6029SerdesTuner self)
    {
    OverrideAtObject(self);
    OverrideTha6029SerdesTuner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SerdesTuner);
    }

static Tha6029SerdesTuner ObjectInit(Tha6029SerdesTuner self, AtSerdesController serdes, uint32 baseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesTunerObjectInit(self, serdes) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->baseAddress = baseAddress;

    return self;
    }

Tha6029SerdesTuner Tha60290011SerdesTunerNew(AtSerdesController serdes, uint32 baseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha6029SerdesTuner newTuner = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newTuner, serdes, baseAddress);
    }


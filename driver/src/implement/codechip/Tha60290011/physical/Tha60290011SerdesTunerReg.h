/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef __REG_Xilinx_Serdes_Turning_1x_H_
#define __REG_Xilinx_Serdes_Turning_1x_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : SERDES DRP PORT
Reg Addr   : XFI (0x400-0x7FF) - SGMII&Other (0x200-x3FF)
Reg Formula: XFI + $DRP
    Where  :
           + For XFI must control port enable by TOP register control
           + $DRP(0-1023) : DRP address, see UG578
Reg Desc   :
Read/Write DRP address of SERDES,

------------------------------------------------------------------------------*/
#define cReg_SERDES_DRP_PORT_Base                                                                          XFI

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [09:00]
--------------------------------------*/
#define c_SERDES_DRP_PORT_drp_rw_Mask                                                                  cBit9_0
#define c_SERDES_DRP_PORT_drp_rw_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LoopBack
Reg Addr   : 0x02
Reg Formula:
    Where  :
Reg Desc   :
Configurate LoopBack,

------------------------------------------------------------------------------*/
#define cReg_SERDES_LoopBack_Base                                                                         0x02

/*--------------------------------------
BitField Name: lpback_serdes
BitField Type: R/W
BitField Desc: Loop back mode
BitField Bits: [03:00]
--------------------------------------*/
#define c_SERDES_LoopBack_lpback_serdes_Mask                                                           cBit3_0
#define c_SERDES_LoopBack_lpback_serdes_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES POWER DOWN
Reg Addr   : 0x03
Reg Formula:
    Where  :
Reg Desc   :
Configurate Power Down ,

------------------------------------------------------------------------------*/
#define cReg_SERDES_POWER_DOWN_Base                                                                       0x03

/*--------------------------------------
BitField Name: TXELECIDLE
BitField Type: R/W
BitField Desc: TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port
BitField Bits: [04]
--------------------------------------*/
#define c_SERDES_POWER_DOWN_TXELECIDLE_Mask                                                              cBit4
#define c_SERDES_POWER_DOWN_TXELECIDLE_Shift                                                                 4

/*--------------------------------------
BitField Name: TXPD
BitField Type: R/W
BitField Desc: TX Power Down
BitField Bits: [03:02]
--------------------------------------*/
#define c_SERDES_POWER_DOWN_TXPD_Mask                                                                  cBit3_2
#define c_SERDES_POWER_DOWN_TXPD_Shift                                                                       2

/*--------------------------------------
BitField Name: RXPD
BitField Type: R/W
BitField Desc: RX Power Down
BitField Bits: [01:00]
--------------------------------------*/
#define c_SERDES_POWER_DOWN_RXPD_Mask                                                                  cBit1_0
#define c_SERDES_POWER_DOWN_RXPD_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TX Reset
Reg Addr   : 0x0C
Reg Formula:
    Where  :
Reg Desc   :
Reset TX SERDES,

------------------------------------------------------------------------------*/
#define cReg_SERDES_TX_Reset_Base                                                                         0x0C

/*--------------------------------------
BitField Name: txrst_done
BitField Type: RO
BitField Desc: TX Reset Done, bit per sub port
BitField Bits: [17:16]
--------------------------------------*/
#define c_SERDES_TX_Reset_txrst_done_Mask                                                            cBit17_16
#define c_SERDES_TX_Reset_txrst_done_Shift                                                                  16

/*--------------------------------------
BitField Name: txrst_en
BitField Type: R/W
BitField Desc: Should reset TX_PMA SERDES about 300-500 ns
BitField Bits: [01:00]
--------------------------------------*/
#define c_SERDES_TX_Reset_txrst_en_Mask                                                                cBit1_0
#define c_SERDES_TX_Reset_txrst_en_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SERDES RX Reset
Reg Addr   : 0x0D
Reg Formula:
    Where  :
Reg Desc   :
Reset RX SERDES,

------------------------------------------------------------------------------*/
#define cReg_SERDES_RX_Reset_Base                                                                         0x0D

/*--------------------------------------
BitField Name: rxrst_done
BitField Type: RO
BitField Desc: RX Reset Done
BitField Bits: [17:16]
--------------------------------------*/
#define c_SERDES_RX_Reset_rxrst_done_Mask                                                            cBit17_16
#define c_SERDES_RX_Reset_rxrst_done_Shift                                                                  16

/*--------------------------------------
BitField Name: rxrst_en
BitField Type: R/W
BitField Desc: Should reset reset RX_PMA SERDES about 300-500 ns
BitField Bits: [01:00]
--------------------------------------*/
#define c_SERDES_RX_Reset_rxrst_en_Mask                                                                cBit1_0
#define c_SERDES_RX_Reset_rxrst_en_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Mode
Reg Addr   : 0x0E
Reg Formula:
    Where  :
Reg Desc   :
Configure LPM/DFE mode ,

------------------------------------------------------------------------------*/
#define cReg_SERDES_LPMDFE_Mode_Base                                                                      0x0E

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: bit per sub port
BitField Bits: [00:00]
--------------------------------------*/
#define c_SERDES_LPMDFE_Mode_lpmdfe_mode_Mask                                                            cBit0
#define c_SERDES_LPMDFE_Mode_lpmdfe_mode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Reset
Reg Addr   : 0x0F
Reg Formula:
    Where  :
Reg Desc   :
Reset LPM/DFE ,

------------------------------------------------------------------------------*/
#define cReg_SERDES_LPMDFE_Reset_Base                                                                     0x0F

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: bit per sub port, Must be toggled after switching between modes
to initialize adaptation
BitField Bits: [00:00]
--------------------------------------*/
#define c_SERDES_LPMDFE_Reset_lpmdfe_reset_Mask                                                          cBit0
#define c_SERDES_LPMDFE_Reset_lpmdfe_reset_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXDIFFCTRL
Reg Addr   : 0x10
Reg Formula:
    Where  :
Reg Desc   :
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is  2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7

------------------------------------------------------------------------------*/
#define cReg_SERDES_TXDIFFCTRL_Base                                                                       0x10

/*--------------------------------------
BitField Name: TXDIFFCTRL
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define c_SERDES_TXDIFFCTRL_TXDIFFCTRL_Mask                                                            cBit4_0
#define c_SERDES_TXDIFFCTRL_TXDIFFCTRL_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPOSTCURSOR
Reg Addr   : 0x11
Reg Formula:
    Where  :
Reg Desc   :
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is  2 sub ports

------------------------------------------------------------------------------*/
#define cReg_SERDES_TXPOSTCURSOR_Base                                                                     0x11

/*--------------------------------------
BitField Name: TXPOSTCURSOR
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define c_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_Mask                                                        cBit4_0
#define c_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPRECURSOR
Reg Addr   : 0x12
Reg Formula:
    Where  :
Reg Desc   :
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports

------------------------------------------------------------------------------*/
#define cReg_SERDES_TXPRECURSOR_Base                                                                      0x12

/*--------------------------------------
BitField Name: TXPRECURSOR
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define c_SERDES_TXPRECURSOR_TXPRECURSOR_Mask                                                          cBit4_0
#define c_SERDES_TXPRECURSOR_TXPRECURSOR_Shift                                                               0

#endif /* __REG_Xilinx_Serdes_Turning_1x_H_ */

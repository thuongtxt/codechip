/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290011Drp.c
 *
 * Created Date: Oct 22, 2016
 *
 * Description : DRP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesController.h"
#include "AtDrp.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "Tha60290011Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011Drp *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011Drp
    {
    tAtDrp super;

    /* Private data */
    AtSerdesController serdes;
    }tTha60290011Drp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDrpMethods m_AtDrpOverride;

/* Save super implementation */
static const tAtDrpMethods *m_AtDrpMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DrpAllow(AtDrp self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(mThis(self)->serdes);
    AtSerdesManager manager = AtSerdesControllerManagerGet(mThis(self)->serdes);
    Tha60290011SerdesManagerDrpAllow(manager, serdesId, cAtTrue);
    }

static uint32 Read(AtDrp self, uint32 address)
    {
    DrpAllow(self);
    return m_AtDrpMethods->Read(self, address);
    }

static eAtRet Write(AtDrp self, uint32 address, uint32 value)
    {
    DrpAllow(self);
    return m_AtDrpMethods->Write(self, address, value);
    }

static void OverrideAtDrp(AtDrp self)
    {
    if (!m_methodsInit)
        {
        m_AtDrpMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtDrpOverride, m_AtDrpMethods, sizeof(m_AtDrpOverride));

        mMethodOverride(m_AtDrpOverride, Read);
        mMethodOverride(m_AtDrpOverride, Write);
        }

    mMethodsSet(self, &m_AtDrpOverride);
    }

static void Override(AtDrp self)
    {
    OverrideAtDrp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011Drp);
    }

static AtDrp ObjectInit(AtDrp self, uint32 baseAddress, AtHal hal, AtSerdesController serdes)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDrpObjectInit(self, baseAddress, hal) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->serdes = serdes;

    return self;
    }

AtDrp Tha60290011DrpNew(uint32 baseAddress, AtHal hal, AtSerdesController serdes)
    {
    AtDrp newDrp = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newDrp, baseAddress, hal, serdes);
    }

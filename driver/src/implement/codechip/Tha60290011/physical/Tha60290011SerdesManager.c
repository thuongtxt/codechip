/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes manager
 *
 * File        : Tha60290011SerdesManager.c
 *
 * Created Date: Aug 11, 2016
 *
 * Description : Serdes manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/physical/Tha6029SerdesManagerInternal.h"
#include "../man/Tha60290011DeviceReg.h"
#include "Tha60290011SerdesManager.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011SerdesManager *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011SerdesManager
    {
    tTha6029SerdesManager super;

    /* Private data */
    eBool xfi0DrpEnabled;
    eBool xfi1DrpEnabled;
    }tTha60290011SerdesManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesManagerMethods m_AtSerdesManagerOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSerdesControllers(AtSerdesManager self)
    {
    AtUnused(self);
    return 6;
    }

static eBool SerdesIsBackplane(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    return ((serdesId == 0) || (serdesId == 1)) ? cAtTrue : cAtFalse;
    }

static AtSerdesController BackplaneSerdesController(AtSerdesManager self, uint32 localSerdesId)
    {
    if ((localSerdesId == 0) || (localSerdesId == 1))
        return AtSerdesManagerSerdesControllerGet(self, localSerdesId);
    return NULL;
    }

static eBool IsAxi4Serdes(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    return (serdesId == 2) ? cAtTrue : cAtFalse;
    }

static AtSerdesController Axi4SerdesController(AtSerdesManager self)
    {
    return AtSerdesManagerSerdesControllerGet(self, 2);
    }

static uint32 SgmiiSerdesStartId(AtSerdesManager self)
    {
    AtUnused(self);
    return 3;
    }

static eBool IsSgmiiSerdes(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    return (serdesId >= SgmiiSerdesStartId(self)) ? cAtTrue : cAtFalse;
    }

static AtSerdesController SgmiiSerdesController(AtSerdesManager self, uint32 localSerdesId)
    {
    return AtSerdesManagerSerdesControllerGet(self, localSerdesId + 3);
    }

static eBool IsNewSerdesBackplaneImplemented(AtSerdesManager self)
    {
    AtDevice dev = AtSerdesManagerDeviceGet(self);
    return Tha60290011DeviceIsMroSerdesBackplaneImplemented(dev);
    }

static AtSerdesController BackplaneSerdesCreate(AtSerdesManager self, uint32 serdesId)
    {
    if (IsNewSerdesBackplaneImplemented(self))
        return Tha60290011BackplaneSerdesControllerV2New(NULL, serdesId, self);

    return Tha60290011BackplaneSerdesControllerNew(NULL, serdesId, self);
    }

static AtSerdesController SerdesControllerObjectCreate (AtSerdesManager self, uint32 serdesId)
    {
    if (SerdesIsBackplane(self, serdesId))
        return BackplaneSerdesCreate(self, serdesId);

    if (IsAxi4Serdes(self, serdesId))
        return Tha60290011Axi4SerdesControllerNew(NULL, serdesId, self);

    if (IsSgmiiSerdes(self, serdesId))
        return Tha60290011SgmiiSerdesControllerNew(NULL, serdesId, self);

    return NULL;
    }

static uint32 SgmiiSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    return serdesId - SgmiiSerdesStartId(self);
    }

static uint32 BackplaneSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    return serdesId;
    }

static eBool BackplaneDrpIsAllowed(AtSerdesManager self, uint32 serdesId)
    {
    if (serdesId == 0)
        return mThis(self)->xfi0DrpEnabled;
    if (serdesId == 1)
        return mThis(self)->xfi1DrpEnabled;

    return cAtFalse;
    }

static eAtRet BackplaneDrpAllow(AtSerdesManager self, uint32 serdesId, eBool allow)
    {
    uint32 regAddr = cAf6Reg_o_control4_Base + cTopBaseAddress;
    AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(self, serdesId);
    uint32 regVal;

    if ((BackplaneDrpIsAllowed(self, serdesId) ? 1 : 0) == (allow ? 1 : 0))
        return cAtOk;

    regVal = AtSerdesControllerRead(serdes, regAddr, cAtModuleEth);

    if (serdesId == 0)
        {
        mRegFieldSet(regVal, cAf6_o_control4_xfi0_drp_en_, allow ? 1 : 0);
        mThis(self)->xfi0DrpEnabled = allow;
        }

    if (serdesId == 1)
        {
        mRegFieldSet(regVal, cAf6_o_control4_xfi1_drp_en_, allow ? 1 : 0);
        mThis(self)->xfi1DrpEnabled = allow;
        }

    AtSerdesControllerWrite(serdes, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet AllBackplaneDrpDisAllow(AtSerdesManager self)
    {
    eAtRet ret = cAtOk;

    ret |= BackplaneDrpAllow(self, 0, cAtFalse);
    ret |= BackplaneDrpAllow(self, 1, cAtFalse);

    return ret;
    }

static eAtRet DrpAllow(AtSerdesManager self, uint32 serdesId, eBool allow)
    {
    if (SerdesIsBackplane(self, serdesId))
        return BackplaneDrpAllow(self, serdesId, allow);

    /* Need to disable DRP on XFI so that DRP can be accessed on SGMII */
    if (allow)
        return AllBackplaneDrpDisAllow(self);

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290011SerdesManager* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(xfi0DrpEnabled);
    mEncodeUInt(xfi1DrpEnabled);
    }

static void OverrideAtObject(AtSerdesManager self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesManager(AtSerdesManager self)
    {
    AtSerdesManager object = (AtSerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesManagerOverride, mMethodsGet(object), sizeof(m_AtSerdesManagerOverride));

        mMethodOverride(m_AtSerdesManagerOverride, NumSerdesControllers);
        mMethodOverride(m_AtSerdesManagerOverride, SerdesControllerObjectCreate);
        }

    mMethodsSet(object, &m_AtSerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesManager(self);
    }

static AtSerdesManager ObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011SerdesManager));

    /* Super constructor */
    if (Tha6029SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha60290011SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60290011SerdesManager));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

AtSerdesController Tha60290011SerdesManagerBackplaneSerdesController(AtSerdesManager self, uint32 localSerdesId)
    {
    if (self)
        return BackplaneSerdesController(self, localSerdesId);
    return NULL;
    }

AtSerdesController Tha60290011SerdesManagerAxi4SerdesController(AtSerdesManager self)
    {
    if (self)
        return Axi4SerdesController(self);
    return NULL;
    }

AtSerdesController Tha60290011SerdesManagerSgmiiSerdesController(AtSerdesManager self, uint32 localSerdesId)
    {
    if (self)
        return SgmiiSerdesController(self, localSerdesId);
    return NULL;
    }

char *Tha60290011SerdesManagerSgmiiDescription(AtSerdesManager self, uint32 serdesId, char *buffer, uint32 bufferSize)
    {
    uint32 localPortId = serdesId - SgmiiSerdesStartId(self);

    if (localPortId == 0)
        {
        AtSnprintf(buffer, bufferSize, "sgmii_dim_ctr");
        return buffer;
        }

    if (localPortId == 1)
        {
        AtSnprintf(buffer, bufferSize, "sgmii_dcc");
        return buffer;
        }

    if (localPortId == 2)
        {
        AtSnprintf(buffer, bufferSize, "sgmii_spare");
        return buffer;
        }

    return buffer;
    }

eBool Tha60290011SerdesManagerSerdesIsBackplane(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return SerdesIsBackplane(self, serdesId);
    return cAtFalse;
    }

eBool Tha60290011SerdesManagerSerdesIsAxi4(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return IsAxi4Serdes(self, serdesId);
    return cAtFalse;
    }

uint32 Tha60290011SerdesManagerSgmiiSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return SgmiiSerdesLocalId(self, serdesId);
    return cInvalidUint32;
    }

uint32 Tha60290011SerdesManagerBackplaneSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return BackplaneSerdesLocalId(self, serdesId);
    return cInvalidUint32;
    }

eAtRet Tha60290011SerdesManagerDrpAllow(AtSerdesManager self, uint32 serdesId, eBool allow)
    {
    if (self)
        return DrpAllow(self, serdesId, allow);
    return cAtErrorNullPointer;
    }

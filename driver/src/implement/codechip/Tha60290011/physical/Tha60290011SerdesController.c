/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60290011SerdesController.c
 *
 * Created Date: Aug 18, 2016
 *
 * Description : Serdes Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "Tha60290011SerdesControllerInternal.h"
#include "Tha60290011SerdesManager.h"
#include "Tha60290011Physical.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290011SerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011SerdesControllerMethods m_methods;

/* Override */
static tAtSerdesControllerMethods       m_AtSerdesControllerOverride;
static tTha6029SerdesControllerMethods  m_Tha6029SerdesControllerOverride;

/* To save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwIdGet(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);

    /*
     * MDIO#1,2,3,4,5 XFI/SGMII2G5/SGMII1G
     */
    if (serdesId == cTha60290011XfiSerdesId0)       return 1;
    if (serdesId == cTha60290011XfiSerdesId1)       return 2;
    if (serdesId == cTha60290011Sgmii25GSerdesId)   return 3;
    if (serdesId == cTha60290011SgmiiSerdesId0)     return 4;
    if (serdesId == cTha60290011SgmiiSerdesId1)     return 5;

    return cInvalidUint32;
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    return (AtChannel)AtModuleEthPortGet(ModuleEth(self), 0);
    }

static AtMdio Mdio(AtSerdesController self)
    {
    return AtModuleEthSerdesMdio(ModuleEth(self), self);
    }

static uint32 TuningBaseAddress(Tha60290011SerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static Tha6029SerdesTuner Tuner(AtSerdesController self)
    {
    return Tha6029SerdesControllerSerdesTunerGet((Tha6029SerdesController)self);
    }

static Tha6029SerdesTuner SerdesTunerObjectCreate(Tha6029SerdesController self)
    {
    uint32 baseAddress = mMethodsGet(mThis(self))->TuningBaseAddress(mThis(self));
    return Tha60290011SerdesTunerNew((AtSerdesController)self, baseAddress);
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    return Tha6029SerdesTunerCanLoopback(Tuner(self), loopbackMode, enable);
    }

static uint32 DrpLocalBaseAddress(Tha60290011SerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    Tha60290011SerdesController serdes = mThis(self);
    uint32 localBaseAddress = mMethodsGet(serdes)->DrpLocalBaseAddress(serdes);
    uint32 tuningBaseAddress = mMethodsGet(serdes)->TuningBaseAddress(serdes);
    uint32 drpBaseAddress = tuningBaseAddress + localBaseAddress;
    return Tha6029SerdesTunerDrpCreateWithBaseAddress(Tuner(self), drpBaseAddress);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    /* TODO: open this when making eye-scan */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LoopbackSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    return Tha6029SerdesTunerLoopbackSet(Tuner(self), loopback);
    }

static eAtLoopbackMode LoopbackGet(AtSerdesController self)
    {
    return Tha6029SerdesTunerLoopbackGet(Tuner(self));
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeDfe;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    return Tha6029SerdesTunerEqualizerModeSet(Tuner(self), mode);
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    return Tha6029SerdesTunerEqualizerModeGet(Tuner(self));
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    return Tha6029SerdesTunerEqualizerModeIsSupported(Tuner(self), mode);
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    return Tha6029SerdesTunerEqualizerCanControl(Tuner(self));
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    return Tha6029SerdesTunerPhysicalParamSet(Tuner(self), param, value);
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6029SerdesTunerPhysicalParamGet(Tuner(self), param);
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6029SerdesTunerPhysicalParamIsSupported(Tuner(self), param);
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    return Tha6029SerdesTunerPhysicalParamValueIsInRange(Tuner(self), param, value);
    }

static eBool PllIsLocked (AtSerdesController self)
    {
    return Tha6029SerdesTunerPllIsLocked(Tuner(self));
    }

static eBool PllChanged (AtSerdesController self)
    {
    return Tha6029SerdesTunerPllChanged(Tuner(self));
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    return Tha6029SerdesTunerPowerCanControl(Tuner(self));
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    return Tha6029SerdesTunerPowerDown(Tuner(self), powerDown);
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    return Tha6029SerdesTunerPowerIsDown(Tuner(self));
    }

static eAtRet Reset(AtSerdesController self)
    {
    return Tha6029SerdesTunerReset(Tuner(self));
    }

static eAtRet RxReset(AtSerdesController self)
    {
    return Tha6029SerdesTunerRxReset(Tuner(self));
    }

static eAtRet TxReset(AtSerdesController self)
    {
    return Tha6029SerdesTunerTxReset(Tuner(self));
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);

        mMethodOverride(m_AtSerdesControllerOverride, DrpObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);

        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);

        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);

        /* EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);

        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, RxReset);
        mMethodOverride(m_AtSerdesControllerOverride, TxReset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6029SerdesController(AtSerdesController self)
    {
    Tha6029SerdesController controller = (Tha6029SerdesController)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029SerdesControllerOverride));

        mMethodOverride(m_Tha6029SerdesControllerOverride, SerdesTunerObjectCreate);
        }

    mMethodsSet(controller, &m_Tha6029SerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6029SerdesController(self);
    }

static void MethodsInit(Tha60290011SerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TuningBaseAddress);
        mMethodOverride(m_methods, DrpLocalBaseAddress);
        }

    mMethodsSet(self, &m_methods);
    }

AtSerdesController Tha60290011SerdesControllerObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011SerdesController));

    /* Super constructor */
    if (Tha6029SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

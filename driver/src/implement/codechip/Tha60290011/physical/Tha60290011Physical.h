/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290011Physical.h
 * 
 * Created Date: Oct 22, 2016
 *
 * Description : Physical common declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PHYSICAL_H_
#define _THA60290011PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/physical/Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha6029SerdesTuner Tha60290011SerdesTunerNew(AtSerdesController serdes, uint32 baseAddress);

AtDrp Tha60290011DrpNew(uint32 baseAddress, AtHal hal, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011PHYSICAL_H_ */


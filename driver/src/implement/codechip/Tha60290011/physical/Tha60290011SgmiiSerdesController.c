/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes controller
 *
 * File        : Tha60290011SgmiiSerdesController.c
 *
 * Created Date: Aug 12, 2016
 *
 * Description : Sgmii Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEth.h"
#include "AtEyeScanController.h"
#include "../../Tha60290021/physical/Tha6029Physical.h"
#include "../prbs/Tha60290011SgmiiSerdesPrbsEngine.h"
#include "../man/Tha60290011DeviceReg.h"
#include "Tha60290011SerdesControllerInternal.h"
#include "Tha60290011SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cSgmiiSerdesPrbsEngineId(localId) (1 + (localId))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011SgmiiSerdesController
    {
    tTha60290011SerdesController super;
    }tTha60290011SgmiiSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                    m_AtObjectOverride;
static tAtSerdesControllerMethods          m_AtSerdesControllerOverride;
static tTha60290011SerdesControllerMethods m_Tha60290011SerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth1G) ? cAtTrue : cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    return ModeIsSupported(self, mode) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth1G;
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    AtDevice dev = AtChannelDeviceGet(channel);
    static char description[64];
    static char str[128];
    AtSerdesController serdes = (AtSerdesController)self;
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    AtSprintf(str, "%s%s", AtDeviceIdToString(dev), Tha60290011SerdesManagerSgmiiDescription(manager, serdesId, description, sizeof(description)));
    return str;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static uint32 PrbsHwEngineId(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 localId = Tha60290011SerdesManagerSgmiiSerdesLocalId(manager, serdesId);
    return cSgmiiSerdesPrbsEngineId(localId);
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60290011SgmiiSerdesPrbsEngineWithHwIdNew(self, PrbsHwEngineId(self));
    }

static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static uint32 SerdesIdToPhyPortId(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    if (serdesId == 3 || serdesId == 4)
        return (uint32)(serdesId - 1);
    return cInvalidUint32;
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    uint32 phyPortId = SerdesIdToPhyPortId(self);

    return (AtChannel)AtModuleEthPortGet(ModuleEth(self), (uint8)phyPortId);
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    return (float)(1.25);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    return AtEyeScanControllerGthE3New(self, drpBaseAddress);
    }

static uint32 TuningBaseAddress(Tha60290011SerdesController self)
    {
    AtSerdesController serdes = (AtSerdesController)self;
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    uint32 localId = Tha60290011SerdesManagerSgmiiSerdesLocalId(manager, serdesId);
    return (cSgmiiDimSerdesTuningBaseAddress + (0x1000UL * localId));
    }

static uint32 DrpLocalBaseAddress(Tha60290011SerdesController self)
    {
    AtUnused(self);
    return 0x200; /* See Xilinx_Serdes_Turning_1x.atreg */
    }

static uint32 AlarmGet(AtSerdesController self)
	{
	return Tha60290011SgmiiSerdesLinkAlarmGet(self);
	}

static uint32 AlarmHistoryGet(AtSerdesController self)
	{
	return Tha60290011SgmiiSerdesLinkAlarmHistoryGet(self, cAtFalse);
	}

static uint32 AlarmHistoryClear(AtSerdesController self)
	{
	return Tha60290011SgmiiSerdesLinkAlarmHistoryGet(self, cAtTrue);
	}

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
	{
	return (Tha60290011SgmiiSerdesLinkAlarmGet(self) == 0) ? cAtSerdesLinkStatusUp: cAtSerdesLinkStatusDown;
	}

static eAtRet Reset(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtSerdesControllerMethods->Reset(self);
    ret |= Tha6029SgmiiElectricallyIsolatePHYFromGMII(self, cAtFalse);

    return ret;
    }

static void OverrideTha60290011SerdesController(AtSerdesController self)
    {
    Tha60290011SerdesController serdes = (Tha60290011SerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011SerdesControllerOverride, mMethodsGet(serdes), sizeof(m_Tha60290011SerdesControllerOverride));

        mMethodOverride(m_Tha60290011SerdesControllerOverride, TuningBaseAddress);
        mMethodOverride(m_Tha60290011SerdesControllerOverride, DrpLocalBaseAddress);
        }

    mMethodsSet(serdes, &m_Tha60290011SerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha60290011SerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SgmiiSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290011SgmiiSerdesControllerNew(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, physicalPort, serdesId, manager);
    }

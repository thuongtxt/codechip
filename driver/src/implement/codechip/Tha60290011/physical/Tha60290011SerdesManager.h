/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Serdes manager
 * 
 * File        : Tha60290011SerdesManager.h
 * 
 * Created Date: Aug 11, 2016
 *
 * Description : Serdes manager header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SERDESMANAGER_H_
#define _THA60290011SERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannelClasses.h"
#include "../../../../generic/physical/AtSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60290011XfiSerdesId0        0
#define cTha60290011XfiSerdesId1        1
#define cTha60290011Sgmii25GSerdesId    2
#define cTha60290011SgmiiSerdesId0      3
#define cTha60290011SgmiiSerdesId1      4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60290011SerdesManagerNew(AtDevice device);

AtSerdesController Tha60290011BackplaneSerdesControllerNew(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);
AtSerdesController Tha60290011BackplaneSerdesControllerV2New(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);
AtSerdesController Tha60290011Axi4SerdesControllerNew(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);
AtSerdesController Tha60290011SgmiiSerdesControllerNew(AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);

AtSerdesController Tha60290011SerdesManagerBackplaneSerdesController(AtSerdesManager self, uint32 localSerdesId);
AtSerdesController Tha60290011SerdesManagerAxi4SerdesController(AtSerdesManager self);
AtSerdesController Tha60290011SerdesManagerSgmiiSerdesController(AtSerdesManager self, uint32 localSerdesId);


/* Backplane EyeScan */
AtEyeScanController Tha60290011BackPlanEyeScanControllerNew(AtSerdesController serdesController, uint32 drpBaseAddress);

char *Tha60290011SerdesManagerSgmiiDescription(AtSerdesManager self, uint32 serdesId, char *buffer, uint32 bufferSize);
eBool Tha60290011SerdesManagerSerdesIsBackplane(AtSerdesManager self, uint32 serdesId);
eBool Tha60290011SerdesManagerSerdesIsAxi4(AtSerdesManager self, uint32 serdesId);
uint32 Tha60290011SerdesManagerSgmiiSerdesLocalId(AtSerdesManager self, uint32 serdesId);
uint32 Tha60290011SerdesManagerBackplaneSerdesLocalId(AtSerdesManager self, uint32 serdesId);

eAtRet Tha60290011SerdesManagerDrpAllow(AtSerdesManager self, uint32 serdesId, eBool allow);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SERDESMANAGER_H_ */


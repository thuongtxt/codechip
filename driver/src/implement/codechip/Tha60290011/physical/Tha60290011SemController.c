/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : Tha60290011SemController.c
 *
 * Created Date: Oct 21, 2016
 *
 * Description : Tha60290011SemController implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/physical/Tha60210011SemControllerInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "Tha60290011SemController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011SemController
    {
    tTha60210011SemController super;
    }tTha60290011SemController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSemControllerMethods m_ThaSemControllerOverride;
static tAtSemControllerMethods  m_AtSemControllerOverride;

static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(ThaSemController self)
    {
    AtUnused(self);
    return 0xF43000;
    }

static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return "XCKU060";
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x00009313;
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, DeviceDescription);
        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void OverrideThaSemController(AtSemController self)
    {
    ThaSemController sem = (ThaSemController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSemControllerOverride, mMethodsGet(sem), sizeof(m_ThaSemControllerOverride));

        mMethodOverride(m_ThaSemControllerOverride, DefaultOffset);
        }

    mMethodsSet(sem, &m_ThaSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtSemController(self);
    OverrideThaSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SemController);
    }

static AtSemController ObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60290011SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, device, semId);
    }


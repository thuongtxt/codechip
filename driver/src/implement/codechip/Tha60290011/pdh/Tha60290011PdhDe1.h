/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290011ModulePdhDe1.h
 * 
 * Created Date: Jun 11, 2016
 *
 * Description : Interface of the DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290011_PDH_THA60290011MODULEPDHDE1_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290011_PDH_THA60290011MODULEPDHDE1_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60290011PdhDe1InterfaceChannelId2HwId(ThaPdhDe1 self, uint32 *sts, uint32 *vtg, uint32 *vt);
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290011_PDH_THA60290011MODULEPDHDE1_H_ */


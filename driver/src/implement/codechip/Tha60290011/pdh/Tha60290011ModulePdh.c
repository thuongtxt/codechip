/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011ModulePdh.c
 *
 * Created Date: Jun 10, 2016
 *
 * Description : implementation for PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/sdh/AtModuleSdhInternal.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../../default/pdh/ThaPdhDe3.h"
#include "../../../default/pdh/ThaPdhPrmController.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../../Tha60290021/pdh/Tha60290021ModulePdh.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../../Tha60290022/pdh/Tha60290022PdhUtil.h"
#include "../cdr/Tha60290011ModuleCdr.h"
#include "../sdh/Tha60290011ModuleSdh.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "../map/Tha60290011ModuleMapDemap.h"
#include "Tha60290011PdhDe1.h"
#include "Tha60290011PdhSerialLine.h"
#include "Tha60290011ModulePdhInternal.h"
#include "Tha60290011PdhPrmController.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumTu11InTug2  4UL
#define cNumTu12InTug2  3UL
#define cNumDe1         84UL
#define cNumDs1InOneSts 28UL
#define cNumE1InOneSts  21UL
#define cNumDe3         24UL
#define cNumVtgPerSts   7UL
#define cMaxLiuDs1E1    (84UL)
#define cMaxLiuEc1De3   (24UL)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290011ModulePdh)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tThaStmModulePdhMethods      m_ThaStmModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tAtModulePdhMethods  *m_AtModulePdhMethods  = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;
static const tThaStmModulePdhMethods    *m_ThaStmModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(ThaModulePdh self)
    {
    return AtModuleDeviceGet((AtModule)self);
    }

static AtModuleSdh SdhModule(ThaModulePdh self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(Device(self), cAtModuleSdh);
    }

static eAtRet PdhRxMuxFromLiuSet(AtModule self)
    {
    uint32 hwSts;

    for (hwSts = 0; hwSts < cNumDe3; hwSts++)
        {
        uint32 regAddr = (uint32)(cThaRegDS1E1J1RxFrmrMuxCtrl2 + hwSts);
        mModuleHwWrite(self, regAddr, 0xFFFFFFFF);
        }

    return cAtOk;
    }

static AtPdhSerialLine De3SerialCreate(AtModulePdh self, uint32 serialLineId)
    {
    return Tha60290011PdhSerialLineNew(serialLineId, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
	{
	AtUnused(de2);
	return Tha60290021PdhDe2De1New(de1Id, self);
	}

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha60290011PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static eAtRet AllLiuDe1FramerAllocate(AtModulePdh self)
    {
    eAtRet ret = cAtOk;
    uint32 de1_i;

    for (de1_i = 0; de1_i < AtModulePdhNumberOfDe1sGet(self); de1_i++)
        {
        AtPdhDe1 de1 = AtModulePdhDe1Get(self, de1_i);
        ret |= Tha60290011ModulePdhLiuDe1FramerAllocateWithE1Mode((ThaModulePdh)self, de1, cAtFalse);
        }

    return ret;
    }

static eAtRet AllDe1Create(AtModulePdh self)
    {
    eAtRet ret = m_AtModulePdhMethods->AllDe1Create(self);
    if (ret != cAtOk)
        return ret;

    return AllLiuDe1FramerAllocate(self);
    }

static uint8 VtgTypeSw2Hw(uint8 pldType)
    {
    if (pldType == cThaPdhTu12Vt2)
        return 0x1;

    if (pldType == cThaPdhTu11Vt15)
        return 0x0;

    return cInvalidUint8;
    }

static uint8 VtgTypeToDe1HwFrameType(uint8 pldType)
    {
    if (pldType == cThaPdhTu12Vt2)
        return 0x8;

    if (pldType == cThaPdhTu11Vt15)
        return 0x0;

    return cInvalidUint8;
    }

static eAtRet DefaultDe1FrameTypePerVtGroupHwSet(AtModulePdh self, uint32 sts, uint32 vtg, uint8 pldType)
    {
    uint8 vtIdx;
    uint8 numVtPerVtg = (pldType == cThaPdhTu11Vt15) ? 4 : 3;
    uint32 regValue, regAddress, offset;
    uint8 hwPldType = VtgTypeToDe1HwFrameType(pldType);

    for (vtIdx = 0; vtIdx < numVtPerVtg; vtIdx++)
        {
        offset = (sts * 32UL) + (vtg * 4UL) + vtIdx;
        regAddress = cThaRegDS1E1J1RxFrmrCtrl + offset;
        regValue = mModuleHwRead(self, regAddress);
        mFieldIns(&regValue, cThaRxDE1MdMask, cThaRxDE1MdShift, hwPldType);
        mModuleHwWrite(self, regAddress, regValue);

        regAddress = cThaRegDS1E1J1TxFrmrCtrl + offset;
        regValue = mModuleHwRead(self, regAddress);
        mFieldIns(&regValue, cThaTxDE1MdMask, cThaTxDE1MdShift, hwPldType);
        mModuleHwWrite(self, regAddress, regValue);
        }

    return cAtOk;
    }

static eAtRet VtgInStsTypeHwSet(AtModulePdh self, uint32 sts, uint8 pldType)
    {
    uint32 vtg;
    uint32 regValue, regAddress;
    uint8 hwPldType = VtgTypeSw2Hw(pldType);
    uint32 regBase = ThaModulePdhRxPerStsVCPldCtrlRegister((ThaModulePdh)self);

    for (vtg = 0; vtg < cNumVtgPerSts; vtg++)
        {
        regAddress = regBase + sts;
        regValue = mModuleHwRead(self, regAddress);
        mFieldIns(&regValue, cThaPdhVtRxMapTug2TypeMask2(vtg), cThaPdhVtRxMapTug2TypeShift2(vtg), hwPldType);
        mModuleHwWrite(self, regAddress, regValue);

        DefaultDe1FrameTypePerVtGroupHwSet(self, sts, vtg, pldType);
        }

    return cAtOk;
    }

static eAtRet PdhDemapVtgTypeDs1Set(AtModulePdh self)
    {
    uint32 startSts = cTha60290011StartDs1StsHwId;
    uint32 maxSts = startSts + cTha60290011NumDs1HwSts;
    uint32 sts;

    for (sts = startSts; sts < maxSts; sts++)
        VtgInStsTypeHwSet(self, sts, cThaPdhTu11Vt15);

    return cAtOk;
    }

static eAtRet PdhDemapVtgTypeE1Set(AtModulePdh self)
    {
    uint32 startSts = cTha60290011StartE1StsHwId;
    uint32 maxSts = startSts + cTha60290011NumE1HwSts;
    uint32 sts;

    for (sts = startSts; sts < maxSts; sts++)
        VtgInStsTypeHwSet(self, sts, cThaPdhTu12Vt2);

    return cAtOk;
    }

static eAtRet PdhStsVcPayloadTypeCtrlDe1Assign(AtModulePdh self)
    {
    eAtRet ret = cAtOk;

    ret |= PdhDemapVtgTypeDs1Set(self);
    ret |= PdhDemapVtgTypeE1Set(self);

    return ret;
    }

static eAtRet HwFlushPdhMux(AtModule self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    ThaDeviceMemoryFlush(device, 0xC01400,  0xC01453, 0);
    ThaDeviceMemoryFlush(device, 0xC01000,  0xC013FF, 0);
    return cAtOk;
    }

static uint16 DefaultLengthGet(ThaModulePdh self)
    {
    eTha60290011PdhInterfaceType interface = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self);
    if (interface == cTha60290011PdhInterfaceTypeDe1)
        return (cMaxLiuDs1E1 * 2UL) + 2UL + 84UL;

    return (cMaxLiuEc1De3 * 16UL) + 2 + 36;
    }

static eAtRet PdhMuxToAxi4Init(AtModule self, eTha60290011PdhInterfaceType interface)
    {
    uint16 ethLen = Tha60290011ModulePdhLengthDefaultGet((ThaModulePdh)self);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleEth);
    ThaEthPort axiaPort = (ThaEthPort)AtModuleEthPortGet(ethModule, cTha60290011ModuleEth2Dot5GPortId);
    uint8 subtype =  (interface == cTha60290011PdhInterfaceTypeDe1) ? 0x0 : 0x10;

    Tha60290011ModulePdhEthSubtypeSet((ThaModulePdh)self, subtype);
    Tha60290011ModulePdhEthLengthSet((ThaModulePdh)self, ethLen);
    Tha60290011ClsInterfaceEthPortEthLengthSet(axiaPort, ethLen);

    return Tha60290011ModulePdhMuxActive((ThaModulePdh)self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;
    eTha60290011PdhInterfaceType interface = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self);

    HwFlushPdhMux(self);
    if (interface == cTha60290011PdhInterfaceTypeDe1)
        {
        Tha60290011ModuleCdrDe1LineModeControlDefault((ThaModuleCdr)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleCdr));
        Tha60290011ModuleMapDe1LineModeControlDefault((ThaModuleMap)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleMap));
        ret = PdhStsVcPayloadTypeCtrlDe1Assign((AtModulePdh)self);
        }

    ret |= PdhMuxToAxi4Init(self, interface);

    ret |= m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    PdhRxMuxFromLiuSet(self);

    return cAtOk;
    }

static uint32 De1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint8 hwId, sliceId, tugOrDe2Id, tu1xOrDe1Id;
    uint32 offset;
    uint32 baseAddress;

    /* (STSID*32 + VTGID*4 + VTID) */
    if (ThaPdhChannelHwIdGet((AtPdhChannel)de1, cAtModulePdh, &sliceId, &hwId) != cAtOk)
        return cBit31_0;


    tu1xOrDe1Id = Tha60210031Tu1xOrDe1IdGet(de1, &tugOrDe2Id);
    offset = (uint32)((hwId * 32) + (tugOrDe2Id * 4) + tu1xOrDe1Id);
    baseAddress = ThaModulePdhPrmBaseAddress(self);
    return (offset + baseAddress);
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60290011PdhPrmControllerNew(de1);
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
    if (Tha60290011ModulePdhInterfaceTypeGet(self) == cTha60290011PdhInterfaceTypeDe1)
        return cNumDe1;

    return 0;
    }

static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
    return Tha60290011PdhDe1New(de1Id, self);
    }

static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    return Tha60290011PdhDe3New(de3Id, self);
    }

static uint32 NumberOfDe3sGet(AtModulePdh self)
    {
    if (Tha60290011ModulePdhInterfaceTypeGet(self) == cTha60290011PdhInterfaceTypeDe3)
        return cNumDe3;
    return 0;
    }

static uint32 NumberOfDe3SerialLinesGet(AtModulePdh self)
    {
    if (Tha60290011ModulePdhInterfaceTypeGet(self) == cTha60290011PdhInterfaceTypeDe3)
        return cNumDe3;
    return 0;
    }

static eAtRet DefaultSet(ThaModulePdh self)
    {
    if (Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self) == cTha60290011PdhInterfaceTypeDe3)
        return m_ThaModulePdhMethods->DefaultSet(self);

    /* TBD */
    return cAtOk;
    }

static eBool De3TxLiuEnableIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    /* Not support config LIU from GLB RD */
    return cAtFalse;
    }

static uint32 StartVersionSupportMdlFilterMessageByType(ThaModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet Ds1E1FrmRxMuxSet(ThaStmModulePdh self, AtSdhChannel sdhVc)
    {
    if ((AtSdhChannelTypeGet(sdhVc) == cAtSdhChannelTypeVc3) &&
        (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)sdhVc)))
        return m_ThaStmModulePdhMethods->Ds1E1FrmRxMuxSet(self, sdhVc);

    return cAtOk;
    }

static uint32 StartHardwareVersionSupportsDataLinkFcsLsbBitOrder(ThaModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static void PdhChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);
    if (channelType == cAtPdhChannelTypeE1 || channelType == cAtPdhChannelTypeDs1)
        {
        uint32 sts, vtg, vt;
        Tha60290011PdhDe1InterfaceChannelId2HwId((ThaPdhDe1)pdhChannel, &sts, &vtg, &vt);
        AtPrintc(cSevInfo, "   - HW-ID: sts(%d) vtg(%d) vt(%d):\r\n", sts, vtg, vt);
        }

    m_ThaModulePdhMethods->PdhChannelRegsShow(self, pdhChannel);
    Tha60290011ModulePdhMuxChannelRegsDebugShow(self, pdhChannel);
    }

static eBool DatalinkFcsBitOrderIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionFixDs3AisInterrupt(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportDe3Disabling(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportDe3Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    /* TODO: HW Not support yet */
    return cInvalidUint32;
    }

static uint32 StartVersionSupportDe3FramedSatop(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static eBool LiuDe3OverVc3IsSupported(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet LiuDe1FramerAllocateWithE1Mode(ThaModulePdh self, AtPdhDe1 de1, eBool isE1)
    {
    AtSdhChannel vc1x;
    uint32 tu1xId, tug2Id, sts1Id;
    eAtSdhChannelType vc1xChannelType;
    uint32 de1Id = AtChannelIdGet((AtChannel)de1);

    if (AtPdhChannelVcInternalGet((AtPdhChannel)de1))
        return cAtOk;

    if (isE1)
        {
        sts1Id   = cTha60290011StartE1StsHwId + (de1Id / cNumE1InOneSts);
        tug2Id = (de1Id % cNumE1InOneSts) / cNumTu12InTug2;
        tu1xId = (de1Id % cNumE1InOneSts) % cNumTu12InTug2;
        vc1xChannelType = cAtSdhChannelTypeVc12;
        }
    else
        {
        sts1Id   = cTha60290011StartDs1StsHwId + (de1Id / cNumDs1InOneSts);
        tug2Id = (de1Id % cNumDs1InOneSts) / cNumTu11InTug2;
        tu1xId = (de1Id % cNumDs1InOneSts) % cNumTu11InTug2;
        vc1xChannelType = cAtSdhChannelTypeVc11;
        }

    vc1x = (AtSdhChannel)Tha60290011SerialDe1Vc1xNew(0, vc1xChannelType, SdhModule(self));
    SdhChannelLineIdSet(vc1x, (uint8)sts1Id);
    SdhChannelSts1IdSet(vc1x, (uint8)sts1Id);
    SdhChannelTug2IdSet(vc1x, (uint8)tug2Id);
    SdhChannelTu1xIdSet(vc1x, (uint8)tu1xId);

    AtPdhChannelVcSet((AtPdhChannel)de1, (AtSdhVc)vc1x);
    return cAtOk;
    }

static eBool IsHwIdBelongToE1(uint8 sts, uint8 vtgId, uint8 vtId)
    {
    if (((sts >= cTha60290011StartE1StsHwId) && (sts < (cTha60290011StartE1StsHwId + cTha60290011NumE1HwSts)))
        && (vtgId < 7)
        && (vtId < 3))
        return cAtTrue;
    return cAtFalse;
    }

static AtPdhDe1 HwIdToE1Object(AtModulePdh self, uint8 sts, uint8 vtgId, uint8 vtId)
    {
    int32 de1Id = ((sts - cTha60290011StartE1StsHwId) * (int32)cNumE1InOneSts) + (vtgId * 3) + vtId;
    return AtModulePdhDe1Get(self, (uint32)de1Id);
    }

static eBool IsHwIdBelongToDs1(uint8 sts, uint8 vtgId, uint8 vtId)
    {
    if ((sts < (cTha60290011StartDs1StsHwId + cTha60290011NumDs1HwSts))
        && (vtgId < 7)
        && (vtId < 4))
        return cAtTrue;
    return cAtFalse;
    }

static AtPdhDe1 HwIdToDs1Object(AtModulePdh self, uint8 sts, uint8 vtgId, uint8 vtId)
    {
    int32 de1Id = ((sts - cTha60290011StartDs1StsHwId) * (int32)cNumDs1InOneSts) + (vtgId * 4) + vtId;
    return AtModulePdhDe1Get(self, (uint32)de1Id);
    }

static AtPdhDe1 De1HwIdToLiuDe1Object(AtModulePdh self, uint8 sts, uint8 vtgId, uint8 vtId)
    {
    AtPdhDe1 de1 = NULL;
    if (IsHwIdBelongToE1(sts, vtgId, vtId))
        {
        de1 = HwIdToE1Object(self, sts, vtgId, vtId);
        return AtPdhDe1IsE1(de1) ? de1 : NULL;
        }

    if (IsHwIdBelongToDs1(sts, vtgId, vtId))
        {
        de1 = HwIdToDs1Object(self, sts, vtgId, vtId);
        return AtPdhDe1IsE1(de1) ? NULL : de1;
        }

    return NULL;
    }

static eAtRet LiuDe1FramerDeallocate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);

    AtUnused(self);

    if (vc == NULL)
        return cAtOk;

    AtObjectDelete((AtObject)vc);
    AtPdhChannelVcSet((AtPdhChannel)de1, NULL);

    return cAtOk;
    }

static eBool InterfaceSupported(eTha60290011PdhInterfaceType type)
    {
    if ((type == cTha60290011PdhInterfaceTypeDe3) || (type == cTha60290011PdhInterfaceTypeDe1))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet InterfaceTypeSet(AtModulePdh self, eTha60290011PdhInterfaceType type)
    {
    eAtRet ret0 = cAtOk, ret1 = cAtOk, ret2 = cAtOk;
    eTha60290011PdhInterfaceType currentType = Tha60290011ModulePdhInterfaceTypeGet(self);
    eBool usePmcCounter = cAtFalse;

    if (!InterfaceSupported(type))
        return cAtErrorInvlParm;

    if (type == currentType)
        return cAtOk;

    /* delete all all objects */
    ret1 = AtModulePdhAllManagedObjectsDelete(self);
    mModuleErrorLog(self, ret1, "AtModulePdhAllManagedObjectsDelete");

    /* create new objects for new pdh interface */
    mThis(self)->interfaceType = type;
    ret2 = AtModulePdhAllManagedObjectsCreate(self);
    mModuleErrorLog(self, ret2, "AtModulePdhAllManagedObjectsCreate");

    /* re-init module with new interface */
    if (ThaModulePdhDebugCountersModuleGet((ThaModulePdh)self) == cThaModulePmc)
        usePmcCounter = cAtTrue;

    ret0 = AtModuleInit((AtModule)self);
    if (usePmcCounter)
        ThaModulePdhDebugCountersModuleSet((ThaModulePdh)self, cThaModulePmc);

    mModuleErrorLog(self, ret0, "AtModuleInit");

    return ret0 | ret1 | ret2;
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 MdlRxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB020 : 0xB120;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB000 : 0xB100;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB040 : 0xB140;
        default:                                 return 0;
        }
    }

static uint32 MdlRxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB420 : 0xB4A0;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB400 : 0xB480;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB440 : 0xB4C0;
        default:                                 return 0;
        }
    }

static uint32 MdlRxDropOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return (r2c) ? 0xB080 : 0xB180;
        case cAtPdhMdlControllerTypeIdleSignal:  return (r2c) ? 0xB060 : 0xB160;
        case cAtPdhMdlControllerTypeTestSignal:  return (r2c) ? 0xB0A0 : 0xB1A0;
        default:                                 return 0;
        }
    }

static uint32 RxMdlIdFromChannelGet(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (uint32)hwIdInSlice;
    }

static uint32 MdlRxBufWordOffset(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 32UL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290011ModulePdh object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(interfaceType);
    }

static eBool NeedClearanceNotifyLogic(AtModulePdh self)
    {
    AtUnused(self);
    return Tha602900xxNeedClearanceNotifyLogic();
    }

static eBool Ec1DiagLiuModeShouldEnable(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool Ec1LiuRxClockInvertionShouldEnable(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    return Tha60290011DeviceStartVersionSupportDe1Ssm(Device(self));
    }

static eBool RxHw3BitFeacSignalIsSupported(ThaModulePdh self)
    {
    return Tha60290011DeviceRxHw3BitFeacSignalIsSupported(Device(self));
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    return Tha60290011DeviceStartVersionSupportDe1LomfConsequentialAction(Device(self));
    }

static AtPdhDe1 De1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id)
    {
    eTha60290011PdhInterfaceType intf = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self);

    if (intf == cTha60290011PdhInterfaceTypeDe3)
        return m_ThaModulePdhMethods->De1ChannelFromHwIdGet(self, phyModule, sliceId, de3Id, de2Id, de1Id);

    if (intf == cTha60290011PdhInterfaceTypeDe1)
        return Tha60290011ModulePdhHwIdToLiuDe1Object((AtModulePdh)self, de3Id, de2Id, de1Id);

    return NULL;
    }

static uint32 StartVersionSupportDe1IdleCodeInsertion(ThaModulePdh self)
    {
    return Tha60290011DeviceStartVersionSupportDe1IdleCodeInsertion(Device(self));
    }

static eBool ShouldRejectTxChangeWhenChannelBoundPw(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RegExist(ThaModulePdh self, uint32 addressBase)
    {
    return Tha602900xxModulePdhRegAddressExist(self, addressBase);
    }

static eAtRet StsVtDemapVc4FractionalVc3Set(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    /* This product does not have this configuration as registers are removed */
    AtUnused(self);
    AtUnused(sdhVc);
    AtUnused(isVc4FractionalVc3);
    return cAtOk;
    }

static eBool De1FastSearchIsSupported(ThaModulePdh self)
    {
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x0, 0x0000);
    ThaVersionReader reader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(reader);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, AllDe1Create);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3SerialLinesGet);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe3sGet);
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, De3SerialCreate);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, NeedClearanceNotifyLogic);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, DefaultSet);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3TxLiuEnableIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionFixDs3AisInterrupt);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Disabling);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Ssm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3FramedSatop);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1PrmOffset);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1Ssm);
        mMethodOverride(m_ThaModulePdhOverride, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_ThaModulePdhOverride, De1ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_ThaModulePdhOverride, ShouldRejectTxChangeWhenChannelBoundPw);
        mMethodOverride(m_ThaModulePdhOverride, RegExist);
        mMethodOverride(m_ThaModulePdhOverride, De1FastSearchIsSupported);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh stmPdh = (ThaStmModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaStmModulePdhMethods = mMethodsGet(stmPdh);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(stmPdh), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, Ds1E1FrmRxMuxSet);
        mMethodOverride(m_ThaStmModulePdhOverride, LiuDe3OverVc3IsSupported);
        mMethodOverride(m_ThaStmModulePdhOverride, StsVtDemapVc4FractionalVc3Set);
        }

    mMethodsSet(stmPdh, &m_ThaStmModulePdhOverride);
    }

static void OverrideTha60210031ModulePdh(AtModulePdh self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, StartVersionSupportMdlFilterMessageByType);
        mMethodOverride(m_Tha60210031ModulePdhOverride, StartHardwareVersionSupportsDataLinkFcsLsbBitOrder);
        mMethodOverride(m_Tha60210031ModulePdhOverride, DatalinkFcsBitOrderIsSupported);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxPktOffset);
	    mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxByteOffset);
	    mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxDropOffset);
	    mMethodOverride(m_Tha60210031ModulePdhOverride, RxMdlIdFromChannelGet);
	    mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufWordOffset);
	    mMethodOverride(m_Tha60210031ModulePdhOverride, Ec1LiuRxClockInvertionShouldEnable);
	    mMethodOverride(m_Tha60210031ModulePdhOverride, Ec1DiagLiuModeShouldEnable);
        }

    mMethodsSet(pdhModule, &m_Tha60210031ModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideThaStmModulePdh(self);
    OverrideTha60210031ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePdh);
    }

AtModulePdh Tha60290011ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->interfaceType = cTha60290011PdhInterfaceTypeDe3;

    return self;
    }

AtModulePdh Tha60290011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePdhObjectInit(newModule, device);
    }

eTha60290011PdhInterfaceType Tha60290011ModulePdhInterfaceTypeGet(AtModulePdh self)
    {
    return mThis(self)->interfaceType;
    }

eAtRet Tha60290011ModulePdhInterfaceTypeSet(AtModulePdh self, eTha60290011PdhInterfaceType type)
    {
    if (self)
        return InterfaceTypeSet(self, type);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhLiuDe1FramerAllocateWithE1Mode(ThaModulePdh self, AtPdhDe1 de1, eBool isE1)
    {
    if (self)
        return LiuDe1FramerAllocateWithE1Mode(self, de1, isE1);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhLiuDe1FramerDeallocate(ThaModulePdh self, AtPdhDe1 de1)
    {
    if (self)
        return LiuDe1FramerDeallocate(self, de1);
    return cAtErrorNullPointer;
    }

uint16 Tha60290011ModulePdhLengthDefaultGet(ThaModulePdh self)
    {
    if (self)
        return DefaultLengthGet(self);
    return cAtErrorNullPointer;
    }

AtPdhDe1 Tha60290011ModulePdhHwIdToLiuDe1Object(AtModulePdh self, uint8 sts, uint8 vtgId, uint8 vtId)
    {
    if (self)
        return De1HwIdToLiuDe1Object(self, sts, vtgId, vtId);
    return NULL;
    }

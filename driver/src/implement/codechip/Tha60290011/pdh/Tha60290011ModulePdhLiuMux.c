/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011ModulePdhLiuMux.c
 *
 * Created Date: Dec 1, 2016
 *
 * Description : PDH Liu Mux
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../pmc/Tha60290011ModulePmc.h"
#include "Tha60290011ModulePdhMuxReg.h"
#include "Tha60290011ModulePdhInternal.h"
#include "AtPdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mBoolToString(_en) (_en ? "Enable" : "Disable")

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void HwRegToMac(uint32 msb16b, uint32 lsb32b, uint8 *address)
    {
    address[0] = (uint8)((msb16b & cBit15_8) >> 8);
    address[1] = (uint8)(msb16b & cBit7_0);

    address[2] = (uint8)((lsb32b & cBit31_24) >> 24);
    address[3] = (uint8)((lsb32b & cBit23_16) >> 16);
    address[4] = (uint8)((lsb32b & cBit15_8) >> 8);
    address[5] = (uint8)(lsb32b & cBit7_0);
    }

static void MacToHwReg(uint32 * msb16b, uint32 * lsb32b, uint8 *address)
    {
    mFieldIns(msb16b, cBit15_8, 8, address[0]);
    mFieldIns(msb16b, cBit7_0, 0, address[1]);

    mFieldIns(lsb32b, cBit31_24, 24, address[2]);
    mFieldIns(lsb32b, cBit23_16, 16, address[3]);
    mFieldIns(lsb32b, cBit15_8, 8, address[4]);
    mFieldIns(lsb32b, cBit7_0, 0, address[5]);
    }

static eAtRet PdhToEthEnable(ThaModulePdh self, eBool isEnabled)
    {
    uint32 regAddr = cAf6Reg_upen0 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen0_Tx_Eth_dis_, isEnabled ? 0 : 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool PdhToEthIsEnabled(ThaModulePdh self)
    {
    uint32 regAddr = cAf6Reg_upen0 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen0_Tx_Eth_dis_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PdhMuxActive(ThaModulePdh self)
    {
    uint32 regAddr = cAf6Reg_upen0 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen0_Active_, 0);
    mModuleHwWrite(self, regAddr, regVal);
    mRegFieldSet(regVal, cAf6_upen0_Active_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet EthLengthSet(ThaModulePdh self, uint16 length)
    {
    uint32 regAddr = cAf6Reg_upen6 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen6_Ether_Len_, length);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint16 EthLengthGet(ThaModulePdh self)
    {
    uint32 regAddr = cAf6Reg_upen6 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint16)mRegField(regVal, cAf6_upen6_Ether_Len_);
    }

static eAtRet EthTypeSet(ThaModulePdh self, uint16 type)
    {
    uint32 regAddr = cAf6Reg_upen6 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen6_Ether_Type_, type);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint16 EthTypeGet(ThaModulePdh self)
    {
    uint32 regAddr = cAf6Reg_upen6 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint16)mRegField(regVal, cAf6_upen6_Ether_Type_);
    }

static eAtRet PdhInterfaceAxi4Loopback(ThaModulePdh self, uint8 loopMode)
    {
    uint32 regAddress = cAf6Reg_upen0 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddress);
    mRegFieldSet(regVal, cAf6_upen0_AXI4_Stream_loopout_, (loopMode == cAtLoopbackModeRemote) ? 0x1 : 0x0);
    mRegFieldSet(regVal, cAf6_upen0_AXI4_Stream_loopin_, (loopMode == cAtLoopbackModeLocal) ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddress, regVal);
    return cAtOk;
    }

static eAtRet EthEncapSubTypeSet(ThaModulePdh self, uint8 subtype)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regAddr = muxBase + cAf6Reg_upen4;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen4_Ether_Subtype_, subtype);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint8 EthEncapSubTypeGet(ThaModulePdh self)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regAddr = muxBase + cAf6Reg_upen4;
    uint32 regVal = mModuleHwRead(self, regAddr);

    return (uint8)mRegField(regVal, cAf6_upen4_Ether_Subtype_);
    }

static eAtRet TxHwLineEnable(ThaModulePdh self, uint32 pdhId, eBool enable)
    {
    /* PDH to LIU */
    uint32 regAddr = cAf6Reg_upen_c_0 + pdhId + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_0_LiuEnb_, enable ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool TxHwLineIsEnabled(ThaModulePdh self, uint32 pdhId)
    {
    uint32 regAddr = cAf6Reg_upen_c_0 + pdhId + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen_c_0_LiuEnb_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet RxHwLineEnable(ThaModulePdh self, uint32 liuId, eBool enable)
    {
    /* LIU to PDH */
    uint32 regAddr = cAf6Reg_upen_c_1 + liuId + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32  regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_1_LiuEnb_, enable ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool RxHwLineIsEnabled(ThaModulePdh self, uint32 liuId)
    {
    uint32 regAddr = cAf6Reg_upen_c_1 + liuId + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen_c_1_LiuEnb_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 NcoModeSwToHw(AtPdhChannel pdhChannel)
    {
    uint8 channelType = AtPdhChannelTypeGet(pdhChannel);
    uint32 mode;

    if (channelType == cAtPdhChannelTypeE1)
        return 0x1;

    if (channelType == cAtPdhChannelTypeDs1)
        return 0x0;

    mode = AtPdhSerialLineModeGet((AtPdhSerialLine)pdhChannel);
    if (mode == cAtPdhDe3SerialLineModeEc1)
        return 0x4;

    if (mode == cAtPdhDe3SerialLineModeE3)
        return 0x3;

    return 0x2;
    }

static eAtRet NcoLiuToPdhSet(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 pdhId = AtChannelIdGet((AtChannel)pdhChannel);
    uint32 regVal = 0;
    uint32 regAddr = muxBase + cAf6Reg_upen_c_2 + pdhId;
    mRegFieldSet(regVal, cAf6_upen_c_2_txncomd_, NcoModeSwToHw(pdhChannel));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet LineMap(ThaModulePdh self, uint32 pdhId, uint32 liuId, eBool isMap)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regAddr = muxBase + cAf6Reg_upen_c_0 + pdhId;
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 liuMode;
    if (Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)self) == cTha60290011PdhInterfaceTypeDe1)
        liuMode = cLiuModeDe1;
    else
        liuMode = cLiuModeDe3Ec1;

    mRegFieldSet(regVal, cAf6_upen_c_0_LiuMod_, liuMode);
    mRegFieldSet(regVal, cAf6_upen_c_0_LiuId_, isMap ? liuId : 0x0);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = muxBase + cAf6Reg_upen_c_1 + liuId;
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_c_1_PDHId_, isMap ? pdhId : 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet SourceMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 msbRegAddr = cAf6Reg_upen4 + muxBase;
    uint32 lsbRegAddr = cAf6Reg_upen5 + muxBase;
    uint32 lsb32Mac = 0;

    uint32 msb16Mac = mModuleHwRead(self, msbRegAddr);
    MacToHwReg(&msb16Mac, &lsb32Mac, address);
    mModuleHwWrite(self, msbRegAddr, msb16Mac);
    mModuleHwWrite(self, lsbRegAddr, lsb32Mac);

    return cAtOk;
    }

static eAtRet DestMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 msbRegAddr = cAf6Reg_upen2 + muxBase;
    uint32 lsbRegAddr = cAf6Reg_upen3 + muxBase;
    uint32 lsb32Mac = 0;
    uint32 msb16Mac = mModuleHwRead(self, msbRegAddr);
    MacToHwReg(&msb16Mac, &lsb32Mac, address);
    mModuleHwWrite(self, msbRegAddr, msb16Mac);
    mModuleHwWrite(self, lsbRegAddr, lsb32Mac);

    return cAtOk;
    }

static eAtRet SourceMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 msb16Mac = mModuleHwRead(self, cAf6Reg_upen4 + muxBase);
    uint32 lsb32Mac = mModuleHwRead(self, cAf6Reg_upen5 + muxBase);
    HwRegToMac(msb16Mac, lsb32Mac, address);

    return cAtOk;
    }

static eAtRet DestMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    uint32 muxBase = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 msb16Mac = mModuleHwRead(self, cAf6Reg_upen2 + muxBase);
    uint32 lsb32Mac = mModuleHwRead(self, cAf6Reg_upen3 + muxBase);
    HwRegToMac(msb16Mac, lsb32Mac, address);

    return cAtOk;
    }

static uint32 InComBytes(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_upen13 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);
    return mRegField(regVal, cAf6_upen13_axisbytecnt_);
    }

static uint32 InComPackets(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_upen12 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);
    return mRegField(regVal, cAf6_upen12_axispktcnt_);
    }

static uint32 OutComingBytes(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_upen15 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);
    return mRegField(regVal, cAf6_upen15_axisbytecnt_);
    }

static uint32 OutComingPackets(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_upen14 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);
    return mRegField(regVal, cAf6_upen14_axispktcnt_);
    }

static uint32 DaSaMacErrorPackets(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_upen16 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0x0);
    return mRegField(regVal, cAf6_upen16_pktcnt_);
    }

static const char *NcoMode2String(uint32 ncoMode)
    {
    switch (ncoMode)
        {
        case 0 : return "DS1";
        case 1 : return "E1";
        case 2 : return "DS3";
        case 3 : return "E3";
        default: return "EC1";
        }
    }

static const char *SubTypeMode2String(uint32 subType)
    {
    if ((subType >= 0x80) && (subType <= 0xFF))
        return "control frames";

    if (subType == 0x0)
        return "DS1/E1 Frames";

    if (subType == 0x10)
        return "DS3/E3/EC-1 Frames";

    return "reserved";
    }

static void DebugPrintVal(const char *debugString, uint32 fieldVal)
    {
    AtPrintc(cSevNormal, "        %-30s: 0x%X\r\n", debugString, fieldVal);
    }

static void DebugPrintStr(const char * desc, const char *debugString)
    {
    AtPrintc(cSevNormal, "        %-30s: %s\r\n", desc, debugString);
    }

static uint32 RegsDisplay(ThaModulePdh self, AtPdhChannel de1, const char* regName, uint32 regBaseAddress)
    {
    uint32 regOffset = Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regAddr = regBaseAddress + regOffset;

    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)de1, regAddr, cAtModulePdh);
    return mModuleHwRead(self, regAddr);
    }

static eAtRet PdhMuxChannelRegsDebugShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint32 pathOffset, regVal = 0, liuId;
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    if (channelType == cAtPdhChannelTypeE1 || channelType == cAtPdhChannelTypeDs1)
        pathOffset = ThaPdhDe1FlatId((ThaPdhDe1)pdhChannel);
    else
        pathOffset = AtChannelIdGet((AtChannel)pdhChannel) * 32UL;

    AtPrintf("\r\n");
    regVal = RegsDisplay(self, pdhChannel, "Ethernet Encapsulation SA bit47_32 and Sub_Type Configuration", cAf6Reg_upen4);
    DebugPrintStr("Sub Type", SubTypeMode2String(mRegField(regVal, cAf6_upen4_Ether_Subtype_)));

    regVal = RegsDisplay(self, pdhChannel, "PDH to LIU Look-up Table", cAf6Reg_upen_c_0 + pathOffset);
    liuId = mRegField(regVal, cAf6_upen_c_0_LiuId_);
    DebugPrintStr("en/dis", mBoolToString(mRegField(regVal, cAf6_upen_c_0_LiuEnb_)));
    DebugPrintStr("Liu Mode", (mRegField(regVal, cAf6_upen_c_0_LiuMod_) == 0) ? "DE1" : "DE3/EC1");
    DebugPrintVal("Liu Id", liuId);

    regVal = RegsDisplay(self, pdhChannel, "LIU to PDH Look-up Table", cAf6Reg_upen_c_1 + liuId);
    DebugPrintStr("en/dis", mBoolToString(mRegField(regVal, cAf6_upen_c_1_LiuEnb_)));
    DebugPrintVal("Pdh Id", mRegField(regVal, cAf6_upen_c_1_PDHId_));

    regVal = RegsDisplay(self, pdhChannel, "NCO mode LIU to PDH", cAf6Reg_upen_c_2 + liuId);
    DebugPrintStr("NCO Mode", NcoMode2String(mRegField(regVal, cAf6_upen_c_2_txncomd_)));
    return cAtOk;
    }

static eBool StickyIsSet(uint32 regVal, uint32 mask)
    {
    return (mask & regVal) ? cAtTrue : cAtFalse;
    }

static uint32 StickiesRead2Clear(ThaModulePdh self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_upen17 + Tha60290011ModulePdhMuxBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if (r2c)
        mModuleHwWrite(self, regAddr, 0xFFFFFFFF);
    return regVal;
    }

static eBool IsSeqenceError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_Seq_err_Mask);
    }

static eBool IsEopError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_Eop_err_Mask);
    }

static eBool IsEthLengthError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_EthLen_err_Mask);
    }

static eBool IsEthTypeError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_EthTyp_err_Mask);
    }

static eBool IsSubTypeError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_SubTyp_err_Mask);
    }

static eBool IsDaMacError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_DA_err_Mask);
    }

static eBool IsSaMacError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Dec_SA_err_Mask);
    }

static eBool IsDecapReadEmptyBufferError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Decbuf_read_err_Mask);
    }

static eBool IsDecapWriteFullBufferError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Decbuf_write_err_Mask);
    }

static eBool IsEncapReadEmptyBufferError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Encbuf_read_err_Mask);
    }

static eBool IsEncapWriteFullBufferError(uint32 stickies)
    {
    return StickyIsSet(stickies, cAf6_upen17_Encbuf_write_err_Mask);
    }

static void ErrStickyPrint(const char * str, eBool isSet)
    {
    uint8 color = isSet ? cSevCritical : cSevInfo;
    AtPrintc(cSevNormal, "%s", str);
    AtPrintc(color, "%s\n", isSet ? "set" : "clear");
    }

static void Axi4StreamStickyStatusShow(ThaModulePdh self)
    {
    uint32 regVal = StickiesRead2Clear(self, cAtTrue);
    AtPrintc(cSevNormal, "\nStickies\n");
    ErrStickyPrint("  Sequence error                   : ", IsSeqenceError(regVal));
    ErrStickyPrint("  Eop error                        : ", IsEopError(regVal));
    ErrStickyPrint("  Ethernet length error            : ", IsEthLengthError(regVal));
    ErrStickyPrint("  Ethernet type error              : ", IsEthTypeError(regVal));
    ErrStickyPrint("  Sub type error                   : ", IsSubTypeError(regVal));
    ErrStickyPrint("  Da Mac error                     : ", IsDaMacError(regVal));
    ErrStickyPrint("  Sa Mac error                     : ", IsSaMacError(regVal));
    ErrStickyPrint("  Decapsulation read buffer empty  : ", IsDecapReadEmptyBufferError(regVal));
    ErrStickyPrint("  Decapsulation write buffer full  : ", IsDecapWriteFullBufferError(regVal));
    ErrStickyPrint("  Encapsulation read buffer empty  : ", IsEncapReadEmptyBufferError(regVal));
    ErrStickyPrint("  Encapsulation write buffer full  : ", IsEncapWriteFullBufferError(regVal));
    }

static ThaModulePmc PmcModule(ThaModulePdh self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePmc);
    }

static void Axi4StreamCounterStatusFromPmcShow(ThaModulePdh self)
    {
    AtPrintc(cSevNormal, "\nCounters\n");
    AtPrintc(cSevNormal, "  DE1 Byte counter           : %u\n", Tha60290011ModulePmcPdhMuxBytesGet(PmcModule(self), 0, cAtTrue));
    AtPrintc(cSevNormal, "  DE1 Packet counter         : %u\n", Tha60290011ModulePmcPdhMuxPacketsGet(PmcModule(self), 0, cAtTrue));
    AtPrintc(cSevNormal, "  DE3EC1 Byte Counter        : %u\n", Tha60290011ModulePmcPdhMuxBytesGet(PmcModule(self), 1, cAtTrue));
    AtPrintc(cSevNormal, "  DE3EC1 Packet Counter      : %u\n", Tha60290011ModulePmcPdhMuxPacketsGet(PmcModule(self), 1, cAtTrue));
    AtPrintc(cSevNormal, "  CTRL Byte Counter          : %u\n", Tha60290011ModulePmcPdhMuxBytesGet(PmcModule(self), 2, cAtTrue));
    AtPrintc(cSevNormal, "  CTRL Packet Counter        : %u\n", Tha60290011ModulePmcPdhMuxPacketsGet(PmcModule(self), 2, cAtTrue));
    }

static void Axi4StreamCounterStatusFromInternalShow(ThaModulePdh self)
    {
    AtPrintc(cSevNormal, "\nCounters\n");
    AtPrintc(cSevNormal, "  MAC to PDH Byte counter          : %u\n", InComBytes(self, cAtTrue));
    AtPrintc(cSevNormal, "  MAC to PDH Packet counter        : %u\n", InComPackets(self, cAtTrue));
    AtPrintc(cSevNormal, "  PDH to MAC Byte Counter          : %u\n", OutComingBytes(self, cAtTrue));
    AtPrintc(cSevNormal, "  PDH to MAC Packet Counter        : %u\n", OutComingPackets(self, cAtTrue));
    AtPrintc(cSevNormal, "  DA/SA error packet counter       : %u\n", DaSaMacErrorPackets(self, cAtTrue));
    }

static void Axi4StreamCounterStatusShow(ThaModulePdh self)
    {
    if (ThaModulePdhDebugCountersModuleGet(self) == cThaModulePmc)
        Axi4StreamCounterStatusFromPmcShow(self);
    else
        Axi4StreamCounterStatusFromInternalShow(self);
    }

static eAtRet Axi4StreamDebugShow(ThaModulePdh self)
    {
    uint8 daMacAddr[6], saMacAddr[6];
    SourceMacAddressGet(self, saMacAddr);
    DestMacAddressGet(self, daMacAddr);

    AtPrintc(cSevInfo, "  \nPDH MUX Info:\n");
    AtPrintc(cSevNormal, "  TX Da Mac Pdh Mux                   : %s\n", AtBytes2String(daMacAddr, cAtMacAddressLen, 16));
    AtPrintc(cSevNormal, "  TX Sa Mac Pdh Mux                   : %s\n", AtBytes2String(saMacAddr, cAtMacAddressLen, 16));
    AtPrintc(cSevNormal, "  TX Eth Type                         : 0x%X\n", EthTypeGet(self));
    AtPrintc(cSevNormal, "  TX Subtype                          : 0x%X\n", EthEncapSubTypeGet(self));
    AtPrintc(cSevNormal, "  TX Eth Length                       : %d\n", EthLengthGet(self));

    Axi4StreamStickyStatusShow(self);
    Axi4StreamCounterStatusShow(self);

    return cAtOk;
    }

uint32 Tha60290011ModulePdhMuxBaseAddress(ThaModulePdh self)
    {
    if(self)
        return 0xC00000;
    return cInvalidUint32;
    }

eAtRet Tha60290011ModulePdhMuxActive(ThaModulePdh self)
    {
    if (self)
        return PdhToEthEnable(self, cAtFalse) | PdhMuxActive(self);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhMuxTxEthEnable(AtModulePdh self, eBool enabled)
    {
    if (self)
        return PdhToEthEnable((ThaModulePdh)self, enabled);

    return cAtErrorNullPointer;
    }

eBool Tha60290011ModulePdhMuxTxEthIsEnabled(AtModulePdh self)
    {
    if (self)
        return PdhToEthIsEnabled((ThaModulePdh)self);

    return cAtFalse;
    }

eAtRet Tha60290011ModulePdhInterfaceAxi4Loopback(ThaModulePdh self, uint8 loopMode)
    {
    if (self)
        return PdhInterfaceAxi4Loopback(self, loopMode);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhLineMap(ThaModulePdh self, uint32 pdhId, uint32 liuId, eBool isMap)
    {
    if (self)
        return LineMap(self, pdhId, liuId, isMap);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhTxHwLineEnable(ThaModulePdh self, uint32 pdhId, eBool enable)
    {
    if (self)
        return TxHwLineEnable(self, pdhId, enable);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhRxHwLineEnable(ThaModulePdh self, uint32 liuId, eBool enable)
    {
    if (self)
        return RxHwLineEnable(self, liuId, enable);

    return cAtErrorNullPointer;
    }

eBool Tha60290011ModulePdhTxHwLineIsEnabled(ThaModulePdh self, uint32 pdhId)
    {
    if (self)
        return TxHwLineIsEnabled(self, pdhId);

    return cAtFalse;
    }

eBool Tha60290011ModulePdhRxHwLineIsEnabled(ThaModulePdh self, uint32 liuId)
    {
    if (self)
        return RxHwLineIsEnabled(self, liuId);

    return cAtTrue;
    }

eAtRet Tha60290011ModulePdhNcoLiuToPdhSet(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    if (self)
        return NcoLiuToPdhSet(self, pdhChannel);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhEthSubtypeSet(ThaModulePdh self, uint8 subtype)
    {
    if (self)
        return EthEncapSubTypeSet(self, subtype);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhEthLengthSet(ThaModulePdh self, uint16 length)
    {
    if (self)
        return EthLengthSet(self, length);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhEthTypeSet(ThaModulePdh self, uint16 type)
    {
    if (self)
        return EthTypeSet(self, type);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhMuxChannelRegsDebugShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    if (self)
        return PdhMuxChannelRegsDebugShow(self, pdhChannel);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhMuxAxi4StreamDebugShow(ThaModulePdh self)
    {
    if (self)
        return Axi4StreamDebugShow(self);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhSourceMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return SourceMacAddressSet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhDestMacAddressSet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return DestMacAddressSet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhSourceMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return SourceMacAddressGet(self, address);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModulePdhDestMacAddressGet(ThaModulePdh self, uint8 *address)
    {
    if (self)
        return DestMacAddressGet(self, address);

    return cAtErrorNullPointer;
    }

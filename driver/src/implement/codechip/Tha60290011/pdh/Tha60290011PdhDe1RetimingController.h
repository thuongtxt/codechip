/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * File        : Tha60290011PdhDe1RetimingController.h
 * 
 * Created Date: Dec 5, 2017
 *
 * Description : DE1 retiming controller interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHDE1RETIMINGCONTROLLER_H_
#define _THA60290011PDHDE1RETIMINGCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011PdhDe1RetimingController *Tha60290011PdhDe1RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290011PdhDe1RetimingController Tha60290011PdhDe1RetimingControllerNew(AtPdhChannel owner);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011PDHDE1RETIMINGCONTROLLER_H_ */


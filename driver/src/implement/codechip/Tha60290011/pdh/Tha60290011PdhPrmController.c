/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhPrmController.c
 *
 * Created Date: Jan 4, 2017
 *
 * Description : PRM implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../Tha60210031/pdh/Tha60210031PdhPrmControllerInternal.h"
#include "Tha60290011PdhPrmController.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011PdhPrmController
    {
    tTha60210031PdhPrmController super;
    }tTha60290011PdhPrmController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaPdhPrmControllerMethods m_ThaPdhPrmControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1Get(AtPdhPrmController self)
    {
    return (ThaPdhDe1)AtPdhPrmControllerDe1Get(self);
    }

static uint32 PrmOffset(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    ThaModulePdh modulePdh = (ThaModulePdh) AtChannelModuleGet((AtChannel) de1);

    return ThaModulePdhDe1PrmOffset(modulePdh, de1);
    }

static uint32 CounterOffset(ThaPdhPrmController self, eBool read2Clear)
    {
    uint8 hwRead2clear = read2Clear ? 0x0 : 0x1;
    return (PrmOffset((AtPdhPrmController)self) + (uint32)(hwRead2clear * 1024));
    }

static AtDevice Device(ThaPdhPrmController self)
    {
    return AtChannelDeviceGet((AtChannel)De1Get((AtPdhPrmController)self));
    }

static uint32 StartVersionHasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    return Tha60290011DeviceStartVersionHasPrmRxEnabledCfgRegister(Device(self));
    }

static void OverrideThaPdhPrmController(AtPdhPrmController self)
    {
    ThaPdhPrmController controller = (ThaPdhPrmController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmControllerOverride, mMethodsGet(controller), sizeof(m_ThaPdhPrmControllerOverride));

        mMethodOverride(m_ThaPdhPrmControllerOverride, CounterOffset);
        mMethodOverride(m_ThaPdhPrmControllerOverride, StartVersionHasRxEnabledCfgRegister);
        }

    mMethodsSet(controller, &m_ThaPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideThaPdhPrmController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhPrmController);
    }

static AtPdhPrmController ObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController Tha60290011PdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, de1);
    }

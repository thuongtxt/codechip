/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhVcDe1.c
 *
 * Created Date: Dec 14, 2016
 *
 * Description : Concrete class for PDH VC DE1 of 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "Tha60290011ModulePdh.h"
#include "Tha60290011PdhVcDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods               m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe1CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtFalse);

	return m_AtChannelMethods->CounterGet(self, counterType);
	}

static uint32 CounterClear(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe1CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtTrue);

	return m_AtChannelMethods->CounterClear(self, counterType);
	}

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtPdhLoopbackModeLocalPayload)
        return Tha602900xxDe1LocalPayloadIsSupported(self, loopbackMode);

    return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011PdhVcDe1);
    }

AtPdhDe1 Tha60290011PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210031PdhVcDe1ObjectInit(self, vc1x, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    return self;
    }

AtPdhDe1 Tha60290011PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return Tha60290011PdhVcDe1ObjectInit(newDe1, vc1x, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhSerialLine.c
 *
 * Created Date: Oct 31, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011PdhSerialLineInternal.h"
#include "Tha60290011ModulePdh.h"
#include "Tha60290011ModulePdhMuxReg.h"
#include "Tha60290011PdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPdhSerialLineMethods m_AtPdhSerialLineOverride;

/* Save super implementation */
static const tAtPdhSerialLineMethods *m_AtPdhSerialLineMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanChangeMode(AtPdhSerialLine self)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtChannelModuleGet((AtChannel)self);
    uint16 interface = Tha60290011ModulePdhInterfaceTypeGet(modulePdh);

    if (interface == cTha60290011PdhInterfaceTypeDe3)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ModeSet(AtPdhSerialLine self, uint32 mode)
    {
    eAtRet ret = cAtOk;

    if (!CanChangeMode(self))
        return cAtErrorNotApplicable;

    ret = m_AtPdhSerialLineMethods->ModeSet((AtPdhSerialLine)self, mode);
    if (ret != cAtOk)
        return ret;

    return Tha60290011ModulePdhNcoLiuToPdhSet((ThaModulePdh)AtChannelModuleGet((AtChannel)self), (AtPdhChannel)self);
    }

static void OverrideAtPdhSerialLine(AtPdhSerialLine self)
    {
    AtPdhSerialLine serialLine = (AtPdhSerialLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhSerialLineMethods = mMethodsGet(serialLine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhSerialLineOverride, m_AtPdhSerialLineMethods, sizeof(tAtPdhSerialLineMethods));

        mMethodOverride(m_AtPdhSerialLineOverride, ModeSet);
        }

    mMethodsSet(serialLine, &m_AtPdhSerialLineOverride);
    }

static void Override(AtPdhSerialLine self)
    {
    OverrideAtPdhSerialLine(self);
    }

AtPdhSerialLine Tha60290011PdhSerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011PdhSerialLine));

    /* Supper constructor */
    if (Tha60210031PdhSerialLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhSerialLine Tha60290011PdhSerialLineNew(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhSerialLine newSerialLine = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60290011PdhSerialLine));
    if (newSerialLine == NULL)
        return NULL;

    /* construct it */
    return Tha60290011PdhSerialLineObjectInit(newSerialLine, channelId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhDe1.c
 *
 * Created Date: Jun 11, 2016
 *
 * Description : DE1 implement for DE1 object from the PDH DE1 interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../map/Tha60290011ModuleMapDemap.h"
#include "../pmc/Tha60290011ModulePmc.h"
#include "Tha60290011PdhDe1Internal.h"
#include "Tha60290011PdhDe1.h"
#include "Tha60290011ModulePdhMuxReg.h"
#include "Tha60290011ModulePdh.h"
#include "Tha60290011PdhDe1RetimingController.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mRegWithBaseAddress(pdhModule, addr) ((addr) + Tha60290011ModulePdhMuxBaseAddress((ThaModulePdh)pdhModule))
#define mThis(de1) ((tTha60290011PdhDe1 *) de1)

/*--------------------------- Local typedefs ---------------------------------*/

typedef struct tTha60290011De1BackupInfo
    {
    eBool isSlipBufferEnable;
    eBool isSsmEnable;
    } tTha60290011De1BackupInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tThaPdhDe1Methods    m_ThaPdhDe1Override;
static tThaPdhVcDe1Methods  m_ThaPdhVcDe1Override;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;
static const tThaPdhDe1Methods    *m_ThaPdhDe1Methods    = NULL;
static const tThaPdhVcDe1Methods  *m_ThaPdhVcDe1Methods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh ModulePdh(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe1CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtFalse);

	return m_AtChannelMethods->CounterGet(self, counterType);
	}

static uint32 CounterClear(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe1CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtTrue);

	return m_AtChannelMethods->CounterClear(self, counterType);
	}

static void InInitProgressSet(AtPdhChannel self, eBool enable)
    {
    mThis(self)->isInInitProgress = enable;
    }

static eBool IsInInitProgress(AtPdhChannel self)
    {
    return mThis(self)->isInInitProgress;
    }

static eBool FrameTypeIsSwappedDs1E1Mode(AtPdhChannel self, uint16 frameType)
    {
    eBool ret = (AtPdhDe1IsE1((AtPdhDe1)self) == AtPdhDe1FrameTypeIsE1(frameType)) ? cAtFalse : cAtTrue;

    if (AtChannelShouldPreventReconfigure((AtChannel)self))
        return ret;

    return cAtTrue;
    }

static void De1BackupReset(tTha60290011De1BackupInfo *backup)
    {
    backup->isSlipBufferEnable = cAtFalse;
    backup->isSsmEnable = cAtFalse;
    }

static void De1Backup(AtPdhChannel self, tTha60290011De1BackupInfo *backup)
    {
    backup->isSlipBufferEnable = AtPdhChannelTxSlipBufferIsEnabled(self);
    backup->isSsmEnable = AtPdhChannelSsmIsEnabled(self);
    }

static void De1Restore(AtPdhChannel self, tTha60290011De1BackupInfo *backup)
    {
    if (ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        AtPdhChannelTxSlipBufferEnable(self, backup->isSlipBufferEnable);
    if (ThaPdhDe1SsmIsSupported((ThaPdhDe1)self))
        AtPdhChannelSsmEnable(self, backup->isSsmEnable);
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    ThaModulePdh pdhModule = ModulePdh((AtChannel)self);
    eTha60290011PdhInterfaceType currentType = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)pdhModule);
    tTha60290011De1BackupInfo backup;
    eBool isInited = cAtFalse;
    eAtRet dryRunRet = ThaPdhDe1BeforeChangeFrameTypeCheck(self, frameType);

    if (dryRunRet != cAtRetMoveNext)
        return dryRunRet;

    De1BackupReset(&backup);
    if (!IsInInitProgress(self) && FrameTypeIsSwappedDs1E1Mode(self, frameType))
        {
        uint32 flatDe1Id = ThaPdhDe1FlatId((ThaPdhDe1)self);
        uint32 de1Id = AtChannelIdGet((AtChannel)self);
        AtChannelEnable((AtChannel)self, cAtFalse);
        Tha60290011ModulePdhLineMap(pdhModule, flatDe1Id, de1Id, cAtFalse);
        ret |= Tha60290011ModulePdhLiuDe1FramerDeallocate(pdhModule, (AtPdhDe1)self);
        ret |= Tha60290011ModulePdhLiuDe1FramerAllocateWithE1Mode(pdhModule, (AtPdhDe1)self, AtPdhDe1FrameTypeIsE1(frameType));
        if (ret != cAtOk)
            return ret;

        AtChannelEnable((AtChannel)self, cAtTrue);
        InInitProgressSet(self, cAtTrue);
        De1Backup(self, &backup);
        ret |= AtChannelInit((AtChannel)self);
        isInited = cAtTrue;
        InInitProgressSet(self, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    if (isInited)
        De1Restore(self, &backup);

    /* After frame type set, Tha60290011ModulePdhNcoLiuToPdhSet will base on frame type to cfg the NCO */
    if (currentType == cTha60290011PdhInterfaceTypeDe1)
        {
        uint32 flatDe1Id = ThaPdhDe1FlatId((ThaPdhDe1)self);
        uint32 de1Id = AtChannelIdGet((AtChannel)self);
        Tha60290011ModulePdhLineMap(pdhModule, flatDe1Id, de1Id, cAtTrue);
        Tha60290011ModulePdhNcoLiuToPdhSet(pdhModule, self);
        }

    if (!AtChannelShouldPreventReconfigure((AtChannel)self))
        AtPdhChannelCacheFrameTypeSet(self, frameType);

    return ret;
    }

static void Delete(AtObject self)
    {
    AtSdhVc vc = AtPdhChannelVcInternalGet((AtPdhChannel)self);

    if (vc != NULL)
        {
        AtObjectDelete((AtObject)vc);
        AtPdhChannelVcSet((AtPdhChannel)self, NULL);
        }

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290011PdhDe1* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(isInInitProgress);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    uint32 flatDe1Id = ThaPdhDe1FlatId((ThaPdhDe1)self);
    uint32 de1Id = AtChannelIdGet(self);
    eAtRet ret = m_AtChannelMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    /* PDH to LIU */
    Tha60290011ModulePdhTxHwLineEnable(ModulePdh(self), flatDe1Id, enable);

    /* LIU to PDH */
    Tha60290011ModulePdhRxHwLineEnable(ModulePdh(self), de1Id, enable);

    return cAtOk;
    }

static eAtRet HwIdFactorGet(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1)
    {
    AtUnused(phyModule);
    Tha60290011PdhDe1InterfaceChannelId2HwId(self, hwStsInSlice, hwVtg, hwDe1);
    *slice = 0;
    return cAtOk;
    }

static uint32 FlatId(ThaPdhDe1 self, uint32 phyModule)
    {
    uint32 sts = 0, vtg = 0, vt = 0;
    AtUnused(phyModule);

    Tha60290011PdhDe1InterfaceChannelId2HwId(self, &sts, &vtg, &vt);
    return (sts * 32UL) + (vtg * 4UL) + vt;
    }

static uint8 PartId(ThaPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 Ds1E1J1DefaultOffset(ThaPdhDe1 self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    uint32 partOffset = ThaDeviceModulePartOffset(device, cAtModulePdh, mMethodsGet(self)->PartId(self));

    return (uint32)((int32)ThaPdhDe1FlatId(self) + (int32)partOffset);
    }

static uint32 De1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn)
    {
    uint32 offset = mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1);
    return r2cEn ? offset : (1024 + offset);
    }

static uint32 MapLineOffset(ThaPdhDe1 de1)
    {
    uint32 sts = 0, vtg = 0, vt = 0;

    Tha60290011PdhDe1InterfaceChannelId2HwId(de1, &sts, &vtg, &vt);
    return (uint32)(sts << 5) + (uint32)(vtg << 2) + vt;
    }

static uint32 SatopDs0BitMaskGet(ThaPdhDe1 self)
    {
    return AtPdhDe1IsE1((AtPdhDe1)self) ? cBit31_0 : cBit23_0;
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];
    AtSprintf(idString, "%u", AtChannelIdGet(self) + 1);
    return idString;
    }

static eBool ShouldPreserveService(ThaPdhDe1 self)
    {
    return Tha602900xxPdhDe1ShouldPreserveService(self);
    }

static AtSdhVc VcGet(AtPdhChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool ShouldConstrainToVc1xMapping(ThaPdhVcDe1 self)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtChannelModuleGet((AtChannel)self);

    if (Tha60290011ModulePdhInterfaceTypeGet(pdhModule) == cTha60290011PdhInterfaceTypeDe1)
        return cAtFalse;

    return m_ThaPdhVcDe1Methods->ShouldConstrainToVc1xMapping(self);
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void DebugFromPmc(AtChannel self)
    {
    AtPrintc(cSevNormal, "PDH MUX DE1 Bit counter : %u\r\n", Tha60290011ModulePmcPdhDe1BitGet((ThaModulePmc)PmcModule(self), AtChannelIdGet(self), cAtTrue));
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    if (ThaTdmChannelShouldGetCounterFromPmc(self))
        DebugFromPmc(self);

    return cAtOk;
    }

static eAtRet DefaultThreshold(ThaPdhDe1 self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    eAtRet ret = ThaModulePdhDefaultDe1Threshold(pdhModule, threshold);
    if (ret != cAtOk)
        return ret;

    threshold->losCntThres = 3;
    return cAtOk;
    }

static eBool RetimingIsSupported(ThaPdhDe1 self)
    {
    return Tha60290011DeviceDe1RetimingIsSupported(AtChannelDeviceGet((AtChannel)self));
    }

static ThaPdhRetimingController RetimingControllerCreate(ThaPdhDe1 self)
    {
    return (ThaPdhRetimingController)Tha60290011PdhDe1RetimingControllerNew((AtPdhChannel)self);
    }

static void OverrideThaPdhVcDe1(AtPdhDe1 self)
    {
    ThaPdhVcDe1 de1 = (ThaPdhVcDe1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhVcDe1Methods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhVcDe1Override, m_ThaPdhVcDe1Methods, sizeof(m_ThaPdhVcDe1Override));

        mMethodOverride(m_ThaPdhVcDe1Override, ShouldConstrainToVc1xMapping);
        }

    mMethodsSet(de1, &m_ThaPdhVcDe1Override);
    }

static void OverrideAtObject(AtPdhDe1 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(tAtPdhChannelMethods));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, VcGet);
        mMethodOverride(m_AtPdhChannelOverride, HasLineLayer);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPdhDe1(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe1Methods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, m_ThaPdhDe1Methods, sizeof(m_ThaPdhDe1Override));

        mMethodOverride(m_ThaPdhDe1Override, MapLineOffset);
        mMethodOverride(m_ThaPdhDe1Override, HwIdFactorGet);
        mMethodOverride(m_ThaPdhDe1Override, Ds1E1J1DefaultOffset);
        mMethodOverride(m_ThaPdhDe1Override, De1ErrCounterOffset);
        mMethodOverride(m_ThaPdhDe1Override, FlatId);
        mMethodOverride(m_ThaPdhDe1Override, SatopDs0BitMaskGet);
        mMethodOverride(m_ThaPdhDe1Override, PartId);
        mMethodOverride(m_ThaPdhDe1Override, ShouldPreserveService);
        mMethodOverride(m_ThaPdhDe1Override, DefaultThreshold);
        mMethodOverride(m_ThaPdhDe1Override, RetimingControllerCreate);
        mMethodOverride(m_ThaPdhDe1Override, RetimingIsSupported);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideThaPdhDe1(self);
    OverrideThaPdhVcDe1(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011PdhDe1);
    }

AtPdhDe1 Tha60290011PdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210031PdhVcDe1ObjectInit(self, NULL, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Correct channel ID */
    AtChannelIdSet((AtChannel)self, channelId);

    return self;
    }

AtPdhDe1 Tha60290011PdhDe1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011PdhDe1ObjectInit(newDe1, channelId, module);
    }

void Tha60290011PdhDe1InterfaceChannelId2HwId(ThaPdhDe1 self, uint32 *sts, uint32 *vtg, uint32 *vt)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);

    *sts = AtSdhChannelSts1Get(vc);
    *vtg = AtSdhChannelTug2Get(vc);
    *vt  = AtSdhChannelTu1xGet(vc);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhDe1RetimingController.c
 *
 * Created Date: Dec 4, 2017
 *
 * Description : DE1 Retiming controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#include "../../../default/pdh/retimingcontrollers/ThaPdhDe1RetimingControllerInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60290011ModulePdh.h"
#include "Tha60290011PdhDe1RetimingReg.h"
#include "Tha60290011PdhDe1RetimingController.h"

/*--------------------------- Define -----------------------------------------*/
typedef struct tTha60290011PdhDe1RetimingController
    {
    tThaPdhDe1RetimingController super;
    }tTha60290011PdhDe1RetimingController;

/*--------------------------- Macros -----------------------------------------*/
#define mThaDe1RetimingController(self) ((ThaPdhDe1RetimingController)(self))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaPdhRetimingControllerMethods    m_ThaPdhRetimingControllerOverride;
static tThaPdhDe1RetimingControllerMethods m_ThaPdhDe1RetimingControllerOverride;

/* Save super implementation */
static const tThaPdhRetimingControllerMethods    *m_ThaPdhRetimingControllerMethods    = NULL;
static const tThaPdhDe1RetimingControllerMethods *m_ThaPdhDe1RetimingControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1(ThaPdhDe1RetimingController self)
    {
    return (ThaPdhDe1)ThaPdhRetimingControllerOwnerGet((ThaPdhRetimingController)self);
    }

static ThaModulePdh ModulePdh(ThaPdhDe1RetimingController self)
    {
    return (ThaModulePdh)AtChannelModuleGet((AtChannel)De1(self));
    }

static uint32 LiuBaseAddress(ThaPdhDe1RetimingController self)
    {
    return Tha60290011ModulePdhMuxBaseAddress(ModulePdh(self));
    }

static uint32 ChannelOffset(ThaPdhDe1RetimingController self)
    {
    return AtChannelIdGet((AtChannel)De1(self));
    }

static uint32 AddressWithLocalAddress(ThaPdhDe1RetimingController self, uint32 localAddress)
    {
    return localAddress + LiuBaseAddress(self) + ChannelOffset(self);
    }

static uint32 SlipBufferTxFramerControlRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_txctrl00_pen_Base);
    }

static uint32 SlipBufferTxFramerControlRegister1(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_txctrl01_pen_Base);
    }

static uint32 SlipBufferRxFramerControlRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_rxctrl_pen_Base);
    }

static uint32 SlipBufferSignalingEnableRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_ssmsigbuf_pen_Base);
    }

static uint32 SlipBufferSsmEnableRegister(ThaPdhDe1RetimingController self)
    {
    return SlipBufferTxFramerControlRegister1(self);
    }

static uint32 SlipBufferSsmRxStatusRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_status_pen_Base);
    }

static uint32 SlipBufferSsmRxCrcCounterRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_rxfrm_crc_err_cnt_Base);
    }

static uint32 SlipBufferSsmRxReiCounterRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_rxfrm_rei_cnt_Base);
    }

static uint32 SlipBufferSsmRxFbeCounterRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_rxfrm_fbe_cnt_Base);
    }

static void SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate(ThaPdhDe1RetimingController self, uint16 frameType)
    {
    uint32 address;
    uint32 regVal;
    ThaPdhDe1 channel = De1(self);

    address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_TxDE1Md_, ThaPdhDe1FrameModeSw2Hw(channel, frameType));
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);
    }

static uint32 DataSlipStickyRegister(ThaPdhDe1RetimingController self)
    {
    return cAf6Reg_stk0_upen_Base +  LiuBaseAddress(self) + (ChannelOffset(self)/32)*2;
    }

static uint32 FrameSlipStickyRegister(ThaPdhDe1RetimingController self)
    {
    return cAf6Reg_stk1_upen_Base +  LiuBaseAddress(self) + (ChannelOffset(self)/32)*2;
    }

static void SlipBufferTxHwDebugFrameStickyShow(ThaPdhDe1RetimingController self)
    {
    uint32 address, regVal, regField, fieldIdx;
    AtPdhChannel channel = (AtPdhChannel)De1(self);

    fieldIdx = ChannelOffset(self) % 32;
    /* show data slip sticky */
    address = DataSlipStickyRegister(self);
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mFieldGet(regVal, (cBit0 << fieldIdx), fieldIdx, uint32, &regField);
    AtPrintc(cSevNormal, "  Slip Buffer Data slip sticky: %s \n", regField ? "raise":"clear");
    if (regField)
        {
        regVal = 0;
        mFieldIns(&regVal, (cBit0 << fieldIdx), fieldIdx, regField);
        mChannelHwWrite(channel, address, regVal, cAtModulePdh);
        }

    /* show frame slip sticky */
    address = FrameSlipStickyRegister(self);
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mFieldGet(regVal, (cBit0 << fieldIdx), fieldIdx, uint32, &regField);
    AtPrintc(cSevNormal, "  Slip Buffer Frame slip sticky: %s \n", regField ? "raise":"clear");
    if (regField)
        {
        regVal = 0;
        mFieldIns(&regVal, (cBit0 << fieldIdx), fieldIdx, regField);
        mChannelHwWrite(channel, address, regVal, cAtModulePdh);
        }
    }

static eAtRet SlipBufferTxSignalingEnable(ThaPdhDe1RetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferSignalingEnableRegister(mThaDe1RetimingController(self));
    uint32 regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_ssmsigbuf_pen_sigins_enb_, enable ? 1 : 0);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferTxSignalingIsEnabled(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferSignalingEnableRegister(mThaDe1RetimingController(self));
    uint32 regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (mRegField(regVal, cAf6_ssmsigbuf_pen_sigins_enb_) ? cAtTrue : cAtFalse);
    }

static eAtRet SlipBufferEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferSsmEnableRegister(mThaDe1RetimingController(self));
    uint32 mask = cAf6_txctrl01_pen_RetEna_Mask;
    uint32 shift = cAf6_txctrl01_pen_RetEna_Shift;
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferIsEnabled(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferSsmEnableRegister(mThaDe1RetimingController(self));
    uint32 mask = cAf6_txctrl01_pen_RetEna_Mask;
    uint32 regVal;
    regVal = mChannelHwRead(channel , address, cAtModulePdh);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static void SlipBufferDefaultSet(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address;
    uint32 regVal;

    address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    regVal = 0;
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    address = SlipBufferTxFramerControlRegister1(mThaDe1RetimingController(self));
    regVal = 0;
    mFieldIns(&regVal, cAf6_txctrl01_pen_E1Bypass_Mask, cAf6_txctrl01_pen_E1Bypass_Shift, 1);
    mFieldIns(&regVal, cAf6_txctrl01_pen_E2Bypass_Mask, cAf6_txctrl01_pen_E2Bypass_Shift, 1);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    address = mMethodsGet(mThaDe1RetimingController(self))->SlipBufferRxFramerControlRegister(mThaDe1RetimingController(self));
    mChannelHwWrite(channel, address, 0, cAtModulePdh);

    SlipBufferEnable(self, cAtFalse);

    if (ThaModulePdhTxRetimingSignalingIsSupported(ModulePdh((ThaPdhDe1RetimingController)self)))
        {
        address = SlipBufferSignalingEnableRegister(mThaDe1RetimingController(self));
        mChannelHwWrite(channel, address, 0, cAtModulePdh);
        }
    }

static eAtRet SlipBufferSsmBomValueSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 address = SlipBufferTxFramerControlRegister(self);
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMMess_, value);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMCount_, nTimes);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferBomSendingIsReady(ThaPdhDe1RetimingController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SlipBufferBomSendingCanWaitForReady(ThaPdhDe1RetimingController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet SlipBufferBomSendingWaitForReady(ThaPdhDe1RetimingController self)
    {
    uint32 elapse = 0, timeoutMs = 100;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtOsalCurTime curTime, startTime;

    if (!mMethodsGet(self)->SlipBufferBomSendingCanWaitForReady(self))
        return cAtOk;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);

    while (elapse < timeoutMs)
        {
        if (mMethodsGet(self)->SlipBufferBomSendingIsReady(self))
            return cAtOk;

        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorDevBusy;
    }

static eAtRet SlipBufferSsmBomToggleSending(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 regVal;
    uint32 address = SlipBufferTxFramerControlRegister(self);
    eAtRet ret = cAtOk;

    ret = SlipBufferBomSendingWaitForReady(self);
    if (ret != cAtOk)
        return ret;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMEna_, 0);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    SlipBufferSsmBomValueSet(self, value, nTimes);

    /* Must sleep to compliant to HW design */
    AtOsalUSleep(250);

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMEna_, 1);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static void SlipBufferSsmSaBitsMaskSet(ThaPdhDe1RetimingController self, uint8 saBitsMask)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMSasel_, saBitsMask);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);
    }

static uint8 SlipBufferSsmSaBitsMaskGet(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_txctrl00_pen_SSMSasel_);
    }

static uint32 TxSlipCounterRegister(void)
    {
    return cAf6Reg_cnt0_pen_Base;
    }

static uint32 TxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 channelId = ChannelOffset(mThaDe1RetimingController(self));
    uint32 offset = r2c ? channelId : (128 + channelId);
    uint32 baseAddress = TxSlipCounterRegister() + LiuBaseAddress(mThaDe1RetimingController(self));
    return mChannelHwRead(channel, (baseAddress + offset), cAtModulePdh);
    }

static eAtRet SlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    eBool isE1 = AtPdhDe1IsE1((AtPdhDe1)channel);

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMEna_, enable ? 1 : 0);
    mRegFieldSet(regVal, cAf6_txctrl00_pen_SSMCount_, isE1 ? 0 : 10); /* Just need to send continously in case of E1 */
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferSsmIsEnabled(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;
    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (regVal & cAf6_txctrl00_pen_SSMEna_Mask) ? cAtTrue : cAtFalse;
    }

static uint8 SlipBufferTxSsmGet(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThaDe1RetimingController(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThaDe1RetimingController(self));
    uint32 regVal;

    regVal = mChannelHwRead(channel, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_txctrl00_pen_SSMMess_);
    }

static void OverrideThaPdhRetimingController(Tha60290011PdhDe1RetimingController self)
    {
    ThaPdhRetimingController variable = (ThaPdhRetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhRetimingControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhRetimingControllerOverride, m_ThaPdhRetimingControllerMethods, sizeof(m_ThaPdhRetimingControllerOverride));

        mMethodOverride(m_ThaPdhRetimingControllerOverride, TxCsCounter);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxSsmGet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferDefaultSet);
        }

    mMethodsSet(variable, &m_ThaPdhRetimingControllerOverride);
    }

static void OverrideThaPdhDe1RetimingController(Tha60290011PdhDe1RetimingController self)
    {
    ThaPdhDe1RetimingController variable = (ThaPdhDe1RetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe1RetimingControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1RetimingControllerOverride, m_ThaPdhDe1RetimingControllerMethods, sizeof(m_ThaPdhDe1RetimingControllerOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferRxFramerControlRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxStatusRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxCrcCounterRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxReiCounterRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmRxFbeCounterRegister);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmSaBitsMaskSet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmSaBitsMaskGet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferBomSendingCanWaitForReady);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmBomValueSet);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferBomSendingIsReady);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferSsmBomToggleSending);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferTxHwDebugFrameStickyShow);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferTxSignalingEnable);
        mMethodOverride(m_ThaPdhDe1RetimingControllerOverride, SlipBufferTxSignalingIsEnabled);
        }

    mMethodsSet(variable, &m_ThaPdhDe1RetimingControllerOverride);
    }

static void Override(Tha60290011PdhDe1RetimingController self)
    {
    OverrideThaPdhRetimingController(self);
    OverrideThaPdhDe1RetimingController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011PdhDe1RetimingController);
    }

static Tha60290011PdhDe1RetimingController ObjectInit(Tha60290011PdhDe1RetimingController self, AtPdhChannel owner)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDe1RetimingControllerObjectInit((ThaPdhDe1RetimingController)self, owner) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60290011PdhDe1RetimingController Tha60290011PdhDe1RetimingControllerNew(AtPdhChannel owner)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290011PdhDe1RetimingController newTha60290011PdhDe1RetimingController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newTha60290011PdhDe1RetimingController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newTha60290011PdhDe1RetimingController, owner);
    }

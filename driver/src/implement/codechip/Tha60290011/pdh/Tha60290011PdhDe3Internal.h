/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhDe3Internal.h
 *
 * Created Date: Nov 21, 2016 
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHDE3INTERNAL_H_
#define _THA60290011PDHDE3INTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011PdhDe3
    {
    tTha60210031PdhDe3 super;
    }tTha60290011PdhDe3;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha60290011PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011PDHDE3INTERNAL_H_ */

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_PDHMUX_RD_H_
#define _AF6_REG_AF6CNC0011_RD_PDHMUX_RD_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Active Regiter
Reg Addr   : 0x0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures Active for all Arrive modules

------------------------------------------------------------------------------*/
#define cAf6Reg_upen0                                                                                   0x0000

/*--------------------------------------
BitField Name: Tx_Eth_dis
--------------------------------------*/
#define cAf6_upen0_Tx_Eth_dis_Mask                                                                    cBit3
#define cAf6_upen0_Tx_Eth_dis_Shift                                                                       3

/*--------------------------------------
BitField Name: AXI4_Stream_loopout
BitField Type: R/W
BitField Desc: AXI4 Stream loop_out
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen0_AXI4_Stream_loopout_Mask                                                              cBit2
#define cAf6_upen0_AXI4_Stream_loopout_Shift                                                                 2

/*--------------------------------------
BitField Name: AXI4_Stream_loopin
BitField Type: R/W
BitField Desc: AXI4 Stream loop_in
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen0_AXI4_Stream_loopin_Mask                                                               cBit1
#define cAf6_upen0_AXI4_Stream_loopin_Shift                                                                  1

/*--------------------------------------
BitField Name: Active
BitField Type: RW
BitField Desc: Module Active - Set 1: Active all modules - Set 0: De-active all
modules + Flush all FIFOs + Reset all Status and Control Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen0_Active_Mask                                                                           cBit0
#define cAf6_upen0_Active_Shift                                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Version ID
Reg Addr   : 0x0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register shows Version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen1                                                                                   0x0001

/*--------------------------------------
BitField Name: Verion_Year
BitField Type: RO
BitField Desc: Version Year
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen1_Verion_Year_Mask                                                                  cBit31_24
#define cAf6_upen1_Verion_Year_Shift                                                                        24

/*--------------------------------------
BitField Name: Verion_Month
BitField Type: RO
BitField Desc: Version Month
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen1_Verion_Month_Mask                                                                 cBit23_16
#define cAf6_upen1_Verion_Month_Shift                                                                       16

/*--------------------------------------
BitField Name: Verion_Date
BitField Type: RO
BitField Desc: Version Date
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen1_Verion_Date_Mask                                                                   cBit15_8
#define cAf6_upen1_Verion_Date_Shift                                                                         8

/*--------------------------------------
BitField Name: Verion_Number
BitField Type: RO
BitField Desc: Version Number
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen1_Verion_Number_Mask                                                                  cBit7_0
#define cAf6_upen1_Verion_Number_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : PDH to LIU Look-up Table
Reg Addr   : 0x1000 - 0x13FF
Reg Formula: 0x1000 + stsid*32 + vtgid*4 + vtid
    Where  : 
           + $stsid(0-23): stsid
           + $vtgid(0-6): stsid
           + $vtid(0-2): $vtid
Reg Desc   : 
This configure is used to convert ID PDH to LIU ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_c_0                                                                                0x1000

/*--------------------------------------
BitField Name: LiuEnb
BitField Type: R/W
BitField Desc: LIU Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_c_0_LiuEnb_Mask                                                                        cBit8
#define cAf6_upen_c_0_LiuEnb_Shift                                                                           8

/*--------------------------------------
BitField Name: LiuMod
BitField Type: R/W
BitField Desc: LIU Mode - 0: For DE1 - 1: For DE3/EC1
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_c_0_LiuMod_Mask                                                                        cBit7
#define cAf6_upen_c_0_LiuMod_Shift                                                                           7

/*--------------------------------------
BitField Name: LiuId
BitField Type: R/W
BitField Desc: Liu ID - 0-83:For DE1 - 0-23:For DE3/EC1
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_upen_c_0_LiuId_Mask                                                                       cBit6_0
#define cAf6_upen_c_0_LiuId_Shift                                                                            0


/*------------------------------------------------------------------------------
Reg Name   : LIU to PDH Look-up Table
Reg Addr   : 0x1400 - 0x1453
Reg Formula: 0x1400 + liuid
    Where  : 
           + $liuid(0-83): liuid
Reg Desc   : 
This configure is used to convert ID PDH to LIU ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_c_1                                                                                0x1400

/*--------------------------------------
BitField Name: LiuEnb
BitField Type: R/W
BitField Desc: LIU Enable
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_c_1_LiuEnb_Mask                                                                       cBit11
#define cAf6_upen_c_1_LiuEnb_Shift                                                                          11

/*--------------------------------------
BitField Name: PDHId
BitField Type: R/W
BitField Desc: PDH ID (STS,VTG,VT)
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_upen_c_1_PDHId_Mask                                                                       cBit9_0
#define cAf6_upen_c_1_PDHId_Shift                                                                            0


/*------------------------------------------------------------------------------
Reg Name   : NCO mode LIU to PDH
Reg Addr   : 0x1500 - 0x1553
Reg Formula: 0x1500 + liuid
    Where  : 
           + $liuid(0-83): liuid
Reg Desc   : 
This configure is used to convert ID PDH to LIU ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_c_2                                                                                0x1500

/*--------------------------------------
BitField Name: txncomd
BitField Type: R/W
BitField Desc: tx_nco_mode - Set 4: EC1. - Set 3: E3. - Set 2: DS3. - Set 1: E1.
- Set 0: DS1
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_c_2_txncomd_Mask                                                                     cBit3_0
#define cAf6_upen_c_2_txncomd_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation DA bit47_32 Configuration
Reg Addr   : 0x0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit47_32

------------------------------------------------------------------------------*/
#define cAf6Reg_upen2                                                                                   0x0002

/*--------------------------------------
BitField Name: DA_bit47_32
BitField Type: R/W
BitField Desc: DA bit47_32
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen2_DA_bit47_32_Mask                                                                   cBit15_0
#define cAf6_upen2_DA_bit47_32_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation DA bit32_00 Configuration
Reg Addr   : 0x0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit32_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen3                                                                                   0x0003

/*--------------------------------------
BitField Name: DA_bit31_00
BitField Type: R/W
BitField Desc: DA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen3_DA_bit31_00_Mask                                                                   cBit31_0
#define cAf6_upen3_DA_bit31_00_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation SA bit47_32 Configuration
Reg Addr   : 0x0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the SA bit47_32 and Sub_Type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen4                                                                                   0x0004

/*--------------------------------------
BitField Name: Ether_Subtype
BitField Type: R/W
BitField Desc: Ethernet SubType - 0x0           : DS1/E1 Frames - 0x1-0xF
: reserved - 0x10               : DS3/E3/EC-1 Frames - 0x11-0x7F        :
reserved - 0x80-0xFF  : control frames
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen4_Ether_Subtype_Mask                                                                cBit23_16
#define cAf6_upen4_Ether_Subtype_Shift                                                                      16

/*--------------------------------------
BitField Name: SA_bit47_32
BitField Type: R/W
BitField Desc: SA bit47_32
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen4_SA_bit47_32_Mask                                                                   cBit15_0
#define cAf6_upen4_SA_bit47_32_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation SA bit32_00 Configuration
Reg Addr   : 0x0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the SA bit32_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen5                                                                                   0x0005

/*--------------------------------------
BitField Name: SA_bit31_00
BitField Type: R/W
BitField Desc: SA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen5_SA_bit31_00_Mask                                                                   cBit31_0
#define cAf6_upen5_SA_bit31_00_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation EtheType and EtherLength  Configuration
Reg Addr   : 0x0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the EtheType and EtherLength

------------------------------------------------------------------------------*/
#define cAf6Reg_upen6                                                                                   0x0006

/*--------------------------------------
BitField Name: Ether_Type
BitField Type: R/W
BitField Desc: Ethernet Type
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen6_Ether_Type_Mask                                                                   cBit31_16
#define cAf6_upen6_Ether_Type_Shift                                                                         16

/*--------------------------------------
BitField Name: Ether_Len
BitField Type: R/W
BitField Desc: Ethernet Length - DS1/E1: number of DS1/E1 * 2 bytes(TDM data) +
2 bytes (seq num) + 84 bytes (TDM counter) - DS3/E3/EC-1: number of DS3/E3/EC-1
* 16 bytes(TDM data) + 2 bytes (seq num) + 36 bytes (TDM counter) - Control
frames: length of control frame payload
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen6_Ether_Len_Mask                                                                     cBit15_0
#define cAf6_upen6_Ether_Len_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Overrun Counter
Reg Addr   : 0x0200 - 0x0253
Reg Formula: 0x0200 + liuid
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
TX LIU overrun counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen7                                                                                   0x0200

/*--------------------------------------
BitField Name: txliuoverrun_cnt
BitField Type: R/W
BitField Desc: Tx_LUI_overrun_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen7_txliuoverrun_cnt_Mask                                                              cBit31_0
#define cAf6_upen7_txliuoverrun_cnt_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : TX LIU FIFO Underrun Counter
Reg Addr   : 0x0280 - 0x02D3
Reg Formula: 0x0280 + liuid
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
TX LIU Underrun counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen8                                                                                   0x0280

/*--------------------------------------
BitField Name: txliuunderrun_cnt
BitField Type: R/W
BitField Desc: Tx_LUI_underrun_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen8_txliuunderrun_cnt_Mask                                                             cBit31_0
#define cAf6_upen8_txliuunderrun_cnt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RX PDH FIFO Overrun
Reg Addr   : 0x0010
Reg Formula: 
    Where  : 
Reg Desc   : 
RX LIU FIFO is full. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen9                                                                                   0x0010

/*--------------------------------------
BitField Name: rxliufull
BitField Type: R/W
BitField Desc: Rx_Liu_Fifo_Full - Bit#0 for LIU ID#0 - Bit#1 for LIU ID#1 -
Bit#31 for LIU ID#31 - Set 1: shown FIFO Full
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen9_rxliufull_Mask                                                                     cBit31_0
#define cAf6_upen9_rxliufull_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (PDH to MAC) Packet Counter
Reg Addr   : 0x0013
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Packet Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen12                                                                                  0x0013

/*--------------------------------------
BitField Name: axispktcnt
BitField Type: R/W
BitField Desc: axis_packect_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen12_axispktcnt_Mask                                                                   cBit31_0
#define cAf6_upen12_axispktcnt_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (MAC to PDH) Byte Counter
Reg Addr   : 0x0014
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Byte Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen13                                                                                  0x0014

/*--------------------------------------
BitField Name: axisbytecnt
BitField Type: R/W
BitField Desc: axis_byte_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen13_axisbytecnt_Mask                                                                  cBit31_0
#define cAf6_upen13_axisbytecnt_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (PDH to MAC) Packet Counter
Reg Addr   : 0x0015
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Packet Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen14                                                                                  0x0015

/*--------------------------------------
BitField Name: axispktcnt
BitField Type: R/W
BitField Desc: axis_packect_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen14_axispktcnt_Mask                                                                   cBit31_0
#define cAf6_upen14_axispktcnt_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : AXI4-Stream (PDH to MAC) Byte Counter
Reg Addr   : 0x0016
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4-Stream Byte Counter. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen15                                                                                  0x0016

/*--------------------------------------
BitField Name: axisbytecnt
BitField Type: R/W
BitField Desc: axis_byte_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen15_axisbytecnt_Mask                                                                  cBit31_0
#define cAf6_upen15_axisbytecnt_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DA/SA error packet counter
Reg Addr   : 0x0016
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter DA/SA error packet of Decapsulation. Write 0x0 to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen16                                                                                  0x0016

/*--------------------------------------
BitField Name: pktcnt
BitField Type: R/W
BitField Desc: axis_byte_counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen16_pktcnt_Mask                                                                       cBit31_0
#define cAf6_upen16_pktcnt_Shift                                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Debug Stick 0
Reg Addr   : 0x0017
Reg Formula: 
    Where  : 
Reg Desc   : 
This sticky is used to debug. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen17                                                                                  0x0017

/*--------------------------------------
BitField Name: Dec_Seq_err
BitField Type: R/W
BitField Desc: Dec detect Sequence Error
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen17_Dec_Seq_err_Mask                                                                    cBit10
#define cAf6_upen17_Dec_Seq_err_Shift                                                                       10

/*--------------------------------------
BitField Name: Dec_Eop_err
BitField Type: R/W
BitField Desc: Dec detect EOP Error
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen17_Dec_Eop_err_Mask                                                                     cBit9
#define cAf6_upen17_Dec_Eop_err_Shift                                                                        9

/*--------------------------------------
BitField Name: Dec_EthLen_err
BitField Type: R/W
BitField Desc: Dec detect Ethernet Lenght Error
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen17_Dec_EthLen_err_Mask                                                                  cBit8
#define cAf6_upen17_Dec_EthLen_err_Shift                                                                     8

/*--------------------------------------
BitField Name: Dec_EthTyp_err
BitField Type: R/W
BitField Desc: Dec detect Ethernet Type Error
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen17_Dec_EthTyp_err_Mask                                                                  cBit7
#define cAf6_upen17_Dec_EthTyp_err_Shift                                                                     7

/*--------------------------------------
BitField Name: Dec_SubTyp_err
BitField Type: R/W
BitField Desc: Dec detect Sub Type Error
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen17_Dec_SubTyp_err_Mask                                                                  cBit6
#define cAf6_upen17_Dec_SubTyp_err_Shift                                                                     6

/*--------------------------------------
BitField Name: Dec_DA_err
BitField Type: R/W
BitField Desc: Dec detect DA error
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen17_Dec_DA_err_Mask                                                                      cBit5
#define cAf6_upen17_Dec_DA_err_Shift                                                                         5

/*--------------------------------------
BitField Name: Dec_SA_err
BitField Type: R/W
BitField Desc: Dec detect SA error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen17_Dec_SA_err_Mask                                                                      cBit4
#define cAf6_upen17_Dec_SA_err_Shift                                                                         4

/*--------------------------------------
BitField Name: Decbuf_read_err
BitField Type: R/W
BitField Desc: Dec buffer read when buffer is empty
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen17_Decbuf_read_err_Mask                                                                 cBit3
#define cAf6_upen17_Decbuf_read_err_Shift                                                                    3

/*--------------------------------------
BitField Name: Decbuf_write_err
BitField Type: R/W
BitField Desc: Dec buffer write when buffer is full
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen17_Decbuf_write_err_Mask                                                                cBit2
#define cAf6_upen17_Decbuf_write_err_Shift                                                                   2

/*--------------------------------------
BitField Name: Encbuf_read_err
BitField Type: R/W
BitField Desc: Enc buffer read when buffer is empty
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen17_Encbuf_read_err_Mask                                                                 cBit1
#define cAf6_upen17_Encbuf_read_err_Shift                                                                    1

/*--------------------------------------
BitField Name: Encbuf_write_err
BitField Type: R/W
BitField Desc: Enc buffer write when buffer is full
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen17_Encbuf_write_err_Mask                                                                cBit0
#define cAf6_upen17_Encbuf_write_err_Shift                                                                   0

#endif /* _AF6_REG_AF6CNC0011_RD_PDHMUX_RD_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011ModulePdhInternal.h
 *
 * Created Date: Nov 21, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPDHINTERNAL_H_
#define _THA60290011MODULEPDHINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "Tha60290011ModulePdh.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePdh
    {
    tTha60210031ModulePdh super;

    /* Private data */
    eTha60290011PdhInterfaceType interfaceType;
    }tTha60290011ModulePdh;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60290011ModulePdhObjectInit(AtModulePdh self, AtDevice device);
uint16 Tha60290011ModulePdhLengthDefaultGet(ThaModulePdh self);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPDHINTERNAL_H_ */

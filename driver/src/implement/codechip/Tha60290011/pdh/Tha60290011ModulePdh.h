/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : 60290011 PDH
 *
 * File        : Tha60290011ModulePdh.h
 *
 * Created Date: Jun 11, 2016
 *
 * Description : Interface of the PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPDH_H_
#define _THA60290011MODULEPDH_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/
#define cTha60290011StartDs1StsHwId 0
#define cTha60290011StartE1StsHwId  3
#define cTha60290011NumDs1HwSts     3
#define cTha60290011NumE1HwSts      4
#define cLiuModeDe1                 0
#define cLiuModeDe3Ec1              1

/*--------------------------- Macros -----------------------------------------*/
#define mDe3ToLiuMuxId(de3Id) (de3Id)
#define mPdhDe3HwId(stsId) ((stsId) * 32UL)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011ModulePdh *Tha60290011ModulePdh;

typedef enum eTha60290011PdhInterfaceType
    {
    cTha60290011PdhInterfaceTypeDe3 = 0,
    cTha60290011PdhInterfaceTypeDe1 = 1,
    cTha60290011PdhInterfaceNum
    } eTha60290011PdhInterfaceType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eTha60290011PdhInterfaceType Tha60290011ModulePdhInterfaceTypeGet(AtModulePdh self);
eAtRet Tha60290011ModulePdhInterfaceTypeSet(AtModulePdh self, eTha60290011PdhInterfaceType type);
uint32 Tha60290011ModulePdhMuxBaseAddress(ThaModulePdh self);
eAtRet Tha60290011ModulePdhLiuDe1FramerAllocateWithE1Mode(ThaModulePdh self, AtPdhDe1 de1, eBool isE1);
eAtRet Tha60290011ModulePdhLiuDe1FramerDeallocate(ThaModulePdh self, AtPdhDe1 de1);
eAtRet Tha60290011ModulePdhInterfaceAxi4Loopback(ThaModulePdh self, uint8 loopMode);
eAtRet Tha60290011ModulePdhNcoLiuToPdhSet(ThaModulePdh self, AtPdhChannel pdh);
eAtRet Tha60290011ModulePdhTxHwLineEnable(ThaModulePdh self, uint32 pdhId, eBool enable);
eAtRet Tha60290011ModulePdhRxHwLineEnable(ThaModulePdh self, uint32 liuId, eBool enable);
eBool Tha60290011ModulePdhTxHwLineIsEnabled(ThaModulePdh self, uint32 pdhId);
eBool Tha60290011ModulePdhRxHwLineIsEnabled(ThaModulePdh self, uint32 liuId);
eAtRet Tha60290011ModulePdhLineMap(ThaModulePdh self, uint32 pdhId, uint32 liuId, eBool isMap);
eAtRet Tha60290011ModulePdhNcoLiuToPdhSet(ThaModulePdh self, AtPdhChannel pdh);
eAtRet Tha60290011ModulePdhEthSubtypeSet(ThaModulePdh self, uint8 subtype);
eAtRet Tha60290011ModulePdhEthLengthSet(ThaModulePdh self, uint16 length);
eAtRet Tha60290011ModulePdhEthTypeSet(ThaModulePdh self, uint16 type);
eAtRet Tha60290011ModulePdhSourceMacAddressSet(ThaModulePdh self, uint8 *address);
eAtRet Tha60290011ModulePdhDestMacAddressSet(ThaModulePdh self, uint8 *address);
eAtRet Tha60290011ModulePdhSourceMacAddressGet(ThaModulePdh self, uint8 *address);
eAtRet Tha60290011ModulePdhDestMacAddressGet(ThaModulePdh self, uint8 *address);
eAtRet Tha60290011ModulePdhMuxActive(ThaModulePdh self);
eAtRet Tha60290011ModulePdhMuxChannelRegsDebugShow(ThaModulePdh self, AtPdhChannel pdhChannel);
eAtRet Tha60290011ModulePdhMuxAxi4StreamDebugShow(ThaModulePdh self);
AtPdhDe1 Tha60290011PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
eAtRet Tha60290011ModulePdhMuxTxEthEnable(AtModulePdh self, eBool enabled);
eBool Tha60290011ModulePdhMuxTxEthIsEnabled(AtModulePdh self);
AtPdhDe1 Tha60290011ModulePdhHwIdToLiuDe1Object(AtModulePdh self, uint8 sts, uint8 vtgId, uint8 vtId);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPDH_H_ */

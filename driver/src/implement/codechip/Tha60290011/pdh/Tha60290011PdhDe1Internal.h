/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhDe1Internal.h
 *
 * Created Date: Oct 4, 2016 
 *
 * Description : Internal Interface of Pdh De1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHDE1INTERNAL_H_
#define _THA60290011PDHDE1INTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhVcDe1Internal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011PdhDe1
    {
    tTha60210031PdhVcDe1 super;

    /* Private */
    eBool isInInitProgress;
    }tTha60290011PdhDe1;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60290011PdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011PDHDE1INTERNAL_H_ */

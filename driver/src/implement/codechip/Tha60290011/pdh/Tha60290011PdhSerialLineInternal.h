/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : 
 *
 * File        : Tha60290011PdhSerialLineInternal.h
 *
 * Created Date: Nov 25, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHSERIALLINEINTERNAL_H_
#define _THA60290011PDHSERIALLINEINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhSerialLineInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011PdhSerialLine
    {
    tTha60210031PdhSerialLine super;
    }tTha60290011PdhSerialLine;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhSerialLine Tha60290011PdhSerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011PDHSERIALLINEINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhVcDe1Internal.h
 *
 * Created Date: Aug 2, 2017
 *
 * Description : Internal Interface of Pdh VcDe1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHVCDE1INTERNAL_H_
#define _THA60290011PDHVCDE1INTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhVcDe1Internal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011PdhVcDe1
    {
    tTha60210031PdhVcDe1 super;
    }tTha60290011PdhVcDe1;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60290011PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011PDHVCDE1INTERNAL_H_ */

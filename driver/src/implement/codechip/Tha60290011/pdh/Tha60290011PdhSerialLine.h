/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : 
 *
 * File        : Tha60290011PdhSerialLine.h
 *
 * Created Date: Oct 31, 2016 
 *
 * Description : TODO Note.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHSERIALLINE_H_
#define _THA60290011PDHSERIALLINE_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhSerialLine Tha60290011PdhSerialLineNew(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011PDHSERIALLINE_H_ */

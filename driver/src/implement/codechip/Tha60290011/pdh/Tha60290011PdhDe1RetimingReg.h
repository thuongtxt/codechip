/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_ReTiming_H_
#define _AF6_REG_AF6CNC0011_RD_ReTiming_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : SSM Engine Status 1
Reg Addr   : 0x00030200
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used to enable SSM engine

------------------------------------------------------------------------------*/
#define cAf6Reg_ssmsta1_pen                                                                          0x00030200

/*--------------------------------------
BitField Name: SSMEngSta
BitField Type: RW
BitField Desc: SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM
Engine Busy
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ssmsta1_pen_SSMEngSta_Mask                                                                cBit31_0
#define cAf6_ssmsta1_pen_SSMEngSta_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : SSM Engine Status 2
Reg Addr   : 0x00030201
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used to enable SSM engine

------------------------------------------------------------------------------*/
#define cAf6Reg_ssmsta2_pen                                                                          0x00030201

/*--------------------------------------
BitField Name: SSMEngSta
BitField Type: RW
BitField Desc: SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM
Engine Busy
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_ssmsta2_pen_SSMEngSta_Mask                                                                cBit31_0
#define cAf6_ssmsta2_pen_SSMEngSta_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : SSM Engine Status 3
Reg Addr   : 0x00030202
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used to enable SSM engine

------------------------------------------------------------------------------*/
#define cAf6Reg_ssmsta3_pen                                                                          0x00030202

/*--------------------------------------
BitField Name: SSMEngSta
BitField Type: RW
BitField Desc: SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM
Engine Busy
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_ssmsta3_pen_SSMEngSta_Mask                                                                cBit19_0
#define cAf6_ssmsta3_pen_SSMEngSta_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Tx SSM Framer Control
Reg Addr   : 0x00030000 - 0x00030053
Reg Formula: 0x00030000 + LiuID
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
This is used to configure the SSM transmitter

------------------------------------------------------------------------------*/
#define cAf6Reg_txctrl00_pen_Base                                                                   0x00030000

/*--------------------------------------
BitField Name: E1ErrIns
BitField Type: RW
BitField Desc: E1 Error Insert Enable
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_txctrl00_pen_E1ErrIns_Mask                                                                 cBit31
#define cAf6_txctrl00_pen_E1ErrIns_Shift                                                                    31

/*--------------------------------------
BitField Name: SiValCfg
BitField Type: RW
BitField Desc: Si Value default
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_txctrl00_pen_SiValCfg_Mask                                                                 cBit30
#define cAf6_txctrl00_pen_SiValCfg_Shift                                                                    30

/*--------------------------------------
BitField Name: SaValCfg
BitField Type: RW
BitField Desc: Sa4-Sa8 Value default
BitField Bits: [29:25]
--------------------------------------*/
#define cAf6_txctrl00_pen_SaValCfg_Mask                                                              cBit29_25
#define cAf6_txctrl00_pen_SaValCfg_Shift                                                                    25

/*--------------------------------------
BitField Name: SSMEna
BitField Type: RW
BitField Desc: SSM enable 1: Enable SSM 0: Disable SSM
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_txctrl00_pen_SSMEna_Mask                                                                   cBit24
#define cAf6_txctrl00_pen_SSMEna_Shift                                                                      24

/*--------------------------------------
BitField Name: SSMCount
BitField Type: RW
BitField Desc: SSM Message Counter. These bits are used to store the amount of
repetitions the Transmit SSM message will be sent - 0 : the transmit BOC will be
set continuously - other: the amount of repetitions the Transmit SSM message
will be sent before an all ones
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_txctrl00_pen_SSMCount_Mask                                                              cBit23_16
#define cAf6_txctrl00_pen_SSMCount_Shift                                                                    16

/*--------------------------------------
BitField Name: SSMMess
BitField Type: RW
BitField Desc: SSM Message. These bits are used to store the SSM message to be
transmitted out the National bits or Si International bit or FDL - E1: [13:10]
for SSM Message - T1: [15:10] for BOC code
BitField Bits: [15:10]
--------------------------------------*/
#define cAf6_txctrl00_pen_SSMMess_Mask                                                               cBit15_10
#define cAf6_txctrl00_pen_SSMMess_Shift                                                                     10

/*--------------------------------------
BitField Name: SSMSasel
BitField Type: RW
BitField Desc: Only one of the five Sa bits can be chosen for SSM transmission
at a time. Don't care in T1 case - Bit9: for Sa8 - Bit8: for Sa7 - Bit7: for Sa6
- Bit6: for Sa5 - Bit5: for Sa4
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_txctrl00_pen_SSMSasel_Mask                                                                cBit9_5
#define cAf6_txctrl00_pen_SSMSasel_Shift                                                                     5

/*--------------------------------------
BitField Name: SSMSiSaSel
BitField Type: RW
BitField Desc: Select Si or Sa to transmit SSM. Don't care in T1 case - 0: Sa
National Bits (Only one of the five Sa bits can be chosen for SSM transmission
at a time) - 1: Si International Bits
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_txctrl00_pen_SSMSiSaSel_Mask                                                                cBit4
#define cAf6_txctrl00_pen_SSMSiSaSel_Shift                                                                   4

/*--------------------------------------
BitField Name: TxDE1Md
BitField Type: RW
BitField Desc: Transmit DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX
Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For
bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other:
Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_txctrl00_pen_TxDE1Md_Mask                                                                 cBit3_0
#define cAf6_txctrl00_pen_TxDE1Md_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Tx SSM Framer Control
Reg Addr   : 0x00030600 - 0x00030653
Reg Formula: 0x00030600 + LiuID
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
This is used to configure the SSM transmitter

------------------------------------------------------------------------------*/
#define cAf6Reg_txctrl01_pen_Base                                                                   0x00030600

/*--------------------------------------
BitField Name: RetEna
BitField Type: RW
BitField Desc: Retiming enalbe 1: enable 0: disalbe
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_txctrl01_pen_RetEna_Mask                                                                    cBit9
#define cAf6_txctrl01_pen_RetEna_Shift                                                                       9

/*--------------------------------------
BitField Name: SSM Priority
BitField Type: RW
BitField Desc: Set SSM priority 1: RAI highest priority 0: SSM highest priority
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_txctrl01_pen_SSM_Priority_Mask                                                              cBit8
#define cAf6_txctrl01_pen_SSM_Priority_Shift                                                                 8

/*--------------------------------------
BitField Name: AISneadis
BitField Type: RW
BitField Desc: AIS near-end disable 1: Disable forwading AIS 0: Enable forwading
AIS
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_txctrl01_pen_AISneadis_Mask                                                                 cBit7
#define cAf6_txctrl01_pen_AISneadis_Shift                                                                    7

/*--------------------------------------
BitField Name: Raifardis
BitField Type: RW
BitField Desc: RAI far-end disable 1: Disable forwading RAI 0: Enable forwading
RAI
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_txctrl01_pen_Raifardis_Mask                                                                 cBit6
#define cAf6_txctrl01_pen_Raifardis_Shift                                                                    6

/*--------------------------------------
BitField Name: Raineadis
BitField Type: RW
BitField Desc: RAI near-end disable 1: Disable forwading RAI 0: Enable forwading
RAI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_txctrl01_pen_Raineadis_Mask                                                                 cBit5
#define cAf6_txctrl01_pen_Raineadis_Shift                                                                    5

/*--------------------------------------
BitField Name: SiBypass
BitField Type: RW
BitField Desc: Si CRC4 Bypass Enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_txctrl01_pen_SiBypass_Mask                                                                  cBit4
#define cAf6_txctrl01_pen_SiBypass_Shift                                                                     4

/*--------------------------------------
BitField Name: SaBypass
BitField Type: RW
BitField Desc: Sa4-Sa8 Bypass Enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_txctrl01_pen_SaBypass_Mask                                                                  cBit3
#define cAf6_txctrl01_pen_SaBypass_Shift                                                                     3

/*--------------------------------------
BitField Name: E2Bypass
BitField Type: RW
BitField Desc: E2 of CRC4 Bypass Enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_txctrl01_pen_E2Bypass_Mask                                                                  cBit2
#define cAf6_txctrl01_pen_E2Bypass_Shift                                                                     2

/*--------------------------------------
BitField Name: E1Bypass
BitField Type: RW
BitField Desc: E1 of CRC4 Bypass Enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_txctrl01_pen_E1Bypass_Mask                                                                  cBit1
#define cAf6_txctrl01_pen_E1Bypass_Shift                                                                     1

/*--------------------------------------
BitField Name: E2ErrIns
BitField Type: RW
BitField Desc: E2 Error Insert Enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_txctrl01_pen_E2ErrIns_Mask                                                                  cBit0
#define cAf6_txctrl01_pen_E2ErrIns_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Rx SSM Framer Control
Reg Addr   : 0x0003C000 - 0x0003C053
Reg Formula: 0x0003C000 + LiuID
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
This is used to configure the SSM transmitter

------------------------------------------------------------------------------*/
#define cAf6Reg_rxctrl_pen_Base                                                                     0x0003C000

/*--------------------------------------
BitField Name: RxDE1CRCcheck
BitField Type: RW
BitField Desc: Set 1 to check CRC6
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1CRCcheck_Mask                                                              cBit10
#define cAf6_rxctrl_pen_RxDE1CRCcheck_Shift                                                                 10

/*--------------------------------------
BitField Name: RxDE1LofThres
BitField Type: RW
BitField Desc: Threshold for declare LOF
BitField Bits: [09:06]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1LofThres_Mask                                                             cBit9_6
#define cAf6_rxctrl_pen_RxDE1LofThres_Shift                                                                  6

/*--------------------------------------
BitField Name: RxDE1FLofmod
BitField Type: RW
BitField Desc: Select mode Slide Window or Continous to declare LOF - 1: Slide
Window detection (window 8, threshold RxDE1LofThres) - 0: Continous detection
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1FLofmod_Mask                                                                cBit5
#define cAf6_rxctrl_pen_RxDE1FLofmod_Shift                                                                   5

/*--------------------------------------
BitField Name: RxDE1FrcLof
BitField Type: RW
BitField Desc: Set 1 to force Re-frame (default 0)
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1FrcLof_Mask                                                                 cBit4
#define cAf6_rxctrl_pen_RxDE1FrcLof_Shift                                                                    4

/*--------------------------------------
BitField Name: RxDE1Md
BitField Type: RW
BitField Desc: Receive DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX
Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For
bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other:
Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1Md_Mask                                                                   cBit3_0
#define cAf6_rxctrl_pen_RxDE1Md_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Data Sticky
Reg Addr   : 0x00034001
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used for line 31-0

------------------------------------------------------------------------------*/
#define cAf6Reg_stk0_upen_Base                                                                      0x00034001

/*--------------------------------------
BitField Name: SSMSlipDat
BitField Type: RW
BitField Desc: SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data
Slip Enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk0_upen_SSMSlipDat_Mask                                                                cBit31_0
#define cAf6_stk0_upen_SSMSlipDat_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Frame Sticky
Reg Addr   : 0x00034002
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used for line 31-0

------------------------------------------------------------------------------*/
#define cAf6Reg_stk1_upen_Base                                                                      0x00034002

/*--------------------------------------
BitField Name: SSMSlipFrm
BitField Type: RW
BitField Desc: SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM
Frame Slip Enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk1_upen_SSMSlipFrm_Mask                                                                cBit31_0
#define cAf6_stk1_upen_SSMSlipFrm_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Data Sticky
Reg Addr   : 0x00034003
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used for 63-32

------------------------------------------------------------------------------*/
#define cAf6Reg_stk01_upen_Base                                                                      0x00034003

/*--------------------------------------
BitField Name: SSMSlipDat
BitField Type: RW
BitField Desc: SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data
Slip Enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk01_upen_SSMSlipDat_Mask                                                               cBit31_0
#define cAf6_stk01_upen_SSMSlipDat_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Frame Sticky
Reg Addr   : 0x00034004
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used for 63-32

------------------------------------------------------------------------------*/
#define cAf6Reg_stk11_upen_Base                                                                      0x00034004

/*--------------------------------------
BitField Name: SSMSlipFrm
BitField Type: RW
BitField Desc: SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM
Frame Slip Enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_stk11_upen_SSMSlipFrm_Mask                                                               cBit31_0
#define cAf6_stk11_upen_SSMSlipFrm_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Data Sticky
Reg Addr   : 0x00034005
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used for 83-64

------------------------------------------------------------------------------*/
#define cAf6Reg_stk02_upen_Base                                                                      0x00034005

/*--------------------------------------
BitField Name: SSMSlipDat
BitField Type: RW
BitField Desc: SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data
Slip Enable
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_stk02_upen_SSMSlipDat_Mask                                                               cBit19_0
#define cAf6_stk02_upen_SSMSlipDat_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Frame Sticky
Reg Addr   : 0x00034006
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used 83-64

------------------------------------------------------------------------------*/
#define cAf6Reg_stk12_upen_Base                                                                      0x00034006

/*--------------------------------------
BitField Name: SSMSlipFrm
BitField Type: RW
BitField Desc: SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM
Frame Slip Enable
BitField Bits: [15:19]
--------------------------------------*/
#define cAf6_stk12_upen_SSMSlipFrm_Mask                                                              cBit15_19
#define cAf6_stk12_upen_SSMSlipFrm_Shift                                                                    19


/*------------------------------------------------------------------------------
Reg Name   : SSM Slip Buffer Frame Sticky
Reg Addr   : 0x00034100-0x00034153
Reg Formula: 0x00034100 + 128*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $LiuID(0 - 15):
Reg Desc   : 
This is the per channel CRC error counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_cnt0_pen_Base                                                                       0x00034100

/*--------------------------------------
BitField Name: SlipCnt
BitField Type: RW
BitField Desc: Slip Buffer Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_cnt0_pen_SlipCnt_Mask                                                                    cBit31_0
#define cAf6_cnt0_pen_SlipCnt_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Rx SSM Framer HW Status
Reg Addr   : 0x0003C400 - 0x0003C053
Reg Formula: 0x0003C400 + liuid
    Where  : 
           + $liuid(0-83):
Reg Desc   : 
These registers are used for Hardware status only

------------------------------------------------------------------------------*/
#define cAf6Reg_status_pen_Base                                                                     0x0003C400

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RO
BitField Desc: RX Framer Status - 5: In Frame - 0: LOF - Other: Searching State
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_status_pen_RxDE1Sta_Mask                                                                  cBit2_0
#define cAf6_status_pen_RxDE1Sta_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Rx SSM Framer CRC Error Counter
Reg Addr   : 0x0003E800 - 0x0003E8FF
Reg Formula: 0x0003E800 + 128*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $LiuID(0 - 15):
Reg Desc   : 
This is the per channel CRC error counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrm_crc_err_cnt_Base                                                              0x0003E800

/*--------------------------------------
BitField Name: DE1CrcErrCnt
BitField Type: RW
BitField Desc: CRC Error Counter
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_rxfrm_crc_err_cnt_DE1CrcErrCnt_Mask                                                       cBit7_0
#define cAf6_rxfrm_crc_err_cnt_DE1CrcErrCnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Rx SSM Framer REI Counter
Reg Addr   : 0x0003F000 - 0x0003F0FF
Reg Formula: 0x0003F000 + 128*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $liuid(0-83):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrm_rei_cnt_Base                                                                  0x0003F000

/*--------------------------------------
BitField Name: DE1REIErrCnt
BitField Type: RW
BitField Desc: REI Error Counter
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_rxfrm_rei_cnt_DE1REIErrCnt_Mask                                                           cBit7_0
#define cAf6_rxfrm_rei_cnt_DE1REIErrCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Rx SSM Framer FBE Counter
Reg Addr   : 0x0003E000 - 0x0003E0FF
Reg Formula: 0x0003E000 + 128*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $liuid(0-83):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrm_fbe_cnt_Base                                                                  0x0003E000

/*--------------------------------------
BitField Name: DE1FBEErrCnt
BitField Type: RW
BitField Desc: CRC Error Counter
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_rxfrm_fbe_cnt_DE1FBEErrCnt_Mask                                                           cBit7_0
#define cAf6_rxfrm_fbe_cnt_DE1FBEErrCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Re-Timing SSM signaling buffer
Reg Addr   : 0x00036000
Reg Formula: 0x00036000 + LiuID
    Where  : 
           + $LiuID (0-83)
Reg Desc   : 
These registers are used to

------------------------------------------------------------------------------*/
#define cAf6Reg_ssmsigbuf_pen_Base                                                                  0x00036000
#define cAf6Reg_ssmsigbuf_pen(LiuID)                                                      (0x00036000+(LiuID))

/*--------------------------------------
BitField Name: idle_mode
BitField Type: RW
BitField Desc: Force IDLE for signaling - 1: Enable, Send out ABCD by
patern_idle[3:0] field - 0: Disable, Send out ABCD by input source
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_idle_mode_Mask                                                               cBit12
#define cAf6_ssmsigbuf_pen_idle_mode_Shift                                                                  12

/*--------------------------------------
BitField Name: oos_replace_en
BitField Type: RW
BitField Desc: OOS Replace enable in case of DE1 LOF or E1-TS16 signaling LOMF -
1: Enable, Send out ABCD by patern_oos[3:0] field - 0: Disable, Send out ABCD by
input source
BitField Bits: [11]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_oos_replace_en_Mask                                                          cBit11
#define cAf6_ssmsigbuf_pen_oos_replace_en_Shift                                                             11

/*--------------------------------------
BitField Name: patern_oos
BitField Type: RO
BitField Desc: Pattern Signaling oos
BitField Bits: [10:7]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_patern_oos_Mask                                                            cBit10_7
#define cAf6_ssmsigbuf_pen_patern_oos_Shift                                                                  7

/*--------------------------------------
BitField Name: patern_idle
BitField Type: RO
BitField Desc: Pattern Signaling IDLE
BitField Bits: [06:3]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_patern_idle_Mask                                                            cBit6_3
#define cAf6_ssmsigbuf_pen_patern_idle_Shift                                                                 3

/*--------------------------------------
BitField Name: sigins_enb
BitField Type: RW
BitField Desc: Signaling insert enable to Tx - 1: Enable, - 0: Disable, forward
all data from input
BitField Bits: [02]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_sigins_enb_Mask                                                               cBit2
#define cAf6_ssmsigbuf_pen_sigins_enb_Shift                                                                  2

/*--------------------------------------
BitField Name: sig_y_forward_dis
BitField Type: RW
BitField Desc: Signaling bit y forward disable - 1: Disbale forward, using
sigy_up bit field to send out for y bit - 0: Enable forward, using input y-data
to send out for y bit
BitField Bits: [01]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_sig_y_forward_dis_Mask                                                        cBit1
#define cAf6_ssmsigbuf_pen_sig_y_forward_dis_Shift                                                           1

/*--------------------------------------
BitField Name: sigy_up
BitField Type: RO
BitField Desc: Signaling bit y value from cpu configure. This value is used to
send out for y bit if idle_mode = 1      or sigy_forward_dis = 1
BitField Bits: [00]
--------------------------------------*/
#define cAf6_ssmsigbuf_pen_sigy_up_Mask                                                                  cBit0
#define cAf6_ssmsigbuf_pen_sigy_up_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Re-Timing Signaling loss of multiframe
Reg Addr   : 0x00034024-0x00034026
Reg Formula: 0x00034024 + ID
    Where  : 
           + $ID (0-2)
Reg Desc   : 
This is the sticky E1 TS16 Signaling loss of multiframe (LOMF),  each bit per channel for E1 Rx Framer,
ID = 0 for DE1#0-31, ID = 1 for DE1#32-63, ID = 2 for DE1#64-83,

------------------------------------------------------------------------------*/
#define cAf6Reg_e1siglomf_stkpen_Base                                                               0x00034024

/*--------------------------------------
BitField Name: e1siglomf_sticky
BitField Type: RW
BitField Desc: Alarm sticky signaling lomf
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_e1siglomf_stkpen_e1siglomf_sticky_Mask                                                   cBit31_0
#define cAf6_e1siglomf_stkpen_e1siglomf_sticky_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Re-Timing loss of frame
Reg Addr   : 0x00034014-0x00034016
Reg Formula: 0x00034014 + ID
    Where  : 
           + $ID (0-2)
Reg Desc   : 
This is the sticky DE1 loss of frame, each bit per channel for Rx Framer
ID = 0 for DE1#0-31, ID = 1 for DE1#32-63, ID = 2 for DE1#64-83,

------------------------------------------------------------------------------*/
#define cAf6Reg_de1lof_stkpen_Base                                                                  0x00034014

/*--------------------------------------
BitField Name: de1lof_sticky
BitField Type: RW
BitField Desc: Alarm sticky loss of frame
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_de1lof_stkpen_de1lof_sticky_Mask                                                         cBit31_0
#define cAf6_de1lof_stkpen_de1lof_sticky_Shift                                                               0

#endif /* _AF6_REG_AF6CNC0011_RD_ReTiming_H_ */

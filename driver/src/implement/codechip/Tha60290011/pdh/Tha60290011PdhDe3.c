/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011PdhDe3.c
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : Implementation for DE3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../pmc/Tha60290011ModulePmc.h"
#include "Tha60290011PdhDe3Internal.h"
#include "Tha60290011ModulePdhMuxReg.h"
#include "Tha60290011ModulePdh.h"
#include "AtPdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods         m_AtChannelOverride;
static tAtPdhChannelMethods      m_AtPdhChannelOverride;
static tTha60210031PdhDe3Methods m_Tha60210031PdhDe3Override;

/* Save Super Implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh ModulePdh(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    uint32 de3Id = AtChannelIdGet((AtChannel)self);
    eAtRet ret = cAtOk;
    ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    if (ret != cAtOk)
        return ret;

    Tha60290011ModulePdhLineMap(ModulePdh((AtChannel)self), mPdhDe3HwId(de3Id), mDe3ToLiuMuxId(de3Id), cAtTrue);

    return cAtOk;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret;
    uint32 de3Id = AtChannelIdGet(self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);
    AtPdhSerialLine serLine = AtModulePdhDe3SerialLineGet((AtModulePdh)module, de3Id);

    ret = m_AtChannelMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (AtPdhSerialLineModeGet(serLine) == cAtPdhDe3SerialLineModeEc1)
        return cAtOk;

    /* PDH to LIU */
    Tha60290011ModulePdhTxHwLineEnable(ModulePdh(self), mPdhDe3HwId(de3Id), enable);

    /* LIU to PDH */
    Tha60290011ModulePdhRxHwLineEnable(ModulePdh(self), mDe3ToLiuMuxId(de3Id), enable);

    return cAtOk;
    }

static eBool TxSlipBufferIsEnabled(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe3CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtFalse);

	return m_AtChannelMethods->CounterGet(self, counterType);
	}

static uint32 CounterClear(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe3CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtTrue);

	return m_AtChannelMethods->CounterClear(self, counterType);
	}

static eBool TxFramerSsmIsSupported(Tha60210031PdhDe3 self)
    {
    if (!ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        return cAtFalse;

    /* TODO: need to check with HW if this feature is here and open it with
     * specific version  */
    return cAtFalse;
    }

static void DebugFromPmc(AtChannel self)
    {
    AtPrintc(cSevNormal, "PDH MUX DE3EC1 Bit counter : %u\r\n", Tha60290011ModulePmcPdhDe3BitGet((ThaModulePmc)PmcModule(self), AtChannelIdGet(self), cAtTrue));
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    if (ThaTdmChannelShouldGetCounterFromPmc(self))
        DebugFromPmc(self);

    return cAtOk;
    }

static void OverrideTha60210031PdhDe3(AtPdhDe3 self)
    {
    Tha60210031PdhDe3 de3 = (Tha60210031PdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031PdhDe3Override, mMethodsGet(de3), sizeof(m_Tha60210031PdhDe3Override));

        mMethodOverride(m_Tha60210031PdhDe3Override, TxFramerSsmIsSupported);
        }

    mMethodsSet(de3, &m_Tha60210031PdhDe3Override);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, CounterGet);
		mMethodOverride(m_AtChannelOverride, CounterClear);
		mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(tAtPdhChannelMethods));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, TxSlipBufferIsEnabled);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideTha60210031PdhDe3(self);
    }

AtPdhDe3 Tha60290011PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60290011PdhDe3));

    /* Supper constructor */
    if (Tha60210031PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60290011PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60290011PdhDe3));
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return Tha60290011PdhDe3ObjectInit(newDe3, channelId, module);
    }

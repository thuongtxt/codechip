/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290011HdclChannelV2.c
 *
 * Created Date: Mar 4, 2018
 *
 * Description : Implement HDLC v2 due to Tx HDLC FCS MSB feature
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60290011HdlcChannelInternal.h"
#include "Tha60290011HdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg3 0x30006
#define cAf6Reg_upen_hdlc_enc_ctrl_reg4 0x30007

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;

/* Save super implementation */
static const tAtHdlcChannelMethods *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtChannel self)
    {
    return AtModuleDeviceGet((AtModule)AtChannelModuleGet(self));
    }

static ThaModuleOcn OcnModule(AtChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 GlobalRegisterWithLocalAddress(AtHdlcChannel self, uint32 localAddress)
    {
    return BaseAddress((AtChannel)self) + localAddress;
    }

static uint32 LocalId(AtHdlcChannel self)
    {
    return AtChannelIdGet((AtChannel)self) % 32;
    }

static uint32 GroupId(AtHdlcChannel self)
    {
    return AtChannelIdGet((AtChannel)self) / 32;
    }

static uint32 EnableMask(AtHdlcChannel self)
    {
    return cBit0 << LocalId(self);
    }

static uint32 EnableShift(AtHdlcChannel self)
    {
    return LocalId(self);
    }

static uint32 TxHdlcFcsCalculation_InputDataBitOrderRegAddress(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAf6Reg_upen_hdlc_enc_ctrl_reg3;
    }

static uint32 TxHdlcFcsTransmit_BitOrderRegAddress(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAf6Reg_upen_hdlc_enc_ctrl_reg4;
    }

static eAtRet TxHdlcFcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode fcsCalculationMode)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr =  GlobalRegisterWithLocalAddress(self, TxHdlcFcsCalculation_InputDataBitOrderRegAddress(self));
    eBool enable = (fcsCalculationMode == cAtHdlcFcsCalculationModeMsb) ? 1 : 0;

    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);
    mFieldIns(&longRegVal[GroupId(self)],
              EnableMask(self),
              EnableShift(self),
              mBoolToBin(enable));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);

    regAddr = GlobalRegisterWithLocalAddress(self, TxHdlcFcsTransmit_BitOrderRegAddress(self));
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);
    mFieldIns(&longRegVal[GroupId(self)],
              EnableMask(self),
              EnableShift(self),
              mBoolToBin(enable));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode fcsCalculationMode)
    {
    eAtRet ret = m_AtHdlcChannelMethods->FcsCalculationModeSet(self, fcsCalculationMode);

    if (ret != cAtOk)
        return ret;

    return TxHdlcFcsCalculationModeSet(self, fcsCalculationMode);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeSet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011HdlcChannelV2);
    }

AtHdlcChannel Tha60290011HdlcChannelV2ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011HdlcChannelObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290011HdlcChannelV2New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011HdlcChannelV2ObjectInit(newChannel, channelId, line, layers, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290011HdlcChannelInternal.h
 * 
 * Created Date: Mar 4, 2018
 *
 * Description : Internal data of HDCL channels
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011HDLCCHANNELINTERNAL_H_
#define _THA60290011HDLCCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/encap/Tha60290021HdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011HdlcChannel
    {
    tTha60290021HdlcChannel super;
    }tTha60290011HdlcChannel;

typedef struct tTha60290011HdlcChannelV2
    {
    tTha60290011HdlcChannel super;
    }tTha60290011HdlcChannelV2;

typedef struct tTha60290011HdlcChannelV3
    {
    tTha60290011HdlcChannelV2 super;
    }tTha60290011HdlcChannelV3;

typedef struct tTha60290011HdlcChannelV4
    {
    tTha60290011HdlcChannelV3 super;
    }tTha60290011HdlcChannelV4;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60290011HdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290011HdlcChannelV2ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290011HdlcChannelV3ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011HDLCCHANNELINTERNAL_H_ */


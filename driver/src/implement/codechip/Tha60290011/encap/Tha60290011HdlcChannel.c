/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290011HdlcChannel.c
 *
 * Created Date: Oct 13, 2016
 *
 * Description : DCC HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290011DccKbyteV2Reg.h"
#include "../pw/Tha60290011ModulePw.h"
#include "Tha60290011HdlcChannelInternal.h"
#include "Tha60290011HdlcChannel.h"


/*--------------------------- Define -----------------------------------------*/
#define mThis(self)         ((tTha60290011HdlcChannel*)self)
#define cAf6Reg_upen_dcc_cnt_V2_Base 0x13000
#define cAf6Reg_upen_dcc_cfg_testgen_enacid_Base  0x0C000
#define cAf6Reg_upen_dcctx_fcsrem_Base 0x10003

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods              m_AtChannelOverride;
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;
static tTha60290021HdlcChannelMethods m_Tha60290021HdlcChannelOverride;

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods     = NULL;
static const tAtHdlcChannelMethods *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtChannel self)
    {
    return AtModuleDeviceGet((AtModule)AtChannelModuleGet(self));
    }

static ThaModuleOcn OcnModule(AtChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 RegisterWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    return BaseAddress(self) + localAddress;
    }

static uint32 LocalId(AtChannel self)
    {
    return AtChannelIdGet(self) % 32;
    }

static uint32 GroupId(AtChannel self)
    {
    return AtChannelIdGet(self) / 32;
    }

static uint32 EnableMask(AtChannel self)
    {
    return cBit0 << LocalId(self);
    }

static uint32 EnableShift(AtChannel self)
    {
    return LocalId(self);
    }

static uint32 RxDccEnableRegister(AtChannel self)
    {
    return RegisterWithLocalAddress(self,  cAf6Reg_upen_dcctx_enacid);
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = RxDccEnableRegister(self);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);
    mFieldIns(&longRegVal[GroupId(self)],
              EnableMask(self),
              EnableShift(self),
              mBoolToBin(enable));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);
    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = RxDccEnableRegister(self);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEncap);
    return (longRegVal[GroupId(self)] & EnableMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet TxDccHwEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    uint32 regAddr = Tha60290021HdlcChannelUpenDccdecAddress(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_dccdec_Channel_enable_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint16 HwLongReadOnModule(AtChannel self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, eAtModule module)
    {
    AtUnused(module);
    return ThaModuleOcnSohOverEthLongReadOnCore(OcnModule(self), localAddress, dataBuffer, bufferLen, IpCore(self));
    }

static uint16 HwLongWriteOnModule(AtChannel self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, eAtModule module)
    {
    AtUnused(module);
    return ThaModuleOcnSohOverEthLongWriteOnCore(OcnModule(self), localAddress, dataBuffer, bufferLen, IpCore(self));
    }

static eBool HasTohBusEnabling(Tha60290021HdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 CounterBaseAddressByHwType(Tha60290021HdlcChannel self, uint32 hwType)
    {
    AtUnused(self);
    return cAf6Reg_upen_dcc_cnt_V2_Base + (hwType * 64);
    }

static uint32 PktGeneratorChannelEnableMask(AtChannel self)
    {
    return EnableMask(self);
    }

static uint32 PktGeneratorChannelEnableShift(AtChannel self)
    {
    return EnableShift(self);
    }

static uint32 UpenDccCfgTestgenEnacid(ThaModuleOcn self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(self) + cAf6Reg_upen_dcc_cfg_testgen_enacid_Base;
    }

static eAtRet PktGeneratorChannelEnable(ThaModuleOcn self, AtChannel hdlc, eBool enable)
    {
    uint32 regAddr = UpenDccCfgTestgenEnacid(self);
    uint32 regVal[cThaLongRegMaxSize];
    uint32 regIndex = GroupId(hdlc);
    mChannelHwLongRead(hdlc, regAddr, regVal, cThaLongRegMaxSize, cThaModuleOcn);
    mFieldIns(&regVal[regIndex], PktGeneratorChannelEnableMask(hdlc), PktGeneratorChannelEnableShift(hdlc), mBoolToBin(enable));
    mChannelHwLongWrite(hdlc, regAddr, regVal, cThaLongRegMaxSize, cThaModuleOcn);
    return cAtOk;
    }

static eBool PktGeneratorChannelIsEnabled(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = UpenDccCfgTestgenEnacid(self);
    uint32 regVal[cThaLongRegMaxSize];
    uint32 regIndex = GroupId(hdlc);
    mChannelHwLongRead(hdlc, regAddr, regVal, cThaLongRegMaxSize, cThaModuleOcn);
    return (regVal[regIndex] & PktGeneratorChannelEnableMask(hdlc)) ? cAtTrue : cAtFalse;
    }

static eAtRet PktGeneratorEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    return PktGeneratorChannelEnable(OcnModule((AtChannel)self), (AtChannel)self, enable);
    }

static eBool  PktGeneratorIsEnabled(Tha60290021HdlcChannel self)
    {
    return PktGeneratorChannelIsEnabled(OcnModule((AtChannel)self), (AtChannel)self);
    }

static eAtRet DecapFcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 regAddr, regIndex;
    uint32 hwFcsMode_Mask = EnableMask((AtChannel)self);
    uint32 hwFcsMode_Shift = EnableShift((AtChannel)self);
    uint32 longRegVal[cThaLongRegMaxSize];

    regAddr = RegisterWithLocalAddress((AtChannel)self, cAf6Reg_upen_dcctx_fcsrem_Base);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleSdh);

    regIndex = GroupId((AtChannel)self);
    mRegFieldSet(longRegVal[regIndex], hwFcsMode_, Tha60290011HdlcChannelFcsSw2Hw(fcsMode));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    eAtRet ret = cAtOk;

    ret |= Tha60290021HdlcChannelFcsModePrivateSet(self, fcsMode);
    if (ret == cAtOk)
        ret |= DecapFcsModeSet(self, fcsMode);

    return ret;
    }

static eBool HasInterruptV2(Tha60290021HdlcChannel self)
    {
    Tha60290011ModulePw pwModule = (Tha60290011ModulePw)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePw);
    return Tha60290011ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, HwLongReadOnModule);
        mMethodOverride(m_AtChannelOverride, HwLongWriteOnModule);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideTha60290021HdlcChannel(AtHdlcChannel self)
    {
    Tha60290021HdlcChannel dcc = (Tha60290021HdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021HdlcChannelOverride, mMethodsGet(dcc), sizeof(tTha60290021HdlcChannelMethods));

        mMethodOverride(m_Tha60290021HdlcChannelOverride, HasTohBusEnabling);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, TxDccHwEnable);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, CounterBaseAddressByHwType);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, PktGeneratorEnable);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, PktGeneratorIsEnabled);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, HasInterruptV2);
        }

    mMethodsSet(dcc, &m_Tha60290021HdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideTha60290021HdlcChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011HdlcChannel);
    }

AtHdlcChannel Tha60290011HdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HdlcChannelV2ObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290011HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011HdlcChannelObjectInit(newChannel, channelId, line, layers, module);
    }

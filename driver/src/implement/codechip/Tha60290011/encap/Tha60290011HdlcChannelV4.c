/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290011HdlcChannelV4.c
 *
 * Created Date: Jun 28, 2018
 *
 * Description : DCC HDLC channelV4 (TX DCC RS and MS enable/disable) implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60290011HdlcChannelInternal.h"
#include "Tha60290011HdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021HdlcChannelMethods m_Tha60290021HdlcChannelOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasTohBusEnabling(Tha60290021HdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxHwOcnDccInsertionEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    return Tha60290021HdlcChannelTxRsMsDccInsHandle(self, enable);
    }

static void OverrideTha60290021HdlcChannel(AtHdlcChannel self)
    {
    Tha60290021HdlcChannel dcc = (Tha60290021HdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021HdlcChannelOverride, mMethodsGet(dcc), sizeof(tTha60290021HdlcChannelMethods));

        mMethodOverride(m_Tha60290021HdlcChannelOverride, HasTohBusEnabling);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, TxHwOcnDccInsertionEnable);
        }

    mMethodsSet(dcc, &m_Tha60290021HdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideTha60290021HdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011HdlcChannelV4);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011HdlcChannelV3ObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290011HdlcChannelV4New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, line, layers, module);
    }

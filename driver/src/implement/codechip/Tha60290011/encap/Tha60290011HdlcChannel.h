/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60290011HdlcChannel.h
 * 
 * Created Date: Oct 13, 2016
 *
 * Description : HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011HDLCCHANNEL_H_
#define _THA60290011HDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60290011HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290011HdlcChannelV2New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290011HdlcChannelV3New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290011HdlcChannelV4New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011HDLCCHANNEL_H_ */


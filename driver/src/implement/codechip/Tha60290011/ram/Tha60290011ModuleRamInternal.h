/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290011ModuleRamInternal.h
 * 
 * Created Date: Oct 15, 2018
 *
 * Description : 60290011 Module RAM internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULERAMINTERNAL_H_
#define _THA60290011MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/ram/Tha60210031ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleRam
    {
    tTha60210031ModuleRam super;
    }tTha60290011ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60290011ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULERAMINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290011ModuleRam.c
 *
 * Created Date: Sep 5, 2016
 *
 * Description : Module RAM for 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/ram/Tha6029Ddr.h"
#include "../../Tha60210031/ram/Tha60210031InternalRam.h"
#include "../../Tha60210031/ram/diag/Tha60210031RamErrorGenerator.h"
#include "Tha60290011ModuleRam.h"
#include "Tha60290011ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/* Super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return (ddrId == 2) ? 16 : 32;
    }

static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
    return Tha6029Ddr32BitNew(self, core, ddrId);
    }

static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void StickyDebug(ThaModuleRam self)
    {
    AtUnused(self);
    }

static eBool RamIsInSlice1(AtInternalRam ram)
    {
    const char *name = AtInternalRamName(ram);

    if (AtStrstr(name, "SliceId 1") != NULL)
        return cAtTrue;

    return cAtFalse;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (m_AtModuleMethods->InternalRamIsReserved(self, ram))
        return cAtTrue;

    if (RamIsInSlice1(ram))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule ramModule = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(ramModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(ramModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(ramModule, &m_AtModuleOverride);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, DdrDataBusSizeGet);
        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, InternalRamMonitoringIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, StickyDebug);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule(self);
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleRam);
    }

AtModuleRam Tha60290011ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60290011ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleRamObjectInit(newModule, device);
    }

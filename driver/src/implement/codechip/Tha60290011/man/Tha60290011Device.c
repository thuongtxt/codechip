/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60290011Device.c
 *
 * Created Date: Jul 2, 2016
 *
 * Description : 24 DS3/E3/EC1 or 84 DS1/E1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/physical/ThaThermalSensor.h"
#include "../../../default/physical/ThaPowerSupplySensor.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../../Tha60210031/man/Tha60210031Device.h"
#include "../../Tha60210011/ber/Tha60210011ModuleBer.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../Tha60290021/common/Tha6029CommonDevice.h"
#include "../../Tha60290021/physical/Tha6029Physical.h"
#include "../../Tha60290021/man/Tha60290021Device.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "../physical/Tha60290011SemController.h"
#include "../ber/Tha60290011ModuleBer.h"
#include "../cla/Tha60290011ModuleClaInternal.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "../ram/Tha60290011ModuleRam.h"
#include "../pmc/Tha60290011ModulePmc.h"
#include "../poh/Tha60290011PohProcessorInternal.h"
#include "Tha60290011DeviceInternal.h"
#include "Tha60290011DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cDebugCoreStatus                0x120
#define cDebugCoreStatusGe10lltxenb     cBit31
#define cDebugCoreStatusGe10lltxsop     cBit30
#define cDebugCoreStatusGe10lltxeop     cBit29
#define cDebugCoreStatusGe10lltxvld     cBit28
#define cDebugCoreStatusPwe_ena         cBit27
#define cDebugCoreStatusPwe_oeop        cBit26
#define cDebugCoreStatusPwe_osop        cBit25
#define cDebugCoreStatusPwe_ovld        cBit24
#define cDebugCoreStatusGe00lltxenb     cBit23
#define cDebugCoreStatusMap2pla1_dpwval cBit22
#define cDebugCoreStatusPla2pwe_opweop  cBit21
#define cDebugCoreStatusPla2pwe_opwvld  cBit20
#define cDebugCoreStatusPwepk_req       cBit19
#define cDebugCoreStatusPweca_req       cBit18
#define cDebugCoreStatusPlapk_req       cBit17
#define cDebugCoreStatusPlaca_req       cBit16
#define cDebugCoreStatusReor_req        cBit15
#define cDebugCoreStatusJb_req          cBit14
#define cDebugCoreStatusRdseg_req       cBit13
#define cDebugCoreStatusWrseg_req       cBit12
#define cDebugCoreStatusWrca_req        cBit11
#define cDebugCoreStatusRdca_req        cBit10
#define cDebugCoreStatusAvl3_rdy        cBit9
#define cDebugCoreStatusAvl3_done       cBit8
#define cDebugCoreStatusAvl2_rdy        cBit7
#define cDebugCoreStatusAvl2_done       cBit6
#define cDebugCoreStatusAvl1_rdy        cBit5
#define cDebugCoreStatusAvl1_done       cBit4
#define cDebugCoreStatusRxclslbit       cBit3
#define cDebugCoreStatusRxclseop        cBit2
#define cDebugCoreStatusRxclssop        cBit1
#define cDebugCoreStatusRxclsvld        cBit0

#define cDebugCoreSticky                0x110
#define cDebugCorePwe_owerr4_2Mask  cBit22_20
#define cDebugCorePwe_owerr4_2Shift  20
#define cDebugCorePwe_owerr1_0Mask  cBit19_18
#define cDebugCorePwe_owerr1_0Shift  18
#define cDebugCoreGe10llrxful_4     cBit17
#define cDebugCoreMap2pla_b3err cBit16
#define cDebugCoreCla2geena     cBit15
#define cDebugCoreRxeth_eop     cBit14
#define cDebugCoreRxeth_sop     cBit13
#define cDebugCoreRxeth_vld     cBit12
#define cDebugCoreRxclserr      cBit11
#define cDebugCoreRxclseop      cBit10
#define cDebugCoreRxclssop      cBit9
#define cDebugCoreRxclsvld      cBit8
#define cDebugCorePlawerr       cBit7
#define cDebugCorePwe_ovld      cBit6
#define cDebugCorePwe_osop      cBit5
#define cDebugCorePwe_oeop      cBit4
#define cDebugCoreGe10llrxfulMask  cBit3_0
#define cDebugCoreGe10llrxfulShift 0

#define cAf6Reg_mac_add_47_32_ctrl_Base                                                               0x000101
#define cReg_PohEnable_Mask cBit0
#define cReg_PohEnable_Shift 0

/*--------------------------- Macros -----------------------------------------*/
#define mNoneDebuggerPrint(color, string)                                     \
        if (!debugger)                                                        \
            AtPrintc(color, string);

#define mThis(self) ((Tha60290011Device)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
 static tTha60290011DeviceMethods m_methods;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210031DeviceMethods m_Tha60210031DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tThaDeviceMethods         *m_ThaDeviceMethods         = NULL;
static const tTha60210031DeviceMethods *m_Tha60210031DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldOpenFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static eBool IsPmcV3dot3Supported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x6, 0x0005);
    }

static eBool IsPmcV3dot2Supported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x6, 0x0000);
    }

static eBool IsPmcV3Supported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x3, 0x0008);
    }

static eBool IsPmcV3dot4FcsErrorCounterSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x8, 0x0009);
    }

static AtModule PmcModuleNew(AtDevice self)
    {
    if (IsPmcV3dot4FcsErrorCounterSupported(self))
        return Tha60290011ModulePmcV3dot4New(self);

    if (IsPmcV3dot3Supported(self))
        return Tha60290011ModulePmcV3dot3New(self);

    if (IsPmcV3dot2Supported(self))
        return Tha60290011ModulePmcV3dot2New(self);

    if (IsPmcV3Supported(self))
        return Tha60290011ModulePmcV3New(self);

    return Tha60290011ModulePmcNew(self);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModulePrbs)        return (AtModule)Tha60290011ModulePrbsNew(self);
    if (moduleId  == cAtModuleSdh)         return (AtModule)Tha60290011ModuleSdhNew(self);
    if (moduleId  == cAtModulePdh)         return (AtModule)Tha60290011ModulePdhNew(self);
    if (moduleId  == cAtModuleBer)         return (AtModule)Tha60290011ModuleBerNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)Tha60290011ModuleEthNew(self);
    if (moduleId  == cAtModuleRam)         return (AtModule)Tha60290011ModuleRamNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)Tha60290011ModuleClockNew(self);
    if (moduleId  == cAtModulePw)          return (AtModule)Tha60290011ModulePwNew(self);
    if (moduleId  == cAtModuleSur)         return (AtModule)Tha60290011ModuleSurNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60290011ModulePktAnalyzerNew(self);

    /* Private modules */
    if (phyModule == cThaModulePoh)   return Tha60290011ModulePohNew(self);
    if (phyModule == cThaModuleOcn)   return Tha60290011ModuleOcnNew(self);
    if (phyModule == cThaModuleMap)   return Tha60290011ModuleMapNew(self);
    if (phyModule == cThaModuleDemap) return Tha60290011ModuleDemapNew(self);
    if (phyModule == cThaModulePwe)   return Tha60290011ModulePweNew(self);
    if (phyModule == cThaModulePda)   return Tha60290011ModulePdaNew(self);
    if (phyModule == cThaModuleCdr)   return Tha60290011ModuleCdrNew(self);
    if (phyModule == cThaModuleCla)   return Tha60290011ModuleClaNew(self);
    if (phyModule == cThaModulePmc)   return PmcModuleNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha60290011SerdesManagerNew(self);
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60290011VersionReaderNew((AtDevice)self);
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    /* This must be supported from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasRole(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RoleSet(AtDevice self, eAtDeviceRole role)
    {
    AtModule pweModule = AtDeviceModuleGet(self, cThaModulePwe);
    return AtModuleRoleSet(pweModule, role);
    }

static eAtDeviceRole RoleGet(AtDevice self)
    {
    AtModule pweModule = AtDeviceModuleGet(self, cThaModulePwe);
    return AtModuleRoleGet(pweModule);
    }

static eAtDeviceRole AutoRoleGet(AtDevice self)
    {
    AtModule pweModule = AtDeviceModuleGet(self, cThaModulePwe);
    return AtModuleRoleInputStatus(pweModule);
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    return Tha60290011SemControllerNew(self, semId);
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0, 0x0005);
    return (currentVersion >= supportedVersion) ? cAtTrue : cAtFalse;
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    return AtDeviceSemControllerIsSupported(self) ? 1 : 0;
    }

static eBool ThermalSensorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtThermalSensor ThermalSensorObjectCreate(AtDevice self)
    {
    return Tha60290021ThermalSensorNew(self);
    }

static uint32 NumInterruptPins(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static AtInterruptPin InterruptPinObjectCreate(AtDevice self, uint32 pinId)
    {
    AtUnused(self);
    AtUnused(pinId);
    return NULL;
    }

static eAtTriggerMode InterruptPinDefaultTriggerMode(AtDevice self, AtInterruptPin pin)
    {
    return Tha6029DeviceInterruptPinDefaultTriggerMode(self, pin);
    }

static uint32 RegisterWithLocalAddress(AtDevice self, uint32 localAddress)
    {
    AtUnused(self);
    return localAddress + cTopBaseAddress;
    }

static eAtRet HwDiagnosticModeEnable(Tha60210031Device self, eBool enable)
    {
    AtDevice device = (AtDevice)self;
    AtIpCore core = AtDeviceIpCoreGet(device, 0);
    uint32 regAddr = RegisterWithLocalAddress(device, cAf6Reg_o_control3_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    uint32 hwEnabled = enable ? 1 : 0;

    mRegFieldSet(regVal, cAf6_o_control3_SGMII_SP_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_SGMII_DCC_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_SGMII_DIM_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_Basex2500_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_STBY_XFI_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_ACT_XFI_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_DDR3_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_DDR2_DiagEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_o_control3_DDR1_DiagEn_, hwEnabled);

    AtIpCoreWrite(core, regAddr, regVal);

    return cAtOk;
    }

static AtUart DiagnosticUartObjectCreate(AtDevice self)
    {
    return Tha6029DiagUartNew(self);
    }

static eAtRet Setup(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtDeviceMethods->Setup(self);
    ret |= AtDeviceDiagnosticCheckEnable(self, cAtTrue);

    return ret;
    }

static void ClockDebug(ThaDevice self)
    {
    if (ThaDeviceClockDebugIsSupported(self))
        Tha60150011DeviceClockStatusDisplay((AtDevice)self);
    }

static void EthStickyDebug(Tha60210031Device self)
    {
    /* Cannot display super information as not applicable on this product. Need
     * to consider what will be displayed if have */
    AtUnused(self);
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    AtUnused(self);
    return cAf6_i_sticky0_SystemPll_Mask;
    }

static uint32 DdrCurrentStatusRegister(Tha60210031Device self)
    {
    AtUnused(self);
    return cTopBaseAddress + cAf6Reg_status_top_Base;
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
    mVersionReset(self);
    }

static uint32 ClockMonitorRegister(AtDevice self, uint32 portId)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_clock_mon_high_Base + portId);
    }

static uint32 SlowClockMonitorRegister(AtDevice self, uint32 portId)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_clock_mon_slow_Base + portId);
    }

static void SgmiiClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* SERDES clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "basex2500_user_clk" : "  - basex2500_user_clk", ClockMonitorRegister(self, 0x08), 312500000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "dimgmii_user_clk  " : "  - dimgmii_user_clk", ClockMonitorRegister(self, 0x09), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "dccgmii_user_clk  " : "  - dccgmii_user_clk", ClockMonitorRegister(self, 0x0A), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "spgmii_user_clk   " : "  - spgmii_user_clk", ClockMonitorRegister(self, 0x0B), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "10g_act_rxclk     " : "  - 10g_act_rxclk", ClockMonitorRegister(self, 0x06), 156250000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "10g_act_txclk     " : "  - 10g_act_txclk", ClockMonitorRegister(self, 0x04), 156250000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "10g_stby_txclk    " : "  - 10g_stby_txclk", ClockMonitorRegister(self, 0x05), 156250000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "10g_stby_rxclk    " : "  - 10g_stby_rxclk", ClockMonitorRegister(self, 0x07), 156250000, 5);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void DdrClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* DDR clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR3#1 user clock" : "  - DDR3#1 user clock   ", ClockMonitorRegister(self, 0x01), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR3#2 user clock" : "  - DDR3#2 user clock   ", ClockMonitorRegister(self, 0x02), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR3#3 user clock" : "  - DDR3#3 user clock   ", ClockMonitorRegister(self, 0x03), 125000000, 5);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void RefClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Ref clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "ext_ref clock   " : "  - ext_ref clock       ", ClockMonitorRegister(self, 0x0c), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "prc_ref clock   " : "  - prc_ref clock       ", ClockMonitorRegister(self, 0x0d), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "System clock    " : "  - System clock        ", ClockMonitorRegister(self, 0x0e), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "pcie_upclk clock" : "  - pcie_upclk clock    ", ClockMonitorRegister(self, 0x00),  62500000, 5);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void SlowClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Ticks:\r\n");
    Tha6029DeviceTickClockStatusDisplay(self, SlowClockMonitorRegister(self, 0x1c), debugger ? "pm_tick" : "  - pm_tick");
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void FsmPcpPinFrequencyDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* FSM PCP Pins:\r\n");
    Tha6029DevicePinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0xa), debugger ? "fsm_pcp_0" : "  - fsm_pcp_0");
    Tha6029DevicePinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0xb), debugger ? "fsm_pcp_1" : "  - fsm_pcp_1");
    Tha6029DevicePinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0xc), debugger ? "fsm_pcp_2" : "  - fsm_pcp_2");
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void SpareGpioFrequencyDisplay(AtDevice self, uint8 maxPins)
    {
    uint8 i;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Spare GPIO Pins:\r\n");
    for (i = 0; i < maxPins; i++)
        {
        char buf[32];
        AtSprintf(buf, debugger ? "spare_gpio_%u" : "  - spare_gpio_%u", i);
        Tha6029DevicePinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, (uint32)(0x02 + i)), buf);
        }
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void SpareFrequencyDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Spare Pins:\r\n");
    Tha6029DevicePinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0x0), debugger ? "spare_clk0" : "  - spare_clk_0");
    Tha6029DevicePinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0x1), debugger ? "spare_clk1" : "  - spare_clk_1");
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void ClockStatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;

    SgmiiClockStatusDisplay(device);
    DdrClockStatusDisplay(device);
    RefClockStatusDisplay(device);
    SlowClockStatusDisplay(device);
    FsmPcpPinFrequencyDisplay(device);
    SpareGpioFrequencyDisplay(device, 8);
    SpareFrequencyDisplay(device);
    }

static uint32 StartVersionSwHandleQsgmiiLanes(Tha60210031Device self)
    {
    mVersionReset(self);
    }

static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPowerSupplySensor PowerSupplySensorObjectCreate(AtDevice self)
    {
    return Tha60290021PowerSupplySensorNew(self);
    }

static eBool PowerSupplySensorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DdrCalibCurrentStatusIsDone(Tha60210031Device self, uint32 regVal, uint32 mask)
    {
    /* IMPORANT: this is different with other projects, for DDR calib current
     * status, hardware is now interface:
     * - 0: calib done
     * - 1: calib fail
     */
    AtUnused(self);
    return (regVal & mask) ? cAtFalse : cAtTrue;
    }

static uint32 StickyAddress(void)
    {
    return cTopBaseAddress + cAf6Reg_i_sticky0_Base;
    }

static void PllStickyDebug(Tha60210031Device self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);
    AtHal hal = AtDeviceIpCoreHalGet((AtDevice)self, 0);

    regAddres = StickyAddress();
    regValue  = AtHalRead(hal, regAddres);
    Tha6021DebugPrintRegName(debugger, "* Device PLL sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "SystemPll", regValue, cAf6_i_sticky0_SystemPll_Mask);
    Tha6021DebugPrintStop(debugger);

    Tha6021DebugPrintRegName(debugger, "* Device DDR sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "Ddr33Urst", regValue, cAf6_i_sticky0_Ddr33Urst_Mask);
    Tha6021DebugPrintErrorBit(debugger, "Ddr32Urst", regValue, cAf6_i_sticky0_Ddr32Urst_Mask);
    Tha6021DebugPrintErrorBit(debugger, "Ddr31Urst", regValue, cAf6_i_sticky0_Ddr31Urst_Mask);
    Tha6021DebugPrintErrorBit(debugger, "Ddr33Calib", regValue, cAf6_i_sticky0_Ddr33Calib_Mask);
    Tha6021DebugPrintErrorBit(debugger, "Ddr32Calib", regValue, cAf6_i_sticky0_Ddr32Calib_Mask);
    Tha6021DebugPrintErrorBit(debugger, "Ddr31Calib", regValue, cAf6_i_sticky0_Ddr31Calib_Mask);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

static void PllStatusDebug(Tha60210031Device self)
    {
    /* Sticky debug is enough for now */
    AtUnused(self);
    }

static void CoreStickyDebug(Tha60210031Device self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);
    AtHal hal = AtDeviceIpCoreHalGet((AtDevice)self, 0);

    regAddres = cDebugCoreSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Core sticky", regAddres, regValue);
    Tha6021DebugPrintBitFieldVal(debugger, "pwe_owerr4_2", regValue, cDebugCorePwe_owerr4_2Mask, cDebugCorePwe_owerr4_2Shift);
    Tha6021DebugPrintBitFieldVal(debugger, "pwe_owerr1_0", regValue, cDebugCorePwe_owerr1_0Mask, cDebugCorePwe_owerr1_0Shift);
    Tha6021DebugPrintErrorBit(debugger, "ge10llrxful_4", regValue, cDebugCoreGe10llrxful_4);
    Tha6021DebugPrintErrorBit(debugger, "map2pla_b3err", regValue, cDebugCoreMap2pla_b3err);
    Tha6021DebugPrintErrorBit(debugger, "cla2geena", regValue, cDebugCoreCla2geena);
    Tha6021DebugPrintErrorBit(debugger, "rxeth_eop", regValue, cDebugCoreRxeth_eop);
    Tha6021DebugPrintErrorBit(debugger, "rxeth_sop", regValue, cDebugCoreRxeth_sop);
    Tha6021DebugPrintErrorBit(debugger, "rxeth_vld", regValue, cDebugCoreRxeth_vld);
    Tha6021DebugPrintErrorBit(debugger, "rxclserr", regValue, cDebugCoreRxclserr);
    Tha6021DebugPrintErrorBit(debugger, "rxclseop", regValue, cDebugCoreRxclseop);
    Tha6021DebugPrintErrorBit(debugger, "rxclssop", regValue, cDebugCoreRxclssop);
    Tha6021DebugPrintErrorBit(debugger, "rxclsvld", regValue, cDebugCoreRxclsvld);
    Tha6021DebugPrintErrorBit(debugger, "plawerr", regValue, cDebugCorePlawerr);
    Tha6021DebugPrintErrorBit(debugger, "pwe_ovld", regValue, cDebugCorePwe_ovld);
    Tha6021DebugPrintErrorBit(debugger, "pwe_osop", regValue, cDebugCorePwe_osop);
    Tha6021DebugPrintErrorBit(debugger, "pwe_oeop", regValue, cDebugCorePwe_oeop);
    Tha6021DebugPrintBitFieldVal(debugger, "ge10llrxful3_0", regValue, cDebugCoreGe10llrxfulMask, cDebugCoreGe10llrxfulShift);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

static void CoreStatusDebug(Tha60210031Device self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);
    AtHal hal = AtDeviceIpCoreHalGet((AtDevice)self, 0);

    regAddres = cDebugCoreStatus;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Core status", regAddres, regValue);
    Tha6021DebugPrintInfoBit(debugger, "Ge10lltxenb", regValue, cDebugCoreStatusGe10lltxenb);
    Tha6021DebugPrintInfoBit(debugger, "Ge10lltxsop", regValue, cDebugCoreStatusGe10lltxsop);
    Tha6021DebugPrintInfoBit(debugger, "Ge10lltxeop", regValue, cDebugCoreStatusGe10lltxeop);
    Tha6021DebugPrintInfoBit(debugger, "Ge10lltxvld", regValue, cDebugCoreStatusGe10lltxvld);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_ena", regValue, cDebugCoreStatusPwe_ena);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_oeop", regValue, cDebugCoreStatusPwe_oeop);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_osop", regValue, cDebugCoreStatusPwe_osop);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_ovld", regValue, cDebugCoreStatusPwe_ovld);
    Tha6021DebugPrintInfoBit(debugger, "Ge00lltxenb", regValue, cDebugCoreStatusGe00lltxenb);
    Tha6021DebugPrintInfoBit(debugger, "Map2pla1_dpwval", regValue, cDebugCoreStatusMap2pla1_dpwval);
    Tha6021DebugPrintInfoBit(debugger, "Pla2pwe_opweop", regValue, cDebugCoreStatusPla2pwe_opweop);
    Tha6021DebugPrintInfoBit(debugger, "Pla2pwe_opwvld", regValue, cDebugCoreStatusPla2pwe_opwvld);
    Tha6021DebugPrintInfoBit(debugger, "Pwepk_req", regValue, cDebugCoreStatusPwepk_req);
    Tha6021DebugPrintInfoBit(debugger, "Pweca_req", regValue, cDebugCoreStatusPweca_req);
    Tha6021DebugPrintInfoBit(debugger, "Plapk_req", regValue, cDebugCoreStatusPlapk_req);
    Tha6021DebugPrintInfoBit(debugger, "Placa_req", regValue, cDebugCoreStatusPlaca_req);
    Tha6021DebugPrintInfoBit(debugger, "Reor_req", regValue, cDebugCoreStatusReor_req);
    Tha6021DebugPrintInfoBit(debugger, "Jb_req", regValue, cDebugCoreStatusJb_req);
    Tha6021DebugPrintInfoBit(debugger, "Rdseg_req", regValue, cDebugCoreStatusRdseg_req);
    Tha6021DebugPrintInfoBit(debugger, "Wrseg_req", regValue, cDebugCoreStatusWrseg_req);
    Tha6021DebugPrintInfoBit(debugger, "Wrca_req", regValue, cDebugCoreStatusWrca_req);
    Tha6021DebugPrintInfoBit(debugger, "Rdca_req", regValue, cDebugCoreStatusRdca_req);
    Tha6021DebugPrintInfoBit(debugger, "Avl3_rdy", regValue, cDebugCoreStatusAvl3_rdy);
    Tha6021DebugPrintInfoBit(debugger, "Avl3_done", regValue, cDebugCoreStatusAvl3_done);
    Tha6021DebugPrintInfoBit(debugger, "Avl2_rdy", regValue, cDebugCoreStatusAvl2_rdy);
    Tha6021DebugPrintInfoBit(debugger, "Avl2_done", regValue, cDebugCoreStatusAvl2_done);
    Tha6021DebugPrintInfoBit(debugger, "Avl1_rdy", regValue, cDebugCoreStatusAvl1_rdy);
    Tha6021DebugPrintInfoBit(debugger, "Avl1_done", regValue, cDebugCoreStatusAvl1_done);
    Tha6021DebugPrintInfoBit(debugger, "Rxclslbit", regValue, cDebugCoreStatusRxclslbit);
    Tha6021DebugPrintInfoBit(debugger, "Rxclseop", regValue, cDebugCoreStatusRxclseop);
    Tha6021DebugPrintInfoBit(debugger, "Rxclssop", regValue, cDebugCoreStatusRxclssop);
    Tha6021DebugPrintInfoBit(debugger, "Rxclsvld", regValue, cDebugCoreStatusRxclsvld);
    Tha6021DebugPrintStop(debugger);
    }

static uint32 PohEnableToHwValue(AtDevice self, eBool enable)
	{
	uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
	uint32 checkedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x9, 0x1900);

	if (currentVersion >= checkedVersion)
		return enable ? 1 : 0;

	return enable ? 0 : 1;
	}

static void GlobalPohEnable(AtDevice self, eBool enable)
	{
	uint32 regAddress = cAf6Reg_mac_add_47_32_ctrl_Base;
	uint32 regVal = AtHalRead(AtDeviceIpCoreHalGet(self, 0), regAddress);
	uint32 hwVal = mMethodsGet(mThis(self))->PohEnableToHwValue(self, enable);

	mFieldIns(&regVal, cReg_PohEnable_Mask, cReg_PohEnable_Shift, hwVal);
	AtHalWrite(AtDeviceIpCoreHalGet(self, 0), regAddress, regVal);
	}

void Tha60290011DeviceHwFlushPmFm(ThaDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    /*flush EC1 FM interrupt mask*/
    ThaDeviceMemoryFlush(device, 0x918110, 0x91811F, 0x0);
	ThaDeviceMemoryFlush(device, 0x91B800, 0x91B9FF, 0x0);
	/*flush STS FM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x918090, 0x91809F, 0x0);
	ThaDeviceMemoryFlush(device, 0x91A200, 0x91A3FF, 0x0);
	/*flush DE3 FM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x9180B0, 0x9180BF, 0x0);
	ThaDeviceMemoryFlush(device, 0x91AA00, 0x91ABFF, 0x0);
	/*flush VT FM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x91AE00, 0x91AFFF, 0x0);
	ThaDeviceMemoryFlush(device, 0x928000, 0x92BFFF, 0x0);

	/*flush DE1 FM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x91B200, 0x91B3FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x934000, 0x937FFF, 0x0);
	/*flush PW FM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x91B500, 0x91B5FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x91E000, 0x91FFFF, 0x0);

    /*flush EC1 TCA PMFM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x958110, 0x95811F, 0x0);
	ThaDeviceMemoryFlush(device, 0x95B800, 0x95B9FF, 0x0);
	/*flush STS TCA PMFM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x958090, 0x95809F, 0x0);
	ThaDeviceMemoryFlush(device, 0x95A200, 0x95A3FF, 0x0);
	/*flush DE3 TCA PMFM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x9580B0, 0x9580BF, 0x0);
	ThaDeviceMemoryFlush(device, 0x95A800, 0x95A9FF, 0x0);
	/*flush VT TCA PMFM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x95AE00, 0x95AFFF, 0x0);
	ThaDeviceMemoryFlush(device, 0x968000, 0x96BFFF, 0x0);
	/*flush DE1 TCA PMFM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x95B200, 0x95B3FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x974000, 0x977FFF, 0x0);
	/*flush PW TCA PMFM interrupt mask*/
	ThaDeviceMemoryFlush(device, 0x95B500, 0x95B5FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x95E000, 0x95FFFF, 0x0);
    }

static void PmFmHwFlush(Tha60210031Device self)
    {
    Tha60290011DeviceHwFlushPmFm((ThaDevice)self);
    }

static eAtRet DccPwCleanup(AtDevice self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh);
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    uint8 line_i;
    eAtRet ret = cAtOk;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, line_i);
        if (line)
            ret |= AtSdhLineDccHdlcCleanup(line);
        }

    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = cAtOk;
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(self, cThaModulePoh);
    Tha60210011ModuleBer moduleBer = (Tha60210011ModuleBer)AtDeviceModuleGet(self, cAtModuleBer);

    AtDeviceAllDiagServiceStop(self);
    DccPwCleanup(self);
    GlobalPohEnable(self, cAtFalse);
    Tha60210011ModuleBerEnable(moduleBer, cAtFalse);
    Tha60290011ModulePohJnRequestDdrEnable(modulePoh, cAtFalse);

    ret = Tha60290011JnRequestDdrAndPohCpuDisableIsDoneHwCheck(modulePoh);
    if (ret != cAtOk)
    	return ret;

    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = Tha60290011JnRequestDdrAndPohCpuDisableIsDoneHwCheck(modulePoh);
    if (ret != cAtOk)
        return ret;

    Tha60290011ModulePohJnRequestDdrEnable(modulePoh, cAtTrue);
    GlobalPohEnable(self, cAtTrue);
    return AtSerdesManagerReset(AtDeviceSerdesManagerGet((AtDevice)self));
    }

static eAtRet SubPortVlanEnable(ThaDevice self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleClaSubPortVlanCheckingEnable((ThaModuleCla)AtDeviceModuleGet((AtDevice)self, cThaModuleCla), enable);
    ret |= ThaModulePweSubPortVlanBypass((ThaModulePwe)AtDeviceModuleGet((AtDevice)self, cThaModulePwe), enable ? cAtFalse : cAtTrue);

    return ret;
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return 1;
    }

static eBool Accessible(AtDevice self)
    {
    return Tha602900xxDeviceAccessible(self);
    }

static eAtRet HwReset(AtDevice self)
    {
    eBool shouldApplyNewReset = AtDeviceSemControllerIsSupported(self);
    return Tha60210011DeviceHwResetWithHandler(self, shouldApplyNewReset, m_AtDeviceMethods->HwReset);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60290011IntrControllerNew(core);
    }

static eBool ShouldSwitchToPmc(ThaDevice self)
    {
    return ShouldOpenFeatureFromVersion((AtDevice)self, 0x2, 0x3, 0x0001);
    }

static uint32 DefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    if (mMethodsGet(self)->ShouldSwitchToPmc(self))
        return cThaModulePmc;

    return m_ThaDeviceMethods->DefaultCounterModule(self, moduleId);
    }

static eBool ShouldRestoreDatabaseOnWarmRestoreStop(AtDevice self)
    {
    return Tha602900xxShouldRestoreDatabaseOnWarmRestoreStop(self);
    }

static eBool ShouldEnableInterruptAfterProcessing(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool DccTxFcsMsbIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x6, 0x0006);
    }

static eBool DccHdlcFrameTransmissioniBitOderPerChannelIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x6, 0x0008);
    }

static eBool JnDdrInitIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x9, 0x0101);
    }

static eBool TohSonetSdhModeIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x0, 0x0003);
    }

static eBool TwoDot5GitSerdesPrbsIsRemoved(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x8, 0x0007);
    }

static eBool HasStandardClearTime(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0010);
    }

static eBool PwEthPortHasMacFunctionality(AtDevice self)
    {
    if (ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0))
        return cAtFalse;
    return cAtTrue;
    }

static eBool RxFsmPinSelV2IsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0007);
    }

static eBool EthInterruptIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0007);
    }

static eBool EthSohTransparentIsReady(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0008);
    }

static eBool OcnSohOverEthIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x0, 0x0000);
    }

static eBool OcnVcNewLoopbackIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x08);
    }

static eBool OcnTohByteIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0012);
    }

static eBool PdaCanControlLopsPktReplaceMode(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x2, 0x0);
    }

static uint32 StartVersionSupportDe1Ssm(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x3, 0x0005);
    }

static eBool RxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x9, 0x0000);
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x0);
    }

static uint32 StartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x0017);
    }

static eBool De1RetimingIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x3, 0x0005);
    }

static uint32 StartVersionHasPrmRxEnabledCfgRegister(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x00);
    }

static eBool IsMroSerdesBackplaneImplemented(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0007);
    }

static eBool JnRequestDdrAndPohCpuDisableIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x9, 0x0202);
    }

static uint32 StartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x9, 0x1900);
    }

static eBool PwDccKbyteInterruptIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x0, 0x0004);
    }

static eBool SurFailureForwardingStatusIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x5, 0x0000);
    }

static eBool SurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x1, 0x6, 0x0000);
    }

static eBool OcnTxDccRsMsInsControlIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x0, 0x0008);
    }

static eBool DccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x0, 0x0018);
    }

static eBool BackplaneSerdesEthPortFecIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0000);
    }

static eBool BackplaneSerdesEthPortInterruptIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x1, 0x0000);
    }

static eBool ShouldEnableLosPpmDetection(Tha60290011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void SemControllerSerialize(AtDevice object, AtCoder encoder)
    {
    AtUnused(object);
    AtUnused(encoder);
    }

static void PdhHwFlush(Tha60210031Device self)
    {
    Tha60210031DeviceHwFlushPdhSlice0((ThaDevice)self);
    }

static void MapHwFlush(Tha60210031Device self)
    {
    Tha60210031DeviceHwFlushMap0((ThaDevice)self);
    }

static void CdrHwFlush(Tha60210031Device self)
    {
    Tha60210031DeviceHwFlushCdr0((ThaDevice)self);
    }

static eBool Ec1LoopbackIsSupported(Tha60210031Device self)
    {
    return Tha60290011DeviceOcnVcNewLoopbackIsSupported((AtDevice)self);
    }

static void MethodsInit(Tha60290011Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohEnableToHwValue);
        mMethodOverride(m_methods, DccTxFcsMsbIsSupported);
        mMethodOverride(m_methods, DccHdlcFrameTransmissioniBitOderPerChannelIsSupported);
        mMethodOverride(m_methods, JnDdrInitIsSupported);
        mMethodOverride(m_methods, TohSonetSdhModeIsSupported);
        mMethodOverride(m_methods, TwoDot5GitSerdesPrbsIsRemoved);
        mMethodOverride(m_methods, HasStandardClearTime);
        mMethodOverride(m_methods, PwEthPortHasMacFunctionality);
        mMethodOverride(m_methods, RxFsmPinSelV2IsSupported);
        mMethodOverride(m_methods, EthInterruptIsSupported);
        mMethodOverride(m_methods, EthSohTransparentIsReady);
        mMethodOverride(m_methods, OcnSohOverEthIsSupported);
        mMethodOverride(m_methods, OcnVcNewLoopbackIsSupported);
        mMethodOverride(m_methods, OcnTohByteIsSupported);
        mMethodOverride(m_methods, PdaCanControlLopsPktReplaceMode);
        mMethodOverride(m_methods, StartVersionSupportDe1Ssm);
        mMethodOverride(m_methods, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_methods, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_methods, StartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_methods, De1RetimingIsSupported);
        mMethodOverride(m_methods, StartVersionHasPrmRxEnabledCfgRegister);
        mMethodOverride(m_methods, IsMroSerdesBackplaneImplemented);
        mMethodOverride(m_methods, JnRequestDdrAndPohCpuDisableIsSupported);
        mMethodOverride(m_methods, StartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_methods, PwDccKbyteInterruptIsSupported);
        mMethodOverride(m_methods, SurFailureForwardingStatusIsSupported);
        mMethodOverride(m_methods, SurFailureHoldOnTimerConfigurable);
        mMethodOverride(m_methods, OcnTxDccRsMsInsControlIsSupported);
        mMethodOverride(m_methods, DccEthMaxLengthConfigurationIsSupported);
        mMethodOverride(m_methods, BackplaneSerdesEthPortFecIsSupported);
        mMethodOverride(m_methods, BackplaneSerdesEthPortInterruptIsSupported);
        mMethodOverride(m_methods, ShouldEnableLosPpmDetection);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, HasRole);
        mMethodOverride(m_AtDeviceOverride, RoleSet);
        mMethodOverride(m_AtDeviceOverride, RoleGet);
        mMethodOverride(m_AtDeviceOverride, AutoRoleGet);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, NumSemControllersGet);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorObjectCreate);
        mMethodOverride(m_AtDeviceOverride, NumInterruptPins);
        mMethodOverride(m_AtDeviceOverride, InterruptPinObjectCreate);
        mMethodOverride(m_AtDeviceOverride, InterruptPinDefaultTriggerMode);
        mMethodOverride(m_AtDeviceOverride, DiagnosticUartObjectCreate);
        mMethodOverride(m_AtDeviceOverride, Setup);
        mMethodOverride(m_AtDeviceOverride, PowerSupplySensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, PowerSupplySensorObjectCreate);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, Accessible);
        mMethodOverride(m_AtDeviceOverride, HwReset);
        mMethodOverride(m_AtDeviceOverride, ShouldRestoreDatabaseOnWarmRestoreStop);
        mMethodOverride(m_AtDeviceOverride, ShouldEnableInterruptAfterProcessing);
        mMethodOverride(m_AtDeviceOverride, SemControllerSerialize);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        mMethodOverride(m_ThaDeviceOverride, StartVersionSupportSurveillance);
        mMethodOverride(m_ThaDeviceOverride, ClockDebug);
        mMethodOverride(m_ThaDeviceOverride, ClockStateValueV2IsSupported);
        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, DefaultCounterModule);
        mMethodOverride(m_ThaDeviceOverride, ShouldSwitchToPmc);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60210031Device(AtDevice self)
    {
    Tha60210031Device device = (Tha60210031Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031DeviceOverride, m_Tha60210031DeviceMethods, sizeof(m_Tha60210031DeviceOverride));

        mMethodOverride(m_Tha60210031DeviceOverride, HwDiagnosticModeEnable);
        mMethodOverride(m_Tha60210031DeviceOverride, DdrCurrentStatusRegister);
        mMethodOverride(m_Tha60210031DeviceOverride, StartVersionSwHandleQsgmiiLanes);
        mMethodOverride(m_Tha60210031DeviceOverride, EthStickyDebug);
        mMethodOverride(m_Tha60210031DeviceOverride, DdrCalibCurrentStatusIsDone);
        mMethodOverride(m_Tha60210031DeviceOverride, PllStickyDebug);
        mMethodOverride(m_Tha60210031DeviceOverride, PllStatusDebug);
        mMethodOverride(m_Tha60210031DeviceOverride, CoreStickyDebug);
        mMethodOverride(m_Tha60210031DeviceOverride, CoreStatusDebug);
        mMethodOverride(m_Tha60210031DeviceOverride, PdhHwFlush);
        mMethodOverride(m_Tha60210031DeviceOverride, MapHwFlush);
        mMethodOverride(m_Tha60210031DeviceOverride, PmFmHwFlush);
        mMethodOverride(m_Tha60210031DeviceOverride, CdrHwFlush);
        mMethodOverride(m_Tha60210031DeviceOverride, Ec1LoopbackIsSupported);
        }

    mMethodsSet(device, &m_Tha60210031DeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, SemUartIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, ClockStatusDisplay);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210031Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011Device);
    }

AtDevice Tha60290011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60290011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011DeviceObjectInit(newDevice, driver, productCode);
    }

uint32 Tha60290011SuperDevicePllClkSysStatBitMaskGet(ThaDevice self)
    {
    return m_ThaDeviceMethods->PllClkSysStatBitMask(self);
    }

uint32 Tha60290011SuperDeviceDdrCurrentStatusRegister(ThaDevice self)
    {
    return m_Tha60210031DeviceMethods->DdrCurrentStatusRegister((Tha60210031Device)self);
    }

eAtRet Tha60290011DeviceSubPortVlanEnable(ThaDevice self, eBool enable)
    {
    if (self)
        return SubPortVlanEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290011DeviceDccTxFcsMsbIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccTxFcsMsbIsSupported(self);
    }

eBool Tha60290011DeviceDccHdlcFrameTransmissioniBitOderPerChannelIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccHdlcFrameTransmissioniBitOderPerChannelIsSupported(self);
    }

eBool Tha60290011DeviceJnDdrInitIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->JnDdrInitIsSupported(self);
    }

eBool Tha60290011DeviceTohSonetSdhModeIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->TohSonetSdhModeIsSupported(self);
    }

eBool Tha60290011Device2Dot5GitSerdesPrbsIsRemoved(AtDevice self)
    {
    return mMethodsGet(mThis(self))->TwoDot5GitSerdesPrbsIsRemoved(self);
    }

eBool Tha60290011DeviceHasStandardClearTime(AtDevice self)
    {
    return mMethodsGet(mThis(self))->HasStandardClearTime(self);
    }

eBool Tha60290011DevicePwEthPortHasMacFunctionality(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PwEthPortHasMacFunctionality(self);
    }

eBool Tha60290011DeviceRxFsmPinSelV2IsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->RxFsmPinSelV2IsSupported(self);
    }

eBool Tha60290011DeviceEthInterruptIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->EthInterruptIsSupported(self);
    }

eBool Tha60290011DeviceEthSohTransparentIsReady(AtDevice self)
    {
    return mMethodsGet(mThis(self))->EthSohTransparentIsReady(self);
    }

eBool Tha60290011DeviceOcnSohOverEthIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->OcnSohOverEthIsSupported(self);
    }

eBool Tha60290011DeviceOcnVcNewLoopbackIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->OcnVcNewLoopbackIsSupported(self);
    }

eBool Tha60290011DeviceOcnTohByteIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->OcnTohByteIsSupported(self);
    }

eBool Tha60290011DevicePdaCanControlLopsPktReplaceMode(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdaCanControlLopsPktReplaceMode(self);
    }

uint32 Tha60290011DeviceStartVersionSupportDe1Ssm(AtDevice self)
    {
    return mMethodsGet(mThis(self))->StartVersionSupportDe1Ssm(self);
    }

eBool Tha60290011DeviceRxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->RxHw3BitFeacSignalIsSupported(self);
    }

uint32 Tha60290011DeviceStartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    return mMethodsGet(mThis(self))->StartVersionSupportDe1LomfConsequentialAction(self);
    }

uint32 Tha60290011DeviceStartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    return mMethodsGet(mThis(self))->StartVersionSupportDe1IdleCodeInsertion(self);
    }

eBool Tha60290011DeviceDe1RetimingIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->De1RetimingIsSupported(self);
    }

uint32 Tha60290011DeviceStartVersionHasPrmRxEnabledCfgRegister(AtDevice self)
    {
    return mMethodsGet(mThis(self))->StartVersionHasPrmRxEnabledCfgRegister(self);
    }

eBool Tha60290011DeviceIsMroSerdesBackplaneImplemented(AtDevice self)
    {
    return mMethodsGet(mThis(self))->IsMroSerdesBackplaneImplemented(self);
    }

eBool Tha60290011DeviceJnRequestDdrAndPohCpuDisableIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->JnRequestDdrAndPohCpuDisableIsSupported(self);
    }

uint32 Tha60290011DeviceStartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    return mMethodsGet(mThis(self))->StartVersionSupportSeparateRdiErdiS(self);
    }

eBool Tha60290011DevicePwDccKbyteInterruptIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PwDccKbyteInterruptIsSupported(self);
    }

eBool Tha60290011DeviceSurFailureForwardingStatusIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SurFailureForwardingStatusIsSupported(self);
    }

eBool Tha60290011DeviceSurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SurFailureHoldOnTimerConfigurable(self);
    }

eBool Tha60290011DeviceOcnTxDccRsMsInsControlIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->OcnTxDccRsMsInsControlIsSupported(self);
    }

eBool Tha60290011DeviceDccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccEthMaxLengthConfigurationIsSupported(self);
    }

eBool Tha60290011DeviceBackplaneSerdesEthPortFecIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->BackplaneSerdesEthPortFecIsSupported(self);
    }

eBool Tha60290011DeviceBackplaneSerdesEthPortInterruptIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->BackplaneSerdesEthPortInterruptIsSupported(self);
    }

eBool Tha60290011DeviceShouldEnableLosPpmDetection(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->ShouldEnableLosPpmDetection(mThis(self));
    return cAtFalse;
    }


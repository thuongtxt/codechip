/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_TOP_H_
#define _AF6_REG_AF6CNC0011_RD_TOP_H_

/*--------------------------- Define -----------------------------------------*/
#define cTopBaseAddress                    0xF00000
#define cXfiSerdesMdioBaseAddress          0xF54800
#define cSgmiiSerdesMdioBaseAddress        0xF54000
#define cSgmii2500MSerdesTuningBaseAddress 0xF56000
#define cXfiActSerdesTuningBaseAddress     0xF60000
#define cXfiStbSerdesTuningBaseAddress     0xF70000
#define cSgmiiDimSerdesTuningBaseAddress   0xF50000
#define cSgmiiDccSerdesTuningBaseAddress   0xF51000
#define cSgmiiSpareSerdesTuningBaseAddress 0xF52000
#define cXfiActDiagBaseAddress             0xF55000
#define cXfiStbDiagBaseAddress             0xF55800

#define cAf6_global_interrupt_status_XilinxSerdesBackplane0_IntStatus_Mask cBit22
#define cAf6_global_interrupt_status_XilinxSerdesBackplane1_IntStatus_Mask cBit23
#define cAf6_global_interrupt_status_DCCKIntEnable_Mask                    cBit31

/*------------------------------------------------------------------------------
Reg Name   : Device Product ID
Reg Addr   : 0xF0_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates Product ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_ProductID_Base                                                                        0xF00000

/*--------------------------------------
BitField Name: ProductID
BitField Type: RO
BitField Desc: ProductId
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ProductID_ProductID_Mask                                                                 cBit31_0
#define cAf6_ProductID_ProductID_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Device Year Month Day Version ID
Reg Addr   : 0xF0_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates Year Month Day and main version ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_YYMMDD_VerID_Base                                                                     0xF00001

/*--------------------------------------
BitField Name: Year
BitField Type: RO
BitField Desc: Year
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Year_Mask                                                                  cBit31_24
#define cAf6_YYMMDD_VerID_Year_Shift                                                                        24

/*--------------------------------------
BitField Name: Month
BitField Type: RO
BitField Desc: Month
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Month_Mask                                                                 cBit23_16
#define cAf6_YYMMDD_VerID_Month_Shift                                                                       16

/*--------------------------------------
BitField Name: Day
BitField Type: RO
BitField Desc: Day
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Day_Mask                                                                    cBit15_8
#define cAf6_YYMMDD_VerID_Day_Shift                                                                          8

/*--------------------------------------
BitField Name: Version
BitField Type: RO
BitField Desc: Version
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Version_Mask                                                                 cBit7_0
#define cAf6_YYMMDD_VerID_Version_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Device Internal ID
Reg Addr   : 0xF0_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates internal ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_InternalID_Base                                                                       0xF00003
#define cAf6Reg_InternalID_WidthVal                                                                         32

/*--------------------------------------
BitField Name: InternalID
BitField Type: RO
BitField Desc: InternalID
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_InternalID_InternalID_Mask                                                               cBit15_0
#define cAf6_InternalID_InternalID_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : general purpose inputs for TX Uart
Reg Addr   : 0x40
Reg Formula: 0x40
    Where  : 
Reg Desc   : 
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control0_Base                                                                           0x40


#define cAf6_o_control0_dup_tx_10g_Mask                                                                 cBit31
#define cAf6_o_control0_dup_tx_10g_Shift                                                                31

#define cAf6_o_control0_sel_fsm_card_Mask                                                               cBit28
#define cAf6_o_control0_sel_fsm_card_Shift                                                              28

#define cAf6_o_control0_cpu_sel_card_Mask                                                               cBit27
#define cAf6_o_control0_cpu_sel_card_Shift                                                              27

/*--------------------------------------
BitField Name: Sel10GPort
BitField Type: RW
BitField Desc: Select RX 10G port inside
BitField Bits: [17]
--------------------------------------*/
#define cAf6_o_control0_Sel10GPort_Mask                                                                 cBit17
#define cAf6_o_control0_Sel10GPort_Shift                                                                    17

#define cAf6_o_control0_Sel10GPort_V2_Mask                                                              cBit29
#define cAf6_o_control0_Sel10GPort_V2_Shift                                                             29

/*--------------------------------------
BitField Name: EnRxFSMpin
BitField Type: RW
BitField Desc: Enable RX FSM pin1 from outsite for 10G port select
BitField Bits: [16]
--------------------------------------*/
#define cAf6_o_control0_EnRxFSMpin_Mask                                                                 cBit16
#define cAf6_o_control0_EnRxFSMpin_Shift                                                                    16

#define cAf6_o_control0_EnRxFSMpin_V2_Mask                                                              cBit30
#define cAf6_o_control0_EnRxFSMpin_V2_Shift                                                                 30

/*--------------------------------------
BitField Name: tx_trig_send
BitField Type: RW
BitField Desc: Uart TX trig start send out byte
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_o_control0_tx_trig_send_Mask                                                                cBit8
#define cAf6_o_control0_tx_trig_send_Shift                                                                   8

/*--------------------------------------
BitField Name: tx_data_out
BitField Type: RW
BitField Desc: Uart TX 8-bit data out
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_o_control0_tx_data_out_Mask                                                               cBit7_0
#define cAf6_o_control0_tx_data_out_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Flow Control Mechanism for Ethernet pass-through Diagnostic Value
Reg Addr   : 0x41
Reg Formula: 0x41
    Where  : 
Reg Desc   : 
This is the global configuration register for Flow Control Mechanism for Ethernet pass-through Diagnostic

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control1_Base                                                                           0x41


/*------------------------------------------------------------------------------
Reg Name   : o_control2
Reg Addr   : 0x42
Reg Formula: 0x42
    Where  : 
Reg Desc   : 
This is the global configuration Error register for Flow Control Mechanism for Ethernet pass-through Diagnostic

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control2_Base                                                                           0x42


/*------------------------------------------------------------------------------
Reg Name   : Diagnostic Enable Control
Reg Addr   : 0x43
Reg Formula: 0x43
    Where  : 
Reg Desc   : 
This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control3_Base                                                                           0x43

/*--------------------------------------
BitField Name: SGMII_SP_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for SGMII_SP 0: Disable 1: Enable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_o_control3_SGMII_SP_DiagEn_Mask                                                             cBit9
#define cAf6_o_control3_SGMII_SP_DiagEn_Shift                                                                9

/*--------------------------------------
BitField Name: SGMII_DCC_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for SGMII_DCC 0: Disable 1: Enable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_o_control3_SGMII_DCC_DiagEn_Mask                                                            cBit8
#define cAf6_o_control3_SGMII_DCC_DiagEn_Shift                                                               8

/*--------------------------------------
BitField Name: SGMII_DIM_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for SGMII_DIM 0: Disable 1: Enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_o_control3_SGMII_DIM_DiagEn_Mask                                                            cBit7
#define cAf6_o_control3_SGMII_DIM_DiagEn_Shift                                                               7

/*--------------------------------------
BitField Name: Basex2500_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for Basex2500 0: Disable 1: Enable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_o_control3_Basex2500_DiagEn_Mask                                                            cBit6
#define cAf6_o_control3_Basex2500_DiagEn_Shift                                                               6

/*--------------------------------------
BitField Name: STBY_XFI_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for ACT_STBY 0: Disable 1: Enable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_o_control3_STBY_XFI_DiagEn_Mask                                                             cBit5
#define cAf6_o_control3_STBY_XFI_DiagEn_Shift                                                                5

/*--------------------------------------
BitField Name: ACT_XFI_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for ACT_XFI 0: Disable 1: Enable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_o_control3_ACT_XFI_DiagEn_Mask                                                              cBit4
#define cAf6_o_control3_ACT_XFI_DiagEn_Shift                                                                 4

/*--------------------------------------
BitField Name: DDR3_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#2 0: Disable 1: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_o_control3_DDR3_DiagEn_Mask                                                                 cBit2
#define cAf6_o_control3_DDR3_DiagEn_Shift                                                                    2

/*--------------------------------------
BitField Name: DDR2_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#1 0: Disable 1: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_o_control3_DDR2_DiagEn_Mask                                                                 cBit1
#define cAf6_o_control3_DDR2_DiagEn_Shift                                                                    1

/*--------------------------------------
BitField Name: DDR1_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#1 0: Disable 1: Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_control3_DDR1_DiagEn_Mask                                                                 cBit0
#define cAf6_o_control3_DDR1_DiagEn_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Serdes Turning Bus Select for XFI
Reg Addr   : 0x44
Reg Formula: 0x44
    Where  : 
Reg Desc   : 
This is the global configuration register for the Global Serdes Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag port 1 to 8

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control4_Base                                                                           0x44

/*--------------------------------------
BitField Name: xfi1_drp_en
BitField Type: RW
BitField Desc: Select cpu control for serdes DRP or Global control of XFI_STBY
0: Global control 1: DRP access
BitField Bits: [1]
--------------------------------------*/
#define cAf6_o_control4_xfi1_drp_en_Mask                                                                 cBit1
#define cAf6_o_control4_xfi1_drp_en_Shift                                                                    1

/*--------------------------------------
BitField Name: xfi0_drp_en
BitField Type: RW
BitField Desc: Select CPU control for serdes DRP or Global control of XFI_ACT 0:
Global control 1: DRP access
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_control4_xfi0_drp_en_Mask                                                                 cBit0
#define cAf6_o_control4_xfi0_drp_en_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Configure Clock monitor output
Reg Addr   : 0x49
Reg Formula: 0x49
    Where  : 
Reg Desc   : 
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control9_Base                                                                           0x49

/*--------------------------------------
BitField Name: cfgrefpid
BitField Type: RW
BitField Desc: Configure Source Clock monitor output for clock_mon 0x0 :
pcie_refclk_div2 0x1 : System PLL 155.52Mhz output 0x2 : refin_prc 0x3 :
ext_ref_timing 0x4 : xfi_156p25_refclk#0_div2 0x5 : xfi_156p25_refclk#1_div2 0x6
: spare_serdes_refclk_div2 0x7 : basex2500_ref_div2 0x8 : ddr3_refclk#1 0x9 :
ddr3_refclk#2 0xa : ddr3_refclk#3
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_o_control9_cfgrefpid_Mask                                                                 cBit4_0
#define cAf6_o_control9_cfgrefpid_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : general purpose outputs from RX Uart
Reg Addr   : 0x60
Reg Formula: 0x60
    Where  : 
Reg Desc   : 
This is value output from RX Uart

------------------------------------------------------------------------------*/
#define cAf6Reg_c_uart_Base                                                                               0x60

/*--------------------------------------
BitField Name: i_status0
BitField Type: RO
BitField Desc: Uart RX 8-bit data in
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_c_uart_i_status0_Mask                                                                     cBit7_0
#define cAf6_c_uart_i_status0_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : unused
Reg Addr   : 0x61
Reg Formula: 0x61
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_unused                                                                                    0x61


/*------------------------------------------------------------------------------
Reg Name   : Top Status
Reg Addr   : 0x62
Reg Formula: 0x62
    Where  : 
Reg Desc   : 
Top Interface Alarm Status

------------------------------------------------------------------------------*/
#define cAf6Reg_status_top_Base                                                                           0x62

/*--------------------------------------
BitField Name: rxfsm1sta
BitField Type: RO
BitField Desc: current FSM pin1 for RX port select
BitField Bits: [30]
--------------------------------------*/
#define cAf6_status_top_rxfsm1sta_Mask                                                                  cBit30
#define cAf6_status_top_rxfsm1sta_Shift                                                                     30

/*--------------------------------------
BitField Name: txfsm0sta
BitField Type: RO
BitField Desc: current FSM pin0 for TX pw
BitField Bits: [28]
--------------------------------------*/
#define cAf6_status_top_txfsm0sta_Mask                                                                  cBit28
#define cAf6_status_top_txfsm0sta_Shift                                                                     28

/*--------------------------------------
BitField Name: ot_out
BitField Type: RO
BitField Desc: Current Status  Over-Temperature alarm .
BitField Bits: [25]
--------------------------------------*/
#define cAf6_status_top_ot_out_Mask                                                                     cBit25
#define cAf6_status_top_ot_out_Shift                                                                        25

/*--------------------------------------
BitField Name: vbram_alarm_out
BitField Type: RO
BitField Desc: Current Status  VCCBRAM-sensor alarm .
BitField Bits: [24]
--------------------------------------*/
#define cAf6_status_top_vbram_alarm_out_Mask                                                            cBit24
#define cAf6_status_top_vbram_alarm_out_Shift                                                               24

/*--------------------------------------
BitField Name: vccaux_alarm_out
BitField Type: RO
BitField Desc: Current Status  VCCAUX-sensor alarm .
BitField Bits: [23]
--------------------------------------*/
#define cAf6_status_top_vccaux_alarm_out_Mask                                                           cBit23
#define cAf6_status_top_vccaux_alarm_out_Shift                                                              23

/*--------------------------------------
BitField Name: vccint_alarm_out
BitField Type: RO
BitField Desc: Current Status  VCCINT-sensor alarm .
BitField Bits: [22]
--------------------------------------*/
#define cAf6_status_top_vccint_alarm_out_Mask                                                           cBit22
#define cAf6_status_top_vccint_alarm_out_Shift                                                              22

/*--------------------------------------
BitField Name: user_temp_alarm_out
BitField Type: RO
BitField Desc: Current Status  Temperature-sensor alarm .
BitField Bits: [21]
--------------------------------------*/
#define cAf6_status_top_user_temp_alarm_out_Mask                                                        cBit21
#define cAf6_status_top_user_temp_alarm_out_Shift                                                           21

/*--------------------------------------
BitField Name: alarm_out
BitField Type: RO
BitField Desc: Current Status  SysMon Error .
BitField Bits: [20]
--------------------------------------*/
#define cAf6_status_top_alarm_out_Mask                                                                  cBit20
#define cAf6_status_top_alarm_out_Shift                                                                     20

/*--------------------------------------
BitField Name: SystemPll
BitField Type: RO
BitField Desc: Current Status System PLL not locked. 0: Locked 1: Unlocked
BitField Bits: [16]
--------------------------------------*/
#define cAf6_status_top_SystemPll_Mask                                                                  cBit16
#define cAf6_status_top_SystemPll_Shift                                                                     16

/*--------------------------------------
BitField Name: Ddr33Urst
BitField Type: RO
BitField Desc: Current Status DDR#3 user Reset. 0: OutReseset 1: InReset
BitField Bits: [10]
--------------------------------------*/
#define cAf6_status_top_Ddr33Urst_Mask                                                                  cBit10
#define cAf6_status_top_Ddr33Urst_Shift                                                                     10

/*--------------------------------------
BitField Name: Ddr32Urst
BitField Type: RO
BitField Desc: Current Status DDR#2 user Reset. 0: OutReseset 1: InReset
BitField Bits: [9]
--------------------------------------*/
#define cAf6_status_top_Ddr32Urst_Mask                                                                   cBit9
#define cAf6_status_top_Ddr32Urst_Shift                                                                      9

/*--------------------------------------
BitField Name: Ddr31Urst
BitField Type: RO
BitField Desc: Current Status DDR#1 user Reset. 0: OutReseset 1: InReset
BitField Bits: [8]
--------------------------------------*/
#define cAf6_status_top_Ddr31Urst_Mask                                                                   cBit8
#define cAf6_status_top_Ddr31Urst_Shift                                                                      8

/*--------------------------------------
BitField Name: Ddr33Calib
BitField Type: RO
BitField Desc: Current Status DDR#3 Calib . 0: Calib PASS 1: Calib FAIL
BitField Bits: [2]
--------------------------------------*/
#define cAf6_status_top_Ddr33Calib_Mask                                                                  cBit2
#define cAf6_status_top_Ddr33Calib_Shift                                                                     2

/*--------------------------------------
BitField Name: Ddr32Calib
BitField Type: RO
BitField Desc: Current Status DDR#2 Calib . 0: Calib PASS 1: Calib FAIL
BitField Bits: [1]
--------------------------------------*/
#define cAf6_status_top_Ddr32Calib_Mask                                                                  cBit1
#define cAf6_status_top_Ddr32Calib_Shift                                                                     1

/*--------------------------------------
BitField Name: Ddr31Calib
BitField Type: RO
BitField Desc: Current Status DDR#1 Calib . 0: Calib PASS 1: Calib FAIL
BitField Bits: [0]
--------------------------------------*/
#define cAf6_status_top_Ddr31Calib_Mask                                                                  cBit0
#define cAf6_status_top_Ddr31Calib_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Top Interface Alarm Sticky
Reg Addr   : 0x50
Reg Formula: 
    Where  : 
Reg Desc   : 
Top Interface Alarm Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_i_sticky0_Base                                                                            0x50

/*--------------------------------------
BitField Name: ot_out
BitField Type: W1C
BitField Desc: Set 1 When Over-Temperature alarm Happens.
BitField Bits: [25]
--------------------------------------*/
#define cAf6_i_sticky0_ot_out_Mask                                                                      cBit25
#define cAf6_i_sticky0_ot_out_Shift                                                                         25

/*--------------------------------------
BitField Name: vbram_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When VCCBRAM-sensor alarm Happens.
BitField Bits: [24]
--------------------------------------*/
#define cAf6_i_sticky0_vbram_alarm_out_Mask                                                             cBit24
#define cAf6_i_sticky0_vbram_alarm_out_Shift                                                                24

/*--------------------------------------
BitField Name: vccaux_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When VCCAUX-sensor alarm Happens.
BitField Bits: [23]
--------------------------------------*/
#define cAf6_i_sticky0_vccaux_alarm_out_Mask                                                            cBit23
#define cAf6_i_sticky0_vccaux_alarm_out_Shift                                                               23

/*--------------------------------------
BitField Name: vccint_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When VCCINT-sensor alarm Happens.
BitField Bits: [22]
--------------------------------------*/
#define cAf6_i_sticky0_vccint_alarm_out_Mask                                                            cBit22
#define cAf6_i_sticky0_vccint_alarm_out_Shift                                                               22

/*--------------------------------------
BitField Name: user_temp_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When Temperature-sensor alarm Happens.
BitField Bits: [21]
--------------------------------------*/
#define cAf6_i_sticky0_user_temp_alarm_out_Mask                                                         cBit21
#define cAf6_i_sticky0_user_temp_alarm_out_Shift                                                            21

/*--------------------------------------
BitField Name: alarm_out
BitField Type: W1C
BitField Desc: Set 1 When SysMon Error Happens.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_i_sticky0_alarm_out_Mask                                                                   cBit20
#define cAf6_i_sticky0_alarm_out_Shift                                                                      20

/*--------------------------------------
BitField Name: SystemPll
BitField Type: W1C
BitField Desc: Set 1 while PLL Locked state change event happens when System PLL
not locked.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_i_sticky0_SystemPll_Mask                                                                   cBit16
#define cAf6_i_sticky0_SystemPll_Shift                                                                      16

/*--------------------------------------
BitField Name: Ddr33Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#3 Reset
User.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr33Urst_Mask                                                                   cBit10
#define cAf6_i_sticky0_Ddr33Urst_Shift                                                                      10

/*--------------------------------------
BitField Name: Ddr32Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#2 Reset
User.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr32Urst_Mask                                                                    cBit9
#define cAf6_i_sticky0_Ddr32Urst_Shift                                                                       9

/*--------------------------------------
BitField Name: Ddr31Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#1 Reset
User.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr31Urst_Mask                                                                    cBit8
#define cAf6_i_sticky0_Ddr31Urst_Shift                                                                       8

/*--------------------------------------
BitField Name: Ddr33Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#3 Calib
Fail.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr33Calib_Mask                                                                   cBit2
#define cAf6_i_sticky0_Ddr33Calib_Shift                                                                      2

/*--------------------------------------
BitField Name: Ddr32Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#2 Calib
Fail.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr32Calib_Mask                                                                   cBit1
#define cAf6_i_sticky0_Ddr32Calib_Shift                                                                      1

/*--------------------------------------
BitField Name: Ddr31Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#1 Calib
Fail.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr31Calib_Mask                                                                   cBit0
#define cAf6_i_sticky0_Ddr31Calib_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Clock Monitoring Status
Reg Addr   : 0x46000 - 0x4601F
Reg Formula: 0x46000 + port_id
    Where  : 
           + $port_id(0-31
Reg Desc   : 

0x0e : System clk 		     (155.52 Mhz)
0x0d : prc_ref clock        (155.52 Mhz)
0x0c : ext_ref clock        (155.52 Mhz)
0x0b : spgmii_user_clk      (125 Mhz)
0x0a : dccgmii_user_clk     (125 Mhz)
0x09 : dimgmii_user_clk     (125 Mhz)
0x08 : basex2500_user_clk	 (312.5 Mhz)
0x07 : 10g_stby_rxclk     	 (156.25 Mhz)
0x06 : 10g_act_rxclk     	 (156.25 Mhz)
0x05 : 10g_stby_txclk     	 (156.25 Mhz)
0x04 : 10g_act_txclk     	 (156.25 Mhz)
0x03 : DDR3#3 user clock    (200 Mhz)
0x02 : DDR3#2 user clock    (200 Mhz)
0x01 : DDR3#1 user clock    (200 Mhz)
0x00 : pcie_upclk clock     (62.5 Mhz)

------------------------------------------------------------------------------*/
#define cAf6Reg_clock_mon_high_Base                                                                    0x46000

/*--------------------------------------
BitField Name: i_statusx
BitField Type: RO
BitField Desc: Clock Value Monitor Change from hex format to DEC format to get
Clock Value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_clock_mon_high_i_statusx_Mask                                                            cBit31_0
#define cAf6_clock_mon_high_i_statusx_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Clock Monitoring Status
Reg Addr   : 0x47000 - 0x4701f
Reg Formula: 0x47000 + port_id
    Where  : 
           + $port_id(0-31
Reg Desc   : 

0x1f : unused (0 Mhz)
0x1e : unused (0 Mhz)
0x1d : unused (0 Mhz)
0x1c : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value
0x1b : unused (0 Mhz)
0x1a : unused (0 Mhz)
0x19 : unused (0 Mhz)
0x18 : unused (0 Mhz)
0x17 : unused (0 Mhz)
0x16 : unused (0 Mhz)
0x15 : unused (0 Mhz)
0x14 : unused (0 Mhz)
0x13 : unused (0 Mhz)
0x12 : unused (0 Mhz)
0x11 : unused (0 Mhz)
0x10 : unused (0 Mhz)
0x0f : unused (0 Mhz)
0x0e : unused (0 Mhz)
0x0d : unused (0 Mhz)
0x0c : fsm_pcp_2   		(1 Mhz or 2 Mhz  frequency clk Diagnostic)
0x0b : fsm_pcp_1   		(1 Mhz or 2 Mhz  frequency clk Diagnostic)
0x0a : fsm_pcp_0   		(1 Mhz or 2 Mhz  frequency clk Diagnostic)
0x09 : spare_gpio_7        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x08 : spare_gpio_6        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x07 : spare_gpio_5        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x06 : spare_gpio_4        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x05 : spare_gpio_3        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x04 : spare_gpio_2        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x03 : spare_gpio_1        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x02 : spare_gpio_0        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x01 : spare_clk_1         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x00 : spare_clk_0         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

------------------------------------------------------------------------------*/
#define cAf6Reg_clock_mon_slow_Base                                                                    0x47000

/*--------------------------------------
BitField Name: i_statusx
BitField Type: RO
BitField Desc: Clock Value Monitor Change from hex format to DEC format to get
Clock Value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_clock_mon_slow_i_statusx_Mask                                                            cBit31_0
#define cAf6_clock_mon_slow_i_statusx_Shift                                                                  0

#define cAf6_CONFIGURATION_FEC_REG_address             0x30
#define cAf6_ctl_fec_rx_enable_Mask                    cBit0
#define cAf6_ctl_fec_rx_enable_Shift                   0
#define cAf6_ctl_fec_tx_enable_Mask                    cBit1
#define cAf6_ctl_fec_tx_enable_Shift                   1
#define cAf6_ctl_fec_enable_error_to_pcs_Mask          cBit2
#define cAf6_ctl_fec_enable_error_to_pcs_Shift         2

#define cAf6_CONFIGURATION_AN_CONTROL_REG1_address     0x31
#define cAf6_ctl_restart_negotiation_Mask              cBit11
#define cAf6_ctl_restart_negotiation_Shift             11
#define cAf6_ctl_autoneg_bypass_Mask                   cBit1
#define cAf6_ctl_autoneg_bypass_Shift                  1
#define cAf6_ctl_autoneg_enable_Mask                   cBit0
#define cAf6_ctl_autoneg_enable_Shift                  0

#define cAf6_CONFIGURATION_AN_CONTROL_REG2_address     0x32
#define cAf6_ctl_an_fec_10g_request_Mask               cBit16
#define cAf6_ctl_an_fec_10g_request_Shift              16

#define cAf6_CONFIGURATION_AN_ABILITY_address          0x33
#define cAf6_ctl_an_ability_10gbase_kr_Mask            cBit2
#define cAf6_ctl_an_ability_10gbase_kr_Shift           2

#define cAf6_CONFIGURATION_LT_CONTROL_REG1_address     0x34
#define cAf6_CONFIGURATION_LT_TRAINED_REG_address      0x35
#define cAf6_CONFIGURATION_LT_PRESET_REG_address       0x36
#define cAf6_CONFIGURATION_LT_INIT_REG_address         0x37
#define cAf6_CONFIGURATION_LT_SEED_REG0_address        0x38
#define cAf6_CONFIGURATION_LT_COEFFICIENT_REG0_address 0x39

#define cAf6_STAT_RX_FEC_STATUS_REG_address            0x40
#define cAf6_STAT_RX_FEC_STATUS_fec_rx_lock_Mask       cBit0
#define cAf6_STAT_RX_FEC_STATUS_fec_rx_lock_Shift      0
#define cAf6_STAT_RX_FEC_STATUS_fec_rx_lock_error_Mask cBit16
#define cAf6_STAT_RX_FEC_STATUS_fec_rx_lock_error_Shift 16

#define cAf6_STAT_AN_STATUS_address                    0x41
#define cAf6_stat_an_parallel_detection_fault_Mask     cBit3
#define cAf6_stat_an_parallel_detection_fault_Shift    3
#define cAf6_stat_an_autoneg_complete_Mask             cBit2
#define cAf6_stat_an_autoneg_complete_Shift            2

#define cAf6_STAT_AN_ABILITY_address                   0x42
#define cAf6_STAT_AN_LINK_CTLaddress                   0x43
#define cAf6_STAT_LT_STATUS_REG1_address               0x44
#define cAf6_STAT_LT_STATUS_REG2_address               0x45
#define cAf6_STAT_LT_STATUS_REG3_address               0x46
#define cAf6_STAT_LT_STATUS_REG4_address               0x47
#define cAf6_STAT_LT_COEFFICIENT0_REG_address          0x48

#endif /* _AF6_REG_AF6CNC0011_RD_TOP_H_ */

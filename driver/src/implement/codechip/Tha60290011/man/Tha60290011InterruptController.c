/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60290011InterruptController.c
 *
 * Created Date: Nov 3, 2017
 *
 * Description : 60290011 interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011InterruptController.h"
#include "Tha60290011DeviceInternal.h"
#include "Tha60290011DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_XilinxSerdesBackplane0_IntStatus_Mask |
                          cAf6_global_interrupt_status_XilinxSerdesBackplane1_IntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool PwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (m_ThaIpCoreImplement->PwCauseInterrupt(self, intrStatus))
        return cAtTrue;

    return (intrStatus & cAf6_global_interrupt_status_DCCKIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void EthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self,
                               cAf6_global_interrupt_status_XilinxSerdesBackplane0_IntStatus_Mask |
                               cAf6_global_interrupt_status_XilinxSerdesBackplane1_IntStatus_Mask, enable);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    m_ThaIpCoreImplement->PwHwInterruptEnable(self, enable);
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_status_DCCKIntEnable_Mask, enable);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, CanEnableHwInterruptPin);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        mMethodOverride(m_ThaIpCoreOverride, EthCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EthHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PwCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011InterruptController);
    }

ThaIntrController Tha60290011IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031InterruptControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60290011IntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011IntrControllerObjectInit(newController, ipCore);
    }

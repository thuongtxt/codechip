/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device Management
 *
 * File        : Tha60290011DeviceInternal.h
 *
 * Created Date: Sep 28, 2016 
 *
 * Description : Internal Interface of Product 24 DS3/E3/EC1 or 84 DS1/E1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011DEVICEINTERNAL_H_
#define _THA60290011DEVICEINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/man/Tha60210031Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011Device *Tha60290011Device;

typedef struct tTha60290011DeviceMethods
    {
    uint32 (*PohEnableToHwValue)(AtDevice self, eBool enable);
    eBool (*DccTxFcsMsbIsSupported)(AtDevice self);
    eBool (*DccHdlcFrameTransmissioniBitOderPerChannelIsSupported)(AtDevice self);
    eBool (*JnDdrInitIsSupported)(AtDevice self);
    eBool (*TohSonetSdhModeIsSupported)(AtDevice self);
    eBool (*TwoDot5GitSerdesPrbsIsRemoved)(AtDevice self);
    eBool (*HasStandardClearTime)(AtDevice self);
    eBool (*PwEthPortHasMacFunctionality)(AtDevice self);
    eBool (*RxFsmPinSelV2IsSupported)(AtDevice self);
    eBool (*EthInterruptIsSupported)(AtDevice self);
    eBool (*EthSohTransparentIsReady)(AtDevice self);
    eBool (*OcnSohOverEthIsSupported)(AtDevice self);
    eBool (*OcnVcNewLoopbackIsSupported)(AtDevice self);
    eBool (*OcnTohByteIsSupported)(AtDevice self);
    eBool (*PdaCanControlLopsPktReplaceMode)(AtDevice self);
    uint32 (*StartVersionSupportDe1Ssm)(AtDevice self);
    eBool (*RxHw3BitFeacSignalIsSupported)(AtDevice self);
    uint32 (*StartVersionSupportDe1LomfConsequentialAction)(AtDevice self);
    uint32 (*StartVersionSupportDe1IdleCodeInsertion)(AtDevice self);
    eBool (*De1RetimingIsSupported)(AtDevice self);
    uint32 (*StartVersionHasPrmRxEnabledCfgRegister)(AtDevice self);
    eBool (*IsMroSerdesBackplaneImplemented)(AtDevice self);
    eBool (*JnRequestDdrAndPohCpuDisableIsSupported)(AtDevice self);
    uint32 (*StartVersionSupportSeparateRdiErdiS)(AtDevice self);
    eBool (*PwDccKbyteInterruptIsSupported)(AtDevice self);
    eBool (*SurFailureForwardingStatusIsSupported)(AtDevice self);
    eBool (*SurFailureHoldOnTimerConfigurable)(AtDevice self);
    eBool (*OcnTxDccRsMsInsControlIsSupported)(AtDevice self);
    eBool (*DccEthMaxLengthConfigurationIsSupported)(AtDevice self);
    eBool (*BackplaneSerdesEthPortFecIsSupported)(AtDevice self);
    eBool (*BackplaneSerdesEthPortInterruptIsSupported)(AtDevice self);
    eBool (*EthFecIsSupported)(AtDevice self);
    eBool (*ShouldEnableLosPpmDetection)(Tha60290011Device self);
    }tTha60290011DeviceMethods;

typedef struct tTha60290011Device
    {
    tTha60210031Device super;
    const tTha60290011DeviceMethods *methods;
    }tTha60290011Device;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60290011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);
uint32 Tha60290011SuperDevicePllClkSysStatBitMaskGet(ThaDevice self);
uint32 Tha60290011SuperDeviceDdrCurrentStatusRegister(ThaDevice self);
eAtRet Tha60290011DeviceSubPortVlanEnable(ThaDevice self, eBool enable);
eBool Tha60290011DeviceDccTxFcsMsbIsSupported(AtDevice self);
eBool Tha60290011DeviceDccHdlcFrameTransmissioniBitOderPerChannelIsSupported(AtDevice self);
eBool Tha60290011DeviceJnDdrInitIsSupported(AtDevice self);
eBool Tha60290011DeviceTohSonetSdhModeIsSupported(AtDevice self);
eBool Tha60290011Device2Dot5GitSerdesPrbsIsRemoved(AtDevice self);
eBool Tha60290011DeviceHasStandardClearTime(AtDevice self);
eBool Tha60290011DevicePwEthPortHasMacFunctionality(AtDevice self);
eBool Tha60290011DeviceRxFsmPinSelV2IsSupported(AtDevice self);
eBool Tha60290011DeviceEthInterruptIsSupported(AtDevice self);
eBool Tha60290011DeviceEthSohTransparentIsReady(AtDevice self);
eBool Tha60290011DeviceOcnSohOverEthIsSupported(AtDevice self);
eBool Tha60290011DeviceOcnVcNewLoopbackIsSupported(AtDevice self);
eBool Tha60290011DeviceOcnTohByteIsSupported(AtDevice self);
eBool Tha60290011DevicePdaCanControlLopsPktReplaceMode(AtDevice self);
uint32 Tha60290011DeviceStartVersionSupportDe1Ssm(AtDevice self);
eBool Tha60290011DeviceRxHw3BitFeacSignalIsSupported(AtDevice self);
uint32 Tha60290011DeviceStartVersionSupportDe1LomfConsequentialAction(AtDevice self);
uint32 Tha60290011DeviceStartVersionSupportDe1IdleCodeInsertion(AtDevice self);
eBool Tha60290011DeviceDe1RetimingIsSupported(AtDevice self);
uint32 Tha60290011DeviceStartVersionHasPrmRxEnabledCfgRegister(AtDevice self);
uint32 Tha60290011DeviceStartVersionSupportSeparateRdiErdiS(AtDevice self);
eBool Tha60290011DeviceJnRequestDdrAndPohCpuDisableIsSupported(AtDevice self);
eBool Tha60290011DeviceIsMroSerdesBackplaneImplemented(AtDevice self);
eBool Tha60290011DevicePwDccKbyteInterruptIsSupported(AtDevice self);
eBool Tha60290011DeviceSurFailureForwardingStatusIsSupported(AtDevice self);
eBool Tha60290011DeviceSurFailureHoldOnTimerConfigurable(AtDevice self);
eBool Tha60290011DeviceOcnTxDccRsMsInsControlIsSupported(AtDevice self);
eBool Tha60290011DeviceDccEthMaxLengthConfigurationIsSupported(AtDevice self);
eBool Tha60290011DeviceBackplaneSerdesEthPortFecIsSupported(AtDevice self);
eBool Tha60290011DeviceBackplaneSerdesEthPortInterruptIsSupported(AtDevice self);
eBool Tha60290011DeviceShouldEnableLosPpmDetection(AtDevice self);

void Tha60290011DeviceHwFlushPmFm(ThaDevice self);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011DEVICEINTERNAL_H_ */

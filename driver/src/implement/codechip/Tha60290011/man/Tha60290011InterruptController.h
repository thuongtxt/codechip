/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A290021InterruptControllerInternal.h
 *
 * Created Date: Jul 27, 2015
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011INTERRUPTCONTROLLER_H_
#define _THA60290011INTERRUPTCONTROLLER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/man/Tha60210031InterruptControllerInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011InterruptController
    {
    tTha60210031InterruptController super;
    }tTha60290011InterruptController;
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha60290011IntrControllerNew(AtIpCore core);
ThaIntrController Tha60290011IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011INTERRUPTCONTROLLER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290011ModulePrbs.c
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : implementation for PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290011ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods      m_AtModuleOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "28 engines";
    }

static uint32 MaxNumEngines(AtModulePrbs self)
    {
    AtUnused(self);
    return 28;
    }

static AtPrbsEngine De3LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static eBool IsPrbsV2Used(void)
    {
    return cAtFalse;
    }

static eBool IsPrbsV2Supported(ThaModulePrbs self)
    {
    ThaVersionReader reader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(reader);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0);

    if (currentVersion >= supportedVersion)
        return cAtTrue;
    return cAtFalse;
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    if (IsPrbsV2Used() && IsPrbsV2Supported(self))
        return Tha60290011PrbsRegProviderV2();

    return Tha60290011PrbsRegProvider();
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPw pw)
    {
    AtUnused(self);
    return Tha60210021PwPrbsEngineNew(pw, engineId);
    }

static AtPrbsEngine SdhLinePrbsEngineCreate(AtModulePrbs self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static eBool ShouldInvalidateStatusWhenRxIsDisabled(AtModulePrbs self)
    {
    /* As this is required by customer */
    AtUnused(self);
    return cAtFalse;
    }

static eBool FlexiblePsnSide(ThaModulePrbs self)
    {
    return Tha602900xxFlexiblePsnSide(self);
    }

static eBool ChannelizedPrbsAllowed(AtModulePrbs self)
    {
    return Tha602900xxModulePrbsChannelizedPrbsAllowed(self);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De3LinePrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, MaxNumEngines);
        mMethodOverride(m_AtModulePrbsOverride, SdhLinePrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, ShouldInvalidateStatusWhenRxIsDisabled);
        mMethodOverride(m_AtModulePrbsOverride, ChannelizedPrbsAllowed);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, mMethodsGet(self), sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, FlexiblePsnSide);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs((ThaModulePrbs)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePrbs);
    }

AtModulePrbs Tha60290011ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60290011ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePrbsObjectInit(newModule, device);
    }


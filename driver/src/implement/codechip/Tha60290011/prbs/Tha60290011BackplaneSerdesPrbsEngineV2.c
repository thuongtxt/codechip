/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290011BackplaneSerdesPrbsEngineV2.c
 *
 * Created Date: Nov 9, 2017
 *
 * Description : Backplane serdes prbs engine version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../Tha60290021/prbs/Tha60290021BackplaneSerdesPrbsEngineInternal.h"
#include "../../Tha60290021/prbs/gating/Tha60290021PrbsGatingTimer.h"
#include "Tha60290011XfiSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_TXPKT_Base    0x2022
#define cAf6Reg_ETH_10G_Diag_TXNOB_Base    0x2024
#define cAf6Reg_ETH_10G_Diag_RXPKT_Base    0x20C2
#define cAf6Reg_ETH_10G_Diag_RXNOB_Base    0x20C4
#define cAf6Reg_ETH_10G_Diag_RXPKTERR_Base 0x20C6

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011BackplaneSerdesPrbsEngineV2
    {
    tTha60290021BackplaneSerdesPrbsEngine super;
    }tTha60290011BackplaneSerdesPrbsEngineV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtPrbsEngine self)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    return AtSerdesControllerBaseAddress(controller);
    }

static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return BaseAddress(self) + localAddress;
    }

static uint32 CounterLocalAddress(uint16 counterType, eBool read2Clear)
    {
    eBool readOnly = !read2Clear;

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame: return (readOnly ? cAf6Reg_ETH_10G_Diag_TXPKT_Base : (cAf6Reg_ETH_10G_Diag_TXPKT_Base + 1));
        case cAtPrbsEngineCounterRxFrame: return (readOnly ? cAf6Reg_ETH_10G_Diag_RXPKT_Base : (cAf6Reg_ETH_10G_Diag_RXPKT_Base + 1));
        case cAtPrbsEngineCounterTxBit  : return (readOnly ? cAf6Reg_ETH_10G_Diag_TXNOB_Base : (cAf6Reg_ETH_10G_Diag_TXNOB_Base + 1));
        case cAtPrbsEngineCounterRxBit  : return (readOnly ? cAf6Reg_ETH_10G_Diag_RXNOB_Base : (cAf6Reg_ETH_10G_Diag_RXNOB_Base + 1));
        case cAtPrbsEngineCounterRxErrorFrame  : return (readOnly ? cAf6Reg_ETH_10G_Diag_RXPKTERR_Base : (cAf6Reg_ETH_10G_Diag_RXPKTERR_Base + 1));
        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, CounterLocalAddress(counterType, read2Clear));
    uint32 regVal;

    if (regAddr == cInvalidUint32)
        return 0;

    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    if ((counterType == cAtPrbsEngineCounterTxBit) ||
        (counterType == cAtPrbsEngineCounterRxBit))
        regVal = regVal * 8;

    return regVal;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    return (CounterLocalAddress(counterType, cAtTrue) == cInvalidUint32) ? cAtFalse : cAtTrue;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    return Tha60290021PrbsGatingTimerBackplaneSerdesNew(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011BackplaneSerdesPrbsEngineV2);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021BackplaneSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290011BackplaneSerdesPrbsEngineV2New(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290011PrbsRegProviderInternal.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : 60290011 PRBS reg provider internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PRBSREGPROVIDERINTERNAL_H_
#define _THA60290011PRBSREGPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/prbs/Tha60210031PrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011PrbsRegProvider
    {
    tTha60210031PrbsRegProviderV2 super;
    }tTha60290011PrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPrbsRegProvider Tha60290011PrbsRegProviderObjectInit(ThaPrbsRegProvider self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011PRBSREGPROVIDERINTERNAL_H_ */


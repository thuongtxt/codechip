/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290011SgmiiSerdesPrbsEngine.c
 *
 * Created Date: Oct 22, 2016
 *
 * Description : SGMII PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../Tha60290021/prbs/Tha6029SgmiiSerdesPrbsEngineInternal.h"
#include "../../Tha60290021/prbs/Tha60290021ModulePrbs.h"
#include "Tha60290011PrbsEngineGatingTimer.h"
#include "Tha60290011SerdesMIIDiagReg.h"
#include "AtSerdesController.h"
#include "Tha60290011SgmiiSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011SgmiiSerdesPrbsEngine *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011SgmiiSerdesPrbsEngine
    {
    tTha6029SerdesPrbsEngine super;

    /* Private data */
    uint32 hwId;
    eBool gatingLogicEnabled;
    }tTha60290011SgmiiSerdesPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0xF53000;
    }

static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return (mThis(self)->hwId * 0x000200) + BaseAddress(self) + localAddress;
    }

static uint32 MiiEthTestControlAddress(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cReg_MiiEthTestControl_Base);
    }

static uint32 EthPacketConfigAddress(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cReg_EthPacketConfig_Base);
    }

static uint32 MiiStatusGlbAddress(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cReg_MiiStatusGlb_Base);
    }

static uint32 CounterLocalAddress(uint16 counterType, eBool r2c)
    {
    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame   : return r2c ? cReg_itxpkr2c_Base : cReg_counter4_Base;
        case cAtPrbsEngineCounterRxFrame   : return r2c ? cReg_counter2_Base : cReg_counter5_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint32 MiiAlarmGlbAddress(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cReg_MiiAlarmGlb_Base);
    }

static eAtModulePrbsRet HwEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = MiiEthTestControlAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_MiiEthTestControl_test_en_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = cAtOk;

    /* When enable, it is required to toggle this attribute */
    if (enable)
        {
        ret |= HwEnable(self, cAtFalse);
        AtOsalUSleep(1000);
        }

    ret |= HwEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = MiiEthTestControlAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & c_MiiEthTestControl_test_en_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet TestInit(AtPrbsEngine self)
    {
    uint32 regAddr = EthPacketConfigAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mRegFieldSet(regVal, c_EthPacketConfig_icfgpat_, 0x55); /* Fix-pattern 0x55 */
    mRegFieldSet(regVal, c_EthPacketConfig_ibandwidth_,  0); /* 100% bandwidth */
    mRegFieldSet(regVal, c_EthPacketConfig_idatamod_, 0);  /* PRBS-15*/
    mRegFieldSet(regVal, c_EthPacketConfig_ipklen_, 256); /* 256 bytes */

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = MiiEthTestControlAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_MiiEthTestControl_iforceerr_, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = MiiEthTestControlAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & c_MiiEthTestControl_iforceerr_Mask) ? cAtTrue : cAtFalse;
    }

static eBool ErrorForcingIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ModeSw2Hw(eAtPrbsMode hwMode)
    {
    switch ((uint32)hwMode)
        {
        case cAtPrbsModePrbs7 : return 0;
        case cAtPrbsModePrbs15: return 1;
        case cAtPrbsModePrbs23: return 2;
        case cAtPrbsModePrbs31: return 3;
        default:
            return cInvalidUint32;
        }
    }

static eAtPrbsMode ModeHw2Sw(uint32 hwMode)
    {
    switch (hwMode)
        {
        case 0: return cAtPrbsModePrbs7 ;
        case 1: return cAtPrbsModePrbs15;
        case 2: return cAtPrbsModePrbs23;
        case 3: return cAtPrbsModePrbs31;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet HwModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = EthPacketConfigAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_EthPacketConfig_idatamod_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (AtPrbsEngineModeIsSupported(self, prbsMode))
        return HwModeSet(self, prbsMode);
    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = EthPacketConfigAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return ModeHw2Sw(mRegField(regVal, c_EthPacketConfig_idatamod_));
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr = EthPacketConfigAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, c_EthPacketConfig_icfgpat_, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr = EthPacketConfigAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return mRegField(regVal, c_EthPacketConfig_icfgpat_);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if (ModeSw2Hw(prbsMode) == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static eAtModulePrbsRet DefaultSet(AtPrbsEngine self)
    {
    eAtRet ret = cAtOk;

    ret |= TestInit(self);
    ret |= AtPrbsEngineModeSet(self, cAtPrbsModePrbs31);

    return ret;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static AtPrbsEngine PrbsEngine(AtSerdesController self)
    {
	if (self)
		return AtSerdesControllerPrbsEngine(self);
    return NULL;
    }

static uint32 LinkAlarm(AtPrbsEngine self)
    {
    uint32 regAddr = MiiStatusGlbAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & c_MiiAlarmGlb_link_sta_Mask) ? cAtSerdesAlarmTypeLinkDown:cAtSerdesAlarmTypeNone;
    }

static uint32 LinkAlarmHistory(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddr = MiiAlarmGlbAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

	if (!(regVal & c_MiiAlarmGlb_link_sta_Mask))
        return cAtSerdesAlarmTypeNone;

	if (read2Clear)
    	AtPrbsEngineWrite(self, regAddr, c_MiiAlarmGlb_link_sta_Mask, cAtModulePrbs);

	return cAtSerdesAlarmTypeLinkDown;
    }

static uint32 StickyRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddr = MiiAlarmGlbAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    if (read2Clear)
        {
		regVal &= ~ c_MiiAlarmGlb_link_sta_Mask; /* Do not clear Link Sticky */
    	AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return regVal;
    }

static uint32 ErrorMask(void)
    {
	return (c_MiiAlarmGlb_odaterr_Mask |
			c_MiiAlarmGlb_olenerr_Mask |
			c_MiiAlarmGlb_opkterr_Mask);
    }

static uint32 LossSyncMask(void)
    {
	return (c_MiiAlarmGlb_odaterr_Mask | c_MiiAlarmGlb_link_sta_Mask);
    }

static uint32 AlarmHw2Sw(uint32 hwAlarms)
    {
    uint32 alarms = 0;

    if (hwAlarms & ErrorMask())
        alarms |= cAtPrbsEngineAlarmTypeError;

    if (hwAlarms & LossSyncMask())
        alarms |= cAtPrbsEngineAlarmTypeLossSync;

    return alarms;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    return AlarmHw2Sw(StickyRead2Clear(self, read2Clear));
    }

static void HwAlarmDetailShow(AtPrbsEngine self, uint32 hwAlarm, const char *title)
    {
    AtUnused(self);

    AtPrintc(cSevNormal, "* %s: ", title);
    if ((hwAlarm & (ErrorMask() | LossSyncMask())) == 0)
        {
        AtPrintc(cSevInfo, "%s\r\n", "none");
        return;
        }

    if (hwAlarm & c_MiiAlarmGlb_opkterr_Mask)
        AtPrintc(cSevCritical, "packet_error ");
    if (hwAlarm & c_MiiAlarmGlb_olenerr_Mask)
        AtPrintc(cSevCritical, "packet_len_error ");
    if (hwAlarm & c_MiiAlarmGlb_odaterr_Mask)
        AtPrintc(cSevCritical, "data_error ");
    if (hwAlarm & c_MiiAlarmGlb_link_sta_Mask)
        AtPrintc(cSevCritical, "Link down");
    }

static uint32 HwAlarmGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRead(self, MiiStatusGlbAddress(self), cAtModulePrbs);
    }

static const char *BandwidthString(uint32 hwValue)
    {
    switch (hwValue)
        {
        case 0: return "100%";
        case 1: return "50%";
        case 2: return "75%";
        default: return "unknown";
        }
    }

static void HwConfigureShow(AtPrbsEngine self)
    {
    uint32 regAddr = EthPacketConfigAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    AtPrintc(cSevNormal, "* HW configure detail: \r\n");
    AtPrintc(cSevNormal, "  - Bandwidth: %s\r\n", BandwidthString(mRegField(regVal, c_EthPacketConfig_ibandwidth_)));
    AtPrintc(cSevNormal, "  - Payload: %d (bytes)\r\n", mRegField(regVal, c_EthPacketConfig_ipklen_));
    }

static void Debug(AtPrbsEngine self)
    {
    m_AtPrbsEngineMethods->Debug(self);

    HwConfigureShow(self);
    HwAlarmDetailShow(self, StickyRead2Clear(self, cAtTrue), "Sticky");
    HwAlarmDetailShow(self, HwAlarmGet(self), "Alarm");
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return AlarmHw2Sw(HwAlarmGet(self));
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (CounterLocalAddress(counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool read2Clear)
    {
    uint32 localAddr = CounterLocalAddress(counterType, read2Clear);
    if (localAddr == cInvalidUint32)
        return 0;
    return AtPrbsEngineRead(self, AddressWithLocalAddress(self, localAddr), cAtModulePrbs);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eAtRet GatingLogicEnabled(AtPrbsEngine self, eBool enabled)
    {
    mThis(self)->gatingLogicEnabled = enabled;
    return cAtOk;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    if (mThis(self)->gatingLogicEnabled)
        return Tha60290011SgmiiSerdesPrbsEngineGatingTimerNew(self);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290011SgmiiSerdesPrbsEngine* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(hwId);
    mEncodeUInt(gatingLogicEnabled);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SgmiiSerdesPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController, uint32 hwId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    mThis(self)->hwId = hwId;

    return self;
    }

AtPrbsEngine Tha60290011SgmiiSerdesPrbsEngineWithHwIdNew(AtSerdesController serdesController, uint32 hwId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController, hwId);
    }

uint32 Tha60290011SgmiiSerdesLinkAlarmHistoryGet(AtSerdesController self, eBool r2c)
    {
	AtPrbsEngine engine = PrbsEngine(self);
	if (engine)
		return LinkAlarmHistory(engine, r2c);
	return cAtSerdesAlarmTypeNone;
    }

uint32 Tha60290011SgmiiSerdesLinkAlarmGet(AtSerdesController self)
    {
	AtPrbsEngine engine = PrbsEngine(self);
	if (engine)
		return LinkAlarm(engine);
	return cAtSerdesAlarmTypeNone;
    }

eAtRet Tha60290011SgmiiSerdesPrbsEngineGatingLogicEnabled(AtPrbsEngine self, eBool enabled)
    {
    if (self)
        return GatingLogicEnabled(self, enabled);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60290011SgmiiSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    if (self)
        return AddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290011SgmiiSerdesPrbsEngine.h
 * 
 * Created Date: Apr 6, 2017
 *
 * Description : SGMII PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SGMIISERDESPRBSENGINE_H_
#define _THA60290011SGMIISERDESPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290011SgmiiSerdesPrbsEngineWithHwIdNew(AtSerdesController serdesController, uint32 hwId);

eAtRet Tha60290011SgmiiSerdesPrbsEngineGatingLogicEnabled(AtPrbsEngine self, eBool enabled);
uint32 Tha60290011SgmiiSerdesLinkAlarmHistoryGet(AtSerdesController self, eBool r2c);
uint32 Tha60290011SgmiiSerdesLinkAlarmGet(AtSerdesController self);
uint32 Tha60290011SgmiiSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SGMIISERDESPRBSENGINE_H_ */


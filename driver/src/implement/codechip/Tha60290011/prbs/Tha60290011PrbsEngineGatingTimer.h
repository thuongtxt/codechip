/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290011PrbsEngineGatingTimer.h
 * 
 * Created Date: Apr 8, 2017
 *
 * Description : Gating timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PRBSENGINEGATINGTIMER_H_
#define _THA60290011PRBSENGINEGATINGTIMER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTimer Tha60290011SgmiiSerdesPrbsEngineGatingTimerNew(AtPrbsEngine engine);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011PRBSENGINEGATINGTIMER_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef __REG_ES_MII_RD_DIAG_H_
#define __REG_ES_MII_RD_DIAG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : MiiAlarmGlb
Reg Addr   : 0x00
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status of packet diagnostic

------------------------------------------------------------------------------*/
#define cReg_MiiAlarmGlb_Base                                                                             0x00

/*--------------------------------------
BitField Name: opkterr
BitField Type: R1W
BitField Desc: Packet Error
BitField Bits: [12:12]
--------------------------------------*/
#define c_MiiAlarmGlb_opkterr_Mask                                                                      cBit12
#define c_MiiAlarmGlb_opkterr_Shift                                                                         12

/*--------------------------------------
BitField Name: olenerr
BitField Type: R1W
BitField Desc: Packet Len Error
BitField Bits: [08:08]
--------------------------------------*/
#define c_MiiAlarmGlb_olenerr_Mask                                                                       cBit8
#define c_MiiAlarmGlb_olenerr_Shift                                                                          8

/*--------------------------------------
BitField Name: odaterr
BitField Type: R1W
BitField Desc: PRBS payload Data Error ( Not Syn)
BitField Bits: [04:04]
--------------------------------------*/
#define c_MiiAlarmGlb_odaterr_Mask                                                                       cBit4
#define c_MiiAlarmGlb_odaterr_Shift                                                                          4

/*--------------------------------------
BitField Name: link_sta
BitField Type: R1W
BitField Desc: PHY Link up status
BitField Bits: [00:00]
--------------------------------------*/
#define c_MiiAlarmGlb_link_sta_Mask                                                                      cBit0
#define c_MiiAlarmGlb_link_sta_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MiiStatusGlb
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status of packet diagnostic

------------------------------------------------------------------------------*/
#define cReg_MiiStatusGlb_Base                                                                            0x10

/*--------------------------------------
BitField Name: opkterr
BitField Type: RO
BitField Desc: Packet Error
BitField Bits: [12:12]
--------------------------------------*/
#define c_MiiStatusGlb_opkterr_Mask                                                                     cBit12
#define c_MiiStatusGlb_opkterr_Shift                                                                        12

/*--------------------------------------
BitField Name: olenerr
BitField Type: RO
BitField Desc: Packet Len Error
BitField Bits: [08:08]
--------------------------------------*/
#define c_MiiStatusGlb_olenerr_Mask                                                                      cBit8
#define c_MiiStatusGlb_olenerr_Shift                                                                         8

/*--------------------------------------
BitField Name: odaterr
BitField Type: RO
BitField Desc: PRBS payload Data Error ( Not Syn)
BitField Bits: [04:04]
--------------------------------------*/
#define c_MiiStatusGlb_odaterr_Mask                                                                      cBit4
#define c_MiiStatusGlb_odaterr_Shift                                                                         4

/*--------------------------------------
BitField Name: link_sta
BitField Type: RO
BitField Desc: PHY Link up status
BitField Bits: [00:00]
--------------------------------------*/
#define c_MiiStatusGlb_link_sta_Mask                                                                     cBit0
#define c_MiiStatusGlb_link_sta_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : EthPacketConfig
Reg Addr   : 0x01
Reg Formula: 
    Where  : 
Reg Desc   : 
Configuration Packet Diagnostic,

------------------------------------------------------------------------------*/
#define cReg_EthPacketConfig_Base                                                                         0x01

/*--------------------------------------
BitField Name: icfgpat
BitField Type: R/W
BitField Desc: Data fix value for  idatamod = 0x2
BitField Bits: [27:20]
--------------------------------------*/
#define c_EthPacketConfig_icfgpat_Mask                                                               cBit27_20
#define c_EthPacketConfig_icfgpat_Shift                                                                     20

/*--------------------------------------
BitField Name: ibandwidth
BitField Type: R/W
BitField Desc: Band Width
BitField Bits: [17:16]
--------------------------------------*/
#define c_EthPacketConfig_ibandwidth_Mask                                                            cBit17_16
#define c_EthPacketConfig_ibandwidth_Shift                                                                  16

/*--------------------------------------
BitField Name: idatamod
BitField Type: R/W
BitField Desc: Payload Data Mode 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31
BitField Bits: [14:12]
--------------------------------------*/
#define c_EthPacketConfig_idatamod_Mask                                                              cBit14_12
#define c_EthPacketConfig_idatamod_Shift                                                                    12

/*--------------------------------------
BitField Name: ipklen
BitField Type: R/W
BitField Desc: Packet len
BitField Bits: [11:00]
--------------------------------------*/
#define c_EthPacketConfig_ipklen_Mask                                                                 cBit11_0
#define c_EthPacketConfig_ipklen_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MiiEthTestControl
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 
Configuration Diagnostic,

------------------------------------------------------------------------------*/
#define cReg_MiiEthTestControl_Base                                                                       0x03

/*--------------------------------------
BitField Name: start_diag
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [2]
--------------------------------------*/
#define c_MiiEthTestControl_start_diag_Mask                                                              cBit2
#define c_MiiEthTestControl_start_diag_Shift                                                                 2

/*--------------------------------------
BitField Name: iforceerr
BitField Type: R/W
BitField Desc: PRBS Data Error Insert
BitField Bits: [01:01]
--------------------------------------*/
#define c_MiiEthTestControl_iforceerr_Mask                                                               cBit1
#define c_MiiEthTestControl_iforceerr_Shift                                                                  1

/*--------------------------------------
BitField Name: test_en
BitField Type: R/W
BitField Desc: PRBS test Enable
BitField Bits: [00:00]
--------------------------------------*/
#define c_MiiEthTestControl_test_en_Mask                                                                 cBit0
#define c_MiiEthTestControl_test_en_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : gatetime config value
Reg Addr   : 0x04
Reg Formula:
    Where  :
Reg Desc   :
This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4

------------------------------------------------------------------------------*/
#define cReg_gatetime_cfg_Base                                                                            0x04

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:00]
--------------------------------------*/
#define c_gatetime_cfg_time_cfg_Mask                                                                  cBit16_0
#define c_gatetime_cfg_time_cfg_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Gatetime Current
Reg Addr   : 0x05
Reg Formula:
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cReg_Gatetime_current_Base                                                                        0x05

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic port 16 to port1 bit per port
1:Running 0:Done-Ready
BitField Bits: [17]
--------------------------------------*/
#define c_Gatetime_current_status_gatetime_diag_Mask                                                    cBit17
#define c_Gatetime_current_status_gatetime_diag_Shift                                                       17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define c_Gatetime_current_currert_gatetime_diag_Mask                                                 cBit16_0
#define c_Gatetime_current_currert_gatetime_diag_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MiiTxCounterR2C.
Reg Addr   : 0x0C
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_itxpkr2c_Base                                                                                0x0C

/*--------------------------------------
BitField Name: Tx_counter_R2C
BitField Type: R2C
BitField Desc: Tx Packet counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define c_itxpkr2c_Tx_counter_R2C_Mask                                                                cBit31_0
#define c_itxpkr2c_Tx_counter_R2C_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : MiiRxCounterR2C..
Reg Addr   : 0x0D
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_counter2_Base                                                                                0x0D

/*--------------------------------------
BitField Name: Rx_counter_R2C
BitField Type: R2C
BitField Desc: Rx counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define c_counter2_Rx_counter_R2C_Mask                                                                cBit31_0
#define c_counter2_Rx_counter_R2C_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : MiiTxCounterRO.
Reg Addr   : 0x08
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_counter4_Base                                                                                0x08

/*--------------------------------------
BitField Name: Tx_counter_RO
BitField Type: RO
BitField Desc: Tx counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define c_counter4_Tx_counter_RO_Mask                                                                 cBit31_0
#define c_counter4_Tx_counter_RO_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : MiiRxCounterRO.
Reg Addr   : 0x09
Reg Formula: 
    Where  : 
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cReg_counter5_Base                                                                                0x09

/*--------------------------------------
BitField Name: Rx_counter_RO
BitField Type: RO
BitField Desc: Rx counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define c_counter5_Rx_counter_RO_Mask                                                                 cBit31_0
#define c_counter5_Rx_counter_RO_Shift                                                                       0

#endif /* __REG_ES_MII_RD_DIAG_H_ */

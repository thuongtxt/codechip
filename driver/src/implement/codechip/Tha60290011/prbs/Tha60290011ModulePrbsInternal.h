/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290011ModulePrbsInternal.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : Initernal data of 60290011 module PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPRBSINTERNAL_H_
#define _THA60290011MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsEngine.h"
#include "../../../default/prbs/ThaPrbsRegProvider.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../../Tha60210011/prbs/Tha60210011ModulePrbsInternal.h"
#include "../../Tha60210031/prbs/Tha60210031ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePrbs
    {
    tTha60210031ModulePrbs super;
    }tTha60290011ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha60290011ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEPRBSINTERNAL_H_ */


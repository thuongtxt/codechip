/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS module name
 * 
 * File        : Tha60290011XfiSerdesPrbsEngine.h
 * 
 * Created Date: Aug 5, 2017
 *
 * Description : 60290011 xfi prbs engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011XFISERDESPRBSENGINE_H_
#define _THA60290011XFISERDESPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290011XfiSerdesPrbsEngineNew(AtSerdesController serdesController, uint32 baseAddress);
AtPrbsEngine Tha60290011BackplaneSerdesPrbsEngineV2New(AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011XFISERDESPRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290011PrbsRegProvider.c
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : implementation for Prbs Register Provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
 
/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011PrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* BASE */
#define cBertTdmGenBaseAddress                       (0x350000UL)
#define cBertTdmMonBaseAddress                       (0x360000UL)
#define cBertPsnMonBaseAddress                       (0x370000UL)

/* GEN */
#define cThaRegMapBERTGenErrorRateInsert             (0x83C0 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenFixedPatternControl         (0x8320 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenSingleBitErrInsert          (0x83FE + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenTxPtgErrIDInsMask           cBit4_0

/* MON */
#define cThaRegMapBERTMonSelectedChannel             (0x8500 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonMode                        (0x8420 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonFixedPatternControl         (0x8440 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonStatus                      (0x84E0 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonErrorCounter                (0x8540 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonGoodBitCounter              (0x8580 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonLossBitCounter              (0x85C0 + cBertPsnMonBaseAddress)

/* MON PW */
#define cThaRegDemapBERTMonSelectedChannel           (0x8500 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonMode                      (0x8420 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonFixedPatternControl       (0x8440 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonStatus                    (0x84E0 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonErrorCounter              (0x8540 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonGoodBitCounter            (0x8580 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonLossBitCounter            (0x85C0 + cBertTdmMonBaseAddress)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTGenFixedPatternControl + engineId;
    }

static uint32 RegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit10;
    }

static uint32 RegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit9_0;
    }

static uint32 RegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTGenErrorRateInsert + engineId;
    }

static uint32 RegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSingleBitErrInsert + engineId;
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonSelectedChannel + engineId;
    }

static uint32 RegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonMode + engineId;
    }

static uint32 RegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonFixedPatternControl + engineId;
    }

static uint32 RegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonStatus + engineId;
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonErrorCounter + engineId;
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonGoodBitCounter + engineId;
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonLossBitCounter + engineId;
    }

static uint32 RegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonSelectedChannel + engineId;
    }

static uint32 RegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonMode + engineId;
    }

static uint32 RegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonFixedPatternControl + engineId;
    }

static uint32 RegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonErrorCounter + engineId;
    }

static uint32 RegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonGoodBitCounter + engineId;
    }

static uint32 RegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonLossBitCounter + engineId;
    }

static uint32 RegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonStatus + engineId;
    }

static uint32 RegMapBERTGenTxSingleErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxPtgErrIDInsMask;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgIdMask);

        /* GEN */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenErrorRateInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSingleBitErrInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxSingleErrMask);

        /* MON PW */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounter);

        /* MON TDM */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStatus);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011PrbsRegProvider);
    }

ThaPrbsRegProvider Tha60290011PrbsRegProviderObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PrbsRegProviderV2ObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60290011PrbsRegProvider(void)
    {
    static tTha60290011PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = Tha60290011PrbsRegProviderObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

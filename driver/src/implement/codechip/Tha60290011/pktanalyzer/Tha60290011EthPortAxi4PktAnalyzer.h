/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PktAnalyzer
 *
 * File        : Tha60290011EthPortAxia4PktAnalyzer.h
 *
 * Created Date: Aug 24, 2017
 *
 * Description : Axia4 packet analyzer internal data
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA60290011ETHPORTAXI4PKTANALYZER_H_
#define _THA60290011ETHPORTAXI4PKTANALYZER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60150011/pktanalyzer/Tha60150011EthPortPktAnalyzerInternal.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011EthPortAxi4PktAnalyzer
    {
    tTha60150011EthPortPktAnalyzer super;
    }tTha60290011EthPortAxi4PktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtPktAnalyzer Tha60290011EthPortAxi4PktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011ETHPORTAXI4PKTANALYZER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60290011ModulePktAnalyzer.c
 *
 * Created Date: Oct 14, 2016
 *
 * Description : Packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pktanalyzer/ThaPktAnalyzerInternal.h"
#include "../../Tha60150011/pktanalyzer/Tha60150011ModulePktAnalyzerInternal.h"
#include "Tha60290011ModulePktAnalyzer.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011ModulePktAnalyzer *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011ModulePktAnalyzer
    {
    tTha60150011ModulePktAnalyzer super;

    /* Private data */
    ThaPktAnalyzer clsPktAnalyzer;
    }tTha60290011ModulePktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtModulePktAnalyzerMethods m_AtModulePktAnalyzerOverride;

/* Super's implementation */
static const tAtObjectMethods       *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DumpModeWithPreambleSw2Hw(uint32 pktDumpMode)
    {
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return 0;
    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        return 1;

    return 0;
    }

static uint32 DccPortDumpModeWithDASw2Hw(uint32 pktDumpMode)
    {
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return 2;
    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        return 3;

    return 0;
    }

static uint32 DumpModeSw2Hw(uint32 pktDumpMode, eBool dumpAtMacUser)
    {
    if (dumpAtMacUser)
        return DccPortDumpModeWithDASw2Hw(pktDumpMode);
    return DumpModeWithPreambleSw2Hw(pktDumpMode);
    }

static uint8 TwoDotFiveMacClsPktTypeSw2Hw(uint32 pktDumpMode)
    {
    if (pktDumpMode == cClsPktAnalyzerRx)       return 0;
    if (pktDumpMode == cClsPktAnalyzerDs1Tx)    return 1;
    if (pktDumpMode == cClsPktAnalyzerDs3Tx)    return 2;
    if (pktDumpMode == cClsPktAnalyzerControlTx)    return 3;

    return 0;
    }

static uint32 Sw2HwDumpMode(uint32 hwDumpBlock, uint32 swDumpMode)
    {
    if (hwDumpBlock == cBlockType2Dot5Mac)
        return DumpModeSw2Hw(swDumpMode, cAtTrue);

    if (hwDumpBlock == cBlockType1GitDcc)
        return DumpModeSw2Hw(swDumpMode, cAtTrue);

    if (hwDumpBlock == cBlockType1GitDim)
        return DumpModeSw2Hw(swDumpMode, cAtTrue);

    if (hwDumpBlock == cBlockType2Dot5Cls)
        return TwoDotFiveMacClsPktTypeSw2Hw(swDumpMode);

    return 0;
    }

static uint32 DumpModeHw2Sw(uint32 pktDumpMode)
    {
    if ((pktDumpMode == 0) || (pktDumpMode == 2)) return cThaPktAnalyzerDumpGbeTx;
    if ((pktDumpMode == 1) || (pktDumpMode == 3)) return cThaPktAnalyzerDumpGbeRx;

    return cThaPktAnalyzerDumpGbeTx;
    }

static uint32 TwoDotFiveMacClsHw2SwDumpMode(uint32 hwDumpMode)
    {
    if (hwDumpMode == 0)
        return cClsPktAnalyzerRx;
    if (hwDumpMode == 1)
        return cClsPktAnalyzerDs1Tx;
    if (hwDumpMode == 2)
        return cClsPktAnalyzerDs3Tx;
    if (hwDumpMode == 3)
        return cClsPktAnalyzerControlTx;

    return cClsPktAnalyzerRx;
    }

static uint32 Hw2SwDumpMode(uint32 hwDumpBlock, uint32 hwDumpMode)
    {
    if (hwDumpBlock == cBlockType2Dot5Mac)
        return DumpModeHw2Sw(hwDumpMode);

    if (hwDumpBlock == cBlockType1GitDcc)
        return DumpModeHw2Sw(hwDumpMode);

    if (hwDumpBlock == cBlockType1GitDim)
        return DumpModeHw2Sw(hwDumpMode);

    if (hwDumpBlock == cBlockType2Dot5Cls)
        return TwoDotFiveMacClsHw2SwDumpMode(hwDumpMode);

    return cThaPktAnalyzerDumpGbeTx;
    }

static void HwDumpModeMaskShift(uint32 hwDumpBlock, uint32 *mask, uint32 *shift)
    {
    if (hwDumpBlock == cBlockType2Dot5Mac)
        {
        *mask = cThaGlobalConfigPktType2Dot5Mask;
        *shift = cThaGlobalConfigPktType2Dot5Shift;
        return;
        }

    if (hwDumpBlock == cBlockType1GitDcc)
        {
        *mask = cThaGlobalConfigPktTypeDccMask;
        *shift = cThaGlobalConfigPktTypeDccShift;
        return;
        }

    if (hwDumpBlock == cBlockType1GitDim)
        {
        *mask = cThaGlobalConfigPktTypeDimMask;
        *shift = cThaGlobalConfigPktTypeDimShift;
        return;
        }

    if (hwDumpBlock == cBlockType2Dot5Cls)
        {
        *mask = cThaGlobalConfigPktTypeClsMask;
        *shift = cThaGlobalConfigPktTypeClsShift;
        return;
        }

    *mask = 0;
    *shift = 0;
    }

static uint32 SwDumpModeGet(AtChannel self)
    {
    uint32 regAddr, regVal, hwDumpMode, hwDumpBlock;

    regAddr = cThaGlobalConfigSelBlockReg;
    regVal = mChannelHwRead(self, regAddr, cAtModulePktAnalyzer);
    hwDumpBlock = mRegField(regVal, cThaGlobalConfigSelBlockType);

    regAddr = cThaGlobalConfigPktTypeReg;
    regVal = mChannelHwRead(self, regAddr, cAtModulePktAnalyzer);
    hwDumpMode = mRegField(regVal, cThaGlobalConfigPktTypeCls);

    return Hw2SwDumpMode(hwDumpBlock, hwDumpMode);
    }

static void HwDumpModeSet(AtChannel self, uint32 hwblock, uint32 swDumpMode, eBool isUsedHwBlock, eBool isUsedSwDumMode)
    {
    uint32 regAddr, regVal, mask, shift, hwDumpMode;

    if (isUsedSwDumMode)
        {
        hwDumpMode = Sw2HwDumpMode(hwblock, swDumpMode);
        HwDumpModeMaskShift(hwblock, &mask, &shift);
        regAddr = cThaGlobalConfigPktTypeReg;
        regVal = mChannelHwRead(self, regAddr, cAtModulePktAnalyzer);
        mFieldIns(&regVal, mask, shift, hwDumpMode);
        mChannelHwWrite(self, regAddr, regVal, cAtModulePktAnalyzer);
        }

    if (isUsedHwBlock)
        {
        regAddr = cThaGlobalConfigSelBlockReg;
        regVal = mChannelHwRead(self, regAddr, cAtModulePktAnalyzer);
        mRegFieldSet(regVal, cThaGlobalConfigSelBlockType, hwblock);
        mChannelHwWrite(self, regAddr, regVal, cAtModulePktAnalyzer);
        }
    }

static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
    if (Tha60290011ModuleEthIsDimSgmiiPort(port))
        return Tha60290011EthPortDimControlPktAnalyzerNew(port);

    if (Tha60290011ModuleEthIsDccSgmiiPort(port))
        {
        AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)port);
        if (Tha60290011ModuleEthDccIsEnabled(ethModule))
            return Tha60290011EthPortDccSgmiiPktAnalyzerV2New(port);
        return Tha60290011EthPortDccSgmiiPktAnalyzerNew(port);
        }

    if (Tha60290011ModuleEthIsAxi4EthPort(port))
        {
        if (mThis(self)->clsPktAnalyzer == NULL)
            mThis(self)->clsPktAnalyzer = (ThaPktAnalyzer)Tha60290011ClsPktAnalyzerNew(port);

        return Tha60290011EthPortAxi4PktAnalyzerNew(port);
        }

    if (Tha60290011ModuleEthIsPwPort(port))
        return Tha60290011EthPortPktAnalyzerNew(port);

    return NULL;
    }

static AtEthPort DccSgmiiPort(AtModulePktAnalyzer self)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    return Tha60290011ModuleEthSgmiiDccPortGet(moduleEth);
    }

static AtPktAnalyzer HdlcChannelPktAnalyzerCreate(AtModulePktAnalyzer self, AtHdlcChannel channel)
    {
    if (Tha60290011DeviceDccTxFcsMsbIsSupported(AtModuleDeviceGet((AtModule)self)))
        {
        AtEthPort ethPort = DccSgmiiPort(self);
        return Tha60290011DccHdlcPktAnalyzerNew(ethPort, channel);
        }

    return NULL;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->clsPktAnalyzer);
    mThis(self)->clsPktAnalyzer = NULL;
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290011ModulePktAnalyzer* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(clsPktAnalyzer);
    }

static void OverrideAtObject(AtModulePktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, HdlcChannelPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePktAnalyzer);
    }

static AtModulePktAnalyzer ObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->clsPktAnalyzer = NULL;

    return self;
    }

AtModulePktAnalyzer Tha60290011ModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

ThaPktAnalyzer Tha60290011ClsPacketAnalyzerGet(AtModulePktAnalyzer self)
    {
    if (self && mThis(self)->clsPktAnalyzer)
        {
        AtPktAnalyzerInit((AtPktAnalyzer)mThis(self)->clsPktAnalyzer);
        return mThis(self)->clsPktAnalyzer;
        }
    return NULL;
    }

uint32 Tha60290011ModulePktAnalyzerSwDumpModeGet(AtChannel self)
    {
    if (self)
        return SwDumpModeGet(self);

    return cThaPktAnalyzerDumpGbeTx;
    }

void Tha60290011ModulePktAnalyzerHwDumpModeSet(AtChannel self, uint32 hwblock, uint32 swDumpMode, eBool isUsedHwBlock, eBool isUsedSwDumMode)
    {
    if (self)
        HwDumpModeSet(self, hwblock, swDumpMode, isUsedHwBlock, isUsedSwDumMode);
    }

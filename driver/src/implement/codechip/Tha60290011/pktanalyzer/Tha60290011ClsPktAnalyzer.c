/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60290011ClsPktAnalyzer.c
 *
 * Created Date: Dec 20, 2016
 *
 * Description : Implementation of CLS Packet analyzer for debugging purpose
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60150011/pktanalyzer/Tha60150011EthPortPktAnalyzerInternal.h"
#include "../../Tha60290021/pktanalyzer/Tha6029EthSubportVlanPacket.h"
#include "Tha60290011ModulePktAnalyzer.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011ClsPktAnalyzer *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011ClsPktAnalyzer
    {
    tTha60150011EthPortPktAnalyzer super;
    }tTha60290011ClsPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods            m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods           m_ThaPktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtUnused(direction);
    AtUnused(self);
    return Tha6029EthSubportVlanPacketNew(data, length, cAtPacketCacheModeCacheData, cAtFalse);
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0;
    }

static uint16 LongRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    return mChannelHwLongRead(AtPktAnalyzerChannelGet((AtPktAnalyzer)self), address, data, cThaLongRegMaxSize, cAtModulePktAnalyzer);
    }

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtUnused(self);
    AtUnused(pktDumpMode);
    return cAtTrue;
    }

/* ***************************************************2.5G port CLS RX side *************************************************
 * ----->2.5G serdes ---> 2.5G MAC ----> CLS[--> Rx ---> ds1/ds3 TX --> ] ----> PHD AXIA4 ---> PDH MUX --> PDH RX Framer.
 *                                                   |-> control TX --> ] ----> 1G DIM control TX user Mac --> DIM sgmii port.
 ****************************************************************************************************************************/
static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    Tha60290011ModulePktAnalyzerHwDumpModeSet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self), cBlockType2Dot5Cls, pktDumpMode, cAtTrue, cAtFalse);
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    return Tha60290011ModulePktAnalyzerSwDumpModeGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self));
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(tAtPktAnalyzerMethods));
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeIsSupported);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeGet);
        mMethodOverride(m_ThaPktAnalyzerOverride, LongRead);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ClsPktAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011EthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290011ClsPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

void Tha60290011ClsPktAnalyzerPacketTypeSet(ThaPktAnalyzer self, uint32 type)
    {
    Tha60290011ModulePktAnalyzerHwDumpModeSet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self), cBlockType2Dot5Cls, type, cAtFalse, cAtTrue);
    }

eAtRet Tha60290011ClsPktAnalyzer(ThaPktAnalyzer self)
    {
    if (self)
        {
        AtPktAnalyzerAnalyzeRxPackets((AtPktAnalyzer)self);
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

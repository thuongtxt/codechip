/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : pktanalyzer
 * 
 * File        : Tha60290011EthPortDccSgmiiPktAnalyzerInternal.h
 * 
 * Created Date: Jan 17, 2018
 *
 * Description : 1g dcc/data path sgmii pktanalyzer internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011ETHPORTDCCSGMIIPKTANALYZERINTERNAL_H_
#define _THA60290011ETHPORTDCCSGMIIPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290011EthPortAxi4PktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011EthPortDccSgmiiPktAnalyzer
    {
    tTha60290011EthPortAxi4PktAnalyzer super;
    }tTha60290011EthPortDccSgmiiPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60290011EthPortDccSgmiiPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011ETHPORTDCCSGMIIPKTANALYZERINTERNAL_H_ */


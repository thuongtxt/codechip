/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pkt Ananlyzer
 *
 * File        : Tha60290011IEthPktAnalyzer.h
 *
 * Created Date: Nov 28, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPKTANALYZER_H_
#define _THA60290011MODULEPKTANALYZER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pktanalyzer/ThaPktAnalyzer.h"
#include "AtPacket.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/
#define cThaGlobalConfigSelBlockReg         0xC0000
#define cThaGlobalConfigSelBlockTypeMask    cBit15_12
#define cThaGlobalConfigSelBlockTypeShift   12
#define cThaGlobalConfigPktTypeReg          0xC000B
#define cThaGlobalConfigPktTypeClsMask      cBit7_6
#define cThaGlobalConfigPktTypeClsShift     6
#define cThaGlobalConfigPktTypeDimMask      cBit5_4
#define cThaGlobalConfigPktTypeDimShift     4
#define cThaGlobalConfigPktTypeDccMask      cBit3_2
#define cThaGlobalConfigPktTypeDccShift     2
#define cThaGlobalConfigPktType2Dot5Mask      cBit1_0
#define cThaGlobalConfigPktType2Dot5Shift     0

/* RxETH to CLA (for both 1G or 10G) */
#define cBlockTypeRxEthToPwCla  0
/* PWE to TxETH (for both 1G or 10G) */
#define cBlockTypePweToTxEth    1
/* CLA to PDA */
#define cBlockTypeClatoPda      2
/* PLA to PWE */
#define cBlockTypePlaToPwe      3
/* PDA to MAP */
#define cBlockTypePdaToMap      4
/* DeMAP to PLA */
#define cBlockTypeDemapToPla    5
#define cBlockType2Dot5Mac      6
#define cBlockType1GitDcc       7
#define cBlockType1GitDim       8
#define cBlockType2Dot5Cls      9
/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eClsPktAnalyzerPktType
    {
    cClsPktAnalyzerUnknown,
    cClsPktAnalyzerRx,
    cClsPktAnalyzerDs1Tx,
    cClsPktAnalyzerDs3Tx,
    cClsPktAnalyzerControlTx
    }eClsPktAnalyzerPktType;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290011ModulePktAnalyzerSwDumpModeGet(AtChannel self);
void Tha60290011ModulePktAnalyzerHwDumpModeSet(AtChannel self, uint32 hwblock, uint32 swDumpMode, eBool isUsedHwBlock, eBool isUsedSwDumMode);

AtPktAnalyzer Tha60290011EthPortAxi4PktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011EthPortDimControlPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011EthPortDccSgmiiPktAnalyzerNew(AtEthPort port);
AtPktAnalyzer Tha60290011ClsPktAnalyzerNew(AtEthPort port);
ThaPktAnalyzer Tha60290011ClsPacketAnalyzerGet(AtModulePktAnalyzer self);
void Tha60290011ClsPktAnalyzerPacketTypeSet(ThaPktAnalyzer self, uint32 type);
eAtRet Tha60290011ClsPktAnalyzer(ThaPktAnalyzer self);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPKTANALYZER_H_ */

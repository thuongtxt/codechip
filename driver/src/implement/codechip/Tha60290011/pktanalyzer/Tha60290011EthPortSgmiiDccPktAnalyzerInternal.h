/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pktanalyzer
 * 
 * File        : Tha60290011EthPortSgmiiDccPktAnalyzerInternal.h
 * 
 * Created Date: Mar 4, 2018
 *
 * Description : Internal data of sgmii dcc pktanalyzer at the DCC interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011ETHPORTSGMIIDCCPKTANALYZERINTERNAL_H_
#define _THA60290011ETHPORTSGMIIDCCPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pktanalyzer/Tha60290021EthPortSgmiiPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011EthPortSgmiiDccPktAnalyzer
    {
    tTha60290021EthPortSgmiiPktAnalyzer super;
    }tTha60290011EthPortSgmiiDccPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60290011EthPortSgmiiDccPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011ETHPORTSGMIIDCCPKTANALYZERINTERNAL_H_ */


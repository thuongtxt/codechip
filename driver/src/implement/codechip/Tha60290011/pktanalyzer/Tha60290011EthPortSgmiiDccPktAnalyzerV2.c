/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : pktanalyzer
 *
 * File        : Tha60290011EthPortSgmiiDccPktAnalyzerV2.c
 *
 * Created Date: Jan 17, 2018
 *
 * Description : packet analyzer v2 for dcc packet which is from sgmii packet implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011EthPortDccSgmiiPktAnalyzerInternal.h"
#include "../../Tha60290021/pw/Tha60290021ModulePw.h"
#include "../../Tha60290021/pktanalyzer/Tha6029SgmiiEthPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011EthPortSgmiiDccPktAnalyzerV2
    {
    tTha60290011EthPortDccSgmiiPktAnalyzer super;
    }tTha60290011EthPortSgmiiDccPktAnalyzerV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;


/* Save super implementation */
static const tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods  = NULL;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtPacket newPacket = Tha6029SgmiiEthPacketNew(self, data, length, cAtPacketCacheModeCacheData);

    AtUnused(direction);

    Tha6029SgmiiEthPacketDccEthTypeSet(newPacket, cThaDccPwEthTypeDefault);

    return newPacket;
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011EthPortSgmiiDccPktAnalyzerV2);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011EthPortDccSgmiiPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290011EthPortDccSgmiiPktAnalyzerV2New(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

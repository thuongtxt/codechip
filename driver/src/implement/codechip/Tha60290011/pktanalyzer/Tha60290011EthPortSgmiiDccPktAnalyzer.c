/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60290011EthPortSgmiiDccPktAnalyzer.c
 *
 * Created Date: Oct 14, 2016
 *
 * Description : Packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011EthPortSgmiiDccPktAnalyzerInternal.h"
#include "../../Tha60290021/pktanalyzer/Tha6029SgmiiEthPacket.h"
#include "../pw/Tha60290011DccKbyteReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods                      m_AtPktAnalyzerOverride;
static tTha60290021EthPortSgmiiPktAnalyzerMethods m_Tha60290021EthPortSgmiiPktAnalyzerOverride;

/* Super implementation */
static const tAtPktAnalyzerMethods                *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BufferPointerResetMask(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAf6_upen_trig_encap_enb_cap_dcc_Mask;
    }

static uint32 BufferPointerResetShift(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAf6_upen_trig_encap_enb_cap_dcc_Shift;
    }

static eAtRet PacketTypeSet(AtPktAnalyzer self, uint8 pktType)
    {
    if (pktType == cAtPktAnalyzerSohPktTypeDcc)
        return m_AtPktAnalyzerMethods->PacketTypeSet(self, pktType);

    return cAtErrorModeNotSupport;
    }

static void OverrideTha60290021EthPortSgmiiPktAnalyzer(AtPktAnalyzer self)
    {
    Tha60290021EthPortSgmiiPktAnalyzer pktAnalyzer = (Tha60290021EthPortSgmiiPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021EthPortSgmiiPktAnalyzerOverride, mMethodsGet(pktAnalyzer), sizeof(m_Tha60290021EthPortSgmiiPktAnalyzerOverride));

        mMethodOverride(m_Tha60290021EthPortSgmiiPktAnalyzerOverride, BufferPointerResetMask);
        mMethodOverride(m_Tha60290021EthPortSgmiiPktAnalyzerOverride, BufferPointerResetShift);
        }

    mMethodsSet(pktAnalyzer, &m_Tha60290021EthPortSgmiiPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketTypeSet);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideTha60290021EthPortSgmiiPktAnalyzer(self);
    OverrideAtPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011EthPortSgmiiDccPktAnalyzer);
    }

AtPktAnalyzer Tha60290011EthPortSgmiiDccPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021EthPortSgmiiPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290011EthPortSgmiiDccPktAnalyzerNew(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011EthPortSgmiiDccPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

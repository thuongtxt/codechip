/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PktAnalyzer
 *
 * File        : Tha60290022DccHdlcPktAnalyzer.c
 *
 * Created Date: Mar 4, 2018
 *
 * Description : implement the DCC SGMII analyzer with a selected hdlc channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60290011EthPortSgmiiDccPktAnalyzerInternal.h"
#include "../pw/Tha60290011DccKbyteReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_cfg_dump_lid_Base    0x30

#define cAf6_sel_cap_DCC_ENC_DEC_Mask  cBit8
#define cAf6_sel_cap_DCC_ENC_DEC_Shift 8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011DccHdlcPktAnalyzer*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011DccHdlcPktAnalyzer *Tha60290011DccHdlcPktAnalyzer;

typedef struct tTha60290011DccHdlcPktAnalyzer
    {
    tTha60290011EthPortSgmiiDccPktAnalyzer super;
    AtHdlcChannel hdlc;
    }tTha60290011DccHdlcPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods                      m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods                     m_ThaPktAnalyzerOverride;
static tAtObjectMethods                           m_AtObjectOverride;
static tTha60290021EthPortSgmiiPktAnalyzerMethods m_Tha60290021EthPortSgmiiPktAnalyzerOverride;

/* Super implementation */
static const tAtPktAnalyzerMethods                *m_AtPktAnalyzerMethods = NULL;
static const tThaPktAnalyzerMethods               *m_ThaPktAnalyzerMethods = NULL;
static const tAtObjectMethods                     *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtPktAnalyzer self)
    {
    AtDevice device =  AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    return (ThaModuleOcn) AtDeviceModuleGet(device, cThaModuleOcn);
    }

static uint32 CfgDumpIdAddress(AtPktAnalyzer self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self)) + cAf6Reg_upen_cfg_dump_lid_Base;
    }

static void ChannelIdSet(ThaPktAnalyzer self)
    {
    AtChannel channel = (AtChannel)mThis(self)->hdlc;
    uint32 channelId = AtChannelIdGet(channel);

    uint32 regAddr = CfgDumpIdAddress((AtPktAnalyzer)self);

    mChannelHwWrite(channel, regAddr, channelId, cAtModuleSdh);
    }

static void Init(AtPktAnalyzer self)
    {
    m_AtPktAnalyzerMethods->Init(self);
    mMethodsGet((ThaPktAnalyzer)self)->ChannelIdSet((ThaPktAnalyzer)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290011DccHdlcPktAnalyzer object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(hdlc);
    }

static uint32 BufferPointerResetMask(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAf6_upen_trig_encap_enb_cap_dec_Mask;
    }

static uint32 BufferPointerResetShift(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAf6_upen_trig_encap_enb_cap_dec_Shift;
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtPacketFactory factory = AtPktAnalyzerPacketFactory(self);
    return AtPacketFactoryHdlcPacketCreate(factory, data, length, cAtPacketCacheModeCacheData, direction);
    }

static uint32 CfgSideSelectionAddress(AtPktAnalyzer self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self)) + cAf6Reg_upen_loopen_Base;
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtChannel channel = (AtChannel)mThis(self)->hdlc;
    uint32 regAddr = CfgSideSelectionAddress((AtPktAnalyzer)self);
    uint32 regVal = mChannelHwRead(channel, regAddr, cAtModuleEth);
    uint32 hwDumpMode = (pktDumpMode == cThaPktAnalyzerDumpGbeRx) ? 1 : 0;
    mRegFieldSet(regVal, cAf6_sel_cap_DCC_ENC_DEC_, hwDumpMode);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleEth);
    }

static void OverrideTha60290021EthPortSgmiiPktAnalyzer(AtPktAnalyzer self)
    {
    Tha60290021EthPortSgmiiPktAnalyzer pktAnalyzer = (Tha60290021EthPortSgmiiPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021EthPortSgmiiPktAnalyzerOverride, mMethodsGet(pktAnalyzer), sizeof(m_Tha60290021EthPortSgmiiPktAnalyzerOverride));

        mMethodOverride(m_Tha60290021EthPortSgmiiPktAnalyzerOverride, BufferPointerResetMask);
        mMethodOverride(m_Tha60290021EthPortSgmiiPktAnalyzerOverride, BufferPointerResetShift);
        }

    mMethodsSet(pktAnalyzer, &m_Tha60290021EthPortSgmiiPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, m_ThaPktAnalyzerMethods, sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, ChannelIdSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        }

    mMethodsSet(pktAnalyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void OverrideAtObject(AtPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideTha60290021EthPortSgmiiPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    OverrideAtPktAnalyzer(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011DccHdlcPktAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011EthPortSgmiiDccPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->hdlc = hdlcChannel;

    return self;
    }

AtPktAnalyzer Tha60290011DccHdlcPktAnalyzerNew(AtEthPort port, AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port, hdlcChannel);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60290011EthPortAxi4PktAnalyzer.c
 *
 * Created Date: Dec 15, 2016
 *
 * Description : Implementation of Eth port Axi4 Packet Analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011EthPortAxi4PktAnalyzer.h"
#include "Tha60290011ModulePktAnalyzer.h"
#include "AtEthPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods     m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods    m_ThaPktAnalyzerOverride;

/* Save super implementation */
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtUnused(self);
    AtUnused(direction);
    return AtEthPacketNew(data, length, cAtPacketCacheModeCacheData);
    }

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtUnused(self);
    AtUnused(pktDumpMode);
    return cAtTrue;
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    Tha60290011ModulePktAnalyzerHwDumpModeSet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self), cBlockType2Dot5Mac, pktDumpMode, cAtTrue, cAtTrue);
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    return Tha60290011ModulePktAnalyzerSwDumpModeGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self));
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeIsSupported);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeGet);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011EthPortAxi4PktAnalyzer);
    }

AtPktAnalyzer Tha60290011EthPortAxi4PktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011EthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290011EthPortAxi4PktAnalyzerNew(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011EthPortAxi4PktAnalyzerObjectInit(newPktAnalyzer, port);
    }

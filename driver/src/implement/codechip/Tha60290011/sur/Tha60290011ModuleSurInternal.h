/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290011ModuleSurInternal.h
 * 
 * Created Date: Aug 1, 2018
 *
 * Description : 60290011 SUR internal data
 * Created Date: Aug 1, 2018
 *
 * Description : 60290011 SUR internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULESURINTERNAL_H_
#define _THA60290011MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/sur/Tha60210031ModuleSurInternal.h"
#include "Tha60290011ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleSurMethods
    {
    uint8 (*PwNumSlice)(Tha60290011ModuleSur self);
    }tTha60290011ModuleSurMethods;

typedef struct tTha60290011ModuleSur
    {
    tTha60210031ModuleSur super;
    const tTha60290011ModuleSurMethods *methods;
    }tTha60290011ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha60290011ModuleSurObjectInit(AtModuleSur self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULESURINTERNAL_H_ */


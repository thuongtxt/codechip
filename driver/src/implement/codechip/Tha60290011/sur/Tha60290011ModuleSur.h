/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveilance
 * 
 * File        : Tha60290011ModuleSur.h
 * 
 * Created Date: Oct 4, 2017
 *
 * Description : Surveilance interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULESUR_H_
#define _THA60290011MODULESUR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleSur *Tha60290011ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290011SurInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290011SurTcaInterruptManagerNew(AtModule module);
uint8 Tha60290011ModuleSurPwNumSlice(Tha60290011ModuleSur self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULESUR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60290011ModuleSur.c
 *
 * Created Date: Oct 29, 2016
 *
 * Description : SUR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/engine/ThaHardSurEngine.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "Tha60290011ModuleSurInternal.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290011ModuleSur)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011ModuleSurMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleSurMethods          m_AtModuleSurOverride;
static tThaModuleHardSurMethods     m_ThaModuleHardSurOverride;
static tTha60210031ModuleSurMethods m_Tha60210031ModuleSurOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionChangeTo155dot50MhzClock(Tha60210031ModuleSur self)
    {
    /* This device will use 155.52Mhz */
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool TcaIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnableHwPmEngine(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static eBool HasMonitorTimer(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumThresholdProfiles(AtModuleSur self)
    {
    /* This is type ID */
    AtUnused(self);
    return 8;
    }

static eBool ShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self)
    {
    return Tha602900xxShouldMaskFailuresWhenNonAutonomousFailures(self);
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    return Tha60290011DeviceSurFailureForwardingStatusIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return Tha60290011DeviceSurFailureHoldOnTimerConfigurable(AtModuleDeviceGet((AtModule)self));
    }

static eAtSurPeriodExpireMethod DefaultExpireMethod(ThaModuleHardSur self)
    {
    return Tha602900xxModuleSurDefaultExpireMethod(self);
    }

static eAtSurTickSource DefaultTickSourceGet(ThaModuleHardSur self)
    {
    return Tha602900xxModuleSurDefaultTickSource(self);
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == 0)
        return Tha60290011SurInterruptManagerNew(self);

    if (managerIndex == 1)
        return Tha60290011SurTcaInterruptManagerNew(self);

    return NULL;
    }

static uint8 PwNumSlice(Tha60290011ModuleSur self)
    {
    AtUnused(self);
    return 1;
    }

static void MethodsInit(AtModuleSur self)
    {
    Tha60290011ModuleSur sur = (Tha60290011ModuleSur)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwNumSlice);
        }

    mMethodsSet(sur, &m_methods);
    }

static void OverrideTha60210031ModuleSur(AtModuleSur self)
    {
    Tha60210031ModuleSur module = (Tha60210031ModuleSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModuleSurOverride, mMethodsGet(module), sizeof(m_Tha60210031ModuleSurOverride));

        mMethodOverride(m_Tha60210031ModuleSurOverride, StartVersionChangeTo155dot50MhzClock);
        }

    mMethodsSet(module, &m_Tha60210031ModuleSurOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(surModule), sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, CanEnableHwPmEngine);
        mMethodOverride(m_ThaModuleHardSurOverride, HasMonitorTimer);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        mMethodOverride(m_ThaModuleHardSurOverride, DefaultExpireMethod);
        mMethodOverride(m_ThaModuleHardSurOverride, DefaultTickSourceGet);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, TcaIsSupported);
        mMethodOverride(m_AtModuleSurOverride, NumThresholdProfiles);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, ShouldMaskFailuresWhenNonAutonomousFailures);
        mMethodOverride(m_AtModuleSurOverride, TcaIsSupported);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideTha60210031ModuleSur(self);
    OverrideThaModuleHardSur(self);
    OverrideAtModuleSur(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleSur);
    }

AtModuleSur Tha60290011ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60290011ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleSurObjectInit(newModule, device);
    }

uint8 Tha60290011ModuleSurPwNumSlice(Tha60290011ModuleSur self)
    {
    if (self)
        return mMethodsGet(self)->PwNumSlice(self);
    return 0;
    }

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : Tha60290011SurInterruptController.c
 *
 * Created Date: Oct 4, 2017
 *
 * Description : Module Surveillance interrupt controller.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/sur/Tha60210031SurInterruptManagerInternal.h"
#include "Tha60290011ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60290011SurInterruptManager
    {
    tTha60210031SurInterruptManager super;
    }tTha60290011SurInterruptManager;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaInterruptManagerMethods    m_ThaInterruptManagerOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    Tha60290011ModuleSur sur = (Tha60290011ModuleSur)AtInterruptManagerModuleGet((AtInterruptManager)self);
    return Tha60290011ModuleSurPwNumSlice(sur);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SurInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SurInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290011SurInterruptManagerNew(AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newIntrController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newIntrController == NULL)
        return NULL;

    return ObjectInit(newIntrController, module);
    }

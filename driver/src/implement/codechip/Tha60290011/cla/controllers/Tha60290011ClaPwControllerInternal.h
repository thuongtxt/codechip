/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290011ClaPwControllerInternal.h
 *
 * Created Date: Aug 4, 2017
 *
 * Description : Internal Interface of CLA PW Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011CLAPWCONTROLLERINTERNAL_H_
#define _THA60290011CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210031/cla/Tha60210031ClaPwControllerInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ClaPwController
    {
    tTha60210031ClaPwController super;
    }tTha60290011ClaPwController;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60290011ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011CLAPWCONTROLLERINTERNAL_H_ */

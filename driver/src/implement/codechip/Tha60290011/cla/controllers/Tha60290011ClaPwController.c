/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290011ClaPwController.c
 *
 * Created Date: Dec 14, 2016
 *
 * Description : Concrete class of 60290011 CLA controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/* Save super implementation */
static const tThaClaPwControllerMethods *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePmc PmcModule(AtPw self)
	{
	return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwPmc);
	}

static eBool GetCounterFromPmc(ThaClaPwController self)
	{
	AtDevice device = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self));
	ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
	return (ThaModulePwDebugCountersModuleGet(pwModule) == cThaModulePwPmc) ? cAtTrue : cAtFalse;
	}

static uint32 RxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxPacketsGet(PmcModule(pw), pw, clear);
    return m_ThaClaPwControllerMethods->RxPacketsGet(self, pw, clear);
    }

static uint32 RxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxMalformedPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxMalformedPacketsGet(self, pw, clear);
    }

static uint32 RxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxStrayPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxStrayPacketsGet(self, pw, clear);
    }

static uint32 RxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxLbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxLbitPacketsGet(self, pw, clear);
    }

static uint32 RxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxRbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxRbitPacketsGet(self, pw, clear);
    }

static uint32 RxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxMbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxMbitPacketsGet(self, pw, clear);
    }

static uint32 RxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxNbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxNbitPacketsGet(self, pw, clear);
    }

static uint32 RxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxPbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaClaPwControllerMethods->RxPbitPacketsGet(self, pw, clear);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));
        mMethodOverride(m_ThaClaPwControllerOverride, RxPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMalformedPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxStrayPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxLbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxRbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxNbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPbitPacketsGet);
        }
    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ClaPwController);
    }

ThaClaPwController Tha60290011ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60290011ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ClaPwControllerObjectInit(newController, cla);
    }

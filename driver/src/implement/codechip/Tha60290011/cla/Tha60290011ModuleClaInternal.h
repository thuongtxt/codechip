/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290011ModuleClaInternal.h
 * 
 * Created Date: Dec 9, 2016
 *
 * Description : Interface of Cla Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULECLAINTERNAL_H_
#define _THA60290011MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/ThaModuleCla.h"
#include "../../Tha60210031/cla/Tha60210031ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleCla
    {
    tTha60210031ModuleCla super;
    }tTha60290011ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModuleClaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULECLAINTERNAL_H_ */

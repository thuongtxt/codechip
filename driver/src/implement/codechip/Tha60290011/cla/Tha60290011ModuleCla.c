/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290011ModuleCla.c
 *
 * Created Date: Oct 29, 2016
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/ThaModuleClaReg.h"
#include "Tha60290011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegClaGlbPsnCtrlSubPortVlanEnMask       (cBit31)
#define cThaRegClaGlbPsnCtrlSubPortVlanEnShift      (31)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods         m_ThaModuleClaOverride;
static tTha60210031ModuleClaMethods m_Tha60210031ModuleClaOverride;

/* Save Super Implementation */
static const tThaModuleClaMethods *m_ThaModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FourKCellSupported(Tha60210031ModuleCla self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet SubPortVlanCheckingEnable(ThaModuleCla self, eBool enable)
    {
    uint32 regAddr = cThaRegClaGlbPsnCtrl;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cThaRegClaGlbPsnCtrlSubPortVlanEn, enable ? 0x1 : 0x0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool SubPortVlanCheckingIsEnabled(ThaModuleCla self)
    {
    uint32 regAddr = cThaRegClaGlbPsnCtrl;
    uint32 regVal = mModuleHwRead(self, regAddr);

    return mRegField(regVal, cThaRegClaGlbPsnCtrlSubPortVlanEn) == 1 ? cAtTrue : cAtFalse;
    }

static eAtRet DefaultSet(ThaModuleCla self)
    {
    eAtRet ret = m_ThaModuleClaMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    return SubPortVlanCheckingEnable(self, cAtTrue);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60290011ClaPwControllerNew(self);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, m_ThaModuleClaMethods, sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, DefaultSet);
        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, SubPortVlanCheckingEnable);
        mMethodOverride(m_ThaModuleClaOverride, SubPortVlanCheckingIsEnabled);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideTha60210031ModuleCla(AtModule self)
    {
    Tha60210031ModuleCla module = (Tha60210031ModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModuleClaOverride, mMethodsGet(module), sizeof(m_Tha60210031ModuleClaOverride));

        mMethodOverride(m_Tha60210031ModuleClaOverride, FourKCellSupported);
        }

    mMethodsSet(module, &m_Tha60210031ModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    OverrideTha60210031ModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleCla);
    }

AtModule Tha60290011ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleClaObjectInit(newModule, device);
    }

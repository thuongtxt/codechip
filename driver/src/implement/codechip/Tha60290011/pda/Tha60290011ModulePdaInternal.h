/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60290011ModulePdaInternal.h
 * 
 * Created Date: Jul 10, 2018
 *
 * Description : 60290011 module PDA internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPDAINTERNAL_H_
#define _THA60290011MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pda/Tha60210031ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePda
    {
    tTha60210031ModulePda super;
    }tTha60290011ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModulePdaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEPDAINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60290011ModulePda.c
 *
 * Created Date: Oct 28, 2016
 *
 * Description : PDA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePdaInternal.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tTha60210031ModulePdaMethods m_Tha60210031ModulePdaOverride;

/* Save super implementation */
static const tThaModulePdaMethods  *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated(Tha60210031ModulePda self)
    {
    AtUnused(self);
    return 0;
    }

static eBool JitterBufferAdditionalBytesIsSupported(Tha60210031ModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool JitterBufferWatermarkIsSupported(ThaModulePda self, AtPw pw)
    {
    /* TODO: need to work with HW to see if this is also supported on this
     * product as well */
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static ThaModulePmc PmcModule(AtPw self)
	{
	return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwPmc);
	}

static eBool GetCounterFromPmc(ThaModulePda self)
	{
	ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
	return (ThaModulePwDebugCountersModuleGet(pwModule) == cThaModulePwPmc) ? cAtTrue : cAtFalse;
	}

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxOutOfSeqDropedPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePdaMethods->PwRxOutOfSeqDropedPacketsGet(self, pw, clear);
    }

static uint32 PwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxBytesGet(PmcModule(pw), pw, clear);
    return m_ThaModulePdaMethods->PwRxBytesGet(self, pw, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxReorderedPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePdaMethods->PwRxReorderedPacketsGet(self, pw, clear);
    }

static uint32 PwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxLopsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePdaMethods->PwRxLopsGet(self, pw, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxJitBufOverrunGet(PmcModule(pw), pw, clear);
	return m_ThaModulePdaMethods->PwRxJitBufOverrunGet(self, pw, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxJitBufUnderrunGet(PmcModule(pw), pw, clear);
	return m_ThaModulePdaMethods->PwRxJitBufUnderrunGet(self, pw, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwRxLostPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePdaMethods->PwRxLostPacketsGet(self, pw, clear);
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanControlLopsPktReplaceMode(ThaModulePda self)
    {
    return Tha60290011DevicePdaCanControlLopsPktReplaceMode(AtModuleDeviceGet((AtModule)self));
    }

static eBool PwJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    return ThaModulePdaPwVariantJitterBufferSizeIsSupported(self, pw, microseconds);
    }

static eBool PwJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    return ThaModulePdaPwVariantJitterBufferDelayIsSupported(self, pw, microseconds);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLopsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferWatermarkIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayIsSupported);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210031ModulePda(AtModule self)
    {
    Tha60210031ModulePda pda = (Tha60210031ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdaOverride, mMethodsGet(pda), sizeof(m_Tha60210031ModulePdaOverride));

        mMethodOverride(m_Tha60210031ModulePdaOverride, StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated);
        mMethodOverride(m_Tha60210031ModulePdaOverride, JitterBufferAdditionalBytesIsSupported);
        }

    mMethodsSet(pda, &m_Tha60210031ModulePdaOverride);
    }

static void Override(AtModule self)
    {
	OverrideThaModulePda(self);
    OverrideTha60210031ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePda);
    }

AtModule Tha60290011ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePdaObjectInit(newModule, device);
    }

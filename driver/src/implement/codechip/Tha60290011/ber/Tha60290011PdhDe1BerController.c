/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60290011PdhDe1PathBerController.c
 *
 * Created Date: Jul 15, 2016
 *
 * Description : DE1 BER controller of 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/ber/Tha60210031ModuleBerInternal.h"
#include "../pdh/Tha60290011PdhDe1.h"
#include "Tha60290011ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011PdhDe1BerController
    {
    tTha60210031PdhDe1BerController super;
    }tTha60290011PdhDe1BerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;
static tTha60210011SdhVc1xBerControllerMethods m_Tha60210011SdhVc1xBerControllerOverride;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtBerControllerMonitoredChannel((AtBerController)self);
    uint32 sts = 0, vtg = 0, vt = 0, slice = 0;

    Tha60290011PdhDe1InterfaceChannelId2HwId(de1, &sts, &vtg, &vt);

    return (sts * 32UL + (slice + 2UL) * 8UL + vtg);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtBerControllerMonitoredChannel((AtBerController)self);
    uint32 sts = 0, vtg = 0, vt = 0, slice = 0;

    Tha60290011PdhDe1InterfaceChannelId2HwId(de1, &sts, &vtg, &vt);

    return (sts * 112UL + (slice + 2UL) * 28UL + vtg * 4 + vt);
    }

static uint8 ChannelId(Tha60210011SdhVc1xBerController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtBerControllerMonitoredChannel((AtBerController)self);
    uint32 sts = 0, vtg = 0, vt = 0;

    Tha60290011PdhDe1InterfaceChannelId2HwId(de1, &sts, &vtg, &vt);
    return (uint8)vt;
    }

static void OverrideTha60210011SdhVc1xBerController(AtBerController self)
    {
    Tha60210011SdhVc1xBerController controller = (Tha60210011SdhVc1xBerController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhVc1xBerControllerOverride, mMethodsGet(controller), sizeof(m_Tha60210011SdhVc1xBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhVc1xBerControllerOverride, ChannelId);
        }

    mMethodsSet(controller, &m_Tha60210011SdhVc1xBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideTha60210011SdhAuVcBerController(self);
    OverrideTha60210011SdhVc1xBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011PdhDe1BerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PdhDe1BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60290011PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210031ModuleBer.c
 *
 * Created Date: Jul 15, 2016
 *
 * Description : Module BER of 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/ber/Tha60210031ModuleBerInternal.h"
#include "Tha60290011ModuleBer.h"
#include "../pdh/Tha60290011ModulePdh.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011ModuleBer
    {
    tTha60210031ModuleBer super;
    }tTha60290011ModuleBer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleBerMethods           m_AtModuleBerOverride;
static tTha60210011ModuleBerMethods  m_Tha60210011ModuleBerOverride;

static const tAtModuleBerMethods           *m_AtModuleBerMethods = NULL;
static const tTha60210011ModuleBerMethods  *m_Tha60210011ModuleBerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtBerController PdhChannelPathBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    eAtPdhChannelType channelType;
    AtModulePdh pdhModule = (AtModulePdh)AtChannelModuleGet(pdhChannel);
    if (Tha60290011ModulePdhInterfaceTypeGet(pdhModule) == cTha60290011PdhInterfaceTypeDe3)
        return m_AtModuleBerMethods->PdhChannelPathBerControlerCreate(self, pdhChannel);

    channelType = AtPdhChannelTypeGet((AtPdhChannel)pdhChannel);
    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return Tha60290011PdhDe1BerControllerNew(AtChannelIdGet(pdhChannel), pdhChannel, self);

    return NULL;
    }

static AtBerController PdhChannelLineBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    AtUnused(self);
    AtUnused(pdhChannel);
    /* No BPV supported */
    return NULL;
    }

static eBool  HasStandardClearTime(Tha60210011ModuleBer self)
    {
    return Tha60290011DeviceHasStandardClearTime(AtModuleDeviceGet((AtModule)self));
    }

static eBool MeasureTimeEngineIsSupported(AtModuleBer self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleBerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, m_AtModuleBerMethods, sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, PdhChannelPathBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, PdhChannelLineBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, MeasureTimeEngineIsSupported);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleBerMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, m_Tha60210011ModuleBerMethods, sizeof(m_Tha60210011ModuleBerOverride));

        mMethodOverride(m_Tha60210011ModuleBerOverride, HasStandardClearTime);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    OverrideTha60210011ModuleBer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleBer);
    }

static AtModuleBer ObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60290011ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBerModule, device);
    }

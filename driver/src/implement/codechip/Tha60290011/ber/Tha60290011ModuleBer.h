/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60290011ModuleBer.h
 * 
 * Created Date: Jun 11, 2016
 *
 * Description : Ber inteface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290011_BER_THA60290011MODULEBER_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290011_BER_THA60290011MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtChannel.h"
#include "AtBerController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60290011PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtModuleBer Tha60290011ModuleBerNew(AtDevice device);
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290011_BER_THA60290011MODULEBER_H_ */


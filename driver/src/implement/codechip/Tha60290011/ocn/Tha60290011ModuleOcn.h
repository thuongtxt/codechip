/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290011ModuleOcn.h
 * 
 * Created Date: Oct 26, 2016
 *
 * Description : Tha60290011 OCN Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEOCN_H_
#define _THA60290011MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModuleOcnNew(AtDevice device);
eAtRet Tha60290011ModuleOcnDccMappingInit(ThaModuleOcn self);
eBool Tha60290011ModuleOcnNewLoopbackIsSupported(ThaModuleOcn self);
eAtRet Tha60290011ModuleOcnFpgaAuVcLocalLoopEnable(AtChannel auVc, eBool enable);
eBool Tha60290011ModuleOcnFpgaAuVcLocalLoopIsEnabled(AtChannel auVc);
eAtRet Tha60290011ModuleOcnFpgaAuVcRemoteLoopEnable(AtChannel auVc, eBool enable);
eBool Tha60290011ModuleOcnFpgaAuVcRemoteLoopIsEnabled(AtChannel auVc);
eAtRet Tha60290011ModuleOcnFpgaAuVcTxAisForce(AtChannel self, eBool enable);
eBool Tha60290011ModuleOcnFpgaAuVcTxAisIsForced(AtChannel self);
eAtRet Tha60290011ModuleOcnFpgaAuVcRxAisForce(AtChannel self, eBool enable);
eBool Tha60290011ModuleOcnFpgaAuVcRxAisIsForced(AtChannel self);

eBool Tha60290011ModuleOcnTohByteIsSupported(AtSdhChannel self, uint32 tohbyte);
eAtRet Tha60290011ModuleOcnFpgaLineTxOhByteSet(AtSdhChannel self, uint32 overHeadByteId, uint8 value);
uint8 Tha60290011ModuleOcnFpgaLineTxOhByteGet(AtSdhChannel self, uint32 overHeadByteId);
uint8 Tha60290011ModuleOcnFpgaLineRxOhByteGet(AtSdhChannel self, uint32 overHeadByteId);

eAtRet Tha60290011ModuleOcnEc1LineModeSet(ThaModuleOcn self, uint32 lineId, eAtSdhChannelMode mode);
eAtSdhChannelMode Tha60290011ModuleOcnEc1LineModeGet(ThaModuleOcn self, uint32 lineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEOCN_H_ */


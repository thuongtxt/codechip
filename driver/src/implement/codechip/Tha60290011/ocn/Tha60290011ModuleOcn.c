/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60290011ModuleOcn.c
 *
 * Created Date: Oct 13, 2016
 *
 * Description : Module OCN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcnInternal.h"
#include "Tha60290011ModuleOcnReg.h"
#include "Tha60290011ModuleOcnInternal.h"
#include "../pw/Tha60290011DccKbyteV2Reg.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* line loopback reg, mask ,shift */
#define cAf6Reg_linetslblc_Mask(lineId) (cBit0 << (lineId))
#define cAf6Reg_linetslblc_Shift(lineId) (lineId)

#define cAf6Reg_linelbrm_Mask(lineId) (cBit0 << (lineId))
#define cAf6Reg_linelbrm_Shift(lineId) (lineId)

/* AuVc loopback reg, mask ,shift */
#define cAf6Reg_stslblc_Mask(hwStsId) (cBit0 << (hwStsId))
#define cAf6Reg_stslblc_Shift(hwStsId) (hwStsId)

#define cAf6Reg_stslbrm_Mask(hwStsId) (cBit0 << (hwStsId))
#define cAf6Reg_stslbrm_Shift(hwStsId) (hwStsId)


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;
static tTha60210031ModuleOcnMethods m_Tha60210031ModuleOcnOverride;

/* Supper implementation */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModuleOcnMethods *m_ThaModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    return Tha60290011DeviceOcnSohOverEthIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static uint32 SohOverEthBaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0x0D00000;
    }

static AtLongRegisterAccess SohOverEthLongRegisterAccessCreate(ThaModuleOcn self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 DccEnCfgRegAddress(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (Tha60290011DeviceDccTxFcsMsbIsSupported(device))
        return cAf6Reg_upen_cfg_dcc_en_V2;

    return cAf6Reg_upen_cfg_dcc_en;
    }

static eAtRet DccEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 address = ThaModuleOcnSohOverEthBaseAddress(self) + DccEnCfgRegAddress(self);
    mModuleHwWrite(self, address, (enable ? 1:0));
    return cAtOk;
    }

static AtIpCore Core(ThaModuleOcn self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    }

static void DccHdlcTestPktGeneratorDisable(ThaModuleOcn self)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address = ThaModuleOcnSohOverEthBaseAddress(self) + cAf6Reg_upen_dcc_cfg_testgen_enacid;

    AtOsalMemInit(longReg, 0, sizeof(uint32)*cThaLongRegMaxSize);
    mModuleHwLongWrite(self, address, longReg, cThaLongRegMaxSize, Core(self));
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    eAtRet ret = Tha6029ModuleOcnSohOverEthInit(self);

    ret |= DccEnable(self, cAtTrue);
    DccHdlcTestPktGeneratorDisable(self);
    return ret;
    }

static uint32 UpenDccdecBase(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_upen_dccdec_Base;
    }

static uint32 StartVersionNeedHwRxPiPjAdjThreshold(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return 0;
    }

static void RxPgDefaultUpdateForEpar(ThaModuleOcn self)
    {
    uint32 regAddr = Tha60210011ModuleOcnGlbspiBase(self) + Tha60210011ModuleOcnBaseAddress(self);
    uint32 regVal =  mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgAdjThresh_, 0xa);
    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgFlowThresh_, 0x3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiAisMskDis_, 0x0);
    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgNorPtrThresh_, 0x3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiAdjCntSel_, 0x0);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void LineScrambleEnable(ThaModuleOcn self)
    {
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_glbrfm_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal =  mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cAf6_glbrfm_reg_RxFrmDescrEn_Mask, cAf6_glbrfm_reg_RxFrmDescrEn_Shift, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_glbtfm_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cAf6_glbtfm_reg_TxLineScrEn_Mask, cAf6_glbtfm_reg_TxLineScrEn_Shift, 1);
    mModuleHwWrite(self, regAddr, regVal);
    }

static ThaModuleOcn ModuleOcn(AtChannel channel)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModuleOcn);
    }

static void TxOhByteIdToMaskShift(uint32 ohByteId, uint32 *mask, uint32 *shift)
    {
    if (ohByteId == cAtSdhLineRsOverheadByteE1)
        {
        *mask = cAf6_OCNTxRsOhE1Pat_Mask;
        *shift = cAf6_OCNTxRsOhE1Pat_Shift;
        return;
        }
    if (ohByteId == cAtSdhLineRsOverheadByteF1)
        {
        *mask = cAf6_OCNTxRsOhF1Pat_Mask;
        *shift = cAf6_OCNTxRsOhF1Pat_Shift;
        return;
        }
    if (ohByteId == cAtSdhLineMsOverheadByteE2)
        {
        *mask = cAf6_OCNTxRsOhE2Pat_Mask;
        *shift = cAf6_OCNTxRsOhE2Pat_Shift;
        return;
        }

    *mask = 0;
    *shift = 0;
    }

static eAtRet LineTxOhByteSet(AtSdhChannel self, uint32 overHeadByteId, uint8 value)
    {
    ThaModuleOcn module = ModuleOcn((AtChannel)self);
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 address, mask, shift, regValue;

    address = cAf6Reg_tfmregctl1_Base + Tha60210011ModuleOcnBaseAddress(module) + lineId;
    TxOhByteIdToMaskShift(overHeadByteId, &mask, &shift);

    regValue = mModuleHwRead(module, address);
    mFieldIns(&regValue, mask, shift, value);
    mModuleHwWrite(module, address, regValue);
    return cAtOk;
    }

static uint8 LineTxOhByteGet(AtSdhChannel self, uint32 overHeadByteId)
    {
    ThaModuleOcn module = ModuleOcn((AtChannel)self);
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 address, mask, shift, regValue, fieldVal;

    address = cAf6Reg_tfmregctl1_Base + Tha60210011ModuleOcnBaseAddress(module) + lineId;
    TxOhByteIdToMaskShift(overHeadByteId, &mask, &shift);

    regValue = mModuleHwRead(module, address);
    mFieldGet(regValue, mask, shift, uint32, &fieldVal);

    return (uint8)fieldVal;
    }

static uint32 RxOhByteIdToRegBase(uint32 ohByteId)
    {
    if (ohByteId == cAtSdhLineRsOverheadByteE1)
        return cAf6Reg_tohe1byte_Base;

    if (ohByteId == cAtSdhLineRsOverheadByteF1)
        return cAf6Reg_tohf1byte_Base;

    if (ohByteId == cAtSdhLineMsOverheadByteE2)
        return cAf6Reg_tohe2byte_Base;

    return cInvalidUint32;
    }

static uint8 LineRxOhByteGet(AtSdhChannel self, uint32 overHeadByteId)
    {
    ThaModuleOcn module = ModuleOcn((AtChannel)self);
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 address, regValue;

    address = RxOhByteIdToRegBase(overHeadByteId) + Tha60210011ModuleOcnBaseAddress(module) + lineId;
    regValue = mModuleHwRead(module, address);

    return (uint8)regValue;
    }

static eAtRet AuVcLocalLoopEnable(AtChannel self, eBool enable)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_stslblc_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslblc_Mask(sts);
    shift = cAf6Reg_stslblc_Shift(sts);
    enableBinary = enable ? 1 : 0;
    regValue = mModuleHwRead(module, address);
    mFieldIns(&regValue, mask, shift, enableBinary);
    mModuleHwWrite(module, address, regValue);
    return cAtOk;
    }

static eBool AuVcLocalLoopIsEnabled(AtChannel self)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_stslblc_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslblc_Mask(sts);
    shift = cAf6Reg_stslblc_Shift(sts);
    regValue = mModuleHwRead(module, address);
    mFieldGet(regValue, mask, shift, uint32, &enableBinary);

    return enableBinary ? cAtTrue : cAtFalse;
    }

static eAtRet AuVcRemoteLoopEnable(AtChannel self, eBool enable)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

   if (ret != cAtOk)
       return ret;

    address = cAf6Reg_stslbrm_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslbrm_Mask(sts);
    shift = cAf6Reg_stslbrm_Shift(sts);
    enableBinary = enable ? 1 : 0;
    regValue = mModuleHwRead(module, address);
    mFieldIns(&regValue, mask, shift, enableBinary);
    mModuleHwWrite(module, address, regValue);
    return cAtOk;
    }

static eBool AuVcRemoteLoopIsEnabled(AtChannel self)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_stslbrm_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslbrm_Mask(sts);
    shift = cAf6Reg_stslbrm_Shift(sts);
    regValue = mModuleHwRead(module, address);
    mFieldGet(regValue, mask, shift, uint32, &enableBinary);

    return enableBinary ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaAuVcTxAisForce(AtChannel self, eBool enable)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_ststxfrcaislc_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslblc_Mask(sts);
    shift = cAf6Reg_stslblc_Shift(sts);
    enableBinary = enable ? 1 : 0;
    regValue = mModuleHwRead(module, address);
    mFieldIns(&regValue, mask, shift, enableBinary);
    mModuleHwWrite(module, address, regValue);
    return cAtOk;
    }

static eBool FpgaAuVcTxAisIsForced(AtChannel self)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_ststxfrcaislc_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslblc_Mask(sts);
    shift = cAf6Reg_stslblc_Shift(sts);
    regValue = mModuleHwRead(module, address);
    mFieldGet(regValue, mask, shift, uint32, &enableBinary);

    return enableBinary ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaAuVcRxAisForce(AtChannel self, eBool enable)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

   if (ret != cAtOk)
       return ret;

    address = cAf6Reg_stsrxfrcaislc_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslbrm_Mask(sts);
    shift = cAf6Reg_stslbrm_Shift(sts);
    enableBinary = enable ? 1 : 0;
    regValue = mModuleHwRead(module, address);
    mFieldIns(&regValue, mask, shift, enableBinary);
    mModuleHwWrite(module, address, regValue);
    return cAtOk;
    }

static eBool FpgaAuVcRxAisIsForced(AtChannel self)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 address, mask, shift, regValue, enableBinary;
    uint8 slice, sts;
    eAtRet ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &sts);

    if (ret != cAtOk)
        return ret;

    address = cAf6Reg_stsrxfrcaislc_reg + Tha60210011ModuleOcnBaseAddress(module);
    mask = cAf6Reg_stslbrm_Mask(sts);
    shift = cAf6Reg_stslbrm_Shift(sts);
    regValue = mModuleHwRead(module, address);
    mFieldGet(regValue, mask, shift, uint32, &enableBinary);

    return enableBinary ? cAtTrue : cAtFalse;
    }

static eBool NewLoopbackIsSupported(ThaModuleOcn self)
    {
    return Tha60290011DeviceOcnVcNewLoopbackIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static eBool TohByteIsSupported(AtSdhChannel self, uint32 tohByte)
    {
    if (!Tha60290011DeviceOcnTohByteIsSupported(AtChannelDeviceGet((AtChannel)self)))
        return cAtFalse;

    if ((tohByte == cAtSdhLineRsOverheadByteE1)
        || (tohByte == cAtSdhLineRsOverheadByteF1)
        || (tohByte == cAtSdhLineMsOverheadByteE2))
        return cAtTrue;

    return cAtFalse;
    }

static eBool TuVcIsLoopbackAtPointerProcessor(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    AtUnused(tuVc);
    return Tha60290011ModuleOcnNewLoopbackIsSupported((ThaModuleOcn)self);
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret = m_ThaModuleOcnMethods->DefaultSet(self);

    RxPgDefaultUpdateForEpar(self);
    LineScrambleEnable(self);

    return ret;
    }

static eBool IsSohRegister(ThaModuleOcn self, uint32 localAddress)
    {
    AtUnused(self);
    if (mInRange(localAddress, 0xD00000, 0xD3FFFF))
        return cAtTrue;
    return cAtFalse;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {

    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static eBool KByteOverEthIsSupported(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Vc3ToTu3VcEnable(ThaModuleOcn self, AtSdhChannel vc3, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 tu3cepTovc3TxEnable = 0;

    /* Line-side VC3 to TU3 CEP. */
    regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) + offset;
    regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_demramctl_PiIwkRxVc3toTu3_, enable ? 1 : 0);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModuleOcn);

    /* TU3 CEP to Line-side VC3. */
    if (!ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)vc3))
        tu3cepTovc3TxEnable = (enable ? 1 : 0);

    regAddr = ThaModuleOcnTxStsMultiplexingControl(self, vc3) + offset;
    regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgIwkTxTu3toVc3_, tu3cepTovc3TxEnable);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool Vc3ToTu3VcIsEnabled(ThaModuleOcn self, AtSdhChannel vc3)
    {
    uint32 regAddr, regVal;
    uint32 offset = Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 tu3cepTovc3RxEnable = 0;

    /* Line-side VC3 to TU3 CEP. */
    regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) + offset;
    regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);
    tu3cepTovc3RxEnable = mRegField(regVal, cAf6_demramctl_PiIwkRxVc3toTu3_);

    return tu3cepTovc3RxEnable ? cAtTrue : cAtFalse;
    }

static uint32 OcnVtTuPointerGeneratorNdfIntrMask(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6_vtpgstkram_VtPgNdfIntr_Mask;
    }

static eAtRet LineIdSw2HwGet(ThaModuleOcn self, AtSdhLine sdhLine, eAtModule moduleId, uint8* sliceId, uint8 *hwLineInSlice)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)sdhLine);
    uint32 _phyModule = (uint32)moduleId;

    if (moduleId == cAtModuleSur)
        {
        *sliceId = 0;
        *hwLineInSlice = (uint8)lineId;
        return cAtOk;
        }

    if (_phyModule == cThaModulePoh)
        {
        *sliceId = 1;
        *hwLineInSlice = (uint8)lineId;
        return cAtOk;
        }

    return m_ThaModuleOcnMethods->LineIdSw2HwGet(self, sdhLine, moduleId, sliceId, hwLineInSlice);
    }

static eAtRet NewStsLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            ret  = AuVcLocalLoopEnable(self, cAtTrue);
            ret |= AuVcRemoteLoopEnable(self, cAtFalse);
            break;
        case cAtLoopbackModeRemote:
            ret  = AuVcLocalLoopEnable(self, cAtFalse);
            ret |= AuVcRemoteLoopEnable(self, cAtTrue);
            break;
        case cAtLoopbackModeRelease:
            ret  = AuVcLocalLoopEnable(self, cAtFalse);
            ret |= AuVcRemoteLoopEnable(self, cAtFalse);
            break;
        default:
            return cAtErrorModeNotSupport;
        }

    return ret;
    }

static eAtRet StsLoopbackSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 loopbackMode)
    {
    if (Tha60290011ModuleOcnNewLoopbackIsSupported(self))
        return NewStsLoopbackSet((AtChannel)channel, loopbackMode);

    return m_ThaModuleOcnMethods->StsLoopbackSet(self, channel, stsId, loopbackMode);
    }

static uint8 NewStsLoopbackGet(AtChannel self)
    {
    if (AuVcLocalLoopIsEnabled(self))
        return cAtLoopbackModeLocal;

    if (AuVcRemoteLoopIsEnabled(self))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static uint8 StsLoopbackGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    if (Tha60290011ModuleOcnNewLoopbackIsSupported(self))
        return NewStsLoopbackGet((AtChannel)channel);

    return m_ThaModuleOcnMethods->StsLoopbackGet(self, channel, stsId);
    }

static uint32 TohSonetSdhModeMask(ThaModuleOcn self, uint32 localId)
    {
    AtUnused(self);
    return cAf6_glbsonetsdhmode_TohSonetSdhMode_Line0_Mask << localId;
    }

static uint32 TohSonetSdhModeShift(ThaModuleOcn self, uint32 localId)
    {
    AtUnused(self);
    return cAf6_glbsonetsdhmode_TohSonetSdhMode_Line0_Shift + localId;
    }

static uint32 TohSonetSdhModeAddress(ThaModuleOcn self)
    {
    return cAf6Reg_glbsonetsdhmode_reg_Base + ThaModuleOcnBaseAddress(self);
    }

static eAtRet Ec1LineModeSet(ThaModuleOcn self, uint32 localId, eAtSdhChannelMode mode)
    {
    uint32 regAddr, regVal;
    uint32 tohSonetSdhModeMask = TohSonetSdhModeMask(self, localId);
    uint32 tohSonetSdhModeShift = TohSonetSdhModeShift(self, localId);

    regAddr = TohSonetSdhModeAddress(self);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, tohSonetSdhMode, ThaSdhHwChannelMode(mode));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtSdhChannelMode Ec1LineModeGet(ThaModuleOcn self, uint32 localId)
    {
    uint32 regAddr, regVal;
    uint32 tohSonetSdhModeMask = TohSonetSdhModeMask(self, localId);
    uint32 tohSonetSdhModeShift = TohSonetSdhModeShift(self, localId);

    regAddr = TohSonetSdhModeAddress(self);
    regVal  = mModuleHwRead(self, regAddr);
    return ThaSdhSwChannelMode((uint8)mRegField(regVal, tohSonetSdhMode));
    }

static uint32 RsDccInsControlRegister(ThaModuleOcn self, uint32 lineId)
    {
    uint32 address = ThaModuleOcnBaseAddress(self) + cAf6Reg_txrsdccctl_reg_Base;
    AtUnused(lineId);
    return address;
    }

static uint32 MsDccInsControlRegister(ThaModuleOcn self, uint32 lineId)
    {
    uint32 address = ThaModuleOcnBaseAddress(self) + cAf6Reg_txmsdccctl_reg_Base;
    AtUnused(lineId);
    return address;
    }

static uint32 TxDccEnableMask(ThaModuleOcn self, uint32 lineId)
    {
    return cAf6_txrsdccctl_TxTohBusRsDccEn_line0_Mask << lineId;
    AtUnused(self);
    }

static uint32 TxDccEnableShift(ThaModuleOcn self, uint32 lineId)
    {
    return (cAf6_txrsdccctl_TxTohBusRsDccEn_line0_Shift + lineId);
    AtUnused(self);
    }

static eBool DccIsEnabledCheckWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & TxDccEnableMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static eAtRet LineTxRsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = RsDccInsControlRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              TxDccEnableMask(self, lineId),
              TxDccEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool LineTxRsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = RsDccInsControlRegister(self, lineId);
    return DccIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static eAtRet LineTxMsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = MsDccInsControlRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              TxDccEnableMask(self, lineId),
              TxDccEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool LineTxMsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = MsDccInsControlRegister(self, lineId);
    return DccIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static eBool ShouldEnableLosPpmDetection(Tha60210011ModuleOcn self)
    {
    return Tha60290011DeviceShouldEnableLosPpmDetection(AtModuleDeviceGet((AtModule)self));
    }

static uint32 LineLocalLoopbackRegAddr(Tha60210031ModuleOcn self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAf6Reg_linetslblc_reg;
    }

static uint32 LineRemoteLoopbackRegAddr(Tha60210031ModuleOcn self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAf6Reg_linelbrm_reg;
    }

static void OverrideAtModule(AtModule self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, StartVersionNeedHwRxPiPjAdjThreshold);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, TuVcIsLoopbackAtPointerProcessor);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorNdfIntrMask);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, ShouldEnableLosPpmDetection);
        }

    mMethodsSet(ocnModule, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideTha60210031ModuleOcn(AtModule self)
    {
    Tha60210031ModuleOcn ocnModule = (Tha60210031ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_Tha60210031ModuleOcnOverride));

        mMethodOverride(m_Tha60210031ModuleOcnOverride, LineLocalLoopbackRegAddr);
        mMethodOverride(m_Tha60210031ModuleOcnOverride, LineRemoteLoopbackRegAddr);
        }

    mMethodsSet(ocnModule, &m_Tha60210031ModuleOcnOverride);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthIsSupported);
        mMethodOverride(m_ThaModuleOcnOverride, UpenDccdecBase);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthInit);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthBaseAddress);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthLongRegisterAccessCreate);
        mMethodOverride(m_ThaModuleOcnOverride, IsSohRegister);
        mMethodOverride(m_ThaModuleOcnOverride, KByteOverEthIsSupported);
        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcEnable);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, LineIdSw2HwGet);
        mMethodOverride(m_ThaModuleOcnOverride, StsLoopbackSet);
        mMethodOverride(m_ThaModuleOcnOverride, StsLoopbackGet);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxRsDccInsByOhBusEnable);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxRsDccInsByOhBusEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxMsDccInsByOhBusEnable);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxMsDccInsByOhBusEnabled);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    OverrideTha60210031ModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleOcn);
    }

AtModule Tha60290011ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290011ModuleOcnObjectInit(newModule, device);
    }

eBool Tha60290011ModuleOcnNewLoopbackIsSupported(ThaModuleOcn self)
    {
    return NewLoopbackIsSupported(self);
    }

eBool Tha60290011ModuleOcnTohByteIsSupported(AtSdhChannel self, uint32 tohbyte)
    {
    return TohByteIsSupported(self, tohbyte);
    }

eAtRet Tha60290011ModuleOcnFpgaAuVcLocalLoopEnable(AtChannel self, eBool enable)
    {
    if (self)
        return AuVcLocalLoopEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290011ModuleOcnFpgaAuVcLocalLoopIsEnabled(AtChannel self)
    {
    if (self)
        return AuVcLocalLoopIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha60290011ModuleOcnFpgaAuVcRemoteLoopEnable(AtChannel self, eBool enable)
    {
    if (self)
        return AuVcRemoteLoopEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290011ModuleOcnFpgaAuVcRemoteLoopIsEnabled(AtChannel self)
    {
    if (self)
        return AuVcRemoteLoopIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha60290011ModuleOcnFpgaAuVcTxAisForce(AtChannel self, eBool enable)
    {
    if (self)
        return FpgaAuVcTxAisForce(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290011ModuleOcnFpgaAuVcTxAisIsForced(AtChannel self)
    {
    if (self)
        return FpgaAuVcTxAisIsForced(self);
    return cAtFalse;
    }

eAtRet Tha60290011ModuleOcnFpgaAuVcRxAisForce(AtChannel self, eBool enable)
    {
    if (self)
        return FpgaAuVcRxAisForce(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290011ModuleOcnFpgaAuVcRxAisIsForced(AtChannel self)
    {
    if (self)
        return FpgaAuVcRxAisIsForced(self);
    return cAtFalse;
    }

eAtRet Tha60290011ModuleOcnFpgaLineTxOhByteSet(AtSdhChannel self, uint32 overHeadByteId, uint8 value)
    {
    if (self)
        return LineTxOhByteSet(self, overHeadByteId, value);
    return cAtErrorNullPointer;
    }

uint8 Tha60290011ModuleOcnFpgaLineTxOhByteGet(AtSdhChannel self, uint32 overHeadByteId)
    {
    if (self)
        return LineTxOhByteGet(self, overHeadByteId);
    return 0;
    }

uint8 Tha60290011ModuleOcnFpgaLineRxOhByteGet(AtSdhChannel self, uint32 overHeadByteId)
    {
    if (self)
        return LineRxOhByteGet(self, overHeadByteId);
    return 0;
    }

eAtRet Tha60290011ModuleOcnEc1LineModeSet(ThaModuleOcn self, uint32 lineId, eAtSdhChannelMode mode)
    {
    if (self)
        return Ec1LineModeSet(self, lineId, mode);
    return cAtErrorNullPointer;
    }

eAtSdhChannelMode Tha60290011ModuleOcnEc1LineModeGet(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return Ec1LineModeGet(self, lineId);
    return cAtSdhChannelModeUnknown;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011ModulePohInternal.h
 *
 * Created Date: May 07, 2016
 *
 * Description : 60290011 POH Internal
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPOHINTERNAL_H_
#define _THA60290011MODULEPOHINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/poh/Tha60210031ModulePohInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePoh
    {
    tTha60210031ModulePoh super;
    }tTha60290011ModulePoh;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModulePohObjectInit(AtModule self, AtDevice device);


#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPOHINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290011ModulePoh.c
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : SDH implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePohInternal.h"
#include "Tha60290011PohProcessorInternal.h"
#include "Tha60290011ModulePohReg.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegJnRequestCpu_Mask  cBit15
#define cRegJnRequestCpu_Shift 15
#define cRegJnRequestDdr_Mask  cBit4
#define cRegJnRequestDdr_Shift 4

#define Af6Reg_JnDdrandPohCpuRequestDisableIsDone 0x2
#define cJnDdrandPohCpuRequestDisableIsDone_Mask  cBit11
#define cJnDdrandPohCpuRequestDisableIsDone_Shift 11

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;
static tThaModulePohV2Methods       m_ThaModulePohV2Override;

static const tThaModulePohV2Methods       *m_ThaModulePohV2Methods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60290011SdhLineTtiProcessorNew((AtSdhChannel)line);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290011AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290011Vc1xPohProcessorNew(vc);
    }

static uint32 Reg_pohmsgstsexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 256UL + (stsid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgstsins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 256UL + (stsid) * 8UL + (msgid);
    }

static uint32 Reg_ter_ctrlhi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return (sts) + (ocid) * 32UL;
    }

static uint32 Reg_ter_ctrllo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return (sts) * 28UL + (ocid) * 672UL + (vt);
    }

static uint32 Reg_pohcpestssta(ThaModulePoh self, uint8 sliceid, uint8 stsid)
    {
    AtUnused(self);
    return (sliceid) * 32UL + (stsid);
    }

static uint32 Reg_ipm_cnthi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return (sts) + (ocid) * 32UL;
    }

static uint32 Reg_ipm_cntlo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return (sts) * 28UL + (ocid) * 672UL + (vt);
    }

static eBool PslTtiInterruptIsSupported(ThaModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SoftStickyIsNeeded(ThaModulePoh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportSdSfInterrupt(Tha60210011ModulePoh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportBlockModeCounters(Tha60210011ModulePoh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportPohMonitorEnabling(ThaModulePoh self)
    {
    mVersionReset(self);
    }

static void JnRequestDdrEnable(ThaModulePoh self, eBool enable)
    {
    uint32 regAddress = ThaModulePohBaseAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddress);
    mRegFieldSet(regVal, cRegJnRequestDdr_, (enable) ? 1 : 0);
    mRegFieldSet(regVal, cRegJnRequestCpu_, (enable) ? 1 : 0);
    mModuleHwWrite(self, regAddress, regVal);
    }

static eAtRet JnRequestDdrAndPohCpuDisableIsDoneHwCheck(ThaModulePoh self)
    {
    uint32 address = Af6Reg_JnDdrandPohCpuRequestDisableIsDone + ThaModulePohBaseAddress(self);
    uint32 regVal;
    eBool isDone = cAtFalse;
    tAtOsalCurTime startTime, curTime;
    uint32 deltaTime = 0;
    static uint32 cMaxDeltaTime = 1000;

    if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self)))
        return cAtOk;

    if (AtModuleInAccessible((AtModule)self))
        return cAtOk;

    AtOsalCurTimeGet(&startTime);
    while (!isDone && (deltaTime < cMaxDeltaTime))
        {
        regVal = mModuleHwRead(self, address);
        mFieldIns(&regVal, cJnDdrandPohCpuRequestDisableIsDone_Mask, cJnDdrandPohCpuRequestDisableIsDone_Shift, 1);
        mModuleHwWrite(self, address, regVal);
        AtOsalUSleep(1000);
        regVal = mModuleHwRead(self, address);
        isDone = regVal&cJnDdrandPohCpuRequestDisableIsDone_Mask ? cAtFalse : cAtTrue;
        if (isDone)
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        deltaTime = AtOsalDifferenceTimeInMs(&curTime, &startTime);
        }

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                "Request Jn DDR and disabling CPU access is timeout\r\n");

    return cAtErrorOperationTimeout;
    }

static eBool JnRequestDdrAndPohCpuDisableIsSupported(ThaModulePoh self)
	{
	return Tha60290011DeviceJnRequestDdrAndPohCpuDisableIsSupported(AtModuleDeviceGet((AtModule)self));
	}

static uint32 StartVersionSupportSeparateRdiErdiS(Tha60210011ModulePoh self)
    {
    return Tha60290011DeviceStartVersionSupportSeparateRdiErdiS(AtModuleDeviceGet((AtModule)self));
    }

static uint32 PohMsgInitControlAddress(ThaModulePoh self)
    {
    uint32 regAddress = ThaModulePohBaseAddress(self) + cAf6Reg_pohmsginitctrl_Base;
    return regAddress;
    }

static eAtRet JnDdrInit(ThaModulePohV2 self)
    {
    ThaModulePoh modulePoh = (ThaModulePoh)self;
    uint32 regAddress = PohMsgInitControlAddress(modulePoh);
    mModuleHwWrite(self, regAddress, 0);
    mModuleHwWrite(self, regAddress, 1);

    return Tha60290011JnRequestDdrAndPohCpuDisableIsDoneHwCheck(modulePoh);
    }

static eAtRet DefaultSet(ThaModulePohV2 self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    eAtRet ret = m_ThaModulePohV2Methods->DefaultSet(self);

    if (ret != cAtOk)
        return ret;

    if (Tha60290011DeviceJnDdrInitIsSupported(device))
        return JnDdrInit(self);

    return cAtOk;
    }

static uint32 NumberStsSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 NumberVtSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 1;
    }

static eBool PohInterruptIsSupported(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePohV2(AtModule self)
    {
    ThaModulePohV2 pohModule = (ThaModulePohV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePohV2Methods = mMethodsGet(pohModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohV2Override, m_ThaModulePohV2Methods, sizeof(m_ThaModulePohV2Override));

        mMethodOverride(m_ThaModulePohV2Override, DefaultSet);
        }

    mMethodsSet(pohModule, &m_ThaModulePohV2Override);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, SoftStickyIsNeeded);
        mMethodOverride(m_ThaModulePohOverride, StartVersionSupportPohMonitorEnabling);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, mMethodsGet(pohModule), sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgstsexp);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgstsins);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ter_ctrlhi);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ter_ctrllo);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohcpestssta);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ipm_cnthi);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ipm_cntlo);
        mMethodOverride(m_Tha60210011ModulePohOverride, PslTtiInterruptIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSdSfInterrupt);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportBlockModeCounters);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberStsSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberVtSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, PohInterruptIsSupported);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePohV2(self);
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePoh);
    }

AtModule Tha60290011ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePohObjectInit(newModule, device);
    }

void Tha60290011ModulePohJnRequestDdrEnable(ThaModulePoh self, eBool enable)
    {
    if (self)
        JnRequestDdrEnable(self, enable);
    }

eAtRet Tha60290011JnRequestDdrAndPohCpuDisableIsDoneHwCheck(ThaModulePoh self)
	{
	if (JnRequestDdrAndPohCpuDisableIsSupported(self))
		return JnRequestDdrAndPohCpuDisableIsDoneHwCheck(self);

	return cAtOk;
	}

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290011SdhLineTtiProcessor.c
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : Implementation of Sdh Line TtiProcessor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60290011PohProcessorInternal.h"
#include "Tha60290011ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210031SdhLineTtiProcessorMethods m_Tha60210031SdhLineTtiProcessorrOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint32 PohBufferOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (hwStsInSlice) * 8UL + slice * 256UL + ThaModulePohBaseAddress(ModulePoh(processor));

    return cInvalidUint32;
    }

static uint32 PohCpeOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor poh = (ThaTtiProcessor)self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(poh);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return slice * 48UL + (hwStsInSlice) * 2UL + ThaModulePohBaseAddress(ModulePoh(poh));

    return cInvalidUint32;
    }

static uint32 PohGrabberOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (hwStsInSlice) * 8UL + slice + ThaModulePohBaseAddress(ModulePoh(processor));

    return cInvalidUint32;
    }

static uint32 PohJ0StatusOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return slice * 32UL + hwStsInSlice + ThaModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static uint32 PohJ0StatusRegAddress(Tha60210031SdhLineTtiProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestssta_Base;
    }

static void OverrideTha60210031SdhLineTtiProcessor(ThaTtiProcessor self)
    {
    Tha60210031SdhLineTtiProcessor processor = (Tha60210031SdhLineTtiProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineTtiProcessorrOverride, mMethodsGet(processor), sizeof(m_Tha60210031SdhLineTtiProcessorrOverride));

        mMethodOverride(m_Tha60210031SdhLineTtiProcessorrOverride, PohBufferOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorrOverride, PohCpeOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorrOverride, PohGrabberOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorrOverride, PohJ0StatusOffset);
        mMethodOverride(m_Tha60210031SdhLineTtiProcessorrOverride, PohJ0StatusRegAddress);
        }

    mMethodsSet(processor, &m_Tha60210031SdhLineTtiProcessorrOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideTha60210031SdhLineTtiProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SdhLineTtiProcessor);
    }

static ThaTtiProcessor ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineTtiProcessorObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60290011SdhLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, sdhChannel);
    }

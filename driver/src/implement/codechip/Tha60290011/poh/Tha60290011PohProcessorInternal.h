/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290011PohProcessorInternal.h
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : POH Procecssor Internal Interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011POHPROCESSORINTERNAL_H_
#define _THA60290011POHPROCESSORINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/poh/Tha60210031PohProcessorInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011SdhLineTtiProcessor
    {
    tTha60210031SdhLineTtiProcessor super;
    }tTha60290011SdhLineTtiProcessor;

typedef struct tTha60290011AuVcPohProcessor
    {
    tTha60210031AuVcPohProcessor super;
    }tTha60290011AuVcPohProcessor;

typedef struct tTha60290011Vc1xPohProcessor
    {
    tTha60210031Vc1xPohProcessor super;
    }tTha60290011Vc1xPohProcessor;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaTtiProcessor Tha60290011SdhLineTtiProcessorNew(AtSdhChannel sdhChannel);
ThaPohProcessor Tha60290011AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60290011Vc1xPohProcessorNew(AtSdhVc vc);
void Tha60290011ModulePohJnRequestDdrEnable(ThaModulePoh self, eBool enable);
eAtRet Tha60290011JnRequestDdrAndPohCpuDisableIsDoneHwCheck(ThaModulePoh self);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011POHPROCESSORINTERNAL_H_ */

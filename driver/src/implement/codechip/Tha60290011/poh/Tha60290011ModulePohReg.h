/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_POH_BER_H_
#define _AF6_REG_AF6CNC0011_RD_POH_BER_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : POH Threshold Global Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to set Threshold for stable detection.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_trshglbctr                                                                       0x000003

/*--------------------------------------
BitField Name: jnmsgdebound
BitField Type: RW
BitField Desc: Debound Threshold for Jn 16/64byte
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_jnmsgdebound_Mask                                                       cBit31_28
#define cAf6_pcfg_trshglbctr_jnmsgdebound_Shift                                                             28

/*--------------------------------------
BitField Name: V5RFIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Mask                                                       cBit27_24
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Shift                                                             24

/*--------------------------------------
BitField Name: V5RDIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Mask                                                       cBit23_20
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Shift                                                             20

/*--------------------------------------
BitField Name: V5SlbStbTrsh
BitField Type: RW
BitField Desc: V5 Signal Lable Stable Thershold
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Mask                                                       cBit19_16
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Shift                                                             16

/*--------------------------------------
BitField Name: G1RDIStbTrsh
BitField Type: RW
BitField Desc: G1 RDI Path Stable Thershold
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Mask                                                       cBit15_12
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Shift                                                             12

/*--------------------------------------
BitField Name: C2PlmStbTrsh
BitField Type: RW
BitField Desc: C2 Path Signal Lable Stable Thershold
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Mask                                                        cBit11_8
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Shift                                                              8

/*--------------------------------------
BitField Name: JnStbTrsh
BitField Type: RW
BitField Desc: J1/J2 Message Stable Threshold
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Mask                                                            cBit7_4
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Shift                                                                 4

/*--------------------------------------
BitField Name: debound
BitField Type: RW
BitField Desc: Debound Threshold
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_debound_Mask                                                              cBit3_0
#define cAf6_pcfg_trshglbctr_debound_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Hi-order Path Over Head Grabber
Reg Addr   : 0x02_4000
Reg Formula: 0x02_4000 + $stsid * 8 + $sliceid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-23): STS Identification
Reg Desc   : 
This register is used to grabber Hi-Order Path Overhead

------------------------------------------------------------------------------*/
#define cAf6Reg_pohstspohgrb                                                                          0x024000
#define cAf6Reg_pohstspohgrb_WidthVal                                                                       96

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [67]
--------------------------------------*/
#define cAf6_pohstspohgrb_hlais_Mask                                                                     cBit3
#define cAf6_pohstspohgrb_hlais_Shift                                                                        3

/*--------------------------------------
BitField Name: lom
BitField Type: RO
BitField Desc: LOM  from OCN
BitField Bits: [66]
--------------------------------------*/
#define cAf6_pohstspohgrb_lom_Mask                                                                       cBit2
#define cAf6_pohstspohgrb_lom_Shift                                                                          2

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [65]
--------------------------------------*/
#define cAf6_pohstspohgrb_lop_Mask                                                                       cBit1
#define cAf6_pohstspohgrb_lop_Shift                                                                          1

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [64]
--------------------------------------*/
#define cAf6_pohstspohgrb_ais_Mask                                                                       cBit0
#define cAf6_pohstspohgrb_ais_Shift                                                                          0

/*--------------------------------------
BitField Name: K3
BitField Type: RO
BitField Desc: K3 byte
BitField Bits: [63:56]
--------------------------------------*/
#define cAf6_pohstspohgrb_K3_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_K3_Shift                                                                          24

/*--------------------------------------
BitField Name: F3
BitField Type: RO
BitField Desc: F3 byte
BitField Bits: [55:48]
--------------------------------------*/
#define cAf6_pohstspohgrb_F3_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_F3_Shift                                                                          16

/*--------------------------------------
BitField Name: H4
BitField Type: RO
BitField Desc: H4 byte
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_pohstspohgrb_H4_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_H4_Shift                                                                           8

/*--------------------------------------
BitField Name: F2
BitField Type: RO
BitField Desc: F2 byte
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_pohstspohgrb_F2_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_F2_Shift                                                                           0

/*--------------------------------------
BitField Name: G1
BitField Type: RO
BitField Desc: G1 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohstspohgrb_G1_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_G1_Shift                                                                          24

/*--------------------------------------
BitField Name: C2
BitField Type: RO
BitField Desc: C2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohstspohgrb_C2_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_C2_Shift                                                                          16

/*--------------------------------------
BitField Name: N1
BitField Type: RO
BitField Desc: N1 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohstspohgrb_N1_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_N1_Shift                                                                           8

/*--------------------------------------
BitField Name: J1
BitField Type: RO
BitField Desc: J1 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohstspohgrb_J1_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_J1_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH Lo-order VT/TU3 Over Head Grabber
Reg Addr   : 0x02_6000
Reg Formula: 0x02_6000 + $sliceid*672 + $stsid*28 + $vtid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $vtid(0-27): VT Identification
Reg Desc   : 
This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber.
Incase VT mode, the $vtid = 0-27, using for VT POH grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohvtpohgrb                                                                           0x026000
#define cAf6Reg_pohvtpohgrb_WidthVal                                                                        64

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [35]
--------------------------------------*/
#define cAf6_pohvtpohgrb_hlais_Mask                                                                      cBit3
#define cAf6_pohvtpohgrb_hlais_Shift                                                                         3

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [33]
--------------------------------------*/
#define cAf6_pohvtpohgrb_lop_Mask                                                                        cBit1
#define cAf6_pohvtpohgrb_lop_Shift                                                                           1

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [32]
--------------------------------------*/
#define cAf6_pohvtpohgrb_ais_Mask                                                                     cBit32_0
#define cAf6_pohvtpohgrb_ais_Shift                                                                           0

/*--------------------------------------
BitField Name: Byte3
BitField Type: RO
BitField Desc: G1 byte or K3 byte or K4 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte3_Mask                                                                  cBit31_24
#define cAf6_pohvtpohgrb_Byte3_Shift                                                                        24

/*--------------------------------------
BitField Name: Byte2
BitField Type: RO
BitField Desc: C2 byte or F3 byte or N2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte2_Mask                                                                  cBit23_16
#define cAf6_pohvtpohgrb_Byte2_Shift                                                                        16

/*--------------------------------------
BitField Name: Byte1
BitField Type: RO
BitField Desc: N1 byte or H4 byte or J2 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte1_Mask                                                                   cBit15_8
#define cAf6_pohvtpohgrb_Byte1_Shift                                                                         8

/*--------------------------------------
BitField Name: Byte0
BitField Type: RO
BitField Desc: J1 byte or F2 byte or V5 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte0_Mask                                                                    cBit7_0
#define cAf6_pohvtpohgrb_Byte0_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS/TU3 Control Register
Reg Addr   : 0x02_A000
Reg Formula: 0x02_A000 + $sliceid*48 + $stsid*2 + $tu3en
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-23): STS Identification
           + $tu3en(0-1): Tu3enable, 0: STS, 1:Tu3
Reg Desc   : 
This register is used to configure the POH Hi-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestsctr                                                                          0x02A000

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: PLM enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmEnb_Mask                                                                   cBit19
#define cAf6_pohcpestsctr_PlmEnb_Shift                                                                      19

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pohcpestsctr_VcaisDstren_Mask                                                              cBit18
#define cAf6_pohcpestsctr_VcaisDstren_Shift                                                                 18

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmDstren_Mask                                                                cBit17
#define cAf6_pohcpestsctr_PlmDstren_Shift                                                                   17

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pohcpestsctr_UneqDstren_Mask                                                               cBit16
#define cAf6_pohcpestsctr_UneqDstren_Shift                                                                  16

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren. For slice id 1, this will enable/disable send TIM
alarm to OCN block for generating TIM alarm.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimDstren_Mask                                                                cBit15
#define cAf6_pohcpestsctr_TimDstren_Shift                                                                   15

/*--------------------------------------
BitField Name: Sdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestsctr_Sdhmode_Mask                                                                  cBit14
#define cAf6_pohcpestsctr_Sdhmode_Shift                                                                     14

/*--------------------------------------
BitField Name: Blkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpestsctr_Blkmden_Mask                                                                  cBit13
#define cAf6_pohcpestsctr_Blkmden_Shift                                                                     13

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestsctr_ERDIenb_Mask                                                                  cBit12
#define cAf6_pohcpestsctr_ERDIenb_Shift                                                                     12

/*--------------------------------------
BitField Name: PslExp
BitField Type: RW
BitField Desc: C2 Expected Path Signal Lable Value
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_pohcpestsctr_PslExp_Mask                                                                 cBit11_4
#define cAf6_pohcpestsctr_PslExp_Shift                                                                       4

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimEnb_Mask                                                                    cBit3
#define cAf6_pohcpestsctr_TimEnb_Shift                                                                       3

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpestsctr_Reiblkmden_Mask                                                                cBit2
#define cAf6_pohcpestsctr_Reiblkmden_Shift                                                                   2

/*--------------------------------------
BitField Name: J1mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpestsctr_J1mode_Mask                                                                  cBit1_0
#define cAf6_pohcpestsctr_J1mode_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT Control Register
Reg Addr   : 0x02_8000
Reg Formula: 0x02_8000 + $sliceid*672 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to configure the POH Lo-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtctr                                                                           0x028000

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmEnb_Mask                                                                    cBit14
#define cAf6_pohcpevtctr_PlmEnb_Shift                                                                       14

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtctr_VcaisDstren_Mask                                                               cBit13
#define cAf6_pohcpevtctr_VcaisDstren_Shift                                                                  13

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmDstren_Mask                                                                 cBit12
#define cAf6_pohcpevtctr_PlmDstren_Shift                                                                    12

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pohcpevtctr_UneqDstren_Mask                                                                cBit11
#define cAf6_pohcpevtctr_UneqDstren_Shift                                                                   11

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimDstren_Mask                                                                 cBit10
#define cAf6_pohcpevtctr_TimDstren_Shift                                                                    10

/*--------------------------------------
BitField Name: VSdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pohcpevtctr_VSdhmode_Mask                                                                   cBit9
#define cAf6_pohcpevtctr_VSdhmode_Shift                                                                      9

/*--------------------------------------
BitField Name: VBlkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pohcpevtctr_VBlkmden_Mask                                                                   cBit8
#define cAf6_pohcpevtctr_VBlkmden_Shift                                                                      8

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pohcpevtctr_ERDIenb_Mask                                                                    cBit7
#define cAf6_pohcpevtctr_ERDIenb_Shift                                                                       7

/*--------------------------------------
BitField Name: VslExp
BitField Type: RW
BitField Desc: V5 Expected Path Signal Lable Value
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_pohcpevtctr_VslExp_Mask                                                                   cBit6_4
#define cAf6_pohcpevtctr_VslExp_Shift                                                                        4

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimEnb_Mask                                                                     cBit3
#define cAf6_pohcpevtctr_TimEnb_Shift                                                                        3

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpevtctr_Reiblkmden_Mask                                                                 cBit2
#define cAf6_pohcpevtctr_Reiblkmden_Shift                                                                    2

/*--------------------------------------
BitField Name: J2mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpevtctr_J2mode_Mask                                                                   cBit1_0
#define cAf6_pohcpevtctr_J2mode_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS Status Register
Reg Addr   : 0x02_C2C0
Reg Formula: 0x02_C2C0 + $sliceid*32 + $stsid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-23): STS Identification
Reg Desc   : 
This register is used to get POH Hi-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestssta_Base                                                                          0x02C2C0

/*--------------------------------------
BitField Name: C2stbsta
BitField Type: RO
BitField Desc: C2 stable status
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbsta_Mask                                                                 cBit12
#define cAf6_pohcpestssta_C2stbsta_Shift                                                                    12

/*--------------------------------------
BitField Name: C2stbcnt
BitField Type: RO
BitField Desc: C2 stable counter
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbcnt_Mask                                                               cBit11_8
#define cAf6_pohcpestssta_C2stbcnt_Shift                                                                     8

/*--------------------------------------
BitField Name: C2acpt
BitField Type: RO
BitField Desc: C2 accept byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohcpestssta_C2acpt_Mask                                                                  cBit7_0
#define cAf6_pohcpestssta_C2acpt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT/TU3 Status Register
Reg Addr   : 0x02_C000
Reg Formula: 0x02_C000 + $sliceid*672 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to get POH Lo-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtsta                                                                           0x02C000

/*--------------------------------------
BitField Name: Vslstbsta
BitField Type: RO
BitField Desc: VSL stable status
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbsta_Mask                                                                 cBit13
#define cAf6_pohcpevtsta_Vslstbsta_Shift                                                                    13

/*--------------------------------------
BitField Name: Vslstbcnt
BitField Type: RO
BitField Desc: VSL stable counter
BitField Bits: [12:9]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbcnt_Mask                                                               cBit12_9
#define cAf6_pohcpevtsta_Vslstbcnt_Shift                                                                     9

/*--------------------------------------
BitField Name: Vslacpt
BitField Type: RO
BitField Desc: VSL accept byte
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslacpt_Mask                                                                  cBit8_6
#define cAf6_pohcpevtsta_Vslacpt_Shift                                                                       6

/*--------------------------------------
BitField Name: RFIstatus
BitField Type: RO
BitField Desc: RFI status
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_pohcpevtsta_RFIstatus_Mask                                                                cBit5_0
#define cAf6_pohcpevtsta_RFIstatus_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Expected Message buffer
Reg Addr   : 0x0B_0000
Reg Formula: 0x0B_0000 + $sliceid*256 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-23): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsexp                                                                          0x0B0000

/*--------------------------------------
BitField Name: J1ExpMsg
BitField Type: RW
BitField Desc: J1 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Current Message buffer
Reg Addr   : 0x0B_1000
Reg Formula: 0x0B_1000 + $sliceid*256 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-1): Slice Identification
           + $stsid(0-23): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstscur                                                                          0x0B1000

/*--------------------------------------
BitField Name: J1CurMsg
BitField Type: RW
BitField Desc: J1 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstscur_J1CurMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_01                                                                  0
#define cAf6_pohmsgstscur_J1CurMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Expected Message buffer
Reg Addr   : 0x08_0000
Reg Formula: 0x08_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtexp                                                                           0x080000

/*--------------------------------------
BitField Name: J2ExpMsg
BitField Type: RW
BitField Desc: J2 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Current Message buffer
Reg Addr   : 0x09_0000
Reg Formula: 0x09_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtcur                                                                           0x090000

/*--------------------------------------
BitField Name: J2CurMsg
BitField Type: RW
BitField Desc: J2 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 Insert Message buffer
Reg Addr   : 0x0B_2000
Reg Formula: 0x0B_2000 + $sliceid*256 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsins                                                                          0x0B2000

/*--------------------------------------
BitField Name: J1InsMsg
BitField Type: RW
BitField Desc: J1 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsins_J1InsMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsins_J1InsMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Insert Message buffer
Reg Addr   : 0x0A_0000
Reg Formula: 0x0A_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-0): Slice Identification
           + $stsid(0-23): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Insert Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtins                                                                           0x0A0000

/*--------------------------------------
BitField Name: J2InsMsg
BitField Type: RW
BitField Desc: J2 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtins_J2InsMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtins_J2InsMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control STS
Reg Addr   : 0x04_0400
Reg Formula: 0x04_0400 + $STS + $OCID*32
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-0)  : Line ID
Reg Desc   : 
This register is used to control STS POH insert .

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrlhi                                                                            0x040400

/*--------------------------------------
BitField Name: g1spare
BitField Type: RW
BitField Desc: G1 spare value
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ter_ctrlhi_g1spare_Mask                                                                     cBit6
#define cAf6_ter_ctrlhi_g1spare_Shift                                                                        6

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrlhi_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrlhi_plm_Shift                                                                            5

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrlhi_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrlhi_uneq_Shift                                                                           4

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrlhi_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrlhi_timmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrlhi_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrlhi_aislopmsk_Shift                                                                      2

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrlhi_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrlhi_jnfrmd_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control VT/TU3
Reg Addr   : 0x04_4000
Reg Formula: 0x04_4000 + $STS*28 + $OCID*672 + $VT
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-0)  : Line ID
           + $VT(0-27)
Reg Desc   : 
This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrllo                                                                            0x044000

/*--------------------------------------
BitField Name: k4b0b1
BitField Type: RW
BitField Desc: K4b0b1 value
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4b0b1_Mask                                                                  cBit14_13
#define cAf6_ter_ctrllo_k4b0b1_Shift                                                                        13

/*--------------------------------------
BitField Name: k4aps
BitField Type: RW
BitField Desc: K4aps value
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4aps_Mask                                                                   cBit12_11
#define cAf6_ter_ctrllo_k4aps_Shift                                                                         11

/*--------------------------------------
BitField Name: k4spare
BitField Type: RW
BitField Desc: K4spare value
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4spare_Mask                                                                    cBit10
#define cAf6_ter_ctrllo_k4spare_Shift                                                                       10

/*--------------------------------------
BitField Name: rfival
BitField Type: RW
BitField Desc: RFI value
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ter_ctrllo_rfival_Mask                                                                      cBit9
#define cAf6_ter_ctrllo_rfival_Shift                                                                         9

/*--------------------------------------
BitField Name: vslval
BitField Type: RW
BitField Desc: VT signal label value
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_ter_ctrllo_vslval_Mask                                                                    cBit8_6
#define cAf6_ter_ctrllo_vslval_Shift                                                                         6

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrllo_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrllo_plm_Shift                                                                            5

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrllo_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrllo_uneq_Shift                                                                           4

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrllo_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrllo_timmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrllo_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrllo_aislopmsk_Shift                                                                      2

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrllo_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrllo_jnfrmd_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer STS
Reg Addr   : 0x01_0800
Reg Formula: 0x01_0800 + $OCID*256 + $STS*4 + $BGRP
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-0)  : Line ID
           + $BGRP(0-3)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. %%
BGRP = 0 : G1,J1  %%
BGRP = 1 : N1,C2  %%
BGRP = 2 : H4,F2  %%
BGRP = 3 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbufhi                                                                      0x010800

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbufhi_byte1msk_Shift                                                                17

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (G1/N1/H4/K3)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbufhi_byte1_Shift                                                                    9

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbufhi_byte0msk_Shift                                                                 8

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (J1/C2/F2/F3)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbufhi_byte0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer TU3/VT
Reg Addr   : 0x01_8000
Reg Formula: 0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-0)  : Line ID
           + $VT(0-27)
           + $BGRP(0-1)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%
For VT %%
BGRP = 0 : J2,V5 %%
BGRP = 1 : K4,N2 %%
For TU3 %%
VT = 0, BGRP = 0 : G1,J1 %%
VT = 0, BGRP = 1 : N1,C2 %%
VT = 1, BGRP = 0 : H4,F2 %%
VT = 1, BGRP = 1 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbuflo                                                                      0x018000

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbuflo_byte1msk_Shift                                                                17

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (J2/K4)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbuflo_byte1_Shift                                                                    9

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbuflo_byte0msk_Shift                                                                 8

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (V5/N2)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbuflo_byte0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Global Control
Reg Addr   : 0x06_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable STS,VT,DSN globally.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbenb                                                                           0x060000

/*--------------------------------------
BitField Name: timerenb
BitField Type: RW
BitField Desc: Enable timer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbenb_timerenb_Mask                                                                   cBit3
#define cAf6_pcfg_glbenb_timerenb_Shift                                                                      3

/*--------------------------------------
BitField Name: stsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbenb_stsenb_Mask                                                                     cBit2
#define cAf6_pcfg_glbenb_stsenb_Shift                                                                        2

/*--------------------------------------
BitField Name: vtenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbenb_vtenb_Mask                                                                      cBit1
#define cAf6_pcfg_glbenb_vtenb_Shift                                                                         1

/*--------------------------------------
BitField Name: dsnsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbenb_dsnsenb_Mask                                                                    cBit0
#define cAf6_pcfg_glbenb_dsnsenb_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Code Select Control 0
Reg Addr   : 0x06_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to select error to monitor .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_errsel0                                                                          0x060004

/*--------------------------------------
BitField Name: linecodeen
BitField Type: RW
BitField Desc: 1: Line code violation , 0: Parity code. For
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_pcfg_errsel0_linecodeen_Mask                                                             cBit23_0
#define cAf6_pcfg_errsel0_linecodeen_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Code Select Control 0
Reg Addr   : 0x06_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to select error to monitor .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_errsel1                                                                          0x060005

/*--------------------------------------
BitField Name: linecodeen
BitField Type: RW
BitField Desc: 1: Line code violation , 0: Parity code. For
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_pcfg_errsel1_linecodeen_Mask                                                             cBit23_0
#define cAf6_pcfg_errsel1_linecodeen_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Sticky
Reg Addr   : 0x06_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to check error in BER engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_stkalarm                                                                              0x060001

/*--------------------------------------
BitField Name: stserr
BitField Type: W1C
BitField Desc: STS error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stkalarm_stserr_Mask                                                                        cBit1
#define cAf6_stkalarm_stserr_Shift                                                                           1

/*--------------------------------------
BitField Name: vterr
BitField Type: W1C
BitField Desc: VT error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stkalarm_vterr_Mask                                                                         cBit0
#define cAf6_stkalarm_vterr_Shift                                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 1
Reg Addr   : 0x06_2300
Reg Formula: 0x06_2300 + $Rate
    Where  : 
           + $Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,(0-16)...VT1.5,VT2,DS1,E1(65,67,69,71)....
Reg Desc   : 
This register is used to configure threshold of BER level 3.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh1                                                                          0x062300

/*--------------------------------------
BitField Name: setthres
BitField Type: RW
BitField Desc: SetThreshold
BitField Bits: [18:9]
--------------------------------------*/
#define cAf6_imemrwptrsh1_setthres_Mask                                                               cBit18_9
#define cAf6_imemrwptrsh1_setthres_Shift                                                                     9

/*--------------------------------------
BitField Name: winthres
BitField Type: RW
BitField Desc: WindowThreshold
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_imemrwptrsh1_winthres_Mask                                                                cBit8_0
#define cAf6_imemrwptrsh1_winthres_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 2
Reg Addr   : 0x06_0400
Reg Formula: 0x06_0400 + $Rate*8 + $Thresloc
    Where  : 
           + $Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....
           + $Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8
Reg Desc   : 
This register is used to configure threshold of BER level 4 to level 8.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh2                                                                          0x060400
#define cAf6Reg_imemrwptrsh2_WidthVal                                                                       64

/*--------------------------------------
BitField Name: scwthres1
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [33:17]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres1_Mask_01                                                          cBit31_17
#define cAf6_imemrwptrsh2_scwthres1_Shift_01                                                                17
#define cAf6_imemrwptrsh2_scwthres1_Mask_02                                                            cBit1_0
#define cAf6_imemrwptrsh2_scwthres1_Shift_02                                                                 0

/*--------------------------------------
BitField Name: scwthres2
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres2_Mask                                                              cBit16_0
#define cAf6_imemrwptrsh2_scwthres2_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control VT/DSN
Reg Addr   : 0x06_2000
Reg Formula: 0x06_2000 + $STS*32 + $OCID*8+ $VTG
    Where  : 
           + $STS(0-23): STS
           + $OCID(0-3)  : Line ID, 0-1: VT, 2-3: DE1
           + $VTG(0-6)   : VT group
Reg Desc   : 
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl1                                                                          0x062000
#define cAf6Reg_imemrwpctrl1_WidthVal                                                                       64

/*--------------------------------------
BitField Name: tcatrsh4
BitField Type: RW
BitField Desc: TCA threshold raise channel 4
BitField Bits: [45:43]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh4_Mask                                                              cBit13_11
#define cAf6_imemrwpctrl1_tcatrsh4_Shift                                                                    11

/*--------------------------------------
BitField Name: tcatrsh3
BitField Type: RW
BitField Desc: TCA threshold raise channel 3
BitField Bits: [42:40]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh3_Mask                                                               cBit10_8
#define cAf6_imemrwpctrl1_tcatrsh3_Shift                                                                     8

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl1_tcatrsh2_Shift                                                                     5

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl1_tcatrsh1_Shift                                                                     2

/*--------------------------------------
BitField Name: etype4
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 3
BitField Bits: [31]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype4_Mask                                                                   cBit31
#define cAf6_imemrwpctrl1_etype4_Shift                                                                      31

/*--------------------------------------
BitField Name: sftrsh4
BitField Type: RW
BitField Desc: SF threshold raise channel 3
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh4_Mask                                                               cBit30_28
#define cAf6_imemrwpctrl1_sftrsh4_Shift                                                                     28

/*--------------------------------------
BitField Name: sdtrsh4
BitField Type: RW
BitField Desc: SD threshold raise channel 3
BitField Bits: [27:25]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh4_Mask                                                               cBit27_25
#define cAf6_imemrwpctrl1_sdtrsh4_Shift                                                                     25

/*--------------------------------------
BitField Name: ena4
BitField Type: RW
BitField Desc: Enable channel 3
BitField Bits: [24]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena4_Mask                                                                     cBit24
#define cAf6_imemrwpctrl1_ena4_Shift                                                                        24

/*--------------------------------------
BitField Name: etype3
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 2
BitField Bits: [23]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype3_Mask                                                                   cBit23
#define cAf6_imemrwpctrl1_etype3_Shift                                                                      23

/*--------------------------------------
BitField Name: sftrsh3
BitField Type: RW
BitField Desc: SF threshold raise channel 2
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh3_Mask                                                               cBit22_20
#define cAf6_imemrwpctrl1_sftrsh3_Shift                                                                     20

/*--------------------------------------
BitField Name: sdtrsh3
BitField Type: RW
BitField Desc: SD threshold raise channel 2
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh3_Mask                                                               cBit19_17
#define cAf6_imemrwpctrl1_sdtrsh3_Shift                                                                     17

/*--------------------------------------
BitField Name: ena3
BitField Type: RW
BitField Desc: Enable channel 2
BitField Bits: [16]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena3_Mask                                                                     cBit16
#define cAf6_imemrwpctrl1_ena3_Shift                                                                        16

/*--------------------------------------
BitField Name: etype2
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype2_Mask                                                                   cBit15
#define cAf6_imemrwpctrl1_etype2_Shift                                                                      15

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh2_Mask                                                               cBit14_12
#define cAf6_imemrwpctrl1_sftrsh2_Shift                                                                     12

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh2_Mask                                                                cBit11_9
#define cAf6_imemrwpctrl1_sdtrsh2_Shift                                                                      9

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [8]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena2_Mask                                                                      cBit8
#define cAf6_imemrwpctrl1_ena2_Shift                                                                         8

/*--------------------------------------
BitField Name: etype1
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 0
BitField Bits: [7]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype1_Mask                                                                    cBit7
#define cAf6_imemrwpctrl1_etype1_Shift                                                                       7

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl1_sftrsh1_Shift                                                                      4

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl1_sdtrsh1_Shift                                                                      1

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl1_ena1_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control STS/TU3
Reg Addr   : 0x06_2007
Reg Formula: 0x06_2007 + $STS*32 + $OCID*8
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3)  : Line ID, Line ID 0-1: STS, Line ID 2-3: Line EC1 channel 1, DE3 channel 2
Reg Desc   : 
This register is used to enable and set threshold SD SF.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl2                                                                          0x062007
#define cAf6Reg_imemrwpctrl2_WidthVal                                                                       64

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl2_tcatrsh2_Shift                                                                     5

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl2_tcatrsh1_Shift                                                                     2

/*--------------------------------------
BitField Name: rate2
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate2_Mask                                                                 cBit27_21
#define cAf6_imemrwpctrl2_rate2_Shift                                                                       21

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh2_Mask                                                               cBit20_18
#define cAf6_imemrwpctrl2_sftrsh2_Shift                                                                     18

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh2_Mask                                                               cBit17_15
#define cAf6_imemrwpctrl2_sdtrsh2_Shift                                                                     15

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [14]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena2_Mask                                                                     cBit14
#define cAf6_imemrwpctrl2_ena2_Shift                                                                        14

/*--------------------------------------
BitField Name: rate1
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate1_Mask                                                                  cBit13_7
#define cAf6_imemrwpctrl2_rate1_Shift                                                                        7

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl2_sftrsh1_Shift                                                                      4

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl2_sdtrsh1_Shift                                                                      1

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl2_ena1_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report VT/DSN
Reg Addr   : 0x06_8000
Reg Formula: 0x06_8000 + $STS*112 + $OCID*28 + $VT
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3)     : Line ID, VT:0-1,DSN:2-3
           + $VT(0-27)   : VT/DS1 ID
Reg Desc   : 
This register is used to get current BER rate .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberratevtds                                                                        0x068000

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberratevtds_hwsta_Mask                                                                   cBit3
#define cAf6_ramberratevtds_hwsta_Shift                                                                      3

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberratevtds_rate_Mask                                                                  cBit2_0
#define cAf6_ramberratevtds_rate_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report STS/TU3
Reg Addr   : 0x06_8B00
Reg Formula: 0x06_8B00 + $STS*8 + $OCID*2 + $TU3TYPE
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3)     : Line ID,  Line ID 0-1: STS, Line ID 2-3: Line EC1/DE3
           + $TU3TYPE(0-1)  : Type TU3:1, STS:0
Reg Desc   : 
This register is used to get current BER rate . BER DE3 used with OCID 2-3, TU3TYPE = 1. BER EC1 used with OCID 2-3, TU3TYPE = 0.

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberrateststu3                                                                      0x068B00

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberrateststu3_hwsta_Mask                                                                 cBit3
#define cAf6_ramberrateststu3_hwsta_Shift                                                                    3

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberrateststu3_rate_Mask                                                                cBit2_0
#define cAf6_ramberrateststu3_rate_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : POH BIP Counter Report STS
Reg Addr   : 0x0C_82C0
Reg Formula: 0x0C_82C0 + $STS + $OCID*32
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-1)     : Line ID
Reg Desc   : 
This register is used to get POH BIP Counter. OCID 1 is used for B2 counter with 24 B2 each OCID.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_bipcnthi                                                                          0x0C82C0

/*--------------------------------------
BitField Name: bipcnt
BitField Type: W0C
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_bipcnthi_bipcnt_Mask                                                                 cBit15_0
#define cAf6_ipm_bipcnthi_bipcnt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH BIP Counter Report TU3/VT
Reg Addr   : 0x0C_8000
Reg Formula: 0x0C_8000 + $STS*28 + $OCID*672 + $VT
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-0)   : Line ID
           + $VT(0-27)   : VT ID
Reg Desc   : 
This register is used to get POH BIP Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_bipcntlo                                                                          0x0C8000

/*--------------------------------------
BitField Name: bipcnt
BitField Type: W0C
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_bipcntlo_bipcnt_Mask                                                                 cBit15_0
#define cAf6_ipm_bipcntlo_bipcnt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH REI Counter Report STS
Reg Addr   : 0x0C_C2C0
Reg Formula: 0x0C_C2C0 + $STS + $OCID*32
    Where  : 
           + $STS(0-24)  : STS
           + $OCID(0-1)     : Line ID
Reg Desc   : 
This register is used to get POH REI Counter. OCID 1 is used for B2 counter with 24 B2 each OCID.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_reicnthi                                                                          0x0CC2C0

/*--------------------------------------
BitField Name: reicnt
BitField Type: W0C
BitField Desc: REI counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_reicnthi_reicnt_Mask                                                                 cBit15_0
#define cAf6_ipm_reicnthi_reicnt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH REI Counter Report TU3/VT
Reg Addr   : 0x0C_C000
Reg Formula: 0x0C_C000 + $STS*28 + $OCID*672 + $VT
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-0)   : Line ID
           + $VT(0-27)   : VT ID
Reg Desc   : 
This register is used to get POH REI Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_reicntlo                                                                          0x0CC000

/*--------------------------------------
BitField Name: reicnt
BitField Type: W0C
BitField Desc: REI counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_reicntlo_reicnt_Mask                                                                 cBit15_0
#define cAf6_ipm_reicntlo_reicnt_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report STS
Reg Addr   : 0x0D_0000
Reg Formula: 0x0D_0000 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_mskhi                                                                             0x0D0000

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_mskhi_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_mskhi_bersdmsk_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_mskhi_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_mskhi_bersfmsk_Shift                                                                       10

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_mskhi_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_mskhi_erdimsk_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_mskhi_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_mskhi_bertcamsk_Shift                                                                       8

/*--------------------------------------
BitField Name: jnstbmsk
BitField Type: RW
BitField Desc: jn stable mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_mskhi_jnstbmsk_Mask                                                                     cBit7
#define cAf6_alm_mskhi_jnstbmsk_Shift                                                                        7

/*--------------------------------------
BitField Name: pslstbmsk
BitField Type: RW
BitField Desc: psl stable mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_mskhi_pslstbmsk_Mask                                                                    cBit6
#define cAf6_alm_mskhi_pslstbmsk_Shift                                                                       6

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi/lom mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_mskhi_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_mskhi_rfimsk_Shift                                                                          5

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_mskhi_timmsk_Mask                                                                       cBit4
#define cAf6_alm_mskhi_timmsk_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_mskhi_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_mskhi_uneqmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_mskhi_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_mskhi_plmmsk_Shift                                                                          2

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_mskhi_aismsk_Mask                                                                       cBit1
#define cAf6_alm_mskhi_aismsk_Shift                                                                          1

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_mskhi_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_mskhi_lopmsk_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report STS
Reg Addr   : 0x0D_0040
Reg Formula: 0x0D_0040 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23
Reg Desc   : 
This register is used to get POH alarm status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stahi                                                                             0x0D0040

/*--------------------------------------
BitField Name: bersdsta
BitField Type: W1C
BitField Desc: bersd  status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stahi_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stahi_bersdsta_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfsta
BitField Type: W1C
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stahi_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stahi_bersfsta_Shift                                                                       10

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdi status status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stahi_erdista_Mask                                                                      cBit9
#define cAf6_alm_stahi_erdista_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stahi_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stahi_bertcasta_Shift                                                                       8

/*--------------------------------------
BitField Name: jnstbsta
BitField Type: RO
BitField Desc: jn stable status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stahi_jnstbsta_Mask                                                                     cBit7
#define cAf6_alm_stahi_jnstbsta_Shift                                                                        7

/*--------------------------------------
BitField Name: pslstbsta
BitField Type: RO
BitField Desc: psl stable status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stahi_pslstbsta_Mask                                                                    cBit6
#define cAf6_alm_stahi_pslstbsta_Shift                                                                       6

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi/lom status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stahi_rfista_Mask                                                                       cBit5
#define cAf6_alm_stahi_rfista_Shift                                                                          5

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stahi_timsta_Mask                                                                       cBit4
#define cAf6_alm_stahi_timsta_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stahi_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stahi_uneqsta_Shift                                                                         3

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stahi_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stahi_plmsta_Shift                                                                          2

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stahi_aissta_Mask                                                                       cBit1
#define cAf6_alm_stahi_aissta_Shift                                                                          1

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stahi_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stahi_lopsta_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0D_0020
Reg Formula: 0x0D_0020 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chghi                                                                             0x0D0020

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chghi_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chghi_bersdstachg_Shift                                                                    11

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chghi_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chghi_bersfstachg_Shift                                                                    10

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chghi_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chghi_erdistachg_Shift                                                                      9

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chghi_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chghi_bertcastachg_Shift                                                                    8

/*--------------------------------------
BitField Name: jnstbstachg
BitField Type: W1C
BitField Desc: jn stable status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chghi_jnstbstachg_Mask                                                                  cBit7
#define cAf6_alm_chghi_jnstbstachg_Shift                                                                     7

/*--------------------------------------
BitField Name: pslstbstachg
BitField Type: W1C
BitField Desc: psl stable status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chghi_pslstbstachg_Mask                                                                 cBit6
#define cAf6_alm_chghi_pslstbstachg_Shift                                                                    6

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi/lom status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chghi_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chghi_rfistachg_Shift                                                                       5

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chghi_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chghi_timstachg_Shift                                                                       4

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chghi_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chghi_uneqstachg_Shift                                                                      3

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chghi_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chghi_plmstachg_Shift                                                                       2

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chghi_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chghi_aisstachg_Shift                                                                       1

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chghi_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chghi_lopstachg_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report STS
Reg Addr   : 0x0D_007F
Reg Formula: 0x0D_007F + $OCID*128
    Where  : 
           + $OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchghi                                                                          0x0D007F

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchghi_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchghi_glbstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report STS
Reg Addr   : 0x0D_007E
Reg Formula: 0x0D_007E + $OCID*128
    Where  : 
           + $OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmskhi                                                                          0x0D007E

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global mask
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmskhi_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmskhi_glbmsk_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report VT/TU3
Reg Addr   : 0x0E_0000
Reg Formula: 0x0E_0000 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-1) : Line ID, ID 0,1 for path STS 0-23
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_msklo                                                                             0x0E0000

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_msklo_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_msklo_bersdmsk_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_msklo_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_msklo_bersfmsk_Shift                                                                       10

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_msklo_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_msklo_erdimsk_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_msklo_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_msklo_bertcamsk_Shift                                                                       8

/*--------------------------------------
BitField Name: jnstbmsk
BitField Type: RW
BitField Desc: jn stable mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_msklo_jnstbmsk_Mask                                                                     cBit7
#define cAf6_alm_msklo_jnstbmsk_Shift                                                                        7

/*--------------------------------------
BitField Name: pslstbmsk
BitField Type: RW
BitField Desc: psl stable mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_msklo_pslstbmsk_Mask                                                                    cBit6
#define cAf6_alm_msklo_pslstbmsk_Shift                                                                       6

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_msklo_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_msklo_rfimsk_Shift                                                                          5

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_msklo_timmsk_Mask                                                                       cBit4
#define cAf6_alm_msklo_timmsk_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_msklo_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_msklo_uneqmsk_Shift                                                                         3

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_msklo_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_msklo_plmmsk_Shift                                                                          2

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_msklo_aismsk_Mask                                                                       cBit1
#define cAf6_alm_msklo_aismsk_Shift                                                                          1

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_msklo_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_msklo_lopmsk_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report VT/TU3
Reg Addr   : 0x0E_0800
Reg Formula: 0x0E_0800 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-1) : Line ID, ID 0,1 for path STS 0-23
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stalo                                                                             0x0E0800

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RW
BitField Desc: bersd status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stalo_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stalo_bersdsta_Shift                                                                       11

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RW
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stalo_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stalo_bersfsta_Shift                                                                       10

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdi status status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stalo_erdista_Mask                                                                      cBit9
#define cAf6_alm_stalo_erdista_Shift                                                                         9

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stalo_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stalo_bertcasta_Shift                                                                       8

/*--------------------------------------
BitField Name: jnstbsta
BitField Type: RO
BitField Desc: jn stable status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stalo_jnstbsta_Mask                                                                     cBit7
#define cAf6_alm_stalo_jnstbsta_Shift                                                                        7

/*--------------------------------------
BitField Name: pslstbsta
BitField Type: RO
BitField Desc: psl stable status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stalo_pslstbsta_Mask                                                                    cBit6
#define cAf6_alm_stalo_pslstbsta_Shift                                                                       6

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stalo_rfista_Mask                                                                       cBit5
#define cAf6_alm_stalo_rfista_Shift                                                                          5

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stalo_timsta_Mask                                                                       cBit4
#define cAf6_alm_stalo_timsta_Shift                                                                          4

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stalo_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stalo_uneqsta_Shift                                                                         3

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stalo_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stalo_plmsta_Shift                                                                          2

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stalo_aissta_Mask                                                                       cBit1
#define cAf6_alm_stalo_aissta_Shift                                                                          1

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stalo_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stalo_lopsta_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0E_0400
Reg Formula: 0x0E_0400 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-1) : Line ID, ID 0,1 for path STS 0-23
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chglo                                                                             0x0E0400

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chglo_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chglo_bersdstachg_Shift                                                                    11

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chglo_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chglo_bersfstachg_Shift                                                                    10

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chglo_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chglo_erdistachg_Shift                                                                      9

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chglo_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chglo_bertcastachg_Shift                                                                    8

/*--------------------------------------
BitField Name: jnstbstachg
BitField Type: W1C
BitField Desc: jn stable status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chglo_jnstbstachg_Mask                                                                  cBit7
#define cAf6_alm_chglo_jnstbstachg_Shift                                                                     7

/*--------------------------------------
BitField Name: pslstbstachg
BitField Type: W1C
BitField Desc: psl stable status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chglo_pslstbstachg_Mask                                                                 cBit6
#define cAf6_alm_chglo_pslstbstachg_Shift                                                                    6

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chglo_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chglo_rfistachg_Shift                                                                       5

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chglo_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chglo_timstachg_Shift                                                                       4

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chglo_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chglo_uneqstachg_Shift                                                                      3

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chglo_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chglo_plmstachg_Shift                                                                       2

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chglo_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chglo_aisstachg_Shift                                                                       1

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chglo_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chglo_lopstachg_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Or Status Report VT/TU3
Reg Addr   : 0x0E_0C00
Reg Formula: 0x0E_0C00 + $STS + $OCID*4096
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-1) : Line ID, ID 0,1 for path STS 0-23
Reg Desc   : 
This register is used to get POH alarm or status change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_orstalo                                                                           0x0E0C00

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or status change bit
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_alm_orstalo_orstachg_Mask                                                                cBit27_0
#define cAf6_alm_orstalo_orstachg_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report VT/TU3
Reg Addr   : 0x0E_0FFF
Reg Formula: 0x0E_0FFF + $OCID*4096
    Where  : 
           + $OCID(0-1) : Line ID, ID 0,1 for path STS 0-23
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchglo                                                                          0x0E0FFF

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchglo_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchglo_glbstachg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report VT/TU3
Reg Addr   : 0x0E_0FFE
Reg Formula: 0x0E_0FFE + $OCID*4096
    Where  : 
           + $OCID(0-1) : Line ID, ID 0,1 for path STS 0-23
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsklo                                                                          0x0E0FFE

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmsklo_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmsklo_glbmsk_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global mask report for high,low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsk                                                                            0x000004

/*--------------------------------------
BitField Name: glbmsklo
BitField Type: RW
BitField Desc: global mask bit for low order slice - ocid1,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbmsk_glbmsklo_Mask                                                                cBit27_16
#define cAf6_alm_glbmsk_glbmsklo_Shift                                                                      16

/*--------------------------------------
BitField Name: glbmskhi
BitField Type: RW
BitField Desc: global mask change bit for high order slice -
ocid3,ocid1,ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbmsk_glbmskhi_Mask                                                                 cBit15_0
#define cAf6_alm_glbmsk_glbmskhi_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global change status report for high,low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchg                                                                            0x000005

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order slice - ocid1,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbchg_glbstachglo_Mask                                                             cBit27_16
#define cAf6_alm_glbchg_glbstachglo_Shift                                                                   16

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order slice -
ocid3,ocid1,ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbchg_glbstachghi_Mask                                                              cBit15_0
#define cAf6_alm_glbchg_glbstachghi_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt  Global Status Out Report
Reg Addr   : 0x00_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global change status report for high,low order after ANDED with mask.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchgo                                                                           0x000006

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order slice - ocid1,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbchgo_glbstachglo_Mask                                                            cBit27_16
#define cAf6_alm_glbchgo_glbstachglo_Shift                                                                  16

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order slice -
ocid3,ocid1,ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbchgo_glbstachghi_Mask                                                             cBit15_0
#define cAf6_alm_glbchgo_glbstachghi_Shift                                                                   0

/*------------------------------------------------
POH Message Initial Control
 Register Full Name: POH Message Initial Control
 RTL Instant Name  : pohmsginitctrl
 Address      : 0x00_001f
 Formula      : {N/A}
 Description  : This register is used to get POH alarm global change status report for high,low order after ANDED with mask.
 Width: 32
 Field: [31:3] %%  Unused %%  Unused %% RW %% 0xX %% 0xX
 Field: [2]  %%  initmd          %% Init mode (1/0): 64bytes/16bytes  %% RW %% 0x0  %% 0x0
 Field: [1]  %%  parerrfrc       %% parity error forcing (1/0). write 0 then write 1 to force 1 error when engine or CPU write to DDR   %% RW %% 0x0  %% 0x0
 Field: [0]  %%  init          %% init bit. write 1 to init all DDR Jn, if want to init again, write 0 then write 1.  %% RW %% 0x0  %% 0x0
--------------------------------------------------*/
#define cAf6Reg_pohmsginitctrl_Base                                                                        0x1F

#define cAf6_pohmsginitctrl_JnDddr_init_Mask                                                               cBit0
#define cAf6_pohmsginitctrl_JnDddr_init_Shift                                                              0

#define cAf6_pohmsginitctrl_JnDddr_parerrfrc_Mask                                                          cBit1
#define cAf6_pohmsginitctrl_JnDddr_parerrfrc_Shift                                                         1

#define cAf6_pohmsginitctrl_JnDddr_initmd_Mask                                                             cBit2
#define cAf6_pohmsginitctrl_JnDddr_initmd_Shift                                                            2


#endif /* _AF6_REG_AF6CNC0011_RD_POH_BER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290011AuVcPohProcessor.c
 *
 * Created Date: Oct 6, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011PohProcessorInternal.h"
#include "Tha60290011ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPohProcesser(self) ((Tha60210011AuVcPohProcessor)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PohInternalCounterGet(Tha60210011AuVcPohProcessor self)
	{
	/* REI counter */
	uint32 regAddr = cAf6Reg_ipm_reicnthi + mMethodsGet(self)->CounterRegOffset(self);
	uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
	mChannelHwWrite(self, regAddr, 0, cThaModulePoh);
	self->reiCounter = self->reiCounter + mRegField(regVal, cAf6_ipm_reicnthi_reicnt_);

	/* BIP counter */
	regAddr = cAf6Reg_ipm_bipcnthi + mMethodsGet(self)->CounterRegOffset(self);
	regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
	mChannelHwWrite(self, regAddr, 0, cThaModulePoh);
	self->bipCounter = self->bipCounter + mRegField(regVal, cAf6_ipm_bipcnthi_bipcnt_);

	return cAtOk;
	}

static eAtRet PohCounterGet(Tha60210011AuVcPohProcessor self)
	{
	return PohInternalCounterGet(self);
	}

static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return 0x02C2C0;
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor path = (Tha60210011AuVcPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, mMethodsGet(path), sizeof(m_Tha60210011AuVcPohProcessorOverride));
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCounterGet);
        }

    mMethodsSet(path, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011AuVcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60290011AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290011ModulePmc.c
 *
 * Created Date: Dec 10, 2016
 *
 * Description : PMC module concrete class for 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePmcInternal.h"
#include "Tha60290011ModulePmc.h"
#include "Tha60290011ModulePmcVer2Reg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPmcBaseAddress		0xE00000
#define cAf6Reg_upen_poh_pmr_cnt1_rw_Base 0x91880

/* Classify(CLS) for the PDH MUX interface */
/* Classifier Bytes Counter vld */
#define cThaClaTypeDe1Bytes                     1
#define cThaClaTypeDe3Ec1Bytes                  0
#define cThaClaTypeControlBytes                 2
/* Classifier output counter pkt */
#define cThaClaTxTypeDe1GoodPacket              0
#define cThaClaTxTypeDe3Ec1GoodPacket           1
#define cThaClaTxTypeControlGoodPacket          2
#define cThaClaTxTypeSequenceErrPacket          3
#define cThaClaTxTypeFcsErrPacket               4
#define cThaClaTxTypeLenErrPacket               5
#define cThaClaTxTypeDaErrPacket                6
#define cThaClaTxTypeSaErrPacket                7
#define cThaClaTxTypeEthErrPacket               8
#define cThaClaTxTypeLenFieldErrPacket          9

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290011ModulePmc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011ModulePmcMethods m_methods;

/* Override */
static tThaModulePmcMethods m_ThaModulePmcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaModulePmc self, uint8 partId)
	{
	AtUnused(self);
	AtUnused(partId);
	return cThaPmcBaseAddress;
	}

static uint32 PwCounterOffset(ThaModulePmc self, AtPw pw, uint32 cntIndex, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return AtChannelIdGet((AtChannel)pw) + (1024UL * cntIndex);
    }

static uint32 Reg_upen_cnttxoam_rc_Address(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cnttxoam_Base;
    }

static uint32 Reg_MACGE_tx_type_packet_Address(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_ge00lltx_typepkt_rw_Base;
    }

static uint32 Reg_upen_cla0_ro_Address(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cla0_rw_Base;
    }

static uint32 LineCounter2Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_pmr_cnt1_rw_Base;
    }

static uint32 LineCounterOffset(ThaModulePmc self, ThaSdhLine line, uint32 cntIndex, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return AtChannelIdGet((AtChannel)line) + 24UL * cntIndex;
    }

static uint32 AuVcDefaultOffset(ThaModulePmc self, AtSdhChannel channel)
    {
    uint8 slice, hwStsInSlice;
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(hwStsInSlice);

    return cBit31_0;
    }

static uint32 Vc1xDefaultOffset(ThaModulePmc self, AtSdhChannel sdhChannel)
    {
    uint8 slice, hwStsInSlice;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)((32UL * hwStsInSlice) + (4UL * vtgId) + vtId);

    return cBit31_0;
    }

static uint32 De3DefaultOffset(ThaModulePmc self, AtPdhChannel de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);

    if (ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice) == cAtOk)
        return hwIdInSlice;

    return cBit31_0;
    }

static uint32 De1DefaultOffset(ThaModulePmc self, AtPdhChannel de1)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    AtUnused(self);

    if (ThaPdhDe1HwIdGet((ThaPdhDe1)de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt) == cAtOk)
        return ((32UL * hwIdInSlice) + (4UL * vtg) + vt);

    return cBit31_0;
    }

static uint32 CounterRead2Clear(Tha60290011ModulePmc self, uint32 address, eBool clear)
    {
    ThaModulePmc module = (ThaModulePmc)self;
    return mMethodsGet(module)->HwCounterRead2Clear(module, address, clear);
    }

static uint32 HwClsByteCounterAddress(Tha60290011ModulePmc self, uint32 offset, uint8 sliceId, uint8 cntType, eBool clear)
    {
    uint32 address = offset + ThaModulePmcBaseAddress((ThaModulePmc)self) + sliceId * 3UL + cntType;
    AtUnused(clear);
    return address;
    }

static uint32 ClsTxDe1BytesGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwClsByteCounterAddress(self, cAf6Reg_upen_cls_bcnt_rw_Base, sliceId, cThaClaTypeDe1Bytes, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 ClsTxDe3BytesGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwClsByteCounterAddress(self, cAf6Reg_upen_cls_bcnt_rw_Base, sliceId, cThaClaTypeDe3Ec1Bytes, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 ClsTxControlPacketBytesGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwClsByteCounterAddress(self, cAf6Reg_upen_cls_bcnt_rw_Base, sliceId, cThaClaTypeControlBytes, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 HwClsPacketTxCounterAddress(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, uint8 cntType, eBool clear)
    {
    uint32 address = localAddress + (ThaModulePmcBaseAddress((ThaModulePmc)self) + sliceId * 0xAUL + cntType);
    AtUnused(clear);
    return address;
    }

static uint32 ClsPacketTxCounter(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, uint8 cntType, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwClsPacketTxCounterAddress(self, localAddress, sliceId, cntType, clear);
    uint32 counterVal = CounterRead2Clear(self, address, clear);
    return (counterVal &  cAf6_upen_cls_opcnt0_rw_opktcnt0_opktcnt1__Mask);
    }

static uint32 ClsTxDe1GoodPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeDe1GoodPacket, clear);
    }

static uint32 ClsTxDe3GoodPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeDe3Ec1GoodPacket, clear);
    }

static uint32 ClsTxControlGoodPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeControlGoodPacket, clear);
    }

static uint32 ClsTxDAErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeDaErrPacket, clear);
    }

static uint32 ClsTxSAErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeSaErrPacket, clear);
    }

static uint32 ClsTxEthTypeErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeEthErrPacket, clear);
    }

static uint32 ClsTxLenErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeLenErrPacket, clear);
    }

static uint32 ClsTxLenFieldErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeLenFieldErrPacket, clear);
    }

static uint32 ClsTxFcsErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeFcsErrPacket, clear);
    }

static uint32 ClsTxSequenceErrorPacketCounterAddress(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, eBool clear)
    {
    return HwClsPacketTxCounterAddress(self, localAddress, sliceId, cThaClaTxTypeSequenceErrPacket, clear);
    }

static uint32 ClsTxSequenceErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->ClsTxSequenceErrorPacketCounterAddress(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, clear);
    return CounterRead2Clear(self, address, clear) & cAf6_upen_cls_opcnt0_rw_opktcnt0_opktcnt1__Mask;
    }

static uint32 ClsRxPacketCounterAddress(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = ThaModulePmcBaseAddress((ThaModulePmc)self) + cAf6Reg_upen_cls_ipcnt_rw_Base + (sliceId * 0x2UL);
    AtUnused(clear);
    return address;
    }

static uint32 ClsRxPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->ClsRxPacketCounterAddress(self, sliceId, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PdhMuxPacketCounterAddress(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = ThaModulePmcBaseAddress((ThaModulePmc)self) + (sliceId * 2UL) + cAf6Reg_upen_eth_pckt_rw_Base;
    AtUnused(clear);
    return address;
    }

static uint32 PdhMuxPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->PdhMuxPacketCounterAddress(self, sliceId, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 HwPdhMuxByteCounterAddress(Tha60290011ModulePmc self, uint32 offset, uint8 sliceId, eBool clear)
    {
    uint32 address = offset + ThaModulePmcBaseAddress((ThaModulePmc)self) + (sliceId * 2UL);
    AtUnused(clear);
    return address;
    }

static uint32 PdhMuxBytesGet(Tha60290011ModulePmc self, uint8 slice, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwPdhMuxByteCounterAddress(self, cAf6Reg_upen_eth_bytecnt_rw_Base, slice, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PdhMuxBitAddress(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear)
    {
    uint32 address = localAddress + ThaModulePmcBaseAddress((ThaModulePmc)self) + channelId;
    AtUnused(clear);
    return address;
    }

static uint32 HwPdhMuxDe1BitCounterAddress(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear)
    {
    return PdhMuxBitAddress(self, channelId, localAddress, clear);
    }

static uint32 HwPdhMuxDe3BitCounterAddress(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear)
    {
    return PdhMuxBitAddress(self, channelId, localAddress, clear);
    }

static uint32 PdhMuxDe1BitGet(Tha60290011ModulePmc self, uint32 channelId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwPdhMuxDe1BitCounterAddress(self, channelId, cAf6Reg_upen_de1_tdmvld_cnt_Base, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PdhMuxDe3BitGet(Tha60290011ModulePmc self, uint32 channelId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwPdhMuxDe3BitCounterAddress(self, channelId, cAf6Reg_upen_de3_tdmvld_cnt_Base, clear);
    return CounterRead2Clear(self, address, clear);
    }

static eBool HasLongAccess(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModulePmc(AtModule self)
	{
	ThaModulePmc module = (ThaModulePmc)self;

	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

		mMethodOverride(m_ThaModulePmcOverride, BaseAddress);
		mMethodOverride(m_ThaModulePmcOverride, HasLongAccess);
		mMethodOverride(m_ThaModulePmcOverride, PwCounterOffset);
        mMethodOverride(m_ThaModulePmcOverride, LineCounter2Reg);
        mMethodOverride(m_ThaModulePmcOverride, LineCounterOffset);

		mMethodOverride(m_ThaModulePmcOverride, Reg_upen_cnttxoam_rc_Address);
        mMethodOverride(m_ThaModulePmcOverride, Reg_MACGE_tx_type_packet_Address);
        mMethodOverride(m_ThaModulePmcOverride, Reg_upen_cla0_ro_Address);

        mMethodOverride(m_ThaModulePmcOverride, AuVcDefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, De1DefaultOffset);
		}

	mMethodsSet(module, &m_ThaModulePmcOverride);
	}

static void MethodsInit(Tha60290011ModulePmc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HwClsByteCounterAddress);
        mMethodOverride(m_methods, ClsTxDe1BytesGet);
        mMethodOverride(m_methods, ClsTxDe3BytesGet);
        mMethodOverride(m_methods, ClsTxControlPacketBytesGet);

        mMethodOverride(m_methods, HwClsPacketTxCounterAddress);
        mMethodOverride(m_methods, ClsTxDe1GoodPacketsGet);
        mMethodOverride(m_methods, ClsTxDe3GoodPacketsGet);
        mMethodOverride(m_methods, ClsTxControlGoodPacketsGet);
        mMethodOverride(m_methods, ClsTxDAErrPacketsGet);
        mMethodOverride(m_methods, ClsTxSAErrPacketsGet);
        mMethodOverride(m_methods, ClsTxEthTypeErrPacketsGet);
        mMethodOverride(m_methods, ClsTxLenErrPacketsGet);
        mMethodOverride(m_methods, ClsTxLenFieldErrPacketsGet);
        mMethodOverride(m_methods, ClsTxFcsErrPacketsGet);

        mMethodOverride(m_methods, ClsTxSequenceErrorPacketCounterAddress);
        mMethodOverride(m_methods, ClsTxSequenceErrPacketsGet);

        mMethodOverride(m_methods, ClsRxPacketCounterAddress);
        mMethodOverride(m_methods, ClsRxPacketsGet);

        mMethodOverride(m_methods, PdhMuxPacketCounterAddress);
        mMethodOverride(m_methods, PdhMuxPacketsGet);

        mMethodOverride(m_methods, HwPdhMuxByteCounterAddress);
        mMethodOverride(m_methods, PdhMuxBytesGet);

        mMethodOverride(m_methods, HwPdhMuxDe1BitCounterAddress);
        mMethodOverride(m_methods, HwPdhMuxDe3BitCounterAddress);
        mMethodOverride(m_methods, PdhMuxDe1BitGet);
        mMethodOverride(m_methods, PdhMuxDe3BitGet);
        }
        
    mMethodsSet(self, &m_methods);
    }

static void Override(AtModule self)
	{
	OverrideThaModulePmc(self);
	}

static uint32 ObjectSize(void)
	{
	return sizeof(tTha60290011ModulePmc);
	}

AtModule Tha60290011ModulePmcObjectInit(AtModule self, AtDevice device)
	{
	/* Clear memory */
	AtOsal osal = AtSharedDriverOsalGet();
	mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

	/* Super constructor */
	if (Tha60290022ModulePmcObjectInit(self, device) == NULL)
		return NULL;
    
    /* Setup class */
	Override(self);
	MethodsInit((Tha60290011ModulePmc)self);
	m_methodsInit = 1;

	return self;
	}

AtModule Tha60290011ModulePmcNew(AtDevice self)
	{
	/* Allocate memory */
	AtOsal osal = AtSharedDriverOsalGet();
	AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
	if (module == NULL)
		return NULL;

	/* Construct it */
	return Tha60290011ModulePmcObjectInit(module, self);
	}

uint32 Tha60290011ModulePmcClaTxDe1BytesGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxDe1BytesGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxDe3BytesGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxDe3BytesGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxControlPacketBytesGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxControlPacketBytesGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxDe1GoodPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxDe1GoodPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxDe3GoodPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxDe3GoodPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxControlGoodPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxControlGoodPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxDAErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxDAErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxSAErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxSAErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxEthTypeErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxEthTypeErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxLenErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxLenErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxLenFieldErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxLenFieldErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxSequenceErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxSequenceErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaTxFcsErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsTxFcsErrPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcClaRxPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsRxPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcPdhMuxPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->PdhMuxPacketsGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcPdhMuxBytesGet(ThaModulePmc self, uint8 sliceId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->PdhMuxBytesGet(mThis(self), sliceId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcPdhDe1BitGet(ThaModulePmc self, uint32 channelId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->PdhMuxDe1BitGet(mThis(self), channelId, clear);
    return 0;
    }

uint32 Tha60290011ModulePmcPdhDe3BitGet(ThaModulePmc self, uint32 channelId, eBool clear)
    {
    if (self)
        return mMethodsGet(mThis(self))->PdhMuxDe3BitGet(mThis(self), channelId, clear);
    return 0;
    }

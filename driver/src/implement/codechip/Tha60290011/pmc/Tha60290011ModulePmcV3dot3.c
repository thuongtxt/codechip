/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290011ModulePmcV3dot3.c
 *
 * Created Date: Feb 23, 2018
 *
 * Description : Implement the PMC module V3 dot 2 plus the MAC DIM counter changes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePmcInternal.h"
#include "Tha60290011ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_mac_dim_tx_byte                  0x930F8UL

#define cAf6Reg_mac_dim_txpkjumbo                0x93227UL
#define cAf6Reg_mac_dim_txpkless64               0x93228UL
#define cAf6Reg_mac_dim_txpk65to127              0x93229UL
#define cAf6Reg_mac_dim_txpk128to255             0x9322aUL
#define cAf6Reg_mac_dim_txpk256to511             0x9322bUL
#define cAf6Reg_mac_dim_txpk512to1023            0x9322cUL
#define cAf6Reg_mac_dim_txpk1024to1518           0x9322dUL
#define cAf6Reg_mac_dim_txpkt_total              0x9322eUL

#define cAf6Reg_mac_dim_rxpkjumbo                0x9322fUL
#define cAf6Reg_mac_dim_rxpkless64               0x93230UL
#define cAf6Reg_mac_dim_rxpk65to127              0x93231UL
#define cAf6Reg_mac_dim_rxpk128to255             0x93232UL
#define cAf6Reg_mac_dim_rxpk256to511             0x93233UL
#define cAf6Reg_mac_dim_rxpk512to1023            0x93234UL
#define cAf6Reg_mac_dim_rxpk1024to1518           0x93235UL
#define cAf6Reg_mac_dim_rxpkt_total              0x93236UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePmcMethods           m_ThaModulePmcOverride;

static const tThaModulePmcMethods           *m_ThaModulePmcMethods           = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DimMacPortPacketCounterCommonLocalAddress(ThaModulePmc self, uint32 offset, eBool clear)
    {
    AtUnused(self);

    return offset + (clear ? 64 : 0);
    }

static uint32 DimMacPortByteCounterCommonLocalAddress(ThaModulePmc self, uint8 portType, eBool isRx, eBool clear)
    {
    AtUnused(self);
    AtUnused(portType);

    return cAf6Reg_mac_dim_tx_byte + (isRx ? 2 : 0) + (clear ? 1 : 0);
    }

static uint32 DimMacCounterLocalAdddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets             :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpkt_total, clear);
        case cAtEthPortCounterTxPacketsLen0_64      :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpkless64, clear);
        case cAtEthPortCounterTxPacketsLen65_127    :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpk65to127, clear);
        case cAtEthPortCounterTxPacketsLen128_255   :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpk128to255, clear);
        case cAtEthPortCounterTxPacketsLen256_511   :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpk256to511, clear);
        case cAtEthPortCounterTxPacketsLen512_1023  :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpk512to1023, clear);
        case cAtEthPortCounterTxPacketsLen1024_1518 :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpk1024to1518, clear);
        case cAtEthPortCounterTxPacketsJumbo        :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_txpkjumbo, clear);
        case cAtEthPortCounterRxPackets             :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpkt_total, clear);
        case cAtEthPortCounterRxPacketsLen0_64      :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpkless64, clear);
        case cAtEthPortCounterRxPacketsLen65_127    :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpk65to127, clear);
        case cAtEthPortCounterRxPacketsLen128_255   :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpk128to255, clear);
        case cAtEthPortCounterRxPacketsLen256_511   :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpk256to511, clear);
        case cAtEthPortCounterRxPacketsLen512_1023  :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpk512to1023, clear);
        case cAtEthPortCounterRxPacketsLen1024_1518 :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpk1024to1518, clear);
        case cAtEthPortCounterRxPacketsJumbo        :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpkjumbo, clear);
        case cAtEthPortCounterTxBytes               :
            return DimMacPortByteCounterCommonLocalAddress(self, portType, cAtFalse, clear);
        case cAtEthPortCounterRxBytes               :
            return DimMacPortByteCounterCommonLocalAddress(self, portType, cAtTrue, clear);

        default:
            return cInvalidUint32;
        }
    }

static uint32 EthPortCounterLocalAddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    switch (portType)
        {
        case cThaModulePmcEthPortType10GPort:
        case cThaModulePmcEthPortType2Dot5GPort:
        case cThaModulePmcEthPortTypeDccPort:
            return m_ThaModulePmcMethods->EthPortCounterLocalAddress(self, portType, counterType, clear);
        case cThaModulePmcEthPortTypeDimCtrlPort:
            return DimMacCounterLocalAdddress(self, portType, counterType, clear);
        default:
            return cInvalidUint32;
        }
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePmcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, m_ThaModulePmcMethods, sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, EthPortCounterLocalAddress);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }


static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePmcV3dot3);
    }

AtModule Tha60290011ModulePmcV3dot3ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePmcV3dot2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePmcV3dot3New(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePmcV3dot3ObjectInit(module, self);
    }

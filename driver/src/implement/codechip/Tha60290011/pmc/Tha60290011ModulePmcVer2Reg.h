/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_GLBPMC_ver2_H_
#define _AF6_REG_AF6CNC0011_RD_GLBPMC_ver2_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Counter
Reg Addr   : 0x0_0000-0x0_0BFF(RW)
Reg Formula: 0x0_0000 + 1024*$offset + $pwid
    Where  : 
           + $pwid(0-1023)
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt_info1_rw_Base                                                             0x00000

/*--------------------------------------
BitField Name: txpwcnt
BitField Type: RW
BitField Desc: in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 :
txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit +
offset = 1 : txlbit + offset = 2 : unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt_info1_rw_txpwcnt_Mask                                                       cBit17_0
#define cAf6_upen_txpwcnt_info1_rw_txpwcnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Counter
Reg Addr   : 0x0_8000-0x0_8BFF(RW)
Reg Formula: 0x0_8000 + 1024*$offset + $pwid
    Where  : 
           + $pwid(0-1023)
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_txpwcnt_info1_rw_Base                                                             0x08000
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name: txpwcnt
BitField Type: RW
BitField Desc: in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 :
txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit +
offset = 1 : txlbit + offset = 2 : unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt_info1_rw_txpwcnt_Mask                                                       cBit17_0
#define cAf6_upen_txpwcnt_info1_rw_txpwcnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire TX Byte Counter
Reg Addr   : 0x8_0000-0x8_03FF (RW)
Reg Formula: 0x8_0000 + $pwid
    Where  : 
           + $pwid(0-1023)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt_info0_Base                                                                0x80000

/*--------------------------------------
BitField Name: pwcntbyte
BitField Type: RW
BitField Desc: txpwcntbyte
BitField Bits: [29:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt_info0_pwcntbyte_Mask                                                        cBit29_0
#define cAf6_upen_txpwcnt_info0_pwcntbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info0
Reg Addr   : 0x1_0000-0x1_03FF (RW)
Reg Formula: 0x1_0000 + $pwid
    Where  : 
           + $pwid(0-1023)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info0_rw_Base                                                             0x10000

/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RW
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of
rxpwcnt_info0_cnt1 side: + rxpkt
BitField Bits: [29:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info0_rw_rxpwcnt_info0_Mask                                                 cBit29_0
#define cAf6_upen_rxpwcnt_info0_rw_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info0
Reg Addr   : 0x1_4000-0x1_43FF (RW)
Reg Formula: 0x1_4000 + $pwid
    Where  : 
           + $pwid(0-1023)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_rxpwcnt_info0_rw_Base                                                             0x14000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RW
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of
rxpwcnt_info0_cnt1 side: + rxpkt
BitField Bits: [29:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info0_rw_rxpwcnt_info0_Mask                                                 cBit29_0
#define cAf6_upen_rxpwcnt_info0_rw_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info1
Reg Addr   : 0x2_0000-0x2_0BFF(RW)
Reg Formula: 0x2_0000 + 1024*$offset + $pwid
    Where  : 
           + $pwid(0-1023)
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info1_rw_Base                                                             0x20000

/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RW
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit +
offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: +
offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info1_rw_rxpwcnt_info0_Mask                                                 cBit17_0
#define cAf6_upen_rxpwcnt_info1_rw_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info1
Reg Addr   : 0x2_8000-0x2_8BFF(RW)
Reg Formula: 0x2_8000 + 1024*$offset + $pwid
    Where  : 
           + $pwid(0-1023)
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_rxpwcnt_info1_rw_Base                                                             0x28000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RW
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit +
offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: +
offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info1_rw_rxpwcnt_info0_Mask                                                 cBit17_0
#define cAf6_upen_rxpwcnt_info1_rw_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info2
Reg Addr   : 0x3_0000-0x3_0BFF(RW)
Reg Formula: 0x3_0000 + 1024*$offset + $pwid
    Where  : 
           + $pwid(0-1023)
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info2_rw_Base                                                             0x30000

/*--------------------------------------
BitField Name: rxpwcnt_info1
BitField Type: RW
BitField Desc: in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun +
offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: +
offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info2_rw_rxpwcnt_info1_Mask                                                 cBit17_0
#define cAf6_upen_rxpwcnt_info2_rw_rxpwcnt_info1_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info2
Reg Addr   : 0x3_8000-0x3_8BFF(RW)
Reg Formula: 0x3_8000 + 1024*$offset + $pwid
    Where  : 
           + $pwid(0-1023)
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_rxpwcnt_info2_rw_Base                                                             0x38000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpwcnt_info1
BitField Type: RW
BitField Desc: in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun +
offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: +
offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info2_rw_rxpwcnt_info1_Mask                                                 cBit17_0
#define cAf6_upen_rxpwcnt_info2_rw_rxpwcnt_info1_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : PMR Error Counter
Reg Addr   : 0x9_1800-0x9_182F(RW)
Reg Formula: 0x9_1800 + 16*$offset + $lineid
    Where  : 
           + $offset(0-2)
           + $lineid(0-15)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt0_rw_Base                                                              0x91800

/*--------------------------------------
BitField Name: pmr_err_cnt
BitField Type: RW
BitField Desc: in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 :
b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 :
rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_poh_pmr_cnt0_rw_pmr_err_cnt_Mask                                                    cBit31_0
#define cAf6_upen_poh_pmr_cnt0_rw_pmr_err_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PMR Error Counter
Reg Addr   : 0x9_1840-0x9_186F(RW)
Reg Formula: 0x9_1840 + 16*$offset + $lineid
    Where  : 
           + $offset(0-2)
           + $lineid(0-15)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_poh_pmr_cnt0_rw_Base                                                              0x91840
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: pmr_err_cnt
BitField Type: RW
BitField Desc: in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 :
b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 :
rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_poh_pmr_cnt0_rw_pmr_err_cnt_Mask                                                    cBit31_0
#define cAf6_upen_poh_pmr_cnt0_rw_pmr_err_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Path STS Counter
Reg Addr   : 0x9_2000-0x9_21FF (RW)
Reg Formula: 0x9_2000 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sts_pohpm_rw_Base                                                                 0x92000

/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1
side: + sts_bip(B3)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_sts_pohpm_rw_pohpath_vt_cnt_Mask                                                    cBit17_0
#define cAf6_upen_sts_pohpm_rw_pohpath_vt_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Path STS Counter
Reg Addr   : 0x9_2200-0x9_23FF (RW)
Reg Formula: 0x9_2200 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_sts_pohpm_rw_Base                                                                 0x92200
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1
side: + sts_bip(B3)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_sts_pohpm_rw_pohpath_vt_cnt_Mask                                                    cBit17_0
#define cAf6_upen_sts_pohpm_rw_pohpath_vt_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Path VT Counter
Reg Addr   : 0x7_0000 - 0x7_3FFF(RW)
Reg Formula: 0x7_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_vt_pohpm_rw_Base                                                                  0x70000

/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1
side: + vt_bip
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_vt_pohpm_rw_pohpath_vt_cnt_Mask                                                     cBit17_0
#define cAf6_upen_vt_pohpm_rw_pohpath_vt_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH Path VT Counter
Reg Addr   : 0x7_4000 - 0x7_7FFF(RW)
Reg Formula: 0x7_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_vt_pohpm_rw_Base                                                                  0x74000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1
side: + vt_bip
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_vt_pohpm_rw_pohpath_vt_cnt_Mask                                                     cBit17_0
#define cAf6_upen_vt_pohpm_rw_pohpath_vt_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 0
Reg Addr   : 0x5_0000-0x5_3FFF(RW)
Reg Formula: 0x5_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt0_rw_Base                                                               0x50000

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of
poh_vt_cnt1 side: + pohtx_vt_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 0
Reg Addr   : 0x5_4000-0x5_7FFF(RW)
Reg Formula: 0x5_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_poh_vt_cnt0_rw_Base                                                               0x54000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of
poh_vt_cnt1 side: + pohtx_vt_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 1
Reg Addr   : 0x6_0000-0x6_3FFF(RW)
Reg Formula: 0x6_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt1_rw_Base                                                               0x60000

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of
poh_vt_cnt1 side: + pohrx_vt_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 1
Reg Addr   : 0x6_4000-0x6_7FFF(RW)
Reg Formula: 0x6_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_poh_vt_cnt1_rw_Base                                                               0x64000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of
poh_vt_cnt1 side: + pohrx_vt_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x9_0800-0x9_09FF(RW)
Reg Formula: 0x9_0800 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt0_rw_Base                                                              0x90800

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of
poh_vt_cnt1 side: + pohtx_sts_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt0_rw_poh_sts_cnt_Mask                                                    cBit17_0
#define cAf6_upen_poh_sts_cnt0_rw_poh_sts_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x9_0A00-0x9_0BFF(RW)
Reg Formula: 0x9_0A00 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_poh_sts_cnt0_rw_Base                                                              0x90A00
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of
poh_vt_cnt1 side: + pohtx_sts_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt0_rw_poh_sts_cnt_Mask                                                    cBit17_0
#define cAf6_upen_poh_sts_cnt0_rw_poh_sts_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x9_1000-0x9_11FF(RW)
Reg Formula: 0x9_1000 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt1_rw_Base                                                              0x91000

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of
poh_vt_cnt1 side: + pohrx_sts_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt1_rw_poh_sts_cnt_Mask                                                    cBit17_0
#define cAf6_upen_poh_sts_cnt1_rw_poh_sts_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x9_1200-0x9_13FF(RW)
Reg Formula: 0x9_1200 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_poh_sts_cnt1_rw_Base                                                              0x91200
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of
poh_vt_cnt1 side: + pohrx_sts_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt1_rw_poh_sts_cnt_Mask                                                    cBit17_0
#define cAf6_upen_poh_sts_cnt1_rw_poh_sts_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds1 cntval Counter
Reg Addr   : 0x4_0000-0x4_3FFF(RW)
Reg Formula: 0x4_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de1cntval30_rw_Base                                                           0x40000

/*--------------------------------------
BitField Name: ds1_cntval_bus1_cnt
BitField Type: RW
BitField Desc: (rei/fe/crc)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de1cntval30_rw_ds1_cntval_bus1_cnt_Mask                                         cBit17_0
#define cAf6_upen_pdh_de1cntval30_rw_ds1_cntval_bus1_cnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds1 cntval Counter
Reg Addr   : 0x4_4000-0x4_7FFF(RW)
Reg Formula: 0x4_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_pdh_de1cntval30_rw_Base                                                           0x44000
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: ds1_cntval_bus1_cnt
BitField Type: RW
BitField Desc: (rei/fe/crc)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de1cntval30_rw_ds1_cntval_bus1_cnt_Mask                                         cBit17_0
#define cAf6_upen_pdh_de1cntval30_rw_ds1_cntval_bus1_cnt_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds3 cntval Counter
Reg Addr   : 0x9_0000-0x9_01FF(RW)
Reg Formula: 0x9_0000 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de3cnt0_rw_Base                                                               0x90000

/*--------------------------------------
BitField Name: ds3_cntval_cnt
BitField Type: RW
BitField Desc: (cb/pb/rei/fe)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de3cnt0_rw_ds3_cntval_cnt_Mask                                                  cBit17_0
#define cAf6_upen_pdh_de3cnt0_rw_ds3_cntval_cnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds3 cntval Counter
Reg Addr   : 0x9_0200-0x9_03FF(RW)
Reg Formula: 0x9_0200 + 8*$stsid + $slcid
    Where  : 
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_pdh_de3cnt0_rw_Base                                                               0x90200
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: ds3_cntval_cnt
BitField Type: RW
BitField Desc: (cb/pb/rei/fe)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de3cnt0_rw_ds3_cntval_cnt_Mask                                                  cBit17_0
#define cAf6_upen_pdh_de3cnt0_rw_ds3_cntval_cnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : CLA Counter00
Reg Addr   : 0x3_B321(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cla0_rw_Base                                                                      0x9312C

/*--------------------------------------
BitField Name: cla_err
BitField Type: RW
BitField Desc: cla_err
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cla0_rw_cla_err_Mask                                                                cBit31_0
#define cAf6_upen_cla0_rw_cla_err_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : ETH TX Packet OAM CNT
Reg Addr   : 0x9_3134(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnttxoam_Base                                                                     0x93134

/*--------------------------------------
BitField Name: cnttxoamt
BitField Type: RW
BitField Desc: TX Packet OAM CNT
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnttxoam_cnttxoamt_Mask                                                             cBit31_0
#define cAf6_upen_cnttxoam_cnttxoamt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Classifier Bytes Counter vld
Reg Addr   : 0x9_3140-0x9_3143(RW)
Reg Formula: 0x9_3140 + $clsid
    Where  : 
           + $clsid(0-3)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_bcnt_rw_Base                                                                  0x93140

/*--------------------------------------
BitField Name: bytecnt
BitField Type: RW
BitField Desc: Classifier bytecnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_bcnt_rw_bytecnt_Mask                                                            cBit31_0
#define cAf6_upen_cls_bcnt_rw_bytecnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Classifier Bytes Counter vld
Reg Addr   : 0x9_3160-0x9_3163(RW)
Reg Formula: 0x9_3160 + $clsid
    Where  : 
           + $clsid(0-3)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_cls_bcnt_rw_Base                                                                  0x93160
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: bytecnt
BitField Type: RW
BitField Desc: Classifier bytecnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_bcnt_rw_bytecnt_Mask                                                            cBit31_0
#define cAf6_upen_cls_bcnt_rw_bytecnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Classifier output counter pkt0
Reg Addr   : 0x3_B34C-0x3_B34F(RW)
Reg Formula: 0x3_B34C + $offset
    Where  : 
           + $offset(0-2)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_opcnt0_rw_Base                                                                0x93180

/*--------------------------------------
BitField Name: opktcnt0_opktcnt1_
BitField Type: RW
BitField Desc: in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter +
offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of
opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err
counter + offset = 2 : len pkt err cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_cls_opcnt0_rw_opktcnt0_opktcnt1__Mask                                               cBit17_0
#define cAf6_upen_cls_opcnt0_rw_opktcnt0_opktcnt1__Shift                                                     0

/*------------------------------------------------------------------------------
Reg Name   : Classifier input counter pkt
Reg Addr   : 0x9_3136(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cls_ipcnt_rw_Base                                                                 0x93136

/*--------------------------------------
BitField Name: ipktcnt
BitField Type: RW
BitField Desc: input packet cnt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cls_ipcnt_rw_ipktcnt_Mask                                                           cBit31_0
#define cAf6_upen_cls_ipcnt_rw_ipktcnt_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : PDH Mux Ethernet Byte Counter
Reg Addr   : 0x9_3120(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth_bytecnt_rw_Base                                                               0x93120

/*--------------------------------------
BitField Name: eth_bytecnt
BitField Type: RW
BitField Desc: ETH Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_eth_bytecnt_rw_eth_bytecnt_Mask                                                     cBit31_0
#define cAf6_upen_eth_bytecnt_rw_eth_bytecnt_Shift                                                           0

/*------------------------------------------------------------------------------
Reg Name   : PDH Mux Ethernet Packet Counter
Reg Addr   : 0x9_3126(RW)
Reg Formula:
    Where  :
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.
------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eth_pckt_rw_Base                                                               0x93126

/*--------------------------------------
BitField Name: eth_pktcnt
BitField Type: RW
BitField Desc: ETH Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_eth_bytecnt_rw_eth_pktcnt_Mask                                                      cBit31_0
#define cAf6_upen_eth_bytecnt_rw_eth_pktcnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DE1 TDM bit counter
Reg Addr   : 0x9_3000-0x9_307F(RW)
Reg Formula: 0x9_3000 + $lid
    Where  : 
           + $lid(0-127)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_de1_tdmvld_cnt_Base                                                               0x93000

/*--------------------------------------
BitField Name: de1_bit_cnt
BitField Type: RW
BitField Desc: bit_cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_de1_tdmvld_cnt_de1_bit_cnt_Mask                                                     cBit17_0
#define cAf6_upen_de1_tdmvld_cnt_de1_bit_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : DE3 TDM bit counter
Reg Addr   : 0x9_3100-0x9_311F(RW)
Reg Formula: 0x9_3100 + $lid
    Where  : 
           + $lid(0-31)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_de3_tdmvld_cnt_Base                                                               0x93100

/*--------------------------------------
BitField Name: de3_bit_cnt
BitField Type: RW
BitField Desc: bit_cnt
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_de3_tdmvld_cnt_de3_bit_cnt_Mask                                                     cBit17_0
#define cAf6_upen_de3_tdmvld_cnt_de3_bit_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : MACGE tx type packet
Reg Addr   : 0x9_3080(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge00lltx_typepkt_rw_Base                                                          0x93080

/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00lltx_typepkt_rw_txpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge00lltx_typepkt_rw_txpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACGE tx type packet
Reg Addr   : 0x9_3081(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_ge00lltx_typepkt_rw_Base                                                          0x93081
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00lltx_typepkt_rw_txpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge00lltx_typepkt_rw_txpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACGE tx type packet
Reg Addr   : 0x9_3087(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge00lltx_pkt_Base                                                                 0x93087

/*--------------------------------------
BitField Name: cnttxpkt
BitField Type: RW
BitField Desc: tx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00lltx_pkt_cnttxpkt_Mask                                                          cBit31_0
#define cAf6_upen_ge00lltx_pkt_cnttxpkt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MACGE tx type packet
Reg Addr   : 0x9_3088(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge00lltx_byte_Base                                                                0x93088

/*--------------------------------------
BitField Name: cnttxbyte
BitField Type: RW
BitField Desc: tx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00lltx_byte_cnttxbyte_Mask                                                        cBit31_0
#define cAf6_upen_ge00lltx_byte_cnttxbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MACGE rx type packet
Reg Addr   : 0x9_3090(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge00llrx_typepkt_rw_Base                                                          0x93090

/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00llrx_typepkt_rw_rxpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge00llrx_typepkt_rw_rxpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACGE rx type packet
Reg Addr   : 0x9_3091(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_ge00llrx_typepkt_rw_Base                                                          0x93091
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00llrx_typepkt_rw_rxpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge00llrx_typepkt_rw_rxpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACGE rx type packet
Reg Addr   : 0x9_3097(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge00llrx_pkt_Base                                                                 0x93097

/*--------------------------------------
BitField Name: cntrxpkt
BitField Type: RW
BitField Desc: rx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00llrx_pkt_cntrxpkt_Mask                                                          cBit31_0
#define cAf6_upen_ge00llrx_pkt_cntrxpkt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MACGE rx type packet
Reg Addr   : 0x9_3098(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge00llrx_byte_Base                                                                0x93098

/*--------------------------------------
BitField Name: cntrxbyte
BitField Type: RW
BitField Desc: rx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge00llrx_byte_cntrxbyte_Mask                                                        cBit31_0
#define cAf6_upen_ge00llrx_byte_cntrxbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE tx type packet
Reg Addr   : 0x9_30A0(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge10lltx_typepkt_rw_Base                                                          0x930A0

/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10lltx_typepkt_rw_txpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge10lltx_typepkt_rw_txpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE tx type packet
Reg Addr   : 0x9_30A1(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_ge10lltx_typepkt_rw_Base                                                          0x930A1
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10lltx_typepkt_rw_txpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge10lltx_typepkt_rw_txpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE tx type packet
Reg Addr   : 0x9_30A7(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge10lltx_pkt_Base                                                                 0x930A7

/*--------------------------------------
BitField Name: cnttxpkt
BitField Type: RW
BitField Desc: tx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10lltx_pkt_cnttxpkt_Mask                                                          cBit31_0
#define cAf6_upen_ge10lltx_pkt_cnttxpkt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE tx type packet
Reg Addr   : 0x9_30A8(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge10lltx_byte_Base                                                                0x930A8

/*--------------------------------------
BitField Name: cnttxbyte
BitField Type: RW
BitField Desc: tx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10lltx_byte_cnttxbyte_Mask                                                        cBit31_0
#define cAf6_upen_ge10lltx_byte_cnttxbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE rx type packet
Reg Addr   : 0x9_30B0(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge10llrx_typepkt_rw_Base                                                          0x930B0

/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10llrx_typepkt_rw_rxpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge10llrx_typepkt_rw_rxpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE rx type packet
Reg Addr   : 0x9_30B1(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_ge10llrx_typepkt_rw_Base                                                          0x930B1
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10llrx_typepkt_rw_rxpkt_Mask                                                      cBit31_0
#define cAf6_upen_ge10llrx_typepkt_rw_rxpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE rx type packet
Reg Addr   : 0x9_30B7(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge10llrx_pkt_Base                                                                 0x930B7

/*--------------------------------------
BitField Name: cntrxpkt
BitField Type: RW
BitField Desc: rx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10llrx_pkt_cntrxpkt_Mask                                                          cBit31_0
#define cAf6_upen_ge10llrx_pkt_cntrxpkt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MACTGE rx type packet
Reg Addr   : 0x9_30B8(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge10llrx_byte_Base                                                                0x930B8

/*--------------------------------------
BitField Name: cntrxbyte
BitField Type: RW
BitField Desc: rx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge10llrx_byte_cntrxbyte_Mask                                                        cBit31_0
#define cAf6_upen_ge10llrx_byte_cntrxbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM tx type packet
Reg Addr   : 0x9_30C0(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dimtx_typepkt_rw_Base                                                             0x930C0

/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimtx_typepkt_rw_txpkt_Mask                                                         cBit31_0
#define cAf6_upen_dimtx_typepkt_rw_txpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM tx type packet
Reg Addr   : 0x9_30C1(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_dimtx_typepkt_rw_Base                                                             0x930C1
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimtx_typepkt_rw_txpkt_Mask                                                         cBit31_0
#define cAf6_upen_dimtx_typepkt_rw_txpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM tx type packet
Reg Addr   : 0x9_30C7(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dimtx_pkt_Base                                                                    0x930C7

/*--------------------------------------
BitField Name: cnttxpkt
BitField Type: RW
BitField Desc: tx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimtx_pkt_cnttxpkt_Mask                                                             cBit31_0
#define cAf6_upen_dimtx_pkt_cnttxpkt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM tx type packet
Reg Addr   : 0x9_30C8(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dimtx_byte_Base                                                                   0x930C8

/*--------------------------------------
BitField Name: cnttxbyte
BitField Type: RW
BitField Desc: tx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimtx_byte_cnttxbyte_Mask                                                           cBit31_0
#define cAf6_upen_dimtx_byte_cnttxbyte_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM rx type packet
Reg Addr   : 0x9_30D0(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dimrx_typepkt_rw_Base                                                             0x930D0

/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimrx_typepkt_rw_rxpkt_Mask                                                         cBit31_0
#define cAf6_upen_dimrx_typepkt_rw_rxpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM rx type packet
Reg Addr   : 0x9_30D1(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_dimrx_typepkt_rw_Base                                                             0x930D1
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimrx_typepkt_rw_rxpkt_Mask                                                         cBit31_0
#define cAf6_upen_dimrx_typepkt_rw_rxpkt_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM rx type packet
Reg Addr   : 0x9_30D7(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dimrx_pkt_Base                                                                    0x930D7

/*--------------------------------------
BitField Name: cntrxpkt
BitField Type: RW
BitField Desc: rx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimrx_pkt_cntrxpkt_Mask                                                             cBit31_0
#define cAf6_upen_dimrx_pkt_cntrxpkt_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : MACDIM rx type packet
Reg Addr   : 0x9_30D8(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dimrx_byte_Base                                                                   0x930D8

/*--------------------------------------
BitField Name: cntrxbyte
BitField Type: RW
BitField Desc: rx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dimrx_byte_cntrxbyte_Mask                                                           cBit31_0
#define cAf6_upen_dimrx_byte_cntrxbyte_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 tx type packet
Reg Addr   : 0x9_30E0(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mac2_5tx_typepkt_rw_Base                                                          0x930E0

/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5tx_typepkt_rw_txpkt_Mask                                                      cBit31_0
#define cAf6_upen_mac2_5tx_typepkt_rw_txpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 tx type packet
Reg Addr   : 0x9_30E1(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_mac2_5tx_typepkt_rw_Base                                                          0x930E1
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: txpkt
BitField Type: RW
BitField Desc: txpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5tx_typepkt_rw_txpkt_Mask                                                      cBit31_0
#define cAf6_upen_mac2_5tx_typepkt_rw_txpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 tx type packet
Reg Addr   : 0x9_30E7(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mac2_5tx_pkt_Base                                                                 0x930E7

/*--------------------------------------
BitField Name: cnttxpkt
BitField Type: RW
BitField Desc: tx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5tx_pkt_cnttxpkt_Mask                                                          cBit31_0
#define cAf6_upen_mac2_5tx_pkt_cnttxpkt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 tx type packet
Reg Addr   : 0x9_30E8(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mac2_5tx_byte_Base                                                                0x930E8

/*--------------------------------------
BitField Name: cnttxbyte
BitField Type: RW
BitField Desc: tx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5tx_byte_cnttxbyte_Mask                                                        cBit31_0
#define cAf6_upen_mac2_5tx_byte_cnttxbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 rx type packet
Reg Addr   : 0x9_30F0(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mac2_5rx_typepkt_rw_Base                                                          0x930F0

/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5rx_typepkt_rw_rxpkt_Mask                                                      cBit31_0
#define cAf6_upen_mac2_5rx_typepkt_rw_rxpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 rx type packet
Reg Addr   : 0x9_30F1(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

#define cAf6Reg_upen_mac2_5rx_typepkt_rw_Base                                                          0x930F1
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: rxpkt
BitField Type: RW
BitField Desc: rxpkt
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5rx_typepkt_rw_rxpkt_Mask                                                      cBit31_0
#define cAf6_upen_mac2_5rx_typepkt_rw_rxpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 rx type packet
Reg Addr   : 0x9_30F7(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ge2_5rx_pkt_Base                                                                  0x930F7

/*--------------------------------------
BitField Name: cntrxpkt
BitField Type: RW
BitField Desc: rx Packet Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_ge2_5rx_pkt_cntrxpkt_Mask                                                           cBit31_0
#define cAf6_upen_ge2_5rx_pkt_cntrxpkt_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : MAC2_5 rx type packet
Reg Addr   : 0x9_30F8(RW)
Reg Formula: 
    Where  : 
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mac2_5rx_byte_Base                                                                0x930F8

/*--------------------------------------
BitField Name: cntrxbyte
BitField Type: RW
BitField Desc: rx Byte Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mac2_5rx_byte_cntrxbyte_Mask                                                        cBit31_0
#define cAf6_upen_mac2_5rx_byte_cntrxbyte_Shift                                                              0

#endif /* _AF6_REG_AF6CNC0011_RD_GLBPMC_ver2_H_ */

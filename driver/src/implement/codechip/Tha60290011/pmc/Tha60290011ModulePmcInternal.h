/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60290011ModulePmcInternal.h
 * 
 * Created Date: Dec 11, 2017
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPMCINTERNAL_H_
#define _THA60290011MODULEPMCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pmc/Tha60290022ModulePmcInternal.h"
#include "Tha60290011ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePmcMethods
    {
    uint32 (*HwClsByteCounterAddress)(Tha60290011ModulePmc self, uint32 offset, uint8 sliceId, uint8 cntType, eBool clear);
    uint32 (*ClsTxDe1BytesGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxDe3BytesGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*HwClsPacketTxCounterAddress)(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, uint8 cntType, eBool clear);
    uint32 (*ClsTxControlPacketBytesGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxDe1GoodPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxDe3GoodPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxControlGoodPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxDAErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxSAErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxEthTypeErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxLenErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxLenFieldErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxFcsErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsTxSequenceErrorPacketCounterAddress)(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, eBool clear);
    uint32 (*ClsTxSequenceErrPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsRxPacketCounterAddress)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*ClsRxPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*PdhMuxPacketCounterAddress)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*PdhMuxPacketsGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*HwPdhMuxByteCounterAddress)(Tha60290011ModulePmc self, uint32 offset, uint8 sliceId, eBool clear);
    uint32 (*PdhMuxBytesGet)(Tha60290011ModulePmc self, uint8 sliceId, eBool clear);
    uint32 (*HwPdhMuxDe1BitCounterAddress)(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear);
    uint32 (*HwPdhMuxDe3BitCounterAddress)(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear);
    uint32 (*PdhMuxDe1BitGet)(Tha60290011ModulePmc self, uint32 channelId, eBool clear);
    uint32 (*PdhMuxDe3BitGet)(Tha60290011ModulePmc self, uint32 channelId, eBool clear);
    }tTha60290011ModulePmcMethods;

typedef struct tTha60290011ModulePmc
    {
    tTha60290022ModulePmc super;
    const tTha60290011ModulePmcMethods *methods;
    }tTha60290011ModulePmc;

typedef struct tTha60290011ModulePmcV3Methods
    {
    uint32 (*HwPwCounterAddress)(Tha60290011ModulePmcV3 self, AtPw pw, uint32 offset, eBool clear);
    uint32 (*HwPwCounterR2COffset)(Tha60290011ModulePmcV3 self, AtPw pw, eBool clear);
    uint32 (*HwSdhLineCounterAddress)(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear);
    uint32 (*HwSdhAuVcCounterAddress)(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear);
    uint32 (*HwSdhVtCounterAddress)(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear);
    uint32 (*HwDe1CounterAddress)(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear);
    uint32 (*HwDe3CounterAddress)(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear);
    uint32 (*HwDe1SlipCounterAddress)(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear);

    }tTha60290011ModulePmcV3Methods;

typedef struct tTha60290011ModulePmcV3
    {
    tTha60290011ModulePmc super;
    const tTha60290011ModulePmcV3Methods *methods;
    }tTha60290011ModulePmcV3;

typedef struct tTha60290011ModulePmcV3dot2
    {
    tTha60290011ModulePmcV3 super;
    }tTha60290011ModulePmcV3dot2;

typedef struct tTha60290011ModulePmcV3dot3
    {
    tTha60290011ModulePmcV3dot2 super;
    }tTha60290011ModulePmcV3dot3;

typedef struct tTha60290011ModulePmcV3dot4
    {
    tTha60290011ModulePmcV3dot3 super;
    }tTha60290011ModulePmcV3dot4;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModulePmcObjectInit(AtModule self, AtDevice device);
AtModule Tha60290011ModulePmcV3ObjectInit(AtModule self, AtDevice device);
AtModule Tha60290011ModulePmcV3dot2ObjectInit(AtModule self, AtDevice device);
AtModule Tha60290011ModulePmcV3dot3ObjectInit(AtModule self, AtDevice device);
AtModule Tha60290011ModulePmcV3dot4ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEPMCINTERNAL_H_ */


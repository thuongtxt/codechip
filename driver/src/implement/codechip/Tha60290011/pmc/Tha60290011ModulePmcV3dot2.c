/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290011ModulePmcV3.c
 *
 * Created Date: Jan 24, 2018
 *
 * Description : Global PMC version3.2 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePmcInternal.h"
#include "Tha60290011ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_mac_ethmux_bytecnt_mac_tx01G_byte     0x930C0
#define cAf6Reg_mac2_5g_mac_tx_2_5g_byte                   0x930F0
#define cCLS_Sequence_err_counter_Base                     0x930F4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290011ModulePmcV3Methods m_Tha60290011ModulePmcV3Override;
static tTha60290011ModulePmcMethods   m_Tha60290011ModulePmcOverride;
static tThaModulePmcMethods           m_ThaModulePmcOverride;

static const tTha60290011ModulePmcV3Methods *m_Tha60290011ModulePmcV3Methods = NULL;
static const tTha60290011ModulePmcMethods   *m_Tha60290011ModulePmcMethods   = NULL;
static const tThaModulePmcMethods           *m_ThaModulePmcMethods           = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwCounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    uint32 regVal = mModuleHwRead(self, address);
    AtUnused(clear);
    return regVal;
    }

static uint32 CounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    return mMethodsGet(self)->HwCounterRead2Clear(self, address, clear);
    }

static uint32 HwPwCounterR2COffset(Tha60290011ModulePmcV3 self, AtPw pw, eBool clear)
    {
    uint32 r2cOffset = clear ? 1024 : 0;
    AtUnused(self);
    AtUnused(pw);
    return r2cOffset;
    }

static uint32 HwPwCounterAddress(Tha60290011ModulePmcV3 self, AtPw pw, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = mMethodsGet(self)->HwPwCounterR2COffset(self, pw, clear);
    uint32 address = m_Tha60290011ModulePmcV3Methods->HwPwCounterAddress(self, pw, offset, clear) + r2cOffset;
    return address;
    }

static uint32 HwSdhLineCounterAddress(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = clear ? 32 : 0;
    uint32 address = m_Tha60290011ModulePmcV3Methods->HwSdhLineCounterAddress(self, channel, offset, clear) + r2cOffset;
    return address;
    }

static uint32 HwSdhAuVcCounterAddress(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = clear ? 32 : 0;
    uint32 address =  m_Tha60290011ModulePmcV3Methods->HwSdhAuVcCounterAddress(self, channel, offset, clear) + r2cOffset;
    return address;
    }

static uint32 HwSdhVtCounterAddress(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = clear ? 1024 : 0;
    uint32 address =  m_Tha60290011ModulePmcV3Methods->HwSdhVtCounterAddress(self, channel, offset, clear) + r2cOffset;

    return address;
    }

static uint32 HwDe1CounterAddress(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = clear ? 1024 : 0;
    uint32 address =  m_Tha60290011ModulePmcV3Methods->HwDe1CounterAddress(self, channel, offset, clear) + r2cOffset;
    return address;
    }

static uint32 HwDe3CounterAddress(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = clear ? 32 : 0;
    uint32 address =  m_Tha60290011ModulePmcV3Methods->HwDe3CounterAddress(self, channel, offset, clear) + r2cOffset;
    return address;
    }

static uint32 HwClsByteCounterAddress(Tha60290011ModulePmc self, uint32 offset, uint8 sliceId, uint8 cntType, eBool clear)
    {
    uint32 r2cOffset = clear ? 1 : 0;
    uint32 address = offset + ThaModulePmcBaseAddress((ThaModulePmc)self) + sliceId * 6UL + (cntType*2UL) + r2cOffset;
    return address;
    }

static uint32 HwPdhMuxByteCounterAddress(Tha60290011ModulePmc self, uint32 offset, uint8 sliceId, eBool clear)
    {
    uint32 r2cOffset = clear ? 1 : 0;
    uint32 address = m_Tha60290011ModulePmcMethods->HwPdhMuxByteCounterAddress(self, offset, sliceId, clear) + r2cOffset;
    return address;
    }

static uint32 HwPdhMuxDe1BitCounterAddress(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear)
    {
    uint32 r2cOffset = clear ? 128 : 0;
    uint32 address = m_Tha60290011ModulePmcMethods->HwPdhMuxDe1BitCounterAddress(self, channelId, localAddress, clear) + r2cOffset;
    return address;
    }

static uint32 HwPdhMuxDe3BitCounterAddress(Tha60290011ModulePmc self, uint32 channelId, uint32 localAddress, eBool clear)
    {
    uint32 r2cOffset = clear ? 32 : 0;
    uint32 address = m_Tha60290011ModulePmcMethods->HwPdhMuxDe3BitCounterAddress(self, channelId, localAddress, clear)  + r2cOffset;
    return address;
    }

static uint32 HwDe1SlipCounterAddress(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 r2cOffset = clear ? 128 : 0;
    uint32 address =  m_Tha60290011ModulePmcV3Methods->HwDe1SlipCounterAddress(self, channel, offset, clear) + r2cOffset;
    return address;
    }

static uint32 EthPortByteCounterCommonLocalAddress(ThaModulePmc self, uint8 portType, eBool isRx)
    {
    uint8 hwPortType = Tha60290011ModulePmcV3EthPortTypeSw2Hw(portType);
    AtUnused(self);

    if (hwPortType < Tha60290011ModulePmcV3EthPortTypeSw2Hw(cThaModulePmcEthPortType2Dot5GPort))
        return cAf6Reg_upen_mac_ethmux_bytecnt_mac_tx01G_byte + (hwPortType * 0x4U) + (isRx ? 2 : 0);

    return cAf6Reg_mac2_5g_mac_tx_2_5g_byte + (isRx ? 2 : 0);
    }

static uint32 EthPortCounterLocalAddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    uint32 address = m_ThaModulePmcMethods->EthPortCounterLocalAddress(self, portType, counterType, clear);

    if (address == cInvalidUint32)
        return address;

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets             :
        case cAtEthPortCounterTxPacketsLen0_64      :
        case cAtEthPortCounterTxPacketsLen65_127    :
        case cAtEthPortCounterTxPacketsLen128_255   :
        case cAtEthPortCounterTxPacketsLen256_511   :
        case cAtEthPortCounterTxPacketsLen512_1023  :
        case cAtEthPortCounterTxPacketsLen1024_1518 :
        case cAtEthPortCounterTxPacketsJumbo        :
        case cAtEthPortCounterRxPackets             :
        case cAtEthPortCounterRxPacketsLen0_64      :
        case cAtEthPortCounterRxPacketsLen65_127    :
        case cAtEthPortCounterRxPacketsLen128_255   :
        case cAtEthPortCounterRxPacketsLen256_511   :
        case cAtEthPortCounterRxPacketsLen512_1023  :
        case cAtEthPortCounterRxPacketsLen1024_1518 :
        case cAtEthPortCounterRxPacketsJumbo        :
        case cAtEthPortCounterTxOamPackets          :
        case cAtEthPortCounterRxErrMefPackets       :
        case cAtEthPortCounterRxErrEthHdrPackets    :
        case cAtEthPortCounterRxErrPsnPackets       :
        case cAtEthPortCounterRxPhysicalError       :
            address = address + (clear ? 64 : 0);
            return address;
        case cAtEthPortCounterTxBytes               :
            address = EthPortByteCounterCommonLocalAddress(self, portType, cAtFalse);
            address = address + (clear ? 1 : 0);
            return address;
        case cAtEthPortCounterRxBytes               :
            address = EthPortByteCounterCommonLocalAddress(self, portType, cAtTrue);
            address = address + (clear ? 1 : 0);
            return address;

        default:
            return cInvalidUint32;
        }
    }

static uint32 HwClsPacketTxCounterAddress(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, uint8 cntType, eBool clear)
    {
    uint32 r2cOffset = clear ? 64 : 0;
    uint32 address = m_Tha60290011ModulePmcMethods->HwClsPacketTxCounterAddress(self, localAddress, sliceId, cntType, clear) + r2cOffset;
    return address;
    }

static uint32 ClsRxPacketCounterAddress(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 r2cOffset = clear ? 64 : 0;
    uint32 address = m_Tha60290011ModulePmcMethods->ClsRxPacketCounterAddress(self, sliceId, clear) + r2cOffset;
    return address;
    }

static uint32 ClsTxSequenceErrorPacketCounterAddress(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, eBool clear)
    {
    uint32 r2cOffset = clear ? 1 : 0;
    uint32 address = localAddress + ThaModulePmcBaseAddress((ThaModulePmc)self) + (sliceId * 2UL) + r2cOffset;
    return address;
    }

static uint32 ClsTxSequenceErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->ClsTxSequenceErrorPacketCounterAddress(self, cCLS_Sequence_err_counter_Base, sliceId, clear);
    return CounterRead2Clear((ThaModulePmc)self, address, clear);
    }

static uint32 PdhMuxPacketCounterAddress(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 r2cOffset = clear ? 64 : 0;
    uint32 address = m_Tha60290011ModulePmcMethods->PdhMuxPacketCounterAddress(self, sliceId, clear) + r2cOffset;
    return address;
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePmcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, m_ThaModulePmcMethods, sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, HwCounterRead2Clear);
        mMethodOverride(m_ThaModulePmcOverride, EthPortCounterLocalAddress);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }

static void OverrideTha60290011ModulePmc(AtModule self)
    {
    Tha60290011ModulePmc module = (Tha60290011ModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ModulePmcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModulePmcOverride, m_Tha60290011ModulePmcMethods, sizeof(m_Tha60290011ModulePmcOverride));

        mMethodOverride(m_Tha60290011ModulePmcOverride, HwClsPacketTxCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, HwClsByteCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxSequenceErrorPacketCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxSequenceErrPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsRxPacketCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, PdhMuxPacketCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, HwPdhMuxByteCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, HwPdhMuxDe1BitCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, HwPdhMuxDe3BitCounterAddress);
        }

    mMethodsSet(module, &m_Tha60290011ModulePmcOverride);
    }

static void OverrideTha60290011ModulePmcV3(AtModule self)
    {
    Tha60290011ModulePmcV3 module = (Tha60290011ModulePmcV3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ModulePmcV3Methods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModulePmcV3Override, m_Tha60290011ModulePmcV3Methods, sizeof(m_Tha60290011ModulePmcV3Override));

        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwPwCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwPwCounterR2COffset);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwSdhLineCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwSdhAuVcCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwSdhVtCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwDe1CounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwDe3CounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcV3Override, HwDe1SlipCounterAddress);
        }

    mMethodsSet(module, &m_Tha60290011ModulePmcV3Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    OverrideTha60290011ModulePmc(self);
    OverrideTha60290011ModulePmcV3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePmcV3dot2);
    }

AtModule Tha60290011ModulePmcV3dot2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePmcV3ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePmcV3dot2New(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePmcV3dot2ObjectInit(module, self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290011ModulePmcV3.c
 *
 * Created Date: Dec 11, 2017
 *
 * Description : Global PMC version3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePmcInternal.h"
#include "Tha60290011ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwCounterOffset_txrbit 0x00000
#define cPwCounterOffset_txpbit 0x00800
#define cPwCounterOffset_txnbit 0x01000
#define cPwCounterOffset_txlbit 0x01800
#define cPwCounterOffset_txpkt  0x02000
#define cPwCounterOffset_txpwcntbyte 0x80000

#define cPwCounterOffset_rxpwcnt 0x10000
#define cPwCounterOffset_rxpkt   0x10800
#define cPwCounterOffset_rxrbit  0x20000
#define cPwCounterOffset_rxpbit  0x20800
#define cPwCounterOffset_rxnbit  0x21000
#define cPwCounterOffset_rxlbit  0x21800
#define cPwCounterOffset_rxstray 0x22000
#define cPwCounterOffset_rxmalform 0x22800
#define cPwCounterOffset_rxunderrun 0x30000
#define cPwCounterOffset_rxoverrun  0x30800
#define cPwCounterOffset_rxlops     0x31000
#define cPwCounterOffset_rxlost     0x31800
#define cPwCounterOffset_rxearly    0x32000
#define cPwCounterOffset_rxlate     0x32800

#define cSdhLineCounterOffset_rei_l            0x91800
#define cSdhLineCounterOffset_rei_l_block_err  0x91840
#define cSdhLineCounterOffset_b2               0x91880
#define cSdhLineCounterOffset_b2_block_err     0x918c0
#define cSdhLineCounterOffset_b1               0x91900
#define cSdhLineCounterOffset_b1_block_err     0x91940

#define cSdhStsCounterOffset_pohpath_rei    0x92000
#define cSdhStsCounterOffset_pohpath_bip    0x92040
#define cSdhStsCounterOffset_poh_sts_txdec  0x90800
#define cSdhStsCounterOffset_poh_sts_txinc  0x90840
#define cSdhStsCounterOffset_poh_sts_rxdec  0x91000
#define cSdhStsCounterOffset_poh_sts_rxinc  0x91040

#define cSdhVtCounterOffset_pohpath_rei  0x70000
#define cSdhVtCounterOffset_pohpath_bip  0x70800
#define cSdhVtCounterOffset_poh_vt_txdec 0x50000
#define cSdhVtCounterOffset_poh_vt_txinc 0x50800
#define cSdhVtCounterOffset_poh_vt_rxdec 0x60000
#define cSdhVtCounterOffset_poh_vt_rxinc 0x60800

#define cPdhDe1CounterOffset_fe  0x40000
#define cPdhDe1CounterOffset_crc 0x40800
#define cPdhDe1CounterOffset_rei 0x41000

#define cPdhDe3CounterOffset_fe  0x90000
#define cPdhDe3CounterOffset_rei 0x90040
#define cPdhDe3CounterOffset_pb  0x90080
#define cPdhDe3CounterOffset_cb  0x900c0

#define cAf6Reg_upen_de1_tdmvld_cnt_Base  0x93400
#define cAf6Reg_upen_de3_tdmvld_cnt_Base  0x93500
#define cPDH_Mux_Ethernet_DE1_pktcnt_Base 0x93224

#define cAf6Reg_upen_cls_opcnt0_rw_Base         0x93210
#define cThaClaTxTypeDe1GoodPacket              0
#define cThaClaTxTypeDe3Ec1GoodPacket           1
#define cThaClaTxTypeControlGoodPacket          2
#define cThaClaTxTypeFcsErrPacket               3
#define cThaClaTxTypeLenErrPacket               4
#define cThaClaTxTypeDaErrPacket                5
#define cThaClaTxTypeSaErrPacket                6
#define cThaClaTxTypeEthErrPacket               7
#define cThaClaTxTypeLenFieldErrPacket          8
#define cCLS_Sequence_err_counter_Base          0x930F2
#define cAf6Reg_upen_cls_ipcnt_rw_Base          0x93222

#define cAf6Reg_upen_cntmux0_mac1G_txpkjumbo 0x93300
#define cAf6Reg_mac2_5g_txpkjumbo            0x93200

#define cAf6Reg_upen_mac_ethmux_bytecnt_mac_tx01G_byte     0x930C0
#define cAf6Reg_mac2_5g_mac_tx_2_5g_byte                   0x930F0

#define cAf6Reg_upen_cntmux0_cla_errmefda_ecid       0x93318
#define cAf6Reg_upen_cntmux0_cla_psn_err             0x93319
#define cAf6Reg_upen_cntmux0_cla_cnttxoamt           0x9331A
#define cAf6Reg_upen_cntmux0_cla_eth_err             0x93338
#define cAf6Reg_upen_cntmux0_cla_phy_err             0x93339

#define cAf6Reg_upen_de1_sipen_cnt                   0x93600

#define cAf6Reg_upen_eth_bytecnt                     0x93120UL

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290011ModulePmcV3)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011ModulePmcV3Methods m_methods;

/* Override */
static tThaModulePmcMethods m_ThaModulePmcOverride;
static tTha60290011ModulePmcMethods m_Tha60290011ModulePmcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    return mMethodsGet(self)->HwCounterRead2Clear(self, address, clear);
    }

static uint32 HwPwCounterAddress(Tha60290011ModulePmcV3 self, AtPw pw, uint32 offset, eBool clear)
    {
    uint32 address = AtChannelIdGet((AtChannel)pw) + offset + ThaModulePmcBaseAddress((ThaModulePmc)self);
    AtUnused(clear);
    return address;
    }

static uint32 HwPwCounterR2COffset(Tha60290011ModulePmcV3 self, AtPw pw, eBool clear)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(clear);
    return 0;
    }

static uint32 PwCounterRead2Clear(ThaModulePmc self, AtPw pw, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwPwCounterAddress(mThis(self), pw, offset, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txpkt, clear);
    }

static uint32 PwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txpwcntbyte, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txlbit, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txrbit, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txnbit, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txpbit, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_txnbit, clear);
    }

static uint32 PwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxpkt, clear);
    }

static uint32 PwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxmalform, clear);
    }

static uint32 PwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxstray, clear);
    }

static uint32 PwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlbit, clear);
    }

static uint32 PwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxrbit, clear);
    }

static uint32 PwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxnbit, clear);
    }

static uint32 PwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxpbit, clear);
    }

static uint32 PwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxnbit, clear);
    }

static uint32 PwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxpwcnt, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlost, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxearly, clear);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlate, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxoverrun, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxunderrun, clear);
    }

static uint32 PwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, cPwCounterOffset_rxlops, clear);
    }

static uint32 HwSdhLineCounterAddress(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = AtChannelIdGet((AtChannel)channel) + offset + ThaModulePmcBaseAddress((ThaModulePmc)self);
    AtUnused(clear);
    return address;
    }

static uint32 SdhLineCounterRead2Clear(ThaModulePmc self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwSdhLineCounterAddress(mThis(self), channel, offset, clear);
    return mMethodsGet(self)->HwCounterRead2Clear(self, address, clear);
    }

static uint32 SdhLineCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return SdhLineCounterRead2Clear(self, channel, cSdhLineCounterOffset_b1, clear);

        case cAtSdhLineCounterTypeB2:
            return SdhLineCounterRead2Clear(self, channel, cSdhLineCounterOffset_b2, clear);

        case cAtSdhLineCounterTypeRei:
            return SdhLineCounterRead2Clear(self, channel, cSdhLineCounterOffset_rei_l, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 SdhLineBlockErrorCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    if (mMethodsGet(self)->LineSupportedBlockCounters(self) == cAtFalse)
        return 0;

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return SdhLineCounterRead2Clear(self, channel, cSdhLineCounterOffset_b1_block_err, clear);

        case cAtSdhLineCounterTypeB2:
            return SdhLineCounterRead2Clear(self, channel, cSdhLineCounterOffset_b2_block_err, clear);

        case cAtSdhLineCounterTypeRei:
            return SdhLineCounterRead2Clear(self, channel, cSdhLineCounterOffset_rei_l_block_err, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 HwSdhAuVcCounterAddress(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    ThaModulePmc module = (ThaModulePmc)self;
    uint32 address =  mMethodsGet(module)->AuVcDefaultOffset(module, channel) + offset + ThaModulePmcBaseAddress(module);
    AtUnused(clear);
    return address;
    }

static uint32 SdhAuVcCounterRead2Clear(ThaModulePmc self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwSdhAuVcCounterAddress(mThis(self), channel, offset, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 AuVcPohCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return SdhAuVcCounterRead2Clear(self, channel, cSdhStsCounterOffset_pohpath_bip, clear);

        case cAtSdhPathCounterTypeRei:
            return SdhAuVcCounterRead2Clear(self, channel, cSdhStsCounterOffset_pohpath_rei, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 AuVcPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeRxPPJC:
            return SdhAuVcCounterRead2Clear(self, channel, cSdhStsCounterOffset_poh_sts_rxinc, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return SdhAuVcCounterRead2Clear(self, channel, cSdhStsCounterOffset_poh_sts_rxdec, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return SdhAuVcCounterRead2Clear(self, channel, cSdhStsCounterOffset_poh_sts_txinc, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return SdhAuVcCounterRead2Clear(self, channel, cSdhStsCounterOffset_poh_sts_txdec, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 HwSdhVtCounterAddress(Tha60290011ModulePmcV3 self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    ThaModulePmc module = (ThaModulePmc)self;
    uint32 address =  mMethodsGet(module)->Vc1xDefaultOffset(module, channel) + offset + ThaModulePmcBaseAddress(module);
    AtUnused(clear);
    return address;
    }

static uint32 SdhVtCounterRead2Clear(ThaModulePmc self, AtSdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwSdhVtCounterAddress(mThis(self), channel, offset, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 TuVc1xPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return SdhVtCounterRead2Clear(self, channel, cSdhVtCounterOffset_pohpath_bip, clear);

        case cAtSdhPathCounterTypeRei:
            return SdhVtCounterRead2Clear(self, channel, cSdhVtCounterOffset_pohpath_rei, clear);

        case cAtSdhPathCounterTypeRxPPJC:
            return SdhVtCounterRead2Clear(self, channel, cSdhVtCounterOffset_poh_vt_rxinc, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return SdhVtCounterRead2Clear(self, channel, cSdhVtCounterOffset_poh_vt_rxdec, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return SdhVtCounterRead2Clear(self, channel, cSdhVtCounterOffset_poh_vt_txinc, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return SdhVtCounterRead2Clear(self, channel, cSdhVtCounterOffset_poh_vt_txdec, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 HwDe1CounterAddress(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    ThaModulePmc module = (ThaModulePmc)self;
    uint32 address =  mMethodsGet(module)->De1DefaultOffset(module, channel) + offset + ThaModulePmcBaseAddress(module);
    AtUnused(clear);
    return address;
    }

static uint32 HwDe1SlipCounterAddress(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address =  AtChannelIdGet((AtChannel)channel) + offset + ThaModulePmcBaseAddress((ThaModulePmc)self);
    AtUnused(clear);
    return address;
    }

static uint32 De1CounterRead2Clear(ThaModulePmc self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwDe1CounterAddress(mThis(self), channel, offset, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 De1TxCsCounterRead2Clear(ThaModulePmc self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwDe1SlipCounterAddress(mThis(self), channel, offset, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 VcDe1CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtPdhDe1CounterBpvExz:
            return 0;

        case cAtPdhDe1CounterCrc:
            return De1CounterRead2Clear(self, channel, cPdhDe1CounterOffset_crc, clear);

        case cAtPdhDe1CounterFe:
            return De1CounterRead2Clear(self, channel, cPdhDe1CounterOffset_fe, clear);

        case cAtPdhDe1CounterRei:
            return De1CounterRead2Clear(self, channel, cPdhDe1CounterOffset_rei, clear);

        case cAtPdhDe1CounterTxCs:
            {
            if (AtPdhChannelTxSlipBufferIsEnabled(channel))
                return De1TxCsCounterRead2Clear(self, channel, cAf6Reg_upen_de1_sipen_cnt, clear);
            else
                return 0;
            }

        default:
            return 0;
        }

    return 0;
    }

static uint32 HwDe3CounterAddress(Tha60290011ModulePmcV3 self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    ThaModulePmc module = (ThaModulePmc)self;
    uint32 address =  mMethodsGet(module)->De3DefaultOffset(module, channel) + offset + ThaModulePmcBaseAddress(module);
    AtUnused(clear);
    return address;
    }

static uint32 De3CounterRead2Clear(ThaModulePmc self, AtPdhChannel channel, uint32 offset, eBool clear)
    {
    uint32 address = mMethodsGet(mThis(self))->HwDe3CounterAddress(mThis(self), channel, offset, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 VcDe3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtPdhDe3CounterRei:
            return De3CounterRead2Clear(self, channel, cPdhDe3CounterOffset_rei, clear);

        case cAtPdhDe3CounterFBit:
            return De3CounterRead2Clear(self, channel, cPdhDe3CounterOffset_fe, clear);

        case cAtPdhDe3CounterPBit:
            return De3CounterRead2Clear(self, channel, cPdhDe3CounterOffset_pb, clear);

        case cAtPdhDe3CounterCPBit:
            return De3CounterRead2Clear(self, channel, cPdhDe3CounterOffset_cb, clear);

        case cAtPdhDe3CounterBpvExz:
            return 0;

        default:
            return 0;
        }

    return 0;
    }

static uint32 HwClsPacketTxCounterAddress(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, uint8 cntType, eBool clear)
    {
    uint32 address = localAddress + (ThaModulePmcBaseAddress((ThaModulePmc)self) + sliceId * 0x9UL + cntType);
    AtUnused(clear);
    return address;
    }

static uint32 ClsPacketTxCounter(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, uint8 cntType, eBool clear)
    {
    ThaModulePmc module = (ThaModulePmc)self;
    uint32 address = mMethodsGet(self)->HwClsPacketTxCounterAddress(self, localAddress, sliceId, cntType, clear);
    return CounterRead2Clear(module, address, clear);
    }

static uint32 ClsTxDe1GoodPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeDe1GoodPacket, clear);
    }

static uint32 ClsTxDe3GoodPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeDe3Ec1GoodPacket, clear);
    }

static uint32 ClsTxControlGoodPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeControlGoodPacket, clear);
    }

static uint32 ClsTxDAErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeDaErrPacket, clear);
    }

static uint32 ClsTxSAErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeSaErrPacket, clear);
    }

static uint32 ClsTxEthTypeErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeEthErrPacket, clear);
    }

static uint32 ClsTxLenErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeLenErrPacket, clear);
    }

static uint32 ClsTxLenFieldErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeLenFieldErrPacket, clear);
    }

static uint32 ClsTxSequenceErrorPacketCounterAddress(Tha60290011ModulePmc self, uint32 localAddress, uint8 sliceId, eBool clear)
    {
    uint32 address = localAddress + (ThaModulePmcBaseAddress((ThaModulePmc)self) + sliceId);
    AtUnused(clear);
    return address;
    }

static uint32 ClsTxSequenceErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->ClsTxSequenceErrorPacketCounterAddress(self, cCLS_Sequence_err_counter_Base, sliceId, clear);
    return CounterRead2Clear((ThaModulePmc)self, address, clear);
    }

static uint32 ClsTxFcsErrPacketsGet(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    return ClsPacketTxCounter(self, cAf6Reg_upen_cls_opcnt0_rw_Base, sliceId, cThaClaTxTypeFcsErrPacket, clear);
    }

static uint32 ClsRxPacketCounterAddress(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = ThaModulePmcBaseAddress((ThaModulePmc)self) + cAf6Reg_upen_cls_ipcnt_rw_Base + (sliceId);
    AtUnused(clear);
    return address;
    }

static uint32 PdhMuxPacketCounterAddress(Tha60290011ModulePmc self, uint8 sliceId, eBool clear)
    {
    uint32 address = ThaModulePmcBaseAddress((ThaModulePmc)self) + sliceId + cPDH_Mux_Ethernet_DE1_pktcnt_Base;
    AtUnused(clear);
    return address;
    }

static uint32 PdhMuxDe1BitGet(Tha60290011ModulePmc self, uint32 channelId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwPdhMuxDe1BitCounterAddress(self, channelId, cAf6Reg_upen_de1_tdmvld_cnt_Base, clear);
    return CounterRead2Clear((ThaModulePmc)self, address, clear);
    }

static uint32 PdhMuxDe3BitGet(Tha60290011ModulePmc self, uint32 channelId, eBool clear)
    {
    uint32 address = mMethodsGet(self)->HwPdhMuxDe3BitCounterAddress(self, channelId, cAf6Reg_upen_de3_tdmvld_cnt_Base, clear);
    return CounterRead2Clear((ThaModulePmc)self, address, clear);
    }

static uint8 EthPortTypeSw2Hw(uint8 portType)
    {
    switch (portType)
        {
        case cThaModulePmcEthPortTypeDccPort     : return 0;
        case cThaModulePmcEthPortType10GPort     : return 1;
        case cThaModulePmcEthPortTypeDimCtrlPort : return 2;
        case cThaModulePmcEthPortType2Dot5GPort  : return 3;
        default:
            return 0;
        }
    }

static uint32 EthPortPacketCounterCommonLocalAddress(ThaModulePmc self, uint8 portType, uint8 counterIndex, eBool isRx)
    {
    uint8 hwPortType = EthPortTypeSw2Hw(portType);
    AtUnused(self);

    if (hwPortType < EthPortTypeSw2Hw(cThaModulePmcEthPortType2Dot5GPort))
        return (uint32)(cAf6Reg_upen_cntmux0_mac1G_txpkjumbo + (hwPortType * 0x8U) + (uint32)counterIndex + (isRx ? 0x20U : 0x0U));

    return (uint32)(cAf6Reg_mac2_5g_txpkjumbo + (uint32)counterIndex + (uint32)(isRx ? 0x8U : 0x0U));
    }

static uint32 EthPortByteCounterCommonLocalAddress(ThaModulePmc self, uint8 portType, eBool isRx)
    {
    uint8 hwPortType = EthPortTypeSw2Hw(portType);
    AtUnused(self);

    if (hwPortType < EthPortTypeSw2Hw(cThaModulePmcEthPortType2Dot5GPort))
        return cAf6Reg_upen_mac_ethmux_bytecnt_mac_tx01G_byte + (hwPortType * 0x2U) + (isRx ? 1 : 0);

    return cAf6Reg_mac2_5g_mac_tx_2_5g_byte + (isRx ? 1 : 0);
    }

static uint32 EthPortCounterOthersLocalAddress(ThaModulePmc self, uint8 portType, uint32 counterOffset)
    {
    AtUnused(self);

    /* Just 10G Backplane port is supported */
    if (portType != cThaModulePmcEthPortType10GPort)
        return cInvalidUint32;

    return counterOffset;
    }

static uint32 EthPortCounterLocalAddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    AtUnused(clear);

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets             :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket, cAtFalse);
        case cAtEthPortCounterTxBytes               :
            return EthPortByteCounterCommonLocalAddress(self, portType,   cAtFalse);
        case cAtEthPortCounterTxPacketsLen0_64      :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacketLessThan64, cAtFalse);
        case cAtEthPortCounterTxPacketsLen65_127    :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket65To128, cAtFalse);
        case cAtEthPortCounterTxPacketsLen128_255   :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket129To256, cAtFalse);
        case cAtEthPortCounterTxPacketsLen256_511   :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket257To512, cAtFalse);
        case cAtEthPortCounterTxPacketsLen512_1023  :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket513To1024, cAtFalse);
        case cAtEthPortCounterTxPacketsLen1024_1518 :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket1025To1528, cAtFalse);
        case cAtEthPortCounterTxPacketsJumbo        :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacketJumbo, cAtFalse);

        case cAtEthPortCounterRxPackets             :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket, cAtTrue);
        case cAtEthPortCounterRxBytes               :
            return EthPortByteCounterCommonLocalAddress(self, portType,   cAtTrue);
        case cAtEthPortCounterRxPacketsLen0_64      :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacketLessThan64, cAtTrue);
        case cAtEthPortCounterRxPacketsLen65_127    :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket65To128, cAtTrue);
        case cAtEthPortCounterRxPacketsLen128_255   :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket129To256, cAtTrue);
        case cAtEthPortCounterRxPacketsLen256_511   :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket257To512, cAtTrue);
        case cAtEthPortCounterRxPacketsLen512_1023  :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket513To1024, cAtTrue);
        case cAtEthPortCounterRxPacketsLen1024_1518 :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacket1025To1528, cAtTrue);
        case cAtEthPortCounterRxPacketsJumbo        :
            return EthPortPacketCounterCommonLocalAddress(self, portType, cThaEthTypePacketJumbo, cAtTrue);

        case cAtEthPortCounterTxOamPackets          :
            return EthPortCounterOthersLocalAddress(self, portType, cAf6Reg_upen_cntmux0_cla_cnttxoamt);
        case cAtEthPortCounterRxErrMefPackets       :
            return EthPortCounterOthersLocalAddress(self, portType, cAf6Reg_upen_cntmux0_cla_errmefda_ecid);
        case cAtEthPortCounterRxErrEthHdrPackets    :
            return EthPortCounterOthersLocalAddress(self, portType, cAf6Reg_upen_cntmux0_cla_eth_err);
        case cAtEthPortCounterRxErrPsnPackets       :
            return EthPortCounterOthersLocalAddress(self, portType, cAf6Reg_upen_cntmux0_cla_psn_err);
        case cAtEthPortCounterRxPhysicalError       :
            return EthPortCounterOthersLocalAddress(self, portType, cAf6Reg_upen_cntmux0_cla_phy_err);

        default:
            return cInvalidUint32;
        }
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMalformedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxStrayPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLopsGet);

        mMethodOverride(m_ThaModulePmcOverride, SdhLineCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, SdhLineBlockErrorCounterGet);

        mMethodOverride(m_ThaModulePmcOverride, AuVcPointerCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, AuVcPohCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, TuVc1xPointerCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, VcDe1CounterGet);
        mMethodOverride(m_ThaModulePmcOverride, VcDe3CounterGet);

        mMethodOverride(m_ThaModulePmcOverride, EthPortCounterLocalAddress);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }

static void OverrideTha60290011ModulePmc(AtModule self)
    {
    Tha60290011ModulePmc module = (Tha60290011ModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModulePmcOverride, mMethodsGet(module), sizeof(m_Tha60290011ModulePmcOverride));

        mMethodOverride(m_Tha60290011ModulePmcOverride, HwClsPacketTxCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxDe1GoodPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxDe3GoodPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxControlGoodPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxDAErrPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxSAErrPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxEthTypeErrPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxLenErrPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxLenFieldErrPacketsGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxFcsErrPacketsGet);

        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxSequenceErrorPacketCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsTxSequenceErrPacketsGet);

        mMethodOverride(m_Tha60290011ModulePmcOverride, ClsRxPacketCounterAddress);
        mMethodOverride(m_Tha60290011ModulePmcOverride, PdhMuxPacketCounterAddress);

        mMethodOverride(m_Tha60290011ModulePmcOverride, PdhMuxDe1BitGet);
        mMethodOverride(m_Tha60290011ModulePmcOverride, PdhMuxDe3BitGet);
        }

    mMethodsSet(module, &m_Tha60290011ModulePmcOverride);
    }

static void MethodsInit(Tha60290011ModulePmcV3 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HwPwCounterAddress);
        mMethodOverride(m_methods, HwPwCounterR2COffset);
        mMethodOverride(m_methods, HwSdhLineCounterAddress);
        mMethodOverride(m_methods, HwSdhAuVcCounterAddress);
        mMethodOverride(m_methods, HwSdhVtCounterAddress);
        mMethodOverride(m_methods, HwDe1CounterAddress);
        mMethodOverride(m_methods, HwDe3CounterAddress);
        mMethodOverride(m_methods, HwDe1SlipCounterAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    OverrideTha60290011ModulePmc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePmcV3);
    }

AtModule Tha60290011ModulePmcV3ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePmcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((Tha60290011ModulePmcV3)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePmcV3New(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePmcV3ObjectInit(module, self);
    }

uint8 Tha60290011ModulePmcV3EthPortTypeSw2Hw(uint8 portType)
    {
    return EthPortTypeSw2Hw(portType);
    }

uint32 Tha60290011ModulePmcV3PwCounterRead2Clear(ThaModulePmc self, AtPw pw, uint32 offset, eBool clear)
    {
    return PwCounterRead2Clear(self, pw, offset, clear);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60290011ModulePmc.h
 * 
 * Created Date: Dec 10, 2016
 *
 * Description : PMC module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPMC_H_
#define _THA60290011MODULEPMC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePmc *Tha60290011ModulePmc;
typedef struct tTha60290011ModulePmcV3 *Tha60290011ModulePmcV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModulePmcNew(AtDevice self);
AtModule Tha60290011ModulePmcV3New(AtDevice self);
AtModule Tha60290011ModulePmcV3dot2New(AtDevice self);
uint8 Tha60290011ModulePmcV3EthPortTypeSw2Hw(uint8 portType);
AtModule Tha60290011ModulePmcV3dot3New(AtDevice self);
AtModule Tha60290011ModulePmcV3dot4New(AtDevice self);

/* PW counters */
uint32 Tha60290011ModulePmcV3PwCounterRead2Clear(ThaModulePmc self, AtPw pw, uint32 offset, eBool clear);

/* For Classify debugging */
uint32 Tha60290011ModulePmcClaTxDe1BytesGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxDe3BytesGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxControlPacketBytesGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxDe1GoodPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxDe3GoodPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxControlGoodPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxDAErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxSAErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxEthTypeErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxLenErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxLenFieldErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxSequenceErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaTxFcsErrPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcClaRxPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);

/* For PDH MUX debugging */
uint32 Tha60290011ModulePmcPdhMuxPacketsGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcPdhMuxBytesGet(ThaModulePmc self, uint8 sliceId, eBool clear);
uint32 Tha60290011ModulePmcPdhDe1BitGet(ThaModulePmc self, uint32 channelId, eBool clear);
uint32 Tha60290011ModulePmcPdhDe3BitGet(ThaModulePmc self, uint32 channelId, eBool clear);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEPMC_H_ */


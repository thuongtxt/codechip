/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290011ModulePmcV3dot4.c
 *
 * Created Date: Apr 6, 2018
 *
 * Description : Implement FCS error counter for PMC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ModulePmcInternal.h"
#include "Tha60290011ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_mac1G_rxpkt_fcserr     0x93310UL
#define cAf6Reg_mac10G_rxpkt_fcserr    0x93311UL

#define cAf6Reg_mac2_5g_rxpk_fcserr    0x93237UL
#define cAf6Reg_mac_dim_rxpk_fcserr    0x93238UL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePmcMethods           m_ThaModulePmcOverride;

static const tThaModulePmcMethods           *m_ThaModulePmcMethods           = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DimMacPortPacketCounterCommonLocalAddress(ThaModulePmc self, uint32 offset, eBool clear)
    {
    AtUnused(self);

    return offset + (clear ? 64 : 0);
    }

static uint32 DimMacCounterLocalAdddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtEthPortCounterRxErrFcsPackets       :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac_dim_rxpk_fcserr, clear);
        default:
            return m_ThaModulePmcMethods->EthPortCounterLocalAddress(self, portType, counterType, clear);
        }
    }

static uint32 TwoDotFiveGitMacCounterLocalAdddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtEthPortCounterRxErrFcsPackets       :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac2_5g_rxpk_fcserr, clear);
        default:
            return m_ThaModulePmcMethods->EthPortCounterLocalAddress(self, portType, counterType, clear);
        }
    }

static uint32 OneGitDccMacCounterLocalAdddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtEthPortCounterRxErrFcsPackets       :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac1G_rxpkt_fcserr, clear);
        default:
            return m_ThaModulePmcMethods->EthPortCounterLocalAddress(self, portType, counterType, clear);
        }
    }

static uint32 TenGitDccMacCounterLocalAdddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {

    switch (counterType)
        {
        case cAtEthPortCounterRxErrFcsPackets       :
            return DimMacPortPacketCounterCommonLocalAddress(self, cAf6Reg_mac10G_rxpkt_fcserr, clear);
        default:
            return m_ThaModulePmcMethods->EthPortCounterLocalAddress(self, portType, counterType, clear);
        }
    }

static uint32 EthPortCounterLocalAddress(ThaModulePmc self, uint8 portType, uint16 counterType, eBool clear)
    {
    switch (portType)
        {
        case cThaModulePmcEthPortType10GPort:
            return TenGitDccMacCounterLocalAdddress(self, portType, counterType, clear);
        case cThaModulePmcEthPortType2Dot5GPort:
            return TwoDotFiveGitMacCounterLocalAdddress(self, portType, counterType, clear);
        case cThaModulePmcEthPortTypeDccPort:
            return OneGitDccMacCounterLocalAdddress(self, portType, counterType, clear);
        case cThaModulePmcEthPortTypeDimCtrlPort:
            return DimMacCounterLocalAdddress(self, portType, counterType, clear);
        default:
            return cInvalidUint32;
        }
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePmcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, m_ThaModulePmcMethods, sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, EthPortCounterLocalAddress);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }


static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePmcV3dot4);
    }

AtModule Tha60290011ModulePmcV3dot4ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePmcV3dot3ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePmcV3dot4New(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePmcV3dot4ObjectInit(module, self);
    }

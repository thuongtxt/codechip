/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210031ModuleMap.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha60290011PdhDe1.h"
#include "../pdh/Tha60290011ModulePdh.h"
#include "Tha60290011ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaMapTmDs1E1IdleEnableMask                 cBit28
#define cThaMapTmDs1E1IdleEnableShift                28
#define cThaMapTmDs1E1IdleCodeMask                   cBit27_20
#define cThaMapTmDs1E1IdleCodeShift                  20

#define cThaMapFirstTsMask     cBit14
#define cThaMapFirstTsShift    14

#define cThaMapChnTypeMask     cBit13_11
#define cThaMapChnTypeShift    11

#define cThaMapTsEnMask        cBit10
#define cThaMapTsEnShift       10

#define cThaMapPWIdFieldMask   cBit9_0
#define cThaMapPWIdFieldShift  0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleMapMethods         m_ThaModuleMapOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Save Super Implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1IdleCode)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1IdleEnable)

static uint8 De1SignalType(uint16 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs1J1UnFrm: return 3;
        case cAtPdhE1UnFrm   : return 3;

        case cAtPdhDs1FrmSf  : return 0;
        case cAtPdhJ1FrmSf   : return 0;
        case cAtPdhE1Frm     : return 0;

        case cAtPdhDs1FrmEsf : return 1;
        case cAtPdhJ1FrmEsf  : return 1;
        case cAtPdhE1MFCrc   : return 1;

        default:
            return 2;
        }
    }

static uint8 De1SigType(uint16 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs1J1UnFrm: return 0;
        case cAtPdhDs1FrmSf  : return 0;
        case cAtPdhJ1FrmSf   : return 0;
        case cAtPdhDs1FrmEsf : return 0;
        case cAtPdhJ1FrmEsf  : return 0;

        case cAtPdhE1UnFrm   : return 1;
        case cAtPdhE1MFCrc   : return 1;
        case cAtPdhE1Frm     : return 1;

        default:
            return 0;
        }
    }

static eAtRet De1FrameModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType)
    {
    uint32 sts, vtg, vt;
    AtModule pdhModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePdh);
    eTha60290011PdhInterfaceType type = Tha60290011ModulePdhInterfaceTypeGet((AtModulePdh)pdhModule);

    if (type == cTha60290011PdhInterfaceTypeDe1)
        {
        uint32 offset, address, regVal;
        Tha60290011PdhDe1InterfaceChannelId2HwId((ThaPdhDe1)de1, &sts, &vtg, &vt);
        offset = (sts * 32UL) + (vtg * 4UL) + vt;
        address = ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)self, NULL) + offset;
        regVal  = mChannelHwRead(de1, address, cThaModuleMap);

        mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, NULL), mModuleMapShift(self, MapFrmType, NULL), De1SignalType(frameType));
        mChannelHwWrite(de1, address, regVal, cThaModuleMap);

        return cAtOk;
        }

    return m_ThaModuleAbstractMapMethods->De1FrameModeSet(self, de1, frameType);
    }

static void AllDe1InHwStsFrameModeDefaultSet(ThaModuleMap self, uint32 hwSts, uint16 frameType)
    {
    uint32 vtg, vt;
    const uint32 cMaxChannelInVtg = AtPdhDe1FrameTypeIsE1(frameType) ? 3 : 4;
    const uint32 cMaxVtgInSts = 7;

    for (vtg = 0; vtg < cMaxVtgInSts; vtg++)
        for (vt = 0; vt < cMaxChannelInVtg; vt++)
            {
            uint32 offset = (hwSts * 32UL) + (vtg * 4UL) + vt;
            uint32 address = ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)self, NULL) + offset;
            uint32 regVal  = mModuleHwRead(self, address);

            mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, NULL), mModuleMapShift(self, MapFrmType, NULL), De1SignalType(frameType));
            mFieldIns(&regVal, mModuleMapMask(self, MapSigType, NULL), mModuleMapShift(self, MapSigType, NULL), De1SigType(frameType));
            mModuleHwWrite(self, address, regVal);
            }
    }

static void LiuAllDs1MapLineControlDefaultAssign(ThaModuleMap self)
    {
    uint32 sts;
    uint32 startSts = cTha60290011StartDs1StsHwId;
    uint32 maxSts   = startSts + cTha60290011NumDs1HwSts;

    for (sts = startSts; sts < maxSts; sts++)
        AllDe1InHwStsFrameModeDefaultSet(self, sts, cAtPdhDs1J1UnFrm);
    }

static void LiuAllE1MapLineControlDefaultAssign(ThaModuleMap self)
    {
    uint32 sts;
    uint32 startSts = cTha60290011StartE1StsHwId;
    uint32 maxSts   = startSts + cTha60290011NumE1HwSts;

    for (sts = startSts; sts < maxSts; sts++)
        AllDe1InHwStsFrameModeDefaultSet(self, sts, cAtPdhE1UnFrm);
    }


static void OverrideThaModuleAbstractMap(ThaModuleMap self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, De1FrameModeSet);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideThaModuleMap(ThaModuleMap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(self), sizeof(m_ThaModuleMapOverride));

        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapPWIdField)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1E1IdleCode)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1E1IdleEnable)
        }

    mMethodsSet(self, &m_ThaModuleMapOverride);
    }

static void Override(ThaModuleMap self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleMap);
    }

AtModule Tha60290011ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaModuleMap)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleMapObjectInit(newModule, device);
    }

void Tha60290011ModuleMapDe1LineModeControlDefault(ThaModuleMap self)
    {
    LiuAllDs1MapLineControlDefaultAssign(self);
    LiuAllE1MapLineControlDefaultAssign(self);
    }

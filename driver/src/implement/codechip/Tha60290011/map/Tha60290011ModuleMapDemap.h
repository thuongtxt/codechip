/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60290011ModuleMapDemap.h
 * 
 * Created Date: Jun 11, 2016
 *
 * Description : Interface of the MAP/DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEMAPDEMAP_H_
#define _THA60290011MODULEMAPDEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "AtPdhDe1.h"
#include "../../Tha60210031/map/Tha60210031ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleMap *Tha60290011ModuleMap;
typedef struct tTha60290011ModuleDemap *Tha60290011ModuleDemap;

typedef struct tTha60290011ModuleMap
    {
    tTha60210031ModuleMap super;
    }tTha60290011ModuleMap;

typedef struct tTha60290011ModuleDemap
    {
    tTha60210031ModuleDemap super;
    }tTha60290011ModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60290011ModuleMapDe1LineModeControlDefault(ThaModuleMap self);
AtModule Tha60290011ModuleDemapObjectInit(AtModule self, AtDevice device);
AtModule Tha60290011ModuleMapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEMAPDEMAP_H_ */


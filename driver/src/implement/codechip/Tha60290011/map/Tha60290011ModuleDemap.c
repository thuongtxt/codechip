/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60210031ModuleDemap.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : DEMAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/map/Tha60210031ModuleMapDemap.h"
#include "Tha60290011ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaDmapFirstTsMask     cBit14
#define cThaDmapFirstTsShift    14
#define cThaDmapChnTypeMask     cBit13_11
#define cThaDmapChnTypeShift    11
#define cThaDmapTsEnMask        cBit10
#define cThaDmapTsEnShift       10
#define cThaDmapPwIdMask        cBit9_0
#define cThaDmapPwIdShift       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleDemapMethods m_ThaModuleDemapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap mapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapChnType)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapFirstTs)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapTsEn)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapPwId)
        }

    mMethodsSet(mapModule, &m_ThaModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleDemap);
    }

AtModule Tha60290011ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleDemapObjectInit(newModule, device);
    }


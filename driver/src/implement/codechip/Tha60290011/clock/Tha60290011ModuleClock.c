/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60290011ModuleClock.c
 *
 * Created Date: Oct 24, 2016
 *
 * Description : Tha60290011ModuleClock implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60150011/man/Tha60150011Device.h"
#include "../../Tha60210031/clock/Tha60210031ModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011ModuleClock
    {
    tTha60210031ModuleClock super;
    }tTha60290011ModuleClock;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 MonitorOutputSw2Hw(ThaModuleClock self, eThaClockMonitorOutput refClockSource)
    {
    AtUnused(self);

    switch ((uint16)refClockSource)
        {
        case cThaClockMonitorOutputSpareSerdesDiv2 : return 0x6;
        case cThaClockMonitorOutputXfi155_52M_0Div2: return 0x4;
        case cThaClockMonitorOutputXfi155_52M_1Div2: return 0x5;
        case cThaClockMonitorOutputDdr_1Div2       : return 0x8;
        case cThaClockMonitorOutputDdr_2Div2       : return 0x9;
        case cThaClockMonitorOutputDdr_3Div2       : return 0xa;
        case cThaClockMonitorOutputPcieDiv2        : return 0x0;
        case cThaClockMonitorOutputPrc             : return 0x2;
        case cThaClockMonitorOutputExt             : return 0x3;
        case cThaClockMonitorOutputSystem          : return 0x1;

        default:
            return 0x1F;
        }
    }

static eThaClockMonitorOutput MonitorOutputHw2Sw(ThaModuleClock self, uint16 refClockSource)
    {
    AtUnused(self);

    switch (refClockSource)
        {
        case 0x6: return cThaClockMonitorOutputSpareSerdesDiv2;
        case 0x4: return cThaClockMonitorOutputXfi155_52M_0Div2;
        case 0x5: return cThaClockMonitorOutputXfi155_52M_1Div2;
        case 0x8: return cThaClockMonitorOutputDdr_1Div2;
        case 0x9: return cThaClockMonitorOutputDdr_2Div2;
        case 0xa: return cThaClockMonitorOutputDdr_3Div2;
        case 0x0: return cThaClockMonitorOutputPcieDiv2;
        case 0x2: return cThaClockMonitorOutputPrc;
        case 0x3: return cThaClockMonitorOutputExt;
        case 0x1: return cThaClockMonitorOutputSystem;

        default:
            return cThaClockMonitorOutputNone;
        }
    }

static eBool MonitorOutputIsSupported(ThaModuleClock self, eThaClockMonitorOutput refClockSource)
    {
    AtUnused(self);

    switch ((uint16)refClockSource)
        {
        case cThaClockMonitorOutputOverheadDiv2    : return cAtFalse;
        case cThaClockMonitorOutputXfi156_25M_0Div2: return cAtFalse;
        case cThaClockMonitorOutputXfi156_25M_1Div2: return cAtFalse;
        case cThaClockMonitorOutputEth40G_0Div2    : return cAtFalse;
        case cThaClockMonitorOutputEth40G_1Div2    : return cAtFalse;
        case cThaClockMonitorOutputQdrDiv2         : return cAtFalse;
        case cThaClockMonitorOutputDdr_4Div2       : return cAtFalse;

        default:
            return cAtTrue;
        }
    }

static uint8 MonitorOutputNumGet(ThaModuleClock self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 AllClockCheck(AtModuleClock self)
    {
    AtUnused(self);
    return 0;
    }

static eBool AllClockCheckIsSupported(AtModuleClock self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Debug(AtModule self)
    {
    Tha60150011DeviceClockStatusDisplay(AtModuleDeviceGet(self));
    return cAtOk;
    }

static eBool SquelchingIsSupported(ThaModuleClock self)
    {
    /* No requirement on this product */
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, AllClockCheck);
        mMethodOverride(m_AtModuleClockOverride, AllClockCheckIsSupported);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, MonitorOutputNumGet);
        mMethodOverride(m_ThaModuleClockOverride, MonitorOutputSw2Hw);
        mMethodOverride(m_ThaModuleClockOverride, MonitorOutputHw2Sw);
        mMethodOverride(m_ThaModuleClockOverride, MonitorOutputIsSupported);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingIsSupported);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModule(self);
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleClock);
    }

static AtModuleClock ObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60290011ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

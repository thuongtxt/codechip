/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290011ModulePwe.h
 *
 * Created Date: Nov 18, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPWE_H_
#define _THA60290011MODULEPWE_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290011ModulePwePwSubPortVlanIndexSet(ThaModulePwe self, AtPw adapter, uint32 subPortVlanIndex);
uint32 Tha60290011ModulePwePwSubPortVlanIndexGet(ThaModulePwe self, AtPw adapter);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPWE_H_ */

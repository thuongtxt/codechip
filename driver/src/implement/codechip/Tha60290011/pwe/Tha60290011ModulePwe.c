/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290011ModulePwe.c
 *
 * Created Date: Oct 28, 2016
 *
 * Description : PWE module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../man/Tha60290011DeviceReg.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "Tha60290011ModulePwe.h"
#include "Tha60290011ModulePweInternal.h"
#include "Tha60290011ModulePweReg.h"
#include "Tha60290011ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mBaseAddress(self) ThaModulePweBaseAddress((ThaModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePweMethods         m_ThaModulePweOverride;
static tTha60150011ModulePweMethods m_Tha60150011ModulePweOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModulePweMethods *m_ThaModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePw PwModule(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 PwPlaDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    ThaModulePw pwModule = PwModule(self);
    uint8 slice = 0;/* Note this is for the product with number of slice is 1 */
    return AtChannelHwIdGet((AtChannel)pw) + (slice * 0x010000UL) + ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    }

static eAtRet MaxPldSizeDefaultSet(ThaModulePwe self)
	{
	uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
	uint32 value  = mModuleHwRead(self, address);

	mFieldIns(&value, cAf6_pw_tx_glb_ctrl_MaxPldSize_Mask, cAf6_pw_tx_glb_ctrl_MaxPldSize_Shift, 0xFFF);
	mModuleHwWrite(self, address, value);

	return cAtOk;
	}

static eBool JitterAttenuatorIsSupported(Tha60150011ModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet DefaultSet(ThaModulePwe self)
    {
	eAtRet ret = m_ThaModulePweMethods->DefaultSet(self);

	ThaModulePweSubPortVlanBypass(self, cAtFalse);
	MaxPldSizeDefaultSet(self);

    return ret;
    }

static eAtRet Debug(AtModule self)
	{
	ThaModulePwe pwe = (ThaModulePwe)self;

	m_AtModuleMethods->Debug(self);
	AtPrintc(cSevInfo, "\r\n");
	AtPrintc(cSevInfo, "Subport VLAN bypass: %s\r\n", ThaModulePweSubPortVlanIsBypassed(pwe) ? "Enabled" : "Disabled");

	return cAtOk;
	}

static eAtRet SubPortVlanBypass(ThaModulePwe self, eBool bypass)
	{
    uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
    uint32 value  = mModuleHwRead(self, address);

    mFieldIns(&value, cAf6_pw_tx_glb_ctrl_SubPortVlanByPass_Mask, cAf6_pw_tx_glb_ctrl_SubPortVlanByPass_Shift, bypass ? 1 : 0);
    mModuleHwWrite(self, address, value);

    return cAtOk;
    }

static eBool SubPortVlanIsBypassed(ThaModulePwe self)
	{
    uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
    uint32 value  = mModuleHwRead(self, address);
    return (value & cAf6_pw_tx_glb_ctrl_SubPortVlanByPass_Mask) ? cAtTrue : cAtFalse;
    }

static eBool HasRole(AtModule self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet HwRoleActiveForce(AtModule self, eBool forced)
    {
    uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
    uint32 value  = mModuleHwRead(self, address);

    mFieldIns(&value, cAf6_pw_tx_glb_ctrl_ForceFSM_Mask, cAf6_pw_tx_glb_ctrl_ForceFSM_Shift, forced ? 1 : 0);
    mModuleHwWrite(self, address, value);

    return cAtOk;
    }

static eAtRet RoleActiveForce(AtModule self, eBool forced)
    {
    HwRoleActiveForce(self, forced);
    return Tha60290011ModuleEthTxFsmPinForceStandby((ThaModuleEth)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleEth), !forced);
    }

static eBool RoleActiveIsForced(AtModule self)
    {
    uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
    uint32 value = mModuleHwRead(self, address);
    
    return (mRegField(value, cAf6_pw_tx_glb_ctrl_ForceFSM_) == 1) ? cAtTrue : cAtFalse;
    }


static eAtRet HwRoleInputEnable(AtModule self, eBool enabled)
    {
    uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
    uint32 value = mModuleHwRead(self, address);

    mFieldIns(&value, cAf6_pw_tx_glb_ctrl_FSMPinEn_Mask, cAf6_pw_tx_glb_ctrl_FSMPinEn_Shift, enabled ? 1 : 0);
    mModuleHwWrite(self, address, value);

    return cAtOk;
    }

static eAtRet RoleInputEnable(AtModule self, eBool enabled)
    {
    HwRoleInputEnable(self, enabled);
    return Tha60290011ModuleEthTxFsmPinEnable((ThaModuleEth)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleEth), enabled);
    }

static eBool RoleInputIsEnabled(AtModule self)
    {
    uint32 address = cAf6Reg_pw_tx_glb_ctrl + mBaseAddress(self);
    uint32 value = mModuleHwRead(self, address);
    
    return (mRegField(value, cAf6_pw_tx_glb_ctrl_FSMPinEn_) == 1) ? cAtTrue : cAtFalse;
    }

static eBool TxFsmPinStatus(AtModule self)
    {
    uint32 topRegAddr = cTopBaseAddress + cAf6Reg_status_top_Base;
    uint32 regVal  = mModuleHwRead(self, topRegAddr);

    return mRegField(regVal, cAf6_status_top_txfsm0sta_) ? cAtTrue : cAtFalse;
    }

static eAtDeviceRole RoleInputStatus(AtModule self)
    {
    return TxFsmPinStatus(self) ? cAtDeviceRoleStandby : cAtDeviceRoleActive;
    }

static eAtRet PwSubPortVlanIndexSet(ThaModulePwe self, AtPw adapter, uint32 subPortVlanIndex)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_len_ctrl + mPwPweOffset(self, adapter) + mBaseAddress(self);
    uint32 regVal  = mChannelHwRead(adapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_len_ctrl_SubType_VLAN_, subPortVlanIndex);
    mChannelHwWrite(adapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PwSubPortVlanIndexGet(ThaModulePwe self, AtPw adapter)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_len_ctrl + mPwPweOffset(self, adapter) + mBaseAddress(self);
    uint32 regVal  = mChannelHwRead(adapter, regAddr, cThaModulePwe);

    return (uint8)mRegField(regVal, cAf6_pw_txeth_hdr_len_ctrl_SubType_VLAN_);
    }

static eBool TxPwActiveHwIsReady(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModulePmc PmcModule(AtPw self)
	{
	return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwPmc);
	}

static eBool GetCounterFromPmc(ThaModulePwe self)
	{
	ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
	return (ThaModulePwDebugCountersModuleGet(pwModule) == cThaModulePwPmc) ? cAtTrue : cAtFalse;
	}

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxPacketsGet(PmcModule(pw), pw, clear);
    return m_ThaModulePweMethods->PwTxPacketsGet(self, pw, clear);
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxBytesGet(PmcModule(pw), pw, clear);
	return m_ThaModulePweMethods->PwTxBytesGet(self, pw, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxLbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePweMethods->PwTxLbitPacketsGet(self, pw, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxRbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePweMethods->PwTxRbitPacketsGet(self, pw, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxMbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePweMethods->PwTxMbitPacketsGet(self, pw, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxPbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePweMethods->PwTxPbitPacketsGet(self, pw, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	if (GetCounterFromPmc(self))
		return ThaModulePmcPwTxNbitPacketsGet(PmcModule(pw), pw, clear);
	return m_ThaModulePweMethods->PwTxNbitPacketsGet(self, pw, clear);
    }

static eAtRet PwJitterAttenuatorEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    eBool shouldEnable = enable;

    /* HW recommends only enable Tx Shaper for DE1 over STM (async paths) */
    if (!ThaModulePweUtilPwCircuitIsDe1OverStm(pw))
        shouldEnable = cAtFalse;

    return m_ThaModulePweMethods->PwJitterAttenuatorEnable(self, pw, shouldEnable);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, TxPwActiveHwIsReady);
        mMethodOverride(m_ThaModulePweOverride, DefaultSet);
        mMethodOverride(m_ThaModulePweOverride, SubPortVlanBypass);
        mMethodOverride(m_ThaModulePweOverride, SubPortVlanIsBypassed);
        mMethodOverride(m_ThaModulePweOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwJitterAttenuatorEnable);
        mMethodOverride(m_ThaModulePweOverride, PwPlaDefaultOffset);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideTha60150011ModulePwe(AtModule self)
    {
    Tha60150011ModulePwe pweModule = (Tha60150011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePweOverride, mMethodsGet(pweModule), sizeof(m_Tha60150011ModulePweOverride));

        mMethodOverride(m_Tha60150011ModulePweOverride, JitterAttenuatorIsSupported);
        }

    mMethodsSet(pweModule, &m_Tha60150011ModulePweOverride);
    }

static void OverrideAtModule(AtModule self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, HasRole);
        mMethodOverride(m_AtModuleOverride, RoleActiveForce);
        mMethodOverride(m_AtModuleOverride, RoleActiveIsForced);
        mMethodOverride(m_AtModuleOverride, RoleInputEnable);
        mMethodOverride(m_AtModuleOverride, RoleInputIsEnabled);
        mMethodOverride(m_AtModuleOverride, RoleInputStatus);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60150011ModulePwe(self);
    OverrideThaModulePwe(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePwe);
    }

AtModule Tha60290011ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePweObjectInit(newModule, device);
    }

eAtRet Tha60290011ModulePwePwSubPortVlanIndexSet(ThaModulePwe self, AtPw adapter, uint32 subPortVlanIndex)
    {
    if (self && adapter)
        return PwSubPortVlanIndexSet(self, adapter, subPortVlanIndex);
    return cAtErrorNullPointer;
    }

uint32 Tha60290011ModulePwePwSubPortVlanIndexGet(ThaModulePwe self, AtPw adapter)
    {
    if (self && adapter)
        return PwSubPortVlanIndexGet(self, adapter);
    return cInvalidUint32;
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_PWE_H_
#define _AF6_REG_AF6CNC0011_RD_PWE_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet Header Value Control
Reg Addr   : 0x00008000-0x0000AA00
Reg Formula: 0x00008000  + $PwId*16 + $Entry
    Where  : 
           + IP_Protocol[7:0]: 0x11 to signify UDP
           + IP_Header_Sum[31:0]:
           + {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +
           + IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +
           + {IP_TTL[7:0], IP_Protocol[7:0]} +
           + IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +
           + IP_DesAdr[31:16] + IP_DesAdr[15:0]
           + UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused
           + UDP_DesPort : used as remote pseudowire identification or 0x85E if unused
           + PSN header is UDP/Ipv6:
           + EthType:  0x86DD
           + 48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:
           + {IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}
           + {16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]}
           + {IP_SrcAdr[127:96]}
           + {IP_SrcAdr[95:64]}
           + {IP_SrcAdr[63:32]}
           + {IP_SrcAdr[31:0]}
           + {IP_DesAdr[127:96]}
           + {IP_DesAdr[95:64]}
           + {IP_DesAdr[63:32]}
           + {IP_DesAdr[31:0]}
           + {UDP_SrcPort[15:0], UDP_DesPort[15:0]}
           + {UDP_Sum[31:0]}
           + IP_Next_Header[7:0]: 0x11 to signify UDP
           + UDP_Sum[31:0]:
           + IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] +
           + IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +
           + IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +
           + IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +
           + IP_DesAdr[127:112] + 	IP_DesAdr[111:96] +
           + IP_DesAdr[95:80]  + IP_DesAdr[79:64] +
           + IP_DesAdr[63:48]  + IP_DesAdr[47:32] +
           + IP_DesAdr[31:16]  + IP_DesAdr[15:0] +
           + {8-bit zeros, IP_Next_Header[7:0]} +
           + UDP_SrcPort[15:0] +  UDP_DesPort[15:0]
           + UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused
           + UDP_DesPort : used as remote pseudowire identification or 0x85E if unused
           + User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field.
           + PSN header is MPLS over Ipv4:
           + IPV4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS
           + After IPV4 header is MPLS labels where inner label is used for PW identification
           + PSN header is MPLS over Ipv6:
           + IPV6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS
           + After IPV6 header is MPLS labels where inner label is used for PW identification
Reg Desc   : 
This register configures value of transmit Ethernet pseudowire header. Each pseudowire has 16 entries (1 entry contains 32 bits) for header value configuration. The header format from %% //entry#0 to entry#15 is as follow:  %%
{DA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 48 bytes)} %%
Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%
PSN header is MEF-8:  %%
EthType:  0x88D8 %%
4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %%
PSN header is MPLS:  %%
EthType:  0x8847 %%
4/8-byte PSN Header with format: {OuterLabel[31:0](optional), InnerLabel[31:0]}  where 	InnerLabel[31:12] is pseodowire identification for remote receive side. %%
Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit = 1 for InnerLabel %%
PSN header is UDP/Ipv4:   %%
EthType:  0x0800 %%
28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%

{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
{IP_SrcAdr[31:0]}%%
{IP_DesAdr[31:0]}%%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
{32-bit zeros}%%


------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_hdr                                                                        0x00008000

/*--------------------------------------
BitField Name: TxEthPwHeadValue
BitField Type: RW
BitField Desc: Transmit Ethernet Pseudowire Header value in each entry
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_TxEthPwHeadValue_Mask                                                       cBit31_0
#define cAf6_pw_txeth_hdr_TxEthPwHeadValue_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet Header Length Control
Reg Addr   : 0x0001000-0x0000129F #The address format for these registers is 0x0001000 + $PWID
Reg Formula: 0x0001000 +  $PWID
    Where  : 
           + $PWID(0-671): Pseudowire ID
Reg Desc   : 
This register configures length in number of bytes of AF6F1 transmit Ethernet header.

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_hdr_len_ctrl                                                                0x0001000

/*--------------------------------------
BitField Name: SubType VLAN
BitField Type: RW
BitField Desc: Select SubType VLAN
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_SubType_VLAN_Mask                                                    cBit23
#define cAf6_pw_txeth_hdr_len_ctrl_SubType_VLAN_Shift                                                       23

/*--------------------------------------
BitField Name: TxEthPwRtpPtValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the PT value of RTP header, define in
http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwRtpPtValue_Mask                                            cBit22_16
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwRtpPtValue_Shift                                                  16

/*--------------------------------------
BitField Name: TxEthPwRtpEn
BitField Type: RW
BitField Desc: Used for TDM PW 1: Enable RTP field in PSN header (used for TDM
PW with DCR timing) 0: Disable RTP field in PSN header (used for ATM PW or TDM
PW without DCR timing)
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwRtpEn_Mask                                                    cBit15
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwRtpEn_Shift                                                       15

/*--------------------------------------
BitField Name: TxEthPwPsnType
BitField Type: RW
BitField Desc: PW PSN Type 1: PW PSN header is UDP/IPv4 2: PW PSN header is
UDP/IPv6 3: PW PSN header is MPLS and EXP bits of inner label is replaced by ATM
cell priority bits (used for ATM PW) 4: PW MPLS no outer label over Ipv4 (total
1 MPLS label) 5: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 6: PW
MPLS one outer label over Ipv4 (total 2 MPLS label) 7: PW MPLS one outer label
over Ipv6 (total 2 MPLS label) Others: for other PW PSN header type (include
MPLS with configuration EXP bits)
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwPsnType_Mask                                               cBit14_12
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwPsnType_Shift                                                     12

/*--------------------------------------
BitField Name: TxEthPwCwType
BitField Type: RW
BitField Desc: 0: Control word 4-byte (used for PDH/ATM N to one pseudowire) 1:
Control word 3-byte (used for ATM one to one VCC/VPC pseudowire)
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwCwType_Mask                                                   cBit11
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwCwType_Shift                                                      11

/*--------------------------------------
BitField Name: TxEthPwNumVlan
BitField Type: RW
BitField Desc: Number of VLANs in Transmit Ethernet Pseudowire Header.
BitField Bits: [10:9]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwNumVlan_Mask                                                cBit10_9
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwNumVlan_Shift                                                      9

/*--------------------------------------
BitField Name: TxEthPwNumMplsOutLb
BitField Type: RW
BitField Desc: Number of MPLS outer labels in Transmit Ethernet Pseudowire
Header. This field is only applicable when the pseudowire selects MPLS as its
PSN header, // //                              otherwise, this field must be set
to zero value.
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwNumMplsOutLb_Mask                                            cBit8_7
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwNumMplsOutLb_Shift                                                 7

/*--------------------------------------
BitField Name: TxEthPwHeadLen
BitField Type: RW
BitField Desc: Length in number of bytes of Transmit Ethernet Pseudowire Header.
This length is counted from start of Ethernet packet (DA) to end of pseudowire
header (the next of //          pseudowire header is control word and TDM
payload). Be noted that this length field includes SA field for counting too
(although the below Pseudowire Transmit Ethernet Header Value Control does //
not contains SA field because AF6FHW0013 already has a global MacAddress for the
SA field). In case of VLAN TAG mode contains PWID (DA,SA,VLAN1,VLAN2), header
length value equal to 0x14
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwHeadLen_Mask                                                 cBit6_0
#define cAf6_pw_txeth_hdr_len_ctrl_TxEthPwHeadLen_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet PAD Control
Reg Addr   : 0x0002000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures mode to insert number of PAD bytes of AF6FH1 transmit Ethernet

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_pad_ctrl                                                                    0x0002000

/*--------------------------------------
BitField Name: TxEthPadMode
BitField Type: RW
BitField Desc: Modes to insert PAD for pseudowire packet at Ethernet transmit
direction 0: Insert PAD when total Ethernet packet length is less than 64 bytes.
Refer to IEEE 802.3 1: Insert PAD when control word plus payload length is less
than 64 bytes 2: Insert PAD when total Ethernet packet length minus VLANs and
MPLS outer labels(if exist) is less than 64 bytes. The reason is that networking
devices often insert or remove VLANs and MPLS outer //labels when packet
traverse them
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pw_txeth_pad_ctrl_TxEthPadMode_Mask                                                       cBit1_0
#define cAf6_pw_txeth_pad_ctrl_TxEthPadMode_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Header RTP SSRC Value Control
Reg Addr   : 0x0003000-0x0000329F #The address format for these registers is 0x0003000 + $PWID
Reg Formula: 0x0003000 +  $PWID
    Where  : 
           + $PWID(0-671): Pseudowire ID
Reg Desc   : 
Used for TDM PW. This register configures RTP SSRC value.

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_rtp_ssrc_ctrl                                                               0x0003000

/*--------------------------------------
BitField Name: TxEthPwRtpSsrcValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the SSRC value of RTP header, define in
RFC3550
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pw_txeth_rtp_ssrc_ctrl_TxEthPwRtpSsrcValue_Mask                                          cBit31_0
#define cAf6_pw_txeth_rtp_ssrc_ctrl_TxEthPwRtpSsrcValue_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Enable Control
Reg Addr   : 0x0021000-0x0002129F #The address format for these registers is 0x0021000 + $PWID
Reg Formula: 0x0021000 +  $PWID
    Where  : 
           + $PWID(0-671): Pseudowire ID
Reg Desc   : 
This register configures pseudowire enable for transmit to Ethernet direction

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_enable_ctrl                                                                 0x0021000

/*--------------------------------------
BitField Name: TxPwEthPortId
BitField Type: RW
BitField Desc: 0: PW will be sent to ETH SGMII port0, ignore in XGMII mode 1: PW
will be sent to ETH SGMII port1, ignore in XGMII mode 2: PW will be sent to ETH
SGMII port2, ignore in XGMII mode 3: PW will be sent to ETH SGMII port3, ignore
in XGMII mode 4: PW will be sent to ETH SGMII port4, ignore in XGMII mode
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwEthPortId_Mask                                                  cBit10_8
#define cAf6_pw_txeth_enable_ctrl_TxPwEthPortId_Shift                                                        8

/*--------------------------------------
BitField Name: TxPwSuppressEn
BitField Type: RW
BitField Desc: 0: normal 1: Enable payload suppression when AIS
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwSuppressEn_Mask                                                    cBit7
#define cAf6_pw_txeth_enable_ctrl_TxPwSuppressEn_Shift                                                       7

/*--------------------------------------
BitField Name: TxPwCwMbitDisable
BitField Type: RW
BitField Desc: 0: normal 1: Disable Mbit/NPbit in control word
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwCwMbitDisable_Mask                                                 cBit6
#define cAf6_pw_txeth_enable_ctrl_TxPwCwMbitDisable_Shift                                                    6

/*--------------------------------------
BitField Name: TxPwCwLbitDisable
BitField Type: RW
BitField Desc: 0: normal 1: Disable Lbit in control word
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwCwLbitDisable_Mask                                                 cBit5
#define cAf6_pw_txeth_enable_ctrl_TxPwCwLbitDisable_Shift                                                    5

/*--------------------------------------
BitField Name: TxPwCwMbitCpu
BitField Type: RW
BitField Desc: low priority than TxPwLbitDisable 0: normal other: CPU force Mbit
BitField Bits: [4:3]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwCwMbitCpu_Mask                                                   cBit4_3
#define cAf6_pw_txeth_enable_ctrl_TxPwCwMbitCpu_Shift                                                        3

/*--------------------------------------
BitField Name: TxPwCwLbitCpu
BitField Type: RW
BitField Desc: low priority than TxPwLbitDisable 0: normal 1: CPU force Lbit in
control word
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwCwLbitCpu_Mask                                                     cBit2
#define cAf6_pw_txeth_enable_ctrl_TxPwCwLbitCpu_Shift                                                        2

/*--------------------------------------
BitField Name: TxPwCwType
BitField Type: RW
BitField Desc: 0: Control word 4-byte (used for PDH/ATM N to one pseudowire) 1:
Control word 3-byte (used for ATM one to one VCC/VPC pseudowire)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwCwType_Mask                                                        cBit1
#define cAf6_pw_txeth_enable_ctrl_TxPwCwType_Shift                                                           1

/*--------------------------------------
BitField Name: TxPwEn
BitField Type: RW
BitField Desc: 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pw_txeth_enable_ctrl_TxPwEn_Mask                                                            cBit0
#define cAf6_pw_txeth_enable_ctrl_TxPwEn_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit R bit Control
Reg Addr   : 0x0025000-0x0002529F #The address format for these registers is 0x0025000 + $PWID
Reg Formula: 0x0025000 +  $PWID
    Where  : 
           + $PWID(0-671): Pseudowire ID
Reg Desc   : 
This register configures pseudowire L/M bit control  for transmit to Ethernet direction

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_Rbit_ctrl                                                                   0x0025000

/*--------------------------------------
BitField Name: TxPwCepType
BitField Type: RW
BitField Desc: 0: CEP basic PW 1: CEP VC3(STS) fractional PW (only 1 dword VT
mask) 2: CEP VC4 fractional PW (3 dword   VT/STS mask)
BitField Bits: [4:3]
--------------------------------------*/
#define cAf6_pw_txeth_Rbit_ctrl_TxPwCepType_Mask                                                       cBit4_3
#define cAf6_pw_txeth_Rbit_ctrl_TxPwCepType_Shift                                                            3

/*--------------------------------------
BitField Name: TxPwCepEn
BitField Type: RW
BitField Desc: 0: CES PW 1: CEP PW
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pw_txeth_Rbit_ctrl_TxPwCepEn_Mask                                                           cBit2
#define cAf6_pw_txeth_Rbit_ctrl_TxPwCepEn_Shift                                                              2

/*--------------------------------------
BitField Name: TxPwRbitCpu
BitField Type: RW
BitField Desc: low priority than TxPwRbitDisable 0: Normal 1: CPU force Rbit at
transmit PW direction
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pw_txeth_Rbit_ctrl_TxPwRbitCpu_Mask                                                         cBit1
#define cAf6_pw_txeth_Rbit_ctrl_TxPwRbitCpu_Shift                                                            1

/*--------------------------------------
BitField Name: TxPwRbitDisable
BitField Type: RW
BitField Desc: 1: Disable sending Rbit in PW control word 0: Normal
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pw_txeth_Rbit_ctrl_TxPwRbitDisable_Mask                                                     cBit0
#define cAf6_pw_txeth_Rbit_ctrl_TxPwRbitDisable_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit HSPW Label Control
Reg Addr   : 0x005000-0x00529F
Reg Formula: 0x005000 + PwId
    Where  : 
           + $PwId(0 - 671): Pseudowire Identification
Reg Desc   : 
Used for TDM PW. This register configures "label ID" when HSPW working at protection mode value.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_HSPW_Label_Control                                                0x005000

/*--------------------------------------
BitField Name: TxEthPwHspwLabelValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW label
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit UPSR and HSPW Control
Reg Addr   : 0x006000-0x00629F
Reg Formula: 0x006000 + PwId
    Where  : 
           + $PwId(0 - 671): Pseudowire Identification
Reg Desc   : 
Used for TDM PW. This register configures UPSR and HSPW value.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_UPSR_and_HSPW_Control                                             0x006000
#define cAf6Reg_Pseudowire_Transmit_UPSR_and_HSPW_Control_WidthVal                                          32

/*--------------------------------------
BitField Name: TxEthPwUpsrUseValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the UPSR using or not 0: PW not join any
UPSR Group 1: PW join a UPSR Group
BitField Bits: [21]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Mask                                  cBit21
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Shift                                      21

/*--------------------------------------
BitField Name: TxEthPwHspwUseValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW using or not 0: PW not join any
HSPW Group 1: PW join a HSPW Group
BitField Bits: [20]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Mask                                  cBit20
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Shift                                      20

/*--------------------------------------
BitField Name: TxEthPwUpsrGrpValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the UPSR group
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Mask                               cBit19_10
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Shift                                      10

/*--------------------------------------
BitField Name: TxEthPwHspwGrpValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW group
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Mask                                 cBit9_0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit UPSR Group Control
Reg Addr   : 0x007000-0x00729F
Reg Formula: 0x007000 + TxEthPwUpsrGrpValue
    Where  : 
           + $TxEthPwUpsrGrpValue(0 - 671): UPSR Group Identification
Reg Desc   : 
Used for TDM PW. This register configures UPSR Group enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_UPSR_Group_Control                                                0x007000
#define cAf6Reg_Pseudowire_Transmit_UPSR_Group_Control_WidthVal                                             32

/*--------------------------------------
BitField Name: TxEthPwUpsrEnValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the UPSR Group enable or not 0: UPSR
Group disable 1: UPSR Group enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_Mask                                   cBit0
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit HSPW Protection Control
Reg Addr   : 0x004000-0x00429F
Reg Formula: 0x004000 + TxEthPwHspwGrpValue
    Where  : 
           + $TxEthPwHspwGrpValue(0 - 671): HSPW Group Identification
Reg Desc   : 
Used for TDM PW. This register configures HSPW Group Protection enable

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_HSPW_Protection_Control                                           0x004000
#define cAf6Reg_Pseudowire_Transmit_HSPW_Protection_Control_WidthVal                                        32

/*--------------------------------------
BitField Name: TxEthPwHspwEnValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW enable 0: HSPW Group using
normal label 1: HSPW Group using "HSPW Label" as lable for packet transmit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_Mask                                   cBit0
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Psedowire Transmit Global Control
Reg Addr   : 0x00020000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register controls operation global modes of Psedowire.

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_tx_glb_ctrl                                                                      0x00020000

#define cAf6_pw_tx_glb_ctrl_MaxPldSize_Mask                                                                cBit17_4
#define cAf6_pw_tx_glb_ctrl_MaxPldSize_Shift                                                                   4

/*--------------------------------------
BitField Name: ForceFSM
BitField Type: RW
BitField Desc: Force FSM state for selftest
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pw_tx_glb_ctrl_ForceFSM_Mask                                                                cBit3
#define cAf6_pw_tx_glb_ctrl_ForceFSM_Shift                                                                   3

/*--------------------------------------
BitField Name: FSMPinEn
BitField Type: RW
BitField Desc: Enable FSM from pin outside
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pw_tx_glb_ctrl_FSMPinEn_Mask                                                                cBit2
#define cAf6_pw_tx_glb_ctrl_FSMPinEn_Shift                                                                   2

/*--------------------------------------
BitField Name: SubPortVlanByPass
BitField Type: RW
BitField Desc: Bypass SubPort VLAN
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pw_tx_glb_ctrl_SubPortVlanByPass_Mask                                                       cBit1
#define cAf6_pw_tx_glb_ctrl_SubPortVlanByPass_Shift                                                          1

#endif /* _AF6_REG_AF6CNC0011_RD_PWE_H_ */

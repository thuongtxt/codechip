/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290011ModulePweInternal.h
 * 
 * Created Date: Jul 18, 2018
 *
 * Description : 60290011 module PWE internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPWEINTERNAL_H_
#define _THA60290011MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pwe/Tha60210031ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePwe
    {
    tTha60210031ModulePwe super;
    }tTha60290011ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModulePweObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPWEINTERNAL_H_ */


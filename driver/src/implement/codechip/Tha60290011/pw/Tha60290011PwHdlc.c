/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290011PwHdlc.c
 *
 * Created Date: Oct 13, 2016
 *
 * Description : HDLC PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60290021/pw/Tha60290021PwHdlcInternal.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "Tha60290011DccKbyteV2Reg.h"
#include "Tha60290011ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011PwHdlc
    {
    tTha60290021PwHdlc super;
    }tTha60290011PwHdlc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods            m_AtChannelOverride;
static tTha60290021PwHdlcMethods    m_Tha60290021PwHdlcOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods = NULL;
static const tTha60290021PwHdlcMethods *m_Tha60290021PwHdlcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(Tha60290021PwHdlc self)
    {
    return AtModuleDeviceGet((AtModule) AtChannelModuleGet((AtChannel) self));
    }

static ThaModuleOcn OcnModule(Tha60290021PwHdlc self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static AtIpCore Core(Tha60290021PwHdlc self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 BaseAddress(Tha60290021PwHdlc self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static eBool ShouldUseInterruptV2(AtChannel self)
    {
    return mMethodsGet((Tha60290021PwHdlc)self)->HasInterruptV2((Tha60290021PwHdlc)self);
    }

static uint32 LocalId(AtChannel self)
    {
    return AtChannelIdGet(self) % 32;
    }

static uint32 GroupId(AtChannel self)
    {
    return AtChannelIdGet(self) / 32;
    }

static uint32 EnableMask(AtChannel self)
    {
    return cBit0 << LocalId(self);
    }

static uint32 EnableShift(AtChannel self)
    {
    return LocalId(self);
    }

static uint32 DccInterruptEnableRegAddr(AtChannel self)
    {
    uint32 regAddr = (AtChannelIdGet(self) < 32) ? cAf6Reg_upen_rxdcc0_intenb_r0 : cAf6Reg_upen_rxdcc0_intenb_r1;
    return BaseAddress((Tha60290021PwHdlc)self) + regAddr;
    }

static eAtRet HwInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (defectMask & cTha6029DccPwAlarmTypePktDiscards)
        {
        uint32 regAddr = DccInterruptEnableRegAddr(self);
        uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
        mFieldIns(&regVal,
                  EnableMask(self),
                  EnableShift(self),
                  (enableMask & cTha6029DccPwAlarmTypePktDiscards) ? 1 : 0);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
        }

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (ShouldUseInterruptV2(self))
        return m_AtChannelMethods->InterruptMaskSet(self, defectMask, enableMask);
    return HwInterruptMaskSet(self, defectMask, enableMask);
    }

static uint32 HwInterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = DccInterruptEnableRegAddr(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask = 0;

    if (regVal & EnableMask(self))
        alarmMask |= cTha6029DccPwAlarmTypePktDiscards;

    return alarmMask;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return m_AtChannelMethods->InterruptMaskGet(self);
    return HwInterruptMaskGet(self);
    }

static uint32 DccStatusRegAddr(AtChannel self)
    {
    uint32 regAddr = (AtChannelIdGet(self) < 32) ? cAf6Reg_upen_rxdcc0_intenb_r0 : cAf6Reg_upen_rxdcc0_intenb_r1; /* TODO: Wait for correct address. */
    return BaseAddress((Tha60290021PwHdlc)self) + regAddr;
    }

static uint32 HwDefectGet(AtChannel self)
    {
    uint32 regAddr = DccStatusRegAddr(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);

    if (regVal & EnableMask(self))
        return cTha6029DccPwAlarmTypePktDiscards;

    return 0;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return m_AtChannelMethods->DefectGet(self);
    return HwDefectGet(self);
    }

static uint32 DccInterruptStatusRegAddr(AtChannel self)
    {
    uint32 regAddr = (AtChannelIdGet(self) < 32) ? cAf6Reg_upen_int_rxdcc0_stt_r0 : cAf6Reg_upen_int_rxdcc0_stt_r1;
    return BaseAddress((Tha60290021PwHdlc)self) + regAddr;
    }

static uint32 HwInterruptGet(AtChannel self, eBool r2c)
    {
    uint32 regAddr = DccInterruptStatusRegAddr(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 channelMask = EnableMask(self);
    uint32 alarmMask = 0;

    if (regVal & channelMask)
        {
        alarmMask |= cTha6029DccPwAlarmTypePktDiscards;
        /* Clear Interrupt Status */
        if (r2c)
            mChannelHwWrite(self, regAddr, channelMask, cAtModuleSdh);
        }

    return alarmMask;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return m_AtChannelMethods->DefectHistoryGet(self);
    return HwInterruptGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return m_AtChannelMethods->DefectHistoryClear(self);
    return HwInterruptGet(self, cAtTrue);
    }

static AtEthPort DefaultSgmiiEthPort(Tha60290021PwHdlc self)
    {
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel) self), cAtModuleEth);
    return Tha60290011ModuleEthSgmiiDccPortGet(ethModule);
    }

static uint32 DccRxMacCheckRegister(Tha60290021PwHdlc self)
    {
    return cAf6Reg_upen_dccrxmacenb + BaseAddress(self);
    }

static uint16 LongReadOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen)
    {
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);
    return mModuleHwLongRead(ocnModule, regAddr, longRegVal, bufferLen, core);
    }

static uint16 LongWriteOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen)
    {
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);
    return mModuleHwLongWrite(ocnModule, regAddr, longRegVal, bufferLen, core);
    }

static eAtRet DccPwMacCheckEnable(Tha60290021PwHdlc self, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)self;
    uint32 regAddr = DccRxMacCheckRegister(self);
    Tha60290021PwHdlcLongReadOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    mFieldIns(&longRegVal[GroupId(channel)], EnableMask(channel), EnableShift(channel), mBoolToBin(enable));
    Tha60290021PwHdlcLongWriteOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    return cAtOk;
    }

static eBool DccPwMacCheckIsEnabled(Tha60290021PwHdlc self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)self;
    uint32 regAddr = DccRxMacCheckRegister(self);
    Tha60290021PwHdlcLongReadOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    return (longRegVal[GroupId(channel)] & EnableMask(channel)) ? cAtTrue : cAtFalse;
    }

static uint32 DccRxCVlanCheckRegister(Tha60290021PwHdlc self)
    {
    return cAf6Reg_upen_dccrxcvlenb + BaseAddress(self);
    }

static eAtRet DccPwCVlanCheckEnable(Tha60290021PwHdlc self, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)self;
    uint32 regAddr = DccRxCVlanCheckRegister(self);
    Tha60290021PwHdlcLongReadOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    mFieldIns(&longRegVal[GroupId(channel)], EnableMask(channel), EnableShift(channel), mBoolToBin(enable));
    Tha60290021PwHdlcLongWriteOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    return cAtOk;
    }

static eBool DccPwCVlanCheckIsEnabled(Tha60290021PwHdlc self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)self;
    uint32 regAddr = DccRxCVlanCheckRegister(self);
    Tha60290021PwHdlcLongReadOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    return (longRegVal[GroupId(channel)] & EnableMask(channel)) ? cAtTrue : cAtFalse;
    }

static uint8 LabelMaxValue(Tha60290021PwHdlc self)
    {
    AtUnused(self);
    return cBit5_0;
    }

static eBool IsExpectedLabelIsUsed(Tha60290021PwHdlc self, uint8 label)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    uint8 line_i;
    eBool ret = cAtFalse;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, line_i);
        if (line)
            ret = ret || Tha6029DccPwIsExpectedLabelUsedByLine(self, line, label);
        }

    return ret;
    }

static uint32 DccTxHeaderRegAddress(Tha60290021PwHdlc self)
    {
    return Tha60290021DccAddressWithLocalAddress((AtPw)self, cAf6Reg_upen_dcchdr_Base + Tha60290021DccChannelId((AtPw)self) * 16UL);
    }

static uint32 AddressWithLocalAddress(Tha60290021PwHdlc self, uint32 localAddress)
    {
    return localAddress + ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 UpenDccdecAddress(Tha60290021PwHdlc self, uint8 label)
    {
    uint32 regBase = ThaModuleOcnUpenDccdecBase(OcnModule(self));
    return AddressWithLocalAddress(self, regBase + label);
    }

static eAtRet HwDccPwLabelChannelSet(Tha60290021PwHdlc self, uint8 label, uint32 channelId)
    {
    uint32 regAddr = UpenDccdecAddress(self, label);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_dccdec_Mapping_ChannelID_, channelId);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint32 HwDccPwLabelChannelGet(Tha60290021PwHdlc self, uint8 label)
    {
    uint32 regAddr = UpenDccdecAddress(self, label);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAf6_upen_dccdec_Mapping_ChannelID_);
    }

static eBool HasInterruptV2(Tha60290021PwHdlc self)
    {
    Tha60290011ModulePw pwModule = (Tha60290011ModulePw)AtChannelModuleGet((AtChannel)self);
    return Tha60290011ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static eAtRet HwDccPwEthTypeSet(Tha60290021PwHdlc self, uint16 ethType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = BaseAddress(self) + cAf6Reg_upen_dccrxhdr;

    Tha60290021PwHdlcLongReadOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);
    AtUtilLongFieldSet(longRegVal, 1, ethType,
                       cAf6_upen_dccrxhdr_ETH_TYP_01_Mask,
                       cAf6_upen_dccrxhdr_ETH_TYP_01_Shift,
                       cAf6_upen_dccrxhdr_ETH_TYP_02_Mask,
                       cAf6_upen_dccrxhdr_ETH_TYP_02_Shift);
    Tha60290021PwHdlcLongWriteOnCore(self, regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet DccPwEthTypeSet(Tha60290021PwHdlc self, uint16 ethType)
    {
    if (ethType == cThaDccPwEthTypeDefault)
        return HwDccPwEthTypeSet(self, ethType);

    return cAtErrorNotApplicable;
    }

static void OverrideAtChannel(AtPwHdlc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideTha60290021PwHdlc(AtPwHdlc self)
    {
    Tha60290021PwHdlc pwHdlc = (Tha60290021PwHdlc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021PwHdlcMethods = mMethodsGet(pwHdlc);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021PwHdlcOverride, m_Tha60290021PwHdlcMethods, sizeof(m_Tha60290021PwHdlcOverride));

        mMethodOverride(m_Tha60290021PwHdlcOverride, DefaultSgmiiEthPort);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwMacCheckEnable);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwMacCheckIsEnabled);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwCVlanCheckEnable);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwCVlanCheckIsEnabled);
        mMethodOverride(m_Tha60290021PwHdlcOverride, LabelMaxValue);
        mMethodOverride(m_Tha60290021PwHdlcOverride, IsExpectedLabelIsUsed);
        mMethodOverride(m_Tha60290021PwHdlcOverride, LongReadOnCore);
        mMethodOverride(m_Tha60290021PwHdlcOverride, LongWriteOnCore);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccTxHeaderRegAddress);
        mMethodOverride(m_Tha60290021PwHdlcOverride, HwDccPwLabelChannelSet);
        mMethodOverride(m_Tha60290021PwHdlcOverride, HwDccPwLabelChannelGet);
        mMethodOverride(m_Tha60290021PwHdlcOverride, HasInterruptV2);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwEthTypeSet);
        }

    mMethodsSet(pwHdlc, &m_Tha60290021PwHdlcOverride);
    }

static void Override(AtPwHdlc self)
    {
    OverrideAtChannel(self);
    OverrideTha60290021PwHdlc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011PwHdlc);
    }

static AtPwHdlc ObjectInit(AtPwHdlc self, AtHdlcChannel dcc, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (Tha60290021PwDccV2ObjectInit(self, dcc, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwHdlc Tha60290011PwDccNew(AtHdlcChannel dcc, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwHdlc newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, dcc, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290011ModulePwInternal.h
 *
 * Created Date: Oct 6, 2016 
 *
 * Description : Internal Interface for Pw Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPWINTERNAL_H_
#define _THA60290011MODULEPWINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pw/Tha60210031ModulePwInternal.h"
#include "Tha60290011ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60290011ModulePw
    {
    tTha60210031ModulePw super;
    }tTha60290011ModulePw;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60290011ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEPWINTERNAL_H_ */

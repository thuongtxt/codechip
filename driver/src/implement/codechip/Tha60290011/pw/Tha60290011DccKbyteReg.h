/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_DCCK_H_
#define _AF6_REG_AF6CNC0011_RD_DCCK_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Interrupt Status
Reg Addr   : 0x02001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbint_Base                                                                       0x02001
#define cAf6Reg_upen_glbint_WidthVal                                                                        96

/*--------------------------------------
BitField Name: dcc_hdlc_undsize_len
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN undered minimum allowed
length (2 bytes)
BitField Bits: [65:65]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_hdlc_undsize_len_Mask                                                       cBit1
#define cAf6_upen_glbint_dcc_hdlc_undsize_len_Shift                                                          1

/*--------------------------------------
BitField Name: dcc_hdlc_ovrsize_len
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN overed maximum allowed
length     (1536 bytes)
BitField Bits: [64:64]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_hdlc_ovrsize_len_Mask                                                       cBit0
#define cAf6_upen_glbint_dcc_hdlc_ovrsize_len_Shift                                                          0

/*--------------------------------------
BitField Name: dcc_hdlc_crc_error
BitField Type: W1C
BitField Desc: Received HDLC frame from OCN has FCS error
BitField Bits: [63:63]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_hdlc_crc_error_Mask                                                        cBit31
#define cAf6_upen_glbint_dcc_hdlc_crc_error_Shift                                                           31

/*--------------------------------------
BitField Name: dcc_ocn2eth_blk_ept
BitField Type: W1C
BitField Desc: DCC packet buffer for OCN2ETH direction was fulled, some packets
will be lost
BitField Bits: [62:62]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_ocn2eth_blk_ept_Mask                                                       cBit30
#define cAf6_upen_glbint_dcc_ocn2eth_blk_ept_Shift                                                          30

/*--------------------------------------
BitField Name: dcc_eth2ocn_blk_ept
BitField Type: W1C
BitField Desc: DCC packet buffer for ETH2OCN direction was fulled, some packets
will be lost
BitField Bits: [61:61]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_eth2ocn_blk_ept_Mask                                                       cBit29
#define cAf6_upen_glbint_dcc_eth2ocn_blk_ept_Shift                                                          29

/*--------------------------------------
BitField Name: dcc_sgm_ovrsize_len
BitField Type: W1C
BitField Desc: Received DCC packet's length from SGMII port over maximum allowed
length (1318 bytes)
BitField Bits: [60:60]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_sgm_ovrsize_len_Mask                                                       cBit28
#define cAf6_upen_glbint_dcc_sgm_ovrsize_len_Shift                                                          28

/*--------------------------------------
BitField Name: dcc_channel_disable
BitField Type: W1C
BitField Desc: DCC Local Channel Identifier mapping is disable Bit[12] ->
Bit[43] indicate channel 0 -> 31.
BitField Bits: [59:12]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_channel_disable_Mask_01                                                 cBit31_12
#define cAf6_upen_glbint_dcc_channel_disable_Shift_01                                                       12
#define cAf6_upen_glbint_dcc_channel_disable_Mask_02                                                  cBit27_0
#define cAf6_upen_glbint_dcc_channel_disable_Shift_02                                                        0

/*--------------------------------------
BitField Name: dcc_sgm_crc_error
BitField Type: W1C
BitField Desc: Received packet from SGMII port has FCS error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_sgm_crc_error_Mask                                                          cBit2
#define cAf6_upen_glbint_dcc_sgm_crc_error_Shift                                                             2

/*--------------------------------------
BitField Name: dcc_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_cvlid_mismat_Mask                                                           cBit1
#define cAf6_upen_glbint_dcc_cvlid_mismat_Shift                                                              1

/*--------------------------------------
BitField Name: dcc_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glbint_dcc_macda_mismat_Mask                                                           cBit0
#define cAf6_upen_glbint_dcc_macda_mismat_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Loopback Enable Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides loopback enable configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_loopen_Base                                                                       0x00001
#define cAf6Reg_upen_loopen_WidthVal                                                                        32

/*--------------------------------------
BitField Name: hdlc_loop_en
BitField Type: RW
BitField Desc: Enable loopback from HDLC Encap to HDLC DEcap of DCC byte.
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_loopen_hdlc_loop_en_Mask                                                               cBit1
#define cAf6_upen_loopen_hdlc_loop_en_Shift                                                                  1

/*--------------------------------------
BitField Name: dsgm_loop_en
BitField Type: RW
BitField Desc: Enable SGMII loopback of DCC Port.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_loopen_dsgm_loop_en_Mask                                                               cBit0
#define cAf6_upen_loopen_dsgm_loop_en_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Interrupt Enable
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for enabling interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_intenb_Base                                                                       0x00002
#define cAf6Reg_upen_intenb_WidthVal                                                                        96

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_undsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
undered minimum allowed length (2 bytes)     "
BitField Bits: [65:65]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_hdlc_undsize_len_Mask                                               cBit1
#define cAf6_upen_intenb_enb_int_dcc_hdlc_undsize_len_Shift                                                  1

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
overed maximum allowed length (1536 bytes)"
BitField Bits: [64:64]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_hdlc_ovrsize_len_Mask                                               cBit0
#define cAf6_upen_intenb_enb_int_dcc_hdlc_ovrsize_len_Shift                                                  0

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_crc_error
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC frame from OCN has FCS error
"
BitField Bits: [63:63]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_hdlc_crc_error_Mask                                                cBit31
#define cAf6_upen_intenb_enb_int_dcc_hdlc_crc_error_Shift                                                   31

/*--------------------------------------
BitField Name: enb_int_dcc_ocn2eth_blk_ept
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for OCN2ETH direction was
fulled, some packets will be lost
BitField Bits: [62:62]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_ocn2eth_blk_ept_Mask                                               cBit30
#define cAf6_upen_intenb_enb_int_dcc_ocn2eth_blk_ept_Shift                                                  30

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_blk_ept
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for ETH2OCN direction was
fulled, some packets will be lost
BitField Bits: [61:61]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_eth2ocn_blk_ept_Mask                                               cBit29
#define cAf6_upen_intenb_enb_int_dcc_eth2ocn_blk_ept_Shift                                                  29

/*--------------------------------------
BitField Name: enb_int_dcc_sgm_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received DCC packet's length from SGMII port
over maximum allowed length (1318 bytes)"
BitField Bits: [60:60]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_sgm_ovrsize_len_Mask                                               cBit28
#define cAf6_upen_intenb_enb_int_dcc_sgm_ovrsize_len_Shift                                                  28

/*--------------------------------------
BitField Name: enb_int_dcc_chan_dis
BitField Type: RW
BitField Desc: Enable Interrupt of DCC Local Channel Identifier mapping per
channel Bit[12] -> Bit[43] indicate channel 0 -> 31.
BitField Bits: [59:12]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_chan_dis_Mask_01                                                cBit31_12
#define cAf6_upen_intenb_enb_int_dcc_chan_dis_Shift_01                                                      12
#define cAf6_upen_intenb_enb_int_dcc_chan_dis_Mask_02                                                 cBit27_0
#define cAf6_upen_intenb_enb_int_dcc_chan_dis_Shift_02                                                       0

/*--------------------------------------
BitField Name: enb_int_dcc_sgm_crc_error
BitField Type: RW
BitField Desc: Enable Interrupt of "Received packet from SGMII port has FCS
error"
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_sgm_crc_error_Mask                                                  cBit2
#define cAf6_upen_intenb_enb_int_dcc_sgm_crc_error_Shift                                                     2

/*--------------------------------------
BitField Name: enb_int_dcc_cvlid_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 12b CVLAN ID value of DCC frame
different from global provisioned CVID "
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_cvlid_mismat_Mask                                                   cBit1
#define cAf6_upen_intenb_enb_int_dcc_cvlid_mismat_Shift                                                      1

/*--------------------------------------
BitField Name: enb_int_dcc_macda_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 43b MAC DA value of DCC frame
different from global provisioned DA"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_intenb_enb_int_dcc_macda_mismat_Mask                                                   cBit0
#define cAf6_upen_intenb_enb_int_dcc_macda_mismat_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Buffer_Linklist_Init
Reg Addr   : 0x10001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register use to trigger Link-List initialization

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_llst_Base                                                                     0x10001
#define cAf6Reg_upen_cfg_llst_WidthVal                                                                      32

/*--------------------------------------
BitField Name: tx_ini_done
BitField Type: W1C
BitField Desc: Indicate TX Link-list initialization successed
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_llst_tx_ini_done_Mask                                                              cBit3
#define cAf6_upen_cfg_llst_tx_ini_done_Shift                                                                 3

/*--------------------------------------
BitField Name: rx_ini_done
BitField Type: W1C
BitField Desc: Indicate RX Link-list initialization successed
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_llst_rx_ini_done_Mask                                                              cBit2
#define cAf6_upen_cfg_llst_rx_ini_done_Shift                                                                 2

/*--------------------------------------
BitField Name: tx_ll_init
BitField Type: RW
BitField Desc: Use to trigger Linklist of DCC TX packet buffer. Write 0 first,
then write 1 to trigger
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_llst_tx_ll_init_Mask                                                               cBit1
#define cAf6_upen_cfg_llst_tx_ll_init_Shift                                                                  1

/*--------------------------------------
BitField Name: rx_ll_init
BitField Type: RW
BitField Desc: Use to trigger Linklist of DCC RX packet buffer. Write 0 first,
then write 1 to trigger
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_llst_rx_ll_init_Mask                                                               cBit0
#define cAf6_upen_cfg_llst_rx_ll_init_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : DDC_TX_Header_Per_Channel_reg0
Reg Addr   : 0x11400 - 0x117ff
Reg Formula: 0x11400 + $channelid*16
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctxhdr_reg0_Base                                                                0x11400

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: MAC DA value of Channel ID
BitField Bits: [63:16]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_Mask_01                                                       cBit31_16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_Shift_01                                                             16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_Mask_02                                                        cBit31_0
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_Shift_02                                                              0

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: 16b MSB MAC SA value of Channel ID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_Mask                                                           cBit15_0
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DDC_TX_Header_Per_Channel_Reg1
Reg Addr   : 0x11201 - 0x112DD
Reg Formula: 0x11201 + $channelid*4
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctxhdr_reg1_Base                                                                0x11201

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: 48b LSB MAC SA value of Channel ID
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_MAC_SA_Mask                                                           cBit31_0
#define cAf6_upen_dcctxhdr_reg1_MAC_SA_Shift                                                                 0

/*--------------------------------------
BitField Name: VLAN
BitField Type: RW
BitField Desc: 16b
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_VLAN_Mask                                                            cBit31_16
#define cAf6_upen_dcctxhdr_reg1_VLAN_Shift                                                                  16

/*--------------------------------------
BitField Name: VERSION
BitField Type: RW
BitField Desc: 8b Type field
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_VERSION_Mask                                                          cBit15_8
#define cAf6_upen_dcctxhdr_reg1_VERSION_Shift                                                                8

/*--------------------------------------
BitField Name: TYPE
BitField Type: RW
BitField Desc: 8b Version Field
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_TYPE_Mask                                                              cBit7_0
#define cAf6_upen_dcctxhdr_reg1_TYPE_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Channel_Enable
Reg Addr   : 0x11000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for enable transmission DDC packet per channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctx_enacid_Base                                                                 0x11000
#define cAf6Reg_upen_dcctx_enacid_WidthVal                                                                  64

/*--------------------------------------
BitField Name: Channel_Enable
BitField Type: RW
BitField Desc: Enable transmitting of DDC packet per channel. Bit[47:00]
represent for channel 47->00
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dcctx_enacid_Channel_Enable_Mask_01                                                 cBit31_0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Shift_01                                                       0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Mask_02                                                 cBit15_0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Shift_02                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Global_ProvisionedHeader_Configuration
Reg Addr   : 0x12001
Reg Formula: 0x12001
    Where  : 
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxhdr_Base                                                                     0x12001
#define cAf6Reg_upen_dccrxhdr_WidthVal                                                                      96

/*--------------------------------------
BitField Name: ETH_TYP
BitField Type: RW
BitField Desc: 16b value of Provisioned ETHERTYPE of DCC
BitField Bits: [69:54]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_ETH_TYP_Mask_01                                                           cBit31_22
#define cAf6_upen_dccrxhdr_ETH_TYP_Shift_01                                                                 22
#define cAf6_upen_dccrxhdr_ETH_TYP_Mask_02                                                             cBit5_0
#define cAf6_upen_dccrxhdr_ETH_TYP_Shift_02                                                                  0

/*--------------------------------------
BitField Name: CVID
BitField Type: RW
BitField Desc: 12b value of Provisioned C-VLAN ID. This value is used to
compared with received CVLAN ID value.
BitField Bits: [53:42]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_CVID_Mask                                                                 cBit21_10
#define cAf6_upen_dccrxhdr_CVID_Shift                                                                       10

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: 42b MSB of Provisioned MAC DA value. This value is used to
compared with received MAC_DA[47:06] value. If a match is confirmed,
MAC_DA[05:00] is used to represent channelID value before mapping.
BitField Bits: [41:00]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_MAC_DA_Mask_01                                                             cBit31_0
#define cAf6_upen_dccrxhdr_MAC_DA_Shift_01                                                                   0
#define cAf6_upen_dccrxhdr_MAC_DA_Mask_02                                                              cBit9_0
#define cAf6_upen_dccrxhdr_MAC_DA_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_MAC_Check_Enable_Configuration
Reg Addr   : 0x12002
Reg Formula: 0x12002
    Where  : 
Reg Desc   : 
The register provides configuration that enable base MAC DA check per channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxmacenb_Base                                                                  0x12002
#define cAf6Reg_upen_dccrxmacenb_WidthVal                                                                   64

/*--------------------------------------
BitField Name: MAC_check_enable
BitField Type: RW
BitField Desc: Enable channel checking of received MAC DA compare to globally
provisioned Base MAC.On mismatch, frame is discarded.
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Mask_01                                                cBit31_0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Shift_01                                                      0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Mask_02                                                cBit15_0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Shift_02                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_CVLAN_Check_Enable_Configuration
Reg Addr   : 0x12003
Reg Formula: 0x12003
    Where  : 
Reg Desc   : 
The register provides configuration that enable base CVLAN Check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxcvlenb_Base                                                                  0x12003
#define cAf6Reg_upen_dccrxcvlenb_WidthVal                                                                   64

/*--------------------------------------
BitField Name: CVL_check_enable
BitField Type: RW
BitField Desc: Enable channel checking of received CVLAN ID compare to globally
provisioned CVLAN ID.On mismatch, frame is discarded.
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Mask_01                                                cBit31_0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Shift_01                                                      0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Mask_02                                                cBit15_0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Shift_02                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Channel_Mapping
Reg Addr   : 0x12400 - 0x1242F
Reg Formula: 0x12400 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
The register provides channel mapping from received MAC DA to internal channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccdec_Base                                                                       0x12400
#define cAf6Reg_upen_dccdec_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Local Channel Identifier. Rx packet is discarded if enable
is not set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_dccdec_Channel_enable_Mask                                                             cBit6
#define cAf6_upen_dccdec_Channel_enable_Shift                                                                6

/*--------------------------------------
BitField Name: Mapping_ChannelID
BitField Type: RW
BitField Desc: Local ChannelID that is mapped from received bit[5:0] of MAC DA
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_dccdec_Mapping_ChannelID_Mask                                                        cBit5_0
#define cAf6_upen_dccdec_Mapping_ChannelID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Sop_From_SGMII_Port1
Reg Addr   : 0x12020
Reg Formula: 0x12020
    Where  : 
Reg Desc   : 
Counter of number of SOP receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxsop_Base                                                          0x12020

/*--------------------------------------
BitField Name: sop_counter
BitField Type: WC
BitField Desc: Counter of SOP receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxsop_sop_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxsop_sop_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Eop_From_SGMII_Port1
Reg Addr   : 0x12021
Reg Formula: 0x12021
    Where  : 
Reg Desc   : 
Counter of number of EOP receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxeop_Base                                                          0x12021

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter of EOP receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Err_From_SGMII_Port1
Reg Addr   : 0x12022
Reg Formula: 0x12022
    Where  : 
Reg Desc   : 
Counter of number of ERR receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxerr_Base                                                          0x12022

/*--------------------------------------
BitField Name: err_counter
BitField Type: WC
BitField Desc: Counter of ERR receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Byte_From_SGMII_Port1
Reg Addr   : 0x12023
Reg Formula: 0x12023
    Where  : 
Reg Desc   : 
Counter of number of BYTE receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxbyt_Base                                                          0x12023

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter of BYTE receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port1
Reg Addr   : 0x12024
Reg Formula: 0x12024
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxfail_Base                                                         0x12024

/*--------------------------------------
BitField Name: unk_counter
BitField Type: WC
BitField Desc: Counter of unknow receive packet from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_Pkt_From_SGMII_Port1_Per_Chan
Reg Addr   : 0x12200 - 0x12137
Reg Formula: 0x12200 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter of number of DCC Packet From SGMII Port Per Channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_sokpkt_pcid_Base                                                              0x12200

/*--------------------------------------
BitField Name: dcc_pkt_cnt
BitField Type: WC
BitField Desc: Counter of DCC packets receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_sokpkt_pcid_dcc_pkt_cnt_Mask                                                    cBit31_0
#define cAf6_upen_dcc_sokpkt_pcid_dcc_pkt_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_Byte_From_SGMII_Port1_Per_Chan
Reg Addr   : 0x12380 - 0x123BF
Reg Formula: 0x12380 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter of number of DCC Bytes From SGMII Port Per Channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_sokbyt_pcid_Base                                                              0x12380

/*--------------------------------------
BitField Name: dcc_byte_cnt
BitField Type: WC
BitField Desc: Counter of DCC Bytes receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_sokbyt_pcid_dcc_byte_cnt_Mask                                                   cBit31_0
#define cAf6_upen_dcc_sokbyt_pcid_dcc_byte_cnt_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_SOP_From_RxOcn_Per_Chan
Reg Addr   : 0x11400 - 0x11437
Reg Formula: 0x11400 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of SOP of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sop2dcc_pcid_Base                                                                 0x11400

/*--------------------------------------
BitField Name: sop_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of SOP of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_sop2dcc_pcid_sop_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_sop2dcc_pcid_sop_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_EOP_From_RxOcn_Per_Chan
Reg Addr   : 0x11440 - 0x11477
Reg Formula: 0x11440 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of EOP of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eop2dcc_pcid_Base                                                                 0x11440

/*--------------------------------------
BitField Name: eop_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of EOP of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_eop2dcc_pcid_eop_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_eop2dcc_pcid_eop_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_ERR_From_RxOcn_Per_Chan
Reg Addr   : 0x11480 - 0x114B7
Reg Formula: 0x11480 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of ERR of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err2dcc_pcid_Base                                                                 0x11480

/*--------------------------------------
BitField Name: err_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of ERR of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_err2dcc_pcid_err_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_err2dcc_pcid_err_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_BYTE_From_RxOcn_Per_Chan
Reg Addr   : 0x114C0 - 0x114FF
Reg Formula: 0x114C0 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of BYTE of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_byt2dcc_pcid_Base                                                                 0x114C0

/*--------------------------------------
BitField Name: byt_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of BYTE of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_byt2dcc_pcid_byt_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_byt2dcc_pcid_byt_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Sop_From_OCN
Reg Addr   : 0x11504
Reg Formula: 0x11504
    Where  : 
Reg Desc   : 
Counter of number of SOP receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxsop_Base                                                          0x11504

/*--------------------------------------
BitField Name: sop_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total SOP receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxsop_sop_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxsop_sop_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Eop_From_OCN
Reg Addr   : 0x11505
Reg Formula: 0x11505
    Where  : 
Reg Desc   : 
Counter of number of EOP receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxeop_Base                                                          0x11505

/*--------------------------------------
BitField Name: eop_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total EOP receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxeop_eop_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxeop_eop_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Err_From_OCN
Reg Addr   : 0x11506
Reg Formula: 0x11506
    Where  : 
Reg Desc   : 
Counter of number of ERR receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxerr_Base                                                          0x11506

/*--------------------------------------
BitField Name: err_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total ERR  receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxerr_err_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxerr_err_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Byte_From_OCN
Reg Addr   : 0x11507
Reg Formula: 0x11507
    Where  : 
Reg Desc   : 
Counter of number of BYTE receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxbyt_Base                                                          0x11507

/*--------------------------------------
BitField Name: byt_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total RX BYTE from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxbyt_byt_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxbyt_byt_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_DCC_SOP_To_SGMII_Per_Chan
Reg Addr   : 0x11540 - 0x11577
Reg Formula: 0x11540 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of SOP of TX DCC packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccsop2sgm_pcid_Base                                                              0x11540

/*--------------------------------------
BitField Name: sop_counter
BitField Type: WC
BitField Desc: Counter number of SOP of TX DCC packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dccsop2sgm_pcid_sop_counter_Mask                                                    cBit31_0
#define cAf6_upen_dccsop2sgm_pcid_sop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_DCC_EOP_To_SGMII_Per_Chan
Reg Addr   : 0x11580 - 0x115B7
Reg Formula: 0x11580 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of EOP of TX DCC packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcceop2sgm_pcid_Base                                                              0x11580

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter number of EOP of TX DCC packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcceop2sgm_pcid_eop_counter_Mask                                                    cBit31_0
#define cAf6_upen_dcceop2sgm_pcid_eop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_DCC_BYTE_To_SGMII_Per_Chan
Reg Addr   : 0x115C0 - 0x115F7
Reg Formula: 0x115C0 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of BYTE of TX DCC packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccbyt2sgm_pcid_Base                                                              0x115C0

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of TX DCC packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dccbyt2sgm_pcid_byte_counter_Mask                                                   cBit31_0
#define cAf6_upen_dccbyt2sgm_pcid_byte_counter_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Sop_To_SGMII_Port1
Reg Addr   : 0x11500
Reg Formula: 0x11500
    Where  : 
Reg Desc   : 
Counter of number of SOP transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxsop_Base                                                          0x11500

/*--------------------------------------
BitField Name: sgm_txglb_sop_counter
BitField Type: WC
BitField Desc: Counter of SOP transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxsop_sgm_txglb_sop_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxsop_sgm_txglb_sop_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Eop_To_SGMII_Port1
Reg Addr   : 0x11501
Reg Formula: 0x11501
    Where  : 
Reg Desc   : 
Counter of number of EOP transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxeop_Base                                                          0x11501

/*--------------------------------------
BitField Name: sgm_txglb_eop_counter
BitField Type: WC
BitField Desc: Counter of EOP transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Err_To_SGMII_Port1
Reg Addr   : 0x11502
Reg Formula: 0x11502
    Where  : 
Reg Desc   : 
Counter of number of ERR transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxerr_Base                                                          0x11502

/*--------------------------------------
BitField Name: sgm_txglb_err_counter
BitField Type: WC
BitField Desc: Counter of ERR  transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Byte_To_SGMII_Port1
Reg Addr   : 0x11503
Reg Formula: 0x11503
    Where  : 
Reg Desc   : 
Counter of number of BYTE transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxbyt_Base                                                          0x11503

/*--------------------------------------
BitField Name: sgm_txglb_byte_counter
BitField Type: WC
BitField Desc: Counter of TX BYTE to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Mask                                     cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_SOP_to_RxOcn_Per_Chan
Reg Addr   : 0x12240 - 0x1227f
Reg Formula: 0x12240 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of SOP of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sop2ocn_pcid_Base                                                                 0x12240

/*--------------------------------------
BitField Name: sop_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of SOP of DCC packets transmit to RX-OCN  Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_sop2ocn_pcid_sop_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_sop2ocn_pcid_sop_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_EOP_to_RxOcn_Per_Chan
Reg Addr   : 0x12280 - 0x122BF
Reg Formula: 0x12280 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of EOP of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eop2ocn_pcid_Base                                                                 0x12280

/*--------------------------------------
BitField Name: eop_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of EOP of DCC packets transmit to RX-OCN Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_eop2ocn_pcid_eop_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_eop2ocn_pcid_eop_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_ERR_to_RxOcn_Per_Chan
Reg Addr   : 0x122C0 - 0x122FF
Reg Formula: 0x122C0 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of ERR of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err2ocn_pcid_Base                                                                 0x122C0

/*--------------------------------------
BitField Name: err_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of ERR of DCC packets transmit to RX-OCN Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_err2ocn_pcid_err_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_err2ocn_pcid_err_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_EPT_to_RxOcn_Per_Chan
Reg Addr   : 0x12300 - 0x1233F
Reg Formula: 0x12300 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of EPT of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ept2ocn_pcid_Base                                                                 0x12300

/*--------------------------------------
BitField Name: ept_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of EPT signal respond to request from RX-OCN Per
ChannelID. An empty indicate data is not available when OCN request DCC data
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ept2ocn_pcid_ept_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_ept2ocn_pcid_ept_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_BYTE_to_RxOcn_Per_Chan
Reg Addr   : 0x12340 - 0x1237F
Reg Formula: 0x12340 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of BYTE of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_byt2ocn_pcid_Base                                                                 0x12340

/*--------------------------------------
BitField Name: byt_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of DCC packets transmit to RX-OCN Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_byt2ocn_pcid_byt_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_byt2ocn_pcid_byt_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_Drop_to_RxOcn_Per_Chan
Reg Addr   : 0x123C0 - 0x123FF
Reg Formula: 0x123C0 + $channelID
    Where  : 
           + $channelID(0-47): DCC channelID
Reg Desc   : 
Counter number of Drop DCC packet transmit to RX-OCN Due to Buffer Full Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ful2ocn_pcid_Base                                                                 0x123C0

/*--------------------------------------
BitField Name: drp_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of Drop DCC packets transmit to RX-OCN due to
Buffer Full Per ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ful2ocn_pcid_drp_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_ful2ocn_pcid_drp_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Sop_To_OCN
Reg Addr   : 0x12025
Reg Formula: 0x12025
    Where  : 
Reg Desc   : 
Counter Total number of SOP transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxsop_Base                                                          0x12025

/*--------------------------------------
BitField Name: sop_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total SOP transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxsop_sop_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxsop_sop_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Eop_To_OCN
Reg Addr   : 0x12026
Reg Formula: 0x12026
    Where  : 
Reg Desc   : 
Counter Total number of EOP transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxeop_Base                                                          0x12026

/*--------------------------------------
BitField Name: eop_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total EOP transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxeop_eop_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxeop_eop_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Err_To_OCN
Reg Addr   : 0x12027
Reg Formula: 0x12027
    Where  : 
Reg Desc   : 
Counter Total number of ERR transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxerr_Base                                                          0x12027

/*--------------------------------------
BitField Name: err_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total ERR transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxerr_err_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxerr_err_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Byte_To_OCN
Reg Addr   : 0x12028
Reg Formula: 0x12028
    Where  : 
Reg Desc   : 
Counter Total number of BYTE transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxbyt_Base                                                          0x12028

/*--------------------------------------
BitField Name: byt_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total BYTE transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxbyt_byt_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxbyt_byt_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Drop_To_RxOcn_Pkt
Reg Addr   : 0x12029
Reg Formula: 0x12029
    Where  : 
Reg Desc   : 
Counter Total number Packet Drop due to buffer full (ETH2OCN Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ful2ocn_Base                                                           0x12029

/*--------------------------------------
BitField Name: drp_pkt_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total Packet Dropped Due to Buffer full (ETH to RX-
OCN)
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ful2ocn_drp_pkt_sgm2ocn_cnt_glb_Mask                                     cBit31_0
#define cAf6_upen_dcc_glbcnt_ful2ocn_drp_pkt_sgm2ocn_cnt_glb_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC LO DEC
Reg Addr   : 0x04000 - 0x04037
Reg Formula: 0x04000+ $CID
    Where  : 
           + $CID (0-47) : Channel ID
Reg Desc   : 
config HDLC ID 0-31
HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_locfg_Base                                                                   0x04000
#define cAf6Reg_upen_hdlc_locfg_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_scren
BitField Type: R/W
BitField Desc: config to enable scramble, (1) is enable, (0) is disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_scren_Mask                                                              cBit4
#define cAf6_upen_hdlc_locfg_cfg_scren_Shift                                                                 4

/*--------------------------------------
BitField Name: cfg_bitstuff
BitField Type: R/W
BitField Desc: config to select bit stuff or byte sutff, (1) is bit stuff, (0)
is byte stuff
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Mask                                                           cBit2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Shift                                                              2

/*--------------------------------------
BitField Name: cfg_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Mask                                                             cBit1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_fcsmode
BitField Type: R/W
BitField Desc: config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS
16
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Mask                                                            cBit0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC GLOBAL LO DEC
Reg Addr   : 0x04404
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_loglbcfg_Base                                                                0x04404
#define cAf6Reg_upen_hdlc_loglbcfg_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: config to receive LSB first, (1) is LSB first, (0) is default MSB
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Mask                                                        cBit3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Shift                                                           3


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Master Control
Reg Addr   : 0x30003
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC Encode Master

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_master_ctrl_Base                                                         0x30003
#define cAf6Reg_upen_hdlc_enc_master_ctrl_WidthVal                                                          32

/*--------------------------------------
BitField Name: encap_lsbfirst
BitField Type: R/W
BitField Desc: config to transmit LSB first, (1) is LSB first, (0) is default
MSB
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_Mask                                               cBit0
#define cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 1
Reg Addr   : 0x38000 - 0x38037
Reg Formula: 0x38000+ $CID
    Where  : 
           + $CID (0-47) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg1_Base                                                           0x38000

/*--------------------------------------
BitField Name: encap_screnb
BitField Type: R/W
BitField Desc: Enable Scamble (1) Enable      , (0) disable
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Mask                                                   cBit9
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Shift                                                      9

/*--------------------------------------
BitField Name: encap_idlemod
BitField Type: R/W
BitField Desc: This bit is only used in Bit Stuffing mode Used to configured
IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC
is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            ,
(0) Disabe
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Mask                                                  cBit7
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Shift                                                     7

/*--------------------------------------
BitField Name: encap_sabimod
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1
byte
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Mask                                                  cBit6
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Shift                                                     6

/*--------------------------------------
BitField Name: encap_sabiins
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Mask                                                  cBit5
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Shift                                                     5

/*--------------------------------------
BitField Name: encap_ctrlins
BitField Type: R/W
BitField Desc: Address/Control Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Mask                                                  cBit4
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Shift                                                     4

/*--------------------------------------
BitField Name: encap_fcsmod
BitField Type: R/W
BitField Desc: FCS Select Mode (1) 32b FCS           , (0) 16b FCS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Mask                                                   cBit3
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Shift                                                      3

/*--------------------------------------
BitField Name: encap_fcsins
BitField Type: R/W
BitField Desc: FCS Insert Enable (1) Enable Insert     , (0) Disable Insert
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Mask                                                   cBit2
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Shift                                                      2

/*--------------------------------------
BitField Name: encap_flgmod
BitField Type: R/W
BitField Desc: Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Mask                                                   cBit1
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Shift                                                      1

/*--------------------------------------
BitField Name: encap_stfmod
BitField Type: R/W
BitField Desc: Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Mask                                                   cBit0
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 2
Reg Addr   : 0x39000 - 0x39037
Reg Formula: 0x39000+ $CID
    Where  : 
           + $CID (0-47) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 2 Byte Stuff Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg2_Base                                                           0x39000

/*--------------------------------------
BitField Name: encap_addrval
BitField Type: R/W
BitField Desc: Address Field
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Mask                                              cBit31_24
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Shift                                                    24

/*--------------------------------------
BitField Name: encap_ctlrval
BitField Type: R/W
BitField Desc: Control Field
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Mask                                              cBit23_16
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Shift                                                    16

/*--------------------------------------
BitField Name: encap_sapival0
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 1st byte
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Mask                                              cBit15_8
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Shift                                                    8

/*--------------------------------------
BitField Name: encap_sapival1
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 2nd byte
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Mask                                               cBit7_0
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Enable Packet Capture
Reg Addr   : 0x0A000
Reg Formula: 
    Where  : 
Reg Desc   : 
config enable capture ethernet packet.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_trig_encap_Base                                                                   0x0A000
#define cAf6Reg_upen_trig_encap_WidthVal                                                                    32

/*--------------------------------------
BitField Name: enb_cap_dec
BitField Type: R/W
BitField Desc: Enable capture packets from HDLC Decapsulation. Only one type of
packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to
trigger capture process (1) Enable            , (0) Disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_dec_Mask                                                            cBit2
#define cAf6_upen_trig_encap_enb_cap_dec_Shift                                                               2

/*--------------------------------------
BitField Name: enb_cap_aps
BitField Type: R/W
BitField Desc: Enable capture APS packets from SGMII interface. Only one type of
packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to
trigger capture process (1) Enable            , (0) Disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_aps_Mask                                                            cBit1
#define cAf6_upen_trig_encap_enb_cap_aps_Shift                                                               1

/*--------------------------------------
BitField Name: enb_cap_dcc
BitField Type: R/W
BitField Desc: Enable capture DCC packets from SGMII interface. Only one type of
packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to
trigger capture process (1) Enable            , (0) Disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_dcc_Mask                                                            cBit0
#define cAf6_upen_trig_encap_enb_cap_dcc_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Captured Packet Data
Reg Addr   : 0x08000 - 0x080FF
Reg Formula: 0x08000 + $loc
    Where  : 
           + $loc(0-255) : data location
Reg Desc   : 
Captured packet data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pktcap_Base                                                                       0x08000
#define cAf6Reg_upen_pktcap_WidthVal                                                                        96

/*--------------------------------------
BitField Name: cap_sop
BitField Type: RO
BitField Desc: Start of packet
BitField Bits: [69:69]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_sop_Mask                                                                    cBit5
#define cAf6_upen_pktcap_cap_sop_Shift                                                                       5

/*--------------------------------------
BitField Name: cap_eop
BitField Type: RO
BitField Desc: End   of packet
BitField Bits: [68:68]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_eop_Mask                                                                    cBit4
#define cAf6_upen_pktcap_cap_eop_Shift                                                                       4

/*--------------------------------------
BitField Name: cap_err
BitField Type: RO
BitField Desc: Error of packet
BitField Bits: [67:67]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_err_Mask                                                                    cBit3
#define cAf6_upen_pktcap_cap_err_Shift                                                                       3

/*--------------------------------------
BitField Name: cap_nob
BitField Type: RO
BitField Desc: Number of valid bytes in 8-bytes data captured
BitField Bits: [66:64]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_nob_Mask                                                                  cBit2_0
#define cAf6_upen_pktcap_cap_nob_Shift                                                                       0

/*--------------------------------------
BitField Name: cap_dat
BitField Type: RO
BitField Desc: packet data captured
BitField Bits: [63:00]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_dat_Mask_01                                                              cBit31_0
#define cAf6_upen_pktcap_cap_dat_Shift_01                                                                    0
#define cAf6_upen_pktcap_cap_dat_Mask_02                                                              cBit31_0
#define cAf6_upen_pktcap_cap_dat_Shift_02                                                                    0

#endif /* _AF6_REG_AF6CNC0011_RD_DCCK_H_ */

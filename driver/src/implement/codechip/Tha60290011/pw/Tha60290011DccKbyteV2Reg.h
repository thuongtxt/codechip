/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290011DCCKBYTEV2REG_H_
#define _THA60290011DCCKBYTEV2REG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Status
Reg Addr   : 0x02001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides global interrupt status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbint_stt                                                                        0x02001

/*--------------------------------------
BitField Name: kbyte_interrupt
BitField Type: W1C
BitField Desc: interrupt from Kbyte event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glbint_stt_kbyte_interrupt_Mask                                                        cBit1
#define cAf6_upen_glbint_stt_kbyte_interrupt_Shift                                                           1

/*--------------------------------------
BitField Name: dcc_interrupt
BitField Type: W1C
BitField Desc: interrupt from DCC event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glbint_stt_dcc_interrupt_Mask                                                          cBit0
#define cAf6_upen_glbint_stt_dcc_interrupt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status "Local Channel Mapping Is Disable Channel 31 to 0"
Reg Addr   : 0x02004
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc0_stt_r0                                                                 0x02004

/*--------------------------------------
BitField Name: dcc_channel_disable_31_0
BitField Type: W1C
BitField Desc: Indicate mapping from 6LSB bit of received MAC DA value from
SGMII port to local channel ID is disable Bit[0] -> Bit[31] indicate mapping is
disable for MAC_DA[5:0] value from 0->31 .
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc0_stt_r0_dcc_channel_disable_31_0_Mask                                     cBit31_0
#define cAf6_upen_int_rxdcc0_stt_r0_dcc_channel_disable_31_0_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status "Local Channel Mapping Is Disable Channel 47 to 32"
Reg Addr   : 0x02014
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc0_stt_r1                                                                 0x02014
#define cAf6Reg_upen_int_rxdcc0_stt_r1_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_channel_disable_47_32
BitField Type: W1C
BitField Desc: Indicate mapping from 6LSB bit of received MAC DA value from
SGMII port to local channel ID is disable Bit[0] -> Bit[15] indicate mapping is
disable for MAC_DA[5:0] value from 32->47 .
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc0_stt_r1_dcc_channel_disable_47_32_Mask                                    cBit15_0
#define cAf6_upen_int_rxdcc0_stt_r1_dcc_channel_disable_47_32_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status of Packet Classification Error
Reg Addr   : 0x02005
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc1_stt                                                                    0x02005
#define cAf6Reg_upen_int_rxdcc1_stt_WidthVal                                                                32

/*--------------------------------------
BitField Name: dcc_eth2ocn_buffer_full
BitField Type: W1C
BitField Desc: ETH2OCN: One or more channels has buffer full error (reg 0x02006,
0x02016)
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_buffer_full_Mask                                            cBit5
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_buffer_full_Shift                                               5

/*--------------------------------------
BitField Name: dcc_eth2ocn_rxchid_dis
BitField Type: W1C
BitField Desc: One or more local channels is not be enabled mapping from
received MAC DA value (reg 0x02004, 0x02014)
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_rxchid_dis_Mask                                             cBit4
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_rxchid_dis_Shift                                                4

/*--------------------------------------
BitField Name: dcc_eth2ocn_ovrsize_len
BitField Type: W1C
BitField Desc: Received DCC packet's length from SGMII port over maximum allowed
length (1318 bytes)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_ovrsize_len_Mask                                            cBit3
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_ovrsize_len_Shift                                               3

/*--------------------------------------
BitField Name: dcc_eth2ocn_crc_error
BitField Type: W1C
BitField Desc: Received packet from SGMII port has FCS error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_crc_error_Mask                                              cBit2
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_crc_error_Shift                                                 2

/*--------------------------------------
BitField Name: dcc_eth2ocn_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_cvlid_mismat_Mask                                           cBit1
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_cvlid_mismat_Shift                                              1

/*--------------------------------------
BitField Name: dcc_eth2ocn_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_macda_mismat_Mask                                           cBit0
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_macda_mismat_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status Buffer Full Channel 31 to 0
Reg Addr   : 0x02006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc2_r0_stt                                                                 0x02006

/*--------------------------------------
BitField Name: dcc_eth2ocn_buf_ful_31_0
BitField Type: W1C
BitField Desc: DCC packet buffer for ETH2OCN direction was fulled, some packets
will be lost. Bit[00] -> Bit[31] indicate status of channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc2_r0_stt_dcc_eth2ocn_buf_ful_31_0_Mask                                     cBit31_0
#define cAf6_upen_int_rxdcc2_r0_stt_dcc_eth2ocn_buf_ful_31_0_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status Buffer Full Channel 47 to 32
Reg Addr   : 0x02016
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc2_r1_stt                                                                 0x02016
#define cAf6Reg_upen_int_rxdcc2_r1_stt_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_eth2ocn_buf_ful_47_32
BitField Type: W1C
BitField Desc: DCC packet buffer for ETH2OCN direction was fulled, some packets
will be lost. Bit[00] -> Bit[15] indicate status of channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc2_r1_stt_dcc_eth2ocn_buf_ful_47_32_Mask                                    cBit15_0
#define cAf6_upen_int_rxdcc2_r1_stt_dcc_eth2ocn_buf_ful_47_32_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Status Buffer Full Channel 31 to 0
Reg Addr   : 0x02008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc0_r0_stt                                                                 0x02008

/*--------------------------------------
BitField Name: dcc_ocn2eth_buf_ful_31_0
BitField Type: W1C
BitField Desc: DCC packet buffer for OCN2ETH direction was fulled, some packets
will be lost. Bit[00] -> Bit[31] indicate status of channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc0_r0_stt_dcc_ocn2eth_buf_ful_31_0_Mask                                     cBit31_0
#define cAf6_upen_int_txdcc0_r0_stt_dcc_ocn2eth_buf_ful_31_0_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Status Buffer Full Channel 47 to 32
Reg Addr   : 0x02018
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc2_r1_stt1                                                                 0x02018
#define cAf6Reg_upen_int_rxdcc2_r1_stt_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_ocn2eth_buf_ful_47_32
BitField Type: W1C
BitField Desc: DCC packet buffer for OCN2ETH direction was fulled, some packets
will be lost. Bit[00] -> Bit[15] indicate status of channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc2_r1_stt_dcc_ocn2eth_buf_ful_47_32_Mask                                    cBit15_0
#define cAf6_upen_int_rxdcc2_r1_stt_dcc_ocn2eth_buf_ful_47_32_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt CRC Error Status Channel 31 to 0
Reg Addr   : 0x02009
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc1_r0_stt                                                                 0x02009

/*--------------------------------------
BitField Name: dcc_hdlc_crc_error_31_0
BitField Type: W1C
BitField Desc: Indicate Received HDLC frame from OCN has FCS error
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc1_r0_stt_dcc_hdlc_crc_error_31_0_Mask                                      cBit31_0
#define cAf6_upen_int_txdcc1_r0_stt_dcc_hdlc_crc_error_31_0_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt CRC Error Status Channel 47 to 32
Reg Addr   : 0x02019
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc1_r1_stt                                                                 0x02019
#define cAf6Reg_upen_int_txdcc1_r1_stt_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_hdlc_crc_error_47_32
BitField Type: W1C
BitField Desc: Indicate Received HDLC frame from OCN has FCS error
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc1_r1_stt_dcc_hdlc_crc_error_47_32_Mask                                     cBit15_0
#define cAf6_upen_int_txdcc1_r1_stt_dcc_hdlc_crc_error_47_32_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Oversize HDLC Length Status Channel 31 to 0
Reg Addr   : 0x0200D
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc2_r0_stt                                                                 0x0200D

/*--------------------------------------
BitField Name: dcc_hdlc_ovrsize_len_31_0
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN overed maximum allowed
length     (1536 bytes)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc2_r0_stt_dcc_hdlc_ovrsize_len_31_0_Mask                                    cBit31_0
#define cAf6_upen_int_txdcc2_r0_stt_dcc_hdlc_ovrsize_len_31_0_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Oversize Length Status Channel 47 to 32
Reg Addr   : 0x0201D
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc2_r1_stt                                                                 0x0201D
#define cAf6Reg_upen_int_txdcc2_r1_stt_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_hdlc_ovrsize_len_47_32
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN overed maximum allowed
length     (1536 bytes)
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc2_r1_stt_dcc_hdlc_ovrsize_len_47_32_Mask                                   cBit15_0
#define cAf6_upen_int_txdcc2_r1_stt_dcc_hdlc_ovrsize_len_47_32_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Status Channel 32 to 0
Reg Addr   : 0x0200E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc3_r0_stt                                                                 0x0200E

/*--------------------------------------
BitField Name: dcc_hdlc_undsize_len_31_0
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN undered minimum allowed
length (2 bytes)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc3_r0_stt_dcc_hdlc_undsize_len_31_0_Mask                                    cBit31_0
#define cAf6_upen_int_txdcc3_r0_stt_dcc_hdlc_undsize_len_31_0_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction HDLC Interupt Status Channel 47 to 32
Reg Addr   : 0x0201E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc3_r1_stt                                                                 0x0201E
#define cAf6Reg_upen_int_txdcc3_r1_stt_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_hdlc_undsize_len_47_32
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN undered minimum allowed
length (2 bytes)
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc3_r1_stt_dcc_hdlc_undsize_len_47_32_Mask                                   cBit15_0
#define cAf6_upen_int_txdcc3_r1_stt_dcc_hdlc_undsize_len_47_32_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status of Packet Classification Error
Reg Addr   : 0x02010
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_txdcc_glb_stt                                                                 0x02010
#define cAf6Reg_upen_int_txdcc_glb_stt_WidthVal                                                             32

/*--------------------------------------
BitField Name: dcc_ocn2eth_buf_ful
BitField Type: W1C
BitField Desc: OCN2ETH: One or more channels has buffer full error (reg 0x02008,
0x02018)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_buf_ful_Mask                                             cBit3
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_buf_ful_Shift                                                3

/*--------------------------------------
BitField Name: dcc_ocn2eth_crc_err
BitField Type: W1C
BitField Desc: OCN2ETH: One or more channels has CRC error packet (reg 0x02009,
0x02019)
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_crc_err_Mask                                             cBit2
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_crc_err_Shift                                                2

/*--------------------------------------
BitField Name: dcc_ocn2eth_undsize_len
BitField Type: W1C
BitField Desc: OCN2ETH: One or more channels has minimum packet length violation
(reg 0x0200E, 0x0201E)
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_undsize_len_Mask                                         cBit1
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_undsize_len_Shift                                            1

/*--------------------------------------
BitField Name: dcc_ocn2eth_ovrsize_len
BitField Type: W1C
BitField Desc: OCN2ETH: One or more channels has maximum packet length violation
(reg 0x0200D, 0x0201D)
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_ovrsize_len_Mask                                         cBit0
#define cAf6_upen_int_txdcc_glb_stt_dcc_ocn2eth_ovrsize_len_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Enable
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable global interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glb_intenb                                                                        0x00002
#define cAf6Reg_upen_glb_intenb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: kbyte_interrupt_enb
BitField Type: RW
BitField Desc: Enable interrupt from Kbyte event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glb_intenb_kbyte_interrupt_enb_Mask                                                    cBit1
#define cAf6_upen_glb_intenb_kbyte_interrupt_enb_Shift                                                       1

/*--------------------------------------
BitField Name: dcc_interrupt_enb
BitField Type: RW
BitField Desc: Enable interrupt from DCC even
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glb_intenb_dcc_interrupt_enb_Mask                                                      cBit0
#define cAf6_upen_glb_intenb_dcc_interrupt_enb_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interupt " Local Channel Mapping Is Disable" Channel 31 to 0
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc0_intenb_r0                                                                  0x00004

/*--------------------------------------
BitField Name: enb_int_dcc_chan_dis_31_0
BitField Type: RW
BitField Desc: Enable Interrupt of DCC Local Channel Identifier mapping per
channel (reg 0x02004) Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxdcc0_intenb_r0_enb_int_dcc_chan_dis_31_0_Mask                                     cBit31_0
#define cAf6_upen_rxdcc0_intenb_r0_enb_int_dcc_chan_dis_31_0_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interupt " Local Channel Mapping Is Disable" Channel 47 to 32
Reg Addr   : 0x00014
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc0_intenb_r1                                                                  0x00014
#define cAf6Reg_upen_rxdcc0_intenb_r1_WidthVal                                                              32

/*--------------------------------------
BitField Name: enb_int_dcc_chan_dis_47_32
BitField Type: RW
BitField Desc: Enable Interrupt of DCC Local Channel Identifier mapping per
channel (reg 0x02014) Bit[00] -> Bit[15] indicate channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rxdcc0_intenb_r1_enb_int_dcc_chan_dis_47_32_Mask                                    cBit15_0
#define cAf6_upen_rxdcc0_intenb_r1_enb_int_dcc_chan_dis_47_32_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interrupt of "Packet Classification Error"
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc1_intenb                                                                     0x00005
#define cAf6Reg_upen_rxdcc1_intenb_WidthVal                                                                 32

/*--------------------------------------
BitField Name: enb_int_dcc_eth2eth_buffer_full
BitField Type: RW
BitField Desc: Enable Interrupt of "ETH2OCN: One or more channels has buffer
full error" (reg 0x02005)
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2eth_buffer_full_Mask                                     cBit5
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2eth_buffer_full_Shift                                        5

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_rxchid_dis
BitField Type: RW
BitField Desc: Enable interrupt of "One or more local channels is not be enabled
mapping from received MAC DA value" (reg 0x02005)
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_rxchid_dis_Mask                                      cBit4
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_rxchid_dis_Shift                                         4

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received DCC packet's length from SGMII port
over maximum allowed length (1318 bytes)"(reg 0x02005)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_ovrsize_len_Mask                                     cBit3
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_ovrsize_len_Shift                                        3

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_crc_error
BitField Type: RW
BitField Desc: Enable Interrupt of "Received packet from SGMII port has FCS
error" (reg 0x02005)
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_crc_error_Mask                                       cBit2
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_crc_error_Shift                                          2

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_cvlid_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 12b CVLAN ID value of DCC frame
different from global provisioned CVID " (reg 0x02005)
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_cvlid_mismat_Mask                                    cBit1
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_cvlid_mismat_Shift                                       1

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_macda_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 43b MAC DA value of DCC frame
different from global provisioned DA" (reg 0x02005)
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_macda_mismat_Mask                                    cBit0
#define cAf6_upen_rxdcc1_intenb_enb_int_dcc_eth2ocn_macda_mismat_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0
Reg Addr   : 0x00006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc2_intenb_r0                                                                  0x00006

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_buf_ful_31_0
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for ETH2OCN direction
fulled"     (reg 0x2006) Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxdcc2_intenb_r0_enb_int_dcc_eth2ocn_buf_ful_31_0_Mask                                cBit31_0
#define cAf6_upen_rxdcc2_intenb_r0_enb_int_dcc_eth2ocn_buf_ful_31_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0
Reg Addr   : 0x00016
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc2_intenb_r1                                                                  0x00016
#define cAf6Reg_upen_rxdcc2_intenb_r1_WidthVal                                                              32

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_buf_ful_32_47
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for ETH2OCN direction
fulled"     (reg 0x2016) Bit[00] -> Bit[15] indicate channel 32-> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rxdcc2_intenb_r1_enb_int_dcc_eth2ocn_buf_ful_32_47_Mask                                cBit15_0
#define cAf6_upen_rxdcc2_intenb_r1_enb_int_dcc_eth2ocn_buf_ful_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0
Reg Addr   : 0x00008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_ept_r0                                                               0x00008

/*--------------------------------------
BitField Name: enb_int_dcc_ocn2eth_buf_ful_31_0
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for OCN2ETH direction
fulled"     (reg 0x2008) Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_ept_r0_enb_int_dcc_ocn2eth_buf_ful_31_0_Mask                                cBit31_0
#define cAf6_upen_txdcc_intenb_ept_r0_enb_int_dcc_ocn2eth_buf_ful_31_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt of "Buffer Full per Channel" Channel 31 to 0
Reg Addr   : 0x00018
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_ept_r1                                                               0x00018
#define cAf6Reg_upen_txdcc_intenb_ept_r1_WidthVal                                                           32

/*--------------------------------------
BitField Name: enb_int_dcc_ocn2eth_buf_ful_32_47
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for OCN2ETH direction
fulled"     (reg 0x2018) Bit[00] -> Bit[15] indicate channel 32-> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_ept_r1_enb_int_dcc_ocn2eth_buf_ful_32_47_Mask                                cBit15_0
#define cAf6_upen_txdcc_intenb_ept_r1_enb_int_dcc_ocn2eth_buf_ful_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt "Packet's FCS Error" Channel 31 to 0
Reg Addr   : 0x00009
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_crc_r0                                                               0x00009

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_crc_error_31_0
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC frame from OCN has FCS error
" per channel(Ref reg 0x02009) Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_crc_r0_enb_int_dcc_hdlc_crc_error_31_0_Mask                                cBit31_0
#define cAf6_upen_txdcc_intenb_crc_r0_enb_int_dcc_hdlc_crc_error_31_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt "Packet's FCS Error" Channel 47 to 32
Reg Addr   : 0x00019
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_crc_r1                                                               0x00019
#define cAf6Reg_upen_txdcc_intenb_crc_r1_WidthVal                                                           32

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_crc_error_47_32
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC frame from OCN has FCS error
" per channel (Ref reg 0x02019) Bit[00] -> Bit[15] indicate channel 32-> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_crc_r1_enb_int_dcc_hdlc_crc_error_47_32_Mask                                cBit15_0
#define cAf6_upen_txdcc_intenb_crc_r1_enb_int_dcc_hdlc_crc_error_47_32_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt "Oversize HDLC Packet's Length" Channel 31 to 0
Reg Addr   : 0x0000D
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_ovr_r0                                                               0x0000D

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_ovrsize_len_31_0
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
overed maximum allowed length (1536 bytes)   " (Ref reg 0x0200D) Bit[00] ->
Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_ovr_r0_enb_int_dcc_hdlc_ovrsize_len_31_0_Mask                                cBit31_0
#define cAf6_upen_txdcc_intenb_ovr_r0_enb_int_dcc_hdlc_ovrsize_len_31_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt "Oversize HDLC Packet's Length" Channel 31 to 0
Reg Addr   : 0x0001D
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_ovr_r1                                                               0x0001D
#define cAf6Reg_upen_txdcc_intenb_ovr_r1_WidthVal                                                           32

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_ovrsize_len_47_32
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
overed maximum allowed length (1536 bytes)" (Ref reg 0x0201D) Bit[00] -> Bit[15]
indicate channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_ovr_r1_enb_int_dcc_hdlc_ovrsize_len_47_32_Mask                                cBit15_0
#define cAf6_upen_txdcc_intenb_ovr_r1_enb_int_dcc_hdlc_ovrsize_len_47_32_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt "Undersize HDLC Packet's Length " Channel 31 to 0
Reg Addr   : 0x0000E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_und_r0                                                               0x0000E

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_und_len_31_0
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
undered minimum allowed length (2 bytes)" (Ref reg 0x200E) Bit[00] -> Bit[31]
indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_und_r0_enb_int_dcc_hdlc_und_len_31_0_Mask                                cBit31_0
#define cAf6_upen_txdcc_intenb_und_r0_enb_int_dcc_hdlc_und_len_31_0_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Enable Interrupt "Undersize HDLC Packet's Length " Channel 47 to 32
Reg Addr   : 0x0001E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_und_r1                                                               0x0001E
#define cAf6Reg_upen_txdcc_intenb_und_r1_WidthVal                                                           32

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_und_len_47_32
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
undered minimum allowed length (2 bytes)" (Ref reg 0x201E) Bit[00] -> Bit[15]
indicate channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_und_r1_enb_int_dcc_hdlc_und_len_47_32_Mask                                cBit15_0
#define cAf6_upen_txdcc_intenb_und_r1_enb_int_dcc_hdlc_und_len_47_32_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interrupt Global Packet Error
Reg Addr   : 0x00010
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_glb                                                                  0x00010
#define cAf6Reg_upen_txdcc_intenb_glb_WidthVal                                                              32

/*--------------------------------------
BitField Name: int_enb_dcc_ocn2eth_buf_ful
BitField Type: RW
BitField Desc: Enable Interrupt of  "OCN2ETH: One or more channels has buffer
full error" (Ref Reg 0x02010)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_glb_int_enb_dcc_ocn2eth_buf_ful_Mask                                      cBit3
#define cAf6_upen_txdcc_intenb_glb_int_enb_dcc_ocn2eth_buf_ful_Shift                                         3

/*--------------------------------------
BitField Name: int_enb_dcc_ocn2eth_crc_err
BitField Type: RW
BitField Desc: Enable Interrupt of  "OCN2ETH: One or more channels has CRC error
packet "
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_glb_int_enb_dcc_ocn2eth_crc_err_Mask                                      cBit2
#define cAf6_upen_txdcc_intenb_glb_int_enb_dcc_ocn2eth_crc_err_Shift                                         2

/*--------------------------------------
BitField Name: int_enb_dcc_ocn2eth_undsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of  "OCN2ETH: One or more channels has minimum
packet length violation"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_glb_int_enb_dcc_ocn2eth_undsize_len_Mask                                   cBit1
#define cAf6_upen_txdcc_intenb_glb_int_enb_dcc_ocn2eth_undsize_len_Shift                                       1

/*--------------------------------------
BitField Name: enb_int_dcc_ocn2eth_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of  "OCN2ETH: One or more channels has maximum
packet length violation "
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_glb_enb_int_dcc_ocn2eth_ovrsize_len_Mask                                   cBit0
#define cAf6_upen_txdcc_intenb_glb_enb_int_dcc_ocn2eth_ovrsize_len_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Loopback Enable Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides loopback enable configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_loopen                                                                            0x00001
#define cAf6Reg_upen_loopen_WidthVal                                                                        32

/*--------------------------------------
BitField Name: dcc_buffer_loop_en
BitField Type: RW
BitField Desc: Enable loopback of RX-BUFFER to TX-BUFFER      .
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_loopen_dcc_buffer_loop_en_Mask                                                         cBit5
#define cAf6_upen_loopen_dcc_buffer_loop_en_Shift                                                            5

/*--------------------------------------
BitField Name: genmon_loop_en
BitField Type: RW
BitField Desc: Enable loopback of data from DCC generator to RX-DCC  .
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_loopen_genmon_loop_en_Mask                                                             cBit4
#define cAf6_upen_loopen_genmon_loop_en_Shift                                                                4

/*--------------------------------------
BitField Name: ksdh_loop_en
BitField Type: RW
BitField Desc: Enable loopback of Kbyte information           .
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_loopen_ksdh_loop_en_Mask                                                               cBit3
#define cAf6_upen_loopen_ksdh_loop_en_Shift                                                                  3

/*--------------------------------------
BitField Name: ksgm_loop_en
BitField Type: RW
BitField Desc: Enable SGMII loopback of Kbyte Port.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_loopen_ksgm_loop_en_Mask                                                               cBit2
#define cAf6_upen_loopen_ksgm_loop_en_Shift                                                                  2

/*--------------------------------------
BitField Name: hdlc_loop_en
BitField Type: RW
BitField Desc: Enable loopback from HDLC Encap to HDLC DEcap of DCC byte.
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_loopen_hdlc_loop_en_Mask                                                               cBit1
#define cAf6_upen_loopen_hdlc_loop_en_Shift                                                                  1

/*--------------------------------------
BitField Name: dsgm_loop_en
BitField Type: RW
BitField Desc: Enable SGMII loopback of DCC Port.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_loopen_dsgm_loop_en_Mask                                                               cBit0
#define cAf6_upen_loopen_dsgm_loop_en_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Enable DCC Engine
Reg Addr   : 0x0000a
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to enable DCC Engine to receive/transmit data from/to OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_dcc_en                                                                        0x0000a
#define cAf6Reg_upen_cfg_dcc_en_V2                                                                        0x0001F
#define cAf6Reg_upen_cfg_dcc_en_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_dcc_en
BitField Type: RW
BitField Desc: Enable DCC Engine to receive/transmit data from/to OCN.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_dcc_en_cfg_dcc_en_Mask                                                             cBit0
#define cAf6_upen_cfg_dcc_en_cfg_dcc_en_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Dcc_Fcs_Rem_Mode
Reg Addr   : 0x10003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to remove FCS32 or FCS16

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctx_fcsrem                                                                      0x10003

/*--------------------------------------
BitField Name: FCS_Remove_Mode
BitField Type: RW
BitField Desc: Remove 4 bytes FCS32 or 2byte FCS16 This configuration depend on
the FCS checking mode at HDLC DEC (which is configed at reg address : 0x04000 -
0x04037)
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dcctx_fcsrem_FCS_Remove_Mode_Mask                                                      cBit0
#define cAf6_upen_dcctx_fcsrem_FCS_Remove_Mode_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDC_TX_Header_Per_Channel
Reg Addr   : 0x11400 - 0x117FF
Reg Formula: 0x11400 + $channelid*16 + $hdrpos
    Where  : 
           + $channelid(0-47): Channel ID
           + $hdrpos(0-15):header's byte position
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID, in which, only 16byte is configurable
the configuration postion of header byte is as follow; DA(6byte) + SA(6byte) + {PCP,DEI,VID}(2byte) + VERSION(1byte) + TYPE(1byte)
hdrpos = 0 is position of DA[47:40] and hdrpos = 15 is position of TYPE field

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcchdr_Base                                                                       0x11400
#define cAf6Reg_upen_dcchdr(channelid, hdrpos)                               (0x11400+(channelid)*16+(hdrpos))
#define cAf6Reg_upen_dcchdr_WidthVal                                                                        32

/*--------------------------------------
BitField Name: HEADER_POS
BitField Type: RW
BitField Desc: 8b value of header per Channel ID
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_dcchdr_HEADER_POS_Mask                                                               cBit7_0
#define cAf6_upen_dcchdr_HEADER_POS_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Channel_Enable
Reg Addr   : 0x11000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for enable transmission DDC packet per channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctx_enacid                                                                      0x11000
#define cAf6Reg_upen_dcctx_enacid_WidthVal                                                                  64

/*--------------------------------------
BitField Name: Channel_Enable
BitField Type: RW
BitField Desc: Enable transmitting of DDC packet per channel. Bit[47:00]
represent for channel 47->00
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dcctx_enacid_Channel_Enable_Mask_01                                                 cBit31_0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Shift_01                                                       0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Mask_02                                                 cBit15_0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Shift_02                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Global_ProvisionedHeader_Configuration
Reg Addr   : 0x12001
Reg Formula: 0x12001
    Where  : 
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxhdr                                                                          0x12001
#define cAf6Reg_upen_dccrxhdr_WidthVal                                                                      96

/*--------------------------------------
BitField Name: ETH_TYP
BitField Type: RW
BitField Desc: 16b value of Provisioned ETHERTYPE of DCC
BitField Bits: [69:54]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_ETH_TYP_01_Mask                                                           cBit31_22
#define cAf6_upen_dccrxhdr_ETH_TYP_01_Shift                                                                 22
#define cAf6_upen_dccrxhdr_ETH_TYP_02_Mask                                                             cBit5_0
#define cAf6_upen_dccrxhdr_ETH_TYP_02_Shift                                                                  0

/*--------------------------------------
BitField Name: CVID
BitField Type: RW
BitField Desc: 12b value of Provisioned C-VLAN ID. This value is used to
compared with received CVLAN ID value.
BitField Bits: [53:42]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_CVID_Mask                                                                 cBit21_10
#define cAf6_upen_dccrxhdr_CVID_Shift                                                                       10

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: 42b MSB of Provisioned MAC DA value. This value is used to
compared with received MAC_DA[47:06] value. If a match is confirmed,
MAC_DA[05:00] is used to represent channelID value before mapping.
BitField Bits: [41:00]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_MAC_DA_Mask_01                                                             cBit31_0
#define cAf6_upen_dccrxhdr_MAC_DA_Shift_01                                                                   0
#define cAf6_upen_dccrxhdr_MAC_DA_Mask_02                                                              cBit9_0
#define cAf6_upen_dccrxhdr_MAC_DA_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_MAC_Check_Enable_Configuration
Reg Addr   : 0x12002
Reg Formula: 0x12002
    Where  : 
Reg Desc   : 
The register provides configuration that enable base MAC DA check per channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxmacenb                                                                       0x12002
#define cAf6Reg_upen_dccrxmacenb_WidthVal                                                                   64

/*--------------------------------------
BitField Name: MAC_check_enable
BitField Type: RW
BitField Desc: Enable channel checking of received MAC DA compare to globally
provisioned Base MAC.On mismatch, frame is discarded.
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Mask_01                                                cBit31_0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Shift_01                                                      0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Mask_02                                                cBit15_0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Shift_02                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_CVLAN_Check_Enable_Configuration
Reg Addr   : 0x12003
Reg Formula: 0x12003
    Where  : 
Reg Desc   : 
The register provides configuration that enable base CVLAN Check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxcvlenb                                                                       0x12003
#define cAf6Reg_upen_dccrxcvlenb_WidthVal                                                                   64

/*--------------------------------------
BitField Name: CVL_check_enable
BitField Type: RW
BitField Desc: Enable channel checking of received CVLAN ID compare to globally
provisioned CVLAN ID.On mismatch, frame is discarded.
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Mask_01                                                cBit31_0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Shift_01                                                      0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Mask_02                                                cBit15_0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Shift_02                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Channel_Mapping
Reg Addr   : 0x12400 - 0x1242F
Reg Formula: 0x12400 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
The register provides channel mapping from received MAC DA to internal channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccdec_Base                                                                       0x12400
#define cAf6Reg_upen_dccdec(channelid)                                                   (0x12400+(channelid))
#define cAf6Reg_upen_dccdec_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Local Channel Identifier. Rx packet is discarded if enable
is not set
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_dccdec_Channel_enable_Mask                                                             cBit6
#define cAf6_upen_dccdec_Channel_enable_Shift                                                                6

/*--------------------------------------
BitField Name: Mapping_ChannelID
BitField Type: RW
BitField Desc: Local ChannelID that is mapped from received bit[5:0] of MAC DA
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_dccdec_Mapping_ChannelID_Mask                                                        cBit5_0
#define cAf6_upen_dccdec_Mapping_ChannelID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_Length_Alarm_Sticky
Reg Addr   : 0x11004
Reg Formula: 0x11004
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_pktlen                                                                     0x11004

/*--------------------------------------
BitField Name: dcc_overize_err
BitField Type: W1C
BitField Desc: HDLC Length Oversize Error. Alarm per channel Received HDLC Frame
from OCN has frame length over maximum allowed length (1536bytes) Bit[95] ->
Bit[48]: channel ID 47 ->0
BitField Bits: [95:48]
--------------------------------------*/
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Mask_01                                              cBit31_16
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Shift_01                                                    16
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Mask_02                                               cBit31_0
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Shift_02                                                     0

/*--------------------------------------
BitField Name: dcc_undsize_err
BitField Type: W1C
BitField Desc: HDLC Length Undersize Error. Alarm per channel Received HDLC
Frame from OCN has frame length below minimum allowed length (2bytes) Bit[47] ->
Bit[0]: channel ID 47 ->0
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Mask_01                                               cBit31_0
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Shift_01                                                     0
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Mask_02                                               cBit15_0
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Shift_02                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky
Reg Addr   : 0x11005
Reg Formula: 0x11005
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_crcbuf                                                                     0x11005

/*--------------------------------------
BitField Name: dcc_crc_err
BitField Type: W1C
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[95] -> Bit[48]: channel ID 47 ->0
BitField Bits: [95:48]
--------------------------------------*/
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Mask_01                                                  cBit31_16
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Shift_01                                                        16
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Mask_02                                                   cBit31_0
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Shift_02                                                         0

/*--------------------------------------
BitField Name: dcc_buffull_err
BitField Type: W1C
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[47]
-> Bit[0]: channel ID 47 ->0
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Mask_01                                               cBit31_0
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Shift_01                                                     0
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Mask_02                                               cBit15_0
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Shift_02                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DCC_ETH2OCN_Alarm_Sticky
Reg Addr   : 0x12008
Reg Formula: 0x12008
    Where  : 
Reg Desc   : 
The register provides Alarms of ETH2OCN Direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_rx_eth2ocn                                                                 0x12008
#define cAf6Reg_upen_stkerr_rx_eth2ocn_WidthVal                                                            128

/*--------------------------------------
BitField Name: dcc_eth2ocn_buffull_err
BitField Type: W1C
BitField Desc: Packet buffer full (ETH to OCN direction) . Alarm per channel
Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[101] ->
Bit[53]: channel ID 47 ->0
BitField Bits: [101:53]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Mask_01                                  cBit31_21
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Shift_01                                        21
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Mask_02                                    cBit5_0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Shift_02                                         0

/*--------------------------------------
BitField Name: dcc_rxeth_maxlenerr
BitField Type: W1C
BitField Desc: Received DCC packet from SGMII port has violated maximum packet's
length error
BitField Bits: [52:52]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_maxlenerr_Mask                                            cBit20
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_maxlenerr_Shift                                               20

/*--------------------------------------
BitField Name: dcc_rxeth_crcerr
BitField Type: W1C
BitField Desc: Received DCC packet from SGMII port has CRC error
BitField Bits: [51:51]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_crcerr_Mask                                               cBit19
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_crcerr_Shift                                                  19

/*--------------------------------------
BitField Name: dcc_channel_disable
BitField Type: W1C
BitField Desc: DCC Local Channel Identifier mapping is disable Bit[50] ->
Bit[03] indicate channel 47-> 00.
BitField Bits: [50:03]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Mask_01                                       cBit31_3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Shift_01                                             3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Mask_02                                       cBit18_0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Shift_02                                             0

/*--------------------------------------
BitField Name: dcc_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_cvlid_mismat_Mask                                                cBit2
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_cvlid_mismat_Shift                                                   2

/*--------------------------------------
BitField Name: dcc_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_macda_mismat_Mask                                                cBit1
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_macda_mismat_Shift                                                   1

/*--------------------------------------
BitField Name: dcc_ethtp_mismat
BitField Type: W1C
BitField Desc: Received Ethernet Type of DCC frame different from global
provisioned value
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_ethtp_mismat_Mask                                                cBit0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_ethtp_mismat_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Min Pkt Length Alarm Current Status Reg0
Reg Addr   : 0x11008
Reg Formula: 0x11008
    Where  : 
Reg Desc   : 
The register provides Current Status Alarm Related to HDLC Length Error Undersize (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_undsz_r0                                                                   0x11008

/*--------------------------------------
BitField Name: dcc_ocn2eth_undsz_err_cursta_0_31
BitField Type: RO
BitField Desc: Current status of HDLC Length Undersize Error. Status per channel
Received HDLC Frame from OCN has frame length below minimum allowed length
(2bytes) Bit[31] -> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_undsz_r0_dcc_ocn2eth_undsz_err_cursta_0_31_Mask                                cBit31_0
#define cAf6_upen_curerr_undsz_r0_dcc_ocn2eth_undsz_err_cursta_0_31_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Min Pkt Length Current Status Reg1
Reg Addr   : 0x11018
Reg Formula: 0x11018
    Where  : 
Reg Desc   : 
The register provides Current Status Alarm Related to HDLC Length Error Undersize (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_undsz_r1                                                                   0x11018
#define cAf6Reg_upen_curerr_undsz_r1_WidthVal                                                               32

/*--------------------------------------
BitField Name: dcc_ocn2eth_undsz_err_cursta_32_47
BitField Type: RO
BitField Desc: HDLC Length Undersize Error. Status per channel Received HDLC
Frame from OCN has frame length below minimum allowed length (2bytes) Bit[0] ->
Bit[15]: channel ID 32 -> 47
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_curerr_undsz_r1_dcc_ocn2eth_undsz_err_cursta_32_47_Mask                                cBit15_0
#define cAf6_upen_curerr_undsz_r1_dcc_ocn2eth_undsz_err_cursta_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Max Pkt Length urrent Status Reg0
Reg Addr   : 0x11009
Reg Formula: 0x11009
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_ovrsz_r0                                                                   0x11009

/*--------------------------------------
BitField Name: dcc_ocn2eth_ovrsz_err_cursta_0_31
BitField Type: RO
BitField Desc: Current status of HDLC Length Oversize Error. Status per channel
Received HDLC Frame from OCN has frame length over maximum allowed length
(1536bytes) Bit[] -> Bit[31]: channel ID 0 -> 31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_ovrsz_r0_dcc_ocn2eth_ovrsz_err_cursta_0_31_Mask                                cBit31_0
#define cAf6_upen_curerr_ovrsz_r0_dcc_ocn2eth_ovrsz_err_cursta_0_31_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Max Pkt Length Current Status Reg1
Reg Addr   : 0x11019
Reg Formula: 0x11019
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_ovrsz_r1                                                                   0x11019
#define cAf6Reg_upen_curerr_ovrsz_r1_WidthVal                                                               32

/*--------------------------------------
BitField Name: dcc_ocn2eth_ovrsz_err_cursta_32_47
BitField Type: RO
BitField Desc: HDLC Length Oversize Error. Status per channel Received HDLC
Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[0 ]
-> Bit[15]: channel ID 32 -> 47
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_curerr_ovrsz_r1_dcc_ocn2eth_ovrsz_err_cursta_32_47_Mask                                cBit15_0
#define cAf6_upen_curerr_ovrsz_r1_dcc_ocn2eth_ovrsz_err_cursta_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_CRC_Error_Current_Alarm_Status_Reg0
Reg Addr   : 0x1100A
Reg Formula: 0x1100A
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_crcbuf_r0                                                                  0x1100A

/*--------------------------------------
BitField Name: dcc_ocn2eth_crc_err_cursta_0_31
BitField Type: RO
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[0] -> Bit[31]: channel ID 0 ->31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_crcbuf_r0_dcc_ocn2eth_crc_err_cursta_0_31_Mask                                cBit31_0
#define cAf6_upen_curerr_crcbuf_r0_dcc_ocn2eth_crc_err_cursta_0_31_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status
Reg Addr   : 0x1101A
Reg Formula: 0x1101A
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_crcbuf_r1                                                                  0x1101A
#define cAf6Reg_upen_curerr_crcbuf_r1_WidthVal                                                              32

/*--------------------------------------
BitField Name: dcc_ocn2eth_crc_err_cursta_32_47
BitField Type: RO
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[0] -> Bit[15]: channel ID 32 ->47
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_curerr_crcbuf_r1_dcc_ocn2eth_crc_err_cursta_32_47_Mask                                cBit15_0
#define cAf6_upen_curerr_crcbuf_r1_dcc_ocn2eth_crc_err_cursta_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Buffer Full Current Status Reg 0
Reg Addr   : 0x1100B
Reg Formula: 0x1100B
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_bufept_r0                                                                  0x1100B

/*--------------------------------------
BitField Name: dcc_ocn2eth_bufful_err_cursta_0_31
BitField Type: RO
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[0]
-> Bit[31]: channel ID 0 -> 31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_bufept_r0_dcc_ocn2eth_bufful_err_cursta_0_31_Mask                                cBit31_0
#define cAf6_upen_curerr_bufept_r0_dcc_ocn2eth_bufful_err_cursta_0_31_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Buffer_Full_Alarm_Status
Reg Addr   : 0x1101B
Reg Formula: 0x1101B
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_bufept_r1                                                                  0x1101B
#define cAf6Reg_upen_curerr_bufept_r1_WidthVal                                                              32

/*--------------------------------------
BitField Name: dcc_ocn2eth_bufful_err_cursta_32_47
BitField Type: RO
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[0]
-> Bit[15]: channel ID 32 ->47
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_curerr_bufept_r1_dcc_ocn2eth_bufful_err_cursta_32_47_Mask                                cBit15_0
#define cAf6_upen_curerr_bufept_r1_dcc_ocn2eth_bufful_err_cursta_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Current Status of Global Alarm
Reg Addr   : 0x1100E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides current status of global alarm of DCC, OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_glb                                                                        0x1100E
#define cAf6Reg_upen_curerr_glb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_ful_err
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has buffer full
error"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ful_err_Mask                                         cBit3
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ful_err_Shift                                            3

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_crc_err
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has CRC error
packet "
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_crc_err_Mask                                         cBit2
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_crc_err_Shift                                            2

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_undsz_len
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has minimum
packet length violation"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_undsz_len_Mask                                       cBit1
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_undsz_len_Shift                                          1

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_ovrsz_len
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has maximum
packet length violation "
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ovrsz_len_Mask                                       cBit0
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ovrsz_len_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Alarm " Local Channel Mapping Is Disable" Current Status of Channel 31 to 0
Reg Addr   : 0x12004
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cur_rxdcc0_r0                                                                     0x12004

/*--------------------------------------
BitField Name: dcc_eth2ocn_chandis_cursta_31_0
BitField Type: RO
BitField Desc: Current Status of Alarm "DCC Local Channel Identifier mapping is
disable" of channel 0 to 31 Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cur_rxdcc0_r0_dcc_eth2ocn_chandis_cursta_31_0_Mask                                  cBit31_0
#define cAf6_upen_cur_rxdcc0_r0_dcc_eth2ocn_chandis_cursta_31_0_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Alarm " Local Channel Mapping Is Disable" Current Status of Channel 32 to 47
Reg Addr   : 0x12014
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cur_rxdcc0_r1                                                                     0x12014
#define cAf6Reg_upen_cur_rxdcc0_r1_WidthVal                                                                 32

/*--------------------------------------
BitField Name: dcc_eth2ocn_chandis_cursta_32_47
BitField Type: RO
BitField Desc: Current Status of Alarm "DCC Local Channel Identifier mapping is
disable" of Channel 32 to 47 Bit[00] -> Bit[15] indicate channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_cur_rxdcc0_r1_dcc_eth2ocn_chandis_cursta_32_47_Mask                                 cBit15_0
#define cAf6_upen_cur_rxdcc0_r1_dcc_eth2ocn_chandis_cursta_32_47_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Curren Status of Buffer Full Channel 31 to 0
Reg Addr   : 0x12006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides current alarm status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cur_rxdcc2_r0                                                                     0x12006

/*--------------------------------------
BitField Name: dcc_eth2ocn_bufful_cursta_31_0
BitField Type: RO
BitField Desc: Current status of "DCC packet buffer for ETH2OCN direction was
fulled, some packets will be lost." channel 0 to 31 Bit[00] -> Bit[31] indicate
status of channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cur_rxdcc2_r0_dcc_eth2ocn_bufful_cursta_31_0_Mask                                   cBit31_0
#define cAf6_upen_cur_rxdcc2_r0_dcc_eth2ocn_bufful_cursta_31_0_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Curren Status of Buffer Full Channel 32 to 47
Reg Addr   : 0x12016
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides current alarm status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cur_rxdcc2_r1                                                                     0x12016
#define cAf6Reg_upen_cur_rxdcc2_r1_WidthVal                                                                 32

/*--------------------------------------
BitField Name: dcc_eth2ocn_bufful_cursta_32_47
BitField Type: RO
BitField Desc: Current status of "DCC packet buffer for ETH2OCN direction was
fulled, some packets will be lost." channel 32 to 47 Bit[00] -> Bit[15] indicate
status of channel 32 -> 47.
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_cur_rxdcc2_r1_dcc_eth2ocn_bufful_cursta_32_47_Mask                                  cBit15_0
#define cAf6_upen_cur_rxdcc2_r1_dcc_eth2ocn_bufful_cursta_32_47_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC_ETH2OCN_Alarm_Status
Reg Addr   : 0x12005
Reg Formula: 0x12005
    Where  : 
Reg Desc   : 
The register provides Alarms of ETH2OCN Direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_rx_glb_sta                                                                    0x12005
#define cAf6Reg_upen_dcc_rx_glb_sta_WidthVal                                                                32

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_bufful
BitField Type: RO
BitField Desc: One or more channels has buffer full error
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_bufful_Mask                                             cBit5
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_bufful_Shift                                                5

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_rxchid_dis
BitField Type: RO
BitField Desc: One or more "DCC Local Channel Identifier mapping is disable"
status
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_rxchid_dis_Mask                                         cBit4
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_rxchid_dis_Shift                                            4

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_ovrsz_len
BitField Type: RO
BitField Desc: Received DCC packet from SGMII port has violated maximum packet's
length error
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_ovrsz_len_Mask                                          cBit3
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_ovrsz_len_Shift                                             3

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_crcerr
BitField Type: RO
BitField Desc: Received packet from SGMII port has FCS error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_crcerr_Mask                                             cBit2
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_crcerr_Shift                                                2

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_cvlid_mismat
BitField Type: RO
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_cvlid_mismat_Mask                                       cBit2
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_cvlid_mismat_Shift                                          2

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_macda_mismat
BitField Type: RO
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_macda_mismat_Mask                                       cBit1
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_macda_mismat_Shift                                          1


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Eop_From_SGMII_Port1
Reg Addr   : 0x12021
Reg Formula: 0x12021
    Where  : 
Reg Desc   : 
Counter of number of EOP receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxeop                                                               0x12021

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter of EOP receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Err_From_SGMII_Port1
Reg Addr   : 0x12022
Reg Formula: 0x12022
    Where  : 
Reg Desc   : 
Counter of number of ERR receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxerr                                                               0x12022

/*--------------------------------------
BitField Name: err_counter
BitField Type: WC
BitField Desc: Counter of ERR receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Byte_From_SGMII_Port1
Reg Addr   : 0x12023
Reg Formula: 0x12023
    Where  : 
Reg Desc   : 
Counter of number of BYTE receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxbyt                                                               0x12023

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter of BYTE receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port1
Reg Addr   : 0x12024
Reg Formula: 0x12024
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxfail                                                              0x12024

/*--------------------------------------
BitField Name: unk_counter
BitField Type: WC
BitField Desc: Counter of unknow receive packet from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Eop_To_SGMII_Port1
Reg Addr   : 0x11011
Reg Formula: 0x11011
    Where  : 
Reg Desc   : 
Counter of number of EOP transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxeop                                                               0x11011

/*--------------------------------------
BitField Name: sgm_txglb_eop_counter
BitField Type: WC
BitField Desc: Counter of EOP transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Err_To_SGMII_Port1
Reg Addr   : 0x11012
Reg Formula: 0x11012
    Where  : 
Reg Desc   : 
Counter of number of ERR transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxerr                                                               0x11012

/*--------------------------------------
BitField Name: sgm_txglb_err_counter
BitField Type: WC
BitField Desc: Counter of ERR  transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Byte_To_SGMII_Port1
Reg Addr   : 0x11013
Reg Formula: 0x11013
    Where  : 
Reg Desc   : 
Counter of number of BYTE transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxbyt                                                               0x11013

/*--------------------------------------
BitField Name: sgm_txglb_byte_counter
BitField Type: WC
BitField Desc: Counter of TX BYTE to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Mask                                     cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Packet_And_Byte_Per_Channel
Reg Addr   : 0x13000 - 0x13FFF
Reg Formula: 0x13000 + $cnt_type*64 + $channelID
    Where  : 
           + $cnt_type(1-14): counter type
           + $channelID(0-47): channel value
Reg Desc   : 
Counter of DCC at different point represent by cnt_type value:
{1} : counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{2} : counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{3} : counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{4} : counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{5} : counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{6} : counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{7} : counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{8} : counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{9} : counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{10}: counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{11}: counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{12}: counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
{13}: counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
{14}: counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cnt_Base                                                                      0x13000
#define cAf6Reg_upen_dcc_cnt(cnttype, channelID)                            (0x13000+(cnttype)*64+(channelID))

/*--------------------------------------
BitField Name: dcc_counter
BitField Type: WC
BitField Desc: Counter of DCC packet and byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_cnt_dcc_counter_Mask                                                            cBit31_0
#define cAf6_upen_dcc_cnt_dcc_counter_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC LO DEC
Reg Addr   : 0x04000 - 0x04037
Reg Formula: 0x04000+ $CID
    Where  : 
           + $CID (0-47) : Channel ID
Reg Desc   : 
config HDLC ID 0-31
HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_locfg_Base                                                                   0x04000
#define cAf6Reg_upen_hdlc_locfg(CID)                                                           (0x04000+(CID))
#define cAf6Reg_upen_hdlc_locfg_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_scren
BitField Type: R/W
BitField Desc: config to enable scramble, (1) is enable, (0) is disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_scren_Mask                                                              cBit4
#define cAf6_upen_hdlc_locfg_cfg_scren_Shift                                                                 4

/*--------------------------------------
BitField Name: cfg_bitstuff
BitField Type: R/W
BitField Desc: config to select bit stuff or byte sutff, (1) is bit stuff, (0)
is byte stuff
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Mask                                                           cBit2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Shift                                                              2

/*--------------------------------------
BitField Name: cfg_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Mask                                                             cBit1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_fcsmode
BitField Type: R/W
BitField Desc: config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS
16
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Mask                                                            cBit0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC GLOBAL LO DEC
Reg Addr   : 0x04404
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_loglbcfg                                                                     0x04404
#define cAf6Reg_upen_hdlc_loglbcfg_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: config to receive LSB first, (1) is LSB first, (0) is default MSB
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Mask                                                        cBit3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Shift                                                           3


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Master Control
Reg Addr   : 0x30003
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC Encode Master

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_master_ctrl                                                              0x30003
#define cAf6Reg_upen_hdlc_enc_master_ctrl_WidthVal                                                          32

/*--------------------------------------
BitField Name: encap_lsbfirst
BitField Type: R/W
BitField Desc: config to transmit LSB first, (1) is LSB first, (0) is default
MSB
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_Mask                                               cBit0
#define cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 1
Reg Addr   : 0x38000 - 0x38037
Reg Formula: 0x38000+ $CID
    Where  : 
           + $CID (0-47) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg1_Base                                                           0x38000
#define cAf6Reg_upen_hdlc_enc_ctrl_reg1(CID)                                                   (0x38000+(CID))

/*--------------------------------------
BitField Name: encap_screnb
BitField Type: R/W
BitField Desc: Enable Scamble (1) Enable      , (0) disable
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Mask                                                   cBit9
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Shift                                                      9

/*--------------------------------------
BitField Name: encap_idlemod
BitField Type: R/W
BitField Desc: This bit is only used in Bit Stuffing mode Used to configured
IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC
is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            ,
(0) Disabe
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Mask                                                  cBit7
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Shift                                                     7

/*--------------------------------------
BitField Name: encap_sabimod
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1
byte
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Mask                                                  cBit6
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Shift                                                     6

/*--------------------------------------
BitField Name: encap_sabiins
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Mask                                                  cBit5
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Shift                                                     5

/*--------------------------------------
BitField Name: encap_ctrlins
BitField Type: R/W
BitField Desc: Address/Control Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Mask                                                  cBit4
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Shift                                                     4

/*--------------------------------------
BitField Name: encap_fcsmod
BitField Type: R/W
BitField Desc: FCS Select Mode (1) 32b FCS           , (0) 16b FCS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Mask                                                   cBit3
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Shift                                                      3

/*--------------------------------------
BitField Name: encap_fcsins
BitField Type: R/W
BitField Desc: FCS Insert Enable (1) Enable Insert     , (0) Disable Insert
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Mask                                                   cBit2
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Shift                                                      2

/*--------------------------------------
BitField Name: encap_flgmod
BitField Type: R/W
BitField Desc: Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Mask                                                   cBit1
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Shift                                                      1

/*--------------------------------------
BitField Name: encap_stfmod
BitField Type: R/W
BitField Desc: Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Mask                                                   cBit0
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 2
Reg Addr   : 0x39000 - 0x39037
Reg Formula: 0x39000+ $CID
    Where  : 
           + $CID (0-47) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 2 Byte Stuff Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg2_Base                                                           0x39000
#define cAf6Reg_upen_hdlc_enc_ctrl_reg2(CID)                                                   (0x39000+(CID))

/*--------------------------------------
BitField Name: encap_addrval
BitField Type: R/W
BitField Desc: Address Field
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Mask                                              cBit31_24
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Shift                                                    24

/*--------------------------------------
BitField Name: encap_ctlrval
BitField Type: R/W
BitField Desc: Control Field
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Mask                                              cBit23_16
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Shift                                                    16

/*--------------------------------------
BitField Name: encap_sapival0
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 1st byte
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Mask                                              cBit15_8
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Shift                                                    8

/*--------------------------------------
BitField Name: encap_sapival1
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 2nd byte
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Mask                                               cBit7_0
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Sdh_Req_Interval_Configuration
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to configure time interval to generate fake SDH request signal

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_genmon_reqint                                                                     0x00003
#define cAf6Reg_upen_genmon_reqint_WidthVal                                                                 32

/*--------------------------------------
BitField Name: Timer_Enable
BitField Type: RW
BitField Desc: Enable generation of fake request SDH signal
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_genmon_reqint_Timer_Enable_Mask                                                       cBit28
#define cAf6_upen_genmon_reqint_Timer_Enable_Shift                                                          28

/*--------------------------------------
BitField Name: Timer_Value
BitField Type: RW
BitField Desc: Counter of number of clk 155MHz between fake request SDH signal.
This counter is used make a delay interval between 2 consecutive SDH requests
generated by DCC GENERATOR. For example: to create a delay of 125us between SDH
request signals, with a clock 155Mz, the value of counter to be configed to this
field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_genmon_reqint_Timer_Value_Mask                                                      cBit27_0
#define cAf6_upen_genmon_reqint_Timer_Value_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Header_Per_Channel
Reg Addr   : 0x0C100 - 0x0C120
Reg Formula: 0x0C100 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of VID and MAC DA Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_hdr_Base                                                          0x0C100
#define cAf6Reg_upen_dcc_cfg_testgen_hdr(channelid)                                      (0x0C100+(channelid))
#define cAf6Reg_upen_dcc_cfg_testgen_hdr_WidthVal                                                           32

/*--------------------------------------
BitField Name: HDR_VID
BitField Type: RW
BitField Desc: 12b of VID value in header of Generated Packet
BitField Bits: [19:08]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_Mask                                                    cBit19_8
#define cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_Shift                                                          8

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: Bit [7:0] of MAC DA value [47:0] of generated Packet
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_Mask                                                      cBit7_0
#define cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Mode_Per_Channel
Reg Addr   : 0x0C200 - 0x0C220
Reg Formula: 0x0C200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of generation mode of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_mod_Base                                                          0x0C200
#define cAf6Reg_upen_dcc_cfg_testgen_mod(channelid)                                      (0x0C200+(channelid))

/*--------------------------------------
BitField Name: gen_payload_mod
BitField Type: RW
BitField Desc: Modes of generated packet payload:
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_Mask                                           cBit31_30
#define cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_Shift                                                 30

/*--------------------------------------
BitField Name: gen_len_mode
BitField Type: RW
BitField Desc: Modes of generated packet length :
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_Mask                                              cBit29_28
#define cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_Shift                                                    28

/*--------------------------------------
BitField Name: gen_fix_pattern
BitField Type: RW
BitField Desc: 8b fix pattern payload if mode "fix payload" is used
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_Mask                                           cBit27_20
#define cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_Shift                                                 20

/*--------------------------------------
BitField Name: gen_number_of_packet
BitField Type: RW
BitField Desc: Number of packets the GEN generates for each channelID in gen
burst mode
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_Mask                                       cBit19_0
#define cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Enable_Channel
Reg Addr   : 0x0C000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable Channel to generate DCC packets

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_enacid                                                            0x0C000
#define cAf6Reg_upen_dcc_cfg_testgen_enacid_WidthVal                                                        64

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Channel to generate DCC packets. Bit[47:0] <-> Channel
47->0
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Mask_01                                       cBit31_0
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Shift_01                                             0
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Mask_02                                       cBit15_0
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Shift_02                                             0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Mode
Reg Addr   : 0x0C001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides global configuration of GEN mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_mode                                                      0x0C001

/*--------------------------------------
BitField Name: gen_force_err_len
BitField Type: RW
BitField Desc: Create wrong packet's length fiedl generated packets
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Mask                                    cBit6
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Shift                                       6

/*--------------------------------------
BitField Name: gen_force_err_fcs
BitField Type: RW
BitField Desc: Create wrong FCS field in generated packets
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Mask                                    cBit5
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Shift                                       5

/*--------------------------------------
BitField Name: gen_force_err_dat
BitField Type: RW
BitField Desc: Create wrong data value in generated packets
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Mask                                    cBit4
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Shift                                       4

/*--------------------------------------
BitField Name: gen_force_err_seq
BitField Type: RW
BitField Desc: Create wrong Sequence field in generated packets
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_seq_Mask                                    cBit3
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_seq_Shift                                       3

/*--------------------------------------
BitField Name: gen_force_err_vcg
BitField Type: RW
BitField Desc: Create wrong VCG field in generated packets
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_vcg_Mask                                    cBit2
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_vcg_Shift                                       2

/*--------------------------------------
BitField Name: gen_burst_mod
BitField Type: RW
BitField Desc: Gen each Channel a number of packet as configured in "Gen mode
per channel" resgister
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Mask                                        cBit1
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Shift                                           1

/*--------------------------------------
BitField Name: gen_conti_mod
BitField Type: RW
BitField Desc: Gen packet forever without stopping
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Mask                                        cBit0
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Packet_Length
Reg Addr   : 0x0C002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration min length max length of generated Packets

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_length                                                        0x0C002

/*--------------------------------------
BitField Name: gen_max_length
BitField Type: RW
BitField Desc: Maximum length of generated packet
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Mask                                     cBit31_16
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Shift                                           16

/*--------------------------------------
BitField Name: gen_min_length
BitField Type: RW
BitField Desc: Minimum length of generated packet, also used for fix length
packets in case fix length mode is used
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Mask                                      cBit15_0
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Interval
Reg Addr   : 0x0C003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for packet generating interval

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval                                                  0x0C003
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval_WidthVal                                              32

/*--------------------------------------
BitField Name: Gen_Intv_Enable
BitField Type: RW
BitField Desc: Enable using this interval value
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_Mask                                  cBit28
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_Shift                                      28

/*--------------------------------------
BitField Name: Gen_Intv_Value
BitField Type: RW
BitField Desc: Counter of number of clk 155MHz between packet generation. This
counter is used make a delay interval between 2 consecutive packet generation.
For example: to create a delay of 125us between 2 packet generating, with a
clock 155Mz, the value of counter to be configed to this field is
(125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Mask                                cBit27_0
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Good_Packet_Counter
Reg Addr   : 0x0E100 - 0x0E130
Reg Formula: 0x0E100 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
Counter of receive good packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_godpkt_Base                                                                   0x0E100
#define cAf6Reg_upen_mon_godpkt(channelid)                                               (0x0E100+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_good_cnt
BitField Type: WC
BitField Desc: Counter of Receive Good Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_godpkt_dcc_test_mon_good_cnt_Mask                                               cBit31_0
#define cAf6_upen_mon_godpkt_dcc_test_mon_good_cnt_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_Data_Packet_Counter
Reg Addr   : 0x0E200 - 0x0E230
Reg Formula: 0x0E200 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
Counter of received packets has error data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errpkt_Base                                                                   0x0E200
#define cAf6Reg_upen_mon_errpkt(channelid)                                               (0x0E200+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errdat_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error Data Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errpkt_dcc_test_mon_errdat_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errpkt_dcc_test_mon_errdat_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_VCG_Packet_Counter
Reg Addr   : 0x0E300 - 0x0E330
Reg Formula: 0x0E300 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
Counter of received packet has wrong VCG value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errvcg_Base                                                                   0x0E300
#define cAf6Reg_upen_mon_errvcg(channelid)                                               (0x0E300+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errvcg_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error VCG Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errvcg_dcc_test_mon_errvcg_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errvcg_dcc_test_mon_errvcg_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_SEQ_Packet_Counter
Reg Addr   : 0x0E400 - 0x0E430
Reg Formula: 0x0E400 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
Counter of received packet has wrong Sequence value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errseq_Base                                                                   0x0E400
#define cAf6Reg_upen_mon_errseq(channelid)                                               (0x0E400+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errseq_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error Sequence Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errseq_dcc_test_mon_errseq_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errseq_dcc_test_mon_errseq_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_FCS_Packet_Counter
Reg Addr   : 0x0E500 - 0x0E530
Reg Formula: 0x0E500 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
Counter of received packet has wrong FCS value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errfcs_Base                                                                   0x0E500
#define cAf6Reg_upen_mon_errfcs(channelid)                                               (0x0E500+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errfcs_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error FCS Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errfcs_dcc_test_mon_errfcs_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errfcs_dcc_test_mon_errfcs_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Abort_VCG_Packet_Counter
Reg Addr   : 0x0E600 - 0x0E630
Reg Formula: 0x0E600 + $channelid
    Where  : 
           + $channelid(0-47): Channel ID
Reg Desc   : 
Counter of received packet has been abbort due to wrong length information

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_abrpkt_Base                                                                   0x0E600
#define cAf6Reg_upen_mon_abrpkt(channelid)                                               (0x0E600+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_abrpkt_cnt
BitField Type: WC
BitField Desc: Counter of Abbort Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_abrpkt_dcc_test_mon_abrpkt_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_abrpkt_dcc_test_mon_abrpkt_cnt_Shift                                                   0

#endif /* _THA60290011DCCKBYTEV2REG_H_ */

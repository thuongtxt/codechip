/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290011ModulePw.h
 *
 * Created Date: Oct 30, 2016
 *
 * Description : PW module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEPW_H_
#define _THA60290011MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModulePw * Tha60290011ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController Tha60290011PwHeaderControllerNew(ThaPwAdapter adapter);
AtPwHdlc Tha60290011PwDccNew(AtHdlcChannel dcc, AtModulePw module);

eBool Tha60290011ModulePwDccKbyteInterruptIsSupported(Tha60290011ModulePw self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEPW_H_ */

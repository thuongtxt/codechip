/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290011ModulePw.c
 *
 * Created Date: Oct 29, 2016
 *
 * Description : PW module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pw/defectcontroller/Tha60210031PwDefectController.h"
#include "../../Tha60210031/pw/Tha60210031ModulePwInternal.h"
#include "../../Tha60290021/common/Tha602900xxCommon.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "../pwe/Tha60290011ModulePwe.h"
#include "Tha60290011ModulePwInternal.h"
#include "Tha60290011KbyteDccInterruptManager.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaTxPwSubPortVlanCtrlReg  (0x00010E)
#define cThaRxPwSubPortVlanCtrlReg  (0x00010F)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011ModulePw *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods            m_AtModuleOverride;
static tAtModulePwMethods          m_AtModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;
static tTha60210031ModulePwMethods m_Tha60210031ModulePwOverride;

/* Save super implementation */
static const tAtModuleMethods   *m_AtModuleMethods   = NULL;
static const tAtModulePwMethods *m_AtModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1024;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StartVersionSupportHspw(ThaModulePw self)
    {
    /* Will not support HSPW for this project */
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan)
    {
    uint32 longReg[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    uint32 regVal = ThaPktUtilVlanToUint32(vlan);
    AtOsalMemInit(longReg, 0, sizeof(uint32) * cThaLongRegMaxSize);

    mModuleHwLongRead(self, cThaTxPwSubPortVlanCtrlReg, longReg, cThaLongRegMaxSize, core);
    longReg[subPortVlanIndex] = regVal;
    mModuleHwLongWrite(self, cThaTxPwSubPortVlanCtrlReg, longReg, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eAtRet TxSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan)
    {
    uint32 longReg[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    AtOsalMemInit(longReg, 0, sizeof(uint32) * cThaLongRegMaxSize);

    mModuleHwLongRead(self, cThaTxPwSubPortVlanCtrlReg, longReg, cThaLongRegMaxSize, core);
    ThaPktUtilRegUint32ToVlan(longReg[subPortVlanIndex], vlan);

    return cAtOk;
    }

static eAtRet ExpectedSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan)
    {
    uint32 longReg[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    uint32 regVal = ThaPktUtilVlanToUint32(expectedVlan);
    AtOsalMemInit(longReg, 0, sizeof(uint32) * cThaLongRegMaxSize);

    mModuleHwLongRead(self, cThaRxPwSubPortVlanCtrlReg, longReg, cThaLongRegMaxSize, core);
    longReg[subPortVlanIndex] = regVal;
    mModuleHwLongWrite(self, cThaRxPwSubPortVlanCtrlReg, longReg, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eAtRet ExpectedSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan)
    {
    uint32 longReg[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    AtOsalMemInit(longReg, 0, sizeof(uint32) * cThaLongRegMaxSize);

    mModuleHwLongRead(self, cThaRxPwSubPortVlanCtrlReg, longReg, cThaLongRegMaxSize, core);
    ThaPktUtilRegUint32ToVlan(longReg[subPortVlanIndex], expectedVlan);

    return cAtOk;
    }

static uint32 NumSubPortVlans(ThaModulePw self)
    {
    AtUnused(self);
    return 2UL;
    }

static eBool MBitDefectIsSupported(Tha60210031ModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet DefaultSet(AtModule self)
    {
    return Tha602900xxSubPortVlanDefaultTpidSet((ThaModulePw)self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static AtPwHdlc HdlcPwObjectCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    return Tha60290011PwDccNew(hdlcChannel, self);
    }

static eAtRet DeleteHdlcPwObject(AtModulePw self, AtPw pw)
    {
    return Tha602900xxModulePwDccHdlcPwObjectDelete(self, pw);
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtModuleMethods->Debug(self);
    ret |= ThaModulePwSubPortVlanDebug((ThaModulePw)self);

    return ret;
    }

static eBool CanBindEthPort(ThaModulePw self, AtPw pw, AtEthPort port)
    {
    AtUnused(pw);
    AtUnused(self);

    if (port)
        return Tha60290011ModuleEthIsPwPort(port);

    return cAtTrue;
    }

static ThaModulePwe PweModule(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static eAtRet TxPwSubPortVlanIndexSet(ThaModulePw self, AtPw adapter, uint32 subPortVlanIndex)
    {
    return Tha60290011ModulePwePwSubPortVlanIndexSet(PweModule(self), adapter, subPortVlanIndex);
    }

static uint32 TxPwSubPortVlanIndexGet(ThaModulePw self, AtPw adapter)
    {
    return Tha60290011ModulePwePwSubPortVlanIndexGet(PweModule(self), adapter);
    }

static eBool SubportVlansSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TxActiveForceSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePwRet CepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return m_AtModulePwMethods->CesRtpTimestampFrequencySet(self, khz);
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    return m_AtModulePwMethods->CesRtpTimestampFrequencyGet(self);
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return m_AtModulePwMethods->CesRtpTimestampFrequencySet(self, khz);
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
    return m_AtModulePwMethods->CesRtpTimestampFrequencyGet(self);
    }

static eBool ShouldHandleCircuitAisForcingOnEnabling(ThaModulePw self)
    {
    AtUnused(self);
    return cTha602900xxShouldHandleCircuitAisForcingOnEnabling;
    }

static eBool ShouldMaskDefectAndFailureOnPwDisabling(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    return Tha60290011KbyteDccInterruptManagerNew(self);
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 1;
    }

static ThaPwHeaderController BackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    /* Not here in this product */
    AtUnused(self);
    AtUnused(adapter);
    return NULL;
    }

static eBool DefaultCESoPPayloadSizeInBytes(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, StartVersionSupportHspw);
        mMethodOverride(m_ThaModulePwOverride, NumSubPortVlans);
        mMethodOverride(m_ThaModulePwOverride, ExpectedSubportVlanSet);
        mMethodOverride(m_ThaModulePwOverride, ExpectedSubportVlanGet);
        mMethodOverride(m_ThaModulePwOverride, TxSubportVlanGet);
        mMethodOverride(m_ThaModulePwOverride, TxSubportVlanSet);
        mMethodOverride(m_ThaModulePwOverride, TxPwSubPortVlanIndexSet);
        mMethodOverride(m_ThaModulePwOverride, TxPwSubPortVlanIndexGet);
        mMethodOverride(m_ThaModulePwOverride, CanBindEthPort);
        mMethodOverride(m_ThaModulePwOverride, SubportVlansSupported);
        mMethodOverride(m_ThaModulePwOverride, TxActiveForceSupported);
        mMethodOverride(m_ThaModulePwOverride, ShouldHandleCircuitAisForcingOnEnabling);
        mMethodOverride(m_ThaModulePwOverride, ShouldMaskDefectAndFailureOnPwDisabling);
        mMethodOverride(m_ThaModulePwOverride, BackupHeaderControllerObjectCreate);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideTha60210031ModulePw(AtModulePw self)
    {
    Tha60210031ModulePw module = (Tha60210031ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60210031ModulePwOverride));

        mMethodOverride(m_Tha60210031ModulePwOverride, MBitDefectIsSupported);
        }

    mMethodsSet(module, &m_Tha60210031ModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, HdlcPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, DeleteHdlcPwObject);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, DefaultCESoPPayloadSizeInBytes);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModule(self);
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60210031ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModulePw);
    }

AtModulePw Tha60290011ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60290011ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModulePwObjectInit(newModule, device);
    }

eBool Tha60290011ModulePwDccKbyteInterruptIsSupported(Tha60290011ModulePw self)
    {
    return Tha60290011DevicePwDccKbyteInterruptIsSupported(AtModuleDeviceGet((AtModule)self));
    }

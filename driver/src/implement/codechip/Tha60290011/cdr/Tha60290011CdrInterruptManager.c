/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290011ModuleCdrInterruptManager.c
 *
 * Created Date: Oct 8, 2017
 *
 * Description : CDR module interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pdh//Tha60290011ModulePdh.h"
#include "Tha60290011CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods   m_ThaCdrInterruptManagerOverride;

static const tThaCdrInterruptManagerMethods   *m_ThaCdrInterruptManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 1;
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtDevice device = AtModuleDeviceGet(cdrModule);
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    eTha60290011PdhInterfaceType intf = Tha60290011ModulePdhInterfaceTypeGet(modulePdh);

    if (intf == cTha60290011PdhInterfaceTypeDe3)
        return m_ThaCdrInterruptManagerMethods->AcrDcrControllerFromHwIdGet(self, slice, sts, vt);

    if (intf == cTha60290011PdhInterfaceTypeDe1)
        {
        uint8 vtgId = vt >> 2;
        uint8 vtId  = vt & 0x3;
        return ThaPdhDe1CdrControllerGet((ThaPdhDe1)Tha60290011ModulePdhHwIdToLiuDe1Object(modulePdh, sts, vtgId, vtId));
        }

    return NULL;
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrInterruptManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, m_ThaCdrInterruptManagerMethods, sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, AcrDcrControllerFromHwIdGet);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011CdrInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031CdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290011CdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }


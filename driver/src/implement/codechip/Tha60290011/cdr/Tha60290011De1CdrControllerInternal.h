/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290011De1CdrControllerInternal.h
 * 
 * Created Date: Aug 2, 2017
 *
 * Description : DE1 CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011DE1CDRCONTROLLERINTERNAL_H_
#define _THA60290011DE1CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/controllers/ThaVcDe1CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011VcDe1CdrController
    {
    tThaVcDe1CdrController super;
    }tTha60290011VcDe1CdrController;

typedef struct tTha60290011De1CdrController
    {
    tTha60290011VcDe1CdrController super;
    }tTha60290011De1CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha60290011VcDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011DE3CDRCONTROLLERINTERNAL_H_ */


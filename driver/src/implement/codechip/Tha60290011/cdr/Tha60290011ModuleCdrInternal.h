/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290011ModuleCdrInternal.h
 * 
 * Created Date: Aug 16, 2018
 *
 * Description : 60290011 module CDR internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULECDRINTERNAL_H_
#define _THA60290011MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/cdr/Tha60210031ModuleCdrInternal.h"
#include "Tha60290011ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleCdr *Tha60290011ModuleCdr;

typedef struct tTha60290011ModuleCdrMethods
    {
    eAtRet (*HwVtgPldDefaultSet)(ThaModuleCdr self, uint8 hwSts, uint8 vtgId, uint32 hwPayloadType);
    eAtRet (*HwStsPldDefaultSet)(ThaModuleCdr self, uint8 hwSts, uint8 hwStsMd, uint8 hwDe3Md);
    }tTha60290011ModuleCdrMethods;

typedef struct tTha60290011ModuleCdr
    {
    tTha60210031ModuleCdr super;
    const tTha60290011ModuleCdrMethods *methods;
    }tTha60290011ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290011ModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULECDRINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : INTERRUPT
 * 
 * File        : Tha60290031ModuleCdrInterruptManager.h
 * 
 * Created Date: Oct 8, 2017
 *
 * Description : CDR module interrupt manager for product 60290011.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011CDRINTERRUPTMANAGER_H_
#define _THA60290011CDRINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/ThaCdrInterruptManagerInternal.h"
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011CdrInterruptManager
    {
    tTha60210031CdrInterruptManager super;
    }tTha60290011CdrInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290011CdrInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011CDRINTERRUPTMANAGER_H_ */


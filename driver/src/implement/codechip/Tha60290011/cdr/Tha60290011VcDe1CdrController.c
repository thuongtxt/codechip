/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210031De3CdrController.c
 *
 * Created Date: Apr 24, 2015
 *
 * Description : DE3 CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "Tha60290011De1CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/* Super implement */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(self) ;

    uint32 regAddr = ThaCdrControllerCDREngineTimingCtrl(self) + mMethodsGet(self)->TimingCtrlOffset(self);
    uint32 regVal = mModuleHwRead(cdrModule, regAddr);
    mFieldIns(&regVal, cThaLineTypeMask, cThaLineTypeShift, AtPdhDe1IsE1(de1) ? 0x0 : 0x1);
    mModuleHwWrite(cdrModule, regAddr, regVal);

    return ThaCdrDe1TimingControlMapVtModeSet(cdrModule, (AtPdhChannel)de1);
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlVcDe1Offset(self);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011VcDe1CdrController);
    }

ThaCdrController Tha60290011VcDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcDe1CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60290011VcDe1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011VcDe1CdrControllerObjectInit(newController, engineId, channel);
    }

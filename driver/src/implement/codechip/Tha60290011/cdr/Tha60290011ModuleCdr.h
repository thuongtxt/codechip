/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290011ModuleCdr.h
 * 
 * Created Date: Oct 5, 2016
 *
 * Description : Tha60290011ModuleCdr declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULECDR_H_
#define _THA60290011MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/ThaModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60290011ModuleCdrDe1LineModeControlDefault(ThaModuleCdr self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULECDR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290011ModuleCdr.c
 *
 * Created Date: Oct 28, 2016
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/cdr/ThaModuleCdrStmReg.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../Tha60210031/cdr/Tha60210031CdrInterruptManager.h"
#include "../pdh/Tha60290011ModulePdh.h"
#include "Tha60290011ModuleCdr.h"
#include "Tha60290011CdrInterruptManager.h"
#include "Tha60290011ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290011ModuleCdr)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011ModuleCdrMethods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleCdrMethods m_ThaModuleCdrOverride;

/* Save super implementation */
static const tThaModuleCdrMethods * m_ThaModuleCdrMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LineModeCtrlOffset(uint8 hwStsId)
    {
    /* In one slice:
     * STS: 0,2,4,...,22  -> group = 0
     * STS: 1,3,5,...,23  -> group = 1
     */
    return (hwStsId % 2);
    }

static uint8 LocalHwStsInGroup12(uint8 hwSts)
    {
    return hwSts / 2;
    }

static eAtRet HwVtgPldDefaultSet(ThaModuleCdr self, uint8 hwSts, uint8 vtgId, uint32 hwPayloadType)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 hwStsLocalInGroup12 = LocalHwStsInGroup12(hwSts);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);

    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + LineModeCtrlOffset(hwSts);
    mModuleHwLongRead(self, regAddr, longReg, cThaLongRegMaxSize, core);
    mFieldIns(&longReg[hwStsLocalInGroup12 / 4],
              cThaStsVtTypeMask(hwStsLocalInGroup12, vtgId),
              cThaStsVtTypeShift(hwStsLocalInGroup12, vtgId),
              hwPayloadType);
    mModuleHwLongWrite(self, regAddr, longReg, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eAtRet HwStsPldDefaultSet(ThaModuleCdr self,
                     uint8 hwSts,
                     uint8 hwStsMd,
                     uint8 hwDe3Md)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 hwStsLocalInGroup12 = LocalHwStsInGroup12(hwSts);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);

    /* There are 2 line mode control registers in each slice.
     * One for 12 odd STSs and one for 12 even STSs
     */
    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + LineModeCtrlOffset(hwSts);

    mModuleHwLongRead(self, regAddr, longReg, cThaLongRegMaxSize, core);
    mFieldIns(&longReg[hwStsLocalInGroup12 / 4],
              cThaStsStsMdMask(hwStsLocalInGroup12),
              cThaStsStsMdShift(hwStsLocalInGroup12),
              hwStsMd);
    mFieldIns(&longReg[3],
              cThaStsDE3ModMask(hwStsLocalInGroup12),
              cThaStsDE3ModShift(hwStsLocalInGroup12),
              hwDe3Md);
    mModuleHwLongWrite(self, regAddr, longReg, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eAtRet HwStsVtgCdrLineDefaultSet(ThaModuleCdr self, uint8 hwSts, uint32 hwVtMd)
    {
    uint8 vtgId;
    eAtRet ret = cAtOk;
    const uint32 cMaxVtgNum = 7;
    const uint8 cHwStsModeDe1Vt = 0;
    const uint8 cHwDe3ModeDs3 = 1;

    mMethodsGet(mThis(self))->HwStsPldDefaultSet(self, hwSts, cHwStsModeDe1Vt, cHwDe3ModeDs3);
    for (vtgId = 0; vtgId < cMaxVtgNum; vtgId++)
        ret |= mMethodsGet(mThis(self))->HwVtgPldDefaultSet(self, hwSts, vtgId, hwVtMd);

    return ret;
    }

static void LiuAllDs1LineModeControlDefaultAssign(ThaModuleCdr self)
    {
    const uint8 cHwDe1VtModeDs1Vt1_5 = 1;
    uint32 sts;
    uint32 startSts = cTha60290011StartDs1StsHwId;
    uint32 maxSts   = startSts + cTha60290011NumDs1HwSts;

    for (sts = startSts; sts < maxSts; sts++)
        HwStsVtgCdrLineDefaultSet(self, (uint8)sts, cHwDe1VtModeDs1Vt1_5);
    }

static void LiuAllE1LineModeControlDefaultAssign(ThaModuleCdr self)
    {
    const uint8 cHwDe1VtModeE1Vt2 = 0;
    uint32 sts;
    uint32 startSts = cTha60290011StartE1StsHwId;
    uint32 maxSts   = startSts + cTha60290011NumE1HwSts;

    for (sts = startSts; sts < maxSts; sts++)
        HwStsVtgCdrLineDefaultSet(self, (uint8)sts, cHwDe1VtModeE1Vt2);
    }

static uint32 DefaultDcrClockFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 19440;
    }

static uint32 DefaultExtFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 9720UL;
    }

static eAtRet DefaultSet(ThaModuleCdr self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 ext1FrequencyDefault, regAddr;
    AtIpCore core;

    eAtRet ret = m_ThaModuleCdrMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    ext1FrequencyDefault = mMethodsGet(self)->DefaultExtFrequency(self);
    regAddr = mMethodsGet(self)->ExtRefAndPrcCtrlRegister(self);
    core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cThaDcrExt2N2k, ext1FrequencyDefault);
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    return Tha60290011CdrInterruptManagerNew(self);
    }

static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    mVersionReset(self);
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaCdrController VcDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    if (AtPdhChannelHasLineLayer((AtPdhChannel)de1))
        return Tha60290011De1CdrControllerNew(ThaModuleCdrStmEngineIdOfDe1(self, de1), (AtChannel)de1);

    return Tha60290011VcDe1CdrControllerNew(ThaModuleCdrStmEngineIdOfDe1(self, de1), (AtChannel)de1);
    }

static ThaCdrController De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    return Tha60290011De1CdrControllerNew(ThaModuleCdrStmEngineIdOfDe1(self, de1), (AtChannel)de1);
    }

static uint32 TimingCtrlVcDe1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->ChannelDefaultOffset(controller);
    }

static eBool SdhChannelIsFakeVc(AtSdhChannel vc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(vc);
    if ((vcType != cAtSdhChannelTypeVc12) && (vcType != cAtSdhChannelTypeVc11))
        return cAtFalse;

    /* Abstract VC does not map to any upper layer */
    if (AtSdhChannelParentChannelGet(vc))
        return cAtFalse;

    return cAtTrue;
    }

static eBool Vc1xCdrControllerShouldInitAfterCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    if (SdhChannelIsFakeVc((AtSdhChannel)vc))
        return cAtFalse;

    return m_ThaModuleCdrMethods->Vc1xCdrControllerShouldInitAfterCreate(self, vc);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultExtFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultDcrClockFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultSet);
        mMethodOverride(m_ThaModuleCdrOverride, ShouldEnableJitterAttenuator);
        mMethodOverride(m_ThaModuleCdrOverride, VcDe1CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, De1CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlVcDe1Offset);
        mMethodOverride(m_ThaModuleCdrOverride, Vc1xCdrControllerShouldInitAfterCreate);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void MethodsInit(AtModule self)
    {

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwVtgPldDefaultSet);
        mMethodOverride(m_methods, HwStsPldDefaultSet);
        }

    mMethodsSet((Tha60290011ModuleCdr)self, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleCdr);
    }

AtModule Tha60290011ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290011ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleCdrObjectInit(newModule, device);
    }

void Tha60290011ModuleCdrDe1LineModeControlDefault(ThaModuleCdr self)
    {
    LiuAllDs1LineModeControlDefaultAssign(self);
    LiuAllE1LineModeControlDefaultAssign(self);
    }

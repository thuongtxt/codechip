/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011SgmiiEthPort.c
 *
 * Created Date: Aug 10, 2016
 *
 * Description : SGMII ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../src/generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "Tha60290011ClsInterfaceEthPortInternal.h"
#include "Tha60290011ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011DimSgmiiEthPort
    {
    tTha60290011ClsInterfaceEthPort super;
    }tTha60290011DimSgmiiEthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                       m_AtObjectOverride;
static tAtChannelMethods                      m_AtChannelOverride;
static tAtEthPortMethods                      m_AtEthPortOverride;
static tTha60290011ClsInterfaceEthPortMethods m_Tha60290011ClsInterfaceEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed1000M) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed1000M) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed1000M;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    return (interface == cAtEthPortInterfaceSgmii) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceSgmii;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    uint8 localId = Tha60290011ModuleEthLocalSgmiiPort(self);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);
    return Tha60290011SerdesManagerSgmiiSerdesController(serdesManager, localId);
    }

static ThaModulePmc PmcModule(AtChannel self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return ThaModulePmcEthPortCounterIsSupported(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortTypeDimCtrlPort, counterType);

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static uint32 PmcCounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    return ThaModulePmcEthPortCountersGet(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortTypeDimCtrlPort, counterType, read2Clear);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 ClsBaseAddress(ThaEthPort self)
    {
    AtUnused(self);
    return 0x490000;
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)self;
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[64];

    AtSprintf(str, "%ssgmii_dimctrl", AtDeviceIdToString(dev));
    return str;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290011ClsInterfaceEthPort(AtEthPort self)
    {
    Tha60290011ClsInterfaceEthPort ethPort = (Tha60290011ClsInterfaceEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ClsInterfaceEthPortOverride, mMethodsGet(ethPort), sizeof(m_Tha60290011ClsInterfaceEthPortOverride));

        mMethodOverride(m_Tha60290011ClsInterfaceEthPortOverride, ClsBaseAddress);
        }

    mMethodsSet(ethPort, &m_Tha60290011ClsInterfaceEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290011ClsInterfaceEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011DimSgmiiEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ClsInterfaceEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290011DimSgmiiEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

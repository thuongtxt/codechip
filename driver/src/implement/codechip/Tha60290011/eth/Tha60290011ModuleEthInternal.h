/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth
 *
 * File        : Tha60290011ModuleEthInternal.h
 *
 * Created Date: Nov 21, 2016 
 *
 * Description : 
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEETHINTERNAL_H_
#define _THA60290011MODULEETHINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/eth/Tha60210031ModuleEth.h"
#include "Tha60290011ModuleEth.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleEthMethods
    {
    AtEthPort (*Axi4PortObjectCreate)(Tha60290011ModuleEth self, uint8 portId);
    AtEthPort (*SerdesBackplaneEthPortObjectCreate)(Tha60290011ModuleEth self, uint8 portId);
    }tTha60290011ModuleEthMethods;

typedef struct tTha60290011ModuleEth
    {
    tTha60210031ModuleEth super;
    const tTha60290011ModuleEthMethods *methods;

    AtMdio sgmiiMdio;
    }tTha60290011ModuleEth;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60290011ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011MODULEETHINTERNAL_H_ */

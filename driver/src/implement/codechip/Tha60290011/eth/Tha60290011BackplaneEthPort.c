/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011BacklaneEthPort.c
 *
 * Created Date: Aug 10, 2016
 *
 * Description : Backplane ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPort10GbReg.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "../man/Tha60290011DeviceReg.h"
#include "Tha60290011EthPortInternal.h"
#include "Tha60290011ModuleEth.h"
#include "../man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaGlbRegGeModeMask    (cBit26)
#define cThaGlbRegGeModeShift   (26)

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self) (mMethodsGet((Tha60210031EthPort)self)->XgmiiDefaultOffset((Tha60210031EthPort)self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011BackplaneEthPort
    {
    tTha60290011EthPort super;
    }tTha60290011BackplaneEthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtChannelMethods          m_AtChannelOverride;
static tAtEthPortMethods          m_AtEthPortOverride;
static tThaEthPortMethods         m_ThaEthPortOverride;
static tTha60210031EthPortMethods m_Tha60210031EthPortOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtEthPort self)
    {
    return (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static AtSerdesManager SerdesManager(AtEthPort self)
    {
    return AtDeviceSerdesManagerGet(AtChannelDeviceGet((AtChannel)self));
    }

static AtEthPort SgmiiPortGet(AtModuleEth module)
    {
    return AtModuleEthPortGet(module, cTha60290011ModuleEthDccPortId);
    }

static AtEthPort AnySerdesMacGet(AtModuleEth module)
    {
    return AtModuleEthPortGet(module, cTha60290011ModuleEthSerdesBackPlanActivePortId);
    }

static eAtModuleEthRet AllBackplanesSerdesMacIpgSet(AtEthPort self, uint8 ipg, eAtModuleEthRet (*IpgSet)(AtEthPort self, uint8 ipg))
    {
    AtModuleEth ethModule = EthModule(self);
    eAtRet ret = cAtOk;

    ret |= IpgSet(AtModuleEthPortGet(ethModule, cTha60290011ModuleEthSerdesBackPlanActivePortId), ipg);
    ret |= IpgSet(AtModuleEthPortGet(ethModule, cTha60290011ModuleEthSerdesBackPlanStandbyPortId), ipg);

    return ret;
    }

static eBool IsDccPortUsed(AtModuleEth module)
    {
    return Tha60290011ModuleEthSohTransparentIsReady(module);
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    eAtRet ret = cAtOk;

    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        ret = m_AtEthPortMethods->TxIpgSet(self, txIpg);
    else
        ret = AllBackplanesSerdesMacIpgSet(self, txIpg, AtEthPortTxIpgSet);

    if (ret != cAtOk)
        return ret;

    if (!IsDccPortUsed(EthModule(self)))
        ret = AtEthPortTxIpgSet(SgmiiPortGet(EthModule(self)), txIpg);

    return ret;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        return m_AtEthPortMethods->TxIpgGet(self);

    return AtEthPortTxIpgGet(AnySerdesMacGet(EthModule(self)));
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    eAtRet ret = cAtOk;

    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        ret = m_AtEthPortMethods->RxIpgSet(self, rxIpg);
    else
        ret = AllBackplanesSerdesMacIpgSet(self, rxIpg, AtEthPortRxIpgSet);

    if (ret != cAtOk)
        return ret;

    if (!IsDccPortUsed(EthModule(self)))
        ret = AtEthPortRxIpgSet(SgmiiPortGet(EthModule(self)), rxIpg);

    return ret;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        return m_AtEthPortMethods->RxIpgGet(self);

    return AtEthPortRxIpgGet(AnySerdesMacGet(EthModule(self)));
    }

static eBool TxIpgIsConfigurable(AtEthPort self)
    {
    if (AtEthPortHasMacFunctionality(self))
        return cAtTrue;

    return AtEthPortTxIpgIsConfigurable(AnySerdesMacGet(EthModule(self)));
    }

static eBool RxIpgIsConfigurable(AtEthPort self)
    {
    if (AtEthPortHasMacFunctionality(self))
        return cAtTrue;

    return AtEthPortRxIpgIsConfigurable(AnySerdesMacGet(EthModule(self)));
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    uint32 regAddr = cThaRegEthMacAddr47_32;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    if (mRegField(regVal, cThaGlbRegGeMode))
        return cAtEthPortInterfaceSgmii;

    return cAtEthPortInterfaceXGMii;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    eAtEthPortInterface interface = AtEthPortInterfaceGet(self);

    if (interface == cAtEthPortInterfaceXGMii)
        return (speed == cAtEthPortSpeed10G) ? cAtTrue : cAtFalse;

    if (interface == cAtEthPortInterfaceSgmii)
        return (speed == cAtEthPortSpeed1000M) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    eAtEthPortInterface interface = AtEthPortInterfaceGet(self);

    AtUnused(self);
    if (interface == cAtEthPortInterfaceXGMii)
        return (speed == cAtEthPortSpeed10G) ? cAtOk : cAtErrorModeNotSupport;

    if (interface == cAtEthPortInterfaceSgmii)
        return (speed == cAtEthPortSpeed1000M) ? cAtOk : cAtErrorModeNotSupport;

    return cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    eAtEthPortInterface interface = AtEthPortInterfaceGet(self);

    if (interface == cAtEthPortInterfaceXGMii)
        return cAtEthPortSpeed10G;

    if (interface == cAtEthPortInterfaceSgmii)
        return cAtEthPortSpeed1000M;

    return cAtEthPortSpeedUnknown;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);
    return Tha60290011SerdesManagerBackplaneSerdesController(serdesManager, 0);
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    eAtRet ret = cAtOk;

    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        {
        ret = m_ThaEthPortMethods->MacActivate(self, activate);
        if (ret != cAtOk)
            return ret;
        }

    if (!IsDccPortUsed(EthModule((AtEthPort)self)))
        ThaEthPortMacActivate((ThaEthPort)SgmiiPortGet(EthModule((AtEthPort)self)), activate);

    return cAtOk;
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    eAtRet ret = cAtOk;

    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        {
        ret = m_ThaEthPortMethods->LoopInEnable(self, enable);
        if (ret != cAtOk)
            return ret;
        }

    if (!IsDccPortUsed(EthModule((AtEthPort)self)))
        {
        ThaEthPort dccPort = (ThaEthPort)SgmiiPortGet(EthModule((AtEthPort)self));
        mMethodsGet(dccPort)->LoopInEnable(dccPort, enable);
        }

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    eAtRet ret = m_ThaEthPortMethods->LoopOutEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (!IsDccPortUsed(EthModule((AtEthPort)self)))
        {
        ThaEthPort dccPort = (ThaEthPort)SgmiiPortGet(EthModule((AtEthPort)self));
        mMethodsGet(dccPort)->LoopOutEnable(dccPort, enable);
        }

    return cAtOk;
    }

static eBool LoopInIsEnabled(ThaEthPort self)
    {
    if (AtEthPortInterfaceGet((AtEthPort)self) == cAtEthPortInterfaceXGMii)
        return m_ThaEthPortMethods->LoopInIsEnabled(self);

    if (!IsDccPortUsed(EthModule((AtEthPort)self)))
        {
        ThaEthPort dccPort = (ThaEthPort)SgmiiPortGet(EthModule((AtEthPort)self));
        return mMethodsGet(dccPort)->LoopInIsEnabled(dccPort);
        }

    return cAtFalse;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    if (AtEthPortInterfaceGet((AtEthPort)self) == cAtEthPortInterfaceXGMii)
        return m_ThaEthPortMethods->LoopOutIsEnabled(self);

    if (!IsDccPortUsed(EthModule((AtEthPort)self)))
        {
        ThaEthPort dccPort = (ThaEthPort)SgmiiPortGet(EthModule((AtEthPort)self));
        return mMethodsGet(dccPort)->LoopOutIsEnabled(dccPort);
        }

    return cAtFalse;
    }

static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    if (AtEthPortInterfaceGet((AtEthPort)self) == cAtEthPortInterfaceSgmii)
        return 1000000; /* 1G */

    return m_ThaEthPortMethods->MaxBandwidthInKbps(self);
    }

static eBool InterfaceIsSupported(Tha60210031EthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    if ((interface == cAtEthPortInterfaceXGMii) || (interface == cAtEthPortInterfaceSgmii))
        return cAtTrue;

    return cAtFalse;
    }

static eBool EpEthPortInterfaceIsApplicable(Tha60210031EthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModulePmc PmcModule(AtChannel self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return ThaModulePmcEthPortCounterIsSupported(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortType10GPort, counterType);

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static uint32 PmcCounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    return ThaModulePmcEthPortCountersGet(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortType10GPort, counterType, read2Clear);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
	if (ThaEthPortShouldGetCounterFromPmc(self))
		return PmcCounterReadToClear(self, counterType, cAtTrue);

	return m_AtChannelMethods->CounterClear(self, counterType);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static eBool CanSelectNullSerdes(AtEthPort self)
    {
    /* Select a NULL SERDES to work base on FSM input signal */
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    ThaModuleEth module = (ThaModuleEth)EthModule(self);
    if (serdes == NULL)
        return ThaModuleEthRxFsmPinEnable(module, cAtTrue);

    if (Tha60290011SerdesManagerSerdesIsBackplane(SerdesManager(self), AtSerdesControllerIdGet(serdes)))
        return Tha60290011ModuleEthRxBackPlaneSerdesSelect(module, AtSerdesControllerIdGet(serdes));

    return cAtErrorInvlParm;
    }

static AtSerdesController RxSelectedSerdesOnConfiguration(AtEthPort self)
    {
    uint32 serdesId = Tha60290011ModuleEthRxBackPlaneSelectedSerdes((ThaModuleEth)EthModule(self));
    return Tha60290011SerdesManagerBackplaneSerdesController(SerdesManager(self), serdesId);
    }

static AtSerdesController RxSelectedSerdesOnRxFsmPinStatus(AtEthPort self)
    {
    eBool rxFsmPinStatus = ThaModuleEthRxFsmPinStatusGet((ThaModuleEth)EthModule(self));
    return Tha60290011SerdesManagerBackplaneSerdesController(SerdesManager(self), rxFsmPinStatus ? 1 : 0);
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    if (!ThaModuleEthRxFsmPinIsEnabled((ThaModuleEth)EthModule(self)))
        return RxSelectedSerdesOnConfiguration(self);

    return RxSelectedSerdesOnRxFsmPinStatus(self);
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    return Tha60290011SerdesManagerBackplaneSerdesController(SerdesManager(self), 1);
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)self;
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[64];

    AtSprintf(str, "%s10g_pw", AtDeviceIdToString(dev));
    return str;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtTrue;

    if (!AtEthPortHasMacFunctionality((AtEthPort)self))
        return cAtFalse;

    /* When DCC is used, SGMII loopback is done at SERDES */
    if ((AtEthPortInterfaceGet((AtEthPort)self) == cAtEthPortInterfaceSgmii) &&
        IsDccPortUsed((AtModuleEth)AtChannelModuleGet(self)))
        return cAtFalse;

    return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);
    }

static eBool HasMacFunctionality(AtEthPort self)
    {
    return Tha60290011DevicePwEthPortHasMacFunctionality(AtChannelDeviceGet((AtChannel)self));
    }

static eAtModuleEthRet HiGigEnable(AtEthPort self, eBool enable)
    {
    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        return m_AtEthPortMethods->HiGigEnable(self, enable);

    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
    if (AtEthPortHasMacFunctionality((AtEthPort)self))
        return m_AtEthPortMethods->HiGigIsEnabled(self);

    return cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (!AtEthPortHasMacFunctionality((AtEthPort)self))
        return 0;
    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (!AtEthPortHasMacFunctionality((AtEthPort)self))
        return 0;
    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (!AtEthPortHasMacFunctionality((AtEthPort)self))
        return 0;
    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static eAtRet Debug(AtChannel self)
    {
    ThaEthPortShowCurrentBandwidth((ThaEthPort)self);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Need to specify IPG, otherwise, warm restore test case will be fail
     * because of mismatching in bandwidth constrain database */
    if (AtChannelInAccessible(self))
        {
        AtEthPort port = (AtEthPort)self;
        if (AtEthPortTxIpgIsConfigurable(port))
            ret |= AtEthPortTxIpgSet(port, AtEthPortDefaultIpg(port));
        }

    return ret;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, CanSelectNullSerdes);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, HasMacFunctionality);
        mMethodOverride(m_AtEthPortOverride, TxIpgIsConfigurable);
        mMethodOverride(m_AtEthPortOverride, RxIpgIsConfigurable);
        mMethodOverride(m_AtEthPortOverride, HiGigEnable);
        mMethodOverride(m_AtEthPortOverride, HiGigIsEnabled);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, LoopInEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        mMethodOverride(m_ThaEthPortOverride, MaxBandwidthInKbps);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }

static void OverrideTha60210031EthPort(AtEthPort self)
    {
    Tha60210031EthPort port = (Tha60210031EthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031EthPortOverride, mMethodsGet(port), sizeof(m_Tha60210031EthPortOverride));

        mMethodOverride(m_Tha60210031EthPortOverride, InterfaceIsSupported);
        mMethodOverride(m_Tha60210031EthPortOverride, EpEthPortInterfaceIsApplicable);
        }

    mMethodsSet(port, &m_Tha60210031EthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    OverrideTha60210031EthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011BackplaneEthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290011BackplaneEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

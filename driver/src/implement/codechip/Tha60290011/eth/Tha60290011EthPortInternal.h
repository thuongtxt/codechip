/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290011EthPortInternal.h
 * 
 * Created Date: Nov 9, 2016
 *
 * Description : ETH port for 60290011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011ETHPORTINTERNAL_H_
#define _THA60290011ETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/eth/Tha60210031EthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011EthPort
    {
    tTha60210031EthPort super;
    }tTha60290011EthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290011EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011ETHPORTINTERNAL_H_ */


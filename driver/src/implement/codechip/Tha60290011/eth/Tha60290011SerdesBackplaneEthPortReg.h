/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_ETH10G_RD_H_
#define _AF6_REG_AF6CNC0011_ETH10G_RD_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDRP
Reg Addr   : 0x1000-0x1FFF
Reg Formula: 0x1000+$P*0x400+$DRP
    Where  : 
           + $P(0-3) : Lane ID
           + $DRP(0-1023) : DRP address, see UG578
Reg Desc   : 
Read/Write DRP address of SERDES

------------------------------------------------------------------------------*/
#define cAf6Reg_OETH_10G_DRP_Base                                                                       0x1000

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_OETH_10G_DRP_drp_rw_Mask                                                                  cBit9_0
#define cAf6_OETH_10G_DRP_drp_rw_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GLoopBack
Reg Addr   : 0x0002
Reg Formula: 
    Where  : 
Reg Desc   : 
Configurate LoopBack

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_LoopBack_Base                                                                   0x0002

/*--------------------------------------
BitField Name: lpback_lane0
BitField Type: R/W
BitField Desc: Loopback lane0
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_10G_LoopBack_lpback_lane0_Mask                                                        cBit3_0
#define cAf6_ETH_10G_LoopBack_lpback_lane0_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GQLL Status
Reg Addr   : 0x000B
Reg Formula: 
    Where  : 
Reg Desc   : 
QPLL status

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_QLL_Status_Base                                                                 0x000B

/*--------------------------------------
BitField Name: QPLL1_Lock_change
BitField Type: W1C
BitField Desc: QPLL1 has transition lock/unlock, Group 0-3
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_10G_QLL_Status_QPLL1_Lock_change_Mask                                                  cBit29
#define cAf6_ETH_10G_QLL_Status_QPLL1_Lock_change_Shift                                                     29

/*--------------------------------------
BitField Name: QPLL0_Lock_change
BitField Type: W1C
BitField Desc: QPLL0 has transition lock/unlock, Group 0-3
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_10G_QLL_Status_QPLL0_Lock_change_Mask                                                  cBit28
#define cAf6_ETH_10G_QLL_Status_QPLL0_Lock_change_Shift                                                     28

/*--------------------------------------
BitField Name: QPLL1_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-3
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_ETH_10G_QLL_Status_QPLL1_Lock_Mask                                                         cBit25
#define cAf6_ETH_10G_QLL_Status_QPLL1_Lock_Shift                                                            25

/*--------------------------------------
BitField Name: QPLL0_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-3
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_10G_QLL_Status_QPLL0_Lock_Mask                                                         cBit24
#define cAf6_ETH_10G_QLL_Status_QPLL0_Lock_Shift                                                            24


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTX Reset
Reg Addr   : 0x000C
Reg Formula: 
    Where  : 
Reg Desc   : 
Reset TX SERDES

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TX_Reset_Base                                                                   0x000C

/*--------------------------------------
BitField Name: txrst_done
BitField Type: W1C
BitField Desc: TX Reset Done
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_10G_TX_Reset_txrst_done_Mask                                                           cBit16
#define cAf6_ETH_10G_TX_Reset_txrst_done_Shift                                                              16

/*--------------------------------------
BitField Name: txrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset TX SERDES
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_TX_Reset_txrst_trig_Mask                                                            cBit0
#define cAf6_ETH_10G_TX_Reset_txrst_trig_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GRX Reset
Reg Addr   : 0x000D
Reg Formula: 
    Where  : 
Reg Desc   : 
Reset RX SERDES

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_49G_RX_Reset_Base                                                                   0x000D

/*--------------------------------------
BitField Name: rxrst_done
BitField Type: W1C
BitField Desc: RX Reset Done
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_49G_RX_Reset_rxrst_done_Mask                                                           cBit16
#define cAf6_ETH_49G_RX_Reset_rxrst_done_Shift                                                              16

/*--------------------------------------
BitField Name: rxrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset RX SERDES
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_49G_RX_Reset_rxrst_trig_Mask                                                            cBit0
#define cAf6_ETH_49G_RX_Reset_rxrst_trig_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GLPMDFE Mode
Reg Addr   : 0x000E
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure LPM/DFE mode

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_LPMDFE_Mode_Base                                                                0x000E

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: LPM/DFE mode
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_LPMDFE_Mode_lpmdfe_mode_Mask                                                        cBit0
#define cAf6_ETH_10G_LPMDFE_Mode_lpmdfe_mode_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GLPMDFE Reset
Reg Addr   : 0x000F
Reg Formula: 
    Where  : 
Reg Desc   : 
Reset LPM/DFE

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_LPMDFE_Reset_Base                                                               0x000F

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: LPM/DFE reset
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_LPMDFE_Reset_lpmdfe_reset_Mask                                                      cBit0
#define cAf6_ETH_10G_LPMDFE_Reset_lpmdfe_reset_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTXDIFFCTRL
Reg Addr   : 0x0010
Reg Formula: 
    Where  : 
Reg Desc   : 
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TXDIFFCTRL_Base                                                                 0x0010

/*--------------------------------------
BitField Name: TXDIFFCTRL
BitField Type: R/W
BitField Desc: TXDIFFCTRL
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ETH_10G_TXDIFFCTRL_TXDIFFCTRL_Mask                                                        cBit4_0
#define cAf6_ETH_10G_TXDIFFCTRL_TXDIFFCTRL_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTXPOSTCURSOR
Reg Addr   : 0x0011
Reg Formula: 
    Where  : 
Reg Desc   : 
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TXPOSTCURSOR_Base                                                               0x0011

/*--------------------------------------
BitField Name: TXPOSTCURSOR
BitField Type: R/W
BitField Desc: TXPOSTCURSOR
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ETH_10G_TXPOSTCURSOR_TXPOSTCURSOR_Mask                                                    cBit4_0
#define cAf6_ETH_10G_TXPOSTCURSOR_TXPOSTCURSOR_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTXPRECURSOR
Reg Addr   : 0x0012
Reg Formula: 
    Where  : 
Reg Desc   : 
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TXPRECURSOR_Base                                                                0x0012

/*--------------------------------------
BitField Name: TXPRECURSOR
BitField Type: R/W
BitField Desc: TXPRECURSOR
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ETH_10G_TXPRECURSOR_TXPRECURSOR_Mask                                                      cBit4_0
#define cAf6_ETH_10G_TXPRECURSOR_TXPRECURSOR_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GCtrl FCS
Reg Addr   : 0x0080
Reg Formula: 
    Where  : 
Reg Desc   : 
configure FCS mode

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Ctrl_FCS_Base                                                                   0x0080

/*--------------------------------------
BitField Name: txfcs_ignore
BitField Type: R/W
BitField Desc: TX ignore check FCS when txfcs_ins is low
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_Ctrl_FCS_txfcs_ignore_Mask                                                          cBit5
#define cAf6_ETH_10G_Ctrl_FCS_txfcs_ignore_Shift                                                             5

/*--------------------------------------
BitField Name: txfcs_ins
BitField Type: R/W
BitField Desc: TX inserts 4bytes FCS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_Ctrl_FCS_txfcs_ins_Mask                                                             cBit4
#define cAf6_ETH_10G_Ctrl_FCS_txfcs_ins_Shift                                                                4

/*--------------------------------------
BitField Name: rxfcs_ignore
BitField Type: R/W
BitField Desc: RX ignore check FCS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_Ctrl_FCS_rxfcs_ignore_Mask                                                          cBit1
#define cAf6_ETH_10G_Ctrl_FCS_rxfcs_ignore_Shift                                                             1

/*--------------------------------------
BitField Name: rxfcs_rmv
BitField Type: R/W
BitField Desc: RX remove 4bytes FCS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_Ctrl_FCS_rxfcs_rmv_Mask                                                             cBit0
#define cAf6_ETH_10G_Ctrl_FCS_rxfcs_rmv_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GAutoNeg
Reg Addr   : 0x0081
Reg Formula: 
    Where  : 
Reg Desc   : 
configure Auto-Neg

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_AutoNeg_Base                                                                    0x0081

/*--------------------------------------
BitField Name: fec_rx_enb
BitField Type: R/W
BitField Desc: FEC RX enable
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_fec_rx_enb_Mask                                                            cBit29
#define cAf6_ETH_10G_AutoNeg_fec_rx_enb_Shift                                                               29

/*--------------------------------------
BitField Name: fec_tx_enb
BitField Type: R/W
BitField Desc: FEC TX enable
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_fec_tx_enb_Mask                                                            cBit28
#define cAf6_ETH_10G_AutoNeg_fec_tx_enb_Shift                                                               28

/*--------------------------------------
BitField Name: an_pseudo_sel
BitField Type: R/W
BitField Desc: Selects the polynomial generator for the bit 49 random bit
generator
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_an_pseudo_sel_Mask                                                         cBit24
#define cAf6_ETH_10G_AutoNeg_an_pseudo_sel_Shift                                                            24

/*--------------------------------------
BitField Name: an_nonce_seed
BitField Type: R/W
BitField Desc: 8-bit seed to initialize the nonce field polynomial generator.
Non-zero.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_an_nonce_seed_Mask                                                      cBit23_16
#define cAf6_ETH_10G_AutoNeg_an_nonce_seed_Shift                                                            16

/*--------------------------------------
BitField Name: an_lt_sta
BitField Type: R/W
BitField Desc: Link Control outputs from the auto-negotiationcontroller for the
various Ethernet protocols.
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_an_lt_sta_Mask                                                            cBit9_8
#define cAf6_ETH_10G_AutoNeg_an_lt_sta_Shift                                                                 8

/*--------------------------------------
BitField Name: lt_restart
BitField Type: R/W
BitField Desc: This signal triggers a restart of link training regardless of the
current state.
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_lt_restart_Mask                                                             cBit5
#define cAf6_ETH_10G_AutoNeg_lt_restart_Shift                                                                5

/*--------------------------------------
BitField Name: lt_enb
BitField Type: R/W
BitField Desc: Enables link training. When link training is disabled, all PCS
lanes function in mission mode.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_lt_enb_Mask                                                                 cBit4
#define cAf6_ETH_10G_AutoNeg_lt_enb_Shift                                                                    4

/*--------------------------------------
BitField Name: an_restart
BitField Type: R/W
BitField Desc: This input is used to trigger a restart of the auto-negotiation,
regardless of what state the circuit is currently in.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_an_restart_Mask                                                             cBit2
#define cAf6_ETH_10G_AutoNeg_an_restart_Shift                                                                2

/*--------------------------------------
BitField Name: an_bypass
BitField Type: R/W
BitField Desc: Input to disable auto-negotiation and bypass the auto-negotiation
function. If this input is asserted, auto-negotiation is turned off, but the PCS
is connected to the output to allow operation.
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_an_bypass_Mask                                                              cBit1
#define cAf6_ETH_10G_AutoNeg_an_bypass_Shift                                                                 1

/*--------------------------------------
BitField Name: an_enb
BitField Type: R/W
BitField Desc: Enable signal for auto-negotiation
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_AutoNeg_an_enb_Mask                                                                 cBit0
#define cAf6_ETH_10G_AutoNeg_an_enb_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl0
Reg Addr   : 0x0020
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 0

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl0_Base                                                                 0x0020

/*--------------------------------------
BitField Name: diag_err
BitField Type: W1C
BitField Desc: Error detection
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl0_diag_err_Mask                                                           cBit24
#define cAf6_ETH_10G_Diag_ctrl0_diag_err_Shift                                                              24

/*--------------------------------------
BitField Name: diag_ferr
BitField Type: R/W
BitField Desc: Enable force error data of diagnostic packet
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl0_diag_ferr_Mask                                                          cBit20
#define cAf6_ETH_10G_Diag_ctrl0_diag_ferr_Shift                                                             20

/*--------------------------------------
BitField Name: diag_datmod
BitField Type: R/W
BitField Desc: payload mod of ethernet frame
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl0_diag_datmod_Mask                                                       cBit5_4
#define cAf6_ETH_10G_Diag_ctrl0_diag_datmod_Shift                                                            4

/*--------------------------------------
BitField Name: diag_enb
BitField Type: R/W
BitField Desc: enable diagnostic block
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl0_diag_enb_Mask                                                            cBit0
#define cAf6_ETH_10G_Diag_ctrl0_diag_enb_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl1
Reg Addr   : 0x0021
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 1

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl1_Base                                                                 0x0021

/*--------------------------------------
BitField Name: diag_lenmax
BitField Type: R/W
BitField Desc: Maximum length of diagnostic packet, count from 0, min value is
63
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl1_diag_lenmax_Mask                                                     cBit31_16
#define cAf6_ETH_10G_Diag_ctrl1_diag_lenmax_Shift                                                           16

/*--------------------------------------
BitField Name: diag_lenmin
BitField Type: R/W
BitField Desc: Minimum length of diagnostic packet, count from 0, min value is
63
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl1_diag_lenmin_Mask                                                      cBit15_0
#define cAf6_ETH_10G_Diag_ctrl1_diag_lenmin_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl2
Reg Addr   : 0x0022
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 2

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl2_Base                                                                 0x0022

/*--------------------------------------
BitField Name: diag_dalsb
BitField Type: R/W
BitField Desc: 32bit-LSB DA of diagnostic packet
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl2_diag_dalsb_Mask                                                       cBit31_0
#define cAf6_ETH_10G_Diag_ctrl2_diag_dalsb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl3
Reg Addr   : 0x0023
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 3

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl3_Base                                                                 0x0023

/*--------------------------------------
BitField Name: diag_salsb
BitField Type: R/W
BitField Desc: 32bit-LSB SA of diagnostic packet
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl3_diag_salsb_Mask                                                       cBit31_0
#define cAf6_ETH_10G_Diag_ctrl3_diag_salsb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl4
Reg Addr   : 0x0024
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 4

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl4_Base                                                                 0x0024

/*--------------------------------------
BitField Name: diag_damsb
BitField Type: R/W
BitField Desc: 16bit-MSB DA of diagnostic packet
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl4_diag_damsb_Mask                                                      cBit31_16
#define cAf6_ETH_10G_Diag_ctrl4_diag_damsb_Shift                                                            16

/*--------------------------------------
BitField Name: diag_samsb
BitField Type: R/W
BitField Desc: 16bit-MSB SA of diagnostic packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl4_diag_samsb_Mask                                                       cBit15_0
#define cAf6_ETH_10G_Diag_ctrl4_diag_samsb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl6
Reg Addr   : 0x0026
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 7

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl6_Base                                                                 0x0026
#define cAf6Reg_ETH_10G_Diag_ctrl6_WidthVal                                                                 32

/*--------------------------------------
BitField Name: enb_type
BitField Type: RW
BitField Desc: Config enable insert type from CPU, (1) is enable, (0) is disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl6_enb_type_Mask                                                           cBit17
#define cAf6_ETH_10G_Diag_ctrl6_enb_type_Shift                                                              17

/*--------------------------------------
BitField Name: type_cfg
BitField Type: RW
BitField Desc: value type that is configured
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl6_type_cfg_Mask                                                         cBit16_0
#define cAf6_ETH_10G_Diag_ctrl6_type_cfg_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Ctrl7
Reg Addr   : 0x0027
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic control 7

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_ctrl7_Base                                                                 0x0027

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:19]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl7_Unsed_Mask                                                           cBit31_19
#define cAf6_ETH_10G_Diag_ctrl7_Unsed_Shift                                                                 19

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic 1:Running 0:Done-Ready
BitField Bits: [18]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl7_status_gatetime_diag_Mask                                               cBit18
#define cAf6_ETH_10G_Diag_ctrl7_status_gatetime_diag_Shift                                                  18

/*--------------------------------------
BitField Name: start_gatetime_diag
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl7_start_gatetime_diag_Mask                                                cBit17
#define cAf6_ETH_10G_Diag_ctrl7_start_gatetime_diag_Shift                                                   17

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_ctrl7_time_cfg_Mask                                                         cBit16_0
#define cAf6_ETH_10G_Diag_ctrl7_time_cfg_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Gatetime Current
Reg Addr   : 0x28
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Gatetime_current_Base                                                                     0x28

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:17]
--------------------------------------*/
#define cAf6_Gatetime_current_Unsed_Mask                                                             cBit31_17
#define cAf6_Gatetime_current_Unsed_Shift                                                                   17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_Gatetime_current_currert_gatetime_diag_Mask                                              cBit16_0
#define cAf6_Gatetime_current_currert_gatetime_diag_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag Sta0
Reg Addr   : 0x0040
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic Sta0

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_Sta0_Base                                                                  0x0040

/*--------------------------------------
BitField Name: diag_txmis_sop
BitField Type: W1C
BitField Desc: Packet miss SOP
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_txmis_sop_Mask                                                       cBit7
#define cAf6_ETH_10G_Diag_Sta0_diag_txmis_sop_Shift                                                          7

/*--------------------------------------
BitField Name: diag_txmis_eop
BitField Type: W1C
BitField Desc: Packet miss EOP
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_txmis_eop_Mask                                                       cBit6
#define cAf6_ETH_10G_Diag_Sta0_diag_txmis_eop_Shift                                                          6

/*--------------------------------------
BitField Name: diag_txsop_eop
BitField Type: W1C
BitField Desc: Short packet, length is less than 16bytes
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_txsop_eop_Mask                                                       cBit5
#define cAf6_ETH_10G_Diag_Sta0_diag_txsop_eop_Shift                                                          5

/*--------------------------------------
BitField Name: diag_txwff_ful
BitField Type: W1C
BitField Desc: TX-Fifo is full
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_txwff_ful_Mask                                                       cBit4
#define cAf6_ETH_10G_Diag_Sta0_diag_txwff_ful_Shift                                                          4

/*--------------------------------------
BitField Name: diag_rxmis_sop
BitField Type: W1C
BitField Desc: Packet miss SOP
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_rxmis_sop_Mask                                                       cBit3
#define cAf6_ETH_10G_Diag_Sta0_diag_rxmis_sop_Shift                                                          3

/*--------------------------------------
BitField Name: diag_rxmis_eop
BitField Type: W1C
BitField Desc: Packet miss EOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_rxmis_eop_Mask                                                       cBit2
#define cAf6_ETH_10G_Diag_Sta0_diag_rxmis_eop_Shift                                                          2

/*--------------------------------------
BitField Name: diag_rxsop_eop
BitField Type: W1C
BitField Desc: Short packet, length is less than 16bytes  t
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_rxsop_eop_Mask                                                       cBit1
#define cAf6_ETH_10G_Diag_Sta0_diag_rxsop_eop_Shift                                                          1

/*--------------------------------------
BitField Name: diag_rxwff_ful
BitField Type: W1C
BitField Desc: RX-Fifo is full
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_Sta0_diag_rxwff_ful_Mask                                                       cBit0
#define cAf6_ETH_10G_Diag_Sta0_diag_rxwff_ful_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag TXPKT
Reg Addr   : 0x0042
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic TX packet counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_TXPKT_Base                                                                 0x0042

/*--------------------------------------
BitField Name: diag_txpkt
BitField Type: R2C
BitField Desc: TX packet counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_TXPKT_diag_txpkt_Mask                                                       cBit31_0
#define cAf6_ETH_10G_Diag_TXPKT_diag_txpkt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag TXNOB
Reg Addr   : 0x0043
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic TX number of bytes counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_TXNOB_Base                                                                 0x0043

/*--------------------------------------
BitField Name: diag_txnob
BitField Type: R2C
BitField Desc: TX number of byte counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_TXNOB_diag_txnob_Mask                                                       cBit31_0
#define cAf6_ETH_10G_Diag_TXNOB_diag_txnob_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag RXPKT
Reg Addr   : 0x0044
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic RX packet counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_RXPKT_Base                                                                 0x0044

/*--------------------------------------
BitField Name: diag_rxpkt
BitField Type: R2C
BitField Desc: RX packet counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_RXPKT_diag_rxpkt_Mask                                                       cBit31_0
#define cAf6_ETH_10G_Diag_RXPKT_diag_rxpkt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GDiag RXNOB
Reg Addr   : 0x0045
Reg Formula: 
    Where  : 
Reg Desc   : 
Diagnostic RX number of bytes counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Diag_RXNOB_Base                                                                 0x0045

/*--------------------------------------
BitField Name: diag_rxnob
BitField Type: R2C
BitField Desc: RX number of byte counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Diag_RXNOB_diag_rxnob_Mask                                                       cBit31_0
#define cAf6_ETH_10G_Diag_RXNOB_diag_rxnob_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTX CFG
Reg Addr   : 0x2101
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TX_CFG_Base                                                                     0x2101

/*--------------------------------------
BitField Name: cfg_sel_tick
BitField Type: RW
BitField Desc: configure select pm_tick from CPU configure (tick_reg) or from
signal pm_tick, (1) from signal pm_tick, (0) from CPU
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_10G_TX_CFG_cfg_sel_tick_Mask                                                            cBit8
#define cAf6_ETH_10G_TX_CFG_cfg_sel_tick_Shift                                                               8

/*--------------------------------------
BitField Name: upactive
BitField Type: RW
BitField Desc: enable active,
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_TX_CFG_upactive_Mask                                                                cBit5
#define cAf6_ETH_10G_TX_CFG_upactive_Shift                                                                   5

/*--------------------------------------
BitField Name: cfg_txen
BitField Type: RW
BitField Desc: enable transmit side,
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_TX_CFG_cfg_txen_Mask                                                                cBit4
#define cAf6_ETH_10G_TX_CFG_cfg_txen_Shift                                                                   4

/*--------------------------------------
BitField Name: ipg_cfg
BitField Type: RW
BitField Desc: configure Tx IPG
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_10G_TX_CFG_ipg_cfg_Mask                                                               cBit3_0
#define cAf6_ETH_10G_TX_CFG_ipg_cfg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GRX CFG
Reg Addr   : 0x2102
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_RX_CFG_Base                                                                     0x2102

/*--------------------------------------
BitField Name: cfg_rxen
BitField Type: RW
BitField Desc: enable receive side,
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_10G_RX_CFG_cfg_rxen_Mask                                                               cBit24
#define cAf6_ETH_10G_RX_CFG_cfg_rxen_Shift                                                                  24

/*--------------------------------------
BitField Name: cfg_minlen
BitField Type: configure Rx MTU, min len packet receive
BitField Desc:
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ETH_10G_RX_CFG_cfg_minlen_Mask                                                          cBit7_0
#define cAf6_ETH_10G_RX_CFG_cfg_minlen_Shift                                                               0

/*--------------------------------------
BitField Name: cfg_maxlen
BitField Type: RW
BitField Desc: configure Rx MTU, max len packet receive
BitField Bits: [22:8]
--------------------------------------*/
#define cAf6_ETH_10G_RX_CFG_cfg_maxlen_Mask                                                           cBit22_8
#define cAf6_ETH_10G_RX_CFG_cfg_maxlen_Shift                                                                 8


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GCFG TICK REG
Reg Addr   : 0x2109
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_CFG_TICK_REG_Base                                                               0x2109
#define cAf6Reg_ETH_10G_CFG_TICK_REG_WidthVal                                                               32

/*--------------------------------------
BitField Name: tick_reg
BitField Type: RW
BitField Desc: write value "1" for tick, auto low (value "0")
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_CFG_TICK_REG_tick_reg_Mask                                                          cBit0
#define cAf6_ETH_10G_CFG_TICK_REG_tick_reg_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTX STICKY
Reg Addr   : 0x2110
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TX_STICKY_Base                                                                  0x2110
#define cAf6Reg_ETH_10G_TX_STICKY_WidthVal                                                                  32

/*--------------------------------------
BitField Name: Tx_underflow_err
BitField Type: W1C
BitField Desc: not support with xilinx IP- reserve
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_Tx_underflow_err_Mask                                                     cBit6
#define cAf6_ETH_10G_TX_STICKY_Tx_underflow_err_Shift                                                        6

/*--------------------------------------
BitField Name: stktx_frame_error
BitField Type: W1C
BitField Desc: for packets with tx_axis_tuser set to indicate an End of Packet
(EOP) abort.
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_stktx_frame_error_Mask                                                    cBit5
#define cAf6_ETH_10G_TX_STICKY_stktx_frame_error_Shift                                                       5

/*--------------------------------------
BitField Name: stklt_tx_bad_fcs
BitField Type: W1C
BitField Desc: for packets greater than 64 bytes that have FCS errors.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_stklt_tx_bad_fcs_Mask                                                     cBit4
#define cAf6_ETH_10G_TX_STICKY_stklt_tx_bad_fcs_Shift                                                        4

/*--------------------------------------
BitField Name: stktx_local_fault
BitField Type: W1C
BitField Desc: A value of 1 indicates the transmit encoder state machine is in
the TX_INIT state
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_stktx_local_fault_Mask                                                    cBit3
#define cAf6_ETH_10G_TX_STICKY_stktx_local_fault_Shift                                                       3

/*--------------------------------------
BitField Name: stklt_signal_detect
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine has entered the SEND_DATA state, in which normal PCS operation can
resume
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_stklt_signal_detect_Mask                                                  cBit2
#define cAf6_ETH_10G_TX_STICKY_stklt_signal_detect_Shift                                                     2

/*--------------------------------------
BitField Name: stklt_training
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine is performing link training, per lane
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_stklt_training_Mask                                                       cBit1
#define cAf6_ETH_10G_TX_STICKY_stklt_training_Shift                                                          1

/*--------------------------------------
BitField Name: stklt_training_fail
BitField Type: W1C
BitField Desc: This signal is asserted during link training if the corresponding
link training state machine detects a time-out during the training period
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_TX_STICKY_stklt_training_fail_Mask                                                  cBit0
#define cAf6_ETH_10G_TX_STICKY_stklt_training_fail_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTX ALARM
Reg Addr   : 0x2111
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TX_ALARM_Base                                                                   0x2111
#define cAf6Reg_ETH_10G_TX_ALARM_WidthVal                                                                   32

/*--------------------------------------
BitField Name: Tx_underflow_err
BitField Type: W1C
BitField Desc: not support with xilinx IP- reserve
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_Tx_underflow_err_Mask                                                      cBit6
#define cAf6_ETH_10G_TX_ALARM_Tx_underflow_err_Shift                                                         6

/*--------------------------------------
BitField Name: stat_tx_frame_error
BitField Type: W1C
BitField Desc: for packets with tx_axis_tuser set to indicate an End of Packet
(EOP) abort.
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_stat_tx_frame_error_Mask                                                   cBit5
#define cAf6_ETH_10G_TX_ALARM_stat_tx_frame_error_Shift                                                      5

/*--------------------------------------
BitField Name: stat_tx_bad_fcs
BitField Type: W1C
BitField Desc: for packets greater than 64 bytes that have FCS errors.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_stat_tx_bad_fcs_Mask                                                       cBit4
#define cAf6_ETH_10G_TX_ALARM_stat_tx_bad_fcs_Shift                                                          4

/*--------------------------------------
BitField Name: stat_tx_local_fault
BitField Type: W1C
BitField Desc: A value of 1 indicates the transmit encoder state machine is in
the TX_INIT state
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_stat_tx_local_fault_Mask                                                   cBit3
#define cAf6_ETH_10G_TX_ALARM_stat_tx_local_fault_Shift                                                      3

/*--------------------------------------
BitField Name: stat_lt_signal_detect
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine has entered the SEND_DATA state, in which normal PCS operation can
resume
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_stat_lt_signal_detect_Mask                                                 cBit2
#define cAf6_ETH_10G_TX_ALARM_stat_lt_signal_detect_Shift                                                    2

/*--------------------------------------
BitField Name: stat_lt_training
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine is performing link training, per lane
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_stat_lt_training_Mask                                                      cBit1
#define cAf6_ETH_10G_TX_ALARM_stat_lt_training_Shift                                                         1

/*--------------------------------------
BitField Name: stat_lt_training_fail
BitField Type: W1C
BitField Desc: This signal is asserted during link training if the corresponding
link training state machine detects a time-out during the training period
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_TX_ALARM_stat_lt_training_fail_Mask                                                 cBit0
#define cAf6_ETH_10G_TX_ALARM_stat_lt_training_fail_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GRX STICKY
Reg Addr   : 0x2112
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_RX_STICKY_Base                                                                  0x2112

/*--------------------------------------
BitField Name: stkrx_got_signal_os_0
BitField Type: W1C
BitField Desc: stat_rx_got_signal_os_0
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_got_signal_os_0_Mask                                               cBit21
#define cAf6_ETH_10G_RX_STICKY_stkrx_got_signal_os_0_Shift                                                  21

/*--------------------------------------
BitField Name: stkrx_inrangeerr_0
BitField Type: W1C
BitField Desc: stat_rx_inrangeerr_0
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_inrangeerr_0_Mask                                                  cBit20
#define cAf6_ETH_10G_RX_STICKY_stkrx_inrangeerr_0_Shift                                                     20

/*--------------------------------------
BitField Name: stklt_frame_lock
BitField Type: W1C
BitField Desc: When link training has begun, these signals are asserted
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stklt_frame_lock_Mask                                                    cBit19
#define cAf6_ETH_10G_RX_STICKY_stklt_frame_lock_Shift                                                       19

/*--------------------------------------
BitField Name: stklt_rx_sof_0
BitField Type: W1C
BitField Desc: stklt_rx_sof_0
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stklt_rx_sof_0_Mask                                                      cBit18
#define cAf6_ETH_10G_RX_STICKY_stklt_rx_sof_0_Shift                                                         18

/*--------------------------------------
BitField Name: stklt_preset_from_rx_0
BitField Type: W1C
BitField Desc: stklt_preset_from_rx_0
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stklt_preset_from_rx_0_Mask                                              cBit17
#define cAf6_ETH_10G_RX_STICKY_stklt_preset_from_rx_0_Shift                                                 17

/*--------------------------------------
BitField Name: stklt_initialize_from_rx_0
BitField Type: W1C
BitField Desc: stklt_initialize_from_rx_0
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stklt_initialize_from_rx_0_Mask                                          cBit16
#define cAf6_ETH_10G_RX_STICKY_stklt_initialize_from_rx_0_Shift                                             16

/*--------------------------------------
BitField Name: stkfec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: stkfec_inc_cant_correct_count, per lane
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkfec_inc_cant_correct_count_Mask                                       cBit15
#define cAf6_ETH_10G_RX_STICKY_stkfec_inc_cant_correct_count_Shift                                          15

/*--------------------------------------
BitField Name: stkfec_inc_correct_count
BitField Type: W1C
BitField Desc: stkfec_inc_correct_count , per lane
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkfec_inc_correct_count_Mask                                            cBit14
#define cAf6_ETH_10G_RX_STICKY_stkfec_inc_correct_count_Shift                                               14

/*--------------------------------------
BitField Name: stkfec_lock_error
BitField Type: W1C
BitField Desc: stkfec_lock_error, per lane
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkfec_lock_error_Mask                                                   cBit13
#define cAf6_ETH_10G_RX_STICKY_stkfec_lock_error_Shift                                                      13

/*--------------------------------------
BitField Name: stklt_fec_rx_lock_0
BitField Type: W1C
BitField Desc: This signal is asserted while the ctl_fec_rx_enable is asserted
when the FEC decoder detects the frame boundary
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stklt_fec_rx_lock_0_Mask                                                 cBit12
#define cAf6_ETH_10G_RX_STICKY_stklt_fec_rx_lock_0_Shift                                                    12

/*--------------------------------------
BitField Name: stkrx_local_fault
BitField Type: W1C
BitField Desc: stkrx_local_fault
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_local_fault_Mask                                                   cBit11
#define cAf6_ETH_10G_RX_STICKY_stkrx_local_fault_Shift                                                      11

/*--------------------------------------
BitField Name: stkrx_remote_fault
BitField Type: W1C
BitField Desc: stkrx_remote_fault
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_remote_fault_Mask                                                  cBit10
#define cAf6_ETH_10G_RX_STICKY_stkrx_remote_fault_Shift                                                     10

/*--------------------------------------
BitField Name: stkrx_internal_local_fault
BitField Type: W1C
BitField Desc: stkrx_internal_local_fault
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_internal_local_fault_Mask                                           cBit9
#define cAf6_ETH_10G_RX_STICKY_stkrx_internal_local_fault_Shift                                              9

/*--------------------------------------
BitField Name: stkrx_received_local_fault
BitField Type: W1C
BitField Desc: stkrx_received_local_fault
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_received_local_fault_Mask                                           cBit8
#define cAf6_ETH_10G_RX_STICKY_stkrx_received_local_fault_Shift                                              8

/*--------------------------------------
BitField Name: stkrx_framing_err
BitField Type: W1C
BitField Desc: stk_framing_err, per lane
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_framing_err_Mask                                                    cBit7
#define cAf6_ETH_10G_RX_STICKY_stkrx_framing_err_Shift                                                       7

/*--------------------------------------
BitField Name: stkrx_truncated
BitField Type: W1C
BitField Desc: stk truncated
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_truncated_Mask                                                      cBit6
#define cAf6_ETH_10G_RX_STICKY_stkrx_truncated_Shift                                                         6

/*--------------------------------------
BitField Name: stkrx_hi_ber
BitField Type: W1C
BitField Desc: stk stkrx_hi_ber
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_hi_ber_Mask                                                         cBit5
#define cAf6_ETH_10G_RX_STICKY_stkrx_hi_ber_Shift                                                            5

/*--------------------------------------
BitField Name: stkrx_rx_valid_ctrl_code_0
BitField Type: W1C
BitField Desc: Indicates that a PCS block with a valid control code was
received.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_rx_valid_ctrl_code_0_Mask                                           cBit4
#define cAf6_ETH_10G_RX_STICKY_stkrx_rx_valid_ctrl_code_0_Shift                                              4

/*--------------------------------------
BitField Name: stkrx_bad_sfd
BitField Type: W1C
BitField Desc: This signal indicates if the Ethernet packet received was
preceded by a valid SFD. A value of 1 indicates that an invalid SFD was
received.
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_bad_sfd_Mask                                                        cBit3
#define cAf6_ETH_10G_RX_STICKY_stkrx_bad_sfd_Shift                                                           3

/*--------------------------------------
BitField Name: stkrx_bad_preamble
BitField Type: W1C
BitField Desc: This signal indicates if the Ethernet packet received was
preceded by a valid preamble. A value of 1 indicates that an invalid preamble
was received.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_bad_preamble_Mask                                                   cBit2
#define cAf6_ETH_10G_RX_STICKY_stkrx_bad_preamble_Shift                                                      2

/*--------------------------------------
BitField Name: stkrx_block_lock
BitField Type: W1C
BitField Desc: Block lock status. A value of 1 indicates that block lock is
achieved
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_block_lock_Mask                                                     cBit1
#define cAf6_ETH_10G_RX_STICKY_stkrx_block_lock_Shift                                                        1

/*--------------------------------------
BitField Name: stkrx_status
BitField Type: W1C
BitField Desc: Indicates current status of the link.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_RX_STICKY_stkrx_status_Mask                                                         cBit0
#define cAf6_ETH_10G_RX_STICKY_stkrx_status_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GRX ALARM
Reg Addr   : 0x2113
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_RX_ALARM_Base                                                                   0x2113

/*--------------------------------------
BitField Name: stat_rx_got_signal_os_0
BitField Type: W1C
BitField Desc: stat_rx_got_signal_os_0
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_got_signal_os_0_Mask                                              cBit21
#define cAf6_ETH_10G_RX_ALARM_stat_rx_got_signal_os_0_Shift                                                 21

/*--------------------------------------
BitField Name: stat_rx_inrangeerr_0
BitField Type: W1C
BitField Desc: stat_rx_inrangeerr_0
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_inrangeerr_0_Mask                                                 cBit20
#define cAf6_ETH_10G_RX_ALARM_stat_rx_inrangeerr_0_Shift                                                    20

/*--------------------------------------
BitField Name: stat_lt_frame_lock
BitField Type: W1C
BitField Desc: When link training has begun, these signals are asserted
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_lt_frame_lock_Mask                                                   cBit19
#define cAf6_ETH_10G_RX_ALARM_stat_lt_frame_lock_Shift                                                      19

/*--------------------------------------
BitField Name: stat_lt_rx_sof_0
BitField Type: W1C
BitField Desc: stat_lt_rx_sof_0
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_lt_rx_sof_0_Mask                                                     cBit18
#define cAf6_ETH_10G_RX_ALARM_stat_lt_rx_sof_0_Shift                                                        18

/*--------------------------------------
BitField Name: stat_lt_preset_from_rx_0
BitField Type: W1C
BitField Desc: stat_lt_preset_from_rx_0
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_lt_preset_from_rx_0_Mask                                             cBit17
#define cAf6_ETH_10G_RX_ALARM_stat_lt_preset_from_rx_0_Shift                                                17

/*--------------------------------------
BitField Name: stat_lt_initialize_from_rx_0
BitField Type: W1C
BitField Desc: stat_lt_initialize_from_rx_0
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_lt_initialize_from_rx_0_Mask                                         cBit16
#define cAf6_ETH_10G_RX_ALARM_stat_lt_initialize_from_rx_0_Shift                                            16

/*--------------------------------------
BitField Name: stat_fec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: stat_fec_inc_cant_correct_count, per lane
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_fec_inc_cant_correct_count_Mask                                      cBit15
#define cAf6_ETH_10G_RX_ALARM_stat_fec_inc_cant_correct_count_Shift                                         15

/*--------------------------------------
BitField Name: stat_fec_inc_correct_count
BitField Type: W1C
BitField Desc: stat_fec_inc_correct_count , per lane
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_fec_inc_correct_count_Mask                                           cBit14
#define cAf6_ETH_10G_RX_ALARM_stat_fec_inc_correct_count_Shift                                              14

/*--------------------------------------
BitField Name: stat_fec_lock_error
BitField Type: W1C
BitField Desc: stat_fec_lock_error, per lane
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_fec_lock_error_Mask                                                  cBit13
#define cAf6_ETH_10G_RX_ALARM_stat_fec_lock_error_Shift                                                     13

/*--------------------------------------
BitField Name: stat_lt_fec_rx_lock_0
BitField Type: W1C
BitField Desc: This signal is asserted while the ctl_fec_rx_enable is asserted
when the FEC decoder detects the frame boundary
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_lt_fec_rx_lock_0_Mask                                                cBit12
#define cAf6_ETH_10G_RX_ALARM_stat_lt_fec_rx_lock_0_Shift                                                   12

/*--------------------------------------
BitField Name: stat_rx_local_fault
BitField Type: W1C
BitField Desc: stat_rx_local_fault
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_local_fault_Mask                                                  cBit11
#define cAf6_ETH_10G_RX_ALARM_stat_rx_local_fault_Shift                                                     11

/*--------------------------------------
BitField Name: stat_rx_remote_fault
BitField Type: W1C
BitField Desc: stat_rx_remote_fault
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_remote_fault_Mask                                                 cBit10
#define cAf6_ETH_10G_RX_ALARM_stat_rx_remote_fault_Shift                                                    10

/*--------------------------------------
BitField Name: stat_rx_internal_local_fault
BitField Type: W1C
BitField Desc: stat_rx_internal_local_fault
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_internal_local_fault_Mask                                          cBit9
#define cAf6_ETH_10G_RX_ALARM_stat_rx_internal_local_fault_Shift                                             9

/*--------------------------------------
BitField Name: stat_rx_received_local_fault
BitField Type: W1C
BitField Desc: stat_rx_received_local_fault
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_received_local_fault_Mask                                          cBit8
#define cAf6_ETH_10G_RX_ALARM_stat_rx_received_local_fault_Shift                                             8

/*--------------------------------------
BitField Name: stat_rx_framing_err
BitField Type: W1C
BitField Desc: stat_framing_err, per lane
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_framing_err_Mask                                                   cBit7
#define cAf6_ETH_10G_RX_ALARM_stat_rx_framing_err_Shift                                                      7

/*--------------------------------------
BitField Name: stat_rx_truncated
BitField Type: W1C
BitField Desc: stat_truncated
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_truncated_Mask                                                     cBit6
#define cAf6_ETH_10G_RX_ALARM_stat_rx_truncated_Shift                                                        6

/*--------------------------------------
BitField Name: stat_rx_hi_ber
BitField Type: W1C
BitField Desc: stat_rx_hi_ber
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_hi_ber_Mask                                                        cBit5
#define cAf6_ETH_10G_RX_ALARM_stat_rx_hi_ber_Shift                                                           5

/*--------------------------------------
BitField Name: stat_rx_rx_valid_ctrl_code_0
BitField Type: W1C
BitField Desc: Indicates that a PCS block with a valid control code was
received.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_rx_valid_ctrl_code_0_Mask                                          cBit4
#define cAf6_ETH_10G_RX_ALARM_stat_rx_rx_valid_ctrl_code_0_Shift                                             4

/*--------------------------------------
BitField Name: stat_rx_bad_sfd
BitField Type: W1C
BitField Desc: This signal indicates if the Ethernet packet received was
preceded by a valid SFD. A value of 1 indicates that an invalid SFD was
received.
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_bad_sfd_Mask                                                       cBit3
#define cAf6_ETH_10G_RX_ALARM_stat_rx_bad_sfd_Shift                                                          3

/*--------------------------------------
BitField Name: stat_rx_bad_preamble
BitField Type: W1C
BitField Desc: This signal indicates if the Ethernet packet received was
preceded by a valid preamble. A value of 1 indicates that an invalid preamble
was received.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_bad_preamble_Mask                                                  cBit2
#define cAf6_ETH_10G_RX_ALARM_stat_rx_bad_preamble_Shift                                                     2

/*--------------------------------------
BitField Name: stat_rx_block_lock
BitField Type: W1C
BitField Desc: Block lock status. A value of 1 indicates that block lock is
achieved
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_block_lock_Mask                                                    cBit1
#define cAf6_ETH_10G_RX_ALARM_stat_rx_block_lock_Shift                                                       1

/*--------------------------------------
BitField Name: stat_rx_status
BitField Type: W1C
BitField Desc: Indicates current status of the link.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_RX_ALARM_stat_rx_status_Mask                                                        cBit0
#define cAf6_ETH_10G_RX_ALARM_stat_rx_status_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G_AN STICKY
Reg Addr   : 0x2114
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G-pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_AN_STICKY_Base                                                                  0x2114
#define cAf6Reg_ETH_10G_AN_STICKY_WidthVal                                                                  32

/*--------------------------------------
BitField Name: stkan_start_tx_disable
BitField Type: W1C
BitField Desc: stkan_start_tx_disable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_AN_STICKY_stkan_start_tx_disable_Mask                                               cBit3
#define cAf6_ETH_10G_AN_STICKY_stkan_start_tx_disable_Shift                                                  3

/*--------------------------------------
BitField Name: stkan_start_an_good_check
BitField Type: W1C
BitField Desc: stkan_start_an_good_check
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_AN_STICKY_stkan_start_an_good_check_Mask                                            cBit2
#define cAf6_ETH_10G_AN_STICKY_stkan_start_an_good_check_Shift                                               2

/*--------------------------------------
BitField Name: stkan_autoneg_complete
BitField Type: W1C
BitField Desc: stkan_autoneg_complete
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_AN_STICKY_stkan_autoneg_complete_Mask                                               cBit1
#define cAf6_ETH_10G_AN_STICKY_stkan_autoneg_complete_Shift                                                  1

/*--------------------------------------
BitField Name: stkan_parallel_detection_fault
BitField Type: W1C
BitField Desc: stkan_parallel_detection_fault
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_AN_STICKY_stkan_parallel_detection_fault_Mask                                       cBit0
#define cAf6_ETH_10G_AN_STICKY_stkan_parallel_detection_fault_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G_AN ALARM
Reg Addr   : 0x2115
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G-pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_AN_ALARM_Base                                                                   0x2115
#define cAf6Reg_ETH_10G_AN_ALARM_WidthVal                                                                   32

/*--------------------------------------
BitField Name: stat_an_start_tx_disable
BitField Type: W1C
BitField Desc: stat_an_start_tx_disable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_AN_ALARM_stat_an_start_tx_disable_Mask                                              cBit3
#define cAf6_ETH_10G_AN_ALARM_stat_an_start_tx_disable_Shift                                                 3

/*--------------------------------------
BitField Name: stat_an_start_an_good_check
BitField Type: W1C
BitField Desc: stat_an_start_an_good_check
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_AN_ALARM_stat_an_start_an_good_check_Mask                                           cBit2
#define cAf6_ETH_10G_AN_ALARM_stat_an_start_an_good_check_Shift                                              2

/*--------------------------------------
BitField Name: stat_an_autoneg_complete
BitField Type: W1C
BitField Desc: stat_an_autoneg_complete
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_AN_ALARM_stat_an_autoneg_complete_Mask                                              cBit1
#define cAf6_ETH_10G_AN_ALARM_stat_an_autoneg_complete_Shift                                                 1

/*--------------------------------------
BitField Name: stat_an_parallel_detection_fault
BitField Type: W1C
BitField Desc: stat_an_parallel_detection_fault
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_AN_ALARM_stat_an_parallel_detection_fault_Mask                                      cBit0
#define cAf6_ETH_10G_AN_ALARM_stat_an_parallel_detection_fault_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GTX INTTERUPT ENABLE
Reg Addr   : 0x2116
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_TX_INTEN_Base                                                                   0x2116
#define cAf6Reg_ETH_10G_TX_INTEN_WidthVal                                                                   32

/*--------------------------------------
BitField Name: int_en_tx_frame_error
BitField Type: W1C
BitField Desc: for packets with tx_axis_tuser set to indicate an End of Packet
(EOP) abort.
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_TX_INTEN_int_en_tx_frame_error_Mask                                                 cBit5
#define cAf6_ETH_10G_TX_INTEN_int_en_tx_frame_error_Shift                                                    5

/*--------------------------------------
BitField Name: int_en_tx_bad_fcs
BitField Type: W1C
BitField Desc: for packets greater than 64 bytes that have FCS errors.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_TX_INTEN_int_en_tx_bad_fcs_Mask                                                     cBit4
#define cAf6_ETH_10G_TX_INTEN_int_en_tx_bad_fcs_Shift                                                        4

/*--------------------------------------
BitField Name: int_en_tx_local_fault
BitField Type: W1C
BitField Desc: A value of 1 indicates the transmit encoder state machine is in
the TX_INIT state
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_Mask                                                 cBit3
#define cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_Shift                                                    3

/*--------------------------------------
BitField Name: int_en_lt_signal_detect
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine has entered the SEND_DATA state, in which normal PCS operation can
resume
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_TX_INTEN_int_en_lt_signal_detect_Mask                                               cBit2
#define cAf6_ETH_10G_TX_INTEN_int_en_lt_signal_detect_Shift                                                  2

/*--------------------------------------
BitField Name: int_en_lt_training
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine is performing link training, per lane
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_TX_INTEN_int_en_lt_training_Mask                                                    cBit1
#define cAf6_ETH_10G_TX_INTEN_int_en_lt_training_Shift                                                       1

/*--------------------------------------
BitField Name: int_en_lt_training_fail
BitField Type: W1C
BitField Desc: This signal is asserted during link training if the corresponding
link training state machine detects a time-out during the training period
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_TX_INTEN_int_en_lt_training_fail_Mask                                               cBit0
#define cAf6_ETH_10G_TX_INTEN_int_en_lt_training_fail_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GRX INTTERUPT ENABLE
Reg Addr   : 0x2117
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_RX_INTEN_Base                                                                   0x2117

/*--------------------------------------
BitField Name: int_en_rx_got_signal_os_0
BitField Type: W1C
BitField Desc: int_en_rx_got_signal_os_0
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_got_signal_os_0_Mask                                            cBit21
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_got_signal_os_0_Shift                                               21

/*--------------------------------------
BitField Name: int_en_rx_inrangeerr_0
BitField Type: W1C
BitField Desc: int_en_rx_inrangeerr_0
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_inrangeerr_0_Mask                                               cBit20
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_inrangeerr_0_Shift                                                  20

/*--------------------------------------
BitField Name: int_en_lt_frame_lock
BitField Type: W1C
BitField Desc: When link training has begun, these signals are asserted
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_frame_lock_Mask                                                 cBit19
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_frame_lock_Shift                                                    19

/*--------------------------------------
BitField Name: int_en_lt_rx_sof_0
BitField Type: W1C
BitField Desc: int_en_lt_rx_sof_0
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_rx_sof_0_Mask                                                   cBit18
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_rx_sof_0_Shift                                                      18

/*--------------------------------------
BitField Name: int_en_lt_preset_from_rx_0
BitField Type: W1C
BitField Desc: int_en_lt_preset_from_rx_0
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_preset_from_rx_0_Mask                                           cBit17
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_preset_from_rx_0_Shift                                              17

/*--------------------------------------
BitField Name: int_en_lt_initialize_from_rx_0
BitField Type: W1C
BitField Desc: int_en_lt_initialize_from_rx_0
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_initialize_from_rx_0_Mask                                       cBit16
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_initialize_from_rx_0_Shift                                          16

/*--------------------------------------
BitField Name: int_en_fec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: int_en_fec_inc_cant_correct_count, per lane
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_Mask                                    cBit15
#define cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_Shift                                       15

/*--------------------------------------
BitField Name: int_en_fec_inc_correct_count
BitField Type: W1C
BitField Desc: int_en_fec_inc_correct_count , per lane
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_Mask                                         cBit14
#define cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_Shift                                            14

/*--------------------------------------
BitField Name: int_en_fec_lock_error
BitField Type: W1C
BitField Desc: int_en_fec_lock_error, per lane
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_Mask                                                cBit13
#define cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_Shift                                                   13

/*--------------------------------------
BitField Name: int_en_lt_fec_rx_lock_0
BitField Type: W1C
BitField Desc: This signal is asserted while the ctl_fec_rx_enable is asserted
when the FEC decoder detects the frame boundary
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_fec_rx_lock_0_Mask                                              cBit12
#define cAf6_ETH_10G_RX_INTEN_int_en_lt_fec_rx_lock_0_Shift                                                 12

/*--------------------------------------
BitField Name: int_en_rx_local_fault
BitField Type: W1C
BitField Desc: int_en_rx_local_fault
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_Mask                                                cBit11
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_Shift                                                   11

/*--------------------------------------
BitField Name: int_en_rx_remote_fault
BitField Type: W1C
BitField Desc: int_en_rx_remote_fault
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_Mask                                               cBit10
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_Shift                                                  10

/*--------------------------------------
BitField Name: int_en_rx_internal_local_fault
BitField Type: W1C
BitField Desc: int_en_rx_internal_local_fault
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_Mask                                        cBit9
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_Shift                                           9

/*--------------------------------------
BitField Name: int_en_rx_received_local_fault
BitField Type: W1C
BitField Desc: int_en_rx_received_local_fault
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_Mask                                        cBit8
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_Shift                                           8

/*--------------------------------------
BitField Name: int_en_rx_framing_err
BitField Type: W1C
BitField Desc: int_en_framing_err, per lane
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_Mask                                                 cBit7
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_Shift                                                    7

/*--------------------------------------
BitField Name: int_en_rx_truncated
BitField Type: W1C
BitField Desc: int_en_truncated
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_Mask                                                   cBit6
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_Shift                                                      6

/*--------------------------------------
BitField Name: int_en_rx_hi_ber
BitField Type: W1C
BitField Desc: int_en_rx_hi_ber
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_Mask                                                      cBit5
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_Shift                                                         5

/*--------------------------------------
BitField Name: int_en_rx_rx_valid_ctrl_code_0
BitField Type: W1C
BitField Desc: Indicates that a PCS block with a valid control code was
received.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_rx_valid_ctrl_code_0_Mask                                        cBit4
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_rx_valid_ctrl_code_0_Shift                                           4

/*--------------------------------------
BitField Name: int_en_rx_bad_sfd
BitField Type: W1C
BitField Desc: This signal indicates if the Ethernet packet received was
preceded by a valid SFD. A value of 1 indicates that an invalid SFD was
received.
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_bad_sfd_Mask                                                     cBit3
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_bad_sfd_Shift                                                        3

/*--------------------------------------
BitField Name: int_en_rx_bad_preamble
BitField Type: W1C
BitField Desc: This signal indicates if the Ethernet packet received was
preceded by a valid preamble. A value of 1 indicates that an invalid preamble
was received.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_bad_preamble_Mask                                                cBit2
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_bad_preamble_Shift                                                   2

/*--------------------------------------
BitField Name: int_en_rx_block_lock
BitField Type: W1C
BitField Desc: Block lock status. A value of 1 indicates that block lock is
achieved
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_block_lock_Mask                                                  cBit1
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_block_lock_Shift                                                     1

/*--------------------------------------
BitField Name: int_en_rx_status
BitField Type: W1C
BitField Desc: Indicates current status of the link.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_status_Mask                                                      cBit0
#define cAf6_ETH_10G_RX_INTEN_int_en_rx_status_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G_AN INTTERUPT ENABLE
Reg Addr   : 0x2118
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G-pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_AN_INTEN_Base                                                                   0x2118
#define cAf6Reg_ETH_10G_AN_INTEN_WidthVal                                                                   32

/*--------------------------------------
BitField Name: int_en_an_start_tx_disable
BitField Type: W1C
BitField Desc: int_en_an_start_tx_disable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_10G_AN_INTEN_int_en_an_start_tx_disable_Mask                                            cBit3
#define cAf6_ETH_10G_AN_INTEN_int_en_an_start_tx_disable_Shift                                               3

/*--------------------------------------
BitField Name: int_en_an_start_an_good_check
BitField Type: W1C
BitField Desc: int_en_an_start_an_good_check
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_10G_AN_INTEN_int_en_an_start_an_good_check_Mask                                         cBit2
#define cAf6_ETH_10G_AN_INTEN_int_en_an_start_an_good_check_Shift                                            2

/*--------------------------------------
BitField Name: int_en_an_autoneg_complete
BitField Type: W1C
BitField Desc: int_en_an_autoneg_complete
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_10G_AN_INTEN_int_en_an_autoneg_complete_Mask                                            cBit1
#define cAf6_ETH_10G_AN_INTEN_int_en_an_autoneg_complete_Shift                                               1

/*--------------------------------------
BitField Name: int_en_an_parallel_detection_fault
BitField Type: W1C
BitField Desc: int_en_an_parallel_detection_fault
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_Mask                                    cBit0
#define cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GStatistics TX COUNTER
Reg Addr   : 0x2000 - 0x201B
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 69-70/table 2-22)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Statistics_TX_COUNTER_Base                                                      0x2000

/*--------------------------------------
BitField Name: cnt_tx_val
BitField Type: RW
BitField Desc: value of resgister Statistics Tx Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Statistics_TX_COUNTER_cnt_tx_val_Mask                                            cBit31_0
#define cAf6_ETH_10G_Statistics_TX_COUNTER_cnt_tx_val_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10GStatistics RX Counters
Reg Addr   : 0x2080 - 0x20A6
Reg Formula: 
    Where  : 
Reg Desc   : 
AXI4 Statistics Counters ETH 10G- pg210-25g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 71-75/table 2-22)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_10G_Statistics_RX_Counters_Base                                                     0x2080

/*--------------------------------------
BitField Name: cnt_rx_val
BitField Type: RW
BitField Desc: value of resgister Statistics Rx Counters
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_10G_Statistics_RX_Counters_cnt_rx_val_Mask                                           cBit31_0
#define cAf6_ETH_10G_Statistics_RX_Counters_cnt_rx_val_Shift                                                 0

#endif /* _AF6_REG_AF6CNC0011_ETH10G_RD_H_ */

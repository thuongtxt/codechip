/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_CLS_H_
#define _AF6_REG_AF6CNC0011_RD_CLS_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Ethernet DA bit47_32 Configuration
Reg Addr   : 0x_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit47_32

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dasa                                                                               0x0000

/*--------------------------------------
BitField Name: DA_bit47_32
BitField Type: R/W
BitField Desc: DA bit47_32
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dasa_DA_bit47_32_Mask                                                              cBit31_16
#define cAf6_upen_dasa_DA_bit47_32_Shift                                                                    16

/*--------------------------------------
BitField Name: SA_bit47_32
BitField Type: R/W
BitField Desc: SA bit47_32
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dasa_SA_bit47_32_Mask                                                               cBit15_0
#define cAf6_upen_dasa_SA_bit47_32_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet DA bit31_00 Configuration
Reg Addr   : 0x_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit32_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_da31                                                                               0x0001

/*--------------------------------------
BitField Name: DA_bit31_00
BitField Type: R/W
BitField Desc: DA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_da31_DA_bit31_00_Mask                                                               cBit31_0
#define cAf6_upen_da31_DA_bit31_00_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet SA bit31_00 Configuration
Reg Addr   : 0x_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the SA bit31_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sa31                                                                               0x0002

/*--------------------------------------
BitField Name: SA_bit31_00
BitField Type: R/W
BitField Desc: SA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_sa31_SA_bit31_00_Mask                                                               cBit31_0
#define cAf6_upen_sa31_SA_bit31_00_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Sub_Type of packet control Configuration
Reg Addr   : 0x_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure E-type and Sub_Type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_type                                                                               0x0003

/*--------------------------------------
BitField Name: Sub_type mask
BitField Type: R/W
BitField Desc: Bit enable to mask a sub-type bit
BitField Bits: [30:24]
--------------------------------------*/
#define cAf6_upen_type_Sub_type_mask_Mask                                                            cBit30_24
#define cAf6_upen_type_Sub_type_mask_Shift                                                                  24

/*--------------------------------------
BitField Name: Sub_type value
BitField Type: R/W
BitField Desc: Ethernet SubType of control packet 0thers        : data frame
0x80-0xFF  : control frames
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_type_Sub_type_value_Mask                                                           cBit23_16
#define cAf6_upen_type_Sub_type_value_Shift                                                                 16

/*--------------------------------------
BitField Name: E_type
BitField Type: R/W
BitField Desc: E_type of pkt
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_type_E_type_Mask                                                                    cBit15_0
#define cAf6_upen_type_E_type_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Classified Ethernet field mask & Lengh pkt  Configuration
Reg Addr   : 0x_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mask                                                                               0x0004

/*--------------------------------------
BitField Name: Eth_len
BitField Type: R/W
BitField Desc: Len field , same as pdhmux cfg
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_mask_Eth_len_Mask                                                                  cBit31_16
#define cAf6_upen_mask_Eth_len_Shift                                                                        16

/*--------------------------------------
BitField Name: cbchk_mode
BitField Type: R/W
BitField Desc: Count bytes chk compare with length as mode 00           : use
length cfg Others         : use length calc by RTL
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_mask_cbchk_mode_Mask                                                                 cBit9_8
#define cAf6_upen_mask_cbchk_mode_Shift                                                                      8

/*--------------------------------------
BitField Name: Sequence mask
BitField Type: R/W
BitField Desc: mask sequence check
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_mask_Sequence_mask_Mask                                                                cBit4
#define cAf6_upen_mask_Sequence_mask_Shift                                                                   4

/*--------------------------------------
BitField Name: Length mask
BitField Type: R/W
BitField Desc: mask length of packet
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_mask_Length_mask_Mask                                                                  cBit3
#define cAf6_upen_mask_Length_mask_Shift                                                                     3

/*--------------------------------------
BitField Name: E_type mask
BitField Type: R/W
BitField Desc: mask E_type of packet
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_mask_E_type_mask_Mask                                                                  cBit2
#define cAf6_upen_mask_E_type_mask_Shift                                                                     2

/*--------------------------------------
BitField Name: SA mask
BitField Type: R/W
BitField Desc: mask SA of packet
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_mask_SA_mask_Mask                                                                      cBit1
#define cAf6_upen_mask_SA_mask_Shift                                                                         1

/*--------------------------------------
BitField Name: DA mask
BitField Type: R/W
BitField Desc: mask DA of packet
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_mask_DA_mask_Mask                                                                      cBit0
#define cAf6_upen_mask_DA_mask_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation EtheType and EtherLength  Configuration
Reg Addr   : 0x1000 - 0x101F
Reg Formula: 0x1000 + $ro*16 + $id
    Where  : 
           + $ro(0-1) : ro bit
           + $id(0-15) : Counter ID
Reg Desc   : 
This register is used to read classified packet counters , support both mode r2c and ro
Counter ID detail :
+ 0 	: DS1      packet counter
+ 1 	: DS3      packet counter
+ 2 	: Control  packet counter
+ 4		: err seq counter
+ 5		: fcs err counter
+ 8		: da err discard packet counter
+ 9		: sa err discard packet counter
+ 10	: etype err discard packet counter
+ 11	: len err  packet counter
+ 7		: unused
ro			: enable mean read only (not clear)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_count                                                                              0x1000

/*--------------------------------------
BitField Name: Ether_Type
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_count_Ether_Type_Mask                                                               cBit31_0
#define cAf6_upen_count_Ether_Type_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Dump packet  Configuration
Reg Addr   : 0x_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure dump packet type , use to dump pkt & debug

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dump                                                                               0x0006

/*--------------------------------------
BitField Name: dump_typ
BitField Type: R/W
BitField Desc: Dump pkt type 00 : dump pkt input 01 : dump pkt ds1 output 10 :
dump pkt ds3 output 11 : dump pkt control output
BitField Bits: [02:01]
--------------------------------------*/
#define cAf6_upen_dump_dump_typ_Mask                                                                   cBit2_1
#define cAf6_upen_dump_dump_typ_Shift                                                                        1

/*--------------------------------------
BitField Name: dfsop_enb
BitField Type: R/W
BitField Desc: dump from sop  enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dump_dfsop_enb_Mask                                                                    cBit0
#define cAf6_upen_dump_dfsop_enb_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Dump packet data
Reg Addr   : 0x2000 - 0x2FFF
Reg Formula: 0x2000 + $id
    Where  : 
           + $id(0-4095) : Counter ID
Reg Desc   : 
This register is used to read data of dump packets,use to debug

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dbpkt                                                                              0x2000

/*--------------------------------------
BitField Name: pkt_dat
BitField Type: R/W
BitField Desc: Data of dump packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dbpkt_pkt_dat_Mask                                                                  cBit31_0
#define cAf6_upen_dbpkt_pkt_dat_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Debug Stick 0
Reg Addr   : 0x0007
Reg Formula: 
    Where  : 
Reg Desc   : 
This sticky is used to debug. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk                                                                                0x0007

/*--------------------------------------
BitField Name: Len_err_stk
BitField Type: R/W
BitField Desc: Length error sticky
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_stk_Len_err_stk_Mask                                                                  cBit13
#define cAf6_upen_stk_Len_err_stk_Shift                                                                     13

/*--------------------------------------
BitField Name: Etp_err_stk
BitField Type: R/W
BitField Desc: E-type error sticky
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_stk_Etp_err_stk_Mask                                                                  cBit12
#define cAf6_upen_stk_Etp_err_stk_Shift                                                                     12

/*--------------------------------------
BitField Name: Sa_err_stk
BitField Type: R/W
BitField Desc: SA Cnt error sticky
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_stk_Sa_err_stk_Mask                                                                   cBit11
#define cAf6_upen_stk_Sa_err_stk_Shift                                                                      11

/*--------------------------------------
BitField Name: Da_err_stk
BitField Type: R/W
BitField Desc: DA Cnt error sticky
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_stk_Da_err_stk_Mask                                                                   cBit10
#define cAf6_upen_stk_Da_err_stk_Shift                                                                      10

/*--------------------------------------
BitField Name: Fcn_err_stk
BitField Type: R/W
BitField Desc: Fcs Cnt  error sticky
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_stk_Fcn_err_stk_Mask                                                                   cBit9
#define cAf6_upen_stk_Fcn_err_stk_Shift                                                                      9

/*--------------------------------------
BitField Name: Seq_err_stk
BitField Type: R/W
BitField Desc: Sequence error sticky
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_stk_Seq_err_stk_Mask                                                                   cBit8
#define cAf6_upen_stk_Seq_err_stk_Shift                                                                      8

/*--------------------------------------
BitField Name: Ctl_pkt_stk
BitField Type: R/W
BitField Desc: Control packet output sticky
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_stk_Ctl_pkt_stk_Mask                                                                   cBit5
#define cAf6_upen_stk_Ctl_pkt_stk_Shift                                                                      5

/*--------------------------------------
BitField Name: Ds3_pkt_stk
BitField Type: R/W
BitField Desc: Ds3     packet output sticky
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_stk_Ds3_pkt_stk_Mask                                                                   cBit4
#define cAf6_upen_stk_Ds3_pkt_stk_Shift                                                                      4

/*--------------------------------------
BitField Name: Ds1_pkt_stk
BitField Type: R/W
BitField Desc: DS1     packet output sticky
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_stk_Ds1_pkt_stk_Mask                                                                   cBit3
#define cAf6_upen_stk_Ds1_pkt_stk_Shift                                                                      3

/*--------------------------------------
BitField Name: Fcs_pkt_stk
BitField Type: R/W
BitField Desc: FCS     packet  input sticky
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stk_Fcs_pkt_stk_Mask                                                                   cBit2
#define cAf6_upen_stk_Fcs_pkt_stk_Shift                                                                      2

/*--------------------------------------
BitField Name: Eop_vld_stk
BitField Type: R/W
BitField Desc: Eop     valid   input sticky
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stk_Eop_vld_stk_Mask                                                                   cBit1
#define cAf6_upen_stk_Eop_vld_stk_Shift                                                                      1

/*--------------------------------------
BitField Name: Pkt_vld_stk
BitField Type: R/W
BitField Desc: Pkt     valid   input sticky
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stk_Pkt_vld_stk_Mask                                                                   cBit0
#define cAf6_upen_stk_Pkt_vld_stk_Shift                                                                      0

/* tx pkt at gmiii */
#define cAf6_upen_debug_tx_pkt_at_gmii(r2c)                                                               (0x60 + (r2c)*8)

/* rx pkt at gmii */
#define cAf6_upen_debug_rx_pkt_at_gmii(r2c)                                                                (0x61 + (r2c)*8)

/* pkt at txuser */
#define cAf6_upen_debug_tx_pkt_at_mac_user(r2c)                                                            (0x62 + (r2c)*8)

/* pkt at rxuser*/
#define cAf6_upen_debug_rx_pkt_at_mac_user(r2c)                                                            (0x63 + (r2c)*8)

/* bytes cnt  at tx user*/
#define cAf6_upen_debug_tx_byte_at_mac_user(r2c)                                                            (0x64 + (r2c)*8)

/* bytes cnt  at rx user*/
#define cAf6_upen_debug_rx_byte_at_mac_user(r2c)                                                             (0x65 + (r2c)*8)

/* bytes cnt  at tx gmii*/
#define cAf6_upen_debug_tx_byte_at_gmii(r2c)                                                              (0x66 + (r2c)*8)

/* bytes cnt  at rx gmii*/
#define cAf6_upen_debug_rx_byte_at_gmii(r2c)                                                               (0x67 + (r2c)*8)

#endif /* _AF6_REG_AF6CNC0011_RD_CLS_H_ */

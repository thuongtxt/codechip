/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290011DccSgmiiEthPortInternal.h
 * 
 * Created Date: Mar 4, 2018
 *
 * Description : DCC SGMII port internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011DCCSGMIIETHPORTINTERNAL_H_
#define _THA60290011DCCSGMIIETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290011EthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011DccSgmiiEthPort
    {
    tTha60290011EthPort super;
    }tTha60290011DccSgmiiEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290011DccSgmiiEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011DCCSGMIIETHPORTINTERNAL_H_ */


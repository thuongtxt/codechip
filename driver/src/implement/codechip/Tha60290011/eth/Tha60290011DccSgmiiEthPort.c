/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth
 *
 * File        : Tha60290011DccSgmiiEthPort.c
 *
 * Created Date: Dec 27, 2016
 *
 * Description : Implementation of DCC Sgmii Eth Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../src/generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/common/Tha6029DccKbyte.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "../pw/Tha60290011DccKbyteV2Reg.h"
#include "../pw/Tha60290011ModulePw.h"
#include "../man/Tha60290011DeviceInternal.h"
#include "Tha60290011ModuleEth.h"
#include "Tha60290011DccSgmiiEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSohTransparentIsSupported(self) AtEthPortSohTransparentIsSupported((AtEthPort)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods   m_AtObjectOverride;
static tAtChannelMethods  m_AtChannelOverride;
static tAtEthPortMethods  m_AtEthPortOverride;
static tThaEthPortMethods m_ThaEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEthRet ReturnModeNotSupport(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eBool HasSourceMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasDestMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed1000M) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed1000M) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed1000M;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    return (interface == cAtEthPortInterfaceSgmii) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceSgmii;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    uint8 localId = Tha60290011ModuleEthLocalSgmiiPort(self);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);
    return Tha60290011SerdesManagerSgmiiSerdesController(serdesManager, localId);
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)self;
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[64];

    AtSprintf(str, "%ssgmii_kbyte_dcc", AtDeviceIdToString(dev));
    return str;
    }

static eBool SohTransparentIsSupported(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60290011ModuleEthSohTransparentIsReady(ethModule);
    }

static void MacDwordSw2Hw(uint32 macLsbVal, uint32 macMsbVal, uint32 *hwMacLsb, uint32 *hwMacMsb)
    {
    *hwMacLsb = (uint32) ((macLsbVal >> 6) + ((macMsbVal & cBit5_0) << 26));
    *hwMacMsb = (uint32) (macMsbVal >> 6);
    }

static void MacDwordsHw2Sw(uint32 *longRegVal, uint32 *macLsb, uint32 *macMsb)
    {
    *macLsb = (uint32)((longRegVal[0] & cBit25_0) << 6);
    *macMsb = (uint32)((longRegVal[0] >> 26) | ((longRegVal[1] & cBit9_0) << 6));
    }

static eBool HasExpectedDestMac(AtEthPort self)
    {
    return mSohTransparentIsSupported(self);
    }

static eAtRet ExpectedDestMacSet(AtEthPort self, const uint8 *mac)
    {
    if (AtEthPortHasExpectedDestMac(self))
        return Tha6029DccKByteSgmiiExpectedDestMacSet((ThaEthPort)self, mac, cAf6_upen_dccrxhdr_MAC_DA_Mask_02, cAf6_upen_dccrxhdr_MAC_DA_Shift_02, MacDwordSw2Hw);
    return m_AtEthPortMethods->ExpectedDestMacSet(self, mac);
    }

static eAtRet ExpectedDestMacGet(AtEthPort self, uint8 *mac)
    {
    if (AtEthPortHasExpectedDestMac(self))
        return Tha6029DccKByteSgmiiExpectedDestMacGet((ThaEthPort)self, mac, MacDwordsHw2Sw);
    return m_AtEthPortMethods->ExpectedDestMacGet(self, mac);
    }

static eBool ExpectedCVlanIsSupported(AtEthPort self)
    {
    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiExpectedCVlanIsSupported(self);
    return m_AtEthPortMethods->ExpectedCVlanIsSupported(self);
    }

static eAtRet ExpectedCVlanIdSet(AtEthPort self, uint16 vlanId)
    {
    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiExpectedCVlanSet(self, vlanId, cAf6_upen_dccrxhdr_CVID_Mask, cAf6_upen_dccrxhdr_CVID_Shift);
    return m_AtEthPortMethods->ExpectedCVlanIdSet(self, vlanId);
    }

static uint16 ExpectedCVlanIdGet(AtEthPort self)
    {
    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiExpectedCVlanGet(self, cAf6_upen_dccrxhdr_CVID_Mask, cAf6_upen_dccrxhdr_CVID_Shift);
    return m_AtEthPortMethods->ExpectedCVlanIdGet(self);
    }

static eBool ShouldUseInterruptV2(AtChannel self)
    {
    Tha60290011ModulePw pwModule = (Tha60290011ModulePw)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePw);
    return Tha60290011ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiDefectGet(self);

    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiDefectGet(self);
    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiDefectHistoryRead2Clear(self, cAtFalse);

    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiDefectHistoryRead2Clear(self, cAtFalse);
    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiDefectHistoryRead2Clear(self, cAtTrue);

    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiDefectHistoryRead2Clear(self, cAtTrue);
    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static ThaModulePmc PmcModule(AtChannel self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (Tha6029DccKByteSgmiiIsDccKByteCounter(self, counterType) && mSohTransparentIsSupported(self))
        return cAtTrue;

    if (ThaEthPortShouldGetCounterFromPmc(self))
        return ThaModulePmcEthPortCounterIsSupported(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortTypeDccPort, counterType);

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static uint32 PmcCounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    return ThaModulePmcEthPortCountersGet(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortTypeDccPort, counterType, read2Clear);
    }

static eBool UseCounterV3(ThaEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return Tha60290011DeviceDccTxFcsMsbIsSupported(device);
    }

static uint32 CounterLocalAddress(ThaEthPort self, uint16 counterType)
    {
    if (UseCounterV3(self))
        return Tha6029PdhDccKbyteEthPortCounterV3BaseAddress(self, counterType);

    return Tha6029DccKbyteEthPortCounterV2BaseAddress(self, counterType);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (Tha6029DccKByteSgmiiIsDccKByteCounter(self, counterType) && mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiCounterRead2Clear(self, counterType, cAtFalse);

    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (Tha6029DccKByteSgmiiIsDccKByteCounter(self, counterType) && mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiCounterRead2Clear(self, counterType, cAtTrue);

    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiInterruptMaskSet(self, defectMask, enableMask);

    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiInterruptMaskSet(self, defectMask, enableMask);
    return m_AtChannelMethods->InterruptMaskSet(self, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiInterruptMaskGet(self);

    if (mSohTransparentIsSupported(self))
        return Tha6029DccKByteSgmiiInterruptMaskGet(self);
    return m_AtChannelMethods->InterruptMaskGet(self);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    return AtChannelInterruptMaskGet(self) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiInterruptProcess(self, offset);
    return 0;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiSupportedInterruptMasks(self);

    return Tha6029DccKByteSgmiiSupportedInterruptMasks(self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtChannelMethods->Init(self);

    if (mSohTransparentIsSupported(self))
        ret |= AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0);

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, HasSourceMac);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, HasDestMac);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, HiGigIsEnabled);
        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanIsSupported);
        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanIdSet);
        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanIdGet);

        mMethodOverride(m_AtEthPortOverride, ExpectedDestMacSet);
        mMethodOverride(m_AtEthPortOverride, ExpectedDestMacGet);
        mMethodOverride(m_AtEthPortOverride, HasExpectedDestMac);
        mMethodOverride(m_AtEthPortOverride, SohTransparentIsSupported);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort object = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, CounterLocalAddress);
        }

    mMethodsSet(object, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011DccSgmiiEthPort);
    }

AtEthPort Tha60290011DccSgmiiEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290011DccSgmiiEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011DccSgmiiEthPortObjectInit(newPort, portId, module);
    }

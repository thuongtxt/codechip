/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth
 *
 * File        : Tha60290011ClsEthPort.c
 *
 * Created Date: Dec 23, 2016
 *
 * Description : Implementation of Cls Interface Eth Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../pmc/Tha60290011ModulePmc.h"
#include "Tha60290011ClsInterfaceEthPortInternal.h"
#include "Tha60290011Axi4EthPortReg.h"
#include "Tha60290011EthPortReg.h"
#include "Tha60290011ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cClsDs1PktCounterType               0
#define cClsDs3PktCounterType               1
#define cClsCtrlPktCounterType              2
#define cClsSeqErrCounterType               4
#define cClsFcsErrCounterType               5
#define cClsDaErrDiscardPktCounterType      8
#define cClsSaErrDiscardPktCounterType      9
#define cClsEtypeErrDiscardPktCounterType   10
#define cClsLenErrPktCounterType            11

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)                            ((Tha60290011ClsInterfaceEthPort)self)
#define mRegWithClsBaseAddress(self, addr)     ((addr) + Tha60290011ClsInterfaceEthPortClsBaseAddress((ThaEthPort)self))
#define mBoolToStr(_bool)       ((_bool) ? "en" : "dis")

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011ClsInterfaceEthPortMethods m_methods;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtEthPortMethods    m_AtEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void HwRegToMac(uint32 msb16b, uint32 lsb32b, uint8 *address)
    {
    address[0] = (uint8)((msb16b & cBit15_8) >> 8);
    address[1] = (uint8)(msb16b & cBit7_0);

    address[2] = (uint8)((lsb32b & cBit31_24) >> 24);
    address[3] = (uint8)((lsb32b & cBit23_16) >> 16);
    address[4] = (uint8)((lsb32b & cBit15_8) >> 8);
    address[5] = (uint8)(lsb32b & cBit7_0);
    }

static void MacToHwReg(uint32 * msb16b, uint32 * lsb32b, uint8 *address)
    {
    mFieldIns(msb16b, cBit15_8, 8, address[0]);
    mFieldIns(msb16b, cBit7_0, 0, address[1]);

    mFieldIns(lsb32b, cBit31_24, 24, address[2]);
    mFieldIns(lsb32b, cBit23_16, 16, address[3]);
    mFieldIns(lsb32b, cBit15_8, 8, address[4]);
    mFieldIns(lsb32b, cBit7_0, 0, address[5]);
    }

static eAtModuleEthRet ReturnModeNotSupport(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    return ReturnModeNotSupport(self, address);
    }

static uint32 ClsBaseAddress(ThaEthPort self)
    {
    AtUnused(self);
    return 0x480000;
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed2500M) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed2500M) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed2500M;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    return (interface == cAtEthPortInterface2500BaseX) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterface2500BaseX;
    }

static eBool HasDestMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet EthTypeSet(ThaEthPort self, uint16 type)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_type);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_type_E_type_, type);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint16 EthTypeGet(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_type);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint16)mRegField(regVal, cAf6_upen_type_E_type_);
    }

static eAtRet EthLengthSet(ThaEthPort self, uint16 length)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_Eth_len_, length);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint16 EthLengthGet(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint16)mRegField(regVal, cAf6_upen_mask_Eth_len_);
    }

static eAtModuleEthRet EthSubtypeSet(ThaEthPort self, uint8 subtype)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_type);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_type_Sub_type_value_, subtype);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 EthSubtypeGet(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_type);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint8)mRegField(regVal, cAf6_upen_type_Sub_type_value_);
    }

static eAtModuleEthRet SequenceCheckEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_Sequence_mask_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool SequenceCheckIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_mask_Sequence_mask_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet LengthCheckEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_Length_mask_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LengthCheckIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_mask_Length_mask_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet EthTypeCheckEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_E_type_mask_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool EthTypeCheckIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_mask_E_type_mask_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet ExpectedSourceMacCheckEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_SA_mask_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool ExpectedSourceMacCheckIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_mask_SA_mask_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet ExpectedDestMacCheckEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_DA_mask_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool ExpectedDestMacCheckIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_mask_DA_mask_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SubTypeMaskSet(ThaEthPort self, uint8 mask)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_type);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_type_Sub_type_mask_, mask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 SubTypeMaskGet(ThaEthPort self)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_type);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint8)mRegField(regVal, cAf6_upen_type_Sub_type_mask_);
    }

static eAtModuleEthRet ExpectedSourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    uint32 lsb32Mac = 0;
    uint32 msb16Mac = 0;
    uint32 regVal = mChannelHwRead(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_dasa), cAtModuleEth);
    MacToHwReg(&msb16Mac, &lsb32Mac, address);
    mRegFieldSet(regVal, cAf6_upen_dasa_SA_bit47_32_, msb16Mac);
    mChannelHwWrite(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_dasa), regVal, cAtModuleEth);
    mChannelHwWrite(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_sa31), lsb32Mac, cAtModuleEth);
    return cAtOk;
    }

static eAtModuleEthRet ExpectedSourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    uint32 regVal   = mChannelHwRead(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_dasa), cAtModuleEth);
    uint32 msb16Mac = mRegField(regVal, cAf6_upen_dasa_SA_bit47_32_);
    uint32 lsb32Mac = mChannelHwRead(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_sa31), cAtModuleEth);
    HwRegToMac(msb16Mac, lsb32Mac, address);

    return cAtOk;
    }

static eAtModuleEthRet ExpectedDestMacAddressSet(AtEthPort self, uint8 *address)
    {
    uint32 lsb32Mac = 0;
    uint32 msb16Mac = 0;
    uint32 regVal = mChannelHwRead(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_dasa), cAtModuleEth);
    MacToHwReg(&msb16Mac, &lsb32Mac, address);
    mRegFieldSet(regVal, cAf6_upen_dasa_DA_bit47_32_, msb16Mac);
    mChannelHwWrite(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_dasa), regVal, cAtModuleEth);
    mChannelHwWrite(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_da31), lsb32Mac, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet ExpectedDestMacAddressGet(AtEthPort self, uint8 *address)
    {
    uint32 regVal = mChannelHwRead(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_dasa), cAtModuleEth);
    uint32 msb16Mac = mRegField(regVal, cAf6_upen_dasa_DA_bit47_32_);
    uint32 lsb32Mac = mChannelHwRead(self, mRegWithClsBaseAddress(self, cAf6Reg_upen_da31), cAtModuleEth);
    HwRegToMac(msb16Mac, lsb32Mac, address);

    return cAtOk;
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    return ExpectedSourceMacAddressSet(self, address);
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    return ExpectedSourceMacAddressGet(self, address);
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    return ExpectedDestMacAddressSet(self, address);
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    return ExpectedDestMacAddressGet(self, address);
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    return ExpectedDestMacCheckEnable((ThaEthPort)self, enable);
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    return ExpectedDestMacCheckIsEnabled((ThaEthPort)self);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(counterType);
    AtUnused(self);
    return cAtFalse;
    }

static eBool StickyIsSet(uint32 regVal, uint32 mask)
    {
    return (mask & regVal) ? cAtTrue : cAtFalse;
    }


static const char * ErrorToString(eBool isError)
    {
    return isError ? "set" : "clear";
    }

static eBool IsLatchPauGen(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatPauGen_Mask);
    }

static eBool IsLatchUsrRxVl(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatUsrRxVl_Mask);
    }

static eBool IsLatchUsrTxVl(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatUsrTxVl_Mask);
    }

static eBool IsLatchSmacTxRdEr(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxRdEr_Mask);
    }

static eBool IsLatchSmacTxReq(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxReq_Mask);
    }

static eBool IsLatchSmacTxVl(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxVl_Mask);
    }

static eBool IsLatchSmacRxEr(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacRxEr_Mask);
    }

static eBool IsLatchSmacRxDv(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacRxDv_Mask);
    }

static eBool IsLatchSmacTxEr(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxEr_Mask);
    }

static eBool IsLatchSmacTxEn(uint32 ethStickies)
    {
    return StickyIsSet(ethStickies, cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxEn_Mask);
    }

static void CounterDisplay(ThaEthPort self, uint32 relativeAddr, const char *name)
    {
    uint32 regAddr = relativeAddr + ThaEthPortMacBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    AtPrintc(cSevNormal, "%s: %d\n", name, regVal);
    }

static void MacCounterShow(ThaEthPort self, eBool isR2c)
    {
    uint32 r2c = isR2c ? 1 : 0;

    AtPrintc(cSevInfo, "\r\nEth Port debug counters:\r\n");

    CounterDisplay(self, cAf6_upen_debug_tx_pkt_at_gmii(r2c),      "  TX GMII packets           ");
    CounterDisplay(self, cAf6_upen_debug_tx_byte_at_gmii(r2c),     "  TX GMII bytes             ");
    CounterDisplay(self, cAf6_upen_debug_tx_pkt_at_mac_user(r2c),  "  TX MAC USER packets       ");
    CounterDisplay(self, cAf6_upen_debug_tx_byte_at_mac_user(r2c), "  TX MAC USER bytes         ");
    CounterDisplay(self, cAf6_upen_debug_rx_pkt_at_gmii(r2c),      "  RX GMII packets           ");
    CounterDisplay(self, cAf6_upen_debug_rx_byte_at_gmii(r2c),     "  RX GMII bytes             ");
    CounterDisplay(self, cAf6_upen_debug_rx_pkt_at_mac_user(r2c),  "  RX MAC USER packets       ");
    CounterDisplay(self, cAf6_upen_debug_rx_byte_at_mac_user(r2c), "  RX MAC USER bytes         ");
    }

static void MacStatusShow(ThaEthPort self)
    {
    uint32 regAddr = cAf6Reg_ip_eth_trip_speed_simple_mac_latch_status + ThaEthPortMacBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    AtPrintc(cSevInfo, "\r\nEth Port status:\r\n");
    AtPrintc(cSevNormal, "  IpEthTripSmacLatPauGen           : %s\n", ErrorToString(IsLatchPauGen(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatUsrRxVl          : %s\n", ErrorToString(IsLatchUsrRxVl(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatUsrTxVl          : %s\n", ErrorToString(IsLatchUsrTxVl(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacTxRdEr       : %s\n", ErrorToString(IsLatchSmacTxRdEr(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacTxReq        : %s\n", ErrorToString(IsLatchSmacTxReq(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacTxVl         : %s\n", ErrorToString(IsLatchSmacTxVl(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacRxEr         : %s\n", ErrorToString(IsLatchSmacRxEr(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacRxDv         : %s\n", ErrorToString(IsLatchSmacRxDv(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacTxEr         : %s\n", ErrorToString(IsLatchSmacTxEr(regVal)));
    AtPrintc(cSevNormal, "  IpEthTripSmacLatSmacTxEn         : %s\n", ErrorToString(IsLatchSmacTxEn(regVal)));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    }

static uint32 ClsStickiesGet(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_stk);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    if (r2c)
        mChannelHwWrite(self, regAddr, 0xFFFFFFFF, cAtModulePdh);
    return regVal;
    }

static eBool IsClsLengthErr(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Len_err_stk_Mask);
    }

static eBool IsClsSaMacErr(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Sa_err_stk_Mask);
    }

static eBool IsClsDaMacErr(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Da_err_stk_Mask);
    }

static eBool IsClsFcsErr(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Fcn_err_stk_Mask);
    }

static eBool IsClsSequenceErr(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Seq_err_stk_Mask);
    }

static eBool IsClsControlPacketOut(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Ctl_pkt_stk_Mask);
    }

static eBool IsClsDs3PacketOut(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Ds3_pkt_stk_Mask);
    }

static eBool IsClsDs1PacketOut(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Ds1_pkt_stk_Mask);
    }

static eBool IsClsFcsPacketIn(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Fcs_pkt_stk_Mask);
    }

static eBool IsClsEopValidInput(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Eop_vld_stk_Mask);
    }

static eBool IsClsPktValidInput(uint32 clsStickies)
    {
    return StickyIsSet(clsStickies, cAf6_upen_stk_Pkt_vld_stk_Mask);
    }

static uint32 ClassifiedPacketCounters(ThaEthPort self, uint8 counterType, eBool r2c)
    {
    uint32 regAddr = mRegWithClsBaseAddress(self, cAf6Reg_upen_count) + (r2c ? 0 : 0x16UL) + counterType;
    return mChannelHwRead(self, regAddr, cAtModuleEth);
    }

static uint32 Ds1PacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsDs1PktCounterType, r2c);
    }

static uint32 Ds3PacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsDs3PktCounterType, r2c);
    }

static uint32 CtrlPacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsCtrlPktCounterType, r2c);
    }

static uint32 SeqErrPacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsSeqErrCounterType, r2c);
    }

static uint32 FcsErrPacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsFcsErrCounterType, r2c);
    }

static uint32 DaErrDiscardPacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsDaErrDiscardPktCounterType, r2c);
    }

static uint32 SaErrDiscardPacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsSaErrDiscardPktCounterType, r2c);
    }

static uint32 LengthErrDiscardPacketCounter(ThaEthPort self, eBool r2c)
    {
    return ClassifiedPacketCounters(self, cClsLenErrPktCounterType, r2c);
    }

static void StickyPrint(const char * str, eBool isErrStk, eBool isSet)
    {
    uint8 color;
    if (isErrStk)
        color = isSet ? cSevCritical : cSevInfo;
    else
        color = isSet ? cSevInfo : cSevNormal;

    AtPrintc(cSevNormal, "%s", str);
    AtPrintc(color, "%s\n", isSet ? "set" : "clear");
    }

static void CounterPrint(const char * str, eBool isErrCnt, uint32 counterVal)
    {
    uint8 color;
    if (isErrCnt)
        color = (counterVal > 0) ? cSevCritical : cSevInfo;
    else
        color = (counterVal > 0) ? cSevInfo : cSevNormal;

    AtPrintc(cSevNormal, "%s", str);
    AtPrintc(color, "%u\n", counterVal);
    }

static void ClsConfigDebugShow(ThaEthPort self)
    {
    uint8 daMacAddr[6], saMacAddr[6];
    SourceMacAddressGet((AtEthPort)self, saMacAddr);
    DestMacAddressGet((AtEthPort)self, daMacAddr);

    AtPrintc(cSevInfo, "\r\nCLS Info\n");
    AtPrintc(cSevNormal, "  Da Mac Cls                       : %s\n", AtBytes2String(daMacAddr, cAtMacAddressLen, 16));
    AtPrintc(cSevNormal, "  Sa Mac Cls                       : %s\n", AtBytes2String(saMacAddr, cAtMacAddressLen, 16));
    AtPrintc(cSevNormal, "  Eth Type                         : 0x%X\n", EthTypeGet(self));
    AtPrintc(cSevNormal, "  Subtype                          : 0x%X\n", EthSubtypeGet(self));
    AtPrintc(cSevNormal, "  Eth Length                       : %d\n", EthLengthGet(self));
    AtPrintc(cSevNormal, "  Sequence check                   : %s\n", mBoolToStr(SequenceCheckIsEnabled(self)));
    AtPrintc(cSevNormal, "  Length check                     : %s\n", mBoolToStr(LengthCheckIsEnabled(self)));
    AtPrintc(cSevNormal, "  Eth Type check                   : %s\n", mBoolToStr(EthTypeCheckIsEnabled(self)));
    AtPrintc(cSevNormal, "  Source Mac check                 : %s\n", mBoolToStr(ExpectedSourceMacCheckIsEnabled(self)));
    AtPrintc(cSevNormal, "  Dest Mac check                   : %s\n", mBoolToStr(ExpectedDestMacCheckIsEnabled(self)));
    AtPrintc(cSevNormal, "  Subtype Mask                     : 0x%X\n", SubTypeMaskGet(self));
    }

static void ClsStickiesStatusDebugShow(ThaEthPort self)
    {
    uint32 clsStickies = ClsStickiesGet(self, cAtTrue);
    AtPrintc(cSevNormal, "\nStickies\n");
    StickyPrint("  Length error                     : ", cAtTrue, IsClsLengthErr(clsStickies));
    StickyPrint("  Expected SA Cnt error            : ", cAtTrue, IsClsSaMacErr(clsStickies));
    StickyPrint("  Expected DA Cnt error            : ", cAtTrue, IsClsDaMacErr(clsStickies));
    StickyPrint("  Fcs Cnt error                    : ", cAtTrue, IsClsFcsErr(clsStickies));
    StickyPrint("  Sequence error                   : ", cAtTrue, IsClsSequenceErr(clsStickies));
    StickyPrint("  Control packet output            : ", cAtFalse, IsClsControlPacketOut(clsStickies));
    StickyPrint("  Ds3 packet output                : ", cAtFalse, IsClsDs3PacketOut(clsStickies));
    StickyPrint("  DS1 packet output                : ", cAtFalse, IsClsDs1PacketOut(clsStickies));
    StickyPrint("  FCS packet input                 : ", cAtTrue, IsClsFcsPacketIn(clsStickies));
    StickyPrint("  Eop valid input                  : ", cAtFalse, IsClsEopValidInput(clsStickies));
    StickyPrint("  Pkt valid input                  : ", cAtFalse, IsClsPktValidInput(clsStickies));
    }

static uint8 PortToPmcSlice(ThaEthPort self)
    {
    if (Tha60290011ModuleEthIsAxi4EthPort((AtEthPort)self))
        return 0;
    if (Tha60290011ModuleEthIsDimSgmiiPort((AtEthPort)self))
        return 1;

    return 0;
    }

static ThaModulePmc PmcModule(ThaEthPort self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePmc);
    }

static void ClsCountersStatusDebugFromPmcShow(ThaEthPort self)
    {
    uint8 pmcSlice = PortToPmcSlice(self);

    AtPrintc(cSevNormal, "\nCounters\n");
    CounterPrint("  DS1 byte counter                 : ", cAtFalse, Tha60290011ModulePmcClaTxDe1BytesGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  DS3Ec1 byte counter              : ", cAtFalse, Tha60290011ModulePmcClaTxDe3BytesGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Control byte counter              : ", cAtFalse, Tha60290011ModulePmcClaTxControlPacketBytesGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  DS1 good packet counter          : ", cAtFalse, Tha60290011ModulePmcClaTxDe1GoodPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  DS3Ec1 good packet counter       : ", cAtFalse, Tha60290011ModulePmcClaTxDe3GoodPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Control good packet counter      : ", cAtFalse, Tha60290011ModulePmcClaTxControlGoodPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Err seq packet counter           : ", cAtTrue, Tha60290011ModulePmcClaTxSequenceErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Fcs err packet counter           : ", cAtTrue, Tha60290011ModulePmcClaTxFcsErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Expxected Da err discard packet counter   : ", cAtTrue, Tha60290011ModulePmcClaTxDAErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Expected Sa err discard packet counter    : ", cAtTrue, Tha60290011ModulePmcClaTxSAErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Len err packet counter                    : ", cAtTrue, Tha60290011ModulePmcClaTxLenErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Expected Len field err packet counter     : ", cAtTrue, Tha60290011ModulePmcClaTxLenFieldErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Type field err packet counter             : ", cAtTrue, Tha60290011ModulePmcClaTxEthTypeErrPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    CounterPrint("  Rx packet counter                         : ", cAtFalse, Tha60290011ModulePmcClaRxPacketsGet(PmcModule(self), pmcSlice, cAtTrue));
    }

static void ClsCountersStatusDebugFromInternalShow(ThaEthPort self)
    {
    AtPrintc(cSevNormal, "\nCounters\n");
    CounterPrint("  DS1 packet counter               : ", cAtFalse, Ds1PacketCounter(self, cAtTrue));
    CounterPrint("  DS3 packet counter               : ", cAtFalse, Ds3PacketCounter(self, cAtTrue));
    CounterPrint("  Control packet counter           : ", cAtFalse, CtrlPacketCounter(self, cAtTrue));
    CounterPrint("  Err seq counter                  : ", cAtTrue, SeqErrPacketCounter(self, cAtTrue));
    CounterPrint("  Fcs err counter                  : ", cAtTrue, FcsErrPacketCounter(self, cAtTrue));
    CounterPrint("  Expxected Da err discard packet counter   : ", cAtTrue, DaErrDiscardPacketCounter(self, cAtTrue));
    CounterPrint("  Expected Sa err discard packet counter    : ", cAtTrue, SaErrDiscardPacketCounter(self, cAtTrue));
    CounterPrint("  Len err counter                           : ", cAtTrue, LengthErrDiscardPacketCounter(self, cAtTrue));
    }

static void ClsCountersStatusDebugShow(ThaEthPort self)
    {
    if (ThaEthPortShouldGetCounterFromPmc((AtChannel)self))
        ClsCountersStatusDebugFromPmcShow(self);
    else
        ClsCountersStatusDebugFromInternalShow(self);
    }

static eAtRet ClsDefaultSet(AtChannel self)
    {
    ThaEthPort port = (ThaEthPort)self;

    mMethodsGet(mThis(self))->EthTypeSet(port, 0x8800);

    EthSubtypeSet(port, 0x80);
    SubTypeMaskSet(port, 0x80);

    SequenceCheckEnable(port, cAtFalse);
    LengthCheckEnable(port, cAtFalse);
    EthTypeCheckEnable(port, cAtFalse);
    ExpectedSourceMacCheckEnable(port, cAtFalse);
    ExpectedDestMacCheckEnable(port, cAtFalse);

    return cAtOk;
    }

static eAtRet Debug(AtChannel self)
    {
    ThaEthPort port = (ThaEthPort)self;
    ClsConfigDebugShow(port);
    ClsStickiesStatusDebugShow(port);
    ClsCountersStatusDebugShow(port);
    MacStatusShow(port);
    MacCounterShow(port, cAtTrue);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ClsDefaultSet(self);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, HasDestMac);
        mMethodOverride(m_AtEthPortOverride, HiGigIsEnabled);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static void MethodsInit(AtEthPort self)
    {
    Tha60290011ClsInterfaceEthPort port = (Tha60290011ClsInterfaceEthPort)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ClsBaseAddress);
        mMethodOverride(m_methods, EthTypeSet);
        }

    mMethodsSet(port, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ClsInterfaceEthPort);
    }

AtEthPort Tha60290011ClsInterfaceEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthTypeSet(ThaEthPort self, uint16 type)
    {
    if (self)
        return mMethodsGet(mThis(self))->EthTypeSet(self, type);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthLengthSet(ThaEthPort self, uint16 length)
    {
    if (self)
        return EthLengthSet(self, length);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthSubTypeSet(ThaEthPort self, uint8 subtype)
    {
    if (self)
        return EthSubtypeSet(self, subtype);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortSequenceCheckEnable(ThaEthPort self, eBool enable)
    {
    if (self)
        return SequenceCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortLengthCheckEnable(ThaEthPort self, eBool enable)
    {
    if (self)
        return LengthCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthTypeCheckEnable(ThaEthPort self, eBool enable)
    {
    if (self)
        return EthTypeCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortSourceMacCheckEnable(ThaEthPort self, eBool enable)
    {
    if (self)
        return ExpectedSourceMacCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortDestMacCheckEnable(ThaEthPort self, eBool enable)
    {
    if (self)
        return ExpectedDestMacCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortSubTypeMaskCheckSet(ThaEthPort self, uint8 mask)
    {
    if (self)
        return SubTypeMaskSet(self, mask);
    return cAtErrorNullPointer;
    }

uint32 Tha60290011ClsInterfaceEthPortClsBaseAddress(ThaEthPort self)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClsBaseAddress(self);
    return 0x0;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortSourceMacAddressSet(ThaEthPort self, uint8 *address)
    {
    if (self)
        return SourceMacAddressSet((AtEthPort)self, address);
    return cAtErrorNullPointer;
    }

eAtModuleEthRet Tha60290011ClsInterfaceEthPortDestMacAddressSet(ThaEthPort self, uint8 *address)
    {
    if (self)
        return DestMacAddressSet((AtEthPort)self, address);
    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011ModuleEth.c
 *
 * Created Date: Aug 10, 2016
 *
 * Description : ETH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEth.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210031/man/Tha60210031Device.h"
#include "../../Tha60210031/eth/Tha60210031ModuleEth.h"
#include "../../Tha60290021/eth/Tha60290021ModuleEth.h"
#include "../../Tha60290021/common/Tha6029DccKbyte.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "../man/Tha60290011DeviceReg.h"
#include "../man/Tha60290011DeviceInternal.h"
#include "Tha60290011ModuleEthInternal.h"
#include "Tha60290011ModuleEth.h"
#include "Tha60290011SerdesBackplaneEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_Reg_mac_add_47_32_ctrl 0x101
#define cAf6_mac_add_47_32_ctrl_DCCEnMode_Mask  cBit29
#define cAf6_mac_add_47_32_ctrl_DCCEnMode_Shift 29
#define cMDIO1GBaseAddress                  0xF54000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290011ModuleEth)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290011ModuleEthMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods     = NULL;
static const tAtModuleEthMethods *m_AtModuleEthMethods  = NULL;
static const tAtModuleMethods       *m_AtModuleMethods     = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SerdesApsSupported(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 6;
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool IsDimSgmiiPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return (portId == cTha60290011ModuleEthDimCtrlPortId) ? cAtTrue : cAtFalse;
    }

static eBool IsDccSgmiiPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return (portId == cTha60290011ModuleEthDccPortId) ? cAtTrue : cAtFalse;
    }

static eBool IsSgmiiPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return ((IsDimSgmiiPort(self, portId)) || (IsDccSgmiiPort(self, portId))) ? cAtTrue : cAtFalse;
    }

static uint8 LocalSgmiiPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    if (IsDimSgmiiPort(self, portId)) return 0;
    if (IsDccSgmiiPort(self, portId)) return 1;
    return cInvalidUint8;
    }

static eBool IsAxi4EthPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return (portId == cTha60290011ModuleEth2Dot5GPortId) ? cAtTrue : cAtFalse;
    }

static eBool IsPwPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return (portId == cTha60290011ModuleEthPwPortId) ? cAtTrue : cAtFalse;
    }

static eBool IsSerdesBackplaneActivePort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    if (portId == cTha60290011ModuleEthSerdesBackPlanActivePortId)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSerdesBackplaneStbPort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    if (portId == cTha60290011ModuleEthSerdesBackPlanStandbyPortId)
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsSerdesBackplanePort(AtModuleEth self, uint8 portId)
    {
    if (IsSerdesBackplaneActivePort(self, portId) || IsSerdesBackplaneStbPort(self, portId))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 LocalSerdesBackplanePort(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    if (IsSerdesBackplaneActivePort(self, portId))
        return 0;

    if (IsSerdesBackplaneStbPort(self, portId))
        return 1;

    return cInvalidUint8;
    }

static AtSerdesManager SerdesManager(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceSerdesManagerGet(device);
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    AtModuleEth module = (AtModuleEth)self;
    uint8 portId = (uint8)AtChannelIdGet((AtChannel)port);

    if (IsDimSgmiiPort(module, portId))
        return 0x400100;

    if (IsDccSgmiiPort(module, portId))
        return 0x400200;

    if (IsAxi4EthPort(module, portId))
        return 0x400000;

    if (IsPwPort(module, portId))
        return 0x3C1000;

    if (IsSerdesBackplaneActivePort(module, portId))
        return cXfiActSerdesTuningBaseAddress;

    if (IsSerdesBackplaneStbPort(module, portId))
        return cXfiStbSerdesTuningBaseAddress;

    return cInvalidUint32;
    }

static AtEthPort DccSgmiiPortObjectCreate(AtModuleEth self, uint8 portId)
    {
    return Tha60290011DccSgmiiEthPortNew(portId, self);
    }

static AtEthPort PortObjectCreate(AtModuleEth self, uint8 portId)
    {
    if (IsDimSgmiiPort(self, portId))
        return Tha60290011DimSgmiiEthPortNew(portId, self);

    if (IsDccSgmiiPort(self, portId))
        return DccSgmiiPortObjectCreate(self, portId);

    if (IsAxi4EthPort(self, portId))
        return mMethodsGet(mThis(self))->Axi4PortObjectCreate(mThis(self), portId);

    if (IsPwPort(self, portId))
        return Tha60290011BackplaneEthPortNew(portId, self);

    if (IsSerdesBackplanePort(self, portId))
        return mMethodsGet(mThis(self))->SerdesBackplaneEthPortObjectCreate(mThis(self), portId);

    return NULL;
    }

static AtSerdesController SerdesForPort(AtModuleEth self, uint8 portId)
    {
    AtSerdesManager serdesManager = SerdesManager(self);

    if (IsSgmiiPort(self, portId))
        return Tha60290011SerdesManagerSgmiiSerdesController(serdesManager, LocalSgmiiPort(self, portId));

    if (IsAxi4EthPort(self, portId))
        return Tha60290011SerdesManagerAxi4SerdesController(serdesManager);

    if (IsPwPort(self, portId))
        return Tha60290011SerdesManagerBackplaneSerdesController(serdesManager, 0);

    if (IsSerdesBackplaneActivePort(self, portId))
        return Tha60290011SerdesManagerBackplaneSerdesController(serdesManager, 0);

    if (IsSerdesBackplaneStbPort(self, portId))
        return Tha60290011SerdesManagerBackplaneSerdesController(serdesManager, 1);

    return NULL;
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    AtEthPort port = PortObjectCreate(self, portId);
    AtSerdesController serdes = SerdesForPort(self, portId);
    AtSerdesControllerPhysicalPortSet(serdes, (AtChannel)port);
    return port;
    }

static uint32 NumSerdesControllers(AtModuleEth self)
    {
    return AtSerdesManagerNumSerdesControllers(SerdesManager(self));
    }

static AtSerdesController SerdesController(AtModuleEth self, uint32 serdesId)
    {
    return AtSerdesManagerSerdesControllerGet(SerdesManager(self), serdesId);
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    /* Give control to device SERDES manager */
    AtUnused(self);
    AtUnused(ethPort);
    return NULL;
    }

static eBool PortIsInternalMac(ThaModuleEth self, uint8 portId)
    {
    return IsSerdesBackplanePort((AtModuleEth)self, portId) ? cAtFalse : cAtTrue;
    }

static eBool SerdesApsSupportedOnPort(AtModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    return (AtChannelIdGet((AtChannel)port) == 0) ? cAtTrue : cAtFalse;
    }

static uint32 DiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);

    if (serdesId == cTha60290011XfiSerdesId0) return 0xF20000;
    if (serdesId == cTha60290011XfiSerdesId1) return 0xF30000;

    return cInvalidUint32;
    }

static uint32 SerdesDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF10000; /* BaseAddress for XFI */
    }

static AtDrp DrpCreate(AtModuleEth self, uint32 baseAddress)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    const uint32 DrpBaseAddress = baseAddress;
    AtDrp drp = AtDrpNew(DrpBaseAddress, hal);

    AtDrpNumberOfSerdesControllersSet(drp, 1);
    AtDrpNeedSelectPortSet(drp, cAtFalse);
    return drp;
    }

static AtDrp SerdesAxi4DrpCreate(AtModuleEth self)
    {
    return DrpCreate(self, 0xF80000);
    }

static AtDrp SerdesSgmiiDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    return DrpCreate(self, (serdesId == cTha60290011SgmiiSerdesId0) ? 0xF90000 : 0xfA0000);
    }

static AtDrp SerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(AtModuleEthSerdesController(self, serdesId));

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterface2500BaseX)
        return SerdesAxi4DrpCreate(self);

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceSgmii)
        return SerdesSgmiiDrpCreate(self, serdesId);

    return m_AtModuleEthMethods->SerdesDrpCreate(self, serdesId);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    uint8 portId = (uint8)AtChannelIdGet((AtChannel)port);

    if (IsAxi4EthPort(self, portId))
        return cAtEthPortInterface2500BaseX;

    if (IsPwPort(self, portId) || IsSerdesBackplanePort(self, portId))
        return cAtEthPortInterfaceXGMii;

    if (IsSgmiiPort(self, portId))
        return cAtEthPortInterfaceSgmii;

    return m_AtModuleEthMethods->PortDefaultInterface(self, port);
    }

static uint32 SerdesMdioBaseAddress(AtModuleEth self, AtSerdesController serdes)
    {
    AtSerdesManager manager = SerdesManager(self);

    if (Tha60290011SerdesManagerSerdesIsBackplane(manager, AtSerdesControllerIdGet(serdes)))
        return cXfiSerdesMdioBaseAddress;

    return cSgmiiSerdesMdioBaseAddress;
    }

static uint32 MdioLocalPortId(AtSerdesManager serdesManager, uint32 serdesId)
    {
    if (Tha60290011SerdesManagerSerdesIsAxi4(serdesManager, serdesId))
        return 0;

    return Tha60290011SerdesManagerSgmiiSerdesLocalId(serdesManager, serdesId) + 1;
    }

static uint32 MdioPortAddress(AtMdio self, uint32 selectedPortId)
    {
    AtUnused(self);
    return cBit0 << selectedPortId;
    }

static const tAtMdioAddressCalculator *MdioAddressCalculator(void)
    {
    static tAtMdioAddressCalculator calculator;
    static tAtMdioAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(calculator));
    calculator.PortAddress = MdioPortAddress;
    pCalculator = &calculator;

    return pCalculator;
    }

static uint32 MDIO1GBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return cMDIO1GBaseAddress;
    }

static AtMdio SgmiiSerdesMdioCreate(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    uint32 mdio1GBaseAddress = MDIO1GBaseAddress(self);
    AtMdio mdio = AtMdioSgmiiNew(mdio1GBaseAddress, hal);
    AtMdioAddressCalculatorSet(mdio, MdioAddressCalculator());
    AtMdioSimulateEnable(mdio, AtDeviceIsSimulated(device));
    return mdio;
    }

static AtMdio SerdesSgmiiMdio(AtModuleEth self, AtSerdesManager serdesManager, uint32 serdesId)
    {
    if (mThis(self)->sgmiiMdio == NULL)
        mThis(self)->sgmiiMdio = SgmiiSerdesMdioCreate(self);

    AtMdioPortSelect(mThis(self)->sgmiiMdio, MdioLocalPortId(serdesManager, serdesId));
    return mThis(self)->sgmiiMdio;
    }

static AtMdio SerdesMdio(AtModuleEth self, AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    AtSerdesManager serdesManager = SerdesManager(self);

    if (Tha60290011SerdesManagerSerdesIsBackplane(serdesManager, serdesId))
        return m_AtModuleEthMethods->SerdesMdio(self, serdes);

    return SerdesSgmiiMdio(self, serdesManager, serdesId);
    }

static void Delete(AtObject self)
    {
    AtMdioDelete(mThis(self)->sgmiiMdio);
    mThis(self)->sgmiiMdio = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    AtCoderEncodeString(encoder, AtMdioToString(mThis(self)->sgmiiMdio), "sgmiiMdio");
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    AtUnused(self);
    return 0;
    }

static eBool RxFsmPinSelV2IsSupported(ThaModuleEth self)
    {
    return Tha60290011DeviceRxFsmPinSelV2IsSupported(AtModuleDeviceGet((AtModule)self));
    }

static void RxFSMpinEnMaskShift(ThaModuleEth self, uint32 *mask, uint32 *shift)
    {
    if (RxFsmPinSelV2IsSupported(self))
        {
        *mask = cAf6_o_control0_EnRxFSMpin_V2_Mask;
        *shift = cAf6_o_control0_EnRxFSMpin_V2_Shift;
        return;
        }

    *mask = cAf6_o_control0_EnRxFSMpin_Mask;
    *shift = cAf6_o_control0_EnRxFSMpin_Shift;
    }

static eAtRet FsmControlDefaultSet(ThaModuleEth self)
    {
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_o_control0_dup_tx_10g_, 1);
    mRegFieldSet(regVal, cAf6_o_control0_sel_fsm_card_, 1);
    mRegFieldSet(regVal, cAf6_o_control0_cpu_sel_card_, 0);

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet TxFsmPinEnable(ThaModuleEth self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!RxFsmPinSelV2IsSupported(self))
        return cAtOk;

    regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_o_control0_sel_fsm_card_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet TxFsmPinForceStandby(ThaModuleEth self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!RxFsmPinSelV2IsSupported(self))
        return cAtOk;

    regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_o_control0_cpu_sel_card_, enable ? 1 : 0);

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet RxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    uint32 field_Mask, field_Shift;
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    RxFSMpinEnMaskShift(self, &field_Mask, &field_Shift);
    mRegFieldSet(regVal, field_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool RxFsmPinIsEnabled(ThaModuleEth self)
    {
    uint32 field_Mask, field_Shift;
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    RxFSMpinEnMaskShift(self, &field_Mask, &field_Shift);
    return mRegField(regVal, field_) ? cAtTrue : cAtFalse;
    }

static eBool RxFsmPinStatusGet(ThaModuleEth self)
    {
    uint32 regVal, fsmPin1Status;

    regVal = mModuleHwRead(self, cTopBaseAddress + cAf6Reg_status_top_Base);
    fsmPin1Status = mRegField(regVal, cAf6_status_top_rxfsm1sta_);

    return fsmPin1Status ? cAtTrue : cAtFalse;
    }

static void RxFSMpinSelectMaskShift(ThaModuleEth self, uint32 *mask, uint32 *shift)
    {
    if (RxFsmPinSelV2IsSupported(self))
        {
        *mask = cAf6_o_control0_Sel10GPort_V2_Mask;
        *shift = cAf6_o_control0_Sel10GPort_V2_Shift;
        return;
        }

    *mask = cAf6_o_control0_Sel10GPort_Mask;
    *shift = cAf6_o_control0_Sel10GPort_Shift;
    }

static eAtRet RxBackPlaneSerdesSelect(ThaModuleEth self, uint32 serId)
    {
    uint32 field_Mask, field_Shift;
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    RxFSMpinSelectMaskShift(self, &field_Mask, &field_Shift);
    mRegFieldSet(regVal, field_, serId);
    mModuleHwWrite(self, regAddr, regVal);

    RxFsmPinEnable(self, cAtFalse);
    return cAtOk;
    }

static uint32 RxBackPlaneSelectedSerdes(ThaModuleEth self)
    {
    uint32 field_Mask, field_Shift;
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    RxFSMpinSelectMaskShift(self, &field_Mask, &field_Shift);
    return  mRegField(regVal, field_);
    }

static eBool FsmIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet SohTransparentEnable(AtModuleEth self, eBool enable)
    {
    uint32 address, regVal, binVal;

    binVal = enable ? 1:0;
    address = cAf6_Reg_mac_add_47_32_ctrl;
    regVal = mModuleHwRead(self, address);
    mFieldIns(&regVal, cAf6_mac_add_47_32_ctrl_DCCEnMode_Mask, cAf6_mac_add_47_32_ctrl_DCCEnMode_Shift, binVal);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static eBool SohTransparentIsEnabled(AtModuleEth self)
    {
    uint32 address, regVal, binVal;

    address = cAf6_Reg_mac_add_47_32_ctrl;
    regVal = mModuleHwRead(self, address);
    mFieldGet(regVal, cAf6_mac_add_47_32_ctrl_DCCEnMode_Mask, cAf6_mac_add_47_32_ctrl_DCCEnMode_Shift, uint32, &binVal);

    return binVal ? cAtTrue : cAtFalse;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (Tha60290011ModuleEthSohTransparentIsReady((AtModuleEth)self))
        ret = Tha60290011ModuleEthDccEnable((AtModuleEth)self, cAtTrue);

    if (RxFsmPinSelV2IsSupported((ThaModuleEth)self))
        FsmControlDefaultSet((ThaModuleEth)self);

    return ret;
    }

static eBool PortCauseInterrupt(ThaModuleEth self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & (cAf6_global_interrupt_status_XilinxSerdesBackplane0_IntStatus_Mask|
                       cAf6_global_interrupt_status_XilinxSerdesBackplane1_IntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static void PortInterruptProcess(ThaModuleEth self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(hal);
    if (glbIntr & cAf6_global_interrupt_status_XilinxSerdesBackplane0_IntStatus_Mask)
        {
        AtChannelInterruptProcess((AtChannel)AtModuleEthPortGet((AtModuleEth)self, cTha60290011ModuleEthSerdesBackPlanActivePortId), 0);
        }

    if (glbIntr & cAf6_global_interrupt_status_XilinxSerdesBackplane1_IntStatus_Mask)
        {
        AtChannelInterruptProcess((AtChannel)AtModuleEthPortGet((AtModuleEth)self, cTha60290011ModuleEthSerdesBackPlanStandbyPortId), 0);
        }
    }

static eBool InterruptIsSupported(ThaModuleEth self)
    {
    return Tha60290011DeviceEthInterruptIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static AtEthPort DccEthPortGet(ThaModuleEth self)
    {
    return AtModuleEthPortGet((AtModuleEth)self, cTha60290011ModuleEthDccPortId);
    }

static eBool DccEthMaxLengthConfigurationIsSupported(ThaModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60290011DeviceDccEthMaxLengthConfigurationIsSupported(device);
    }

static eAtRet TxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthTxDccPacketMaxLengthSet(self, maxLength);

    return cAtErrorModeNotSupport;
    }

static uint32 TxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthTxDccPacketMaxLengthGet(self);

    return 0;
    }

static eAtRet RxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthRxDccPacketMaxLengthSet(self, maxLength);

    return cAtErrorModeNotSupport;
    }

static uint32 RxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthRxDccPacketMaxLengthGet(self);

    return 0;
    }

static AtEthPort Axi4PortObjectCreate(Tha60290011ModuleEth self, uint8 portId)
    {
    return Tha60290011Axi4EthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort SerdesBackplaneEthPortObjectCreate(Tha60290011ModuleEth self, uint8 portId)
    {
    return Tha60290011SerdesBackplaneEthPortNew(portId, (AtModuleEth)self);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, DiagBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionSupportsSerdesAps);
        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, RxFsmPinEnable);
        mMethodOverride(m_ThaModuleEthOverride, RxFsmPinIsEnabled);
        mMethodOverride(m_ThaModuleEthOverride, RxFsmPinStatusGet);
        mMethodOverride(m_ThaModuleEthOverride, FsmIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortIsInternalMac);
        mMethodOverride(m_ThaModuleEthOverride, PortCauseInterrupt);
        mMethodOverride(m_ThaModuleEthOverride, PortInterruptProcess);
        mMethodOverride(m_ThaModuleEthOverride, InterruptIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, DccEthPortGet);
        mMethodOverride(m_ThaModuleEthOverride, TxDccPacketMaxLengthSet);
        mMethodOverride(m_ThaModuleEthOverride, TxDccPacketMaxLengthGet);
        mMethodOverride(m_ThaModuleEthOverride, RxDccPacketMaxLengthSet);
        mMethodOverride(m_ThaModuleEthOverride, RxDccPacketMaxLengthGet);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, m_AtModuleEthMethods, sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, NumSerdesControllers);
        mMethodOverride(m_AtModuleEthOverride, SerdesController);
        mMethodOverride(m_AtModuleEthOverride, SerdesApsSupportedOnPort);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesApsSupported);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdioBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdio);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleEth(self);
    OverrideAtModuleEth(self);
    }

static void MethodsInit(Tha60290011ModuleEth self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Axi4PortObjectCreate);
        mMethodOverride(m_methods, SerdesBackplaneEthPortObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011ModuleEth);
    }

AtModuleEth Tha60290011ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60290011ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011ModuleEthObjectInit(newModule, device);
    }

eBool Tha60290011ModuleEthIsAxi4EthPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return IsAxi4EthPort(module, portId);
        }

    return cAtFalse;
    }

uint8 Tha60290011ModuleEthLocalSgmiiPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return LocalSgmiiPort(module, portId);
        }
    return cInvalidUint8;
    }

eBool Tha60290011ModuleEthIsPwPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return IsPwPort(module, portId);
        }

    return cAtFalse;
    }

eBool Tha60290011ModuleEthIsSgmiiPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return IsSgmiiPort(module, portId);
        }

    return cAtFalse;
    }

eBool Tha60290011ModuleEthIsDimSgmiiPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return IsDimSgmiiPort(module, portId);
        }

    return cAtFalse;
    }

eBool Tha60290011ModuleEthIsDccSgmiiPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return IsDccSgmiiPort(module, portId);
        }

    return cAtFalse;
    }

eAtRet Tha60290011ModuleEthRxBackPlaneSerdesSelect(ThaModuleEth self, uint32 serdesId)
    {
    return RxBackPlaneSerdesSelect(self, serdesId);
    }

uint32 Tha60290011ModuleEthRxBackPlaneSelectedSerdes(ThaModuleEth self)
    {
    return RxBackPlaneSelectedSerdes(self);
    }

AtEthPort Tha60290011ModuleEthSgmiiDccPortGet(AtModuleEth self)
    {
    return ThaModuleEthDccEthPortGet((ThaModuleEth)self);
    }

eBool Tha60290011ModuleEthSohTransparentIsReady(AtModuleEth self)
    {
    return Tha60290011DeviceEthSohTransparentIsReady(AtModuleDeviceGet((AtModule)self));
    }

eAtRet Tha60290011ModuleEthDccEnable(AtModuleEth self, eBool enable)
    {
    return SohTransparentEnable(self, enable);
    }

eBool Tha60290011ModuleEthDccIsEnabled(AtModuleEth self)
    {
    return SohTransparentIsEnabled(self);
    }

uint8 Tha60290011ModuleEthSerdesBackplanePortLocalId(AtModuleEth self, uint8 portId)
    {
    return LocalSerdesBackplanePort(self, portId);
    }

AtSerdesController Tha60290011ModuleEthSerdesForPort(AtModuleEth self, uint8 portId)
    {
    return SerdesForPort(self, portId);
    }

eBool Tha60290011ModuleEthIsSerdesBackplanePort(AtEthPort port)
    {
    if (port)
        {
        AtChannel channel = (AtChannel)port;
        AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(channel);
        return IsSerdesBackplanePort(ethModule, (uint8)AtChannelIdGet(channel));
        }

    return cAtFalse;
    }

eAtRet Tha60290011ModuleEthTxFsmPinEnable(ThaModuleEth self, eBool enable)
    {
    if (self)
        return TxFsmPinEnable(self, enable);

    return cAtErrorNullPointer;
    }

eAtRet Tha60290011ModuleEthTxFsmPinForceStandby(ThaModuleEth self, eBool enable)
    {
    if (self)
        return TxFsmPinForceStandby(self, enable);

    return cAtErrorNullPointer;
    }

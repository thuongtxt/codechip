/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH port
 * 
 * File        : Tha60290011SerdesBackplaneEthPort.h
 * 
 * Created Date: Mar 21, 2017
 *
 * Description : Serdes backplane eth port interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SERDESBACKPLANEETHPORT_H_
#define _THA60290011SERDESBACKPLANEETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290011SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SERDESBACKPLANEETHPORT_H_ */


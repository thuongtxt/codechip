/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290011SerdesBackplaneEthPortInternal.h
 * 
 * Created Date: May 27, 2019
 *
 * Description : SERDES Backplane ETH port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011SERDESBACKPLANEETHPORTINTERNAL_H_
#define _THA60290011SERDESBACKPLANEETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/eth/Tha60290021SerdesBackplaneEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011SerdesBackplaneEthPort
    {
    tTha60290021SerdesBackplaneEthPort super;
    }tTha60290011SerdesBackplaneEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290011SerdesBackplaneEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011SERDESBACKPLANEETHPORTINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011EthPort.c
 *
 * Created Date: Nov 9, 2016
 *
 * Description : Ethernet port for 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPortReg.h"
#include "Tha60290011ModuleEth.h"
#include "Tha60290011EthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterField, counterType)          \
    if (AtChannelCounterIsSupported(self, counterType)) \
        {                                               \
        value = CounterGetFunc(self, counterType);      \
        if (counters)                                   \
            counters->counterField = value;             \
        }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods          m_AtChannelOverride;
static tAtEthPortMethods          m_AtEthPortOverride;
static tThaEthPortMethods         m_ThaEthPortOverride;

/* Save super implementation */
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + ThaEthPortMacBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 hwActivate = activate ? 1 : 0;
    mFieldIns(&regVal, cThaIpEthTripSmacTxActMask, cThaIpEthTripSmacTxActShift, hwActivate);
    mFieldIns(&regVal, cThaIpEthTripSmacRxActMask, cThaIpEthTripSmacRxActShift, hwActivate);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    if (serdes == AtEthPortSerdesController(self))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    return AtEthPortSerdesController(self);
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    if (serdes == AtEthPortSerdesController(self))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    return AtEthPortSerdesController(self);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    if (serdes == NULL)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtFalse;
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortLinkStatusNotSupported;
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + ThaEthPortMacBaseAddress(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopinMask, cThaIpEthTripSmacLoopinShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + ThaEthPortMacBaseAddress(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopoutMask, cThaIpEthTripSmacLoopoutShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + ThaEthPortMacBaseAddress(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cThaIpEthTripSmacLoopoutMask) ? cAtTrue : cAtFalse;
    }

static eBool LoopInIsEnabled(ThaEthPort self)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + ThaEthPortMacBaseAddress(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cThaIpEthTripSmacLoopinMask) ? cAtTrue : cAtFalse;
    }

static uint32 DefaultOffset(ThaEthPort port)
    {
    AtUnused(port);
    return 0;
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
    {
    uint32 value;
    tAtEthPortCounters *counters;
    uint32 (*CounterGetFunc)(AtChannel, uint16) = AtChannelCounterGet;

    /* Initialize counters */
    counters = (tAtEthPortCounters *)pAllCounters;
    if (counters)
        AtOsalMemInit(counters, 0, sizeof(tAtEthPortCounters));

    /* Put all of supported counters */
    AtChannelAllCountersLatchAndClear(self, clear);

    if (clear)
        CounterGetFunc = AtChannelCounterClear;

    mPutCounter(txPackets, cAtEthPortCounterTxPackets);
    mPutCounter(txBytes, cAtEthPortCounterTxBytes);
    mPutCounter(txGoodPackets, cAtEthPortCounterTxGoodPackets);
    mPutCounter(txGoodBytes, cAtEthPortCounterTxGoodBytes);
    mPutCounter(txOamPackets, cAtEthPortCounterTxOamPackets);
    mPutCounter(txPeriodOamPackets, cAtEthPortCounterTxPeriodOamPackets);
    mPutCounter(txPwPackets, cAtEthPortCounterTxPwPackets);
    mPutCounter(txPacketsLen0_64, cAtEthPortCounterTxPacketsLen0_64);
    mPutCounter(txPacketsLen65_127, cAtEthPortCounterTxPacketsLen65_127);
    mPutCounter(txPacketsLen128_255, cAtEthPortCounterTxPacketsLen128_255);
    mPutCounter(txPacketsLen256_511, cAtEthPortCounterTxPacketsLen256_511);
    mPutCounter(txPacketsLen512_1024, cAtEthPortCounterTxPacketsLen512_1024);
    mPutCounter(txPacketsLen1025_1528, cAtEthPortCounterTxPacketsLen1025_1528);
    mPutCounter(txPacketsLen1529_2047, cAtEthPortCounterTxPacketsLen1529_2047);
    mPutCounter(txPacketsJumbo, cAtEthPortCounterTxPacketsJumbo);
    mPutCounter(txTopPackets, cAtEthPortCounterTxTopPackets);
    mPutCounter(txPausePackets, cAtEthPortCounterTxPausePackets);
    mPutCounter(txBrdCastPackets, cAtEthPortCounterTxBrdCastPackets);
    mPutCounter(txMultCastPackets, cAtEthPortCounterTxMultCastPackets);
    mPutCounter(txUniCastPackets, cAtEthPortCounterTxUniCastPackets);
    mPutCounter(txOversizePackets, cAtEthPortCounterTxOversizePackets);
    mPutCounter(txUndersizePackets, cAtEthPortCounterTxUndersizePackets);
    mPutCounter(txDiscardedPackets, cAtEthPortCounterTxDiscardedPackets);
    mPutCounter(txOverFlowDroppedPackets, cAtEthPortCounterTxOverFlowDroppedPackets);
    mPutCounter(rxPackets, cAtEthPortCounterRxPackets);
    mPutCounter(rxBytes, cAtEthPortCounterRxBytes);
    mPutCounter(rxGoodPackets, cAtEthPortCounterRxGoodPackets);
    mPutCounter(rxGoodBytes, cAtEthPortCounterRxGoodBytes);
    mPutCounter(rxErrEthHdrPackets, cAtEthPortCounterRxErrEthHdrPackets);
    mPutCounter(rxErrBusPackets, cAtEthPortCounterRxErrBusPackets);
    mPutCounter(rxErrFcsPackets, cAtEthPortCounterRxErrFcsPackets);
    mPutCounter(rxOversizePackets, cAtEthPortCounterRxOversizePackets);
    mPutCounter(rxUndersizePackets, cAtEthPortCounterRxUndersizePackets);
    mPutCounter(rxPacketsLen0_64, cAtEthPortCounterRxPacketsLen0_64);
    mPutCounter(rxPacketsLen65_127, cAtEthPortCounterRxPacketsLen65_127);
    mPutCounter(rxPacketsLen128_255, cAtEthPortCounterRxPacketsLen128_255);
    mPutCounter(rxPacketsLen256_511, cAtEthPortCounterRxPacketsLen256_511);
    mPutCounter(rxPacketsLen512_1024, cAtEthPortCounterRxPacketsLen512_1024);
    mPutCounter(rxPacketsLen1025_1528, cAtEthPortCounterRxPacketsLen1025_1528);
    mPutCounter(rxPacketsLen1529_2047, cAtEthPortCounterRxPacketsLen1529_2047);
    mPutCounter(rxPacketsJumbo, cAtEthPortCounterRxPacketsJumbo);
    mPutCounter(rxPwUnsupportedPackets, cAtEthPortCounterRxPwUnsupportedPackets);
    mPutCounter(rxErrPwLabelPackets, cAtEthPortCounterRxErrPwLabelPackets);
    mPutCounter(rxDiscardedPackets, cAtEthPortCounterRxDiscardedPackets);
    mPutCounter(rxPausePackets, cAtEthPortCounterRxPausePackets);
    mPutCounter(rxErrPausePackets, cAtEthPortCounterRxErrPausePackets);
    mPutCounter(rxUniCastPackets, cAtEthPortCounterRxUniCastPackets);
    mPutCounter(rxBrdCastPackets, cAtEthPortCounterRxBrdCastPackets);
    mPutCounter(rxMultCastPackets, cAtEthPortCounterRxMultCastPackets);
    mPutCounter(rxArpPackets, cAtEthPortCounterRxArpPackets);
    mPutCounter(rxOamPackets, cAtEthPortCounterRxOamPackets);
    mPutCounter(rxEfmOamPackets, cAtEthPortCounterRxEfmOamPackets);
    mPutCounter(rxErrEfmOamPackets, cAtEthPortCounterRxErrEfmOamPackets);
    mPutCounter(rxEthOamType1Packets, cAtEthPortCounterRxEthOamType1Packets);
    mPutCounter(rxEthOamType2Packets, cAtEthPortCounterRxEthOamType2Packets);
    mPutCounter(rxIpv4Packets, cAtEthPortCounterRxIpv4Packets);
    mPutCounter(rxErrIpv4Packets, cAtEthPortCounterRxErrIpv4Packets);
    mPutCounter(rxIcmpIpv4Packets, cAtEthPortCounterRxIcmpIpv4Packets);
    mPutCounter(rxIpv6Packets, cAtEthPortCounterRxIpv6Packets);
    mPutCounter(rxErrIpv6Packets, cAtEthPortCounterRxErrIpv6Packets);
    mPutCounter(rxIcmpIpv6Packets, cAtEthPortCounterRxIcmpIpv6Packets);
    mPutCounter(rxMefPackets, cAtEthPortCounterRxMefPackets);
    mPutCounter(rxErrMefPackets, cAtEthPortCounterRxErrMefPackets);
    mPutCounter(rxMplsPackets, cAtEthPortCounterRxMplsPackets);
    mPutCounter(rxErrMplsPackets, cAtEthPortCounterRxErrMplsPackets);
    mPutCounter(rxMplsErrOuterLblPackets, cAtEthPortCounterRxMplsErrOuterLblPackets);
    mPutCounter(rxMplsDataPackets, cAtEthPortCounterRxMplsDataPackets);
    mPutCounter(rxLdpIpv4Packets, cAtEthPortCounterRxLdpIpv4Packets);
    mPutCounter(rxLdpIpv6Packets, cAtEthPortCounterRxLdpIpv6Packets);
    mPutCounter(rxMplsIpv4Packets, cAtEthPortCounterRxMplsIpv4Packets);
    mPutCounter(rxMplsIpv6Packets, cAtEthPortCounterRxMplsIpv6Packets);
    mPutCounter(rxErrL2tpv3Packets, cAtEthPortCounterRxErrL2tpv3Packets);
    mPutCounter(rxL2tpv3Packets, cAtEthPortCounterRxL2tpv3Packets);
    mPutCounter(rxL2tpv3Ipv4Packets, cAtEthPortCounterRxL2tpv3Ipv4Packets);
    mPutCounter(rxL2tpv3Ipv6Packets, cAtEthPortCounterRxL2tpv3Ipv6Packets);
    mPutCounter(rxUdpPackets, cAtEthPortCounterRxUdpPackets);
    mPutCounter(rxErrUdpPackets, cAtEthPortCounterRxErrUdpPackets);
    mPutCounter(rxUdpIpv4Packets, cAtEthPortCounterRxUdpIpv4Packets);
    mPutCounter(rxUdpIpv6Packets, cAtEthPortCounterRxUdpIpv6Packets);
    mPutCounter(rxErrPsnPackets, cAtEthPortCounterRxErrPsnPackets);
    mPutCounter(rxPacketsSendToCpu, cAtEthPortCounterRxPacketsSendToCpu);
    mPutCounter(rxPacketsSendToPw, cAtEthPortCounterRxPacketsSendToPw);
    mPutCounter(rxTopPackets, cAtEthPortCounterRxTopPackets);
    mPutCounter(rxPacketDaMis, cAtEthPortCounterRxPacketDaMis);
    mPutCounter(rxPhysicalError, cAtEthPortCounterRxPhysicalError);
    mPutCounter(rxBip8Errors, cAtEthPortCounterRxBip8Errors);
    mPutCounter(rxOverFlowDroppedPackets, cAtEthPortCounterRxOverFlowDroppedPackets);
    mPutCounter(rxFragmentPackets, cAtEthPortCounterRxFragmentPackets);
    mPutCounter(rxJabberPackets, cAtEthPortCounterRxJabberPackets);
    mPutCounter(rxLoopDaPackets, cAtEthPortCounterRxLoopDaPackets);
    mPutCounter(rxPcsErrorPackets, cAtEthPortCounterRxPcsErrorPackets);
    mPutCounter(rxPcsInvalidCount, cAtEthPortCounterRxPcsInvalidCount);
    mPutCounter(txPacketsLen64, cAtEthPortCounterTxPacketsLen64);
    mPutCounter(txPacketsLen512_1023, cAtEthPortCounterTxPacketsLen512_1023);
    mPutCounter(txPacketsLen1024_1518, cAtEthPortCounterTxPacketsLen1024_1518);
    mPutCounter(txPacketsLen1519_1522, cAtEthPortCounterTxPacketsLen1519_1522);
    mPutCounter(txPacketsLen1523_1548, cAtEthPortCounterTxPacketsLen1523_1548);
    mPutCounter(txPacketsLen1549_2047, cAtEthPortCounterTxPacketsLen1549_2047);
    mPutCounter(txPacketsLen2048_4095, cAtEthPortCounterTxPacketsLen2048_4095);
    mPutCounter(txPacketsLen4096_8191, cAtEthPortCounterTxPacketsLen4096_8191);
    mPutCounter(txPacketsLen8192_9215, cAtEthPortCounterTxPacketsLen8192_9215);
    mPutCounter(txPacketsLarge, cAtEthPortCounterTxPacketsLarge);
    mPutCounter(txPacketsSmall, cAtEthPortCounterTxPacketsSmall);
    mPutCounter(txErrFcsPackets, cAtEthPortCounterTxErrFcsPackets);
    mPutCounter(txErrPackets, cAtEthPortCounterTxErrPackets);
    mPutCounter(txVlanPackets, cAtEthPortCounterTxVlanPackets);
    mPutCounter(txUserPausePackets, cAtEthPortCounterTxUserPausePackets);
    mPutCounter(rxPacketsLen64, cAtEthPortCounterRxPacketsLen64);
    mPutCounter(rxPacketsLen512_1023, cAtEthPortCounterRxPacketsLen512_1023);
    mPutCounter(rxPacketsLen1024_1518, cAtEthPortCounterRxPacketsLen1024_1518);
    mPutCounter(rxPacketsLen1519_1522, cAtEthPortCounterRxPacketsLen1519_1522);
    mPutCounter(rxPacketsLen1523_1548, cAtEthPortCounterRxPacketsLen1523_1548);
    mPutCounter(rxPacketsLen1549_2047, cAtEthPortCounterRxPacketsLen1549_2047);
    mPutCounter(rxPacketsLen2048_4095, cAtEthPortCounterRxPacketsLen2048_4095);
    mPutCounter(rxPacketsLen4096_8191, cAtEthPortCounterRxPacketsLen4096_8191);
    mPutCounter(rxPacketsLen8192_9215, cAtEthPortCounterRxPacketsLen8192_9215);
    mPutCounter(rxPacketsLarge, cAtEthPortCounterRxPacketsLarge);
    mPutCounter(rxPacketsSmall, cAtEthPortCounterRxPacketsSmall);
    mPutCounter(rxErrPackets, cAtEthPortCounterRxErrPackets);
    mPutCounter(rxVlanPackets, cAtEthPortCounterRxVlanPackets);
    mPutCounter(rxUserPausePackets, cAtEthPortCounterRxUserPausePackets);
    mPutCounter(rxPacketsTooLong, cAtEthPortCounterRxPacketsTooLong);
    mPutCounter(rxStompedFcsPackets, cAtEthPortCounterRxStompedFcsPackets);
    mPutCounter(rxInRangeErrPackets, cAtEthPortCounterRxInRangeErrPackets);
    mPutCounter(rxTruncatedPackets, cAtEthPortCounterRxTruncatedPackets);
    mPutCounter(rxFecIncCorrectCount, cAtEthPortCounterRxFecIncCorrectCount);
    mPutCounter(rxFecIncCantCorrectCount, cAtEthPortCounterRxFecIncCantCorrectCount);
    mPutCounter(rxFecLockErrorCount, cAtEthPortCounterRxFecLockErrorCount);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesSet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesGet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(port), sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        mMethodOverride(m_ThaEthPortOverride, LoopInEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, DefaultOffset);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }
    
static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011EthPort);
    }

AtEthPort Tha60290011EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

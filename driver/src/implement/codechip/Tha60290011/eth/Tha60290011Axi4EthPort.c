/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011Axi4EthPort.c
 *
 * Created Date: Aug 10, 2016
 *
 * Description : ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha60290011ModulePdh.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "Tha60290011ModuleEth.h"
#include "Tha60290011Axi4EthPortInternal.h"
#include "../../../default/eth/ThaEthPortReg.h"
#include "Tha60290011EthPortReg.h"
#include "Tha60290011ClsInterfaceEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cDataEthType 0x88E4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290011Axi4EthPort)self)
#define mSimulation(self) SimulationDb((AtEthPort)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                       m_AtObjectOverride;
static tAtChannelMethods                      m_AtChannelOverride;
static tAtEthPortMethods                      m_AtEthPortOverride;
static tTha60290011ClsInterfaceEthPortMethods m_Tha60290011ClsInterfaceEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods                       *m_AtObjectMethods                       = NULL;
static const tAtChannelMethods                      *m_AtChannelMethods                      = NULL;
static const tAtEthPortMethods                      *m_AtEthPortMethods                      = NULL;
static const tTha60290011ClsInterfaceEthPortMethods *m_Tha60290011ClsInterfaceEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtEthPort self)
    {
    return (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    }

static eAtRet EthTypeSet(ThaEthPort self, uint16 type)
    {
    eAtRet ret = m_Tha60290011ClsInterfaceEthPortMethods->EthTypeSet(self, type);
    if (ret != cAtOk)
        return ret;

    return Tha60290011ModulePdhEthTypeSet(PdhModule((AtEthPort)self), type);
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    eAtModuleEthRet ret = m_AtEthPortMethods->SourceMacAddressSet(self, address);
    if (ret != cAtOk)
        return ret;

    return Tha60290011ModulePdhSourceMacAddressSet(PdhModule(self), address);
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    return Tha60290011ModulePdhSourceMacAddressGet(PdhModule(self), address);
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    eAtModuleEthRet ret = m_AtEthPortMethods->DestMacAddressSet(self, address);
    if (ret != cAtOk)
        return ret;

    return Tha60290011ModulePdhDestMacAddressSet(PdhModule(self), address);
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    return Tha60290011ModulePdhDestMacAddressGet(PdhModule(self), address);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);
    return Tha60290011SerdesManagerAxi4SerdesController(serdesManager);
    }

static ThaModulePmc PmcModule(AtChannel self)
    {
    return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return ThaModulePmcEthPortCounterIsSupported(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortType2Dot5GPort, counterType);

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static uint32 PmcCounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    return ThaModulePmcEthPortCountersGet(PmcModule(self), (AtEthPort)self, cThaModulePmcEthPortType2Dot5GPort, counterType, read2Clear);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortShouldGetCounterFromPmc(self))
        return PmcCounterReadToClear(self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    Tha60290011ModulePdhMuxAxi4StreamDebugShow(PdhModule((AtEthPort)self));

    return cAtOk;
    }

static eAtRet DefaultSet(AtEthPort self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdGlbCtrl + ThaEthPortMacBaseAddress((ThaEthPort)self);

    uint32 regVal = 0;
    mRegFieldSet(regVal, cThaIpEthTripGlbCtlElDepth, cThaIpEthTripGlbCtlElDepthRstVal);
    mRegFieldSet(regVal, cThaIpEthTripGlbCtlSpStb, cThaIpEthTripGlbCtlSpStbRstVal);
    mRegFieldSet(regVal, cThaIpEthTripCfgCtlSelect, 0x1);
    mRegFieldSet(regVal, cThaIpEthTripGlbCtlIntEn, cThaIpEthTripGlbCtlIntEnRstVal);
    mRegFieldSet(regVal, cThaIpEthTripGlbCtlAct, cThaIpEthTripGlbCtlActRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    regAddr = cThaRegIPEthernetTripleSpdGlbIntfCtrl + ThaEthPortMacBaseAddress((ThaEthPort)self);
    mChannelHwWrite(self, regAddr, 0x82, cAtModuleEth);

    Tha60290011ClsInterfaceEthPortEthTypeSet((ThaEthPort)self, cDataEthType);
    Tha60290011ClsInterfaceEthPortEthTypeCheckEnable((ThaEthPort)self, cAtTrue);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet((AtEthPort)self);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static const char *ToString(AtObject self)
    {
    AtChannel channel = (AtChannel)self;
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[64];

    AtSprintf(str, "%saxi4", AtDeviceIdToString(dev));
    return str;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290011ClsInterfaceEthPort(AtEthPort self)
    {
    Tha60290011ClsInterfaceEthPort ethPort = (Tha60290011ClsInterfaceEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ClsInterfaceEthPortMethods = mMethodsGet(ethPort);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ClsInterfaceEthPortOverride, m_Tha60290011ClsInterfaceEthPortMethods, sizeof(m_Tha60290011ClsInterfaceEthPortOverride));

        mMethodOverride(m_Tha60290011ClsInterfaceEthPortOverride, EthTypeSet);
        }

    mMethodsSet(ethPort, &m_Tha60290011ClsInterfaceEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290011ClsInterfaceEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011Axi4EthPort);
    }

AtEthPort Tha60290011Axi4EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ClsInterfaceEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290011Axi4EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011Axi4EthPortObjectInit(newPort, portId, module);
    }

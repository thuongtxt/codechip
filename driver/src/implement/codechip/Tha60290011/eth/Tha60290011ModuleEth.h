/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290011ModuleEth.h
 * 
 * Created Date: Aug 10, 2016
 *
 * Description : ETH module internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011MODULEETH_H_
#define _THA60290011MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/eth/Tha60210031ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60290011ModuleEthPwPortId     (0)
#define cTha60290011ModuleEth2Dot5GPortId  (1)
#define cTha60290011ModuleEthDimCtrlPortId (2)
#define cTha60290011ModuleEthDccPortId     (3)
#define cTha60290011ModuleEthSerdesBackPlanActivePortId      (4)
#define cTha60290011ModuleEthSerdesBackPlanStandbyPortId     (5)
#define cTha60290011ModuleEthSerdesBackPlanPort              (2)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ModuleEth *Tha60290011ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Ports */
AtEthPort Tha60290011SgmiiSohEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290011BackplaneEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290011Axi4EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290011DimSgmiiEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290011DccSgmiiEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290011DccSgmiiEthPortV2New(uint8 portId, AtModuleEth module);

eBool Tha60290011ModuleEthIsAxi4EthPort(AtEthPort self);
uint8 Tha60290011ModuleEthLocalSgmiiPort(AtEthPort self);
uint8 Tha60290011ModuleEthIsPwPort(AtEthPort self);
eBool Tha60290011ModuleEthIsSgmiiPort(AtEthPort self);
eBool Tha60290011ModuleEthIsSerdesBackplanePort(AtEthPort port);
AtEthPort Tha60290011ModuleEthSgmiiDccPortGet(AtModuleEth self);
eBool Tha60290011ModuleEthIsDimSgmiiPort(AtEthPort self);
eBool Tha60290011ModuleEthIsDccSgmiiPort(AtEthPort self);
eBool Tha60290011ModuleEthSohTransparentIsReady(AtModuleEth self);

uint32 Tha60290011ClsInterfaceEthPortClsBaseAddress(ThaEthPort self);
uint16 Tha60290011ClsInterfaceEthPortEthLengthGet(ThaEthPort self);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthSubTypeSet(ThaEthPort self, uint8 subtype);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthTypeSet(ThaEthPort self, uint16 type);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthLengthSet(ThaEthPort self, uint16 length);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortSequenceCheckEnable(ThaEthPort self, eBool enable);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortLengthCheckEnable(ThaEthPort self, eBool enable);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortEthTypeCheckEnable(ThaEthPort self, eBool enable);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortSourceMacCheckEnable(ThaEthPort self, eBool enable);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortDestMacCheckEnable(ThaEthPort self, eBool enable);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortSubTypeMaskCheckSet(ThaEthPort self, uint8 mask);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortDestMacAddressSet(ThaEthPort self, uint8 *address);
eAtModuleEthRet Tha60290011ClsInterfaceEthPortSourceMacAddressSet(ThaEthPort self, uint8 *address);

eAtRet Tha60290011ModuleEthRxBackPlaneSerdesSelect(ThaModuleEth self, uint32 serdesId);
uint32 Tha60290011ModuleEthRxBackPlaneSelectedSerdes(ThaModuleEth self);
uint8 Tha60290011ModuleEthSerdesBackplanePortLocalId(AtModuleEth self, uint8 portId);
AtSerdesController Tha60290011ModuleEthSerdesForPort(AtModuleEth self, uint8 portId);

eAtRet Tha60290011ModuleEthDccEnable(AtModuleEth self, eBool enable);
eBool Tha60290011ModuleEthDccIsEnabled(AtModuleEth self);
eAtRet Tha60290011ModuleEthTxFsmPinEnable(ThaModuleEth self, eBool enable);
eAtRet Tha60290011ModuleEthTxFsmPinForceStandby(ThaModuleEth self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULEETH_H_ */


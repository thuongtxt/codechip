/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011Axi4EthPortInternal.h
 *
 * Created Date: Nov 22, 2016 
 *
 * Description : .
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011AXI4ETHPORTINTERNAL_H_
#define _THA60290011AXI4ETHPORTINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290011ClsInterfaceEthPortInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011Axi4EthPort * Tha60290011Axi4EthPort;

typedef struct tTha60290011Axi4EthPort
    {
    tTha60290011ClsInterfaceEthPort super;
    }tTha60290011Axi4EthPort;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290011Axi4EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290011AXI4ETHPORTINTERNAL_H_ */

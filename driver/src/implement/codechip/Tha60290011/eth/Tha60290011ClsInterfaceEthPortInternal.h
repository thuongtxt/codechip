/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Eth
 * 
 * File        : Tha60290011ClsInterfaceEthPortInternal.h
 * 
 * Created Date: Dec 23, 2016
 *
 * Description : Interface of Cls Layer of Eth port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011CLSINTERFACEETHPORTINTERNAL_H_
#define _THA60290011CLSINTERFACEETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290011EthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290011ClsInterfaceEthPort * Tha60290011ClsInterfaceEthPort;

typedef struct tTha60290011ClsInterfaceEthPortMethods
    {
    uint32 (*ClsBaseAddress)(ThaEthPort self);
    eAtRet (*EthTypeSet)(ThaEthPort self, uint16 type);
    }tTha60290011ClsInterfaceEthPortMethods;

typedef struct tTha60290011ClsInterfaceEthPort
    {
    tTha60290011EthPort super;
    const tTha60290011ClsInterfaceEthPortMethods * methods;
    }tTha60290011ClsInterfaceEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290011ClsInterfaceEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011CLSINTERFACEETHPORTINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_RD_ETH_H_
#define _AF6_REG_AF6CNC0011_RD_ETH_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_glb_ver
Reg Addr   : 0x00
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to indicate version

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_glb_ver                                                                 0x00

/*--------------------------------------
BitField Name: IpEthTripGlbVendor
BitField Type: RO
BitField Desc:
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ver_IpEthTripGlbVendor_Mask                                        cBit15_8
#define cAf6_ip_eth_trip_speed_glb_ver_IpEthTripGlbVendor_Shift                                              8

/*--------------------------------------
BitField Name: IpEthTripGlbVerionID
BitField Type: RO
BitField Desc:
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ver_IpEthTripGlbVerionID_Mask                                       cBit7_0
#define cAf6_ip_eth_trip_speed_glb_ver_IpEthTripGlbVerionID_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : IP Ethernet Triple Speed Global Control
Reg Addr   : 0x01
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is global Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_glb_ctrl                                                             0x01

/*--------------------------------------
BitField Name: IpEthTripGlbCtlElFifoDepth
BitField Type: RW
BitField Desc: Elastic FIFO Depth, Only used to NONE PCS
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlElFifoDepth_Mask                                cBit10_8
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlElFifoDepth_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripGlbCtlSpStable
BitField Type: RW
BitField Desc: Software must write 0x1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlSpStable_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlSpStable_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripGlbCtlSpIsPhy
BitField Type: RW
BitField Desc: 1: Enable SMAC auto update speed operation by PHY device (or PCS)
0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlSpIsPhy_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlSpIsPhy_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripCfgCtlSelect
BitField Type: RW
BitField Desc: 1: Enable engine used con-fig interface by register IP Ethernet
Triple Speed Global Interface Control 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripCfgCtlSelect_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripCfgCtlSelect_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripGlbCtlFlush
BitField Type: RW
BitField Desc: Software reset 1: Software Reset 0: Normal
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlFlush_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlFlush_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripGlbCtlIntEn
BitField Type: RW
BitField Desc: Interrupt Enable 1: Enable Interrupt 0: Disable Interrupt
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlIntEn_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlIntEn_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripGlbCtlActive
BitField Type: RW
BitField Desc: Port Active 1: Active 0: No Active
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlActive_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_glb_ctrl_IpEthTripGlbCtlActive_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_glb_interrupt_type_req
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is global interrupt type request

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_glb_interrupt_type_req                                                  0x02

/*--------------------------------------
BitField Name: IpEthTripGlbIntTypMdio
BitField Type: RW
BitField Desc: MDIO interrupt request 1: SMAC interrupt request 0: SMAC no
request
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypMdio_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypMdio_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripGlbIntTypSmac
BitField Type: RW
BitField Desc: Simple MAC interrupt request 1: SMAC interrupt request 0: SMAC no
request
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypSmac_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypSmac_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripGlbIntTypGlb
BitField Type: RW
BitField Desc: Global interrupt request 1: Global interrupt request 0: Global no
request
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypGlb_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypGlb_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripGlbIntTypPcs
BitField Type: RW
BitField Desc: PCS interrupt request 1: PCS interrupt request 0: PCS no request
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypPcs_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_glb_interrupt_type_req_IpEthTripGlbIntTypPcs_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_glb_interrupt_status
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is global status

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_glb_interrupt_status                                                    0x03

/*--------------------------------------
BitField Name: IpEthTripGlbStaTxifEr
BitField Type: RW
BitField Desc: Transmitter Interface check error occur
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxifEr_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxifEr_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripGlbStaRxifOver
BitField Type: RW
BitField Desc: Receiver Interface Rate match FIFO overflow occur
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaRxifOver_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaRxifOver_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripGlbStaRxifUnder
BitField Type: RW
BitField Desc: Receiver Interface Rate match FIFO underflow occur
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaRxifUnder_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaRxifUnder_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripGlbStaRxffRdEr
BitField Type: RW
BitField Desc: Receiver User FIFO convert clock domain read errror occur
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaRxffRdEr_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaRxffRdEr_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripGlbStaTxffWrEr
BitField Type: RW
BitField Desc: Transmitter User FIFO convert clock domain write error occur
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxffWrEr_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxffWrEr_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripGlbStaTxshRdEr
BitField Type: RW
BitField Desc: Transmitter User shift engine read error assert
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxshRdEr_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxshRdEr_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripGlbStaTxshWrEr
BitField Type: RW
BitField Desc: Transmitter User shift engine write error assert
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxshWrEr_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_glb_interrupt_status_IpEthTripGlbStaTxshWrEr_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_glb_interface_ctrl
Reg Addr   : 0x04
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Interface Control %%
Ethernet interface will description below %%
TBI RGMII SGMII Functional %%
1 0 0 - TBI ( GMII) <--> PCS 1000BaseX <--> SMAC <---> USER speed 1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%
1 0 1 - TBI ( GMII) <--> PCS SGMII <--> SMAC <---> USER speed 10/100/1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%
1 1 0 - TBI (RGMII) <--> PCS 1000BaseX <--> SMAC <---> USER speed 1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%
1 1 1 - TBI (RGMII) <--> PCS SGMII <--> SMAC <---> USER speed 10/100/1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%
0 0 X - GMII/MII <------------------> SMAC <---> USER speed 10/100/1000Mbps - rxclk (2.5/25/125Mhz), gtxclk (2.5/25/125Mhz) %%
0 1 X - RGMII <------------------> SMAC <---> USER speed 10/100/1000Mbps - rxclk (2.5/25/125Mhz), gtxclk (2.5/25/125Mhz)

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_glb_interface_ctrl                                                      0x04

/*--------------------------------------
BitField Name: IpEthTripGlbStaRgmii
BitField Type: RW
BitField Desc: RGMII interface Status 0x1: RGMII Active 0x0: GMII/MII Active
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaRgmii_Mask                                  cBit14
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaRgmii_Shift                                      14

/*--------------------------------------
BitField Name: IpEthTripGlbStaSgmii
BitField Type: RW
BitField Desc: SGMII interface status 0x1: PCS-SGMII 0x0: PCS-1000Base-X
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaSgmii_Mask                                  cBit13
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaSgmii_Shift                                      13

/*--------------------------------------
BitField Name: IpEthTripGlbStaTbi
BitField Type: RW
BitField Desc: TBI interface status 0x1:  TBI interface Acitve (include PCS
1000Base-X or SGMII) 0x0:  None PCS
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaTbi_Mask                                  cBit12
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaTbi_Shift                                      12

/*--------------------------------------
BitField Name: IpEthTripGlbStaSpeed
BitField Type: RW
BitField Desc: Speed operation status 0x0: 10Mbps 0x1: 100Mbps 0x2: 1000Mbps
0x3: Unused
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaSpeed_Mask                                 cBit9_8
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbStaSpeed_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripGlbCfgRgmii
BitField Type: RW
BitField Desc: RGMII interface Active 0x1: RGMII Active 0x0: GMII Active
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgRgmii_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgRgmii_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripGlbCfgSgmii
BitField Type: RW
BitField Desc: SGMII interface Active 0x1: PCS-SGMII 0x0: PCS-1000Base-X
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgSgmii_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgSgmii_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripGlbCfgTbi
BitField Type: RW
BitField Desc: TBI interface 0x1:  TBI interface Acitve (include PCS 1000Base-X
or SGMII) 0x0:  None PCS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgTbi_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgTbi_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripGlbCfgSpeed
BitField Type: RW
BitField Desc: Speed MAC operation 0x0: 10Mbps 0x1: 100Mbps 0x2: 1000Mbps 0x3:
Unused
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgSpeed_Mask                                 cBit1_0
#define cAf6_ip_eth_trip_speed_glb_interface_ctrl_IpEthTripGlbCfgSpeed_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_glb_user_req_status
Reg Addr   : 0x08
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is User status, Used to HW debug

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_glb_user_req_status                                                     0x08

/*--------------------------------------
BitField Name: IpEthTripGlbStaUsrRxVl
BitField Type: RW
BitField Desc:
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaUsrRxVl_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaUsrRxVl_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripGlbStaUsrTxVl
BitField Type: RW
BitField Desc:
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaUsrTxVl_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaUsrTxVl_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripGlbStaPauGen
BitField Type: RW
BitField Desc:
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaPauGen_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaPauGen_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripGlbStaUsrTxEnb
BitField Type: RW
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaUsrTxEnb_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_glb_user_req_status_IpEthTripGlbStaUsrTxEnb_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_ctrl
Reg Addr   : 0x20
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_ctrl                                                                0x20

/*--------------------------------------
BitField Name: IpEthTripPcsReset
BitField Type: RW
BitField Desc: 1: Core Reset request 0: Normal Operation
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsReset_Mask                                          cBit15
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsReset_Shift                                             15

/*--------------------------------------
BitField Name: IpEthTripPcsLoopback
BitField Type: RW
BitField Desc: 1: Enable Line Loop in (LVDS) 0: No loop
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsLoopback_Mask                                       cBit14
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsLoopback_Shift                                          14

/*--------------------------------------
BitField Name: IpEthTripPcsSpeedLsb
BitField Type: RW
BitField Desc: Speed Selection (LSB)
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsSpeedLsb_Mask                                       cBit13
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsSpeedLsb_Shift                                          13

/*--------------------------------------
BitField Name: IpEthTripPcsAnEn
BitField Type: RW
BitField Desc: 1: Enable Auto-Negotiation process 0: Disable Auto-Negotiation
process
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsAnEn_Mask                                           cBit12
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsAnEn_Shift                                              12

/*--------------------------------------
BitField Name: IpEthTripPcsPowerdown
BitField Type: RW
BitField Desc: 1: Power down, Force Lost of Signal 0: Normal Operation
BitField Bits: [11]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsPowerdown_Mask                                      cBit11
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsPowerdown_Shift                                         11

/*--------------------------------------
BitField Name: IpEthTripPcsIsolate
BitField Type: RW
BitField Desc: 1: Electrically Isolate PCS from GMII 0: Normal Operation
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsIsolate_Mask                                        cBit10
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsIsolate_Shift                                           10

/*--------------------------------------
BitField Name: IpEthTripPcsRestartAn
BitField Type: RW
BitField Desc: 1: Restart Auto-Negotiation Process 0: Normal Operation
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsRestartAn_Mask                                       cBit9
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsRestartAn_Shift                                          9

/*--------------------------------------
BitField Name: IpEthTripPcsFullduplex
BitField Type: RW
BitField Desc: 1: Full-Duplex Mode 0: Haft-duplex Mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsFullduplex_Mask                                      cBit8
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsFullduplex_Shift                                         8

/*--------------------------------------
BitField Name: IpEthTripPcsCollisionTest
BitField Type: RW
BitField Desc: 1: Enable Collision test 0: Disable Collision test
BitField Bits: [7]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsCollisionTest_Mask                                   cBit7
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsCollisionTest_Shift                                       7

/*--------------------------------------
BitField Name: IpEthTripPcsSpeedMsb
BitField Type: RW
BitField Desc: Speed Selection (MSB) Case:
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsSpeedMsb_Mask                                        cBit6
#define cAf6_ip_eth_trip_speed_pcs_ctrl_IpEthTripPcsSpeedMsb_Shift                                           6


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_status
Reg Addr   : 0x21
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS Status

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_status                                                              0x21

/*--------------------------------------
BitField Name: IpEthTripPcsStaAutoNegComplete
BitField Type: RW
BitField Desc: 1: Auto-Negotiation process completed 0: Auto-Negotiation process
not completed
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_status_IpEthTripPcsStaAutoNegComplete_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_pcs_status_IpEthTripPcsStaAutoNegComplete_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripPcsStaRemoteFault
BitField Type: RW
BitField Desc: 1: Remote fault condition detected 0: No remote fault condition
detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_status_IpEthTripPcsStaRemoteFault_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_pcs_status_IpEthTripPcsStaRemoteFault_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripPcsStaLinkStatus
BitField Type: RW
BitField Desc: 1: Link is up 0: Link is down
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_status_IpEthTripPcsStaLinkStatus_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_pcs_status_IpEthTripPcsStaLinkStatus_Shift                                       2


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_phy_id_0
Reg Addr   : 0x22
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PHY Identifier 0

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_phy_id_0                                                            0x22

/*--------------------------------------
BitField Name: value
BitField Type: RW
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_phy_id_0_value_Mask                                                cBit15_0
#define cAf6_ip_eth_trip_speed_pcs_phy_id_0_value_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_phy_id_1
Reg Addr   : 0x23
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PHY Identifier 1

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_phy_id_1                                                            0x23

/*--------------------------------------
BitField Name: IpEthTripPcsVendor21_7
BitField Type: RW
BitField Desc:
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_phy_id_1_IpEthTripPcsVendor21_7_Mask                                cBit15_0
#define cAf6_ip_eth_trip_speed_pcs_phy_id_1_IpEthTripPcsVendor21_7_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_advertise
Reg Addr   : 0x24
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Auto-Negotiation Advertisement

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_advertise                                                           0x24

/*--------------------------------------
BitField Name: IpEthTripPcsAnAdvNextPage
BitField Type: RW
BitField Desc: 1 : Next Page functionality is supported 0 : Next Page
functionality is not supported
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvNextPage_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvNextPage_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsAnAdvRemoteFault
BitField Type: RW
BitField Desc: Remote Fault, self clearing to 0x0 after auto-negotiation 0 : No
Error 1 : Offline 2 : Link Failure 3 : Auto-Negotiation Error
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvRemoteFault_Mask                               cBit13_12
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvRemoteFault_Shift                                      12

/*--------------------------------------
BitField Name: IpEthTripPcsAnAdvPause
BitField Type: RW
BitField Desc: 0 : No PAUSE 1 : Symmetric PAUSE 2 : Asymmetric PAUSE towards
link partner 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvPause_Mask                                 cBit8_7
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvPause_Shift                                       7

/*--------------------------------------
BitField Name: IpEthTripPcsAnAdvHalfDuplex
BitField Type: RW
BitField Desc: 1 : Half Duplex Mode is supported 0 : Half Duplex Mode is not
supported
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvHalfDuplex_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvHalfDuplex_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripPcsAnAdvFullDuplex
BitField Type: RW
BitField Desc: 1 : Full Duplex Mode is supported 0 : Full Duplex Mode is not
supported
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvFullDuplex_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_pcs_advertise_IpEthTripPcsAnAdvFullDuplex_Shift                                       5


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_link_partner_abi_base
Reg Addr   : 0x25
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Auto-Negotiation Link Partner Ability Base

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_link_partner_abi_base                                               0x25

/*--------------------------------------
BitField Name: IpEthTripPcsAnLpANextPage
BitField Type: RW
BitField Desc: 1 : Next Page functionality is supported 0 : Next Page
functionality is not supported
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpANextPage_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpANextPage_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsAnLpAAcknowledge
BitField Type: RW
BitField Desc: Used by Auto-Negotiation function to indicate reception of a link
partners base or next page
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAAcknowledge_Mask                                  cBit14
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAAcknowledge_Shift                                      14

/*--------------------------------------
BitField Name: IpEthTripPcsAnLpARemoteFault
BitField Type: RW
BitField Desc: Remote Fault 0 : No Error 1 : Offline 2 : Link Failure 3 : Auto-
Negotiation Error
BitField Bits: [13:9]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpARemoteFault_Mask                                cBit13_9
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpARemoteFault_Shift                                       9

/*--------------------------------------
BitField Name: IpEthTripPcsAnLpAPause
BitField Type: RW
BitField Desc: 0 : No PAUSE 1 : Symmetric PAUSE 2 : Asymmetric PAUSE towards
link partner 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAPause_Mask                                 cBit8_7
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAPause_Shift                                       7

/*--------------------------------------
BitField Name: IpEthTripPcsAnLpAHalfDuplex
BitField Type: RW
BitField Desc: 1 : Half Duplex Mode is supported 0 : Half Duplex Mode is not
supported
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAHalfDuplex_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAHalfDuplex_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripPcsAnLpAFullDuplex
BitField Type: RW
BitField Desc: 1 : Full Duplex Mode is supported 0 : Full Duplex Mode is not
supported
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAFullDuplex_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_pcs_link_partner_abi_base_IpEthTripPcsAnLpAFullDuplex_Shift                                       5


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_expansion
Reg Addr   : 0x26
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Auto-Negotiation Expansion Register

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_expansion                                                           0x26

/*--------------------------------------
BitField Name: IpEthTripPcsAnExpNextPageAble
BitField Type: RW
BitField Desc: Next Page Able
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_expansion_IpEthTripPcsAnExpNextPageAble_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_pcs_expansion_IpEthTripPcsAnExpNextPageAble_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripPcsAnExpPageReceived
BitField Type: RW
BitField Desc: Page Received 1 : A new page has been received 0 : A new page has
not been received
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_expansion_IpEthTripPcsAnExpPageReceived_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_pcs_expansion_IpEthTripPcsAnExpPageReceived_Shift                                       1


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_next_page_transmit
Reg Addr   : 0x27
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Auto-Negotiation Next Page Transmitter

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_next_page_transmit                                                  0x27

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxNextPage
BitField Type: RW
BitField Desc: 1: Additional Next Page(s) will follow 0: Last page
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxNextPage_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxNextPage_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxMessagePage
BitField Type: RW
BitField Desc: Message Page 1: Message Page 0: Unformatted Page
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxMessagePage_Mask                                  cBit13
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxMessagePage_Shift                                      13

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxAck
BitField Type: RW
BitField Desc: Acknowledge 1: Comply with message 0: Cannot comply with message
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxAck_Mask                                  cBit12
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxAck_Shift                                      12

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxToggle
BitField Type: RW
BitField Desc: Value toggles between subsequent Next Page
BitField Bits: [11]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxToggle_Mask                                  cBit11
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcsAnNpTxToggle_Shift                                      11

/*--------------------------------------
BitField Name: IpEthTripPcaAnNpTxMeassge
BitField Type: RW
BitField Desc: Message Code Field or Unformatted Page, (00000000001 is Null
Message Code)
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcaAnNpTxMeassge_Mask                                cBit10_0
#define cAf6_ip_eth_trip_speed_pcs_next_page_transmit_IpEthTripPcaAnNpTxMeassge_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_next_page_receive
Reg Addr   : 0x28
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Auto-Negotiation Next Page Receiver

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_next_page_receive                                                   0x28

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpRxNextPage
BitField Type: RW
BitField Desc: 1: Additional Next Page(s) will follow 0: Last page
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxNextPage_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxNextPage_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpRxAck
BitField Type: RW
BitField Desc: Acknowledge, Used by Auto-Negotiation function to indicate
reception of a link partner�s base or next page
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxAck_Mask                                  cBit14
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxAck_Shift                                      14

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpRxMessagePage
BitField Type: RW
BitField Desc: Message Page 1: Message Page 0: Unformatted Page
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxMessagePage_Mask                                  cBit13
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxMessagePage_Shift                                      13

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpRxAck2
BitField Type: RW
BitField Desc: Acknowledge2 1: Comply with message 0: Cannot comply with message
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxAck2_Mask                                  cBit12
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxAck2_Shift                                      12

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpRxToggle
BitField Type: RW
BitField Desc: Value toggles between subsequent Next Page
BitField Bits: [11]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxToggle_Mask                                  cBit11
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcsAnNpRxToggle_Shift                                      11

/*--------------------------------------
BitField Name: IpEthTripPcaAnNpRxMeassge
BitField Type: RW
BitField Desc: Message Code Field or Unformatted Page
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcaAnNpRxMeassge_Mask                                cBit10_0
#define cAf6_ip_eth_trip_speed_pcs_next_page_receive_IpEthTripPcaAnNpRxMeassge_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_extend_status
Reg Addr   : 0x2F
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Extended Status

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_extend_status                                                       0x2F

/*--------------------------------------
BitField Name: IpEthTripPcsEx1000BXFdupx
BitField Type: RW
BitField Desc: Support 1000Base_X full duplex 1: Supported 0: No Supported
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BXFdupx_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BXFdupx_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsEx1000BXHdupx
BitField Type: RW
BitField Desc: Support 1000Base_X halfl duplex 1: Supported 0: No Supported
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BXHdupx_Mask                                  cBit14
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BXHdupx_Shift                                      14

/*--------------------------------------
BitField Name: IpEthTripPcsEx1000BTFdupx
BitField Type: RW
BitField Desc: Support 1000Base_T full duplex 1: Supported 0: No Supported
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BTFdupx_Mask                                  cBit13
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BTFdupx_Shift                                      13

/*--------------------------------------
BitField Name: IpEthTripPcsEx1000BTHdupx
BitField Type: RW
BitField Desc: Support 1000Base_T halfl duplex 1: Supported 0: No Supported
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BTHdupx_Mask                                  cBit12
#define cAf6_ip_eth_trip_speed_pcs_extend_status_IpEthTripPcsEx1000BTHdupx_Shift                                      12


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_ctrl
Reg Addr   : 0x32
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS Option Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_ctrl                                                            0x32

/*--------------------------------------
BitField Name: IpEthTripPcsCtlFifoAdjC2
BitField Type: RW
BitField Desc: Enable rate match FIFO used adjust C2 code word when Auto-
Negotiation enable 0 : Disable 1 : Enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlFifoAdjC2_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlFifoAdjC2_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsCtlFifoDepth
BitField Type: RW
BitField Desc: Rate match FIFO depth, support support up to SGMII speed 10Mbps
jumbo frame 12K bytes 0 : +/- 4 words 1 : +/- 8 words 2 : +/- 16 words 3 : +/-
32 words 4 : +/- 64 words 5 : +/- 128 words 6 : +/- 256 words 7 : Unused
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlFifoDepth_Mask                               cBit14_12
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlFifoDepth_Shift                                      12

/*--------------------------------------
BitField Name: IpEthTripPcsCtlLoopin
BitField Type: RW
BitField Desc: Diagnostic, loop in enable 1 : Loop back in enable 0 : Normal
Operations
BitField Bits: [11]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlLoopin_Mask                                  cBit11
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlLoopin_Shift                                      11

/*--------------------------------------
BitField Name: IpEthTripPcsCtlLoopout
BitField Type: RW
BitField Desc: Diagnostic, loop out enable 1 : Loop back out enable 0 : Normal
Operations
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlLoopout_Mask                                  cBit10
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlLoopout_Shift                                      10

/*--------------------------------------
BitField Name: IpEthTripPcsCtlTestDisp
BitField Type: RW
BitField Desc: Value Disparity Test
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTestDisp_Mask                                   cBit9
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTestDisp_Shift                                       9

/*--------------------------------------
BitField Name: IpEthTripPcsCtlTestFDisp
BitField Type: RW
BitField Desc: 1 : Enable Disparity Test 0 : Normal Operations
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTestFDisp_Mask                                   cBit8
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTestFDisp_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripPcsCtlTxSwap
BitField Type: RW
BitField Desc: Enable Swap output TBI data 1 :  Enable 0 :  Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTxSwap_Mask                                   cBit7
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTxSwap_Shift                                       7

/*--------------------------------------
BitField Name: IpEthTripPcsCtlTxPolar
BitField Type: RW
BitField Desc: Enable Invert output TBI data 1 :  Enable 0 :  Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTxPolar_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlTxPolar_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripPcsCtlRxSwap
BitField Type: RW
BitField Desc: Enable Swap input  TBI data 1 :  Enable 0 :  Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlRxSwap_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlRxSwap_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripPcsCtlRxPolar
BitField Type: RW
BitField Desc: Enable Invert input TBI data 1 :  Enable 0 :  Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlRxPolar_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlRxPolar_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripPcsCtlSgmiiSel
BitField Type: RW
BitField Desc: Enable select SGMII mode when IpEthTripPcsCtlModSel assert 1 :
SGMII mode 0 :  1000BASE-X
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlSgmiiSel_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlSgmiiSel_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripPcsCtlModSel
BitField Type: RW
BitField Desc: Mode Interface select 1 :  Enable select sgmii/1000base_x depend
on  IpEthTripPcsCtlSgmiiSel bit 0 :  Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlModSel_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsCtlModSel_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripPcsIntLinkEn
BitField Type: RW
BitField Desc: Enable interrupt Link status 1 :  Enable 0 :  Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsIntLinkEn_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsIntLinkEn_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripPcsIntAnEn
BitField Type: RW
BitField Desc: Enable interrupt Auto-Negotiation 1 :  Enable 0 :  Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsIntAnEn_Mask                                     cBit0
#define cAf6_ip_eth_trip_speed_pcs_opt_ctrl_IpEthTripPcsIntAnEn_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_status
Reg Addr   : 0x33
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS Option Status

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_status                                                          0x33

/*--------------------------------------
BitField Name: IpEthTripPcsStaFifoUnderflow
BitField Type: RW
BitField Desc: Under flow elastic FIFO status 1: Under flow elastic FIFO
occurred,  resolve by increase  Rate match FIFO depth at
IpEthTripPcsCtlFifoDepth[2:0] at IP Ethernet Triple Speed PCS Option Control
register 0: Under flow elastic FIFO occurred
BitField Bits: [7]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaFifoUnderflow_Mask                                   cBit7
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaFifoUnderflow_Shift                                       7

/*--------------------------------------
BitField Name: IpEthTripPcsStaFifoOverflow
BitField Type: RW
BitField Desc: Over flow elastic FIFO status 1: Over flow elastic FIFO occurred,
resolve by increase  Rate match FIFO depth at  IpEthTripPcsCtlFifoDepth[2:0] at
IP Ethernet Triple Speed PCS Option Control register 0: No over flow elastic
FIFO occurred
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaFifoOverflow_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaFifoOverflow_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripPcsStaDispEr
BitField Type: RW
BitField Desc: Disparity violation 1: Disparity violation 0: Disparity no
violation
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaDispEr_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaDispEr_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripPcsStaCodeEr
BitField Type: RW
BitField Desc: Code word invalid detection 1: Detect an code word invalid 0: No
Code word invalid detection
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaCodeEr_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaCodeEr_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripPcsStaLinkFail
BitField Type: RW
BitField Desc: Link fail status 1: Link down 0: Link up
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaLinkFail_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaLinkFail_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripPcsStaAnComplete
BitField Type: RW
BitField Desc: Auto-Negotiation status 1: Auto-Neg complete 0: Auto-Neg Fail
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaAnComplete_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsStaAnComplete_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripPcsIntLinkReq
BitField Type: RW
BitField Desc: Link interrupt request 1: Interrupt request 0: No Interrupt
request
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsIntLinkReq_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsIntLinkReq_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripPcsIntAnReq
BitField Type: RW
BitField Desc: Auto-Negotiation interrupt request 1: Interrupt request 0: No
Interrupt request
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsIntAnReq_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_pcs_opt_status_IpEthTripPcsIntAnReq_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_fsm_status_0
Reg Addr   : 0x34
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS FSM Current Status 0, used to HW debug

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_fsm_status_0                                                    0x34

/*--------------------------------------
BitField Name: IpEthTripPcsCurXmit
BitField Type: RW
BitField Desc: Xmit current status (define at 802.3 section 3) 0x0 : XMIT_IDL
0x1 : XMIT_CFG 0x2 : XMIT_DAT
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_0_IpEthTripPcsCurXmit_Mask                                 cBit9_8
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_0_IpEthTripPcsCurXmit_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripPcsCurAnFsm
BitField Type: RW
BitField Desc: Auto-Negotiation Current FSM 0x0 : AN_ENABLE 0x1 : AN_RESTART 0x2
: AN_DIS_LINK_OK 0x3 : ABILITY_DET 0x4 : ACK_DET 0x5 : NEXT_PAGE_WAIT 0x6 :
COMPLETE_ACK 0x7 : DLE_DET 0x8 : LINK_OK
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_0_IpEthTripPcsCurAnFsm_Mask                                 cBit7_4
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_0_IpEthTripPcsCurAnFsm_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripPcsCurSyncFsm
BitField Type: RW
BitField Desc: Synchronization current FSM 0x0 : LOSYNC 0x1 : COM_DET1 0x2 :
ACQ_SYNC1 0x3 : COM_DET2 0x4 : ACQ_SYNC2 0x5 : ACQ_SYNC2 0x6 : SYNC_ACQ1 0x7 :
SYNC_ACQ2 0x8 : SYNC_ACQ3 0x9 : SYNC_ACQ4 0xa : SYNC_ACQ2A 0xb : SYNC_ACQ3A 0xc
: SYNC_ACQ3A
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_0_IpEthTripPcsCurSyncFsm_Mask                                 cBit3_0
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_0_IpEthTripPcsCurSyncFsm_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_fsm_status_1
Reg Addr   : 0x35
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS FSM Current Status 1, only used to HW debug

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_fsm_status_1                                                    0x35

/*--------------------------------------
BitField Name: IpEthTripPcsCurRxFsm
BitField Type: RW
BitField Desc: Receiver Engine current FSM 0x00 :  LINK_FAIL 0x01 :  WAIT_FOR_K
0x02 :  RX_K 0x03 :  RX_CB 0x04 :  RX_CC 0x05 :  RX_CD 0x06 :  IDLE 0x07 :
CARRIER_DET 0x08 :  FALSE_CARR 0x09 :  RX_INVALID 0x0A :  START_OF_PACKET 0x0B :
RECEIVE 0x0C :  EARLY_END 0x0D :  TRI_RRI 0x0E :  TRR_EXTEND 0x0F :  RX_DATA_ERR
0x10 :   RX_DATA 0x11 :   EARLY_END_EXT 0x12 :   EPD2_CHECK_END 0x13 :
EXTEND_ERR
BitField Bits: [9:5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_1_IpEthTripPcsCurRxFsm_Mask                                 cBit9_5
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_1_IpEthTripPcsCurRxFsm_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripPcsCurTxFsm
BitField Type: RW
BitField Desc: Transmitter Engine current FSM 0x00 :   CFG_C1A 0x01 :   CFG_C1B
0x02 :   CFG_C1C 0x03 :   CFG_C1D 0x04 :   CFG_C2A 0x05 :   CFG_C2B 0x06 :
CFG_C2C 0x07 :   CFG_C2D 0x08 :   IDLE_DISP 0x09 :   IDLE_I1B 0x0A :  IDLE_I2B
0x0B :  START_ERROR 0x0C :  TX_DATA_ERROR 0x0D :  START_OF_PACKET 0x0E :
TX_DATA 0x0F :   EOP_NOEXT 0x10 :   EOP_EXT 0x11 :   EPD2_NOEXT 0x12 :   EPD3
0x13 :   EXTEND_BY_1 0x14 :   CARR_EXT
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_1_IpEthTripPcsCurTxFsm_Mask                                 cBit4_0
#define cAf6_ip_eth_trip_speed_pcs_opt_fsm_status_1_IpEthTripPcsCurTxFsm_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_link_timer
Reg Addr   : 0x38
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS Link Timer, unit = 65535 ns ~ 65us

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_link_timer                                                      0x38

/*--------------------------------------
BitField Name: IpEthTripPcsCfgLinkTimerSgmii
BitField Type: RW
BitField Desc: Auto-Negotiation cycle by the Link timer with SGMII standard.
Default (0x19h * 65us ~ 1.6ms)
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_link_timer_IpEthTripPcsCfgLinkTimerSgmii_Mask                                cBit15_8
#define cAf6_ip_eth_trip_speed_pcs_opt_link_timer_IpEthTripPcsCfgLinkTimerSgmii_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripPcsCfgLinkTimerBasex
BitField Type: RW
BitField Desc: Auto-Negotiation cycle by the Link timer with 1000 BASE-XI
standard, Default  (0x99h * 65us ~ 10ms)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_link_timer_IpEthTripPcsCfgLinkTimerBasex_Mask                                 cBit7_0
#define cAf6_ip_eth_trip_speed_pcs_opt_link_timer_IpEthTripPcsCfgLinkTimerBasex_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_sync_timer
Reg Addr   : 0x39
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PCS Sync Timer, unit = 65535 ns ~ 65us

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_sync_timer                                                      0x39
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_sync_timer_WidthVal                                               32

/*--------------------------------------
BitField Name: IpEthTripPcsCfgSyncDis
BitField Type: RW
BitField Desc: Disable monitor sync
BitField Bits: [16]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_sync_timer_IpEthTripPcsCfgSyncDis_Mask                                  cBit16
#define cAf6_ip_eth_trip_speed_pcs_opt_sync_timer_IpEthTripPcsCfgSyncDis_Shift                                      16

/*--------------------------------------
BitField Name: IpEthTripPcsCfgSyncTimer
BitField Type: RW
BitField Desc: standard. Default (0x7Fh * 65us ~ 8.25ms)
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_sync_timer_IpEthTripPcsCfgSyncTimer_Mask                                cBit15_8
#define cAf6_ip_eth_trip_speed_pcs_opt_sync_timer_IpEthTripPcsCfgSyncTimer_Shift                                       8


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_test_ctrl
Reg Addr   : 0x3C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PRBS test status

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_test_ctrl                                                       0x3C

/*--------------------------------------
BitField Name: IpEthTripPcsCtlTestPrbsEn
BitField Type: RW
BitField Desc: Enable PRBS test 1 : Enable 0 : Normal Operations
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IpEthTripPcsCtlTestPrbsEn_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IpEthTripPcsCtlTestPrbsEn_Shift                                      15

/*--------------------------------------
BitField Name: IpEthTripPcsCtlTestAlignEn
BitField Type: RW
BitField Desc: Enable PRBS monitor request align to SERDES 1 : Enable 0 :
Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IpEthTripPcsCtlTestAlignEn_Mask                                  cBit14
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IpEthTripPcsCtlTestAlignEn_Shift                                      14

/*--------------------------------------
BitField Name: IPEthTripPcsTestFixDat
BitField Type: RW
BitField Desc: Fix Data generation
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IPEthTripPcsTestFixDat_Mask                                cBit13_4
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IPEthTripPcsTestFixDat_Shift                                       4

/*--------------------------------------
BitField Name: IPEthTripPcsTestMode
BitField Type: RW
BitField Desc: Test mode [0]: Set 1 PRBS15 Test, else Fix Pattern Test [3:2]:
Force Error
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IPEthTripPcsTestMode_Mask                                 cBit3_0
#define cAf6_ip_eth_trip_speed_pcs_opt_test_ctrl_IPEthTripPcsTestMode_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_pcs_opt_test_status
Reg Addr   : 0x3D
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is PRBS test status

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_pcs_opt_test_status                                                     0x3D

/*--------------------------------------
BitField Name: IPEthTripPcsTestRxAlign
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestRxAlign_Mask                                  cBit15
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestRxAlign_Shift                                      15

/*--------------------------------------
BitField Name: IPEthTripPcsTestCurrRxData
BitField Type: RW
BitField Desc:
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestCurrRxData_Mask                                cBit13_4
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestCurrRxData_Shift                                       4

/*--------------------------------------
BitField Name: IPEthTripPcsTestCurrPrbs15Er
BitField Type: RW
BitField Desc:
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestCurrPrbs15Er_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestCurrPrbs15Er_Shift                                       3

/*--------------------------------------
BitField Name: IPEthTripPcsTestCurrFixDatEr
BitField Type: RW
BitField Desc:
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestCurrFixDatEr_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestCurrFixDatEr_Shift                                       2

/*--------------------------------------
BitField Name: IPEthTripPcsTestLatchPrbs15Er
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestLatchPrbs15Er_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestLatchPrbs15Er_Shift                                       1

/*--------------------------------------
BitField Name: IPEthTripPcsTestLatchFixDatEr
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestLatchFixDatEr_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_pcs_opt_test_status_IPEthTripPcsTestLatchFixDatEr_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_active_ctrl
Reg Addr   : 0x40
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Active Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_active_ctrl                                                  0x40

/*--------------------------------------
BitField Name: IpEthTripSmacLoopout
BitField Type: RW
BitField Desc: Loop out Enable (TX ? RX) 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacLoopout_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacLoopout_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripSmacTxActive
BitField Type: RW
BitField Desc: Active transmitter side 1: Active 0: No Active
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacTxActive_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacTxActive_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripSmacLoopin
BitField Type: RW
BitField Desc: Loop In Enable (TX ? RX) 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacLoopin_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacLoopin_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripSmacRxActive
BitField Type: RW
BitField Desc: Active receiver side 1: Active 0: No Active
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacRxActive_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_simple_mac_active_ctrl_IpEthTripSmacRxActive_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_receiver_ctrl
Reg Addr   : 0x42
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Simple MAC Receiver Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_receiver_ctrl                                                0x42

/*--------------------------------------
BitField Name: IpEthTripSmacRxMask
BitField Type: RW
BitField Desc: Mask Error forward to user [3]: Ipg_Error: set 1 to Enable, else
is disable [2]: Rx_Error: set 1 to Enable, else is disable [1]: Prm_Error: set 1
to Enable, else is disable [0]: Fcs_Error: set 1 to Enable, else is disable
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxMask_Mask                               cBit15_12
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxMask_Shift                                      12

/*--------------------------------------
BitField Name: IpEthTripSmacRxIpg
BitField Type: RW
BitField Desc: Number of Inter packet Gap can detected for Receiver sides
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxIpg_Mask                                cBit11_8
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxIpg_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripSmacRxPrm
BitField Type: RW
BitField Desc: Number of preamble bytes that receiver side can be received, this
is maximum preamble bytes need to detect and one SFD byte
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxPrm_Mask                                 cBit7_4
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxPrm_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripSamcRxPauSaChk
BitField Type: RW
BitField Desc: Enable Pause  frame terminate SA check 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSamcRxPauSaChk_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSamcRxPauSaChk_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripSamcRxPauDaChk
BitField Type: RW
BitField Desc: Enable Pause  frame terminate DA check 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSamcRxPauDaChk_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSamcRxPauDaChk_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripSmacRxPauDiChk
BitField Type: RW
BitField Desc: Disable Pause frame termination 1: Disable 0: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxPauDiChk_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxPauDiChk_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripSmacRxFcsPass
BitField Type: RW
BitField Desc: Enable pass 4 byte FCS 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxFcsPass_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_simple_mac_receiver_ctrl_IpEthTripSmacRxFcsPass_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_transmitter_ctrl
Reg Addr   : 0x42 0x43
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Simple MAC Transmitter Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_transmitter_ctrl                                        0x42 0x43

/*--------------------------------------
BitField Name: IpEthTripSmacTxIpg
BitField Type: RW
BitField Desc: Number of Inter packet Gap generated for Transmitter sides. Value
range of parameter is from 4 to 31
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxIpg_Mask                                cBit12_8
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxIpg_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripSmacTxPrm
BitField Type: RW
BitField Desc: Number of preamble bytes (not include SFD byte) that Transmitter
generator.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxPrm_Mask                                 cBit7_4
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxPrm_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripSmacTxEr
BitField Type: RW
BitField Desc: Force TX_ER
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxEr_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxEr_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripSamcTxPadDis
BitField Type: RW
BitField Desc: Enable PAD insertion 1: Disable 0: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSamcTxPadDis_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSamcTxPadDis_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripSmacTxPauDis
BitField Type: RW
BitField Desc: Enable Pause  frame generator 1: Disable 0: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxPauDis_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxPauDis_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripSmacTxFcsIns
BitField Type: RW
BitField Desc: Enable FCS Insertion 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxFcsIns_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_simple_mac_transmitter_ctrl_IpEthTripSmacTxFcsIns_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_data_inter_frame
Reg Addr   : 0x45
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Simple MAC Data Inter Frame Control

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_data_inter_frame                                             0x45

/*--------------------------------------
BitField Name: IpEthTripSmacIdleRxData
BitField Type: RW
BitField Desc: Data Inter Frame Receive from PHY or PCS
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_data_inter_frame_IpEthTripSmacIdleRxData_Mask                                cBit15_8
#define cAf6_ip_eth_trip_speed_simple_mac_data_inter_frame_IpEthTripSmacIdleRxData_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripSmacIdleTxData
BitField Type: RW
BitField Desc: Data Inter Frame Transmit to PHY or PCS
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_data_inter_frame_IpEthTripSmacIdleTxData_Mask                                 cBit7_0
#define cAf6_ip_eth_trip_speed_simple_mac_data_inter_frame_IpEthTripSmacIdleTxData_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_flow_ctrl_time_interval
Reg Addr   : 0x46
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Simple MAC the Time Interval, it used for engine flow control.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_flow_ctrl_time_interval                                      0x46

/*--------------------------------------
BitField Name: IpEthTripSmacTimeInterval
BitField Type: RW
BitField Desc: In an time interval, a pause frame with quanta value that is
configured bel0, will be transmitted if received packet FIFO is full, unit value
is 512 bits time or 64 bytes time
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_flow_ctrl_time_interval_IpEthTripSmacTimeInterval_Mask                                cBit15_0
#define cAf6_ip_eth_trip_speed_simple_mac_flow_ctrl_time_interval_IpEthTripSmacTimeInterval_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_flow_ctrl_quanta
Reg Addr   : 0x47
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Simple MAC the Quanta, it used for engine flow control.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_flow_ctrl_quanta                                             0x47

/*--------------------------------------
BitField Name: IpEthTripSmacQuanta
BitField Type: RW
BitField Desc: Quanta value of pause ON frame, unit is 512 bits time or 64 bytes
time
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_flow_ctrl_quanta_IpEthTripSmacQuanta_Mask                                cBit15_0
#define cAf6_ip_eth_trip_speed_simple_mac_flow_ctrl_quanta_IpEthTripSmacQuanta_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_interrupt_status
Reg Addr   : 0x48
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is Interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_interrupt_status                                             0x48

/*--------------------------------------
BitField Name: IpEthTripSmacViolInfo
BitField Type: RW
BitField Desc: Information violation between SMAC and USER at TxSide
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_interrupt_status_IpEthTripSmacViolInfo_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_simple_mac_interrupt_status_IpEthTripSmacViolInfo_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripSmacLostSop
BitField Type: RW
BitField Desc: Lost SOP detection
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_interrupt_status_IpEthTripSmacLostSop_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_simple_mac_interrupt_status_IpEthTripSmacLostSop_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripSmacLostEop
BitField Type: RW
BitField Desc: Lost EOP detection or TX FIFO empty when packet transmitting
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_interrupt_status_IpEthTripSmacLostEop_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_simple_mac_interrupt_status_IpEthTripSmacLostEop_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ip_eth_trip_speed_simple_mac_latch_status
Reg Addr   : 0x49
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is latch status, Used to HW debug

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_eth_trip_speed_simple_mac_latch_status                                                 0x49

/*--------------------------------------
BitField Name: IpEthTripSmacLatPauGen
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatPauGen_Mask                                  cBit10
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatPauGen_Shift                                      10

/*--------------------------------------
BitField Name: IpEthTripSmacLatUsrRxVl
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatUsrRxVl_Mask                                   cBit9
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatUsrRxVl_Shift                                       9

/*--------------------------------------
BitField Name: IpEthTripSmacLatUsrTxVl
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatUsrTxVl_Mask                                   cBit8
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatUsrTxVl_Shift                                       8

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacTxRdEr
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxRdEr_Mask                                   cBit6
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxRdEr_Shift                                       6

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacTxReq
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxReq_Mask                                   cBit5
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxReq_Shift                                       5

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacTxVl
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxVl_Mask                                   cBit4
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxVl_Shift                                       4

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacRxEr
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacRxEr_Mask                                   cBit3
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacRxEr_Shift                                       3

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacRxDv
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacRxDv_Mask                                   cBit2
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacRxDv_Shift                                       2

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacTxEr
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxEr_Mask                                   cBit1
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxEr_Shift                                       1

/*--------------------------------------
BitField Name: IpEthTripSmacLatSmacTxEn
BitField Type: RW
BitField Desc: 0x0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxEn_Mask                                   cBit0
#define cAf6_ip_eth_trip_speed_simple_mac_latch_status_IpEthTripSmacLatSmacTxEn_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH FCS Force Error Control
Reg Addr   : 0x0001007
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_FCS_errins_en_cfg                                                                0x0001007

/*--------------------------------------
BitField Name: ETHFCSErrMode
BitField Type: RW
BitField Desc: 0: FCS (MAC); 1:Tx interface MAC2PCS; 2: Rx interface PCS2MAC
BitField Bits: [30:29]
--------------------------------------*/
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrMode_Mask                                                cBit30_29
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrMode_Shift                                                      29

/*--------------------------------------
BitField Name: ETHFCSErrRate
BitField Type: RW
BitField Desc: 0: One-shot with number of errors; 1:Line rate
BitField Bits: [28]
--------------------------------------*/
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrRate_Mask                                                   cBit28
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrRate_Shift                                                      28

/*--------------------------------------
BitField Name: ETHFCSErrThr
BitField Type: RW
BitField Desc: Error Threshold in bit unit or the number of error in one-shot
mode. Valid value from 1
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrThr_Mask                                                  cBit27_0
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrThr_Shift                                                        0

#endif /* _AF6_REG_AF6CNC0011_RD_ETH_H_ */

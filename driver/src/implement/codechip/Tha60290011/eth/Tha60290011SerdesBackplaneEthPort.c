/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290011SerdesBackplaneEthPort.c
 *
 * Created Date: Mar 21, 2017
 *
 * Description : Serdes backplan eth port implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtEthPortInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/eth/Tha60290021SerdesBackplaneEthPort.h"
#include "../man/Tha60290011DeviceReg.h"
#include "../man/Tha60290011DeviceInternal.h"
#include "../physical/Tha60290011SerdesManager.h"
#include "../eth/Tha60290011ModuleEth.h"
#include "Tha60290011SerdesBackplaneEthPort.h"
#include "Tha60290011SerdesBackplaneEthPortReg.h"
#include "Tha60290011SerdesBackplaneEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290011SerdesBackplaneEthPort *)self)
#define mAddressWithLocalAddress(self, localAddress) mMethodsGet((Tha60290021SerdesBackplaneEthPort)self)->AddressWithLocalAddress((Tha60290021SerdesBackplaneEthPort)self, localAddress)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;
static tTha60290021SerdesBackplaneEthPortMethods m_Tha60290021SerdesBackplaneEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;
static const tTha60290021SerdesBackplaneEthPortMethods *m_Tha60290021SerdesBackplaneEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtChannel self)
    {
    return ThaModuleEthMacBaseAddress((ThaModuleEth)AtChannelModuleGet(self), (AtEthPort)self);
    }

static uint32 AddressWithLocalAddress(Tha60290021SerdesBackplaneEthPort self, uint32 localAddress)
    {
    AtChannel port = (AtChannel)self;
    return localAddress + BaseAddress(port);
    }

static uint32 CounterLocalAddress(Tha60290021SerdesBackplaneEthPort self, uint16 counterType)
    {
    switch (counterType)
        {
        case cAtEthPortCounterRxFecIncCorrectCount:     return 0x20A2;
        case cAtEthPortCounterRxFecIncCantCorrectCount: return 0x20A3;
        case cAtEthPortCounterRxFecLockErrorCount:      return 0x20A4;
        case cAtEthPortCounterRxErrPackets      :       return 0x20A5;
        case cAtEthPortCounterRxPcsInvalidCount :       return 0x20A6;
        default:
            return m_Tha60290021SerdesBackplaneEthPortMethods->CounterLocalAddress(self, counterType);
        }
    }

static uint32 TxCfgAddress(Tha60290021SerdesBackplaneEthPort self)
    {
    return mAddressWithLocalAddress((AtEthPort)self, cAf6Reg_ETH_10G_TX_CFG_Base);
    }

static uint32 RxCfgAddress(Tha60290021SerdesBackplaneEthPort self)
    {
    return mAddressWithLocalAddress((AtEthPort)self, cAf6Reg_ETH_10G_RX_CFG_Base);
    }

static eAtModuleEthRet CounterTickModeSet(Tha60290021SerdesBackplaneEthPort self, eTha602900xxEthPortCounterTickMode mode)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 regAddr = mAddressWithLocalAddress(port, cAf6Reg_ETH_10G_TX_CFG_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 hwMode = 1; /* To work with input PM tick by default */

    /* Need to always enable counter logic */
    mRegFieldSet(regVal, cAf6_ETH_10G_TX_CFG_upactive_, 1);

    if (mode == cTha602900xxEthPortCounterTickModeManual)
        hwMode = 0;
    mRegFieldSet(regVal, cAf6_ETH_10G_TX_CFG_cfg_sel_tick_, hwMode);

    mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eTha602900xxEthPortCounterTickMode CounterTickModeGet(Tha60290021SerdesBackplaneEthPort self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 regAddr = mAddressWithLocalAddress(port, cAf6Reg_ETH_10G_TX_CFG_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);

    if ((regVal & cAf6_ETH_10G_TX_CFG_upactive_Mask) == 0)
        return cTha602900xxEthPortCounterTickModeDisable;

    if (regVal & cAf6_ETH_10G_TX_CFG_cfg_sel_tick_Mask)
        return cTha602900xxEthPortCounterTickModeAuto;

    return cTha602900xxEthPortCounterTickModeManual;
    }

static uint32 Reg_ETH_CFG_TICK_REG_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_10G_CFG_TICK_REG_Base;
    }

static uint32 Reg_ETH_AN_STICKY_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_10G_AN_STICKY_Base;
    }

static uint32 Reg_ETH_RX_INTEN_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_10G_RX_INTEN_Base;
    }

static uint32 Reg_ETH_TX_INTEN_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_10G_TX_INTEN_Base;
    }

static uint32 Reg_ETH_AN_INTEN_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_10G_AN_INTEN_Base;
    }

static eBool FecIsSupported(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return Tha60290011DeviceBackplaneSerdesEthPortFecIsSupported(device);
    }

static eBool HasInterruptMaskRegister(Tha60290021SerdesBackplaneEthPort self)
    {
    /* Interrupt mask registers are added and some interrupt status registers
     * change address */
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return Tha60290011DeviceBackplaneSerdesEthPortInterruptIsSupported(device);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    uint32 supportedMask = cAtEthPortAlarmTxFault                       |
                           cAtEthPortAlarmRemoteFault                   |
                           cAtEthPortAlarmRxInternalFault               |
                           cAtEthPortAlarmRxReceivedLocalFault          |
                           cAtEthPortAlarmLocalFault                    |
                           cAtEthPortAlarmRxTruncated                   |
                           cAtEthPortAlarmRxHiBer                       |
                           cAtEthPortAlarmAutoNegParallelDetectionFault |
                           cAtEthPortAlarmRxFramingError                |
                           cAtEthPortAlarmAutoNegFecIncCantCorrect      |
                           cAtEthPortAlarmAutoNegFecIncCorrect          |
                           cAtEthPortAlarmAutoNegFecLockError           |
                           cAtEthPortAlarmLinkDown;

    AtUnused(self);
    return supportedMask;
    }

static eAtRet RxInterruptMaskSet(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = Tha60290021SerdesbackplaneEthPortRxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (defectMask & cAtEthPortAlarmLocalFault)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_,
                     (enableMask & cAtEthPortAlarmLocalFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRemoteFault)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_,
                     (enableMask & cAtEthPortAlarmRemoteFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxInternalFault)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_,
                     (enableMask & cAtEthPortAlarmRxInternalFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxReceivedLocalFault)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_,
                     (enableMask & cAtEthPortAlarmRxReceivedLocalFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxTruncated)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_,
                     (enableMask & cAtEthPortAlarmRxTruncated) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxHiBer)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_,
                     (enableMask & cAtEthPortAlarmRxHiBer) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxFramingError)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_,
                     (enableMask & cAtEthPortAlarmRxFramingError) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmAutoNegFecIncCantCorrect)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_,
                     (enableMask & cAtEthPortAlarmAutoNegFecIncCantCorrect) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmAutoNegFecIncCorrect)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_,
                     (enableMask & cAtEthPortAlarmAutoNegFecIncCorrect) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmAutoNegFecLockError)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_,
                     (enableMask & cAtEthPortAlarmAutoNegFecLockError) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmLinkDown)
        mRegFieldSet(regVal, cAf6_ETH_10G_RX_INTEN_int_en_rx_status_,
                     (enableMask & cAtEthPortAlarmLinkDown) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet TxInterruptMaskSet(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = Tha60290021SerdesbackplaneEthPortTxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (defectMask & cAtEthPortAlarmTxFault)
        mRegFieldSet(regVal, cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_, (enableMask & cAtEthPortAlarmTxFault) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet AutoNegInterruptMaskSet(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = Tha60290021SerdesbackplaneEthPortAutoNegInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (defectMask & cAtEthPortAlarmAutoNegParallelDetectionFault)
        mRegFieldSet(regVal, cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_, (enableMask & cAtEthPortAlarmAutoNegParallelDetectionFault) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 RxInterruptMaskGet(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 regAddr = Tha60290021SerdesbackplaneEthPortRxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defectMask = 0;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_Mask)
        defectMask |= cAtEthPortAlarmLocalFault;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_Mask)
        defectMask |= cAtEthPortAlarmRemoteFault;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_Mask)
        defectMask |= cAtEthPortAlarmRxInternalFault;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_Mask)
        defectMask |= cAtEthPortAlarmRxReceivedLocalFault;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_Mask)
        defectMask |= cAtEthPortAlarmRxTruncated;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_Mask)
        defectMask |= cAtEthPortAlarmRxHiBer;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_Mask)
        defectMask |= cAtEthPortAlarmRxFramingError;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_Mask)
        defectMask |= cAtEthPortAlarmAutoNegFecIncCantCorrect;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_Mask)
        defectMask |= cAtEthPortAlarmAutoNegFecIncCorrect;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_Mask)
        defectMask |= cAtEthPortAlarmAutoNegFecLockError;

    if (regVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_status_Mask)
        defectMask |= cAtEthPortAlarmLinkDown;

    return defectMask;
    }

static uint32 TxInterruptMaskGet(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 regAddr = Tha60290021SerdesbackplaneEthPortTxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defectMask = 0;

    if (regVal & cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_Mask)
        defectMask |= cAtEthPortAlarmTxFault;

    return defectMask;
    }

static uint32 AutoNegInterruptMaskGet(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 regAddr = Tha60290021SerdesbackplaneEthPortAutoNegInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defectMask = 0;

    if (regVal & cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_Mask)
        defectMask |= cAtEthPortAlarmAutoNegParallelDetectionFault;

    return defectMask;
    }

static uint32 RxInterruptStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_10G_RX_STICKY_Base);
    }

static uint32 TxInterruptStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_10G_TX_STICKY_Base);
    }

static uint32 AutoNegInterruptStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_10G_AN_STICKY_Base);
    }

static uint32 RxCurrentStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_10G_RX_ALARM_Base);
    }

static uint32 TxCurrentStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_10G_TX_ALARM_Base);
    }

static uint32 AutoNegCurrentStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_10G_AN_ALARM_Base);
    }

static uint32 RxInterruptProcess(AtEthPort self)
    {
    uint32 intrEnReg  = Tha60290021SerdesbackplaneEthPortRxInterruptMaskRegister(self);
    uint32 intrStaReg = RxInterruptStatusRegister(self);
    uint32 currStaReg = RxCurrentStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_Mask)
        {
        events |= cAtEthPortAlarmLocalFault;
        intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_Mask;

        if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_local_fault_Mask)
            defects |= cAtEthPortAlarmLocalFault;
        }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_Mask)
        {
        events |= cAtEthPortAlarmRemoteFault;
        intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_Mask;

        if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_remote_fault_Mask)
            defects |= cAtEthPortAlarmRemoteFault;
        }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_Mask)
        {
        events |= cAtEthPortAlarmRxInternalFault;
        intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_Mask;

        if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_internal_local_fault_Mask)
            defects |= cAtEthPortAlarmRxInternalFault;
        }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_Mask)
        {
        events |= cAtEthPortAlarmRxReceivedLocalFault;
        intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_Mask;

        if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_received_local_fault_Mask)
            defects |= cAtEthPortAlarmRxReceivedLocalFault;
        }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_Mask)
        {
        events |= cAtEthPortAlarmRxTruncated;
        intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_Mask;

        if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_truncated_Mask)
            defects |= cAtEthPortAlarmRxTruncated;
        }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_Mask)
         {
         events |= cAtEthPortAlarmRxHiBer;
         intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_Mask;

         if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_hi_ber_Mask)
             defects |= cAtEthPortAlarmRxHiBer;
         }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_Mask)
         {
         events |= cAtEthPortAlarmRxFramingError;
         intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_Mask;

         if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_framing_err_Mask)
             defects |= cAtEthPortAlarmRxFramingError;
         }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_Mask)
         {
         events |= cAtEthPortAlarmAutoNegFecIncCantCorrect;
         intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_Mask;

         if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_cant_correct_count_Mask)
             defects |= cAtEthPortAlarmAutoNegFecIncCantCorrect;
         }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_Mask)
         {
         events |= cAtEthPortAlarmAutoNegFecIncCorrect;
         intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_Mask;

         if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_inc_correct_count_Mask)
             defects |= cAtEthPortAlarmAutoNegFecIncCorrect;
         }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_Mask)
         {
         events |= cAtEthPortAlarmAutoNegFecLockError;
         intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_Mask;

         if (currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_fec_lock_error_Mask)
             defects |= cAtEthPortAlarmAutoNegFecLockError;
         }

    if (intrStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_status_Mask)
         {
         events |= cAtEthPortAlarmLinkDown;
         intrSta2Clear |= cAf6_ETH_10G_RX_INTEN_int_en_rx_status_Mask;

         if (~currStaVal & cAf6_ETH_10G_RX_INTEN_int_en_rx_status_Mask)
             defects |= cAtEthPortAlarmLinkDown;
         }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 TxInterruptProcess(AtEthPort self)
    {
    uint32 intrEnReg  = Tha60290021SerdesbackplaneEthPortTxInterruptMaskRegister(self);
    uint32 intrStaReg = TxInterruptStatusRegister(self);
    uint32 currStaReg = TxCurrentStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    if (intrStaVal & cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_Mask)
        {
        events |= cAtEthPortAlarmTxFault;
        intrSta2Clear |= cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_Mask;

        if (currStaVal & cAf6_ETH_10G_TX_INTEN_int_en_tx_local_fault_Mask)
            defects |= cAtEthPortAlarmTxFault;
        }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 AutoNegInterruptProcess(AtEthPort self)
    {
    uint32 intrEnReg  = Tha60290021SerdesbackplaneEthPortAutoNegInterruptMaskRegister(self);
    uint32 intrStaReg = AutoNegInterruptStatusRegister(self);
    uint32 currStaReg = AutoNegCurrentStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    if (intrStaVal & cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_Mask)
        {
        events |= cAtEthPortAlarmAutoNegParallelDetectionFault;
        intrSta2Clear |= cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_Mask;

        if (currStaVal & cAf6_ETH_10G_AN_INTEN_int_en_an_parallel_detection_fault_Mask)
            defects |= cAtEthPortAlarmAutoNegParallelDetectionFault;
        }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 RxDefectGet(AtEthPort self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = RxCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_local_fault_Mask)
        swDefect |= cAtEthPortAlarmLocalFault;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_remote_fault_Mask)
        swDefect |= cAtEthPortAlarmRemoteFault;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_internal_local_fault_Mask)
        swDefect |= cAtEthPortAlarmRxInternalFault;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_received_local_fault_Mask)
        swDefect |= cAtEthPortAlarmRxReceivedLocalFault;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_truncated_Mask)
        swDefect |= cAtEthPortAlarmRxTruncated;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_hi_ber_Mask)
        swDefect |= cAtEthPortAlarmRxHiBer;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_framing_err_Mask)
        swDefect |= cAtEthPortAlarmRxFramingError;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_fec_inc_cant_correct_count_Mask)
        swDefect |= cAtEthPortAlarmAutoNegFecIncCantCorrect;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_fec_inc_correct_count_Mask)
        swDefect |= cAtEthPortAlarmAutoNegFecIncCorrect;

    if (regVal & cAf6_ETH_10G_RX_ALARM_stat_fec_lock_error_Mask)
        swDefect |= cAtEthPortAlarmAutoNegFecLockError;

    if (~regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_status_Mask)
        swDefect |= cAtEthPortAlarmLinkDown;

    return swDefect;
    }

static uint32 TxDefectGet(AtEthPort self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = TxCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_10G_TX_ALARM_stat_tx_local_fault_Mask)
        swDefect |= cAtEthPortAlarmTxFault;

    return swDefect;
    }

static uint32 AutoNegDefectGet(AtEthPort self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = AutoNegCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_10G_AN_ALARM_stat_an_parallel_detection_fault_Mask)
        swDefect |= cAtEthPortAlarmAutoNegParallelDetectionFault;

    return swDefect;
    }

static uint32 RxDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = RxInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmLocalFault;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_local_fault_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_remote_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmRemoteFault;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_remote_fault_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_internal_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmRxInternalFault;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_internal_local_fault_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_received_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmRxReceivedLocalFault;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_received_local_fault_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_truncated_Mask)
        {
        swDefect |= cAtEthPortAlarmRxTruncated;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_truncated_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_hi_ber_Mask)
        {
        swDefect |= cAtEthPortAlarmRxHiBer;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_hi_ber_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_framing_err_Mask)
        {
        swDefect |= cAtEthPortAlarmRxFramingError;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_framing_err_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkfec_inc_cant_correct_count_Mask)
        {
        swDefect |= cAtEthPortAlarmAutoNegFecIncCantCorrect;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkfec_inc_cant_correct_count_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkfec_inc_correct_count_Mask)
        {
        swDefect |= cAtEthPortAlarmAutoNegFecIncCorrect;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkfec_inc_correct_count_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkfec_lock_error_Mask)
        {
        swDefect |= cAtEthPortAlarmAutoNegFecLockError;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkfec_lock_error_Mask;
        }

    if (regVal & cAf6_ETH_10G_RX_STICKY_stkrx_status_Mask)
        {
        swDefect |= cAtEthPortAlarmLinkDown;
        hwDefect |= cAf6_ETH_10G_RX_STICKY_stkrx_status_Mask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static uint32 TxDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = TxInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_10G_TX_STICKY_stktx_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmTxFault;
        hwDefect |= cAf6_ETH_10G_TX_STICKY_stktx_local_fault_Mask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static uint32 AutoNegHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = AutoNegInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_10G_AN_STICKY_stkan_parallel_detection_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmAutoNegParallelDetectionFault;
        hwDefect |= cAf6_ETH_10G_AN_STICKY_stkan_parallel_detection_fault_Mask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60290011ModuleEthSerdesForPort(module, (uint8)AtChannelIdGet((AtChannel)self));
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed10G;
    }

static eAtEthPortSpeed AutoNegSpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed10G;
    }

static AtEthPort SubPortObjectCreate(AtEthPort self, uint8 subPortId)
    {
    AtUnused(self);
    AtUnused(subPortId);
    return NULL;
    }

static uint32 MaxSubPortsGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtEthPortInterface DefaultInterface(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceBaseKR;
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    uint32 regAddr = RxCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_10G_RX_ALARM_stat_rx_status_Mask) ? cAtEthPortLinkStatusUp : cAtEthPortLinkStatusDown;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideTha60290021SerdesBackplaneEthPort(AtEthPort self)
    {
    Tha60290021SerdesBackplaneEthPort port = (Tha60290021SerdesBackplaneEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SerdesBackplaneEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesBackplaneEthPortOverride, m_Tha60290021SerdesBackplaneEthPortMethods,
                                  sizeof(m_Tha60290021SerdesBackplaneEthPortOverride));

        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, AddressWithLocalAddress);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, CounterLocalAddress);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, TxCfgAddress);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, RxCfgAddress);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, CounterTickModeSet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, CounterTickModeGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, Reg_ETH_CFG_TICK_REG_Base);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, Reg_ETH_AN_STICKY_Base);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, Reg_ETH_AN_INTEN_Base);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, Reg_ETH_TX_INTEN_Base);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, Reg_ETH_RX_INTEN_Base);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, HasInterruptMaskRegister);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, RxInterruptMaskSet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, TxInterruptMaskSet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, AutoNegInterruptMaskSet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, RxInterruptMaskGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, TxInterruptMaskGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, AutoNegInterruptMaskGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, RxInterruptProcess);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, TxInterruptProcess);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, AutoNegInterruptProcess);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, RxDefectGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, TxDefectGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, AutoNegDefectGet);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, RxDefectHistoryRead2Clear);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, TxDefectHistoryRead2Clear);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, AutoNegHistoryRead2Clear);
        }

    mMethodsSet(port, &m_Tha60290021SerdesBackplaneEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, AutoNegSpeedGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, MaxSubPortsGet);
        mMethodOverride(m_AtEthPortOverride, SubPortObjectCreate);
        mMethodOverride(m_AtEthPortOverride, FecIsSupported);
        mMethodOverride(m_AtEthPortOverride, DefaultInterface);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290021SerdesBackplaneEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011SerdesBackplaneEthPort);
    }

AtEthPort Tha60290011SerdesBackplaneEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SerdesBackplaneEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290011SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290011SerdesBackplaneEthPortObjectInit(newPort, portId, module);
    }

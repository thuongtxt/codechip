/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210031ModulePda.h
 * 
 * Created Date: Jan 20, 2016
 *
 * Description : PDA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEPDA_H_
#define _THA60210031MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pda/ThaModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha6021DsxModulePdaJitterBufferAdditionalBytesIsSupported(ThaModulePda self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEPDA_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210031ModulePdaInternal.h
 * 
 * Created Date: Oct 28, 2016
 *
 * Description : PDA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEPDAINTERNAL_H_
#define _THA60210031MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/pda/Tha60150011ModulePdaInternal.h"
#include "Tha60210031ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModulePda * Tha60210031ModulePda;

typedef struct tTha60210031ModulePdaMethods
    {
    uint32 (*StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated)(Tha60210031ModulePda self);
    eBool (*JitterBufferAdditionalBytesIsSupported)(Tha60210031ModulePda self);
    }tTha60210031ModulePdaMethods;

typedef struct tTha60210031ModulePda
    {
    tTha60150011ModulePda super;
    const tTha60210031ModulePdaMethods *methods;
    }tTha60210031ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210031ModulePdaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEPDAINTERNAL_H_ */


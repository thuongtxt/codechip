/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA Module Name
 *
 * File        : Tha60210031ModulePda.c
 *
 * Created Date: Jul 1, 2015
 *
 * Description : Overriden PDA functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/pda/Tha60210051ModulePda.h"
#include "Tha60210031ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cCepIndicateMask  cBit24
#define cCepIndicateShift 24

/* For RDI disable */
#define cThaRegPWPdaMdCtrlPDARdiOffMask   cBit28
#define cThaRegPWPdaMdCtrlPDARdiOffShift  28

/*--------------------------- Macros -----------------------------------------*/
#define mModulePdaV2(self) ((ThaModulePdaV2)self)
#define mThis(self) ((Tha60210031ModulePda)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031ModulePdaMethods m_methods;

/* Override */
static tThaModulePdaMethods   m_ThaModulePdaOverride;
static tThaModulePdaV2Methods m_ThaModulePdaV2Override;

/* Save super implementation */
static const tThaModulePdaMethods *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 SAToPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
    eAtPdhChannelType channelType = (pdhChannel) ? AtPdhChannelTypeGet(pdhChannel) : cAtPdhChannelTypeUnknown;
    AtUnused(self);

    if (channelType == cAtPdhChannelTypeE1)
        return 128;

    if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
        return 256;

    return 96;
    }

static uint16 MaxPayloadSize(uint16 mtu, AtPw pw)
    {
    uint32 totalHeaderLength = ThaPwAdapterTotalHeaderLengthGet(pw);
    if (mtu < totalHeaderLength)
        return 0;

    return (uint16)(mtu - totalHeaderLength);
    }

static uint16 SAToPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    const uint16 cSAToPMtu = 2048;
    AtUnused(self);

    return MaxPayloadSize(cSAToPMtu, pw);
    }

static uint16 CEPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    const uint16 cCEPMtu = 2044;
    AtUnused(self);

    return MaxPayloadSize(cCEPMtu, pw);
    }

static uint16 CEPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);

    if ((channelType == cAtSdhChannelTypeVc11) ||
        (channelType == cAtSdhChannelTypeUnknown))
        return 104;

    if (channelType == cAtSdhChannelTypeVc12)
        return 140;

    return 256;
    }

static uint16 CESoPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    return Tha6021ModulePdaCESoPMinPayloadSize(self, pw);
    }

static eBool PwRxPacketsSentToTdmCounterIsSupported(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MinNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return 250;
    }

static uint32 MaxNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return 256000;
    }

static uint32 CEPMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(vc);
    AtUnused(self);

    if ((channelType == cAtSdhChannelTypeVc11) ||
        (channelType == cAtSdhChannelTypeVc12))
        return 2;

    return 3;
    }

static ThaVersionReader VersionReader(ThaModulePda self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated(Tha60210031ModulePda self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x04, 0x03, 0);
    }

static eBool CepPayloadSizeShouldExcludeHeader(ThaModulePda self)
    {
    uint32 hwVersion;

    hwVersion = ThaVersionReaderHardwareVersion(VersionReader(self));
    if (hwVersion < mMethodsGet(mThis(self))->StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated(mThis(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool CepMustBeIndicated(ThaModulePda self)
    {
    uint32 hwVersion;

    hwVersion = ThaVersionReaderHardwareVersion(VersionReader(self));
    if (hwVersion < mMethodsGet(mThis(self))->StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated(mThis(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet CepIndicate(ThaModulePda self, AtPw pw, eBool isCep)
    {
    uint32 address = mMethodsGet(mModulePdaV2(self))->PWPdaJitBufCtrl(mModulePdaV2(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    if (!CepMustBeIndicated(self))
        return cAtOk;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[0], cCepIndicate, isCep ? 1 : 0);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 EparLineIdMask(ThaModulePda self)
    {
    AtUnused(self);
    return cBit10_1;
    }

static uint8 EparLineIdShift(ThaModulePda self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 EparSliceIdMask(ThaModulePda self)
    {
    AtUnused(self);
    return cBit11;
    }

static uint8 EparSliceIdShift(ThaModulePda self)
    {
    AtUnused(self);
    return 11;
    }

static eAtRet PwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    eAtRet ret = cAtOk;
    eAtPwType pwType = AtPwTypeGet(pw);

    /* Incase SAToP and CESoP, field LOPS also need to configure corresponding */
    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        ret = mMethodsGet(self)->PwLopsSetThresholdSet(self, pw, numPackets);
    if (ret != cAtOk)
        return ret;

    return m_ThaModulePdaMethods->PwLofsSetThresholdSet(self, pw, numPackets);
    }

static uint32 StartHwVersionJitterBufferStatusAdditionalBytesNoMoreSupported(ThaModulePda self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x04, 0x03, 0);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    if (Tha6021DsxModulePdaJitterBufferAdditionalBytesIsSupported(self))
        return m_ThaModulePdaMethods->NumCurrentAdditionalBytesInJitterBuffer(self, pwAdapter);

    return 0;
    }

static eBool LopsThresholdHasLimitOneSecond(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool JitterBufferAdditionalBytesIsSupported(Tha60210031ModulePda self)
    {
    uint32 hwVersion;
    ThaModulePda pda = (ThaModulePda)self;

    hwVersion = ThaVersionReaderHardwareVersion(VersionReader(pda));
    if (hwVersion < StartHwVersionJitterBufferStatusAdditionalBytesNoMoreSupported(pda))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwCwAutoRxMBitEnable(ThaModulePda self, AtPw pwAdapter, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pwAdapter) + mPwOffset(self, pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);

    mRegFieldSet(regVal, cThaRegPWPdaMdCtrlPDARdiOff, enable ? 0 : 1);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool PwCwAutoRxMBitIsEnabled(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pwAdapter) + mPwOffset(self, pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    return (regVal & cThaRegPWPdaMdCtrlPDARdiOffMask) ? cAtFalse : cAtTrue;
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4644);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionSupport) ? cAtTrue : cAtFalse;
    }

static uint32 PDATdmJitBufStat(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 0x24A000;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePda self)
    {
    return mModuleHwRead(self, 0x0C0007);
    }

static eBool CanControlLopsPktReplaceMode(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4646);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionSupport) ? cAtTrue : cAtFalse;
    }

static uint32 MinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return Tha6021ModulePdaMinJitterDelay(self, pw, circuit, payloadSize);
    }

static uint32 CEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return Tha6021ModulePdaCEPMinJitterDelay(self, pw, circuit, payloadSize);
    }

static uint32 ExtraJitterBuffer(AtPw pw)
    {
    return ThaPwAdapterExtraJitterBufferGet((ThaPwAdapter)pw);
    }

static uint32 MinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return MinJitterDelay(self, pw, circuit, payloadSize) * 2U + ExtraJitterBuffer(pw);
    }

static uint32 CEPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return CEPMinJitterDelay(self, pw, circuit, payloadSize) * 2U + ExtraJitterBuffer(pw);
    }

static uint16 NumberOfPacketByUs(ThaModulePda self, AtPw pw, uint32 numUs)
    {
    return Tha6021ModulePdaNumberOfPacketByUs(self, pw, numUs);
    }

static eBool PwJitterBufferSizeIsValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs)
    {
    uint32 jitterDelayUs = AtPwJitterBufferDelayGet(pw);
    AtUnused(self);

    /* New formula makes constrain on the value of jitter buffer size, which is
     * at least twice jitter delay. */
    if ((jitterDelayUs * 2U + ExtraJitterBuffer(pw)) > jitterBufferUs)
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwJitterDelayIsValid(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(jitterDelayUs);
    /* New formula does not make constrain on the value of jitter delay. */
    return cAtTrue;
    }

static eBool PwJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs)
    {
    return Tha6021ModulePdaJitterBufferAndJitterDelayAreValid(self, pw, jitterBufferUs, jitterDelayUs);
    }

static eBool ShouldSetDefaultJitterBufferSizeOnJitterDelaySet(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static uint32 DefaultJitterBufferSize(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    AtUnused(self);
    return jitterDelayUs * 2U + ExtraJitterBuffer(pw);
    }

static uint32 CESoPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    return m_ThaModulePdaMethods->CESoPMinJitterDelay(self, pw, circuit, payloadSizeInFrames);
    }

static uint32 CESoPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    return CESoPMinJitterDelay(self, pw, circuit, payloadSizeInFrames) * 2U + ExtraJitterBuffer(pw);
    }

static uint16 CESoPMaxNumFrames(ThaModulePda self, AtPw pw)
    {
    return Tha6021ModulePdaCESoPMaxNumFrames(self, pw);
    }

static uint16 CESoPMinNumFrames(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    if (ThaPwCESoPAdapterWithCas((ThaPwCESoPAdapter)pw))
        return 4U;

    return 1U;
    }

static uint32 MaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us)
    {
    return Tha6021ModulePdaMaxJitterDelayForJitterBuffer(self, pw, jitterBufferSize_us, ExtraJitterBuffer(pw));
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwRxPacketsSentToTdmCounterIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CEPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, MinNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, CepPayloadSizeShouldExcludeHeader);
        mMethodOverride(m_ThaModulePdaOverride, CepIndicate);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, LopsThresholdHasLimitOneSecond);
        mMethodOverride(m_ThaModulePdaOverride, EparLineIdMask);
        mMethodOverride(m_ThaModulePdaOverride, EparLineIdShift);
        mMethodOverride(m_ThaModulePdaOverride, EparSliceIdMask);
        mMethodOverride(m_ThaModulePdaOverride, EparSliceIdShift);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsEnabled);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, NumFreeBlocksHwGet);
        mMethodOverride(m_ThaModulePdaOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinNumPacketsForJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, MinJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, MinJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, NumberOfPacketByUs);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeIsValid);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterDelayIsValid);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferAndJitterDelayAreValid);
        mMethodOverride(m_ThaModulePdaOverride, ShouldSetDefaultJitterBufferSizeOnJitterDelaySet);
        mMethodOverride(m_ThaModulePdaOverride, DefaultJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMaxNumFrames);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinNumFrames);
        mMethodOverride(m_ThaModulePdaOverride, MaxJitterDelayForJitterBuffer);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideThaModulePdaV2(AtModule self)
    {
    ThaModulePdaV2 pdaModuleV2 = (ThaModulePdaV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaV2Override, mMethodsGet(pdaModuleV2), sizeof(m_ThaModulePdaV2Override));

        mMethodOverride(m_ThaModulePdaV2Override, PDATdmJitBufStat);
        }

    mMethodsSet(pdaModuleV2, &m_ThaModulePdaV2Override);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210031ModulePda pda = (Tha60210031ModulePda)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartHwVersionPayloadSideShouldExcludeHeaderAndCepMustBeIndicated);
        mMethodOverride(m_methods, JitterBufferAdditionalBytesIsSupported);
        }

    mMethodsSet(pda, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePda(self);
    OverrideThaModulePdaV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModulePda);
    }

AtModule Tha60210031ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModulePdaObjectInit(newModule, device);
    }

eBool Tha6021DsxModulePdaJitterBufferAdditionalBytesIsSupported(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(mThis(self))->JitterBufferAdditionalBytesIsSupported(mThis(self));
    return cAtFalse;
    }

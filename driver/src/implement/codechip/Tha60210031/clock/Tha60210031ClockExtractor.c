/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhPwProductClockExtractor.c
 *
 * Created Date: Aug 10, 2015
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031ClockExtractor.h"
#include "AtModulePdh.h"
#include "AtPdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/
#define cCDRReference8KOutputControl      0x000010C

#define cCdrRefLosDisMask(extractorId)   (((extractorId) == 0) ? cBit15 : cBit31)
#define cCdrRefLosDisShift(extractorId)  (((extractorId) == 0) ?     15 : 31)

#define cCdrRefIdMask(extractorId)       (((extractorId) == 0) ? cBit14_4 : cBit30_20)
#define cCdrRefIdShift(extractorId)      (((extractorId) == 0) ?        4 : 20)

#define cCdrRefPdhTypeMask(extractorId)  (((extractorId) == 0) ? cBit3_2 : cBit19_18)
#define cCdrRefPdhTypeShift(extractorId) (((extractorId) == 0) ?       2 : 18)

#define cCdrRefModeMask(extractorId)     (((extractorId) == 0) ? cBit1_0 : cBit17_16)
#define cCdrRefModeShift(extractorId)    (((extractorId) == 0) ?       0 : 16)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031ClockExtractor
    {
    tThaClockExtractor super;
    }tTha60210031ClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/* Save super implementation */
static const tAtClockExtractorMethods *m_AtClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ClockExtractor);
    }

static uint32 ReferenceSync8KOutputControl(ThaClockExtractor self)
    {
    AtUnused(self);
    return cCDRReference8KOutputControl;
    }

static eAtModuleClockRet HwSystemClockExtract(ThaClockExtractor self)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), 0);
    mFieldIns(&regVal, cCdrRefIdMask(extractorId), cCdrRefIdShift(extractorId), 0);
    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 0); /* System */

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eBool SystemClockIsExtracted(AtClockExtractor self)
    {
    uint8 extractMode;
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);
    uint32 regVal = AtClockExtractorRead(self, ReferenceSync8KOutputControl((ThaClockExtractor)self));
    mFieldGet(regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), uint8, &extractMode);
    return (extractMode == 0) ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    return ThaPwCommonProductClockExtractEnable((ThaClockExtractor)self, enable);
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    return ThaPwCommonProductClockExtractIsEnabled((ThaClockExtractor)self);
    }

static eBool CanExtractPdhDe1Clock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtTrue;
    }

static eBool CanExtractPdhDe1LiuClock(ThaClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleClockRet De1AcrDcrTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);
    uint32 cdrId = ThaCdrControllerIdGet(ThaPdhDe1CdrControllerGet((ThaPdhDe1 )de1)) + ThaPdhDe1SliceGet((ThaPdhDe1 )de1)* 0x300UL;

    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 2); /* CDR timing mode */
    mFieldIns(&regVal, cCdrRefIdMask(extractorId),  cCdrRefIdShift(extractorId), cdrId);
    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), 0); /* As Default mode */
    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eAtModuleClockRet De1LoopTimingModeExtract(ThaClockExtractor self, AtPdhDe1 de1)
    {
    AtUnused(de1);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool CanExtractPdhDe3Clock(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanExtractPdhDe3LiuClock(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanExtractSdhLineClock(ThaClockExtractor self, AtSdhLine line)
    {
    AtUnused(line);
    AtUnused(self);
    return cAtTrue;
    }

static uint32 HwPdhTypeGet(AtChannel line)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(line), cAtModulePdh);
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(modulePdh, AtChannelIdGet(line));
    uint32 serialLineMode = AtPdhSerialLineModeGet(serialLine);

    switch (serialLineMode)
        {
        case cAtPdhDe3SerialLineModeEc1: return 2;
        case cAtPdhDe3SerialLineModeDs3: return 1;
        case cAtPdhDe3SerialLineModeE3:  return 0;
        default:                         return 3;
        }
    }

static eBool LineModeIsValid(AtChannel line, eAtPdhDe3SerialLineMode expectedMode)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(line), cAtModulePdh);
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(modulePdh, AtChannelIdGet(line));
    uint32 serialLineMode = AtPdhSerialLineModeGet(serialLine);

    return (serialLineMode == expectedMode);
    }

static eAtModuleClockRet HwLineClockExtract(ThaClockExtractor self, AtChannel line, uint32 channelType)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    uint32 cdrId = (lineId << 5);

    mFieldIns(&regVal, cCdrRefLosDisMask(extractorId), cCdrRefLosDisShift(extractorId), 0); /* LOS Mask Output Clock! */
    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 1); /* Loop timing mode (or LIU) */
    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), channelType);
    mFieldIns(&regVal, cCdrRefIdMask(extractorId), cCdrRefIdShift(extractorId), cdrId);

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    AtChannel channel = (AtChannel)line;

    if (!LineModeIsValid(channel, cAtPdhDe3SerialLineModeEc1))
        return cAtErrorInvlParm;

    return HwLineClockExtract(self, channel, HwPdhTypeGet(channel));
    }

static eAtModuleClockRet HwPdhDe3LiuClockExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtChannel channel = (AtChannel)de3;

    if (LineModeIsValid(channel, cAtPdhDe3SerialLineModeDs3) ||
        LineModeIsValid(channel, cAtPdhDe3SerialLineModeE3))
        return HwLineClockExtract(self, channel, HwPdhTypeGet(channel));

    return cAtErrorInvlParm;
    }

static eAtModuleClockRet De3AcrDcrTimingModeExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 2); /* CDR timing mode */
    mFieldIns(&regVal, cCdrRefIdMask(extractorId), cCdrRefIdShift(extractorId), ThaCdrControllerIdGet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)de3)));
    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), AtPdhDe3IsE3(de3) ? 0 : 1);

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static eAtModuleClockRet De3LoopTimingModeExtract(ThaClockExtractor self, AtPdhDe3 de3)
    {
    AtClockExtractor extractor = (AtClockExtractor)self;
    uint32 regAddr = ReferenceSync8KOutputControl(self);
    uint32 regVal  = AtClockExtractorRead(extractor, regAddr);
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor)self);

    mFieldIns(&regVal, cCdrRefModeMask(extractorId), cCdrRefModeShift(extractorId), 1); /* Loop timing mode */
    mFieldIns(&regVal, cCdrRefIdMask(extractorId), cCdrRefIdShift(extractorId), ThaCdrControllerIdGet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)de3)));
    mFieldIns(&regVal, cCdrRefPdhTypeMask(extractorId), cCdrRefPdhTypeShift(extractorId), AtPdhDe3IsE3(de3) ? 0 : 1);

    AtClockExtractorWrite(extractor, regAddr, regVal);

    return cAtOk;
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, mMethodsGet(extractor), sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1Clock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe1LiuClock);
        mMethodOverride(m_ThaClockExtractorOverride, HwSystemClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De1AcrDcrTimingModeExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De1LoopTimingModeExtract);

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe3Clock);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractPdhDe3LiuClock);
        mMethodOverride(m_ThaClockExtractorOverride, HwPdhDe3LiuClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De3AcrDcrTimingModeExtract);
        mMethodOverride(m_ThaClockExtractorOverride, De3LoopTimingModeExtract);
        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSdhLineClock);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtClockExtractorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, SystemClockIsExtracted);
        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60210031ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60210031ModuleClock.h
 * 
 * Created Date: Aug, 2015
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60210031ClockExtractor_H_
#define _Tha60210031ClockExtractor_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/clock/ThaClockExtractorInternal.h"
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtClockExtractor Tha60210031ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _Tha60210031ClockExtractor_H_ */


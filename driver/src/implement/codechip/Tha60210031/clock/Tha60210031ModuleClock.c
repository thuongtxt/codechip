/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhPwProductModuleClock.c
 *
 * Created Date: Aug 10, 2015
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031ModuleClockInternal.h"
#include "Tha60210031ModuleClock.h"
#include "Tha60210031ClockExtractor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleClock);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
	AtUnused(self);
    return 2;
    }

static AtClockExtractor ExtractorCreate(ThaModuleClock self, uint8 extractorId)
    {
    return Tha60210031ClockExtractorNew((AtModuleClock)self, extractorId);
    }

static eBool ShouldEnableCdrInterrupt(ThaModuleClock self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SquelchingHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0x10d;
    }

static uint32 SquelchingHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return (cBit2_0 << (8 * id));
    }

static uint32 SquelchingHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return (8 * id);
    }

static uint32 OutputCounterHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0xF0006A;
    }

static uint32 OutputCounterHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return (cBit15_0 << (16 * id));
    }

static uint32 OutputCounterHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return (16 * id);
    }

static eBool SquelchingIsSupported(ThaModuleClock self)
    {
    AtModule module = (AtModule)self;
    AtDevice device = AtModuleDeviceGet(module);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersionSupportClockSquel = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4641);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startVersionSupportClockSquel)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, ExtractorCreate);
        mMethodOverride(m_ThaModuleClockOverride, ShouldEnableCdrInterrupt);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwAddress);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwMask);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwShift);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwAddress);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwMask);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwShift);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingIsSupported);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    }

AtModuleClock Tha60210031ModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60210031ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleClockObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210031ModulePwe.h
 * 
 * Created Date: Oct 16, 2015
 *
 * Description : 60210031 module pwe interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEPWE_H_
#define _THA60210031MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210031ModulePwePwEthPortQueueSet(ThaModulePwe self, AtPw adapter, uint8 queueId);
uint8 Tha60210031ModulePwePwEthPortQueueGet(ThaModulePwe self, AtPw adapter);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEPWE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210031ModulePwe.c
 *
 * Created Date: Oct 16, 2015
 *
 * Description : Module pwe of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cTxPwEthPortQueueMask  cBit10_8
#define cTxPwEthPortQueueShift 8

#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Mask                                  cBit21
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Shift                                     21
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Mask                                  cBit20
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Shift                                     20
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Mask                                cBit19_10
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Shift                                      10
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Mask                                 cBit9_0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Shift                                      0

#define cTxPwAutoNPbitMask    cBit6
#define cTxPwAutoNPbitShift   6

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods         m_ThaModulePweOverride;
static tThaModulePweV2Methods       m_ThaModulePweV2Override;
static tTha60150011ModulePweMethods m_Tha60150011ModulePweOverride;

/* Save super implementations */
static const tThaModulePweV2Methods *m_ThaModulePweV2Methods = NULL;
static const tThaModulePweMethods   *m_ThaModulePweMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Pseudowire_Transmit_HSPW_Protection_Control(ThaModulePweV2 self, uint32 groupIdx)
    {
    return ThaModulePweBaseAddress((ThaModulePwe)self) + 0x004000 + groupIdx;
    }

static eAtRet ApsGroupPwHwAdd(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 pwAdapterHwId = AtChannelHwIdGet((AtChannel)pwAdapter);
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, pwAdapterHwId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_, 1);
    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_, pwGroupHwId);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ApsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, AtChannelHwIdGet((AtChannel)pwAdapter));
    uint32 regVal = mModuleHwRead(self, regAddr);

    AtUnused(pwGroup);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, AtChannelHwIdGet((AtChannel)pwAdapter));
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_, 1);
    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_, AtPwGroupIdGet(pwGroup));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, AtChannelHwIdGet((AtChannel)pwAdapter));
    uint32 regVal = mModuleHwRead(self, regAddr);

    AtUnused(pwGroup);

    mRegFieldSet(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ShouldResetAllPwGroups(ThaModulePweV2 self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return ThaModulePwHspwIsSupported(modulePw);
    }

static eBool PwCwAutoTxNPBitIsConfigurable(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cTxPwAutoNPbit, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cTxPwAutoNPbitMask) ? cAtFalse : cAtTrue;
    }

static eBool PwGroupingShow(ThaModulePwe self, AtPw pw)
    {
    AtPwGroup apsGroup = AtPwApsGroupGet(pw);
    AtPwGroup hsGroup = AtPwHsGroupGet(pw);
    uint32 address;
    ThaModulePweV2 modulePweV2 = (ThaModulePweV2)self;

    if (apsGroup)
        {
        address = ThaModulePweV2Pseudowire_Transmit_UPSR_Group_Control(modulePweV2, AtChannelHwIdGet((AtChannel)pw));
        ThaModulePweRegDisplay(pw, "Pseudowire Transmit UPSR Group Control", address);

        address = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(modulePweV2, AtChannelHwIdGet((AtChannel)pw));
        ThaModulePweRegDisplay(pw, "Pseudowire Transmit UPSR and HSPW Control", address);
        }

    if (hsGroup)
        {
        address = ThaModulePweV2Pseudowire_Transmit_HSPW_Protection_Control(modulePweV2, AtChannelHwIdGet((AtChannel)pw));
        ThaModulePweRegDisplay(pw, "Pseudowire Transmit HSPW Protection Control", address);

        if (apsGroup == NULL)
            {
            address = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(modulePweV2, AtChannelHwIdGet((AtChannel)pw));
            ThaModulePweRegDisplay(pw, "Pseudowire Transmit UPSR and HSPW Control", address);
            }

        address = ThaModulePweV2Pseudowire_Transmit_HSPW_Label_Control(modulePweV2, AtChannelHwIdGet((AtChannel)pw));
        ThaModulePweRegDisplay(pw, "Pseudowire Transmit HSPW Label Control", address);
        }

    return (((apsGroup != NULL) || (hsGroup != NULL)) ? cAtTrue : cAtFalse);
    }

static void PwRegsShow(ThaModulePwe self, AtPw pw)
    {
    m_ThaModulePweMethods->PwRegsShow(self, pw);

    AtPrintc(cSevInfo, "* Pw grouping information:\r\n");
    if (PwGroupingShow(self, pw) == cAtFalse)
        AtPrintc(cSevNormal, "        Pw does not join to any group\r\n");
    }

static uint32 StartVersionSupportJitterAttenuator(Tha60150011ModulePwe self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x04, 0x06, 0x4624);
    }

static ThaVersionReader VersionReader(Tha60150011ModulePwe self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool JitterAttenuatorIsSupported(Tha60150011ModulePwe self)
    {
    uint32 hwVersion;

    hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    if (hwVersion < StartVersionSupportJitterAttenuator(self))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PwJitterAttenuatorEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    eBool shouldEnable = enable;

    /* HW recommends only enable Tx Shaper when run SAToP */
    if (AtPwTypeGet(pw) != cAtPwTypeSAToP)
        shouldEnable = cAtFalse;

    return m_ThaModulePweMethods->PwJitterAttenuatorEnable(self, pw, shouldEnable);
    }

static eBool ApsGroupPwIsInHwGroup(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 pwAdapterHwId = AtChannelHwIdGet((AtChannel)pwAdapter);
    uint32 regAddr = ThaModulePweV2Pseudowire_Transmit_UPSR_and_HSPW_Control(self, pwAdapterHwId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 isInGroup, hwGroupId;

    isInGroup = mRegField(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_);
    hwGroupId = mRegField(regVal, cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_);

    if ((hwGroupId == pwGroupHwId) && (isInGroup == 1))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 MaxNumCacheBlocks(ThaModulePwe self)
    {
    AtUnused(self);
    return 4096;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePwe self)
    {
    uint32 regVal = mModuleHwRead(self, 0xC0008) & cBit11_0;
    if (regVal == 0)
        return MaxNumCacheBlocks(self);

    return regVal;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitIsConfigurable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwRegsShow);
        mMethodOverride(m_ThaModulePweOverride, PwJitterAttenuatorEnable);
        mMethodOverride(m_ThaModulePweOverride, MaxNumCacheBlocks);
        mMethodOverride(m_ThaModulePweOverride, NumFreeBlocksHwGet);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweV2Methods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, m_ThaModulePweV2Methods, sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, Pseudowire_Transmit_HSPW_Protection_Control);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwAdd);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, ShouldResetAllPwGroups);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwHwAdd);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwIsInHwGroup);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void OverrideTha60150011ModulePwe(AtModule self)
    {
    Tha60150011ModulePwe pweModule = (Tha60150011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePweOverride, mMethodsGet(pweModule), sizeof(m_Tha60150011ModulePweOverride));

        mMethodOverride(m_Tha60150011ModulePweOverride, JitterAttenuatorIsSupported);
        }

    mMethodsSet(pweModule, &m_Tha60150011ModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    OverrideThaModulePweV2(self);
    OverrideTha60150011ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModulePwe);
    }

AtModule Tha60210031ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModulePweObjectInit(newModule, device);
    }

eAtRet Tha60210031ModulePwePwEthPortQueueSet(ThaModulePwe self, AtPw adapter, uint8 queueId)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, adapter);
    uint32 regVal  = mChannelHwRead(adapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cTxPwEthPortQueue, queueId);
    mChannelHwWrite(adapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

uint8 Tha60210031ModulePwePwEthPortQueueGet(ThaModulePwe self, AtPw adapter)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, adapter);
    uint32 regVal  = mChannelHwRead(adapter, regAddr, cThaModulePwe);

    return (uint8)mRegField(regVal, cTxPwEthPortQueue);
    }

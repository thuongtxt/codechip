/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0031_RD_BERT_MON_H_
#define _AF6_REG_AF6CCI0031_RD_BERT_MON_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Sel Bert ID Mon
Reg Addr   : 0x8510 - 0x851F
Reg Formula: 0x8510 + $mid
    Where  : 
           + $mid(0-15)
Reg Desc   : 
Sel 16 chanel id which enable moniter BERT from 12xOC24 channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_id_reg_mon_Base                                                                    0x8510

/*--------------------------------------
BitField Name: mon_en
BitField Type: RW
BitField Desc: bit enable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_upen_id_reg_mon_mon_en_Mask                                                                cBit13
#define cAf6_upen_id_reg_mon_mon_en_Shift                                                                   13

/*--------------------------------------
BitField Name: bus_sel
BitField Type: RW
BitField Desc: indicate DS3 bus select "3" : PW bus2, PWID[10:0] "2" : PW bus1,
PWID[10:0] "1" : TDM bus2, TDMID[9:0] "0" : TDM bus1, TDMID[9:0]
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_upen_id_reg_mon_bus_sel_Mask                                                            cBit12_11
#define cAf6_upen_id_reg_mon_bus_sel_Shift                                                                  11

/*--------------------------------------
BitField Name: mon_id
BitField Type: RW
BitField Desc: channel id
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_id_reg_mon_mon_id_Mask                                                              cBit10_0
#define cAf6_upen_id_reg_mon_mon_id_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Bert Gen Global Register
Reg Addr   : 0x8400
Reg Formula: 
    Where  : 
Reg Desc   : 
global mon config

------------------------------------------------------------------------------*/
#define cAf6Reg_mglb_pen_Base                                                                           0x8400

/*--------------------------------------
BitField Name: lopthrcfg
BitField Type: RW
BitField Desc: Threshold for lost pattern moniter
BitField Bits: [09:06]
--------------------------------------*/
#define cAf6_mglb_pen_lopthrcfg_Mask                                                                   cBit9_6
#define cAf6_mglb_pen_lopthrcfg_Shift                                                                        6

/*--------------------------------------
BitField Name: synthrcfg
BitField Type: RW
BitField Desc: Threshold for gosyn state of pattern moniter
BitField Bits: [05:02]
--------------------------------------*/
#define cAf6_mglb_pen_synthrcfg_Mask                                                                   cBit5_2
#define cAf6_mglb_pen_synthrcfg_Shift                                                                        2

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: enable swap data
BitField Bits: [01]
--------------------------------------*/
#define cAf6_mglb_pen_swapmode_Mask                                                                      cBit1
#define cAf6_mglb_pen_swapmode_Shift                                                                         1

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: enable invert data
BitField Bits: [00]
--------------------------------------*/
#define cAf6_mglb_pen_invmode_Mask                                                                       cBit0
#define cAf6_mglb_pen_invmode_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Enable
Reg Addr   : 0x8401
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the test pattern sticky mode

------------------------------------------------------------------------------*/
#define cAf6Reg_stkcfg_pen_Base                                                                         0x8401

/*--------------------------------------
BitField Name: stkcfg_reg
BitField Type: RC
BitField Desc:
BitField Bits: [00]
--------------------------------------*/
#define cAf6_stkcfg_pen_stkcfg_reg_Mask                                                                  cBit0
#define cAf6_stkcfg_pen_stkcfg_reg_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Error
Reg Addr   : 0x8402
Reg Formula: 
    Where  : 
Reg Desc   : 
Bert sticky error

------------------------------------------------------------------------------*/
#define cAf6Reg_stk_pen_Base                                                                            0x8402

/*--------------------------------------
BitField Name: stkdi
BitField Type: RW
BitField Desc: indicate error of 16id moniter bert
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_stk_pen_stkdi_Mask                                                                       cBit15_0
#define cAf6_stk_pen_stkdi_Shift                                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter Error
Reg Addr   : 0x8404
Reg Formula: 
    Where  : 
Reg Desc   : 
Latch error counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errlat_pen_Base                                                                         0x8404

/*--------------------------------------
BitField Name: errcnt_lat
BitField Type: RW
BitField Desc: counter err
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_errlat_pen_errcnt_lat_Mask                                                               cBit31_0
#define cAf6_errlat_pen_errcnt_lat_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter Good
Reg Addr   : 0x8405
Reg Formula: 
    Where  : 
Reg Desc   : 
Latch good counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodlat_pen_Base                                                                        0x8405

/*--------------------------------------
BitField Name: goodcnt_lat
BitField Type: RW
BitField Desc: counter good
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_goodlat_pen_goodcnt_lat_Mask                                                             cBit31_0
#define cAf6_goodlat_pen_goodcnt_lat_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Counter Lost
Reg Addr   : 0x8406
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_lostlat_pen_Base                                                                        0x8406

/*--------------------------------------
BitField Name: lostcnt_lat
BitField Type: RW
BitField Desc: counter good
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_lostlat_pen_lostcnt_lat_Mask                                                             cBit31_0
#define cAf6_lostlat_pen_lostcnt_lat_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Mon Control Bert Generate
Reg Addr   : 0x8410 - 0x841F
Reg Formula: 0x8410 + $mctrl_id
    Where  : 
           + $mctrl_id(0-15)
Reg Desc   : 
Used to control bert gen mode of 16 id

------------------------------------------------------------------------------*/
#define cAf6Reg_mon_ctrl_pen_Base                                                                       0x8410

/*--------------------------------------
BitField Name: mswapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [11]
--------------------------------------*/
#define cAf6_mon_ctrl_pen_mswapmode_Mask                                                                cBit11
#define cAf6_mon_ctrl_pen_mswapmode_Shift                                                                   11

/*--------------------------------------
BitField Name: minvmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [10]
--------------------------------------*/
#define cAf6_mon_ctrl_pen_minvmode_Mask                                                                 cBit10
#define cAf6_mon_ctrl_pen_minvmode_Shift                                                                    10

/*--------------------------------------
BitField Name: mpatbit
BitField Type: RW
BitField Desc: number of bit fix patt use
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_mon_ctrl_pen_mpatbit_Mask                                                                 cBit9_5
#define cAf6_mon_ctrl_pen_mpatbit_Shift                                                                      5

/*--------------------------------------
BitField Name: mpatt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x0 : Prbs9 # 0x1 : Prbs11 # 0x2 : Prbs15 # 0x3
: Prbs20r # 0x4 : Prbs20 # 0x5 : qrss20 # 0x6 : Prbs23 # 0x7 : dds1 # 0x8 : dds2
# 0x9 : dds3 # 0xA : dds4 # 0xB : dds5 # 0xC : daly # 0XD : octet55_v2 # 0xE :
octet55_v3 # 0xF : fix3in24 # 0x10: fix1in8 # 0x11: fix2in8 # 0x12: fixpat #
0x13: sequence # 0x14: ds0prbs # 0x15: fixnin32
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_mon_ctrl_pen_mpatt_mode_Mask                                                              cBit4_0
#define cAf6_mon_ctrl_pen_mpatt_mode_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Config expected Fix pattern gen
Reg Addr   : 0x8420 - 0x842F
Reg Formula: 0x8420 + $genid
    Where  : 
           + $genid (0-15)
Reg Desc   : 
config fix pattern gen

------------------------------------------------------------------------------*/
#define cAf6Reg_thrnfix_pen_Base                                                                        0x8420

/*--------------------------------------
BitField Name: crrthrnfix
BitField Type: RW
BitField Desc: config fix pattern gen
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_thrnfix_pen_crrthrnfix_Mask                                                              cBit31_0
#define cAf6_thrnfix_pen_crrthrnfix_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Status of mon bert
Reg Addr   : 0x8440 - 0x844F
Reg Formula: 0x8440 + $msttid
    Where  : 
           + $msttid (0-15)
Reg Desc   : 
Indicate current status of bert mon

------------------------------------------------------------------------------*/
#define cAf6Reg_mon_status_pen_Base                                                                     0x8440

/*--------------------------------------
BitField Name: mon_crr_stt
BitField Type: R_O
BitField Desc: Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA =
2'd2; INFSTA = 2'd3;
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_mon_status_pen_mon_crr_stt_Mask                                                           cBit1_0
#define cAf6_mon_status_pen_mon_crr_stt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : counter error
Reg Addr   : 0x8460 - 0x846F
Reg Formula: 0x8460 + $errid
    Where  : 
           + $errid(0-15)
Reg Desc   : 
Moniter error bert counter of 16 chanel id

------------------------------------------------------------------------------*/
#define cAf6Reg_merrcnt_pen_Base                                                                        0x8460

/*--------------------------------------
BitField Name: errcnt_pdo
BitField Type: R2C
BitField Desc: value of err counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_merrcnt_pen_errcnt_pdo_Mask                                                              cBit31_0
#define cAf6_merrcnt_pen_errcnt_pdo_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Moniter Good Bit
Reg Addr   : 0x8480 - 0x848F
Reg Formula: 0x8480 + $mgb_id
    Where  : 
           + $mgb_id(0-15)
Reg Desc   : 
Moniter good bert counter of 16 chanel id

------------------------------------------------------------------------------*/
#define cAf6Reg_mgoodbit_pen_Base                                                                       0x8480

/*--------------------------------------
BitField Name: goodbit_pdo
BitField Type: RW
BitField Desc: value of good cnt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mgoodbit_pen_goodbit_pdo_Mask                                                            cBit31_0
#define cAf6_mgoodbit_pen_goodbit_pdo_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Moniter Lost Bit
Reg Addr   : 0x84A0 - 0x84AF
Reg Formula: 0x84A0 + $mlos_id
    Where  : 
           + $mlos_id(0-15)
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_mlostbit_pen_Base                                                                       0x84A0

/*--------------------------------------
BitField Name: lostbit_pdo
BitField Type: RW
BitField Desc: Value of lost cnt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mlostbit_pen_lostbit_pdo_Mask                                                            cBit31_0
#define cAf6_mlostbit_pen_lostbit_pdo_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Config nxDS0 moniter
Reg Addr   : 0x84C0 - 0x84CF
Reg Formula: 0x84C0 + $mid
    Where  : 
           + $mid(0-15)
Reg Desc   : 
enable moniter 32 timeslot E1 or 24timeslot T1

------------------------------------------------------------------------------*/
#define cAf6Reg_mtsen_pen_Base                                                                          0x84C0

/*--------------------------------------
BitField Name: mtsen_pdo
BitField Type: RW
BitField Desc: set "1" to en moniter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mtsen_pen_mtsen_pdo_Mask                                                                 cBit31_0
#define cAf6_mtsen_pen_mtsen_pdo_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : moniter counter load id
Reg Addr   : 0x8403
Reg Formula: 
    Where  : 
Reg Desc   : 
used to load the error/good/loss counter report of bert id which the channel ID is value of register.

------------------------------------------------------------------------------*/
#define cAf6Reg_cntld_strb_Base                                                                         0x8403

/*--------------------------------------
BitField Name: idload
BitField Type: RW
BitField Desc:
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_cntld_strb_idload_Mask                                                                    cBit3_0
#define cAf6_cntld_strb_idload_Shift                                                                         0

#endif /* _AF6_REG_AF6CCI0031_RD_BERT_MON_H_ */

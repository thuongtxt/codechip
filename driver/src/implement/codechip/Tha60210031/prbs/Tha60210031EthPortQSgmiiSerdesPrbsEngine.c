/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210031EthPortQSgmiiSerdesPrbsEngine.c
 *
 * Created Date: Jul 12, 2015
 *
 * Description : PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../Tha60210011/eth/Tha6021ModuleEthReg.h"
#include "Tha60210031EthPortQSgmiiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* PRSS mode */
#define cDiagPrbsModeMask        cBit13_12
#define cDiagPrbsModeShift       12
#define cDiagPrbsFixPatternMask  cBit27_20
#define cDiagPrbsFixPatternShift 20

/* PRBS control */
#define cPrbsEnableMask      cBit0
#define cPrbsEnableShift     0
#define cPrbsErrorForceMask  cBit1
#define cPrbsErrorForceShift 1
#define cPrbsIpModeMask      cBit4
#define cPrbsIpModeShift     4
#define cPrbsIpModeXilinx    0
#define cPrbsIpModeAltera    1

/* Status */
#define cLaneLinkUpMask cBit0
#define cLaneLinkPrbsFailMask cBit4
#define cLaneLinkPrbsLengthErrorMask cBit8
#define cLaneLinkPrbsPacketErrorMask cBit12
#define cLaneLinkPrbsPacketSyncMask  cBit16
#define cLaneLinkPrbsPacketAllErrorBitsMask  (cLaneLinkPrbsFailMask        | \
                                              cLaneLinkPrbsLengthErrorMask | \
                                              cLaneLinkPrbsPacketErrorMask)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031EthPortQSgmiiSerdesPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(AtPrbsEngine self)
    {
    AtChannel port = AtPrbsEngineChannelGet(self);
    return (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet(port), cAtModuleEth);
    }

static uint8 SerdesId(AtPrbsEngine self)
    {
    return (uint8)AtPrbsEngineIdGet(self);
    }

static uint32 LaneRegisterWithOffset(AtPrbsEngine self, uint16 laneId, uint32 offset)
    {
    return ThaModuleEthQsgmiiSerdesLaneRegisterWithOffset(EthModule(self), SerdesId(self), laneId, offset);
    }

static uint32 LanePrbsModeRegister(AtPrbsEngine self, uint16 laneId)
    {
    return LaneRegisterWithOffset(self, laneId, 0x1);
    }

static uint32 LaneMacRegister(AtPrbsEngine self, uint16 laneId)
    {
    return LaneRegisterWithOffset(self, laneId, 0x2);
    }

static uint32 PrbsControlRegister(AtPrbsEngine self, uint16 laneId)
    {
    return LaneRegisterWithOffset(self, laneId, 0x3);
    }

static uint32 LaneCounterRegister(AtPrbsEngine self, uint16 laneId, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterTxFrame)
        return LaneRegisterWithOffset(self, laneId, 0x8);
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return LaneRegisterWithOffset(self, laneId, 0x9);

    return cBit31_0;
    }

static uint32 LaneCurrentStatusRegister(AtPrbsEngine self, uint16 laneId)
    {
    return ThaModuleEthQsgmiiLaneCurrentStatusRegister(EthModule(self), SerdesId(self), laneId);
    }

static uint32 LaneStickyStatusRegister(AtPrbsEngine self, uint16 laneId)
    {
    return ThaModuleEthQsgmiiLaneStickyStatusRegister(EthModule(self), SerdesId(self), laneId);
    }

static eBool SerdesApsSupported(AtPrbsEngine self)
    {
    return AtModuleEthSerdesApsSupported((AtModuleEth)EthModule(self));
    }

static eBool CanUseAllLanes(AtPrbsEngine self)
    {
    return SerdesApsSupported(self) ? cAtFalse : cAtTrue;
    }

static uint16 ControlledLane(AtPrbsEngine self)
    {
    /* Default use lane 0 to diagnostic if hardware does not support full diagnostic */
    AtUnused(self);
    return 0;
    }

static uint16 NumLanes(AtPrbsEngine self)
    {
    if (CanUseAllLanes(self))
        return ThaModuleEthQsgmiiSerdesNumLanes(EthModule(self), SerdesId(self));
    return 1;
    }

static eBool LaneIsSelected(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = cEthPortQSGMIIXFIControlReg;
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    if (mRegField(regVal, cEthPortQSGMIISerDesIdSelect) != SerdesId(self))
        return cAtFalse;

    if (mRegField(regVal, cEthPortSGMIILaneSelect) != laneId)
        return cAtFalse;

    return cAtTrue;
    }

static eBool DiagnosticIsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = cEthPortQSGMIIXFIControlReg;
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return (regVal & cEthPortQSGMIIDiagEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet LaneSelect(AtPrbsEngine self, uint16 laneId, eBool enable)
    {
    uint32 regAddr = cEthPortQSGMIIXFIControlReg;
    uint32 regVal;

    if (CanUseAllLanes(self))
        return cAtOk;

    /* In case of disabling, if this SERDES is not being selected, just do nothing */
    if (!enable && !LaneIsSelected(self, laneId))
        return cAtOk;

    regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cEthPortQSGMIISerDesIdSelect, SerdesId(self));
    mRegFieldSet(regVal, cEthPortQSGMIIDiagEnable, enable ? 1 : 0);
    mRegFieldSet(regVal, cEthPortSGMIILaneSelect, laneId);
    if (enable)
        mRegFieldSet(regVal, cEthPortXfiDiagEnable, 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LanePrbsEnable(AtPrbsEngine self, uint16 laneId, eBool enable)
    {
    uint32 regAddr, regVal = PrbsControlRegister(self, laneId);

    /* In case of disabling, if this SERDES is not being selected, just do nothing */
    if (!CanUseAllLanes(self) && !enable && !LaneIsSelected(self, laneId))
        return cAtOk;

    regAddr = PrbsControlRegister(self, laneId);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cPrbsEnableMask,
              cPrbsEnableShift,
              enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    return LaneSelect(self, laneId, enable);
    }

static eBool LanePrbsIsEnabled(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr, regVal;

    if (!CanUseAllLanes(self))
        {
        if (!DiagnosticIsEnabled(self))
            return cAtFalse;
        if (!LaneIsSelected(self, laneId))
            return cAtFalse;
        }

    regAddr = PrbsControlRegister(self, laneId);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    return (regVal & cPrbsEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet LanePrbsErrorForce(AtPrbsEngine self, uint16 laneId, eBool force)
    {
	uint32 regAddr, regVal;

    /* In case of disabling, if this SERDES is not being selected, just do nothing */
    if (!CanUseAllLanes(self) && !force && !LaneIsSelected(self, laneId))
        return cAtOk;

    regAddr = PrbsControlRegister(self, laneId);
	regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cPrbsErrorForceMask,
              cPrbsErrorForceShift,
              force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LanePrbsErrorIsForced(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr, regVal;

    if (!CanUseAllLanes(self))
        {
        if (!DiagnosticIsEnabled(self))
            return cAtFalse;
        if (!LaneIsSelected(self, laneId))
            return cAtFalse;
        }

    regAddr = PrbsControlRegister(self, laneId);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return (regVal & cPrbsErrorForceMask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtModulePrbsRet ret = cAtOk;
    uint16 numLanes = NumLanes(self);
    uint8 lane_i;

    if (!CanUseAllLanes(self))
        return LanePrbsEnable(self, ControlledLane(self), enable);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        ret |= LanePrbsEnable(self, lane_i, enable);

    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return LanePrbsIsEnabled(self, ControlledLane(self));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool enable)
    {
    eAtModulePrbsRet ret = cAtOk;
    uint16 numLanes = NumLanes(self);
    uint8 lane_i;

    if (!CanUseAllLanes(self))
        return LanePrbsErrorForce(self, ControlledLane(self), enable);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        ret |= LanePrbsErrorForce(self, lane_i, enable);

    return ret;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    return LanePrbsErrorIsForced(self, ControlledLane(self));
    }

static uint32 LossSyncMask(AtPrbsEngine self)
    {
    if (SerdesApsSupported(self))
        return cLaneLinkPrbsFailMask;
    return cLaneLinkPrbsPacketSyncMask;
    }

static uint32 AlarmsHw2Sw(AtPrbsEngine self, uint32 hwValue)
    {
    uint32 alarms = 0;

    if (hwValue & (cLaneLinkPrbsLengthErrorMask | cLaneLinkPrbsPacketErrorMask))
        alarms |= cAtPrbsEngineAlarmTypeError;

    if (hwValue & LossSyncMask(self))
        alarms |= cAtPrbsEngineAlarmTypeLossSync;

    return alarms;
    }

static uint32 LaneAlarmGet(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = LaneCurrentStatusRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return AlarmsHw2Sw(self, regVal);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 alarm = 0;
    uint16 numLanes = NumLanes(self);
    uint8 lane_i;

    if (!CanUseAllLanes(self))
        return LaneAlarmGet(self, ControlledLane(self));

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        alarm |= LaneAlarmGet(self, lane_i);

    return alarm;
    }

static uint32 LaneAlarmHistoryRead2Clear(AtPrbsEngine self, uint16 laneId, eBool clear)
    {
    uint32 regAddr = LaneStickyStatusRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 alarms = AlarmsHw2Sw(self, regVal);

    if (clear)
        {
        uint32 clearAlarms = cLaneLinkPrbsPacketAllErrorBitsMask | LossSyncMask(self);
        AtPrbsEngineWrite(self, regAddr, clearAlarms, cAtModuleEth);
        }

    return alarms;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool clear)
    {
    uint32 alarm = 0;
    uint16 numLanes = NumLanes(self);
    uint8 lane_i;

    if (!CanUseAllLanes(self))
        return LaneAlarmHistoryRead2Clear(self, ControlledLane(self), clear);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        alarm |= LaneAlarmHistoryRead2Clear(self, lane_i, clear);

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 mode = prbsMode;

    AtUnused(self);

    switch (mode)
        {
        case cAtPrbsModePrbs15 : return cAtTrue;
        case cAtPrbsModePrbs23 : return cAtTrue;
        case cAtPrbsModePrbsSeq: return cAtTrue;
        case cAtPrbsModePrbsFixedPattern1Byte: return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint8 PrbsModeSw2Hw(eAtPrbsMode prbsMode)
    {
    uint32 value = prbsMode;
    switch (value)
        {
        case cAtPrbsModePrbs15 : return 0;
        case cAtPrbsModePrbs23 : return 1;
        case cAtPrbsModePrbsSeq: return 2;
        case cAtPrbsModePrbsFixedPattern1Byte: return 3;
        default:
            return cAtFalse;
        }
    }

static eAtPrbsMode PrbsModeHw2Sw(uint32 hwPrbsMode)
    {
    switch (hwPrbsMode)
        {
        case 0: return cAtPrbsModePrbs15;
        case 1: return cAtPrbsModePrbs23;
        case 2: return cAtPrbsModePrbsSeq;
        case 3: return cAtPrbsModePrbsFixedPattern1Byte;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet LanePrbsModeSet(AtPrbsEngine self, uint16 laneId, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = LanePrbsModeRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cDiagPrbsMode, PrbsModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtPrbsMode LanePrbsModeGet(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = LanePrbsModeRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return PrbsModeHw2Sw(mRegField(regVal, cDiagPrbsMode));
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint16 lane_i, numLanes = NumLanes(self);
    eAtRet ret = cAtOk;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    if (!CanUseAllLanes(self))
        return LanePrbsModeSet(self, ControlledLane(self), prbsMode);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        ret |= LanePrbsModeSet(self, lane_i, prbsMode);

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return LanePrbsModeGet(self, ControlledLane(self));
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame: return cAtTrue;
        case cAtPrbsEngineCounterRxFrame: return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 LaneCounterReadToClear(AtPrbsEngine self, uint16 laneId, uint16 counterType, eBool r2c)
    {
    uint32 regAddr = LaneCounterRegister(self, laneId, counterType);

    if (r2c && SerdesApsSupported(self))
        regAddr = regAddr + 0x4;

    return AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    }

static uint32 CounterReadToClear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 numLanes = NumLanes(self);
    uint16 lane_i;
    uint32 counter = 0;

    if (!CounterIsSupported(self, counterType))
        return 0;

    if (!CanUseAllLanes(self))
        return LaneCounterReadToClear(self, ControlledLane(self), counterType, r2c);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        counter = counter + LaneCounterReadToClear(self, lane_i, counterType, r2c);

    return counter;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtTrue);
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static void LaneMacInit(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr  = LaneMacRegister(self, laneId);
    uint32 regVal   = (AtPrbsEngineIdGet(self) << 16) | laneId;
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    }

static void MacInit(AtPrbsEngine self)
    {
    uint16 lane_i, numLanes = NumLanes(self);

    if (!CanUseAllLanes(self))
        {
        LaneMacInit(self, ControlledLane(self));
        return;
        }

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        LaneMacInit(self, lane_i);
    }

static uint8 DefaultIpMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cPrbsIpModeXilinx;
    }

static void LaneIpModeInit(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = PrbsControlRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cPrbsIpMode, DefaultIpMode(self));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    }

static void IpModeInit(AtPrbsEngine self)
    {
    uint16 lane_i, numLanes = NumLanes(self);

    if (!CanUseAllLanes(self))
        {
        LaneIpModeInit(self, ControlledLane(self));
        return;
        }

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        LaneIpModeInit(self, lane_i);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);

    ret |= AtPrbsEngineErrorForce(self, cAtFalse);
    ret |= AtPrbsEngineEnable(self, cAtFalse);

    MacInit(self);
    IpModeInit(self);

    return ret;
    }

static void LaneGoodCounterDisplay(const char *name, uint32 counter)
    {
    AtPrintc(cSevNormal, "  - %s: ", name);
    AtPrintc(counter ? cSevInfo : cSevCritical, "%u\r\n", counter);
    }

static void LaneCountersShow(AtPrbsEngine self, uint16 laneId)
    {
    LaneGoodCounterDisplay("TX packets", LaneCounterReadToClear(self, laneId, cAtPrbsEngineCounterTxFrame, cAtTrue));
    LaneGoodCounterDisplay("RX packets", LaneCounterReadToClear(self, laneId, cAtPrbsEngineCounterRxFrame, cAtTrue));
    }

static eBool LaneHasNoAlarms(uint32 hwAlarms)
    {
    if (hwAlarms & cLaneLinkPrbsPacketAllErrorBitsMask)
        return cAtFalse;
    return cAtTrue;
    }

static void HelperLaneStatusShow(AtPrbsEngine self, uint32 hwAlarms, const char *type)
    {
    AtPrintc(cSevNormal, "%s: ", type);

    if (LaneHasNoAlarms(hwAlarms))
        {
        AtPrintc(cSevInfo, "%s\r\n", "none");
        return;
        }

    if (hwAlarms & cLaneLinkPrbsFailMask)
        AtPrintc(cSevCritical, "PrbsFail ");
    if (hwAlarms & cLaneLinkPrbsLengthErrorMask)
        AtPrintc(cSevCritical, "PrbsLengthError ");
    if (hwAlarms & cLaneLinkPrbsPacketErrorMask)
        AtPrintc(cSevCritical, "PrbsPacketError ");

    if (!SerdesApsSupported(self))
        {
        if ((hwAlarms & cLaneLinkPrbsPacketSyncMask) == 0)
            AtPrintc(cSevCritical, "NotSync ");
        }

    AtPrintc(cSevNormal, "\r\n");
    }

static void LaneStickyShow(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = LaneStickyStatusRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    HelperLaneStatusShow(self, regVal, "  - Sticky");
    AtPrbsEngineWrite(self, regAddr, cLaneLinkPrbsPacketAllErrorBitsMask | LossSyncMask(self), cAtModuleEth);
    }

static void LaneStatusShow(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = LaneCurrentStatusRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    HelperLaneStatusShow(self, regVal, "  - Status");
    }

static void LaneDebug(AtPrbsEngine self, uint16 laneId)
    {
    LaneCountersShow(self, laneId);
    LaneStatusShow(self, laneId);
    LaneStickyShow(self, laneId);
    }

static void AllLanesDebug(AtPrbsEngine self)
    {
    uint16 numLanes = NumLanes(self);
    uint16 lane_i;

    m_AtPrbsEngineMethods->Debug(self);

    if (!CanUseAllLanes(self))
        return;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtPrintc(cSevInfo, "* Lane %u:\r\n", lane_i + 1);
        LaneDebug(self, lane_i);
        }
    }

static void Debug(AtPrbsEngine self)
    {
    m_AtPrbsEngineMethods->Debug(self);

    if (CanUseAllLanes(self))
        AllLanesDebug(self);
    else
        LaneDebug(self, ControlledLane(self));
    }

static eAtModulePrbsRet LaneFixedPatternSet(AtPrbsEngine self, uint16 laneId, uint32 fixedPattern)
    {
    uint32 regAddr = LanePrbsModeRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cDiagPrbsFixPattern, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 LaneFixedPatternGet(AtPrbsEngine self, uint16 laneId)
    {
    uint32 regAddr = LanePrbsModeRegister(self, laneId);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return (uint8)mRegField(regVal, cDiagPrbsFixPattern);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint16 lane_i, numLanes = NumLanes(self);
    eAtRet ret = cAtOk;

    if (fixedPattern > cBit7_0)
        return cAtErrorOutOfRangParm;

    if (!CanUseAllLanes(self))
        return LaneFixedPatternSet(self, ControlledLane(self), fixedPattern);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        ret |= LaneFixedPatternSet(self, lane_i, fixedPattern);

    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return LaneFixedPatternGet(self, ControlledLane(self));
    }

static const char *TypeString(AtPrbsEngine self)
    {
    AtUnused(self);
    return "QSGMII";
    }

static void OverrideAtPrbsEngineSerdes(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TypeString);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngineSerdes(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031EthPortQSgmiiSerdesPrbsEngine);
    }

AtPrbsEngine Tha60210031EthPortQSgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self,
                               AtSerdesControllerPhysicalPortGet(serdesController),
                               AtSerdesControllerIdGet(serdesController)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210031EthPortQSgmiiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031EthPortQSgmiiSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

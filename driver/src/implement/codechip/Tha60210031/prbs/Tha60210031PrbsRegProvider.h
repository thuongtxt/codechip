/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210031PrbsRegProvider.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PRBSREGPROVIDER_H_
#define _THA60210031PRBSREGPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PrbsRegProvider
    {
    tThaPrbsRegProvider super;
    }tTha60210031PrbsRegProvider;

typedef struct tTha60210031PrbsRegProviderV2
    {
    tTha60210031PrbsRegProvider super;
    }tTha60210031PrbsRegProviderV2;

typedef struct tTha60210031PrbsRegProviderV3
    {
    tTha60210031PrbsRegProviderV2 super;
    }tTha60210031PrbsRegProviderV3;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPrbsRegProvider Tha60210031PrbsRegProviderObjectInit(ThaPrbsRegProvider self);
ThaPrbsRegProvider Tha60210031PrbsRegProviderV2ObjectInit(ThaPrbsRegProvider self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PRBSREGPROVIDER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6021DsxXfiSerdesPrbsEngine.c
 *
 * Created Date: Sep 6, 2015
 *
 * Description : XFI SERDES PRBS for DSx product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../Tha60210011/prbs/Tha6021XfiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6021DsxXfiSerdesPrbsEngine
    {
    tTha6021XfiSerdesPrbsEngine super;
    }tTha6021DsxXfiSerdesPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6021XfiSerdesPrbsEngineMethods m_Tha6021EthPortSerdesPrbsEngineOverride;

/* Save super implementation */
static const tTha6021XfiSerdesPrbsEngineMethods *m_Tha6021EthPortSerdesPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static uint32 PrbsTxCountersR2cRegister(AtPrbsEngine self, eBool r2c)
    {
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, r2c ? 0x6C : 0x68);
    }

static uint32 PrbsRxCountersR2cRegister(AtPrbsEngine self, eBool r2c)
    {
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, r2c ? 0x6D : 0x69);
    }

static uint32 HwCounterReadToClear(Tha6021XfiSerdesPrbsEngine self, uint16 counterType, eBool r2c)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (!AtModuleEthSerdesApsSupported(EthModule(engine)))
        return m_Tha6021EthPortSerdesPrbsEngineMethods->HwCounterReadToClear(self, counterType, r2c);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return AtPrbsEngineRead(engine, PrbsTxCountersR2cRegister(engine, r2c), cAtModulePrbs);

    if (counterType == cAtPrbsEngineCounterRxFrame)
        return AtPrbsEngineRead(engine, PrbsRxCountersR2cRegister(engine, r2c), cAtModulePrbs);

    return 0;
    }

static void OverrideTha6021EthPortSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha6021XfiSerdesPrbsEngine engine = (Tha6021XfiSerdesPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6021EthPortSerdesPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6021EthPortSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha6021EthPortSerdesPrbsEngineOverride, HwCounterReadToClear);
        }

    mMethodsSet(engine, &m_Tha6021EthPortSerdesPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6021EthPortSerdesPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021DsxXfiSerdesPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021XfiSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6021DsxXfiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

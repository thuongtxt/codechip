/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210031PrbsRegProvider.c
 *
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031PrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegMapBERTGenTxBerErrMask   cBit31_0
#define cThaRegMapBERTGenTxBerErrShift  0
#define cThaRegMapBERTGenTxBerMdMask    cBit31_0
#define cThaRegMapBERTGenTxBerMdShift   0

/* PRBS */
#define cPwPdaPwPrbsGenCtrl             0x007000
#define cPwPdaPwPrbsMonCtrl             0x007800

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegMapBERTGenTxBerMdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdMask;
    }

static uint8  RegMapBERTGenTxBerMdShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdShift;
    }

static uint32 RegMapBERTGenTxBerErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrMask;
    }

static uint8  RegMapBERTGenTxBerErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrShift;
    }

static uint32 RegMapBERTGenTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit10;
    }

static uint32 RegMapBERTGenTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit9_0;
    }

static uint32 RegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit11;
    }

static uint32 RegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit10_0;
    }

static uint32 RegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit10;
    }

static uint32 RegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit9_0;
    }

static uint32 RegPdaPwPrbsGenCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cPwPdaPwPrbsGenCtrl;
    }

static uint32 RegPdaPwPrbsMonCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cPwPdaPwPrbsMonCtrl;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegPdaPwPrbsGenCtrl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegPdaPwPrbsMonCtrl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerMdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerMdShift);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerErrMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerErrShift);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PrbsRegProvider);
    }

ThaPrbsRegProvider Tha60210031PrbsRegProviderObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsRegProviderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60210031PrbsRegProvider(void)
    {
    static tTha60210031PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = Tha60210031PrbsRegProviderObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

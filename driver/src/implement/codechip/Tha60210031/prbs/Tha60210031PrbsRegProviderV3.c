/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210031PrbsRegProviderV3.c
 *
 * Created Date: Jan 8, 2018
 *
 * Description : PRBS register providers Version 2 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031PrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

static const tThaPrbsRegProviderMethods *m_ThaPrbsRegProviderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit13;
    }

static uint32 RegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit10_0;
    }

static uint32 RegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit13;
    }

static uint32 RegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit10_0;
    }

static uint32 RegMapBERTMonTdmMonAndPsnMonSelectMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit12_11;
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonSelectedChannel(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonNxDs0ConcateControl(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonGlobalControl(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonStickyEnable(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonSticky(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonMode(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonFixedPatternControl(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonStatus(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonErrorCounter(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonGoodBitCounter(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonLossBitCounter(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonCounterLoadId(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonErrorCounterLoading(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonGoodBitCounterLoading(self, prbsModule, engineId, slice);
    }

static uint32 RegMapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonLossBitCounterLoading(self, prbsModule, engineId, slice);
    }

static uint32 MonTdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 2048;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsRegProviderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, m_ThaPrbsRegProviderMethods, sizeof(m_ThaPrbsRegProviderOverride));

        /* MON */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTdmMonAndPsnMonSelectMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGlobalControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStickyEnable);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSticky);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonCounterLoadId);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, MonTdmChannelIdSliceFactor);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PrbsRegProviderV3);
    }

static ThaPrbsRegProvider Tha60210031PrbsRegProviderV3ObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PrbsRegProviderV2ObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60210031PrbsRegProviderV3(void)
    {
    static tTha60210031PrbsRegProviderV3 shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = Tha60210031PrbsRegProviderV3ObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

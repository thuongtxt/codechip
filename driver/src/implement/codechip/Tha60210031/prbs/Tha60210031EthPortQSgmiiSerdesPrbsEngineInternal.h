/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210031EthPortQSgmiiSerdesPrbsEngineInternal.h
 * 
 * Created Date: Jul 12, 2015
 *
 * Description : PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHPORTQSGMIISERDESPRBSENGINEINTERNAL_H_
#define _THA60210031ETHPORTQSGMIISERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031EthPortQSgmiiSerdesPrbsEngine * Tha60210031EthPortQSgmiiSerdesPrbsEngine;

typedef struct tTha60210031EthPortQSgmiiSerdesPrbsEngine
    {
    tAtPrbsEngine super;
    }tTha60210031EthPortQSgmiiSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60210031EthPortQSgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHPORTQSGMIISERDESPRBSENGINEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210031ModulePrbs.c
 *
 * Created Date: May 29, 2015
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtPdhNxDs0.h"
#include "../../../default/prbs/ThaPrbsEngine.h"
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/prbs/ThaPrbsRegProvider.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/prbs/Tha60210011ModulePrbsInternal.h"
#include "Tha60210031ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePrbsMethods m_ThaModulePrbsOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tAtModuleMethods      m_AtModuleOverride;

/* Save super implementation */
static const tThaModulePrbsMethods *m_ThaModulePrbsMethods = NULL;
static const tAtModulePrbsMethods  *m_AtModulePrbsMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    return Tha60210021NxDs0PrbsEngineNew(nxDs0, engineId);
    }

static ThaVersionReader VersionReader(ThaModulePrbs self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportPwPrbsEngine(ThaModulePrbs self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x13, 0x00);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPw pw)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device) || (AtDeviceVersionNumber(device) >= StartVersionSupportPwPrbsEngine(self)))
        return Tha60210021PwPrbsEngineNew(pw, engineId);
        
    return NULL;
    }

static uint32 ForceEnableValue(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    return ThaPrbsEngineErrorRateSw2Hw(engine, cAtBerRate1E3);
    }

static uint8 SliceOfPw(AtPw pw)
    {
    AtSdhChannel sdhChannel = NULL;
    AtPdhChannel pdhChannel = NULL;
    uint8 slice = 0;
    uint8 sts = 0;

    if (pw == NULL)
        return 0;

    switch (AtPwTypeGet(pw))
        {
        case cAtPwTypeSAToP:
            pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
            break;

        case cAtPwTypeCESoP:
            pdhChannel = (AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)AtPwBoundCircuitGet(pw));
            break;

        case cAtPwTypeCEP:
            sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
            break;

        case cAtPwTypeInvalid:
        case cAtPwTypeATM:
        case cAtPwTypeHdlc:
        case cAtPwTypePpp:
        case cAtPwTypeFr:
        case cAtPwTypeToh:
        case cAtPwTypeMlppp:
        case cAtPwTypeKbyte:
        default:
            return 0;
        }

    if (sdhChannel)
        ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleSdh, &slice, &sts);

    else if (pdhChannel)
        ThaPdhChannelHwIdGet(pdhChannel, cAtModulePdh, &slice, &sts);

    else
        return 0;

    return slice;
    }

static uint32 PdaBaseAddress(void)
    {
    return 0x240000;
    }

static uint32 PrbsPwPdaDefaultOffset(ThaModulePrbs self, AtPrbsEngine engine)
    {
    AtChannel channel = (AtChannel)AtPrbsEngineChannelGet(engine);
    AtUnused(self);
    return (32768UL * SliceOfPw((AtPw)channel)) + AtChannelIdGet(channel) + PdaBaseAddress();
    }

static AtPrbsEngine De3LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe3 de3)
    {
    AtUnused(self);
    return Tha6021PdhLinePrbsEngineNew((AtChannel)de3, AtChannelIdGet((AtChannel)de3));
    }

static AtPrbsEngine SdhLinePrbsEngineCreate(AtModulePrbs self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha6021SerialLinePrbsEngineNew((AtChannel)line, AtChannelIdGet((AtChannel)line));
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    if (AtSdhChannelTypeGet(sdhVc) == cAtSdhChannelTypeVc4)
        return NULL;

    return m_AtModulePrbsMethods->SdhVcPrbsEngineCreate(self, engineId, sdhVc);
    }

static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "Total 16 engines: vc3, vc1x, de1, nxds0";
    }

static eBool IsPrbsV2Supported(ThaModulePrbs self)
    {
    uint32 hwVersion;
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    hwVersion = ThaVersionReaderHardwareVersion(ThaDeviceVersionReader(device));
    if (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x04, 0x03, 0))
        return cAtFalse;

    return cAtTrue;
    }

static eBool IsPrbsV3Supported(ThaModulePrbs self)
    {
    uint32 hwVersion;
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    if (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x05, 0x02, 0x5207))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PrbsEngineIsCommonMonSide(ThaModulePrbs self)
    {
    return IsPrbsV3Supported(self);
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtDevice device;

    if (self->regProvider)
        return self->regProvider;

    /* Cannot determine version, just temporary use default provider */
    device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self)) == NULL)
        return Tha60210031PrbsRegProvider();

    /* Determine corresponding provider and cache it */
    if (ThaModulePrbsEngineIsCommonMonSide(self))
        self->regProvider = Tha60210031PrbsRegProviderV3();
    else if (IsPrbsV2Supported(self))
        self->regProvider = Tha60210031PrbsRegProviderV2();
    else
        self->regProvider = Tha60210031PrbsRegProvider();

    return self->regProvider;
    }

static uint32 PrbsEngineToHwBusSelectValue(ThaPrbsEngine engine, eAtPrbsSide side)
    {
    uint32 hwVal = 0;
    uint8 slice = ThaPrbsEngineHwSliceId(engine);
    if (side == cAtPrbsSidePsn)
        hwVal = slice + 2UL;
    else
        hwVal = slice;

    return hwVal;
    }

static eAtPrbsSide HwBusSelectValueToEngineSide(uint32 hwBusValue)
    {
    if (hwBusValue < 2)
        return cAtPrbsSideTdm;
    else
        return cAtPrbsSidePsn;
    }

static eAtRet PrbsEngineMonSideSet(ThaModulePrbs self, ThaPrbsEngine engine, eAtPrbsSide side)
    {
    uint32 mask, shift;
    uint32 regAddr = ThaPrbsEngineBertMonSelectedChannelRegAddress(engine);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 hwVal = PrbsEngineToHwBusSelectValue(engine, side);

    mask  = ThaPrbsEngineBertMonSelectMask(engine);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, hwVal);

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsSide PrbsEngineMonSideGet(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    uint32 mask, shift;
    uint32 regAddr = ThaPrbsEngineBertMonSelectedChannelRegAddress(engine);
    uint32 regVal = mModuleHwRead(self, regAddr);
    eAtPrbsSide side = cAtPrbsSideUnknown;
    uint32 hwVal = 0;

    mask  = ThaPrbsEngineBertMonSelectMask(engine);
    shift = AtRegMaskToShift(mask);
    mFieldGet(regVal, mask, shift, uint32, &hwVal);

    side = HwBusSelectValueToEngineSide(hwVal);
    return side;
    }

static eBool De3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode)
    {
    return Tha6021ModulePrbsDe3InvertModeShouldSwap(self, prbsMode);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De3LinePrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhLinePrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, m_ThaModulePrbsMethods, sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, ForceEnableValue);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsPwPdaDefaultOffset);
        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, De3InvertModeShouldSwap);
        m_ThaModulePrbsOverride.ErrorRateSw2Hw = Tha6021ErrorRateSw2Hw;
        m_ThaModulePrbsOverride.ErrorRateHw2Sw = Tha6021ErrorRateHw2Sw;
        mMethodOverride(m_ThaModulePrbsOverride, PrbsEngineIsCommonMonSide);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsEngineMonSideSet);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsEngineMonSideGet);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideThaModulePrbs((ThaModulePrbs)self);
    OverrideAtModulePrbs(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModulePrbs);
    }

AtModulePrbs Tha60210031ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60210031ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModulePrbsObjectInit(newModule, device);
    }

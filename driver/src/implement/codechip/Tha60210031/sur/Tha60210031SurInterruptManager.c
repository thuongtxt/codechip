/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : Tha60210031SurInterruptController.c
 *
 * Created Date: Sep 15, 2015
 *
 * Description : Module Surveillance interrupt controller.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../man/Tha60210031DeviceReg.h"
#include "Tha60210031SurInterruptManagerInternal.h"
#include "Tha60210031ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods     m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods    m_ThaInterruptManagerOverride;
static tThaSurInterruptManagerMethods m_ThaSurInterruptManagerOverride;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_inten_status_FMIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool LineHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_MSK_ECSTM_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 InterruptMask(ThaSurInterruptManager self)
    {
    AtUnused(self);
    return (cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_ECSTM_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_VTGTUG_Mask |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_STSAU_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS3E3_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Mask);
    }

static ThaModuleHardSur SurModule(ThaSurInterruptManager self)
    {
    return (ThaModuleHardSur)AtInterruptManagerModuleGet((AtInterruptManager)self);
    }

static ThaModuleSdh SdhModule(ThaSurInterruptManager self)
    {
    return (ThaModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)SurModule(self)), cAtModuleSdh);
    }

static void LineInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_EC1_Interrupt_OR);
    ThaModuleSdh sdhModule = SdhModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint8 slice, line;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            /* Get STS24 OR interrupt status */
            uint32 intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1_Base + (uint32)slice);

            for (line = 0; line < 24; line++)
                {
                if (intrSts24Val & cIteratorMask(line))
                    {
                    AtSdhLine sdhLine;
                    uint32 offset = ThaModuleHardSurLineOffset(SurModule(self), slice, line);

                    /* Get line from hardware ID */
                    sdhLine = ThaModuleSdhLineFromHwIdGet(sdhModule, cAtModuleSur, slice, line);

                    /* Exception */
                    if (sdhLine == NULL)
                        mThrowNullEngine(self, Line, offset)

                    /* Execute path interrupt process */
                    AtSurEngineNotificationProcess(AtChannelSurEngineGet((AtChannel)sdhLine), offset);
                    }
                }
            }
        }
    }

static void OverrideThaSurInterruptManager(AtInterruptManager self)
    {
    ThaSurInterruptManager manager = (ThaSurInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSurInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaSurInterruptManagerOverride));

        mMethodOverride(m_ThaSurInterruptManagerOverride, LineHasInterrupt);
        mMethodOverride(m_ThaSurInterruptManagerOverride, InterruptMask);
        mMethodOverride(m_ThaSurInterruptManagerOverride, LineInterruptProcess);
        }

    mMethodsSet(manager, &m_ThaSurInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideThaSurInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideAtInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SurInterruptManager);
    }

AtInterruptManager Tha60210031SurInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSurInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210031SurInterruptManagerNew(AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newIntrController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newIntrController == NULL)
        return NULL;

    return Tha60210031SurInterruptManagerObjectInit(newIntrController, module);
    }

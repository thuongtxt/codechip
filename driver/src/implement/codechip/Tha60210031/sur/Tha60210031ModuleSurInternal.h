/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60210031ModuleSurInternal.h
 * 
 * Created Date: Oct 29, 2016
 *
 * Description : SUR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULESURINTERNAL_H_
#define _THA60210031MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sur/hard/ThaModuleHardSurInternal.h"
#include "Tha60210031ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleSurMethods
    {
    uint32 (*StartVersionChangeTo155dot50MhzClock)(Tha60210031ModuleSur self);
    }tTha60210031ModuleSurMethods;

typedef struct tTha60210031ModuleSur
    {
    tThaModuleHardSur super;
    const tTha60210031ModuleSurMethods *methods;
    }tTha60210031ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha60210031ModuleSurObjectInit(AtModuleSur self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULESURINTERNAL_H_ */


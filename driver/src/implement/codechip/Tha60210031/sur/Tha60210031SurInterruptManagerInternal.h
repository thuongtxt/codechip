/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Surveilance
 * 
 * File        : Tha60210031SurInterruptManagerInternal.h
 * 
 * Created Date: Oct 4, 2017
 *
 * Description : Surveilance interrupt manager internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SURINTERRUPTMANAGERINTERNAL_H_
#define _THA60210031SURINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sur/hard/ThaSurInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SurInterruptManager
    {
    tThaSurInterruptManager super;
    }tTha60210031SurInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60210031SurInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SURINTERRUPTMANAGERINTERNAL_H_ */


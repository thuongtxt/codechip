/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : Tha60210031ModuleSur.h
 *
 * Created Date: Sep 8, 2015
 *
 * Description : Surveillance header
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULESUR_H_
#define _THA60210031MODULESUR_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleSur * Tha60210031ModuleSur;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60210031SurInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210031MODULESUR_H_ */

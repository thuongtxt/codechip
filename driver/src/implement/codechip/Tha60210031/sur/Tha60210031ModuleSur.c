/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module SUR
 *
 * File        : Tha60210031ModuleSur.c
 *
 * Created Date: Aug 12, 2015
 *
 * Description : Ethernet module for product 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/sur/hard/engine/ThaHardSurEngineInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60210031DeviceReg.h"
#include "Tha60210031ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031ModuleSur)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031ModuleSurMethods m_methods;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tAtModuleSurMethods      m_AtModuleSurOverride;
static tThaModuleHardSurMethods m_ThaModuleHardSurOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsLoPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static uint32 BaseAddress(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 0x900000UL;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    return Tha60210031SurInterruptManagerNew(self);
    }

static uint32 StartVersionChangeTo155dot50MhzClock(Tha60210031ModuleSur self)
    {
    /* Initial version uses system clock of 155.52Mhz. Since this version, FPGA
     * use system clock of 155.50Mhz. */
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x04, 0x05, 0x46);
    }

static uint32 Clock125usDefaultValue(ThaModuleHardSur self)
    {
    const uint32 Clock155dot52Mhz = 0x4BEE;
    const uint32 Clock155dot50Mhz = 0x4BEC;
    uint32 startVersion = mMethodsGet(mThis(self))->StartVersionChangeTo155dot50MhzClock(mThis(self));
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= startVersion)
        return Clock155dot50Mhz;
    else
        return Clock155dot52Mhz;
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x0403);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureTimersConfigurable(AtModuleSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4644);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(surModule), sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, IsLoPw);
        mMethodOverride(m_ThaModuleHardSurOverride, BaseAddress);
        mMethodOverride(m_ThaModuleHardSurOverride, Clock125usDefaultValue);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule surModule = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(surModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(surModule, &m_AtModuleOverride);
    }

static void MethodsInit(AtModuleSur self)
    {
    Tha60210031ModuleSur module = (Tha60210031ModuleSur)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionChangeTo155dot50MhzClock);
        }

    mMethodsSet(module, &m_methods);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleSur);
    }

AtModuleSur Tha60210031ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleHardSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60210031ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleSurObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210031Vc1xPohProcessor.c
 *
 * Created Date: Sep 25, 2015
 *
 * Description : VC1x POH processor implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60210031PohProcessorInternal.h"
#include "Tha60210031ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModulePoh(channel) ModulePoh((AtChannel)channel)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(AtChannel self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePoh);
    }

static uint32 PohInterruptRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return (slice) * 4096UL + hwSts * 32UL + Tha60210011Vc1xPohProcessorVtId(self) + Tha60210011ModulePohBaseAddress(mModulePoh(self));

    return 0;
    }

static uint32 PohCounterReportRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cntlo_Base;
    }

static void OverrideTha60210011AuVcPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, mMethodsGet(processor), sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohInterruptRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCounterReportRegAddr);
        }

    mMethodsSet(processor, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031Vc1xPohProcessor);
    }

ThaPohProcessor Tha60210031Vc1xPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Vc1xPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaAuVcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210031Vc1xPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210031Vc1xPohProcessorObjectInit(newProcessor, vc);
    }

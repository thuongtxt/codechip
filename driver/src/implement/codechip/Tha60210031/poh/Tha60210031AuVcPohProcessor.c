/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210031AuVcPohProcessor.c
 *
 * Created Date: Sep 25, 2015
 *
 * Description : VC POH processor implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60210031PohProcessorInternal.h"
#include "Tha60210031ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModulePoh(channel) ModulePoh((AtChannel)channel)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/* Save super implementation */
static const tTha60210011AuVcPohProcessorMethods *m_Tha60210011AuVcPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(AtChannel self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePoh);
    }

static uint32 PohInterruptRegOffset(Tha60210011AuVcPohProcessor self)
    {
    uint8 hwSts, slice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwSts) == cAtOk)
        return slice * 128UL + hwSts + Tha60210011ModulePohBaseAddress(mModulePoh(self));

    return 0;
    }

static uint32 PohCounterReportRegAddr(Tha60210011AuVcPohProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cnthi_Base;
    }

static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    if (Tha60210031PohProcessorCpeStatusRegIsChanged(self))
        return cAf6Reg_pohcpestssta_Base;

    return m_Tha60210011AuVcPohProcessorMethods->PohCpeStatusRegAddr(self);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011AuVcPohProcessorMethods = mMethodsGet(processor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, m_Tha60210011AuVcPohProcessorMethods, sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohInterruptRegOffset);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCounterReportRegAddr);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegAddr);
        }

    mMethodsSet(processor, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031AuVcPohProcessor);
    }

ThaPohProcessor Tha60210031AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaAuVcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210031AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210031AuVcPohProcessorObjectInit(newProcessor, vc);
    }

eBool Tha60210031PohProcessorCpeStatusRegIsChanged(Tha60210011AuVcPohProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startHwVersionSupports = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4616);

    return (hwVersion >= startHwVersionSupports) ? cAtTrue : cAtFalse;
    }

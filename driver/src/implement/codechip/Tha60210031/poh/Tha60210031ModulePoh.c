/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Tha60210031ModulePoh
 *
 * File        : Tha60210031ModulePoh.c
 *
 * Created Date: Aug 31, 2015
 *
 * Description : Poh module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031ModulePohInternal.h"
#include "Tha60210031PohProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_PohThreshold_Offset 0x000003
#define cAf6_PohDebound_Threshold_Value_Default 0x5555555D

/*--------------------------- Typedef -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePohMethods         m_ThaModulePohOverride;
static tThaModulePohV2Methods       m_ThaModulePohV2Override;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/* save supper */
static const tAtModuleMethods      *m_AtModuleMethods = NULL;
static const tTha60210011ModulePohMethods *m_Tha60210011ModulePohMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModulePoh);
    }

static uint32 BaseAddress(ThaModulePoh self)
    {
    AtUnused(self);
    return 0xB00000;
    }

static eBool CpeStsStatRegIsChanged(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule) self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startHwVersionSupports = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4616);

    return (hwVersion >= startHwVersionSupports) ? cAtTrue : cAtFalse;
    }

static uint32 Reg_pohcpestssta(ThaModulePoh self, uint8 sliceid, uint8 stsid)
    {
    if (!CpeStsStatRegIsChanged(self))
        m_Tha60210011ModulePohMethods->Reg_pohcpestssta(self, sliceid, stsid);

    return sliceid + (stsid * 4UL);
    }

static uint32 Reg_pohvtpohgrb(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    AtUnused(self);
    return (sliceid) * 672UL + (stsid) * 28UL + (vtid);
    }

static uint32 Reg_pohcpestsctr(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 tu3en)
    {
    AtUnused(self);
    return (sliceid) * 48UL + (stsid) * 2UL + (tu3en);
    }

static uint32 Reg_pohcpevtctr(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    AtUnused(self);
    return (sliceid) * 672UL + (stsid) * 28UL + (vtid);
    }

static uint32 Reg_pohmsgstsexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 8UL + (stsid) * 32UL + (msgid);
    }

static uint32 Reg_pohmsgstscur(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 8UL + (stsid) * 32UL + (msgid);
    }

static uint32 Reg_pohmsgvtexp(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 5376UL + (stsid) * 224UL + (vtid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgvtcur(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 5376UL + (stsid) * 224UL + (vtid) * 8UL + (msgid);
    }

static uint32 Reg_pohmsgstsins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 8UL + (stsid) * 32UL + (msgid);
    }

static uint32 Reg_pohmsgvtins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid) * 5376UL + (stsid) * 224UL + (vtid) * 8UL + (msgid);
    }

static uint32 Reg_ter_ctrlhi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return (sts) * 2UL + (ocid);
    }

static uint32 Reg_ter_ctrllo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return (sts) * 56UL + (ocid) * 28UL + (vt);
    }

static uint32 Reg_rtlpohccterbufhi(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 bgrp)
    {
    AtUnused(self);
    return (ocid) * 256UL + (sts) * 4UL + (bgrp);
    }

static uint32 Reg_rtlpohccterbuflo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt, uint8 bgrp)
    {
    AtUnused(self);
    return (ocid) * 4096UL + (sts) * 64UL + (vt) * 2UL + (bgrp);
    }

static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60210031SdhLineTtiProcessorNew((AtSdhChannel)line);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210031AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return NULL;
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210031Vc1xPohProcessorNew(vc);
    }

static uint32 Reg_ipm_cnthi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return (sts) * 4UL + (ocid);
    }

static uint32 Reg_ipm_cntlo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return (sts) * 56UL + (ocid) * 28UL + (vt);
    }

static ThaVersionReader VersionReader(Tha60210011ModulePoh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportSdSfInterrupt(Tha60210011ModulePoh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x09, 0x28, 0x0);
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    AtUnused(self);
    AtUnused(ramId);
    AtUnused(localRamId);
    return NULL;
    }

static uint32 StartVersionSupportBlockModeCounters(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x02, 0x36);
    }

static eAtRet DefaultSet(ThaModulePohV2 self)
    {
    uint32 address = cAf6Reg_PohThreshold_Offset + ThaModulePohBaseAddress((ThaModulePoh)self);
    mModuleHwWrite(self, address, cAf6_PohDebound_Threshold_Value_Default);
    return cAtOk;
    }

static uint32 StartVersionSupportPohMonitorEnabling(ThaModulePoh self)
    {
    /* This feature has not been available for this product. (see OCN product) */
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool UpsrIsSupported(Tha60210011ModulePoh self)
    {
    /* This feature has not been available for this product. (see OCN product) */
    AtUnused(self);
    return cAtFalse;
    }

static eBool PslTtiInterruptIsSupported(ThaModulePoh self)
    {
    /* Just the same version support */
    return CpeStsStatRegIsChanged(self);
    }

static uint32 StartVersionSupportNewInterruptMasking(AtDevice device)
    {
    /* From this version, workaround of missed PLM interrupt will be dropped. */
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x16, 0x42);
    }

static uint32 StartBuiltNumberSupportNewInterruptMasking(AtDevice device)
    {
    AtUnused(device);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x0, 0x0, 0x07);
    }

static eBool SoftStickyIsNeeded(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceVersionNumber(device) > StartVersionSupportNewInterruptMasking(device))
        return cAtFalse;

    if (AtDeviceVersionNumber(device) == StartVersionSupportNewInterruptMasking(device))
       return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportNewInterruptMasking(device)) ? cAtFalse : cAtTrue;

    return cAtTrue;
    }

static uint32 NumberStsSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumberVtSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 StartVersionSupportPohInterrupt(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x08, 0x14, 0x00);
    }

static eBool PohInterruptIsSupported(Tha60210011ModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionHasThis = StartVersionSupportPohInterrupt(device);
    return (AtDeviceVersionNumber(device) >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, StartVersionSupportPohMonitorEnabling);
        mMethodOverride(m_ThaModulePohOverride, SoftStickyIsNeeded);
        mMethodOverride(m_ThaModulePohOverride, BaseAddress);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideThaModulePohV2(AtModule self)
    {
    ThaModulePohV2 pohModule = (ThaModulePohV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohV2Override, mMethodsGet(pohModule), sizeof(m_ThaModulePohV2Override));

        mMethodOverride(m_ThaModulePohV2Override, DefaultSet);
        }

    mMethodsSet(pohModule, &m_ThaModulePohV2Override);
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePohMethods = mMethodsGet(pohModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, m_Tha60210011ModulePohMethods, sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohvtpohgrb);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohcpestsctr);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohcpevtctr);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgstsexp);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgstscur);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgvtexp);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgvtcur);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgstsins);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgvtins);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ter_ctrlhi);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ter_ctrllo);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_rtlpohccterbufhi);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_rtlpohccterbuflo);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ipm_cnthi);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ipm_cntlo);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSdSfInterrupt);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportBlockModeCounters);
        mMethodOverride(m_Tha60210011ModulePohOverride, UpsrIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohcpestssta);
        mMethodOverride(m_Tha60210011ModulePohOverride, PslTtiInterruptIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberStsSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberVtSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, PohInterruptIsSupported);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePoh(self);
    OverrideThaModulePohV2(self);
    OverrideTha60210011ModulePoh(self);
    }

AtModule Tha60210031ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModulePohObjectInit(newModule, device);
    }



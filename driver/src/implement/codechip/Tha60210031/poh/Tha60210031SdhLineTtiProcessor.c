/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210031SdhLineTtiProcessor.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : SDH line TTI processor implement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/poh/ThaModulePohInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"
#include "../../Tha60210011/poh/Tha60210011PohProcessorInternal.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../ocn/Tha60210031ModuleOcnReg.h"
#include "Tha60210031PohProcessorInternal.h"
#include "Tha60210031ModulePohReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaSonetNumBytesInDword        4
#define cAf6_pohcpestssta_JnBadFrame_Mask      cBit24

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031SdhLineTtiProcessor)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031SdhLineTtiProcessorMethods m_methods;

/* Override */
static tThaTtiProcessorMethods m_ThaTtiProcessorOverride;
static tThaSdhLineTtiProcessorV2Methods m_ThaSdhLineTtiProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn ModuleOcn(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static ThaModulePoh ModulePoh(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint32 OcnJ0TxMsgBufDefaultOffset(ThaSdhLineTtiProcessorV2 self, uint8 bufId)
    {
    uint8 sliceId, lineId;
    ThaTtiProcessor processor = (ThaTtiProcessor)self;

    if (ThaSdhLineHwIdGet((AtSdhLine)ThaTtiProcessorChannelGet(processor), cThaModuleOcn, &sliceId, &lineId) != cAtOk)
        return cInvalidUint32;

    return 1024UL * sliceId + 16UL * lineId + bufId + Tha60210011ModuleOcnBaseAddress(ModuleOcn(processor));
    }

static uint32 TxJ0MessageBufferRegAddress(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_tfmj0insctl_Base;
    }

static uint32 PohInterruptRegOffset(Tha60210031SdhLineTtiProcessor self)
    {
    uint8 hwStsInSlice, slice;
    ThaTtiProcessor processor = (ThaTtiProcessor)self;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) != cAtOk)
        return slice * 128UL + hwStsInSlice + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static uint32 AlarmGet(ThaTtiProcessor self)
    {
    uint32 defects = 0;
    uint32 regAddr = cAf6Reg_alm_stahi_Base + mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    uint32 regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_stahi_bertcasta_Mask)
        defects |= cAtSdhLineAlarmBerTca;
    if (regVal & cAf6_alm_stahi_timsta_Mask)
        defects |= cAtSdhLineAlarmTim;
    if (regVal & cAf6_alm_stahi_bersfsta_Mask)
        defects |= cAtSdhLineAlarmBerSf;
    if (regVal & cAf6_alm_stahi_bersdsta_Mask)
        defects |= cAtSdhLineAlarmBerSd;

    return defects;
    }

static uint32 AlarmInterruptHelper(ThaTtiProcessor self)
    {
    uint32 stickies = 0;
    uint32 regAddr = cAf6Reg_alm_chghi_Base + mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    uint32 regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_chghi_bertcastachg_Mask)
        stickies |= cAtSdhLineAlarmBerTca;
    if (regVal & cAf6_alm_chghi_timstachg_Mask)
        stickies |= cAtSdhLineAlarmTim;
    if (regVal & cAf6_alm_chghi_bersfstachg_Mask)
        stickies |= cAtSdhLineAlarmBerSf;
    if (regVal & cAf6_alm_chghi_bersdstachg_Mask)
        stickies |= cAtSdhLineAlarmBerSd;
    return stickies;
    }

static uint32 AlarmInterruptGet(ThaTtiProcessor self)
    {
    return AlarmInterruptHelper(self);
    }

static uint32 AlarmInterruptClear(ThaTtiProcessor self)
    {
    uint32 regAddr = cAf6Reg_alm_chghi_Base + mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    uint32 stickies = AlarmInterruptHelper(self);
    ThaTtiProcessorWrite(self, regAddr, 0xFFFFFFFF, cThaModulePoh);
    return stickies;
    }

static uint32 PohCpeOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor poh = (ThaTtiProcessor)self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(poh);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return slice * 48UL + (hwStsInSlice) * 2UL + Tha60210011ModulePohBaseAddress(ModulePoh(poh));

    return 0;
    }

static eAtRet TimAffectingEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_pohcpestsctr_Base + mMethodsGet(mThis(self))->PohCpeOffset(mThis(self));
    uint32 regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_TimDstren_, mBoolToBin(enable));
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);
    return cAtOk;
    }

static eBool TimAffectingIsEnabled(ThaTtiProcessor self)
    {
    uint32 regAddr = cAf6Reg_pohcpestsctr_Base + mMethodsGet(mThis(self))->PohCpeOffset(mThis(self));
    uint32 regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpestsctr_TimDstren_));
    }

static eAtRet InterruptMaskSet(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_alm_mskhi_Base + mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    uint32 regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    if (defectMask & cAtSdhLineAlarmBerTca)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bertcamsk_, (enableMask & cAtSdhLineAlarmBerTca) ? 1 : 0);
    if (defectMask & cAtSdhLineAlarmTim)
        mRegFieldSet(regVal, cAf6_alm_mskhi_timmsk_, (enableMask & cAtSdhLineAlarmTim) ? 1 : 0);
    if (defectMask & cAtSdhLineAlarmBerSd)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bersdmsk_, (enableMask & cAtSdhLineAlarmBerSd) ? 1 : 0);
    if (defectMask & cAtSdhLineAlarmBerSf)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bersfmsk_, (enableMask & cAtSdhLineAlarmBerSf) ? 1 : 0);
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(ThaTtiProcessor self)
    {
    uint32 mask    = 0;
    uint32 regAddr = cAf6Reg_alm_mskhi_Base + mMethodsGet(mThis(self))->PohInterruptRegOffset(mThis(self));
    uint32 regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_mskhi_bertcamsk_Mask)
        mask |= cAtSdhLineAlarmBerTca;
    if (regVal & cAf6_alm_mskhi_timmsk_Mask)
        mask |= cAtSdhLineAlarmTim;
    if (regVal & cAf6_alm_mskhi_bersdmsk_Mask)
        mask |= cAtSdhLineAlarmBerSd;
    if (regVal & cAf6_alm_mskhi_bersfmsk_Mask)
        mask |= cAtSdhLineAlarmBerSf;
    return mask;
    }

static eAtRet MonitorEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_pohcpestsctr_Base + mMethodsGet(mThis(self))->PohCpeOffset(mThis(self));
    uint32 regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_TimEnb_, mBoolToBin(enable));
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool MonitorIsEnabled(ThaTtiProcessor self)
    {
    uint32 regAddr = cAf6Reg_pohcpestsctr_Base + mMethodsGet(mThis(self))->PohCpeOffset(mThis(self));
    uint32 regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    return mBinToBool(mRegField(regVal, cAf6_pohcpestsctr_TimEnb_));
    }

static uint32 PohBufferOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (hwStsInSlice) * 32UL + slice * 8UL + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static eAtSdhTtiMode TtiModeHw2Sw(uint32 value)
    {
    if (value == 0)
        return cAtSdhTtiMode1Byte;
    if (value == 1)
        return cAtSdhTtiMode16Byte;
    if (value == 2)
        return cAtSdhTtiMode64Byte;
    return cAtSdhTtiMode1Byte;
    }

static eAtSdhTtiMode RxTtiModeGet(ThaTtiProcessor self)
    {
    uint32 regAddr = cAf6Reg_pohcpestsctr_Base + mMethodsGet(mThis(self))->PohCpeOffset(mThis(self));
    uint32 regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    return TtiModeHw2Sw(mRegField(regVal, cAf6_pohcpestsctr_J1mode_));
    }

static uint8 TtiModeSw2Hw(eAtSdhTtiMode value)
    {
    if (value == cAtSdhTtiMode1Byte)
        return 0;
    if (value == cAtSdhTtiMode16Byte)
        return 1;
    if (value == cAtSdhTtiMode64Byte)
        return 2;
    return 0;
    }

static eAtRet RxTtiModeSet(ThaTtiProcessor self, eAtSdhTtiMode ttiMode)
    {
    uint32 regAddr = cAf6Reg_pohcpestsctr_Base + mMethodsGet(mThis(self))->PohCpeOffset(mThis(self));
    uint32 regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cAf6_pohcpestsctr_J1mode_, TtiModeSw2Hw(ttiMode));
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);
    return cAtOk;
    }

static eAtRet ExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint32 regAddr;
    tti->mode = RxTtiModeGet(self);

    regAddr = cAf6Reg_pohmsgstsexp_Base + mMethodsGet(mThis(self))->PohBufferOffset(mThis(self));
    return Tha60210011AuVcPohProcessorTtiRead(tti, regAddr, (AtChannel)ThaTtiProcessorChannelGet(self));
    }

static eAtRet ExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    uint32 regAddr;
    RxTtiModeSet(self, tti->mode);

    regAddr = cAf6Reg_pohmsgstsexp_Base + mMethodsGet(mThis(self))->PohBufferOffset(mThis(self));
    return Tha60210011AuVcPohProcessorTtiWrite(tti, regAddr, (AtChannel)ThaTtiProcessorChannelGet(self));
    }

static uint32 PohGrabberOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (hwStsInSlice) * 8UL + slice + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static eAtModuleSdhRet RxTtiModeCheck(ThaTtiProcessor self)
    {
    Tha60210031SdhLineTtiProcessor processor = (Tha60210031SdhLineTtiProcessor)self;
    uint32 regAddr, regVal;

    if (!Tha60210011ModulePohShouldCheckTtiOofStatus((Tha60210011ModulePoh)ModulePoh(self)))
        return cAtOk;

    regAddr = mMethodsGet(processor)->PohJ0StatusRegAddress(processor) +
              mMethodsGet(processor)->PohJ0StatusOffset(processor);
    regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
    regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
    if (regVal & cAf6_pohcpestssta_JnBadFrame_Mask)
        return cAtModuleSdhErrorTtiModeMismatch;

    return cAtOk;
    }

static eAtRet RxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    eAtRet ret;
    uint32 regAddr;
    uint32 regVal;
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhChannel line = ThaTtiProcessorChannelGet(self);

    mMethodsGet(osal)->MemInit(osal, tti->message, 0, cAtSdhChannelMaxTtiLength);
    tti->mode = RxTtiModeGet(self);

    if (ThaTtiProcessorHasAlarmForwarding(self))
        return cAtOk;

    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        regAddr = ThaModulePohstspohgrbBase(ModulePoh(self), line) + mMethodsGet(mThis(self))->PohGrabberOffset(mThis(self));
        regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
        tti->message[0] = (uint8)mRegField(regVal, cAf6_pohstspohgrb_J1_);
        return cAtOk;
        }

    ret = RxTtiModeCheck(self);
    if (ret != cAtOk)
        return ret;

    regAddr = cAf6Reg_pohmsgstscur_Base + mMethodsGet(mThis(self))->PohBufferOffset(mThis(self));
    return Tha60210011AuVcPohProcessorTtiRead(tti, regAddr, (AtChannel)ThaTtiProcessorChannelGet(self));
    }

static uint32 PohJ0StatusRegAddress(Tha60210031SdhLineTtiProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestssta_Base;
    }

static eBool HasAlarmForwarding(ThaTtiProcessor self)
    {
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);
    uint32 regVal[cThaLongRegMaxSize];
    uint32 regAddr = ThaModulePohstspohgrbBase(ModulePoh(self), (AtSdhChannel)channel) +
            mMethodsGet(mThis(self))->PohGrabberOffset(mThis(self));
    mChannelHwLongRead(channel, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
    return mRegField(regVal[2], cAf6_pohstspohgrb_ais_) ? cAtTrue : cAtFalse;
    }

static uint32 PohJ0StatusOffset(Tha60210031SdhLineTtiProcessor self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor) self;
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = ThaTtiProcessorChannelGet(processor);

    if (ThaSdhLineHwIdGet((AtSdhLine)channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (hwStsInSlice) * 4UL + slice + Tha60210011ModulePohBaseAddress(ModulePoh(processor));

    return 0;
    }

static void MethodsInit(ThaTtiProcessor self)
    {
    Tha60210031SdhLineTtiProcessor processor = (Tha60210031SdhLineTtiProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohBufferOffset);
        mMethodOverride(m_methods, PohCpeOffset);
        mMethodOverride(m_methods, PohGrabberOffset);
        mMethodOverride(m_methods, PohInterruptRegOffset);
        mMethodOverride(m_methods, PohJ0StatusRegAddress);
        mMethodOverride(m_methods, PohJ0StatusOffset);
        }

    mMethodsSet(processor, &m_methods);
    }

static void OverrideThaTtiProcessor(ThaTtiProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaTtiProcessorOverride, mMethodsGet(self), sizeof(m_ThaTtiProcessorOverride));

        mMethodOverride(m_ThaTtiProcessorOverride, AlarmGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmInterruptGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmInterruptClear);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingIsEnabled);
        mMethodOverride(m_ThaTtiProcessorOverride, InterruptMaskSet);
        mMethodOverride(m_ThaTtiProcessorOverride, InterruptMaskGet);
        mMethodOverride(m_ThaTtiProcessorOverride, MonitorEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, MonitorIsEnabled);

        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiSet);
        mMethodOverride(m_ThaTtiProcessorOverride, RxTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, HasAlarmForwarding);
        }

    mMethodsSet(self, &m_ThaTtiProcessorOverride);
    }

static void OverrideThaSdhLineTtiProcessorV2(ThaTtiProcessor self)
    {
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineTtiProcessorV2Override, mMethodsGet(processor), sizeof(m_ThaSdhLineTtiProcessorV2Override));

        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, OcnJ0TxMsgBufDefaultOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, TxJ0MessageBufferRegAddress);
        }

    mMethodsSet(processor, &m_ThaSdhLineTtiProcessorV2Override);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideThaTtiProcessor(self);
    OverrideThaSdhLineTtiProcessorV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SdhLineTtiProcessor);
    }

ThaTtiProcessor Tha60210031SdhLineTtiProcessorObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineTtiProcessorV2ObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60210031SdhLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031SdhLineTtiProcessorObjectInit(newProcessor, sdhChannel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210031PohProcessorInternal.h
 * 
 * Created Date: Sep 25, 2015
 *
 * Description : POH processor internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031POHPROCESSORINTERNAL_H_
#define _THA60210031POHPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/poh/v2/ThaSdhLineTtiProcessorV2Internal.h"
#include "../../Tha60210011/poh/Tha60210011PohProcessor.h"
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SdhLineTtiProcessor * Tha60210031SdhLineTtiProcessor;

typedef struct tTha60210031SdhLineTtiProcessorMethods
    {
    uint32 (*PohBufferOffset)(Tha60210031SdhLineTtiProcessor self);
    uint32 (*PohCpeOffset)(Tha60210031SdhLineTtiProcessor self);
    uint32 (*PohGrabberOffset)(Tha60210031SdhLineTtiProcessor self);
    uint32 (*PohInterruptRegOffset)(Tha60210031SdhLineTtiProcessor self);
    uint32 (*PohJ0StatusRegAddress)(Tha60210031SdhLineTtiProcessor self);
    uint32 (*PohJ0StatusOffset)(Tha60210031SdhLineTtiProcessor self);
    }tTha60210031SdhLineTtiProcessorMethods;

typedef struct tTha60210031SdhLineTtiProcessor
    {
    tThaSdhLineTtiProcessorV2 super;
    const tTha60210031SdhLineTtiProcessorMethods * methods;
    }tTha60210031SdhLineTtiProcessor;

typedef struct tTha60210031AuVcPohProcessor
    {
    tTha60210051AuVcPohProcessor super;
    }tTha60210031AuVcPohProcessor;

typedef struct tTha60210031Vc1xPohProcessor
    {
    tTha60210051Vc1xPohProcessor super;
    }tTha60210031Vc1xPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaTtiProcessor Tha60210031SdhLineTtiProcessorNew(AtSdhChannel sdhChannel);
ThaPohProcessor Tha60210031AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210031Vc1xPohProcessorNew(AtSdhVc vc);

ThaPohProcessor Tha60210031AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
ThaPohProcessor Tha60210031Vc1xPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
ThaTtiProcessor Tha60210031SdhLineTtiProcessorObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel);

eBool Tha60210031PohProcessorCpeStatusRegIsChanged(Tha60210011AuVcPohProcessor self);

#endif /* _THA60210031POHPROCESSORINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210031ModuleOcn.h
 * 
 * Created Date: Sep 18, 2015
 *
 * Description : Module OCN header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEOCN_H_
#define _THA60210031MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleOcn *Tha60210031ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60210031OcnLineTimingModeSet(AtChannel line, eAtTimingMode timingMode);
eAtTimingMode Tha60210031OcnLineTimingModeGet(AtChannel line);

eAtRet Tha60210031OcnLineLocalLoopEnable(AtChannel, eBool enable);
eBool Tha60210031OcnLineLocalLoopIsEnabled(AtChannel self);
eAtRet Tha60210031OcnLineRemoteLoopEnable(AtChannel self, eBool enable);
eBool Tha60210031OcnLineRemoteLoopIsEnabled(AtChannel self);
eAtRet Tha60210031OcnLineTxAisForce(AtChannel self, eBool enable);
eBool Tha60210031OcnLineTxAisIsForced(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEOCN_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210031ModuleOcnReg.h
 * 
 * Created Date: Sep 15, 2015
 *
 * Description : OCN regs of 60210031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEOCNREG_H_
#define _THA60210031MODULEOCNREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Master  Control
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for Rx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfm_reg_Base                                                                        0x00000
#define cAf6Reg_glbrfm_reg                                                                             0x00000
#define cAf6Reg_glbrfm_reg_WidthVal                                                                         32
#define cAf6Reg_glbrfm_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RxFrmAislRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when AIS_L
condition detected. 1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_Bit_Start                                                           16
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_Bit_End                                                             16
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_Mask                                                            cBit16
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_Shift                                                               16
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_MaxVal                                                             0x1
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_MinVal                                                             0x0
#define cAf6_glbrfm_reg_RxFrmAislRdiLEn_RstVal                                                             0x1

/*--------------------------------------
BitField Name: RxFrmTimRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when TIM
condition detected. 1: Enable 0: Disable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_Bit_Start                                                            15
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_Bit_End                                                              15
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_Mask                                                             cBit15
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_Shift                                                                15
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmTimRdiLEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: RxFrmOofRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when OOF
condition detected. 1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_Bit_Start                                                            14
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_Bit_End                                                              14
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_Mask                                                             cBit14
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_Shift                                                                14
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmOofRdiLEn_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxFrmLofRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOF
condition detected. 1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_Bit_Start                                                            13
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_Bit_End                                                              13
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_Mask                                                             cBit13
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_Shift                                                                13
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmLofRdiLEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: RxFrmLosRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOS
condition detected. 1: Enable 0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_Bit_Start                                                            12
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_Bit_End                                                              12
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_Mask                                                             cBit12
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_Shift                                                                12
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmLosRdiLEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: RxFrmAislAisPEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when AIS_L condition detected. 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_Bit_Start                                                            8
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_Bit_End                                                              8
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_Mask                                                             cBit8
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_Shift                                                                8
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_MaxVal                                                             0x1
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_MinVal                                                             0x0
#define cAf6_glbrfm_reg_RxFrmAislAisPEn_RstVal                                                             0x1

/*--------------------------------------
BitField Name: RxFrmTimAisPEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when TIM condition detected. 1: Enable 0: Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_Bit_Start                                                             7
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_Bit_End                                                               7
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_Mask                                                              cBit7
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_Shift                                                                 7
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmTimAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: RxFrmOofAisPEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when OOF condition detected. 1: Enable 0: Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_Bit_Start                                                             6
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_Bit_End                                                               6
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_Mask                                                              cBit6
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_Shift                                                                 6
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmOofAisPEn_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxFrmLofAisPEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOF condition detected. 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_Bit_Start                                                             5
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_Bit_End                                                               5
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_Mask                                                              cBit5
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_Shift                                                                 5
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmLofAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: RxFrmLosAisPEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOS condition detected. 1: Enable 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_Bit_Start                                                             4
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_Bit_End                                                               4
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_Mask                                                              cBit4
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_Shift                                                                 4
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmLosAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: RxFrmLofCfg
BitField Type: RW
BitField Desc: Detected LOF mode. 1: OOF counter is reset in In-frame state. 0:
OOF counter is only reset when In-frame state exists continuously more than 3 ms
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLofCfg_Bit_Start                                                                3
#define cAf6_glbrfm_reg_RxFrmLofCfg_Bit_End                                                                  3
#define cAf6_glbrfm_reg_RxFrmLofCfg_Mask                                                                 cBit3
#define cAf6_glbrfm_reg_RxFrmLofCfg_Shift                                                                    3
#define cAf6_glbrfm_reg_RxFrmLofCfg_MaxVal                                                                 0x1
#define cAf6_glbrfm_reg_RxFrmLofCfg_MinVal                                                                 0x0
#define cAf6_glbrfm_reg_RxFrmLofCfg_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: RxFrmLosDetDis
BitField Type: RW
BitField Desc: Enable/Disable detect LOS. 1: Disable 0: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLosDetDis_Bit_Start                                                             2
#define cAf6_glbrfm_reg_RxFrmLosDetDis_Bit_End                                                               2
#define cAf6_glbrfm_reg_RxFrmLosDetDis_Mask                                                              cBit2
#define cAf6_glbrfm_reg_RxFrmLosDetDis_Shift                                                                 2
#define cAf6_glbrfm_reg_RxFrmLosDetDis_MaxVal                                                              0x1
#define cAf6_glbrfm_reg_RxFrmLosDetDis_MinVal                                                              0x0
#define cAf6_glbrfm_reg_RxFrmLosDetDis_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxFrmLosCfg
BitField Type: RW
BitField Desc: Detected LOS mode. 1: the LOS declare when all zero or all one
detected 0: the LOS declare when all zero detected
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmLosCfg_Bit_Start                                                                1
#define cAf6_glbrfm_reg_RxFrmLosCfg_Bit_End                                                                  1
#define cAf6_glbrfm_reg_RxFrmLosCfg_Mask                                                                 cBit1
#define cAf6_glbrfm_reg_RxFrmLosCfg_Shift                                                                    1
#define cAf6_glbrfm_reg_RxFrmLosCfg_MaxVal                                                                 0x1
#define cAf6_glbrfm_reg_RxFrmLosCfg_MinVal                                                                 0x0
#define cAf6_glbrfm_reg_RxFrmLosCfg_RstVal                                                                 0x1

/*--------------------------------------
BitField Name: RxFrmDescrEn
BitField Type: RW
BitField Desc: Enable/disable de-scrambling of the Rx coming data stream. 1:
Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmDescrEn_Bit_Start                                                               0
#define cAf6_glbrfm_reg_RxFrmDescrEn_Bit_End                                                                 0
#define cAf6_glbrfm_reg_RxFrmDescrEn_Mask                                                                cBit0
#define cAf6_glbrfm_reg_RxFrmDescrEn_Shift                                                                   0
#define cAf6_glbrfm_reg_RxFrmDescrEn_MaxVal                                                                0x1
#define cAf6_glbrfm_reg_RxFrmDescrEn_MinVal                                                                0x0
#define cAf6_glbrfm_reg_RxFrmDescrEn_RstVal                                                                0x1


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Threshold
Reg Addr   : 0x00006
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for LOS detection at receive SONET/SDH framer,There are three thresholds

------------------------------------------------------------------------------*/
#define cAf6Reg_glblosthr_reg_Base                                                                     0x00006
#define cAf6Reg_glblosthr_reg                                                                          0x00006
#define cAf6Reg_glblosthr_reg_WidthVal                                                                      32
#define cAf6Reg_glblosthr_reg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxFrmNumValMonDis
BitField Type: RW
BitField Desc: Disable to generate LOS to Rx Framer base on detecting number of
valid on Rx data valid. 1: Disable 0: Enable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_Bit_Start                                                      28
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_Bit_End                                                        28
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_Mask                                                       cBit28
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_Shift                                                          28
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_MaxVal                                                        0x1
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_MinVal                                                        0x0
#define cAf6_glblosthr_reg_RxFrmNumValMonDis_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxFrmNumValMonThr
BitField Type: RW
BitField Desc: Threshold to generate LOS to Rx Framer base on detecting number
of valid on Rx data valid.(Threshold = PPM * 1). Default 0x20 ~ 32ppm
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_Bit_Start                                                      20
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_Bit_End                                                        27
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_Mask                                                    cBit27_20
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_Shift                                                          20
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_MaxVal                                                       0xff
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_MinVal                                                        0x0
#define cAf6_glblosthr_reg_RxFrmNumValMonThr_RstVal                                                       0x20

/*--------------------------------------
BitField Name: RxFrmLosClr2Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for reset no transition counter. The
recommended value is 0x8 (~5us).
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_Bit_Start                                                        16
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_Bit_End                                                          19
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_Mask                                                      cBit19_16
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_Shift                                                            16
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_MaxVal                                                          0xf
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_MinVal                                                          0x0
#define cAf6_glblosthr_reg_RxFrmLosClr2Thr_RstVal                                                          0x8

/*--------------------------------------
BitField Name: RxFrmLosSetThr
BitField Type: RW
BitField Desc: Configure the value that define the period of time in which there
is no transition detected from incoming data from SERDES. The recommended value
is 0x28 (~25us).
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glblosthr_reg_RxFrmLosSetThr_Bit_Start                                                          8
#define cAf6_glblosthr_reg_RxFrmLosSetThr_Bit_End                                                           15
#define cAf6_glblosthr_reg_RxFrmLosSetThr_Mask                                                        cBit15_8
#define cAf6_glblosthr_reg_RxFrmLosSetThr_Shift                                                              8
#define cAf6_glblosthr_reg_RxFrmLosSetThr_MaxVal                                                          0xff
#define cAf6_glblosthr_reg_RxFrmLosSetThr_MinVal                                                           0x0
#define cAf6_glblosthr_reg_RxFrmLosSetThr_RstVal                                                          0x28

/*--------------------------------------
BitField Name: RxFrmLosClr1Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for counting at INFRM to change state
to LOS The recommended value is 0x51 (~50us).
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_Bit_Start                                                         0
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_Bit_End                                                           7
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_Mask                                                        cBit7_0
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_Shift                                                             0
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_MaxVal                                                         0xff
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_MinVal                                                          0x0
#define cAf6_glblosthr_reg_RxFrmLosClr1Thr_RstVal                                                         0x51


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOF Threshold
Reg Addr   : 0x00007
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds

------------------------------------------------------------------------------*/
#define cAf6Reg_glblofthr_reg_Base                                                                     0x00007
#define cAf6Reg_glblofthr_reg                                                                          0x00007
#define cAf6Reg_glblofthr_reg_WidthVal                                                                      32
#define cAf6Reg_glblofthr_reg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxFrmLofSetThr
BitField Type: RW
BitField Desc: Configure the OOF time counter threshold for entering LOF state.
Resolution of this threshold is one frame.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Bit_Start                                                          8
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Bit_End                                                           15
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Mask                                                        cBit15_8
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Shift                                                              8
#define cAf6_glblofthr_reg_RxFrmLofSetThr_MaxVal                                                          0xff
#define cAf6_glblofthr_reg_RxFrmLofSetThr_MinVal                                                           0x0
#define cAf6_glblofthr_reg_RxFrmLofSetThr_RstVal                                                          0x18

/*--------------------------------------
BitField Name: RxFrmLofClrThr
BitField Type: RW
BitField Desc: Configure the In-frame time counter threshold for exiting LOF
state. Resolution of this threshold is one frame.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Bit_Start                                                          0
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Bit_End                                                            7
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Mask                                                         cBit7_0
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Shift                                                              0
#define cAf6_glblofthr_reg_RxFrmLofClrThr_MaxVal                                                          0xff
#define cAf6_glblofthr_reg_RxFrmLofClrThr_MinVal                                                           0x0
#define cAf6_glblofthr_reg_RxFrmLofClrThr_RstVal                                                          0x18


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtfm_reg_Base                                                                        0x00001
#define cAf6Reg_glbtfm_reg                                                                             0x00001
#define cAf6Reg_glbtfm_reg_WidthVal                                                                         32
#define cAf6Reg_glbtfm_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: TxLineOofFrc
BitField Type: RW
BitField Desc: Enable/disable force OOF for tx lines.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxLineOofFrc_Bit_Start                                                               2
#define cAf6_glbtfm_reg_TxLineOofFrc_Bit_End                                                                 2
#define cAf6_glbtfm_reg_TxLineOofFrc_Mask                                                                cBit2
#define cAf6_glbtfm_reg_TxLineOofFrc_Shift                                                                   2
#define cAf6_glbtfm_reg_TxLineOofFrc_MaxVal                                                                0x1
#define cAf6_glbtfm_reg_TxLineOofFrc_MinVal                                                                0x0
#define cAf6_glbtfm_reg_TxLineOofFrc_RstVal                                                                0x0

/*--------------------------------------
BitField Name: TxLineB1errFrc
BitField Type: RW
BitField Desc: Enable/disable force B1 error for  tx lines.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxLineB1errFrc_Bit_Start                                                             1
#define cAf6_glbtfm_reg_TxLineB1errFrc_Bit_End                                                               1
#define cAf6_glbtfm_reg_TxLineB1errFrc_Mask                                                              cBit1
#define cAf6_glbtfm_reg_TxLineB1errFrc_Shift                                                                 1
#define cAf6_glbtfm_reg_TxLineB1errFrc_MaxVal                                                              0x1
#define cAf6_glbtfm_reg_TxLineB1errFrc_MinVal                                                              0x0
#define cAf6_glbtfm_reg_TxLineB1errFrc_RstVal                                                              0x0

/*--------------------------------------
BitField Name: TxLineScrEn
BitField Type: RW
BitField Desc: Enable/disable scrambling of the Tx data stream. 1: Enable 0:
Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxLineScrEn_Bit_Start                                                                0
#define cAf6_glbtfm_reg_TxLineScrEn_Bit_End                                                                  0
#define cAf6_glbtfm_reg_TxLineScrEn_Mask                                                                 cBit0
#define cAf6_glbtfm_reg_TxLineScrEn_Shift                                                                    0
#define cAf6_glbtfm_reg_TxLineScrEn_MaxVal                                                                 0x1
#define cAf6_glbtfm_reg_TxLineScrEn_MinVal                                                                 0x0
#define cAf6_glbtfm_reg_TxLineScrEn_RstVal                                                                 0x1


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Line clock 6M selection 1
Reg Addr   : 0x0000a
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for selecting clock 6M source to transmit Tx line stream (EC1 Id: 0-23)

------------------------------------------------------------------------------*/
#define cAf6Reg_glbclksel_reg1_Base                                                                    0x0000a
#define cAf6Reg_glbclksel_reg1                                                                         0x0000a
#define cAf6Reg_glbclksel_reg1_WidthVal                                                                     32
#define cAf6Reg_glbclksel_reg1_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: TxLineClk6mSel1
BitField Type: RW
BitField Desc: Bit #0 for line #0. 1: Clock 6M is soure2 (External) 0: Clock 6M
is soure1 (System).
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_Bit_Start                                                        0
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_Bit_End                                                         23
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_Mask                                                      cBit23_0
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_Shift                                                            0
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_MaxVal                                                    0xffffff
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_MinVal                                                         0x0
#define cAf6_glbclksel_reg1_TxLineClk6mSel1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Line clock 6M selection 2
Reg Addr   : 0x0000b
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for selecting clock 6M source to transmit Tx line stream (EC1 Id: 24-47)

------------------------------------------------------------------------------*/
#define cAf6Reg_glbclksel_reg2_Base                                                                    0x0000b
#define cAf6Reg_glbclksel_reg2                                                                         0x0000b
#define cAf6Reg_glbclksel_reg2_WidthVal                                                                     32
#define cAf6Reg_glbclksel_reg2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: TxLineClk6mSel2
BitField Type: RW
BitField Desc: Bit #0 for line #24. 1: Clock 6M is source2 (External) 0: Clock
6M is source1 (System).
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_Bit_Start                                                        0
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_Bit_End                                                         23
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_Mask                                                      cBit23_0
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_Shift                                                            0
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_MaxVal                                                    0xffffff
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_MinVal                                                         0x0
#define cAf6_glbclksel_reg2_TxLineClk6mSel2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Line clock Looptime mode 1
Reg Addr   : 0x0000c
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for selecting clock source to transmit Tx line stream (EC1 Id: 0-23) is looptime or selected from 2 sources 6M (sysclk & extclk)

------------------------------------------------------------------------------*/
#define cAf6Reg_glbclklooptime_reg1_Base                                                               0x0000c
#define cAf6Reg_glbclklooptime_reg1                                                                    0x0000c
#define cAf6Reg_glbclklooptime_reg1_WidthVal                                                                32
#define cAf6Reg_glbclklooptime_reg1_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TxLineClkLoopTime1
BitField Type: RW
BitField Desc: Bit #0 for line #0. 1: Looptime Mode 0: Using TxLineClk6mSel1 for
transmit Tx line source clock.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_Bit_Start                                                0
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_Bit_End                                                 23
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_Mask                                              cBit23_0
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_Shift                                                    0
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_MaxVal                                            0xffffff
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_MinVal                                                 0x0
#define cAf6_glbclklooptime_reg1_TxLineClkLoopTime1_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Line clock Looptime mode 2
Reg Addr   : 0x0000d
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for selecting clock source to transmit Tx line stream (EC1 Id: 24-47) is looptime or selected from 2 sources(sysclk & extclk)

------------------------------------------------------------------------------*/
#define cAf6Reg_glbclklooptime_reg2_Base                                                               0x0000d
#define cAf6Reg_glbclklooptime_reg2                                                                    0x0000d
#define cAf6Reg_glbclklooptime_reg2_WidthVal                                                                32
#define cAf6Reg_glbclklooptime_reg2_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TxLineClkLoopTime2
BitField Type: RW
BitField Desc: Bit #0 for line #24. 1: Looptime Mode 0: Using TxLineClk6mSel2
for transmit Tx line source clock.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_Bit_Start                                                0
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_Bit_End                                                 23
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_Mask                                              cBit23_0
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_Shift                                                    0
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_MaxVal                                            0xffffff
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_MinVal                                                 0x0
#define cAf6_glbclklooptime_reg2_TxLineClkLoopTime2_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer RDI-L Insertion Threshold Control
Reg Addr   : 0x00009
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. The register contains two numbers

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrdiinsthr_reg_Base                                                                  0x00009
#define cAf6Reg_glbrdiinsthr_reg                                                                       0x00009
#define cAf6Reg_glbrdiinsthr_reg_WidthVal                                                                   32
#define cAf6Reg_glbrdiinsthr_reg_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: TxFrmRdiInsThr2
BitField Type: RW
BitField Desc: Threshold 2 for SDH mode
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Bit_Start                                                      8
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Bit_End                                                       12
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Mask                                                    cBit12_8
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Shift                                                          8
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_MaxVal                                                      0x1f
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_MinVal                                                       0x0
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_RstVal                                                       0x8

/*--------------------------------------
BitField Name: TxFrmRdiInsThr1
BitField Type: RW
BitField Desc: Threshold 1 for Sonet mode
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Bit_Start                                                      0
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Bit_End                                                        4
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Mask                                                     cBit4_0
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Shift                                                          0
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_MaxVal                                                      0x1f
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_MinVal                                                       0x0
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_RstVal                                                      0x14


/*------------------------------------------------------------------------------
Reg Name   : OCN Global STS Pointer Interpreter Control
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the STS Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbspi_reg_Base                                                                        0x00002
#define cAf6Reg_glbspi_reg                                                                             0x00002
#define cAf6Reg_glbspi_reg_WidthVal                                                                         32
#define cAf6Reg_glbspi_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: StsPiAdjCntSel
BitField Type: RW
BitField Desc: Select pointer adjustment counter at "OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter"
                                            1: Counter for N/P event which is transfered to PLA after Virtual FIFO module
                                            0: Counter for Pointer interpreter
BitField Bits: [31]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAdjCntSel_Mask                                                             cBit31
#define cAf6_glbspi_reg_StsPiAdjCntSel_Shift                                                                31

/*--------------------------------------
BitField Name: RxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous frames within pointer adjustments for Virtual fifo PG.
BitField Bits: [30:29]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgNorPtrThresh_Mask                                                        cBit30_29
#define cAf6_glbspi_reg_RxPgNorPtrThresh_Shift                                                              29

/*--------------------------------------
BitField Name: StsPiHoBusSel2
BitField Type: RW
BitField Desc: Select bus between SPI slice2  and HOBus slice2  for downstream
to PDH. 1: Hobus slice2 selected 0: SPI slice2 (normal) selected
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiHoBusSel2_Bit_Start                                                            29
#define cAf6_glbspi_reg_StsPiHoBusSel2_Bit_End                                                              29
#define cAf6_glbspi_reg_StsPiHoBusSel2_Mask                                                             cBit29
#define cAf6_glbspi_reg_StsPiHoBusSel2_Shift                                                                29
#define cAf6_glbspi_reg_StsPiHoBusSel2_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiHoBusSel2_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiHoBusSel2_RstVal                                                              0x0

/*--------------------------------------
BitField Name: StsPiHoBusSel1
BitField Type: RW
BitField Desc: Select bus between SPI slice1  and HOBus slice1  for downstream
to PDH. 1: Hobus slice1 selected 0: SPI slice1 (normal) selected
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiHoBusSel1_Bit_Start                                                            28
#define cAf6_glbspi_reg_StsPiHoBusSel1_Bit_End                                                              28
#define cAf6_glbspi_reg_StsPiHoBusSel1_Mask                                                             cBit28
#define cAf6_glbspi_reg_StsPiHoBusSel1_Shift                                                                28
#define cAf6_glbspi_reg_StsPiHoBusSel1_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiHoBusSel1_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiHoBusSel1_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgFlowThresh_Bit_Start                                                            24
#define cAf6_glbspi_reg_RxPgFlowThresh_Bit_End                                                              27
#define cAf6_glbspi_reg_RxPgFlowThresh_Mask                                                          cBit27_24
#define cAf6_glbspi_reg_RxPgFlowThresh_Shift                                                                24
#define cAf6_glbspi_reg_RxPgFlowThresh_MaxVal                                                              0xf
#define cAf6_glbspi_reg_RxPgFlowThresh_MinVal                                                              0x0
#define cAf6_glbspi_reg_RxPgFlowThresh_RstVal                                                              0x3

/*--------------------------------------
BitField Name: RxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a pointer increment/decrement.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgAdjThresh_Bit_Start                                                             20
#define cAf6_glbspi_reg_RxPgAdjThresh_Bit_End                                                               23
#define cAf6_glbspi_reg_RxPgAdjThresh_Mask                                                           cBit23_20
#define cAf6_glbspi_reg_RxPgAdjThresh_Shift                                                                 20
#define cAf6_glbspi_reg_RxPgAdjThresh_MaxVal                                                               0xf
#define cAf6_glbspi_reg_RxPgAdjThresh_MinVal                                                               0x0
#define cAf6_glbspi_reg_RxPgAdjThresh_RstVal                                                               0xc

/*--------------------------------------
BitField Name: StsPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when AIS state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAisAisPEn_Bit_Start                                                            18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Bit_End                                                              18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Mask                                                             cBit18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Shift                                                                18
#define cAf6_glbspi_reg_StsPiAisAisPEn_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiAisAisPEn_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiAisAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: StsPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when LOP state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiLopAisPEn_Bit_Start                                                            17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Bit_End                                                              17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Mask                                                             cBit17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Shift                                                                17
#define cAf6_glbspi_reg_StsPiLopAisPEn_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiLopAisPEn_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiLopAisPEn_RstVal                                                              0x1

/*--------------------------------------
BitField Name: StsPiMajorMode
BitField Type: RW
BitField Desc: Majority mode for detecting increment/decrement at STS pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiMajorMode_Bit_Start                                                            16
#define cAf6_glbspi_reg_StsPiMajorMode_Bit_End                                                              16
#define cAf6_glbspi_reg_StsPiMajorMode_Mask                                                             cBit16
#define cAf6_glbspi_reg_StsPiMajorMode_Shift                                                                16
#define cAf6_glbspi_reg_StsPiMajorMode_MaxVal                                                              0x1
#define cAf6_glbspi_reg_StsPiMajorMode_MinVal                                                              0x0
#define cAf6_glbspi_reg_StsPiMajorMode_RstVal                                                              0x1

/*--------------------------------------
BitField Name: StsPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Bit_Start                                                         12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Bit_End                                                           13
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Mask                                                       cBit13_12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Shift                                                             12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_MaxVal                                                           0x3
#define cAf6_glbspi_reg_StsPiNorPtrThresh_MinVal                                                           0x0
#define cAf6_glbspi_reg_StsPiNorPtrThresh_RstVal                                                           0x3

/*--------------------------------------
BitField Name: StsPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Bit_Start                                                          8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Bit_End                                                           11
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Mask                                                        cBit11_8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Shift                                                              8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_MaxVal                                                           0xf
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_MinVal                                                           0x0
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_RstVal                                                           0x8

/*--------------------------------------
BitField Name: StsPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Bit_Start                                                          4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Bit_End                                                            7
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Mask                                                         cBit7_4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Shift                                                              4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_MaxVal                                                           0xf
#define cAf6_glbspi_reg_StsPiBadPtrThresh_MinVal                                                           0x0
#define cAf6_glbspi_reg_StsPiBadPtrThresh_RstVal                                                           0x8

/*--------------------------------------
BitField Name: StsPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable STS POH defect types to downstream AIS in case of
terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM
defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for
PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiPohAisType_Bit_Start                                                            0
#define cAf6_glbspi_reg_StsPiPohAisType_Bit_End                                                              3
#define cAf6_glbspi_reg_StsPiPohAisType_Mask                                                           cBit3_0
#define cAf6_glbspi_reg_StsPiPohAisType_Shift                                                                0
#define cAf6_glbspi_reg_StsPiPohAisType_MaxVal                                                             0xf
#define cAf6_glbspi_reg_StsPiPohAisType_MinVal                                                             0x0
#define cAf6_glbspi_reg_StsPiPohAisType_RstVal                                                             0xf


/*------------------------------------------------------------------------------
Reg Name   : OCN Global VTTU Pointer Interpreter Control
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the VTTU Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbvpi_reg_Base                                                                        0x00003
#define cAf6Reg_glbvpi_reg                                                                             0x00003
#define cAf6Reg_glbvpi_reg_WidthVal                                                                         32
#define cAf6Reg_glbvpi_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: VtPiLomAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Bit_Start                                                             29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Bit_End                                                               29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Mask                                                              cBit29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Shift                                                                 29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiLomAisPEn_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiLomAisPEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiLomInvlCntMod
BitField Type: RW
BitField Desc: H4 monitoring mode. 1: Expected H4 is current frame in the
validated sequence plus one. 0: Expected H4 is the last received value plus one.
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Bit_Start                                                         28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Bit_End                                                           28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Mask                                                          cBit28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Shift                                                             28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_MaxVal                                                           0x1
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_MinVal                                                           0x0
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: VtPiLomGoodThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with validated sequence
of multi framers in LOM state for condition to entering IM state.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Bit_Start                                                         24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Bit_End                                                           27
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Mask                                                       cBit27_24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Shift                                                             24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_MaxVal                                                           0xf
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_MinVal                                                           0x0
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_RstVal                                                           0x3

/*--------------------------------------
BitField Name: VtPiLomInvlThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with invalidated
sequence of multi framers in IM state  for condition to entering LOM state.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Bit_Start                                                         20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Bit_End                                                           23
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Mask                                                       cBit23_20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Shift                                                             20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_MaxVal                                                           0xf
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_MinVal                                                           0x0
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_RstVal                                                           0x8

/*--------------------------------------
BitField Name: VtPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when AIS state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Bit_Start                                                             18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Bit_End                                                               18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Mask                                                              cBit18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Shift                                                                 18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiAisAisPEn_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiAisAisPEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOP state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Bit_Start                                                             17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Bit_End                                                               17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Mask                                                              cBit17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Shift                                                                 17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiLopAisPEn_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiLopAisPEn_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiMajorMode
BitField Type: RW
BitField Desc: Majority mode detecting increment/decrement in VTTU pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiMajorMode_Bit_Start                                                             16
#define cAf6_glbvpi_reg_VtPiMajorMode_Bit_End                                                               16
#define cAf6_glbvpi_reg_VtPiMajorMode_Mask                                                              cBit16
#define cAf6_glbvpi_reg_VtPiMajorMode_Shift                                                                 16
#define cAf6_glbvpi_reg_VtPiMajorMode_MaxVal                                                               0x1
#define cAf6_glbvpi_reg_VtPiMajorMode_MinVal                                                               0x0
#define cAf6_glbvpi_reg_VtPiMajorMode_RstVal                                                               0x1

/*--------------------------------------
BitField Name: VtPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Bit_Start                                                          12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Bit_End                                                            13
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Mask                                                        cBit13_12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Shift                                                              12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_MaxVal                                                            0x3
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_MinVal                                                            0x0
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_RstVal                                                            0x3

/*--------------------------------------
BitField Name: VtPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Bit_Start                                                           8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Bit_End                                                            11
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Mask                                                         cBit11_8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Shift                                                               8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_MaxVal                                                            0xf
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_MinVal                                                            0x0
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_RstVal                                                            0x8

/*--------------------------------------
BitField Name: VtPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Bit_Start                                                           4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Bit_End                                                             7
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Mask                                                          cBit7_4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Shift                                                               4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_MaxVal                                                            0xf
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_MinVal                                                            0x0
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_RstVal                                                            0x8

/*--------------------------------------
BitField Name: VtPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable VTTU POH defect types to downstream AIS in case of
terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-
equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiPohAisType_Bit_Start                                                             0
#define cAf6_glbvpi_reg_VtPiPohAisType_Bit_End                                                               3
#define cAf6_glbvpi_reg_VtPiPohAisType_Mask                                                            cBit3_0
#define cAf6_glbvpi_reg_VtPiPohAisType_Shift                                                                 0
#define cAf6_glbvpi_reg_VtPiPohAisType_MaxVal                                                              0xf
#define cAf6_glbvpi_reg_VtPiPohAisType_MinVal                                                              0x0
#define cAf6_glbvpi_reg_VtPiPohAisType_RstVal                                                              0xf


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Pointer Generator Control
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Pointer Generator

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtpg_reg_Base                                                                        0x00004
#define cAf6Reg_glbtpg_reg                                                                             0x00004
#define cAf6Reg_glbtpg_reg_WidthVal                                                                         32
#define cAf6Reg_glbtpg_reg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: TxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames to make a condition of pointer adjustments.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Bit_Start                                                           8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Bit_End                                                             9
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Mask                                                          cBit9_8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Shift                                                               8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_MaxVal                                                            0x3
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_MinVal                                                            0x0
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_RstVal                                                            0x3

/*--------------------------------------
BitField Name: TxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer
of TxFiFo.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgFlowThresh_Bit_Start                                                             4
#define cAf6_glbtpg_reg_TxPgFlowThresh_Bit_End                                                               7
#define cAf6_glbtpg_reg_TxPgFlowThresh_Mask                                                            cBit7_4
#define cAf6_glbtpg_reg_TxPgFlowThresh_Shift                                                                 4
#define cAf6_glbtpg_reg_TxPgFlowThresh_MaxVal                                                              0xf
#define cAf6_glbtpg_reg_TxPgFlowThresh_MinVal                                                              0x0
#define cAf6_glbtpg_reg_TxPgFlowThresh_RstVal                                                              0x3

/*--------------------------------------
BitField Name: TxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a condition of pointer
increment/decrement.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgAdjThresh_Bit_Start                                                              0
#define cAf6_glbtpg_reg_TxPgAdjThresh_Bit_End                                                                3
#define cAf6_glbtpg_reg_TxPgAdjThresh_Mask                                                             cBit3_0
#define cAf6_glbtpg_reg_TxPgAdjThresh_Shift                                                                  0
#define cAf6_glbtpg_reg_TxPgAdjThresh_MaxVal                                                               0xf
#define cAf6_glbtpg_reg_TxPgAdjThresh_MinVal                                                               0x0
#define cAf6_glbtpg_reg_TxPgAdjThresh_RstVal                                                               0xd


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Loopback Control
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for loopback modes at OCN block

------------------------------------------------------------------------------*/
#define cAf6Reg_glbloop_reg_Base                                                                       0x00005
#define cAf6Reg_glbloop_reg                                                                            0x00005
#define cAf6Reg_glbloop_reg_WidthVal                                                                        32
#define cAf6Reg_glbloop_reg_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: GlbLbEc1Remote
BitField Type: RW
BitField Desc: Remote loopback for 48 EC1 lines.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glbloop_reg_GlbLbEc1Remote_Bit_Start                                                            3
#define cAf6_glbloop_reg_GlbLbEc1Remote_Bit_End                                                              3
#define cAf6_glbloop_reg_GlbLbEc1Remote_Mask                                                             cBit3
#define cAf6_glbloop_reg_GlbLbEc1Remote_Shift                                                                3
#define cAf6_glbloop_reg_GlbLbEc1Remote_MaxVal                                                             0x1
#define cAf6_glbloop_reg_GlbLbEc1Remote_MinVal                                                             0x0
#define cAf6_glbloop_reg_GlbLbEc1Remote_RstVal                                                             0x0

/*--------------------------------------
BitField Name: GlbLbEc1Local
BitField Type: RW
BitField Desc: Local loopback for 48 EC1 lines .
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbloop_reg_GlbLbEc1Local_Bit_Start                                                             2
#define cAf6_glbloop_reg_GlbLbEc1Local_Bit_End                                                               2
#define cAf6_glbloop_reg_GlbLbEc1Local_Mask                                                              cBit2
#define cAf6_glbloop_reg_GlbLbEc1Local_Shift                                                                 2
#define cAf6_glbloop_reg_GlbLbEc1Local_MaxVal                                                              0x1
#define cAf6_glbloop_reg_GlbLbEc1Local_MinVal                                                              0x0
#define cAf6_glbloop_reg_GlbLbEc1Local_RstVal                                                              0x0

/*--------------------------------------
BitField Name: GlbLbHoLocal
BitField Type: RW
BitField Desc: Local loopback for HoBus.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbloop_reg_GlbLbHoLocal_Bit_Start                                                              1
#define cAf6_glbloop_reg_GlbLbHoLocal_Bit_End                                                                1
#define cAf6_glbloop_reg_GlbLbHoLocal_Mask                                                               cBit1
#define cAf6_glbloop_reg_GlbLbHoLocal_Shift                                                                  1
#define cAf6_glbloop_reg_GlbLbHoLocal_MaxVal                                                               0x1
#define cAf6_glbloop_reg_GlbLbHoLocal_MinVal                                                               0x0
#define cAf6_glbloop_reg_GlbLbHoLocal_RstVal                                                               0x0

/*--------------------------------------
BitField Name: GlbLbLoLocal
BitField Type: RW
BitField Desc: Local loopback for LoBus.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbloop_reg_GlbLbLoLocal_Bit_Start                                                              0
#define cAf6_glbloop_reg_GlbLbLoLocal_Bit_End                                                                0
#define cAf6_glbloop_reg_GlbLbLoLocal_Mask                                                               cBit0
#define cAf6_glbloop_reg_GlbLbLoLocal_Shift                                                                  0
#define cAf6_glbloop_reg_GlbLbLoLocal_MaxVal                                                               0x1
#define cAf6_glbloop_reg_GlbLbLoLocal_MinVal                                                               0x0
#define cAf6_glbloop_reg_GlbLbLoLocal_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global select path DS3 over SDH config 1
Reg Addr   : 0x00010
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for selecting path DS3 over SDH (STS1 Id: 0-23) at SPI

------------------------------------------------------------------------------*/
#define cAf6Reg_glbds3osdh_reg1_Base                                                                   0x00010
#define cAf6Reg_glbds3osdh_reg1                                                                        0x00010
#define cAf6Reg_glbds3osdh_reg1_WidthVal                                                                    32
#define cAf6Reg_glbds3osdh_reg1_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: SpiDs3overSdhSel1
BitField Type: RW
BitField Desc: Bit #0 for STS1 #0. 1: Path  DS3 over SDH selected. 0: Normal
path.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_Bit_Start                                                     0
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_Bit_End                                                      23
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_Mask                                                   cBit23_0
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_Shift                                                         0
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_MaxVal                                                 0xffffff
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_MinVal                                                      0x0
#define cAf6_glbds3osdh_reg1_SpiDs3overSdhSel1_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global select path DS3 over SDH config 2
Reg Addr   : 0x00011
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for selecting path DS3 over SDH (STS1 Id: 24-47) at SPI

------------------------------------------------------------------------------*/
#define cAf6Reg_glbds3osdh_reg2_Base                                                                   0x00011
#define cAf6Reg_glbds3osdh_reg2                                                                        0x00011
#define cAf6Reg_glbds3osdh_reg2_WidthVal                                                                    32
#define cAf6Reg_glbds3osdh_reg2_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: SpiDs3overSdhSel2
BitField Type: RW
BitField Desc: Bit #0 for STS1 #24. 1: Path  DS3 over SDH selected. 0: Normal
path.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_Bit_Start                                                     0
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_Bit_End                                                      23
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_Mask                                                   cBit23_0
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_Shift                                                         0
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_MaxVal                                                 0xffffff
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_MinVal                                                      0x0
#define cAf6_glbds3osdh_reg2_SpiDs3overSdhSel2_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Interpreter Per Channel Control
Reg Addr   : 0x22000 - 0x22e2f
Reg Formula: 0x22000 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.
Backdoor		: irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spiramctl_Base                                                                         0x22000
#define cAf6Reg_spiramctl(SliceId, StsId)                                      (0x22000+512*(SliceId)+(StsId))
#define cAf6Reg_spiramctl_WidthVal                                                                          32
#define cAf6Reg_spiramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: StsPiChkLom
BitField Type: RW
BitField Desc: Enable/disable LOM checking. This field will be set to 1 when
payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_spiramctl_StsPiChkLom_Bit_Start                                                                14
#define cAf6_spiramctl_StsPiChkLom_Bit_End                                                                  14
#define cAf6_spiramctl_StsPiChkLom_Mask                                                                 cBit14
#define cAf6_spiramctl_StsPiChkLom_Shift                                                                    14
#define cAf6_spiramctl_StsPiChkLom_MaxVal                                                                  0x1
#define cAf6_spiramctl_StsPiChkLom_MinVal                                                                  0x0
#define cAf6_spiramctl_StsPiChkLom_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_spiramctl_StsPiSSDetPatt_Bit_Start                                                             12
#define cAf6_spiramctl_StsPiSSDetPatt_Bit_End                                                               13
#define cAf6_spiramctl_StsPiSSDetPatt_Mask                                                           cBit13_12
#define cAf6_spiramctl_StsPiSSDetPatt_Shift                                                                 12
#define cAf6_spiramctl_StsPiSSDetPatt_MaxVal                                                               0x3
#define cAf6_spiramctl_StsPiSSDetPatt_MinVal                                                               0x0
#define cAf6_spiramctl_StsPiSSDetPatt_RstVal                                                               0x0

/*--------------------------------------
BitField Name: StsPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [11]
--------------------------------------*/
#define cAf6_spiramctl_StsPiAisFrc_Bit_Start                                                                11
#define cAf6_spiramctl_StsPiAisFrc_Bit_End                                                                  11
#define cAf6_spiramctl_StsPiAisFrc_Mask                                                                 cBit11
#define cAf6_spiramctl_StsPiAisFrc_Shift                                                                    11
#define cAf6_spiramctl_StsPiAisFrc_MaxVal                                                                  0x1
#define cAf6_spiramctl_StsPiAisFrc_MinVal                                                                  0x0
#define cAf6_spiramctl_StsPiAisFrc_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in STSPI state machine. 1: Enable
0: Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_spiramctl_StsPiSSDetEn_Bit_Start                                                               10
#define cAf6_spiramctl_StsPiSSDetEn_Bit_End                                                                 10
#define cAf6_spiramctl_StsPiSSDetEn_Mask                                                                cBit10
#define cAf6_spiramctl_StsPiSSDetEn_Shift                                                                   10
#define cAf6_spiramctl_StsPiSSDetEn_MaxVal                                                                 0x1
#define cAf6_spiramctl_StsPiSSDetEn_MinVal                                                                 0x0
#define cAf6_spiramctl_StsPiSSDetEn_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: StsPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_spiramctl_StsPiAdjRule_Bit_Start                                                                9
#define cAf6_spiramctl_StsPiAdjRule_Bit_End                                                                  9
#define cAf6_spiramctl_StsPiAdjRule_Mask                                                                 cBit9
#define cAf6_spiramctl_StsPiAdjRule_Shift                                                                    9
#define cAf6_spiramctl_StsPiAdjRule_MaxVal                                                                 0x1
#define cAf6_spiramctl_StsPiAdjRule_MinVal                                                                 0x0
#define cAf6_spiramctl_StsPiAdjRule_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Generator Per Channel Control
Reg Addr   : 0x23000 - 0x23e2f
Reg Formula: 0x23000 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
Each register is used to configure for STS pointer Generator engines.
Backdoor		: itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spgramctl_Base                                                                         0x23000
#define cAf6Reg_spgramctl(SliceId, StsId)                                      (0x23000+512*(SliceId)+(StsId))
#define cAf6Reg_spgramctl_WidthVal                                                                          32
#define cAf6Reg_spgramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: StsPiB3BipErrFrc
BitField Type: RW
BitField Desc: Forcing B3 Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_spgramctl_StsPiB3BipErrFrc_Bit_Start                                                            7
#define cAf6_spgramctl_StsPiB3BipErrFrc_Bit_End                                                              7
#define cAf6_spgramctl_StsPiB3BipErrFrc_Mask                                                             cBit7
#define cAf6_spgramctl_StsPiB3BipErrFrc_Shift                                                                7
#define cAf6_spgramctl_StsPiB3BipErrFrc_MaxVal                                                             0x1
#define cAf6_spgramctl_StsPiB3BipErrFrc_MinVal                                                             0x0
#define cAf6_spgramctl_StsPiB3BipErrFrc_RstVal                                                             0x0

/*--------------------------------------
BitField Name: StsPiLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_spgramctl_StsPiLopFrc_Bit_Start                                                                 6
#define cAf6_spgramctl_StsPiLopFrc_Bit_End                                                                   6
#define cAf6_spgramctl_StsPiLopFrc_Mask                                                                  cBit6
#define cAf6_spgramctl_StsPiLopFrc_Shift                                                                     6
#define cAf6_spgramctl_StsPiLopFrc_MaxVal                                                                  0x1
#define cAf6_spgramctl_StsPiLopFrc_MinVal                                                                  0x0
#define cAf6_spgramctl_StsPiLopFrc_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiUeqFrc
BitField Type: RW
BitField Desc: Forcing SFM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_spgramctl_StsPiUeqFrc_Bit_Start                                                                 5
#define cAf6_spgramctl_StsPiUeqFrc_Bit_End                                                                   5
#define cAf6_spgramctl_StsPiUeqFrc_Mask                                                                  cBit5
#define cAf6_spgramctl_StsPiUeqFrc_Shift                                                                     5
#define cAf6_spgramctl_StsPiUeqFrc_MaxVal                                                                  0x1
#define cAf6_spgramctl_StsPiUeqFrc_MinVal                                                                  0x0
#define cAf6_spgramctl_StsPiUeqFrc_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_spgramctl_StsPiAisFrc_Bit_Start                                                                 4
#define cAf6_spgramctl_StsPiAisFrc_Bit_End                                                                   4
#define cAf6_spgramctl_StsPiAisFrc_Mask                                                                  cBit4
#define cAf6_spgramctl_StsPiAisFrc_Shift                                                                     4
#define cAf6_spgramctl_StsPiAisFrc_MaxVal                                                                  0x1
#define cAf6_spgramctl_StsPiAisFrc_MinVal                                                                  0x0
#define cAf6_spgramctl_StsPiAisFrc_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: StsPiSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_spgramctl_StsPiSSInsPatt_Bit_Start                                                              2
#define cAf6_spgramctl_StsPiSSInsPatt_Bit_End                                                                3
#define cAf6_spgramctl_StsPiSSInsPatt_Mask                                                             cBit3_2
#define cAf6_spgramctl_StsPiSSInsPatt_Shift                                                                  2
#define cAf6_spgramctl_StsPiSSInsPatt_MaxVal                                                               0x3
#define cAf6_spgramctl_StsPiSSInsPatt_MinVal                                                               0x0
#define cAf6_spgramctl_StsPiSSInsPatt_RstVal                                                               0x0

/*--------------------------------------
BitField Name: StsPiSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_spgramctl_StsPiSSInsEn_Bit_Start                                                                1
#define cAf6_spgramctl_StsPiSSInsEn_Bit_End                                                                  1
#define cAf6_spgramctl_StsPiSSInsEn_Mask                                                                 cBit1
#define cAf6_spgramctl_StsPiSSInsEn_Shift                                                                    1
#define cAf6_spgramctl_StsPiSSInsEn_MaxVal                                                                 0x1
#define cAf6_spgramctl_StsPiSSInsEn_MinVal                                                                 0x0
#define cAf6_spgramctl_StsPiSSInsEn_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: StsPiPohIns
BitField Type: RW
BitField Desc: Enable/disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_spgramctl_StsPiPohIns_Bit_Start                                                                 0
#define cAf6_spgramctl_StsPiPohIns_Bit_End                                                                   0
#define cAf6_spgramctl_StsPiPohIns_Mask                                                                  cBit0
#define cAf6_spgramctl_StsPiPohIns_Shift                                                                     0
#define cAf6_spgramctl_StsPiPohIns_MaxVal                                                                  0x1
#define cAf6_spgramctl_StsPiPohIns_MinVal                                                                  0x0
#define cAf6_spgramctl_StsPiPohIns_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN RXPP Per STS payload Control
Reg Addr   : 0x40000 - 0x5402f
Reg Formula: 0x40000 + 16384*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
Each register is used to configure VT payload mode per STS.
Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_demramctl_Base                                                                         0x40000
#define cAf6Reg_demramctl(SliceId, StsId)                                    (0x40000+16384*(SliceId)+(StsId))
#define cAf6Reg_demramctl_WidthVal                                                                          32
#define cAf6Reg_demramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: PiDemStsDs3oTu3Cep
BitField Type: RW
BitField Desc: Config to enable path DS3 over TU3 CEP,
               MUST set bit enable of registers "OCN Global select path DS3 over SDH config 1/2"
               to enable for path DS3 over TU3 CEP.
BitField Bits: [17]
--------------------------------------*/
#define cAf6_demramctl_PiDemStsDs3oTu3Cep_Mask                                                                cBit17
#define cAf6_demramctl_PiDemStsDs3oTu3Cep_Shift                                                                   17

/*--------------------------------------
BitField Name: PiDemStsTerm
BitField Type: RW
BitField Desc: Enable to terminate the related STS/VC. It means that STS POH
defects related to the STS/VC to generate AIS to downstream. Must be set to 1.
1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_demramctl_PiDemStsTerm_Bit_Start                                                               16
#define cAf6_demramctl_PiDemStsTerm_Bit_End                                                                 16
#define cAf6_demramctl_PiDemStsTerm_Mask                                                                cBit16
#define cAf6_demramctl_PiDemStsTerm_Shift                                                                   16
#define cAf6_demramctl_PiDemStsTerm_MaxVal                                                                 0x1
#define cAf6_demramctl_PiDemStsTerm_MinVal                                                                 0x0
#define cAf6_demramctl_PiDemStsTerm_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: PiDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_demramctl_PiDemSpeType_Bit_Start                                                               14
#define cAf6_demramctl_PiDemSpeType_Bit_End                                                                 15
#define cAf6_demramctl_PiDemSpeType_Mask                                                             cBit15_14
#define cAf6_demramctl_PiDemSpeType_Shift                                                                   14
#define cAf6_demramctl_PiDemSpeType_MaxVal                                                                 0x3
#define cAf6_demramctl_PiDemSpeType_MinVal                                                                 0x0
#define cAf6_demramctl_PiDemSpeType_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: PiDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug26Type_Bit_Start                                                             12
#define cAf6_demramctl_PiDemTug26Type_Bit_End                                                               13
#define cAf6_demramctl_PiDemTug26Type_Mask                                                           cBit13_12
#define cAf6_demramctl_PiDemTug26Type_Shift                                                                 12
#define cAf6_demramctl_PiDemTug26Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug26Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug26Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug25Type_Bit_Start                                                             10
#define cAf6_demramctl_PiDemTug25Type_Bit_End                                                               11
#define cAf6_demramctl_PiDemTug25Type_Mask                                                           cBit11_10
#define cAf6_demramctl_PiDemTug25Type_Shift                                                                 10
#define cAf6_demramctl_PiDemTug25Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug25Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug25Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug24Type_Bit_Start                                                              8
#define cAf6_demramctl_PiDemTug24Type_Bit_End                                                                9
#define cAf6_demramctl_PiDemTug24Type_Mask                                                             cBit9_8
#define cAf6_demramctl_PiDemTug24Type_Shift                                                                  8
#define cAf6_demramctl_PiDemTug24Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug24Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug24Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug23Type_Bit_Start                                                              6
#define cAf6_demramctl_PiDemTug23Type_Bit_End                                                                7
#define cAf6_demramctl_PiDemTug23Type_Mask                                                             cBit7_6
#define cAf6_demramctl_PiDemTug23Type_Shift                                                                  6
#define cAf6_demramctl_PiDemTug23Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug23Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug23Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug22Type_Bit_Start                                                              4
#define cAf6_demramctl_PiDemTug22Type_Bit_End                                                                5
#define cAf6_demramctl_PiDemTug22Type_Mask                                                             cBit5_4
#define cAf6_demramctl_PiDemTug22Type_Shift                                                                  4
#define cAf6_demramctl_PiDemTug22Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug22Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug22Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug21Type_Bit_Start                                                              2
#define cAf6_demramctl_PiDemTug21Type_Bit_End                                                                3
#define cAf6_demramctl_PiDemTug21Type_Mask                                                             cBit3_2
#define cAf6_demramctl_PiDemTug21Type_Shift                                                                  2
#define cAf6_demramctl_PiDemTug21Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug21Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug21Type_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PiDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug20Type_Bit_Start                                                              0
#define cAf6_demramctl_PiDemTug20Type_Bit_End                                                                1
#define cAf6_demramctl_PiDemTug20Type_Mask                                                             cBit1_0
#define cAf6_demramctl_PiDemTug20Type_Shift                                                                  0
#define cAf6_demramctl_PiDemTug20Type_MaxVal                                                               0x3
#define cAf6_demramctl_PiDemTug20Type_MinVal                                                               0x0
#define cAf6_demramctl_PiDemTug20Type_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Interpreter Per Channel Control
Reg Addr   : 0x40800 - 0x54fff
Reg Formula: 0x40800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer interpreter engines.
Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpiramctl_Base                                                                         0x40800
#define cAf6Reg_vpiramctl(SliceId, StsId, VtgId, VtId)                (0x40800+16384*(SliceId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_vpiramctl_WidthVal                                                                          32
#define cAf6Reg_vpiramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: VtPiLoTerm
BitField Type: RW
BitField Desc: Enable to terminate the related VTTU. It means that VTTU POH
defects related to the VTTU to generate AIS to downstream.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiLoTerm_Bit_Start                                                                  5
#define cAf6_vpiramctl_VtPiLoTerm_Bit_End                                                                    5
#define cAf6_vpiramctl_VtPiLoTerm_Mask                                                                   cBit5
#define cAf6_vpiramctl_VtPiLoTerm_Shift                                                                      5
#define cAf6_vpiramctl_VtPiLoTerm_MaxVal                                                                   0x1
#define cAf6_vpiramctl_VtPiLoTerm_MinVal                                                                   0x0
#define cAf6_vpiramctl_VtPiLoTerm_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPiAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiAisFrc_Bit_Start                                                                  4
#define cAf6_vpiramctl_VtPiAisFrc_Bit_End                                                                    4
#define cAf6_vpiramctl_VtPiAisFrc_Mask                                                                   cBit4
#define cAf6_vpiramctl_VtPiAisFrc_Shift                                                                      4
#define cAf6_vpiramctl_VtPiAisFrc_MaxVal                                                                   0x1
#define cAf6_vpiramctl_VtPiAisFrc_MinVal                                                                   0x0
#define cAf6_vpiramctl_VtPiAisFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiSSDetPatt_Bit_Start                                                               2
#define cAf6_vpiramctl_VtPiSSDetPatt_Bit_End                                                                 3
#define cAf6_vpiramctl_VtPiSSDetPatt_Mask                                                              cBit3_2
#define cAf6_vpiramctl_VtPiSSDetPatt_Shift                                                                   2
#define cAf6_vpiramctl_VtPiSSDetPatt_MaxVal                                                                0x3
#define cAf6_vpiramctl_VtPiSSDetPatt_MinVal                                                                0x0
#define cAf6_vpiramctl_VtPiSSDetPatt_RstVal                                                                0x0

/*--------------------------------------
BitField Name: VtPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in PI State Machine. 1: Enable 0:
Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiSSDetEn_Bit_Start                                                                 1
#define cAf6_vpiramctl_VtPiSSDetEn_Bit_End                                                                   1
#define cAf6_vpiramctl_VtPiSSDetEn_Mask                                                                  cBit1
#define cAf6_vpiramctl_VtPiSSDetEn_Shift                                                                     1
#define cAf6_vpiramctl_VtPiSSDetEn_MaxVal                                                                  0x1
#define cAf6_vpiramctl_VtPiSSDetEn_MinVal                                                                  0x0
#define cAf6_vpiramctl_VtPiSSDetEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: VtPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpiramctl_VtPiAdjRule_Bit_Start                                                                 0
#define cAf6_vpiramctl_VtPiAdjRule_Bit_End                                                                   0
#define cAf6_vpiramctl_VtPiAdjRule_Mask                                                                  cBit0
#define cAf6_vpiramctl_VtPiAdjRule_Shift                                                                     0
#define cAf6_vpiramctl_VtPiAdjRule_MaxVal                                                                  0x1
#define cAf6_vpiramctl_VtPiAdjRule_MinVal                                                                  0x0
#define cAf6_vpiramctl_VtPiAdjRule_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TXPP Per STS Multiplexing Control
Reg Addr   : 0x60000 - 0x7402f
Reg Formula: 0x60000 + 16384*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
Each register is used to configure VT payload mode per STS at Tx pointer generator.
Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_pgdemramctl_Base                                                                       0x60000
#define cAf6Reg_pgdemramctl(SliceId, StsId)                                  (0x60000+16384*(SliceId)+(StsId))
#define cAf6Reg_pgdemramctl_WidthVal                                                                        32
#define cAf6Reg_pgdemramctl_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: PgDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemSpeType_Bit_Start                                                             14
#define cAf6_pgdemramctl_PgDemSpeType_Bit_End                                                               15
#define cAf6_pgdemramctl_PgDemSpeType_Mask                                                           cBit15_14
#define cAf6_pgdemramctl_PgDemSpeType_Shift                                                                 14
#define cAf6_pgdemramctl_PgDemSpeType_MaxVal                                                               0x3
#define cAf6_pgdemramctl_PgDemSpeType_MinVal                                                               0x0
#define cAf6_pgdemramctl_PgDemSpeType_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PgDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug26Type_Bit_Start                                                           12
#define cAf6_pgdemramctl_PgDemTug26Type_Bit_End                                                             13
#define cAf6_pgdemramctl_PgDemTug26Type_Mask                                                         cBit13_12
#define cAf6_pgdemramctl_PgDemTug26Type_Shift                                                               12
#define cAf6_pgdemramctl_PgDemTug26Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug26Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug26Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug25Type_Bit_Start                                                           10
#define cAf6_pgdemramctl_PgDemTug25Type_Bit_End                                                             11
#define cAf6_pgdemramctl_PgDemTug25Type_Mask                                                         cBit11_10
#define cAf6_pgdemramctl_PgDemTug25Type_Shift                                                               10
#define cAf6_pgdemramctl_PgDemTug25Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug25Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug25Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug24Type_Bit_Start                                                            8
#define cAf6_pgdemramctl_PgDemTug24Type_Bit_End                                                              9
#define cAf6_pgdemramctl_PgDemTug24Type_Mask                                                           cBit9_8
#define cAf6_pgdemramctl_PgDemTug24Type_Shift                                                                8
#define cAf6_pgdemramctl_PgDemTug24Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug24Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug24Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug23Type_Bit_Start                                                            6
#define cAf6_pgdemramctl_PgDemTug23Type_Bit_End                                                              7
#define cAf6_pgdemramctl_PgDemTug23Type_Mask                                                           cBit7_6
#define cAf6_pgdemramctl_PgDemTug23Type_Shift                                                                6
#define cAf6_pgdemramctl_PgDemTug23Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug23Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug23Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug22Type_Bit_Start                                                            4
#define cAf6_pgdemramctl_PgDemTug22Type_Bit_End                                                              5
#define cAf6_pgdemramctl_PgDemTug22Type_Mask                                                           cBit5_4
#define cAf6_pgdemramctl_PgDemTug22Type_Shift                                                                4
#define cAf6_pgdemramctl_PgDemTug22Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug22Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug22Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug21Type_Bit_Start                                                            2
#define cAf6_pgdemramctl_PgDemTug21Type_Bit_End                                                              3
#define cAf6_pgdemramctl_PgDemTug21Type_Mask                                                           cBit3_2
#define cAf6_pgdemramctl_PgDemTug21Type_Shift                                                                2
#define cAf6_pgdemramctl_PgDemTug21Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug21Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug21Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PgDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug20Type_Bit_Start                                                            0
#define cAf6_pgdemramctl_PgDemTug20Type_Bit_End                                                              1
#define cAf6_pgdemramctl_PgDemTug20Type_Mask                                                           cBit1_0
#define cAf6_pgdemramctl_PgDemTug20Type_Shift                                                                0
#define cAf6_pgdemramctl_PgDemTug20Type_MaxVal                                                             0x3
#define cAf6_pgdemramctl_PgDemTug20Type_MinVal                                                             0x0
#define cAf6_pgdemramctl_PgDemTug20Type_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Generator Per Channel Control
Reg Addr   : 0x60800 - 0x74fff
Reg Formula: 0x60800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer Generator engines.
Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpgramctl_Base                                                                         0x60800
#define cAf6Reg_vpgramctl(SliceId, StsId, VtgId, VtId)                (0x60800+16384*(SliceId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_vpgramctl_WidthVal                                                                          32
#define cAf6Reg_vpgramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: VtPgBipErrFrc
BitField Type: RW
BitField Desc: Forcing Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgBipErrFrc_Bit_Start                                                               7
#define cAf6_vpgramctl_VtPgBipErrFrc_Bit_End                                                                 7
#define cAf6_vpgramctl_VtPgBipErrFrc_Mask                                                                cBit7
#define cAf6_vpgramctl_VtPgBipErrFrc_Shift                                                                   7
#define cAf6_vpgramctl_VtPgBipErrFrc_MaxVal                                                                0x1
#define cAf6_vpgramctl_VtPgBipErrFrc_MinVal                                                                0x0
#define cAf6_vpgramctl_VtPgBipErrFrc_RstVal                                                                0x0

/*--------------------------------------
BitField Name: VtPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgLopFrc_Bit_Start                                                                  6
#define cAf6_vpgramctl_VtPgLopFrc_Bit_End                                                                    6
#define cAf6_vpgramctl_VtPgLopFrc_Mask                                                                   cBit6
#define cAf6_vpgramctl_VtPgLopFrc_Shift                                                                      6
#define cAf6_vpgramctl_VtPgLopFrc_MaxVal                                                                   0x1
#define cAf6_vpgramctl_VtPgLopFrc_MinVal                                                                   0x0
#define cAf6_vpgramctl_VtPgLopFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgUeqFrc
BitField Type: RW
BitField Desc: Forcing SFM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgUeqFrc_Bit_Start                                                                  5
#define cAf6_vpgramctl_VtPgUeqFrc_Bit_End                                                                    5
#define cAf6_vpgramctl_VtPgUeqFrc_Mask                                                                   cBit5
#define cAf6_vpgramctl_VtPgUeqFrc_Shift                                                                      5
#define cAf6_vpgramctl_VtPgUeqFrc_MaxVal                                                                   0x1
#define cAf6_vpgramctl_VtPgUeqFrc_MinVal                                                                   0x0
#define cAf6_vpgramctl_VtPgUeqFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgAisFrc
BitField Type: RW
BitField Desc: Forcing SFM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgAisFrc_Bit_Start                                                                  4
#define cAf6_vpgramctl_VtPgAisFrc_Bit_End                                                                    4
#define cAf6_vpgramctl_VtPgAisFrc_Mask                                                                   cBit4
#define cAf6_vpgramctl_VtPgAisFrc_Shift                                                                      4
#define cAf6_vpgramctl_VtPgAisFrc_MaxVal                                                                   0x1
#define cAf6_vpgramctl_VtPgAisFrc_MinVal                                                                   0x0
#define cAf6_vpgramctl_VtPgAisFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to Pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsPatt_Bit_Start                                                               2
#define cAf6_vpgramctl_VtPgSSInsPatt_Bit_End                                                                 3
#define cAf6_vpgramctl_VtPgSSInsPatt_Mask                                                              cBit3_2
#define cAf6_vpgramctl_VtPgSSInsPatt_Shift                                                                   2
#define cAf6_vpgramctl_VtPgSSInsPatt_MaxVal                                                                0x3
#define cAf6_vpgramctl_VtPgSSInsPatt_MinVal                                                                0x0
#define cAf6_vpgramctl_VtPgSSInsPatt_RstVal                                                                0x0

/*--------------------------------------
BitField Name: VtPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsEn_Bit_Start                                                                 1
#define cAf6_vpgramctl_VtPgSSInsEn_Bit_End                                                                   1
#define cAf6_vpgramctl_VtPgSSInsEn_Mask                                                                  cBit1
#define cAf6_vpgramctl_VtPgSSInsEn_Shift                                                                     1
#define cAf6_vpgramctl_VtPgSSInsEn_MaxVal                                                                  0x1
#define cAf6_vpgramctl_VtPgSSInsEn_MinVal                                                                  0x0
#define cAf6_vpgramctl_VtPgSSInsEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: VtPgPohIns
BitField Type: RW
BitField Desc: Enable/ disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgPohIns_Bit_Start                                                                  0
#define cAf6_vpgramctl_VtPgPohIns_Bit_End                                                                    0
#define cAf6_vpgramctl_VtPgPohIns_Mask                                                                   cBit0
#define cAf6_vpgramctl_VtPgPohIns_Shift                                                                      0
#define cAf6_vpgramctl_VtPgPohIns_MaxVal                                                                   0x1
#define cAf6_vpgramctl_VtPgPohIns_MinVal                                                                   0x0
#define cAf6_vpgramctl_VtPgPohIns_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Interrupt Status
Reg Addr   : 0x22140 - 0x22f6f
Reg Formula: 0x22140 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%
Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstkram_Base                                                                     0x22140
#define cAf6Reg_upstschstkram(SliceId, StsId)                                  (0x22140+512*(SliceId)+(StsId))
#define cAf6Reg_upstschstkram_WidthVal                                                                      32
#define cAf6Reg_upstschstkram_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: StsPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Bit_Start                                                      4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Bit_End                                                        4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Mask                                                       cBit4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_Shift                                                          4
#define cAf6_upstschstkram_StsPiStsNewDetIntr_MaxVal                                                       0x1
#define cAf6_upstschstkram_StsPiStsNewDetIntr_MinVal                                                       0x0
#define cAf6_upstschstkram_StsPiStsNewDetIntr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: StsPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at STS/VC pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsNdfIntr_Bit_Start                                                         3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Bit_End                                                           3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Mask                                                          cBit3
#define cAf6_upstschstkram_StsPiStsNdfIntr_Shift                                                             3
#define cAf6_upstschstkram_StsPiStsNdfIntr_MaxVal                                                          0x1
#define cAf6_upstschstkram_StsPiStsNdfIntr_MinVal                                                          0x0
#define cAf6_upstschstkram_StsPiStsNdfIntr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: StsPiCepUneqStatePChgIntr
BitField Type: W1C
BitField Desc: Set to 1  while there is change in CEP Un-equip Path in the
related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm
Current Status register of the related STS/VC to know the STS/VC whether in CEP
Un-equip Path state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Bit_Start                                               2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Bit_End                                                 2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Mask                                                cBit2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_Shift                                                   2
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_MaxVal                                                0x1
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_MinVal                                                0x0
#define cAf6_upstschstkram_StsPiCepUneqStatePChgIntr_RstVal                                                0x0

/*--------------------------------------
BitField Name: StsPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set to 1 while there is change in AIS state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in AIS state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Bit_Start                                                 1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Bit_End                                                   1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Mask                                                  cBit1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_Shift                                                     1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_MaxVal                                                  0x1
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_MinVal                                                  0x0
#define cAf6_upstschstkram_StsPiStsAISStateChgIntr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: StsPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in LOP state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Bit_Start                                                 0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Bit_End                                                   0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Mask                                                  cBit0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_Shift                                                     0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_MaxVal                                                  0x1
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_MinVal                                                  0x0
#define cAf6_upstschstkram_StsPiStsLopStateChgIntr_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Current Status
Reg Addr   : 0x22180 - 0x22faf
Reg Formula: 0x22180 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This is the per Alarm current status of STS/VC pointer interpreter.  %%
Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstaram_Base                                                                     0x22180
#define cAf6Reg_upstschstaram(SliceId, StsId)                                  (0x22180+512*(SliceId)+(StsId))
#define cAf6Reg_upstschstaram_WidthVal                                                                      32
#define cAf6Reg_upstschstaram_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: StsPiStsCepUneqPCurStatus
BitField Type: RO
BitField Desc: CEP Un-eqip Path current status in the related STS/VC. When it
changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the
OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Bit_Start                                               2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Bit_End                                                 2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Mask                                                cBit2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_Shift                                                   2
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_MaxVal                                                0x1
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_MinVal                                                0x0
#define cAf6_upstschstaram_StsPiStsCepUneqPCurStatus_RstVal                                                0x0

/*--------------------------------------
BitField Name: StsPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per
Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Bit_Start                                                    1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Bit_End                                                      1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Mask                                                     cBit1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_Shift                                                        1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_MaxVal                                                     0x1
#define cAf6_upstschstaram_StsPiStsAisCurStatus_MinVal                                                     0x0
#define cAf6_upstschstaram_StsPiStsAisCurStatus_RstVal                                                     0x0

/*--------------------------------------
BitField Name: StsPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per
Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Bit_Start                                                    0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Bit_End                                                      0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Mask                                                     cBit0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_Shift                                                        0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_MaxVal                                                     0x1
#define cAf6_upstschstaram_StsPiStsLopCurStatus_MinVal                                                     0x0
#define cAf6_upstschstaram_StsPiStsLopCurStatus_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Interrupt Status
Reg Addr   : 0x42800 - 0x56fff
Reg Formula: 0x42800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstkram_Base                                                                      0x42800
#define cAf6Reg_upvtchstkram(SliceId, StsId, VtgId, VtId)             (0x42800+16384*(SliceId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_upvtchstkram_WidthVal                                                                       32
#define cAf6Reg_upvtchstkram_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: VtPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
VT/TU pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Bit_Start                                                        4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Bit_End                                                          4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Mask                                                         cBit4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Shift                                                            4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_MaxVal                                                         0x1
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_MinVal                                                         0x0
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: VtPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at VT/TU pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Bit_Start                                                           3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Bit_End                                                             3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Mask                                                            cBit3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Shift                                                               3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_MaxVal                                                            0x1
#define cAf6_upvtchstkram_VtPiStsNdfIntr_MinVal                                                            0x0
#define cAf6_upvtchstkram_VtPiStsNdfIntr_RstVal                                                            0x0

/*--------------------------------------
BitField Name: VtPiStsCepUneqVStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in Unequip state in the related
VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in Uneqip state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Bit_Start                                              2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Bit_End                                                2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Mask                                               cBit2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Shift                                                  2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_MaxVal                                               0x1
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_MinVal                                               0x0
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_RstVal                                               0x0

/*--------------------------------------
BitField Name: VtPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in AIS state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in LOP state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Bit_Start                                                   1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Bit_End                                                     1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Mask                                                    cBit1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Shift                                                       1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_MaxVal                                                    0x1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_MinVal                                                    0x0
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: VtPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in AIS state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Bit_Start                                                   0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Bit_End                                                     0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Mask                                                    cBit0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Shift                                                       0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_MaxVal                                                    0x1
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_MinVal                                                    0x0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Current Status
Reg Addr   : 0x43000 - 0x57fff
Reg Formula: 0x43000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstaram_Base                                                                      0x43000
#define cAf6Reg_upvtchstaram(SliceId, StsId, VtgId, VtId)             (0x43000+16384*(SliceId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_upvtchstaram_WidthVal                                                                       32
#define cAf6Reg_upvtchstaram_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: VtPiStsCepUneqCurStatus
BitField Type: RO
BitField Desc: Unequip current status in the related VT/TU. When it changes for
0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU
per Alarm Interrupt Status register of the related VT/TU is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Bit_Start                                                  2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Bit_End                                                    2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask                                                   cBit2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Shift                                                      2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_MaxVal                                                   0x1
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_MinVal                                                   0x0
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_RstVal                                                   0x0

/*--------------------------------------
BitField Name: VtPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Bit_Start                                                      1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Bit_End                                                        1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Mask                                                       cBit1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Shift                                                          1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_MaxVal                                                       0x1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_MinVal                                                       0x0
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_RstVal                                                       0x0

/*--------------------------------------
BitField Name: VtPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Bit_Start                                                      0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Bit_End                                                        0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Mask                                                       cBit0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Shift                                                          0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_MaxVal                                                       0x1
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_MinVal                                                       0x0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x22080 - 0x22eaf
Reg Formula: 0x22080 + 512*SliceId + 32*AdjMode + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $AdjMode(0-1)
           + $StsId(0-23)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstsram_Base                                                                   0x22080
#define cAf6Reg_adjcntperstsram(SliceId, AdjMode, StsId)              (0x22080+512*(SliceId)+32*(AdjMode)+(StsId))
#define cAf6Reg_adjcntperstsram_WidthVal                                                                    32
#define cAf6Reg_adjcntperstsram_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [6] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Bit_Start                                                     0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Bit_End                                                      17
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Mask                                                   cBit17_0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_Shift                                                         0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_MaxVal                                                  0x3ffff
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_MinVal                                                      0x0
#define cAf6_adjcntperstsram_RxPpStsPiPtAdjCnt_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x41000 - 0x55fff
Reg Formula: 0x41000 + 16384*SliceId + 1024*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $AdjMode(0-1)
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstkram_Base                                                                   0x41000
#define cAf6Reg_adjcntperstkram(SliceId, AdjMode, StsId, VtgId, VtId) (0x41000+16384*(SliceId)+1024*(AdjMode)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_adjcntperstkram_WidthVal                                                                    32
#define cAf6Reg_adjcntperstkram_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Bit_Start                                                     0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Bit_End                                                      17
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Mask                                                   cBit17_0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_Shift                                                         0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_MaxVal                                                  0x3ffff
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_MinVal                                                      0x0
#define cAf6_adjcntperstkram_RxPpStsPiPtAdjCnt_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS per Alarm Interrupt Status
Reg Addr   : 0x23140 - 0x23f6f
Reg Formula: 0x23140 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_stspgstkram_Base                                                                       0x23140
#define cAf6Reg_stspgstkram(SliceId, StsId)                                    (0x23140+512*(SliceId)+(StsId))
#define cAf6Reg_stspgstkram_WidthVal                                                                        32
#define cAf6Reg_stspgstkram_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: StsPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgAisIntr_Bit_Start                                                              3
#define cAf6_stspgstkram_StsPgAisIntr_Bit_End                                                                3
#define cAf6_stspgstkram_StsPgAisIntr_Mask                                                               cBit3
#define cAf6_stspgstkram_StsPgAisIntr_Shift                                                                  3
#define cAf6_stspgstkram_StsPgAisIntr_MaxVal                                                               0x1
#define cAf6_stspgstkram_StsPgAisIntr_MinVal                                                               0x0
#define cAf6_stspgstkram_StsPgAisIntr_RstVal                                                               0x0

/*--------------------------------------
BitField Name: StsPgFiFoOvfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx STS
Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Bit_Start                                                          2
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Bit_End                                                            2
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Mask                                                           cBit2
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_Shift                                                              2
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_MaxVal                                                           0x1
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_MinVal                                                           0x0
#define cAf6_stspgstkram_StsPgFiFoOvfIntr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: StsPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stspgstkram_StsPgNdfIntr_Bit_Start                                                              1
#define cAf6_stspgstkram_StsPgNdfIntr_Bit_End                                                                1
#define cAf6_stspgstkram_StsPgNdfIntr_Mask                                                               cBit1
#define cAf6_stspgstkram_StsPgNdfIntr_Shift                                                                  1
#define cAf6_stspgstkram_StsPgNdfIntr_MaxVal                                                               0x1
#define cAf6_stspgstkram_StsPgNdfIntr_MinVal                                                               0x0
#define cAf6_stspgstkram_StsPgNdfIntr_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS pointer adjustment per channel counter
Reg Addr   : 0x23080 - 0x23eaf
Reg Formula: 0x23080 + 512*SliceId + 32*AdjMode + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $AdjMode(0-1)
           + $StsId(0-23)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgperstsram_Base                                                                 0x23080
#define cAf6Reg_adjcntpgperstsram(SliceId, AdjMode, StsId)            (0x23080+512*(SliceId)+32*(AdjMode)+(StsId))
#define cAf6Reg_adjcntpgperstsram_WidthVal                                                                  32
#define cAf6Reg_adjcntpgperstsram_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: StsPgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Bit_Start                                                       0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Bit_End                                                        17
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Mask                                                     cBit17_0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_Shift                                                           0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_MaxVal                                                    0x3ffff
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_MinVal                                                        0x0
#define cAf6_adjcntpgperstsram_StsPgPtAdjCnt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU per Alarm Interrupt Status
Reg Addr   : 0x62800 - 0x76fff
Reg Formula: 0x62800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_vtpgstkram_Base                                                                        0x62800
#define cAf6Reg_vtpgstkram(SliceId, StsId, VtgId, VtId)               (0x62800+16384*(SliceId)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_vtpgstkram_WidthVal                                                                         32
#define cAf6Reg_vtpgstkram_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: VtPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgAisIntr_Bit_Start                                                                3
#define cAf6_vtpgstkram_VtPgAisIntr_Bit_End                                                                  3
#define cAf6_vtpgstkram_VtPgAisIntr_Mask                                                                 cBit3
#define cAf6_vtpgstkram_VtPgAisIntr_Shift                                                                    3
#define cAf6_vtpgstkram_VtPgAisIntr_MaxVal                                                                 0x1
#define cAf6_vtpgstkram_VtPgAisIntr_MinVal                                                                 0x0
#define cAf6_vtpgstkram_VtPgAisIntr_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: VtPgFiFoOvfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx VTTU
Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Bit_Start                                                            2
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Bit_End                                                              2
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Mask                                                             cBit2
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Shift                                                                2
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_MaxVal                                                             0x1
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_MinVal                                                             0x0
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: VtPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgNdfIntr_Bit_Start                                                                1
#define cAf6_vtpgstkram_VtPgNdfIntr_Bit_End                                                                  1
#define cAf6_vtpgstkram_VtPgNdfIntr_Mask                                                                 cBit1
#define cAf6_vtpgstkram_VtPgNdfIntr_Shift                                                                    1
#define cAf6_vtpgstkram_VtPgNdfIntr_MaxVal                                                                 0x1
#define cAf6_vtpgstkram_VtPgNdfIntr_MinVal                                                                 0x0
#define cAf6_vtpgstkram_VtPgNdfIntr_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU pointer adjustment per channel counter
Reg Addr   : 0x61000 - 0x75fff
Reg Formula: 0x61000 + 16384*SliceId + 1024*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $AdjMode(0-1)
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgpervtram_Base                                                                  0x61000
#define cAf6Reg_adjcntpgpervtram(SliceId, AdjMode, StsId, VtgId, VtId)(0x61000+16384*(SliceId)+1024*(AdjMode)+32*(StsId)+4*(VtgId)+(VtId))
#define cAf6Reg_adjcntpgpervtram_WidthVal                                                                   32
#define cAf6Reg_adjcntpgpervtram_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: VtpgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Bit_Start                                                         0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Bit_End                                                          17
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Mask                                                       cBit17_0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Shift                                                             0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_MaxVal                                                      0x3ffff
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_MinVal                                                          0x0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K1 Stable Monitoring Threshold Control
Reg Addr   : 0x24000
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk1stbthr_reg_Base                                                                0x24000
#define cAf6Reg_tohglbk1stbthr_reg                                                                     0x24000
#define cAf6Reg_tohglbk1stbthr_reg_WidthVal                                                                 32
#define cAf6Reg_tohglbk1stbthr_reg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TohK1StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable K1 status.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Bit_Start                                                       8
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Bit_End                                                        10
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Mask                                                     cBit10_8
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Shift                                                           8
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_MaxVal                                                        0x7
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_MinVal                                                        0x0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_RstVal                                                        0x3

/*--------------------------------------
BitField Name: TohK1StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable K1 status.
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Bit_Start                                                       0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Bit_End                                                         2
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Mask                                                      cBit2_0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Shift                                                           0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_MaxVal                                                        0x7
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_MinVal                                                        0x0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_RstVal                                                        0x4


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K2 Stable Monitoring Threshold Control
Reg Addr   : 0x24001
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk2stbthr_reg_Base                                                                0x24001
#define cAf6Reg_tohglbk2stbthr_reg                                                                     0x24001
#define cAf6Reg_tohglbk2stbthr_reg_WidthVal                                                                 32
#define cAf6Reg_tohglbk2stbthr_reg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TohK2StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable K2 status.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Bit_Start                                                       8
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Bit_End                                                        10
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Mask                                                     cBit10_8
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Shift                                                           8
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_MaxVal                                                        0x7
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_MinVal                                                        0x0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_RstVal                                                        0x3

/*--------------------------------------
BitField Name: TohK2StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable K2 status.
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Bit_Start                                                       0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Bit_End                                                         2
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Mask                                                      cBit2_0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Shift                                                           0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_MaxVal                                                        0x7
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_MinVal                                                        0x0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_RstVal                                                        0x4


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global S1 Stable Monitoring Threshold Control
Reg Addr   : 0x24002
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbs1stbthr_reg_Base                                                                0x24002
#define cAf6Reg_tohglbs1stbthr_reg                                                                     0x24002
#define cAf6Reg_tohglbs1stbthr_reg_WidthVal                                                                 32
#define cAf6Reg_tohglbs1stbthr_reg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TohS1StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable S1 status.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Bit_Start                                                       8
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Bit_End                                                        11
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Mask                                                     cBit11_8
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Shift                                                           8
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_MaxVal                                                        0xf
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_MinVal                                                        0x0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_RstVal                                                        0x5

/*--------------------------------------
BitField Name: TohS1StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable S1 status.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Bit_Start                                                       0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Bit_End                                                         3
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Mask                                                      cBit3_0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Shift                                                           0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_MaxVal                                                        0xf
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_MinVal                                                        0x0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_RstVal                                                        0x5


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global RDI_L Detecting Threshold Control
Reg Addr   : 0x24003
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbrdidetthr_reg_Base                                                               0x24003
#define cAf6Reg_tohglbrdidetthr_reg                                                                    0x24003
#define cAf6Reg_tohglbrdidetthr_reg_WidthVal                                                                32
#define cAf6Reg_tohglbrdidetthr_reg_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TohRdiDetThr2
BitField Type: RW
BitField Desc: The second threshold for detecting RDI_L.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Bit_Start                                                     8
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Bit_End                                                      11
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Mask                                                   cBit11_8
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Shift                                                         8
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_MaxVal                                                      0xf
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_MinVal                                                      0x0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_RstVal                                                      0x4

/*--------------------------------------
BitField Name: TohRdiDetThr1
BitField Type: RW
BitField Desc: The first threshold for detecting RDI_L.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Bit_Start                                                     0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Bit_End                                                       3
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Mask                                                    cBit3_0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Shift                                                         0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_MaxVal                                                      0xf
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_MinVal                                                      0x0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_RstVal                                                      0x9


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global AIS_L Detecting Threshold Control
Reg Addr   : 0x24004
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbaisdetthr_reg_Base                                                               0x24004
#define cAf6Reg_tohglbaisdetthr_reg                                                                    0x24004
#define cAf6Reg_tohglbaisdetthr_reg_WidthVal                                                                32
#define cAf6Reg_tohglbaisdetthr_reg_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TohAisDetThr2
BitField Type: RW
BitField Desc: The second threshold for detecting AIS_L.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Bit_Start                                                     8
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Bit_End                                                      11
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Mask                                                   cBit11_8
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Shift                                                         8
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_MaxVal                                                      0xf
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_MinVal                                                      0x0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_RstVal                                                      0x3

/*--------------------------------------
BitField Name: TohAisDetThr1
BitField Type: RW
BitField Desc: The first threshold for detecting AIS_L.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Bit_Start                                                     0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Bit_End                                                       3
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Mask                                                    cBit3_0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Shift                                                         0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_MaxVal                                                      0xf
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_MinVal                                                      0x0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_RstVal                                                      0x4


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K1 Sampling Threshold Control
Reg Addr   : 0x24005
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk1smpthr_reg_Base                                                                0x24005
#define cAf6Reg_tohglbk1smpthr_reg                                                                     0x24005
#define cAf6Reg_tohglbk1smpthr_reg_WidthVal                                                                 32
#define cAf6Reg_tohglbk1smpthr_reg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TohK1SmpThr2
BitField Type: RW
BitField Desc: The second threshold for sampling K1 to detect APS defect.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Bit_Start                                                       8
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Bit_End                                                        11
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Mask                                                     cBit11_8
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Shift                                                           8
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_MaxVal                                                        0xf
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_MinVal                                                        0x0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_RstVal                                                        0x7

/*--------------------------------------
BitField Name: TohK1SmpThr1
BitField Type: RW
BitField Desc: The first threshold for sampling K1 to detect APS defect.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Bit_Start                                                       0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Bit_End                                                         3
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Mask                                                      cBit3_0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Shift                                                           0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_MaxVal                                                        0xf
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_MinVal                                                        0x0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_RstVal                                                        0x7


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global Error Counter Control
Reg Addr   : 0x24006
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure mode for counters in TOH monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglberrcntmod_reg_Base                                                               0x24006
#define cAf6Reg_tohglberrcntmod_reg                                                                    0x24006
#define cAf6Reg_tohglberrcntmod_reg_WidthVal                                                                32
#define cAf6Reg_tohglberrcntmod_reg_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TohReiErrCntMod
BitField Type: RW
BitField Desc: REI counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Bit_Start                                                   2
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Bit_End                                                     2
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Mask                                                    cBit2
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Shift                                                       2
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_MaxVal                                                    0x1
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_MinVal                                                    0x0
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_RstVal                                                    0x1

/*--------------------------------------
BitField Name: TohB2ErrCntMod
BitField Type: RW
BitField Desc: B2 counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Bit_Start                                                    1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Bit_End                                                      1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Mask                                                     cBit1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Shift                                                        1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_MaxVal                                                     0x1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_MinVal                                                     0x0
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_RstVal                                                     0x1

/*--------------------------------------
BitField Name: TohB1ErrCntMod
BitField Type: RW
BitField Desc: B1 counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Bit_Start                                                    0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Bit_End                                                      0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Mask                                                     cBit0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Shift                                                        0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_MaxVal                                                     0x1
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_MinVal                                                     0x0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_RstVal                                                     0x1


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Montoring Affect Control
Reg Addr   : 0x24007
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure affective mode for TOH monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbaffen_reg_Base                                                                   0x24007
#define cAf6Reg_tohglbaffen_reg                                                                        0x24007
#define cAf6Reg_tohglbaffen_reg_WidthVal                                                                    32
#define cAf6Reg_tohglbaffen_reg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: TohAisAffStbMon
BitField Type: RW
BitField Desc: AIS affects to Stable monitoring status of the line at which LOF
or LOS is detected 1: Disable 0: Enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Bit_Start                                                       3
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Bit_End                                                         3
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Mask                                                        cBit3
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Shift                                                           3
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_MaxVal                                                        0x1
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_MinVal                                                        0x0
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TohAisAffRdilMon
BitField Type: RW
BitField Desc: AIS affects to RDI-L monitoring status of the line at which LOF
or LOS is detected. 1: Disable 0: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Bit_Start                                                      2
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Bit_End                                                        2
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Mask                                                       cBit2
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Shift                                                          2
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_MaxVal                                                       0x1
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_MinVal                                                       0x0
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TohAisAffAislMon
BitField Type: RW
BitField Desc: AIS affects to AIS-L monitoring  status of the line at which LOF
or LOS is detected. 1: Disable 0: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Bit_Start                                                      1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Bit_End                                                        1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Mask                                                       cBit1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Shift                                                          1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_MaxVal                                                       0x1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_MinVal                                                       0x0
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TohAisAffErrCnt
BitField Type: RW
BitField Desc: AIS affects to error counters (B1,B2,REI) of the line at which
LOF or LOS is detected. 1: Disable 0: Enable.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Bit_Start                                                       0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Bit_End                                                         0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Mask                                                        cBit0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Shift                                                           0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_MaxVal                                                        0x1
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_MinVal                                                        0x0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring Per Channel Control
Reg Addr   : 0x24100 - 0x2412f
Reg Formula: 0x24100  + Ec1Id
    Where  : 
           + $Ec1Id(0-47)
Reg Desc   : 
Each register is used to configure for TOH monitoring engine of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohramctl_Base                                                                         0x24100
#define cAf6Reg_tohramctl(Ec1Id)                                                             (0x24100+(Ec1Id))
#define cAf6Reg_tohramctl_WidthVal                                                                          32
#define cAf6Reg_tohramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: B1ErrCntBlkMod
BitField Type: RW
BitField Desc: B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohramctl_B1ErrCntBlkMod_Bit_Start                                                              8
#define cAf6_tohramctl_B1ErrCntBlkMod_Bit_End                                                                8
#define cAf6_tohramctl_B1ErrCntBlkMod_Mask                                                               cBit8
#define cAf6_tohramctl_B1ErrCntBlkMod_Shift                                                                  8
#define cAf6_tohramctl_B1ErrCntBlkMod_MaxVal                                                               0x1
#define cAf6_tohramctl_B1ErrCntBlkMod_MinVal                                                               0x0
#define cAf6_tohramctl_B1ErrCntBlkMod_RstVal                                                               0x0

/*--------------------------------------
BitField Name: B2ErrCntBlkMod
BitField Type: RW
BitField Desc: B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohramctl_B2ErrCntBlkMod_Bit_Start                                                              7
#define cAf6_tohramctl_B2ErrCntBlkMod_Bit_End                                                                7
#define cAf6_tohramctl_B2ErrCntBlkMod_Mask                                                               cBit7
#define cAf6_tohramctl_B2ErrCntBlkMod_Shift                                                                  7
#define cAf6_tohramctl_B2ErrCntBlkMod_MaxVal                                                               0x1
#define cAf6_tohramctl_B2ErrCntBlkMod_MinVal                                                               0x0
#define cAf6_tohramctl_B2ErrCntBlkMod_RstVal                                                               0x0

/*--------------------------------------
BitField Name: ReiErrCntBlkMod
BitField Type: RW
BitField Desc: REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohramctl_ReiErrCntBlkMod_Bit_Start                                                             6
#define cAf6_tohramctl_ReiErrCntBlkMod_Bit_End                                                               6
#define cAf6_tohramctl_ReiErrCntBlkMod_Mask                                                              cBit6
#define cAf6_tohramctl_ReiErrCntBlkMod_Shift                                                                 6
#define cAf6_tohramctl_ReiErrCntBlkMod_MaxVal                                                              0x1
#define cAf6_tohramctl_ReiErrCntBlkMod_MinVal                                                              0x0
#define cAf6_tohramctl_ReiErrCntBlkMod_RstVal                                                              0x0

/*--------------------------------------
BitField Name: StbRdiisLThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting RDI_L 1: Threshold 2 is
selected 0: Threshold 1 is selected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohramctl_StbRdiisLThresSel_Bit_Start                                                           5
#define cAf6_tohramctl_StbRdiisLThresSel_Bit_End                                                             5
#define cAf6_tohramctl_StbRdiisLThresSel_Mask                                                            cBit5
#define cAf6_tohramctl_StbRdiisLThresSel_Shift                                                               5
#define cAf6_tohramctl_StbRdiisLThresSel_MaxVal                                                            0x1
#define cAf6_tohramctl_StbRdiisLThresSel_MinVal                                                            0x0
#define cAf6_tohramctl_StbRdiisLThresSel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: StbAisLThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting AIS_L 1: Threshold 2 is
selected 0: Threshold 1 is selected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohramctl_StbAisLThresSel_Bit_Start                                                             4
#define cAf6_tohramctl_StbAisLThresSel_Bit_End                                                               4
#define cAf6_tohramctl_StbAisLThresSel_Mask                                                              cBit4
#define cAf6_tohramctl_StbAisLThresSel_Shift                                                                 4
#define cAf6_tohramctl_StbAisLThresSel_MaxVal                                                              0x1
#define cAf6_tohramctl_StbAisLThresSel_MinVal                                                              0x0
#define cAf6_tohramctl_StbAisLThresSel_RstVal                                                              0x0

/*--------------------------------------
BitField Name: K2StbMd
BitField Type: RW
BitField Desc: Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0]
value is selected 0: K2[7:3] value is selected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohramctl_K2StbMd_Bit_Start                                                                     3
#define cAf6_tohramctl_K2StbMd_Bit_End                                                                       3
#define cAf6_tohramctl_K2StbMd_Mask                                                                      cBit3
#define cAf6_tohramctl_K2StbMd_Shift                                                                         3
#define cAf6_tohramctl_K2StbMd_MaxVal                                                                      0x1
#define cAf6_tohramctl_K2StbMd_MinVal                                                                      0x0
#define cAf6_tohramctl_K2StbMd_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: StbK1K2ThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting stable or non-stable K1/K2
status 1: Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohramctl_StbK1K2ThresSel_Bit_Start                                                             2
#define cAf6_tohramctl_StbK1K2ThresSel_Bit_End                                                               2
#define cAf6_tohramctl_StbK1K2ThresSel_Mask                                                              cBit2
#define cAf6_tohramctl_StbK1K2ThresSel_Shift                                                                 2
#define cAf6_tohramctl_StbK1K2ThresSel_MaxVal                                                              0x1
#define cAf6_tohramctl_StbK1K2ThresSel_MinVal                                                              0x0
#define cAf6_tohramctl_StbK1K2ThresSel_RstVal                                                              0x0

/*--------------------------------------
BitField Name: StbS1ThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting stable or non-stable S1
status 1: Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohramctl_StbS1ThresSel_Bit_Start                                                               1
#define cAf6_tohramctl_StbS1ThresSel_Bit_End                                                                 1
#define cAf6_tohramctl_StbS1ThresSel_Mask                                                                cBit1
#define cAf6_tohramctl_StbS1ThresSel_Shift                                                                   1
#define cAf6_tohramctl_StbS1ThresSel_MaxVal                                                                0x1
#define cAf6_tohramctl_StbS1ThresSel_MinVal                                                                0x0
#define cAf6_tohramctl_StbS1ThresSel_RstVal                                                                0x0

/*--------------------------------------
BitField Name: SmpK1ThresSel
BitField Type: RW
BitField Desc: Select the sample threshold for detecting APS defect. 1:
Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohramctl_SmpK1ThresSel_Bit_Start                                                               0
#define cAf6_tohramctl_SmpK1ThresSel_Bit_End                                                                 0
#define cAf6_tohramctl_SmpK1ThresSel_Mask                                                                cBit0
#define cAf6_tohramctl_SmpK1ThresSel_Shift                                                                   0
#define cAf6_tohramctl_SmpK1ThresSel_MaxVal                                                                0x1
#define cAf6_tohramctl_SmpK1ThresSel_MinVal                                                                0x0
#define cAf6_tohramctl_SmpK1ThresSel_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Error Read Only Counter
Reg Addr   : 0x24800 - 0x2482f
Reg Formula: 0x24800 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B1 error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1errrocnt_Base                                                                     0x24800
#define cAf6Reg_tohb1errrocnt(Ec1Id)                                                         (0x24800+(Ec1Id))
#define cAf6Reg_tohb1errrocnt_WidthVal                                                                      32
#define cAf6Reg_tohb1errrocnt_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: B1ErrRoCnt
BitField Type: RO
BitField Desc: B1 Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Bit_Start                                                              0
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Bit_End                                                               22
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Mask                                                            cBit22_0
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Shift                                                                  0
#define cAf6_tohb1errrocnt_B1ErrRoCnt_MaxVal                                                          0x7fffff
#define cAf6_tohb1errrocnt_B1ErrRoCnt_MinVal                                                               0x0
#define cAf6_tohb1errrocnt_B1ErrRoCnt_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Error Read to Clear Counter
Reg Addr   : 0x24a00 - 0x24a2f
Reg Formula: 0x24a00 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B1 error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1errr2ccnt_Base                                                                    0x24a00
#define cAf6Reg_tohb1errr2ccnt(Ec1Id)                                                        (0x24a00+(Ec1Id))
#define cAf6Reg_tohb1errr2ccnt_WidthVal                                                                     32
#define cAf6Reg_tohb1errr2ccnt_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: B1ErrR2cCnt
BitField Type: RC
BitField Desc: B1 Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Bit_Start                                                            0
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Bit_End                                                             22
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Mask                                                          cBit22_0
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Shift                                                                0
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_MaxVal                                                        0x7fffff
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_MinVal                                                             0x0
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Block Error Read Only Counter
Reg Addr   : 0x24c00 - 0x24c2f
Reg Formula: 0x24c00 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B1 Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1blkerrrocnt_Base                                                                  0x24c00
#define cAf6Reg_tohb1blkerrrocnt(Ec1Id)                                                      (0x24c00+(Ec1Id))
#define cAf6Reg_tohb1blkerrrocnt_WidthVal                                                                   32
#define cAf6Reg_tohb1blkerrrocnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: B1BlkErrRoCnt
BitField Type: RO
BitField Desc: B1 Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Bit_Start                                                        0
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Bit_End                                                         15
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Mask                                                      cBit15_0
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Shift                                                            0
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_MaxVal                                                      0xffff
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_MinVal                                                         0x0
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Block Error Read to Clear Counter
Reg Addr   : 0x24e00 - 0x24e2f
Reg Formula: 0x24e00 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B1 Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1blkerrr2ccnt_Base                                                                 0x24e00
#define cAf6Reg_tohb1blkerrr2ccnt(Ec1Id)                                                     (0x24e00+(Ec1Id))
#define cAf6Reg_tohb1blkerrr2ccnt_WidthVal                                                                  32
#define cAf6Reg_tohb1blkerrr2ccnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: B1BlkErrR2cCnt
BitField Type: RC
BitField Desc: B1 Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Bit_Start                                                      0
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Bit_End                                                       15
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Mask                                                    cBit15_0
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Shift                                                          0
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_MaxVal                                                    0xffff
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_MinVal                                                       0x0
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Error Read Only Counter
Reg Addr   : 0x24840 - 0x2486f
Reg Formula: 0x24840 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B2 error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2errrocnt_Base                                                                     0x24840
#define cAf6Reg_tohb2errrocnt(Ec1Id)                                                         (0x24840+(Ec1Id))
#define cAf6Reg_tohb2errrocnt_WidthVal                                                                      32
#define cAf6Reg_tohb2errrocnt_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: B2ErrRoCnt
BitField Type: RO
BitField Desc: B2 Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Bit_Start                                                              0
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Bit_End                                                               22
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Mask                                                            cBit22_0
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Shift                                                                  0
#define cAf6_tohb2errrocnt_B2ErrRoCnt_MaxVal                                                          0x7fffff
#define cAf6_tohb2errrocnt_B2ErrRoCnt_MinVal                                                               0x0
#define cAf6_tohb2errrocnt_B2ErrRoCnt_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Error Read to Clear Counter
Reg Addr   : 0x24a40 - 0x24a6f
Reg Formula: 0x24a40 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B2 error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2errr2ccnt_Base                                                                    0x24a40
#define cAf6Reg_tohb2errr2ccnt(Ec1Id)                                                        (0x24a40+(Ec1Id))
#define cAf6Reg_tohb2errr2ccnt_WidthVal                                                                     32
#define cAf6Reg_tohb2errr2ccnt_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: B2ErrR2cCnt
BitField Type: RC
BitField Desc: B2 Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Bit_Start                                                            0
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Bit_End                                                             22
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Mask                                                          cBit22_0
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Shift                                                                0
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_MaxVal                                                        0x7fffff
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_MinVal                                                             0x0
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Block Error Read Only Counter
Reg Addr   : 0x24c40 - 0x24c6f
Reg Formula: 0x24c40 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B2 Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2blkerrrocnt_Base                                                                  0x24c40
#define cAf6Reg_tohb2blkerrrocnt(Ec1Id)                                                      (0x24c40+(Ec1Id))
#define cAf6Reg_tohb2blkerrrocnt_WidthVal                                                                   32
#define cAf6Reg_tohb2blkerrrocnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: B2BlkErrRoCnt
BitField Type: RO
BitField Desc: B2 Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Bit_Start                                                        0
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Bit_End                                                         15
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Mask                                                      cBit15_0
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Shift                                                            0
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_MaxVal                                                      0xffff
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_MinVal                                                         0x0
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Block Error Read to Clear Counter
Reg Addr   : 0x24e40 - 0x24e6f
Reg Formula: 0x24e40 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store B2 Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2blkerrr2ccnt_Base                                                                 0x24e40
#define cAf6Reg_tohb2blkerrr2ccnt(Ec1Id)                                                     (0x24e40+(Ec1Id))
#define cAf6Reg_tohb2blkerrr2ccnt_WidthVal                                                                  32
#define cAf6Reg_tohb2blkerrr2ccnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: B2BlkErrR2cCnt
BitField Type: RC
BitField Desc: B2 Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Bit_Start                                                      0
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Bit_End                                                       15
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Mask                                                    cBit15_0
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Shift                                                          0
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_MaxVal                                                    0xffff
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_MinVal                                                       0x0
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Error Read Only Counter
Reg Addr   : 0x24940 - 0x2496f
Reg Formula: 0x24940 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store REI error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreierrrocnt_Base                                                                    0x24940
#define cAf6Reg_tohreierrrocnt(Ec1Id)                                                        (0x24940+(Ec1Id))
#define cAf6Reg_tohreierrrocnt_WidthVal                                                                     32
#define cAf6Reg_tohreierrrocnt_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: ReiErrRoCnt
BitField Type: RO
BitField Desc: REI Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Bit_Start                                                            0
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Bit_End                                                             22
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Mask                                                          cBit22_0
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Shift                                                                0
#define cAf6_tohreierrrocnt_ReiErrRoCnt_MaxVal                                                        0x7fffff
#define cAf6_tohreierrrocnt_ReiErrRoCnt_MinVal                                                             0x0
#define cAf6_tohreierrrocnt_ReiErrRoCnt_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Error Read to Clear Counter
Reg Addr   : 0x24b40 - 0x24b6f
Reg Formula: 0x24b40 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store REI error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreierrr2ccnt_Base                                                                   0x24b40
#define cAf6Reg_tohreierrr2ccnt(Ec1Id)                                                       (0x24b40+(Ec1Id))
#define cAf6Reg_tohreierrr2ccnt_WidthVal                                                                    32
#define cAf6Reg_tohreierrr2ccnt_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReiErrR2cCnt
BitField Type: RC
BitField Desc: REI Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Bit_Start                                                          0
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Bit_End                                                           22
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Mask                                                        cBit22_0
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Shift                                                              0
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_MaxVal                                                      0x7fffff
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_MinVal                                                           0x0
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Block Error Read Only Counter
Reg Addr   : 0x24d40 - 0x24d6f
Reg Formula: 0x24d40 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store REI Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreiblkerrrocnt_Base                                                                 0x24d40
#define cAf6Reg_tohreiblkerrrocnt(Ec1Id)                                                     (0x24d40+(Ec1Id))
#define cAf6Reg_tohreiblkerrrocnt_WidthVal                                                                  32
#define cAf6Reg_tohreiblkerrrocnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReiBlkErrRoCnt
BitField Type: RO
BitField Desc: REI Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Bit_Start                                                      0
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Bit_End                                                       15
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Mask                                                    cBit15_0
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Shift                                                          0
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_MaxVal                                                    0xffff
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_MinVal                                                       0x0
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Block Error Read to Clear Counter
Reg Addr   : 0x24f40 - 0x24f6f
Reg Formula: 0x24f40 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store REI Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreiblkerrr2ccnt_Base                                                                0x24f40
#define cAf6Reg_tohreiblkerrr2ccnt(Ec1Id)                                                    (0x24f40+(Ec1Id))
#define cAf6Reg_tohreiblkerrr2ccnt_WidthVal                                                                 32
#define cAf6Reg_tohreiblkerrr2ccnt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ReiBlkErrR2cCnt
BitField Type: RC
BitField Desc: REI Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Bit_Start                                                    0
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Bit_End                                                     15
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Mask                                                  cBit15_0
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Shift                                                        0
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_MaxVal                                                  0xffff
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_MinVal                                                     0x0
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH K1 Monitoring Status
Reg Addr   : 0x24880 - 0x248af
Reg Formula: 0x24880 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store K1 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohk1monsta_Base                                                                       0x24880
#define cAf6Reg_tohk1monsta(Ec1Id)                                                           (0x24880+(Ec1Id))
#define cAf6Reg_tohk1monsta_WidthVal                                                                        32
#define cAf6Reg_tohk1monsta_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: CurApsDef
BitField Type: RW
BitField Desc: current APS Defect. 1: APS defect is detected from APS bytes. 0:
APS defect is not detected from APS bytes.
BitField Bits: [23]
--------------------------------------*/
#define cAf6_tohk1monsta_CurApsDef_Bit_Start                                                                23
#define cAf6_tohk1monsta_CurApsDef_Bit_End                                                                  23
#define cAf6_tohk1monsta_CurApsDef_Mask                                                                 cBit23
#define cAf6_tohk1monsta_CurApsDef_Shift                                                                    23
#define cAf6_tohk1monsta_CurApsDef_MaxVal                                                                  0x1
#define cAf6_tohk1monsta_CurApsDef_MinVal                                                                  0x0
#define cAf6_tohk1monsta_CurApsDef_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: K1SmpCnt
BitField Type: RW
BitField Desc: Sampling counter.
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_tohk1monsta_K1SmpCnt_Bit_Start                                                                 19
#define cAf6_tohk1monsta_K1SmpCnt_Bit_End                                                                   22
#define cAf6_tohk1monsta_K1SmpCnt_Mask                                                               cBit22_19
#define cAf6_tohk1monsta_K1SmpCnt_Shift                                                                     19
#define cAf6_tohk1monsta_K1SmpCnt_MaxVal                                                                   0xf
#define cAf6_tohk1monsta_K1SmpCnt_MinVal                                                                   0x0
#define cAf6_tohk1monsta_K1SmpCnt_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: SameK1Cnt
BitField Type: RW
BitField Desc: The number of same contiguous K1 bytes. It is held at StbK1Thr
value when the number of same contiguous K1 bytes is equal to or more than the
StbK1Thr value.In this case, K1 bytes are stable.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_tohk1monsta_SameK1Cnt_Bit_Start                                                                16
#define cAf6_tohk1monsta_SameK1Cnt_Bit_End                                                                  18
#define cAf6_tohk1monsta_SameK1Cnt_Mask                                                              cBit18_16
#define cAf6_tohk1monsta_SameK1Cnt_Shift                                                                    16
#define cAf6_tohk1monsta_SameK1Cnt_MaxVal                                                                  0x7
#define cAf6_tohk1monsta_SameK1Cnt_MinVal                                                                  0x0
#define cAf6_tohk1monsta_SameK1Cnt_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: K1StbVal
BitField Type: RW
BitField Desc: Stable K1 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohk1monsta_K1StbVal_Bit_Start                                                                  8
#define cAf6_tohk1monsta_K1StbVal_Bit_End                                                                   15
#define cAf6_tohk1monsta_K1StbVal_Mask                                                                cBit15_8
#define cAf6_tohk1monsta_K1StbVal_Shift                                                                      8
#define cAf6_tohk1monsta_K1StbVal_MaxVal                                                                  0xff
#define cAf6_tohk1monsta_K1StbVal_MinVal                                                                   0x0
#define cAf6_tohk1monsta_K1StbVal_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: K1CurVal
BitField Type: RW
BitField Desc: Current K1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohk1monsta_K1CurVal_Bit_Start                                                                  0
#define cAf6_tohk1monsta_K1CurVal_Bit_End                                                                    7
#define cAf6_tohk1monsta_K1CurVal_Mask                                                                 cBit7_0
#define cAf6_tohk1monsta_K1CurVal_Shift                                                                      0
#define cAf6_tohk1monsta_K1CurVal_MaxVal                                                                  0xff
#define cAf6_tohk1monsta_K1CurVal_MinVal                                                                   0x0
#define cAf6_tohk1monsta_K1CurVal_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH K2 Monitoring Status
Reg Addr   : 0x248c0 - 0x248ef
Reg Formula: 0x248c0 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store K2 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohk2monsta_Base                                                                       0x248c0
#define cAf6Reg_tohk2monsta(Ec1Id)                                                           (0x248c0+(Ec1Id))
#define cAf6Reg_tohk2monsta_WidthVal                                                                        32
#define cAf6Reg_tohk2monsta_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: Internal
BitField Type: RW
BitField Desc: Internal.
BitField Bits: [28:21]
--------------------------------------*/
#define cAf6_tohk2monsta_Internal_Bit_Start                                                                 21
#define cAf6_tohk2monsta_Internal_Bit_End                                                                   28
#define cAf6_tohk2monsta_Internal_Mask                                                               cBit28_21
#define cAf6_tohk2monsta_Internal_Shift                                                                     21
#define cAf6_tohk2monsta_Internal_MaxVal                                                                  0xff
#define cAf6_tohk2monsta_Internal_MinVal                                                                   0x0
#define cAf6_tohk2monsta_Internal_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: CurAisL
BitField Type: RW
BitField Desc: current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes.
0: AIS-L defect is not detected from K2 bytes.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_tohk2monsta_CurAisL_Bit_Start                                                                  20
#define cAf6_tohk2monsta_CurAisL_Bit_End                                                                    20
#define cAf6_tohk2monsta_CurAisL_Mask                                                                   cBit20
#define cAf6_tohk2monsta_CurAisL_Shift                                                                      20
#define cAf6_tohk2monsta_CurAisL_MaxVal                                                                    0x1
#define cAf6_tohk2monsta_CurAisL_MinVal                                                                    0x0
#define cAf6_tohk2monsta_CurAisL_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: CurRdiL
BitField Type: RW
BitField Desc: current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes.
0: RDI-L defect is not detected from K2 bytes.
BitField Bits: [19]
--------------------------------------*/
#define cAf6_tohk2monsta_CurRdiL_Bit_Start                                                                  19
#define cAf6_tohk2monsta_CurRdiL_Bit_End                                                                    19
#define cAf6_tohk2monsta_CurRdiL_Mask                                                                   cBit19
#define cAf6_tohk2monsta_CurRdiL_Shift                                                                      19
#define cAf6_tohk2monsta_CurRdiL_MaxVal                                                                    0x1
#define cAf6_tohk2monsta_CurRdiL_MinVal                                                                    0x0
#define cAf6_tohk2monsta_CurRdiL_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: SameK2Cnt
BitField Type: RW
BitField Desc: The number of same contiguous K2 bytes. It is held at StbK2Thr
value when the number of same contiguous K2 bytes is equal to or more than the
StbK2Thr value.In this case, K2 bytes are stable.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_tohk2monsta_SameK2Cnt_Bit_Start                                                                16
#define cAf6_tohk2monsta_SameK2Cnt_Bit_End                                                                  18
#define cAf6_tohk2monsta_SameK2Cnt_Mask                                                              cBit18_16
#define cAf6_tohk2monsta_SameK2Cnt_Shift                                                                    16
#define cAf6_tohk2monsta_SameK2Cnt_MaxVal                                                                  0x7
#define cAf6_tohk2monsta_SameK2Cnt_MinVal                                                                  0x0
#define cAf6_tohk2monsta_SameK2Cnt_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: K2StbVal
BitField Type: RW
BitField Desc: Stable K2 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohk2monsta_K2StbVal_Bit_Start                                                                  8
#define cAf6_tohk2monsta_K2StbVal_Bit_End                                                                   15
#define cAf6_tohk2monsta_K2StbVal_Mask                                                                cBit15_8
#define cAf6_tohk2monsta_K2StbVal_Shift                                                                      8
#define cAf6_tohk2monsta_K2StbVal_MaxVal                                                                  0xff
#define cAf6_tohk2monsta_K2StbVal_MinVal                                                                   0x0
#define cAf6_tohk2monsta_K2StbVal_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: K2CurVal
BitField Type: RW
BitField Desc: Current K2 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohk2monsta_K2CurVal_Bit_Start                                                                  0
#define cAf6_tohk2monsta_K2CurVal_Bit_End                                                                    7
#define cAf6_tohk2monsta_K2CurVal_Mask                                                                 cBit7_0
#define cAf6_tohk2monsta_K2CurVal_Shift                                                                      0
#define cAf6_tohk2monsta_K2CurVal_MaxVal                                                                  0xff
#define cAf6_tohk2monsta_K2CurVal_MinVal                                                                   0x0
#define cAf6_tohk2monsta_K2CurVal_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH S1 Monitoring Status
Reg Addr   : 0x24900 - 0x2492f
Reg Formula: 0x24900 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
Each register is used to store S1 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohs1monsta_Base                                                                       0x24900
#define cAf6Reg_tohs1monsta(Ec1Id)                                                           (0x24900+(Ec1Id))
#define cAf6Reg_tohs1monsta_WidthVal                                                                        32
#define cAf6Reg_tohs1monsta_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: SameS1Cnt
BitField Type: RW
BitField Desc: The number of same contiguous S1 bytes. It is held at StbS1Thr
value when the number of same contiguous S1 bytes is equal to or more than the
StbS1Thr value.In this case, S1 bytes are stable.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_tohs1monsta_SameS1Cnt_Bit_Start                                                                16
#define cAf6_tohs1monsta_SameS1Cnt_Bit_End                                                                  19
#define cAf6_tohs1monsta_SameS1Cnt_Mask                                                              cBit19_16
#define cAf6_tohs1monsta_SameS1Cnt_Shift                                                                    16
#define cAf6_tohs1monsta_SameS1Cnt_MaxVal                                                                  0xf
#define cAf6_tohs1monsta_SameS1Cnt_MinVal                                                                  0x0
#define cAf6_tohs1monsta_SameS1Cnt_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: S1StbVal
BitField Type: RW
BitField Desc: Stable S1 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohs1monsta_S1StbVal_Bit_Start                                                                  8
#define cAf6_tohs1monsta_S1StbVal_Bit_End                                                                   15
#define cAf6_tohs1monsta_S1StbVal_Mask                                                                cBit15_8
#define cAf6_tohs1monsta_S1StbVal_Shift                                                                      8
#define cAf6_tohs1monsta_S1StbVal_MaxVal                                                                  0xff
#define cAf6_tohs1monsta_S1StbVal_MinVal                                                                   0x0
#define cAf6_tohs1monsta_S1StbVal_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: S1CurVal
BitField Type: RW
BitField Desc: Current S1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohs1monsta_S1CurVal_Bit_Start                                                                  0
#define cAf6_tohs1monsta_S1CurVal_Bit_End                                                                    7
#define cAf6_tohs1monsta_S1CurVal_Mask                                                                 cBit7_0
#define cAf6_tohs1monsta_S1CurVal_Shift                                                                      0
#define cAf6_tohs1monsta_S1CurVal_MaxVal                                                                  0xff
#define cAf6_tohs1monsta_S1CurVal_MinVal                                                                   0x0
#define cAf6_tohs1monsta_S1CurVal_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Interrupt Enable Control
Reg Addr   : 0x24200 - 0x2422f
Reg Formula: 0x24200 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperalrenbctl_Base                                                                0x24200
#define cAf6Reg_tohintperalrenbctl(Ec1Id)                                                    (0x24200+(Ec1Id))
#define cAf6Reg_tohintperalrenbctl_WidthVal                                                                 32
#define cAf6Reg_tohintperalrenbctl_WriteMask                                                               0x0

#define cAf6_tohintperalrenbctl_TcaStateChgIntrEn_Mask                                                  cBit12
#define cAf6_tohintperalrenbctl_TcaStateChgIntrEn_Shift                                                     12

/*--------------------------------------
BitField Name: SdLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable SD-L state change event in the related line to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Bit_Start                                                 11
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Bit_End                                                   11
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Mask                                                  cBit11
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Shift                                                     11
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_MaxVal                                                   0x1
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_MinVal                                                   0x0
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SfLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable SF-L state change event in the related line to
generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Bit_Start                                                 10
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Bit_End                                                   10
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Mask                                                  cBit10
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Shift                                                     10
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_MaxVal                                                   0x1
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_MinVal                                                   0x0
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_RstVal                                                   0x0

/*--------------------------------------
BitField Name: TimLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable TIM-L state change event in the related line to
generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Bit_Start                                                 9
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Bit_End                                                   9
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Mask                                                  cBit9
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Shift                                                     9
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_MaxVal                                                  0x1
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_MinVal                                                  0x0
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: S1StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable S1 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Bit_Start                                                8
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Bit_End                                                  8
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Mask                                                 cBit8
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Shift                                                    8
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_MaxVal                                                 0x1
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_MinVal                                                 0x0
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: K1StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable K1 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Bit_Start                                                7
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Bit_End                                                  7
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Mask                                                 cBit7
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Shift                                                    7
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_MaxVal                                                 0x1
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_MinVal                                                 0x0
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: ApsLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable APS-L Defect Stable state change event in the
related line to generate an interrupt..
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Bit_Start                                                 6
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Bit_End                                                   6
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Mask                                                  cBit6
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Shift                                                     6
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_MaxVal                                                  0x1
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_MinVal                                                  0x0
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: K2StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable K2 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Bit_Start                                                5
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Bit_End                                                  5
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Mask                                                 cBit5
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Shift                                                    5
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_MaxVal                                                 0x1
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_MinVal                                                 0x0
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RdiLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable RDI-L Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Bit_Start                                                 4
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Bit_End                                                   4
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Mask                                                  cBit4
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Shift                                                     4
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_MaxVal                                                  0x1
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_MinVal                                                  0x0
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: AisLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable AIS-L Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Bit_Start                                                 3
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Bit_End                                                   3
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Mask                                                  cBit3
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Shift                                                     3
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_MaxVal                                                  0x1
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_MinVal                                                  0x0
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: OofStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable OOF Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Bit_Start                                                  2
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Bit_End                                                    2
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Mask                                                   cBit2
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Shift                                                      2
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_MaxVal                                                   0x1
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_MinVal                                                   0x0
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_RstVal                                                   0x0

/*--------------------------------------
BitField Name: LofStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable LOF Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Bit_Start                                                  1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Bit_End                                                    1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Mask                                                   cBit1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Shift                                                      1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_MaxVal                                                   0x1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_MinVal                                                   0x0
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_RstVal                                                   0x0

/*--------------------------------------
BitField Name: LosStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable LOS Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Bit_Start                                                  0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Bit_End                                                    0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Mask                                                   cBit0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Shift                                                      0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_MaxVal                                                   0x1
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_MinVal                                                   0x0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Interrupt Status
Reg Addr   : 0x24240 - 0x2426f
Reg Formula: 0x24240 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintsta_Base                                                                         0x24240
#define cAf6Reg_tohintsta(Ec1Id)                                                             (0x24240+(Ec1Id))
#define cAf6Reg_tohintsta_WidthVal                                                                          32
#define cAf6Reg_tohintsta_WriteMask                                                                        0x0

#define cAf6_tohintsta_TcaLStateChgIntr_Mask                                                             cBit12
#define cAf6_tohintsta_TcaLStateChgIntr_Shift                                                                12
/*--------------------------------------
BitField Name: SdLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while SD-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohintsta_SdLStateChgIntr_Bit_Start                                                            11
#define cAf6_tohintsta_SdLStateChgIntr_Bit_End                                                              11
#define cAf6_tohintsta_SdLStateChgIntr_Mask                                                             cBit11
#define cAf6_tohintsta_SdLStateChgIntr_Shift                                                                11
#define cAf6_tohintsta_SdLStateChgIntr_MaxVal                                                              0x1
#define cAf6_tohintsta_SdLStateChgIntr_MinVal                                                              0x0
#define cAf6_tohintsta_SdLStateChgIntr_RstVal                                                              0x0

/*--------------------------------------
BitField Name: SfLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while SF-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohintsta_SfLStateChgIntr_Bit_Start                                                            10
#define cAf6_tohintsta_SfLStateChgIntr_Bit_End                                                              10
#define cAf6_tohintsta_SfLStateChgIntr_Mask                                                             cBit10
#define cAf6_tohintsta_SfLStateChgIntr_Shift                                                                10
#define cAf6_tohintsta_SfLStateChgIntr_MaxVal                                                              0x1
#define cAf6_tohintsta_SfLStateChgIntr_MinVal                                                              0x0
#define cAf6_tohintsta_SfLStateChgIntr_RstVal                                                              0x0

/*--------------------------------------
BitField Name: TimLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while Tim-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohintsta_TimLStateChgIntr_Bit_Start                                                            9
#define cAf6_tohintsta_TimLStateChgIntr_Bit_End                                                              9
#define cAf6_tohintsta_TimLStateChgIntr_Mask                                                             cBit9
#define cAf6_tohintsta_TimLStateChgIntr_Shift                                                                9
#define cAf6_tohintsta_TimLStateChgIntr_MaxVal                                                             0x1
#define cAf6_tohintsta_TimLStateChgIntr_MinVal                                                             0x0
#define cAf6_tohintsta_TimLStateChgIntr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: S1StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable S1 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohintsta_S1StbStateChgIntr_Bit_Start                                                           8
#define cAf6_tohintsta_S1StbStateChgIntr_Bit_End                                                             8
#define cAf6_tohintsta_S1StbStateChgIntr_Mask                                                            cBit8
#define cAf6_tohintsta_S1StbStateChgIntr_Shift                                                               8
#define cAf6_tohintsta_S1StbStateChgIntr_MaxVal                                                            0x1
#define cAf6_tohintsta_S1StbStateChgIntr_MinVal                                                            0x0
#define cAf6_tohintsta_S1StbStateChgIntr_RstVal                                                            0x0

/*--------------------------------------
BitField Name: K1StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable K1 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohintsta_K1StbStateChgIntr_Bit_Start                                                           7
#define cAf6_tohintsta_K1StbStateChgIntr_Bit_End                                                             7
#define cAf6_tohintsta_K1StbStateChgIntr_Mask                                                            cBit7
#define cAf6_tohintsta_K1StbStateChgIntr_Shift                                                               7
#define cAf6_tohintsta_K1StbStateChgIntr_MaxVal                                                            0x1
#define cAf6_tohintsta_K1StbStateChgIntr_MinVal                                                            0x0
#define cAf6_tohintsta_K1StbStateChgIntr_RstVal                                                            0x0

/*--------------------------------------
BitField Name: ApsLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while APS-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohintsta_ApsLStateChgIntr_Bit_Start                                                            6
#define cAf6_tohintsta_ApsLStateChgIntr_Bit_End                                                              6
#define cAf6_tohintsta_ApsLStateChgIntr_Mask                                                             cBit6
#define cAf6_tohintsta_ApsLStateChgIntr_Shift                                                                6
#define cAf6_tohintsta_ApsLStateChgIntr_MaxVal                                                             0x1
#define cAf6_tohintsta_ApsLStateChgIntr_MinVal                                                             0x0
#define cAf6_tohintsta_ApsLStateChgIntr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: K2StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable K2 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohintsta_K2StbStateChgIntr_Bit_Start                                                           5
#define cAf6_tohintsta_K2StbStateChgIntr_Bit_End                                                             5
#define cAf6_tohintsta_K2StbStateChgIntr_Mask                                                            cBit5
#define cAf6_tohintsta_K2StbStateChgIntr_Shift                                                               5
#define cAf6_tohintsta_K2StbStateChgIntr_MaxVal                                                            0x1
#define cAf6_tohintsta_K2StbStateChgIntr_MinVal                                                            0x0
#define cAf6_tohintsta_K2StbStateChgIntr_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RdiLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while RDI-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohintsta_RdiLStateChgIntr_Bit_Start                                                            4
#define cAf6_tohintsta_RdiLStateChgIntr_Bit_End                                                              4
#define cAf6_tohintsta_RdiLStateChgIntr_Mask                                                             cBit4
#define cAf6_tohintsta_RdiLStateChgIntr_Shift                                                                4
#define cAf6_tohintsta_RdiLStateChgIntr_MaxVal                                                             0x1
#define cAf6_tohintsta_RdiLStateChgIntr_MinVal                                                             0x0
#define cAf6_tohintsta_RdiLStateChgIntr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: AisLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while AIS-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohintsta_AisLStateChgIntr_Bit_Start                                                            3
#define cAf6_tohintsta_AisLStateChgIntr_Bit_End                                                              3
#define cAf6_tohintsta_AisLStateChgIntr_Mask                                                             cBit3
#define cAf6_tohintsta_AisLStateChgIntr_Shift                                                                3
#define cAf6_tohintsta_AisLStateChgIntr_MaxVal                                                             0x1
#define cAf6_tohintsta_AisLStateChgIntr_MinVal                                                             0x0
#define cAf6_tohintsta_AisLStateChgIntr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: OofStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while OOF state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohintsta_OofStateChgIntr_Bit_Start                                                             2
#define cAf6_tohintsta_OofStateChgIntr_Bit_End                                                               2
#define cAf6_tohintsta_OofStateChgIntr_Mask                                                              cBit2
#define cAf6_tohintsta_OofStateChgIntr_Shift                                                                 2
#define cAf6_tohintsta_OofStateChgIntr_MaxVal                                                              0x1
#define cAf6_tohintsta_OofStateChgIntr_MinVal                                                              0x0
#define cAf6_tohintsta_OofStateChgIntr_RstVal                                                              0x0

/*--------------------------------------
BitField Name: LofStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while LOF state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintsta_LofStateChgIntr_Bit_Start                                                             1
#define cAf6_tohintsta_LofStateChgIntr_Bit_End                                                               1
#define cAf6_tohintsta_LofStateChgIntr_Mask                                                              cBit1
#define cAf6_tohintsta_LofStateChgIntr_Shift                                                                 1
#define cAf6_tohintsta_LofStateChgIntr_MaxVal                                                              0x1
#define cAf6_tohintsta_LofStateChgIntr_MinVal                                                              0x0
#define cAf6_tohintsta_LofStateChgIntr_RstVal                                                              0x0

/*--------------------------------------
BitField Name: LosStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while LOS state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintsta_LosStateChgIntr_Bit_Start                                                             0
#define cAf6_tohintsta_LosStateChgIntr_Bit_End                                                               0
#define cAf6_tohintsta_LosStateChgIntr_Mask                                                              cBit0
#define cAf6_tohintsta_LosStateChgIntr_Shift                                                                 0
#define cAf6_tohintsta_LosStateChgIntr_MaxVal                                                              0x1
#define cAf6_tohintsta_LosStateChgIntr_MinVal                                                              0x0
#define cAf6_tohintsta_LosStateChgIntr_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Current Status
Reg Addr   : 0x24280 - 0x242af
Reg Formula: 0x24280 + Ec1Id
    Where  : 
           + Ec1Id(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohcursta_Base                                                                         0x24280
#define cAf6Reg_tohcursta(Ec1Id)                                                             (0x24280+(Ec1Id))
#define cAf6Reg_tohcursta_WidthVal                                                                          32
#define cAf6Reg_tohcursta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: SdLDefCurStatus
BitField Type: RW
BitField Desc: SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L
defect is cleared.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohcursta_SdLDefCurStatus_Bit_Start                                                            11
#define cAf6_tohcursta_SdLDefCurStatus_Bit_End                                                              11
#define cAf6_tohcursta_SdLDefCurStatus_Mask                                                             cBit11
#define cAf6_tohcursta_SdLDefCurStatus_Shift                                                                11
#define cAf6_tohcursta_SdLDefCurStatus_MaxVal                                                              0x1
#define cAf6_tohcursta_SdLDefCurStatus_MinVal                                                              0x0
#define cAf6_tohcursta_SdLDefCurStatus_RstVal                                                              0x0

/*--------------------------------------
BitField Name: SfLDefCurStatus
BitField Type: RW
BitField Desc: SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L
defect is cleared.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohcursta_SfLDefCurStatus_Bit_Start                                                            10
#define cAf6_tohcursta_SfLDefCurStatus_Bit_End                                                              10
#define cAf6_tohcursta_SfLDefCurStatus_Mask                                                             cBit10
#define cAf6_tohcursta_SfLDefCurStatus_Shift                                                                10
#define cAf6_tohcursta_SfLDefCurStatus_MaxVal                                                              0x1
#define cAf6_tohcursta_SfLDefCurStatus_MinVal                                                              0x0
#define cAf6_tohcursta_SfLDefCurStatus_RstVal                                                              0x0

/*--------------------------------------
BitField Name: TimLDefCurStatus
BitField Type: RW
BitField Desc: TIM-L Defect  in the related line. 1: TIM-L defect is set 0:
TIM-L defect is cleared.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohcursta_TimLDefCurStatus_Bit_Start                                                            9
#define cAf6_tohcursta_TimLDefCurStatus_Bit_End                                                              9
#define cAf6_tohcursta_TimLDefCurStatus_Mask                                                             cBit9
#define cAf6_tohcursta_TimLDefCurStatus_Shift                                                                9
#define cAf6_tohcursta_TimLDefCurStatus_MaxVal                                                             0x1
#define cAf6_tohcursta_TimLDefCurStatus_MinVal                                                             0x0
#define cAf6_tohcursta_TimLDefCurStatus_RstVal                                                             0x0

/*--------------------------------------
BitField Name: S1StbStateChgIntr
BitField Type: RW
BitField Desc: S1 stable status  in the related line.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohcursta_S1StbStateChgIntr_Bit_Start                                                           8
#define cAf6_tohcursta_S1StbStateChgIntr_Bit_End                                                             8
#define cAf6_tohcursta_S1StbStateChgIntr_Mask                                                            cBit8
#define cAf6_tohcursta_S1StbStateChgIntr_Shift                                                               8
#define cAf6_tohcursta_S1StbStateChgIntr_MaxVal                                                            0x1
#define cAf6_tohcursta_S1StbStateChgIntr_MinVal                                                            0x0
#define cAf6_tohcursta_S1StbStateChgIntr_RstVal                                                            0x0

/*--------------------------------------
BitField Name: K1StbCurStatus
BitField Type: RW
BitField Desc: K1 stable status  in the related line.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohcursta_K1StbCurStatus_Bit_Start                                                              7
#define cAf6_tohcursta_K1StbCurStatus_Bit_End                                                                7
#define cAf6_tohcursta_K1StbCurStatus_Mask                                                               cBit7
#define cAf6_tohcursta_K1StbCurStatus_Shift                                                                  7
#define cAf6_tohcursta_K1StbCurStatus_MaxVal                                                               0x1
#define cAf6_tohcursta_K1StbCurStatus_MinVal                                                               0x0
#define cAf6_tohcursta_K1StbCurStatus_RstVal                                                               0x0

/*--------------------------------------
BitField Name: ApsLCurStatus
BitField Type: RW
BitField Desc: APS-L Defect  in the related line. 1: APS-L defect is set 0:
APS-L defect is cleared.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohcursta_ApsLCurStatus_Bit_Start                                                               6
#define cAf6_tohcursta_ApsLCurStatus_Bit_End                                                                 6
#define cAf6_tohcursta_ApsLCurStatus_Mask                                                                cBit6
#define cAf6_tohcursta_ApsLCurStatus_Shift                                                                   6
#define cAf6_tohcursta_ApsLCurStatus_MaxVal                                                                0x1
#define cAf6_tohcursta_ApsLCurStatus_MinVal                                                                0x0
#define cAf6_tohcursta_ApsLCurStatus_RstVal                                                                0x0

/*--------------------------------------
BitField Name: K2StbCurStatus
BitField Type: RW
BitField Desc: K2 stable status  in the related line.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohcursta_K2StbCurStatus_Bit_Start                                                              5
#define cAf6_tohcursta_K2StbCurStatus_Bit_End                                                                5
#define cAf6_tohcursta_K2StbCurStatus_Mask                                                               cBit5
#define cAf6_tohcursta_K2StbCurStatus_Shift                                                                  5
#define cAf6_tohcursta_K2StbCurStatus_MaxVal                                                               0x1
#define cAf6_tohcursta_K2StbCurStatus_MinVal                                                               0x0
#define cAf6_tohcursta_K2StbCurStatus_RstVal                                                               0x0

/*--------------------------------------
BitField Name: RdiLDefCurStatus
BitField Type: RW
BitField Desc: RDI-L Defect  in the related line. 1: RDI-L defect is set 0:
RDI-L defect is cleared.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohcursta_RdiLDefCurStatus_Bit_Start                                                            4
#define cAf6_tohcursta_RdiLDefCurStatus_Bit_End                                                              4
#define cAf6_tohcursta_RdiLDefCurStatus_Mask                                                             cBit4
#define cAf6_tohcursta_RdiLDefCurStatus_Shift                                                                4
#define cAf6_tohcursta_RdiLDefCurStatus_MaxVal                                                             0x1
#define cAf6_tohcursta_RdiLDefCurStatus_MinVal                                                             0x0
#define cAf6_tohcursta_RdiLDefCurStatus_RstVal                                                             0x0

/*--------------------------------------
BitField Name: AisLDefCurStatus
BitField Type: RW
BitField Desc: AIS-L Defect  in the related line. 1: AIS-L defect is set 0:
AIS-L defect is cleared.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohcursta_AisLDefCurStatus_Bit_Start                                                            3
#define cAf6_tohcursta_AisLDefCurStatus_Bit_End                                                              3
#define cAf6_tohcursta_AisLDefCurStatus_Mask                                                             cBit3
#define cAf6_tohcursta_AisLDefCurStatus_Shift                                                                3
#define cAf6_tohcursta_AisLDefCurStatus_MaxVal                                                             0x1
#define cAf6_tohcursta_AisLDefCurStatus_MinVal                                                             0x0
#define cAf6_tohcursta_AisLDefCurStatus_RstVal                                                             0x0

/*--------------------------------------
BitField Name: OofCurStatus
BitField Type: RW
BitField Desc: OOF current status in the related line. 1: OOF state 0: Not OOF
state.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohcursta_OofCurStatus_Bit_Start                                                                2
#define cAf6_tohcursta_OofCurStatus_Bit_End                                                                  2
#define cAf6_tohcursta_OofCurStatus_Mask                                                                 cBit2
#define cAf6_tohcursta_OofCurStatus_Shift                                                                    2
#define cAf6_tohcursta_OofCurStatus_MaxVal                                                                 0x1
#define cAf6_tohcursta_OofCurStatus_MinVal                                                                 0x0
#define cAf6_tohcursta_OofCurStatus_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: LofCurStatus
BitField Type: RW
BitField Desc: LOF current status in the related line. 1: LOF state 0: Not LOF
state.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohcursta_LofCurStatus_Bit_Start                                                                1
#define cAf6_tohcursta_LofCurStatus_Bit_End                                                                  1
#define cAf6_tohcursta_LofCurStatus_Mask                                                                 cBit1
#define cAf6_tohcursta_LofCurStatus_Shift                                                                    1
#define cAf6_tohcursta_LofCurStatus_MaxVal                                                                 0x1
#define cAf6_tohcursta_LofCurStatus_MinVal                                                                 0x0
#define cAf6_tohcursta_LofCurStatus_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: LosCurStatus
BitField Type: RW
BitField Desc: LOS current status in the related line. 1: LOS state 0: Not LOS
state.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohcursta_LosCurStatus_Bit_Start                                                                0
#define cAf6_tohcursta_LosCurStatus_Bit_End                                                                  0
#define cAf6_tohcursta_LosCurStatus_Mask                                                                 cBit0
#define cAf6_tohcursta_LosCurStatus_Shift                                                                    0
#define cAf6_tohcursta_LosCurStatus_MaxVal                                                                 0x1
#define cAf6_tohcursta_LosCurStatus_MinVal                                                                 0x0
#define cAf6_tohcursta_LosCurStatus_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line Interrupt Enable Control 1
Reg Addr   : 0x242f0
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 bits for 24 lines (EC1 Id: 0-23) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperlineenctl1_Base                                                               0x242f0
#define cAf6Reg_tohintperlineenctl1                                                                    0x242f0
#define cAf6Reg_tohintperlineenctl1_WidthVal                                                                32
#define cAf6Reg_tohintperlineenctl1_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: OCNRxLLineIntrEn1
BitField Type: RW
BitField Desc: Bit #0 to enable for line #0.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_Bit_Start                                                 0
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_Bit_End                                                  23
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_Mask                                               cBit23_0
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_Shift                                                     0
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_MaxVal                                             0xffffff
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_MinVal                                                  0x0
#define cAf6_tohintperlineenctl1_OCNRxLLineIntrEn1_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line Interrupt Enable Control 2
Reg Addr   : 0x242f1
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 bits for 24 lines (EC1 Id: 24-47) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperlineenctl2_Base                                                               0x242f1
#define cAf6Reg_tohintperlineenctl2                                                                    0x242f1
#define cAf6Reg_tohintperlineenctl2_WidthVal                                                                32
#define cAf6Reg_tohintperlineenctl2_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: OCNRxLLineIntrEn2
BitField Type: RW
BitField Desc: Bit #0 to enable for line #24.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_Bit_Start                                                 0
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_Bit_End                                                  23
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_Mask                                               cBit23_0
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_Shift                                                     0
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_MaxVal                                             0xffffff
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_MinVal                                                  0x0
#define cAf6_tohintperlineenctl2_OCNRxLLineIntrEn2_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Line Interrupt OR Status 1
Reg Addr   : 0x242f2
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 bits for 24 lines (EC1 Id: 0-23) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperline1_Base                                                                    0x242f2
#define cAf6Reg_tohintperline1                                                                         0x242f2
#define cAf6Reg_tohintperline1_WidthVal                                                                     32
#define cAf6Reg_tohintperline1_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: OCNRxLLineIntr1
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be
set and they are enabled to raise interrupt. Bit 0 for line #0, respectively.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_tohintperline1_OCNRxLLineIntr1_Bit_Start                                                        0
#define cAf6_tohintperline1_OCNRxLLineIntr1_Bit_End                                                         23
#define cAf6_tohintperline1_OCNRxLLineIntr1_Mask                                                      cBit23_0
#define cAf6_tohintperline1_OCNRxLLineIntr1_Shift                                                            0
#define cAf6_tohintperline1_OCNRxLLineIntr1_MaxVal                                                    0xffffff
#define cAf6_tohintperline1_OCNRxLLineIntr1_MinVal                                                         0x0
#define cAf6_tohintperline1_OCNRxLLineIntr1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Line Interrupt OR Status 2
Reg Addr   : 0x242f3
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 bits for 24 lines (EC1 Id: 24-47) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperline2_Base                                                                    0x242f3
#define cAf6Reg_tohintperline2                                                                         0x242f3
#define cAf6Reg_tohintperline2_WidthVal                                                                     32
#define cAf6Reg_tohintperline2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: OCNRxLLineIntr2
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be
set and they are enabled to raise interrupt. Bit 0 for line #24, respectively.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_tohintperline2_OCNRxLLineIntr2_Bit_Start                                                        0
#define cAf6_tohintperline2_OCNRxLLineIntr2_Bit_End                                                         23
#define cAf6_tohintperline2_OCNRxLLineIntr2_Mask                                                      cBit23_0
#define cAf6_tohintperline2_OCNRxLLineIntr2_Shift                                                            0
#define cAf6_tohintperline2_OCNRxLLineIntr2_MaxVal                                                    0xffffff
#define cAf6_tohintperline2_OCNRxLLineIntr2_MinVal                                                         0x0
#define cAf6_tohintperline2_OCNRxLLineIntr2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Slice24 Interrupt OR Status
Reg Addr   : 0x242f4
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 slice at Rx side. Each bit is used to store Interrupt OR status of the related slice24.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperslice_Base                                                                    0x242f4
#define cAf6Reg_tohintperslice                                                                         0x242f4
#define cAf6Reg_tohintperslice_WidthVal                                                                     32
#define cAf6Reg_tohintperslice_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: OCNRxLSliceIntr2
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx Line per Line Interrupt OR Status 2 register of the related Slice24
#1 to be set and they are enabled to raise interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintperslice_OCNRxLSliceIntr2_Bit_Start                                                       1
#define cAf6_tohintperslice_OCNRxLSliceIntr2_Bit_End                                                         1
#define cAf6_tohintperslice_OCNRxLSliceIntr2_Mask                                                        cBit1
#define cAf6_tohintperslice_OCNRxLSliceIntr2_Shift                                                           1
#define cAf6_tohintperslice_OCNRxLSliceIntr2_MaxVal                                                        0x1
#define cAf6_tohintperslice_OCNRxLSliceIntr2_MinVal                                                        0x0
#define cAf6_tohintperslice_OCNRxLSliceIntr2_RstVal                                                        0x0

/*--------------------------------------
BitField Name: OCNRxLSliceIntr1
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx Line per Line Interrupt OR Status 1 register of the related Slice24
#1 to be set and they are enabled to raise interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintperslice_OCNRxLSliceIntr1_Bit_Start                                                       0
#define cAf6_tohintperslice_OCNRxLSliceIntr1_Bit_End                                                         0
#define cAf6_tohintperslice_OCNRxLSliceIntr1_Mask                                                        cBit0
#define cAf6_tohintperslice_OCNRxLSliceIntr1_Shift                                                           0
#define cAf6_tohintperslice_OCNRxLSliceIntr1_MaxVal                                                        0x1
#define cAf6_tohintperslice_OCNRxLSliceIntr1_MinVal                                                        0x0
#define cAf6_tohintperslice_OCNRxLSliceIntr1_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Channel Control
Reg Addr   : 0x21000 - 0x2142f
Reg Formula: 0x21000 + 1024*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmramctl_Base                                                                         0x21000
#define cAf6Reg_tfmramctl(SliceId, StsId)                                     (0x21000+1024*(SliceId)+(StsId))
#define cAf6Reg_tfmramctl_WidthVal                                                                          32
#define cAf6Reg_tfmramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: OCNTxS1Pat
BitField Type: RW
BitField Desc: Pattern to insert into S1 byte.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxS1Pat_Bit_Start                                                                 24
#define cAf6_tfmramctl_OCNTxS1Pat_Bit_End                                                                   31
#define cAf6_tfmramctl_OCNTxS1Pat_Mask                                                               cBit31_24
#define cAf6_tfmramctl_OCNTxS1Pat_Shift                                                                     24
#define cAf6_tfmramctl_OCNTxS1Pat_MaxVal                                                                  0xff
#define cAf6_tfmramctl_OCNTxS1Pat_MinVal                                                                   0x0
#define cAf6_tfmramctl_OCNTxS1Pat_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: OCNTxApsPat
BitField Type: RW
BitField Desc: Pattern to insert into APS bytes (K1, K2).
BitField Bits: [23:8]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxApsPat_Bit_Start                                                                 8
#define cAf6_tfmramctl_OCNTxApsPat_Bit_End                                                                  23
#define cAf6_tfmramctl_OCNTxApsPat_Mask                                                               cBit23_8
#define cAf6_tfmramctl_OCNTxApsPat_Shift                                                                     8
#define cAf6_tfmramctl_OCNTxApsPat_MaxVal                                                               0xffff
#define cAf6_tfmramctl_OCNTxApsPat_MinVal                                                                  0x0
#define cAf6_tfmramctl_OCNTxApsPat_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: OCNTxS1LDis
BitField Type: RW
BitField Desc: S1 insertion disable 1: Disable 0: Enable.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxS1LDis_Bit_Start                                                                 7
#define cAf6_tfmramctl_OCNTxS1LDis_Bit_End                                                                   7
#define cAf6_tfmramctl_OCNTxS1LDis_Mask                                                                  cBit7
#define cAf6_tfmramctl_OCNTxS1LDis_Shift                                                                     7
#define cAf6_tfmramctl_OCNTxS1LDis_MaxVal                                                                  0x1
#define cAf6_tfmramctl_OCNTxS1LDis_MinVal                                                                  0x0
#define cAf6_tfmramctl_OCNTxS1LDis_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: OCNTxApsEnb
BitField Type: RW
BitField Desc: Enable to process APS 1: Enable 0: Disable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxApsEnb_Bit_Start                                                                 6
#define cAf6_tfmramctl_OCNTxApsEnb_Bit_End                                                                   6
#define cAf6_tfmramctl_OCNTxApsEnb_Mask                                                                  cBit6
#define cAf6_tfmramctl_OCNTxApsEnb_Shift                                                                     6
#define cAf6_tfmramctl_OCNTxApsEnb_MaxVal                                                                  0x1
#define cAf6_tfmramctl_OCNTxApsEnb_MinVal                                                                  0x0
#define cAf6_tfmramctl_OCNTxApsEnb_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: OCNTxReiLDis
BitField Type: RW
BitField Desc: Auto REI_L insertion disable 1: Disable inserting REI_L
automatically. 0: Enable automatically inserting REI_L.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxReiLDis_Bit_Start                                                                5
#define cAf6_tfmramctl_OCNTxReiLDis_Bit_End                                                                  5
#define cAf6_tfmramctl_OCNTxReiLDis_Mask                                                                 cBit5
#define cAf6_tfmramctl_OCNTxReiLDis_Shift                                                                    5
#define cAf6_tfmramctl_OCNTxReiLDis_MaxVal                                                                 0x1
#define cAf6_tfmramctl_OCNTxReiLDis_MinVal                                                                 0x0
#define cAf6_tfmramctl_OCNTxReiLDis_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: OCNTxRdiLDis
BitField Type: RW
BitField Desc: Auto RDI_L insertion disable 1: Disable inserting RDI_L
automatically. 0: Enable automatically inserting RDI_L.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxRdiLDis_Bit_Start                                                                4
#define cAf6_tfmramctl_OCNTxRdiLDis_Bit_End                                                                  4
#define cAf6_tfmramctl_OCNTxRdiLDis_Mask                                                                 cBit4
#define cAf6_tfmramctl_OCNTxRdiLDis_Shift                                                                    4
#define cAf6_tfmramctl_OCNTxRdiLDis_MaxVal                                                                 0x1
#define cAf6_tfmramctl_OCNTxRdiLDis_MinVal                                                                 0x0
#define cAf6_tfmramctl_OCNTxRdiLDis_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: OCNTxAutoB2Dis
BitField Type: RW
BitField Desc: Auto B2 disable 1: Disable inserting calculated B2 values into B2
positions automatically. 0: Enable automatically inserting calculated B2 values
into B2 positions.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxAutoB2Dis_Bit_Start                                                              3
#define cAf6_tfmramctl_OCNTxAutoB2Dis_Bit_End                                                                3
#define cAf6_tfmramctl_OCNTxAutoB2Dis_Mask                                                               cBit3
#define cAf6_tfmramctl_OCNTxAutoB2Dis_Shift                                                                  3
#define cAf6_tfmramctl_OCNTxAutoB2Dis_MaxVal                                                               0x1
#define cAf6_tfmramctl_OCNTxAutoB2Dis_MinVal                                                               0x0
#define cAf6_tfmramctl_OCNTxAutoB2Dis_RstVal                                                               0x0

/*--------------------------------------
BitField Name: OCNTxRdiLThresSel
BitField Type: RW
BitField Desc: Select the number of frames being inserted RDI-L defects when
receive direction requests generating RDI-L at transmit direction. 1: Threshold2
is selected. 0: Threshold1 is selected.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxRdiLThresSel_Bit_Start                                                           2
#define cAf6_tfmramctl_OCNTxRdiLThresSel_Bit_End                                                             2
#define cAf6_tfmramctl_OCNTxRdiLThresSel_Mask                                                            cBit2
#define cAf6_tfmramctl_OCNTxRdiLThresSel_Shift                                                               2
#define cAf6_tfmramctl_OCNTxRdiLThresSel_MaxVal                                                            0x1
#define cAf6_tfmramctl_OCNTxRdiLThresSel_MinVal                                                            0x0
#define cAf6_tfmramctl_OCNTxRdiLThresSel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: OCNTxRdiLFrc
BitField Type: RW
BitField Desc: RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not
force RDI-L
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxRdiLFrc_Bit_Start                                                                1
#define cAf6_tfmramctl_OCNTxRdiLFrc_Bit_End                                                                  1
#define cAf6_tfmramctl_OCNTxRdiLFrc_Mask                                                                 cBit1
#define cAf6_tfmramctl_OCNTxRdiLFrc_Shift                                                                    1
#define cAf6_tfmramctl_OCNTxRdiLFrc_MaxVal                                                                 0x1
#define cAf6_tfmramctl_OCNTxRdiLFrc_MinVal                                                                 0x0
#define cAf6_tfmramctl_OCNTxRdiLFrc_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: OCNTxAisLFrc
BitField Type: RW
BitField Desc: AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not
force AIS-L
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfmramctl_OCNTxAisLFrc_Bit_Start                                                                0
#define cAf6_tfmramctl_OCNTxAisLFrc_Bit_End                                                                  0
#define cAf6_tfmramctl_OCNTxAisLFrc_Mask                                                                 cBit0
#define cAf6_tfmramctl_OCNTxAisLFrc_Shift                                                                    0
#define cAf6_tfmramctl_OCNTxAisLFrc_MaxVal                                                                 0x1
#define cAf6_tfmramctl_OCNTxAisLFrc_MinVal                                                                 0x0
#define cAf6_tfmramctl_OCNTxAisLFrc_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx J0 Insertion Buffer
Reg Addr   : 0x21200 - 0x2162f
Reg Formula: 0x21200 + 1024*SliceId + 16*StsId + DwId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
           + DwId(0-15): Double word (4bytes) ID
Reg Desc   : 
Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmj0insctl_Base                                                                       0x21200
#define cAf6Reg_tfmj0insctl(SliceId, StsId, DwId)                     (0x21200+1024*(SliceId)+16*(StsId)+(DwId))
#define cAf6Reg_tfmj0insctl_WidthVal                                                                        32
#define cAf6Reg_tfmj0insctl_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: OCNTxJ0
BitField Type: RW
BitField Desc: J0 pattern for insertion.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tfmj0insctl_OCNTxJ0_Bit_Start                                                                   0
#define cAf6_tfmj0insctl_OCNTxJ0_Bit_End                                                                    31
#define cAf6_tfmj0insctl_OCNTxJ0_Mask                                                                 cBit31_0
#define cAf6_tfmj0insctl_OCNTxJ0_Shift                                                                       0
#define cAf6_tfmj0insctl_OCNTxJ0_MaxVal                                                             0xffffffff
#define cAf6_tfmj0insctl_OCNTxJ0_MinVal                                                                    0x0
#define cAf6_tfmj0insctl_OCNTxJ0_RstVal                                                                    0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEOCNREG_H_ */


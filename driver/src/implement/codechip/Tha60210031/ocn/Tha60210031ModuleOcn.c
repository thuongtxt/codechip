/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60210031ModuleOcn.c
 *
 * Created Date: May 6, 2015
 *
 * Description : OCN Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210031ModuleOcnReg.h"
#include "Tha60210031ModuleOcn.h"
#include "Tha60210031ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((Tha60210031ModuleOcn)self)

/* line loopback reg, mask ,shift */
#define cAf6Reg_linetslblc_reg 0x17
#define cAf6Reg_linetslblc_Mask(lineId) (cBit0 << (lineId))
#define cAf6Reg_linetslblc_Shift(lineId) (lineId)

#define cAf6Reg_linelbrm_reg  0x19
#define cAf6Reg_linelbrm_Mask(lineId) (cBit0 << (lineId))
#define cAf6Reg_linelbrm_Shift(lineId) (lineId)

#define cAf6Reg_GlbLineTxLAisFrcLclb0 0x1b
#define cAf6Reg_lineTxAisForce_Mask(lineId) (cBit0 << (lineId))
#define cAf6Reg_lineTxAisForce_Shift(lineId) (lineId)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031ModuleOcnMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;

/* Super implementation */
static const tThaModuleOcnMethods   *m_ThaModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleOcn);
    }

static eAtRet ChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint8 lineId = AtSdhChannelLineGet(channel);
    uint8 numStsInSlice = (mMethodsGet(self))->NumStsInOneSlice(self);

    AtUnused(phyModule);
    AtUnused(swSts);
    *sliceId = lineId / numStsInSlice;
    *hwStsInSlice = lineId % numStsInSlice;

    return cAtOk;
    }

static uint8 NumSlices(ThaModuleOcn self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 NumStsInOneSlice(ThaModuleOcn self)
    {
    AtUnused(self);
    return 24;
    }

static void RxDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr  = cAf6Reg_glbrfm_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmAislRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmTimRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmOofRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLofRdiLEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLosRdiLEn_, 1);

    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmAislAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmTimAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmOofAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLofAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLosAisPEn_, 1);

    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLofCfg_, 0);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLosDetDis_, 0);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmLosCfg_, 1);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmDescrEn_, 1);

    mModuleHwWrite(self, regAddr, regVal);
    }

static uint32 HwPpmThresholdDefault(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 affectedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4635);

    if (currentVersion >= affectedVersion)
        return 0x96;

    return 0x20;
    }

static void RxThresholdDefaultSet(ThaModuleOcn self)
    {
    Tha60210011ModuleOcn moduleOcn = (Tha60210011ModuleOcn)self;
    uint32 regAddr  = cAf6Reg_glblosthr_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    uint32 regVal = 0;
    uint32 ppmMonEn = mMethodsGet(moduleOcn)->ShouldEnableLosPpmDetection(moduleOcn) ? 0 : 1;

    /* Enable to detect LOS when signal PPM is greater than 20PPM */
    mRegFieldSet(regVal, cAf6_glblosthr_reg_RxFrmNumValMonDis_, ppmMonEn);
    mRegFieldSet(regVal, cAf6_glblosthr_reg_RxFrmNumValMonThr_, HwPpmThresholdDefault(self));

    mRegFieldSet(regVal, cAf6_glblosthr_reg_RxFrmLosClr2Thr_, 0x8);
    mRegFieldSet(regVal, cAf6_glblosthr_reg_RxFrmLosSetThr_, 0x28);
    mRegFieldSet(regVal, cAf6_glblosthr_reg_RxFrmLosClr1Thr_, 0x51);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr  = cAf6Reg_glblofthr_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_glblofthr_reg_RxFrmLofSetThr_, 0x18);
    mRegFieldSet(regVal, cAf6_glblofthr_reg_RxFrmLofClrThr_, 0x18);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void TxDefaultSet(ThaModuleOcn self)
    {
    uint32 regVal = 0;
    uint32 regAddr  = cAf6Reg_glbtfm_reg_Base + Tha60210011ModuleOcnBaseAddress(self);

    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxLineOofFrc_, 0);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxLineB1errFrc_, 0);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxLineScrEn_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_glbclksel_reg1_Base + Tha60210011ModuleOcnBaseAddress(self);
    mModuleHwWrite(self, regAddr, 0);
    regAddr = cAf6Reg_glbclksel_reg2_Base + Tha60210011ModuleOcnBaseAddress(self);
    mModuleHwWrite(self, regAddr, 0);
    regAddr = cAf6Reg_glbclklooptime_reg1 + Tha60210011ModuleOcnBaseAddress(self);
    mModuleHwWrite(self, regAddr, 0);
    regAddr = cAf6Reg_glbclklooptime_reg2 + Tha60210011ModuleOcnBaseAddress(self);
    mModuleHwWrite(self, regAddr, 0);

    regVal = 0;
    regAddr  = cAf6Reg_glbrdiinsthr_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    mRegFieldSet(regVal, cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_, 0x8);
    mRegFieldSet(regVal, cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_, 0x14);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void LineTimingEnable(AtChannel self, uint32 address, eBool enable)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 regVal, mask, shift;
    uint32 regAddr  = (lineId > 23) ? (address + 1) : address;

    mask = cBit0 << (lineId % 24);
    shift = lineId % 24;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, mask, shift, enable ? 1:0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    }

static eBool LineTimingIsEnabled(AtChannel self, uint32 address)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    uint32 regVal, mask, shift;
    uint32 regAddr  = (lineId > 23) ? (address + 1): address;
    uint8 ret;

    mask = cBit0 << (lineId % 24);
    shift = lineId % 24;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldGet(regVal, mask, shift, uint8, &ret);

    return (ret == 1) ? cAtTrue:cAtFalse;
    }

static uint32 StartVersionNeedHwRxPiPjAdjThreshold(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4643);
    }

static uint32 HwRxPiPjAdjThresholdDefaultRegUpdate(ThaModuleOcn self, uint32 hwRegVal)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)self;
    uint32 startVersionHasThis = mMethodsGet(ocnModule)->StartVersionNeedHwRxPiPjAdjThreshold(ocnModule);
    uint32 newRegVal = hwRegVal;

    if (currentVersion >= startVersionHasThis)
        {
        mRegFieldSet(newRegVal, cAf6_glbspi_reg_StsPiAdjCntSel_, 0);
        mRegFieldSet(newRegVal, cAf6_glbspi_reg_RxPgNorPtrThresh_, 3);
        }

    return newRegVal;
    }

static void PointerDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr = Tha60210011ModuleOcnGlbspiBase(self) + Tha60210011ModuleOcnBaseAddress(self);
    uint32 regVal = 0;

    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgFlowThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_RxPgAdjThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiAisAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiLopAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiMajorMode_, 1);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiNdfPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiBadPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbspi_reg_StsPiPohAisType_, 0xF);
    regVal = HwRxPiPjAdjThresholdDefaultRegUpdate(self, regVal);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Tha60210011ModuleOcnGlbvpiBase(self) + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomInvlCntMod_, 0);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomGoodThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLomInvlThresh_, 0xC);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiAisAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiLopAisPEn_, 1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiMajorMode_, 1);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiNdfPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiBadPtrThresh_, 8);
    mRegFieldSet(regVal, cAf6_glbvpi_reg_VtPiPohAisType_, 0xF);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Tha60210011ModuleOcnGlbtpgBase(self) + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_glbtpg_reg_TxPgNorPtrThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbtpg_reg_TxPgFlowThresh_, 3);
    mRegFieldSet(regVal, cAf6_glbtpg_reg_TxPgAdjThresh_, 0xC);
    mModuleHwWrite(self, regAddr, regVal);
    }

static eAtRet TohThresholdDefaultSet(ThaModuleOcn self)
    {
    uint32 regAddr = ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(self) + Tha60210011ModuleOcnBaseAddress(self);
    uint32 regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(self) + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(self) + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr2_, 5);
    mRegFieldSet(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr1_, 5);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbrdidetthr_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_, 0x4);
    mRegFieldSet(regVal, cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_, 0x9);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbaisdetthr_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbaisdetthr_reg_TohAisDetThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbaisdetthr_reg_TohAisDetThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbk1smpthr_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_, 7);
    mRegFieldSet(regVal, cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_, 7);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglberrcntmod_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohReiErrCntMod_, 1);
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbaffen_reg_Base + Tha60210011ModuleOcnBaseAddress(self);
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffStbMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffRdilMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffAislMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffErrCnt_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret = cAtOk;
    RxDefaultSet(self);
    RxThresholdDefaultSet(self);
    TxDefaultSet(self);
    PointerDefaultSet(self);
    ret |= mMethodsGet(self)->TohThresholdDefaultSet(self);

    return ret;
    }

static eAtRet PiStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(masterStsId);
    return cAtOk;
    }

static eAtRet PiStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(masterStsId);
    AtUnused(slaveStsId);
    return cAtOk;
    }

static eAtRet StsConcatRelease(ThaModuleOcn self, AtSdhChannel aug, uint8 sts1Id)
    {
    AtUnused(self);
    AtUnused(aug);
    AtUnused(sts1Id);
    return cAtOk;
    }

static eBool StsIsSlaveInChannel(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    return cAtFalse;
    }

static eAtRet PgStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(masterStsId);
    return cAtOk;
    }

static eAtRet PgStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(masterStsId);
    AtUnused(slaveStsId);
    return cAtOk;
    }

static eAtRet PgSlaveIndicate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(stsId);
    AtUnused(isSlave);
    return cAtOk;
    }

static uint32 BaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0x0A00000;
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    AtUnused(self);
    AtUnused(ramId);
    AtUnused(localRamId);
    return NULL;
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    /* Not now */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbs1stbthr_reg_Base;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk1stbthr_reg_Base;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk2stbthr_reg_Base;
    }

static eAtRet Vc3ToTu3VcEnable(ThaModuleOcn self, AtSdhChannel vc3, eBool enable)
    {
    uint32 regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) +
                     Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);

    mRegFieldSet(regVal, cAf6_demramctl_PiDemStsDs3oTu3Cep_, enable ? 1 : 0);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool Vc3ToTu3VcIsEnabled(ThaModuleOcn self, AtSdhChannel vc3)
    {
    uint32 regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) +
                     Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);

    return mRegField(regVal, cAf6_demramctl_PiDemStsDs3oTu3Cep_) ? cAtTrue : cAtFalse;
    }

static ThaModuleOcn ModuleOcn(AtChannel line)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(line), cThaModuleOcn);
    }

static uint32 LineGroupId(AtChannel line)
    {
    return (AtChannelIdGet(line) / 24);
    }

static uint32 LocalLineIdInGroup(AtChannel line)
    {
    return (AtChannelIdGet(line) % 24);
    }

static uint32 LineLocalLoopbackAddress(AtChannel line)
    {
    Tha60210031ModuleOcn module = (Tha60210031ModuleOcn)ModuleOcn(line);
    uint32 regAddr = mMethodsGet(module)->LineLocalLoopbackRegAddr(module, (AtSdhLine)line);
    return regAddr + ThaModuleOcnBaseAddress((ThaModuleOcn)module);
    }

static uint32 LineRemoteLoopbackAddress(AtChannel line)
    {
    Tha60210031ModuleOcn module = (Tha60210031ModuleOcn)ModuleOcn(line);
    uint32 regAddr = mMethodsGet(module)->LineRemoteLoopbackRegAddr(module, (AtSdhLine)line);
    return regAddr + ThaModuleOcnBaseAddress((ThaModuleOcn)module);
    }

static uint32 LineTxAisForceAddress(AtChannel self)
    {
    ThaModuleOcn module = ModuleOcn(self);
    return cAf6Reg_GlbLineTxLAisFrcLclb0 + LineGroupId(self) + ThaModuleOcnBaseAddress(module);
    }

static eAtRet HwLineBitSet(AtChannel self, uint32 address, eBool enable)
    {
    ThaModuleOcn module = ModuleOcn(self);
    uint32 mask, shift, regValue, enableBinary;
    uint32 localLineId = LocalLineIdInGroup(self);

    mask = cAf6Reg_linetslblc_Mask(localLineId);
    shift = cAf6Reg_linetslblc_Shift(localLineId);
    enableBinary = enable ? 1 : 0;
    regValue = mModuleHwRead(module, address);
    mFieldIns(&regValue, mask, shift, enableBinary);
    mModuleHwWrite(module, address, regValue);
    return cAtOk;
    }

static eBool HwLineBitIsSet(AtChannel self, uint32 address)
    {
    uint32 channelMask, channelShift, regValue;
    uint32 localLineId = LocalLineIdInGroup(self);

    channelMask = cAf6Reg_linetslblc_Mask(localLineId);
    channelShift = cAf6Reg_linetslblc_Shift(localLineId);
    regValue = mChannelHwRead(self, address, cThaModuleOcn);
    return mRegField(regValue, channel) ? cAtTrue : cAtFalse;
    }

static eAtRet LineLocalLoopEnable(AtChannel self, eBool enable)
    {
    return HwLineBitSet(self, LineLocalLoopbackAddress(self), enable);
    }

static eBool LineLocalLoopIsEnabled(AtChannel self)
    {
    return HwLineBitIsSet(self, LineLocalLoopbackAddress(self));
    }

static eAtRet LineRemoteLoopEnable(AtChannel self, eBool enable)
    {
    return HwLineBitSet(self, LineRemoteLoopbackAddress(self), enable);
    }

static eBool LineRemoteLoopIsEnabled(AtChannel self)
    {
    return HwLineBitIsSet(self, LineRemoteLoopbackAddress(self));
    }

static eAtRet LineTxAisForce(AtChannel self, eBool enable)
    {
    return HwLineBitSet(self, LineTxAisForceAddress(self), enable);
    }

static eBool LineTxAisIsForced(AtChannel self)
    {
    return HwLineBitIsSet(self, LineTxAisForceAddress(self));
    }

static eAtRet LineIdSw2HwGet(ThaModuleOcn self, AtSdhLine sdhLine, eAtModule moduleId, uint8* sliceId, uint8 *hwLineInSlice)
    {
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)sdhLine);
    uint8 phyModule = moduleId;
    AtUnused(self);

    if (moduleId == cAtModuleSur)
        {
        *sliceId = (uint8)(lineId & 0x1);
        *hwLineInSlice = (uint8)(lineId >> 1);
        return cAtOk;
        }

    /* OCN */
    *sliceId = lineId / 24U;
    *hwLineInSlice = lineId % 24U;

    /* EC1 POH starting at slice 2 */
    if (phyModule == cThaModulePoh)
        *sliceId = (uint8)(*sliceId + 2U);

    return cAtOk;
    }

static eAtRet StsLoopbackSet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 loopbackMode)
    {
    AtUnused(stsId);
    return m_ThaModuleOcnMethods->VtTuLoopbackSet(self, channel, loopbackMode);
    }

static uint8 StsLoopbackGet(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId)
    {
    AtUnused(stsId);
    return m_ThaModuleOcnMethods->VtTuLoopbackGet(self, channel);
    }

static eBool ShouldEnableLosPpmDetection(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 De3OverSdhControlRegister(ThaModuleOcn self, uint32 slice)
    {
    return (uint32)cAf6Reg_glbds3osdh_reg1_Base + slice + ThaModuleOcnBaseAddress(self);
    }

static eAtRet Vc3MapDe3LiuEnable(ThaModuleOcn self, AtSdhChannel sdhVc, eBool enable)
    {
    uint8 slice, hwStsInSlice;
    uint32 regAddr, regVal;
    uint32 enable_Mask, enable_Shift;

    ThaSdhChannel2HwMasterStsId(sdhVc, cThaModuleOcn, &slice, &hwStsInSlice);

    enable_Mask = (cBit0 << hwStsInSlice);
    enable_Shift = hwStsInSlice;

    regAddr = De3OverSdhControlRegister(self, slice);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, enable_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool Vc3MapDe3LiuIsEnabled(ThaModuleOcn self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwStsInSlice;
    uint32 regAddr, regVal;
    uint32 enable_Mask, enable_Shift;

    ThaSdhChannel2HwMasterStsId(sdhVc, cThaModuleOcn, &slice, &hwStsInSlice);

    enable_Mask = (cBit0 << hwStsInSlice);
    enable_Shift = hwStsInSlice;

    regAddr = De3OverSdhControlRegister(self, slice);
    regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, enable_) ? cAtTrue : cAtFalse;
    }

static uint32 LineLocalLoopbackRegAddr(Tha60210031ModuleOcn self, AtSdhLine line)
    {
    AtUnused(self);
    return cAf6Reg_linetslblc_reg + LineGroupId((AtChannel)line);
    }

static uint32 LineRemoteLoopbackRegAddr(Tha60210031ModuleOcn self, AtSdhLine line)
    {
    AtUnused(self);
    return cAf6Reg_linelbrm_reg + LineGroupId((AtChannel)line);
    }

static uint32 LineDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    return AtChannelIdGet((AtChannel)sdhLine) + ThaModuleOcnBaseAddress(self);
    }

static uint32 LineTxDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    uint8 sliceId, stsId;

    if (ThaSdhLineHwIdGet((AtSdhLine)sdhLine, cThaModuleOcn, &sliceId, &stsId) != cAtOk)
        return cInvalidUint32;

    return 1024UL * sliceId + stsId + ThaModuleOcnBaseAddress(self);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, StartVersionNeedHwRxPiPjAdjThreshold);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, ShouldEnableLosPpmDetection);
        }

    mMethodsSet(ocnModule, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, ChannelStsIdSw2HwGet);
        mMethodOverride(m_ThaModuleOcnOverride, NumSlices);
        mMethodOverride(m_ThaModuleOcnOverride, NumStsInOneSlice);
        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, TohThresholdDefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, PiStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, PiStsConcatSlaveSet);
        mMethodOverride(m_ThaModuleOcnOverride, StsConcatRelease);
        mMethodOverride(m_ThaModuleOcnOverride, StsIsSlaveInChannel);
        mMethodOverride(m_ThaModuleOcnOverride, PgStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, PgStsConcatSlaveSet);
        mMethodOverride(m_ThaModuleOcnOverride, PgSlaveIndicate);
        mMethodOverride(m_ThaModuleOcnOverride, BaseAddress);
        mMethodOverride(m_ThaModuleOcnOverride, CanMovePathAisRxForcingPoint);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcEnable);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, LineIdSw2HwGet);
        mMethodOverride(m_ThaModuleOcnOverride, StsLoopbackSet);
        mMethodOverride(m_ThaModuleOcnOverride, StsLoopbackGet);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3MapDe3LiuEnable);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3MapDe3LiuIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, LineDefaultOffset);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxDefaultOffset);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    }

static void MethodsInit(Tha60210031ModuleOcn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LineLocalLoopbackRegAddr);
        mMethodOverride(m_methods, LineRemoteLoopbackRegAddr);
        }

    mMethodsSet(self, &m_methods);
    }

AtModule Tha60210031ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((Tha60210031ModuleOcn)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModuleOcnNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleOcnObjectInit(newModule, device);
    }

void Tha60210031OcnLineTimingModeSet(AtChannel line, eAtTimingMode timingMode)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)line), cThaModuleOcn);
    uint32 extAddress  = cAf6Reg_glbclksel_reg1_Base + Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 looptimeAddr = cAf6Reg_glbclklooptime_reg1 + Tha60210011ModuleOcnBaseAddress(ocnModule);

    switch ((uint32)timingMode)
        {
        case cAtTimingModeSys:
            LineTimingEnable(line, extAddress, cAtFalse);
            LineTimingEnable(line, looptimeAddr, cAtFalse);
            break;

        case cAtTimingModeLoop:
            LineTimingEnable(line, extAddress, cAtFalse);
            LineTimingEnable(line, looptimeAddr, cAtTrue);
            break;

        case cAtTimingModeExt1Ref:
            LineTimingEnable(line, extAddress, cAtTrue);
            LineTimingEnable(line, looptimeAddr, cAtFalse);
            break;

        default:
            break;
        }
    }

eAtTimingMode Tha60210031OcnLineTimingModeGet(AtChannel line)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)line), cThaModuleOcn);
    uint32 ext1Address  = cAf6Reg_glbclksel_reg1_Base + Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 looltimeAddress = cAf6Reg_glbclklooptime_reg1 + Tha60210011ModuleOcnBaseAddress(ocnModule);
    eBool ext1 = LineTimingIsEnabled(line, ext1Address);
    eBool loop = LineTimingIsEnabled(line, looltimeAddress);

    if (!ext1 && !loop)
        return cAtTimingModeSys;

    if (loop && !ext1)
        return cAtTimingModeLoop;

    if (ext1 && !loop)
        return cAtTimingModeExt1Ref;

    return cAtTimingModeUnknown;
    }

eAtRet Tha60210031OcnLineLocalLoopEnable(AtChannel self, eBool enable)
    {
    if (self)
        return LineLocalLoopEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60210031OcnLineLocalLoopIsEnabled(AtChannel self)
    {
    if (self)
        return LineLocalLoopIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha60210031OcnLineRemoteLoopEnable(AtChannel self, eBool enable)
    {
    if (self)
        return LineRemoteLoopEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60210031OcnLineRemoteLoopIsEnabled(AtChannel self)
    {
    if (self)
        return LineRemoteLoopIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha60210031OcnLineTxAisForce(AtChannel self, eBool enable)
    {
    if (self)
        return LineTxAisForce(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60210031OcnLineTxAisIsForced(AtChannel self)
    {
    if (self)
        return LineTxAisIsForced(self);
    return cAtFalse;
    }

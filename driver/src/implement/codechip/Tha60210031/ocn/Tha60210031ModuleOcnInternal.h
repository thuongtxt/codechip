/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210031ModuleOcnInternal.h
 * 
 * Created Date: Oct 13, 2016
 *
 * Description : OCN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEOCNINTERNAL_H_
#define _THA60210031MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"
#include "Tha60210031ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleOcnMethods
    {
    uint32 (*LineLocalLoopbackRegAddr)(Tha60210031ModuleOcn self, AtSdhLine line);
    uint32 (*LineRemoteLoopbackRegAddr)(Tha60210031ModuleOcn self, AtSdhLine line);
    }tTha60210031ModuleOcnMethods;

typedef struct tTha60210031ModuleOcn
    {
    tTha60210011ModuleOcn super;
    const tTha60210031ModuleOcnMethods *methods;
    }tTha60210031ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210031ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEOCNINTERNAL_H_ */


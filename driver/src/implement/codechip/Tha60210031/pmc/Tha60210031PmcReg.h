/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0031_RD_PMC_H_
#define _AF6_REG_AF6CCI0031_RD_PMC_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive FCS Error Packet Counter
Reg Addr   : 0x00000000 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
TCount number of FCS error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_fcs_err_cnt_ro_Base                                                          0x00000000
#define cAf6Reg_eth_rx_fcs_err_cnt_ro                                                               0x00000000
#define cAf6Reg_eth_rx_fcs_err_cnt_ro_WidthVal                                                              32
#define cAf6Reg_eth_rx_fcs_err_cnt_ro_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxFcsErrorPk
BitField Type: RO
BitField Desc: This counter count the number of FCS error Ethernet PHY packets
received.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_Bit_Start                                                    0
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_Bit_End                                                     15
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_Mask                                                  cBit15_0
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_Shift                                                        0
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_MaxVal                                                  0xffff
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_MinVal                                                     0x0
#define cAf6_eth_rx_fcs_err_cnt_ro_RxFcsErrorPk_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive FCS Error Packet Counter
Reg Addr   : 0x00000001 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
TCount number of FCS error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_fcs_err_cnt_r2c_Base                                                         0x00000001
#define cAf6Reg_eth_rx_fcs_err_cnt_r2c                                                              0x00000001
#define cAf6Reg_eth_rx_fcs_err_cnt_r2c_WidthVal                                                             32
#define cAf6Reg_eth_rx_fcs_err_cnt_r2c_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxFcsErrorPk
BitField Type: RO
BitField Desc: This counter count the number of FCS error Ethernet PHY packets
received.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_Bit_Start                                                   0
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_Bit_End                                                    15
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_Mask                                                 cBit15_0
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_Shift                                                       0
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_MaxVal                                                 0xffff
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_MinVal                                                    0x0
#define cAf6_eth_rx_fcs_err_cnt_r2c_RxFcsErrorPk_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Header Error Packet Counter
Reg Addr   : 0x00000002 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Ethernet MAC header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_header_err_cnt_ro_Base                                                       0x00000002
#define cAf6Reg_eth_rx_header_err_cnt_ro                                                            0x00000002
#define cAf6Reg_eth_rx_header_err_cnt_ro_WidthVal                                                           32
#define cAf6Reg_eth_rx_header_err_cnt_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RxEthErrorPk
BitField Type: RO
BitField Desc: This counter count the number of packets with MAC Header Error.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_Bit_Start                                                 0
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_Bit_End                                                  15
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_Mask                                               cBit15_0
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_Shift                                                     0
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_MaxVal                                               0xffff
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_MinVal                                                  0x0
#define cAf6_eth_rx_header_err_cnt_ro_RxEthErrorPk_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Header Error Packet Counter
Reg Addr   : 0x00000003 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Ethernet MAC header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_header_err_cnt_r2c_Base                                                      0x00000003
#define cAf6Reg_eth_rx_header_err_cnt_r2c                                                           0x00000003
#define cAf6Reg_eth_rx_header_err_cnt_r2c_WidthVal                                                          32
#define cAf6Reg_eth_rx_header_err_cnt_r2c_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: RxEthErrorPk
BitField Type: RO
BitField Desc: This counter count the number of packets with MAC Header Error.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_Bit_Start                                                0
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_Bit_End                                                 15
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_Mask                                              cBit15_0
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_Shift                                                    0
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_MaxVal                                              0xffff
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_MinVal                                                 0x0
#define cAf6_eth_rx_header_err_cnt_r2c_RxEthErrorPk_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive PSN Header Error Packet Counter
Reg Addr   : 0x00000004 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of PSN header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_psnheader_err_cnt_ro_Base                                                    0x00000004
#define cAf6Reg_eth_rx_psnheader_err_cnt_ro                                                         0x00000004
#define cAf6Reg_eth_rx_psnheader_err_cnt_ro_WidthVal                                                        32
#define cAf6Reg_eth_rx_psnheader_err_cnt_ro_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: RxPsnErrorPk
BitField Type: RO
BitField Desc: This counter count the number of packets with PSN Header error.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_Bit_Start                                              0
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_Bit_End                                               15
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_Mask                                            cBit15_0
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_Shift                                                  0
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_MaxVal                                            0xffff
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_MinVal                                               0x0
#define cAf6_eth_rx_psnheader_err_cnt_ro_RxPsnErrorPk_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive PSN Header Error Packet Counter
Reg Addr   : 0x00000005 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of PSN header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_psnheader_err_cnt_r2c_Base                                                   0x00000005
#define cAf6Reg_eth_rx_psnheader_err_cnt_r2c                                                        0x00000005
#define cAf6Reg_eth_rx_psnheader_err_cnt_r2c_WidthVal                                                       32
#define cAf6Reg_eth_rx_psnheader_err_cnt_r2c_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: RxPsnErrorPk
BitField Type: RO
BitField Desc: This counter count the number of packets with PSN Header error.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_Bit_Start                                             0
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_Bit_End                                              15
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_Mask                                           cBit15_0
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_Shift                                                 0
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_MaxVal                                           0xffff
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_MinVal                                              0x0
#define cAf6_eth_rx_psnheader_err_cnt_r2c_RxPsnErrorPk_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive UDP Port Error Packet Counter
Reg Addr   : 0x00000006 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of UDP port error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_udpport_err_cnt_ro_Base                                                      0x00000006
#define cAf6Reg_eth_rx_udpport_err_cnt_ro                                                           0x00000006
#define cAf6Reg_eth_rx_udpport_err_cnt_ro_WidthVal                                                          32
#define cAf6Reg_eth_rx_udpport_err_cnt_ro_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: RxUdpErrorPk
BitField Type: RO
BitField Desc: This counter count the number of packets with UDP port error.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_Bit_Start                                                0
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_Bit_End                                                 15
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_Mask                                              cBit15_0
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_Shift                                                    0
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_MaxVal                                              0xffff
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_MinVal                                                 0x0
#define cAf6_eth_rx_udpport_err_cnt_ro_RxUdpErrorPk_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive UDP Port Error Packet Counter
Reg Addr   : 0x00000007 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of UDP port error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_udpport_err_cnt_r2c_Base                                                     0x00000007
#define cAf6Reg_eth_rx_udpport_err_cnt_r2c                                                          0x00000007
#define cAf6Reg_eth_rx_udpport_err_cnt_r2c_WidthVal                                                         32
#define cAf6Reg_eth_rx_udpport_err_cnt_r2c_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: RxUdpErrorPk
BitField Type: RO
BitField Desc: This counter count the number of packets with UDP port error.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_Bit_Start                                               0
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_Bit_End                                                15
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_Mask                                             cBit15_0
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_Shift                                                   0
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_MaxVal                                             0xffff
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_MinVal                                                0x0
#define cAf6_eth_rx_udpport_err_cnt_r2c_RxUdpErrorPk_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : 4.1.5. Ethernet Receive OAM Packet Counter
Reg Addr   : 0x0000000E (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_oam_pkt_cnt_ro_Base                                                          0x0000000E
#define cAf6Reg_eth_rx_oam_pkt_cnt_ro                                                               0x0000000E
#define cAf6Reg_eth_rx_oam_pkt_cnt_ro_WidthVal                                                              32
#define cAf6Reg_eth_rx_oam_pkt_cnt_ro_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxOamPkt
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_Bit_Start                                                        0
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_Bit_End                                                         15
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_Mask                                                      cBit15_0
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_Shift                                                            0
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_MaxVal                                                      0xffff
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_MinVal                                                         0x0
#define cAf6_eth_rx_oam_pkt_cnt_ro_RxOamPkt_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : 4.1.5. Ethernet Receive OAM Packet Counter
Reg Addr   : 0x0000000F (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_oam_pkt_cnt_r2c_Base                                                         0x0000000F
#define cAf6Reg_eth_rx_oam_pkt_cnt_r2c                                                              0x0000000F
#define cAf6Reg_eth_rx_oam_pkt_cnt_r2c_WidthVal                                                             32
#define cAf6Reg_eth_rx_oam_pkt_cnt_r2c_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxOamPkt
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_Bit_Start                                                       0
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_Bit_End                                                        15
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_Mask                                                     cBit15_0
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_Shift                                                           0
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_MaxVal                                                     0xffff
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_MinVal                                                        0x0
#define cAf6_eth_rx_oam_pkt_cnt_r2c_RxOamPkt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit OAM Packet Counter
Reg Addr   : 0x00000010 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_oam_pkt_cnt_ro_Base                                                          0x00000010
#define cAf6Reg_eth_tx_oam_pkt_cnt_ro                                                               0x00000010
#define cAf6Reg_eth_tx_oam_pkt_cnt_ro_WidthVal                                                              32
#define cAf6Reg_eth_tx_oam_pkt_cnt_ro_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: TxOamPkt
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_Bit_Start                                                        0
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_Bit_End                                                         15
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_Mask                                                      cBit15_0
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_Shift                                                            0
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_MaxVal                                                      0xffff
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_MinVal                                                         0x0
#define cAf6_eth_tx_oam_pkt_cnt_ro_TxOamPkt_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit OAM Packet Counter
Reg Addr   : 0x00000011 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_oam_pkt_cnt_r2c_Base                                                         0x00000011
#define cAf6Reg_eth_tx_oam_pkt_cnt_r2c                                                              0x00000011
#define cAf6Reg_eth_tx_oam_pkt_cnt_r2c_WidthVal                                                             32
#define cAf6Reg_eth_tx_oam_pkt_cnt_r2c_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: TxOamPkt
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_Bit_Start                                                       0
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_Bit_End                                                        15
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_Mask                                                     cBit15_0
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_Shift                                                           0
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_MaxVal                                                     0xffff
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_MinVal                                                        0x0
#define cAf6_eth_tx_oam_pkt_cnt_r2c_TxOamPkt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : 4.1.7. Ethernet Receive Total Byte Counter
Reg Addr   : 0x00000012 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of bytes received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_total_byte_cnt_ro_Base                                                       0x00000012
#define cAf6Reg_eth_rx_total_byte_cnt_ro                                                            0x00000012
#define cAf6Reg_eth_rx_total_byte_cnt_ro_WidthVal                                                           32
#define cAf6Reg_eth_rx_total_byte_cnt_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RxByte
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_Bit_Start                                                       0
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_Bit_End                                                        31
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_Mask                                                     cBit31_0
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_Shift                                                           0
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_MaxVal                                                 0xffffffff
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_MinVal                                                        0x0
#define cAf6_eth_rx_total_byte_cnt_ro_RxByte_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : 4.1.7. Ethernet Receive Total Byte Counter
Reg Addr   : 0x00000013 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of bytes received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_total_byte_cnt_r2c_Base                                                      0x00000013
#define cAf6Reg_eth_rx_total_byte_cnt_r2c                                                           0x00000013
#define cAf6Reg_eth_rx_total_byte_cnt_r2c_WidthVal                                                          32
#define cAf6Reg_eth_rx_total_byte_cnt_r2c_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: RxByte
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_Bit_Start                                                      0
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_Bit_End                                                       31
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_Mask                                                    cBit31_0
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_Shift                                                          0
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_MaxVal                                                0xffffffff
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_MinVal                                                       0x0
#define cAf6_eth_rx_total_byte_cnt_r2c_RxByte_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : 4.1.7. Ethernet Receive Total Packt Counter
Reg Addr   : 0x00000014 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of packets received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_total_pkt_cnt_ro_Base                                                        0x00000014
#define cAf6Reg_eth_rx_total_pkt_cnt_ro                                                             0x00000014
#define cAf6Reg_eth_rx_total_pkt_cnt_ro_WidthVal                                                            32
#define cAf6Reg_eth_rx_total_pkt_cnt_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: RxPacket
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_Bit_Start                                                      0
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_Bit_End                                                       31
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_Mask                                                    cBit31_0
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_Shift                                                          0
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_MaxVal                                                0xffffffff
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_MinVal                                                       0x0
#define cAf6_eth_rx_total_pkt_cnt_ro_RxPacket_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : 4.1.7. Ethernet Receive Total Packt Counter
Reg Addr   : 0x00000015 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of packets received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_total_pkt_cnt_r2c_Base                                                       0x00000015
#define cAf6Reg_eth_rx_total_pkt_cnt_r2c                                                            0x00000015
#define cAf6Reg_eth_rx_total_pkt_cnt_r2c_WidthVal                                                           32
#define cAf6Reg_eth_rx_total_pkt_cnt_r2c_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: RxPacket
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_Bit_Start                                                     0
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_Bit_End                                                      31
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_Mask                                                   cBit31_0
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_Shift                                                         0
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_MaxVal                                               0xffffffff
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_MinVal                                                      0x0
#define cAf6_eth_rx_total_pkt_cnt_r2c_RxPacket_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Classified Packet Counter
Reg Addr   : 0x00000016 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified packets (PW packets plus OAM packets) from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_cla_pkt_cnt_ro_Base                                                          0x00000016
#define cAf6Reg_eth_rx_cla_pkt_cnt_ro                                                               0x00000016
#define cAf6Reg_eth_rx_cla_pkt_cnt_ro_WidthVal                                                              32
#define cAf6Reg_eth_rx_cla_pkt_cnt_ro_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: RxClassifyPk
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_Bit_Start                                                    0
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_Bit_End                                                     31
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_Mask                                                  cBit31_0
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_Shift                                                        0
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_MaxVal                                              0xffffffff
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_MinVal                                                     0x0
#define cAf6_eth_rx_cla_pkt_cnt_ro_RxClassifyPk_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Classified Packet Counter
Reg Addr   : 0x00000017 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified packets (PW packets plus OAM packets) from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_cla_pkt_cnt_r2c_Base                                                         0x00000017
#define cAf6Reg_eth_rx_cla_pkt_cnt_r2c                                                              0x00000017
#define cAf6Reg_eth_rx_cla_pkt_cnt_r2c_WidthVal                                                             32
#define cAf6Reg_eth_rx_cla_pkt_cnt_r2c_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: RxClassifyPk
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_Bit_Start                                                   0
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_Bit_End                                                    31
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_Mask                                                 cBit31_0
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_Shift                                                       0
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_MaxVal                                             0xffffffff
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_MinVal                                                    0x0
#define cAf6_eth_rx_cla_pkt_cnt_r2c_RxClassifyPk_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Byte Counter
Reg Addr   : 0x00000018 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  bytes transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_byte_cnt_ro_Base                                                             0x00000018
#define cAf6Reg_eth_tx_byte_cnt_ro                                                                  0x00000018
#define cAf6Reg_eth_tx_byte_cnt_ro_WidthVal                                                                 32
#define cAf6Reg_eth_tx_byte_cnt_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TxByte
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_byte_cnt_ro_TxByte_Bit_Start                                                             0
#define cAf6_eth_tx_byte_cnt_ro_TxByte_Bit_End                                                              31
#define cAf6_eth_tx_byte_cnt_ro_TxByte_Mask                                                           cBit31_0
#define cAf6_eth_tx_byte_cnt_ro_TxByte_Shift                                                                 0
#define cAf6_eth_tx_byte_cnt_ro_TxByte_MaxVal                                                       0xffffffff
#define cAf6_eth_tx_byte_cnt_ro_TxByte_MinVal                                                              0x0
#define cAf6_eth_tx_byte_cnt_ro_TxByte_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Byte Counter
Reg Addr   : 0x00000019 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  bytes transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_byte_cnt_r2c_Base                                                            0x00000019
#define cAf6Reg_eth_tx_byte_cnt_r2c                                                                 0x00000019
#define cAf6Reg_eth_tx_byte_cnt_r2c_WidthVal                                                                32
#define cAf6Reg_eth_tx_byte_cnt_r2c_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TxByte
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_Bit_Start                                                            0
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_Bit_End                                                             31
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_Mask                                                          cBit31_0
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_Shift                                                                0
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_MaxVal                                                      0xffffffff
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_MinVal                                                             0x0
#define cAf6_eth_tx_byte_cnt_r2c_TxByte_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Packet Counter
Reg Addr   : 0x0000001A (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  packets transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_ro_Base                                                              0x0000001A
#define cAf6Reg_eth_tx_pkt_cnt_ro                                                                   0x0000001A
#define cAf6Reg_eth_tx_pkt_cnt_ro_WidthVal                                                                  32
#define cAf6Reg_eth_tx_pkt_cnt_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: TxPacket
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_Bit_Start                                                            0
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_Bit_End                                                             31
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_Mask                                                          cBit31_0
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_Shift                                                                0
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_MaxVal                                                      0xffffffff
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_MinVal                                                             0x0
#define cAf6_eth_tx_pkt_cnt_ro_TxPacket_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Packet Counter
Reg Addr   : 0x0000001B (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  packets transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_r2c_Base                                                             0x0000001B
#define cAf6Reg_eth_tx_pkt_cnt_r2c                                                                  0x0000001B
#define cAf6Reg_eth_tx_pkt_cnt_r2c_WidthVal                                                                 32
#define cAf6Reg_eth_tx_pkt_cnt_r2c_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: TxPacket
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_Bit_Start                                                           0
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_Bit_End                                                            31
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_Mask                                                         cBit31_0
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_Shift                                                               0
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_MaxVal                                                     0xffffffff
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_MinVal                                                            0x0
#define cAf6_eth_tx_pkt_cnt_r2c_TxPacket_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1to64Byte Packet Counter
Reg Addr   : 0x00000020 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1 to 64 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_ro_Base                                                    0x00000020
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_ro                                                         0x00000020
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_ro_WidthVal                                                        32
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_ro_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_Bit_Start                                                 0
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_Bit_End                                                  31
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_Mask                                               cBit31_0
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_Shift                                                     0
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_MaxVal                                           0xffffffff
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_MinVal                                                  0x0
#define cAf6_eth_rx_1to64byte_pkt_cnt_ro_PacketNum_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1to64Byte Packet Counter
Reg Addr   : 0x00000021 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1 to 64 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_r2c_Base                                                   0x00000021
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_r2c                                                        0x00000021
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_r2c_WidthVal                                                       32
#define cAf6Reg_eth_rx_1to64byte_pkt_cnt_r2c_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_Bit_Start                                                0
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_Bit_End                                                 31
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_Mask                                              cBit31_0
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_Shift                                                    0
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_MaxVal                                          0xffffffff
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_MinVal                                                 0x0
#define cAf6_eth_rx_1to64byte_pkt_cnt_r2c_PacketNum_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1to64Byte Packet Counter
Reg Addr   : 0x00000030 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1 to 64 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_ro_Base                                                    0x00000030
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_ro                                                         0x00000030
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_ro_WidthVal                                                        32
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_ro_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_Bit_Start                                                 0
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_Bit_End                                                  31
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_Mask                                               cBit31_0
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_Shift                                                     0
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_MaxVal                                           0xffffffff
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_MinVal                                                  0x0
#define cAf6_eth_tx_1to64byte_pkt_cnt_ro_PacketNum_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1to64Byte Packet Counter
Reg Addr   : 0x00000031 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1 to 64 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_r2c_Base                                                   0x00000031
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_r2c                                                        0x00000031
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_r2c_WidthVal                                                       32
#define cAf6Reg_eth_tx_1to64byte_pkt_cnt_r2c_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_Bit_Start                                                0
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_Bit_End                                                 31
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_Mask                                              cBit31_0
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_Shift                                                    0
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_MaxVal                                          0xffffffff
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_MinVal                                                 0x0
#define cAf6_eth_tx_1to64byte_pkt_cnt_r2c_PacketNum_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 65to128Byte Packet Counter
Reg Addr   : 0x00000022 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 65 to 128 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_ro_Base                                                  0x00000022
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_ro                                                       0x00000022
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_ro_WidthVal                                                      32
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_ro_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_Bit_Start                                               0
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_Bit_End                                                31
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_Mask                                             cBit31_0
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_Shift                                                   0
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_MaxVal                                         0xffffffff
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_MinVal                                                0x0
#define cAf6_eth_rx_65to128byte_pkt_cnt_ro_PacketNum_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 65to128Byte Packet Counter
Reg Addr   : 0x00000023 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 65 to 128 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_r2c_Base                                                 0x00000023
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_r2c                                                      0x00000023
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_r2c_WidthVal                                                     32
#define cAf6Reg_eth_rx_65to128byte_pkt_cnt_r2c_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_Bit_Start                                              0
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_Bit_End                                               31
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_Mask                                            cBit31_0
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_Shift                                                  0
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_MaxVal                                        0xffffffff
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_MinVal                                               0x0
#define cAf6_eth_rx_65to128byte_pkt_cnt_r2c_PacketNum_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 65to128Byte Packet Counter
Reg Addr   : 0x00000032 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 65 to 128 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_ro_Base                                                  0x00000032
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_ro                                                       0x00000032
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_ro_WidthVal                                                      32
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_ro_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_Bit_Start                                               0
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_Bit_End                                                31
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_Mask                                             cBit31_0
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_Shift                                                   0
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_MaxVal                                         0xffffffff
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_MinVal                                                0x0
#define cAf6_eth_tx_65to128byte_pkt_cnt_ro_PacketNum_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 65to128Byte Packet Counter
Reg Addr   : 0x00000033 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 65 to 128 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_r2c_Base                                                 0x00000033
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_r2c                                                      0x00000033
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_r2c_WidthVal                                                     32
#define cAf6Reg_eth_tx_65to128byte_pkt_cnt_r2c_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_Bit_Start                                              0
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_Bit_End                                               31
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_Mask                                            cBit31_0
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_Shift                                                  0
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_MaxVal                                        0xffffffff
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_MinVal                                               0x0
#define cAf6_eth_tx_65to128byte_pkt_cnt_r2c_PacketNum_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 129to256Byte Packet Counter
Reg Addr   : 0x00000024 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 129  to 256 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_ro_Base                                                 0x00000024
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_ro                                                      0x00000024
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_ro_WidthVal                                                     32
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_ro_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_Bit_Start                                              0
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_Bit_End                                               31
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_Mask                                            cBit31_0
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_Shift                                                  0
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_MaxVal                                        0xffffffff
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_MinVal                                               0x0
#define cAf6_eth_rx_129to256byte_pkt_cnt_ro_PacketNum_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 129to256Byte Packet Counter
Reg Addr   : 0x00000025 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 129  to 256 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_r2c_Base                                                0x00000025
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_r2c                                                     0x00000025
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_r2c_WidthVal                                                    32
#define cAf6Reg_eth_rx_129to256byte_pkt_cnt_r2c_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_Bit_Start                                             0
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_Bit_End                                              31
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_Mask                                           cBit31_0
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_Shift                                                 0
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_MaxVal                                       0xffffffff
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_MinVal                                              0x0
#define cAf6_eth_rx_129to256byte_pkt_cnt_r2c_PacketNum_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 129to256Byte Packet Counter
Reg Addr   : 0x00000034 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 129  to 256 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_ro_Base                                                 0x00000034
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_ro                                                      0x00000034
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_ro_WidthVal                                                     32
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_ro_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_Bit_Start                                              0
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_Bit_End                                               31
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_Mask                                            cBit31_0
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_Shift                                                  0
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_MaxVal                                        0xffffffff
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_MinVal                                               0x0
#define cAf6_eth_tx_129to256byte_pkt_cnt_ro_PacketNum_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 129to256Byte Packet Counter
Reg Addr   : 0x00000035 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 129  to 256 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_r2c_Base                                                0x00000035
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_r2c                                                     0x00000035
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_r2c_WidthVal                                                    32
#define cAf6Reg_eth_tx_129to256byte_pkt_cnt_r2c_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_Bit_Start                                             0
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_Bit_End                                              31
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_Mask                                           cBit31_0
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_Shift                                                 0
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_MaxVal                                       0xffffffff
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_MinVal                                              0x0
#define cAf6_eth_tx_129to256byte_pkt_cnt_r2c_PacketNum_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 257to512Byte Packet Counter
Reg Addr   : 0x00000026 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 257  to  512 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_ro_Base                                                 0x00000026
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_ro                                                      0x00000026
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_ro_WidthVal                                                     32
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_ro_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_Bit_Start                                              0
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_Bit_End                                               31
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_Mask                                            cBit31_0
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_Shift                                                  0
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_MaxVal                                        0xffffffff
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_MinVal                                               0x0
#define cAf6_eth_rx_257to512byte_pkt_cnt_ro_PacketNum_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 257to512Byte Packet Counter
Reg Addr   : 0x00000027 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 257  to  512 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_r2c_Base                                                0x00000027
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_r2c                                                     0x00000027
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_r2c_WidthVal                                                    32
#define cAf6Reg_eth_rx_257to512byte_pkt_cnt_r2c_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_Bit_Start                                             0
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_Bit_End                                              31
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_Mask                                           cBit31_0
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_Shift                                                 0
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_MaxVal                                       0xffffffff
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_MinVal                                              0x0
#define cAf6_eth_rx_257to512byte_pkt_cnt_r2c_PacketNum_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 257to512Byte Packet Counter
Reg Addr   : 0x00000036 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 257  to  512 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_ro_Base                                                 0x00000036
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_ro                                                      0x00000036
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_ro_WidthVal                                                     32
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_ro_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_Bit_Start                                              0
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_Bit_End                                               31
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_Mask                                            cBit31_0
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_Shift                                                  0
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_MaxVal                                        0xffffffff
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_MinVal                                               0x0
#define cAf6_eth_tx_257to512byte_pkt_cnt_ro_PacketNum_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 257to512Byte Packet Counter
Reg Addr   : 0x00000037 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 257  to  512 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_r2c_Base                                                0x00000037
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_r2c                                                     0x00000037
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_r2c_WidthVal                                                    32
#define cAf6Reg_eth_tx_257to512byte_pkt_cnt_r2c_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_Bit_Start                                             0
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_Bit_End                                              31
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_Mask                                           cBit31_0
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_Shift                                                 0
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_MaxVal                                       0xffffffff
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_MinVal                                              0x0
#define cAf6_eth_tx_257to512byte_pkt_cnt_r2c_PacketNum_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 513to1024Byte Packet Counter
Reg Addr   : 0x00000028 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 513 to 1024 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_ro_Base                                                0x00000028
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_ro                                                     0x00000028
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_ro_WidthVal                                                    32
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_ro_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_Bit_Start                                             0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_Bit_End                                              31
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_Mask                                           cBit31_0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_Shift                                                 0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_MaxVal                                       0xffffffff
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_MinVal                                              0x0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_ro_PacketNum_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 513to1024Byte Packet Counter
Reg Addr   : 0x00000029 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 513 to 1024 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_r2c_Base                                               0x00000029
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_r2c                                                    0x00000029
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_r2c_WidthVal                                                   32
#define cAf6Reg_eth_rx_513to1024byte_pkt_cnt_r2c_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_Bit_Start                                            0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_Bit_End                                             31
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_Mask                                          cBit31_0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_Shift                                                0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_MaxVal                                      0xffffffff
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_MinVal                                             0x0
#define cAf6_eth_rx_513to1024byte_pkt_cnt_r2c_PacketNum_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 513to1024Byte Packet Counter
Reg Addr   : 0x00000038 (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 513 to 1024 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_ro_Base                                                0x00000038
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_ro                                                     0x00000038
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_ro_WidthVal                                                    32
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_ro_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_Bit_Start                                             0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_Bit_End                                              31
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_Mask                                           cBit31_0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_Shift                                                 0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_MaxVal                                       0xffffffff
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_MinVal                                              0x0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_ro_PacketNum_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 513to1024Byte Packet Counter
Reg Addr   : 0x00000039 (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 513 to 1024 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_r2c_Base                                               0x00000039
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_r2c                                                    0x00000039
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_r2c_WidthVal                                                   32
#define cAf6Reg_eth_tx_513to1024byte_pkt_cnt_r2c_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_Bit_Start                                            0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_Bit_End                                             31
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_Mask                                          cBit31_0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_Shift                                                0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_MaxVal                                      0xffffffff
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_MinVal                                             0x0
#define cAf6_eth_tx_513to1024byte_pkt_cnt_r2c_PacketNum_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1025to1528Byte Packet Counter
Reg Addr   : 0x0000002A (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1025 to 1528 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_ro_Base                                               0x0000002A
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_ro                                                    0x0000002A
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_ro_WidthVal                                                   32
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_ro_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_Bit_Start                                            0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_Bit_End                                             31
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_Mask                                          cBit31_0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_Shift                                                0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_MaxVal                                      0xffffffff
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_MinVal                                             0x0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_ro_PacketNum_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1025to1528Byte Packet Counter
Reg Addr   : 0x0000002B (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1025 to 1528 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_r2c_Base                                              0x0000002B
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_r2c                                                   0x0000002B
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_r2c_WidthVal                                                  32
#define cAf6Reg_eth_rx_1025to1528byte_pkt_cnt_r2c_WriteMask                                                0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_Bit_Start                                           0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_Bit_End                                            31
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_Mask                                         cBit31_0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_Shift                                               0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_MaxVal                                     0xffffffff
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_MinVal                                            0x0
#define cAf6_eth_rx_1025to1528byte_pkt_cnt_r2c_PacketNum_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1025to1528Byte Packet Counter
Reg Addr   : 0x0000003A (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1025 to 1528 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_ro_Base                                               0x0000003A
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_ro                                                    0x0000003A
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_ro_WidthVal                                                   32
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_ro_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_Bit_Start                                            0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_Bit_End                                             31
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_Mask                                          cBit31_0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_Shift                                                0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_MaxVal                                      0xffffffff
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_MinVal                                             0x0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_ro_PacketNum_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1025to1528Byte Packet Counter
Reg Addr   : 0x0000003B (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1025 to 1528 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_r2c_Base                                              0x0000003B
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_r2c                                                   0x0000003B
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_r2c_WidthVal                                                  32
#define cAf6Reg_eth_tx_1025to1528byte_pkt_cnt_r2c_WriteMask                                                0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_Bit_Start                                           0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_Bit_End                                            31
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_Mask                                         cBit31_0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_Shift                                               0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_MaxVal                                     0xffffffff
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_MinVal                                            0x0
#define cAf6_eth_tx_1025to1528byte_pkt_cnt_r2c_PacketNum_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive jumboByte Packet Counter
Reg Addr   : 0x0000002C (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from jumbo bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_ro_Base                                                    0x0000002C
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_ro                                                         0x0000002C
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_ro_WidthVal                                                        32
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_ro_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_Bit_Start                                                 0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_Bit_End                                                  31
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_Mask                                               cBit31_0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_Shift                                                     0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_MaxVal                                           0xffffffff
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_MinVal                                                  0x0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_ro_PacketNum_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive jumboByte Packet Counter
Reg Addr   : 0x0000002D (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from jumbo bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_r2c_Base                                                   0x0000002D
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_r2c                                                        0x0000002D
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_r2c_WidthVal                                                       32
#define cAf6Reg_eth_rx_jumbobyte_pkt_cnt_r2c_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_Bit_Start                                                0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_Bit_End                                                 31
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_Mask                                              cBit31_0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_Shift                                                    0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_MaxVal                                          0xffffffff
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_MinVal                                                 0x0
#define cAf6_eth_rx_jumbobyte_pkt_cnt_r2c_PacketNum_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit jumboByte Packet Counter
Reg Addr   : 0x0000003C (RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from jumbo bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_ro_Base                                                    0x0000003C
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_ro                                                         0x0000003C
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_ro_WidthVal                                                        32
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_ro_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_Bit_Start                                                 0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_Bit_End                                                  31
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_Mask                                               cBit31_0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_Shift                                                     0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_MaxVal                                           0xffffffff
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_MinVal                                                  0x0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_ro_PacketNum_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit jumboByte Packet Counter
Reg Addr   : 0x0000003D (R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from jumbo bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_r2c_Base                                                   0x0000003D
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_r2c                                                        0x0000003D
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_r2c_WidthVal                                                       32
#define cAf6Reg_eth_tx_jumbobyte_pkt_cnt_r2c_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RO
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_Bit_Start                                                0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_Bit_End                                                 31
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_Mask                                              cBit31_0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_Shift                                                    0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_MaxVal                                          0xffffffff
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_MinVal                                                 0x0
#define cAf6_eth_tx_jumbobyte_pkt_cnt_r2c_PacketNum_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Good Packet Counter
Reg Addr   : 0x010800 - 0x010FFF(RO)
Reg Formula: 0x010800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro_Base                                       0x010800
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro(PwId)                             (0x010800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Good Packet Counter
Reg Addr   : 0x010000 - 0x0107FF(RC)
Reg Formula: 0x010000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc_Base                                       0x010000
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc(PwId)                             (0x010000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Payload Octet Counter
Reg Addr   : 0x011800 - 0x011FFF(RO)
Reg Formula: 0x011800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire payload octet received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro_Base                                      0x011800
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro(PwId)                            (0x011800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro_WidthVal                                        32
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro_WriteMask                                      0x0

/*--------------------------------------
BitField Name: RxPwPayOct
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Shift                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Payload Octet Counter
Reg Addr   : 0x011000 - 0x0117FF(RC)
Reg Formula: 0x011000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire payload octet received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc_Base                                      0x011000
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc(PwId)                            (0x011000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc_WidthVal                                        32
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc_WriteMask                                      0x0

/*--------------------------------------
BitField Name: RxPwPayOct
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Shift                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit LBit Packet Counter
Reg Addr   : 0x012800 - 0x012FFF(RO)
Reg Formula: 0x012800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_ro_Base                                       0x012800
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_ro(PwId)                             (0x012800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwLbitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_ro_TxPwLbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit LBit Packet Counter
Reg Addr   : 0x012000 - 0x0127FF(RC)
Reg Formula: 0x012000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_rc_Base                                       0x012000
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_rc(PwId)                             (0x012000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_LBit_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwLbitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_LBit_Packet_Counter_rc_TxPwLbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit RBit Packet Counter
Reg Addr   : 0x013800 - 0x013FFF(RO)
Reg Formula: 0x013800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_ro_Base                                       0x013800
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_ro(PwId)                             (0x013800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwRbitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_ro_TxPwRbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit RBit Packet Counter
Reg Addr   : 0x013000 - 0x0137FF(RC)
Reg Formula: 0x013000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_rc_Base                                       0x013000
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_rc(PwId)                             (0x013000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_RBit_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwRbitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_RBit_Packet_Counter_rc_TxPwRbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit MBit_NBit Packet Counter
Reg Addr   : 0x014800 - 0x014FFF(RO)
Reg Formula: 0x014800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_Base                                  0x014800
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro(PwId)                        (0x014800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: TxPwMbit_NBitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro_TxPwMbit_NBitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit MBit_NBit Packet Counter
Reg Addr   : 0x014000 - 0x0147FF(RC)
Reg Formula: 0x014000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_Base                                  0x014000
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc(PwId)                        (0x014000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: TxPwMbit_NBitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc_TxPwMbit_NBitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit PBit Packet Counter
Reg Addr   : 0x015800 - 0x015FFF(RO)
Reg Formula: 0x015800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_ro_Base                                       0x015800
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_ro(PwId)                             (0x015800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwPbitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_ro_TxPwPbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit PBit Packet Counter
Reg Addr   : 0x015000 - 0x0157FF(RC)
Reg Formula: 0x015000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_rc_Base                                       0x015000
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_rc(PwId)                             (0x015000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_PBit_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwPbitPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_PBit_Packet_Counter_rc_TxPwPbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Good Packet Counter
Reg Addr   : 0x020800 - 0x020FFF(RO)
Reg Formula: 0x020800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDs
Reg Desc   : 
Count number of CESoETH packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro_Base                                        0x020800
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro(PwId)                              (0x020800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Good Packet Counter
Reg Addr   : 0x020000 - 0x0207FF(RC)
Reg Formula: 0x020000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDs
Reg Desc   : 
Count number of CESoETH packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc_Base                                        0x020000
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc(PwId)                              (0x020000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Good Byte Counter
Reg Addr   : 0x021800 - 0x021FFF(RO)
Reg Formula: 0x021800 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDs
Reg Desc   : 
Count number of CESoETH packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_ro_Base                                          0x021800
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_ro(PwId)                                (0x021800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_ro_WidthVal                                            32
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_ro_WriteMask                                          0x0

/*--------------------------------------
BitField Name: RxPwBytePk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_Mask                                  cBit31_0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_Shift                                        0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_ro_RxPwBytePk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Good Byte Counter
Reg Addr   : 0x021000 - 0x0217FF(RC)
Reg Formula: 0x021000 + PwId
    Where  : 
           + $PwId (0 - 1535): Pseudowire IDs
Reg Desc   : 
Count number of CESoETH packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_rc_Base                                          0x021000
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_rc(PwId)                                (0x021000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_rc_WidthVal                                            32
#define cAf6Reg_Pseudowire_Receive_Good_Byte_Counter_rc_WriteMask                                          0x0

/*--------------------------------------
BitField Name: RxPwBytePk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_Mask                                  cBit31_0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_Shift                                        0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Good_Byte_Counter_rc_RxPwBytePk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive LOFS Counter
Reg Addr   : 0x022800 - 0x022FFF(RO)
Reg Formula: 0x022800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_ro_Base                                               0x022800
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_ro(PwId)                                     (0x022800+(PwId))
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_ro_WidthVal                                                 32
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_ro_WriteMask                                               0x0

/*--------------------------------------
BitField Name: RxPwLofsEvent
BitField Type: RW
BitField Desc: This counter count the number of transitions from normal sate to
loss of frame state
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_Bit_End                                       31
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_Mask                                    cBit31_0
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_Shift                                          0
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_MaxVal                                0xffffffff
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_MinVal                                       0x0
#define cAf6_Pseudowire_Receive_LOFS_Counter_ro_RxPwLofsEvent_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive LOFS Counter
Reg Addr   : 0x022000 - 0x0227FF(RC)
Reg Formula: 0x022000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_rc_Base                                               0x022000
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_rc(PwId)                                     (0x022000+(PwId))
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_rc_WidthVal                                                 32
#define cAf6Reg_Pseudowire_Receive_LOFS_Counter_rc_WriteMask                                               0x0

/*--------------------------------------
BitField Name: RxPwLofsEvent
BitField Type: RW
BitField Desc: This counter count the number of transitions from normal sate to
loss of frame state
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_Bit_End                                       31
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_Mask                                    cBit31_0
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_Shift                                          0
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_MaxVal                                0xffffffff
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_MinVal                                       0x0
#define cAf6_Pseudowire_Receive_LOFS_Counter_rc_RxPwLofsEvent_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive LBit Packet Counter
Reg Addr   : 0x023800 - 0x023FFF(RO)
Reg Formula: 0x023800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_ro_Base                                        0x023800
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_ro(PwId)                              (0x023800+(PwId))
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_ro_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_ro_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwLbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive LBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_ro_RxPwLbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive LBit Packet Counter
Reg Addr   : 0x023000 - 0x0237FF(RC)
Reg Formula: 0x023000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_rc_Base                                        0x023000
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_rc(PwId)                              (0x023000+(PwId))
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_rc_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_LBit_Packet_Counter_rc_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwLbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive LBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_LBit_Packet_Counter_rc_RxPwLbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive M_NBit Packet Counter
Reg Addr   : 0x024800 - 0x024FFF(RO)
Reg Formula: 0x024800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_ro_Base                                      0x024800
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_ro(PwId)                            (0x024800+(PwId))
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_ro_WidthVal                                        32
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_ro_WriteMask                                      0x0

/*--------------------------------------
BitField Name: RxPwM_NbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive M_NBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_ro_RxPwM_NbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive M_NBit Packet Counter
Reg Addr   : 0x024000 - 0x0247FF(RC)
Reg Formula: 0x024000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_rc_Base                                      0x024000
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_rc(PwId)                            (0x024000+(PwId))
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_rc_WidthVal                                        32
#define cAf6Reg_Pseudowire_Receive_M_NBit_Packet_Counter_rc_WriteMask                                      0x0

/*--------------------------------------
BitField Name: RxPwM_NbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive M_NBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_M_NBit_Packet_Counter_rc_RxPwM_NbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive PBit Packet Counter
Reg Addr   : 0x025800 - 0x025FFF(RO)
Reg Formula: 0x025800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_ro_Base                                        0x025800
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_ro(PwId)                              (0x025800+(PwId))
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_ro_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_ro_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwPbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive PBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_ro_RxPwPbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive PBit Packet Counter
Reg Addr   : 0x025000 - 0x0257FF(RC)
Reg Formula: 0x025000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_rc_Base                                        0x025000
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_rc(PwId)                              (0x025000+(PwId))
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_rc_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_PBit_Packet_Counter_rc_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwPbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive PBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_PBit_Packet_Counter_rc_RxPwPbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive RBit Packet Counter
Reg Addr   : 0x026800 - 0x026FFF(RO)
Reg Formula: 0x026800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_ro_Base                                        0x026800
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_ro(PwId)                              (0x026800+(PwId))
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_ro_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_ro_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwRbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive RBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_ro_RxPwRbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive RBit Packet Counter
Reg Addr   : 0x026000 - 0x0267FF(RC)
Reg Formula: 0x026000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_rc_Base                                        0x026000
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_rc(PwId)                              (0x026000+(PwId))
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_rc_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_RBit_Packet_Counter_rc_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwRbitEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive RBit Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_RBit_Packet_Counter_rc_RxPwRbitEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Drop Packet Counter
Reg Addr   : 0x027800 - 0x027FFF(RO)
Reg Formula: 0x027800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_Base                                0x027800
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro(PwId)                       (0x027800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorDropEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Reorder Drop Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Drop Packet Counter
Reg Addr   : 0x027000 - 0x0277FF(RC)
Reg Formula: 0x027000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_Base                                0x027000
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc(PwId)                       (0x027000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorDropEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Reorder Drop Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Out Of Sequence Packet Counter
Reg Addr   : 0x028800 - 0x028FFF(RO)
Reg Formula: 0x028800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_Base                                0x028800
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro(PwId)                       (0x028800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorOSeqEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Reorder Out Of Sequence
Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro_RxPwReorOSeqEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Out Of Sequence Packet Counter
Reg Addr   : 0x028000 - 0x0287FF(RC)
Reg Formula: 0x028000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_Base                                0x028000
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc(PwId)                       (0x028000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorOSeqEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Reorder Out Of Sequence
Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc_RxPwReorOSeqEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Lost Packet Counter
Reg Addr   : 0x029800 - 0x029FFF(RO)
Reg Formula: 0x029800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_Base                                0x029800
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro(PwId)                       (0x029800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorLostEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Reorder Lost Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Lost Packet Counter
Reg Addr   : 0x029000 - 0x0297FF(RC)
Reg Formula: 0x029000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_Base                                0x029000
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc(PwId)                       (0x029000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorLostEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Reorder Lost Packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer OverRun Counter
Reg Addr   : 0x02A800 - 0x02AFFF(RO)
Reg Formula: 0x02A800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_Base                                0x02A800
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro(PwId)                       (0x02A800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwOverRunEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Jitter Buffer Overrun
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro_RxPwOverRunEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer OverRun Counter
Reg Addr   : 0x02A000 - 0x02A7FF(RC)
Reg Formula: 0x02A000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_Base                                0x02A000
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc(PwId)                       (0x02A000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwOverRunEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Jitter Buffer Overrun
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc_RxPwOverRunEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer UnderRun Counter
Reg Addr   : 0x02B800 - 0x02BFFF(RO)
Reg Formula: 0x02B800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_Base                                0x02B800
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro(PwId)                       (0x02B800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwUnderRunEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Jitter Buffer UnderRun
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro_RxPwUnderRunEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer UnderRun Counter
Reg Addr   : 0x02B000 - 0x02B7FF(RC)
Reg Formula: 0x02B000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_Base                                0x02B000
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc(PwId)                       (0x02B000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwUnderRunEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Jitter Buffer UnderRun
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc_RxPwUnderRunEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Malform packet Counter
Reg Addr   : 0x02C800 - 0x02CFFF(RO)
Reg Formula: 0x02C800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_ro_Base                                     0x02C800
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_ro(PwId)                           (0x02C800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_ro_WidthVal                                       32
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwMalformEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Malform packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_ro_RxPwMalformEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Malform packet Counter
Reg Addr   : 0x02C000 - 0x02C7FF(RC)
Reg Formula: 0x02C000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_rc_Base                                     0x02C000
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_rc(PwId)                           (0x02C000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_rc_WidthVal                                       32
#define cAf6Reg_Pseudowire_Receive_Malform_packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwMalformEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Malform packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Malform_packet_Counter_rc_RxPwMalformEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Stray packet Counter
Reg Addr   : 0x02D800 - 0x02DFFF(RO)
Reg Formula: 0x02D800 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_ro_Base                                       0x02D800
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_ro(PwId)                             (0x02D800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwStrayEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Stray packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_ro_RxPwStrayEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Stray packet Counter
Reg Addr   : 0x02D000 - 0x02D7FF(RC)
Reg Formula: 0x02D000 + PwId
    Where  : 
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_rc_Base                                       0x02D000
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_rc(PwId)                             (0x02D000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_Stray_packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwStrayEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Stray packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_Bit_Start                                    0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_Bit_End                                     31
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_Mask                                  cBit31_0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_Shift                                        0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Stray_packet_Counter_rc_RxPwStrayEvent_RstVal                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire_Receive_Duplicate_packet_Counter
Reg Addr   : 0x02E800 - 0x02EFFF(RO)
Reg Formula: 0x02E800 + PwId
    Where  :
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_ro_Base                                   0x02E800
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_ro(PwId)                         (0x02E800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwDupliateEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Duplicate packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_ro_RxPwDupliateEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire_Receive_Duplicate_packet_Counter
Reg Addr   : 0x02E000 - 0x02E7FF(RC)
Reg Formula: 0x02E000 + PwId
    Where  :
           + $PwId (0 - 1533): Pseudowire IDs
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_rc_Base                                   0x02E000
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_rc(PwId)                         (0x02E000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwDupliateEvent
BitField Type: RW
BitField Desc: This counter count the number of Receive Duplicate packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Duplicate_packet_Counter_rc_RxPwDupliateEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Interrupt Enable Control
Reg Addr   : 0x040000
Reg Formula: 0x040000 +  GrpID*32 + BitID
    Where  : 
           + $GrpID(0-63):  Pseudowire ID bits[10:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base                                       0x040000
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control(GrpID, BitID)           (0x040000+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_WidthVal                                         32
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_WriteMask                                       0x0

/*--------------------------------------
BitField Name: StrayStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Stray packet event to generate an interrupt
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Bit_End                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Mask                                   cBit8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Shift                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MalformStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Malform packet event to generate an interrupt
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Bit_Start                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Mask                                   cBit7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Shift                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Mbit packet event to generate an interrupt
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Bit_Start                                    6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Bit_End                                      6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Mask                                     cBit6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Shift                                        6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: RbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Rbit packet event to generate an interrupt
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Bit_Start                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Bit_End                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Mask                                   cBit5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Shift                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsSyncStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Lost of frame Sync event to generate an interrupt
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Mask                                   cBit4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Shift                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change jitter buffer state event from normal to
underrun and vice versa in the related pseudowire to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Mask                                   cBit3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Shift                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change jitter buffer state event from normal to
overrun and vice versa in the related pseudowire to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Bit_Start                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Mask                                   cBit2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Shift                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change lost of frame state(LOFS) event from
normal to LOFS and vice versa in the related pseudowire to generate an
interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Bit_Start                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Bit_End                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Mask                                   cBit1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Shift                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Lbit packet event to generate an interrupt
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Bit_Start                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Bit_End                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Mask                                   cBit0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Shift                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Interrupt Status
Reg Addr   : 0x040800
Reg Formula: 0x040800 +  GrpID*32 + BitID
    Where  : 
           + $GrpID(0-63):  Pseudowire ID bits[10:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base                                               0x040800
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status(GrpID, BitID)                 (0x040800+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_WidthVal                                                 32
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_WriteMask                                               0x0

/*--------------------------------------
BitField Name: StrayStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when Stray packet event is detected in the pseudowire
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Bit_End                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Mask                                   cBit8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_Shift                                       8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: MalformStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when Malform packet event is detected in the pseudowire
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Bit_Start                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Mask                                   cBit7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_Shift                                       7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: MbitStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a Mbit packet event is detected in the pseudowire
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Bit_Start                                    6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Bit_End                                      6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Mask                                     cBit6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_Shift                                        6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: RbitStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a Rbit packet event is detected in the pseudowire
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Bit_Start                                    5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Bit_End                                      5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Mask                                     cBit5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Shift                                        5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsSyncStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a lost of frame Sync event is detected in the
pseudowire
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Mask                                   cBit4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Shift                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when there is a change in jitter buffer underrun state  in
the related pseudowire
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Mask                                   cBit3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Shift                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when there is a change in jitter buffer overrun state in
the related pseudowire
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Bit_Start                                    2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Bit_End                                      2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Mask                                     cBit2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Shift                                        2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when there is a change in lost of frame state(LOFS) in the
related pseudowire
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Bit_Start                                    1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Bit_End                                      1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Mask                                     cBit1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Shift                                        1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateChgIntrSta
BitField Type: RW
BitField Desc: Set 1 when a Lbit packet event is detected in the pseudowire
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Bit_Start                                    0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Bit_End                                      0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Mask                                     cBit0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Shift                                        0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Current Status
Reg Addr   : 0x041000
Reg Formula: 0x041000 +  GrpID*32 + BitID
    Where  : 
           + $GrpID(0-63):  Pseudowire ID bits[10:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Current_Status_Base                                                 0x041000
#define cAf6Reg_Counter_Per_Alarm_Current_Status(GrpID, BitID)                   (0x041000+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Current_Status_WidthVal                                                   32
#define cAf6Reg_Counter_Per_Alarm_Current_Status_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: StrayCurStatus
BitField Type: RW
BitField Desc: Stray packet state current status in the related pseudowire.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Bit_End                                         8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Mask                                        cBit8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Shift                                           8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_MaxVal                                        0x1
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_MinVal                                        0x0
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: MalformCurStatus
BitField Type: RW
BitField Desc: Malform packet state current status in the related pseudowire.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Bit_Start                                       7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Mask                                      cBit7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Shift                                         7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_MaxVal                                      0x1
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_MinVal                                      0x0
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_RstVal                                      0x0

/*--------------------------------------
BitField Name: MbitCurStatus
BitField Type: RW
BitField Desc: Mbit packet state current status in the related pseudowire.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Bit_Start                                        6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Bit_End                                          6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Mask                                         cBit6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Shift                                            6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_MaxVal                                         0x1
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_MinVal                                         0x0
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: LbitCurStatus
BitField Type: RW
BitField Desc: Rbit packet state current status in the related pseudowire.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_Bit_Start                                        5
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_Bit_End                                          5
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_Mask                                         cBit5
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_Shift                                            5
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_MaxVal                                         0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_MinVal                                         0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: LofsSyncCurStatus
BitField Type: RW
BitField Desc: Lost of frame Sync state current status in the related
pseudowire.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Mask                                     cBit4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Shift                                        4
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunCurStatus
BitField Type: RW
BitField Desc: Jitter buffer underrun current status in the related pseudowire.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Mask                                     cBit3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Shift                                        3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunCurStatus
BitField Type: RW
BitField Desc: Jitter buffer overrun current status in the related pseudowire.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Bit_Start                                       2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Mask                                      cBit2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Shift                                         2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_MaxVal                                      0x1
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_MinVal                                      0x0
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_RstVal                                      0x0

/*--------------------------------------
BitField Name: LofsStateCurStatus
BitField Type: RW
BitField Desc: Lost of frame state current status in the related pseudowire.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Bit_Start                                       1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Bit_End                                       1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Mask                                    cBit1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Shift                                       1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateCurStatus
BitField Type: RW
BitField Desc: a Lbit packet state current status in the related pseudowire
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Bit_Start                                       0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Bit_End                                       0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Mask                                    cBit0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Shift                                       0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Interrupt OR Status
Reg Addr   : 0x041800
Reg Formula: 0x041800 +  Slice*1024 + GrpID
    Where  : 
           + $Slice(0-1):  Pseudowire ID bits[10]
           + $GrpID(0-31):  Pseudowire ID bits[9:5]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Interrupt_OR_Status_Base                                                      0x041800
#define cAf6Reg_Counter_Interrupt_OR_Status(Slice, GrpID)                      (0x041800+(Slice)*1024+(GrpID))
#define cAf6Reg_Counter_Interrupt_OR_Status_WidthVal                                                        32
#define cAf6Reg_Counter_Interrupt_OR_Status_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: IntrORStatus
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the Counter per Alarm Interrupt Status register of the related pseudowires to be
set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0,
bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32,
respectively.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Bit_Start                                              0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Bit_End                                               31
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Mask                                            cBit31_0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Shift                                                  0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_MaxVal                                        0xffffffff
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_MinVal                                               0x0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt OR Status
Reg Addr   : 0x041BFF
Reg Formula: 0x041BFF +  Slice*1024
    Where  : 
           + $Slice(0-1):  Pseudowire ID bits[10]
Reg Desc   : 
The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_or_stat_Base                                                   0x041BFF
#define cAf6Reg_counter_per_group_intr_or_stat(Slice)                                  (0x041BFF+(Slice)*1024)
#define cAf6Reg_counter_per_group_intr_or_stat_WidthVal                                                     32
#define cAf6Reg_counter_per_group_intr_or_stat_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Group is set and
its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Bit_Start                                         0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Bit_End                                          31
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Mask                                       cBit31_0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Shift                                             0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_MaxVal                                   0xffffffff
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_MinVal                                          0x0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt Enable Control
Reg Addr   : 0x041BFE
Reg Formula: 0x041BFE +  Slice*1024
    Where  : 
           + $Slice(0-1):  Pseudowire ID bits[10]
Reg Desc   : 
The register consists of 8 interrupt enable bits for 8 group in the PW counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_en_ctrl_Base                                                   0x041BFE
#define cAf6Reg_counter_per_group_intr_en_ctrl(Slice)                                  (0x041BFE +(Slice)*1024)
#define cAf6Reg_counter_per_group_intr_en_ctrl_WidthVal                                                     32
#define cAf6Reg_counter_per_group_intr_en_ctrl_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Group to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Bit_Start                                            0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Bit_End                                             31
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask                                          cBit31_0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Shift                                                0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_MaxVal                                      0xffffffff
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_MinVal                                             0x0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Slice Interrupt OR Status
Reg Addr   : 0x042000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 Slice of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_slice_intr_or_stat_Base                                                   0x042000
#define cAf6Reg_counter_per_slice_intr_or_stat                                                        0x042000
#define cAf6Reg_counter_per_slice_intr_or_stat_WidthVal                                                     32
#define cAf6Reg_counter_per_slice_intr_or_stat_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Slice is set and
its interrupt is enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Bit_Start                                         0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Bit_End                                           1
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Mask                                        cBit1_0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_Shift                                             0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_MaxVal                                          0x3
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_MinVal                                          0x0
#define cAf6_counter_per_slice_intr_or_stat_GroupIntrOrSta_RstVal                                          0x0

#endif /* _AF6_REG_AF6CCI0031_RD_PMC_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210031DeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031DEVICEASYNCINIT_H_
#define _THA60210031DEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef void (*tHwAsynFlushFunc)(ThaDevice self);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210031DeviceSuperAsyncInit(AtDevice self);
tHwAsynFlushFunc *Tha60210031DeviceFlushFunctions(uint32 *numFunctions);
eAtRet Tha60210031DeviceAsyncInit(AtDevice self);
eAtRet Tha60210031DeviceReWriteHandlerInstall(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031DEVICEASYNCINIT_H_ */


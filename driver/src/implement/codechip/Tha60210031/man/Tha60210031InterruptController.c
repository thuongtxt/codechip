/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Helper
 *
 * File        : Tha60210031InterruptController.c
 *
 * Created Date: Aug 4, 2015
 *
 * Description : Interrupt helper source.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaIpCoreInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaModulePdhInternal.h"
#include "Tha60210031InterruptControllerInternal.h"
#include "Tha60210031DeviceReg.h"
#include "Tha60210031Device.h"
#include "../pdh/Tha60210031ModulePdhReg.h"
#include "../pmc/Tha60210031PmcReg.h"
#include "../poh/Tha60210031ModulePohReg.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation (option) */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_inten_status_DE1Grp1IntEnable_Mask |
                          cAf6_inten_status_DE3Grp1IntEnable_Mask |
                          cAf6_inten_status_DE1Grp0IntEnable_Mask |
                          cAf6_inten_status_DE3Grp0IntEnable_Mask |
                          cAf6_inten_status_PRMIntEnable_Mask     |
                          cAf6_inten_status_MDLIntEnable_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool EthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_status_EthIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool ThermalCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_status_SysMonTempEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool SemCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_status_SeuIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool ClockCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_inten_status_CDR1IntEnable_Mask|
                          cAf6_inten_status_CDR2IntEnable_Mask)) ? cAtTrue : cAtFalse;
    }

static AtDevice DeviceGet(ThaIpCore self)
    {
    return AtIpCoreDeviceGet((AtIpCore)self);
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(DeviceGet(self), cAtModulePdh);

    ThaIpCoreHwInterruptEnable(self,
                               cAf6_inten_ctrl_DE1Grp1IntEnable_Mask |
                               cAf6_inten_ctrl_DE3Grp1IntEnable_Mask |
                               cAf6_inten_ctrl_DE1Grp0IntEnable_Mask |
                               cAf6_inten_ctrl_DE3Grp0IntEnable_Mask |
                               cAf6_inten_ctrl_PRMIntEnable_Mask     |
                               cAf6_inten_ctrl_MDLIntEnable_Mask,
                               enable);

    ThaModulePdhDe3InterruptEnable(pdhModule, enable);
    ThaModulePdhDe1InterruptEnable(pdhModule, enable);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(DeviceGet(self), cAtModulePw);

    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_PWIntEnable_Mask, enable);

    ThaModulePwInterruptEnable(modulePw, enable);
    }

static void EthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_status_EthIntEnable_Mask, enable);
    }

static void PhysicalHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self,
                               cAf6_inten_status_SysMonTempEnable_Mask|
                               cAf6_inten_status_SeuIntEnable_Mask,
                               enable);
    }

static eBool SdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_inten_ctrl_OCNLineIntEnable_Mask |
                          cAf6_inten_ctrl_OCNSTSIntEnable_Mask |
                          cAf6_inten_ctrl_OCNVTIntEnable_Mask)) ? cAtTrue : cAtFalse;
    }

static void SdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)AtDeviceModuleGet(DeviceGet(self), cAtModuleSdh);

    ThaIpCoreHwInterruptEnable(self,
                               cAf6_inten_ctrl_OCNLineIntEnable_Mask |
                               cAf6_inten_ctrl_OCNSTSIntEnable_Mask |
                               cAf6_inten_ctrl_OCNVTIntEnable_Mask,
                               enable);

    ThaModuleSdhLineInterruptEnable(moduleSdh, enable);
    ThaModuleSdhStsInterruptEnable(moduleSdh, enable);
    ThaModuleSdhVtInterruptEnable(moduleSdh, enable);
    }

static void ClockHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self,
                               cAf6_inten_status_CDR1IntEnable_Mask|
                               cAf6_inten_status_CDR2IntEnable_Mask,
                               enable);
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, PdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EthCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ThermalCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SemCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ClockCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, EthHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PhysicalHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ClockHwInterruptEnable);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

ThaIntrController Tha60210031InterruptControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60210031InterruptController));

    /* Super constructor */
    if (Tha60210021InterruptControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;
    return self;
    }

ThaIntrController Tha60210031InterruptControllerNew(AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60210031InterruptController));
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031InterruptControllerObjectInit(newController, core);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210031Device.h
 * 
 * Created Date: Jun 8, 2015
 *
 * Description : Product 60210031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031DEVICE_H_
#define _THA60210031DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/man/Tha60150011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031Device * Tha60210031Device;

typedef struct tTha60210031DeviceMethods
    {
    eAtRet (*HwDiagnosticModeEnable)(Tha60210031Device self, eBool enable);
    uint32 (*DdrCurrentStatusRegister)(Tha60210031Device self);
    uint32 (*StartVersionSwHandleQsgmiiLanes)(Tha60210031Device self);
    void (*EthStickyDebug)(Tha60210031Device self);
    eBool (*DdrCalibCurrentStatusIsDone)(Tha60210031Device self, uint32 regVal, uint32 mask);
    void (*PllStickyDebug)(Tha60210031Device self);
    void (*PllStatusDebug)(Tha60210031Device self);
    void (*CoreStickyDebug)(Tha60210031Device self);
    void (*CoreStatusDebug)(Tha60210031Device self);
    eAtRet (*ListenerInstall)(Tha60210031Device self);

    /* Backward compatible */
    eBool (*Ec1LoopbackIsSupported)(Tha60210031Device self);

    /* Hardware flush */
    void (*PdhHwFlush)(Tha60210031Device self);
    void (*MapHwFlush)(Tha60210031Device self);
    void (*PmFmHwFlush)(Tha60210031Device self);
    void (*CdrHwFlush)(Tha60210031Device self);
    void (*PweHwFlush)(Tha60210031Device self);
    }tTha60210031DeviceMethods;

typedef struct tTha60210031Device
    {
    tTha60150011Device super;
    const tTha60210031DeviceMethods *methods;

    uint32 writeFailCount;

    /*async init*/
    uint32 asyncHwFlushState;
    uint32 asyncInitState;
    }tTha60210031Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60210031DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);
ThaIntrController Tha60210031InterruptControllerNew(AtIpCore core);
uint32 Tha60210031DeviceStartVersionSupportMbitAlarm(AtDevice self);
uint32 Tha60210031DeviceStartVersionSupportPwDuplicatedPacket(AtDevice self);
uint32 Tha60210031DeviceStartVersionSupportEthSerdesInterrupt(AtDevice self);
uint32 Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(AtDevice self);
uint32 Tha60210031DeviceStartVersionSupportHspw(AtDevice self);
uint32 Tha60210031DeviceStartVersionSupportFourKCell(AtDevice self);
uint32 Tha60210031StartVersionSupportSemValidation(AtDevice self);
uint32 Tha60210031StartVersionSupportThermalSensor(AtDevice self);
eBool Tha60210031ShouldDisableEc1Feature(AtDevice device);

/* For debugging */
void Tha60210031CounterStickyDebug(AtDevice self);
void Tha60210031PweStickyDebug(AtDevice self);
void Tha60210031PdaStickyDebug(AtDevice self);
void Tha60210031EthStickyDebug(AtDevice self);
void Tha60210031CoreStickyDebug(AtDevice self);
void Tha60210031CoreStatusDebug(AtDevice self);
void Tha60210031CdrStickyDebug(AtDevice self);
void Tha60210031CdrOcnStickyDebug(AtDevice self);
void Tha60210031QsgmiiSgmiiSticky1Debug(AtDevice self);
void Tha60210031QsgmiiSgmiiSticky2Debug(AtDevice self);
void Tha60210031QsgmiiSgmiiStatusDebug(AtDevice self);
void Tha60210031XfiSticky1Debug(AtDevice self);
void Tha60210031XfiSticky2Debug(AtDevice self);
void Tha60210031ClockDebug(AtDevice self);
void Tha60210031DevicePllStickyDebug(AtDevice self);
void Tha60210031DevicePllStatusDebug(AtDevice self);

eBool Tha60210031DeviceEc1LoopbackIsSupported(AtDevice self);

AtSemController Tha60210031SemControllerNew(AtDevice device, uint32 semId);
AtThermalSensor Tha60210031ThermalSensorNew(AtDevice device);

eBool Tha60210031DeviceDdrCalibCurrentStatusIsDone(Tha60210031Device self, uint32 regVal, uint32 mask);

/* Hardware flush */
void Tha60210031DeviceHwFlushPdhSlice0(ThaDevice self);
void Tha60210031DeviceHwFlushMap0(ThaDevice self);
void Tha60210031DeviceHwFlushCdr0(ThaDevice self);
void Tha60210031DeviceHwFlushBert(ThaDevice self);
void Tha60210031DeviceHwFlushPla(ThaDevice self);
void Tha60210031DeviceHwFlushPwe(ThaDevice self);
void Tha60210031DeviceHwFlushPrm(ThaDevice self);
void Tha60210031DeviceHwFlushMdl(ThaDevice self);
void Tha60210031DeviceHwFlushRamErrorGenerator(ThaDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031DEVICE_H_ */


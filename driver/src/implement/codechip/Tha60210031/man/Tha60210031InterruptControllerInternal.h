/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60210031InterruptControllerInternal.h
 * 
 * Created Date: Nov 3, 2017
 *
 * Description : 60210031 InterruptController internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031INTERRUPTCONTROLLERINTERNAL_H_
#define _THA60210031INTERRUPTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210021/man/Tha60210021InterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031InterruptController
    {
    tTha60210021InterruptController super;
    } tTha60210031InterruptController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaIntrController Tha60210031InterruptControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031INTERRUPTCONTROLLERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210031DeviceReg.h
 * 
 * Created Date: Jul 24, 2015
 *
 * Description : Device registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031DEVICEREG_H_
#define _THA60210031DEVICEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cDeviceStatusRegisters          0xf00060
#define cDeviceStickyRegisters          0xf00050
#define cAllDdrsCalibStatusMask         cBit2_0
#define cXfiActivePllLockedMask         cBit5
#define cXfiActivePllLockedShift        5
#define cXfiStandbyPllLockedMask        cBit4
#define cXfiStandbyPllLockedShift       4
#define cEthSerdesLinkUpMask(serdesId)  (cBit28 << (serdesId))
#define cEthSerdesLinkUpShift(serdesId) (28 + (serdesId))

/* Customer platform */
#define cDeviceGePllLocked              cBit10
#define cDeviceSpllLocked               cBit9
#define cDeviceSys25PllLocked           cBit8
#define cDevicePllStickyMask (cAllDdrsCalibStatusMask|cDeviceSys25PllLocked|cDeviceSpllLocked|cDeviceGePllLocked)
#define cDdrCalibStatusMask(ddrId)     (cBit0 << (ddrId))
#define cDdrCalibStatusShift(ddrId)    (ddrId)

/* Add this for Kintex Platform */
#define cDeviceKintexAvl4Done             cBit7
#define cDeviceKintexAvl2Done             cBit5
#define cDeviceKintexAvl1Done             cBit4
#define cDeviceKintexSpllLocked           cBit0
#define cDeviceKintexPllStickyMask (cDeviceKintexSpllLocked|cDeviceKintexAvl1Done|cDeviceKintexAvl2Done|cDeviceKintexAvl4Done)
#define cKintexDdrCalibStatusMask(ddrId)  (cBit4 << (ddrId))
#define cKintexDdrCalibStatusShift(ddrId) ((ddrId) + 4)
#define cKintexAllDdrsCalibStatusMask     (cDeviceKintexAvl4Done|cDeviceKintexAvl2Done|cDeviceKintexAvl1Done)

/*------------------------------------------------------------------------------
Reg Name   : Device Version ID
Reg Addr   : 0x00_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates the module' name and version.

------------------------------------------------------------------------------*/
#define cAf6Reg_VersionID_Base                                                                        0x000000
#define cAf6Reg_VersionID                                                                             0x000000
#define cAf6Reg_VersionID_WidthVal                                                                          32
#define cAf6Reg_VersionID_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: Year
BitField Type: RO
BitField Desc: 2013
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_VersionID_Year_Bit_Start                                                                       24
#define cAf6_VersionID_Year_Bit_End                                                                         31
#define cAf6_VersionID_Year_Mask                                                                     cBit31_24
#define cAf6_VersionID_Year_Shift                                                                           24
#define cAf6_VersionID_Year_MaxVal                                                                        0xff
#define cAf6_VersionID_Year_MinVal                                                                         0x0
#define cAf6_VersionID_Year_RstVal                                                                        0x0D

/*--------------------------------------
BitField Name: Week
BitField Type: RO
BitField Desc: Week Synthesized FPGA
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_VersionID_Week_Bit_Start                                                                       16
#define cAf6_VersionID_Week_Bit_End                                                                         23
#define cAf6_VersionID_Week_Mask                                                                     cBit23_16
#define cAf6_VersionID_Week_Shift                                                                           16
#define cAf6_VersionID_Week_MaxVal                                                                        0xff
#define cAf6_VersionID_Week_MinVal                                                                         0x0
#define cAf6_VersionID_Week_RstVal                                                                        0x0C

/*--------------------------------------
BitField Name: Day
BitField Type: RO
BitField Desc: Day Synthesized FPGA
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_VersionID_Day_Bit_Start                                                                         8
#define cAf6_VersionID_Day_Bit_End                                                                          15
#define cAf6_VersionID_Day_Mask                                                                       cBit15_8
#define cAf6_VersionID_Day_Shift                                                                             8
#define cAf6_VersionID_Day_MaxVal                                                                         0xff
#define cAf6_VersionID_Day_MinVal                                                                          0x0
#define cAf6_VersionID_Day_RstVal                                                                         0x16

/*--------------------------------------
BitField Name: VerID
BitField Type: RO
BitField Desc: FPGA Version
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_VersionID_VerID_Bit_Start                                                                       4
#define cAf6_VersionID_VerID_Bit_End                                                                         7
#define cAf6_VersionID_VerID_Mask                                                                      cBit7_4
#define cAf6_VersionID_VerID_Shift                                                                           4
#define cAf6_VersionID_VerID_MaxVal                                                                        0xf
#define cAf6_VersionID_VerID_MinVal                                                                        0x0
#define cAf6_VersionID_VerID_RstVal                                                                        0xO

/*--------------------------------------
BitField Name: NumID
BitField Type: RO
BitField Desc: Number of FPGA Version
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_VersionID_NumID_Bit_Start                                                                       0
#define cAf6_VersionID_NumID_Bit_End                                                                         3
#define cAf6_VersionID_NumID_Mask                                                                      cBit3_0
#define cAf6_VersionID_NumID_Shift                                                                           0
#define cAf6_VersionID_NumID_MaxVal                                                                        0xf
#define cAf6_VersionID_NumID_MinVal                                                                        0x0
#define cAf6_VersionID_NumID_RstVal                                                                        0x1


/*------------------------------------------------------------------------------
Reg Name   : GLB Sub-Core Active
Reg Addr   : 0x00_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_Active_Base                                                                           0x000001
#define cAf6Reg_Active                                                                                0x000001
#define cAf6Reg_Active_WidthVal                                                                             32
#define cAf6Reg_Active_WriteMask                                                                           0x0

/*--------------------------------------
BitField Name: PmcPact
BitField Type: RW
BitField Desc: PMC Active 1: Enable 0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_Active_PmcPact_Bit_Start                                                                        9
#define cAf6_Active_PmcPact_Bit_End                                                                          9
#define cAf6_Active_PmcPact_Mask                                                                         cBit9
#define cAf6_Active_PmcPact_Shift                                                                            9
#define cAf6_Active_PmcPact_MaxVal                                                                         0x1
#define cAf6_Active_PmcPact_MinVal                                                                         0x0
#define cAf6_Active_PmcPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: EthPact
BitField Type: RW
BitField Desc: Ethernet Interface Active 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Active_EthPact_Bit_Start                                                                        8
#define cAf6_Active_EthPact_Bit_End                                                                          8
#define cAf6_Active_EthPact_Mask                                                                         cBit8
#define cAf6_Active_EthPact_Shift                                                                            8
#define cAf6_Active_EthPact_MaxVal                                                                         0x1
#define cAf6_Active_EthPact_MinVal                                                                         0x0
#define cAf6_Active_EthPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: cdrPact
BitField Type: RW
BitField Desc: CDR Active 1: Enable 0: Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Active_cdrPact_Bit_Start                                                                        7
#define cAf6_Active_cdrPact_Bit_End                                                                          7
#define cAf6_Active_cdrPact_Mask                                                                         cBit7
#define cAf6_Active_cdrPact_Shift                                                                            7
#define cAf6_Active_cdrPact_MaxVal                                                                         0x1
#define cAf6_Active_cdrPact_MinVal                                                                         0x0
#define cAf6_Active_cdrPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: claPact
BitField Type: RW
BitField Desc: CLA Active 1: Enable 0: Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Active_claPact_Bit_Start                                                                        6
#define cAf6_Active_claPact_Bit_End                                                                          6
#define cAf6_Active_claPact_Mask                                                                         cBit6
#define cAf6_Active_claPact_Shift                                                                            6
#define cAf6_Active_claPact_MaxVal                                                                         0x1
#define cAf6_Active_claPact_MinVal                                                                         0x0
#define cAf6_Active_claPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: PwePact
BitField Type: RW
BitField Desc: PWE Active 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Active_PwePact_Bit_Start                                                                        5
#define cAf6_Active_PwePact_Bit_End                                                                          5
#define cAf6_Active_PwePact_Mask                                                                         cBit5
#define cAf6_Active_PwePact_Shift                                                                            5
#define cAf6_Active_PwePact_MaxVal                                                                         0x1
#define cAf6_Active_PwePact_MinVal                                                                         0x0
#define cAf6_Active_PwePact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: PdaPact
BitField Type: RW
BitField Desc: PDA Active 1: Enable 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Active_PdaPact_Bit_Start                                                                        4
#define cAf6_Active_PdaPact_Bit_End                                                                          4
#define cAf6_Active_PdaPact_Mask                                                                         cBit4
#define cAf6_Active_PdaPact_Shift                                                                            4
#define cAf6_Active_PdaPact_MaxVal                                                                         0x1
#define cAf6_Active_PdaPact_MinVal                                                                         0x0
#define cAf6_Active_PdaPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: PlaPact
BitField Type: RW
BitField Desc: PLA Active 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Active_PlaPact_Bit_Start                                                                        3
#define cAf6_Active_PlaPact_Bit_End                                                                          3
#define cAf6_Active_PlaPact_Mask                                                                         cBit3
#define cAf6_Active_PlaPact_Shift                                                                            3
#define cAf6_Active_PlaPact_MaxVal                                                                         0x1
#define cAf6_Active_PlaPact_MinVal                                                                         0x0
#define cAf6_Active_PlaPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: MapPact
BitField Type: RW
BitField Desc: MAP Active 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Active_MapPact_Bit_Start                                                                        2
#define cAf6_Active_MapPact_Bit_End                                                                          2
#define cAf6_Active_MapPact_Mask                                                                         cBit2
#define cAf6_Active_MapPact_Shift                                                                            2
#define cAf6_Active_MapPact_MaxVal                                                                         0x1
#define cAf6_Active_MapPact_MinVal                                                                         0x0
#define cAf6_Active_MapPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: PdhPact
BitField Type: RW
BitField Desc: PDH Active 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Active_PdhPact_Bit_Start                                                                        1
#define cAf6_Active_PdhPact_Bit_End                                                                          1
#define cAf6_Active_PdhPact_Mask                                                                         cBit1
#define cAf6_Active_PdhPact_Shift                                                                            1
#define cAf6_Active_PdhPact_MaxVal                                                                         0x1
#define cAf6_Active_PdhPact_MinVal                                                                         0x0
#define cAf6_Active_PdhPact_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: OcnPact
BitField Type: RW
BitField Desc: OCN Active 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Active_OcnPact_Bit_Start                                                                        0
#define cAf6_Active_OcnPact_Bit_End                                                                          0
#define cAf6_Active_OcnPact_Mask                                                                         cBit0
#define cAf6_Active_OcnPact_Shift                                                                            0
#define cAf6_Active_OcnPact_MaxVal                                                                         0x1
#define cAf6_Active_OcnPact_MinVal                                                                         0x0
#define cAf6_Active_OcnPact_RstVal                                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Status
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_status_Base                                                                     0x000002
#define cAf6Reg_inten_status                                                                          0x000002
#define cAf6Reg_inten_status_WidthVal                                                                       32
#define cAf6Reg_inten_status_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: CDR2IntEnable
BitField Type: RW
BitField Desc: CDR Group1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_inten_status_CDR2IntEnable_Bit_Start                                                           29
#define cAf6_inten_status_CDR2IntEnable_Bit_End                                                             29
#define cAf6_inten_status_CDR2IntEnable_Mask                                                            cBit29
#define cAf6_inten_status_CDR2IntEnable_Shift                                                               29
#define cAf6_inten_status_CDR2IntEnable_MaxVal                                                             0x1
#define cAf6_inten_status_CDR2IntEnable_MinVal                                                             0x0
#define cAf6_inten_status_CDR2IntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: CDR1IntEnable
BitField Type: RW
BitField Desc: CDR Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_status_CDR1IntEnable_Bit_Start                                                           28
#define cAf6_inten_status_CDR1IntEnable_Bit_End                                                             28
#define cAf6_inten_status_CDR1IntEnable_Mask                                                            cBit28
#define cAf6_inten_status_CDR1IntEnable_Shift                                                               28
#define cAf6_inten_status_CDR1IntEnable_MaxVal                                                             0x1
#define cAf6_inten_status_CDR1IntEnable_MinVal                                                             0x0
#define cAf6_inten_status_CDR1IntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_status_SysMonTempEnable_Bit_Start                                                        27
#define cAf6_inten_status_SysMonTempEnable_Bit_End                                                          27
#define cAf6_inten_status_SysMonTempEnable_Mask                                                         cBit27
#define cAf6_inten_status_SysMonTempEnable_Shift                                                            27
#define cAf6_inten_status_SysMonTempEnable_MaxVal                                                          0x1
#define cAf6_inten_status_SysMonTempEnable_MinVal                                                          0x0
#define cAf6_inten_status_SysMonTempEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_status_ParIntEnable_Bit_Start                                                            26
#define cAf6_inten_status_ParIntEnable_Bit_End                                                              26
#define cAf6_inten_status_ParIntEnable_Mask                                                             cBit26
#define cAf6_inten_status_ParIntEnable_Shift                                                                26
#define cAf6_inten_status_ParIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_ParIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_ParIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: MDLIntEnable
BitField Type: RW
BitField Desc: MDL DE3 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_inten_status_MDLIntEnable_Bit_Start                                                            25
#define cAf6_inten_status_MDLIntEnable_Bit_End                                                              25
#define cAf6_inten_status_MDLIntEnable_Mask                                                             cBit25
#define cAf6_inten_status_MDLIntEnable_Shift                                                                25
#define cAf6_inten_status_MDLIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_MDLIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_MDLIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM DE1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_status_PRMIntEnable_Bit_Start                                                            24
#define cAf6_inten_status_PRMIntEnable_Bit_End                                                              24
#define cAf6_inten_status_PRMIntEnable_Mask                                                             cBit24
#define cAf6_inten_status_PRMIntEnable_Shift                                                                24
#define cAf6_inten_status_PRMIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_PRMIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_PRMIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_status_PMIntEnable_Bit_Start                                                             21
#define cAf6_inten_status_PMIntEnable_Bit_End                                                               21
#define cAf6_inten_status_PMIntEnable_Mask                                                              cBit21
#define cAf6_inten_status_PMIntEnable_Shift                                                                 21
#define cAf6_inten_status_PMIntEnable_MaxVal                                                               0x1
#define cAf6_inten_status_PMIntEnable_MinVal                                                               0x0
#define cAf6_inten_status_PMIntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_status_FMIntEnable_Bit_Start                                                             20
#define cAf6_inten_status_FMIntEnable_Bit_End                                                               20
#define cAf6_inten_status_FMIntEnable_Mask                                                              cBit20
#define cAf6_inten_status_FMIntEnable_Shift                                                                 20
#define cAf6_inten_status_FMIntEnable_MaxVal                                                               0x1
#define cAf6_inten_status_FMIntEnable_MinVal                                                               0x0
#define cAf6_inten_status_FMIntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: SeuIntEnable
BitField Type: RW
BitField Desc: SEU Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_inten_status_SeuIntEnable_Bit_Start                                                            19
#define cAf6_inten_status_SeuIntEnable_Bit_End                                                              19
#define cAf6_inten_status_SeuIntEnable_Mask                                                             cBit19
#define cAf6_inten_status_SeuIntEnable_Shift                                                                19
#define cAf6_inten_status_SeuIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_SeuIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_SeuIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_status_EthIntEnable_Bit_Start                                                            18
#define cAf6_inten_status_EthIntEnable_Bit_End                                                              18
#define cAf6_inten_status_EthIntEnable_Mask                                                             cBit18
#define cAf6_inten_status_EthIntEnable_Shift                                                                18
#define cAf6_inten_status_EthIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_EthIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_EthIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_status_PWIntEnable_Bit_Start                                                             16
#define cAf6_inten_status_PWIntEnable_Bit_End                                                               16
#define cAf6_inten_status_PWIntEnable_Mask                                                              cBit16
#define cAf6_inten_status_PWIntEnable_Shift                                                                 16
#define cAf6_inten_status_PWIntEnable_MaxVal                                                               0x1
#define cAf6_inten_status_PWIntEnable_MinVal                                                               0x0
#define cAf6_inten_status_PWIntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: DE1Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_inten_status_DE1Grp1IntEnable_Bit_Start                                                        13
#define cAf6_inten_status_DE1Grp1IntEnable_Bit_End                                                          13
#define cAf6_inten_status_DE1Grp1IntEnable_Mask                                                         cBit13
#define cAf6_inten_status_DE1Grp1IntEnable_Shift                                                            13
#define cAf6_inten_status_DE1Grp1IntEnable_MaxVal                                                          0x1
#define cAf6_inten_status_DE1Grp1IntEnable_MinVal                                                          0x0
#define cAf6_inten_status_DE1Grp1IntEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: DE3Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_inten_status_DE3Grp1IntEnable_Bit_Start                                                        12
#define cAf6_inten_status_DE3Grp1IntEnable_Bit_End                                                          12
#define cAf6_inten_status_DE3Grp1IntEnable_Mask                                                         cBit12
#define cAf6_inten_status_DE3Grp1IntEnable_Shift                                                            12
#define cAf6_inten_status_DE3Grp1IntEnable_MaxVal                                                          0x1
#define cAf6_inten_status_DE3Grp1IntEnable_MinVal                                                          0x0
#define cAf6_inten_status_DE3Grp1IntEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: DE1Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_status_DE1Grp0IntEnable_Bit_Start                                                         9
#define cAf6_inten_status_DE1Grp0IntEnable_Bit_End                                                           9
#define cAf6_inten_status_DE1Grp0IntEnable_Mask                                                          cBit9
#define cAf6_inten_status_DE1Grp0IntEnable_Shift                                                             9
#define cAf6_inten_status_DE1Grp0IntEnable_MaxVal                                                          0x1
#define cAf6_inten_status_DE1Grp0IntEnable_MinVal                                                          0x0
#define cAf6_inten_status_DE1Grp0IntEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: DE3Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_inten_status_DE3Grp0IntEnable_Bit_Start                                                         8
#define cAf6_inten_status_DE3Grp0IntEnable_Bit_End                                                           8
#define cAf6_inten_status_DE3Grp0IntEnable_Mask                                                          cBit8
#define cAf6_inten_status_DE3Grp0IntEnable_Shift                                                             8
#define cAf6_inten_status_DE3Grp0IntEnable_MaxVal                                                          0x1
#define cAf6_inten_status_DE3Grp0IntEnable_MinVal                                                          0x0
#define cAf6_inten_status_DE3Grp0IntEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: OCNVTIntEnable
BitField Type: RW
BitField Desc: OCN VT Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_inten_status_OCNVTIntEnable_Bit_Start                                                           2
#define cAf6_inten_status_OCNVTIntEnable_Bit_End                                                             2
#define cAf6_inten_status_OCNVTIntEnable_Mask                                                            cBit2
#define cAf6_inten_status_OCNVTIntEnable_Shift                                                               2
#define cAf6_inten_status_OCNVTIntEnable_MaxVal                                                            0x1
#define cAf6_inten_status_OCNVTIntEnable_MinVal                                                            0x0
#define cAf6_inten_status_OCNVTIntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: OCNSTSIntEnable
BitField Type: RW
BitField Desc: OCN STS Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_inten_status_OCNSTSIntEnable_Bit_Start                                                          1
#define cAf6_inten_status_OCNSTSIntEnable_Bit_End                                                            1
#define cAf6_inten_status_OCNSTSIntEnable_Mask                                                           cBit1
#define cAf6_inten_status_OCNSTSIntEnable_Shift                                                              1
#define cAf6_inten_status_OCNSTSIntEnable_MaxVal                                                           0x1
#define cAf6_inten_status_OCNSTSIntEnable_MinVal                                                           0x0
#define cAf6_inten_status_OCNSTSIntEnable_RstVal                                                           0x0

/*--------------------------------------
BitField Name: OCNLineIntEnable
BitField Type: RW
BitField Desc: OCN Line Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_status_OCNLineIntEnable_Bit_Start                                                         0
#define cAf6_inten_status_OCNLineIntEnable_Bit_End                                                           0
#define cAf6_inten_status_OCNLineIntEnable_Mask                                                          cBit0
#define cAf6_inten_status_OCNLineIntEnable_Shift                                                             0
#define cAf6_inten_status_OCNLineIntEnable_MaxVal                                                          0x1
#define cAf6_inten_status_OCNLineIntEnable_MinVal                                                          0x0
#define cAf6_inten_status_OCNLineIntEnable_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Enable Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_ctrl_Base                                                                       0x000003
#define cAf6Reg_inten_ctrl                                                                            0x000003
#define cAf6Reg_inten_ctrl_WidthVal                                                                         32
#define cAf6Reg_inten_ctrl_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: CDR2IntEnable
BitField Type: RW
BitField Desc: CDR Group1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_inten_ctrl_CDR2IntEnable_Bit_Start                                                             29
#define cAf6_inten_ctrl_CDR2IntEnable_Bit_End                                                               29
#define cAf6_inten_ctrl_CDR2IntEnable_Mask                                                              cBit29
#define cAf6_inten_ctrl_CDR2IntEnable_Shift                                                                 29
#define cAf6_inten_ctrl_CDR2IntEnable_MaxVal                                                               0x1
#define cAf6_inten_ctrl_CDR2IntEnable_MinVal                                                               0x0
#define cAf6_inten_ctrl_CDR2IntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: CDR1IntEnable
BitField Type: RW
BitField Desc: CDR Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_ctrl_CDR1IntEnable_Bit_Start                                                             28
#define cAf6_inten_ctrl_CDR1IntEnable_Bit_End                                                               28
#define cAf6_inten_ctrl_CDR1IntEnable_Mask                                                              cBit28
#define cAf6_inten_ctrl_CDR1IntEnable_Shift                                                                 28
#define cAf6_inten_ctrl_CDR1IntEnable_MaxVal                                                               0x1
#define cAf6_inten_ctrl_CDR1IntEnable_MinVal                                                               0x0
#define cAf6_inten_ctrl_CDR1IntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_ctrl_SysMonTempEnable_Bit_Start                                                          27
#define cAf6_inten_ctrl_SysMonTempEnable_Bit_End                                                            27
#define cAf6_inten_ctrl_SysMonTempEnable_Mask                                                           cBit27
#define cAf6_inten_ctrl_SysMonTempEnable_Shift                                                              27
#define cAf6_inten_ctrl_SysMonTempEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_SysMonTempEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_SysMonTempEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_ctrl_ParIntEnable_Bit_Start                                                              26
#define cAf6_inten_ctrl_ParIntEnable_Bit_End                                                                26
#define cAf6_inten_ctrl_ParIntEnable_Mask                                                               cBit26
#define cAf6_inten_ctrl_ParIntEnable_Shift                                                                  26
#define cAf6_inten_ctrl_ParIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_ParIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_ParIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: MDLIntEnable
BitField Type: RW
BitField Desc: MDL DE3 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_inten_ctrl_MDLIntEnable_Bit_Start                                                              25
#define cAf6_inten_ctrl_MDLIntEnable_Bit_End                                                                25
#define cAf6_inten_ctrl_MDLIntEnable_Mask                                                               cBit25
#define cAf6_inten_ctrl_MDLIntEnable_Shift                                                                  25
#define cAf6_inten_ctrl_MDLIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_MDLIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_MDLIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM DE1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_ctrl_PRMIntEnable_Bit_Start                                                              24
#define cAf6_inten_ctrl_PRMIntEnable_Bit_End                                                                24
#define cAf6_inten_ctrl_PRMIntEnable_Mask                                                               cBit24
#define cAf6_inten_ctrl_PRMIntEnable_Shift                                                                  24
#define cAf6_inten_ctrl_PRMIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_PRMIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_PRMIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_ctrl_PMIntEnable_Bit_Start                                                               21
#define cAf6_inten_ctrl_PMIntEnable_Bit_End                                                                 21
#define cAf6_inten_ctrl_PMIntEnable_Mask                                                                cBit21
#define cAf6_inten_ctrl_PMIntEnable_Shift                                                                   21
#define cAf6_inten_ctrl_PMIntEnable_MaxVal                                                                 0x1
#define cAf6_inten_ctrl_PMIntEnable_MinVal                                                                 0x0
#define cAf6_inten_ctrl_PMIntEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_ctrl_FMIntEnable_Bit_Start                                                               20
#define cAf6_inten_ctrl_FMIntEnable_Bit_End                                                                 20
#define cAf6_inten_ctrl_FMIntEnable_Mask                                                                cBit20
#define cAf6_inten_ctrl_FMIntEnable_Shift                                                                   20
#define cAf6_inten_ctrl_FMIntEnable_MaxVal                                                                 0x1
#define cAf6_inten_ctrl_FMIntEnable_MinVal                                                                 0x0
#define cAf6_inten_ctrl_FMIntEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_ctrl_EthIntEnable_Bit_Start                                                              18
#define cAf6_inten_ctrl_EthIntEnable_Bit_End                                                                18
#define cAf6_inten_ctrl_EthIntEnable_Mask                                                               cBit18
#define cAf6_inten_ctrl_EthIntEnable_Shift                                                                  18
#define cAf6_inten_ctrl_EthIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_EthIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_EthIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_ctrl_PWIntEnable_Bit_Start                                                               16
#define cAf6_inten_ctrl_PWIntEnable_Bit_End                                                                 16
#define cAf6_inten_ctrl_PWIntEnable_Mask                                                                cBit16
#define cAf6_inten_ctrl_PWIntEnable_Shift                                                                   16
#define cAf6_inten_ctrl_PWIntEnable_MaxVal                                                                 0x1
#define cAf6_inten_ctrl_PWIntEnable_MinVal                                                                 0x0
#define cAf6_inten_ctrl_PWIntEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: DE1Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_inten_ctrl_DE1Grp1IntEnable_Bit_Start                                                          13
#define cAf6_inten_ctrl_DE1Grp1IntEnable_Bit_End                                                            13
#define cAf6_inten_ctrl_DE1Grp1IntEnable_Mask                                                           cBit13
#define cAf6_inten_ctrl_DE1Grp1IntEnable_Shift                                                              13
#define cAf6_inten_ctrl_DE1Grp1IntEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_DE1Grp1IntEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_DE1Grp1IntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: DE3Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_inten_ctrl_DE3Grp1IntEnable_Bit_Start                                                          12
#define cAf6_inten_ctrl_DE3Grp1IntEnable_Bit_End                                                            12
#define cAf6_inten_ctrl_DE3Grp1IntEnable_Mask                                                           cBit12
#define cAf6_inten_ctrl_DE3Grp1IntEnable_Shift                                                              12
#define cAf6_inten_ctrl_DE3Grp1IntEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_DE3Grp1IntEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_DE3Grp1IntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: DE1Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_ctrl_DE1Grp0IntEnable_Bit_Start                                                           9
#define cAf6_inten_ctrl_DE1Grp0IntEnable_Bit_End                                                             9
#define cAf6_inten_ctrl_DE1Grp0IntEnable_Mask                                                            cBit9
#define cAf6_inten_ctrl_DE1Grp0IntEnable_Shift                                                               9
#define cAf6_inten_ctrl_DE1Grp0IntEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_DE1Grp0IntEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_DE1Grp0IntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: DE3Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_inten_ctrl_DE3Grp0IntEnable_Bit_Start                                                           8
#define cAf6_inten_ctrl_DE3Grp0IntEnable_Bit_End                                                             8
#define cAf6_inten_ctrl_DE3Grp0IntEnable_Mask                                                            cBit8
#define cAf6_inten_ctrl_DE3Grp0IntEnable_Shift                                                               8
#define cAf6_inten_ctrl_DE3Grp0IntEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_DE3Grp0IntEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_DE3Grp0IntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: OCNVTIntEnable
BitField Type: RW
BitField Desc: OCN VT Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_inten_ctrl_OCNVTIntEnable_Bit_Start                                                             2
#define cAf6_inten_ctrl_OCNVTIntEnable_Bit_End                                                               2
#define cAf6_inten_ctrl_OCNVTIntEnable_Mask                                                              cBit2
#define cAf6_inten_ctrl_OCNVTIntEnable_Shift                                                                 2
#define cAf6_inten_ctrl_OCNVTIntEnable_MaxVal                                                              0x1
#define cAf6_inten_ctrl_OCNVTIntEnable_MinVal                                                              0x0
#define cAf6_inten_ctrl_OCNVTIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: OCNSTSIntEnable
BitField Type: RW
BitField Desc: OCN STS Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_inten_ctrl_OCNSTSIntEnable_Bit_Start                                                            1
#define cAf6_inten_ctrl_OCNSTSIntEnable_Bit_End                                                              1
#define cAf6_inten_ctrl_OCNSTSIntEnable_Mask                                                             cBit1
#define cAf6_inten_ctrl_OCNSTSIntEnable_Shift                                                                1
#define cAf6_inten_ctrl_OCNSTSIntEnable_MaxVal                                                             0x1
#define cAf6_inten_ctrl_OCNSTSIntEnable_MinVal                                                             0x0
#define cAf6_inten_ctrl_OCNSTSIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: OCNLineIntEnable
BitField Type: RW
BitField Desc: OCN Line Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_ctrl_OCNLineIntEnable_Bit_Start                                                           0
#define cAf6_inten_ctrl_OCNLineIntEnable_Bit_End                                                             0
#define cAf6_inten_ctrl_OCNLineIntEnable_Mask                                                            cBit0
#define cAf6_inten_ctrl_OCNLineIntEnable_Shift                                                               0
#define cAf6_inten_ctrl_OCNLineIntEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_OCNLineIntEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_OCNLineIntEnable_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Restore Control
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt restore enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_restore_Base                                                                    0x000004
#define cAf6Reg_inten_restore                                                                         0x000004
#define cAf6Reg_inten_restore_WidthVal                                                                      32
#define cAf6Reg_inten_restore_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CDR2IntEnable
BitField Type: RW
BitField Desc: CDR Group1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_inten_restore_CDR2IntEnable_Bit_Start                                                          29
#define cAf6_inten_restore_CDR2IntEnable_Bit_End                                                            29
#define cAf6_inten_restore_CDR2IntEnable_Mask                                                           cBit29
#define cAf6_inten_restore_CDR2IntEnable_Shift                                                              29
#define cAf6_inten_restore_CDR2IntEnable_MaxVal                                                            0x1
#define cAf6_inten_restore_CDR2IntEnable_MinVal                                                            0x0
#define cAf6_inten_restore_CDR2IntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: CDR1IntEnable
BitField Type: RW
BitField Desc: CDR Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_restore_CDR1IntEnable_Bit_Start                                                          28
#define cAf6_inten_restore_CDR1IntEnable_Bit_End                                                            28
#define cAf6_inten_restore_CDR1IntEnable_Mask                                                           cBit28
#define cAf6_inten_restore_CDR1IntEnable_Shift                                                              28
#define cAf6_inten_restore_CDR1IntEnable_MaxVal                                                            0x1
#define cAf6_inten_restore_CDR1IntEnable_MinVal                                                            0x0
#define cAf6_inten_restore_CDR1IntEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_restore_SysMonTempEnable_Bit_Start                                                       27
#define cAf6_inten_restore_SysMonTempEnable_Bit_End                                                         27
#define cAf6_inten_restore_SysMonTempEnable_Mask                                                        cBit27
#define cAf6_inten_restore_SysMonTempEnable_Shift                                                           27
#define cAf6_inten_restore_SysMonTempEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_SysMonTempEnable_MinVal                                                         0x0
#define cAf6_inten_restore_SysMonTempEnable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_restore_ParIntEnable_Bit_Start                                                           26
#define cAf6_inten_restore_ParIntEnable_Bit_End                                                             26
#define cAf6_inten_restore_ParIntEnable_Mask                                                            cBit26
#define cAf6_inten_restore_ParIntEnable_Shift                                                               26
#define cAf6_inten_restore_ParIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_ParIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_ParIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: MDLIntEnable
BitField Type: RW
BitField Desc: MDL DE3 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_inten_restore_MDLIntEnable_Bit_Start                                                           25
#define cAf6_inten_restore_MDLIntEnable_Bit_End                                                             25
#define cAf6_inten_restore_MDLIntEnable_Mask                                                            cBit25
#define cAf6_inten_restore_MDLIntEnable_Shift                                                               25
#define cAf6_inten_restore_MDLIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_MDLIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_MDLIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM DE1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_restore_PRMIntEnable_Bit_Start                                                           24
#define cAf6_inten_restore_PRMIntEnable_Bit_End                                                             24
#define cAf6_inten_restore_PRMIntEnable_Mask                                                            cBit24
#define cAf6_inten_restore_PRMIntEnable_Shift                                                               24
#define cAf6_inten_restore_PRMIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_PRMIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_PRMIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_restore_PMIntEnable_Bit_Start                                                            21
#define cAf6_inten_restore_PMIntEnable_Bit_End                                                              21
#define cAf6_inten_restore_PMIntEnable_Mask                                                             cBit21
#define cAf6_inten_restore_PMIntEnable_Shift                                                                21
#define cAf6_inten_restore_PMIntEnable_MaxVal                                                              0x1
#define cAf6_inten_restore_PMIntEnable_MinVal                                                              0x0
#define cAf6_inten_restore_PMIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_restore_FMIntEnable_Bit_Start                                                            20
#define cAf6_inten_restore_FMIntEnable_Bit_End                                                              20
#define cAf6_inten_restore_FMIntEnable_Mask                                                             cBit20
#define cAf6_inten_restore_FMIntEnable_Shift                                                                20
#define cAf6_inten_restore_FMIntEnable_MaxVal                                                              0x1
#define cAf6_inten_restore_FMIntEnable_MinVal                                                              0x0
#define cAf6_inten_restore_FMIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_restore_EthIntEnable_Bit_Start                                                           18
#define cAf6_inten_restore_EthIntEnable_Bit_End                                                             18
#define cAf6_inten_restore_EthIntEnable_Mask                                                            cBit18
#define cAf6_inten_restore_EthIntEnable_Shift                                                               18
#define cAf6_inten_restore_EthIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_EthIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_EthIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_restore_PWIntEnable_Bit_Start                                                            16
#define cAf6_inten_restore_PWIntEnable_Bit_End                                                              16
#define cAf6_inten_restore_PWIntEnable_Mask                                                             cBit16
#define cAf6_inten_restore_PWIntEnable_Shift                                                                16
#define cAf6_inten_restore_PWIntEnable_MaxVal                                                              0x1
#define cAf6_inten_restore_PWIntEnable_MinVal                                                              0x0
#define cAf6_inten_restore_PWIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DE1Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_inten_restore_DE1Grp1IntEnable_Bit_Start                                                       13
#define cAf6_inten_restore_DE1Grp1IntEnable_Bit_End                                                         13
#define cAf6_inten_restore_DE1Grp1IntEnable_Mask                                                        cBit13
#define cAf6_inten_restore_DE1Grp1IntEnable_Shift                                                           13
#define cAf6_inten_restore_DE1Grp1IntEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_DE1Grp1IntEnable_MinVal                                                         0x0
#define cAf6_inten_restore_DE1Grp1IntEnable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: DE3Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_inten_restore_DE3Grp1IntEnable_Bit_Start                                                       12
#define cAf6_inten_restore_DE3Grp1IntEnable_Bit_End                                                         12
#define cAf6_inten_restore_DE3Grp1IntEnable_Mask                                                        cBit12
#define cAf6_inten_restore_DE3Grp1IntEnable_Shift                                                           12
#define cAf6_inten_restore_DE3Grp1IntEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_DE3Grp1IntEnable_MinVal                                                         0x0
#define cAf6_inten_restore_DE3Grp1IntEnable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: DE1Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_restore_DE1Grp0IntEnable_Bit_Start                                                        9
#define cAf6_inten_restore_DE1Grp0IntEnable_Bit_End                                                          9
#define cAf6_inten_restore_DE1Grp0IntEnable_Mask                                                         cBit9
#define cAf6_inten_restore_DE1Grp0IntEnable_Shift                                                            9
#define cAf6_inten_restore_DE1Grp0IntEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_DE1Grp0IntEnable_MinVal                                                         0x0
#define cAf6_inten_restore_DE1Grp0IntEnable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: DE3Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_inten_restore_DE3Grp0IntEnable_Bit_Start                                                        8
#define cAf6_inten_restore_DE3Grp0IntEnable_Bit_End                                                          8
#define cAf6_inten_restore_DE3Grp0IntEnable_Mask                                                         cBit8
#define cAf6_inten_restore_DE3Grp0IntEnable_Shift                                                            8
#define cAf6_inten_restore_DE3Grp0IntEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_DE3Grp0IntEnable_MinVal                                                         0x0
#define cAf6_inten_restore_DE3Grp0IntEnable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: OCNVTIntEnable
BitField Type: RW
BitField Desc: OCN VT Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_inten_restore_OCNVTIntEnable_Bit_Start                                                          2
#define cAf6_inten_restore_OCNVTIntEnable_Bit_End                                                            2
#define cAf6_inten_restore_OCNVTIntEnable_Mask                                                           cBit2
#define cAf6_inten_restore_OCNVTIntEnable_Shift                                                              2
#define cAf6_inten_restore_OCNVTIntEnable_MaxVal                                                           0x1
#define cAf6_inten_restore_OCNVTIntEnable_MinVal                                                           0x0
#define cAf6_inten_restore_OCNVTIntEnable_RstVal                                                           0x0

/*--------------------------------------
BitField Name: OCNSTSIntEnable
BitField Type: RW
BitField Desc: OCN STS Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_inten_restore_OCNSTSIntEnable_Bit_Start                                                         1
#define cAf6_inten_restore_OCNSTSIntEnable_Bit_End                                                           1
#define cAf6_inten_restore_OCNSTSIntEnable_Mask                                                          cBit1
#define cAf6_inten_restore_OCNSTSIntEnable_Shift                                                             1
#define cAf6_inten_restore_OCNSTSIntEnable_MaxVal                                                          0x1
#define cAf6_inten_restore_OCNSTSIntEnable_MinVal                                                          0x0
#define cAf6_inten_restore_OCNSTSIntEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: OCNLineIntEnable
BitField Type: RW
BitField Desc: OCN Line Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_restore_OCNLineIntEnable_Bit_Start                                                        0
#define cAf6_inten_restore_OCNLineIntEnable_Bit_End                                                          0
#define cAf6_inten_restore_OCNLineIntEnable_Mask                                                         cBit0
#define cAf6_inten_restore_OCNLineIntEnable_Shift                                                            0
#define cAf6_inten_restore_OCNLineIntEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_OCNLineIntEnable_MinVal                                                         0x0
#define cAf6_inten_restore_OCNLineIntEnable_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Interrupt Enable Control
Reg Addr   : 0x00_00C0
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_Eport_Base                                                                      0x0000C0
#define cAf6Reg_inten_Eport                                                                           0x0000C0
#define cAf6Reg_inten_Eport_WidthVal                                                                        32
#define cAf6Reg_inten_Eport_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RW
BitField Desc: XFI_3    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_3_Bit_Start                                                                    19
#define cAf6_inten_Eport_XFI_3_Bit_End                                                                      19
#define cAf6_inten_Eport_XFI_3_Mask                                                                     cBit19
#define cAf6_inten_Eport_XFI_3_Shift                                                                        19
#define cAf6_inten_Eport_XFI_3_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_3_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_3_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RW
BitField Desc: XFI_2    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_2_Bit_Start                                                                    18
#define cAf6_inten_Eport_XFI_2_Bit_End                                                                      18
#define cAf6_inten_Eport_XFI_2_Mask                                                                     cBit18
#define cAf6_inten_Eport_XFI_2_Shift                                                                        18
#define cAf6_inten_Eport_XFI_2_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_2_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RW
BitField Desc: XFI_1    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_1_Bit_Start                                                                    17
#define cAf6_inten_Eport_XFI_1_Bit_End                                                                      17
#define cAf6_inten_Eport_XFI_1_Mask                                                                     cBit17
#define cAf6_inten_Eport_XFI_1_Shift                                                                        17
#define cAf6_inten_Eport_XFI_1_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_1_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_1_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RW
BitField Desc: XFI_0    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_0_Bit_Start                                                                    16
#define cAf6_inten_Eport_XFI_0_Bit_End                                                                      16
#define cAf6_inten_Eport_XFI_0_Mask                                                                     cBit16
#define cAf6_inten_Eport_XFI_0_Shift                                                                        16
#define cAf6_inten_Eport_XFI_0_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_0_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_0_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RW
BitField Desc: QSGMII_3 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_3_Bit_Start                                                                 12
#define cAf6_inten_Eport_QSGMII_3_Bit_End                                                                   12
#define cAf6_inten_Eport_QSGMII_3_Mask                                                                  cBit12
#define cAf6_inten_Eport_QSGMII_3_Shift                                                                     12
#define cAf6_inten_Eport_QSGMII_3_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_3_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_3_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RW
BitField Desc: QSGMII_2 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_2_Bit_Start                                                                  8
#define cAf6_inten_Eport_QSGMII_2_Bit_End                                                                    8
#define cAf6_inten_Eport_QSGMII_2_Mask                                                                   cBit8
#define cAf6_inten_Eport_QSGMII_2_Shift                                                                      8
#define cAf6_inten_Eport_QSGMII_2_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_2_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RW
BitField Desc: QSGMII_1 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_1_Bit_Start                                                                  4
#define cAf6_inten_Eport_QSGMII_1_Bit_End                                                                    4
#define cAf6_inten_Eport_QSGMII_1_Mask                                                                   cBit4
#define cAf6_inten_Eport_QSGMII_1_Shift                                                                      4
#define cAf6_inten_Eport_QSGMII_1_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_1_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RW
BitField Desc: QSGMII_0 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_0_Bit_Start                                                                  0
#define cAf6_inten_Eport_QSGMII_0_Bit_End                                                                    0
#define cAf6_inten_Eport_QSGMII_0_Mask                                                                   cBit0
#define cAf6_inten_Eport_QSGMII_0_Shift                                                                      0
#define cAf6_inten_Eport_QSGMII_0_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_0_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_0_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Event
Reg Addr   : 0x00_00C4
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report state change of link status.

------------------------------------------------------------------------------*/
#define cAf6Reg_linkalm_Eport_Base                                                                    0x0000C4
#define cAf6Reg_linkalm_Eport                                                                         0x0000C4
#define cAf6Reg_linkalm_Eport_WidthVal                                                                      32
#define cAf6Reg_linkalm_Eport_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: WC
BitField Desc: XFI_3    Link State change Event  1: Changed  0: Normal
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_3_Bit_Start                                                                  19
#define cAf6_linkalm_Eport_XFI_3_Bit_End                                                                    19
#define cAf6_linkalm_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linkalm_Eport_XFI_3_Shift                                                                      19
#define cAf6_linkalm_Eport_XFI_3_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_3_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: WC
BitField Desc: XFI_2    Link State change Event  1: Changed  0: Normal
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_2_Bit_Start                                                                  18
#define cAf6_linkalm_Eport_XFI_2_Bit_End                                                                    18
#define cAf6_linkalm_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linkalm_Eport_XFI_2_Shift                                                                      18
#define cAf6_linkalm_Eport_XFI_2_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_2_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: WC
BitField Desc: XFI_1    Link State change Event  1: Changed  0: Normal
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_1_Bit_Start                                                                  17
#define cAf6_linkalm_Eport_XFI_1_Bit_End                                                                    17
#define cAf6_linkalm_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linkalm_Eport_XFI_1_Shift                                                                      17
#define cAf6_linkalm_Eport_XFI_1_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_1_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: WC
BitField Desc: XFI_0    Link State change Event  1: Changed  0: Normal
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_0_Bit_Start                                                                  16
#define cAf6_linkalm_Eport_XFI_0_Bit_End                                                                    16
#define cAf6_linkalm_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linkalm_Eport_XFI_0_Shift                                                                      16
#define cAf6_linkalm_Eport_XFI_0_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_0_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_0_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: WC
BitField Desc: QSGMII_3 Link State change Event  1: Changed  0: Normal
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_3_Bit_Start                                                               12
#define cAf6_linkalm_Eport_QSGMII_3_Bit_End                                                                 12
#define cAf6_linkalm_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linkalm_Eport_QSGMII_3_Shift                                                                   12
#define cAf6_linkalm_Eport_QSGMII_3_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_3_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_3_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: WC
BitField Desc: QSGMII_2 Link State change Event  1: Changed  0: Normal
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_2_Bit_Start                                                                8
#define cAf6_linkalm_Eport_QSGMII_2_Bit_End                                                                  8
#define cAf6_linkalm_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linkalm_Eport_QSGMII_2_Shift                                                                    8
#define cAf6_linkalm_Eport_QSGMII_2_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_2_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: WC
BitField Desc: QSGMII_1 Link State change Event  1: Changed  0: Normal
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_1_Bit_Start                                                                4
#define cAf6_linkalm_Eport_QSGMII_1_Bit_End                                                                  4
#define cAf6_linkalm_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linkalm_Eport_QSGMII_1_Shift                                                                    4
#define cAf6_linkalm_Eport_QSGMII_1_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_1_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: WC
BitField Desc: QSGMII_0 Link State change Event  1: Changed  0: Normal
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_0_Bit_Start                                                                0
#define cAf6_linkalm_Eport_QSGMII_0_Bit_End                                                                  0
#define cAf6_linkalm_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linkalm_Eport_QSGMII_0_Shift                                                                    0
#define cAf6_linkalm_Eport_QSGMII_0_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_0_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Status
Reg Addr   : 0x00_00C8
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_linksta_Eport_Base                                                                    0x0000C8
#define cAf6Reg_linksta_Eport                                                                         0x0000C8
#define cAf6Reg_linksta_Eport_WidthVal                                                                      32
#define cAf6Reg_linksta_Eport_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RO
BitField Desc: XFI_3 Link Status  1: Up  0: Down
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_3_Bit_Start                                                                  19
#define cAf6_linksta_Eport_XFI_3_Bit_End                                                                    19
#define cAf6_linksta_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linksta_Eport_XFI_3_Shift                                                                      19
#define cAf6_linksta_Eport_XFI_3_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_3_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RO
BitField Desc: XFI_2 Link Status  1: Up  0: Down
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_2_Bit_Start                                                                  18
#define cAf6_linksta_Eport_XFI_2_Bit_End                                                                    18
#define cAf6_linksta_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linksta_Eport_XFI_2_Shift                                                                      18
#define cAf6_linksta_Eport_XFI_2_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_2_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RO
BitField Desc: XFI_1 Link Status  1: Up  0: Down
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_1_Bit_Start                                                                  17
#define cAf6_linksta_Eport_XFI_1_Bit_End                                                                    17
#define cAf6_linksta_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linksta_Eport_XFI_1_Shift                                                                      17
#define cAf6_linksta_Eport_XFI_1_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_1_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RO
BitField Desc: XFI_0 Link Status  1: Up  0: Down
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_0_Bit_Start                                                                  16
#define cAf6_linksta_Eport_XFI_0_Bit_End                                                                    16
#define cAf6_linksta_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linksta_Eport_XFI_0_Shift                                                                      16
#define cAf6_linksta_Eport_XFI_0_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_0_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_0_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RO
BitField Desc: QSGMII_3 Link Status 1: Up  0: Down
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_3_Bit_Start                                                               12
#define cAf6_linksta_Eport_QSGMII_3_Bit_End                                                                 12
#define cAf6_linksta_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linksta_Eport_QSGMII_3_Shift                                                                   12
#define cAf6_linksta_Eport_QSGMII_3_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_3_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_3_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RO
BitField Desc: QSGMII_2 Link Status 1: Up  0: Down
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_2_Bit_Start                                                                8
#define cAf6_linksta_Eport_QSGMII_2_Bit_End                                                                  8
#define cAf6_linksta_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linksta_Eport_QSGMII_2_Shift                                                                    8
#define cAf6_linksta_Eport_QSGMII_2_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_2_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RO
BitField Desc: QSGMII_1 Link Status 1: Up  0: Down
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_1_Bit_Start                                                                4
#define cAf6_linksta_Eport_QSGMII_1_Bit_End                                                                  4
#define cAf6_linksta_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linksta_Eport_QSGMII_1_Shift                                                                    4
#define cAf6_linksta_Eport_QSGMII_1_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_1_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RO
BitField Desc: QSGMII_0 Link Status 1: Up  0: Down
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_0_Bit_Start                                                                0
#define cAf6_linksta_Eport_QSGMII_0_Bit_End                                                                  0
#define cAf6_linksta_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linksta_Eport_QSGMII_0_Shift                                                                    0
#define cAf6_linksta_Eport_QSGMII_0_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_0_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Force
Reg Addr   : 0x00_00CC
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_linkfrc_Eport_Base                                                                    0x0000CC
#define cAf6Reg_linkfrc_Eport                                                                         0x0000CC
#define cAf6Reg_linkfrc_Eport_WidthVal                                                                      32
#define cAf6Reg_linkfrc_Eport_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RO
BitField Desc: XFI_3 Link Force  1: Up  0: Down
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_3_Bit_Start                                                                  19
#define cAf6_linkfrc_Eport_XFI_3_Bit_End                                                                    19
#define cAf6_linkfrc_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linkfrc_Eport_XFI_3_Shift                                                                      19
#define cAf6_linkfrc_Eport_XFI_3_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_3_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RO
BitField Desc: XFI_2 Link Force  1: Up  0: Down
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_2_Bit_Start                                                                  18
#define cAf6_linkfrc_Eport_XFI_2_Bit_End                                                                    18
#define cAf6_linkfrc_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linkfrc_Eport_XFI_2_Shift                                                                      18
#define cAf6_linkfrc_Eport_XFI_2_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_2_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RO
BitField Desc: XFI_1 Link Force  1: Up  0: Down
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_1_Bit_Start                                                                  17
#define cAf6_linkfrc_Eport_XFI_1_Bit_End                                                                    17
#define cAf6_linkfrc_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linkfrc_Eport_XFI_1_Shift                                                                      17
#define cAf6_linkfrc_Eport_XFI_1_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_1_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RO
BitField Desc: XFI_0 Link Force  1: Up  0: Down
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_0_Bit_Start                                                                  16
#define cAf6_linkfrc_Eport_XFI_0_Bit_End                                                                    16
#define cAf6_linkfrc_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linkfrc_Eport_XFI_0_Shift                                                                      16
#define cAf6_linkfrc_Eport_XFI_0_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_0_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_0_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RO
BitField Desc: QSGMII_3 Link Force 1: Up  0: Down
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_3_Bit_Start                                                               12
#define cAf6_linkfrc_Eport_QSGMII_3_Bit_End                                                                 12
#define cAf6_linkfrc_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linkfrc_Eport_QSGMII_3_Shift                                                                   12
#define cAf6_linkfrc_Eport_QSGMII_3_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_3_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_3_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RO
BitField Desc: QSGMII_2 Link Force 1: Up  0: Down
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_2_Bit_Start                                                                8
#define cAf6_linkfrc_Eport_QSGMII_2_Bit_End                                                                  8
#define cAf6_linkfrc_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linkfrc_Eport_QSGMII_2_Shift                                                                    8
#define cAf6_linkfrc_Eport_QSGMII_2_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_2_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RO
BitField Desc: QSGMII_1 Link Force 1: Up  0: Down
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_1_Bit_Start                                                                4
#define cAf6_linkfrc_Eport_QSGMII_1_Bit_End                                                                  4
#define cAf6_linkfrc_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linkfrc_Eport_QSGMII_1_Shift                                                                    4
#define cAf6_linkfrc_Eport_QSGMII_1_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_1_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RO
BitField Desc: QSGMII_0 Link Force 1: Up  0: Down
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_0_Bit_Start                                                                0
#define cAf6_linkfrc_Eport_QSGMII_0_Bit_End                                                                  0
#define cAf6_linkfrc_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linkfrc_Eport_QSGMII_0_Shift                                                                    0
#define cAf6_linkfrc_Eport_QSGMII_0_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_0_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data63_32
Reg Addr   : 0x00_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword #2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_63_32_Base                                                                    0x000011
#define cAf6Reg_wr_hold_63_32                                                                         0x000011
#define cAf6Reg_wr_hold_63_32_WidthVal                                                                      32
#define cAf6Reg_wr_hold_63_32_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: WrHold63_32
BitField Type: RW
BitField Desc: The write dword #2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_63_32_WrHold63_32_Bit_Start                                                             0
#define cAf6_wr_hold_63_32_WrHold63_32_Bit_End                                                              31
#define cAf6_wr_hold_63_32_WrHold63_32_Mask                                                           cBit31_0
#define cAf6_wr_hold_63_32_WrHold63_32_Shift                                                                 0
#define cAf6_wr_hold_63_32_WrHold63_32_MaxVal                                                       0xffffffff
#define cAf6_wr_hold_63_32_WrHold63_32_MinVal                                                              0x0
#define cAf6_wr_hold_63_32_WrHold63_32_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data95_64
Reg Addr   : 0x00_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword #3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_95_64_Base                                                                    0x000012
#define cAf6Reg_wr_hold_95_64                                                                         0x000012
#define cAf6Reg_wr_hold_95_64_WidthVal                                                                      32
#define cAf6Reg_wr_hold_95_64_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: WrHold95_64
BitField Type: RW
BitField Desc: The write dword #3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_95_64_WrHold95_64_Bit_Start                                                             0
#define cAf6_wr_hold_95_64_WrHold95_64_Bit_End                                                              31
#define cAf6_wr_hold_95_64_WrHold95_64_Mask                                                           cBit31_0
#define cAf6_wr_hold_95_64_WrHold95_64_Shift                                                                 0
#define cAf6_wr_hold_95_64_WrHold95_64_MaxVal                                                       0xffffffff
#define cAf6_wr_hold_95_64_WrHold95_64_MinVal                                                              0x0
#define cAf6_wr_hold_95_64_WrHold95_64_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data127_96
Reg Addr   : 0x00_0013
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword #4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_127_96_Base                                                                   0x000013
#define cAf6Reg_wr_hold_127_96                                                                        0x000013
#define cAf6Reg_wr_hold_127_96_WidthVal                                                                     32
#define cAf6Reg_wr_hold_127_96_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: WrHold127_96
BitField Type: RW
BitField Desc: The write dword #4
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_127_96_WrHold127_96_Bit_Start                                                           0
#define cAf6_wr_hold_127_96_WrHold127_96_Bit_End                                                            31
#define cAf6_wr_hold_127_96_WrHold127_96_Mask                                                         cBit31_0
#define cAf6_wr_hold_127_96_WrHold127_96_Shift                                                               0
#define cAf6_wr_hold_127_96_WrHold127_96_MaxVal                                                     0xffffffff
#define cAf6_wr_hold_127_96_WrHold127_96_MinVal                                                            0x0
#define cAf6_wr_hold_127_96_WrHold127_96_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data63_32
Reg Addr   : 0x00_0021
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Read dword #2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_63_32_Base                                                                    0x000021
#define cAf6Reg_rd_hold_63_32                                                                         0x000021
#define cAf6Reg_rd_hold_63_32_WidthVal                                                                      32
#define cAf6Reg_rd_hold_63_32_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: rdHold63_32
BitField Type: RW
BitField Desc: The Read dword #2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_63_32_rdHold63_32_Bit_Start                                                             0
#define cAf6_rd_hold_63_32_rdHold63_32_Bit_End                                                              31
#define cAf6_rd_hold_63_32_rdHold63_32_Mask                                                           cBit31_0
#define cAf6_rd_hold_63_32_rdHold63_32_Shift                                                                 0
#define cAf6_rd_hold_63_32_rdHold63_32_MaxVal                                                       0xffffffff
#define cAf6_rd_hold_63_32_rdHold63_32_MinVal                                                              0x0
#define cAf6_rd_hold_63_32_rdHold63_32_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data95_64
Reg Addr   : 0x00_0022
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Read dword #3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_95_64_Base                                                                    0x000022
#define cAf6Reg_rd_hold_95_64                                                                         0x000022
#define cAf6Reg_rd_hold_95_64_WidthVal                                                                      32
#define cAf6Reg_rd_hold_95_64_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: rdHold95_64
BitField Type: RW
BitField Desc: The Read dword #3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_95_64_rdHold95_64_Bit_Start                                                             0
#define cAf6_rd_hold_95_64_rdHold95_64_Bit_End                                                              31
#define cAf6_rd_hold_95_64_rdHold95_64_Mask                                                           cBit31_0
#define cAf6_rd_hold_95_64_rdHold95_64_Shift                                                                 0
#define cAf6_rd_hold_95_64_rdHold95_64_MaxVal                                                       0xffffffff
#define cAf6_rd_hold_95_64_rdHold95_64_MinVal                                                              0x0
#define cAf6_rd_hold_95_64_rdHold95_64_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data127_96
Reg Addr   : 0x00_0023
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Read dword #4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_127_96_Base                                                                   0x000023
#define cAf6Reg_rd_hold_127_96                                                                        0x000023
#define cAf6Reg_rd_hold_127_96_WidthVal                                                                     32
#define cAf6Reg_rd_hold_127_96_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: rdHold127_96
BitField Type: RW
BitField Desc: The Read dword #4
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_127_96_rdHold127_96_Bit_Start                                                           0
#define cAf6_rd_hold_127_96_rdHold127_96_Bit_End                                                            31
#define cAf6_rd_hold_127_96_rdHold127_96_Mask                                                         cBit31_0
#define cAf6_rd_hold_127_96_rdHold127_96_Shift                                                               0
#define cAf6_rd_hold_127_96_rdHold127_96_MaxVal                                                     0xffffffff
#define cAf6_rd_hold_127_96_rdHold127_96_MinVal                                                            0x0
#define cAf6_rd_hold_127_96_rdHold127_96_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Mac Address Bit31_0 Control
Reg Addr   : 0x00_0100
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 31 to 0 of MAC address.

------------------------------------------------------------------------------*/
#define cAf6Reg_mac_add_31_0_ctrl_Base                                                                0x000100
#define cAf6Reg_mac_add_31_0_ctrl                                                                     0x000100
#define cAf6Reg_mac_add_31_0_ctrl_WidthVal                                                                  32
#define cAf6Reg_mac_add_31_0_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: MacAddress31_0
BitField Type: RW
BitField Desc: Bit 31 to 0 of MAC Address. Bit 31 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_Bit_Start                                                      0
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_Bit_End                                                       31
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_Mask                                                    cBit31_0
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_Shift                                                          0
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_MaxVal                                                0xffffffff
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_MinVal                                                       0x0
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Mac Address Bit47_32 Control
Reg Addr   : 0x00_0101
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 47to 32 of MAC address.

------------------------------------------------------------------------------*/
#define cAf6Reg_mac_add_47_32_ctrl_Base                                                               0x000101
#define cAf6Reg_mac_add_47_32_ctrl                                                                    0x000101
#define cAf6Reg_mac_add_47_32_ctrl_WidthVal                                                                 32
#define cAf6Reg_mac_add_47_32_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RX LIU Phase
BitField Type: RW
BitField Desc: RX LIU phase
BitField Bits: bit[29:28]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_Rx_Liu_Phase_Mask                                                  cBit29_28
#define cAf6_mac_add_47_32_ctrl_Rx_Liu_Phase_Shift                                                        28

/*--------------------------------------
BitField Name: MacAddress47_32
BitField Type: RW
BitField Desc: Bit 47 to 32 of MAC Address. Bit 47 is MSB bit
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_Bit_Start                                                    0
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_Bit_End                                                     15
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_Mask                                                  cBit15_0
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_Shift                                                        0
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_MaxVal                                                  0xffff
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_MinVal                                                     0x0
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_RstVal                                                     0x0

/*--------------------------------------
BitField Name: GeMode
BitField Type: RW
BitField Desc: GE mode 0:XGMII; 1:GMII
BitField Bits: [26]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_GeMode_Bit_Start                                                            26
#define cAf6_mac_add_47_32_ctrl_GeMode_Bit_End                                                              26
#define cAf6_mac_add_47_32_ctrl_GeMode_Mask                                                             cBit26
#define cAf6_mac_add_47_32_ctrl_GeMode_Shift                                                                26
#define cAf6_mac_add_47_32_ctrl_GeMode_MaxVal                                                              0x1
#define cAf6_mac_add_47_32_ctrl_GeMode_MinVal                                                              0x0
#define cAf6_mac_add_47_32_ctrl_GeMode_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PDHLoopOut
BitField Type: RW
BitField Desc: PDH parallel loopback
BitField Bits: [27]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_Bit_Start                                                        27
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_Bit_End                                                          27
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_Mask                                                         cBit27
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_Shift                                                            27
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_MaxVal                                                          0x1
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_MinVal                                                          0x0
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PDHGlbMode
BitField Type: RW
BitField Desc: PDH global mode for loopback, 0:DS3; 1:E3
BitField Bits: [28]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_Bit_Start                                                        28
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_Bit_End                                                          28
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_Mask                                                         cBit28
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_Shift                                                            28
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_MaxVal                                                          0x1
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_MinVal                                                          0x0
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit31_0 Control
Reg Addr   : 0x00_0102
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 31 to 0 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_31_0_ctrl_Base                                                                 0x000102
#define cAf6Reg_ip_add_31_0_ctrl                                                                      0x000102
#define cAf6Reg_ip_add_31_0_ctrl_WidthVal                                                                   32
#define cAf6Reg_ip_add_31_0_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: IpAddress31_0
BitField Type: RW
BitField Desc: Bit 31 to 0 of IP Address. Bit 31 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_Bit_Start                                                        0
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_Bit_End                                                         31
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_Mask                                                      cBit31_0
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_Shift                                                            0
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_MaxVal                                                  0xffffffff
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_MinVal                                                         0x0
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit63_32 Control
Reg Addr   : 0x00_0103
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 63to 32 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_63_32_ctrl_Base                                                                0x000103
#define cAf6Reg_ip_add_63_32_ctrl                                                                     0x000103
#define cAf6Reg_ip_add_63_32_ctrl_WidthVal                                                                  32
#define cAf6Reg_ip_add_63_32_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: IpAddress63_32
BitField Type: RW
BitField Desc: Bit 63 to 32 of IP Address. Bit 63 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_Bit_Start                                                      0
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_Bit_End                                                       31
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_Mask                                                    cBit31_0
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_Shift                                                          0
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_MaxVal                                                0xffffffff
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_MinVal                                                       0x0
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit95_64 Control
Reg Addr   : 0x00_0104
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 95 to 64 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_95_64_ctrl_Base                                                                0x000104
#define cAf6Reg_ip_add_95_64_ctrl                                                                     0x000104
#define cAf6Reg_ip_add_95_64_ctrl_WidthVal                                                                  32
#define cAf6Reg_ip_add_95_64_ctrl_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: IpAddress95_64
BitField Type: RW
BitField Desc: Bit 95 to 64 of IP Address. Bit 95 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_Bit_Start                                                      0
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_Bit_End                                                       31
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_Mask                                                    cBit31_0
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_Shift                                                          0
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_MaxVal                                                0xffffffff
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_MinVal                                                       0x0
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit127_96 Control
Reg Addr   : 0x00_0105
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 127 to 96 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_127_96_ctrl_Base                                                               0x000105
#define cAf6Reg_ip_add_127_96_ctrl                                                                    0x000105
#define cAf6Reg_ip_add_127_96_ctrl_WidthVal                                                                 32
#define cAf6Reg_ip_add_127_96_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: IpAddress127_96
BitField Type: RW
BitField Desc: Bit 127 to 96 of IP Address. Bit 127 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_Bit_Start                                                    0
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_Bit_End                                                     31
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_Mask                                                  cBit31_0
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_Shift                                                        0
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_MaxVal                                              0xffffffff
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_MinVal                                                     0x0
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU line loopout
Reg Addr   : 0x00_0107
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU line loopout.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_line_loopout_Base                                                                 0x000107
#define cAf6Reg_LIU_line_loopout                                                                      0x000107
#define cAf6Reg_LIU_line_loopout_WidthVal                                                                   64
#define cAf6Reg_LIU_line_loopout_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Liuloopout
BitField Type: RW
BitField Desc: LIU line loopout, each bit for each channel
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_line_loopout_Liuloopout_Bit_Start                                                           0
#define cAf6_LIU_line_loopout_Liuloopout_Bit_End                                                            63
#define cAf6_LIU_line_loopout_Liuloopout_Mask_01                                                      cBit31_0
#define cAf6_LIU_line_loopout_Liuloopout_Shift_01                                                            0
#define cAf6_LIU_line_loopout_Liuloopout_Mask_02                                                      cBit31_0
#define cAf6_LIU_line_loopout_Liuloopout_Shift_02                                                            0


/*------------------------------------------------------------------------------
Reg Name   : LIU line loopin
Reg Addr   : 0x00_0108
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU line loopin.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_line_loopin_Base                                                                  0x000108
#define cAf6Reg_LIU_line_loopin                                                                       0x000108
#define cAf6Reg_LIU_line_loopin_WidthVal                                                                    64
#define cAf6Reg_LIU_line_loopin_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Liuloopin
BitField Type: RW
BitField Desc: LIU line loopin, each bit for each channel
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_line_loopin_Liuloopin_Bit_Start                                                             0
#define cAf6_LIU_line_loopin_Liuloopin_Bit_End                                                              63
#define cAf6_LIU_line_loopin_Liuloopin_Mask_01                                                        cBit31_0
#define cAf6_LIU_line_loopin_Liuloopin_Shift_01                                                              0
#define cAf6_LIU_line_loopin_Liuloopin_Mask_02                                                        cBit31_0
#define cAf6_LIU_line_loopin_Liuloopin_Shift_02                                                              0


/*------------------------------------------------------------------------------
Reg Name   : LIU Rx Clock Invert
Reg Addr   : 0x00_0109
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU Rx Clock Invert.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Rx_Clock_Invert_Base                                                              0x000109
#define cAf6Reg_LIU_Rx_Clock_Invert                                                                   0x000109
#define cAf6Reg_LIU_Rx_Clock_Invert_WidthVal                                                                64
#define cAf6Reg_LIU_Rx_Clock_Invert_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Liurxinv
BitField Type: RW
BitField Desc: LIU Rx Clock Invert, each bit for each channel
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Bit_Start                                                          0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Bit_End                                                           63
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Mask_01                                                     cBit31_0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Shift_01                                                           0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Mask_02                                                     cBit31_0
#define cAf6_LIU_Rx_Clock_Invert_Liurxinv_Shift_02                                                           0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx Clock Invert
Reg Addr   : 0x00_010a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU Tx Clock Invert.

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_Clock_Invert_Base                                                              0x00010a
#define cAf6Reg_LIU_Tx_Clock_Invert                                                                   0x00010a
#define cAf6Reg_LIU_Tx_Clock_Invert_WidthVal                                                                64
#define cAf6Reg_LIU_Tx_Clock_Invert_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Liutxinv
BitField Type: RW
BitField Desc: LIU tx Clock Invert, each bit for each channel
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Bit_Start                                                          0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Bit_End                                                           63
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Mask_01                                                     cBit31_0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Shift_01                                                           0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Mask_02                                                     cBit31_0
#define cAf6_LIU_Tx_Clock_Invert_Liutxinv_Shift_02                                                           0

/*------------------------------------------------------------------------------
Reg Name   : LIU Tx EC1 Mode
Reg Addr   : 0x00_010b
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU Tx EC1 Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_EC1_Mode_Base                                                                  0x00010b
#define cAf6Reg_LIU_Tx_EC1_Mode                                                                       0x00010b
#define cAf6Reg_LIU_Tx_EC1_Mode_WidthVal                                                                    64
#define cAf6Reg_LIU_Tx_EC1_Mode_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Liuec1mode
BitField Type: RW
BitField Desc: LIU tx EC1 mode, each bit for each channel, 1:EC1 0:DE3
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_Tx_EC1_Mode_Liuec1mode_Bit_Start                                                            0
#define cAf6_LIU_Tx_EC1_Mode_Liuec1mode_Bit_End                                                             63
#define cAf6_LIU_Tx_EC1_Mode_Liuec1mode_Mask_01                                                       cBit31_0
#define cAf6_LIU_Tx_EC1_Mode_Liuec1mode_Shift_01                                                             0
#define cAf6_LIU_Tx_EC1_Mode_Liuec1mode_Mask_02                                                       cBit31_0
#define cAf6_LIU_Tx_EC1_Mode_Liuec1mode_Shift_02                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CDR REF OUT 8KHZ Control
Reg Addr   : 0x00_010c
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get the status of RX NCO engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_refout_8khz_ctrl_Base                                                             0x00010c
#define cAf6Reg_cdr_refout_8khz_ctrl                                                                  0x00010c
#define cAf6Reg_cdr_refout_8khz_ctrl_WidthVal                                                               32
#define cAf6Reg_cdr_refout_8khz_ctrl_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: Cdr1RefMaskLosDis
BitField Type: RW
BitField Desc: : 0: enable mask out8k in case of LOS 1: disable mask out8k in
case of LOS
BitField Bits: [31]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_Bit_Start                                               31
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_Bit_End                                                 31
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_Mask                                                cBit31
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_Shift                                                   31
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_MaxVal                                                 0x1
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_MinVal                                                 0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Cdr1RefID
BitField Type: RW
BitField Desc: CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode,
bit[10:5]) that used for 8Khz ref out
BitField Bits: [30:20]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_Bit_Start                                                       20
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_Bit_End                                                         30
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_Mask                                                     cBit30_20
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_Shift                                                           20
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_MaxVal                                                       0x7ff
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_MinVal                                                         0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Cdr1RefDs1
BitField Type: RW
BitField Desc: 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for
8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_Bit_Start                                                      18
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_Bit_End                                                        19
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_Mask                                                    cBit19_18
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_Shift                                                          18
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_MaxVal                                                        0x3
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_MinVal                                                        0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Cdr1RefSelect
BitField Type: RW
BitField Desc: : 0: System mode, used system clock to generate 8Khz ref out 1:
Loop timing mode,  used rx LIU to generate 8Khz ref out 2: CDR timing mode,
used clock from CDR engine to generate 8KHZ out. CDR clock can be
ACR/DCR/Loop/System/Ext1/...
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_Bit_Start                                                   16
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_Bit_End                                                     17
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_Mask                                                 cBit17_16
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_Shift                                                       16
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_MaxVal                                                     0x3
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_MinVal                                                     0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Cdr0RefMaskLosDis
BitField Type: RW
BitField Desc: : 0: enable mask out8k in case of LOS 1: disable mask out8k in
case of LOS
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_Bit_Start                                               15
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_Bit_End                                                 15
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_Mask                                                cBit15
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_Shift                                                   15
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_MaxVal                                                 0x1
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_MinVal                                                 0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Cdr0RefID
BitField Type: RW
BitField Desc: CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode,
bit[10:5]) that used for 8Khz ref out
BitField Bits: [14:4]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_Bit_Start                                                        4
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_Bit_End                                                         14
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_Mask                                                      cBit14_4
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_Shift                                                            4
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_MaxVal                                                       0x7ff
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_MinVal                                                         0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Cdr0RefDs1
BitField Type: RW
BitField Desc: : 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for
8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_Bit_Start                                                       2
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_Bit_End                                                         3
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_Mask                                                      cBit3_2
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_Shift                                                           2
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_MaxVal                                                        0x3
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_MinVal                                                        0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Cdr0RefSelect
BitField Type: RW
BitField Desc: 0: System mode, used system clock to generate 8Khz ref out 1:
Loop timing mode,  used rx LIU  to generate 8Khz ref out 2: CDR timing mode,
used clock from CDR engine to generate 8KHZ out. CDR clock can be
ACR/DCR/Loop/System/Ext1/...
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_Bit_Start                                                    0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_Bit_End                                                      1
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_Mask                                                   cBit1_0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_Shift                                                        0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_MaxVal                                                     0x3
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_MinVal                                                     0x0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx EXT Mode
Reg Addr   : 0x00_010E
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU Tx External Timing Mode.
This register is used for both DS3/E3/EC1.
This register should be refer to the timing mode of DS3/E3 timing mode in CDR engine (Ext#1) and EC1 mode in EC1 TxFramer (CLK19#2).

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_EXT_Mode_Base                                                                  0x00010E
#define cAf6Reg_LIU_Tx_EXT Mode                                                                       0x00010E
#define cAf6Reg_LIU_Tx_EXT_Mode_WidthVal                                                                    64
#define cAf6Reg_LIU_Tx_EXT_Mode_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: LiuTxExtMode
BitField Type: RW
BitField Desc: LIU tx Ext timing mode, each bit for each channel, 1:Ext 0:Normal
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_Tx_EXT_Mode_LiuTxExtMode_Bit_Start                                                          0
#define cAf6_LIU_Tx_EXT_Mode_LiuTxExtMode_Bit_End                                                           63
#define cAf6_LIU_Tx_EXT_Mode_LiuTxExtMode_Mask_01                                                     cBit31_0
#define cAf6_LIU_Tx_EXT_Mode_LiuTxExtMode_Shift_01                                                           0
#define cAf6_LIU_Tx_EXT_Mode_LiuTxExtMode_Mask_02                                                     cBit31_0
#define cAf6_LIU_Tx_EXT_Mode_LiuTxExtMode_Shift_02                                                           0


/*------------------------------------------------------------------------------
Reg Name   : LIU Tx Channel Enable
Reg Addr   : 0x00_010F
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures LIU Tx Channel Enable. This register is used for both DS3/E3/EC1

------------------------------------------------------------------------------*/
#define cAf6Reg_LIU_Tx_Ch_Enb_Base                                                                    0x00010F
#define cAf6Reg_LIU_Tx_Ch_Enb                                                                         0x00010F
#define cAf6Reg_LIU_Tx_Ch_Enb_WidthVal                                                                      64
#define cAf6Reg_LIU_Tx_Ch_Enb_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: LiuTxChEnb
BitField Type: RW
BitField Desc: LIU tx Channel timing mode, each bit for each channel, 1:Enable
0:Disable
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_LIU_Tx_Ch_Enb_LiuTxChEnb_Bit_Start                                                              0
#define cAf6_LIU_Tx_Ch_Enb_LiuTxChEnb_Bit_End                                                               63
#define cAf6_LIU_Tx_Ch_Enb_LiuTxChEnb_Mask_01                                                         cBit31_0
#define cAf6_LIU_Tx_Ch_Enb_LiuTxChEnb_Shift_01                                                               0
#define cAf6_LIU_Tx_Ch_Enb_LiuTxChEnb_Mask_02                                                         cBit31_0
#define cAf6_LIU_Tx_Ch_Enb_LiuTxChEnb_Shift_02                                                               0


/*------------------------------------------------------------------------------
Reg Name   : RAM Parity Force Control
Reg Addr   : 0x130
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Force_Control_Base                                                            0x130
#define cAf6Reg_RAM_Parity_Force_Control                                                                 0x130
#define cAf6Reg_RAM_Parity_Force_Control_WidthVal                                                           96
#define cAf6Reg_RAM_Parity_Force_Control_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: DDRTxEccNonCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC non-correctable error for Tx DDR
BitField Bits: [86]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_Bit_Start                                      86
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_Bit_End                                      86
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_Mask                                     cBit22
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_Shift                                        22
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_MaxVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_MinVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_RstVal                                      0x0

/*--------------------------------------
BitField Name: DDRTxEccCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC correctable error for Tx DDR
BitField Bits: [85]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_Bit_Start                                       85
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_Bit_End                                         85
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_Mask                                        cBit21
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_Shift                                           21
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_MaxVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: DDRRxEccNonCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC non-correctable error for Rx DDR
BitField Bits: [84]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_Bit_Start                                      84
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_Bit_End                                      84
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_Mask                                     cBit20
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_Shift                                        20
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_MaxVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_MinVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_RstVal                                      0x0

/*--------------------------------------
BitField Name: DDRRxEccCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC correctable error for Rx DDR
BitField Bits: [83]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_Bit_Start                                       83
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_Bit_End                                         83
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_Mask                                        cBit19
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_Shift                                           19
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_MaxVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: CLASoftwaveCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Softwave Control"
BitField Bits: [82]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_Bit_Start                                      82
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_Bit_End                                      82
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_Mask                                     cBit18
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_Shift                                        18
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_MaxVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_MinVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_RstVal                                      0x0

/*--------------------------------------
BitField Name: CDRPweLookupCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR Pseudowire Look Up Control"
BitField Bits: [81]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_Bit_Start                                      81
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_Bit_End                                      81
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_Mask                                    cBit17
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_Shift                                       17
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAPerGrpEnbCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify Per Group Enable Control"
BitField Bits: [80]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_Bit_Start                                      80
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_Bit_End                                      80
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_Mask                                    cBit16
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_Shift                                       16
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAMefOverMplsCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify Pseudowire MEF over MPLS
Control"
BitField Bits: [79]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_Bit_Start                                      79
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_Bit_End                                      79
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_Mask                                  cBit15
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_Shift                                      15
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAPerPweTypeCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify Per Pseudowire Type
Control"
BitField Bits: [78]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_Bit_Start                                      78
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_Bit_End                                      78
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_Mask                                   cBit14
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_Shift                                      14
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceLookupInfCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify HBCE Looking Up
Information Control"
BitField Bits: [77]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_Bit_Start                                      77
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_Bit_End                                      77
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_Mask                                  cBit13
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_Shift                                      13
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceHashTab1Ctl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 1
BitField Bits: [76]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_Bit_Start                                      76
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_Bit_End                                      76
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_Mask                                  cBit12
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_Shift                                      12
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceHashTab0Ctl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 0
BitField Bits: [75]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_Bit_Start                                      75
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_Bit_End                                      75
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_Mask                                  cBit11
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_Shift                                      11
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHspwGrpProCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit HSPW Group
Protection Control"
BitField Bits: [74]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_Bit_Start                                      74
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_Bit_End                                      74
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_Mask                                  cBit10
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_Shift                                      10
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxUpsaGrpEnbCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit UPSR Group
Enable Control"
BitField Bits: [73]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_Bit_Start                                      73
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_Bit_End                                      73
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_Mask                                   cBit9
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_Shift                                       9
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxUpsaHspwModCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit UPSR and HSPW
mode Control"
BitField Bits: [72]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_Bit_Start                                      72
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_Bit_End                                      72
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_Mask                                   cBit8
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_Shift                                       8
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHspwLabelCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit HSPW Label
Control"
BitField Bits: [71]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_Bit_Start                                      71
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_Bit_End                                      71
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_Mask                                   cBit7
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_Shift                                       7
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHdrRtpSsrcCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Header RTP SSRC
Value Control "
BitField Bits: [70]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_Bit_Start                                      70
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_Bit_End                                      70
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_Mask                                   cBit6
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_Shift                                       6
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEthHdrLenCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Ethernet Header
Length Control"
BitField Bits: [69]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_Bit_Start                                      69
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_Bit_End                                      69
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_Mask                                   cBit5
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_Shift                                       5
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEthHdrValCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Ethernet Header
Value Control"
BitField Bits: [68]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_Bit_Start                                      68
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_Bit_End                                      68
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_Mask                                   cBit4
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_Shift                                       4
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEnbCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Enable Control"
BitField Bits: [67]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_Bit_Start                                       67
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_Bit_End                                         67
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_Mask                                         cBit3
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_Shift                                            3
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_MaxVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: PDAddrCrc_ErrFrc
BitField Type: RW
BitField Desc: Force check DDR CRC error
BitField Bits: [66]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_Bit_Start                                            66
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_Bit_End                                              66
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_Mask                                              cBit2
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_Shift                                                 2
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_MaxVal                                              0x0
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_MinVal                                              0x0
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDATdmMode_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire PDA TDM Mode Control"
BitField Bits: [65]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_Bit_Start                                        65
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_Bit_End                                          65
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_Mask                                          cBit1
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_Shift                                             1
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_MaxVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PDAJitbuf_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire PDA Jitter Buffer
Control"
BitField Bits: [64]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_Bit_Start                                         64
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_Bit_End                                           64
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_Mask                                           cBit0
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_Shift                                              0
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_MaxVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_MinVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_RstVal                                           0x0

/*--------------------------------------
BitField Name: PDAReor_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire PDA Reorder Control"
BitField Bits: [63]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_Bit_Start                                           63
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_Bit_End                                             63
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_Mask                                            cBit31
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_Shift                                               31
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_MaxVal                                             0x0
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_MinVal                                             0x0
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_RstVal                                             0x0

/*--------------------------------------
BitField Name: PLAPaylSizeSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 1
BitField Bits: [62]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_Bit_Start                                      62
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_Bit_End                                      62
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_Mask                                    cBit30
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_Shift                                       30
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PLAPaylSizeSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 0
BitField Bits: [61]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_Bit_Start                                      61
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_Bit_End                                      61
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_Mask                                    cBit29
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_Shift                                       29
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
SliceId 1
BitField Bits: [60]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_Bit_Start                                      60
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_Bit_End                                      60
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_Mask                                  cBit28
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_Shift                                      28
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR VT Timing control" SliceId 1
BitField Bits: [59]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_Bit_Start                                      59
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_Bit_End                                      59
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_Mask                                  cBit27
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_Shift                                      27
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR STS Timing control" SliceId 1
BitField Bits: [58]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_Bit_Start                                      58
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_Bit_End                                      58
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_Mask                                  cBit26
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_Shift                                      26
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
SliceId 0
BitField Bits: [57]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_Bit_Start                                      57
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_Bit_End                                      57
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_Mask                                  cBit25
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_Shift                                      25
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR VT Timing control" SliceId 0
BitField Bits: [56]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_Bit_Start                                      56
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_Bit_End                                      56
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_Mask                                  cBit24
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_Shift                                      24
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR STS Timing control" SliceId 0
BitField Bits: [55]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_Bit_Start                                      55
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_Bit_End                                      55
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_Mask                                  cBit23
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_Shift                                      23
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: DeMapChlCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 1
BitField Bits: [54]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_Bit_Start                                      54
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_Bit_End                                      54
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_Mask                                  cBit22
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_Shift                                      22
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: DeMapChlCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 0
BitField Bits: [53]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_Bit_Start                                      53
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_Bit_End                                      53
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_Mask                                  cBit21
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_Shift                                      21
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 1
BitField Bits: [52]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_Bit_Start                                      52
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_Bit_End                                      52
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_Mask                                  cBit20
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_Shift                                      20
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 1
BitField Bits: [51]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_Bit_Start                                      51
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_Bit_End                                      51
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_Mask                                  cBit19
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_Shift                                      19
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 0
BitField Bits: [50]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_Bit_Start                                      50
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_Bit_End                                      50
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_Mask                                  cBit18
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_Shift                                      18
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 0
BitField Bits: [49]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_Bit_Start                                      49
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_Bit_End                                      49
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_Mask                                  cBit17
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_Shift                                      17
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH VT Async Map Control" SliceId 1
BitField Bits: [48]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_Bit_Start                                      48
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_Bit_End                                      48
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_Mask                                  cBit16
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_Shift                                      16
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Map Control" SliceId 1
BitField Bits: [47]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_Bit_Start                                      47
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_Bit_End                                      47
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_Mask                                  cBit15
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_Shift                                      15
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 1
BitField Bits: [46]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_Bit_Start                                      46
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_Bit_End                                      46
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_Mask                                  cBit14
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_Shift                                      14
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 Control" SliceId 1
BitField Bits: [45]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_Bit_Start                                      45
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_Bit_End                                      45
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_Mask                                  cBit13
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_Shift                                      13
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM12E12 Control" SliceId 1
BitField Bits: [44]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_Bit_Start                                      44
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_Bit_End                                      44
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_Mask                                  cBit12
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_Shift                                      12
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling
Control" SliceId 1
BitField Bits: [43]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_Bit_Start                                      43
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_Bit_End                                      43
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_Mask                                  cBit11
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_Shift                                      11
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 1
BitField Bits: [42]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_Bit_Start                                      42
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_Bit_End                                      42
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_Mask                                  cBit10
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_Shift                                      10
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_Bit_Start                                      41
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_Bit_End                                      41
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_Mask                                   cBit9
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_Shift                                       9
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM12E12 Control" SliceId 1
BitField Bits: [40]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_Bit_Start                                      40
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_Bit_End                                      40
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_Mask                                   cBit8
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_Shift                                       8
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId
1
BitField Bits: [39]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_Bit_Start                                      39
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_Bit_End                                      39
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_Mask                                   cBit7
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_Shift                                       7
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM23E23 Control" SliceId 1
BitField Bits: [38]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_Bit_Start                                      38
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_Bit_End                                      38
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_Mask                                   cBit6
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_Shift                                       6
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Demap Control" SliceId 1
BitField Bits: [37]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_Bit_Start                                      37
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_Bit_End                                      37
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_Mask                                   cBit5
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_Shift                                       5
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 1
BitField Bits: [36]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_Bit_Start                                      36
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_Bit_End                                      36
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_Mask                                   cBit4
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_Shift                                       4
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH VT Async Map Control" SliceId 0
BitField Bits: [35]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_Bit_Start                                      35
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_Bit_End                                      35
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_Mask                                   cBit3
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_Shift                                       3
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Map Control" SliceId 0
BitField Bits: [34]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_Bit_Start                                      34
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_Bit_End                                      34
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_Mask                                   cBit2
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_Shift                                       2
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 0
BitField Bits: [33]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_Bit_Start                                      33
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_Bit_End                                      33
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_Mask                                   cBit1
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_Shift                                       1
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 Control" SliceId 0
BitField Bits: [32]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_Bit_Start                                      32
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_Bit_End                                      32
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_Mask                                cBit32_0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_Shift                                       0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_MaxVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM12E12 Control" SliceId 0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_Bit_Start                                      31
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_Bit_End                                      31
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_Mask                                  cBit31
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_Shift                                      31
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling
Control" SliceId 0
BitField Bits: [30]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_Bit_Start                                      30
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_Bit_End                                      30
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_Mask                                  cBit30
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_Shift                                      30
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 0
BitField Bits: [29]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_Bit_Start                                      29
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_Bit_End                                      29
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_Mask                                  cBit29
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_Shift                                      29
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 0
BitField Bits: [28]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_Bit_Start                                      28
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_Bit_End                                      28
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_Mask                                  cBit28
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_Shift                                      28
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM12E12 Control" SliceId 0
BitField Bits: [27]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_Bit_Start                                      27
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_Bit_End                                      27
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_Mask                                  cBit27
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_Shift                                      27
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId
0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_Bit_Start                                      26
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_Bit_End                                      26
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_Mask                                  cBit26
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_Shift                                      26
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM23E23 Control" SliceId 0
BitField Bits: [25]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_Bit_Start                                      25
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_Bit_End                                      25
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_Mask                                  cBit25
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_Shift                                      25
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Demap Control" SliceId 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_Bit_Start                                      24
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_Bit_End                                      24
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_Mask                                  cBit24
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_Shift                                      24
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 0
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_Bit_Start                                      23
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_Bit_End                                      23
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_Mask                                  cBit23
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_Shift                                      23
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_RstVal                                     0x0

/*--------------------------------------
BitField Name: POHWrCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH BER Control VT/DSN", "POH BER
Control STS/TU3"
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_Bit_Start                                         22
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_Bit_End                                           22
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_Mask                                          cBit22
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_Shift                                             22
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_MaxVal                                           0x1
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_MinVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_RstVal                                           0x0

/*--------------------------------------
BitField Name: POHWrTrsh_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH BER Threshold 2"
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_Bit_Start                                         21
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_Bit_End                                           21
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_Mask                                          cBit21
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_Shift                                             21
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_MaxVal                                           0x1
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_MinVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_RstVal                                           0x0

/*--------------------------------------
BitField Name: POHTerCtrlLo_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH Termintate Insert Control
VT/TU3"
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_Bit_Start                                      20
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_Bit_End                                        20
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_Mask                                       cBit20
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_Shift                                          20
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_MaxVal                                        0x1
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_MinVal                                        0x0
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_RstVal                                        0x0

/*--------------------------------------
BitField Name: POHTerCtrlHi_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH Termintate Insert Control STS"
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_Bit_Start                                      19
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_Bit_End                                        19
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_Mask                                       cBit19
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_Shift                                          19
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_MaxVal                                        0x1
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_MinVal                                        0x0
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_RstVal                                        0x0

/*--------------------------------------
BitField Name: POHCpeVtCtr_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH CPE VT Control Register"
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_Bit_Start                                       18
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_Bit_End                                         18
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_Mask                                        cBit18
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_Shift                                           18
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: POHCpeStsCtr_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH CPE STS/TU3 Control Register"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_Bit_Start                                      17
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_Bit_End                                        17
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_Mask                                       cBit17
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_Shift                                          17
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_MaxVal                                        0x1
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_MinVal                                        0x0
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTohCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN TOH Monitoring Per Channel
Control" SliceId 1
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_Bit_Start                                      16
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_Bit_End                                       16
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_Mask                                      cBit16
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_Shift                                         16
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNSpiCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_Bit_Start                                      15
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_Bit_End                                       15
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_Mask                                      cBit15
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_Shift                                         15
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNSpiCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_Bit_Start                                      14
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_Bit_End                                       14
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_Mask                                      cBit14
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_Shift                                         14
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpiDemSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 1
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_Bit_Start                                      13
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_Bit_End                                       13
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_Mask                                      cBit13
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_Shift                                         13
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpiDemSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 0
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_Bit_Start                                      12
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_Bit_End                                       12
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_Mask                                      cBit12
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_Shift                                         12
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpiCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_Bit_Start                                      11
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_Bit_End                                       11
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_Mask                                      cBit11
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_Shift                                         11
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpiCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_Bit_Start                                      10
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_Bit_End                                       10
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_Mask                                      cBit10
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_Shift                                         10
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpgDemSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 1
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_Bit_Start                                       9
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_Bit_End                                        9
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_Mask                                       cBit9
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_Shift                                          9
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpgDemSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_Bit_Start                                       8
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_Bit_End                                        8
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_Mask                                       cBit8
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_Shift                                          8
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpgCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_Bit_Start                                       7
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_Bit_End                                        7
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_Mask                                       cBit7
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_Shift                                          7
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNVpgCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_Bit_Start                                       6
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_Bit_End                                        6
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_Mask                                       cBit6
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_Shift                                          6
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNSpgCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_Bit_Start                                       5
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_Bit_End                                        5
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_Mask                                       cBit5
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_Shift                                          5
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNSpgCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_Bit_Start                                       4
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_Bit_End                                        4
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_Mask                                       cBit4
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_Shift                                          4
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNTfmCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx Framer Per Channel Control"
SliceId 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_Bit_Start                                       3
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_Bit_End                                        3
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_Mask                                       cBit3
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_Shift                                          3
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNTfmCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx Framer Per Channel Control"
SliceId 0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_Bit_Start                                       2
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_Bit_End                                        2
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_Mask                                       cBit2
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_Shift                                          2
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: OCNTfmJ0Slc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId
1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_Bit_Start                                       1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_Bit_End                                         1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_Mask                                        cBit1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_Shift                                           1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_MaxVal                                        0x1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_MinVal                                        0x0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmJ0Slc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId
0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_Bit_Start                                       0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_Bit_End                                         0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_Mask                                        cBit0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_Shift                                           0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_MaxVal                                        0x1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_MinVal                                        0x0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Disable Control
Reg Addr   : 0x131
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Disable_Control_Base                                                          0x131
#define cAf6Reg_RAM_Parity_Disable_Control                                                               0x131
#define cAf6Reg_RAM_Parity_Disable_Control_WidthVal                                                         96
#define cAf6Reg_RAM_Parity_Disable_Control_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: DDRTxEccNonCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC non-correctable error for Tx DDR
BitField Bits: [86]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_Bit_Start                                      86
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_Bit_End                                      86
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_Mask                                   cBit22
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_Shift                                      22
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: DDRTxEccCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC correctable error for Tx DDR
BitField Bits: [85]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_Bit_Start                                      85
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_Bit_End                                       85
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_Mask                                      cBit21
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_Shift                                         21
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_MaxVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: DDRRxEccNonCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC non-correctable error for Rx DDR
BitField Bits: [84]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_Bit_Start                                      84
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_Bit_End                                      84
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_Mask                                   cBit20
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_Shift                                      20
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: DDRRxEccCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC correctable error for Rx DDR
BitField Bits: [83]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_Bit_Start                                      83
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_Bit_End                                       83
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_Mask                                      cBit19
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_Shift                                         19
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_MaxVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: CLASoftwaveCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Softwave Control"
BitField Bits: [82]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_Bit_Start                                      82
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_Bit_End                                       82
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_Mask                                      cBit18
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_Shift                                         18
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_MaxVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: CDRPweLookupCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR Pseudowire Look Up Control"
BitField Bits: [81]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_Bit_Start                                      81
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_Bit_End                                      81
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_Mask                                     cBit17
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_Shift                                        17
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_MaxVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: CLAPerGrpEnbCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify Per Group Enable
Control"
BitField Bits: [80]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_Bit_Start                                      80
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_Bit_End                                      80
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_Mask                                     cBit16
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_Shift                                        16
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_MaxVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: CLAMefOverMplsCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify Pseudowire MEF over MPLS
Control"
BitField Bits: [79]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_Bit_Start                                      79
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_Bit_End                                      79
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_Mask                                   cBit15
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_Shift                                      15
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAPerPweTypeCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify Per Pseudowire Type
Control"
BitField Bits: [78]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_Bit_Start                                      78
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_Bit_End                                      78
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_Mask                                    cBit14
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_Shift                                       14
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceLookupInfCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify HBCE Looking Up
Information Control"
BitField Bits: [77]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_Bit_Start                                      77
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_Bit_End                                      77
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_Mask                                  cBit13
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_Shift                                      13
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceHashTab1Ctl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 1
BitField Bits: [76]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_Bit_Start                                      76
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_Bit_End                                      76
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_Mask                                  cBit12
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_Shift                                      12
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceHashTab0Ctl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 0
BitField Bits: [75]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_Bit_Start                                      75
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_Bit_End                                      75
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_Mask                                  cBit11
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_Shift                                      11
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHspwGrpProCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit HSPW Group
Protection Control"
BitField Bits: [74]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_Bit_Start                                      74
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_Bit_End                                      74
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_Mask                                  cBit10
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_Shift                                      10
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxUpsaGrpEnbCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit UPSR Group
Enable Control"
BitField Bits: [73]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_Bit_Start                                      73
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_Bit_End                                      73
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_Mask                                   cBit9
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_Shift                                       9
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxUpsaHspwModCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit UPSR and HSPW
mode Control"
BitField Bits: [72]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_Bit_Start                                      72
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_Bit_End                                      72
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_Mask                                   cBit8
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_Shift                                       8
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHspwLabelCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit HSPW Label
Control"
BitField Bits: [71]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_Bit_Start                                      71
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_Bit_End                                      71
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_Mask                                    cBit7
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_Shift                                       7
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHdrRtpSsrcCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Header RTP
SSRC Value Control "
BitField Bits: [70]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_Bit_Start                                      70
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_Bit_End                                      70
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_Mask                                   cBit6
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_Shift                                       6
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEthHdrLenCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Ethernet
Header Length Control"
BitField Bits: [69]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_Bit_Start                                      69
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_Bit_End                                      69
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_Mask                                    cBit5
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_Shift                                       5
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEthHdrValCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Ethernet
Header Value Control"
BitField Bits: [68]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_Bit_Start                                      68
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_Bit_End                                      68
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_Mask                                    cBit4
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_Shift                                       4
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEnbCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Enable
Control"
BitField Bits: [67]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_Bit_Start                                        67
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_Bit_End                                          67
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_Mask                                          cBit3
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_Shift                                             3
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_MaxVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: PDAddrCrc_ErrDis
BitField Type: RW
BitField Desc: Disable check DDR CRC error
BitField Bits: [66]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_Bit_Start                                          66
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_Bit_End                                            66
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_Mask                                            cBit2
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_Shift                                               2
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_MaxVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: PDATdmMode_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire PDA TDM Mode Control"
BitField Bits: [65]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_Bit_Start                                         65
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_Bit_End                                           65
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_Mask                                           cBit1
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_Shift                                              1
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_MaxVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PDAJitbuf_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire PDA Jitter Buffer
Control"
BitField Bits: [64]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_Bit_Start                                          64
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_Bit_End                                            64
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_Mask                                            cBit0
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_Shift                                               0
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_MaxVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: PDAReor_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire PDA Reorder Control"
BitField Bits: [63]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_Bit_Start                                            63
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_Bit_End                                              63
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_Mask                                             cBit31
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_Shift                                                31
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_MaxVal                                              0x0
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_MinVal                                              0x0
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_RstVal                                              0x0

/*--------------------------------------
BitField Name: PLAPaylSizeSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 1
BitField Bits: [62]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_Bit_Start                                      62
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_Bit_End                                      62
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_Mask                                     cBit30
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_Shift                                        30
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_MaxVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: PLAPaylSizeSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 0
BitField Bits: [61]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_Bit_Start                                      61
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_Bit_End                                      61
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_Mask                                     cBit29
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_Shift                                        29
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_MaxVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
SliceId 1
BitField Bits: [60]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_Bit_Start                                      60
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_Bit_End                                      60
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_Mask                                  cBit28
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_Shift                                      28
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR VT Timing control" SliceId 1
BitField Bits: [59]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_Bit_Start                                      59
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_Bit_End                                      59
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_Mask                                  cBit27
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_Shift                                      27
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR STS Timing control" SliceId 1
BitField Bits: [58]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_Bit_Start                                      58
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_Bit_End                                      58
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_Mask                                  cBit26
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_Shift                                      26
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
SliceId 0
BitField Bits: [57]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_Bit_Start                                      57
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_Bit_End                                      57
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_Mask                                  cBit25
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_Shift                                      25
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR VT Timing control" SliceId 0
BitField Bits: [56]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_Bit_Start                                      56
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_Bit_End                                      56
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_Mask                                  cBit24
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_Shift                                      24
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR STS Timing control" SliceId 0
BitField Bits: [55]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_Bit_Start                                      55
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_Bit_End                                      55
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_Mask                                  cBit23
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_Shift                                      23
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 1
BitField Bits: [54]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_Bit_Start                                      54
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_Bit_End                                      54
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_Mask                                    cBit22
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_Shift                                       22
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 0
BitField Bits: [53]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_Bit_Start                                      53
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_Bit_End                                      53
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_Mask                                    cBit21
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_Shift                                       21
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPLineCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 1
BitField Bits: [52]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_Bit_Start                                      52
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_Bit_End                                      52
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_Mask                                     cBit20
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_Shift                                        20
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_MaxVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPChlCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 1
BitField Bits: [51]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_Bit_Start                                      51
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_Bit_End                                       51
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_Mask                                      cBit19
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_Shift                                         19
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_MaxVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: MAPLineCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 0
BitField Bits: [50]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_Bit_Start                                      50
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_Bit_End                                      50
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_Mask                                     cBit18
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_Shift                                        18
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_MaxVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPChlCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 0
BitField Bits: [49]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_Bit_Start                                      49
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_Bit_End                                       49
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_Mask                                      cBit17
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_Shift                                         17
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_MaxVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH VT Async Map Control" SliceId
1
BitField Bits: [48]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_Bit_Start                                      48
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_Bit_End                                      48
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_Mask                                  cBit16
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_Shift                                      16
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Map Control" SliceId 1
BitField Bits: [47]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_Bit_Start                                      47
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_Bit_End                                      47
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_Mask                                  cBit15
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_Shift                                      15
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23TraceSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 1
BitField Bits: [46]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_Bit_Start                                      46
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_Bit_End                                      46
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_Mask                                  cBit14
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_Shift                                      14
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 Control" SliceId 1
BitField Bits: [45]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_Bit_Start                                      45
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_Bit_End                                      45
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_Mask                                  cBit13
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_Shift                                      13
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM12E12CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM12E12 Control" SliceId 1
BitField Bits: [44]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_Bit_Start                                      44
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_Bit_End                                      44
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_Mask                                  cBit12
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_Shift                                      12
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 1
BitField Bits: [43]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_Bit_Start                                      43
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_Bit_End                                      43
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_Mask                                  cBit11
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_Shift                                      11
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 1
BitField Bits: [42]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_Bit_Start                                      42
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_Bit_End                                      42
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_Mask                                  cBit10
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_Shift                                      10
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_Bit_Start                                      41
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_Bit_End                                      41
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_Mask                                   cBit9
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_Shift                                       9
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM12E12CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM12E12 Control" SliceId 1
BitField Bits: [40]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_Bit_Start                                      40
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_Bit_End                                      40
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_Mask                                   cBit8
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_Shift                                       8
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 1
BitField Bits: [39]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_Bit_Start                                      39
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_Bit_End                                      39
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_Mask                                   cBit7
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_Shift                                       7
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM23E23CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM23E23 Control" SliceId 1
BitField Bits: [38]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_Bit_Start                                      38
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_Bit_End                                      38
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_Mask                                   cBit6
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_Shift                                       6
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Demap Control" SliceId
1
BitField Bits: [37]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_Bit_Start                                      37
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_Bit_End                                      37
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_Mask                                   cBit5
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_Shift                                       5
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHMuxCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 1
BitField Bits: [36]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_Bit_Start                                      36
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_Bit_End                                       36
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_Mask                                       cBit4
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_Shift                                          4
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_MaxVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH VT Async Map Control" SliceId
0
BitField Bits: [35]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_Bit_Start                                      35
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_Bit_End                                      35
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_Mask                                   cBit3
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_Shift                                       3
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Map Control" SliceId 0
BitField Bits: [34]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_Bit_Start                                      34
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_Bit_End                                      34
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_Mask                                   cBit2
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_Shift                                       2
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23TraceSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 0
BitField Bits: [33]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_Bit_Start                                      33
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_Bit_End                                      33
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_Mask                                   cBit1
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_Shift                                       1
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 Control" SliceId 0
BitField Bits: [32]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_Bit_Start                                      32
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_Bit_End                                      32
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_Mask                                cBit32_0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_Shift                                       0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_MaxVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM12E12CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM12E12 Control" SliceId 0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_Bit_Start                                      31
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_Bit_End                                      31
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_Mask                                  cBit31
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_Shift                                      31
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 0
BitField Bits: [30]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_Bit_Start                                      30
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_Bit_End                                      30
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_Mask                                  cBit30
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_Shift                                      30
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 0
BitField Bits: [29]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_Bit_Start                                      29
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_Bit_End                                      29
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_Mask                                  cBit29
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_Shift                                      29
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 0
BitField Bits: [28]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_Bit_Start                                      28
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_Bit_End                                      28
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_Mask                                  cBit28
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_Shift                                      28
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM12E12CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM12E12 Control" SliceId 0
BitField Bits: [27]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_Bit_Start                                      27
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_Bit_End                                      27
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_Mask                                  cBit27
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_Shift                                      27
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_Bit_Start                                      26
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_Bit_End                                      26
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_Mask                                  cBit26
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_Shift                                      26
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM23E23CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM23E23 Control" SliceId 0
BitField Bits: [25]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_Bit_Start                                      25
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_Bit_End                                      25
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_Mask                                  cBit25
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_Shift                                      25
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Demap Control" SliceId
0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_Bit_Start                                      24
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_Bit_End                                      24
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_Mask                                  cBit24
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_Shift                                      24
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_MaxVal                                     0x1
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_MinVal                                     0x0
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 0
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_Bit_Start                                         23
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_Bit_End                                           23
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_Mask                                          cBit23
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_Shift                                             23
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: POHWrCtrl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH BER Control VT/DSN", "POH BER
Control STS/TU3"
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_Bit_Start                                          22
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_Bit_End                                            22
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_Mask                                           cBit22
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_Shift                                              22
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_MaxVal                                            0x1
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: POHWrTrsh_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH BER Threshold 2"
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_Bit_Start                                          21
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_Bit_End                                            21
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_Mask                                           cBit21
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_Shift                                              21
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_MaxVal                                            0x1
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: POHTerCtrlLo_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH Termintate Insert Control
VT/TU3"
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_Bit_Start                                       20
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_Bit_End                                         20
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_Mask                                        cBit20
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_Shift                                           20
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_MaxVal                                         0x1
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_MinVal                                         0x0
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_RstVal                                         0x0

/*--------------------------------------
BitField Name: POHTerCtrlHi_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH Termintate Insert Control
STS"
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_Bit_Start                                       19
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_Bit_End                                         19
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_Mask                                        cBit19
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_Shift                                           19
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_MaxVal                                         0x1
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_MinVal                                         0x0
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_RstVal                                         0x0

/*--------------------------------------
BitField Name: POHCpeVtCtr_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH CPE VT Control Register"
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_Bit_Start                                        18
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_Bit_End                                          18
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_Mask                                         cBit18
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_Shift                                            18
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: POHCpeStsCtr_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH CPE STS/TU3 Control Register"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_Bit_Start                                       17
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_Bit_End                                         17
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_Mask                                        cBit17
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_Shift                                           17
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_MaxVal                                         0x1
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_MinVal                                         0x0
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_RstVal                                         0x0

/*--------------------------------------
BitField Name: OCNTohCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN TOH Monitoring Per Channel
Control" SliceId 1
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_Bit_Start                                      16
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_Bit_End                                        16
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_Mask                                       cBit16
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_Shift                                          16
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpiCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_Bit_Start                                      15
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_Bit_End                                        15
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_Mask                                       cBit15
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_Shift                                          15
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpiCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_Bit_Start                                      14
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_Bit_End                                        14
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_Mask                                       cBit14
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_Shift                                          14
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiDemSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 1
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_Bit_Start                                      13
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_Bit_End                                        13
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_Mask                                       cBit13
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_Shift                                          13
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiDemSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 0
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_Bit_Start                                      12
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_Bit_End                                        12
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_Mask                                       cBit12
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_Shift                                          12
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_Bit_Start                                      11
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_Bit_End                                        11
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_Mask                                       cBit11
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_Shift                                          11
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_Bit_Start                                      10
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_Bit_End                                        10
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_Mask                                       cBit10
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_Shift                                          10
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgDemSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 1
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_Bit_Start                                       9
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_Bit_End                                         9
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_Mask                                        cBit9
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_Shift                                           9
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgDemSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_Bit_Start                                       8
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_Bit_End                                         8
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_Mask                                        cBit8
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_Shift                                           8
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_Bit_Start                                       7
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_Bit_End                                         7
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_Mask                                        cBit7
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_Shift                                           7
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_Bit_Start                                       6
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_Bit_End                                         6
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_Mask                                        cBit6
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_Shift                                           6
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpgCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_Bit_Start                                       5
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_Bit_End                                         5
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_Mask                                        cBit5
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_Shift                                           5
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpgCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_Bit_Start                                       4
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_Bit_End                                         4
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_Mask                                        cBit4
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_Shift                                           4
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_Bit_Start                                       3
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_Bit_End                                         3
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_Mask                                        cBit3
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_Shift                                           3
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_Bit_Start                                       2
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_Bit_End                                         2
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_Mask                                        cBit2
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_Shift                                           2
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmJ0Slc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_Bit_Start                                        1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_Bit_End                                          1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_Mask                                         cBit1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_Shift                                            1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_MaxVal                                         0x1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_MinVal                                         0x0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_RstVal                                         0x0

/*--------------------------------------
BitField Name: OCNTfmJ0Slc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_Bit_Start                                        0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_Bit_End                                          0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_Mask                                         cBit0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_Shift                                            0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_MaxVal                                         0x1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_MinVal                                         0x0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Error Sticky
Reg Addr   : 0x112
Reg Formula: 0x112 + Wrd
    Where  : 
           + $Wrd(0-2): RAM parity Error Word 0:wrd[31:0]; 1:wrd[63:32];2:wrd[95:64]
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Error_Sticky_Base                                                             0x112
#define cAf6Reg_RAM_Parity_Error_Sticky(Wrd)                                                     (0x112+(Wrd))
#define cAf6Reg_RAM_Parity_Error_Sticky_WidthVal                                                            96
#define cAf6Reg_RAM_Parity_Error_Sticky_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: DDRTxEccNonCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC non-correctable error for Tx DDR
BitField Bits: [86]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_Bit_Start                                      86
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_Bit_End                                       86
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_Mask                                      cBit22
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_Shift                                         22
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_MaxVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: DDRTxEccCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC correctable error for Tx DDR
BitField Bits: [85]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_Bit_Start                                        85
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_Bit_End                                          85
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_Mask                                         cBit21
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_Shift                                            21
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_MaxVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_RstVal                                          0x0

/*--------------------------------------
BitField Name: DDRRxEccNonCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC non-correctable error for Rx DDR
BitField Bits: [84]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_Bit_Start                                      84
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_Bit_End                                       84
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_Mask                                      cBit20
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_Shift                                         20
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_MaxVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: DDRRxEccCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC correctable error for Rx DDR
BitField Bits: [83]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_Bit_Start                                        83
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_Bit_End                                          83
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_Mask                                         cBit19
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_Shift                                            19
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_MaxVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_RstVal                                          0x0

/*--------------------------------------
BitField Name: CLASoftwaveCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Softwave Control"
BitField Bits: [82]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_Bit_Start                                      82
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_Bit_End                                       82
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_Mask                                      cBit18
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_Shift                                         18
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_MaxVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: CDRPweLookupCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR Pseudowire Look Up
Control"
BitField Bits: [81]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_Bit_Start                                      81
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_Bit_End                                      81
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_Mask                                     cBit17
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_Shift                                        17
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_MaxVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: CLAPerGrpEnbCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify Per Group Enable
Control"
BitField Bits: [80]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_Bit_Start                                      80
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_Bit_End                                      80
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_Mask                                     cBit16
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_Shift                                        16
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_MaxVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: CLAMefOverMplsCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify Pseudowire MEF over
MPLS Control"
BitField Bits: [79]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_Bit_Start                                      79
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_Bit_End                                      79
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_Mask                                   cBit15
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_Shift                                      15
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAPerPweTypeCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify Per Pseudowire Type
Control"
BitField Bits: [78]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_Bit_Start                                      78
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_Bit_End                                      78
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_Mask                                    cBit14
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_Shift                                       14
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceLookupInfCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify HBCE Looking Up
Information Control"
BitField Bits: [77]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_Bit_Start                                      77
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_Bit_End                                      77
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_Mask                                  cBit13
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_Shift                                      13
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceHashTab1Ctl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify HBCE Hashing Table
Control" PageID 1
BitField Bits: [76]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_Bit_Start                                      76
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_Bit_End                                      76
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_Mask                                  cBit12
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_Shift                                      12
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CLAHbceHashTab0Ctl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify HBCE Hashing Table
Control" PageID 0
BitField Bits: [75]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_Bit_Start                                      75
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_Bit_End                                      75
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_Mask                                  cBit11
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_Shift                                      11
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHspwGrpProCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit HSPW
Group Protection Control"
BitField Bits: [74]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_Bit_Start                                      74
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_Bit_End                                      74
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_Mask                                  cBit10
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_Shift                                      10
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxUpsaGrpEnbCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit UPSR
Group Enable Control"
BitField Bits: [73]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_Bit_Start                                      73
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_Bit_End                                      73
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_Mask                                   cBit9
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_Shift                                       9
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxUpsaHspwModCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit UPSR and
HSPW mode Control"
BitField Bits: [72]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_Bit_Start                                      72
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_Bit_End                                      72
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_Mask                                   cBit8
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_Shift                                       8
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHspwLabelCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit HSPW
Label Control"
BitField Bits: [71]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_Bit_Start                                      71
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_Bit_End                                      71
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_Mask                                    cBit7
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_Shift                                       7
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxHdrRtpSsrcCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Header
RTP SSRC Value Control "
BitField Bits: [70]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_Bit_Start                                      70
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_Bit_End                                      70
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_Mask                                   cBit6
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_Shift                                       6
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEthHdrLenCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Ethernet
Header Length Control"
BitField Bits: [69]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_Bit_Start                                      69
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_Bit_End                                      69
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_Mask                                    cBit5
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_Shift                                       5
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEthHdrValCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Ethernet
Header Value Control"
BitField Bits: [68]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_Bit_Start                                      68
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_Bit_End                                      68
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_Mask                                    cBit4
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_Shift                                       4
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PWETxEnbCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Enable
Control"
BitField Bits: [67]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_Bit_Start                                        67
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_Bit_End                                          67
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_Mask                                          cBit3
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_Shift                                             3
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_MaxVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_RstVal                                          0x0

/*--------------------------------------
BitField Name: PDAddrCrc_ErrStk
BitField Type: W1C
BitField Desc: Sticky check DDR CRC error
BitField Bits: [66]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_Bit_Start                                             66
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_Bit_End                                               66
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_Mask                                               cBit2
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_Shift                                                  2
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_MaxVal                                               0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_MinVal                                               0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_RstVal                                               0x0

/*--------------------------------------
BitField Name: PDATdmMode_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire PDA TDM Mode
Control"
BitField Bits: [65]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_Bit_Start                                         65
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_Bit_End                                           65
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_Mask                                           cBit1
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_Shift                                              1
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_MaxVal                                           0x0
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_MinVal                                           0x0
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_RstVal                                           0x0

/*--------------------------------------
BitField Name: PDAJitbuf_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire PDA Jitter Buffer
Control"
BitField Bits: [64]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_Bit_Start                                          64
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_Bit_End                                            64
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_Mask                                            cBit0
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_Shift                                               0
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_MaxVal                                            0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_MinVal                                            0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_RstVal                                            0x0

/*--------------------------------------
BitField Name: PDAReor_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire PDA Reorder
Control"
BitField Bits: [63]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_Bit_Start                                            63
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_Bit_End                                              63
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_Mask                                             cBit31
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_Shift                                                31
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_MaxVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PLAPaylSizeSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Thalassa PDHPW Payload
Assemble Payload Size Control"SliceId 1
BitField Bits: [62]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_Bit_Start                                      62
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_Bit_End                                      62
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_Mask                                     cBit30
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_Shift                                        30
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_MaxVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: PLAPaylSizeSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Thalassa PDHPW Payload
Assemble Payload Size Control"SliceId 0
BitField Bits: [61]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_Bit_Start                                      61
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_Bit_End                                      61
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_Mask                                     cBit29
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_Shift                                        29
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_MaxVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR ACR Engine Timing
control" SliceId 1
BitField Bits: [60]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Bit_Start                                      60
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Bit_End                                      60
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Mask                                  cBit28
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_Shift                                      28
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR VT Timing control"
SliceId 1
BitField Bits: [59]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_Bit_Start                                      59
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_Bit_End                                      59
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_Mask                                  cBit27
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_Shift                                      27
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR STS Timing control"
SliceId 1
BitField Bits: [58]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_Bit_Start                                      58
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_Bit_End                                      58
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_Mask                                  cBit26
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_Shift                                      26
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR ACR Engine Timing
control" SliceId 0
BitField Bits: [57]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Bit_Start                                      57
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Bit_End                                      57
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Mask                                  cBit25
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Shift                                      25
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR VT Timing control"
SliceId 0
BitField Bits: [56]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_Bit_Start                                      56
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_Bit_End                                      56
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_Mask                                  cBit24
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_Shift                                      24
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR STS Timing control"
SliceId 0
BitField Bits: [55]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_Bit_Start                                      55
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_Bit_End                                      55
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_Mask                                  cBit23
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_Shift                                      23
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 1
BitField Bits: [54]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_Bit_Start                                      54
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_Bit_End                                      54
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_Mask                                    cBit22
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_Shift                                       22
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 0
BitField Bits: [53]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_Bit_Start                                      53
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_Bit_End                                      53
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_Mask                                    cBit21
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_Shift                                       21
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPLineCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "MAP Thalassa Map Line
Control" SliceId 1
BitField Bits: [52]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_Bit_Start                                      52
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_Bit_End                                      52
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_Mask                                     cBit20
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_Shift                                        20
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_MaxVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc1_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPChlCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "MAP Thalassa Map Channel
Control" SliceId 1
BitField Bits: [51]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_Bit_Start                                      51
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_Bit_End                                       51
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_Mask                                      cBit19
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_Shift                                         19
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_MaxVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc1_ParErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: MAPLineCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "MAP Thalassa Map Line
Control" SliceId 0
BitField Bits: [50]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_Bit_Start                                      50
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_Bit_End                                      50
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_Mask                                     cBit18
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_Shift                                        18
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_MaxVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPChlCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "MAP Thalassa Map Channel
Control" SliceId 0
BitField Bits: [49]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_Bit_Start                                      49
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_Bit_End                                       49
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_Mask                                      cBit17
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_Shift                                         17
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_MaxVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH VT Async Map Control"
SliceId 1
BitField Bits: [48]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_Bit_Start                                      48
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_Bit_End                                      48
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_Mask                                  cBit16
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_Shift                                      16
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH STS/VT Map Control"
SliceId 1
BitField Bits: [47]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_Bit_Start                                      47
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_Bit_End                                      47
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_Mask                                  cBit15
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_Shift                                      15
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23TraceSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM23E23 E3g832 Trace
Byte" SliceId 1
BitField Bits: [46]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_Bit_Start                                      46
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_Bit_End                                      46
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_Mask                                  cBit14
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_Shift                                      14
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23TraceSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23CtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM23E23 Control"
SliceId 1
BitField Bits: [45]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_Bit_Start                                      45
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_Bit_End                                      45
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_Mask                                  cBit13
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_Shift                                      13
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23CtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM12E12CtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM12E12 Control"
SliceId 1
BitField Bits: [44]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_Bit_Start                                      44
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_Bit_End                                      44
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_Mask                                  cBit12
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_Shift                                      12
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12CtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 1
BitField Bits: [43]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_Bit_Start                                      43
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_Bit_End                                      43
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_Mask                                  cBit11
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_Shift                                      11
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer
Control" SliceId 1
BitField Bits: [42]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_Bit_Start                                      42
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_Bit_End                                      42
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_Mask                                  cBit10
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_Shift                                      10
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer
Control" SliceId 1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_Bit_Start                                      41
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_Bit_End                                      41
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_Mask                                   cBit9
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_Shift                                       9
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM12E12CtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxM12E12 Control"
SliceId 1
BitField Bits: [40]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_Bit_Start                                      40
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_Bit_End                                      40
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_Mask                                   cBit8
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_Shift                                       8
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12CtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 1
BitField Bits: [39]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_Bit_Start                                      39
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_Bit_End                                      39
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_Mask                                   cBit7
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_Shift                                       7
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM23E23CtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxM23E23 Control"
SliceId 1
BitField Bits: [38]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_Bit_Start                                      38
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_Bit_End                                      38
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_Mask                                   cBit6
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_Shift                                       6
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23CtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH STS/VT Demap Control"
SliceId 1
BitField Bits: [37]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_Bit_Start                                      37
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_Bit_End                                      37
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_Mask                                   cBit5
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_Shift                                       5
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrlSlc1_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHMuxCtrlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 1
BitField Bits: [36]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_Bit_Start                                      36
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_Bit_End                                       36
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_Mask                                       cBit4
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_Shift                                          4
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_MaxVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrlSlc1_ParErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH VT Async Map Control"
SliceId 0
BitField Bits: [35]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_Bit_Start                                      35
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_Bit_End                                      35
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_Mask                                   cBit3
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_Shift                                       3
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH STS/VT Map Control"
SliceId 0
BitField Bits: [34]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_Bit_Start                                      34
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_Bit_End                                      34
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_Mask                                   cBit2
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_Shift                                       2
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM23E23 E3g832 Trace
Byte" SliceId 0
BitField Bits: [33]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Bit_Start                                      33
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Bit_End                                      33
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Mask                                     cBit1
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Shift                                        1
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM23E23 Control"
SliceId 0
BitField Bits: [32]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_Bit_Start                                      32
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_Bit_End                                      32
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_Mask                                cBit32_0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_Shift                                       0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_MaxVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM12E12 Control"
SliceId 0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_Bit_Start                                      31
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_Bit_End                                      31
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_Mask                                  cBit31
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_Shift                                      31
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 0
BitField Bits: [30]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_Bit_Start                                      30
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_Bit_End                                      30
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_Mask                                  cBit30
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_Shift                                      30
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer
Control" SliceId 0
BitField Bits: [29]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_Bit_Start                                      29
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_Bit_End                                      29
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_Mask                                  cBit29
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_Shift                                      29
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer
Control" SliceId 0
BitField Bits: [28]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_Bit_Start                                      28
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_Bit_End                                      28
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_Mask                                  cBit28
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_Shift                                      28
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxM12E12 Control"
SliceId 0
BitField Bits: [27]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_Bit_Start                                      27
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_Bit_End                                      27
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_Mask                                  cBit27
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_Shift                                      27
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_Bit_Start                                      26
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_Bit_End                                      26
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_Mask                                  cBit26
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_Shift                                      26
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxM23E23 Control"
SliceId 0
BitField Bits: [25]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_Bit_Start                                      25
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_Bit_End                                      25
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_Mask                                  cBit25
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_Shift                                      25
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH STS/VT Demap Control"
SliceId 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_Bit_Start                                      24
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_Bit_End                                      24
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_Mask                                  cBit24
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_Shift                                      24
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_MinVal                                     0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDHMuxCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 0
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_Bit_Start                                      23
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_Bit_End                                       23
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_Mask                                      cBit23
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_Shift                                         23
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_MaxVal                                       0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_MinVal                                       0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_RstVal                                       0x0

/*--------------------------------------
BitField Name: POHWrCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH BER Control VT/DSN",
"POH BER Control STS/TU3"
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_Bit_Start                                          22
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_Bit_End                                            22
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_Mask                                           cBit22
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_Shift                                              22
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_MaxVal                                            0x1
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_MinVal                                            0x0
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_RstVal                                            0x0

/*--------------------------------------
BitField Name: POHWrTrsh_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH BER Threshold 2"
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_Bit_Start                                          21
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_Bit_End                                            21
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_Mask                                           cBit21
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_Shift                                              21
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_MaxVal                                            0x1
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_MinVal                                            0x0
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_RstVal                                            0x0

/*--------------------------------------
BitField Name: POHTerCtrlLo_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH Termintate Insert
Control VT/TU3"
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_Bit_Start                                       20
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_Bit_End                                         20
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_Mask                                        cBit20
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_Shift                                           20
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_MaxVal                                         0x1
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_MinVal                                         0x0
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_RstVal                                         0x0

/*--------------------------------------
BitField Name: POHTerCtrlHi_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH Termintate Insert
Control STS"
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_Bit_Start                                       19
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_Bit_End                                         19
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_Mask                                        cBit19
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_Shift                                           19
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_MaxVal                                         0x1
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_MinVal                                         0x0
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_RstVal                                         0x0

/*--------------------------------------
BitField Name: POHCpeVtCtr_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH CPE VT Control Register"
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_Bit_Start                                        18
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_Bit_End                                          18
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_Mask                                         cBit18
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_Shift                                            18
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_MaxVal                                          0x1
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_RstVal                                          0x0

/*--------------------------------------
BitField Name: POHCpeStsCtr_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH CPE STS/TU3 Control
Register"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_Bit_Start                                       17
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_Bit_End                                         17
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_Mask                                        cBit17
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_Shift                                           17
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_MaxVal                                         0x1
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_MinVal                                         0x0
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_RstVal                                         0x0

/*--------------------------------------
BitField Name: OCNTohCtlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN TOH Monitoring Per
Channel Control" SliceId 1
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_Bit_Start                                      16
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_Bit_End                                        16
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_Mask                                       cBit16
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_Shift                                          16
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNTohCtlSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpiCtlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN STS Pointer Interpreter
Per Channel Control" SliceId 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_Bit_Start                                      15
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_Bit_End                                        15
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_Mask                                       cBit15
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_Shift                                          15
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpiCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN STS Pointer Interpreter
Per Channel Control" SliceId 0
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_Bit_Start                                      14
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_Bit_End                                        14
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_Mask                                       cBit14
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_Shift                                          14
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiDemSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN RXPP Per STS payload
Control" SliceId 1
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_Bit_Start                                      13
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_Bit_End                                        13
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_Mask                                       cBit13
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_Shift                                          13
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiDemSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN RXPP Per STS payload
Control" SliceId 0
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_Bit_Start                                      12
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_Bit_End                                        12
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_Mask                                       cBit12
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_Shift                                          12
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiCtlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN VTTU Pointer Interpreter
Per Channel Control" SliceId 1
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_Bit_Start                                      11
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_Bit_End                                        11
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_Mask                                       cBit11
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_Shift                                          11
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpiCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN VTTU Pointer Interpreter
Per Channel Control" SliceId 0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_Bit_Start                                      10
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_Bit_End                                        10
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_Mask                                       cBit10
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_Shift                                          10
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgDemSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN TXPP Per STS
Multiplexing Control" SliceId 1
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_Bit_Start                                       9
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_Bit_End                                         9
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_Mask                                        cBit9
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_Shift                                           9
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgDemSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN TXPP Per STS
Multiplexing Control" SliceId 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_Bit_Start                                       8
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_Bit_End                                         8
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_Mask                                        cBit8
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_Shift                                           8
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgCtlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN VTTU Pointer Generator
Per Channel Control" SliceId 1
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_Bit_Start                                       7
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_Bit_End                                         7
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_Mask                                        cBit7
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_Shift                                           7
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNVpgCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN VTTU Pointer Generator
Per Channel Control" SliceId 0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_Bit_Start                                       6
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_Bit_End                                         6
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_Mask                                        cBit6
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_Shift                                           6
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpgCtlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN STS Pointer Generator
Per Channel Control" SliceId 1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_Bit_Start                                       5
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_Bit_End                                         5
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_Mask                                        cBit5
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_Shift                                           5
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNSpgCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN STS Pointer Generator
Per Channel Control" SliceId 0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_Bit_Start                                       4
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_Bit_End                                         4
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_Mask                                        cBit4
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_Shift                                           4
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmCtlSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_Bit_Start                                       3
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_Bit_End                                         3
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_Mask                                        cBit3
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_Shift                                           3
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc1_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_Bit_Start                                       2
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_Bit_End                                         2
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_Mask                                        cBit2
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_Shift                                           2
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_MaxVal                                        0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_MinVal                                        0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_RstVal                                        0x0

/*--------------------------------------
BitField Name: OCNTfmJ0Slc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_Bit_Start                                        1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_Bit_End                                          1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_Mask                                         cBit1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_Shift                                            1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_MaxVal                                         0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_MinVal                                         0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc1_ParErrStk_RstVal                                         0x0

/*--------------------------------------
BitField Name: OCNTfmJ0Slc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_Bit_Start                                        0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_Bit_End                                          0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_Mask                                         cBit0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_Shift                                            0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_MaxVal                                         0x1
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_MinVal                                         0x0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_RstVal                                         0x0

#define cAf6Reg_RAM_Parity_Error_Intr_Group_Base                                                       0x00121
#define cAf6Reg_RAM_Parity_Error_Intr_Group                                                            0x00121
#define cAf6Reg_RAM_Parity_Error_Intr_Group_WidthVal                                                        32
#define cAf6Reg_RAM_Parity_Error_Intr_Group_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: Wrd2
BitField Type: RW
BitField Desc: Word2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_Bit_Start                                                      2
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_Bit_End                                                        2
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_Mask                                                       cBit2
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_Shift                                                          2
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_MaxVal                                                       0x1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_MinVal                                                       0x0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_RstVal                                                         0

/*--------------------------------------
BitField Name: Wrd1
BitField Type: RW
BitField Desc: Word1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_Bit_Start                                                      1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_Bit_End                                                        1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_Mask                                                       cBit1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_Shift                                                          1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_MaxVal                                                       0x1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_MinVal                                                       0x0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_RstVal                                                         0

/*--------------------------------------
BitField Name: Wrd0
BitField Type: RW
BitField Desc: Word0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_Bit_Start                                                      0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_Bit_End                                                        0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_Mask                                                       cBit0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_Shift                                                          0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_MaxVal                                                       0x1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_MinVal                                                       0x0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_RstVal                                                         0

/*------------------------------------------------------------------------------
Reg Name   : Temperate Event
Reg Addr   : 0x00_00D0
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report state change of Temperate status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_temp_Base                                                                         0x0000D0
#define cAf6Reg_alm_temp                                                                              0x0000D0
#define cAf6Reg_alm_temp_WidthVal                                                                           32
#define cAf6Reg_alm_temp_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Temp_alarm
BitField Type: WC
BitField Desc: Temperate change Event  1: Changed  0: Normal
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_alm_temp_Temp_alarm_Bit_Start                                                                   0
#define cAf6_alm_temp_Temp_alarm_Bit_End                                                                     0
#define cAf6_alm_temp_Temp_alarm_Mask                                                                    cBit0
#define cAf6_alm_temp_Temp_alarm_Shift                                                                       0
#define cAf6_alm_temp_Temp_alarm_MaxVal                                                                    0x1
#define cAf6_alm_temp_Temp_alarm_MinVal                                                                    0x0
#define cAf6_alm_temp_Temp_alarm_RstVal                                                                    0x0

/* Debug registers */
#define cDebugCounterSticky 0x500008
#define cDebugCntrxpkin     cBit24
#define cDebugCntrxoam      cBit23
#define cDebugCntrxlbit     cBit22
#define cDebugCntrxnbit     cBit21
#define cDebugCntrxpbit     cBit20
#define cDebugCntrxrbit     cBit19
#define cDebugCntrxlate     cBit18
#define cDebugCntrxearly    cBit17
#define cDebugCntrxlost     cBit16
#define cDebugCntrxlopsta   cBit15
#define cDebugCntrxlopsyn   cBit14
#define cDebugCntrxoverrun  cBit13
#define cDebugCntrxunderrun cBit12
#define cDebugCntrxmalform  cBit11
#define cDebugCntrxstray    cBit10
#define cDebugCnttxpkout    cBit9
#define cDebugCnttxlbit     cBit8
#define cDebugCnttxnbit     cBit7
#define cDebugCnttxpbit     cBit6
#define cDebugCnttxrbit     cBit5
#define cDebugCnttxoam      cBit4
#define cDebugPhy_err       cBit3
#define cDebugCntetherr     cBit2
#define cDebugCntpsnerr     cBit1
#define cDebugCntudperr     cBit0

#define cDebugPweSticky          0x500009

#define cDebugPweDdr4_vlderr     cBit31
#define cDebugPweDdr3_vlderr     cBit20
#define cDebugPweAvl3_alarmMask  cBit29_24
#define cDebugPweAvl3_alarmShift 24

#define cDebugPweDdr2_eccerr     cBit23
#define cDebugPweDdr2_vlderr     cBit22
#define cDebugPweAvl2_alarmMask  cBit21_16
#define cDebugPweAvl2_alarmShift 16

#define cDebugPweDdr1_reorerr    cBit15
#define cDebugPweDdr1_vlderr     cBit14
#define cDebugPweAvl1_alarmMask  cBit13_8
#define cDebugPweAvl1_alarmShift 8

#define cDebugPwecasame          cBit6
#define cDebugPwecaemp           cBit5
#define cDebugPweblksame         cBit4
#define cDebugPweblkemp          cBit3
#define cDebugPwewerr            cBit2
#define cDebugPwevldwerr         cBit1
#define cDebugPwesame            cBit0

#define cDebugPdaSticky                    0x50000A
#define cDebugPdaWrca_oversize             cBit31
#define cDebugPdaWrblkrdy                  cBit30
#define cDebugPdaFifofull                  cBit29

#define cDebugPdaRxclsvldRxclseopRxclserr  cBit27
#define cDebugPdaRxclsvldRxclseop          cBit26

#define cDebugPdaWrca_en                   cBit19
#define cDebugPdaFree_en                   cBit18

#define cDebugPdaDqwrokwerr                cBit16
#define cDebugPdaRdsegwerr                 cBit15
#define cDebugPdaRdcafull                  cBit14
#define cDebugPdaRdllfull                  cBit13
#define cDebugPdaLlvldwerr                 cBit12
#define cDebugPdaLlreqwerr                 cBit11
#define cDebugPdaLostsop                   cBit10
#define cDebugPdaLosteop                   cBit9
#define cDebugPdaTdmcagap                  cBit8
#define cDebugPdaWrca_emp                  cBit7
#define cDebugPdaWrca_err                  cBit6
#define cDebugPdaTdmcaemp                  cBit5
#define cDebugPdaTdmcasame                 cBit4
#define cDebugPdaEncfffull                 cBit3
#define cDebugPdaEncffempt                 cBit2
#define cDebugPdaBlkemp                    cBit1
#define cDebugPdaSameblk                   cBit0

#define cDebugEthernetSticky 0x50000B
#define cDebugEthErrmplsnum  cBit22
#define cDebugEthErrpwdis    cBit21
#define cDebugEthErrmeftyp   cBit20
#define cDebugEthErrmefda    cBit19
#define cDebugEthErrmefecid  cBit18
#define cDebugEthClainit     cBit17
#define cDebugEthErrpt       cBit16
#define cDebugEthOversize    cBit15
#define cDebugEthUndrsize    cBit14
#define cDebugEthClafull     cBit13
#define cDebugEthErreop      cBit12
#define cDebugEthErrphy      cBit11
#define cDebugEthErrmac      cBit10
#define cDebugEthErrtyp      cBit9
#define cDebugEthErrpwid     cBit8
#define cDebugEthErripv4ver  cBit7
#define cDebugEthErripv4add  cBit6
#define cDebugEthErripv4sum  cBit5
#define cDebugEthErripv6ver  cBit4
#define cDebugEthErripv6add  cBit3
#define cDebugEthErrcw       cBit2
#define cDebugEthErrssrc     cBit1
#define cDebugEthErrlen      cBit0

#define cDebugCoreSticky        0x110
#define cDebugCoreMap2pla_b3err cBit16
#define cDebugCoreCla2geena     cBit15
#define cDebugCoreRxeth_eop     cBit14
#define cDebugCoreRxeth_sop     cBit13
#define cDebugCoreRxeth_vld     cBit12
#define cDebugCoreRxclserr      cBit11
#define cDebugCoreRxclseop      cBit10
#define cDebugCoreRxclssop      cBit9
#define cDebugCoreRxclsvld      cBit8
#define cDebugCorePlawerr       cBit7
#define cDebugCorePwe_ovld      cBit6
#define cDebugCorePwe_osop      cBit5
#define cDebugCorePwe_oeop      cBit4
#define cDebugCoreGe00llrxfulMask  cBit3_0
#define cDebugCoreGe00llrxfulShift 0

#define cDebugCoreStatus                0x120
#define cDebugCoreStatusGe00lltxenb     cBit31
#define cDebugCoreStatusGe00lltxsop     cBit30
#define cDebugCoreStatusGe00lltxeop     cBit29
#define cDebugCoreStatusGe00lltxvld     cBit28
#define cDebugCoreStatusPwe_ena         cBit27
#define cDebugCoreStatusPwe_oeop        cBit26
#define cDebugCoreStatusPwe_osop        cBit25
#define cDebugCoreStatusPwe_ovld        cBit24
#define cDebugCoreStatusMap2pla1_dpwval cBit23
#define cDebugCoreStatusMap2pla2_dpwval cBit22
#define cDebugCoreStatusPla2pwe_opweop  cBit21
#define cDebugCoreStatusPla2pwe_opwvld  cBit20
#define cDebugCoreStatusPwepk_req       cBit19
#define cDebugCoreStatusPweca_req       cBit18
#define cDebugCoreStatusPlapk_req       cBit17
#define cDebugCoreStatusPlaca_req       cBit16
#define cDebugCoreStatusReor_req        cBit15
#define cDebugCoreStatusJb_req          cBit14
#define cDebugCoreStatusRdseg_req       cBit13
#define cDebugCoreStatusWrseg_req       cBit12
#define cDebugCoreStatusWrca_req        cBit11
#define cDebugCoreStatusRdca_req        cBit10
#define cDebugCoreStatusAvl3_rdy        cBit9
#define cDebugCoreStatusAvl3_done       cBit8
#define cDebugCoreStatusAvl2_rdy        cBit7
#define cDebugCoreStatusAvl2_done       cBit6
#define cDebugCoreStatusAvl1_rdy        cBit5
#define cDebugCoreStatusAvl1_done       cBit4
#define cDebugCoreStatusRxclslbit       cBit3
#define cDebugCoreStatusRxclseop        cBit2
#define cDebugCoreStatusRxclssop        cBit1
#define cDebugCoreStatusRxclsvld        cBit0

#define cDebugCdrSticky             0x0280828
#define cDebugCdrExt3vc3full        cBit27
#define cDebugCdrExt3vc3empty       cBit26
#define cDebugCdrExt3vc12full       cBit25
#define cDebugCdrExt3vc12empty      cBit24
#define cDebugCdrExt3vc11full       cBit23
#define cDebugCdrExt3vc11empty      cBit22
#define cDebugCdrExt2vc3full        cBit21
#define cDebugCdrExt2vc3empty       cBit20
#define cDebugCdrExt2vc12full       cBit19
#define cDebugCdrExt2vc12empty      cBit18
#define cDebugCdrExt2vc11full       cBit17
#define cDebugCdrExt2vc11empty      cBit16
#define cDebugCdrExt1vc3full        cBit15
#define cDebugCdrExt1vc3empty       cBit14
#define cDebugCdrExt1vc12full       cBit13
#define cDebugCdrExt1vc12empty      cBit12
#define cDebugCdrExt1vc11full       cBit11
#define cDebugCdrExt1vc11empty      cBit10
#define cDebugCdrOcdrvc3wrfifofull  cBit9
#define cDebugCdrOcdrvc3rdfifoempty cBit8
#define cDebugCdrOcdrvtwrfifofull   cBit7
#define cDebugCdrOcdrvtrdfifoempty  cBit6
#define cDebugCdrOltvc3wrfifofull   cBit5
#define cDebugCdrOltvc3rdfifoempty  cBit4
#define cDebugCdrOltde3wrfifofull   cBit3
#define cDebugCdrOltvtwrfifofull    cBit2
#define cDebugCdrOltvtrdfifoempty   cBit1
#define cDebugCdrOltde1wrfifofull   cBit0

#define cDebugCdrOcnSticky          0x0280829
#define cDebugCdrOcn4vc3full        cBit23
#define cDebugCdrOcn4vc3empty       cBit22
#define cDebugCdrOcn4vc12full       cBit21
#define cDebugCdrOcn4vc12empty      cBit20
#define cDebugCdrOcn4vc11full       cBit19
#define cDebugCdrOcn4vc11empty      cBit18
#define cDebugCdrOcn3vc3full        cBit17
#define cDebugCdrOcn3vc3empty       cBit16
#define cDebugCdrOcn3vc12full       cBit15
#define cDebugCdrOcn3vc12empty      cBit14
#define cDebugCdrOcn3vc11full       cBit13
#define cDebugCdrOcn3vc11empty      cBit12
#define cDebugCdrOcn2vc3full        cBit11
#define cDebugCdrOcn2vc3empty       cBit10
#define cDebugCdrOcn2vc12full       cBit9
#define cDebugCdrOcn2vc12empty      cBit8
#define cDebugCdrOcn2vc11full       cBit7
#define cDebugCdrOcn2vc11empty      cBit6
#define cDebugCdrOcn1vc3full        cBit5
#define cDebugCdrOcn1vc3empty       cBit4
#define cDebugCdrOcn1vc12full       cBit3
#define cDebugCdrOcn1vc12empty      cBit2
#define cDebugCdrOcn1vc11full       cBit1
#define cDebugCdrOcn1vc11empty      cBit0

#define cDebugQSGMII_SGMII_MAC1 0x401048
#define cDebugMacUser_rxinf     cBit4
#define cDebugMacTxfm_irer      cBit3
#define cDebugMacTxfmmoderr     cBit2
#define cDebugMacTxfmsoperr     cBit1
#define cDebugMacTxfmeoperr     cBit0

#define cDebugQSGMII_SGMII_MAC2    0x401049
#define cDebugTxfm_iinf            cBit12
#define cDebugPortenb              cBit11
#define cDebugGenpauenb            cBit10
#define cDebugUser_rxvld           cBit9
#define cDebugUser_txvld           cBit8
#define cDebugTxfm_oreq            cBit5
#define cDebugTxfm_ivld            cBit4
#define cDebugRxif_ierAndrxif_idv  cBit3
#define cDebugRxif_ivldAndrxif_idv cBit2
#define cDebugTxif_oerAndtxif_oen  cBit1
#define cDebugTxif_ovldAndtxif_oen cBit0

#define cDebugQSGMII_SGMII_MAC_Status 0x40100e
#define cDebugMacStatusUser_rxvld     cBit5
#define cDebugMacStatusUser_txvld     cBit4
#define cDebugMacStatusUpact          cBit3
#define cDebugMacStatusPortenb        cBit2
#define cDebugMacStatusGenpauenb      cBit1
#define cDebugMacStatusUser_txenb     cBit0

#define cDebugXfiSticky1        0x3c1048
#define cDebugXfiMac_rxstkMask  cBit23_12
#define cDebugXfiMac_rxstkShift 12
#define cDebugXfiDiag_pkterr1   cBit11
#define cDebugXfiDiag_prbserr1  cBit10
#define cDebugXfiRxgeconv_werr  cBit9
#define cDebugXfiUsr_rxerr      cBit8
#define cDebugXfiUsr_rxenb      cBit7
#define cDebugXfiUsr_rxvld      cBit6
#define cDebugXfiUsr_rxsop      cBit5
#define cDebugXfiUsr_rxeop      cBit4
#define cDebugXfiUsr_txenb      cBit3
#define cDebugXfiUsr_txvld      cBit2
#define cDebugXfiUsr_txsop      cBit1
#define cDebugXfiUsr_txeop      cBit0

#define cDebugXfiSticky2        0x3c100a
#define cDebugXfiRxif_sfdmis1   cBit8
#define cDebugXfiRxif_outsel1   cBit7
#define cDebugXfiRxif_over1     cBit6
#define cDebugXfiRxif_under1    cBit5
#define cDebugXfiRxff_rderr1    cBit4
#define cDebugXfiTxif_gmerr1    cBit3
#define cDebugXfiTxff_wrerr1    cBit2
#define cDebugXfiTxsh_err1Mask  cBit1_0
#define cDebugXfiTxsh_err1Shift 0

#define cDebugLink_status            cBit16
#define cDebugGmii1_link             cBit15
#define cDebugGmii2_link             cBit14
#define cDebugGmii3_link             cBit13
#define cDebugGmii4_link             cBit12
#define cDebugGe_pll_locked          cBit10
#define cDebugSys_pll_locked         cBit9
#define cDebugSys25_pll_locked       cBit8
#define cDebugXfi_pll1_locked        cBit5
#define cDebugXfi_pll2_locked        cBit4
#define cDebugC2_init_calib_complete cBit2
#define cDebugC1_init_calib_complete cBit1
#define cDebugC0_init_calib_complete cBit0

#define cDevicePllStickyRegisters    0xf00051
#define cDebugAvlmux_err cBit3
#define cDebugApp2_rderr cBit2
#define cDebugApp1_rderr cBit1
#define cDebugApp0_rderr cBit0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031DEVICEREG_H_ */


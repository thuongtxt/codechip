/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210031VersionReaderInternal.h
 * 
 * Created Date: Oct 27, 2016
 *
 * Description : Version reader
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031VERSIONREADERINTERNAL_H_
#define _THA60210031VERSIONREADERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/man/Tha60210011VersionReader.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031VersionReader
    {
    tTha60210011VersionReader super;
    }tTha60210031VersionReader;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVersionReader Tha60210031VersionReaderObjectInit(ThaVersionReader self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031VERSIONREADERINTERNAL_H_ */


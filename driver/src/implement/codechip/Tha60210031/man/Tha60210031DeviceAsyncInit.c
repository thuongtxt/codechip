/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210031DeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031Device.h"
#include "Tha60210031DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031Device)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum AsyncStates
    {
    cDeviceAsyncState_start = 0,
    cDeviceAsyncState_HwFlush,
    cDeviceAsyncState_SuperCall,
    cDeviceAsyncState_MacActivate,
    cDeviceAsyncState_ModulesResourcesCheck,
    } AsyncState;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncHwFlushStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncHwFlushState = state;
    }

static uint32 AsyncHwFlushStateGet(AtDevice self)
    {
    return mThis(self)->asyncHwFlushState;
    }

static void AsyncInitStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

static void AsyncInitStateReset(AtDevice self)
    {
    mThis(self)->asyncInitState = cDeviceAsyncState_start;
    }

static uint32 AsyncInitStateGet(AtDevice self)
    {
    return mThis(self)->asyncInitState;
    }

static eAtRet AsyncHwFlush(AtDevice self)
    {
    eAtRet ret;
    uint32 state = AsyncHwFlushStateGet(self);
    uint32 numFunctions;
    tHwAsynFlushFunc *functions = Tha60210031DeviceFlushFunctions(&numFunctions);
    char stateStr[32];
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);

    functions[state]((ThaDevice)self);
    AtSprintf(stateStr, "AsyncHwFlush State: = %d\r\n", state);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, stateStr);
    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, __FILE__, __LINE__, stateStr);

    state += 1;
    if (state == numFunctions)
        {
        state = 0;
        ret = cAtOk;
        }
    else
        ret = cAtErrorAgain;

    AsyncHwFlushStateSet(self, state);
    return ret;
    }

eAtRet Tha60210031DeviceAsyncInit(AtDevice self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    eAtRet ret;
    tAtOsalCurTime profileTime;
    uint32 state = AsyncInitStateGet(self);

    AtOsalCurTimeGet(&profileTime);
    switch (state)
        {
        case cDeviceAsyncState_start:
            ret = Tha60210031DeviceReWriteHandlerInstall(self);
            if ((ret != cAtOk) && (ret != cAtErrorDuplicatedEntries))
                return ret;
            else
        	    {
                AsyncInitStateSet(self, cDeviceAsyncState_HwFlush);
                ret = cAtErrorAgain;
        	    }
            break;

        case cDeviceAsyncState_HwFlush:
            ret = AsyncHwFlush(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AsyncHwFlush");
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AsyncInit");
            if (ret == cAtOk)
                {
        	    AsyncInitStateSet(self, cDeviceAsyncState_SuperCall);
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
        	    AsyncInitStateReset(self);
            break;

        case cDeviceAsyncState_SuperCall:
            ret = Tha60210031DeviceSuperAsyncInit(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "Tha60210031DeviceSuperAsyncInit");
            if (ret == cAtOk)
                {
                AsyncInitStateSet(self, cDeviceAsyncState_MacActivate);
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                AsyncInitStateReset(self);
            break;

        case cDeviceAsyncState_MacActivate:
            /* Activate ports as default */
            ret = ThaModuleEthAllPortMacsActivate(ethModule, cAtTrue);
            if (ret == cAtOk)
                {
                AsyncInitStateSet(self, cDeviceAsyncState_ModulesResourcesCheck);
                ret = cAtErrorAgain;
                }

            else if (!AtDeviceAsyncRetValIsInState(ret))
                AsyncInitStateReset(self);

            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "ThaModuleEthAllPortMacsActivate");
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AsyncInit");
            break;

        case cDeviceAsyncState_ModulesResourcesCheck:
            ret = AtDeviceAllModulesHwResourceCheck(self);
            AsyncInitStateReset(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "ModulesResourceCheck");
            AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, "ModulesResourceCheck");
            break;

        default:
            AsyncInitStateReset(self);
            ret = cAtErrorDevFail;
        }

    return ret;
    }

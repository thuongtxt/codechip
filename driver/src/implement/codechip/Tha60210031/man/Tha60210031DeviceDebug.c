/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210031DeviceDebug.c
 *
 * Created Date: Oct 1, 2015
 *
 * Description : Debug information display
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031Device.h"
#include "Tha60210031DeviceReg.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void Tha60210031CounterStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugCounterSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Counters sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "cntrxpkin"   , regValue, cDebugCntrxpkin    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxoamp"   , regValue, cDebugCntrxoam     );
    Tha6021DebugPrintErrorBit(debugger, "cntrxlbit"   , regValue, cDebugCntrxlbit    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxnbit"   , regValue, cDebugCntrxnbit    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxpbit"   , regValue, cDebugCntrxpbit    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxrbit"   , regValue, cDebugCntrxrbit    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxlate"   , regValue, cDebugCntrxlate    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxearl"   , regValue, cDebugCntrxearly   );
    Tha6021DebugPrintErrorBit(debugger, "cntrxlost"   , regValue, cDebugCntrxlost    );
    Tha6021DebugPrintErrorBit(debugger, "cntrxlopsta" , regValue, cDebugCntrxlopsta  );
    Tha6021DebugPrintErrorBit(debugger, "cntrxlopsyn" , regValue, cDebugCntrxlopsyn  );
    Tha6021DebugPrintErrorBit(debugger, "cntrxover"   , regValue, cDebugCntrxoverrun );
    Tha6021DebugPrintErrorBit(debugger, "cntrxunde"   , regValue, cDebugCntrxunderrun);
    Tha6021DebugPrintErrorBit(debugger, "cntrxmalf"   , regValue, cDebugCntrxmalform );
    Tha6021DebugPrintErrorBit(debugger, "cntrxstra"   , regValue, cDebugCntrxstray   );
    Tha6021DebugPrintErrorBit(debugger, "cnttxpkout"  , regValue, cDebugCnttxpkout   );
    Tha6021DebugPrintErrorBit(debugger, "cnttxlbit"   , regValue, cDebugCnttxlbit    );
    Tha6021DebugPrintErrorBit(debugger, "cnttxnbit"   , regValue, cDebugCnttxnbit    );
    Tha6021DebugPrintErrorBit(debugger, "cnttxpbit"   , regValue, cDebugCnttxpbit    );
    Tha6021DebugPrintErrorBit(debugger, "cnttxrbit"   , regValue, cDebugCnttxrbit    );
    Tha6021DebugPrintErrorBit(debugger, "cnttxoam"    , regValue, cDebugCnttxoam     );
    Tha6021DebugPrintErrorBit(debugger, "phy_err"     , regValue, cDebugPhy_err      );
    Tha6021DebugPrintErrorBit(debugger, "cntetherr"   , regValue, cDebugCntetherr    );
    Tha6021DebugPrintErrorBit(debugger, "cntpsnerr"   , regValue, cDebugCntpsnerr    );
    Tha6021DebugPrintErrorBit(debugger, "cntudperr"   , regValue, cDebugCntudperr    );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031PweStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);

    regAddres = cDebugPweSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* PWE sticky", regAddres, regValue);

    Tha6021DebugPrintErrorBit(debugger, "Ddr4_vlderr", regValue, cDebugPweDdr4_vlderr);
    Tha6021DebugPrintErrorBit(debugger, "Ddr3_vlderr", regValue, cDebugPweDdr3_vlderr);
    Tha6021DebugPrintBitFieldVal(debugger, "Avl3_alarm"    , regValue, cDebugPweAvl3_alarmMask, cDebugPweAvl3_alarmShift);

    Tha6021DebugPrintErrorBit(debugger, "Ddr2_eccerr", regValue, cDebugPweDdr2_eccerr);
    Tha6021DebugPrintErrorBit(debugger, "Ddr2_vlderr", regValue, cDebugPweDdr2_vlderr);
    Tha6021DebugPrintBitFieldVal(debugger, "Avl2_alarm"    , regValue, cDebugPweAvl2_alarmMask, cDebugPweAvl2_alarmShift);

    Tha6021DebugPrintErrorBit(debugger, "Ddr1_reorerr", regValue, cDebugPweDdr1_reorerr);
    Tha6021DebugPrintErrorBit(debugger, "Ddr1_vlderr" , regValue, cDebugPweDdr1_vlderr);
    Tha6021DebugPrintBitFieldVal(debugger, "Avl1_alarm" , regValue, cDebugPweAvl1_alarmMask, cDebugPweAvl1_alarmShift);

    Tha6021DebugPrintErrorBit(debugger, "Pwecasame" , regValue, cDebugPwecasame  );
    Tha6021DebugPrintErrorBit(debugger, "Pwecaemp"  , regValue, cDebugPwecaemp   );
    Tha6021DebugPrintErrorBit(debugger, "Pweblksame", regValue, cDebugPweblksame );
    Tha6021DebugPrintErrorBit(debugger, "Pweblkemp" , regValue, cDebugPweblkemp  );
    Tha6021DebugPrintErrorBit(debugger, "Pwewerr"   , regValue, cDebugPwewerr    );
    Tha6021DebugPrintErrorBit(debugger, "Pwevldwerr", regValue, cDebugPwevldwerr );
    Tha6021DebugPrintErrorBit(debugger, "Pwesame"   , regValue, cDebugPwesame    );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031PdaStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugPdaSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* PDA sticky", regAddres, regValue);

    Tha6021DebugPrintErrorBit(debugger, "wrca_oversize", regValue, cDebugPdaWrca_oversize);
    Tha6021DebugPrintErrorBit(debugger, "!wrblkrdy"    , regValue, cDebugPdaWrblkrdy     );
    Tha6021DebugPrintErrorBit(debugger, "fifofull"     , regValue, cDebugPdaFifofull     );
    Tha6021DebugPrintErrorBit(debugger, "rxclsvld & rxclseop & (|rxclserr)", regValue, cDebugPdaRxclsvldRxclseopRxclserr  );
    Tha6021DebugPrintErrorBit(debugger, "rxclsvld & rxclseop", regValue, cDebugPdaRxclsvldRxclseop);
    Tha6021DebugPrintErrorBit(debugger, "!wrca_en"  , regValue, cDebugPdaWrca_en   );
    Tha6021DebugPrintErrorBit(debugger, "!free_en"  , regValue, cDebugPdaFree_en   );
    Tha6021DebugPrintErrorBit(debugger, "dqwrokwerr", regValue, cDebugPdaDqwrokwerr);
    Tha6021DebugPrintErrorBit(debugger, "rdsegwerr" , regValue, cDebugPdaRdsegwerr );
    Tha6021DebugPrintErrorBit(debugger, "rdcafull"  , regValue, cDebugPdaRdcafull  );
    Tha6021DebugPrintErrorBit(debugger, "rdllfull"  , regValue, cDebugPdaRdllfull  );
    Tha6021DebugPrintErrorBit(debugger, "llvldwerr" , regValue, cDebugPdaLlvldwerr );
    Tha6021DebugPrintErrorBit(debugger, "llreqwerr" , regValue, cDebugPdaLlreqwerr );
    Tha6021DebugPrintErrorBit(debugger, "lostsop"   , regValue, cDebugPdaLostsop   );
    Tha6021DebugPrintErrorBit(debugger, "losteop"   , regValue, cDebugPdaLosteop   );
    Tha6021DebugPrintErrorBit(debugger, "tdmcagap"  , regValue, cDebugPdaTdmcagap  );
    Tha6021DebugPrintErrorBit(debugger, "wrca_emp"  , regValue, cDebugPdaWrca_emp  );
    Tha6021DebugPrintErrorBit(debugger, "wrca_err"  , regValue, cDebugPdaWrca_err  );
    Tha6021DebugPrintErrorBit(debugger, "tdmcaemp"  , regValue, cDebugPdaTdmcaemp  );
    Tha6021DebugPrintErrorBit(debugger, "tdmcasame" , regValue, cDebugPdaTdmcasame );
    Tha6021DebugPrintErrorBit(debugger, "encfffull" , regValue, cDebugPdaEncfffull );
    Tha6021DebugPrintErrorBit(debugger, "encffempt" , regValue, cDebugPdaEncffempt );
    Tha6021DebugPrintErrorBit(debugger, "blkemp"    , regValue, cDebugPdaBlkemp    );
    Tha6021DebugPrintErrorBit(debugger, "sameblk"   , regValue, cDebugPdaSameblk   );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031EthStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugEthernetSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Ethernet sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "errmplsnum", regValue, cDebugEthErrmplsnum);
    Tha6021DebugPrintErrorBit(debugger, "errpwdis"  , regValue, cDebugEthErrpwdis  );
    Tha6021DebugPrintErrorBit(debugger, "errmeftyp" , regValue, cDebugEthErrmeftyp );
    Tha6021DebugPrintErrorBit(debugger, "errmefda"  , regValue, cDebugEthErrmefda  );
    Tha6021DebugPrintErrorBit(debugger, "errmefecid", regValue, cDebugEthErrmefecid);
    Tha6021DebugPrintErrorBit(debugger, "clainit"   , regValue, cDebugEthClainit   );
    Tha6021DebugPrintErrorBit(debugger, "errpt"     , regValue, cDebugEthErrpt     );
    Tha6021DebugPrintErrorBit(debugger, "oversize"  , regValue, cDebugEthOversize  );
    Tha6021DebugPrintErrorBit(debugger, "undrsize"  , regValue, cDebugEthUndrsize  );
    Tha6021DebugPrintErrorBit(debugger, "clafull"   , regValue, cDebugEthClafull   );
    Tha6021DebugPrintErrorBit(debugger, "erreop"    , regValue, cDebugEthErreop    );
    Tha6021DebugPrintErrorBit(debugger, "errphy"    , regValue, cDebugEthErrphy    );
    Tha6021DebugPrintErrorBit(debugger, "errmac"    , regValue, cDebugEthErrmac    );
    Tha6021DebugPrintErrorBit(debugger, "errtyp"    , regValue, cDebugEthErrtyp    );
    Tha6021DebugPrintErrorBit(debugger, "errpwid"   , regValue, cDebugEthErrpwid   );
    Tha6021DebugPrintErrorBit(debugger, "erripv4ver", regValue, cDebugEthErripv4ver);
    Tha6021DebugPrintErrorBit(debugger, "erripv4add", regValue, cDebugEthErripv4add);
    Tha6021DebugPrintErrorBit(debugger, "erripv4sum", regValue, cDebugEthErripv4sum);
    Tha6021DebugPrintErrorBit(debugger, "erripv6ver", regValue, cDebugEthErripv6ver);
    Tha6021DebugPrintErrorBit(debugger, "erripv6add", regValue, cDebugEthErripv6add);
    Tha6021DebugPrintErrorBit(debugger, "errcw"     , regValue, cDebugEthErrcw     );
    Tha6021DebugPrintErrorBit(debugger, "errssrc"   , regValue, cDebugEthErrssrc   );
    Tha6021DebugPrintErrorBit(debugger, "errlen"    , regValue, cDebugEthErrlen    );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031CoreStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);

    regAddres = cDebugCoreSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Core sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "map2pla_b3err", regValue, cDebugCoreMap2pla_b3err);
    Tha6021DebugPrintErrorBit(debugger, "cla2geena", regValue, cDebugCoreCla2geena);
    Tha6021DebugPrintErrorBit(debugger, "rxeth_eop", regValue, cDebugCoreRxeth_eop);
    Tha6021DebugPrintErrorBit(debugger, "rxeth_sop", regValue, cDebugCoreRxeth_sop);
    Tha6021DebugPrintErrorBit(debugger, "rxeth_vld", regValue, cDebugCoreRxeth_vld);
    Tha6021DebugPrintErrorBit(debugger, "rxclserr", regValue, cDebugCoreRxclserr);
    Tha6021DebugPrintErrorBit(debugger, "rxclseop", regValue, cDebugCoreRxclseop);
    Tha6021DebugPrintErrorBit(debugger, "rxclssop", regValue, cDebugCoreRxclssop);
    Tha6021DebugPrintErrorBit(debugger, "rxclsvld", regValue, cDebugCoreRxclsvld);
    Tha6021DebugPrintErrorBit(debugger, "plawerr", regValue, cDebugCorePlawerr);
    Tha6021DebugPrintErrorBit(debugger, "pwe_ovld", regValue, cDebugCorePwe_ovld);
    Tha6021DebugPrintErrorBit(debugger, "pwe_osop", regValue, cDebugCorePwe_osop);
    Tha6021DebugPrintErrorBit(debugger, "pwe_oeop", regValue, cDebugCorePwe_oeop);
    Tha6021DebugPrintBitFieldVal(debugger, "ge00llrxful", regValue, cDebugCoreGe00llrxfulMask, cDebugCoreGe00llrxfulShift);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031CoreStatusDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);

    regAddres = cDebugCoreStatus;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Core status", regAddres, regValue);
    Tha6021DebugPrintInfoBit(debugger, "Ge00lltxenb", regValue, cDebugCoreStatusGe00lltxenb);
    Tha6021DebugPrintInfoBit(debugger, "Ge00lltxsop", regValue, cDebugCoreStatusGe00lltxsop);
    Tha6021DebugPrintInfoBit(debugger, "Ge00lltxeop", regValue, cDebugCoreStatusGe00lltxeop);
    Tha6021DebugPrintInfoBit(debugger, "Ge00lltxvld", regValue, cDebugCoreStatusGe00lltxvld);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_ena", regValue, cDebugCoreStatusPwe_ena);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_oeop", regValue, cDebugCoreStatusPwe_oeop);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_osop", regValue, cDebugCoreStatusPwe_osop);
    Tha6021DebugPrintInfoBit(debugger, "Pwe_ovld", regValue, cDebugCoreStatusPwe_ovld);
    Tha6021DebugPrintInfoBit(debugger, "Map2pla1_dpwval", regValue, cDebugCoreStatusMap2pla1_dpwval);
    Tha6021DebugPrintInfoBit(debugger, "Map2pla2_dpwval", regValue, cDebugCoreStatusMap2pla2_dpwval);
    Tha6021DebugPrintInfoBit(debugger, "Pla2pwe_opweop", regValue, cDebugCoreStatusPla2pwe_opweop);
    Tha6021DebugPrintInfoBit(debugger, "Pla2pwe_opwvld", regValue, cDebugCoreStatusPla2pwe_opwvld);
    Tha6021DebugPrintInfoBit(debugger, "Pwepk_req", regValue, cDebugCoreStatusPwepk_req);
    Tha6021DebugPrintInfoBit(debugger, "Pweca_req", regValue, cDebugCoreStatusPweca_req);
    Tha6021DebugPrintInfoBit(debugger, "Plapk_req", regValue, cDebugCoreStatusPlapk_req);
    Tha6021DebugPrintInfoBit(debugger, "Placa_req", regValue, cDebugCoreStatusPlaca_req);
    Tha6021DebugPrintInfoBit(debugger, "Reor_req", regValue, cDebugCoreStatusReor_req);
    Tha6021DebugPrintInfoBit(debugger, "Jb_req", regValue, cDebugCoreStatusJb_req);
    Tha6021DebugPrintInfoBit(debugger, "Rdseg_req", regValue, cDebugCoreStatusRdseg_req);
    Tha6021DebugPrintInfoBit(debugger, "Wrseg_req", regValue, cDebugCoreStatusWrseg_req);
    Tha6021DebugPrintInfoBit(debugger, "Wrca_req", regValue, cDebugCoreStatusWrca_req);
    Tha6021DebugPrintInfoBit(debugger, "Rdca_req", regValue, cDebugCoreStatusRdca_req);
    Tha6021DebugPrintInfoBit(debugger, "Avl3_rdy", regValue, cDebugCoreStatusAvl3_rdy);
    Tha6021DebugPrintInfoBit(debugger, "Avl3_done", regValue, cDebugCoreStatusAvl3_done);
    Tha6021DebugPrintInfoBit(debugger, "Avl2_rdy", regValue, cDebugCoreStatusAvl2_rdy);
    Tha6021DebugPrintInfoBit(debugger, "Avl2_done", regValue, cDebugCoreStatusAvl2_done);
    Tha6021DebugPrintInfoBit(debugger, "Avl1_rdy", regValue, cDebugCoreStatusAvl1_rdy);
    Tha6021DebugPrintInfoBit(debugger, "Avl1_done", regValue, cDebugCoreStatusAvl1_done);
    Tha6021DebugPrintInfoBit(debugger, "Rxclslbit", regValue, cDebugCoreStatusRxclslbit);
    Tha6021DebugPrintInfoBit(debugger, "Rxclseop", regValue, cDebugCoreStatusRxclseop);
    Tha6021DebugPrintInfoBit(debugger, "Rxclssop", regValue, cDebugCoreStatusRxclssop);
    Tha6021DebugPrintInfoBit(debugger, "Rxclsvld", regValue, cDebugCoreStatusRxclsvld);
    Tha6021DebugPrintStop(debugger);
    }

void Tha60210031CdrStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugCdrSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* CDR sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "ext3vc3full", regValue, cDebugCdrExt3vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ext3vc3empty", regValue, cDebugCdrExt3vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ext3vc12full", regValue, cDebugCdrExt3vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ext3vc12empty", regValue, cDebugCdrExt3vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ext3vc11full", regValue, cDebugCdrExt3vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ext3vc11empty", regValue, cDebugCdrExt3vc11empty);
    Tha6021DebugPrintErrorBit(debugger, "ext2vc3full", regValue, cDebugCdrExt2vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ext2vc3empty", regValue, cDebugCdrExt2vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ext2vc12full", regValue, cDebugCdrExt2vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ext2vc12empty", regValue, cDebugCdrExt2vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ext2vc11full", regValue, cDebugCdrExt2vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ext2vc11empty", regValue, cDebugCdrExt2vc11empty);
    Tha6021DebugPrintErrorBit(debugger, "ext1vc3full", regValue, cDebugCdrExt1vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ext1vc3empty", regValue, cDebugCdrExt1vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ext1vc12full", regValue, cDebugCdrExt1vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ext1vc12empty", regValue, cDebugCdrExt1vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ext1vc11full", regValue, cDebugCdrExt1vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ext1vc11empty", regValue, cDebugCdrExt1vc11empty);
    Tha6021DebugPrintErrorBit(debugger, "ocdrvc3wrfifofull", regValue, cDebugCdrOcdrvc3wrfifofull);
    Tha6021DebugPrintErrorBit(debugger, "ocdrvc3rdfifoempty", regValue, cDebugCdrOcdrvc3rdfifoempty);
    Tha6021DebugPrintErrorBit(debugger, "ocdrvtwrfifofull", regValue, cDebugCdrOcdrvtwrfifofull);
    Tha6021DebugPrintErrorBit(debugger, "ocdrvtrdfifoempty", regValue, cDebugCdrOcdrvtrdfifoempty);
    Tha6021DebugPrintErrorBit(debugger, "oltvc3wrfifofull", regValue, cDebugCdrOltvc3wrfifofull);
    Tha6021DebugPrintErrorBit(debugger, "oltvc3rdfifoempty", regValue, cDebugCdrOltvc3rdfifoempty);
    Tha6021DebugPrintErrorBit(debugger, "oltde3wrfifofull", regValue, cDebugCdrOltde3wrfifofull);
    Tha6021DebugPrintErrorBit(debugger, "oltvtwrfifofull", regValue, cDebugCdrOltvtwrfifofull);
    Tha6021DebugPrintErrorBit(debugger, "oltvtrdfifoempty", regValue, cDebugCdrOltvtrdfifoempty);
    Tha6021DebugPrintErrorBit(debugger, "oltde1wrfifofull", regValue, cDebugCdrOltde1wrfifofull);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031CdrOcnStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugCdrOcnSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* CDR OCN sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "ocn4vc3full", regValue, cDebugCdrOcn4vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ocn4vc3empty", regValue, cDebugCdrOcn4vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn4vc12full", regValue, cDebugCdrOcn4vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ocn4vc12empty", regValue, cDebugCdrOcn4vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn4vc11full", regValue, cDebugCdrOcn4vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ocn4vc11empty", regValue, cDebugCdrOcn4vc11empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn3vc3full", regValue, cDebugCdrOcn3vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ocn3vc3empty", regValue, cDebugCdrOcn3vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn3vc12full", regValue, cDebugCdrOcn3vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ocn3vc12empty", regValue, cDebugCdrOcn3vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn3vc11full", regValue, cDebugCdrOcn3vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ocn3vc11empty", regValue, cDebugCdrOcn3vc11empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn2vc3full", regValue, cDebugCdrOcn2vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ocn2vc3empty", regValue, cDebugCdrOcn2vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn2vc12full", regValue, cDebugCdrOcn2vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ocn2vc12empty", regValue, cDebugCdrOcn2vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn2vc11full", regValue, cDebugCdrOcn2vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ocn2vc11empty", regValue, cDebugCdrOcn2vc11empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn1vc3full", regValue, cDebugCdrOcn1vc3full);
    Tha6021DebugPrintErrorBit(debugger, "ocn1vc3empty", regValue, cDebugCdrOcn1vc3empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn1vc12full", regValue, cDebugCdrOcn1vc12full);
    Tha6021DebugPrintErrorBit(debugger, "ocn1vc12empty", regValue, cDebugCdrOcn1vc12empty);
    Tha6021DebugPrintErrorBit(debugger, "ocn1vc11full", regValue, cDebugCdrOcn1vc11full);
    Tha6021DebugPrintErrorBit(debugger, "ocn1vc11empty", regValue, cDebugCdrOcn1vc11empty);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031QsgmiiSgmiiSticky1Debug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugQSGMII_SGMII_MAC1;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* QSGMII/SGMII MAC sticky 1", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "user_rxinf", regValue, cDebugMacUser_rxinf);
    Tha6021DebugPrintErrorBit(debugger, "txfm_irer", regValue, cDebugMacTxfm_irer);
    Tha6021DebugPrintErrorBit(debugger, "txfmmoderr", regValue, cDebugMacTxfmmoderr);
    Tha6021DebugPrintErrorBit(debugger, "txfmsoperr", regValue, cDebugMacTxfmsoperr);
    Tha6021DebugPrintErrorBit(debugger, "txfmeoperr", regValue, cDebugMacTxfmeoperr);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031QsgmiiSgmiiSticky2Debug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugQSGMII_SGMII_MAC2;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* QSGMII/SGMII MAC sticky 2", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, "txfm_iinf", regValue, cDebugTxfm_iinf);
    Tha6021DebugPrintErrorBit(debugger, "portenb", regValue, cDebugPortenb);
    Tha6021DebugPrintErrorBit(debugger, "genpauenb", regValue, cDebugGenpauenb);
    Tha6021DebugPrintErrorBit(debugger, "user_rxvld", regValue, cDebugUser_rxvld);
    Tha6021DebugPrintErrorBit(debugger, "user_txvld", regValue, cDebugUser_txvld);
    Tha6021DebugPrintErrorBit(debugger, "txfm_oreq", regValue, cDebugTxfm_oreq);
    Tha6021DebugPrintErrorBit(debugger, "txfm_ivld", regValue, cDebugTxfm_ivld);
    Tha6021DebugPrintErrorBit(debugger, "rxif_ier && rxif_idv", regValue, cDebugRxif_ierAndrxif_idv);
    Tha6021DebugPrintErrorBit(debugger, "rxif_ivld && rxif_idv", regValue, cDebugRxif_ivldAndrxif_idv);
    Tha6021DebugPrintErrorBit(debugger, "txif_oer && txif_oen", regValue, cDebugTxif_oerAndtxif_oen);
    Tha6021DebugPrintErrorBit(debugger, "txif_ovld && txif_oen", regValue, cDebugTxif_ovldAndtxif_oen);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031QsgmiiSgmiiStatusDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugQSGMII_SGMII_MAC_Status;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* QSGMII/SGMII MAC status", regAddres, regValue);
    Tha6021DebugPrintInfoBit(debugger,"user_rxvld", regValue, cDebugMacStatusUser_rxvld);
    Tha6021DebugPrintInfoBit(debugger,"user_txvld", regValue, cDebugMacStatusUser_txvld);
    Tha6021DebugPrintInfoBit(debugger,"upact", regValue, cDebugMacStatusUpact);
    Tha6021DebugPrintInfoBit(debugger,"portenb", regValue, cDebugMacStatusPortenb);
    Tha6021DebugPrintInfoBit(debugger,"genpauenb", regValue, cDebugMacStatusGenpauenb);
    Tha6021DebugPrintInfoBit(debugger,"user_txenb", regValue, cDebugMacStatusUser_txenb);
    Tha6021DebugPrintStop(debugger);
    }

void Tha60210031XfiSticky1Debug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugXfiSticky1;
    regValue  = AtHalRead(hal, regAddres);
    Tha6021DebugPrintRegName (debugger, "* XFI sticky 1", regAddres, regValue);

    Tha6021DebugPrintBitFieldVal(debugger, "mac_rxstk", regValue, cDebugXfiMac_rxstkMask, cDebugXfiMac_rxstkShift);
    Tha6021DebugPrintErrorBit(debugger, "diag_pkterr1", regValue, cDebugXfiDiag_pkterr1);
    Tha6021DebugPrintErrorBit(debugger, "diag_prbserr1", regValue, cDebugXfiDiag_prbserr1);
    Tha6021DebugPrintErrorBit(debugger, "rxgeconv_werr", regValue, cDebugXfiRxgeconv_werr);
    Tha6021DebugPrintErrorBit(debugger, "usr_rxerr", regValue, cDebugXfiUsr_rxerr);
    Tha6021DebugPrintErrorBit(debugger, "usr_rxenb", regValue, cDebugXfiUsr_rxenb);
    Tha6021DebugPrintErrorBit(debugger, "usr_rxvld", regValue, cDebugXfiUsr_rxvld);
    Tha6021DebugPrintErrorBit(debugger, "usr_rxsop", regValue, cDebugXfiUsr_rxsop);
    Tha6021DebugPrintErrorBit(debugger, "usr_rxeop", regValue, cDebugXfiUsr_rxeop);
    Tha6021DebugPrintErrorBit(debugger, "usr_txenb", regValue, cDebugXfiUsr_txenb);
    Tha6021DebugPrintErrorBit(debugger, "usr_txvld", regValue, cDebugXfiUsr_txvld);
    Tha6021DebugPrintErrorBit(debugger, "usr_txsop", regValue, cDebugXfiUsr_txsop);
    Tha6021DebugPrintErrorBit(debugger, "usr_txeop", regValue, cDebugXfiUsr_txeop);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031XfiSticky2Debug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugXfiSticky2;
    regValue  = AtHalRead(hal, regAddres);
    Tha6021DebugPrintRegName (debugger, "* XFI sticky 2", regAddres, regValue);

    Tha6021DebugPrintErrorBit(debugger, "i_rxif_sfdmis", regValue, cDebugXfiRxif_sfdmis1);
    Tha6021DebugPrintErrorBit(debugger, "i_rxif_outsel", regValue, cDebugXfiRxif_outsel1);
    Tha6021DebugPrintErrorBit(debugger, "i_rxif_over", regValue, cDebugXfiRxif_over1);
    Tha6021DebugPrintErrorBit(debugger, "i_rxif_under", regValue, cDebugXfiRxif_under1);
    Tha6021DebugPrintErrorBit(debugger, "i_rxff_rderr", regValue, cDebugXfiRxff_rderr1);
    Tha6021DebugPrintErrorBit(debugger, "i_txif_gmerr", regValue, cDebugXfiTxif_gmerr1);
    Tha6021DebugPrintErrorBit(debugger, "i_txff_wrerr", regValue, cDebugXfiTxff_wrerr1);
    Tha6021DebugPrintBitFieldVal(debugger, "i_txsh_err", regValue, cDebugXfiTxsh_err1Mask, cDebugXfiTxsh_err1Shift);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031ClockDebug(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Input clocks value:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "Input clock value mongtx_clk   "   : "    mongtx_clk",      0xF00062, 156250000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "Input clock value monocn_ocnrxclk" : "    monocn_ocnrxclk", 0xF00063, 156250000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "Input clock value moncoreclk   "   : "    moncoreclk",      0xF00064, 125000000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "Input clock value monddruser_clk2" : "    monddruser_clk2", 0xF00065, 125000000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "Input clock value monddruser_clk"  : "    monddruser_clk",  0xF00066, 125000000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "Input clock value mongbegtx_clk"   : "    mongbegtx_clk",   0xF00067, 125000000, 30);
    }

void Tha60210031DevicePllStickyDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDeviceStickyRegisters;
    regValue  = AtHalRead(hal, regAddres);
    Tha6021DebugPrintRegName (debugger, "* Device PLL sticky 1", regAddres, regValue);

    Tha6021DebugPrintErrorBit(debugger, "link_status", regValue, cDebugLink_status);
    Tha6021DebugPrintErrorBit(debugger, "gmii1_link", regValue, cDebugGmii1_link);
    Tha6021DebugPrintErrorBit(debugger, "gmii2_link", regValue, cDebugGmii2_link);
    Tha6021DebugPrintErrorBit(debugger, "gmii3_link", regValue, cDebugGmii3_link);
    Tha6021DebugPrintErrorBit(debugger, "gmii4_link", regValue, cDebugGmii4_link);
    Tha6021DebugPrintErrorBit(debugger, "ge_pll_locked", regValue, cDebugGe_pll_locked);
    Tha6021DebugPrintErrorBit(debugger, "sys_pll_locked", regValue, cDebugSys_pll_locked);
    Tha6021DebugPrintErrorBit(debugger, "sys25_pll_locked", regValue, cDebugSys25_pll_locked);
    Tha6021DebugPrintErrorBit(debugger, "xfi_pll1_locked", regValue, cDebugXfi_pll1_locked);
    Tha6021DebugPrintErrorBit(debugger, "xfi_pll2_locked", regValue, cDebugXfi_pll2_locked);
    Tha6021DebugPrintErrorBit(debugger, "c2_init_calib_complete", regValue, cDebugC2_init_calib_complete);
    Tha6021DebugPrintErrorBit(debugger, "c1_init_calib_complete", regValue, cDebugC1_init_calib_complete);
    Tha6021DebugPrintErrorBit(debugger, "c0_init_calib_complete", regValue, cDebugC0_init_calib_complete);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);

    regAddres = cDevicePllStickyRegisters;
    regValue  = AtHalRead(hal, regAddres);
    Tha6021DebugPrintRegName (debugger, "* Device PLL sticky 2", regAddres, regValue);

    Tha6021DebugPrintErrorBit(debugger, "avlmux_err", regValue, cDebugAvlmux_err);
    Tha6021DebugPrintErrorBit(debugger, "app2_rderr", regValue, cDebugApp2_rderr);
    Tha6021DebugPrintErrorBit(debugger, "app1_rderr", regValue, cDebugApp1_rderr);
    Tha6021DebugPrintErrorBit(debugger, "app0_rderr", regValue, cDebugApp0_rderr);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

void Tha60210031DevicePllStatusDebug(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);

    regAddres = cDeviceStatusRegisters;
    regValue  = AtHalRead(hal, regAddres);
    Tha6021DebugPrintRegName (debugger, "* Device PLL status", regAddres, regValue);

    Tha6021DebugPrintInfoBit(debugger, "link_status", regValue, cDebugLink_status);
    Tha6021DebugPrintInfoBit(debugger, "gmii1_link", regValue, cDebugGmii1_link);
    Tha6021DebugPrintInfoBit(debugger, "gmii2_link", regValue, cDebugGmii2_link);
    Tha6021DebugPrintInfoBit(debugger, "gmii3_link", regValue, cDebugGmii3_link);
    Tha6021DebugPrintInfoBit(debugger, "gmii4_link", regValue, cDebugGmii4_link);
    Tha6021DebugPrintInfoBit(debugger, "ge_pll_locked", regValue, cDebugGe_pll_locked);
    Tha6021DebugPrintInfoBit(debugger, "sys_pll_locked", regValue, cDebugSys_pll_locked);
    Tha6021DebugPrintInfoBit(debugger, "sys25_pll_locked", regValue, cDebugSys25_pll_locked);
    Tha6021DebugPrintInfoBit(debugger, "xfi_pll1_locked", regValue, cDebugXfi_pll1_locked);
    Tha6021DebugPrintInfoBit(debugger, "xfi_pll2_locked", regValue, cDebugXfi_pll2_locked);
    Tha6021DebugPrintInfoBit(debugger, "c2_init_calib_complete", regValue, cDebugC2_init_calib_complete);
    Tha6021DebugPrintInfoBit(debugger, "c1_init_calib_complete", regValue, cDebugC1_init_calib_complete);
    Tha6021DebugPrintInfoBit(debugger, "c0_init_calib_complete", regValue, cDebugC0_init_calib_complete);
    Tha6021DebugPrintStop(debugger);
    }

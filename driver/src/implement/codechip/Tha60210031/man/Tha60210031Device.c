/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210031Device.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Product 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/ram/AtModuleRamInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../Tha60210011/eth/Tha6021ModuleEth.h"
#include "../../../default/physical/ThaSemController.h"
#include "Tha60210031Device.h"
#include "Tha60210031DeviceReg.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../Tha60210011/man/Tha60210011InterruptControllerInternal.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../Tha60210031/clock/Tha60210031ModuleClock.h"
#include "Tha60210031DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031Device)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031DeviceMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;

/* Super implementation */
static const tAtObjectMethods          *m_AtObjectMethods  = NULL;
static const tAtDeviceMethods          *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods         *m_ThaDeviceMethods = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HalTest(AtDevice self)
    {
    AtUnused(self);

    return cAtOk;
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    if (moduleId == cAtModulePrbs)
        return 2;
    return m_ThaDeviceMethods->NumPartsOfModule(self, moduleId);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleSdh)    return (AtModule)Tha60210031ModuleSdhNew(self);
    if (moduleId  == cAtModulePdh)    return (AtModule)Tha60210031ModulePdhNew(self);
    if (moduleId  == cAtModuleEth)    return (AtModule)Tha60210031ModuleEthNew(self);
    if (moduleId  == cAtModulePw)     return (AtModule)Tha60210031ModulePwNew(self);
    if (moduleId  == cAtModuleRam)    return (AtModule)Tha60210031ModuleRamNew(self);
    if (moduleId  == cAtModulePrbs)   return (AtModule)Tha60210031ModulePrbsNew(self);
    if (moduleId  == cAtModuleBer)    return (AtModule)Tha60210031ModuleBerNew(self);
    if (moduleId  == cAtModuleSur)    return (AtModule)Tha60210031ModuleSurNew(self);
    if (moduleId  == cAtModuleClock)  return (AtModule)Tha60210031ModuleClockNew(self);

    /* Private modules */
    if (phyModule == cThaModuleOcn)   return Tha60210031ModuleOcnNew(self);
    if (phyModule == cThaModuleCdr)   return Tha60210031ModuleCdrNew(self);
    if (phyModule == cThaModuleMap)   return Tha60210031ModuleMapNew(self);
    if (phyModule == cThaModuleDemap) return Tha60210031ModuleDemapNew(self);
    if (phyModule == cThaModuleCla)   return Tha60210031ModuleClaNew(self);
    if (phyModule == cThaModulePda)   return Tha60210031ModulePdaNew(self);
    if (phyModule == cThaModulePoh)   return Tha60210031ModulePohNew(self);
    if (phyModule == cThaModulePwe)   return Tha60210031ModulePweNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModulePrbs,
                                                 cAtModuleBer,
                                                 cAtModuleSur,
                                                 cAtModuleClock};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    switch (moduleValue)
        {
        case cAtModuleBer :  return cAtTrue;
        case cAtModulePrbs:  return cAtTrue;
        case cAtModuleSur:   return cAtTrue;
        case cAtModuleClock: return cAtTrue;

        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static void StickyDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static void StatusDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static void RamMarginDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static void OcnClockStatusDisplay(Tha60150011Device self)
    {
    AtUnused(self);
    }

static uint32 XGMIIClock156_25MhzValueStatusRegister(Tha60150011Device self, uint8 clockId)
    {
    AtUnused(self);
    AtUnused(clockId);
    return 0xF00063;
    }

static eBool OcnClockIsGood(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
    /* This product does not have SSKey */
    AtUnused(self);
    return cAtFalse;
    }

static eBool NewStartupSequenceIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PllClkSysStatReg(ThaDevice self)
    {
    AtUnused(self);
    return cDeviceStickyRegisters;
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    if(ThaDeviceIsEp(self))
        return cDeviceKintexPllStickyMask;

    return cDevicePllStickyMask;
    }

static eBool PllIsLocked(AtDevice self)
    {
    /* This device only has one core */
    return ThaIpCorePllIsLocked(AtDeviceIpCoreGet(self, 0));
    }

static eBool ShouldShowCommonDebugInfo(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PlatformCanRunDiagnostic(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Setup(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtDeviceMethods->Setup(self);

    /* TODO: Remove this after diagnostic finish */
    ret |= AtDeviceDiagnosticCheckEnable(self, PlatformCanRunDiagnostic(self));

    return ret;
    }

static eBool DdrCalibStickyIsGood(AtDevice self)
    {
    uint32 regVal;
    uint8 numDdrs, ddr_i;
    uint32 allDdrsMask = cAllDdrsCalibStatusMask;
    AtModuleRam ramModule;
    static const uint32 cTimeoutMs = 5000;
    eBool isGood = ThaDeviceStickyIsGood(self, cDeviceStickyRegisters, allDdrsMask, cTimeoutMs);

    if (AtDeviceIsSimulated(self))
        return isGood;

    ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    numDdrs = AtModuleRamNumDdrGet(ramModule);
    regVal = AtHalRead(AtDeviceIpCoreHalGet(self,0), cDeviceStickyRegisters);

    if (AtDeviceIsSimulated(self))
        return isGood;

    for (ddr_i = 0; ddr_i < numDdrs; ddr_i++)
        {
        if (regVal & cDdrCalibStatusMask(ddr_i))
            AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#%u calib fail\r\n", AtFunction, ddr_i + 1);
        else
            AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: DDR#%u calib done\r\n", AtFunction, ddr_i + 1);
        }

    return isGood;
    }

static uint32 DdrCurrentStatusRegister(Tha60210031Device self)
    {
    AtUnused(self);
    return cDeviceStatusRegisters;
    }

static eBool DdrCalibCurrentStatusIsDone(Tha60210031Device self, uint32 regVal, uint32 mask)
    {
    AtUnused(self);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eBool DdrCalibStatusIsGood(AtDevice self)
    {
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    uint8 ddr_i, numDdrs = AtModuleRamNumDdrGet(ramModule);
    uint32 regAddr = mMethodsGet(mThis(self))->DdrCurrentStatusRegister(mThis(self));
    uint32 regVal = AtHalRead(AtDeviceIpCoreHalGet(self, 0), regAddr);
    eBool isGood = cAtTrue;

    if (AtDeviceIsSimulated(self))
        return isGood;

    for (ddr_i = 0; ddr_i < numDdrs; ddr_i++)
        {
        if (mMethodsGet(mThis(self))->DdrCalibCurrentStatusIsDone(mThis(self), regVal, cDdrCalibStatusMask(ddr_i)))
            AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: DDR#%u calib done\r\n", AtFunction, ddr_i + 1);
        else
            {
            AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#%u calib fail\r\n", AtFunction, ddr_i + 1);
            isGood = cAtFalse;
            }
        }

    return isGood;
    }

static eBool EpDdrCalibStickyIsGood(AtDevice self)
    {
    uint32 regVal;
    uint8 numDdrs, ddr_i;
    uint32 allDdrsMask = cKintexAllDdrsCalibStatusMask;
    AtModuleRam ramModule;
    static const uint32 cTimeoutMs = 5000;
    eBool isGood = ThaDeviceStickyIsGood(self, cDeviceStickyRegisters, allDdrsMask, cTimeoutMs);

    ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    numDdrs = AtModuleRamNumDdrGet(ramModule);
    regVal = AtHalRead(AtDeviceIpCoreHalGet(self,0), cDeviceStickyRegisters);
    for (ddr_i = 0; ddr_i < numDdrs; ddr_i++)
        {
        if (ddr_i == 2) continue; /* This DDR is not used */
        if (regVal & cKintexDdrCalibStatusMask(ddr_i))
            AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#%u calib fail\r\n", AtFunction, ddr_i + 1);
        else
            AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: DDR#%u calib done\r\n", AtFunction, ddr_i + 1);
        }

    return isGood;
    }

static eBool EpDdrCalibStatusIsGood(AtDevice self)
    {
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    uint8 ddr_i, numDdrs = AtModuleRamNumDdrGet(ramModule);
    uint32 regVal = AtHalRead(AtDeviceIpCoreHalGet(self, 0), cDeviceStatusRegisters);
    eBool isGood = cAtTrue;

    for (ddr_i = 0; ddr_i < numDdrs; ddr_i++)
        {
        if (ddr_i == 2) continue; /* This DDR is not used */
        if (regVal & cKintexDdrCalibStatusMask(ddr_i))
            AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: DDR#%u calib done\r\n", AtFunction, ddr_i + 1);
        else
            {
            AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#%u calib fail\r\n", AtFunction, ddr_i + 1);
            isGood = cAtFalse;
            }
        }

    return isGood;
    }

static eBool EpDdrCalibCheck(AtDevice self)
    {
    if (!EpDdrCalibStickyIsGood(self))
        return cAtFalse;
    if (!EpDdrCalibStatusIsGood(self))
        return cAtFalse;
    return cAtTrue;
    }

static eBool DdrCalibIsGood(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return EpDdrCalibCheck(self);

    if (!DdrCalibStickyIsGood(self))
        return cAtFalse;
    if (!DdrCalibStatusIsGood(self))
        return cAtFalse;
    return cAtTrue;
    }

static uint8 NumCoreClocks(Tha60150011Device self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 CoreClockValueInHz(Tha60150011Device self, uint8 core)
    {
    AtUnused(self);
    AtUnused(core);
    return 155520000;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60210031InterruptControllerNew(core);
    }

static eBool EthIntfClockIsGood(AtDevice self)
    {
    uint32 regVal;

    /* Do not need to check Ethernet interface clock for
     * EP here, it will be checked in general PLL check function */
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtTrue;

    if (ThaDeviceStickyIsGood(self, cDeviceStickyRegisters, cXfiActivePllLockedMask | cXfiStandbyPllLockedMask, 5000))
        {
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All XFIs done\r\n", AtFunction);
        return cAtTrue;
        }

    regVal = AtHalRead(AtDeviceIpCoreHalGet(self, 0), cDeviceStickyRegisters);

    if (regVal & cXfiActivePllLockedMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XFI#1 PCS not lock\r\n", AtFunction);

    if (regVal & cXfiStandbyPllLockedMask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XFI#2 PCS not lock\r\n", AtFunction);

    return cAtFalse;
    }

static eAtRet HwDiagnosticModeEnable(Tha60210031Device self, eBool enable)
    {
    AtModule ethModule = AtDeviceModuleGet((AtDevice)self, cAtModuleEth);
    return Tha6021DsxModuleEthHwDiagnosticModeEnable(ethModule, enable);
    }

static eAtRet DiagnosticModeEnable(AtDevice self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtDeviceMethods->DiagnosticModeEnable(self, enable);
    ret |= mMethodsGet(mThis(self))->HwDiagnosticModeEnable(mThis(self), enable);

    return ret;
    }

static void RamErrorGeneratorHwFlush(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtHalWrite(hal, 0x000140, 0x0);
    AtHalWrite(hal, 0x000148, 0x0);
    AtHalWrite(hal, 0x000150, 0x0);
    }

void Tha60210031DeviceHwFlushPdhSlice0(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x790002, 0x790003, 0x0);
    ThaDeviceMemoryFlush(self, 0x754000, 0x7543ff, 0);
    ThaDeviceMemoryFlush(self, 0x794000, 0x7943ff, 0);
    ThaDeviceMemoryFlush(self, 0x794800, 0x794bff, 0);
    ThaDeviceMemoryFlush(self, 0x740000, 0x74001f, 0);
    ThaDeviceMemoryFlush(self, 0x780000, 0x78001f, 0);
    ThaDeviceMemoryFlush(self, 0x741000, 0x7410ff, 0);
    ThaDeviceMemoryFlush(self, 0x741300, 0x74131f, 0);
    ThaDeviceMemoryFlush(self, 0x781000, 0x78101f, 0);
    ThaDeviceMemoryFlush(self, 0x724000, 0x7243ff, 0);
    ThaDeviceMemoryFlush(self, 0x776000, 0x7763ff, 0);
    ThaDeviceMemoryFlush(self, 0x730000, 0x73001f, 0);
    ThaDeviceMemoryFlush(self, 0x730020, 0x73003f, 0);
    /* DS3/E3 interrupt mask */
    ThaDeviceMemoryFlush(self, 0x740580, 0x740597, 0);
    /* DS1/E1 interrupt mask */
    ThaDeviceMemoryFlush(self, 0x755000, 0x7552FF, 0);
    }

static void HwFlushPdhSlice1(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x890002, 0x890003, 0x0);
    ThaDeviceMemoryFlush(self, 0x854000, 0x8543ff, 0);
    ThaDeviceMemoryFlush(self, 0x894000, 0x8943ff, 0);
    ThaDeviceMemoryFlush(self, 0x894800, 0x894bff, 0);
    ThaDeviceMemoryFlush(self, 0x840000, 0x84001f, 0);
    ThaDeviceMemoryFlush(self, 0x880000, 0x88001f, 0);
    ThaDeviceMemoryFlush(self, 0x841000, 0x8410ff, 0);
    ThaDeviceMemoryFlush(self, 0x841300, 0x84131f, 0);
    ThaDeviceMemoryFlush(self, 0x881000, 0x88101f, 0);
    ThaDeviceMemoryFlush(self, 0x824000, 0x8243ff, 0);
    ThaDeviceMemoryFlush(self, 0x876000, 0x8763ff, 0);
    ThaDeviceMemoryFlush(self, 0x830000, 0x83001f, 0);
    ThaDeviceMemoryFlush(self, 0x830020, 0x83003f, 0);
    /* DS3/E3 interrupt mask */
    ThaDeviceMemoryFlush(self, 0x840580, 0x840597, 0);
    /* DS1/E1 interrupt mask */
    ThaDeviceMemoryFlush(self, 0x855000, 0x8552FF, 0);
    }

static void HwFlushSsm(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x7f0000, 0x7f003f, 0);
    ThaDeviceMemoryFlush(self, 0x7fC000, 0x7fC03f, 0);

    ThaDeviceMemoryFlush(self, 0x8f0000, 0x8f003f, 0);
    ThaDeviceMemoryFlush(self, 0x8fC000, 0x8fC03f, 0);
    }

void Tha60210031DeviceHwFlushMap0(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x1a0000, 0x1a0FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1a1000, 0x1a1FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1a2000, 0x1a2FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1a3000, 0x1a3FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1b0000, 0x1b03FF, 0);
    ThaDeviceMemoryFlush(self, 0x1b4000, 0x1b4FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1b5000, 0x1b5FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1b6000, 0x1b6FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1b7000, 0x1b7FFF, 0);
    }

static void HwFlushMap1(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x1c0000, 0x1c0FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1c1000, 0x1c1FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1c2000, 0x1c2FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1c3000, 0x1c3FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1d0000, 0x1d03FF, 0);
    ThaDeviceMemoryFlush(self, 0x1d4000, 0x1d4FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1d5000, 0x1d5FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1d6000, 0x1d6FFF, 0);
    ThaDeviceMemoryFlush(self, 0x1d7000, 0x1d7FFF, 0);
    }

void Tha60210031DeviceHwFlushCdr0(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x280800, 0x28081F, 0);
    ThaDeviceMemoryFlush(self, 0x280c00, 0x280fff, 0);
    ThaDeviceMemoryFlush(self, 0x2a0800, 0x2a0bff, 0);
    ThaDeviceMemoryFlush(self, 0x2a5000, 0x2a53ff, 0);
    }

static void HwFlushCdr1(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x2e5000, 0x2e53ff, 0);
    ThaDeviceMemoryFlush(self, 0x2c0800, 0x2c081F, 0);
    ThaDeviceMemoryFlush(self, 0x2c0c00, 0x2c0fff, 0);
    ThaDeviceMemoryFlush(self, 0x2e0800, 0x2e0bff, 0);
    }

static void HwFlushCdr(ThaDevice self)
    {
    Tha60210031DeviceHwFlushCdr0(self);
    HwFlushCdr1(self);
    }

static void HwFlushBert(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x1B8500,  0x1B850F, 0);
    ThaDeviceMemoryFlush(self, 0x1B8320,  0x1B832F, 0);
    ThaDeviceMemoryFlush(self, 0x1B8360,  0x1B836F, 0);
    ThaDeviceMemoryFlush(self, 0x1B8510,  0x1B851F, 0);
    ThaDeviceMemoryFlush(self, 0x1A4200,  0x1A420F, 0);
    }

static void HwFlushPla(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x202000, 0x2025FF, 0);
    }

static void HwFlushPwe(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x308000, 0x30D3FF, 0);
    ThaDeviceMemoryFlush(self, 0x301000, 0x3015FF, 0);
    ThaDeviceMemoryFlush(self, 0x303000, 0x3035FF, 0);
    ThaDeviceMemoryFlush(self, 0x321000, 0x3215FF, 0);
    ThaDeviceMemoryFlush(self, 0x325000, 0x3255FF, 0);
    ThaDeviceMemoryFlush(self, 0x305000, 0x3050FF, 0);
    ThaDeviceMemoryFlush(self, 0x306000, 0x3065FF, 0);
    ThaDeviceMemoryFlush(self, 0x307000, 0x3073FF, 0);
    ThaDeviceMemoryFlush(self, 0x304000, 0x3043FF, 0);
    }

static void HwFlushCla0(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0x40C000, 0x40C5FF);
    ThaDeviceMemoryFlush(self, 0x40A000, 0x40B7FF, 0);
    ThaDeviceMemoryLongFlush(self, 0x40E000, 0x40EFFF);
    }

static void HwFlushCla1(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0x40F000, 0x40FFFF);
    ThaDeviceMemoryFlush(self, 0x403000, 0x4035FF, 0);
    ThaDeviceMemoryLongFlush(self, 0x40D000, 0x40D5FF);
    ThaDeviceMemoryFlush(self, 0x409000, 0x4093FF, 0);
    }

static void HwFlushPda(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0x243000, 0x2435ff);
    ThaDeviceMemoryFlush(self, 0x244000, 0x2445ff, 0);
    ThaDeviceMemoryFlush(self, 0x245000, 0x2455ff, 0);
    ThaDeviceMemoryFlush(self, 0x247000, 0x2475ff, 0);
    ThaDeviceMemoryFlush(self, 0x24f000, 0x24f5ff, 0);
    }

static void HwFlushPrm(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x601000, 0x60153F, 0);
    ThaDeviceMemoryFlush(self, 0x601800, 0x601D3F, 0);
    ThaDeviceMemoryFlush(self, 0x608000, 0x60853F, 0);
    }

static void HwFlushMdl(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x600420, 0x600437, 0);
    ThaDeviceMemoryFlush(self, 0x600440, 0x600457, 0);
    ThaDeviceMemoryFlush(self, 0x600460, 0x600477, 0);

    ThaDeviceMemoryFlush(self, 0x600C20, 0x600C37, 0);
    ThaDeviceMemoryFlush(self, 0x600C40, 0x600C57, 0);
    ThaDeviceMemoryFlush(self, 0x600C60, 0x600C77, 0);

    ThaDeviceMemoryFlush(self, 0x60A000, 0x60A02F, 0);

    ThaDeviceMemoryFlush(self, 0x60A045, 0x60A045, 0xFFFFFFFF);
    ThaDeviceMemoryFlush(self, 0x60A046, 0x60A046, 0xFFFF);
    }

static void HwFlushOcn(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0xA60800, 0xA60BFF, 0);
    ThaDeviceMemoryFlush(self, 0xA64800, 0xA64BFF, 0);
    ThaDeviceMemoryFlush(self, 0xA23000, 0xA23017, 0);
    ThaDeviceMemoryFlush(self, 0xA23200, 0xA23217, 0);
    /* EC1 interrupt mask */
    ThaDeviceMemoryFlush(self, 0xA24200, 0xA2422F, 0);
    }

static void HwFlushBer(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0xB62000,  0xB63FFF);
    ThaDeviceMemoryLongFlush(self, 0xB60400,  0xB607FF);
    }

static void HwFlushPoh(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0xB40400,  0xB407FF, 0);
    ThaDeviceMemoryFlush(self, 0xB44000,  0xB47FFF, 0);
    ThaDeviceMemoryFlush(self, 0xB28000,  0xB29FFF, 0);
    ThaDeviceMemoryFlush(self, 0xB2A000,  0xB2BFFF, 0);
    /* HO interrupt mask */
    ThaDeviceMemoryFlush(self, 0xBD0000,  0xBD0017, 0);
    ThaDeviceMemoryFlush(self, 0xBD0080,  0xBD0097, 0);
    /* LO interrupt mask */
    ThaDeviceMemoryFlush(self, 0xBE0000,  0xBE02FF, 0);
    ThaDeviceMemoryFlush(self, 0xBE1000,  0xBE12FF, 0);
    }

static void HwFlushRamErrorGenerator(ThaDevice self)
    {
    if (ThaDeviceErrorGeneratorIsSupported(self))
        RamErrorGeneratorHwFlush((AtDevice)self);
    }

static void HwFlushPmc(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x540000, 0x540540, 0);
    }

static void HwFlushPmFm0(ThaDevice self)
    {
    /*FMPM FM EC1 Interrupt MASK Per STS1*/
    ThaDeviceMemoryFlush(self, 0x918110, 0x91811F, 0);
    ThaDeviceMemoryFlush(self, 0x91B800, 0x91B817, 0);
    ThaDeviceMemoryFlush(self, 0x91B820, 0x91B837, 0);

    /*FMPM FM STS24 Interrupt MASK Per STS1*/
    ThaDeviceMemoryFlush(self, 0x918090, 0x91809F, 0);
    ThaDeviceMemoryFlush(self, 0x91A200, 0x91A217, 0);
    ThaDeviceMemoryFlush(self, 0x91A220, 0x91A237, 0);

    /*FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3 */
    ThaDeviceMemoryFlush(self, 0x9180B0, 0x9180BF, 0);
    ThaDeviceMemoryFlush(self, 0x91A800, 0x91A817, 0);
    ThaDeviceMemoryFlush(self, 0x91A820, 0x91A837, 0);

    /*FMPM FM VTTU Interrupt MASK Per VTTU*/
    ThaDeviceMemoryFlush(self, 0x91AE00, 0x91AFFF, 0);
    ThaDeviceMemoryFlush(self, 0x928000, 0x9282FF, 0);
    ThaDeviceMemoryFlush(self, 0x928400, 0x9286FF, 0);
    }

static void HwFlushPmFm1(ThaDevice self)
    {
    /*FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1*/
    ThaDeviceMemoryFlush(self, 0x91B200, 0x91B3FF, 0);
    ThaDeviceMemoryFlush(self, 0x934000, 0x9342FF, 0);
    ThaDeviceMemoryFlush(self, 0x934400, 0x9346FF, 0);

    /*FMPM FM PW Framer Interrupt MASK Per PW*/
    ThaDeviceMemoryFlush(self, 0x91B500, 0x91B5FF, 0);
    ThaDeviceMemoryFlush(self, 0x91E000, 0x91E53F, 0);

    /*FMPM PW Def Framer Interrupt MASK Per PW*/
    ThaDeviceMemoryFlush(self, 0x99B500, 0x99B5FF, 0);
    }

static void HwFlush(Tha60150011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    mMethodsGet(device)->HwFlushAtt(device);
    mMethodsGet(mThis(self))->PdhHwFlush(mThis(self));
    HwFlushSsm(device);
    mMethodsGet(mThis(self))->MapHwFlush(mThis(self));
    mMethodsGet(mThis(self))->CdrHwFlush(mThis(self));
    HwFlushBert(device);
    HwFlushPla(device);

    mMethodsGet(mThis(self))->PweHwFlush(mThis(self));
    HwFlushCla0(device);
    HwFlushCla1(device);
    HwFlushPda(device);
    HwFlushPrm(device);
    HwFlushMdl(device);
    HwFlushOcn(device);
    HwFlushBer(device);
    HwFlushPoh(device);
    HwFlushRamErrorGenerator(device);
    HwFlushPmc(device);
    HwFlushPmFm0(device);
    HwFlushPmFm1(device);
    }

static eBool AddressNeedToCheck(uint32 address)
    {
    if ((address >= 0x1A0000) && (address <= 0x1A3FFF))
        return cAtTrue;

    if ((address >= 0x1C0000) && (address <= 0x1C3FFF))
        return cAtTrue;

    if ((address >= 0x1B4000) && (address <= 0x1B7FFF))
        return cAtTrue;

    if ((address >= 0x1D4000) && (address <= 0x1D7FFF))
        return cAtTrue;

    if ((address >= 0x308000) && (address <= 0x30D3FF))
        return cAtTrue;

    if ((address >= 0x305000) && (address <= 0x3050FF))
        return cAtTrue;

    if ((address >= 0x44D000) && (address <= 0x44D53F))
        return cAtTrue;

    return cAtFalse;
    }

static void ReWriteIfFail(AtDevice self, AtHal hal, uint32 address, uint32 value)
    {
    static const uint8 cMaxTryTime = 10;
    uint8 tryTime = 0;

    while (tryTime < cMaxTryTime)
        {
        uint32 hwValue = AtHalRead(hal, address);
        if (hwValue == value)
            return;

        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation,
                    "Address 0x%08X: wrote 0x%08X but read back 0x%08X\r\n", address, value, hwValue);
        AtHalWrite(hal, address, value);
        mThis(self)->writeFailCount++;
        tryTime++;
        }

    AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "Rewrite for address 0x%08X timeout\r\n", address);
    }

static void DidWriteRegister(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData)
    {
    AtHal hal = (AtHal)userData;

    if (AtDeviceInAccessible(self))
        return;

    AtUnused(coreId);
    if (!AddressNeedToCheck(address))
        return;

    ReWriteIfFail(self, hal, address, value);
    }

static tAtDeviceListener *AccessListener(void)
    {
    static tAtDeviceListener listener;
    static tAtDeviceListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidWriteRegister = DidWriteRegister;
    pListener = &listener;

    return pListener;
    }

static AtHal HalOfCore(AtDevice self)
    {
    return AtIpCoreHalGet(AtDeviceIpCoreGet(self, 0));
    }

static eAtRet ReWriteHandlerInstall(Tha60210031Device self)
    {
    return AtDeviceListenerAdd((AtDevice)self, AccessListener(), (void*)HalOfCore((AtDevice)self));
    }

static eAtRet ListenerInstall(Tha60210031Device self)
    {
    return ReWriteHandlerInstall(self);
    }

static eAtRet Init(AtDevice self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    eAtRet ret;

    ret = mMethodsGet(mThis(self))->ListenerInstall(mThis(self));
    if ((ret != cAtOk) && (ret != cAtErrorDuplicatedEntries))
        return ret;

    ret = m_AtDeviceMethods->Init(self);

    /* Activate ports as default */
    if (ret == cAtOk)
        ret = ThaModuleEthAllPortMacsActivate(ethModule, cAtTrue);

    return AtDeviceAllModulesHwResourceCheck(self);
    }

static eBool ShouldEnforceEpLogic(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
    if (AtDeviceAllFeaturesAvailableInSimulation((AtDevice)self))
        return 0;
        
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x15, 0x10, 0x12, 0x00);
    }

static uint32 ClockPcieSwingRangeInPpm(Tha60150011Device self)
    {
    AtUnused(self);
    return 100;
    }

static void ClockDebug(ThaDevice self)
    {
    Tha60210031ClockDebug((AtDevice)self);
    }

static void EthStickyDebug(Tha60210031Device self)
    {
    AtDevice device = (AtDevice)self;

    Tha60210031QsgmiiSgmiiSticky1Debug(device);
    Tha60210031QsgmiiSgmiiSticky2Debug(device);
    Tha60210031QsgmiiSgmiiStatusDebug(device);
    Tha60210031XfiSticky1Debug(device);
    Tha60210031XfiSticky2Debug(device);
    }

static void Debug(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    ThaDeviceClockDebug((ThaDevice)self);

    mMethodsGet(mThis(self))->PllStickyDebug(mThis(self));
    mMethodsGet(mThis(self))->PllStatusDebug(mThis(self));
    if (ThaDeviceStickyDebugIsSupported((ThaDevice)self))
        {
        Tha60210031CounterStickyDebug(self);
        Tha60210031PweStickyDebug(self);
        Tha60210031PdaStickyDebug(self);
        Tha60210031EthStickyDebug(self);
        }
    mMethodsGet(mThis(self))->CoreStickyDebug(mThis(self));
    mMethodsGet(mThis(self))->CoreStatusDebug(mThis(self));
    Tha60210031CdrStickyDebug(self);
    Tha60210031CdrOcnStickyDebug(self);

    mMethodsGet(mThis(self))->EthStickyDebug(mThis(self));

    if (debugger)
        {
        uint32 bufferSize;
        char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
        AtDebuggerEntryAdd(debugger, AtDebugEntryStringFormatNew(mThis(self)->writeFailCount ? cSevWarning : cSevNormal,
                           buffer, bufferSize, "Write fail times", "%u", mThis(self)->writeFailCount));
        return;
        }

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Write fail times: ");
    AtPrintc((mThis(self)->writeFailCount == 0) ? cSevNormal : cSevWarning, "%u\r\n", mThis(self)->writeFailCount);
    AtPrintc(cSevNormal, "\r\n");
    }

static eBool UartIsSupported(AtDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(self);
    uint32 startVersionSupportUart = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4624);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startVersionSupportUart)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    return UartIsSupported((AtDevice)self);
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    return Tha60210031SemControllerNew(self, semId);
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtTrue;

    if (UartIsSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ThermalSensorIsSupported(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(self) >= Tha60210031StartVersionSupportThermalSensor(self))
        return cAtTrue;

    return cAtFalse;
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60210031VersionReaderNew((AtDevice)self);
    }

static eBool ShouldDisableHwAccessBeforeDeleting(AtDevice self)
    {
    return Tha6021ShouldDisableHwAccessBeforeDeleting(self);
    }

static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    uint32 startVersionSupportErrorGenerator = ThaVersionReaderHardwareBuiltNumberBuild(0x4, 0x5, 0x0);
    return (AtDeviceBuiltNumber((AtDevice)self) >= startVersionSupportErrorGenerator) ? cAtTrue : cAtFalse;
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return Tha60210031DeviceAsyncInit(self);
    }

static uint32 StartVersionSwHandleQsgmiiLanes(Tha60210031Device self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x15, 0x10, 0x16, 0x00);
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)self);
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4627);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startSupportedVersion)
        return cAtTrue;

    return cAtFalse;
    }

static void PllStickyDebug(Tha60210031Device self)
    {
    Tha60210031DevicePllStickyDebug((AtDevice)self);
    }

static void PllStatusDebug(Tha60210031Device self)
    {
    Tha60210031DevicePllStatusDebug((AtDevice)self);
    }

static void CoreStickyDebug(Tha60210031Device self)
    {
    Tha60210031CoreStickyDebug((AtDevice)self);
    }

static void CoreStatusDebug(Tha60210031Device self)
    {
    Tha60210031CoreStatusDebug((AtDevice)self);
    }

static void PdhHwFlush(Tha60210031Device self)
    {
    Tha60210031DeviceHwFlushPdhSlice0((ThaDevice)self);
    HwFlushPdhSlice1((ThaDevice)self);
    }

static void MapHwFlush(Tha60210031Device self)
    {
    Tha60210031DeviceHwFlushMap0((ThaDevice)self);
    HwFlushMap1((ThaDevice)self);
    }

static void PmFmHwFlush(Tha60210031Device self)
    {
    HwFlushPmFm0((ThaDevice)self);
    HwFlushPmFm1((ThaDevice)self);
    }

static void CdrHwFlush(Tha60210031Device self)
    {
    HwFlushCdr((ThaDevice)self);
    }

static void PweHwFlush(Tha60210031Device self)
    {
    HwFlushPwe((ThaDevice)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(writeFailCount);
    mEncodeNone(asyncHwFlushState);
    mEncodeNone(asyncInitState);
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldOpenFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static eBool Ec1LoopbackIsSupported(Tha60210031Device self)
    {
    return ShouldOpenFeatureFromVersion((AtDevice)self, 0x4, 0x8, 0x4821);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, Setup);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeEnable);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, ShouldDisableHwAccessBeforeDeleting);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckHwResource);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatReg);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        mMethodOverride(m_ThaDeviceOverride, ShouldEnforceEpLogic);
        mMethodOverride(m_ThaDeviceOverride, StartVersionSupportSurveillance);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaDeviceOverride, ClockStateValueV2IsSupported);
        mMethodOverride(m_ThaDeviceOverride, ClockDebug);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, m_Tha60150011DeviceMethods, sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, HalTest);
        mMethodOverride(m_Tha60150011DeviceOverride, StickyDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, StatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, RamMarginDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, OcnClockStatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, XGMIIClock156_25MhzValueStatusRegister);
        mMethodOverride(m_Tha60150011DeviceOverride, OcnClockIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, NewStartupSequenceIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, ShouldShowCommonDebugInfo);
        mMethodOverride(m_Tha60150011DeviceOverride, DdrCalibIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, NumCoreClocks);
        mMethodOverride(m_Tha60150011DeviceOverride, CoreClockValueInHz);
        mMethodOverride(m_Tha60150011DeviceOverride, PllIsLocked);
        mMethodOverride(m_Tha60150011DeviceOverride, EthIntfClockIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, ClockPcieSwingRangeInPpm);
        mMethodOverride(m_Tha60150011DeviceOverride, SemUartIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    }

static void MethodsInit(Tha60210031Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionSwHandleQsgmiiLanes);
        mMethodOverride(m_methods, HwDiagnosticModeEnable);
        mMethodOverride(m_methods, DdrCurrentStatusRegister);
        mMethodOverride(m_methods, EthStickyDebug);
        mMethodOverride(m_methods, DdrCalibCurrentStatusIsDone);
        mMethodOverride(m_methods, PllStickyDebug);
        mMethodOverride(m_methods, PllStatusDebug);
        mMethodOverride(m_methods, CoreStickyDebug);
        mMethodOverride(m_methods, CoreStatusDebug);
        mMethodOverride(m_methods, ListenerInstall);
        mMethodOverride(m_methods, PdhHwFlush);
        mMethodOverride(m_methods, MapHwFlush);
        mMethodOverride(m_methods, PmFmHwFlush);
        mMethodOverride(m_methods, CdrHwFlush);
        mMethodOverride(m_methods, Ec1LoopbackIsSupported);
        mMethodOverride(m_methods, PweHwFlush);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031Device);
    }

AtDevice Tha60210031DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60210031DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031DeviceObjectInit(newDevice, driver, productCode);
    }

uint32 Tha60210031DeviceStartVersionSupportMbitAlarm(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x15, 0x30);
    }

uint32 Tha60210031DeviceStartVersionSupportPwDuplicatedPacket(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x15, 0x30);
    }

uint32 Tha60210031DeviceStartVersionSupportEthSerdesInterrupt(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x18, 0x28);
    }

uint32 Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSwHandleQsgmiiLanes(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60210031DeviceStartVersionSupportHspw(AtDevice self)
    {
    return Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(self);
    }

eBool Tha60210031ShouldDisableEc1Feature(AtDevice device)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 builtNumberShouldNotControlExt1 = ThaVersionReaderHardwareBuiltNumberBuild(0x3, 0x6, 0x11);

    /* Currently, only this specific FPGA version does not support EC1 */
    if ((ThaVersionReaderHardwareBuiltNumber(versionReader) == builtNumberShouldNotControlExt1) &&
        (ThaVersionReaderReleasedDate(versionReader) == ThaVersionReaderReleasedDateBuild(0x15, 0x11, 0x14)))
        return cAtTrue;

    return cAtFalse;
    }

uint32 Tha60210031DeviceStartVersionSupportFourKCell(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x11, 0x16, 0x37);
    }

uint32 Tha60210031StartVersionSupportThermalSensor(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x12, 0x09, 0x41);
    }

eAtRet Tha60210031DeviceSuperAsyncInit(AtDevice self)
    {
    return m_AtDeviceMethods->AsyncInit(self);
    }

tHwAsynFlushFunc *Tha60210031DeviceFlushFunctions(uint32 *numFunctions)
    {
    static tHwAsynFlushFunc funcList[] = {Tha60210031DeviceHwFlushPdhSlice0, HwFlushPdhSlice1,
                                          HwFlushSsm, Tha60210031DeviceHwFlushMap0,
                                          HwFlushMap1, HwFlushCdr,
                                          HwFlushBert, HwFlushPla,
                                          HwFlushPwe, HwFlushCla0, HwFlushCla1,
                                          HwFlushPda, HwFlushPrm,
                                          HwFlushMdl, HwFlushOcn,
                                          HwFlushBer, HwFlushPoh,
                                          HwFlushRamErrorGenerator,
                                          HwFlushPmc};

    *numFunctions = mCount(funcList);
    return funcList;
    }

eBool Tha60210031DeviceDdrCalibCurrentStatusIsDone(Tha60210031Device self, uint32 regVal, uint32 mask)
    {
    return DdrCalibCurrentStatusIsDone(self, regVal, mask);
    }

eAtRet Tha60210031DeviceReWriteHandlerInstall(AtDevice self)
    {
    if (self)
        return ReWriteHandlerInstall(mThis(self));
    return cAtErrorNullPointer;
    }

void Tha60210031DeviceHwFlushBert(ThaDevice self)
    {
    HwFlushBert(self);
    }

void Tha60210031DeviceHwFlushPla(ThaDevice self)
    {
    HwFlushPla(self);
    }

void Tha60210031DeviceHwFlushPwe(ThaDevice self)
    {
    HwFlushPwe(self);
    }

void Tha60210031DeviceHwFlushPrm(ThaDevice self)
    {
    HwFlushPrm(self);
    }

void Tha60210031DeviceHwFlushMdl(ThaDevice self)
    {
    HwFlushMdl(self);
    }

void Tha60210031DeviceHwFlushRamErrorGenerator(ThaDevice self)
    {
    HwFlushRamErrorGenerator(self);
    }

eBool Tha60210031DeviceEc1LoopbackIsSupported(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->Ec1LoopbackIsSupported(mThis(self));
    return cAtFalse;
    }

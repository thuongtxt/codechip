/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210031VersionReader.c
 *
 * Created Date: Dec 3, 2015
 *
 * Description : Version reader of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210031VersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011VersionReaderMethods m_Tha60210011VersionReaderOverride;
static tThaVersionReaderMethods         m_ThaVersionReaderOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x15120236;
    }

static uint32 StartBuiltNumberIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x3619;
    }

static char *BuilNumberStringMake(ThaVersionReader self, uint8 firstIndex, uint8 secondIndex, uint8 thirdIndex)
    {
    static char buf[16];
    uint8 fourthIndex, fifthIndex;
    AtUnused(self);
    fourthIndex = (thirdIndex >> 4) & cBit3_0;
    fifthIndex = (thirdIndex  & cBit3_0);

    AtSprintf(buf, "%X.%X.%X.%X", firstIndex, secondIndex, fourthIndex, fifthIndex);
    return buf;
    }

static uint16 LongRegisterVersionRead(ThaVersionReader self, uint32 *buffer, uint16 bufferLen)
    {
    static const uint32 cVersionRegister = 0;
    AtDevice device = ThaVersionReaderDeviceGet(self);
    AtLongRegisterAccess glbLongAccess = AtDeviceGlobalLongRegisterAccess(device);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);

    if (hal == NULL)
        return 0;

    return AtLongRegisterAccessRead(glbLongAccess, hal, cVersionRegister, buffer, bufferLen);
    }

static void VersionAndBuiltNumberGet(ThaVersionReader self, uint32 *version, uint32 *built)
    {
    uint32 longRegVal[cThaLongRegMaxSize];

    *version = 0;
    *built = 0;
    AtOsalMemInit(longRegVal, 0, cThaLongRegMaxSize);
    if (LongRegisterVersionRead(self, longRegVal, cThaLongRegMaxSize))
        {
        *version = longRegVal[1];
        *built   = longRegVal[2];
        }
    }

static uint32 BuiltNumberRead(ThaVersionReader self)
    {
    uint32 version, built;
    VersionAndBuiltNumberGet(self, &version, &built);
    return (built & cBit15_0);
    }

static uint32 VersionNumberRead(ThaVersionReader self)
    {
    uint32 version, built;
    VersionAndBuiltNumberGet(self, &version, &built);
    return version;
    }

static uint32 VersionNumberFromHwGet(ThaVersionReader self)
    {
    uint32 version, built;
    VersionAndBuiltNumberGet(self, &version, &built);
    return version;
    }

static uint32 BuiltNumberFromHwGet(ThaVersionReader self)
    {
    return BuiltNumberRead(self);
    }

static void OverrideTha60210011VersionReader(ThaVersionReader self)
    {
    Tha60210011VersionReader reader = (Tha60210011VersionReader)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011VersionReaderOverride, mMethodsGet(reader), sizeof(m_Tha60210011VersionReaderOverride));

        mMethodOverride(m_Tha60210011VersionReaderOverride, StartVersionIgnoreDate);
        mMethodOverride(m_Tha60210011VersionReaderOverride, StartBuiltNumberIgnoreDate);
        mMethodOverride(m_Tha60210011VersionReaderOverride, BuilNumberStringMake);
        }

    mMethodsSet(reader, &m_Tha60210011VersionReaderOverride);
    }

static void OverrideThaVersionReader(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderOverride, mMethodsGet(self), sizeof(m_ThaVersionReaderOverride));

        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberRead);
        mMethodOverride(m_ThaVersionReaderOverride, VersionNumberRead);
        mMethodOverride(m_ThaVersionReaderOverride, VersionNumberFromHwGet);
        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberFromHwGet);
        }

    mMethodsSet(self, &m_ThaVersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideTha60210011VersionReader(self);
    OverrideThaVersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031VersionReader);
    }

ThaVersionReader Tha60210031VersionReaderObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (Tha60210011VersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60210031VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031VersionReaderObjectInit(newReader, device);
    }

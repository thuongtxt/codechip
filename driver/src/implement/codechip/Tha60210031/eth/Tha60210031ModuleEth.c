/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module ethernet
 *
 * File        : Tha60210031ModuleEth.c
 *
 * Created Date: Aug 13, 2014
 *
 * Description : Ethernet module for product 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/eth/ThaEthPort.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "../../Tha60210011/eth/Tha6021ModuleEth.h"
#include "../man/Tha60210031DeviceReg.h"
#include "Tha60210031ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_linkalm_Eport_XFI_Mask(xfi)            (cBit16 << (xfi))
#define cAf6_linkalm_Eport_QSGMII_Mask(id)          (cBit0 << ((id) << 2))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((tTha60210031ModuleEth *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tAtModuleMethods       *m_AtModuleMethods     = NULL;
static const tAtModuleEthMethods    *m_AtModuleEthMethods  = NULL;
static const tThaModuleEthMethods   *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return Tha60210031EthPortNew(portId, self);
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static eBool PortSerdesPrbsIsSupported(ThaModuleEth self, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(ethPort);
    return cAtTrue;
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
    AtUnused(ethPort);
    return Tha6021DsxXfiSerdesPrbsEngineNew(AtModuleEthSerdesController((AtModuleEth)self, serdesId));
    }

static uint32 DiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    eAtEthPortInterface interface = Tha6021ModuleEthSerdesInterface(self, serdesId);
    eBool isXfi = (interface == cAtEthPortInterfaceXGMii);
    if (isXfi)
        return Tha6021ModuleEthDsCardXfiDiagBaseAddress(self, serdesId);
    return Tha6021ModuleEthQSgmiiDiagBaseAddress(self, serdesId);
    }

static eBool HasSerdesControllers(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SerdesHasMdio(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SerdesMdioBaseAddress(AtModuleEth self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return 0xFB0000;
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    return Tha60210031EthSerdesManagerNew((AtModuleEth)self);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return cAtEthPortInterfaceXGMii;
    }

static eBool Port10GbPreambleCanBeConfigured(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaVersionReader VersionReader(ThaModuleEth self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x08, 0x30, 0x25);
    }

static eBool FullSerdesPrbsSupported(ThaModuleEth self)
    {
    return AtModuleEthSerdesApsSupported((AtModuleEth)self) ? cAtFalse : cAtTrue;
    }

static eBool CanEnableTxSerdes(ThaModuleEth self)
    {
    return AtModuleEthSerdesApsSupported((AtModuleEth)self) ? cAtTrue : cAtFalse;
    }

static eBool SerdesCauseInterrupt(ThaModuleEth self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtTrue;
    }

static void SerdesInterruptProcess(ThaModuleEth self, uint32 ethIntr, AtHal hal)
    {
    uint32 serdesId;
    uint32 numSerdeses = AtModuleEthNumSerdesControllers((AtModuleEth)self);
    uint32 intrVal = AtHalRead(hal, cAf6Reg_linkalm_Eport);
    uint32 intrEn = AtHalRead(hal, cAf6Reg_inten_Eport);
    uint32 currVal = AtHalRead(hal, cAf6Reg_linksta_Eport);
    AtUnused(ethIntr);

    intrVal &= intrEn;
    for (serdesId = 0; serdesId < numSerdeses; serdesId++)
        {
        uint32 state = 0;
        uint32 defect = 0;

        if (intrVal & cAf6_linkalm_Eport_XFI_Mask(serdesId))
            {
            defect = cAtSerdesAlarmTypeLinkDown;
            if (~currVal & cAf6_linkalm_Eport_XFI_Mask(serdesId))
                state = cAtSerdesAlarmTypeLinkDown;
            }
        else if (intrVal & cAf6_linkalm_Eport_QSGMII_Mask(serdesId))
            {
            defect = cAtSerdesAlarmTypeLinkDown;
            if (~currVal & cAf6_linkalm_Eport_QSGMII_Mask(serdesId))
                state = cAtSerdesAlarmTypeLinkDown;
            }

        if (defect)
            AtSerdesControllerAlarmNotify(AtModuleEthSerdesController((AtModuleEth)self, serdesId), defect, state);
        }

    /* Clear interrupt. */
    AtHalWrite(hal, cAf6Reg_linkalm_Eport, intrVal);
    return ;
    }

static eBool InterruptIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet AllPortsEnable(AtModuleEth self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    uint8 numPorts = mMethodsGet(self)->MaxPortsGet(self);

    for (i = 0; i < numPorts; i++)
        {
        AtEthPort port = AtModuleEthPortGet((AtModuleEth)self, i);
        ret |= AtChannelEnable((AtChannel)port, cAtTrue);
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;
        
    return AllPortsEnable((AtModuleEth)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eBool SerdesHasDrp(AtModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static uint32 SerdesDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF00000;
    }

static uint32 DrpPortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);

    if ((selectedPortId == 0) || (selectedPortId == 1))
        return 0x10000;
    if ((selectedPortId == 2) || (selectedPortId == 3))
        return 0xd0000;

    return cBit31_0;
    }

static tAtDrpAddressCalculator *SerdesXfiDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator calculator;
    static tAtDrpAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(tAtDrpAddressCalculator));
    calculator.PortOffset = DrpPortOffset;
    pCalculator = &calculator;

    return pCalculator;
    }

static AtDrp SerdesXfiDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtDrp drp = m_AtModuleEthMethods->SerdesDrpCreate(self, serdesId);
    AtDrpAddressCalculatorSet(drp, SerdesXfiDrpAddressCalculator());
    return drp;
    }

static AtDrp SerdesQsgmiiDrpCreate(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    const uint32 qsgmiiDrpBaseAddress = 0xFE0000;
    AtDrp drp = AtDrpNew(qsgmiiDrpBaseAddress, hal);

    AtDrpNumberOfSerdesControllersSet(drp, AtModuleEthNumSerdesControllers(self));
    AtDrpNeedSelectPortSet(drp, cAtFalse);
    return drp;
    }

static AtDrp SerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(AtModuleEthSerdesController(self, serdesId));

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceQSgmii)
        return SerdesQsgmiiDrpCreate(self);

    return SerdesXfiDrpCreate(self, serdesId);
    }

static AtDrp SerdesDrp(AtModuleEth self, uint32 serdesId)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(AtModuleEthSerdesController(self, serdesId));

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceXGMii)
        return m_AtModuleEthMethods->SerdesDrp(self, serdesId);

    if (!mMethodsGet(self)->SerdesHasDrp(self, serdesId))
        return NULL;

    if (mThis(self)->qsgmiiDrp == NULL)
        mThis(self)->qsgmiiDrp = mMethodsGet(self)->SerdesDrpCreate(self, serdesId);

    return mThis(self)->qsgmiiDrp;
    }

static eBool NeedMultipleLanes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Delete(AtObject self)
    {
    AtDrpDelete(mThis(self)->qsgmiiDrp);
    mThis(self)->qsgmiiDrp = NULL;

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210031ModuleEth *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    AtCoderEncodeString(encoder, AtDrpToString(object->qsgmiiDrp), "qsgmiiDrp");
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, m_AtModuleEthMethods, sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, HasSerdesControllers);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasMdio);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdioBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasDrp);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrp);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleEthOverride, Port10GbPreambleCanBeConfigured);
        mMethodOverride(m_ThaModuleEthOverride, DiagBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionSupportsSerdesAps);
        mMethodOverride(m_ThaModuleEthOverride, FullSerdesPrbsSupported);
        mMethodOverride(m_ThaModuleEthOverride, CanEnableTxSerdes);
        mMethodOverride(m_ThaModuleEthOverride, SerdesCauseInterrupt);
        mMethodOverride(m_ThaModuleEthOverride, SerdesInterruptProcess);
        mMethodOverride(m_ThaModuleEthOverride, InterruptIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, NeedMultipleLanes);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleEth);
    }

AtModuleEth Tha60210031ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60210031ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleEthObjectInit(newModule, device);
    }

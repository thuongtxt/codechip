/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210031EthPortErrorGeneratorInternal.h
 * 
 * Created Date: Apr 6, 2016
 *
 * Description : Error generator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHPORTERRORGENERATORINTERNAL_H_
#define _THA60210031ETHPORTERRORGENERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/diag/AtErrorGeneratorInternal.h"
#include "Tha60210031EthPortErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031EthPortErrorGeneratorMethods
    {
    uint32 (*ETH_FCS_errins_en_cfg_Address)(Tha60210031EthPortErrorGenerator self);
    eBool (*NeedDisablePwReoderingOnPcsErrorForcing)(Tha60210031EthPortErrorGenerator self);
    }tTha60210031EthPortErrorGeneratorMethods;

typedef struct tTha60210031EthPortErrorGenerator
    {
    tAtErrorGenerator super;
    const tTha60210031EthPortErrorGeneratorMethods *methods;

    /* Private data */
    eAtSide side;
    } tTha60210031EthPortErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60210031EthPortErrorGeneratorObjectInit(AtErrorGenerator self, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHPORTERRORGENERATORINTERNAL_H_ */


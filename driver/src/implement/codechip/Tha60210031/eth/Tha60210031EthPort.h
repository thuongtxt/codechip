/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210031EthPort.h
 * 
 * Created Date: Aug 26, 2015
 *
 * Description : ETH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHPORT_H_
#define _THA60210031ETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/eth/Tha60150011EthPort10GbInternal.h"
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cMaxNumQsgmiiLanes 5

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031EthPort * Tha60210031EthPort;

typedef struct tTha60210031EthPortMethods
    {
    uint32 (*XgmiiDefaultOffset)(Tha60210031EthPort self);
    eBool (*HasQsgmii)(Tha60210031EthPort self);
    eBool (*InterfaceIsSupported)(Tha60210031EthPort self, eAtEthPortInterface interface);
    eBool (*EpEthPortInterfaceIsApplicable)(Tha60210031EthPort self);
    }tTha60210031EthPortMethods;

typedef struct tTha60210031EthPort
    {
    tTha60150011EthPort10Gb super;
    const tTha60210031EthPortMethods *methods;

    /* Private data */
    uint8 interfaceMode;
    uint32 laneBandwidthInBps[cMaxNumQsgmiiLanes];
    AtList pwsInQueue[cMaxNumQsgmiiLanes];
    AtList boundPws;
    AtErrorGenerator errorGenerator;
    }tTha60210031EthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60210031EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
eBool Tha60210031EthPortIsQsgmii(AtEthPort self);

eAtRet Tha60210031EthPortQueuePwAdd(AtEthPort self, uint8 queueId, AtPw pw, uint32 newPwBandwidth);
eAtRet Tha60210031EthPortQueuePwRemove(AtEthPort self, uint8 queueId, AtPw pw);
eAtRet Tha60210031EthPortBoundPwAdd(AtEthPort self, AtPw pw);
eAtRet Tha60210031EthPortBoundPwRemove(AtEthPort self, AtPw pw);

uint8 Tha60210031EthPortQueueIdDynamicAllocate(AtEthPort self, uint32 pwBandwidth);
eBool Tha60210031EthPortQueueBandwidthIsAvailable(AtEthPort self, uint8 queueId, uint32 pwBandwidth);
eBool Tha60210031EthPortQueueBandwidthCanAdjust(AtEthPort self, uint8 queueId, uint32 oldBandwidth, uint32 newBandwidth);
eAtRet Tha60210031EthPortQueueBandwidthAdjust(AtEthPort self, uint8 queueId, uint32 oldBandwidth, uint32 newBandwidth);
uint32 Tha60210031EthPortQueuePwRemainingBandwidthInBpsGet(AtEthPort ethPort, uint8 queueId, uint32 removeBwInBps);
uint32 Tha60210031EthPortQueuesMaxRemainingBandwidthInBpsGet(AtEthPort self, uint8 queueId, uint32 removeBwInBps);

AtErrorGenerator Tha60210031EthPortErrorGeneratorGet(AtEthPort self, eAtSide side);
#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHPORT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210031EthPortErrorGenerator.c
 *
 * Created Date: Mar 28, 2016
 *
 * Description : Implementation of the Tha60210031EthPortTxErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/util/ThaBitMask.h"
#include "Tha60210031EthPortErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_ETH_FCS_errins_en_cfg_Xgmii_Base  0x3c1000
#define cAf6Reg_ETH_FCS_errins_en_cfg_Qsgmii_Base 0x401000
#define cAf6Reg_ETH_FCS_errins_en_cfg 0x0000007
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrType_Mask  cBit30_29
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrType_Shift 29
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrMode_Mask  cBit28
#define cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrMode_Shift 28
#define cAf6__ETH_FCS_errins_en_cfg_ETHFCSErrThr_Mask   cBit27_0
#define cAf6__ETH_FCS_errins_en_cfg_ETHFCSErrThr_Shift  0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031EthPortErrorGenerator)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031EthPortErrorGeneratorMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtErrorGeneratorMethods m_AtErrorGeneratorOverride;

/* Save super implementation */
static const tAtObjectMethods         *m_AtObjectMethods         = NULL;
static const tAtErrorGeneratorMethods *m_AtErrorGeneratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031EthPortErrorGenerator);
    }

static uint32 EthPortBaseAddress(AtEthPort port)
    {
    eAtEthPortInterface interface = AtEthPortInterfaceGet(port);
    if (interface == cAtEthPortInterfaceXGMii)
        return cAf6Reg_ETH_FCS_errins_en_cfg_Xgmii_Base;
    else
        return cAf6Reg_ETH_FCS_errins_en_cfg_Qsgmii_Base;
    }

static uint32 ETH_FCS_errins_en_cfg_Address(Tha60210031EthPortErrorGenerator self)
    {
    AtErrorGenerator generator = (AtErrorGenerator)self;
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(generator);
    return cAf6Reg_ETH_FCS_errins_en_cfg + EthPortBaseAddress(port);
    }

static void DefaultSet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    mChannelHwWrite(port, address, 0, cAtModuleEth);
    }

static eAtRet SourceSet(AtErrorGenerator self, AtObject channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtErrorModeNotSupport;
    }

static eAtRet Init(AtErrorGenerator self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    DefaultSet(self);
    if (mThis(self)->side == cAtSideTx)
        AtErrorGeneratorErrorTypeSet(self, cAtEthPortErrorGeneratorErrorTypeFcs);
    else
        AtErrorGeneratorErrorTypeSet(self, cAtEthPortErrorGeneratorErrorTypeMacPcs);

    return cAtOk;
    }

static void HwErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    uint32 regVal = mChannelHwRead(port, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6__ETH_FCS_errins_en_cfg_ETHFCSErrThr_, errorNum);

    mChannelHwWrite(port, address, regVal, cAtModuleEth);
    }

static uint32 HwErrorNumGet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    uint32 regVal = mChannelHwRead(port, address, cAtModuleEth);

    return mRegField(regVal, cAf6__ETH_FCS_errins_en_cfg_ETHFCSErrThr_);
    }

static uint32 MaxErrorNum(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0xFFFFFFF;
    }

static eBool NeedDisablePwReoderingOnPcsErrorForcing(Tha60210031EthPortErrorGenerator self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldDisablePwReorder(AtErrorGenerator self)
	{
    if (mThis(self)->side != cAtSideRx)
    	return cAtFalse;

    if (AtErrorGeneratorErrorTypeGet(self) != cAtEthPortErrorGeneratorErrorTypeMacPcs)
    	return cAtFalse;

    return mMethodsGet(mThis(self))->NeedDisablePwReoderingOnPcsErrorForcing(mThis(self));
	}

static AtDevice Device(AtErrorGenerator self)
    {
    AtChannel port = (AtChannel)AtErrorGeneratorSourceGet(self);
    return AtChannelDeviceGet(port);
    }

static AtModulePw PwModule(AtErrorGenerator self)
    {
    return (AtModulePw)AtDeviceModuleGet(Device(self), cAtModulePw);
    }

static ThaBitMask AllPwsReorderDisable(AtErrorGenerator self)
    {
    uint32 pw_i;
    AtModulePw pwModule = PwModule(self);
    uint32 numbsPws = AtModulePwMaxPwsGet(pwModule);
    ThaBitMask pwReorderMask;
    AtEthPort port;

    if (!ShouldDisablePwReorder(self))
        return NULL;

    port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    pwReorderMask = ThaBitMaskNew(numbsPws);
    if (pwReorderMask == NULL)
    	return NULL;

    for (pw_i = 0; pw_i < numbsPws; pw_i++)
        {
        AtPw pw;

        /* Not exist PW */
        pw = AtModulePwGetPw(pwModule, pw_i);
        if (pw == NULL)
            continue;

        /* Only care this port */
        if (AtPwEthPortGet(pw) != port)
            continue;

        /* Only care PWs that have reordering enabled */
        if (!AtPwReorderingIsEnabled(pw))
            continue;

        /* Disable and mark corresponding bit so that this attribute will be restored later */
        if (AtPwReorderingEnable(pw, cAtFalse) == cAtOk)
            ThaBitMaskSetBit(pwReorderMask, AtChannelIdGet((AtChannel)pw));
        }

    return pwReorderMask;
    }

static eAtRet AllPwsReorderEnable(AtErrorGenerator self, ThaBitMask pwReorderMask)
    {
    uint32 pw_i;
    AtModulePw pwModule = PwModule(self);
    uint32 numbsPws = AtModulePwMaxPwsGet(pwModule);
    eAtRet ret = cAtOk;

    for (pw_i = 0; pw_i < numbsPws; pw_i++)
        {
        AtPw pw;

        /* Not exist PW */
        pw = AtModulePwGetPw(pwModule, pw_i);
        if (pw == NULL)
            continue;

        /* Only care PW that has reordering enabled before */
        if (ThaBitMaskBitVal(pwReorderMask, AtChannelIdGet((AtChannel)pw)))
            ret |= AtPwReorderingEnable(pw, cAtTrue);
        }

    return ret;
    }

static eBool ErrorGeneratingDone(AtErrorGenerator self)
    {
    static const uint32 cTimeoutMs = 3000;
    tAtOsalCurTime startTime, curTime;
    uint32 remainedErrors;
    uint32 elapsedTimeMs = 0;

    AtOsalCurTimeGet(&startTime);

    while (elapsedTimeMs < cTimeoutMs)
        {
        /* Done */
        remainedErrors = HwErrorNumGet(self);
        if (remainedErrors == 0)
            return cAtTrue;

        /* Cannot timeout on simulation mode */
        if (AtDeviceIsSimulated(Device(self)))
            return cAtTrue;

        /* Give hardware a moment then retry */
        AtOsalUSleep(1000);
        AtOsalCurTimeGet(&curTime);
        elapsedTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static eAtRet ErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    ThaBitMask pwReorderEnabledMask = NULL;
    eAtRet ret = cAtOk;

    if (errorNum > MaxErrorNum(self))
        return cAtErrorOutOfRangParm;

    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeOneshot)
        return cAtErrorNotApplicable;

    if (errorNum == 0)
    	{
    	AtErrorGeneratorErrorNumDbSet(self, errorNum);
    	return cAtOk;
    	}

    /* Need to disable PW reordering because many PCS error may cause hardware
     * reorder engine malfunction even when this error forcing is stopped */
    pwReorderEnabledMask = AllPwsReorderDisable(self);

	/* Start generating */
    HwErrorNumSet(self, errorNum);
    AtErrorGeneratorErrorNumDbSet(self, errorNum);

    /* It is necessary to wait till this operation is stop, the recover reordering information */
    if (pwReorderEnabledMask == NULL)
        return cAtOk;

    if (!ErrorGeneratingDone(self))
        ret = cAtErrorDevBusy;

    /* No matter what hardware finish its job or not, still need to recover
     * this configuration */
    AllPwsReorderEnable(self, pwReorderEnabledMask);
    AtObjectDelete((AtObject)pwReorderEnabledMask);

    return ret;
    }

static eAtRet ErrorRateSet(AtErrorGenerator self, eAtBerRate errorRate)
    {
    if (AtErrorGeneratorErrorRateToErrorNum(self, errorRate) > MaxErrorNum(self))
        return cAtErrorOutOfRangParm;

    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeContinuous)
        return cAtErrorNotApplicable;

    HwErrorNumSet(self, AtErrorGeneratorErrorRateToErrorNum(self, errorRate));
    AtErrorGeneratorErrorRateDbSet(self, errorRate);
    return cAtOk;
    }

static eBool ErrorTypeTxIsSupported(uint32 errorType)
    {
    if ((errorType == cAtEthPortErrorGeneratorErrorTypeFcs) ||
        (errorType == cAtEthPortErrorGeneratorErrorTypeMacPcs))
        return cAtTrue;
    return cAtFalse;
    }

static eBool ErrorTypeRxIsSupported(uint32 errorType)
    {
    if (errorType == cAtEthPortErrorGeneratorErrorTypeMacPcs)
        return cAtTrue;

    return cAtFalse;
    }

static eBool ErrorTypeIsSupported(AtErrorGenerator self, uint32 errorType)
    {
    if (mThis(self)->side == cAtSideTx)
        return ErrorTypeTxIsSupported(errorType);

    return ErrorTypeRxIsSupported(errorType);
    }

static uint32 ErrorTypeTxSw2Hw(uint32 swErrorType)
    {
    if (swErrorType == cAtEthPortErrorGeneratorErrorTypeFcs)
        return 0;
    if (swErrorType == cAtEthPortErrorGeneratorErrorTypeMacPcs)
        return 1;
    return 0;
    }

static uint32 ErrorTypeRxSw2Hw(uint32 swErrorType)
    {
    if (swErrorType == cAtEthPortErrorGeneratorErrorTypeMacPcs)
        return 2;
    return 0;
    }

static uint32 ErrorTypeSw2Hw(AtErrorGenerator self, uint32 swErrorType)
    {
    if (mThis(self)->side == cAtSideTx)
        return ErrorTypeTxSw2Hw(swErrorType);

    return ErrorTypeRxSw2Hw(swErrorType);
    }

static uint32 ErrorTypeTxHw2Sw(uint32 hwErrorType)
    {
    if (hwErrorType == 0)
        return cAtEthPortErrorGeneratorErrorTypeFcs;
    if (hwErrorType == 1)
        return cAtEthPortErrorGeneratorErrorTypeMacPcs;
    return cAtEthPortErrorGeneratorErrorTypeInvalid;
    }

static uint32 ErrorTypeRxHw2Sw(uint32 hwErrorType)
    {
    if (hwErrorType == 2)
        return cAtEthPortErrorGeneratorErrorTypeMacPcs;
    return cAtEthPortErrorGeneratorErrorTypeInvalid;
    }

static uint32 ErrorTypeHw2Sw(AtErrorGenerator self, uint32 hwErrorType)
    {
    if (mThis(self)->side == cAtSideTx)
        return ErrorTypeTxHw2Sw(hwErrorType);

    return ErrorTypeRxHw2Sw(hwErrorType);
    }

static void HwErrorTypeSet(AtErrorGenerator self, uint32 hwErrorType)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    uint32 regVal = mChannelHwRead(port, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrType_, hwErrorType);

    mChannelHwWrite(port, address, regVal, cAtModuleEth);
    }

static uint32 HwErrorTypeGet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    uint32 regVal = mChannelHwRead(port, address, cAtModuleEth);

    return mRegField(regVal, cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrType_);
    }

static eAtRet ErrorTypeSet(AtErrorGenerator self, uint32 errorType)
    {
    if (!ErrorTypeIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    HwErrorTypeSet(self, ErrorTypeSw2Hw(self, errorType));
    return cAtOk;
    }

static uint32 ErrorTypeGet(AtErrorGenerator self)
    {
    return ErrorTypeHw2Sw(self, HwErrorTypeGet(self));
    }

static void HwModeSet(AtErrorGenerator self, uint32 hwMode)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    uint32 regVal = mChannelHwRead(port, address, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrMode_, hwMode);

    mChannelHwWrite(port, address, regVal, cAtModuleEth);
    }

static uint32 HwModeGet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ETH_FCS_errins_en_cfg_Address(mThis(self));
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet(self);
    uint32 regVal = mChannelHwRead(port, address, cAtModuleEth);

    return mRegField(regVal, cAf6_ETH_FCS_errins_en_cfg_ETHFCSErrMode_);
    }

static uint32 SwMode2HwMode(uint32 swMode)
    {
    if (swMode == cAtErrorGeneratorModeOneshot)
        return 0;
    if (swMode == cAtErrorGeneratorModeContinuous)
        return 1;
    return 0;
    }

static uint32 HwMode2SwMode(uint32 hwMode)
    {
    if (hwMode == 0)
        return cAtErrorGeneratorModeOneshot;
    if (hwMode == 1)
        return cAtErrorGeneratorModeContinuous;
    return cAtErrorGeneratorModeInvalid;
    }

static eAtRet ModeSet(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    if (!AtErrorGeneratorModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    HwModeSet(self, SwMode2HwMode(mode));
    return cAtOk;
    }

static eAtErrorGeneratorMode ModeGet(AtErrorGenerator self)
    {
    return HwMode2SwMode(HwModeGet(self));
    }

static eAtRet OneShotStart(AtErrorGenerator self)
    {
    uint32 errorNum = AtErrorGeneratorErrorNumGet(self);

    if (errorNum == 0)
        return cAtErrorInvlParm;

    HwErrorNumSet(self, AtErrorGeneratorErrorNumGet(self));

    return cAtOk;
    }

static eAtRet ContinuousStart(AtErrorGenerator self)
    {
    uint32 errorNum = AtErrorGeneratorErrorRateToErrorNum(self, AtErrorGeneratorErrorRateGet(self));

    if (errorNum == 0)
        return cAtErrorInvlParm;

    HwErrorNumSet(self, errorNum);

    return cAtOk;
    }

static eAtRet Start(AtErrorGenerator self)
    {
    if (AtErrorGeneratorIsStarted(self))
        return cAtOk;

    if (AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeOneshot)
        return OneShotStart(self);

    if (AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeContinuous)
        return ContinuousStart(self);

    return cAtErrorNotReady;
    }

static eAtRet Stop(AtErrorGenerator self)
    {
    HwErrorNumSet(self, 0);
    return cAtOk;
    }

static eBool IsStarted(AtErrorGenerator self)
    {

    return HwErrorNumGet(self) != 0 ? cAtTrue : cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210031EthPortErrorGenerator object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(side);
    }

static void OverrideAtObject(AtErrorGenerator self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtErrorGenerator(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtErrorGeneratorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtErrorGeneratorOverride, m_AtErrorGeneratorMethods, sizeof(m_AtErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_AtErrorGeneratorOverride, Init);
        mMethodOverride(m_AtErrorGeneratorOverride, SourceSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeGet);
        mMethodOverride(m_AtErrorGeneratorOverride, ModeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ModeGet);
        mMethodOverride(m_AtErrorGeneratorOverride, Start);
        mMethodOverride(m_AtErrorGeneratorOverride, Stop);
        mMethodOverride(m_AtErrorGeneratorOverride, IsStarted);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorNumSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorRateSet);
        }

    mMethodsSet(self, &m_AtErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    OverrideAtObject(self);
    OverrideAtErrorGenerator(self);
    }

static void MethodsInit(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ETH_FCS_errins_en_cfg_Address);
        mMethodOverride(m_methods, NeedDisablePwReoderingOnPcsErrorForcing);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

AtErrorGenerator Tha60210031EthPortErrorGeneratorObjectInit(AtErrorGenerator self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtErrorGeneratorObjectInit(self, (AtObject)channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    mThis(self)->side = cAtSideTx;

    return self;
    }

AtErrorGenerator Tha60210031EthPortErrorGeneratorNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031EthPortErrorGeneratorObjectInit(errorGenerator, channel);
    }

void Tha60210031EthPortErrorGeneratorHwDefaultSet(AtChannel self)
    {
    uint32 address;

    address = cAf6Reg_ETH_FCS_errins_en_cfg + cAf6Reg_ETH_FCS_errins_en_cfg_Xgmii_Base;
    mChannelHwWrite(self, address, 0, cAtModuleEth);
    address = cAf6Reg_ETH_FCS_errins_en_cfg + cAf6Reg_ETH_FCS_errins_en_cfg_Qsgmii_Base;
    mChannelHwWrite(self, address, 0, cAtModuleEth);
    }

void Tha60210031EthPortErrorGeneratorSideSet(AtErrorGenerator self, eAtSide side)
    {
    if (mThis(self)->side != side)
        {
        mThis(self)->side = side;
        AtErrorGeneratorInit(self);
        }
    }

eAtSide Tha60210031EthPortErrorGeneratorSideGet(AtErrorGenerator self)
    {
    return mThis(self)->side;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210031ModuleEth.h
 * 
 * Created Date: Aug 26, 2015
 *
 * Description : Ethernet module implementation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEETH_H_
#define _THA60210031MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/eth/Tha60150011ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleEth
    {
    tTha60150011ModuleEth super;

    /* Private data */
    AtDrp qsgmiiDrp;
    }tTha60210031ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60210031ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEETH_H_ */


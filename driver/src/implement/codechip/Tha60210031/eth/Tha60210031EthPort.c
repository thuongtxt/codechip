/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (coffee) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210031EthPort.c
 *
 * Created Date: May 21, 2015
 *
 * Description : ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../../../generic/util/AtUtil.h"
#include "../../../default/eth/ThaEthPort10GbReg.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/physical/ThaEthSerdesManager.h"
#include "../../Tha60210011/eth/Tha6021ModuleEth.h"
#include "../../Tha60210011/eth/Tha6021ModuleEthReg.h"
#include "../man/Tha60210031Device.h"
#include "Tha60210031EthPort.h"
#include "AtNumber.h"
#include "Tha60210031EthPortErrorGenerator.h"
#include "Tha60210031EthPort.h"

/*--------------------------- Define -----------------------------------------*/
/*Mac Address Bit47_32 Control
  Field: [26]  %%  GeMode %% GE mode 0:XGMII; 1:SGMII   %% RW %% 0x0 %% 0x0
  */
#define cGlbRegGeModeMask  cBit26
#define cGlbRegGeModeShift 26
#define cRegEthTxEnableMask cBit0
#define cRegEthTxEnableShift 0
#define cRegEthRxEnableMask cBit4
#define cRegEthRxEnableShift 4

#define cWorkingOrProtectionSerdesMask  cBit6
#define cWorkingOrProtectionSerdesShift 6

#define cDiagMonQSGMIISerDesIdSelectMask  cBit6_4
#define cDiagMonQSGMIISerDesIdSelectShift 4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)   ((Tha60210031EthPort)self)
#define mPortId(self) (uint8)AtChannelIdGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031EthPortMethods m_methods;

/* Override */
static tAtEthPortMethods      m_AtEthPortOverride;
static tThaEthPortMethods     m_ThaEthPortOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtObjectMethods       m_AtObjectOverride;
static tThaEthPort10GbMethods m_ThaEthPort10GbOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtEthPortMethods      *m_AtEthPortMethods      = NULL;
static const tThaEthPortMethods     *m_ThaEthPortMethods     = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tThaEthPort10GbMethods *m_ThaEthPort10GbMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 XgmiiDefaultOffset(Tha60210031EthPort self)
    {
    return (ThaEthPortDefaultOffset((ThaEthPort)self) + 0x3c1000UL);
    }

static eBool FpgaVersionSwHandleQsgmiiLanes(AtEthPort self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtChannelModuleGet((AtChannel)self));

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(device))
        return cAtTrue;

    return cAtFalse;
    }

static eBool MultipleLanesIsSupported(AtEthPort self)
    {
    if (!Tha60210031EthPortIsQsgmii(self))
        return cAtFalse;

    return FpgaVersionSwHandleQsgmiiLanes(self);
    }

static eBool IsSecondarySerdes(AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    return ((serdesId == 2) || (serdesId == 3)) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    if (!MultipleLanesIsSupported(self))
        return Tha6021DsxEthPortRxSerdesSelect(self, serdes);

    if (IsSecondarySerdes(serdes))
        return cAtErrorModeNotSupport;

    return Tha6021DsxEthPortRxSerdesSelect(self, serdes);
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    return Tha6021DsxEthPortRxSelectedSerdes(self);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (MultipleLanesIsSupported(self) && (IsSecondarySerdes(serdes)))
        return cAtErrorModeNotSupport;

    return Tha6021DsxEthPortTxSerdesBridge(self, serdes);
    }

static AtSerdesController TxBridgedSerdesGet(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    AtSerdesController serdes1;

    /* Do not handle bridging in case of diagnostic mode is enabled */
    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet((AtChannel)self)))
        return NULL;

    /* Just can bridge to serdes 1 */
    serdes1 = AtModuleEthSerdesController(ethModule, 1);
    if (AtSerdesControllerIsEnabled(serdes1))
        return serdes1;

    return NULL;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    if (!MultipleLanesIsSupported(self))
        return Tha6021DsxEthPortTxBridgedSerdes(self);

    return TxBridgedSerdesGet(self);
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (MultipleLanesIsSupported(self) && (IsSecondarySerdes(serdes)))
        return cAtFalse;

    return Tha6021DsxEthPortTxSerdesCanBridge(self, serdes);
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    uint8 portId = mPortId(self);
    uint32 regAddr, regVal;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mMethodsGet(mThis(self))->XgmiiDefaultOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopinMask, cThaIpEthTripSmacLoopinShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    if (mMethodsGet(mThis(self))->HasQsgmii(mThis(self)))
        {
    regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + ThaEthPortMacBaseAddress(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripSmacLoopinPortMask(portId),
              cThaIpEthTripSmacLoopinPortShift(portId),
              mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
        }

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    uint8 portId = mPortId(self);
    uint32 regAddr, regVal;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mMethodsGet(mThis(self))->XgmiiDefaultOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopoutMask, cThaIpEthTripSmacLoopoutShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    if (mMethodsGet(mThis(self))->HasQsgmii(mThis(self)))
        {
    regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + ThaEthPortMacBaseAddress(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripSmacLoopoutPortMask(portId),
              cThaIpEthTripSmacLoopoutPortShift(portId),
              mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
        }

    return cAtOk;
    }

static eAtEthPortInterface CachedInterface(AtEthPort self)
    {
    if (mThis(self)->interfaceMode == 0)
        mThis(self)->interfaceMode = mMethodsGet(self)->DefaultInterface(self);
    return mThis(self)->interfaceMode;
    }

static eBool EpEthPortInterfaceCanBeChanged(AtEthPort self)
    {
    return mMethodsGet(mThis(self))->EpEthPortInterfaceIsApplicable(mThis(self));
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    uint32 regVal;
    uint32 regAddr;
    uint8  gbeMode = 0;
    eBool isEp;
    AtSerdesController serdes;

    if (ThaDeviceIsEp((ThaDevice)AtChannelDeviceGet((AtChannel)self)) && (!EpEthPortInterfaceCanBeChanged(self)))
        return cAtOk;

    /* Interface not change */
    if (AtEthPortInterfaceGet(self) == interface)
        return cAtOk;

    if (AtListLengthGet(mThis(self)->boundPws) > 0)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelWarning, AtSourceLocation, "Cannot change interface when there are still bound PWs\r\n");
        return cAtErrorResourceBusy;
        }

    if (!mMethodsGet(mThis(self))->InterfaceIsSupported(mThis(self), interface))
        return cAtErrorModeNotSupport;

    gbeMode = (interface == cAtEthPortInterfaceXGMii) ? 0 : 1;

    /* Release bridging before change interface mode */
    isEp = ThaDeviceIsEp((ThaDevice)AtChannelDeviceGet((AtChannel)self));
    if (!isEp)
        {
        AtEthPortTxSerdesBridge(self, NULL);
        AtEthPortRxSerdesSelect(self, AtEthPortSerdesController(self));
        }

    /* configure at TOP*/
    regAddr = cThaRegEthMacAddr47_32;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cGlbRegGeModeMask,
              cGlbRegGeModeShift,
              gbeMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    mThis(self)->interfaceMode = interface;

    if (isEp)
        return cAtOk;

    serdes = AtEthPortSerdesController((AtEthPort)self);
    if (AtSerdesControllerCanEnable(serdes, AtChannelIsEnabled((AtChannel)self)))
        return AtSerdesControllerEnable(serdes, AtChannelIsEnabled((AtChannel)self));

    return cAtOk;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    uint32 regAddr, regVal;

    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet((AtChannel)self)))
        return CachedInterface(self);

    regAddr = cThaRegEthMacAddr47_32;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    if (regVal & cGlbRegGeModeMask)
        return cAtEthPortInterfaceQSgmii;

    return cAtEthPortInterfaceXGMii;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager(ethModule);
    return ThaEthSerdesManagerSerdesController(manager, AtChannelIdGet((AtChannel)self));
    }

static eAtRet XgmiiMacActivate(ThaEthPort self, eBool activate)
    {
    uint32 regAddr, regVal;
    uint8 hwActivate;
    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mMethodsGet(mThis(self))->XgmiiDefaultOffset(mThis(self));
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    hwActivate = activate ? 1 : 0;
    mFieldIns(&regVal, cThaIpEthTripSmacTxActMask, cThaIpEthTripSmacTxActShift, hwActivate);
    mFieldIns(&regVal, cThaIpEthTripSmacRxActMask, cThaIpEthTripSmacRxActShift, hwActivate);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtRet QsgmiiMacActivate(ThaEthPort self, eBool activate)
    {
    uint32 regAddr, regVal;
    uint8 hwActivate;
    regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + ThaEthPortMacBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    hwActivate = activate ? 1 : 0;
    mFieldIns(&regVal, cThaIpEthTripSmacTxActMask, cThaIpEthTripSmacTxActShift, hwActivate);
    mFieldIns(&regVal, cThaIpEthTripSmacRxActMask, cThaIpEthTripSmacRxActShift, hwActivate);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    eAtRet ret = m_ThaEthPortMethods->MacActivate(self, activate);

    if (ret == cAtOk)
        ret = XgmiiMacActivate(self, activate);

    if ((ret == cAtOk) && mMethodsGet(mThis(self))->HasQsgmii(mThis(self)))
        ret = QsgmiiMacActivate(self, activate);

    return ret;
    }

static void ErrorGeneratorDelete(AtChannel self)
    {
    AtObjectDelete((AtObject)mThis(self)->errorGenerator);
    mThis(self)->errorGenerator = NULL;
    }

static void ErrorGeneratorDefaultInit(AtChannel self)
    {
    Tha60210031EthPortErrorGeneratorHwDefaultSet(self);
    ErrorGeneratorDelete(self);
    }

static eAtRet Init(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    eAtEthPortInterface interface;
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    interface = AtModuleEthPortDefaultInterface((AtModuleEth)AtChannelModuleGet(self), port);
    ret |= AtEthPortInterfaceSet(port, interface);
    ret |= AtEthPortMacCheckingEnable(port, cAtFalse);
    AtListFlush(mThis(self)->boundPws);

    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtChannelDeviceGet((AtChannel)self)))
        ErrorGeneratorDefaultInit(self);

    return ret;
    }

static void PrintAllPwsInLane(Tha60210031EthPort self, uint8 lane)
    {
    AtList allPwList = AtEthPortQueueAllPwsGet((AtEthPort)self, lane);
    AtIterator iterator = AtListIteratorCreate(allPwList);
    AtPw pw;

    pw = (AtPw)AtIteratorNext(iterator);
    if (pw == NULL)
        {
        AtPrintc(cSevNormal, "None\r\n");
        return;
        }

    AtPrintc(cSevNormal, "%u", AtChannelIdGet((AtChannel)pw) + 1);
    while ((pw = (AtPw)AtIteratorNext(iterator)) != NULL)
        AtPrintc(cSevNormal, ", %u", AtChannelIdGet((AtChannel)pw) + 1);

    AtPrintc(cSevNormal, "\r\n");
    AtObjectDelete((AtObject)iterator);
    }

static void ShowCurrentLanesBandwidth(Tha60210031EthPort self)
    {
    uint8 lane_i;
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Lanes information:\r\n");
    for (lane_i = 0; lane_i < cMaxNumQsgmiiLanes; lane_i++)
        {
        AtPrintc(cSevNormal, "  Lane %u bandwidth: %s bps\r\n", lane_i + 1, AtNumberDigitGroupingString(self->laneBandwidthInBps[lane_i]));
        AtPrintc(cSevNormal, "  Lane %u all pws  : ", lane_i + 1);
        PrintAllPwsInLane(self, lane_i);
        AtPrintc(cSevNormal, "\r\n");
        }
    }

static eAtRet Debug(AtChannel self)
    {
    /* Super */
    eAtRet ret = m_AtChannelMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    ShowCurrentLanesBandwidth(mThis(self));
    return cAtOk;
    }

static eBool QueueIdIsValid(uint8 queueId)
    {
    if (queueId >= cMaxNumQsgmiiLanes)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 QueueMaxBandwidthInBps(void)
    {
    return 900000000; /* 900M */
    }

static eBool QueueBandwidthIsAvailable(AtEthPort self, uint8 queueId, uint32 removeBw, uint32 addBw)
    {
    uint32 currentBw = mThis(self)->laneBandwidthInBps[queueId];
    uint32 landBandwidth = currentBw + addBw;

    landBandwidth = (landBandwidth > removeBw) ? (landBandwidth - removeBw) : 0;
    if (landBandwidth <= QueueMaxBandwidthInBps())
        return cAtTrue;

    return cAtFalse;
    }

static uint32 QueueCurrentBandwidthInBpsGet(AtEthPort self, uint8 queueId)
    {
    if (!QueueIdIsValid(queueId))
        return 0;

    return mThis(self)->laneBandwidthInBps[queueId];
    }

static AtList QueueAllPwsGet(AtEthPort self, uint8 queueId)
    {
    if (!QueueIdIsValid(queueId))
        return NULL;

    return mThis(self)->pwsInQueue[queueId];
    }

static void Delete(AtObject self)
    {
    uint8 queue_i;

    /* Delete internal objects */
    for (queue_i = 0; queue_i < cMaxNumQsgmiiLanes; queue_i++)
        {
        AtObjectDelete((AtObject)(mThis(self)->pwsInQueue[queue_i]));
        mThis(self)->pwsInQueue[queue_i] = NULL;
        }

    AtObjectDelete((AtObject)mThis(self)->boundPws);
    mThis(self)->boundPws = NULL;

    ErrorGeneratorDelete((AtChannel)self);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210031EthPort object = (Tha60210031EthPort)self;
    AtChannel *sortedPws;
    uint32 queueId;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(interfaceMode);
    mEncodeUInt32Array(laneBandwidthInBps, cMaxNumQsgmiiLanes);
    mEncodeObject(errorGenerator);

    for (queueId = 0; queueId < cMaxNumQsgmiiLanes; queueId++)
        {
        char key[16];
        AtSnprintf(key, sizeof(key) - 1, "pwsInQueue[%u]", queueId);

        sortedPws = AtUtilSortedChannelsCreate(object->pwsInQueue[queueId]);
        AtCoderEncodeObjectDescriptionInArray(encoder, (AtObject *)sortedPws, AtListLengthGet(object->pwsInQueue[queueId]), key);
        AtOsalMemFree(sortedPws);
        }

    sortedPws = AtUtilSortedChannelsCreate(object->boundPws);
    AtCoderEncodeObjectDescriptionInArray(encoder, (AtObject *)sortedPws, AtListLengthGet(object->boundPws), "boundPws");
    AtOsalMemFree(sortedPws);
    }

static void QueueBandwidthAdd(AtEthPort self, uint8 queueId, uint32 bandwidth)
    {
    mThis(self)->laneBandwidthInBps[queueId] += bandwidth;
    }

static void QueueBandwidthRemove(AtEthPort self, uint8 queueId, uint32 bandwidth)
    {
    if (mThis(self)->laneBandwidthInBps[queueId] < bandwidth)
        mThis(self)->laneBandwidthInBps[queueId] = 0;
    else
        mThis(self)->laneBandwidthInBps[queueId] -= bandwidth;
    }

static uint32 QueueBandwidthTryRemoving(AtEthPort self, uint8 queueId, uint32 bandwidth)
    {
    if (mThis(self)->laneBandwidthInBps[queueId] < bandwidth)
        return 0;

    return mThis(self)->laneBandwidthInBps[queueId] - bandwidth;
    }

static void QueuePwListAdd(AtEthPort self, uint8 queueId, AtPw pw)
    {
    if (mThis(self)->pwsInQueue[queueId] == NULL)
        mThis(self)->pwsInQueue[queueId] = AtListCreate(0);

    AtListObjectAdd(mThis(self)->pwsInQueue[queueId], (AtObject)pw);
    }

static void QueuePwListRemove(AtEthPort self, uint8 queueId, AtPw pw)
    {
    AtListObjectRemove(mThis(self)->pwsInQueue[queueId], (AtObject)pw);
    }

static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    AtUnused(self);
    return 4500000; /* 4.5G */
    }

static uint32 RemainingBpsOfMaxBandwidth(ThaEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxNumPw(AtEthPort self)
    {
    return AtModulePwMaxPwsGet((AtModulePw)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePw));
    }

static AtList PwList(AtEthPort self)
    {
    if (mThis(self)->boundPws == NULL)
        mThis(self)->boundPws = AtListCreate(MaxNumPw(self));

    return mThis(self)->boundPws;
    }

static uint32 MaxQueuesGet(AtEthPort self)
    {
    if (!Tha60210031EthPortIsQsgmii(self))
        return 0;

    return cMaxNumQsgmiiLanes;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtEthPort self)
    {
    return Tha60210031EthPortErrorGeneratorGet(self, cAtSideTx);
    }

static AtErrorGenerator RxErrorGeneratorGet(AtEthPort self)
    {
    return Tha60210031EthPortErrorGeneratorGet(self, cAtSideRx);
    }

static eBool HasQsgmii(Tha60210031EthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EpEthPortInterfaceIsApplicable(Tha60210031EthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool InterfaceIsSupported(Tha60210031EthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    if ((interface == cAtEthPortInterfaceXGMii) || (interface == cAtEthPortInterfaceQSgmii))
        return cAtTrue;
    return cAtFalse;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    uint32 regAddr, regVal;

    if (mMethodsGet(mThis(self))->HasQsgmii(mThis(self)))
        return m_ThaEthPortMethods->LoopOutIsEnabled(self);

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mMethodsGet(mThis(self))->XgmiiDefaultOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cThaIpEthTripSmacLoopoutMask) ? cAtTrue : cAtFalse;
    }

static eBool LoopInIsEnabled(ThaEthPort self)
    {
    uint32 regAddr, regVal;

    if (mMethodsGet(mThis(self))->HasQsgmii(mThis(self)))
        return m_ThaEthPortMethods->LoopInIsEnabled(self);

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mMethodsGet(mThis(self))->XgmiiDefaultOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cThaIpEthTripSmacLoopinMask) ? cAtTrue : cAtFalse;
    }

static uint8 DefaultTxIpgGet(ThaEthPort10Gb self)
    {
    AtUnused(self);
    return 12;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, QueueCurrentBandwidthInBpsGet);
        mMethodOverride(m_AtEthPortOverride, QueueAllPwsGet);
        mMethodOverride(m_AtEthPortOverride, MaxQueuesGet);
        mMethodOverride(m_AtEthPortOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtEthPortOverride, RxErrorGeneratorGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(ThaEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, LoopInEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        mMethodOverride(m_ThaEthPortOverride, MaxBandwidthInKbps);
        mMethodOverride(m_ThaEthPortOverride, RemainingBpsOfMaxBandwidth);
        }

    mMethodsSet(self, &m_ThaEthPortOverride);
    }

static void OverrideThaEthPort10Gb(AtEthPort self)
    {
    ThaEthPort10Gb port10gb = (ThaEthPort10Gb)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_ThaEthPort10GbMethods = mMethodsGet(port10gb);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPort10GbOverride, m_ThaEthPort10GbMethods, sizeof(m_ThaEthPort10GbOverride));
        mMethodOverride(m_ThaEthPort10GbOverride, DefaultTxIpgGet);
        }

    mMethodsSet(port10gb, &m_ThaEthPort10GbOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort((ThaEthPort)self);
    OverrideThaEthPort10Gb(self);
    }

static void MethodsInit(AtEthPort self)
    {
    Tha60210031EthPort port = mThis(self);

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, XgmiiDefaultOffset);
        mMethodOverride(m_methods, HasQsgmii);
        mMethodOverride(m_methods, InterfaceIsSupported);
        mMethodOverride(m_methods, EpEthPortInterfaceIsApplicable);
        }

    mMethodsSet(port, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031EthPort);
    }

AtEthPort Tha60210031EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011EthPort10GbObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210031EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031EthPortObjectInit(newPort, portId, module);
    }

eBool Tha60210031EthPortIsQsgmii(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) != cAtEthPortInterfaceQSgmii)
        return cAtFalse;

    return cAtTrue;
    }

static uint8 QueueWithMinimumInUsedBandwidth(AtEthPort self)
    {
    uint8 queue_i, queueWithMinBw;
    uint32 minInUsedBw = mThis(self)->laneBandwidthInBps[0];
    queueWithMinBw = 0;

    for (queue_i = 1; queue_i < cMaxNumQsgmiiLanes; queue_i++)
        {
        if (minInUsedBw > mThis(self)->laneBandwidthInBps[queue_i])
            {
            minInUsedBw = mThis(self)->laneBandwidthInBps[queue_i];
            queueWithMinBw = queue_i;
            }
        }

    return queueWithMinBw;
    }

uint8 Tha60210031EthPortQueueIdDynamicAllocate(AtEthPort self, uint32 pwBandwidth)
    {
    uint8 queueWithMinBw = QueueWithMinimumInUsedBandwidth(self);
    if (QueueBandwidthIsAvailable(self, queueWithMinBw, 0, pwBandwidth))
        return queueWithMinBw;

    return cAtInvalidPwEthPortQueueId;
    }

eAtRet Tha60210031EthPortQueuePwAdd(AtEthPort self, uint8 queueId, AtPw adapter, uint32 newPwBandwidth)
    {
    if (!QueueIdIsValid(queueId))
        return cAtErrorInvlParm;

    QueueBandwidthAdd(self, queueId, newPwBandwidth);
    QueuePwListAdd(self, queueId, ThaPwAdapterPwGet((ThaPwAdapter)adapter));

    return cAtOk;
    }

eAtRet Tha60210031EthPortQueuePwRemove(AtEthPort self, uint8 queueId, AtPw adapter)
    {
    if (!QueueIdIsValid(queueId))
        return cAtErrorInvlParm;

    QueueBandwidthRemove(self, queueId, (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(adapter));
    QueuePwListRemove(self, queueId, ThaPwAdapterPwGet((ThaPwAdapter)adapter));

    return cAtOk;
    }

eBool Tha60210031EthPortQueueBandwidthIsAvailable(AtEthPort self, uint8 queueId, uint32 pwBandwidth)
    {
    if (!QueueIdIsValid(queueId))
        return cAtFalse;

    if (ThaModulePwResourcesLimitationIsDisabled((ThaModulePw)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePw)))
        return cAtTrue;

    return QueueBandwidthIsAvailable(self, queueId, 0, pwBandwidth);
    }

eBool Tha60210031EthPortQueueBandwidthCanAdjust(AtEthPort self, uint8 queueId, uint32 oldBandwidth, uint32 newBandwidth)
    {
    if (!QueueIdIsValid(queueId))
        return cAtFalse;

    if (oldBandwidth >= newBandwidth)
        return cAtTrue;

    return QueueBandwidthIsAvailable(self, queueId, oldBandwidth, newBandwidth);
    }

eAtRet Tha60210031EthPortQueueBandwidthAdjust(AtEthPort self, uint8 queueId, uint32 oldBandwidth, uint32 newBandwidth)
    {
    if (!QueueIdIsValid(queueId))
        return cAtErrorInvlParm;

    if (oldBandwidth == newBandwidth)
        return cAtOk;

    QueueBandwidthAdd(self, queueId, newBandwidth);
    QueueBandwidthRemove(self, queueId, oldBandwidth);

    return cAtOk;
    }

uint32 Tha60210031EthPortQueuePwRemainingBandwidthInBpsGet(AtEthPort self, uint8 queueId, uint32 removeBwInBps)
    {
    uint32 queueBwAfterRemoving;
    if (!QueueIdIsValid(queueId))
        return 0;

    queueBwAfterRemoving = QueueBandwidthTryRemoving(self, queueId, removeBwInBps);
    return QueueMaxBandwidthInBps() - queueBwAfterRemoving;
    }

uint32 Tha60210031EthPortQueuesMaxRemainingBandwidthInBpsGet(AtEthPort self, uint8 queueId, uint32 removeBwInBps)
    {
    uint32 maxRemaining = Tha60210031EthPortQueuePwRemainingBandwidthInBpsGet(self, queueId, removeBwInBps);
    uint8 queue_i;

    for (queue_i = 0; queue_i < cMaxNumQsgmiiLanes; queue_i++)
        {
        uint32 queueRemaining;
        if (queue_i == queueId)
            continue;

        queueRemaining = QueueMaxBandwidthInBps() - mThis(self)->laneBandwidthInBps[queue_i];
        if (queueRemaining > maxRemaining)
            maxRemaining = queueRemaining;
        }

    return maxRemaining;
    }

eAtRet Tha60210031EthPortBoundPwAdd(AtEthPort self, AtPw pw)
    {
    return AtListObjectAdd(PwList(self), (AtObject)pw);
    }

eAtRet Tha60210031EthPortBoundPwRemove(AtEthPort self, AtPw pw)
    {
    return AtListObjectRemove(PwList(self), (AtObject)pw);
    }

AtErrorGenerator Tha60210031EthPortErrorGeneratorGet(AtEthPort self, eAtSide side)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtChannelDeviceGet((AtChannel)self)))
        return NULL;

    if (mThis(self)->errorGenerator == NULL)
        mThis(self)->errorGenerator = Tha60210031EthPortErrorGeneratorNew((AtChannel)self);

    if (AtErrorGeneratorIsStarted(mThis(self)->errorGenerator))
        {
        if (Tha60210031EthPortErrorGeneratorSideGet(mThis(self)->errorGenerator) != side)
            return NULL;
        }

    Tha60210031EthPortErrorGeneratorSideSet(mThis(self)->errorGenerator, side);
    return mThis(self)->errorGenerator;
    }

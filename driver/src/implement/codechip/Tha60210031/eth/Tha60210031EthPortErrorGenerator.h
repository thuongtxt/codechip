/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210031EthPortErrorGenerator.h
 * 
 * Created Date: Mar 28, 2016
 *
 * Description : Interface of the Tha60210031EthPortTxErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHPORTERRORGENERATOR_H_
#define _THA60210031ETHPORTERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031EthPortErrorGenerator  *Tha60210031EthPortErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60210031EthPortErrorGeneratorNew(AtChannel channel);
void Tha60210031EthPortErrorGeneratorSideSet(AtErrorGenerator self, eAtSide isTx);
eAtSide Tha60210031EthPortErrorGeneratorSideGet(AtErrorGenerator self);
void Tha60210031EthPortErrorGeneratorHwDefaultSet(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHPORTERRORGENERATOR_H_ */


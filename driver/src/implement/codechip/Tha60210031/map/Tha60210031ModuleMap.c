/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210031ModuleMap.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031ModuleMapDemap.h"
#include "../pw/Tha60210031ModulePw.h"
/*--------------------------- Define -----------------------------------------*/
#define cThaMapFirstTsMask     cBit15
#define cThaMapFirstTsShift    15

#define cThaMapChnTypeMask     cBit14_12
#define cThaMapChnTypeShift    12

#define cThaMapTsEnMask        cBit11
#define cThaMapTsEnShift       11

#define cThaMapPWIdFieldMask   cBit10_0
#define cThaMapPWIdFieldShift  0

/*--------------------------------------
 BitField Name: cThaMapTmDs1LoopcodeMask
 BitField Type: R/W
 BitField Desc: bit [19:17] MapDs1Loopcode[2:0]:
 BitField Bits: 19:17
 --------------------------------------*/
#define cThaMapTmDs1LoopcodeMask                     cBit19_17
#define cThaMapTmDs1LoopcodeShift                    17


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleMapMethods m_ThaModuleMapOverride;

/* Save super implementations */
static const tThaModuleMapMethods *m_ThaModuleMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)
mDefineMaskShift(ThaModuleMap, MapTmDs1Loopcode)

static AtModulePw PwModule(ThaModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eBool BitFieldsNeedShiftUp(ThaModuleAbstractMap self)
    {
    Tha60210031ModulePw pwModule = (Tha60210031ModulePw)PwModule((ThaModuleMap)self);
    uint32 maxPws = Tha60210031ModulePwMaxNumPwsPerSts24Slice(pwModule);
    uint32 originalMaxPws = Tha60210031ModulePwOriginalNumPwsPerSts24Slice(pwModule);
    return (maxPws > originalMaxPws) ? cAtTrue : cAtFalse;
    }

static void OverrideThaModuleMap(ThaModuleMap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, m_ThaModuleMapMethods, sizeof(m_ThaModuleMapOverride));

        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapPWIdField)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1Loopcode)
        }

    mMethodsSet(self, &m_ThaModuleMapOverride);
    }

static void Override(ThaModuleMap self)
    {
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleMap);
    }

AtModule Tha60210031ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaModuleMap)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleMapObjectInit(newModule, device);
    }

eBool Tha60210031ModuleMapBitFieldsNeedShiftUp(ThaModuleAbstractMap self)
    {
    if (self)
        return BitFieldsNeedShiftUp(self);
    return cAtFalse;
    }

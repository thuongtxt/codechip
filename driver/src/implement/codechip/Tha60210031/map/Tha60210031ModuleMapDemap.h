/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60210031ModuleMapInternal.h
 * 
 * Created Date: Jun 11, 2016
 *
 * Description : Internal data and methods of the MAP/DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEMAPINTERNAL_H_
#define _THA60210031MODULEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"
#include "../../../default/util/ThaUtil.h"
#include "../../Tha60150011/map/Tha60150011ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleMap *Tha60210031ModuleMap;
typedef struct tTha60210031ModuleDemap *Tha60210031ModuleDemap;

typedef struct tTha60210031ModuleMap
    {
    tTha60150011ModuleMap super;
    }tTha60210031ModuleMap;

typedef struct tTha60210031ModuleDemap
    {
    tTha60150011ModuleDemap super;
    }tTha60210031ModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210031ModuleMapObjectInit(AtModule self, AtDevice device);
AtModule Tha60210031ModuleDemapObjectInit(AtModule self, AtDevice device);
eBool  Tha60210031ModuleDemapBitFieldsNeedShiftUp(ThaModuleAbstractMap self);
eBool  Tha60210031ModuleMapBitFieldsNeedShiftUp(ThaModuleAbstractMap self);
#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEMAPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60210031ModuleDemap.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : DEMAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleDemapInternal.h"
#include "../../../default/util/ThaUtil.h"
#include "../../Tha60150011/map/Tha60150011ModuleMapDemap.h"
#include "Tha60210031ModuleMapDemap.h"
#include "../pw/Tha60210031ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaDmapFirstTsMask     cBit15
#define cThaDmapFirstTsShift    15
#define cThaDmapChnTypeMask     cBit14_12
#define cThaDmapChnTypeShift    12
#define cThaDmapTsEnMask        cBit11
#define cThaDmapTsEnShift       11
#define cThaDmapPwIdMask        cBit10_0
#define cThaDmapPwIdShift       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleDemapMethods m_ThaModuleDemapOverride;

/* Save super implementations */
static const tThaModuleDemapMethods *m_ThaModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static AtModulePw PwModule(ThaModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap mapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, m_ThaModuleDemapMethods, sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapChnType)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapFirstTs)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapTsEn)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapPwId)
        }

    mMethodsSet(mapModule, &m_ThaModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleDemap);
    }

AtModule Tha60210031ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleDemapObjectInit(newModule, device);
    }

static eBool BitFieldsNeedShiftUp(ThaModuleAbstractMap self)
    {
    Tha60210031ModulePw pwModule = (Tha60210031ModulePw)PwModule((ThaModuleMap)self);
    uint32 maxPws = Tha60210031ModulePwMaxNumPwsPerSts24Slice(pwModule);
    uint32 originalMaxPws = Tha60210031ModulePwOriginalNumPwsPerSts24Slice(pwModule);
    return (maxPws > originalMaxPws) ? cAtTrue : cAtFalse;
    }

eBool Tha60210031ModuleDemapBitFieldsNeedShiftUp(ThaModuleAbstractMap self)
    {
    if (self)
        return BitFieldsNeedShiftUp(self);
    return cAtFalse;
    }

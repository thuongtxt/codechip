/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210031De3CdrControllerInternal.h
 * 
 * Created Date: Sep 21, 2016
 *
 * Description : DE3 CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031DE3CDRCONTROLLERINTERNAL_H_
#define _THA60210031DE3CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/controllers/ThaVcDe3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031De3CdrController
    {
    tThaVcDe3CdrController super;
    }tTha60210031De3CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha60210031De3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031DE3CDRCONTROLLERINTERNAL_H_ */


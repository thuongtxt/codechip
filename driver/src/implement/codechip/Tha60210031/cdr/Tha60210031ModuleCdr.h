/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210031ModuleCdr.h
 * 
 * Created Date: Jun 25, 2014
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60210031MODULECDR_H_
#define _Tha60210031MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/ThaModuleCdr.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha60210031HoVcCdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _Tha60210031MODULECDR_H_ */


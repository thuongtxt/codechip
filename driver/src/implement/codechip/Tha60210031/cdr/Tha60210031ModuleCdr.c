/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210031ModuleCdr.c
 *
 * Created Date: Apr 24, 2015
 *
 * Description : CDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../../default/cdr/ThaModuleCdrStmReg.h"
#include "../../../default/cdr/ThaModuleCdrReg.h"
#include "Tha60210031ModuleCdrInternal.h"
#include "../../Tha60150011/cdr/Tha60150011ModuleCdrInternal.h"
#include "../../Tha60210011/cdr/Tha60210011ModuleCdr.h"
#include "Tha60210031ModuleCdr.h"
#include "Tha60210031CdrReg.h"
#include "Tha60210031CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Mask             cBit17_0
#define cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Shift            0
#define cAf6_ThaDcrRtpFreqCfgMask                                cBit17_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods        m_AtModuleOverride;
static tThaModuleCdrMethods    m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods m_ThaModuleCdrStmOverride;

/* Save super implementation */
static const tThaModuleCdrMethods    *m_ThaModuleCdrMethods = NULL;
static const tThaModuleCdrStmMethods *m_ThaModuleCdrStmMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaCdrController VcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    AtSdhVc vc = AtPdhChannelVcInternalGet((AtPdhChannel)de3);
    return Tha60210031De3CdrControllerNew(ThaModuleCdrStmEngineIdOfHoVc(self, vc), (AtChannel)de3);
    }

static ThaCdrController De3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    return Tha60210031De3CdrControllerNew(ThaModuleCdrStmEngineIdOfDe3(self, de3), (AtChannel)de3);
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    return Tha60210031HoVcCdrControllerNew(ThaModuleCdrStmEngineIdOfHoVc(self, vc), (AtChannel)vc);
    }

static eBool NeedActivateRxEngine(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 EngineTimingOffsetByHwSts(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    uint32 cdrId = ThaModuleCdrStmSdhVcCdrIdCalculate(self, channel, slice, hwStsId, vtgId, vtId);
    uint32 sliceOffset = ThaModuleCdrStmSliceOffset(self, channel, slice);

    return sliceOffset + cdrId;
    }

static eBool OnlySupportPrcForDcr(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultExtFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x30D4;
    }

static uint32 DefaultDcrClockFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 25000;
    }

static eBool NeedToSetExtDefaultFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ExtRefAndPrcCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x280003;
    }

static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x03, 0x39);
    }

static eAtClockState ClockStateFromHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    AtUnused(self);

    if (hwState == 2)
        return cAtClockStateInit;

    if ((hwState <= 1) || (hwState == 7))
        return cAtClockStateHoldOver;

    if (hwState == 5)
        return cAtClockStateLocked;

    return cAtClockStateLearning;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    if (ThaModuleCdrInterruptIsSupported((ThaModuleCdr)self))
        return Tha60210031CdrInterruptManagerNew(self);
    return NULL;
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    AtUnused(self);
    return Tha60210031CdrDebuggerNew();
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedJaVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4631);

    if (hwVersion >= supportedJaVersion)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 DcrClockFrequencyRegMask(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Mask;
    }

static uint32 DcrRtpTimestampFrequencyRegMask(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6_ThaDcrRtpFreqCfgMask;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, VcDe3CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, De3CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, NeedActivateRxEngine);
        mMethodOverride(m_ThaModuleCdrOverride, OnlySupportPrcForDcr);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultDcrClockFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultExtFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, NeedToSetExtDefaultFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, ExtRefAndPrcCtrlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, ClockStateFromHwStateGet);
        mMethodOverride(m_ThaModuleCdrOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModuleCdrOverride, ShouldEnableJitterAttenuator);
        mMethodOverride(m_ThaModuleCdrOverride, DcrClockFrequencyRegMask);
        mMethodOverride(m_ThaModuleCdrOverride, DcrRtpTimestampFrequencyRegMask);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm cdrModule = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, EngineTimingOffsetByHwSts);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrStmOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleCdr);
    }

AtModule Tha60210031ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleCdrObjectInit(newModule, device);
    }

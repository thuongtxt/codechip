/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210031HoVcCdrController.c
 *
 * Created Date: Nov 23, 2015
 *
 * Description : CDR controller for AU-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/controllers/ThaHoVcCdrControllerInternal.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "Tha60210031HoVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaVC3STSTimingMask                            cBit11_8
#define cThaVC3STSTimingShift                           8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet StsTimingModeSet(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaCdrController controller = (ThaCdrController)self;
    uint32 regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(controller) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaVC3TimeModeMask, cThaVC3TimeModeShift, ThaCdrControllerTimingModeSw2Hw(controller, timingMode, timingSource));

    /*
     * Default is System mode for VC3STSTiming, timing generate for VC3 when mapping from DS3.E3 LIU
     * */
    mFieldIns(&regVal, cThaVC3STSTimingMask, cThaVC3STSTimingShift, 0);
    ThaCdrControllerWrite(controller, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static void OverrideThaSdhChannelCdrController(ThaHoVcCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, mMethodsGet(controller), sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, StsTimingModeSet);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void Override(ThaHoVcCdrController self)
    {
    OverrideThaSdhChannelCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031HoVcCdrController);
    }

ThaCdrController Tha60210031HoVcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHoVcCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaHoVcCdrController)self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60210031HoVcCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031HoVcCdrControllerObjectInit(newController, engineId, channel);
    }

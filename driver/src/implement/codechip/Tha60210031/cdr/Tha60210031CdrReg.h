/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : CDR
 *                                                                              
 * File        : Tha60210031CdrReg.h
 *                                                                              
 * Created Date: Dec 12, 2015
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0031_RD_CDR_H_
#define _AF6_REG_AF6CCI0031_RD_CDR_H_

/*--------------------------- Define -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Enable Control
Reg Addr   : 0x00025000-0x000253FF
Reg Formula: 0x00025000 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt enable of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_Base                                                       0x00025000
#define cAf6Reg_cdr_per_chn_intr_en_ctrl(StsID, VtnID)                         (0x00025000+(StsID)*32+(VtnID))
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_WidthVal                                                           32
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CDRUnlokcedIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change UnLocked te event to generate an
interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Bit_Start                                            0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Bit_End                                              0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Mask                                             cBit0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Shift                                                0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_MaxVal                                             0x1
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_MinVal                                             0x0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Status
Reg Addr   : 0x00025400-0x000257FF
Reg Formula: 0x00025400 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_stat_Base                                                          0x00025400
#define cAf6Reg_cdr_per_chn_intr_stat(StsID, VtnID)                            (0x00025400+(StsID)*32+(VtnID))
#define cAf6Reg_cdr_per_chn_intr_stat_WidthVal                                                              32
#define cAf6Reg_cdr_per_chn_intr_stat_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: CDRUnLockedIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in UnLocked the event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Bit_Start                                                 0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Bit_End                                                   0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Mask                                                  cBit0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Shift                                                     0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_MaxVal                                                  0x1
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_MinVal                                                  0x0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Current Status
Reg Addr   : 0x00025800-0x00025BFF
Reg Formula: 0x00025800 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel Current tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_curr_stat_Base                                                          0x00025800
#define cAf6Reg_cdr_per_chn_curr_stat(StsID, VtnID)                            (0x00025800+(StsID)*32+(VtnID))
#define cAf6Reg_cdr_per_chn_curr_stat_WidthVal                                                              32
#define cAf6Reg_cdr_per_chn_curr_stat_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: CDRUnLockedCurrSta
BitField Type: RW
BitField Desc: Current tus of UnLocked event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Bit_Start                                              0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Bit_End                                                0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Mask                                               cBit0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Shift                                                  0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_MaxVal                                               0x1
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_MinVal                                               0x0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt OR Status
Reg Addr   : 0x00025C00-0x00025C1F
Reg Formula: 0x00025C00 +  StsID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_or_stat_Base                                                       0x00025C00
#define cAf6Reg_cdr_per_chn_intr_or_stat(StsID)                                           (0x00025C00+(StsID))
#define cAf6Reg_cdr_per_chn_intr_or_stat_WidthVal                                                           32
#define cAf6Reg_cdr_per_chn_intr_or_stat_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CDRVtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding DS1/E1 is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Bit_Start                                               0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Bit_End                                                31
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Mask                                             cBit31_0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Shift                                                   0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_MaxVal                                         0xffffffff
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_MinVal                                                0x0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt OR Status
Reg Addr   : 0x00025FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_Base                                                     0x00025FFF
#define cAf6Reg_cdr_per_stsvc_intr_or_stat                                                          0x00025FFF
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_WidthVal                                                         32
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: CDRStsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding STS/VC is
set and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Bit_Start                                            0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Bit_End                                             31
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Mask                                          cBit31_0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Shift                                                0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_MaxVal                                      0xffffffff
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_MinVal                                             0x0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt Enable Control
Reg Addr   : 0x00025FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base                                                     0x00025FFE
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl                                                          0x00025FFE
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_WidthVal                                                         32
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: CDRStsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Bit_Start                                               0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Bit_End                                                31
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Mask                                             cBit31_0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Shift                                                   0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_MaxVal                                         0xffffffff
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_MinVal                                                0x0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_RstVal                                                0x0

#endif /* _AF6_REG_AF6CCI0031_RD_CDR_H_ */

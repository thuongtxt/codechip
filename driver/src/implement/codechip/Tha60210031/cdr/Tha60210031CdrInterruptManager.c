/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210031ModuleCdrInterruptManager.c
 *
 * Created Date: Dec 12, 2015
 *
 * Description : CDR module interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "AtPdhSerialLine.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../../default/pdh/ThaPdhDe3SerialLine.h"
#include "../pdh/Tha60210031ModulePdh.h"
#include "Tha60210031CdrInterruptManager.h"
#include "Tha60210031CdrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods   m_ThaCdrInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePdh PdhModule(AtInterruptManager self)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet(self);
    AtDevice device = AtModuleDeviceGet(cdrModule);
    return (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 numSlices = mMethodsGet(mIntrManager(self))->NumSlices(mIntrManager(self));
    uint32 intrSliceOR = mMethodsGet(mIntrManager(self))->SlicesInterruptGet(mIntrManager(self), glbIntr, hal);
    uint8 slice, numStsPerSlice;
    ThaModulePdh pdhModule = (ThaModulePdh)PdhModule(self);

    numStsPerSlice = ThaModulePdhNumStsInSlice(pdhModule);
    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrSliceOR & cIteratorMask(slice))
            {
            uint32 sliceOffset = mMethodsGet(mIntrManager(self))->SliceOffset(mIntrManager(self), slice);
            uint32 sliceBase = baseAddress + sliceOffset;
            uint32 intrMask = AtHalRead(hal, sliceBase + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base);
            uint32 intrStatus = AtHalRead(hal, sliceBase + cAf6Reg_cdr_per_stsvc_intr_or_stat_Base);
            uint32 stsInSlice;

            intrStatus &= intrMask;

            for (stsInSlice = 0; stsInSlice < numStsPerSlice; stsInSlice++)
                {
                if (intrStatus & cIteratorMask(stsInSlice))
                    {
                    uint32 intrStatus32 = AtHalRead(hal, sliceBase + cAf6Reg_cdr_per_chn_intr_or_stat(stsInSlice));
                    uint8 vtInSts;

                    for (vtInSts = 0; vtInSts < 28; vtInSts++)
                        {
                        if (intrStatus32 & cIteratorMask(vtInSts))
                            {
                            uint32 offset = sliceOffset + (stsInSlice << 5) + vtInSts;
                            ThaCdrController controller = ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet((ThaCdrInterruptManager)self, slice, (uint8)stsInSlice, vtInSts);
                            ThaCdrControllerInterruptProcess(controller, offset);
                            }
                        }
                    }
                }
            }
        }
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtModule module = AtInterruptManagerModuleGet(self);
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 numSlices = mMethodsGet(mIntrManager(self))->NumSlices(mIntrManager(self));
    uint8 sliceId;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = baseAddress + mMethodsGet(mIntrManager(self))->SliceOffset(mIntrManager(self), sliceId);
        AtModuleHwWrite(module, sliceBase + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base, (enable) ? cBit23_0 : 0, hal);
        }
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x25800;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x25400;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x25000;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cBit29_28) ? cAtTrue : cAtFalse;
    }

static uint32 SliceOffset(ThaInterruptManager self, uint8 sliceId)
    {
    AtUnused(self);
    return (uint32)(sliceId * 0x40000);
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 SlicesInterruptGet(ThaInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cBit29_28) >> 28;
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtDevice device = AtModuleDeviceGet(cdrModule);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint32 serialId = Tha6021ModulePdhSerialIdFromHwIdGet(pdhModule, slice, sts);
    return Tha6021CdrControllerFromSerialLineIdGet(self, serialId, vt);
    }

static ThaCdrController CdrControllerFromSerialLineIdGet(ThaCdrInterruptManager self, uint32 serialId, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtDevice device = AtModuleDeviceGet(cdrModule);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtPdhDe3 de3 = NULL;
    uint8 vtgId, vtId;
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet((AtModulePdh)pdhModule, serialId);

    if (serialLine == NULL)
        return NULL;

    vtgId = vt >> 2;
    vtId  = vt & 0x3;

    if (AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeEc1)
        {
        AtSdhLine ec1 = ThaPdhDe3SerialLineEc1ObjectGet((ThaPdhDe3SerialLine)serialLine);
        AtSdhChannel au3 = AtSdhChannelSubChannelGet((AtSdhChannel)ec1, 0);
        AtSdhChannel vc3;
        uint8 mapType;

        if (au3 == NULL)
            return NULL;

        vc3 = AtSdhChannelSubChannelGet((AtSdhChannel)au3, 0);
        mapType = AtSdhChannelMapTypeGet(vc3);

        if (mapType == cAtSdhVcMapTypeVc3MapC3)
            return ThaSdhVcCdrControllerGet((AtSdhVc)vc3);

        if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
            {
            AtSdhChannel tug2 = AtSdhChannelSubChannelGet(vc3, vtgId);
            AtSdhChannel vc1x = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug2, vtId), 0);

            if (AtSdhChannelMapTypeGet(vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
                return ThaPdhDe1CdrControllerGet((ThaPdhDe1)AtSdhChannelMapChannelGet(vc1x));
            return ThaSdhVcCdrControllerGet((AtSdhVc)vc1x);
            }

        if (mapType == cAtSdhVcMapTypeVc3MapDe3)
            de3 = (AtPdhDe3)AtSdhChannelMapChannelGet(vc3);
        }

    else
        de3 = Tha6021ModulePdhDe3FromSerialIdGet(pdhModule, serialId);

    if (de3 == NULL)
        return NULL;

    /* There is case DE3 channelized but it is bound pw */
    if (AtChannelBoundPwGet((AtChannel)de3) || !AtPdhDe3IsChannelized(de3))
        return ThaPdhDe3CdrControllerGet((ThaPdhDe3)de3);

    ThaPdhDe3De1Hw2SwIdGet((ThaPdhDe3)de3, vtgId, vtId, &vtgId, &vtId);
    return ThaPdhDe1CdrControllerGet((ThaPdhDe1)AtPdhDe3De1Get(de3, vtgId, vtId));
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, SliceOffset);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, SlicesInterruptGet);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, AcrDcrControllerFromHwIdGet);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031CdrInterruptManager);
    }

AtInterruptManager Tha60210031CdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210031CdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031CdrInterruptManagerObjectInit(newManager, module);
    }

ThaCdrController Tha6021CdrControllerFromSerialLineIdGet(ThaCdrInterruptManager self, uint32 serialId, uint8 vt)
    {
    if (self)
        return CdrControllerFromSerialLineIdGet(self, serialId, vt);
    return NULL;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : INTERRUPT
 * 
 * File        : Tha60210031ModuleCdrInterruptManager.h
 * 
 * Created Date: Dec 12, 2015
 *
 * Description : CDR module interrupt manager for product 60210031.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031CDRINTERRUPTMANAGER_H_
#define _THA60210031CDRINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/ThaCdrInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031CdrInterruptManager
    {
    tThaCdrInterruptManager super;
    }tTha60210031CdrInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60210031CdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module);
AtInterruptManager Tha60210031CdrInterruptManagerNew(AtModule module);

ThaCdrController Tha6021CdrControllerFromSerialLineIdGet(ThaCdrInterruptManager self, uint32 serialId, uint8 vt);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031CDRINTERRUPTMANAGER_H_ */


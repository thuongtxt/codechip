/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210031De3CdrController.c
 *
 * Created Date: Apr 24, 2015
 *
 * Description : DE3 CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../Tha60150011/cdr/Tha60150011ModuleCdrInternal.h"
#include "Tha60210031De3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/* Super implement */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PartGet(ThaCdrController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    uint8 slice = 0, sts = 0;
    AtChannel channel = ThaCdrControllerChannelGet(self);

    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &sts);

    return sts + (slice * 0x40000UL) + ThaCdrControllerPartOffset(self);
    }

static uint32 DefaultOffset(ThaCdrController self)
    {
    uint8 slice = 0, sts = 0;
    AtChannel channel = ThaCdrControllerChannelGet(self);

    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &sts);

    return ((slice * 0x40000UL) + ThaCdrControllerIdGet(self));
    }

static uint32 ThaRegCDRStsTimeCtrl(ThaCdrController self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->StsTimingControlReg(moduleCdr, NULL);

    return 0;
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    uint32 regAddr = ThaRegCDRStsTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaMapStsMdMask, cThaMapStsMdShift, 1);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, DefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, PartGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031De3CdrController);
    }

ThaCdrController Tha60210031De3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcDe3CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60210031De3CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031De3CdrControllerObjectInit(newController, engineId, channel);
    }

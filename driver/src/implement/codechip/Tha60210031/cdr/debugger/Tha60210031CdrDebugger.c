/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210031CdrDebugger.c
 *
 * Created Date: Nov 2, 2016
 *
 * Description : CDR debugger of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60150011/cdr/debugger/Tha60150011CdrDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031CdrDebugger
    {
    tTha60150011CdrDebugger super;
    }tTha60210031CdrDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrDebuggerMethods m_ThaCdrDebuggerOverride;

static const tThaCdrDebuggerMethods *m_ThaCdrDebuggerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool  HoldOverPerChannelIsSupported(ThaCdrDebugger self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaCdrDebugger(ThaCdrDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrDebuggerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrDebuggerOverride, m_ThaCdrDebuggerMethods, sizeof(m_ThaCdrDebuggerOverride));

        mMethodOverride(m_ThaCdrDebuggerOverride, HoldOverPerChannelIsSupported);
        }

    mMethodsSet(self, &m_ThaCdrDebuggerOverride);
    }

static void Override(ThaCdrDebugger self)
    {
    OverrideThaCdrDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031CdrDebugger);
    }

static ThaCdrDebugger ObjectInit(ThaCdrDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011CdrDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrDebugger Tha60210031CdrDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrDebugger newDebugger = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDebugger == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDebugger);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210031SdhVcInternal.h
 * 
 * Created Date: Sep 21, 2015
 *
 * Description : SDH VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SDHVCINTERNAL_H_
#define _THA60210031SDHVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAuVcInternal.h"
#include "Tha60210031SdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SdhVc *Tha60210031SdhVc;

typedef struct tTha60210031SdhVc
    {
    tTha60210011Tfi5LineAuVc super;
    }tTha60210031SdhVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210031SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210031SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210031SdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SDHVCINTERNAL_H_ */


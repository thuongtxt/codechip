/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210031SdhVc.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : SDH VC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/map/ThaModuleStmMap.h"
#include "../../../default/map/ThaModuleDemap.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../ocn/Tha60210031ModuleOcnReg.h"
#include "../pdh/Tha60210031ModulePdh.h"
#include "../pdh/Tha60210031PdhSerialLine.h"
#include "../pdh/Tha60210031PdhDe3.h"
#include "Tha60210031SdhVcInternal.h"
#include "../ocn/Tha60210031ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031SdhVc)(self))
/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tAtChannelMethods               m_AtChannelOverride;
static tAtSdhChannelMethods            m_AtSdhChannelOverride;
static tAtSdhVcMethods                 m_AtSdhVcOverride;
static tThaSdhVcMethods                m_ThaSdhVcOverride;
static tThaSdhAuVcMethods              m_ThaSdhAuVcOverride;
static tTha60210011Tfi5LineAuVcMethods m_Tha60210011Tfi5LineAuVcOverride;

/* Save super implementation */
static const tAtObjectMethods         *m_AtObjectMethods     = NULL;
static const tAtChannelMethods        *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods     *m_AtSdhChannelMethods = NULL;
static const tAtSdhVcMethods          *m_AtSdhVcMethods      = NULL;
static const tThaSdhVcMethods         *m_ThaSdhVcMethods     = NULL;

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    uint32 regAddr;
    uint32 regVal;

    if (errorType != cAtSdhPathErrorBip)
        return cAtErrorModeNotSupport;

    regAddr = cAf6Reg_spgramctl_Base + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPiB3BipErrFrc_, 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    uint32 regAddr;
    uint32 regVal;

    if (errorType != cAtSdhPathErrorBip)
        return cAtOk;

    regAddr = cAf6Reg_spgramctl_Base + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spgramctl_StsPiB3BipErrFrc_, 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    uint32 regAddr;
    uint32 regVal;

    regAddr = cAf6Reg_spgramctl_Base + Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (mRegField(regVal, cAf6_spgramctl_StsPiB3BipErrFrc_) == 1) ? cAtSdhPathErrorBip : cAtSdhPathErrorNone;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathErrorBip;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return Tha6021OcnTuVcRxAlarmForce(self, alarmType);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return Tha6021OcnTuVcRxAlarmUnForce(self, alarmType);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    return Tha6021OcnTuVcRxForcedAlarmGet(self);
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    return Tha6021OcnTuVcRxForcibleAlarms(self);
    }

static eBool OnlySupportXcLoopback(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedManageLoLineSts(Tha60210011Tfi5LineAuVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(self);
    AtUnused(mapType);
    return cAtOk;
    }

static eBool NeedConfigurePdh(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isIncrease)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isIncrease) ? 0 : 1;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice);
    return (uint32)(512UL * slice + 32UL * adjMode + hwStsInSlice + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtChannel)self)));
    }

static eAtRet Debug(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    if (ThaOcnVc3MapDe3LiuIsEnabled((AtSdhChannel)self))
        AtPrintc(cSevInfo, " LIU Over SONET is Enable! \r\n");
    else
        AtPrintc(cSevInfo, " LIU Over SONET is Disable! \r\n");
    return ret;
    }

static void LiuOverVc3MapUpdate(AtSdhChannel self)
    {
    uint8 lineId = AtSdhChannelLineGet(self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    AtPdhSerialLine serLine = AtModulePdhDe3SerialLineGet(pdhModule, lineId);
    eAtPdhDe3SerialLineMode mode = AtPdhSerialLineModeGet(serLine);

    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        Tha60210031PdhSerLineStsMapDe3Update(serLine, mode);
    }

static eAtRet PwCepSet(ThaSdhVc self)
    {
    eAtRet ret = m_ThaSdhVcMethods->PwCepSet(self);
    if (ret != cAtOk)
        return ret;

    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)self))
        {
        ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtTrue);
        LiuOverVc3MapUpdate((AtSdhChannel)self);
        }

    return ret;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    return Tha6021OcnTuVcLoopbackIsSupported(self, loopbackMode);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;

    if (AtSdhChannelMapTypeGet(self) != mapType)
        ret = AtChannelLoopbackSet((AtChannel)self, cAtLoopbackModeRelease);

    ret |= m_AtSdhChannelMethods->MapTypeSet(self, mapType);

    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        LiuOverVc3MapUpdate(self);

    return ret;
    }

static ThaModuleDemap ModuleDemap(AtSdhVc self)
    {
    AtDevice dev = AtChannelDeviceGet((AtChannel)self);
    ThaModuleDemap module = (ThaModuleDemap)AtDeviceModuleGet(dev, cThaModuleDemap);
    return module;
    }

static eBool HasOcnStuffing(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4646);

    if (currentVersion >= supportedVersion)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet StuffEnable(AtSdhVc self, eBool enable)
    {
    eAtRet ret;
    ThaSdhVc vc = (ThaSdhVc)self;

    /* Calling super implementation */
    ret = m_AtSdhVcMethods->StuffEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(vc)->HasOcnStuffing(vc))
        ret |= ThaModuleDemapVc3StuffEnable(ModuleDemap(self), self, enable);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, OnlySupportXcLoopback);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, m_AtSdhVcMethods, sizeof(m_AtSdhVcOverride));

        mMethodOverride(m_AtSdhVcOverride, StuffEnable);
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(vc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));
        mMethodOverride(m_ThaSdhVcOverride, HasOcnStuffing);
        mMethodOverride(m_ThaSdhVcOverride, PwCepSet);
        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc sdhAuVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, mMethodsGet(sdhAuVc), sizeof(m_ThaSdhAuVcOverride));

        mMethodOverride(m_ThaSdhAuVcOverride, NeedConfigurePdh);
        }

    mMethodsSet(sdhAuVc, &m_ThaSdhAuVcOverride);
    }

static void OverrideTha60210011Tfi5LineAuVc(AtSdhVc self)
    {
    Tha60210011Tfi5LineAuVc channel = (Tha60210011Tfi5LineAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineAuVcOverride, mMethodsGet(channel), sizeof(m_Tha60210011Tfi5LineAuVcOverride));

        mMethodOverride(m_Tha60210011Tfi5LineAuVcOverride, NeedManageLoLineSts);
        }

    mMethodsSet(channel, &m_Tha60210011Tfi5LineAuVcOverride);
    }

static void Override(AtSdhVc self)
    {
	OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhVc(self);
    OverrideThaSdhVc(self);
    OverrideThaSdhAuVc(self);
    OverrideTha60210011Tfi5LineAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SdhVc);
    }

AtSdhVc Tha60210031SdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    return self;
    }

AtSdhVc Tha60210031SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031SdhVcObjectInit(self, channelId, channelType, module);
    }

void Tha60210031SdhVcLiuOverVc3MapUpdate(AtSdhChannel self)
    {
    if (self)
        LiuOverVc3MapUpdate(self);
    }

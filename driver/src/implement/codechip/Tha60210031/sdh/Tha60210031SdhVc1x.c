/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210031SdhVc.c
 *
 * Created Date: Sep 21, 2015
 *
 * Description : SDH VC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAuVcInternal.h"
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineVc1xInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../pdh/Tha60210031ModulePdhReg.h"
#include "../pdh/Tha60210031ModulePdh.h"
#include "Tha60210031SdhVcInternal.h"
#include "Tha60210031SdhVc1xInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tThaSdhVcMethods                 m_ThaSdhVcOverride;

/* To save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn ModuleOcn(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isIncrease)
    {
    uint8 slice, hwStsInSlice;
    uint8 adjMode = (isIncrease) ? 0 : 1;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(16384UL * slice + 1024UL * adjMode + 32UL * hwStsInSlice + 4UL * vtgId + vtId + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtChannel)self)));

    return cBit31_0;
    }

static eAtRet DisableLiuOverSonet(AtSdhChannel self)
    {
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    return ThaPdhStsVtDe1LiuMapVc1xEnable(pdhModule, self, cAtFalse);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;
    if (AtSdhChannelMapTypeGet(self) != mapType)
        AtChannelLoopbackSet((AtChannel)self, cAtLoopbackModeRelease);

    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* Handle for private this product */
    return DisableLiuOverSonet(self);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc channel = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(channel), sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(channel, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SdhVc1x);
    }

AtSdhVc Tha60210031SdhVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineVc1xObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210031SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031SdhVc1xObjectInit(self, channelId, channelType, module);
    }

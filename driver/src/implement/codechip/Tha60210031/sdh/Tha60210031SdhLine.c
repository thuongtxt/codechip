/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210031SdhLine.c
 *
 * Created Date: Jun 12, 2014
 *
 * Description : SDH Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"
#include "../ocn/Tha60210031ModuleOcnReg.h"
#include "../ocn/Tha60210031ModuleOcn.h"
#include "../pdh/Tha60210031PdhDe3.h"
#include "../man/Tha60210031Device.h"
#include "Tha60210031SdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_tfmramctl_OCNTxK1Pat_Mask                                                               cBit23_16
#define cAf6_tfmramctl_OCNTxK1Pat_Shift                                                                     16

#define cAf6_tfmramctl_OCNTxK2Pat_Mask                                                               cBit15_8
#define cAf6_tfmramctl_OCNTxK2Pat_Shift                                                                     8

#define cNumBitsInDword 32

#define cSdhLineEvent   (cAtSdhLineAlarmS1Change|cAtSdhLineAlarmK1Change|cAtSdhLineAlarmK2Change)

/*--------------------------- Macros -----------------------------------------*/
#define mTtiProcessor(self)     ThaSdhLineTtiProcessor((AtSdhLine)self)
#define mThis(self)             ((Tha60210031SdhLine)self)
#define mChannelOffset(self)    mMethodsGet(mThis(self))->ChannelOffset(mThis(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031SdhLineMethods m_methods;

/* Override */
static tAtChannelMethods     m_AtChannelOverride;
static tAtSdhChannelMethods  m_AtSdhChannelOverride;
static tThaSdhLineMethods    m_ThaSdhLineOverride;
static tAtSdhLineMethods     m_AtSdhLineOverride;

/* Super implementation */
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods = NULL;
static const tAtSdhLineMethods     *m_AtSdhLineMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static uint32 DefectsHw2Sw(AtChannel self, uint32 regVal);

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn ModuleOcn(AtSdhLine self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 DefaultOffset(ThaSdhLine self)
    {
    return ThaModuleOcnLineDefaultOffset(ModuleOcn((AtSdhLine)self), (AtSdhChannel)self);
    }

static eAtSdhLineRate LowRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm0;
    }

static eAtSdhLineRate HighRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm0;
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm0;
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtUnused(self);
    return (rate == cAtSdhLineRateStm0) ? cAtTrue : cAtFalse;
    }

static void InterruptProcess(AtSdhLine self, uint8 line, AtHal hal)
    {
    uint32 baseAddress = mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    uint32 regAddr  = (uint32)mMethodsGet(mThis(self))->RxLineAlarmInterruptStatusRegAddr(mThis(self)) + baseAddress;
    uint32 regVal   = AtHalRead(hal, regAddr);
    uint32 regMask  = AtHalRead(hal, (uint32)mMethodsGet(mThis(self))->RxLineAlarmInterruptEnableControlRegAddr(mThis(self)) + baseAddress);
    uint32 regCur   = AtHalRead(hal, (uint32)mMethodsGet(mThis(self))->RxLineAlarmCurrentStatusRegAddr(mThis(self)) + baseAddress);
    uint32 events   = 0;
    uint32 defects  = 0;
    AtUnused(line);

    regVal &= regMask;

    if (regVal & cAf6_tohintsta_S1StbStateChgIntr_Mask)
        events |= cAtSdhLineAlarmS1Change;

    if (regVal & cAf6_tohintsta_K1StbStateChgIntr_Mask)
        events |= cAtSdhLineAlarmK1Change;

    if (regVal & cAf6_tohintsta_K2StbStateChgIntr_Mask)
        events |= cAtSdhLineAlarmK2Change;

    if (regVal & cAf6_tohintsta_RdiLStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmRdi;
        if (regCur & cAf6_tohintsta_RdiLStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmRdi;
        }

    if (regVal & cAf6_tohintsta_AisLStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmAis;
        if (regCur & cAf6_tohintsta_AisLStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmAis;
        }

    if (regVal & cAf6_tohintsta_LofStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmLof;
        if (regCur & cAf6_tohintsta_LofStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmLof;
        }

    if (regVal & cAf6_tohintsta_LosStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmLos;
        if (regCur & cAf6_tohintsta_LosStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmLos;
        }

    if (regVal & cAf6_tohintsta_TimLStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmTim;
        if (regCur & cAf6_tohintsta_TimLStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmTim;
        }

    if (regVal & cAf6_tohintsta_SdLStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmBerSd;
        if (regCur & cAf6_tohintsta_SdLStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmBerSd;
        }

    if (regVal & cAf6_tohintsta_SfLStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmBerSf;
        if (regCur & cAf6_tohintsta_SfLStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmBerSf;
        }

    if (regVal & cAf6_tohintsta_TcaLStateChgIntr_Mask)
        {
        events |= cAtSdhLineAlarmBerTca;
        if (regCur & cAf6_tohintsta_TcaLStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmBerTca;
        }

    /* Clear interrupt */
    AtHalWrite(hal, regAddr, regVal);

    AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);

    AtSdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly((AtSdhChannel)self, DefectsHw2Sw((AtChannel)self, regCur), cAtTrue);
    }

static eBool FpgaLoopOutIsApplicable(ThaSdhLine self)
    {
    return Tha60210031DeviceEc1LoopbackIsSupported(AtChannelDeviceGet((AtChannel)self));
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(loopbackMode);
    return Tha60210031DeviceEc1LoopbackIsSupported(AtChannelDeviceGet((AtChannel)self));
    }

static eAtRet InterruptEnableHelper(AtChannel self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 lineId = (uint8)AtChannelIdGet(self);
    uint8 numStsInOneSlice = ThaModuleOcnNumStsInOneSlice(ModuleOcn((AtSdhLine)self));
    uint8 hwEnable = (uint8)mBoolToBin(enable);
    uint8 lineIdInSlice24 = lineId % numStsInOneSlice;

    regAddr = (lineId < numStsInOneSlice) ? cAf6Reg_tohintperlineenctl1_Base : cAf6Reg_tohintperlineenctl2_Base;
    regAddr = regAddr + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cBit0 << lineIdInSlice24, lineIdInSlice24, hwEnable);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return InterruptEnableHelper(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return InterruptEnableHelper(self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint8 hwEnable;
    uint32 regAddr, regVal;
    uint8 lineId = (uint8)AtChannelIdGet(self);
    uint8 numStsInOneSlice = ThaModuleOcnNumStsInOneSlice(ModuleOcn((AtSdhLine)self));
    uint8 lineIdInSlice24 = lineId % numStsInOneSlice;

    regAddr = (lineId < numStsInOneSlice) ? cAf6Reg_tohintperlineenctl1_Base : cAf6Reg_tohintperlineenctl2_Base;
    regAddr = regAddr + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldGet(regVal, cBit0 << lineIdInSlice24, lineIdInSlice24, uint8, &hwEnable);
    return mBinToBool(hwEnable);
    }

static eAtRet TohMonitoringDefaultSet(ThaSdhLine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TohMonitoringChannelControlRegAddr(mThis(self)) +
                     mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tohramctl_StbRdiisLThresSel_, 0);
    mRegFieldSet(regVal, cAf6_tohramctl_StbAisLThresSel_, 0);
    mRegFieldSet(regVal, cAf6_tohramctl_K2StbMd_, 1);
    mRegFieldSet(regVal, cAf6_tohramctl_StbK1K2ThresSel_, 0);
    mRegFieldSet(regVal, cAf6_tohramctl_StbS1ThresSel_, 0);
    mRegFieldSet(regVal, cAf6_tohramctl_SmpK1ThresSel_, 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 regOffset = mMethodsGet(line)->DefaultOffset(line);

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B1CounterRegAddr(mThis(self), cAtFalse) + regOffset, cThaModuleOcn);
        case cAtSdhLineCounterTypeB2:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B2CounterRegAddr(mThis(self), cAtFalse) + regOffset, cThaModuleOcn);
        case cAtSdhLineCounterTypeRei:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->ReiCounterRegAddr(mThis(self), cAtFalse) + regOffset, cThaModuleOcn);

        default:
            return 0;
        }
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 offset = mMethodsGet(line)->DefaultOffset(line);

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B1CounterRegAddr(mThis(self), cAtTrue) + offset, cThaModuleOcn);
        case cAtSdhLineCounterTypeB2:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B2CounterRegAddr(mThis(self), cAtTrue) + offset, cThaModuleOcn);
        case cAtSdhLineCounterTypeRei:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->ReiCounterRegAddr(mThis(self), cAtTrue) + offset, cThaModuleOcn);

        default:
            return 0;
        }
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 offset = mMethodsGet(line)->DefaultOffset(line);

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B1BlockCounterRegAddr(mThis(self), cAtFalse) + offset, cThaModuleOcn);
        case cAtSdhLineCounterTypeB2:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B2BlockCounterRegAddr(mThis(self), cAtFalse) + offset, cThaModuleOcn);
        case cAtSdhLineCounterTypeRei:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->ReiBlockCounterRegAddr(mThis(self), cAtFalse) + offset, cThaModuleOcn);

        default:
            return 0;
        }
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 regOffset = mMethodsGet(line)->DefaultOffset(line);

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B1BlockCounterRegAddr(mThis(self), cAtTrue) + regOffset, cThaModuleOcn);
        case cAtSdhLineCounterTypeB2:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->B2BlockCounterRegAddr(mThis(self), cAtTrue) + regOffset, cThaModuleOcn);
        case cAtSdhLineCounterTypeRei:
            return mChannelHwRead(self, mMethodsGet(mThis(self))->ReiBlockCounterRegAddr(mThis(self), cAtTrue) + regOffset, cThaModuleOcn);
        default:
            return 0;
        }
    }

static uint32 RsDefectGet(AtChannel self, uint32 hwDefect)
    {
    uint32 defects = 0;

    if (mMethodsGet(mThis(self))->ShouldHideLofWhenLos(mThis(self)))
        {
        if (hwDefect & cAf6_tohintsta_LosStateChgIntr_Mask)
            return cAtSdhLineAlarmLos;
        if (hwDefect & cAf6_tohintsta_LofStateChgIntr_Mask)
            return cAtSdhLineAlarmLof;
        }

    else
        {
        if (hwDefect & cAf6_tohintsta_LosStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmLos;
        if (hwDefect & cAf6_tohintsta_LofStateChgIntr_Mask)
            defects |= cAtSdhLineAlarmLof;

        if (defects)
            return defects;
        }

    if (hwDefect & cAf6_tohintsta_TimLStateChgIntr_Mask)
        return cAtSdhLineAlarmTim;

    return 0;
    }

static eBool HasRsAlarmForwarding(AtChannel self, uint32 swDefect)
    {
    if (swDefect & cAtSdhLineAlarmLos)
        return cAtTrue;
    if (swDefect & cAtSdhLineAlarmLof)
        return cAtTrue;
    if ((swDefect & cAtSdhLineAlarmTim) && AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)self, cAtSdhLineAlarmTim))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 MsDefectGet(uint32 hwDefect)
    {
    uint32 defect = 0;
    if (hwDefect & cAf6_tohintsta_AisLStateChgIntr_Mask)
        return cAtSdhLineAlarmAis;

    if (hwDefect & cAf6_tohintsta_RdiLStateChgIntr_Mask)
        defect |= cAtSdhLineAlarmRdi;
    if (hwDefect & cAf6_tohintsta_SdLStateChgIntr_Mask)
        defect |= cAtSdhLineAlarmBerSd;
    if (hwDefect & cAf6_tohintsta_SfLStateChgIntr_Mask)
        defect |= cAtSdhLineAlarmBerSf;
    if (hwDefect & cAf6_tohintsta_TcaLStateChgIntr_Mask)
        defect |= cAtSdhLineAlarmBerTca;
    return defect;
    }

static uint32 DefectsHw2Sw(AtChannel self, uint32 regVal)
    {
    uint32 swDefect = 0;

    /* Get RS defects */
    swDefect = RsDefectGet(self, regVal);
    if (HasRsAlarmForwarding(self, swDefect))
        {
        swDefect |= cAtSdhLineAlarmAis;
        return swDefect;
        }

    /* Get MS defects */
    return swDefect | MsDefectGet(regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr  = mMethodsGet(mThis(self))->RxLineAlarmCurrentStatusRegAddr(mThis(self)) +
                      mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    uint32 regVal   = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return DefectsHw2Sw(self, regVal);
    }

static uint32 DefectMaskGet(uint32 regDefect)
    {
    uint32 swDefect = 0;
    if (regDefect & cAf6_tohintsta_S1StbStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmS1Change;
    if (regDefect & cAf6_tohintsta_K1StbStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmK1Change;
    if (regDefect & cAf6_tohintsta_K2StbStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmK2Change;
    if (regDefect & cAf6_tohintsta_RdiLStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmRdi;
    if (regDefect & cAf6_tohintsta_AisLStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmAis;
    if (regDefect & cAf6_tohintsta_LofStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmLof;
    if (regDefect & cAf6_tohintsta_LosStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmLos;
    if (regDefect & cAf6_tohintsta_TimLStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmTim;
    if (regDefect & cAf6_tohintsta_SdLStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmBerSd;
    if (regDefect & cAf6_tohintsta_SfLStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmBerSf;
    if (regDefect & cAf6_tohintsta_TcaLStateChgIntr_Mask)
        swDefect |= cAtSdhLineAlarmBerTca;
    return swDefect;
    }

static uint32 DefectHistoryRead2Clear(ThaSdhLine self, eBool read2Clear)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RxLineAlarmInterruptStatusRegAddr(mThis(self)) +
                     mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 defectHistory = DefectMaskGet(regVal);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return defectHistory;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryRead2Clear((ThaSdhLine)self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryRead2Clear((ThaSdhLine)self, cAtTrue);
    }

static uint32 ChannelOffset(Tha60210031SdhLine self)
    {
    return ThaModuleOcnLineTxDefaultOffset(ModuleOcn((AtSdhLine)self), (AtSdhChannel)self);
    }

static eAtModuleSdhRet TxK1Set(AtSdhLine self, uint8 value)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self)) +
                     mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxK1Pat_, value);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 TxK1Get(AtSdhLine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self)) +
                     mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cAf6_tfmramctl_OCNTxK1Pat_);
    }

static uint8 KBytesStableSelectedThreshold(ThaSdhLine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TohMonitoringChannelControlRegAddr(mThis(self))
                     + mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cAf6_tohramctl_StbK1K2ThresSel_);
    }

static uint8 K1StableThreshold(ThaSdhLine self)
    {
    uint8 selectedThreshold = KBytesStableSelectedThreshold(self);
    uint32 regAddr = mMethodsGet(mThis(self))->TohGlobalK1StableMonitoringThresholdControlRegAddr(mThis(self))
                     + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (selectedThreshold == 0)
        return (uint8)mRegField(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr1_);

    return (uint8)mRegField(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr2_);
    }

static uint8 RxK1Get(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 regAddr = mMethodsGet(mThis(self))->TohK1MonitoringStatusRegAddr(mThis(self))
                     + mMethodsGet(line)->DefaultOffset(line);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (mRegField(regVal, cAf6_tohk1monsta_SameK1Cnt_) >= K1StableThreshold((ThaSdhLine)self))
        return (uint8)mRegField(regVal, cAf6_tohk1monsta_K1StbVal_);

    return (uint8)mRegField(regVal, cAf6_tohk1monsta_K1CurVal_);
    }

static eAtModuleSdhRet TxK2Set(AtSdhLine self, uint8 value)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self)) +
                     mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxK2Pat_, value);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 TxK2Get(AtSdhLine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self)) + mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cAf6_tfmramctl_OCNTxK2Pat_);
    }

static uint8 K2StableThreshold(ThaSdhLine self)
    {
    uint8 selectedThreshold = KBytesStableSelectedThreshold(self);
    uint32 regAddr = mMethodsGet(mThis(self))->TohGlobalK2StableMonitoringThresholdControlRegAddr(mThis(self))
                     + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (selectedThreshold == 0)
        return (uint8)mRegField(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr1_);
    return (uint8)mRegField(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr2_);
    }

static uint8 RxK2Get(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 regAddr = mMethodsGet(mThis(self))->TohK2MonitoringStatusRegAddr(mThis(self))
                     + mMethodsGet(line)->DefaultOffset(line);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (mRegField(regVal, cAf6_tohk2monsta_SameK2Cnt_) >= K2StableThreshold((ThaSdhLine)self))
        return (uint8)mRegField(regVal, cAf6_tohk2monsta_K2StbVal_);

    return (uint8)mRegField(regVal, cAf6_tohk2monsta_K2CurVal_);
    }

static eAtModuleSdhRet TxS1Set(AtSdhLine self, uint8 value)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxS1LDis_, 0);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxS1Pat_, value);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint8 TxS1Get(AtSdhLine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cAf6_tfmramctl_OCNTxS1Pat_);
    }

static uint8 S1ByteStableSelectedThreshold(ThaSdhLine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TohMonitoringChannelControlRegAddr(mThis(self))
                     + mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (uint8)mRegField(regVal, cAf6_tohramctl_StbS1ThresSel_);
    }

static uint8 S1StableThreshold(ThaSdhLine self)
    {
    uint8 selectedThreshold = S1ByteStableSelectedThreshold(self);
    uint32 regAddr = mMethodsGet(mThis(self))->TohGlobalS1StableMonitoringThresholdControlRegAddr(mThis(self))
                     + Tha60210011ModuleOcnBaseAddress(ModuleOcn((AtSdhLine)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (selectedThreshold == 0)
        return (uint8)mRegField(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr1_);
    return (uint8)mRegField(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr2_);
    }

static uint8 RxS1Get(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;
    uint32 regAddr  = mMethodsGet(mThis(self))->TohS1MonitoringStatusRegAddr(mThis(self))
                      + mMethodsGet(line)->DefaultOffset(line);
    uint32 regVal   = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (mRegField(regVal, cAf6_tohs1monsta_SameS1Cnt_) >= S1StableThreshold((ThaSdhLine)self))
        return (uint8)mRegField(regVal, cAf6_tohs1monsta_S1StbVal_);

    return (uint8)mRegField(regVal, cAf6_tohs1monsta_S1CurVal_);
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
    uint8 hwEnable = enable ? 0 : 1;
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxRdiLDis_, hwEnable);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (mRegField(regVal, cAf6_tfmramctl_OCNTxRdiLDis_) == 0) ? cAtTrue : cAtFalse;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhLineAlarmAis | cAtSdhLineAlarmRdi;
    }

static eBool CanForceTxAlarms(ThaSdhLine self, uint32 alarmType)
    {
    return (alarmType & AtChannelTxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static eAtRet FramerTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    uint32 regVal, address;
    eBool canForce = CanForceTxAlarms((ThaSdhLine)self, alarmType);

    if (canForce == cAtFalse)
        return (force) ? cAtErrorModeNotSupport : cAtOk;

    address = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
              + mChannelOffset(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    if (alarmType & cAtSdhLineAlarmAis)
        mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxAisLFrc_, mBoolToBin(force));

    if (alarmType & cAtSdhLineAlarmRdi)
        mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxRdiLFrc_, mBoolToBin(force));

    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool UseEc1TxAisForce(AtChannel self)
    {
    /* Our HW design has TX AIS force inside TX framer. But we need new AIS
     * force so that AIS is not reflect on EC1 local loopback */
    return mMethodsGet(mThis(self))->Ec1TxAisForceIsSupported(mThis(self));
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    eAtRet ret = cAtOk;

    if (UseEc1TxAisForce(self) && (alarmType & cAtSdhLineAlarmAis))
        {
        alarmType = (uint32)(alarmType & (uint32)~cAtSdhLineAlarmAis);
        ret |= Tha60210031OcnLineTxAisForce(self, force);
        }

    if (alarmType)
        ret |= FramerTxAlarmForce(self, alarmType, force);

    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 FramerTxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 forcedAlarms = 0;

    if (regVal & cAf6_tfmramctl_OCNTxAisLFrc_Mask)
        forcedAlarms |= cAtSdhLineAlarmAis;
    if (regVal & cAf6_tfmramctl_OCNTxRdiLFrc_Mask)
        forcedAlarms |= cAtSdhLineAlarmRdi;

    return forcedAlarms;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;

    if (UseEc1TxAisForce(self) && Tha60210031OcnLineTxAisIsForced(self))
        forcedAlarms |= cAtSdhLineAlarmAis;

    forcedAlarms |= FramerTxForcedAlarmGet(self);
    return forcedAlarms;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType == 0)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet ForceB2(AtChannel self, eBool force)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxAutoB2Dis_, force ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet ForcedB2(AtChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self))
                     + mChannelOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cAf6_tfmramctl_OCNTxAutoB2Dis_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    return (errorType == cAtSdhLineErrorB2) ? ForceB2(self, cAtTrue) : cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhLineErrorB2)
        ForceB2(self, cAtFalse);

    return cAtOk;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    return (ForcedB2(self)) ? cAtSdhLineErrorB2 : cAtSdhLineErrorNone;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtSdhLineAlarmLos|cAtSdhLineAlarmLof|cAtSdhLineAlarmAis|
                                    cAtSdhLineAlarmRdi|cAtSdhLineAlarmK1Change|
                                    cAtSdhLineAlarmK2Change|cAtSdhLineAlarmS1Change|
                                    cAtSdhLineAlarmTim|cAtSdhLineAlarmBerSd|
                                    cAtSdhLineAlarmBerSf|cAtSdhLineAlarmBerTca);
    AtUnused(self);
    return supportedMasks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;
    ThaSdhLine sdhLine = (ThaSdhLine)self;

    address = mMethodsGet(mThis(self))->RxLineAlarmInterruptEnableControlRegAddr(mThis(self)) + mMethodsGet(sdhLine)->DefaultOffset(sdhLine);
    regVal  = mChannelHwRead(sdhLine, address, cThaModuleOcn);
    if (defectMask & cAtSdhLineAlarmLos)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_LosStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmLos) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmLof)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_LofStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmLof) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmAis)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_AisLStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmAis) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmRdi)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmRdi) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmK1Change)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmK1Change) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmK2Change)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmK2Change) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmS1Change)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmS1Change) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmTim)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_TimLStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmTim) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmBerSd)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_SdLStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmBerSd) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmBerSf)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_SfLStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmBerSf) ? 1 : 0);

    if (defectMask & cAtSdhLineAlarmBerTca)
        mRegFieldSet(regVal,
                     cAf6_tohintperalrenbctl_TcaStateChgIntrEn_,
                     (enableMask & cAtSdhLineAlarmBerTca) ? 1 : 0);
    mChannelHwWrite(sdhLine, address, regVal, cThaModuleOcn);

    if (regVal)
        return AtChannelInterruptEnable(self);
    else
        return AtChannelInterruptDisable(self);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RxLineAlarmInterruptEnableControlRegAddr(mThis(self))
                    + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine) self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return DefectMaskGet(regVal);
    }

static eAtModuleSdhRet LedStateSet(AtSdhLine self, eAtLedState ledState)
    {
    AtUnused(self);
    AtUnused(ledState);
    return cAtErrorModeNotSupport;
    }

static eAtLedState LedStateGet(AtSdhLine self)
    {
    AtUnused(self);
    return cAtLedStateOff;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet LoopInEnable(ThaSdhLine self, eBool enable)
    {
    return Tha60210031OcnLineLocalLoopEnable((AtChannel)self, enable);
    }

static eBool LoopInIsEnabled(ThaSdhLine self)
    {
    return Tha60210031OcnLineLocalLoopIsEnabled((AtChannel)self);
    }

static eAtRet LoopOutEnable(ThaSdhLine self, eBool enable)
    {
    return Tha60210031OcnLineRemoteLoopEnable((AtChannel)self, enable);
    }

static eBool LoopOutIsEnabled(ThaSdhLine self)
    {
    return Tha60210031OcnLineRemoteLoopIsEnabled((AtChannel)self);
    }

static eBool HasRateHwConfiguration(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear)
    {
    ThaSdhLine sdhLine = (ThaSdhLine)self;

    *changedDefects = DefectHistoryRead2Clear(sdhLine, read2Clear);
    *currentStatus = AtChannelDefectGet((AtChannel)sdhLine);

    return cAtOk;
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    if (!ThaModuleSdhLineTimingIsSupported((ThaModuleSdh)AtChannelModuleGet(self)))
        return cAtFalse;

    if ((timingMode == cAtTimingModeSys)  ||
        (timingMode == cAtTimingModeLoop) ||
        (timingMode == cAtTimingModeExt1Ref))
        return cAtTrue;

    return cAtFalse;
    }

static AtChannel SerialLineGet(AtChannel self)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    return (AtChannel)AtModulePdhDe3SerialLineGet(modulePdh, AtChannelIdGet(self));
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtUnused(timingSource);

    if (!AtChannelTimingModeIsSupported(self, timingMode))
        return cAtErrorModeNotSupport;

    Tha60210031OcnLineTimingModeSet(self, timingMode);
    AtChannelTimingSet(SerialLineGet(self), timingMode, timingSource);

    return cAtOk;
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    if (ThaModuleSdhLineTimingIsSupported((ThaModuleSdh)AtChannelModuleGet(self)))
        return Tha60210031OcnLineTimingModeGet(self);
    return cAtTimingModeUnknown;
    }

static AtChannel TimingSourceGet(AtChannel self)
    {
    if (AtChannelTimingModeGet(self) == cAtTimingModeLoop)
        return self;

    return NULL;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return enabled;
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return enabled;
    }

static uint32 B1CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1errr2ccnt_Base : cAf6Reg_tohb1errrocnt_Base;
    }

static uint32 B2CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2errr2ccnt_Base : cAf6Reg_tohb2errrocnt_Base;
    }

static uint32 ReiCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreierrr2ccnt_Base : cAf6Reg_tohreierrrocnt_Base;
    }

static uint32 B1BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1blkerrr2ccnt_Base : cAf6Reg_tohb1blkerrrocnt_Base;
    }

static uint32 B2BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2blkerrr2ccnt_Base : cAf6Reg_tohb2blkerrrocnt_Base;
    }

static uint32 ReiBlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreiblkerrr2ccnt_Base : cAf6Reg_tohreiblkerrrocnt_Base;
    }

static uint32 TohMonitoringChannelControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohramctl_Base;
    }

static uint32 RxLineAlarmInterruptStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintsta_Base;
    }

static uint32 RxLineAlarmInterruptEnableControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintperalrenbctl_Base;
    }

static uint32 RxLineAlarmCurrentStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohcursta_Base;
    }

static uint32 TxFramerPerChannelControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tfmramctl_Base;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(ModuleOcn((AtSdhLine)self));
    }

static uint32 TohS1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohs1monsta_Base;
    }

static uint32 TohK1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk1monsta_Base;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(ModuleOcn((AtSdhLine)self));
    }

static uint32 TohK2MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk2monsta_Base;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(ModuleOcn((AtSdhLine)self));
    }

static eBool ShouldHideLofWhenLos(Tha60210031SdhLine self)
    {
    /* Not to affect the current products. Let concrete determine */
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TohMonitoringChannelControlRegAddr(mThis(self))
                     + mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint8 blockMode = (mode == cAtSdhChannelCounterModeBlock) ? 1 : 0;

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (counterType == cAtSdhLineCounterTypeB1)
        mRegFieldSet(regVal, cAf6_tohramctl_B1ErrCntBlkMod_, blockMode);
    if (counterType == cAtSdhLineCounterTypeB2)
        mRegFieldSet(regVal, cAf6_tohramctl_B2ErrCntBlkMod_, blockMode);
    if (counterType == cAtSdhLineCounterTypeRei)
        mRegFieldSet(regVal, cAf6_tohramctl_ReiErrCntBlkMod_, blockMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet AutoReiEnable(AtSdhChannel self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self)) +
                     mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tfmramctl_OCNTxReiLDis_, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool AutoReiIsEnabled(AtSdhChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self)) +
                     mChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return mRegField(regVal, cAf6_tfmramctl_OCNTxReiLDis_) ? cAtFalse : cAtTrue;
    }

static eBool Ec1TxAisForceIsSupported(Tha60210031SdhLine self)
    {
    return Tha60210031DeviceEc1LoopbackIsSupported(AtChannelDeviceGet((AtChannel)self));
    }

static void MethodsInit(AtSdhLine self)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelOffset);
        mMethodOverride(m_methods, B1CounterRegAddr);
        mMethodOverride(m_methods, B2CounterRegAddr);
        mMethodOverride(m_methods, ReiCounterRegAddr);
        mMethodOverride(m_methods, B1BlockCounterRegAddr);
        mMethodOverride(m_methods, B2BlockCounterRegAddr);
        mMethodOverride(m_methods, ReiBlockCounterRegAddr);
        mMethodOverride(m_methods, TohMonitoringChannelControlRegAddr);
        mMethodOverride(m_methods, RxLineAlarmInterruptStatusRegAddr);
        mMethodOverride(m_methods, RxLineAlarmInterruptEnableControlRegAddr);
        mMethodOverride(m_methods, RxLineAlarmCurrentStatusRegAddr);
        mMethodOverride(m_methods, TxFramerPerChannelControlRegAddr);
        mMethodOverride(m_methods, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_methods, TohS1MonitoringStatusRegAddr);
        mMethodOverride(m_methods, TohK1MonitoringStatusRegAddr);
        mMethodOverride(m_methods, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_methods, TohK2MonitoringStatusRegAddr);
        mMethodOverride(m_methods, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_methods, ShouldHideLofWhenLos);
        mMethodOverride(m_methods, Ec1TxAisForceIsSupported);
        }

    mMethodsSet(line, &m_methods);
    }
    
static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);

        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);

        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);

        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);

        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);

        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, TimingSourceGet);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, InterruptAndDefectGet);
        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, LowRate);
        mMethodOverride(m_ThaSdhLineOverride, HighRate);
        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopOutIsApplicable);
        mMethodOverride(m_ThaSdhLineOverride, TohMonitoringDefaultSet);
        mMethodOverride(m_ThaSdhLineOverride, DefaultOffset);
        mMethodOverride(m_ThaSdhLineOverride, LoopInEnable);
        mMethodOverride(m_ThaSdhLineOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaSdhLineOverride, LoopOutEnable);
        mMethodOverride(m_ThaSdhLineOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaSdhLineOverride, HasRateHwConfiguration);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        mMethodOverride(m_AtSdhLineOverride, RateGet);
        mMethodOverride(m_AtSdhLineOverride, InterruptProcess);
        mMethodOverride(m_AtSdhLineOverride, TxK1Set);
        mMethodOverride(m_AtSdhLineOverride, TxK1Get);
        mMethodOverride(m_AtSdhLineOverride, TxK2Set);
        mMethodOverride(m_AtSdhLineOverride, TxK2Get);
        mMethodOverride(m_AtSdhLineOverride, RxK1Get);
        mMethodOverride(m_AtSdhLineOverride, RxK2Get);
        mMethodOverride(m_AtSdhLineOverride, TxS1Set);
        mMethodOverride(m_AtSdhLineOverride, TxS1Get);
        mMethodOverride(m_AtSdhLineOverride, RxS1Get);
        mMethodOverride(m_AtSdhLineOverride, LedStateSet);
        mMethodOverride(m_AtSdhLineOverride, LedStateGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideThaSdhLine(self);
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SdhLine);
    }

AtSdhLine Tha60210031SdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineObjectInit(self, channelId, module, 0) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60210031SdhLineNew(uint32 channelId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031SdhLineObjectInit(newLine, channelId, module);
    }

uint32 Tha60210031SdhLineTxFramerPerChannelControlRegAddr(Tha60210031SdhLine self)
    {
    if (self)
        return mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(self);
    return cInvalidUint32;
    }

uint32 Tha60210031SdhLineChannelOffset(Tha60210031SdhLine self)
    {
    if (self)
        return mMethodsGet(self)->ChannelOffset(self);
    return cInvalidUint32;
    }


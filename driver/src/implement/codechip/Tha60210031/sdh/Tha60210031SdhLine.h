/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210031SdhLine.h
 * 
 * Created Date: Mar 25, 2017
 *
 * Description : SDH Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SDHLINE_H_
#define _THA60210031SDHLINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplusa
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SdhLine * Tha60210031SdhLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210031SdhLineTxFramerPerChannelControlRegAddr(Tha60210031SdhLine self);
uint32 Tha60210031SdhLineChannelOffset(Tha60210031SdhLine self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SDHLINE_H_ */


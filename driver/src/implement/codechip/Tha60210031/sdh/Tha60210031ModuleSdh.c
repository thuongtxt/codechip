/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210031ModuleSdh.c
 *
 * Created Date: Jun 12, 2014
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031ModuleSdhInternal.h"
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sdh/ThaModuleSdhInternal.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/poh/Tha60210011PohReg.h"
#include "../../Tha60210011/poh/Tha60210011ModulePohInternal.h"
#include "../../Tha60210051/sdh/Tha60210051Tfi5LineVc.h"
#include "../ocn/Tha60210031ModuleOcnReg.h"
#include "../man/Tha60210031DeviceReg.h"
#include "../man/Tha60210031Device.h"
#include "Tha60210031SdhVcInternal.h"
#include "Tha60210031ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumSts1InSlice24    24
#define cBitMask(sliceId)    (cBit0 << (sliceId))
#define cNumVtgInSts         7
#define cNumVtInVtg          4
#define cNumBitsInDword 32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods *m_AtModuleSdhMethods   = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumLinesPerPart(ThaModuleSdh self)
    {
    AtUnused(self);
    return (uint8)48;
    }

static uint32 MaxLineRate(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm0;
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (Tha60210031ShouldDisableEc1Feature(device))
        return 0;

    return 48;
    }

static uint32 NumEc1LinesGet(AtModuleSdh self)
    {
    return MaxLinesGet(self);
    }

static eAtSdhLineRate LineDefaultRate(ThaModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtSdhLineRateStm0;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(serdesId);
    return NULL;
    }

static eBool HasLedEngine(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static AtSdhAu AuCreate(AtModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return Tha60210011Tfi5LineAuNew(channelId, channelType, self);
    }

static AtSdhChannel VcCreate(AtModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210031SdhVcNew(channelId, channelType, self);
    }

static AtSdhTug TugCreate(AtModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhTug)Tha60210011Tfi5LineTugNew(channelId, channelType, self);
    }

static AtSdhTu TuCreate(AtModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhTu)Tha60210011Tfi5LineTuNew(channelId, channelType, self);
    }

static AtSdhVc Vc1xCreate(AtModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhVc)Tha60210031SdhVc1xNew(channelId, channelType, self);
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    AtUnused(parent);
    AtUnused(lineId);
    switch (channelType)
        {
        case cAtSdhChannelTypeLine:
            return (AtSdhChannel)Tha60210031SdhLineNew(channelId, (AtModuleSdh)self);

        case cAtSdhChannelTypeAu3:
            return (AtSdhChannel)AuCreate(self, channelType, channelId);

        /* Create VC */
        case cAtSdhChannelTypeVc3:
            return (AtSdhChannel)VcCreate(self, channelType, channelId);

        case cAtSdhChannelTypeTug3:
        case cAtSdhChannelTypeTug2:
            return (AtSdhChannel)TugCreate(self, channelType, channelId);

        case cAtSdhChannelTypeTu12:
        case cAtSdhChannelTypeTu11:
            return (AtSdhChannel)TuCreate(self, channelType, channelId);

        case cAtSdhChannelTypeVc12:
        case cAtSdhChannelTypeVc11:
            return (AtSdhChannel)Vc1xCreate(self, channelType, channelId);

        default:
            return NULL;
        }
    }

static eBool HasClockOutput(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }
    
static eBool HasAps(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasCounters(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void AlarmDownstreamEnable(ThaModuleSdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    }

static ThaModuleOcn ModuleOcn(ThaModuleSdh self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    }

static ThaModulePoh ModulePoh(ThaModuleSdh self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    }

static void LineInterruptProcess(ThaModuleSdh self, uint32 tohIntr, AtHal hal)
    {
    uint8 slice, sts1;
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ModuleOcn(self));
    uint8 numSlices = ThaModuleOcnNumSlices(ModuleOcn(self));

    for (slice = 0; slice < numSlices; slice++)
        {
        if (tohIntr & cBitMask(slice))
            {
            uint32 intrLineVal = AtHalRead(hal, (uint32)cAf6Reg_tohintperline1_Base + baseAddress + slice);
            uint32 intrLineMask = AtHalRead(hal, (uint32)cAf6Reg_tohintperlineenctl1_Base + baseAddress + slice);
            intrLineVal &= intrLineMask;

            for (sts1 = 0; sts1 < cNumSts1InSlice24; sts1++)
                {
                if (intrLineVal & cBitMask(sts1))
                    {
                    AtSdhLine line = ThaModuleSdhLineFromHwIdGet(self, cAtModuleSdh, slice, sts1);
                    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)line);

                    AtSdhLineInterruptProcess(line, lineId, hal);
                    }
                }
            }
        }
    }

static void StsInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    uint8 slice, sts1;
    uint32 baseAddress = Tha60210011ModulePohBaseAddress(ModulePoh(self));
    uint8 numSlices = ThaModuleOcnNumSlices(ModuleOcn(self));

    for (slice = 0; slice < numSlices; slice++)
        {
        uint32 intrStsVal, intrStsMask;

        if ((pohIntr & cBitMask(slice)) == 0)
            continue;

        /* Must AND STS interrupt status with STS interrupt mask
         * due to HW does not automatically AND inside */
        intrStsVal = AtHalRead(hal, (uint32)cAf6Reg_alm_glbchghi(slice) + baseAddress);
        intrStsMask = AtHalRead(hal,(uint32)cAf6Reg_alm_glbmskhi(slice) + baseAddress);

        intrStsVal &= intrStsMask;
        for (sts1 = 0; sts1 < cNumSts1InSlice24; sts1++)
            {
            AtSdhPath auPath;

            if ((intrStsVal & cBitMask(sts1)) == 0)
                continue;

            auPath = ThaModuleSdhAuPathFromHwIdGet(self, cAtModuleSdh, slice, sts1);
            AtSdhPathInterruptProcess(auPath, slice, sts1, 0, hal);
            }
        }
    }

static void VtInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    uint8 slice, sts1;
    uint32 baseAddress = Tha60210011ModulePohBaseAddress(ModulePoh(self));
    uint8 numSlices = ThaModuleOcnNumSlices(ModuleOcn(self));

    for (slice = 0; slice < numSlices; slice++)
        {
        uint32 intrStsVal, intrStsMask;

        if ((pohIntr & cBitMask(slice)) == 0)
             continue;

        /* Must AND STS interrupt status with STS interrupt mask
         * due to HW does not automatically AND inside */
        intrStsVal = AtHalRead(hal, (uint32)cAf6Reg_alm_glbchglo(slice) + baseAddress);
        intrStsMask = AtHalRead(hal,(uint32)cAf6Reg_alm_glbmsklo(slice) + baseAddress);

        intrStsVal &= intrStsMask;
        for (sts1 = 0; sts1 < cNumSts1InSlice24; sts1++)
            {
            uint8 vtg, vt, vt28 = 0;
            uint32 intrVt28Val;

            if ((intrStsVal & cBitMask(sts1)) == 0)
                continue;

            intrVt28Val = AtHalRead(hal, (uint32)cAf6Reg_alm_orstalo(sts1, slice) + baseAddress);
            for (vtg = 0; vtg < cNumVtgInSts; vtg++)
                {
                for (vt = 0; vt < cNumVtInVtg; vt++)
                    {
                    if (intrVt28Val & cBitMask(vt28))
                        {
                        AtSdhPath tu;

                        tu = ThaModuleSdhTuPathFromHwIdGet(self, cAtModuleSdh, slice, sts1, vtg, vt);
                        AtSdhPathInterruptProcess(tu, slice, sts1, vt28, hal);
                        }
                    vt28++;
                    }
                }
            }
        }
    }

static uint32 LineInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(glbIntr);
    return AtHalRead(hal, cAf6Reg_tohintperslice_Base + Tha60210011ModuleOcnBaseAddress(ModuleOcn(self))) & cBit1_0;
    }

static uint32 StsInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 regVal = AtHalRead(hal, cAf6Reg_alm_glbchgo + Tha60210011ModulePohBaseAddress(ModulePoh(self)));
    AtUnused(self);
    AtUnused(glbIntr);

    return (regVal & cBit0) | ((regVal & cBit2) >> 1);
    }

static uint32 VtInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 regVal = AtHalRead(hal, Tha60210011ModulePohBaseAddress(ModulePoh(self)) + cAf6Reg_alm_glbchgo);
    AtUnused(self);
    AtUnused(glbIntr);

    return (regVal & cBit17_16) >> 16;
    }

static eBool HasLineInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_inten_status_OCNLineIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool HasStsInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_inten_status_OCNSTSIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool HasVtInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_inten_status_OCNVTIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static AtSdhLine LineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 localLineInSlice)
    {
    uint8 lineId;

    if (phyModule == cAtModuleSdh)
        {
        lineId = (uint8)(sliceId * cNumSts1InSlice24 + localLineInSlice);
        return AtModuleSdhLineGet((AtModuleSdh)self, lineId);
        }

    if (phyModule == cAtModuleSur)
        {
        lineId = (uint8)((localLineInSlice << 1) | (sliceId & 0x1));
        return AtModuleSdhLineGet((AtModuleSdh)self, lineId);
        }

    return NULL;
    }

static AtSdhPath AuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId)
    {
    uint8 lineId = (uint8)(sliceId * cNumSts1InSlice24 + stsId);
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, lineId);
    AtUnused(phyModule);
    return (AtSdhPath)AtSdhLineAu3Get(line, 0, 0);
    }

static AtSdhPath TuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 lineId = (uint8)(sliceId * cNumSts1InSlice24 + stsId);
    AtSdhLine line = AtModuleSdhLineGet((AtModuleSdh)self, lineId);
    AtUnused(phyModule);
    return (AtSdhPath)AtSdhLineTu1xGet(line, 0, 0, vtgId, vtId);
    }

static ThaVersionReader VersionReader(ThaModuleSdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportLineTiming(ThaModuleSdh self)
    {
    uint32 hwVersion;

    hwVersion = ThaVersionReaderHardwareVersion(VersionReader(self));
    if (hwVersion < ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x03, 0x06, 0))
        return cInvalidUint32;

    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x11, 0x05, 0x36);
    }

static eAtSdhChannelMode LineDefaultMode(AtModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtSdhChannelModeSonet;
    }

static eAtSdhTtiMode DefaultTtiMode(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhTtiMode16Byte;
    }

static eAtRet LineInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet StsInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint32 regAddr, regVal;
    uint32 slice;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk_glbmskhi_Mask) : (regVal & ~cAf6_alm_glbmsk_glbmskhi_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmskhi_glbmsk_Mask : 0x0;
    for (slice = 0; slice < numSlice; slice++)
        mModuleHwWrite(self, baseAddress + cAf6Reg_alm_glbmskhi(slice), regVal);

    return cAtOk;
    }

static eAtRet VtInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberVtSlices((Tha60210011ModulePoh)modulePoh);
    uint32 regAddr, regVal;
    uint32 slice;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk_glbmsklo_Mask) : (regVal & ~cAf6_alm_glbmsk_glbmsklo_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmsklo_glbmsk_Mask : 0x0;
    for (slice = 0; slice < numSlice; slice++)
        mModuleHwWrite(self, baseAddress + cAf6Reg_alm_glbmsklo(slice), regVal);

    return cAtOk;
    }

static eBool AlwaysDisableRdiBackwardWhenTimNotDownstreamAis(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, LineDefaultRate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, NumLinesPerPart);
        mMethodOverride(m_ThaModuleSdhOverride, HasLedEngine);
        mMethodOverride(m_ThaModuleSdhOverride, HasClockOutput);
        mMethodOverride(m_ThaModuleSdhOverride, HasAps);
        mMethodOverride(m_ThaModuleSdhOverride, HasCounters);
        mMethodOverride(m_ThaModuleSdhOverride, AlarmDownstreamEnable);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, HasLineInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, HasStsInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, HasVtInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, LineFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, AuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, TuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, StartVersionSupportLineTiming);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, AlwaysDisableRdiBackwardWhenTimNotDownstreamAis);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, DefaultTtiMode);
        mMethodOverride(m_AtModuleSdhOverride, NumEc1LinesGet);
        mMethodOverride(m_AtModuleSdhOverride, LineDefaultMode);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleSdh);
    }

AtModuleSdh Tha60210031ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60210031ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleSdhObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210031SdhVc1xInternal.h
 * 
 * Created Date: Jul 20, 2016
 *
 * Description : SDH VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SDHVC1XINTERNAL_H_
#define _THA60210031SDHVC1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineVc1xInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SdhVc1x
    {
    tTha60210011Tfi5LineVc1x super;
    }tTha60210031SdhVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60210031SdhVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SDHVC1XINTERNAL_H_ */


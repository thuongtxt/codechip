/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210031SdhVc.h
 * 
 * Created Date: Aug 14, 2017
 *
 * Description : SDH VC interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SDHVC_H_
#define _THA60210031SDHVC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Utils to enable VC Map LIU enable */
void Tha60210031SdhVcLiuOverVc3MapUpdate(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SDHVC_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210031SdhLineInternal.h
 * 
 * Created Date: Sep 3, 2016
 *
 * Description : SDH Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SDHLINEINTERNAL_H_
#define _THA60210031SDHLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhLineInternal.h"
#include "Tha60210031SdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SdhLineMethods
    {
    eBool (*ShouldHideLofWhenLos)(Tha60210031SdhLine self);
    uint32 (*ChannelOffset)(Tha60210031SdhLine self);
    uint32 (*B1CounterRegAddr)(Tha60210031SdhLine self, eBool r2c);
    uint32 (*B2CounterRegAddr)(Tha60210031SdhLine self, eBool r2c);
    uint32 (*ReiCounterRegAddr)(Tha60210031SdhLine self, eBool r2c);
    uint32 (*B1BlockCounterRegAddr)(Tha60210031SdhLine self, eBool r2c);
    uint32 (*B2BlockCounterRegAddr)(Tha60210031SdhLine self, eBool r2c);
    uint32 (*ReiBlockCounterRegAddr)(Tha60210031SdhLine self, eBool r2c);
    uint32 (*TohMonitoringChannelControlRegAddr)(Tha60210031SdhLine self);
    uint32 (*RxLineAlarmInterruptStatusRegAddr)(Tha60210031SdhLine self);
    uint32 (*RxLineAlarmInterruptEnableControlRegAddr)(Tha60210031SdhLine self);
    uint32 (*RxLineAlarmCurrentStatusRegAddr)(Tha60210031SdhLine self);
    uint32 (*TxFramerPerChannelControlRegAddr)(Tha60210031SdhLine self);
    uint32 (*TohGlobalS1StableMonitoringThresholdControlRegAddr)(Tha60210031SdhLine self);
    uint32 (*TohS1MonitoringStatusRegAddr)(Tha60210031SdhLine self);
    uint32 (*TohK1MonitoringStatusRegAddr)(Tha60210031SdhLine self);
    uint32 (*TohGlobalK1StableMonitoringThresholdControlRegAddr)(Tha60210031SdhLine self);
    uint32 (*TohK2MonitoringStatusRegAddr)(Tha60210031SdhLine self);
    uint32 (*TohGlobalK2StableMonitoringThresholdControlRegAddr)(Tha60210031SdhLine self);
    eBool (*Ec1TxAisForceIsSupported)(Tha60210031SdhLine self);
    }tTha60210031SdhLineMethods;

typedef struct tTha60210031SdhLine
    {
    tThaSdhLine super;
    const tTha60210031SdhLineMethods *methods;
    }tTha60210031SdhLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine Tha60210031SdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SDHLINEINTERNAL_H_ */


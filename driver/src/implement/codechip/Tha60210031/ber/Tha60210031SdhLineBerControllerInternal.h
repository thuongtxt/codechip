/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210031SdhLineBerControllerInternal.h
 * 
 * Created Date: Sep 8, 2016
 *
 * Description : Tha60210031SdhLineBerControllerInternal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SDHLINEBERCONTROLLERINTERNAL_H_
#define _THA60210031SDHLINEBERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SdhLineBerController
    {
    tTha60210011SdhAuVcBerController super;
    }tTha60210031SdhLineBerController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60210031SdhLineBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SDHLINEBERCONTROLLERINTERNAL_H_ */


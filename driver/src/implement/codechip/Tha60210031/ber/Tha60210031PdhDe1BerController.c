/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210031PdhDe1BerController.c
 *
 * Created Date: Aug 5, 2015
 *
 * Description : DE1 BER controller of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "Tha60210031ModuleBer.h"
#include "Tha60210031ModuleBerInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/* Cache super */
static const tAtBerControllerMethods           *m_AtBerControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint8 slice = 0, hwSts = 0;
    uint32 tug2Id;

    if (vc)
        tug2Id = AtSdhChannelTug2Get(vc);
    else
        tug2Id = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)de1);

    ThaPdhChannelHwIdGet((AtPdhChannel)de1, cThaModuleOcn, &slice, &hwSts);

    return (hwSts * 32UL + (slice + 2UL) * 8UL + tug2Id);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    uint32 tug2Id;
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint32 tu1xId = (vc == NULL) ? AtChannelIdGet((AtChannel)de1) : AtSdhChannelTu1xGet(vc);
    uint8 slice = 0, hwSts = 0;

    if (vc)
        {
        tug2Id = AtSdhChannelTug2Get(vc);
        tu1xId = AtSdhChannelTu1xGet(vc);
        }
    else
        {
        tug2Id = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)de1);
        tu1xId = ThaPdhDe2De1HwDe1IdGet((ThaPdhDe1)de1);
        }

    ThaPdhChannelHwIdGet((AtPdhChannel)de1, cThaModuleOcn, &slice, &hwSts);

    return (hwSts * 112UL + (slice + 2UL) * 28UL + tug2Id * 4 + tu1xId);
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 56UL + (slice + 2UL) * 28UL + AtSdhChannelTug2Get(sdhChannel) * 4UL + AtSdhChannelTu1xGet(sdhChannel);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhDe1BerController);
    }

AtBerController Tha60210031PdhDe1BerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDe1BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210031PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PdhDe1BerControllerObjectInit(newController, controllerId, channel, berModule);
    }

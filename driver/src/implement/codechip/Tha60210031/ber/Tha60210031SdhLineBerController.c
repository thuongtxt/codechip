/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210031SdhLineBerController.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : SDH Line BER
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031SdhLineBerControllerInternal.h"
#include "Tha60210031ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                  m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods  m_Tha60210011SdhAuVcBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtChannel sdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return (hwSts * 8UL + (slice + 2UL) * 2UL);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtChannel sdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 32UL + (slice + 2UL) * 8UL;
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
	AtUnused(self);
	return 22;
    }

static eBool ThresholdIsSupported(Tha60210011SdhAuVcBerController self, eAtBerRate threshold)
    {
    AtUnused(self);
    if ((threshold >= cAtBerRate1E3)&& (threshold <= cAtBerRate1E10))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    return mMethodsGet(controller)->CurrentBerOffset(controller) + Tha60210011BerMeasureStsChannelOffset(module);
    }

static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhLineAlarmBerSd)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhLineAlarmBerSf)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtSdhLineAlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, ThresholdIsSupported);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SdhLineBerController);
    }

AtBerController Tha60210031SdhLineBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhAuVcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210031SdhLineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031SdhLineBerControllerObjectInit(newController, controllerId, channel, berModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210031SdhVc1xBerController.c
 *
 * Created Date: Sep 24, 2015
 *
 * Description : VC1x BER controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"
#include "Tha60210031ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031SdhVc1xBerController
    {
    tTha60210011SdhVc1xBerController super;
    }tTha60210031SdhVc1xBerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                  m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods  m_Tha60210011SdhAuVcBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);

    return (hwSts * 32UL + slice * 8UL + AtSdhChannelTug2Get(sdhChannel));
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 112UL + slice * 28UL + AtSdhChannelTug2Get(sdhChannel) * 4UL + AtSdhChannelTu1xGet(sdhChannel);
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)sdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 56UL + slice * 28UL + AtSdhChannelTug2Get(sdhChannel) * 4UL + AtSdhChannelTu1xGet(sdhChannel);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SdhVc1xBerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhVc1xBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210031SdhVc1xBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210031ModuleBer.h
 * 
 * Created Date: Aug 5, 2015
 *
 * Description : 60210031 module BER interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEBER_H_
#define _THA60210031MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/ber/AtModuleBerInternal.h"
#include "../../Tha60210011/ber/Tha60210011ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer Tha60210031ModuleBerNew(AtDevice device);
AtBerController Tha60210031PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210031SdhAuVcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210031SdhVc1xBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210031SdhLineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210031PdhDe3PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210031PdhDe3LineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

eBool Tha60210031PdhDe3BerIsLineMode(AtBerController self);
eBool Tha60210031PdhDe3BerIsPathMode(AtBerController self);
void Tha60210031PdhDe3BerLineModeSet(AtBerController self);
void Tha60210031PdhDe3BerPathModeSet(AtBerController self);

void Tha60210031ModuleBerBerLineEc1ThresholdInit(Tha60210011ModuleBer self);
void Tha60210031ModuleBerLineDs3ThresholdInit(Tha60210011ModuleBer self);
void Tha60210031ModuleBerLineE3ThresholdInit(Tha60210011ModuleBer self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEBER_H_ */


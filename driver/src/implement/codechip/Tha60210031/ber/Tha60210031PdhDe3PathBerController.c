/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210031PdhDe3PathBerController.c
 *
 * Created Date: Nov 17, 2015
 *
 * Description : Path layer BER controller of DS3/E3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"
#include "../pdh/Tha60210031PdhDe3.h"
#include "Tha60210031ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
/* Internal BER Line and Path mode switching base on EC1 or DS3/E3 */
#define cAf6_Reg_Ber_Ctrl_Mode0 0x60004
#define cAf6_Reg_Ber_Ctrl_Mode1 0x60005

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031PdhDe3PathBerController
    {
    tTha60210011SdhTu3VcBerController super;
    }tTha60210031PdhDe3PathBerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/* Cache super */
static const tAtBerControllerMethods          *m_AtBerControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaskByHwSts(uint32 hwSts)
    {
    return cBit0 << (hwSts % 24);
    }

static uint32 ShiftByHwSts(uint32 hwSts)
    {
    return hwSts % 24;
    }

static void BerDe3PathEnable(AtBerController self, eBool pathEnable)
    {
    const uint8 cBerPdhLineMode = 1;
    const uint8 cBerPdhPathMode = 0;
    uint32 regAddr, regVal;
    uint8 slice, hwSts;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    AtPdhChannel pdhChannel = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    
    ThaPdhChannelHwIdGet(pdhChannel, cThaModulePoh, &slice, &hwSts);
    regAddr = baseAddress + cAf6_Reg_Ber_Ctrl_Mode0 + slice;
    regVal =  AtBerControllerRead(self, regAddr);
    mFieldIns(&regVal, MaskByHwSts(hwSts), ShiftByHwSts(hwSts), pathEnable ? cBerPdhPathMode : cBerPdhLineMode);
    AtBerControllerWrite(self, regAddr, regVal);
    }

static eBool BerDe3PathIsEnabled(AtBerController self)
    {
    uint32 regAddr, regVal;
    uint8 slice, hwSts;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    AtPdhChannel pdhChannel = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    
    ThaPdhChannelHwIdGet(pdhChannel, cThaModulePoh, &slice, &hwSts);
    regAddr = baseAddress + cAf6_Reg_Ber_Ctrl_Mode0 + slice;
    regVal =  AtBerControllerRead(self, regAddr);
    
    return(regVal & MaskByHwSts(hwSts)) ? cAtFalse: cAtTrue;
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtChannel pdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;
    ThaPdhChannelHwIdGet((AtPdhChannel)pdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 32UL + (slice + 2UL) * 8UL;
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtChannel pdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint8 slice = 0, hwSts = 0;

    ThaPdhChannelHwIdGet((AtPdhChannel)pdhChannel, cThaModulePoh, &slice, &hwSts);
    return hwSts * 8UL + (slice + 2UL) * 2UL + 1UL;
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtChannel pdhChannel = AtBerControllerMonitoredChannel((AtBerController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)pdhChannel);
    switch (frameType)
        {
        case cAtPdhE3Frmg832:
            return 18;
        case cAtPdhE3FrmG751:
            return 19;
        default:
            return 17;
        }
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    eAtRet ret = m_AtBerControllerMethods->Enable(self, enable);
    if ((ret == cAtOk) && (enable))
        Tha60210031PdhDe3BerPathModeSet(self);

    return ret;
    }

static eBool IsEnabled(AtBerController self)
    {
    if (Tha60210031PdhDe3BerIsLineMode(self))
        return cAtFalse;

    return m_AtBerControllerMethods->IsEnabled(self);
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    Tha60210011ModuleBer module = (Tha60210011ModuleBer) AtBerControllerModuleGet(self);
    return mMethodsGet(controller)->CurrentBerOffset(controller) + Tha60210011BerMeasureStsChannelOffset(module);
    }

static eBool IsSd(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmSdBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmSfBer)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    if (AtChannelDefectGet(channel) & cAtPdhDe3AlarmBerTca)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, IsTca);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));
        
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhDe3PathBerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SdhTu3VcBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210031PdhDe3PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

void Tha60210031PdhDe3BerLineModeSet(AtBerController self)
    {
    BerDe3PathEnable(self, cAtFalse);
    }

eBool Tha60210031PdhDe3BerIsLineMode(AtBerController self)
    {
    return BerDe3PathIsEnabled(self) ? cAtFalse : cAtTrue;
    }

void Tha60210031PdhDe3BerPathModeSet(AtBerController self)
    {
    BerDe3PathEnable(self, cAtTrue);
    }

eBool Tha60210031PdhDe3BerIsPathMode(AtBerController self)
    {
    return BerDe3PathIsEnabled(self);
    }


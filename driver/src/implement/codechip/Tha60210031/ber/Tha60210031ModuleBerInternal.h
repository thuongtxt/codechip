/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210031ModuleBerInternal.h
 * 
 * Created Date: Jun 11, 2016
 *
 * Description : Internal data of BER
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210031_BER_THA60210031MODULEBERINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210031_BER_THA60210031MODULEBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210011/ber/Tha60210011ModuleBer.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleBer *Tha60210031ModuleBer;

typedef struct tTha60210031ModuleBer
    {
    tTha60210011ModuleBer super;
    }tTha60210031ModuleBer;

typedef struct tTha60210031PdhDe1BerController
    {
    tTha60210011PdhDe1BerController super;
    }tTha60210031PdhDe1BerController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer Tha60210031ModuleBerObjectInit(AtModuleBer self, AtDevice device);
AtBerController Tha60210031PdhDe1BerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210031_BER_THA60210031MODULEBERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210031ModuleBer.c
 *
 * Created Date: Aug 5, 2015
 *
 * Description : Module BER of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ber/Tha60210011ModuleBer.h"
#include "../poh/Tha60210031ModulePohReg.h"
#include "Tha60210031ModuleBerInternal.h"
#include "Tha60210031ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods              m_AtModuleOverride;
static tAtModuleBerMethods           m_AtModuleBerOverride;
static tThaModuleHardBerMethods      m_ThaModuleHardBerOverride;
static tTha60210011ModuleBerMethods  m_Tha60210011ModuleBerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerStsTu3ReportBaseAddress(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_ramberrateststu3_Base;
    }

static uint32 BerMeasureStsChannelOffset(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return 1344UL;
    }

static AtBerController De1PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    return Tha60210031PdhDe1BerControllerNew(AtChannelIdGet((AtChannel)de1), (AtChannel)de1, (AtModuleBer)self);
    }

static AtBerController De3PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    return Tha60210031PdhDe3PathBerControllerNew(AtChannelIdGet((AtChannel)de3), (AtChannel)de3, (AtModuleBer)self);
    }

static AtBerController De1LineBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return NULL;
    }

static AtBerController De3LineBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    return Tha60210031PdhDe3LineBerControllerNew(AtChannelIdGet((AtChannel)de3), (AtChannel)de3, (AtModuleBer)self);
    }

static AtBerController AuVcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel auVc)
    {
    return Tha60210031SdhAuVcBerControllerNew(AtChannelIdGet((AtChannel)auVc), (AtChannel)auVc, (AtModuleBer)self);
    }

static AtBerController Vc1xBerControllerCreate(ThaModuleHardBer self, AtSdhChannel vc1x)
    {
    return Tha60210031SdhVc1xBerControllerNew(AtChannelIdGet((AtChannel)vc1x), (AtChannel)vc1x, (AtModuleBer)self);
    }


static AtBerController SdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
	if (AtSdhLineRateGet((AtSdhLine)line) == cAtSdhLineRateStm0)
		return Tha60210031SdhLineBerControllerNew(AtChannelIdGet(line), line, self);
    return NULL;
    }

static void BerLineEc1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineEc1Threshold1Rate),     cLineEc1Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate),     cLineEC1Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 1, cLineEC1Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 2, cLineEC1Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 3, cLineEC1Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 4, cLineEC1Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 5, cLineEC1Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 6, cLineEC1Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 7, cLineEC1Threshold2_7);
    }

static void BerLineDs3ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineDs3Threshold1Rate),     cLineDs3Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate),     cLineDs3Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 1, cLineDs3Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 2, cLineDs3Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 3, cLineDs3Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 4, cLineDs3Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 5, cLineDs3Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 6, cLineDs3Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 7, cLineDs3Threshold2_7);
    }


static void BerLineE3ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineE3Threshold1Rate),     cLineE3Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate),     cLineE3Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 1, cLineE3Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 2, cLineE3Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 3, cLineE3Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 4, cLineE3Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 5, cLineE3Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 6, cLineE3Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 7, cLineE3Threshold2_7);
    }


static void BerLineEc1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineEc1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineEc1Threshold1Rate),     0x1D671);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate),     0x440022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 1, 0x440022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 2, 0x24);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 3, 0x31201D3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 4, 0x34C01ED);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 5, 0x35201F1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 6, 0x35401F1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 7, 0x35401F1);
    }

static void BerLineDs3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineDs3ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineDs3Threshold1Rate),     0x338c7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate),     0x3A001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 1, 0x3A001D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 2, 0x21);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 3, 0x2E20271);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 4, 0x2E20271);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 5, 0x2E20271);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 6, 0x2E20271);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs3Threshold2Rate) + 7, 0x2E20271);
    }

static void BerLineE3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineE3ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineE3Threshold1Rate),     0x27296);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate),     0x2A0015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 1, 0x2A0015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 2, 0x18);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 3, 0x23001E0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 4, 0x23001E0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 5, 0x23001E0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 6, 0x23001E0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE3Threshold2Rate) + 7, 0x23001E0);
    }

static void DefaultSet(Tha60210011ModuleBer self)
    {
    Tha60210011ModuleBerSts1ThresholdInit(self);
    Tha60210011ModuleBerVc1xThresholdInit(self);
    Tha60210011ModuleBerE1ThresholdInit(self);
    Tha60210011ModuleBerDs1ThresholdInit(self);
    Tha60210011ModuleBerDs3ThresholdInit(self);
    Tha60210011ModuleBerE3ThresholdInit(self);
    BerLineDs3ThresholdInit(self);
    BerLineE3ThresholdInit(self);
    BerLineEc1ThresholdInit(self);
    }

static eAtRet Debug(AtModule self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    Tha60210011ModuleBerIsEnabledPrint(module);
    Tha60210011ModuleBerThresholdPrint(module, "VT-15",  cVt15Threshold1Rate , cVt15Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "VT-2",   cVt2Threshold1Rate  , cVt2Threshold2Rate  );
    Tha60210011ModuleBerThresholdPrint(module, "E1",     cE1Threshold1Rate   , cE1Threshold2Rate   );
    Tha60210011ModuleBerThresholdPrint(module, "DS1",    cDs1Threshold1Rate  , cDs1Threshold2Rate  );
    Tha60210011ModuleBerThresholdPrint(module, "STS-1",  cSts1Threshold1Rate , cSts1Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "DS3",     cDs3Threshold1Rate , cDs3Threshold2Rate );
    return cAtOk;
    }

static AtBerController SdhLineMsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    return Tha60210031SdhLineBerControllerNew(controllerId, line, self);
    }

static uint32 Imemrwptrsh1Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwptrsh1_Base;
    }

static uint32 Threshold1Offset(Tha60210011ModuleBer self, uint32 rate)
    {
    AtUnused(self);
    return rate;
    }

static eBool MeasureTimeEngineIsSupported(AtModuleBer self)
    {
    AtDevice dev = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader reader = ThaDeviceVersionReader(dev);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(reader);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x8, 0x4810);

    if (currentVersion >= supportedVersion)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtModule(AtModuleBer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, MeasureTimeEngineIsSupported);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void OverrideThaModuleHardBer(AtModuleBer self)
    {
    ThaModuleHardBer module = (ThaModuleHardBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardBerOverride, mMethodsGet(module), sizeof(m_ThaModuleHardBerOverride));

        mMethodOverride(m_ThaModuleHardBerOverride, AuVcBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, Vc1xBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De1PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De1LineBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3LineBerControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleHardBerOverride);
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleBerOverride));

        mMethodOverride(m_Tha60210011ModuleBerOverride, DefaultSet);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerStsTu3ReportBaseAddress);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Imemrwptrsh1Base);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Threshold1Offset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerMeasureStsChannelOffset);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    OverrideTha60210011ModuleBer(self);
    OverrideThaModuleHardBer(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleBer);
    }

AtModuleBer Tha60210031ModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60210031ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleBerObjectInit(newBerModule, device);
    }

void Tha60210031ModuleBerBerLineEc1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerLineEc1ThresholdInit(self);
    }

void Tha60210031ModuleBerLineDs3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerLineDs3ThresholdInit(self);
    }

void Tha60210031ModuleBerLineE3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (self)
        BerLineE3ThresholdInit(self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210031ModuleRamInternal.h
 * 
 * Created Date: Jul 10, 2015
 *
 * Description : RAM module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULERAMINTERNAL_H_
#define _THA60210031MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaModuleRam.h"
#include "../../Tha60210011/ram/Tha6021ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define sPDH_VT_Async_Map_Control                           "PDH VT Async Map Control"
#define sPseudowire_Transmit_Ethernet_Header_Value_Control  "Pseudowire Transmit Ethernet Header Value Control"
#define sSoftwave_Control                                   "Softwave Control"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleRam
    {
    tTha6021ModuleRam super;
    }tTha60210031ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60210031ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULERAMINTERNAL_H_ */


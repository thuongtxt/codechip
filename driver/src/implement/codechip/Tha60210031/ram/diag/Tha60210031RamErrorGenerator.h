/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210031RamErrorGenerator.h
 * 
 * Created Date: Mar 24, 2016
 *
 * Description : Interfaces of the Tha60210031RamErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031RAMERRORGENERATOR_H_
#define _THA60210031RAMERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtErrorGenerator Tha60210031RamCrcErrorGeneratorNew(AtModule module);
AtErrorGenerator Tha60210031TxDdrEccErrorGeneratorNew(AtModule module);
AtErrorGenerator Tha60210031RxDdrEccErrorGeneratorNew(AtModule module);

uint32 Tha60210031RamErrorGeneratorRead(AtErrorGenerator self, uint32 address);
void Tha60210031RamErrorGeneratorWrite(AtErrorGenerator self, uint32 address, uint32 value);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210031RAMERRORGENERATOR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      :  RAM
 *
 * File        : Tha60210031RamPdaErrorGenerator.c
 *
 * Created Date: Mar 24, 2016
 *
 * Description : Implementation of the Tha60210031RamCrcErrorErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "Tha60210031RamErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210031RamCrcErrorGenerator)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031RamCrcErrorGeneratorMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtErrorGeneratorMethods m_AtErrorGeneratorOverride;

/* Save super implementation */
static const tAtObjectMethods         *m_AtObjectMethods         = NULL;
static const tAtErrorGeneratorMethods *m_AtErrorGeneratorMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031RamCrcErrorGenerator);
    }

static eBool ErrorTypeIsSupported(AtErrorGenerator self, eAtRamGeneratorErrorType errorType)
    {
    AtUnused(self);
    return (errorType == cAtRamGeneratorErrorTypeCrcError) ? cAtTrue : cAtFalse;
    }

static uint32 ForceErrorRateCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x000140;
    }

static uint32 ForceErrorRateShift(AtErrorGenerator self)
    {
    AtUnused(self);
    return 28;
    }

static uint32 ForceErrorRateMask(AtErrorGenerator self)
    {
    AtUnused(self);
    return cBit28;
    }

static uint32 ErrorThresholdInsertCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x000140;
    }

static uint32 ForceErrorThresholdShift(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ForceErrorThresholdMask(AtErrorGenerator self)
    {
    AtUnused(self);
    return cBit27_0;
    }

static uint32 ForceErrorTypeCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 ForceErrorTypeShift(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 ForceErrorTypeMask(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet ErrorTypeSet(AtErrorGenerator self, uint32 mode)
    {
    AtUnused(self);
    return (mode == cAtRamGeneratorErrorTypeCrcError) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 ErrorTypeGet(AtErrorGenerator self)
    {
    AtUnused(self);
    return cAtRamGeneratorErrorTypeCrcError;
    }

static void HwModeSet(AtErrorGenerator self, uint32 mode)
    {
    uint32 address = mMethodsGet(mThis(self))->ForceErrorRateCtrlRegister(self);
    uint32 modeShift = mMethodsGet(mThis(self))->ForceErrorRateShift(self);
    uint32 modeMask = mMethodsGet(mThis(self))->ForceErrorRateMask(self);
    uint32 regVal = Tha60210031RamErrorGeneratorRead(self, address);
    mRegFieldSet(regVal, mode, (mode == cAtErrorGeneratorModeContinuous) ? 1 : 0);
    Tha60210031RamErrorGeneratorWrite(self, address, regVal);
    }

static eAtRet ModeSet(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    if (!AtErrorGeneratorModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    HwModeSet(self, mode);
    return cAtOk;
    }

static eAtErrorGeneratorMode ModeGet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ForceErrorRateCtrlRegister(self);
    uint32 modeMask = mMethodsGet(mThis(self))->ForceErrorRateMask(self);
    uint32 regVal = Tha60210031RamErrorGeneratorRead(self, address);
    return (regVal & modeMask) ? cAtErrorGeneratorModeContinuous : cAtErrorGeneratorModeOneshot;
    }

static void HwErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    uint32 address = mMethodsGet(mThis(self))->ErrorThresholdInsertCtrlRegister(self);
    uint32 errorThresShift = mMethodsGet(mThis(self))->ForceErrorThresholdShift(self);
    uint32 errorThresMask = mMethodsGet(mThis(self))->ForceErrorThresholdMask(self);
    uint32 regVal = Tha60210031RamErrorGeneratorRead(self, address);
    mRegFieldSet(regVal, errorThres, errorNum);
    Tha60210031RamErrorGeneratorWrite(self, address, regVal);
    }

static uint32 HwErrorNumGet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ErrorThresholdInsertCtrlRegister(self);
    uint32 errorThresShift = mMethodsGet(mThis(self))->ForceErrorThresholdShift(self);
    uint32 errorThresMask = mMethodsGet(mThis(self))->ForceErrorThresholdMask(self);
    uint32 regVal = Tha60210031RamErrorGeneratorRead(self, address);
    return mRegField(regVal, errorThres);
    }

static eAtRet ErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeOneshot)
        return cAtErrorNotApplicable;

    if (errorNum > mMethodsGet(mThis(self))->ForceErrorThresholdMask(self))
        return cAtErrorOutOfRangParm;

    HwErrorNumSet(self, errorNum);
    return AtErrorGeneratorErrorNumDbSet(self, errorNum);
    }

static eAtRet ErrorRateSet(AtErrorGenerator self, eAtBerRate errorRate)
    {
    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeContinuous)
        return cAtErrorNotApplicable;

    HwErrorNumSet(self, AtErrorGeneratorErrorRateToErrorNum(self, errorRate));
    return AtErrorGeneratorErrorRateDbSet(self, errorRate);
    }

static eAtRet Start(AtErrorGenerator self)
    {
    if (AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeOneshot)
        {
        HwErrorNumSet(self, AtErrorGeneratorErrorNumGet(self));
        return cAtOk;
        }

    if (AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeContinuous)
        {
        HwErrorNumSet(self, AtErrorGeneratorErrorRateToErrorNum(self, AtErrorGeneratorErrorRateGet(self)));
        return cAtOk;
        }

    return cAtErrorNotReady;
    }

static eAtRet Stop(AtErrorGenerator self)
    {
    HwErrorNumSet(self, 0);
    return cAtOk;
    }

static eBool IsStarted(AtErrorGenerator self)
    {
    return HwErrorNumGet(self) != 0 ? cAtTrue : cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210031RamCrcErrorGenerator object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    }

static void OverrideAtObject(AtErrorGenerator self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtErrorGenerator(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtErrorGeneratorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtErrorGeneratorOverride, m_AtErrorGeneratorMethods, sizeof(m_AtErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeGet);
        mMethodOverride(m_AtErrorGeneratorOverride, ModeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ModeGet);
        mMethodOverride(m_AtErrorGeneratorOverride, Start);
        mMethodOverride(m_AtErrorGeneratorOverride, Stop);
        mMethodOverride(m_AtErrorGeneratorOverride, IsStarted);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorNumSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorRateSet);
        }

    mMethodsSet(self, &m_AtErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    OverrideAtObject(self);
    OverrideAtErrorGenerator(self);
    }

static void MethodsInit(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ForceErrorRateCtrlRegister);
        mMethodOverride(m_methods, ForceErrorRateShift);
        mMethodOverride(m_methods, ForceErrorRateMask);
        mMethodOverride(m_methods, ErrorThresholdInsertCtrlRegister);
        mMethodOverride(m_methods, ForceErrorThresholdShift);
        mMethodOverride(m_methods, ForceErrorThresholdMask);
        mMethodOverride(m_methods, ForceErrorTypeCtrlRegister);
        mMethodOverride(m_methods, ForceErrorTypeShift);
        mMethodOverride(m_methods, ForceErrorTypeMask);
        mMethodOverride(m_methods, ErrorTypeIsSupported);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

AtErrorGenerator Tha60210031RamCrcErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtErrorGeneratorObjectInit(self, NULL) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    mThis(self)->module = module;

    return self;
    }

AtErrorGenerator Tha60210031RamCrcErrorGeneratorNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031RamCrcErrorGeneratorObjectInit(errorGenerator, module);
    }

uint32 Tha60210031RamErrorGeneratorRead(AtErrorGenerator self, uint32 address)
    {
    return mModuleHwRead(mThis(self)->module, address);
    }

void Tha60210031RamErrorGeneratorWrite(AtErrorGenerator self, uint32 address, uint32 value)
    {
    mModuleHwWrite(mThis(self)->module, address, value);
    }

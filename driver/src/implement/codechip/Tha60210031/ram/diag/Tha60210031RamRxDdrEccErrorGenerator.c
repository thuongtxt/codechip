/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031RamRxDdrEccErrorGenerator.c
 *
 * Created Date: Mar 24, 2016
 *
 * Description : Implementation of the Tha60210031RamRxDdrEccErrorErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031RamErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tTha60210031RamCrcErrorGeneratorMethods m_Tha60210031RamCrcErrorGeneratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031RamRxDdrEccErrorGenerator);
    }

static uint32 ForceErrorRateCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x148;
    }

static uint32 ErrorThresholdInsertCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x148;
    }

static uint32 ForceErrorTypeCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x148;
    }

static void Tha60210031RamCrcErrorGeneratorOverride(AtErrorGenerator self)
    {
    Tha60210031RamCrcErrorGenerator ramCrcErrGen = (Tha60210031RamCrcErrorGenerator)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_Tha60210031RamCrcErrorGeneratorOverride,
                                  mMethodsGet(ramCrcErrGen),
                                  sizeof(m_Tha60210031RamCrcErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorRateCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ErrorThresholdInsertCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorTypeCtrlRegister);
        }

    mMethodsSet(ramCrcErrGen, &m_Tha60210031RamCrcErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    Tha60210031RamCrcErrorGeneratorOverride(self);
    }

AtErrorGenerator Tha60210031RxDdrEccErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031TxDdrEccErrorGeneratorObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator Tha60210031RxDdrEccErrorGeneratorNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031RxDdrEccErrorGeneratorObjectInit(errorGenerator, module);
    }

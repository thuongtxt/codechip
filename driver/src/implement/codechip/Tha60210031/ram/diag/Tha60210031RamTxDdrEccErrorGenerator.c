/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031RamTxDdrEccErrorGenerator.c
 *
 * Created Date: Mar 24, 2016
 *
 * Description : Implementation of the Tha60210031RamTxDdrEccErrorErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031RamErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210031RamCrcErrorGenerator)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tTha60210031RamCrcErrorGeneratorMethods m_Tha60210031RamCrcErrorGeneratorOverride;
static tAtErrorGeneratorMethods m_AtErrorGeneratorOverride;

static const tAtErrorGeneratorMethods *m_AtErrorGeneratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031RamTxDdrEccErrorGenerator);
    }

static eBool ErrorTypeIsSupported(AtErrorGenerator self, eAtRamGeneratorErrorType errorType)
    {
    AtUnused(self);
    switch ((uint32)errorType)
        {
        case cAtRamGeneratorErrorTypeEccCorrectable: return cAtTrue;
        case cAtRamGeneratorErrorTypeEccUncorrectable: return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 ForceErrorRateCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x000150;
    }

static uint32 ErrorThresholdInsertCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x000150;
    }

static uint32 ForceErrorTypeCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x000150;
    }

static uint32 ForceErrorTypeShift(AtErrorGenerator self)
    {
    AtUnused(self);
    return 29;
    }

static uint32 ForceErrorTypeMask(AtErrorGenerator self)
    {
    AtUnused(self);
    return cBit29;
    }

static void HwErrorTypeSet(AtErrorGenerator self, uint32 mode)
    {
    uint32 address = mMethodsGet(mThis(self))->ForceErrorTypeCtrlRegister(self);
    uint32 modeShift = mMethodsGet(mThis(self))->ForceErrorTypeShift(self);
    uint32 modeMask = mMethodsGet(mThis(self))->ForceErrorTypeMask(self);
    uint32 regVal = Tha60210031RamErrorGeneratorRead(self, address);
    mRegFieldSet(regVal, mode, (mode == cAtRamGeneratorErrorTypeEccUncorrectable) ? 1 : 0);
    Tha60210031RamErrorGeneratorWrite(self, address, regVal);
    }

static eAtRet ErrorTypeSet(AtErrorGenerator self, uint32 mode)
    {
    if (!mMethodsGet(mThis(self))->ErrorTypeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    HwErrorTypeSet(self, mode);
    return cAtOk;
    }

static uint32 ErrorTypeGet(AtErrorGenerator self)
    {
    uint32 address = mMethodsGet(mThis(self))->ForceErrorTypeCtrlRegister(self);
    uint32 modeMask = mMethodsGet(mThis(self))->ForceErrorTypeMask(self);
    uint32 regVal = Tha60210031RamErrorGeneratorRead(self, address);
    return (regVal & modeMask) ? cAtRamGeneratorErrorTypeEccUncorrectable : cAtRamGeneratorErrorTypeEccCorrectable;
    }

static void AtDiagErrorGeneratorOverride(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtErrorGeneratorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtErrorGeneratorOverride, m_AtErrorGeneratorMethods, sizeof(m_AtErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeGet);
        }

    mMethodsSet(self, &m_AtErrorGeneratorOverride);
    }

static void Tha60210031RamCrcErrorGeneratorOverride(AtErrorGenerator self)
    {
    Tha60210031RamCrcErrorGenerator ramCrcErrGen = (Tha60210031RamCrcErrorGenerator)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_Tha60210031RamCrcErrorGeneratorOverride,
                                  mMethodsGet(ramCrcErrGen),
                                  sizeof(m_Tha60210031RamCrcErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorRateCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ErrorThresholdInsertCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorTypeCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorTypeShift);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorTypeMask);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ErrorTypeIsSupported);
        }

    mMethodsSet(ramCrcErrGen, &m_Tha60210031RamCrcErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    AtDiagErrorGeneratorOverride(self);
    Tha60210031RamCrcErrorGeneratorOverride(self);
    }

AtErrorGenerator Tha60210031TxDdrEccErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031RamCrcErrorGeneratorObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator Tha60210031TxDdrEccErrorGeneratorNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031TxDdrEccErrorGeneratorObjectInit(errorGenerator, module);
    }

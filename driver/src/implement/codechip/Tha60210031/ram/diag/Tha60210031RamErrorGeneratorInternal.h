/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210031RamErrorGeneratorInternal.h
 * 
 * Created Date: Mar 24, 2016
 *
 * Description : Methods and data of the Tha60210031RamErrorGenerator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031RAMERRORGENERATORINTERNAL_H_
#define _THA60210031RAMERRORGENERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/diag/AtErrorGeneratorInternal.h"
#include "../../../../../generic/man/AtModuleInternal.h"
#include "Tha60210031RamErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031RamCrcErrorGenerator *Tha60210031RamCrcErrorGenerator;

typedef struct tTha60210031RamCrcErrorGeneratorMethods
    {
    /* For Error-Rate One-shot with number of errors/Continuous */
    uint32 (*ForceErrorRateCtrlRegister)(AtErrorGenerator self);
    uint32 (*ForceErrorRateShift)(AtErrorGenerator self);
    uint32 (*ForceErrorRateMask)(AtErrorGenerator self);

    /* Insert Errors */
    uint32 (*ErrorThresholdInsertCtrlRegister)(AtErrorGenerator self);
    uint32 (*ForceErrorThresholdShift)(AtErrorGenerator self);
    uint32 (*ForceErrorThresholdMask)(AtErrorGenerator self);

    /* For Correctable/Uncorrectable */
    uint32 (*ForceErrorTypeCtrlRegister)(AtErrorGenerator self);
    uint32 (*ForceErrorTypeShift)(AtErrorGenerator self);
    uint32 (*ForceErrorTypeMask)(AtErrorGenerator self);

    /* For Check ErrorType Is support or not */
    eBool (*ErrorTypeIsSupported)(AtErrorGenerator self, eAtRamGeneratorErrorType errorType);
    } tTha60210031RamCrcErrorGeneratorMethods;

typedef struct tTha60210031RamCrcErrorGenerator
    {
    tAtErrorGenerator super;
    tTha60210031RamCrcErrorGeneratorMethods *methods;

    /* Private data */
    AtModule module;
    } tTha60210031RamCrcErrorGenerator;

typedef struct tTha60210031RamTxDdrEccErrorGenerator
    {
    tTha60210031RamCrcErrorGenerator super;
    } tTha60210031RamTxDdrEccErrorGenerator;

typedef struct tTha60210031RamRxDdrEccErrorGenerator
    {
    tTha60210031RamTxDdrEccErrorGenerator super;
    } tTha60210031RamRxDdrEccErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/
AtErrorGenerator Tha60210031RamCrcErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module);
AtErrorGenerator Tha60210031TxDdrEccErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module);
AtErrorGenerator Tha60210031RxDdrEccErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210031RAMERRORGENERATORINTERNAL_H_ */


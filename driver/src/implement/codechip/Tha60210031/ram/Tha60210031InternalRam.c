/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031InternalRam.c
 *
 * Created Date: Dec 7, 2015
 *
 * Description : Internal RAM implementation for product 60210031.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210031InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210031InternalRam*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtIpCore CoreIpGet(AtModule self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet(self), AtModuleDefaultCoreGet(self));
    }

static uint32 RamIdToDwordIndex(ThaInternalRam self)
    {
    return AtInternalRamLocalIdGet((AtInternalRam)self) >> 5;
    }

static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CrcMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    if (AtInternalRamIsReserved(self))
        return 0;

    return cAtRamAlarmParityError;
    }

static uint32 DisableCheckRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x131;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x130;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    return 0x112 + RamIdToDwordIndex(self);
    }

static uint32 ChannelMask(ThaInternalRam self)
    {
    return cBit0 << (AtInternalRamIdGet((AtInternalRam)self) & 0x1F);
    }

static eAtRet ErrorMonitorEnable(ThaInternalRam self, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->DisableCheckRegister(self);
    uint32 regMask = ChannelMask(self);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    longReg[dwordIndex] = (enable) ? longReg[dwordIndex] & ~regMask : longReg[dwordIndex] | regMask;
    mModuleHwLongWrite(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));

    return cAtOk;
    }

static eBool ErrorMonitorIsEnabled(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->DisableCheckRegister(self);
    uint32 regMask = ChannelMask(self);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    return (regMask & longReg[dwordIndex]) ? cAtFalse : cAtTrue;
    }

static eAtRet HwForcedErrorSet(ThaInternalRam self, uint32 errors, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->ErrorForceRegister(self);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;
    uint32 regMask = ChannelMask(self);

    AtUnused(errors);

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    longReg[dwordIndex] = (enable) ? (longReg[dwordIndex] | regMask) : (longReg[dwordIndex] & ~regMask);
    mModuleHwLongWrite(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));

    return cAtOk;
    }

static uint32 HwForcedErrorGet(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = mMethodsGet(self)->ErrorForceRegister(self);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;
    uint32 regMask = ChannelMask(self);

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    if (longReg[dwordIndex] & regMask)
        return mMethodsGet(self)->ErrorMask(self);
        
    return 0;
    }

static uint32 ErrorMask(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtRamAlarmParityError;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static uint32 counterTypeRegister(AtInternalRam self,
                                  uint16 counterType,
                                  eBool r2c)
    {
    AtUnused(self);
    AtUnused(counterType);
    AtUnused(self);
    return (r2c) ? 0x141 : 0x142;
    }

static uint32 HwCounterGet(AtInternalRam self, uint16 counterType, eBool r2c)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = counterTypeRegister(self, counterType, r2c);
    return mModuleHwRead(module, regAddr);
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    if (!AtInternalRamCounterTypeIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    if (!AtInternalRamCounterTypeIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet(self, counterType, cAtTrue);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031InternalRam);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        /* Common */
        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, ForcableErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CounterGet);
        mMethodOverride(m_AtInternalRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamOverride, DisableCheckRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorStickyRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorEnable);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorIsEnabled);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorSet);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorGet);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMask);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

AtInternalRam Tha60210031InternalRamObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210031InternalRamNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210031InternalRamObjectInit(newRam, phyModule, ramId, localId);
    }

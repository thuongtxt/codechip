/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031InternalRam.c
 *
 * Created Date: Aug 31, 2017
 *
 * Description : Internal RAM implementation for product 60210031.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210031InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210031InternalRamCrc*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CrcMonitorIsSupported(AtInternalRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    AtUnused(self);
    return cAtRamAlarmCrcError;
    }

static eBool IsReserved(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ErrorMask(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtRamAlarmCrcError;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtUnused(self);
    return (counterType == cAtRamCounterTypeCrcErrors) ? cAtTrue : cAtFalse;
    }

static eBool ErrorGeneratorIsSupported(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031InternalRamCrc);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        /* Common */
        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, ForcableErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamOverride, ErrorMask);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorIsSupported);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031InternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210031InternalRamCrcNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

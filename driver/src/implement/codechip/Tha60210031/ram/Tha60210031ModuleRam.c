/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031ModuleRam.c
 *
 * Created Date: Dec 9, 2014
 *
 * Description : RAM module of product 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60210031DeviceReg.h"
#include "Tha60210031ModuleRamInternal.h"
#include "Tha60210031InternalRam.h"
#include "diag/Tha60210031RamErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cMaxNumInternalRam                                   100

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumDdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 3;
    }

static uint32 DdrUserClock(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 133000000;
    }

static const char *DdrTypeString(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return "DDR3";
    }

static uint32 DDRUserClockValueStatusRegister(ThaModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    return (0xF00064UL + ddrId);
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 23;
    }

static uint32 DdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return (ddrId == 0) ? 32 : 16;
    }

static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
    return Tha60210031DdrNew(self, core, ddrId);
    }

static eBool BurstTestIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DdrCalibStatusMask(ThaModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    return cDdrCalibStatusMask(ddrId);
    }

static uint8 NumQdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ClockDdrSwingRangeInPpm(ThaModuleRam self)
    {
    AtUnused(self);
    return 100;
    }

static uint32 ClockQdrSwingRangeInPpm(ThaModuleRam self)
    {
    AtUnused(self);
    return 100;
    }

static uint32 InterruptStatusGet(ThaModuleRam self)
    {
    return mModuleHwRead(self, 0x121);
    }

static eBool PhyModuleHasRamInterrupt(ThaModuleRam self, uint32 moduleId, uint32 ramIntr)
    {
    AtUnused(self);
    if (moduleId == cAtModuleRam)
        {
        /* TODO: current design will query all parities if any of 3-bits RAM OR
         * interrupt is set. It's not good solution. */
        return (ramIntr) ? cAtTrue : cAtFalse;
        }

    return cAtFalse;
    }

static uint32 StartVersionSupportInternalRamMonitoring(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x02, 0x37);
    }

static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInternalRamMonitoring(device))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CrcCounterIsSupported(ThaModuleRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceErrorGeneratorIsSupported((ThaDevice)device);
    }

static eBool EccCounterIsSupported(ThaModuleRam self)
    {
    return CrcCounterIsSupported(self);
    }

static const tThaInternalRamEntry* AllInternalRamEntry(ThaModuleRam self, uint32 *numRams)
    {
    static const tThaInternalRamEntry allEntries[] = {
                                          {"OCN Tx J0 Insertion Buffer SliceId 0",                           0xA21200},
                                          {"OCN Tx J0 Insertion Buffer SliceId 1",                           0xA21600},
                                          {"OCN Tx Framer Per Channel Control SliceId 0",                    0xA21000},
                                          {"OCN Tx Framer Per Channel Control SliceId 1",                    0xA21400},
                                          {"OCN STS Pointer Generator Per Channel Control SliceId 0",        0xA23000},
                                          {"OCN STS Pointer Generator Per Channel Control SliceId 1",        0xA23200},
                                          {"OCN VTTU Pointer Generator Per Channel Control SliceId 0",       0xA60800},
                                          {"OCN VTTU Pointer Generator Per Channel Control SliceId 1",       0xA64800},
                                          {"OCN TXPP Per STS Multiplexing Control SliceId 0",                0xA60000},
                                          {"OCN TXPP Per STS Multiplexing Control SliceId 1",                0xA64000},
                                          {"OCN VTTU Pointer Interpreter Per Channel Control SliceId 0",     0xA40800},
                                          {"OCN VTTU Pointer Interpreter Per Channel Control SliceId 1",     0xA44800},
                                          {"OCN RXPP Per STS payload Control SliceId 0",                     0xA40000},
                                          {"OCN RXPP Per STS payload Control SliceId 1",                     0xA44000},
                                          {"OCN STS Pointer Interpreter Per Channel Control SliceId 0",      0xA22000},
                                          {"OCN STS Pointer Interpreter Per Channel Control SliceId 1",      0xA22200},
                                          {"OCN TOH Monitoring Per Channel Control SliceId 1",               0xA24100},
                                          {"POH CPE STS/TU3 Control Register",                               0xB2A000},
                                          {"POH CPE VT Control Register",                                    0xB28000},
                                          {"POH Termintate Insert Control STS",                              0xB40400},
                                          {"POH Termintate Insert Control VT/TU3",                           0xB44000},
                                          {"POH BER Threshold 2",                                            0xB60400},
                                          {"POH BER Control VT/DSN, POH BER Control STS/TU3",                0xB62000},
                                          {"PDH DS1/E1/J1 Rx Framer Mux Control SliceId 0",                  0x730000},
                                          {"PDH STS/VT Demap Control SliceId 0",                             0x724000},
                                          {"PDH RxM23E23 Control SliceId 0",                                 0x740000},
                                          {"PDH RxDS3E3 OH Pro Control SliceId 0",                           0x740420},
                                          {"PDH RxM12E12 Control SliceId 0",                                 0x741000},
                                          {"PDH DS1/E1/J1 Rx Framer Control SliceId 0",                      0x754000},
                                          {"PDH DS1/E1/J1 Tx Framer Control SliceId 0",                      0x794000},
                                          {"PDH DS1/E1/J1 Tx Framer Signalling Control SliceId 0",           0x794800},
                                          {"PDH TxM12E12 Control SliceId 0",                                 0x781000},
                                          {"PDH TxM23E23 Control SliceId 0",                                 0x780000},
                                          {"PDH TxM23E23 E3g832 Trace Byte SliceId 0",                       0x780200},
                                          {"PDH STS/VT Map Control SliceId 0",                               0x776000},
                                          {sPDH_VT_Async_Map_Control" SliceId 0",                            0x776800},
                                          {"PDH DS1/E1/J1 Rx Framer Mux Control SliceId 1",                  0x830000},
                                          {"PDH STS/VT Demap Control SliceId 1",                             0x824000},
                                          {"PDH RxM23E23 Control SliceId 1",                                 0x840000},
                                          {"PDH RxDS3E3 OH Pro Control SliceId 1",                           0x840420},
                                          {"PDH RxM12E12 Control SliceId 1",                                 0x841000},
                                          {"PDH DS1/E1/J1 Rx Framer Control SliceId 1",                      0x854000},
                                          {"PDH DS1/E1/J1 Tx Framer Control SliceId 1",                      0x894000},
                                          {"PDH DS1/E1/J1 Tx Framer Signalling Control SliceId 1",           0x894800},
                                          {"PDH TxM12E12 Control SliceId 1",                                 0x881000},
                                          {"PDH TxM23E23 Control SliceId 1",                                 0x880000},
                                          {"PDH TxM23E23 E3g832 Trace Byte SliceId 1",                       0x880200},
                                          {"PDH STS/VT Map Control SliceId 1",                               0x876000},
                                          {sPDH_VT_Async_Map_Control" SliceId 1",                            0x876800},
                                          {"MAP Thalassa Map Channel Control SliceId 0",                     0x1B4000},
                                          {"MAP Thalassa Map Line Control SliceId 0",                        0x1B0000},
                                          {"MAP Thalassa Map Channel Control SliceId 1",                     0x1D4000},
                                          {"MAP Thalassa Map Line Control SliceId 1",                        0x1D0000},
                                          {"DEMAP Thalassa Demap Channel Control SliceId 0",                 0x1A0000},
                                          {"DEMAP Thalassa Demap Channel Control SliceId 1",                 0x1C0000},
                                          {"CDR STS Timing control SliceId 0",                               0x280800},
                                          {"CDR VT Timing control SliceId 0",                                0x280C00},
                                          {"CDR ACR Engine Timing control SliceId 0",                        0x2A0800},
                                          {"CDR STS Timing control SliceId 1",                               0x2C0800},
                                          {"CDR VT Timing control SliceId 1",                                0x2C0C00},
                                          {"CDR ACR Engine Timing control SliceId 1",                        0x2E0800},
                                          {"Thalassa PDHPW Payload Assemble Payload Size Control SliceId 0", 0x202000},
                                          {"Thalassa PDHPW Payload Assemble Payload Size Control SliceId 1", 0x212000},
                                          {"Pseudowire PDA Reorder Control",                                 0x244000},
                                          {"Pseudowire PDA Jitter Buffer Control",                           0x243000},
                                          {"Pseudowire PDA TDM Mode Control",                                0x245000},
                                          {"PDA DDR CRC error", /* 66 */                                     0x0},
                                          {"Pseudowire Transmit Enable Control",                             0x321000},
                                          {sPseudowire_Transmit_Ethernet_Header_Value_Control,               0x308000},
                                          {"Pseudowire Transmit Ethernet Header Length Control",             0x301000},
                                          {"Pseudowire Transmit Header RTP SSRC Value Control",              0x303000},
                                          {"Pseudowire Transmit HSPW Label Control",                         0x305000},
                                          {"Pseudowire Transmit UPSR and HSPW mode Control",                 0x306000},
                                          {"Pseudowire Transmit UPSR Group Enable Control",                  0x307000},
                                          {"Pseudowire Transmit HSPW Group Protection Control",              0x304000},
                                          {"Classify HBCE Hashing Table Control PageID 0",                   0x44A000},
                                          {"Classify HBCE Hashing Table Control PageID 1",                   0x44B000},
                                          {"Classify HBCE Looking Up Information Control",                   0x44E000},
                                          {"Classify Per Pseudowire Type Control",                           0x44C000},
                                          {"Classify Pseudowire MEF over MPLS Control",                      0x44D000},
                                          {"Classify Per Group Enable Control",                              0x449000},
                                          {"CDR Pseudowire Look Up Control",                                 0x443000},
                                          {sSoftwave_Control,                                                cInvalidUint32},
                                          {"Rx DDR ECC", /* 83 */                                            cInvalidUint32},
                                          {"Tx DDR ECC",  /* 84 */                                            cInvalidUint32}
                                          };
    AtUnused(self);
    *numRams = mCount(allEntries);
    return allEntries;
    }

static const char ** AllRamsDescription(AtModule self, uint32 *numRams)
    {
    uint32 i, numInitialRams = 0, minNumRam = 0;
    const tThaInternalRamEntry *entries = ThaModuleRamAllInternalRamEntry((ThaModuleRam)self, &numInitialRams);
    static const char * description[cMaxNumInternalRam];

    minNumRam = mMin(cMaxNumInternalRam, numInitialRams);
    for (i = 0; i < minNumRam; i++)
        description[i] = entries[i].name;

    if (numRams)
        *numRams = minNumRam;

    return description;
    }

static eBool IsParityRam(ThaModuleRam self, uint32 ramId)
    {
    uint32 localRamId = ramId;

    AtUnused(self);
    if (localRamId < cTha60210031PdaDdrCrcRamId)
        return cAtTrue;

    if ((localRamId > cTha60210031PdaDdrCrcRamId) && (localRamId < cTha60210031RxDdrEccRamId))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsCrcRam(ThaModuleRam self, uint32 ramId)
    {
    uint32 localRamId = ramId;

    AtUnused(self);
    if (localRamId == cTha60210031PdaDdrCrcRamId)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsEccRam(ThaModuleRam self, uint32 ramId)
    {
    uint32 localRamId = ramId;

    AtUnused(self);
    if (localRamId == cTha60210031RxDdrEccRamId)
        return cAtTrue;

    if (localRamId == cTha60210031TxDdrEccRamId)
        return cAtTrue;

    return cAtFalse;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (IsParityRam((ThaModuleRam)self, localRamId))
        return Tha60210031InternalRamNew(self, ramId, localRamId);

    if (IsCrcRam((ThaModuleRam)self, localRamId))
        return Tha60210031InternalRamCrcNew(self, ramId, localRamId);

    if (IsEccRam((ThaModuleRam)self, localRamId))
        return Tha60210031InternalRamEccNew(self, ramId, localRamId);

    return NULL;
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    uint32 numRams;
    AllRamsDescription(self, &numRams);
    return numRams;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    return AllRamsDescription(self, numRams);
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaModuleRam self, AtInternalRam ram)
    {
    uint32 localRamId = AtInternalRamLocalIdGet(ram);
    if (localRamId == cTha60210031PdaDdrCrcRamId) /* PDA DDR CRC error */
        return Tha60210031RamCrcErrorGeneratorNew((AtModule)self);

    if (localRamId == cTha60210031RxDdrEccRamId)
        return Tha60210031RxDdrEccErrorGeneratorNew((AtModule)self);

    if (localRamId == cTha60210031TxDdrEccRamId)
        return Tha60210031TxDdrEccErrorGeneratorNew((AtModule)self);

    /*Default is not supported */
    return NULL;
    }

static uint32 PhyModuleRamLocalCellAddress(ThaModuleRam self, uint32 moduleId, uint32 localRamId)
    {
    uint32 numInitialRams = 0, minNumRam = 0;
    const tThaInternalRamEntry *entries = ThaModuleRamAllInternalRamEntry(self, &numInitialRams);

    minNumRam = mMin(cMaxNumInternalRam, numInitialRams);
    AtUnused(moduleId);
    if (localRamId >= minNumRam)
        return cInvalidUint32;

    return entries[localRamId].address;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *name = AtInternalRamName(ram);
    AtUnused(self);

    if (AtStrstr(name, sPDH_VT_Async_Map_Control) != NULL)
        return cAtTrue;

    if (AtStrcmp(name, sPseudowire_Transmit_Ethernet_Header_Value_Control) == 0)
        return cAtTrue;

    if (AtStrcmp(name, sSoftwave_Control) == 0)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, DDRUserClockValueStatusRegister);
        mMethodOverride(m_ThaModuleRamOverride, BurstTestIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, DdrCalibStatusMask);
        mMethodOverride(m_ThaModuleRamOverride, ClockDdrSwingRangeInPpm);
        mMethodOverride(m_ThaModuleRamOverride, ClockQdrSwingRangeInPpm);
        mMethodOverride(m_ThaModuleRamOverride, InternalRamMonitoringIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, InterruptStatusGet);
        mMethodOverride(m_ThaModuleRamOverride, PhyModuleHasRamInterrupt);
        mMethodOverride(m_ThaModuleRamOverride, ErrorGeneratorObjectCreate);
        mMethodOverride(m_ThaModuleRamOverride, CrcCounterIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, EccCounterIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, PhyModuleRamLocalCellAddress);
        mMethodOverride(m_ThaModuleRamOverride, AllInternalRamEntry);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, DdrUserClock);
        mMethodOverride(m_AtModuleRamOverride, DdrTypeString);
        mMethodOverride(m_AtModuleRamOverride, DdrAddressBusSizeGet);
        mMethodOverride(m_AtModuleRamOverride, DdrDataBusSizeGet);
        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        mMethodOverride(m_AtModuleRamOverride, NumQdrGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule ramModule = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(ramModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(ramModule, &m_AtModuleOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule(self);
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleRam);
    }

AtModuleRam Tha60210031ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60210031ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleRamObjectInit(newModule, device);
    }

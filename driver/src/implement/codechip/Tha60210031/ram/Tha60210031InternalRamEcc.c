/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031InternalRamEcc.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : Internal RAM ECC implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210031InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaInternalRamEccMethods m_ThaInternalRamEccOverride;
static tThaInternalRamMethods    m_ThaInternalRamOverride;
static tAtInternalRamMethods  m_AtInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtIpCore CoreIpGet(AtModule self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet(self), AtModuleDefaultCoreGet(self));
    }

static uint32 RamIdToDwordIndex(ThaInternalRam self)
    {
    return AtInternalRamLocalIdGet((AtInternalRam)self) >> 5;
    }

static eAtRet ErrorMonitorEnable(ThaInternalRam self, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRamEcc eccRam = (ThaInternalRamEcc)self;
    uint32 regAddr = mMethodsGet(self)->DisableCheckRegister(self);
    uint32 regMask = mMethodsGet(eccRam)->CorrectableMask(eccRam) | mMethodsGet(eccRam)->UncorrectableMask(eccRam);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    longReg[dwordIndex] = (enable) ? longReg[dwordIndex] & ~regMask : longReg[dwordIndex] | regMask;
    mModuleHwLongWrite(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));

    return cAtOk;
    }

static eBool ErrorMonitorIsEnabled(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRamEcc eccRam = (ThaInternalRamEcc)self;
    uint32 regAddr = mMethodsGet(self)->DisableCheckRegister(self);
    uint32 regMask = mMethodsGet(eccRam)->CorrectableMask(eccRam) | mMethodsGet(eccRam)->UncorrectableMask(eccRam);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    return (regMask & longReg[dwordIndex]) ? cAtFalse : cAtTrue;
    }

static uint32 DisableCheckRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x131;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x130;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    return 0x112 + RamIdToDwordIndex(self);
    }

static uint32 IsRxDdrEccRam(ThaInternalRamEcc self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) == cTha60210031RxDdrEccRamId)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 IsTxDdrEccRam(ThaInternalRamEcc self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) == cTha60210031TxDdrEccRamId)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 CorrectableMask(ThaInternalRamEcc self)
    {
    if (IsRxDdrEccRam(self))
        return cBit19;

    if (IsTxDdrEccRam(self))
        return cBit21;

    return 0;
    }

static uint32 UncorrectableMask(ThaInternalRamEcc self)
    {
    if (IsRxDdrEccRam(self))
        return cBit20;

    if (IsTxDdrEccRam(self))
        return cBit22;

    return 0;
    }

static eAtRet HwForcedErrorSet(ThaInternalRam self, uint32 error, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRamEcc eccRam = (ThaInternalRamEcc)self;
    uint32 regAddr = mMethodsGet(self)->ErrorForceRegister(self);
    uint32 regMask = 0;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;

    if (error & cAtRamAlarmEccCorrectable)
        regMask |= mMethodsGet(eccRam)->CorrectableMask(eccRam);

    if (error & cAtRamAlarmEccUnnorrectable)
        regMask |= mMethodsGet(eccRam)->UncorrectableMask(eccRam);

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);
    longReg[dwordIndex] = (enable) ? longReg[dwordIndex] | regMask : longReg[dwordIndex] & ~regMask;
    mModuleHwLongWrite(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));

    return cAtOk;
    }

static uint32 HwForcedErrorGet(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    ThaInternalRamEcc eccRam = (ThaInternalRamEcc)self;
    uint32 regAddr = mMethodsGet(self)->ErrorForceRegister(self);
    uint32 errors = 0;
    uint32 longReg[cThaLongRegMaxSize];
    uint32 dwordIndex;

    mModuleHwLongRead(module, regAddr, longReg, cThaLongRegMaxSize, CoreIpGet(module));
    dwordIndex = RamIdToDwordIndex(self);

    if (longReg[dwordIndex] & mMethodsGet(eccRam)->CorrectableMask(eccRam))
        errors |= cAtRamAlarmEccCorrectable;

    if (longReg[dwordIndex] & mMethodsGet(eccRam)->UncorrectableMask(eccRam))
        errors |= cAtRamAlarmEccUnnorrectable;

    return errors;
    }

static uint32 CounterTypeRegister(ThaInternalRamEcc self,
                                  uint16 counterType,
                                  eBool r2c)
    {
    switch (counterType)
        {
        case cAtRamCounterTypeEccCorrectableErrors:
            {
            if (IsRxDdrEccRam(self))
                return (r2c) ? 0x149 : 0x14a;
            if (IsTxDdrEccRam(self))
                return (r2c) ? 0x151 : 0x152;
            }
            break;
        case cAtRamCounterTypeEccUncorrectableErrors:
            {
            if (IsRxDdrEccRam(self))
                return (r2c) ? 0x14b : 0x14c;
            if (IsTxDdrEccRam(self))
                return (r2c) ? 0x153 : 0x154;
            }
            break;
        default:
            return cBit31_0;
        }

    /* Invalid register address! */
    return cBit31_0;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    if (!ThaModuleRamEccCounterIsSupported((ThaModuleRam) module))
        return cAtFalse;

    switch (counterType)
        {
        case cAtRamCounterTypeEccCorrectableErrors: return cAtTrue;
        case cAtRamCounterTypeEccUncorrectableErrors: return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 HwCounterGet(ThaInternalRamEcc self, uint16 counterType, eBool r2c)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = CounterTypeRegister(self, counterType, r2c);
    return mModuleHwRead(module, regAddr);
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    if (!AtInternalRamCounterTypeIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet((ThaInternalRamEcc)self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    if (!AtInternalRamCounterTypeIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet((ThaInternalRamEcc)self, counterType, cAtTrue);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031InternalRamEcc);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorEnable);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorIsEnabled);
        mMethodOverride(m_ThaInternalRamOverride, DisableCheckRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorStickyRegister);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorSet);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorGet);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideThaInternalRamEcc(AtInternalRam self)
    {
    ThaInternalRamEcc ram = (ThaInternalRamEcc)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamEccOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamEccOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamEccOverride, CorrectableMask);
        mMethodOverride(m_ThaInternalRamEccOverride, UncorrectableMask);
        }

    mMethodsSet(ram, &m_ThaInternalRamEccOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CounterGet);
        mMethodOverride(m_AtInternalRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }


static void Override(AtInternalRam self)
    {
    OverrideThaInternalRamEcc(self);
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

AtInternalRam Tha60210031InternalRamEccObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamEccObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210031InternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60210031InternalRamEccObjectInit(newRam, phyModule, ramId, localId);
    }

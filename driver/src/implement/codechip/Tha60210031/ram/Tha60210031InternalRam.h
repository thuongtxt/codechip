/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210031InternalRam.h
 * 
 * Created Date: Dec 7, 2015
 *
 * Description : Internal RAM interface for product 60210031.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031INTERNALRAM_H_
#define _THA60210031INTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cTha60210031PdaDdrCrcRamId 66
#define cTha60210031RxDdrEccRamId  83
#define cTha60210031TxDdrEccRamId  84

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031InternalRam
    {
    tThaInternalRam super;
    }tTha60210031InternalRam;

typedef struct tTha60210031InternalRamCrc
    {
    tTha60210031InternalRam super;
    }tTha60210031InternalRamCrc;

typedef struct tTha60210031InternalRamEcc
    {
    tThaInternalRamEcc super;
    }tTha60210031InternalRamEcc;

/*--------------------------- Forward declarations ---------------------------*/
AtInternalRam Tha60210031InternalRamObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);

AtInternalRam Tha60210031InternalRamNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210031InternalRamCrcNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210031InternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210031InternalRamEccObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031INTERNALRAM_H_ */


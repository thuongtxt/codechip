/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210031Ddr.c
 *
 * Created Date: Jul 13, 2015
 *
 * Description : DDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha6021ModuleRam.h"
#include "../../Tha60210011/ram/Tha6021DdrInternal.h"
#include "../../Tha60210031/man/Tha60210031DeviceReg.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031Ddr
    {
    tTha6021Ddr super;
    }tTha60210031Ddr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods  m_AtRamOverride;
static tThaDdrMethods m_ThaDdrOverride;

/* Save super implementation */
static const tAtRamMethods *m_AtRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaDdr self)
    {
    uint8 ramId = AtRamIdGet((AtRam)self);
    return 0xf50000UL + (ramId * 0x10000UL);
    }

static uint32 DefaultOffset(ThaDdr self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 DdrCalibStatusMask(AtRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)AtRamModuleGet(self);
    return ThaModuleRamDdrCalibStatusMask(ramModule, AtRamIdGet(self));
    }

static eBool TestbenchEnabled(AtRam self)
    {
    AtDevice device =  AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    return AtDeviceTestbenchIsEnabled(device);
    }

static eAtModuleRamRet HelperInitStatusGet(AtRam self, eBool needGetOnTop)
    {
    AtDevice device;
    uint32 mask;
    eBool isGood;
    static const uint32 cTimeoutMs = 5000;

    if (!needGetOnTop)
        return m_AtRamMethods->InitStatusGet(self);

    device = AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    mask = DdrCalibStatusMask(self);
    isGood = ThaDeviceStickyIsGood(device, cDeviceStickyRegisters, mask, cTimeoutMs);

    if (TestbenchEnabled(self))
        return cAtOk;

    return isGood ? cAtOk : cAtErrorDdrCalibFail;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    /* Currently, hardware recommends to get on core */
    return HelperInitStatusGet(self, cAtFalse);
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, m_AtRamMethods, sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, InitStatusGet);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void OverrideThaDdr(AtRam self)
    {
    ThaDdr ddr = (ThaDdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDdrOverride, mMethodsGet(self), sizeof(m_ThaDdrOverride));

        mMethodOverride(m_ThaDdrOverride, BaseAddress);
        mMethodOverride(m_ThaDdrOverride, DefaultOffset);
        }

    mMethodsSet(ddr, &m_ThaDdrOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    OverrideThaDdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031Ddr);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam Tha60210031DdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newRam, ramModule, core, ramId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210031EthPortSerdesControllerInternal.h
 * 
 * Created Date: Jul 13, 2015
 *
 * Description : SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHPORTQSGMIISERDESCONTROLLERINTERNAL_H_
#define _THA60210031ETHPORTQSGMIISERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/physical/ThaEthSerdesManager.h"
#include "../../Tha60210011/physical/Tha6021EthPortXfiSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021EthPortQsgmiiSerdesController *Tha6021EthPortQsgmiiSerdesController;
typedef struct tTha6021EthPortQsgmiiSerdesLane *Tha6021EthPortQsgmiiSerdesLane;

typedef struct tTha6021EthPortQsgmiiSerdesControllerMethods
    {
    uint32 (*QsgmiiSerdesLoopbackRegister)(Tha6021EthPortQsgmiiSerdesController controller);
    eBool (*SwHandleQsgmiiLanes)(Tha6021EthPortQsgmiiSerdesController controller);
    void  (*SgmiiLanesCreate)(Tha6021EthPortQsgmiiSerdesController self);
    }tTha6021EthPortQsgmiiSerdesControllerMethods;

typedef struct tTha6021EthPortQsgmiiSerdesController
    {
    tTha6021EthPortSerdesController super;
    tTha6021EthPortQsgmiiSerdesControllerMethods *methods;

    AtPrbsEngine sgmiiPrbsEngine;
    AtList sgmiiLanes;
    }tTha6021EthPortQsgmiiSerdesController;

typedef struct tTha6021EthPortQsgmiiSerdesLane
    {
    tTha6021EthPortQsgmiiSerdesController super;
    AtSerdesController parent;
    }tTha6021EthPortQsgmiiSerdesLane;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6021EthPortQsgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);
AtSerdesController Tha6021EthPortQsgmiiSerdesLaneNew(AtSerdesController qsgmiiSerdes, uint32 laneIdx);
eBool Tha6021EthPortQsgmiiSwHandleQsgmiiLanes(Tha6021EthPortQsgmiiSerdesController self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHPORTQSGMIISERDESCONTROLLERINTERNAL_H_ */


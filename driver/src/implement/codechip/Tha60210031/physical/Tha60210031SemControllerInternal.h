/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210031SemControllerInternal.h
 * 
 * Created Date: May 3, 2017
 *
 * Description : SEM internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031SEMCONTROLLERINTERNAL_H_
#define _THA60210031SEMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/physical/ThaSemControllerV3Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031SemController
    {
    tThaSemControllerV3 super;
    }tTha60210031SemController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60210031SemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031SEMCONTROLLERINTERNAL_H_ */


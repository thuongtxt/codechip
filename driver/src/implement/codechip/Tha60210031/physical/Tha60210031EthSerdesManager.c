/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210031EthSerdesManager.c
 *
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "Tha60210031EthSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210031EthSerdesManager *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031EthSerdesManagerMethods m_methods;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tThaEthSerdesManagerMethods m_ThaEthSerdesManagerOverride;

/* Save super implementation */
static const tAtObjectMethods            *m_AtObjectMethods = NULL;
static const tThaEthSerdesManagerMethods *m_ThaEthSerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController SerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210031EthPortXfiSerdesControllerNew(port, serdesId);
    }

static AtSerdesController SgmiiSerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210031EthPortQsgmiiSerdesControllerNew(port, serdesId);
    }

static eAtRet Setup(ThaEthSerdesManager self)
    {
    eAtRet ret = m_ThaEthSerdesManagerMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (mThis(self)->sgmiiSerdesControllers == NULL)
        mThis(self)->sgmiiSerdesControllers = ThaEthSerdesManagerSerdesControllersCreate(self, mMethodsGet(mThis(self))->SgmiiSerdesControllerCreate);
    if (mThis(self)->sgmiiSerdesControllers == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void DeleteSgmiiControllers(AtObject self)
    {
    uint32 controller_i;
    uint32 numControllers = ThaEthSerdesManagerNumSerdesController((ThaEthSerdesManager)self);

    for (controller_i = 0; controller_i < numControllers; controller_i++)
        AtObjectDelete((AtObject)mThis(self)->sgmiiSerdesControllers[controller_i]);

    AtOsalMemFree(mThis(self)->sgmiiSerdesControllers);
    mThis(self)->sgmiiSerdesControllers = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteSgmiiControllers(self);
    m_AtObjectMethods->Delete(self);
    }

static eBool SerdesIdIsValid(ThaEthSerdesManager self, uint32 serdesId)
    {
    if (serdesId < ThaEthSerdesManagerNumSerdesController(self))
        return cAtTrue;
    return cAtFalse;
    }

static AtEthPort EthPortForSerdes(ThaEthSerdesManager self, uint32 serdesId)
    {
    uint32 numEthPorts = AtModuleEthMaxPortsGet(self->ethModule);
    uint8 portId = (uint8)(serdesId % numEthPorts);
    return AtModuleEthPortGet(self->ethModule, portId);
    }

static AtSerdesController XfiSerdesController(ThaEthSerdesManager self, uint32 serdesId)
    {
    return m_ThaEthSerdesManagerMethods->SerdesController(self, serdesId);
    }

static AtSerdesController QsgmiiSerdesController(ThaEthSerdesManager self, uint32 serdesId)
    {
    if (SerdesIdIsValid(self, serdesId))
        return mThis(self)->sgmiiSerdesControllers[serdesId];
    return NULL;
    }

static AtSerdesController SerdesController(ThaEthSerdesManager self, uint32 serdesId)
    {
    AtEthPort port = EthPortForSerdes(self, serdesId);
    eAtEthPortInterface interface = AtEthPortInterfaceGet(port);

    if (interface == cAtEthPortInterfaceXGMii)
        return XfiSerdesController(self, serdesId);

    if ((interface == cAtEthPortInterfaceSgmii) ||
        (interface == cAtEthPortInterfaceQSgmii))
        return QsgmiiSerdesController(self, serdesId);

    return NULL;
    }

static uint32 NumSerdesController(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return 4;
    }

static eBool ShouldEnableSerdes(ThaEthSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    return (serdesId == 0) ? cAtTrue : cAtFalse;
    }

static void PhysicalParameterDefault(AtSerdesController self, eAtSerdesParam param, uint32 resetValue)
    {
    if (AtSerdesControllerPhysicalParamIsSupported(self, param))
        AtSerdesControllerPhysicalParamSet(self, param, resetValue);
    }

static eBool QsgmiiSerdesTuningIsSupported(ThaEthSerdesManager self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)(self->ethModule));
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersion = mMethodsGet(mThis(self))->StartVersionSupportQsgmiiTuning(self);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startVersion)
        return cAtTrue;

    return cAtFalse;
    }

static void QsgmiiPhysicalParamsDefault(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    if (!QsgmiiSerdesTuningIsSupported(self))
        return;

    PhysicalParameterDefault(serdes, cAtSerdesParamTxPreCursor,  0x0);
    PhysicalParameterDefault(serdes, cAtSerdesParamTxPostCursor, 0x14);
    PhysicalParameterDefault(serdes, cAtSerdesParamTxDiffCtrl,   0xF);
    }

static eAtRet QSgmiiInit(ThaEthSerdesManager self)
    {
    uint32 numSerdes = ThaEthSerdesManagerNumSerdesController(self);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = mThis(self)->sgmiiSerdesControllers[serdes_i];
        eBool enabled = mMethodsGet(self)->ShouldEnableSerdes(self, serdes_i);
        if (AtSerdesControllerCanEnable(serdes, enabled))
            ret |= AtSerdesControllerEnable(serdes, enabled);

        if (ret == cAtOk)
            QsgmiiPhysicalParamsDefault(self, serdes); /* Need to reset all SERDESs */
        }

    return ret;
    }

static eAtRet Init(ThaEthSerdesManager self)
    {
    eAtRet ret = m_ThaEthSerdesManagerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return QSgmiiInit(self);
    }

static ThaVersionReader VersionReader(ThaEthSerdesManager self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self->ethModule);
    return ThaDeviceVersionReader(device);
    }

static uint32 StartDateSupportXfiEqualizerLpmMode(ThaEthSerdesManager self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x01, 0x36);
    }

static uint32 StartBuiltNumberSupportXfiEqualizerControl(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x3, 0x6, 0x17);
    }

static eBool CanControlXfiEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(serdes));
    uint32 startSuportDate = StartDateSupportXfiEqualizerLpmMode(self);

    if (AtDeviceVersionNumber(device) > startSuportDate)
        return cAtTrue;
        
    if (AtDeviceVersionNumber(device) == startSuportDate)
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportXfiEqualizerControl(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 StartDateSupportQsgmiiEqualizerLpmMode(ThaEthSerdesManager self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x01, 0x36);
    }

static uint32 StartBuiltNumberSupportQsgmiiEqualizerControl(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x3, 0x6, 0x17);
    }

static eBool CanControlQsgmiiEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(serdes)) ;
    uint32 startSupportDate = StartDateSupportQsgmiiEqualizerLpmMode(self);

    if (AtDeviceVersionNumber(device) > startSupportDate)
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == startSupportDate)
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportQsgmiiEqualizerControl(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool CanControlEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(serdes);

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceXGMii)
        return CanControlXfiEqualizer(self, serdes);

    return CanControlQsgmiiEqualizer(self, serdes);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210031EthSerdesManager object = (Tha60210031EthSerdesManager)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(sgmiiSerdesControllers, ThaEthSerdesManagerNumSerdesController((ThaEthSerdesManager)self));
    }

static uint32 StartVersionSupportQsgmiiTuning(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4608);
    }

static void MethodsInit(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SgmiiSerdesControllerCreate);
        mMethodOverride(m_methods, StartVersionSupportQsgmiiTuning);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtObject(ThaEthSerdesManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaEthSerdesManager(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthSerdesManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthSerdesManagerOverride, mMethodsGet(self), sizeof(m_ThaEthSerdesManagerOverride));

        mMethodOverride(m_ThaEthSerdesManagerOverride, Setup);
        mMethodOverride(m_ThaEthSerdesManagerOverride, Init);
        mMethodOverride(m_ThaEthSerdesManagerOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaEthSerdesManagerOverride, SerdesController);
        mMethodOverride(m_ThaEthSerdesManagerOverride, NumSerdesController);
        mMethodOverride(m_ThaEthSerdesManagerOverride, ShouldEnableSerdes);
        mMethodOverride(m_ThaEthSerdesManagerOverride, CanControlEqualizer);
        }

    mMethodsSet(self, &m_ThaEthSerdesManagerOverride);
    }

static void Override(ThaEthSerdesManager self)
    {
    OverrideAtObject(self);
    OverrideThaEthSerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031EthSerdesManager);
    }

ThaEthSerdesManager Tha60210031EthSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthSerdesManagerObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthSerdesManager Tha60210031EthSerdesManagerNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthSerdesManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return Tha60210031EthSerdesManagerObjectInit(newManager, ethModule);
    }

AtSerdesController Tha60210031EthSerdesManagerSgmiiSerdesControllerGet(ThaEthSerdesManager self, uint32 serdesId)
    {
    if (SerdesIdIsValid(self, serdesId))
        return mThis(self)->sgmiiSerdesControllers[serdesId];

    return NULL;
    }

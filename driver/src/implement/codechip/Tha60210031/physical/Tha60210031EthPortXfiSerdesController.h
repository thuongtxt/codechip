/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210031EthPortXfiSerdesController.h
 * 
 * Created Date: Aug 17, 2016
 *
 * Description : Serdes controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHPORTXFISERDESCONTROLLER_H_
#define _THA60210031ETHPORTXFISERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/physical/Tha6021EthPortXfiSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031EthPortXfiSerdesController
    {
    tTha6021EthPortXfiSerdesController super;
    }tTha60210031EthPortXfiSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60210031EthPortXfiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHPORTXFISERDESCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60210031SemController.c
 *
 * Created Date: Dec 10, 2015
 *
 * Description : Tha60210031SemController implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210031SemControllerInternal.h"
#include "../man/Tha60210031Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSemControllerMethods m_ThaSemControllerOverride;
static tAtSemControllerMethods  m_AtSemControllerOverride;

static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(ThaSemController self)
    {
    AtUnused(self);
    return 0xFE2100;
    }

static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return "XC7K410T";
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x00007528;
    }

static void OverrideThaSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSemControllerOverride, mMethodsGet((ThaSemController)self), sizeof(m_ThaSemControllerOverride));

        mMethodOverride(m_ThaSemControllerOverride, DefaultOffset);
        }

    mMethodsSet((ThaSemController)self, &m_ThaSemControllerOverride);
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, DeviceDescription);
        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideThaSemController(self);
    OverrideAtSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031SemController);
    }

AtSemController Tha60210031SemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSemControllerV3ObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60210031SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031SemControllerObjectInit(controller, device, semId);
    }


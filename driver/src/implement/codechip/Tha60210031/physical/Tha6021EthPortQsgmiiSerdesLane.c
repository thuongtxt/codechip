/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6021EthPortQsgmiiSerdesLane.c
 *
 * Created Date: Sep 4, 2016
 *
 * Description : QSGMII sub lanes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60210011/eth/Tha60210011ModuleEthInternal.h"
#include "../../Tha60210011/eth/Tha6021ModuleEthReg.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "../man/Tha60210031DeviceReg.h"
#include "../man/Tha60210031Device.h"
#include "Tha6021EthPortQsgmiiSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6021EthPortQsgmiiSerdesLane)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumLanes(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static AtModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (AtModuleEth)AtChannelModuleGet(channel);
    }

static uint32 LaneEnableMask(uint32 serdesId, uint32 laneId)
    {
    if ((serdesId % 3) == 0)
        return (cBit0 << (((serdesId) * 4) + (laneId)));

    return (cBit0 << (((uint32)(3 - serdesId) * 4) + (laneId)));
    }

static uint8 LaneEnableShift(uint32 serdesId, uint32 laneId)
    {
    if ((serdesId % 3) == 0)
        return (uint8)(((serdesId) * 4) + (laneId));

    return (uint8)(((uint32)(3 - serdesId) * 4) + (laneId));
    }

static eAtRet TxLaneEnable(AtSerdesController self, eBool enable)
    {
    uint32 lane_i = AtSerdesControllerIdGet(self);
    uint32 regAddr = cEthPortSerdesTxEnable;
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 parentSerdesId = AtSerdesControllerIdGet(mThis(self)->parent);

    mFieldIns(&regVal,
              LaneEnableMask(parentSerdesId, lane_i),
              LaneEnableShift(parentSerdesId, lane_i),
              enable ? 1 : 0);

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool TxLaneIsEnabled(AtSerdesController self)
    {
    uint32 regVal   = AtSerdesControllerRead(self, cEthPortSerdesTxEnable, cAtModuleEth);
    uint32 parrentSerdesId = AtSerdesControllerIdGet(mThis(self)->parent);
    uint32 lane_i = AtSerdesControllerIdGet(self);

    if ((regVal & LaneEnableMask(parrentSerdesId, lane_i)) == 0)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    return TxLaneEnable(self, enable);
    }

static eBool IsEnabled(AtSerdesController self)
    {
    return TxLaneIsEnabled(self);
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(loopbackMode);
    AtUnused(enable);
    return cAtFalse;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    return AtSerdesControllerPllIsLocked(mThis(self)->parent);
    }

static uint32 LaneLinkUpMask(AtSerdesController self)
    {
    uint16 laneId = (uint16)AtSerdesControllerIdGet(self);
    uint8 parentId = (uint8)AtSerdesControllerIdGet(mThis(self)->parent);
    uint32 numLanes = ThaModuleEthQsgmiiSerdesNumLanes((ThaModuleEth)EthModule(self), parentId);
    uint32 flatId = (parentId * numLanes) + laneId;
    return (cBit12 << flatId);
    }

static eBool LaneLinkUp(AtSerdesController self)
    {
    uint32 statusValue = AtSerdesControllerRead(self, cDeviceStatusRegisters, cAtModuleEth);
    return (statusValue & LaneLinkUpMask(self)) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    if (LaneLinkUp(self))
        return cAtSerdesAlarmTypeNone;

    return cAtSerdesAlarmTypeLinkDown;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    if (LaneLinkUp(self))
        return cAtSerdesLinkStatusUp;
    return cAtSerdesLinkStatusDown;
    }

static void Debug(AtSerdesController self)
    {
    AtUnused(self);
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet RxAlarmForce(AtSerdesController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnforce(AtSerdesController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static uint32 RxForcedAlarmGet(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxForcableAlarmsGet(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(loopbackMode);
    AtUnused(enable);

    return cAtErrorModeNotSupport;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return AtSerdesControllerLoopbackIsEnabled(mThis(self)->parent, loopbackMode);
    }

static uint32 SupportedInterruptMask(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(alarmMask);
    AtUnused(enableMask);
    return cAtErrorNotImplemented;
    }

static uint32 InterruptMaskGet(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6021EthPortQsgmiiSerdesLane object = (Tha6021EthPortQsgmiiSerdesLane)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(parent);
    }

static AtSerdesController ParentGet(AtSerdesController self)
    {
    return mThis(self)->parent;
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtSerdesControllerOverride, SupportedInterruptMask);
        mMethodOverride(m_AtSerdesControllerOverride, RxAlarmForce);
        mMethodOverride(m_AtSerdesControllerOverride, RxAlarmUnforce);
        mMethodOverride(m_AtSerdesControllerOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, RxForcableAlarmsGet);
        mMethodOverride(m_AtSerdesControllerOverride, NumLanes);
        mMethodOverride(m_AtSerdesControllerOverride, ParentGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021EthPortQsgmiiSerdesLane);
    }

static AtSerdesController ObjectInit(AtSerdesController parent, AtSerdesController self, uint32 laneId)
    {
    /* Clear memory */
    AtEthPort ethPort = (AtEthPort)AtSerdesControllerPhysicalPortGet(parent);
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortQsgmiiSerdesControllerObjectInit(self, ethPort, laneId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->parent = parent;

    return self;
    }

AtSerdesController Tha6021EthPortQsgmiiSerdesLaneNew(AtSerdesController qsgmiiSerdes, uint32 laneId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(qsgmiiSerdes, newController, laneId);
    }

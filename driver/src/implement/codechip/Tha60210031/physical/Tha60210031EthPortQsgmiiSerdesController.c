/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210031EthPortQsgmiiSerdesController.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : SERDES controller for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60210011/eth/Tha6021ModuleEthReg.h"
#include "../man/Tha60210031Device.h"
#include "Tha6021EthPortQsgmiiSerdesControllerInternal.h"
#include "Tha60210031EthSerdesManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031EthPortQsgmiiSerdesController
    {
    tTha6021EthPortQsgmiiSerdesController super;
    }tTha60210031EthPortQsgmiiSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods                   m_AtSerdesControllerOverride;
static tTha6021EthPortQsgmiiSerdesControllerMethods m_Tha6021EthPortQsgmiiSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods                   *m_AtSerdesControllerMethods                   = NULL;
static const tTha6021EthPortQsgmiiSerdesControllerMethods *m_Tha6021EthPortQsgmiiSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (ThaModuleEth)AtChannelModuleGet(channel);
    }

static eBool SwHandleQsgmiiLanes(Tha6021EthPortQsgmiiSerdesController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)EthModule((AtSerdesController)self));

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(device))
        return cAtTrue;

    return cAtFalse;
    }

static AtSerdesController PartnerControllerGet(AtSerdesController self, uint32 serdesId)
    {
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager(EthModule(self));
    return Tha60210031EthSerdesManagerSgmiiSerdesControllerGet(serdesManager, serdesId + 2);
    }

static uint32 LaneEnableMask(uint32 serdesId, uint32 laneId)
    {
    if ((serdesId % 3) == 0)
        return (cBit0 << (((serdesId) * 4) + (laneId)));

    return (cBit0 << (((uint32)(3 - serdesId) * 4) + (laneId)));
    }

static uint8 LaneEnableShift(uint32 serdesId, uint32 laneId)
    {
    if ((serdesId % 3) == 0)
        return (uint8)(((serdesId) * 4) + (laneId));

    return (uint8)(((uint32)(3 - serdesId) * 4) + (laneId));
    }

static eAtRet TxAllLanesEnable(AtSerdesController self, eBool enable)
    {
    uint32 numLanes = AtSerdesControllerNumLanes(self);
    uint32 lane_i;
    uint32 regAddr = cEthPortSerdesTxEnable;
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        mFieldIns(&regVal,
                  LaneEnableMask(serdesId, lane_i),
                  LaneEnableShift(serdesId, lane_i),
                  enable ? 1 : 0);
        }

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    uint32 serdesId;
    eAtRet ret;

    if (!Tha6021EthPortQsgmiiSwHandleQsgmiiLanes((Tha6021EthPortQsgmiiSerdesController)self))
        return m_AtSerdesControllerMethods->Enable(self, enable);

    ret = TxAllLanesEnable(self, enable);
    serdesId = AtSerdesControllerIdGet(self);

    /* SERDES come in couple 0,2 and 1,3, one SERDES is enabled, it's partner need to be enabled too */
    if ((serdesId == 0) || (serdesId == 1))
        ret |= AtSerdesControllerEnable(PartnerControllerGet(self, serdesId), enable);

    return ret;
    }

static eBool TxAllLanesEnabled(AtSerdesController self)
    {
    uint32 numLanes = AtSerdesControllerNumLanes(self);
    uint32 regVal   = AtSerdesControllerRead(self, cEthPortSerdesTxEnable, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint16 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        if ((regVal & LaneEnableMask(serdesId, lane_i)) == 0)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    if (!Tha6021EthPortQsgmiiSwHandleQsgmiiLanes((Tha6021EthPortQsgmiiSerdesController)self))
        return m_AtSerdesControllerMethods->IsEnabled(self);

    return TxAllLanesEnabled(self);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortQsgmiiSerdesController(Tha6021EthPortQsgmiiSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6021EthPortQsgmiiSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortQsgmiiSerdesControllerOverride, mMethodsGet(self), sizeof(m_Tha6021EthPortQsgmiiSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortQsgmiiSerdesControllerOverride, SwHandleQsgmiiLanes);
        }

    mMethodsSet(self, &m_Tha6021EthPortQsgmiiSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortQsgmiiSerdesController((Tha6021EthPortQsgmiiSerdesController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031EthPortQsgmiiSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortQsgmiiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210031EthPortQsgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

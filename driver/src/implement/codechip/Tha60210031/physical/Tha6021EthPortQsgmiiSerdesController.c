/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha6021EthPortQsgmiiSerdesController.c
 *
 * Created Date: Jun 26, 2015
 *
 * Description : SERDES controller for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60210011/eth/Tha60210011ModuleEthInternal.h"
#include "../../Tha60210011/eth/Tha6021ModuleEthReg.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "../man/Tha60210031DeviceReg.h"
#include "../man/Tha60210031Device.h"
#include "Tha6021EthPortQsgmiiSerdesControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cLpmEnableRegister          0xFE6043

#define cPcsRxLockedLl    cBit1
#define cPcsRxLocked      cBit0
#define cPcsRxLinkStatus  cBit12
#define cPmaRxLinkStatus  cBit11

#define cPcsErrBlockCountMask     cBit31_24
#define cPcsErrBlockCountShift    24
#define cPcsBerCountMask          cBit21_16
#define cPcsBerCountShift         16
#define cPcsTestPattErrCountMask  cBit15_0
#define cPcsTestPattErrCountShift 0

#define cDiagLaneLinkupMask cBit0

/* New LoopBack Register: Loopback point is between MAC and the QSGMII */
#define cQSgmiiLoopbackRegiter            0xF00042
#define cQSgmiiLocalLoopbackMask(portId)  (cBit0 << (portId*4))
#define cQSgmiiLocalLoopbackShift(portId) (portId*4)

#define cAf6_linkalm_Eport_QSGMII_Mask(port)        (cBit0 << ((port) << 2))
#define cAf6_linkalm_Eport_QSGMII_Shift(port)       ((port) << 2)

/* New loopback register: loopback point is PCS or PMA inside the serdes IP */
#define cQSgmiiSerdesLoopbackMask(portId)  (cBit2_0 << (portId*3))
#define cQSgmiiSerdesLoopbackShift(portId) (portId*3)
#define cQsgmiiSerdesLoopback_Release     0
#define cQsgmiiSerdesLoopback_PCS_NearEnd 1
#define cQsgmiiSerdesLoopback_PMA_NearEnd 2
#define cQsgmiiSerdesLoopback_PMA_FarEnd  4
#define cQsgmiiSerdesLoopback_PCS_FarEnd  6

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6021EthPortQsgmiiSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6021EthPortQsgmiiSerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods                       m_AtObjectOverride;
static tAtSerdesControllerMethods             m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods m_Tha6021EthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    return (AtModuleEth)AtChannelModuleGet(channel);
    }

static eBool LaneIsUsed(AtSerdesController self, uint32 laneId)
    {
    ThaModuleEth ethModule = (ThaModuleEth)EthModule(self);
    return ThaModuleEthQsgmiiSerdesLaneIsUsed(ethModule, (uint8)AtSerdesControllerIdGet(self), (uint16)laneId);
    }

static uint32 NumLanes(AtSerdesController self)
    {
    return ThaModuleEthQsgmiiSerdesNumLanes((ThaModuleEth)EthModule(self), (uint8)AtSerdesControllerIdGet(self));
    }

static void SgmiiLanesSetup(AtSerdesController self)
    {
    uint32 numLanes = AtSerdesControllerNumLanes(self), i;

    mThis(self)->sgmiiLanes = AtListCreate(numLanes);
    for (i = 0; i < numLanes; i++)
        {
        AtObject alane = (AtObject)Tha6021EthPortQsgmiiSerdesLaneNew(self, i);
        AtListObjectAdd(mThis(self)->sgmiiLanes, alane);
        }
    }

static void SgmiiLanesDelete(AtSerdesController self)
    {
    AtListDeleteWithObjectHandler(mThis(self)->sgmiiLanes, AtObjectDelete);
    mThis(self)->sgmiiLanes = NULL;
    }

static eBool LanesIsControllable(AtSerdesController self)
    {
    eBool lanesIsSupported = mMethodsGet(mThis(self))->SwHandleQsgmiiLanes(mThis(self));
    uint32 numLanes = AtSerdesControllerNumLanes(self);

    if (lanesIsSupported && (numLanes > 0))
        return cAtTrue;

    return cAtFalse;
    }

static void SgmiiLanesCreate(Tha6021EthPortQsgmiiSerdesController self)
    {
    if (LanesIsControllable((AtSerdesController)self) && (mThis(self)->sgmiiLanes == NULL))
        SgmiiLanesSetup((AtSerdesController)self);
    }

static eAtRet LoopbackRelease(AtSerdesController self)
    {
    eAtRet ret = AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);

    if (ret != cAtOk)
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s %d: Release loopback fail with ret = %s\r\n", AtSourceLocation, AtRet2String(ret));

    return ret;
    }

static eAtRet PrbsEngineInit(AtSerdesController self)
    {
    eAtRet ret;
    AtPrbsEngine engine = AtSerdesControllerPrbsEngine(self);

    if (engine == NULL)
        return cAtOk;

    ret = AtPrbsEngineInit(engine);
    if (ret != cAtOk)
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s %d: PRBS initializing fail with ret = %s\r\n", AtSourceLocation, AtRet2String(ret));

    return ret;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret;
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    ret = LoopbackRelease(self);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(controller)->ShouldInitPrbsEngine(controller))
        return PrbsEngineInit(self);

    return ret;
    }

static eAtRet TxAllLanesEnable(AtSerdesController self, eBool enable)
    {
    uint32 numLanes = AtSerdesControllerNumLanes(self);
    uint32 lane_i;
    uint32 regAddr = cEthPortSerdesTxEnable;
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        uint8 hwEnable = enable ? 1 : 0;

        if (!LaneIsUsed(self, lane_i))
            hwEnable = 0;

        mFieldIns(&regVal,
                  cEthPortSerdesQSgmiiLaneEnableMask(serdesId, lane_i),
                  cEthPortSerdesQSgmiiLaneEnableShift(serdesId, lane_i),
                  hwEnable);
        }

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool TxAllLanesEnabled(AtSerdesController self)
    {
    uint32 numLanes = AtSerdesControllerNumLanes(self);
    uint32 regVal   = AtSerdesControllerRead(self, cEthPortSerdesTxEnable, cAtModuleEth);
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint16 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        if (!LaneIsUsed(self, lane_i))
            continue;

        if ((regVal & cEthPortSerdesQSgmiiLaneEnableMask(serdesId, lane_i)) == 0)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ApsSupported(AtSerdesController self)
    {
    return AtModuleEthSerdesApsSupported(EthModule(self));
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    if (ApsSupported(self))
        return TxAllLanesEnable(self, enable);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    if (ApsSupported(self))
        return TxAllLanesEnabled(self);
    return cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    if (ApsSupported(self))
        return cAtTrue;
    return enable ? cAtTrue : cAtFalse;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60210031EthPortQSgmiiSerdesPrbsEngineNew(self);
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(loopbackMode);
    if (!ApsSupported(self))
        return enable ? cAtFalse : cAtTrue;

    return cAtTrue;
    }

static uint8 SerdesId(AtSerdesController self)
    {
    return (uint8)AtSerdesControllerIdGet(self);
    }

static eAtRet HwSerdesLoopbackSet(AtSerdesController self, uint16 hwLoopbackMode)
    {
    uint32 loopbackRegister = mMethodsGet(mThis(self))->QsgmiiSerdesLoopbackRegister(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, loopbackRegister, cAtModuleEth);
    uint8 serdesId = SerdesId(self);
    mFieldIns(&regVal,
              cQSgmiiSerdesLoopbackMask(serdesId),
              cQSgmiiSerdesLoopbackShift(serdesId),
              hwLoopbackMode);
    AtSerdesControllerWrite(self, loopbackRegister, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint16 HwSerdesLoopbackGet(AtSerdesController self)
    {
    uint32 loopbackRegister = mMethodsGet(mThis(self))->QsgmiiSerdesLoopbackRegister(mThis(self));
    uint32 regVal = AtSerdesControllerRead(self, loopbackRegister, cAtModuleEth);
    uint16 hwLoopbackMode;
    uint8 serdesId = SerdesId(self);

    mFieldGet(regVal,
              cQSgmiiSerdesLoopbackMask(serdesId),
              cQSgmiiSerdesLoopbackShift(serdesId),
              uint16, &hwLoopbackMode);

    return hwLoopbackMode;
    }

static uint16 HwSerdesLoopbackModeToSwLoopbackMode(uint16 hwValue)
    {
    switch (hwValue)
        {
        case cQsgmiiSerdesLoopback_Release:
            return cAtLoopbackModeRelease;
        case cQsgmiiSerdesLoopback_PMA_NearEnd:
            return cAtLoopbackModeLocal;
        case cQsgmiiSerdesLoopback_PMA_FarEnd:
            return cAtLoopbackModeRemote;
        case cQsgmiiSerdesLoopback_PCS_NearEnd:
            return cAtLoopbackModeLocal;
        case cQsgmiiSerdesLoopback_PCS_FarEnd:
            return cAtLoopbackModeRemote;
        default:
            return cQsgmiiSerdesLoopback_Release;
        }
    }

static uint16 SwSerdesLoopbackModeToHwLoopbackMode(uint16 swValue)
    {
    switch (swValue)
        {
        case cAtLoopbackModeRelease:
            return cQsgmiiSerdesLoopback_Release;
        case cAtLoopbackModeLocal:
            return cQsgmiiSerdesLoopback_PMA_NearEnd;
        case cAtLoopbackModeRemote:
            return cQsgmiiSerdesLoopback_PMA_FarEnd;
        default:
            return cQsgmiiSerdesLoopback_Release;
        }
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    uint16 hwLoopbackMode = SwSerdesLoopbackModeToHwLoopbackMode(loopbackMode);
    if (enable)
        return HwSerdesLoopbackSet(self, hwLoopbackMode);
    else
        return HwSerdesLoopbackSet(self, cQsgmiiSerdesLoopback_Release);
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    uint16 hwLoopbackMode = HwSerdesLoopbackGet(self);

    if (loopbackMode == HwSerdesLoopbackModeToSwLoopbackMode(hwLoopbackMode))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PllLockedMask(AtSerdesController self)
    {
    if (SerdesId(self) < AtModuleEthMaxPortsGet(EthModule(self)))
        return cXfiActivePllLockedMask;
    return cXfiStandbyPllLockedMask;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 mask = PllLockedMask(self);
    uint32 regVal = AtSerdesControllerRead(self, cDeviceStatusRegisters, cAtModuleEth);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static void StickyShow(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cDeviceStickyRegisters, cAtModuleEth);
    uint32 pllMask = PllLockedMask(self);
    uint32 changed;
    uint32 clearMask;

    changed = regVal & pllMask;
    AtPrintc(cSevNormal, "* PLL locked: ");
    AtPrintc(changed ? cSevWarning : cSevInfo, "%s\r\n", changed ? "changed" : "no changed");

    changed = regVal & cEthSerdesLinkUpMask(serdesId);
    AtPrintc(cSevNormal, "* Link status: ");
    AtPrintc(changed ? cSevWarning : cSevInfo, "%s\r\n", changed ? "changed" : "no changed");

    clearMask = pllMask | cEthSerdesLinkUpMask(serdesId);
    AtSerdesControllerWrite(self, cDeviceStickyRegisters, clearMask, cAtModuleEth);
    }

static uint32 LaneLinkUpMask(AtSerdesController self, uint32 laneId)
    {
    uint8 serdesId = SerdesId(self);
    uint32 numLanes = AtSerdesControllerNumLanes(self);
    uint32 flatId = (serdesId * numLanes) + laneId;
    return (cBit12 << flatId);
    }

static eBool CoreLaneLinkUp(AtSerdesController self, uint32 laneId)
    {
    uint32 statusValue = AtSerdesControllerRead(self, cDeviceStatusRegisters, cAtModuleEth);
    return (statusValue & LaneLinkUpMask(self, laneId)) ? cAtTrue : cAtFalse;
    }

static eBool CoreLaneLinkChanged(AtSerdesController self, uint32 laneId)
    {
    uint32 regAddr = cDeviceStickyRegisters;
    uint32 statusValue = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 mask = LaneLinkUpMask(self, laneId);
    eBool changed = (statusValue & mask) ? cAtTrue : cAtFalse;
    AtSerdesControllerWrite(self, regAddr, mask, cAtModuleEth);
    return changed;
    }

static eBool LaneLinkUp(AtSerdesController self, uint32 laneId)
    {
    return CoreLaneLinkUp(self, laneId);
    }

static eBool AllLanesUp(AtSerdesController self)
    {
    uint32 lane_i, numLanes = AtSerdesControllerNumLanes(self);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        if (LaneLinkUp(self, lane_i))
            continue;

        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool LaneLinkChanged(AtSerdesController self, uint32 laneId)
    {
    return CoreLaneLinkChanged(self, laneId);
    }

static eBool InterruptIsSupported(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSupportEthSerdesInterrupt(device))
        return cAtTrue;
        
    return cAtFalse;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linksta_Eport, cAtModuleEth);
    uint32 portId = AtSerdesControllerIdGet(self);
    uint32 alarms = 0;

    if (~regVal & cAf6_linkalm_Eport_QSGMII_Mask(portId))
        alarms |= cAtSerdesAlarmTypeLinkDown;

    return alarms;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    if (InterruptIsSupported(self))
        return (AlarmGet(self) & cAtSerdesAlarmTypeLinkDown) ? cAtSerdesLinkStatusDown : cAtSerdesLinkStatusUp;
    else
        return AllLanesUp(self);
    }

static void AllLanesStatusShow(AtSerdesController self)
    {
    uint32 lane_i, numLanes = AtSerdesControllerNumLanes(self);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        uint32 up = LaneLinkUp(self, lane_i);
        uint32 changed = LaneLinkChanged(self, lane_i);

        AtPrintc(cSevNormal, "* Lane %u: ", lane_i + 1);
        AtPrintc(up ? cSevInfo : cSevCritical, "%s", up ? "up" : "down");
        AtPrintc(changed ? cSevCritical : cSevInfo, "(%s)", changed ? "changed" : "no changed");
        AtPrintc(cSevNormal, "\r\n");
        }
    }

static void StatusShow(AtSerdesController self)
    {
    StickyShow(self);
    AllLanesStatusShow(self);
    }

static void Debug(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->Debug(self);
    StatusShow(self);
    }

static void StickyClear(AtSerdesController self)
    {
    uint32 serdesId = AtSerdesControllerIdGet(self);
    uint32 clearMask = PllLockedMask(self) | cEthSerdesLinkUpMask(serdesId);
    AtSerdesControllerRead(self, cDeviceStickyRegisters, cAtModuleEth);
    AtSerdesControllerWrite(self, cDeviceStickyRegisters, clearMask, cAtModuleEth);
    }

static void StatusClear(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->StatusClear(self);
    StickyClear(self);
    }

static uint32 AlarmRead2Clear(AtSerdesController self, eBool read2Clear)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linkalm_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_QSGMII_Mask(AtSerdesControllerIdGet(self));
    uint32 alarms = 0;

    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    if (read2Clear)
        AtSerdesControllerWrite(self, cAf6Reg_linkalm_Eport, portMask, cAtModuleEth);

    return alarms;
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    return AlarmRead2Clear(self, cAtTrue);
    }

static eAtRet InterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_inten_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_QSGMII_Mask(AtSerdesControllerIdGet(self));
    uint32 notSupportedAlarms = (uint32)(~cAtSerdesAlarmTypeLinkDown);

    if (alarmMask & notSupportedAlarms)
        return cAtErrorModeNotSupport;

    if (alarmMask & cAtSerdesAlarmTypeLinkDown)
        regVal = (enableMask & cAtSerdesAlarmTypeLinkDown) ? (regVal | portMask) : (regVal & ~portMask);

    AtSerdesControllerWrite(self, cAf6Reg_inten_Eport, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 InterruptMaskGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_inten_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_QSGMII_Mask(AtSerdesControllerIdGet(self));
    uint32 alarms = 0;

    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    return alarms;
    }

static eAtRet HwRxAlarmForce(AtSerdesController self, uint32 alarmType, eBool enable)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linkfrc_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_QSGMII_Mask(AtSerdesControllerIdGet(self));
    uint32 alarms = 0;

    if (alarmType & cAtSerdesAlarmTypeLinkDown)
        {
        regVal = (enable) ? (regVal | portMask) : (regVal & ~portMask);
        }
    AtSerdesControllerWrite(self, cAf6Reg_linkfrc_Eport, regVal, cAtModuleEth);
    return alarms;
    }

static eAtRet RxAlarmForce(AtSerdesController self, uint32 alarmType)
    {
    return HwRxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet RxAlarmUnforce(AtSerdesController self, uint32 alarmType)
    {
    return HwRxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 RxForcedAlarmGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linkfrc_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_QSGMII_Mask(AtSerdesControllerIdGet(self));
    uint32 alarms = 0;

    if (regVal & portMask)
        alarms |= cAtSerdesAlarmTypeLinkDown;

    return alarms;
    }

static uint32 RxForcableAlarmsGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesAlarmTypeLinkDown;
    }

static uint32 QsgmiiSerdesLoopbackRegister(Tha6021EthPortQsgmiiSerdesController self)
    {
    AtUnused(self);
    return 0xF00044;
    }

static uint32 SupportedInterruptMask(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesAlarmTypeLinkDown;
    }

static uint32 LpmEnableRegister(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cLpmEnableRegister;
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_top_o_control2_txprecursor;
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_top_o_control1_txposcursor;
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_top_o_control0_txdiffctrl;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6021EthPortQsgmiiSerdesController object = (Tha6021EthPortQsgmiiSerdesController)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(sgmiiPrbsEngine);
    mEncodeObject(sgmiiLanes);
    }

static void Delete(AtObject self)
    {
    if (mThis(self)->sgmiiLanes != NULL)
        SgmiiLanesDelete((AtSerdesController)self);
    m_AtObjectMethods->Delete(self);
    }

static AtSerdesController LaneGet(AtSerdesController self, uint32 laneId)
    {
    if (laneId >= AtSerdesControllerNumLanes(self))
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s %d %s\r\n", AtSourceLocation, "laneId out of range");
        return NULL;
        }

    mMethodsGet(mThis(self))->SgmiiLanesCreate(mThis(self));

    if (mThis(self)->sgmiiLanes != NULL)
        return (AtSerdesController)AtListObjectGet(mThis(self)->sgmiiLanes, laneId);

    return NULL;
    }

static eBool SwHandleQsgmiiLanes(Tha6021EthPortQsgmiiSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        mMethodOverride(m_AtSerdesControllerOverride, StatusClear);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSerdesControllerOverride, AlarmHistoryClear);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtSerdesControllerOverride, SupportedInterruptMask);
        mMethodOverride(m_AtSerdesControllerOverride, RxAlarmForce);
        mMethodOverride(m_AtSerdesControllerOverride, RxAlarmUnforce);
        mMethodOverride(m_AtSerdesControllerOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtSerdesControllerOverride, RxForcableAlarmsGet);
        mMethodOverride(m_AtSerdesControllerOverride, NumLanes);
        mMethodOverride(m_AtSerdesControllerOverride, LaneGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void MethodsInit(AtSerdesController self)
    {
    Tha6021EthPortQsgmiiSerdesController controller = (Tha6021EthPortQsgmiiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, QsgmiiSerdesLoopbackRegister);
        mMethodOverride(m_methods, SwHandleQsgmiiLanes);
        mMethodOverride(m_methods, SgmiiLanesCreate);
        }

    mMethodsSet(controller, &m_methods);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, LpmEnableRegister);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021EthPortQsgmiiSerdesController);
    }

AtSerdesController Tha6021EthPortQsgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6021EthPortQsgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha6021EthPortQsgmiiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

eBool Tha6021EthPortQsgmiiSwHandleQsgmiiLanes(Tha6021EthPortQsgmiiSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->SwHandleQsgmiiLanes(self);
    return cAtFalse;
    }

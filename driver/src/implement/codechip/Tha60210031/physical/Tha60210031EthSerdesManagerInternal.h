/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210031EthSerdesManagerInternal.h
 * 
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHSERDESMANAGERINTERNAL_H_
#define _THA60210031ETHSERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/physical/ThaEthSerdesManagerInternal.h"
#include "Tha60210031EthSerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031EthSerdesManager * Tha60210031EthSerdesManager;

typedef struct tTha60210031EthSerdesManagerMethods
    {
    AtSerdesController (*SgmiiSerdesControllerCreate)(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId);
    uint32 (*StartVersionSupportQsgmiiTuning)(ThaEthSerdesManager self);
    }tTha60210031EthSerdesManagerMethods;

typedef struct tTha60210031EthSerdesManager
    {
    tThaEthSerdesManager super;
    const tTha60210031EthSerdesManagerMethods *methods;

    /* Private data */
    AtSerdesController *sgmiiSerdesControllers;
    }tTha60210031EthSerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthSerdesManager Tha60210031EthSerdesManagerObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHSERDESMANAGERINTERNAL_H_ */


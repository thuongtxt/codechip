/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210031EthSerdesManager.h
 * 
 * Created Date: Oct 20, 2015
 *
 * Description : 60210031 Ethernet serdes manager interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031ETHSERDESMANAGER_H_
#define _THA60210031ETHSERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60210031EthSerdesManagerSgmiiSerdesControllerGet(ThaEthSerdesManager self, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031ETHSERDESMANAGER_H_ */


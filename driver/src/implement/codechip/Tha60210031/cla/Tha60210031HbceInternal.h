/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031HbceInternal.h
 *
 * Created Date: Oct 4, 2016 
 *
 * Description : Internal Inteface for HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031HBCEINTERNAL_H_
#define _THA60210031HBCEINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/hbce/ThaHbceEntityInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031Hbce
    {
    tThaStmPwProductHbce super;
    }tTha60210031Hbce;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbce Tha60210031HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210031HBCEINTERNAL_H_ */

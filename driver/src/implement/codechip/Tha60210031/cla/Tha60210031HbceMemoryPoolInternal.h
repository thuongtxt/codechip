/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031HbceMemoryPoolInternal.h
 *
 * Created Date: Oct 3, 2016 
 *
 * Description : Internal Interface of HBCE Memory Pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031HBCEMEMORYPOOLINTERNAL_H_
#define _THA60210031HBCEMEMORYPOOLINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/hbce/ThaHbceEntityInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031HbceMemoryPool
    {
    tThaHbceMemoryPool super;
    }tTha60210031HbceMemoryPool;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceMemoryPool Tha60210031HbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210031HBCEMEMORYPOOLINTERNAL_H_ */

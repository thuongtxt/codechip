/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031ClaPwController.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : CLA PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/util/ThaUtil.h"
#include "../pmc/Tha60210031PmcReg.h"
#include "Tha60210031ModuleCla.h"
#include "Tha60210031ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegClaPwMefOverMplsControl               0x44c000
#define cThaRegClaPwMefOverMplsControl1              0x44d000

#define cThaRegClaHbceLookingUpInformationCtrl       0x44a000
#define cThaRegClaHbceLookingUpInformationCtrl1      0x44e000

#define cThaRegCLAHBCEHashingTabCtrl                 0x449000
#define cThaRegCLAHBCEHashingTabCtrl1                0x44a000

#define cThaCLAHbceFlowNumMask         cBit15_12
#define cThaCLAHbceFlowNumShift        12
#define cThaCLAHbceMemoryStartPtrMask  cBit11_0
#define cThaCLAHbceMemoryStartPtrShift 0
#define cThaCLAHbceFlowNumMask1         cBit16_13
#define cThaCLAHbceFlowNumShift1        13
#define cThaCLAHbceMemoryStartPtrMask1  cBit12_0
#define cThaCLAHbceMemoryStartPtrShift1 0

#define cThaClaHbceFlowIdMask   cBit23_13
#define cThaClaHbceFlowIdShift  13
#define cThaClaHbceFlowEnbMask  cBit12
#define cThaClaHbceFlowEnbShift 12
#define cThaClaHbceStoreIdMask  cBit11_0
#define cThaClaHbceStoreIdShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60150011ClaPwControllerMethods m_Tha60150011ClaPwControllerOverride;
static tThaClaPwControllerMethods         m_ThaClaPwControllerOverride;
static tThaStmPwProductClaPwControllerMethods  m_ThaStmPwProductClaPwControllerOverride;

static const tTha60150011ClaPwControllerMethods *m_Tha60150011ClaPwControllerMethods = NULL;
static const tThaClaPwControllerMethods         *m_ThaClaPwControllerMethods = NULL;
static const tThaStmPwProductClaPwControllerMethods *m_ThaStmPwProductClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaClaPwController, ClaHbceFlowId)
mDefineMaskShift(ThaClaPwController, ClaHbceFlowEnb)
mDefineMaskShift(ThaClaPwController, ClaHbceStoreId)

static eBool FourKCellSupported(ThaClaPwController self)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)ThaClaControllerModuleGet((ThaClaController)self);
    return Tha60210031ModuleClaPwV2FourKCellSupported(claModule);
    }

static uint32 CLAHbceFlowNumMask(ThaClaPwController self)
    {
    if (FourKCellSupported(self))
        return cThaCLAHbceFlowNumMask1;

    return cThaCLAHbceFlowNumMask;
    }

static uint8 CLAHbceFlowNumShift(ThaClaPwController self)
    {
    if (FourKCellSupported(self))
        return cThaCLAHbceFlowNumShift1;

    return cThaCLAHbceFlowNumShift;
    }

static uint32 CLAHbceMemoryStartPtrMask(ThaClaPwController self)
    {
    if (FourKCellSupported(self))
        return cThaCLAHbceMemoryStartPtrMask1;

    return cThaCLAHbceMemoryStartPtrMask;
    }

static uint8 CLAHbceMemoryStartPtrShift(ThaClaPwController self)
    {
    if (FourKCellSupported(self))
        return cThaCLAHbceMemoryStartPtrShift1;

    return cThaCLAHbceMemoryStartPtrShift;
    }

static uint32 CLAHBCEHashingTabCtrl(ThaClaPwController self)
    {
    if (FourKCellSupported(self))
        return cThaRegCLAHBCEHashingTabCtrl1;

    return cThaRegCLAHBCEHashingTabCtrl;
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaClaPwController self)
    {
    if (FourKCellSupported(self))
        return cThaRegClaHbceLookingUpInformationCtrl1;

    return cThaRegClaHbceLookingUpInformationCtrl;
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    AtModule claModule = (AtModule)ThaClaControllerModuleGet((ThaClaController)self);
    return Tha60210031HbceNew(hbceId, claModule, core);
    }

static uint32 PwMefOverMplsControl(Tha60150011ClaPwController self)
    {
    if (FourKCellSupported((ThaClaPwController)self))
        return cThaRegClaPwMefOverMplsControl1;

    return cThaRegClaPwMefOverMplsControl;
    }

static eAtRet LookupTableReset(ThaStmPwProductClaPwController self)
    {
    AtUnused(self);

    return cAtOk;
    }

static uint32 PmcBaseAddress(eBool clear)
    {
    return (0x500000UL + ((clear) ? 0 : 0x800UL));
    }

static uint32 RxDuplicatedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (ThaModuleClaPwRxDuplicatedPacketsIsSupported(ThaClaControllerModuleGet((ThaClaController)self)))
        return mChannelHwRead(pw, cAf6Reg_Pseudowire_Receive_Duplicate_packet_Counter_rc_Base + PmcBaseAddress(clear) + mClaPwOffset(self, pw), cThaModulePwPmc);
    return 0;
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, CLAHBCEHashingTabCtrl);
        mMethodOverride(m_ThaClaPwControllerOverride, RxDuplicatedPacketsGet);
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceFlowNum)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceMemoryStartPtr)

        mMethodOverride(m_ThaClaPwControllerOverride, ClaHbceLookingUpInformationCtrl);
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceFlowId)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceFlowEnb)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceStoreId)
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void OverrideThaStmPwProductClaPwController(ThaClaPwController self)
    {
    ThaStmPwProductClaPwController controller = (ThaStmPwProductClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaStmPwProductClaPwControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductClaPwControllerOverride, m_ThaStmPwProductClaPwControllerMethods, sizeof(m_ThaStmPwProductClaPwControllerOverride));

        mMethodOverride(m_ThaStmPwProductClaPwControllerOverride, LookupTableReset);
        }

    mMethodsSet(controller, &m_ThaStmPwProductClaPwControllerOverride);
    }

static void OverrideTha60150011ClaPwController(ThaClaPwController self)
    {
    Tha60150011ClaPwController controller = (Tha60150011ClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011ClaPwControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ClaPwControllerOverride, m_Tha60150011ClaPwControllerMethods, sizeof(m_Tha60150011ClaPwControllerOverride));

        mMethodOverride(m_Tha60150011ClaPwControllerOverride, PwMefOverMplsControl);
        }

    mMethodsSet(controller, &m_Tha60150011ClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaStmPwProductClaPwController(self);
    OverrideTha60150011ClaPwController(self);
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ClaPwController);
    }

ThaClaPwController Tha60210031ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60210031ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ClaPwControllerObjectInit(newController, cla);
    }

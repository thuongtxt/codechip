/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031Hbce.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : Classify HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/cla/pw/ThaModuleClaPw.h"
#include "Tha60210031ModuleCla.h"
#include "Tha60210031HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaClaHbceCodingSelectedModeDwIndex 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods m_ThaHbceOverride;

/* Save super implementation */
static const tThaHbceMethods *m_ThaHbceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClaPwController ClaPwController(ThaHbce self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return ThaModuleClaPwControllerGet(claModule);
    }

static uint32 HashIndexGetOnPart(ThaHbce self, uint8 partId, const uint32 hwLabel)
    {
    uint32 longReg[cLongRegSizeInDwords];
    uint16 codeLevel;
    uint32 swHbcePatern21_11;
    uint32 swHbcePatern10_0;
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    AtDevice device = AtModuleDeviceGet((AtModule)claModule);
    uint32 partOffset = ThaDeviceModulePartOffset((ThaDevice)device, cThaModuleCla, partId);
    uint32 regAddr = ThaClaPwControllerCLAHBCEGlbCtrl(ThaModuleClaPwControllerGet(claModule)) + partOffset;
    ThaClaPwController pwController = ClaPwController(self);

    /* On the standby, reduce read operations as much as possible */
    if (AtModuleInAccessible((AtModule)claModule))
        ThaHbceStandbyGlbCtrlCached(self, longReg, cLongRegSizeInDwords);

    /* On active, no limit on reading */
    else
        mModuleHwLongRead(claModule, regAddr, longReg, cLongRegSizeInDwords, self->core);

    mFieldGet(longReg[cThaClaHbceCodingSelectedModeDwIndex],
              ThaClaPwControllerClaHbceCodingSelectedModeMask(pwController),
              ThaClaPwControllerClaHbceCodingSelectedModeShift(pwController),
              uint16,
              &codeLevel);

    mFieldGet(hwLabel, cBit21_11, 11, uint32, &swHbcePatern21_11);
    mFieldGet(hwLabel, cBit10_0, 0, uint32, &swHbcePatern10_0);

    if (codeLevel == 0)
        return swHbcePatern10_0;
    if (codeLevel == 1)
        return swHbcePatern21_11 ^ swHbcePatern10_0;

    return 0x0;
    }

static uint32 HwHcbeLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType)
    {
    uint32 hwLabel = 0;
    uint8 ethPortId = (uint8)(ethPort ? (uint8)AtChannelHwIdGet((AtChannel)ethPort) : 0);
    AtUnused(self);

    mFieldIns(&hwLabel, cBit22_3, 3, label);
    mFieldIns(&hwLabel, cBit2_1, 1, ThaHbcePktType(psnType));
    mFieldIns(&hwLabel, cBit0, 0, ethPortId);

    return hwLabel;
    }

static eAtRet RemainBitsGet(const uint32 hwLabel, uint32 *remainBits)
    {
    uint32 lsb;

    mFieldGet(hwLabel, cBit22_11, 11, uint32, &lsb);
    remainBits[0] = lsb;

    return cAtOk;
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel   = HwHcbeLabel(self, ethPort, label, psnType);
    uint32 hashIndex = HashIndexGetOnPart(self, partId, hwLabel);

    if (remainBits)
        RemainBitsGet(hwLabel, remainBits);

    return hashIndex;
    }

static uint32 MaxNumEntries(ThaHbce self)
    {
    AtUnused(self);
    return 2*1024;
    }

static uint32 EntryOffset(ThaHbce self, uint32 entryIndex, uint8 pageId)
    {
    if (Tha60210031ModuleClaPwV2FourKCellSupported((ThaModuleClaPwV2)ThaHbceEntityClaModuleGet((ThaHbceEntity)self)))
        return (pageId * 0x1000UL) + entryIndex;

    return (pageId * 0x800UL) + entryIndex;
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60210031HbceMemoryPoolNew(self);
    }

static uint32 CellGroupWorkingMask(ThaHbce self)
    {
    AtUnused(self);
    return cCLAHbceGrpWorking_Mask;
    }

static uint32 CellGroupWorkingShift(ThaHbce self)
    {
    AtUnused(self);
    return cCLAHbceGrpWorking_Shift;
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, m_ThaHbceMethods, sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, PsnHash);
        mMethodOverride(m_ThaHbceOverride, EntryOffset);
        mMethodOverride(m_ThaHbceOverride, CellGroupWorkingMask);
        mMethodOverride(m_ThaHbceOverride, CellGroupWorkingShift);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031Hbce);
    }

ThaHbce Tha60210031HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductHbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60210031HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031HbceObjectInit(newHbce, hbceId, claModule, core);
    }

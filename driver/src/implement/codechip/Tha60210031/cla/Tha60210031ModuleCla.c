/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031ModuleCla.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60210031Device.h"
#include "Tha60210031ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegClaPwTypeCtrl        0x44b000
#define cThaRegClaPwTypeCtrl1       0x44c000

#define cThaRegHspwPerGrpEnbReg     0x44D000
#define cThaRegHspwPerGrpEnbReg1    0x449000

#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlowLo_Mask          cBit31_25
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlowLo_Shift         25
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlowHi_Mask          cBit2_0
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlowHi_Shift         0

#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_Mask          cBit4
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_Shift         4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031ModuleCla)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031ModuleClaMethods m_methods;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tThaModuleClaPwV2Methods m_ThaModuleClaPwV2Override;
static tThaModuleClaMethods     m_ThaModuleClaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ClaPwTypeCtrlReg(ThaModuleClaPwV2 self)
    {
    if (Tha60210031ModuleClaPwV2FourKCellSupported(self))
        return cThaRegClaPwTypeCtrl1;

    return cThaRegClaPwTypeCtrl;
    }

static uint32 HspwPerGrpEnbReg(ThaModuleClaPwV2 self, eBool isWorking, uint32 groupIdx)
    {
    uint32 offset = (isWorking ? 1 : 0) * 0x400UL + groupIdx;
    if (Tha60210031ModuleClaPwV2FourKCellSupported(self))
        return cThaRegHspwPerGrpEnbReg1 + offset;

    return cThaRegHspwPerGrpEnbReg + offset;
    }

static void HsGroupPwAddHwWrite(AtPw pwAdapter, AtPwGroup pwGroup, uint32 regAddr, eBool isPrimary)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 groupId = AtPwGroupIdGet(pwGroup);
    uint32 loGroup = groupId & cBit6_0;
    uint32 hiGroup = groupId >> 7;

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlowLo_, loGroup);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlowHi_, hiGroup);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_, 0);
    mRegFieldSet(longRegVal[1], cCLAHbceGrpWorking_, mBoolToBin(isPrimary));

    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

static void HsGroupPwRemoveHwWrite(AtPw pwAdapter, uint32 regAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_, 1);
    mRegFieldSet(longRegVal[1], cCLAHbceGrpWorking_, 1);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

static eBool FourKCellSupported(Tha60210031ModuleCla self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSupportFourKCell(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Debug(AtModule self)
    {
	ThaMdouleClaPwDebug(self);
    return cAtOk;
    }

static void OverrideAtModule(AtModule self)
	{
	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

		mMethodOverride(m_AtModuleOverride, Debug);
		}

	mMethodsSet(self, &m_AtModuleOverride);
	}

static void OverrideThaModuleClaPwV2(AtModule self)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypeCtrlReg);
        mMethodOverride(m_ThaModuleClaPwV2Override, HspwPerGrpEnbReg);
        mMethodOverride(m_ThaModuleClaPwV2Override, HsGroupPwAddHwWrite);
        mMethodOverride(m_ThaModuleClaPwV2Override, HsGroupPwRemoveHwWrite);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwV2Override);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60210031ClaPwControllerNew(self);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210031ModuleCla module = (Tha60210031ModuleCla)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, FourKCellSupported);
        }

    mMethodsSet(module, &m_methods);
    }

static void Override(AtModule self)
    {
	OverrideAtModule(self);
    OverrideThaModuleClaPwV2(self);
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModuleCla);
    }

AtModule Tha60210031ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210031ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModuleClaObjectInit(newModule, device);
    }

eBool Tha60210031ModuleClaPwV2FourKCellSupported(ThaModuleClaPwV2 self)
    {
    if (self)
        return mMethodsGet(mThis(self))->FourKCellSupported(mThis(self));
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210031ModuleCla.h
 * 
 * Created Date: Nov 17, 2015
 *
 * Description : 60210031 module CLA interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULECLA_H_
#define _THA60210031MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/pw/ThaModuleClaPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cCLAHbceGrpWorking_Mask          cBit3
#define cCLAHbceGrpWorking_Shift         3

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleCla * Tha60210031ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210031ModuleClaPwV2FourKCellSupported(ThaModuleClaPwV2 self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULECLA_H_ */


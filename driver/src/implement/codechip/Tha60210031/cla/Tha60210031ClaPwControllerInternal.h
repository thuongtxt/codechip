/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031ClaPwControllerInternal.h
 *
 * Created Date: Oct 4, 2016 
 *
 * Description : Internal Interface of CLA PW Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031CLAPWCONTROLLERINTERNAL_H_
#define _THA60210031CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60150011/cla/Tha60150011ModuleClaInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ClaPwController
    {
    tTha60150011ClaPwController super;
    }tTha60210031ClaPwController;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60210031ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210031CLAPWCONTROLLERINTERNAL_H_ */

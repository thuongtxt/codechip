/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210031HbceMemoryPool.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : HBCE memory poll
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../default/cla/controllers/ThaClaPwControllerInternal.h"
#include "../man/Tha60210031Device.h"
#include "../../../default/cla/pw/ThaModuleClaPw.h"
#include "Tha60210031ModuleCla.h"
#include "Tha60210031HbceMemoryPoolInternal.h"

/*--------------------------- Define -----------------------------------------*/

/* Dword 1 */
#define cCLAHbceFlowDirectMask     cBit4 /* Bit 36 */
#define cCLAHbceFlowDirectShift    4
#define cCLAHbceGrpWorkingMask     cBit3 /* Bit 35 */
#define cCLAHbceGrpWorkingShift    3
#define cCLAHbceGrpIDFlowHeadMask  cBit2_0 /* Bit 34_32 */
#define cCLAHbceGrpIDFlowHeadShift 0

/* Dword 0 */
#define cCLAHbceGrpIDFlowTailMask  cBit31_25
#define cCLAHbceGrpIDFlowTailShift 25

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override  */
static tThaHbceMemoryPoolMethods m_ThaHbceMemoryPoolOverride;

/* Save super implementation */
static const tThaHbceMemoryPoolMethods *m_ThaHbceMemoryPoolMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    if (Tha60210031ModuleClaPwV2FourKCellSupported((ThaModuleClaPwV2)ThaHbceEntityClaModuleGet((ThaHbceEntity)self)))
        return 4 * 1024;

    return 2 * 1024;
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaHbceMemoryPool self, AtModule claModule)
    {
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet((ThaModuleCla)claModule);
    return ThaClaPwControllerClaHbceLookingUpInformationCtrl(claPwController) + ThaHbcePartOffset(self->hbce);
    }

static eBool HspwIsSupported(AtModule claModule)
    {
    AtDevice device = AtModuleDeviceGet(claModule);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSupportHspw(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 regAddr;
    uint32 longRegVal[cLongRegSizeInDwords];
    const uint8 cFlowNormalMode = 1;

    if (claModule == NULL)
        return cAtErrorObjectNotExist;

    if (!HspwIsSupported(claModule))
        return m_ThaHbceMemoryPoolMethods->CellInit(self, cellIndex);

    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mRegFieldSet(longRegVal[1], cCLAHbceFlowDirect, cFlowNormalMode);

    regAddr = ClaHbceLookingUpInformationCtrl(self, claModule) + mMethodsGet(self)->OffsetForCell(self, cellIndex);
    mMethodsGet(claModule)->HwLongWriteOnCore(claModule, regAddr, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));

    regAddr = ClaHbceLookingUpInformationCtrl(self, claModule) + ThaHbceMemoryPoolMaxNumCells(self) + cellIndex;
    mMethodsGet(claModule)->HwLongWriteOnCore(claModule, regAddr, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));

    return cAtOk;
    }

static void ApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex, uint8 page)
    {
    uint32 address;
    uint32 longRegVal[cLongRegSizeInDwords];
    ThaHbceMemoryCellContent cellContent;
    ThaPwHeaderController headerController;
    ThaPwAdapter pwAdapter;
    uint32 storeId, hspwId = 0, hsPrimary, pwIsNotInGroup;
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaClaPwController pwController;
    AtPwGroup hsPw = NULL;
    AtPw pw;

    if (!HspwIsSupported(claModule))
        {
        m_ThaHbceMemoryPoolMethods->ApplyCellToHardware(self, cellIndex, page);
        return;
        }

    cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex(self, cellIndex));
    if (cellContent == NULL)
        return;

    if (ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL) == NULL)
        return;

    headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
    pwController = ThaModuleClaPwControllerGet((ThaModuleCla)claModule);
    storeId = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
    pwAdapter = ThaPwHeaderControllerAdapterGet(ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent));
    address = ClaHbceLookingUpInformationCtrl(self, claModule) + (ThaHbceMemoryPoolMaxNumCells(self) * page) + cellIndex;
    pw = ThaPwAdapterPwGet(pwAdapter);
    hsPw = pw ? AtPwHsGroupGet(pw) : NULL; /* To avoid unnecessary log messages */

    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));

    if (ThaPwHeaderControllerIsPrimary(headerController) == cAtFalse)
        hsPrimary = 0;
    else
        hsPrimary = 1;

    if (hsPw == NULL)
        pwIsNotInGroup = 1;
    else
        {
        pwIsNotInGroup = 0;
        hspwId = AtPwGroupIdGet(hsPw);
        }
    mRegFieldSet(longRegVal[1], cCLAHbceFlowDirect, pwIsNotInGroup);
    mRegFieldSet(longRegVal[1], cCLAHbceGrpWorking, hsPrimary);
    mRegFieldSet(longRegVal[1], cCLAHbceGrpIDFlowHead, (hspwId >> 7));
    mRegFieldSet(longRegVal[0], cCLAHbceGrpIDFlowTail, (hspwId & cBit6_0));

    mFieldIns(&longRegVal[0],
              mFieldMask(pwController, ClaHbceFlowId),
              mFieldShift(pwController, ClaHbceFlowId),
              (pwAdapter) ? AtChannelHwIdGet((AtChannel)pwAdapter) : 0);

    mFieldIns(&longRegVal[0],
              mFieldMask(pwController, ClaHbceFlowEnb),
              mFieldShift(pwController, ClaHbceFlowEnb),
              ((pwAdapter) && AtChannelIsEnabled((AtChannel)pwAdapter)) ? 1 : 0);

    mFieldIns(&longRegVal[0],
              mFieldMask(pwController, ClaHbceStoreId),
              mFieldShift(pwController, ClaHbceStoreId),
              storeId);

    mModuleHwLongWrite(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));
    }

static ThaPwHeaderController StandbyRestoreCellHeaderController(ThaHbceMemoryPool self, ThaPwAdapter pwAdapter, ThaHbceMemoryCell cell, uint8 page)
    {
    uint32 address;
    uint32 longRegVal[cLongRegSizeInDwords];
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    AtPw pw = ThaPwAdapterPwGet(pwAdapter);
    AtPwGroup pwGroup = AtPwHsGroupGet(pw);
    uint32 cellIndex;
    uint32 groupId;

    if (pwGroup == NULL)
        return m_ThaHbceMemoryPoolMethods->StandbyRestoreCellHeaderController(self, pwAdapter, cell, page);

    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    address = ClaHbceLookingUpInformationCtrl(self, claModule) + (ThaHbceMemoryPoolMaxNumCells(self) * page) + cellIndex;
    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));

    /* For sanity checking */
    groupId  = mRegField(longRegVal[0], cCLAHbceGrpIDFlowTail);
    groupId |= mRegField(longRegVal[1], cCLAHbceGrpIDFlowHead) << 7;
    if (groupId != AtPwGroupIdGet(pwGroup))
        AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation, "Hardware group ID and SW group ID are mismatched\r\n");

    if (longRegVal[1] & cCLAHbceGrpWorkingMask)
        return ThaPwAdapterHeaderController(pwAdapter);
    else
        return ThaPwAdapterBackupHeaderController(pwAdapter);
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint8 activePage;
    uint32 pageStartCell, address;
    uint32 longRegVal[cLongRegSizeInDwords];
    ThaHbceMemoryCellContent cellContent;
    ThaClaPwController pwController;

    if (!HspwIsSupported(claModule))
        return m_ThaHbceMemoryPoolMethods->CellEnable(self, cell, enable);

    activePage = ThaHbceActivePage(self->hbce);
    pageStartCell = ThaHbceMemoryPoolMaxNumCells(self) * activePage;
    address = ClaHbceLookingUpInformationCtrl(self, claModule) + pageStartCell + ThaHbceMemoryCellIndexGet(cell);
    cellContent = ThaHbceMemoryCellContentGet(cell);
    pwController = ThaModuleClaPwControllerGet((ThaModuleCla)claModule);

    /* Update hardware */
    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));
    mFieldIns(&longRegVal[0], mFieldMask(pwController, ClaHbceFlowEnb), mFieldShift(pwController, ClaHbceFlowEnb), enable ? 1 : 0);
    mModuleHwLongWrite(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));

    /* Update software database */
    ThaHbceMemoryCellContentEnable(cellContent, enable);

    return cAtOk;
    }

static eBool CellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 longRegVal[cLongRegSizeInDwords];
    uint8 activePage = ThaHbceActivePage(self->hbce);
    uint32 pageStartCell = ThaHbceMemoryPoolMaxNumCells(self) * activePage;
    uint32 address = ClaHbceLookingUpInformationCtrl(self, claModule) + pageStartCell + ThaHbceMemoryCellIndexGet(cell);
    ThaClaPwController pwController =ThaModuleClaPwControllerGet((ThaModuleCla)claModule);

    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));
    return (longRegVal[0] & mFieldMask(pwController, ClaHbceFlowEnb)) ? cAtTrue : cAtFalse;
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMemoryPoolMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, m_ThaHbceMemoryPoolMethods, sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellInit);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, ApplyCellToHardware);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellIsEnabled);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, StandbyRestoreCellHeaderController);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031HbceMemoryPool);
    }

ThaHbceMemoryPool Tha60210031HbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60210031HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031HbceMemoryPoolObjectInit(newPool, hbce);
    }

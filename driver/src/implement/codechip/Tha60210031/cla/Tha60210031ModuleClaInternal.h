/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210031ModuleClaInternal.h
 * 
 * Created Date: Oct 29, 2016
 *
 * Description : CLA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULECLAINTERNAL_H_
#define _THA60210031MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/cla/Tha60150011ModuleClaInternal.h"
#include "Tha60210031ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModuleClaMethods
    {
    eBool (*FourKCellSupported)(Tha60210031ModuleCla self);
    }tTha60210031ModuleClaMethods;

typedef struct tTha60210031ModuleCla
    {
    tTha60150011ModuleCla super;
    const tTha60210031ModuleClaMethods *methods;
    }tTha60210031ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210031ModuleClaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULECLAINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210031ModulePw.h
 * 
 * Created Date: Oct 24, 2015
 *
 * Description : 60210031 module PW interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEPW_H_
#define _THA60210031MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModulePw * Tha60210031ModulePw;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210031ModulePwQueueIsDynamicAllocated(AtModulePw self);
eAtRet Tha60210031ModulePwQueueDynamicAllocateEnable(AtModulePw self, eBool enable);
uint32 Tha60210031ModulePwMaxNumPwsPerSts24Slice(Tha60210031ModulePw self);
uint32 Tha60210031ModulePwOriginalNumPwsPerSts24Slice(Tha60210031ModulePw self);
#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEPW_H_ */


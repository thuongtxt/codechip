/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210031PwDebugger.h
 * 
 * Created Date: Sep 4, 2015
 *
 * Description : 60210031 PW debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PWDEBUGGER_H_
#define _THA60210031PWDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210031PwPrbsCheck(ThaPwDebugger self, uint32 statusReg, AtPw pwAdapter, uint32 error, uint32 notSync);
ThaPwDebugger Tha60210031PwDebuggerObjectInit(ThaPwDebugger self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PWDEBUGGER_H_ */


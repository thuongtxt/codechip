/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Pw
 *
 * File        : Tha60210031ModulePw.c
 *
 * Created Date: Aug 29, 2014
 *
 * Description : PW module of product 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pw/hspw/ThaPwHsGroup.h"
#include "../../Tha60210011/pw/headercontroller/Tha60210011PwHeaderControllerInternal.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../../Tha60210011/cdr/Tha60210011ModuleCdr.h"
#include "../eth/Tha60210031EthPort.h"
#include "../pmc/Tha60210031PmcReg.h"
#include "../man/Tha60210031Device.h"
#include "headercontrollers/Tha60210031PwHeaderController.h"
#include "defectcontroller/Tha60210031PwDefectController.h"
#include "interruptprocessor/Tha60210031PwInterruptProcessor.h"
#include "Tha60210031ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegModulePwJitterBufferCenter 0x243C01
#define cRegModulePwJitterBufferCenterEnableMask cBit8
#define cRegModulePwJitterBufferCenterEnableShift 8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031ModulePw)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031ModulePwMethods m_methods;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtModuleMethods            m_AtModuleOverride;
static tAtModulePwMethods          m_AtModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;
static tTha60150011ModulePwMethods m_Tha60150011ModulePwOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtModuleMethods    *m_AtModuleMethods    = NULL;
static const tThaModulePwMethods *m_ThaModulePwMethods = NULL;
static const tAtModulePwMethods  *m_AtModulePwMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVersionReader VersionReader(Tha60210031ModulePw self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 OriginalNumPwsPerSts24Slice(Tha60210031ModulePw self)
    {
    AtUnused(self);
    return 1024UL;
    }

static uint32 MaxNumPwsPerSts24Slice(Tha60210031ModulePw self)
    {
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionHas1344PwsPerSts24Slice(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? 1344UL : OriginalNumPwsPerSts24Slice(self);
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1344;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1024;
    }

static ThaPwHeaderController BackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210031PwBackupHeaderControllerNew((ThaPwAdapter)adapter);
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210031PwHeaderControllerNew((ThaPwAdapter)adapter);
    }

static eBool MBitDefectIsSupported(Tha60210031ModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSupportMbitAlarm(device))
        return cAtTrue;

    return cAtFalse;
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (mMethodsGet(mThis(self))->MBitDefectIsSupported(mThis(self)))
        return Tha60210031PwDefectControllerV2New(pw);
    else
        return Tha60210031PwDefectControllerNew(pw);
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    return Tha60210031PwInterruptProcessorNew(self);
    }

static eAtModulePwRet JitterBufferCenteringEnable(AtModulePw self, eBool enable)
    {
    uint32 reg, val;
    reg = cRegModulePwJitterBufferCenter;
    val = mModuleHwRead(self, reg);
    mFieldIns(&val, cRegModulePwJitterBufferCenterEnableMask, cRegModulePwJitterBufferCenterEnableShift, enable?1:0);
    mModuleHwWrite(self, reg, val);

    return cAtOk;
    }

static eBool JitterBufferCenteringIsEnabled(AtModulePw self)
    {
    eBool ret;
    uint32 reg, val;
    reg = cRegModulePwJitterBufferCenter;
    val = mModuleHwRead(self, reg);
    mFieldGet(val, cRegModulePwJitterBufferCenterEnableMask, cRegModulePwJitterBufferCenterEnableShift, eBool, &ret);
    ret = mBinToBool(ret);

    return ret;
    }

static eBool JitterBufferCenteringIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaPwDebugger PwDebuggerCreate(ThaModulePw self)
    {
    AtUnused(self);
    return Tha60210031PwDebuggerNew();
    }

static ThaPwEthPortBinder PwEthPortBinderCreate(ThaModulePw self)
    {
    return Tha60210031PwEthPortBinderNew(self);
    }

static AtEthPort EthPort(AtModulePw self)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    return AtModuleEthPortGet(moduleEth, 0);
    }

static eAtRet DeletePw(AtModulePw self, uint32 pwId)
    {
    uint8 queueId;
    AtPw pw = (AtPw)ThaPwAdapterGet(AtModulePwGetPw(self, pwId));
    AtEthPort boundEthPort;
    if (pw == NULL)
        return cAtOk;

    queueId = AtPwEthPortQueueGet(pw);
    Tha60210031EthPortQueuePwRemove(EthPort(self), queueId, pw);

    boundEthPort = AtPwEthPortGet(pw);
    if (boundEthPort)
        Tha60210031EthPortBoundPwRemove(boundEthPort, pw);

    /* Let super do other things */
    return m_AtModulePwMethods->DeletePw(self, pwId);
    }

static uint32 StartVersionSupportHspw(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0x0;
        
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x24, 00);
    }

static eBool RtpIsEnabledAsDefault(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HwStorageIsSupported(Tha60150011ModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool TimingModeIsApplicableForEPAR(ThaModulePw self, eAtTimingMode timingMode)
    {
    if ((timingMode == cAtTimingModeExt1Ref) ||
        (timingMode == cAtTimingModeExt2Ref))
        return cAtTrue;
    return m_ThaModulePwMethods->TimingModeIsApplicableForEPAR(self, timingMode);
    }

static uint32 DefaultLopsSetThreshold(AtModulePw self)
    {
    AtUnused(self);
    return 30;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210031ModulePw object = (Tha60210031ModulePw)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(queueDynamicAllocate);
    }

static eBool HitlessHeaderChangeShouldBeEnabledByDefault(ThaModulePw self)
    {
    return Tha602100xxModulePwHitlessHeaderChangeShouldBeEnabledByDefault(self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    Tha60210031ModulePwQueueDynamicAllocateEnable((AtModulePw)self, cAtTrue);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->AsyncInit(self);
    Tha60210031ModulePwQueueDynamicAllocateEnable((AtModulePw)self, cAtTrue);

    return ret;
    }

static eBool AutoRxMBitShouldEnable(AtModulePw self)
    {
    return Tha602100xxModuleAutoRxMBitShouldEnable(self);
    }

static ThaModuleCdr CdrModule(AtModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

static eBool RtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)CdrModule(self);

    if (Tha60210011ModuleCdrFlexibleFrequency(cdrModule))
        return ThaModuleCdrTimestampFrequencyInRange(cdrModule, khz);

    return m_AtModulePwMethods->RtpTimestampFrequencyIsSupported(self, khz);
    }

static eAtModulePwRet CepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyRegSet(CdrModule(self), khz);
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(CdrModule(self));
    }

static eAtModulePwRet CesRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyRegSet(CdrModule(self), khz);
    }

static uint32 CesRtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(CdrModule(self));
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyRegSet(CdrModule(self), khz);
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(CdrModule(self));
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60150011ModulePw(AtModulePw self)
    {
    Tha60150011ModulePw pwModule = (Tha60150011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePwOverride, mMethodsGet(pwModule), sizeof(m_Tha60150011ModulePwOverride));
        mMethodOverride(m_Tha60150011ModulePwOverride, HwStorageIsSupported);
        }

    mMethodsSet(pwModule, &m_Tha60150011ModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(m_AtModulePwOverride));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringEnable);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringIsEnabled);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringIsSupported);
        mMethodOverride(m_AtModulePwOverride, DeletePw);
        mMethodOverride(m_AtModulePwOverride, RtpIsEnabledAsDefault);
        mMethodOverride(m_AtModulePwOverride, DefaultLopsSetThreshold);
        mMethodOverride(m_AtModulePwOverride, AutoRxMBitShouldEnable);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyIsSupported);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencyGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(pwModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, m_ThaModulePwMethods, sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, BackupHeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, PwDebuggerCreate);
        mMethodOverride(m_ThaModulePwOverride, InterruptProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, HeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortBinderCreate);
        mMethodOverride(m_ThaModulePwOverride, StartVersionSupportHspw);
        mMethodOverride(m_ThaModulePwOverride, TimingModeIsApplicableForEPAR);
        mMethodOverride(m_ThaModulePwOverride, HitlessHeaderChangeShouldBeEnabledByDefault);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideAtObject(AtModulePw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(AtModulePw self)
    {
    Tha60210031ModulePw module = mThis(self);

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MBitDefectIsSupported);
        }

    mMethodsSet(module, &m_methods);
    }

static void Override(AtModulePw self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60150011ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031ModulePw);
    }

AtModulePw Tha60210031ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    Tha60210031ModulePwQueueDynamicAllocateEnable(self, cAtTrue);

    return self;
    }

AtModulePw Tha60210031ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031ModulePwObjectInit(newModule, device);
    }

eBool Tha60210031ModulePwQueueIsDynamicAllocated(AtModulePw self)
    {
    if (self)
        return mThis(self)->queueDynamicAllocate;
    return cAtFalse;
    }

eAtRet Tha60210031ModulePwQueueDynamicAllocateEnable(AtModulePw self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    mThis(self)->queueDynamicAllocate = enable;
    return cAtOk;
    }

uint32 Tha60210031ModulePwMaxNumPwsPerSts24Slice(Tha60210031ModulePw self)
    {
    if (self)
        return MaxNumPwsPerSts24Slice(self);
    return 0;
    }

uint32 Tha60210031ModulePwOriginalNumPwsPerSts24Slice(Tha60210031ModulePw self)
    {
    if (self)
        return OriginalNumPwsPerSts24Slice(self);
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210031PwDefectController.c
 *
 * Created Date: Aug 11, 2015
 *
 * Description : PW Defect controller of 60210031: 2.5G 1344 PWEs PMC.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/ThaModulePw.h"
#include "../../pmc/Tha60210031PmcReg.h"
#include "../../man/Tha60210031Device.h"
#include "Tha60210031PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                      m_AtChannelOverride;
static tAtPwMethods                           m_AtPwOverride;
static tThaPwDefectControllerMethods          m_ThaPwDefectControllerOverride;

/* Save super's implementation */
static const tAtPwMethods *m_AtPwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CurrentStatusRegister(AtChannel self)
    {
    return ThaModulePwDefectCurrentStatusRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    return ThaModulePwDefectInterruptStatusRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 InterruptMaskRegister(AtChannel self)
    {
    return ThaModulePwDefectInterruptMaskRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 PwDefaultOffset(ThaPwDefectController self)
    {
    return AtChannelHwIdGet((AtChannel)mAdapter(self));
    }

static uint32 PwDefectMaskGet(AtChannel self, uint32 regVal)
    {
    ThaPwDefectController controller = (ThaPwDefectController)self;
    uint32 defects = 0;

    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Mask)
        defects |= cAtPwAlarmTypeRBit;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Mask)
        defects |= cAtPwAlarmTypeLops;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Mask)
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Mask)
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Mask)
        defects |= cAtPwAlarmTypeMissingPacket;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Mask)
        defects |= cAtPwAlarmTypeLBit;
    if (regVal & mMethodsGet(controller)->MalformedMask(controller))
        defects |= cAtPwAlarmTypeMalformedPacket;
    if (regVal & mMethodsGet(controller)->StrayMask(controller))
        defects |= cAtPwAlarmTypeStrayPacket;
    if (regVal & mMethodsGet(controller)->MbitMask(controller))
        defects |= cAtPwAlarmTypeMBit;

    return defects;
    }

static uint32 DefectHwGet(AtChannel self)
    {
    const uint32 address = CurrentStatusRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    return PwDefectMaskGet(self, regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHwGet(self);
    }

static uint32 DefectHistoryHwGet(AtChannel self, eBool read2Clear)
    {
    const uint32 address = InterruptStatusRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    uint32 defects = PwDefectMaskGet(self, regVal);

    if (read2Clear)
        mChannelHwWrite(self, address, regVal, cAtModulePw);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtTrue);
    }

static void InterruptProcess(AtPw self, uint32 pwOffset, AtHal hal)
    {
    const uint32 intrAddress = InterruptStatusRegister((AtChannel)self) + pwOffset;
    ThaPwDefectController controller = (ThaPwDefectController)self;
    uint32 intrMask   = AtHalRead(hal, InterruptMaskRegister((AtChannel)self) + pwOffset);
    uint32 intrStatus = AtHalRead(hal, intrAddress);
    uint32 currStatus;
    uint32 defects    = 0;
    uint32 state      = 0;

    intrStatus &= intrMask;
    /* Clear interrupt */
    AtHalWrite(hal, intrAddress, intrStatus);
    currStatus = AtHalRead(hal, CurrentStatusRegister((AtChannel)self) + pwOffset);

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrSta_Mask)
        {
        defects |= cAtPwAlarmTypeRBit;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_LbitCurStatus_Mask)
            state |= cAtPwAlarmTypeRBit;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_LofsSyncStateChgIntrSta_Mask)
        {
        defects |= cAtPwAlarmTypeLops;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_LofsSyncCurStatus_Mask)
            state |= cAtPwAlarmTypeLops;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrSta_Mask)
        {
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Mask)
            state |= cAtPwAlarmTypeJitterBufferUnderrun;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrSta_Mask)
        {
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Mask)
            state |= cAtPwAlarmTypeJitterBufferOverrun;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrSta_Mask)
        {
        defects |= cAtPwAlarmTypeMissingPacket;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Mask)
            state |= cAtPwAlarmTypeMissingPacket;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrSta_Mask)
        {
        defects |= cAtPwAlarmTypeLBit;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Mask)
            state |= cAtPwAlarmTypeLBit;
        }

    if (intrStatus & mMethodsGet(controller)->MbitMask(controller))
        {
        defects |= cAtPwAlarmTypeMBit;
        if (currStatus & mMethodsGet(controller)->MbitMask(controller))
            state |= cAtPwAlarmTypeMBit;
        }

    if (intrStatus & mMethodsGet(controller)->MalformedMask(controller))
        {
        defects |= cAtPwAlarmTypeMalformedPacket;
        if (currStatus & mMethodsGet(controller)->MalformedMask(controller))
            state |= cAtPwAlarmTypeMalformedPacket;
        }

    if (intrStatus & mMethodsGet(controller)->StrayMask(controller))
        {
        defects |= cAtPwAlarmTypeStrayPacket;
        if (currStatus & mMethodsGet(controller)->StrayMask(controller))
            state |= cAtPwAlarmTypeStrayPacket;
        }

    AtChannelAllAlarmListenersCall((AtChannel)ThaPwAdapterPwGet(mAdapter(self)), defects, state);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtPwAlarmTypeLBit|cAtPwAlarmTypeRBit|cAtPwAlarmTypeLops|
                                    cAtPwAlarmTypeMissingPacket|cAtPwAlarmTypeJitterBufferUnderrun|
                                    cAtPwAlarmTypeJitterBufferOverrun|cAtPwAlarmTypeStrayPacket|
                                    cAtPwAlarmTypeMalformedPacket|cAtPwAlarmTypeMBit);
    AtUnused(self);
    return supportedMasks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    const uint32 address = InterruptMaskRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    ThaPwDefectController controller = (ThaPwDefectController)self;
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);

    mSetIntrBitMask(cAtPwAlarmTypeLBit, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeRBit, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeLops, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsSyncStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeMissingPacket, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferUnderrun, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferOverrun,  defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeMalformedPacket,  defectMask, enableMask, mMethodsGet(controller)->MalformedMask(controller));
    mSetIntrBitMask(cAtPwAlarmTypeStrayPacket,      defectMask, enableMask, mMethodsGet(controller)->StrayMask(controller));
    mSetIntrBitMask(cAtPwAlarmTypeMBit, defectMask, enableMask, mMethodsGet(controller)->MbitMask(controller));
    mChannelHwWrite(self, address, regVal, cAtModulePw);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    const uint32 address = InterruptMaskRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    return PwDefectMaskGet(self, regVal);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPwAlarmTypeLBit:                return cAtTrue;
        case cAtPwAlarmTypeRBit:                return cAtTrue;
        case cAtPwAlarmTypeLops:                return cAtTrue;
        case cAtPwAlarmTypeMissingPacket:       return cAtTrue;
        case cAtPwAlarmTypeJitterBufferUnderrun:return cAtTrue;
        case cAtPwAlarmTypeJitterBufferOverrun: return cAtTrue;
        case cAtPwAlarmTypeMalformedPacket:     return cAtTrue;
        case cAtPwAlarmTypeStrayPacket:         return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 MbitMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StrayMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cBit7;
    }

static uint32 MalformedMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cBit6;
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, mMethodsGet(self), sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, MbitMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, StrayMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, MalformedMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, PwDefaultOffset);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, InterruptProcess);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel pw = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(pw), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(pw, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideThaPwDefectController(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PwDefectController);
    }

ThaPwDefectController Tha60210031PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011PwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60210031PwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PwDefectControllerObjectInit(newController, pw);
    }

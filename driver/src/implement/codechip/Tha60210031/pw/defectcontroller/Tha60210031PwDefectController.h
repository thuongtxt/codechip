/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210031PwDefectController.h
 * 
 * Created Date: Oct 30, 2015
 *
 * Description : PW defect controller interface for 60210031 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PWDEFECTCONTROLLER_H_
#define _THA60210031PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60150011/pw/defectcontrollers/Tha60150011PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PwDefectController
    {
    tTha60150011PwDefectController super;
    }tTha60210031PwDefectController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60210031PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw);

ThaPwDefectController Tha60210031PwDefectControllerNew(AtPw pw);
ThaPwDefectController Tha60210031PwDefectControllerV2New(AtPw pw); /* Support M-bit defect. */

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PWDEFECTCONTROLLER_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210031ModulePwInternal.h
 * 
 * Created Date: Dec 16, 2015
 *
 * Description : 60210031 module PW interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEPWINTERNAL_H_
#define _THA60210031MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60150011/pw/Tha60150011ModulePwInternal.h"
#include "Tha60210031ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60210031ModulePwMethods
    {
    eBool (*MBitDefectIsSupported)(Tha60210031ModulePw self);
    uint32 (*StartVersionHas1344PwsPerSts24Slice)(Tha60210031ModulePw self);
    }tTha60210031ModulePwMethods;

typedef struct tTha60210031ModulePw
    {
    tTha60150011ModulePw super;
    const tTha60210031ModulePwMethods *methods;

    eBool queueDynamicAllocate;
    }tTha60210031ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60210031ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEPWINTERNAL_H_ */


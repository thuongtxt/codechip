/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha6021PwInterruptProcessor.h
 * 
 * Created Date: Oct 29, 2015
 *
 * Description : PW interrupt processor interface for 60210031 products
 *               (1344 PW PMC).
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PWINTERRUPTPROCESSOR_H_
#define _THA60210031PWINTERRUPTPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/interruptprocessor/ThaPwInterruptProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PwInterruptProcessor
    {
    tThaPwInterruptProcessor super;
    } tTha60210031PwInterruptProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwInterruptProcessor Tha60210031PwInterruptProcessorObjectInit(ThaPwInterruptProcessor self, ThaModulePw module);
ThaPwInterruptProcessor Tha60210031PwInterruptProcessorNew(ThaModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PWINTERRUPTPROCESSOR_H_ */


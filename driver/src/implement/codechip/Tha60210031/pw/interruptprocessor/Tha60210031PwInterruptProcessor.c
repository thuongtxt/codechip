/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6021PwInterruptProcessor.c
 *
 * Created Date: Oct 29, 2015
 *
 * Description : PW interrupt processor implementation for 60210031 products
 *               (1344 PW PMC).
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/pw/AtPwInternal.h"
#include "../../../../../generic/man/AtModuleInternal.h"
#include "../../pmc/Tha60210031PmcReg.h"
#include "Tha60210031PwInterruptProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cPw_slice_intr_Mask(sl)     ((cBit0) << (sl))
#define cPw_grp_intr_Mask(grp)      ((cBit0) << (grp))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwInterruptProcessorMethods m_ThaPwInterruptProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0x500000U;
    }

static void Process(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    AtModulePw module = (AtModulePw)ThaPwInterruptProcessorModuleGet(self);
    const uint32 baseAddress = mMethodsGet(self)->BaseAddress(self);
    static uint32 cNumberPwSlice = 2;
    uint32 intrSta = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_slice_intr_or_stat);
    uint32 slice;
    AtUnused(glbIntr);

    for (slice = 0; slice < cNumberPwSlice; slice++)
        {
        if (intrSta & cPw_slice_intr_Mask(slice))
            {
            uint32 intrMask = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(slice));
            uint32 intrStat = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_or_stat(slice));
            uint32 grpid;

            intrStat &= intrMask;

            for (grpid = 0; grpid < 32; grpid++)
                {
                if (intrStat & cPw_grp_intr_Mask(grpid))
                    {
                    uint32 intrStat32 = AtHalRead(hal, baseAddress + cAf6Reg_Counter_Interrupt_OR_Status(slice, grpid));
                    uint32 subid;

                    for (subid = 0; subid < 32; subid++)
                        {
                        if (intrStat32 & cPw_grp_intr_Mask(subid))
                            {
                            uint32 pwid = (slice << 10) + (grpid << 5) + subid;

                            /* Get PW channel */
                            AtPw pwChannel = AtModulePwGetPw(module, pwid);

                            /* Process channel interrupt. */
                            AtPwInterruptProcess(pwChannel, pwid, hal);
                            }
                        }
                    }
                }
            }
        }
    }

static uint32 CurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Current_Status_Base;
    }

static uint32 InterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base;
    }

static uint32 InterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base;
    }

static eAtRet Enable(ThaPwInterruptProcessor self, eBool enable)
    {
    ThaModulePw modulePw = ThaPwInterruptProcessorModuleGet(self);
    uint32 baseAddress = mMethodsGet(self)->BaseAddress(self);
    static uint32 cNumberPwSlice = 2;
    uint32 value = (enable) ? cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask : 0x0;
    uint32 slice;

    for (slice = 0; slice < cNumberPwSlice; slice++)
        mModuleHwWrite(modulePw, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(slice), value);

    return cAtOk;
    }

static void OverrideThaPwInterruptProcessor(ThaPwInterruptProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwInterruptProcessorOverride, mMethodsGet(self), sizeof(m_ThaPwInterruptProcessorOverride));

        mMethodOverride(m_ThaPwInterruptProcessorOverride, BaseAddress);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Process);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptMaskRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Enable);
        }

    mMethodsSet(self, &m_ThaPwInterruptProcessorOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PwInterruptProcessor);
    }

ThaPwInterruptProcessor Tha60210031PwInterruptProcessorObjectInit(ThaPwInterruptProcessor self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwInterruptProcessorObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    OverrideThaPwInterruptProcessor(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwInterruptProcessor Tha60210031PwInterruptProcessorNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwInterruptProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PwInterruptProcessorObjectInit(newProcessor, module);
    }

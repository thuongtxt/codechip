/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210031PwHeaderController.h
 * 
 * Created Date: Aug 28, 2015
 *
 * Description : PW header controller of 60210031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PWHEADERCONTROLLER_H_
#define _THA60210031PWHEADERCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController Tha60210031PwBackupHeaderControllerNew(ThaPwAdapter adapter);
ThaPwHeaderController Tha60210031PwHeaderControllerNew(ThaPwAdapter adapter);
uint8 Tha60210031PwHeaderControllerCachedQueueIdGet(ThaPwHeaderController self);
void Tha60210031PwHeaderControllerCachedQueueIdSet(ThaPwHeaderController self, uint8 queueId);
eAtRet Tha60210031PwHeaderControllerQueueHwSet(ThaPwHeaderController self, AtPw adapter, AtEthPort ethPort, uint8 queueId, uint32 pwBandwidth);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PWHEADERCONTROLLER_H_ */


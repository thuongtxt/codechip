/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210031PwBackupPsnController.c
 *
 * Created Date: Aug 28, 2015
 *
 * Description : Backup Psn Controller, this controller current is used in case of simulation only
 *               implementation need to be updated when HW support this feature
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/man/AtDriverInternal.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderControllerInternal.h"
#include "Tha60210031PwHeaderController.h"
#include "../../../Tha60210021/pw/headercontrollers/Tha60210021PwHeaderController.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDevice(self) AtChannelDeviceGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031PwBackupHeaderController
    {
    tTha60210021PwBackupHeaderController super;
    }tTha60210031PwBackupHeaderController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PwBackupHeaderController);
    }

static ThaPwHeaderController Tha60210031PwBackupHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021PwBackupHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60210031PwBackupHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PwBackupHeaderControllerObjectInit(newProvider, adapter);
    }


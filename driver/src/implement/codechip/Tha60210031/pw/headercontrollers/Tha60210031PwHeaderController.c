/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210031PwHeaderController.c
 *
 * Created Date: Oct 16, 2015
 *
 * Description : PW header controller of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../eth/Tha60210031EthPort.h"
#include "../../pwe/Tha60210031ModulePwe.h"
#include "../../man/Tha60210031Device.h"
#include "../Tha60210031ModulePw.h"
#include "Tha60210031PwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210031PwHeaderController *)self)
#define mAdapter(self) ((AtPw)ThaPwHeaderControllerAdapterGet(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tAtObjectMethods              *m_AtObjectMethods              = NULL;
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool QueueIdIsValid(uint8 queueId)
    {
    if (queueId > cMaxNumQsgmiiLanes)
        return cAtFalse;

    return cAtTrue;
    }

static AtDevice DeviceGet(ThaPwHeaderController self)
    {
    return AtChannelDeviceGet((AtChannel)ThaPwHeaderControllerAdapterGet(self));
    }

static ThaModulePwe ModulePwe(ThaPwHeaderController self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(DeviceGet(self), cThaModulePwe);
    }

static ThaModuleCos ModuleCos(ThaPwHeaderController self)
    {
    return (ThaModuleCos)AtDeviceModuleGet(DeviceGet(self), cThaModuleCos);
    }

static AtModulePw ModulePw(AtPw adapter)
    {
    return (AtModulePw)AtChannelModuleGet((AtChannel)adapter);
    }

static AtEthPort EthernetPort(ThaPwHeaderController self)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)ThaPwHeaderControllerAdapterGet(self)), cAtModuleEth);
    return AtModuleEthPortGet(moduleEth, 0);
    }

static eBool FpgaVersionSwHandleQsgmiiLanes(AtEthPort port)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)port);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(device))
        return cAtTrue;

    return cAtFalse;
    }

static eBool MultipleLanesIsSupported(AtEthPort port)
    {
    if (!Tha60210031EthPortIsQsgmii(port))
        return cAtFalse;

    return FpgaVersionSwHandleQsgmiiLanes(port);
    }

/* This product support only one port, so it is not necessary to wait until PW is bound Ethernet port
 * to configure it's queue */
static eAtRet EthPortQueueSet(ThaPwHeaderController self, uint8 queueId)
    {
    AtPw adapter = mAdapter(self);
    AtEthPort ethPort = EthernetPort(self);
    uint32 pwBandWidth;
    eAtRet ret = cAtOk;
    uint8 currentQueue;

    if (!QueueIdIsValid(queueId))
        return cAtErrorInvlParm;

    /* Just can set queue when PW has been bound circuit */
    if ((!MultipleLanesIsSupported(ethPort)) || (AtPwBoundCircuitGet(adapter) == NULL))
        return cAtErrorModeNotSupport;

    currentQueue = Tha60210031PwHeaderControllerCachedQueueIdGet(self);
    if (queueId == currentQueue)
        {
        /* User explicitly configure queue, turn off dynamic allocation */
        Tha60210031ModulePwQueueDynamicAllocateEnable(ModulePw(adapter), cAtFalse);
        return cAtOk;
        }

    pwBandWidth = (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(adapter);
    if (!Tha60210031EthPortQueueBandwidthIsAvailable(ethPort, queueId, pwBandWidth))
        return cAtErrorBandwidthExceeded;

    /* Remove bandwidth of current queue */
    if (currentQueue != cAtInvalidPwEthPortQueueId)
        ret |= Tha60210031EthPortQueuePwRemove(ethPort, currentQueue, adapter);

    ret |= Tha60210031PwHeaderControllerQueueHwSet(self, adapter, ethPort, queueId, pwBandWidth);
    if (ret != cAtOk)
        return ret;

    /* User explicitly configure queue, turn off dynamic allocation */
    Tha60210031ModulePwQueueDynamicAllocateEnable(ModulePw(adapter), cAtFalse);
    return cAtOk;
    }

static uint8 EthPortQueueGet(ThaPwHeaderController self)
    {
    AtPw adapter = mAdapter(self);
    AtEthPort ethPort = EthernetPort(self);

    if (!Tha60210031EthPortIsQsgmii(ethPort))
        return cAtInvalidPwEthPortQueueId;

    return Tha60210031ModulePwePwEthPortQueueGet(ModulePwe(self), adapter);
    }

static uint8 EthPortMaxNumQueues(ThaPwHeaderController self)
    {
    AtEthPort ethPort = EthernetPort(self);
    return Tha60210031EthPortIsQsgmii(ethPort) ? cMaxNumQsgmiiLanes : 0;
    }

static eAtModulePwRet HeaderLengthSet(ThaPwHeaderController self, uint8 hdrLenInByte)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return ThaModuleCosPwHeaderLengthSet(ModuleCos(self), pw, hdrLenInByte);
    }

static uint8 HeaderLengthGet(ThaPwHeaderController self)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return ThaModuleCosPwHeaderLengthGet(ModuleCos(self), pw);
    }

static eAtModulePwRet HeaderRead(ThaPwHeaderController self, uint8 *buffer, uint8 headerLengthInByte)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return ThaModulePweHeaderRead(ModulePwe(self), pw, buffer, headerLengthInByte);
    }

static eAtModulePwRet HeaderWrite(ThaPwHeaderController self, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode)
    {
    AtPw pw = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    AtUnused(writeMode);
    return ThaModulePweHeaderWrite(ModulePwe(self), pw, buffer, headerLenInByte, psn);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210031PwHeaderController * object = (tTha60210031PwHeaderController *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(queueId);
    }

static void OverrideAtObject(ThaPwHeaderController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, EthPortQueueSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, EthPortQueueGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, EthPortMaxNumQueues);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderLengthGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderRead);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderWrite);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideAtObject(self);
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PwHeaderController);
    }

ThaPwHeaderController Tha60210031PwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->queueId = cAtInvalidPwEthPortQueueId;

    return self;
    }

ThaPwHeaderController Tha60210031PwHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PwHeaderControllerObjectInit(newProvider, adapter);
    }

uint8 Tha60210031PwHeaderControllerCachedQueueIdGet(ThaPwHeaderController self)
    {
    return mThis(self)->queueId;
    }

void Tha60210031PwHeaderControllerCachedQueueIdSet(ThaPwHeaderController self, uint8 queueId)
    {
    mThis(self)->queueId = queueId;
    }

eAtRet Tha60210031PwHeaderControllerQueueHwSet(ThaPwHeaderController self, AtPw adapter, AtEthPort ethPort, uint8 queueId, uint32 newPwBandwidth)
    {
    eAtRet ret = Tha60210031ModulePwePwEthPortQueueSet(ModulePwe(self), adapter, queueId);
    ret |= Tha60210031EthPortQueuePwAdd(ethPort, queueId, adapter, newPwBandwidth);

    if (ret != cAtOk)
        {
        Tha60210031PwHeaderControllerCachedQueueIdSet(self, cAtInvalidPwEthPortQueueId);
        return ret;
        }

    Tha60210031PwHeaderControllerCachedQueueIdSet(self, queueId);

    /* It's time to enable PW */
    return AtChannelEnable((AtChannel)adapter, ThaPwAdapterIsEnabled((ThaPwAdapter)adapter));
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210031PwHeaderControllerInternal.h
 * 
 * Created Date: Oct 30, 2016
 *
 * Description : Header controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PWHEADERCONTROLLERINTERNAL_H_
#define _THA60210031PWHEADERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderControllerInternal.h"
#include "Tha60210031PwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PwHeaderController
    {
    tThaPwHeaderController super;
    uint8 queueId;
    }tTha60210031PwHeaderController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController Tha60210031PwHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PWHEADERCONTROLLERINTERNAL_H_ */


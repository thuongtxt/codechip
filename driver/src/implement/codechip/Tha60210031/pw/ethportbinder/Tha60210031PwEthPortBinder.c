/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210031PwEthPortBinder.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : PW Ethernet port binder of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../../../default/pw/ethportbinder/ThaPwEthPortBinderInternal.h"
#include "../../man/Tha60210031Device.h"
#include "../../eth/Tha60210031EthPort.h"
#include "../../pwe/Tha60210031ModulePwe.h"
#include "../headercontrollers/Tha60210031PwHeaderController.h"
#include "../Tha60210031ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031PwEthPortBinder
    {
    tThaPwEthPortBinder super;
    }tTha60210031PwEthPortBinder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwEthPortBinderMethods m_ThaPwEthPortBinderOverride;

/* Save super implementation */
static const tThaPwEthPortBinderMethods *m_ThaPwEthPortBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FpgaVersionSwHandleQsgmiiLanes(AtEthPort self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtChannelModuleGet((AtChannel)self));

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210031DeviceStartVersionSwHandleQsgmiiLanes(device))
        return cAtTrue;

    return cAtFalse;
    }

static eBool MultipleLanesIsSupported(AtEthPort port)
    {
    ThaModuleEth eth = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    if (ThaModuleEthNeedMultipleLanes(eth)== cAtFalse)
        return cAtFalse;

    if (!Tha60210031EthPortIsQsgmii(port))
        return cAtFalse;

    return FpgaVersionSwHandleQsgmiiLanes(port);
    }

static AtModulePw ModulePw(AtPw adapter)
    {
    return (AtModulePw)AtChannelModuleGet((AtChannel)adapter);
    }

static AtEthPort EthernetPort(AtPw adapter)
    {
    return AtPwEthPortGet(adapter);
    }

static eBool QueueIsValid(uint8 queueId)
    {
    return (queueId != cAtInvalidPwEthPortQueueId) ? cAtTrue : cAtFalse;
    }

static eBool PwBandwidthIsAcceptable(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth)
    {
    AtEthPort ethPort;
    uint8 queueId;
    ThaPwHeaderController headerController;
    uint32 currentBw;

    if (!m_ThaPwEthPortBinderMethods->PwBandwidthIsAcceptable(self, pw, newPwBandwidth))
        return cAtFalse;

    ethPort = EthernetPort(pw);
    if ((ethPort == NULL) || !MultipleLanesIsSupported(ethPort))
        return cAtTrue;

    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pw);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);
    currentBw = (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(pw);

    if (Tha60210031EthPortQueueBandwidthCanAdjust(ethPort, queueId, currentBw, (uint32)newPwBandwidth))
        return cAtTrue;

    /* Queue was select by user */
    if (QueueIsValid(queueId) && !Tha60210031ModulePwQueueIsDynamicAllocated(ModulePw(pw)))
        return cAtFalse;

    /* Is there another queue accepts this bandwidth */
    queueId = Tha60210031EthPortQueueIdDynamicAllocate(ethPort, (uint32)newPwBandwidth);
    return (QueueIsValid(queueId) ? cAtTrue : cAtFalse);
    }

static eAtRet PwBandwidthUpdate(ThaPwEthPortBinder self, AtPw pw, uint64 newPwBandwidth)
    {
    AtEthPort ethPort;
    uint8 queueId;
    ThaPwHeaderController headerController;
    uint32 currentBw = (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(pw);
    eAtRet ret;

    if (currentBw == newPwBandwidth)
        return cAtOk;

    ethPort = EthernetPort(pw);
    if ((ethPort == NULL) || !MultipleLanesIsSupported(ethPort))
        return m_ThaPwEthPortBinderMethods->PwBandwidthUpdate(self, pw, newPwBandwidth);

    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pw);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);

    if (Tha60210031EthPortQueueBandwidthCanAdjust(ethPort, queueId, currentBw, (uint32)newPwBandwidth))
        ret = Tha60210031EthPortQueueBandwidthAdjust(ethPort, queueId, currentBw, (uint32)newPwBandwidth);
    else
        {
        Tha60210031EthPortQueuePwRemove(ethPort, queueId, pw);
        queueId = Tha60210031EthPortQueueIdDynamicAllocate(ethPort, (uint32)newPwBandwidth);
        ret = Tha60210031PwHeaderControllerQueueHwSet(headerController, pw, ethPort, queueId, (uint32)newPwBandwidth);
        }

    /* Super job should be done at last */
    ret |= m_ThaPwEthPortBinderMethods->PwBandwidthUpdate(self, pw, newPwBandwidth);
    return ret;
    }

static eAtRet PwEthPortBind(ThaPwEthPortBinder self, AtPw adapter, AtEthPort ethPort)
    {
    uint8 queueId;
    uint32 pwBandWidth, currentBw;
    ThaPwHeaderController headerController;
    eAtRet ret;

    Tha60210031EthPortBoundPwAdd(ethPort, adapter);
    currentBw = (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(adapter);
    ret = m_ThaPwEthPortBinderMethods->PwEthPortBind(self, adapter, ethPort);
    if (ret != cAtOk)
        return ret;

    if (!MultipleLanesIsSupported(ethPort))
        return cAtOk;

    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)adapter);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);
    pwBandWidth = (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(adapter);

    /* Queue has already been set or static queue allocation */
    if (QueueIsValid(queueId))
        return Tha60210031EthPortQueueBandwidthAdjust(ethPort, queueId, currentBw, pwBandWidth);

    if (!Tha60210031ModulePwQueueIsDynamicAllocated(ModulePw(adapter)))
        return cAtOk;

    /* Dynamic allocate queue */
    queueId = Tha60210031EthPortQueueIdDynamicAllocate(ethPort, pwBandWidth);
    if (!QueueIsValid(queueId))
        return cAtErrorBandwidthExceeded;

    return Tha60210031PwHeaderControllerQueueHwSet(headerController, adapter, ethPort, queueId, pwBandWidth);
    }

static eAtRet PwEthPortUnBind(ThaPwEthPortBinder self, AtPw adapter, AtEthPort ethPort)
    {
    uint8 queueId;
    ThaPwHeaderController headerController;
    eAtRet ret;

    Tha60210031EthPortBoundPwRemove(ethPort, adapter);
    ret = m_ThaPwEthPortBinderMethods->PwEthPortUnBind(self, adapter, ethPort);
    if (ret != cAtOk)
        return ret;

    if (!MultipleLanesIsSupported(ethPort))
        return ret;

    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)adapter);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);
    if (!QueueIsValid(queueId) || !Tha60210031ModulePwQueueIsDynamicAllocated(ModulePw(adapter)))
        return cAtOk;

    ret = Tha60210031EthPortQueuePwRemove(ethPort, queueId, adapter);
    Tha60210031PwHeaderControllerCachedQueueIdSet(headerController, cAtInvalidPwEthPortQueueId);

    return ret;
    }

static eAtRet PwEnable(ThaPwEthPortBinder self, AtPw pw, eBool enable)
    {
    uint8 queueId;
    ThaPwHeaderController headerController;
    AtEthPort ethPort = EthernetPort(pw);

    if ((ethPort == NULL) || !MultipleLanesIsSupported(ethPort))
        return m_ThaPwEthPortBinderMethods->PwEnable(self, pw, enable);

    /* PW is only enabled when it is set queue */
    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pw);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);

    if (QueueIsValid(queueId))
        return m_ThaPwEthPortBinderMethods->PwEnable(self, pw, enable);

    return cAtOk;
    }

static eBool PwCanBindEthPort(ThaPwEthPortBinder self, AtPw pw, AtEthPort ethPort, uint64 bwWithPort)
    {
    uint8 queueId;
    ThaPwHeaderController headerController;

    if (!m_ThaPwEthPortBinderMethods->PwCanBindEthPort(self, pw, ethPort, bwWithPort))
        return cAtFalse;

    if (!MultipleLanesIsSupported(ethPort))
        return cAtTrue;

    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pw);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);

    /* Queue bandwidth has been checked when queue is set */
    if (QueueIsValid(queueId))
        {
        uint32 curBw = (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(pw);
        return Tha60210031EthPortQueueBandwidthCanAdjust(ethPort, queueId, curBw, (uint32)bwWithPort);
        }

    /* Dynamic allocate queue */
    queueId = Tha60210031EthPortQueueIdDynamicAllocate(ethPort, (uint32)bwWithPort);
    if (QueueIsValid(queueId))
        return cAtTrue;

    return cAtFalse;
    }

static eBool NeedToManageEthPortBandwidth(ThaPwEthPortBinder self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint64 PwRemainingBw(ThaPwEthPortBinder self, AtPw pw)
    {
    uint8 queueId;
    ThaPwHeaderController headerController;
    AtEthPort ethPort = EthernetPort(pw);

    if ((ethPort == NULL) || !MultipleLanesIsSupported(ethPort))
        return m_ThaPwEthPortBinderMethods->PwRemainingBw(self, pw);

    headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pw);
    queueId = Tha60210031PwHeaderControllerCachedQueueIdGet(headerController);

    /* User select queue, return on that selected queue */
    if (QueueIsValid(queueId) && !Tha60210031ModulePwQueueIsDynamicAllocated(ModulePw(pw)))
        return Tha60210031EthPortQueuePwRemainingBandwidthInBpsGet(ethPort, queueId, (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(pw));

    /* Dynamic allocate, return max bandwidth of all queues */
    return Tha60210031EthPortQueuesMaxRemainingBandwidthInBpsGet(ethPort, queueId, (uint32)ThaPwAdapterCurrentPwBandwidthInBpsGet(pw));
    }

static void OverrideThaPwEthPortBinder(ThaPwEthPortBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwEthPortBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwEthPortBinderOverride, m_ThaPwEthPortBinderMethods, sizeof(m_ThaPwEthPortBinderOverride));

        mMethodOverride(m_ThaPwEthPortBinderOverride, PwBandwidthIsAcceptable);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwBandwidthUpdate);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwEthPortBind);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwEthPortUnBind);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwEnable);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwCanBindEthPort);
        mMethodOverride(m_ThaPwEthPortBinderOverride, NeedToManageEthPortBandwidth);
        mMethodOverride(m_ThaPwEthPortBinderOverride, PwRemainingBw);
        }

    mMethodsSet(self, &m_ThaPwEthPortBinderOverride);
    }

static void Override(ThaPwEthPortBinder self)
    {
    OverrideThaPwEthPortBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PwEthPortBinder);
    }

static ThaPwEthPortBinder ObjectInit(ThaPwEthPortBinder self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwEthPortBinderObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwEthPortBinder Tha60210031PwEthPortBinderNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwEthPortBinder newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newActivator, module);
    }

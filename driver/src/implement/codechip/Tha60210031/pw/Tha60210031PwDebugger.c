/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210031PwDebugger.c
 *
 * Created Date: Sep 4, 2015
 *
 * Description : PW debugger of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/debugger/ThaPwDebuggerInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "Tha60210031PwDebugger.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPlaPrbsStatus      0x330000
#define cThaPwePrbsStatus      0x331000
#define cThaClaPrbsStatus      0x442000
#define cThaPdaPrbsStatus      0x248000
#define cThaEncPrbsStatus      0x247800

#define cPwDebugGlobalBusSelectReg   0x101
#define cPwDebugGlobalBusSelectMask  cBit21
#define cPwDebugGlobalBusSelectShift 21
#define cPwDebugIdSelect  0xC0000
#define cPwSpeedMap2Pla   0xC0001
#define cPwSpeedPla2Pwe   0xC0002
#define cPwSpeedRxEth     0xC0003
#define cPwSpeedCla2Pda   0xC0004
#define cPwSpeedMap2Pda   0xC0005
#define cPwPktNum         0xC0006
#define cPwBlkNum         0xC0007

#define cPwPweStateLsb    0xC0008
#define cPwPweStateMsb    0xC0009

#define cPweblknumMask    cBit11_0
#define cPweblknumShift   0
#define cPwecanumMask     cBit23_12
#define cPwecanumShift    12
#define cPwecabitMask     cBit31_24
#define cPwecabitShift    24

/* Dword 1 */
#define cPlafifolenMask   cBit11_0
#define cPlafifolenShift  0
#define cPwefifolenMask   cBit23_12
#define cPwefifolenShift  12
#define cPwereqlenMask    cBit27_24
#define cPwereqlenShift   24
#define cPwegetlenMask    cBit31_28
#define cPwegetlenShift   28

#define cPwPdaState                0xC000A
#define cPwPdaStatewrca_numcaMask  cBit27_24
#define cPwPdaStatewrca_numcaShift 24
#define cPwPdaStatetdmcanumMask    cBit23_16
#define cPwPdaStatetdmcanumShift   16
#define cPwPdaStateinitreqMask     cBit15
#define cPwPdaStateinitreqShift    15
#define cPwPdaStateinitreqjbMask   cBit14
#define cPwPdaStateinitreqjbShift  14
#define cPwPdaStaterxclstypeMask   cBit13
#define cPwPdaStaterxclstypeShift  13
#define cPwPdaStaterxclspwidMask   cBit12_3
#define cPwPdaStaterxclspwidShift  3
#define cPwPdaStaterxclserrMask    cBit2
#define cPwPdaStaterxclserrShift   2
#define cPwPdaStaterxclseopMask    cBit1
#define cPwPdaStaterxclseopShift   1
#define cPwPdaStaterxclsvldMask    cBit0
#define cPwPdaStaterxclsvldShift   0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210031PwDebugger
    {
    tThaPwDebugger super;
    }tTha60210031PwDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwDebuggerMethods m_ThaPwDebuggerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwPrbsCheckDefaultOffset(ThaPwDebugger self, AtPw pwAdapter)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pwAdapter);
    }

static eBool PrbsIsRunning(ThaPwDebugger self, uint32 statusReg, AtPw pwAdapter, uint32* firstTimeVal, uint32* lastTimeVal)
    {
    const uint32 cDelayTimeBetweenTwoCheckingInMs = 1;
    uint32 cTimeoutMs = 128;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTimeMs = 0;
    uint32 firstTime, secondTime;
    uint32 address, runningMask;

    uint32 offset = mMethodsGet(self)->PwPrbsCheckDefaultOffset(self, pwAdapter);
    if (offset == cBit31_0)
        return cAtFalse;

    address = statusReg + offset;
    runningMask = mMethodsGet(self)->RegFieldIsRunningMask(self);
    *lastTimeVal = mChannelHwRead(pwAdapter, address, cAtModulePw);
    *firstTimeVal = *lastTimeVal;
    firstTime = *lastTimeVal & runningMask;

    if (AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)pwAdapter)))
        cTimeoutMs = 1;
    AtOsalCurTimeGet(&startTime);

    while (elapseTimeMs < cTimeoutMs)
        {
        /* Engine may be running */
        AtOsalUSleep(cDelayTimeBetweenTwoCheckingInMs * 1000);
        *lastTimeVal = mChannelHwRead(pwAdapter, address, cAtModulePw);
        secondTime =  *lastTimeVal & runningMask;
        if (firstTime != secondTime)
            return cAtTrue;

        /* Timeout waiting to confirm it it really stops */
        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static uint32 PrbsCheckAtPwe(ThaPwDebugger self, AtPw pwAdapter)
    {
    uint32 ret = 0;

    ret |= Tha60210031PwPrbsCheck(self, cThaPlaPrbsStatus, pwAdapter, cThaPwPrbsPlaError, cThaPwPrbsPlaNotSync);
    ret |= Tha60210031PwPrbsCheck(self, cThaPwePrbsStatus, pwAdapter, cThaPwPrbsPweError, cThaPwPrbsPweNotSync);

    return ret;
    }

static uint32 PrbsCheckAtCla(ThaPwDebugger self, AtPw pwAdapter)
    {
    return Tha60210031PwPrbsCheck(self, cThaClaPrbsStatus, pwAdapter, cThaPwPrbsClaError, cThaPwPrbsClaNotSync);
    }

static uint32 PrbsCheckAtPda(ThaPwDebugger self, AtPw pwAdapter)
    {
    return Tha60210031PwPrbsCheck(self, cThaPdaPrbsStatus, pwAdapter, cThaPwPrbsPdaError, cThaPwPrbsPdaNotSync);
    }

static uint8 PwToBus(AtPw pw)
	{
	uint8  slice = 0, hwIdInSlice = 0;
	AtChannel circuit = AtPwBoundCircuitGet(pw);
	eAtPwType pwType = AtPwTypeGet(pw);

	if (circuit == NULL)
		return 0;

	if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
		ThaPdhChannelHwIdGet((AtPdhChannel)circuit, cAtModulePdh, &slice, &hwIdInSlice);
	else if (pwType == cAtPwTypeCEP)
		ThaSdhChannel2HwMasterStsId((AtSdhChannel)circuit, cAtModuleSdh, &slice, &hwIdInSlice);

	return slice;
	}

static void PwIdSelect(ThaPwDebugger self, AtPw pw)
    {
	uint32 reg, val;
	uint8 bus = PwToBus(pw);

	AtUnused(self);
	reg = cPwDebugGlobalBusSelectReg;
    val = mChannelHwRead(pw, reg, cAtModulePw);
    mFieldIns(&val, cPwDebugGlobalBusSelectMask, cPwDebugGlobalBusSelectShift, bus);
    mChannelHwWrite(pw, reg, val, cAtModulePw);

    mChannelHwWrite(pw, cPwDebugIdSelect, AtChannelIdGet((AtChannel)pw), cAtModulePw);
    }

static void PwSpeedPrint(AtPw pw, const char* title, uint32 reg)
    {
    uint32 regVal = mChannelHwRead(pw, reg, cAtModulePw);
    AtPrintc(cSevNormal, "    %-25s: %u (0x%08x = 0x%08x)\r\n", title, regVal, reg, regVal);
    }

static void PdaStatePrint(AtPw pw)
    {
    uint32 regVal = mChannelHwRead(pw, cPwPdaState, cThaModulePda);
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStatewrca_numca", mRegField(regVal, cPwPdaStatewrca_numca));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStatetdmcanum", mRegField(regVal, cPwPdaStatetdmcanum));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStateinitreqMask",   mRegField(regVal, cPwPdaStateinitreq));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStateinitreqjbMask", mRegField(regVal, cPwPdaStateinitreqjb));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStaterxclspwidMask", mRegField(regVal, cPwPdaStaterxclspwid));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStaterxclserrMask",  mRegField(regVal, cPwPdaStaterxclserr));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStaterxclseopMask",  mRegField(regVal, cPwPdaStaterxclseop));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "PwPdaStaterxclsvldMask",  mRegField(regVal, cPwPdaStaterxclsvld));
    }

static void PweStatePrint(AtPw pw)
    {
    uint32 regVal = mChannelHwRead(pw, cPwPweStateLsb, cThaModulePwe);

    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Pweblknum",  mRegField(regVal, cPweblknum));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Pwecanum ", mRegField(regVal, cPwecanum));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Pwecabit ", mRegField(regVal, cPwecabit));

    regVal = mChannelHwRead(pw, cPwPweStateMsb, cThaModulePwe);
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Plafifolen",  mRegField(regVal, cPlafifolen));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Pwefifolen", mRegField(regVal, cPwefifolen));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Pwereqlen", mRegField(regVal, cPwereqlen));
    AtPrintc(cSevNormal, "    %-25s: %u\r\n", "Pwegetlen", mRegField(regVal, cPwegetlen));
    }

static void PwSpeedHwClear(AtPw pw)
    {
    mChannelHwRead(pw, cPwSpeedMap2Pla, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedPla2Pwe, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedRxEth, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedCla2Pda, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedMap2Pda, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedMap2Pda, cAtModulePw);
    mChannelHwRead(pw, cPwPktNum, cAtModulePw);
    mChannelHwRead(pw, cPwBlkNum, cAtModulePw);
    }

static void PwSpeedShow(ThaPwDebugger self, AtPw pw)
    {
    const uint32 cDelayTimeBeforeReadPwSpeedInMs = 1;

    PwIdSelect(self, pw);
    AtOsalSleep(1);
    AtPrintc(cSevInfo, "* Pseudowire %u speed:\r\n", AtChannelIdGet((AtChannel)pw) + 1);

    /* As hardware recommend, need do some actions:
     * - Read first times to clear engine
     * - Delay 1s for hardware update engine
     * - Read to show the real values */
    PwSpeedHwClear(pw);
    AtOsalUSleep(cDelayTimeBeforeReadPwSpeedInMs * 1000);

    PwSpeedPrint(pw, "MAP to PLA" , cPwSpeedMap2Pla);
    PwSpeedPrint(pw, "PLA to PWE" , cPwSpeedPla2Pwe);
    PwSpeedPrint(pw, "Rx Ethernet", cPwSpeedRxEth);
    PwSpeedPrint(pw, "CLA to PDA" , cPwSpeedCla2Pda);
    PwSpeedPrint(pw, "MAP to PDA" , cPwSpeedMap2Pda);
    PwSpeedPrint(pw, "Num packet" , cPwPktNum);
    PwSpeedPrint(pw, "Num block"  , cPwBlkNum);
    PweStatePrint(pw);
    PdaStatePrint(pw);
    }

static uint32 PrbsCheckAtEnc(ThaPwDebugger self, AtPw pwAdapter)
    {
    return Tha60210031PwPrbsCheck(self, cThaEncPrbsStatus, pwAdapter, cThaPwPrbsEncError, cThaPwPrbsEncNotSync);
    }

static void PwCepDebug(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* No information so far */
    }

static void OverrideThaPwDebugger(ThaPwDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDebuggerOverride, mMethodsGet(self), sizeof(m_ThaPwDebuggerOverride));
        mMethodOverride(m_ThaPwDebuggerOverride, PrbsCheckAtPwe);
        mMethodOverride(m_ThaPwDebuggerOverride, PrbsCheckAtCla);
        mMethodOverride(m_ThaPwDebuggerOverride, PrbsCheckAtPda);
        mMethodOverride(m_ThaPwDebuggerOverride, PrbsCheckAtEnc);
        mMethodOverride(m_ThaPwDebuggerOverride, PwSpeedShow);
        mMethodOverride(m_ThaPwDebuggerOverride, PwPrbsCheckDefaultOffset);
        mMethodOverride(m_ThaPwDebuggerOverride, PwCepDebug);
        }

    mMethodsSet(self, &m_ThaPwDebuggerOverride);
    }

static void Override(ThaPwDebugger self)
    {
    OverrideThaPwDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PwDebugger);
    }

ThaPwDebugger Tha60210031PwDebuggerObjectInit(ThaPwDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDebugger Tha60210031PwDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return Tha60210031PwDebuggerObjectInit(newChecker);
    }

uint32 Tha60210031PwPrbsCheck(ThaPwDebugger self, uint32 statusReg, AtPw pwAdapter, uint32 error, uint32 notSync)
    {
    uint32 firstTimeVal, lastTimeVal;
    uint32 ret = 0, errorMask, syncMask;

    errorMask = mMethodsGet(self)->RegFieldErrorMask(self);
    syncMask  = mMethodsGet(self)->RegFieldSyncMask(self);

    if (!PrbsIsRunning(self, statusReg, pwAdapter, &firstTimeVal, &lastTimeVal))
        return (error | notSync);

    if (firstTimeVal & errorMask)
        ret |= error;

    if ((lastTimeVal & syncMask) == 0)
        ret |= notSync;

    return ret;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS module name
 * 
 * File        : Tha60210031ModulePrbs.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : 60210031 Module header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031MODULEPDH_H_
#define _THA60210031MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaModulePdhInternal.h"
#include "../../Tha60150011/pdh/Tha60150011ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031ModulePdh* Tha60210031ModulePdh;
typedef struct tTha60210031ModulePdhMethods
    {
    eBool (*HasLiu)(Tha60210031ModulePdh self);

    uint32 (*MdlTxCtrlReg)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*MdlRxCfgReg)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*MdlRxBufBaseAddress)(Tha60210031ModulePdh self);
    uint32 (*MdlRxBufWordOffset)(Tha60210031ModulePdh self);
    uint32 (*MdlRxStickyBaseAddress)(Tha60210031ModulePdh self, uint32 bufferId);
    uint32 (*MdlRxStickyOffset)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*MdlRxStickyMask)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*RxMdlIdFromChannelGet)(Tha60210031ModulePdh self, AtPdhDe3 de3);

    /* For MDL counters Override */
    uint32 (*MdlTxCounterOffset)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*MdlRxCounterOffset)(Tha60210031ModulePdh self, AtPdhDe3 de3);


    uint32 (*MdlTxPktOffset)(Tha60210031ModulePdh self, uint32 type, eBool r2c);
    uint32 (*MdlTxByteOffset)(Tha60210031ModulePdh self, uint32 type, eBool r2c);
    uint32 (*MdlRxPktOffset)(Tha60210031ModulePdh self, uint32 type, eBool r2c);
    uint32 (*MdlRxByteOffset)(Tha60210031ModulePdh self, uint32 type, eBool r2c);
    uint32 (*MdlRxDropOffset)(Tha60210031ModulePdh self, uint32 type, eBool r2c);

    uint32 (*StartVersionSupportMdlFilterMessageByType)(ThaModulePdh self);
    uint32 (*StartHardwareVersionSupportsDataLinkFcsLsbBitOrder)(ThaModulePdh self);
    eBool (*DatalinkFcsBitOrderIsSupported)(ThaModulePdh self);

    eBool (*Ec1DiagLiuModeShouldEnable)(Tha60210031ModulePdh self);
    uint32 (*SerialIdFromHwIdGet)(Tha60210031ModulePdh self, uint8 sliceId, uint8 de3InSlice);
    eBool (*Ec1LiuRxClockInvertionShouldEnable)(Tha60210031ModulePdh self);

    uint32 (*TxBufferBaseGet)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*TxBufferDwordOffset)(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice);
    uint32 (*RxDe3OhThrshCfgBase)(Tha60210031ModulePdh self);
    uint32 (*UpenMdlBuffer1Base)(Tha60210031ModulePdh self, uint8 hwStsId);
    uint32 (*UpenMdlBuffer1Offset)(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice);
    uint32 (*MdlSliceOffset)(Tha60210031ModulePdh self, uint8 sliceId);
    uint32 (*MdlTxInsCtrlBase)(Tha60210031ModulePdh self);
    uint32 (*UpenCfgMdlBase)(Tha60210031ModulePdh self);
    uint32 (*UpenRxmdlCfgtypeBase)(Tha60210031ModulePdh self);
    uint32 (*UpenRxmdlCfgtypeOffset)(Tha60210031ModulePdh self, AtPdhDe3 de3);
    uint32 (*UpenDestuffCtrl0Base)(Tha60210031ModulePdh self);
    uint32 (*UpenCfgCtrl0Base)(Tha60210031ModulePdh self);
    uint32 (*MdlControllerSliceOffset)(Tha60210031ModulePdh self, uint8 sliceId);
    uint32 (*UpenPrmTxcfgcrBase)(Tha60210031ModulePdh self);
    uint32 (*UpenPrmTxcfglbBase)(Tha60210031ModulePdh self);
    uint32 (*LBOffset)(Tha60210031ModulePdh self, ThaPdhDe1 de1);
    uint32 (*UpenRxprmCfgstdBase)(Tha60210031ModulePdh self);
    }tTha60210031ModulePdhMethods;

typedef struct tTha60210031ModulePdh
    {
    tTha60150011ModulePdh super;
    const tTha60210031ModulePdhMethods * methods;

    /* For MDL Protection */
    AtOsalMutex mdlMutex;
    AtErrorGenerator de3TxErrorGenerator;
    AtErrorGenerator de3RxErrorGenerator;
    AtErrorGenerator de1TxErrorGenerator;
    AtErrorGenerator de1RxErrorGenerator;
    }tTha60210031ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60210031ModulePdhObjectInit(AtModulePdh self, AtDevice device);
eBool Tha60210031ModulePdhHasLiu(ThaModulePdh self);
uint32 Tha60210031RxMdlIdFromChannelGet(Tha60210031ModulePdh self, AtPdhDe3 de3);

/* Utils to control PDH over SONET Over CEP mode */
eAtRet Tha60210031PdhStsDe3LiuOverVc3CepSet(AtModulePdh self, AtSdhChannel sdhVc);
uint32 Tha60210031ModulePdhStartVersionSupportMdlFilterMessageByType(ThaModulePdh self);

/* For MDL */
void Tha60210031ModulePdhMdlLock(AtModulePdh self);
void Tha60210031ModulePdhMdlUnLock(AtModulePdh self);
eBool Tha60210031ModulePdhDatalinkFcsBitOrderIsSupported(ThaModulePdh self);
eBool Tha60210031ModulePdhEc1DiagLiuModeShouldEnable(Tha60210031ModulePdh self);
eBool Tha60210031ModulePdhEc1LiuRxClockInvertionShouldEnable(Tha60210031ModulePdh self);

/* For PRM */
eAtModulePdhRet Tha60210031ModulePdhPrmCrCompareEnable(AtModulePdh self, eBool enable);
eBool Tha60210031ModulePdhPrmCrCompareIsEnabled(AtModulePdh self);
eAtModulePdhRet Tha60210031ModulePdhPrmDefaultSet(ThaModulePdh self);

/* Registers */
uint32 Tha60210031ModulePdhRxDe3OhThrshCfgAddress(Tha60210031ModulePdh self);
uint32 Tha60210031ModulePdhMdlSliceOffset(Tha60210031ModulePdh self, uint8 sliceId);
uint32 Tha60210031ModulePdhMdlTxInsCtrlBase(Tha60210031ModulePdh self);
uint32 Tha60210031ModulePdhMdlControllerSliceOffset(Tha60210031ModulePdh self, uint8 sliceId);
uint32 Tha60210031ModulePdhUpenPrmTxcfgcrBase(Tha60210031ModulePdh self);
uint32 Tha60210031ModulePdhUpenPrmTxcfglbBase(Tha60210031ModulePdh self);
uint32 Tha60210031ModulePdhLBOffset(Tha60210031ModulePdh self, ThaPdhDe1 de1);
uint32 Tha60210031ModulePdhUpenRxprmCfgstdBase(Tha60210031ModulePdh self);

/* Channel from serial interface */
AtPdhDe3 Tha6021ModulePdhDe3FromSerialIdGet(ThaModulePdh self, uint32 serialId);
AtPdhDe1 Tha6021ModulePdhDe1FromSerialIdGet(ThaModulePdh self, uint32 serialId, uint8 de2Id, uint8 de1Id);
uint32 Tha6021ModulePdhSerialIdFromHwIdGet(ThaModulePdh self, uint8 sliceId, uint8 de3InSlice);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031MODULEPDH_H_ */


/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhDe1.h
 *
 * Created Date: Aug 5, 2015
 *
 * Description : PDH DS1/E1 header.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHDE3INTERNAL_H_
#define _THA60210031PDHDE3INTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe3Internal.h"
#include "Tha60210031PdhDe3.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PdhDe3Methods
    {
    eBool (*BpvCounterIsSupported)(Tha60210031PdhDe3 self);
    eBool (*TxFramerSsmIsSupported)(Tha60210031PdhDe3 self);
    }tTha60210031PdhDe3Methods;

typedef struct tTha60210031PdhDe3
    {
    tThaPdhDe3 super;
    const tTha60210031PdhDe3Methods *methods;
    }tTha60210031PdhDe3;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtPdhDe3 Tha60210031PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHDE3INTERNAL_H_ */

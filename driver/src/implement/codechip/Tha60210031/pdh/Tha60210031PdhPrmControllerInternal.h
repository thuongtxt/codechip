/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210031PdhPrmControllerInternal.h
 * 
 * Created Date: Jan 4, 2017
 *
 * Description : PRM internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHPRMCONTROLLERINTERNAL_H_
#define _THA60210031PDHPRMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhPrmControllerInternal.h"
#include "Tha60210031PdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60210031PdhPrmController
    {
    tThaPdhPrmController super;
    }tTha60210031PdhPrmController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhPrmController Tha60210031PdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHPRMCONTROLLERINTERNAL_H_ */


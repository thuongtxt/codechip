/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210031PdhSerialLine.h
 * 
 * Created Date: May 15, 2015
 *
 * Description : Serial Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHSERIALLINE_H_
#define _THA60210031PDHSERIALLINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe3SerialLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhSerialLine Tha60210031PdhSerialLineNew(uint32 channelId, AtModulePdh module);
eAtRet Tha60210031PdhSerLineStsMapDe3Update(AtPdhSerialLine self, eAtPdhDe3SerialLineMode mode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210031PDHSERIALLINE_H_ */


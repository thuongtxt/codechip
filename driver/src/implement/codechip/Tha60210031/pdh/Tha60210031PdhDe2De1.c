/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhDe2De1.c
 *
 * Created Date: June 16, 2015
 *
 * Description : DE1 inside a DE2
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210031ModulePdh.h"
#include "Tha60210031ModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cDebugDe2De1ErrorForceControl     0x790002
#define cDebugDe2De1BitErrorForceEnMask   cBit11
#define cDebugDe2De1BitErrorForceEnShift  11
#define cDebugDe2De1FBitErrorForceEnMask  cBit10
#define cDebugDe2De1FBitErrorForceEnShift 10
#define cDebugDe3IdMask                   cBit9_5
#define cDebugDe3IdShift                  5
#define cDebugDe2IdMask                   cBit4_2
#define cDebugDe2IdShift                  2
#define cDebugDe1IdMask                   cBit1_0
#define cDebugDe1IdShift                  0

#define cDebugDe2De1ErrorForceTheshold    0x790003

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031PdhDe2De1)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60210031PdhDe1Common.h"

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031PdhDe2De1);
    }

AtPdhDe1 Tha60210031PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (ThaPdhDe2De1ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 Tha60210031PdhDe2De1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return Tha60210031PdhDe2De1ObjectInit(newDe1, channelId, module);
    }

uint8 Tha60210031Tu1xOrDe1IdGet(ThaPdhDe1 self, uint8 *tugOrDe2)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    AtPdhChannel de2;

    if (vc)
        {
        *tugOrDe2 = AtSdhChannelTug2Get(vc);
        return AtSdhChannelTu1xGet(vc);
        }

    de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    if (de2 == NULL)
        {
        *tugOrDe2 = 0;
        return (uint8)AtChannelIdGet((AtChannel)self);
        }

    if (AtPdhChannelTypeGet(de2) == cAtPdhChannelTypeE2)
        {
        uint32 flatId = (AtChannelIdGet((AtChannel)de2) * 4) + AtChannelIdGet((AtChannel)self);
        *tugOrDe2 = (uint8)(flatId / 3);
        return (uint8)(flatId % 3);
        }

    *tugOrDe2 = (uint8)AtChannelIdGet((AtChannel)de2);
    return (uint8)AtChannelIdGet((AtChannel)self);
    }

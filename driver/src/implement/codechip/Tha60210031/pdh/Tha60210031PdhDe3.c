/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60150011PdhDe3.c
 *
 * Created Date: Mar 4, 2015
 *
 * Description : DS3/E3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pdh/AtPdhMdlControllerInternal.h"
#include "../../../../generic/ber/AtModuleBerInternal.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../default/pdh/ThaPdhDe3Internal.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/pdh/ThaModulePdhForStmReg.h"
#include "../../../default/pdh/retimingcontrollers/ThaPdhDe3RetimingController.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhMdlControllerInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../man/Tha60210031Device.h"
#include "../sdh/Tha60210031SdhVc.h"
#include "Tha60210031ModulePdh.h"
#include "Tha60210031ModulePdhReg.h"
#include "Tha60210031PdhSerialLine.h"
#include "Tha60210031PdhMdlControllerReg.h"
#include "Tha60210031PdhMdlController.h"
#include "Tha60210031PdhDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/
#define cFirst24PortsStickyRegAddr          (0xF30006)
#define cLast24PortsStickyRegAddr           (0xF40006)

#define cNumberHwMdlController         1

#define cAf6_pdh_de3_intr_Mask(de3)    (1U << (de3))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210031PdhDe3)self)
#define mThaModulePdh(self) (ThaModulePdh)(AtChannelModuleGet((AtChannel)self))
#define mThaPdhDe3(self) ((ThaPdhDe3)self)
#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210031PdhDe3Methods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tAtPdhDe3Methods     m_AtPdhDe3Override;
static tThaPdhDe3Methods    m_ThaPdhDe3Override;

static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;
static const tThaPdhDe3Methods    *m_ThaPdhDe3Methods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static void Delete(AtObject self)
    {
    AtSdhVc sdhChannel = AtPdhChannelVcGet((AtPdhChannel)self);
    m_AtObjectMethods->Delete(self);
    if (sdhChannel != NULL)
        SdhVcPdhChannelSet(sdhChannel, NULL);
    }

static eAtRet HwIdFactorGet(ThaPdhDe3 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    uint32 de3Id = 0;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleOcn moduleOcn = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    uint8 numStsInSlice = ThaModuleOcnNumStsInOneSlice(moduleOcn);

    if (sdhChannel)
        return m_ThaPdhDe3Methods->HwIdFactorGet(self, phyModule, slice, hwIdInSlice);

    de3Id = AtChannelIdGet((AtChannel)self);
    *slice = de3Id / numStsInSlice;
    *hwIdInSlice = de3Id % numStsInSlice;

    return cAtOk;
    }

static ThaModulePdh PdhModule(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static void ControllerStandardUpdate(AtPdhMdlController controller, eAtPdhMdlStandard standard)
    {
    if (controller)
        ThaPdhMdlControllerHwStandardHeaderSet(controller, standard);
    }

static void MdlStandardUpdate(AtPdhDe3 de3, eAtPdhMdlStandard standard)
    {
    ControllerStandardUpdate(AtPdhDe3IdleSignalMdlController(de3), standard);
    ControllerStandardUpdate(AtPdhDe3TestSignalMdlController(de3), standard);
    ControllerStandardUpdate(AtPdhDe3PathMessageMdlController(de3), standard);
    }

static eAtRet MdlStandardSet(AtPdhDe3 de3, eAtPdhMdlStandard standard)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule((AtChannel)de3);
    uint32 regAddr, regVal;
    uint8 hwStandard = (standard == cAtPdhMdlStandardAtt) ? 1 : 0;

    /* RX */
    regAddr = mMethodsGet(pdhModule)->MdlRxCfgReg(pdhModule, de3);
    regVal  = mChannelHwRead(de3, regAddr, cAtModulePdh);
    if (Tha60210031De3MdlRxFilterMessagePerTypeIsSupported(de3))
        mRegFieldSet(regVal, cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_new_, hwStandard);
    else
        mRegFieldSet(regVal, cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_, hwStandard);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);

    /* TX */
    regAddr = mMethodsGet(pdhModule)->MdlTxCtrlReg(pdhModule, de3);
    regVal  = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_cfg_mdl_cfg_mdlstd_tx_, hwStandard);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);

    MdlStandardUpdate(de3, standard);

    return cAtOk;
    }

static uint32 MdlStandardGet(AtPdhDe3 de3)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule((AtChannel)de3);
    uint32 regAddr = mMethodsGet(pdhModule)->MdlRxCfgReg(pdhModule, de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);
    if(Tha60210031De3MdlRxFilterMessagePerTypeIsSupported(de3))
        return (regVal & cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_new_Mask) ? cAtPdhMdlStandardAtt : cAtPdhMdlStandardAnsi;
    return (regVal & cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Mask) ? cAtPdhMdlStandardAtt : cAtPdhMdlStandardAnsi;
    }

static eBool BitOrderIsValid(eAtBitOrder order)
    {
    switch ((uint32)order)
        {
        case cAtBitOrderMsb: return cAtTrue;
        case cAtBitOrderLsb: return cAtTrue;
        default:             return cAtFalse;
        }
    }

static eBool FcsLsbBitOrderIsSupported(AtPdhChannel self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return Tha60210031ModulePdhDatalinkFcsBitOrderIsSupported(pdhModule);
    }

static eAtRet HwDataLinkFcsBitOrderSet(AtPdhDe3 de3, eAtBitOrder order)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule((AtChannel)de3);
    uint32 regAddr, regVal;
    uint32 hwMsb = (order == cAtBitOrderMsb) ? 1 : 0;

    /* RX */
    regAddr = mMethodsGet(pdhModule)->MdlRxCfgReg(pdhModule, de3);
    regVal  = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_, hwMsb);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);

    /* TX */
    regAddr = mMethodsGet(pdhModule)->MdlTxCtrlReg(pdhModule, de3);
    regVal  = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_cfg_mdl_cfg_fcs_tx_, hwMsb);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet DataLinkFcsBitOrderSet(AtPdhChannel self, eAtBitOrder order)
    {
    if (!AtPdhDs3DataLinkIsApplicable((AtPdhDe3) self))
        return cAtErrorNotApplicable;

    if (!FcsLsbBitOrderIsSupported(self))
        return (order == cAtBitOrderMsb) ? cAtOk : cAtErrorModeNotSupport;

    if (!BitOrderIsValid(order))
        return cAtErrorInvlParm;

    return HwDataLinkFcsBitOrderSet((AtPdhDe3) self, order);
    }

static eAtBitOrder HwDataLinkFcsBitOrderGet(AtPdhChannel self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule((AtChannel)self);
    uint32 regAddr = mMethodsGet(pdhModule)->MdlRxCfgReg(pdhModule,  (AtPdhDe3) self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return (regVal & cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Mask) ? cAtBitOrderMsb : cAtBitOrderLsb;
    }

static eAtBitOrder DataLinkFcsBitOrderGet(AtPdhChannel self)
    {
    if (!AtPdhDs3DataLinkIsApplicable((AtPdhDe3) self))
        return cAtBitOrderNotApplicable;

    if (!FcsLsbBitOrderIsSupported(self))
        return cAtBitOrderMsb;

    return HwDataLinkFcsBitOrderGet(self);
    }

static uint32 PartOffset(ThaPdhDe3 self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cAtModulePdh, (uint8)ThaPdhDe3PartId(self));
    }

static uint32 DefaultSliceOffset(ThaPdhDe3 self)
    {
    uint8 sliceId, hwIdInSlice;

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    return PartOffset(self) + (uint32)ThaModulePdhSliceOffset(PdhModule((AtChannel)self), sliceId);
    }

static uint32 SlipBufferSsmEnableRegMask(AtPdhChannel self)
    {
    return cBit0 << (AtChannelIdGet((AtChannel)self) % 24);
    }

static uint32 SlipBufferSsmEnableRegShift(AtPdhChannel self)
    {
    return AtChannelIdGet((AtChannel)self) % 24;
    }

static eAtModulePdhRet RxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        {
        ThaPdhDe3 de3  = (ThaPdhDe3)self;
        uint8 inverted = (bitOrder == cAtBitOrderMsb) ? 0 : 1;
        uint32 regAddr = cAf6Reg_rxde3_SSM_Inv_Base + DefaultSliceOffset(de3);
        uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

        mFieldIns(&regVal,
                  SlipBufferSsmEnableRegMask((AtPdhChannel)self),
                  SlipBufferSsmEnableRegShift((AtPdhChannel)self),
                  inverted);
        mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtBitOrder RxUdlBitOrderGet(AtPdhDe3 self)
    {
    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        {
        ThaPdhDe3 de3  = (ThaPdhDe3)self;
        uint32 regAddr = cAf6Reg_rxde3_SSM_Inv_Base + DefaultSliceOffset(de3);
        uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
        uint32 inverted = regVal & SlipBufferSsmEnableRegMask((AtPdhChannel)self);
        return inverted ? cAtBitOrderLsb : cAtBitOrderMsb;
        }

    return cAtFalse;
    }

static void RxSsmDectionThresholdSet(ThaPdhDe3 self, uint8 times)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 regAddr = Tha60210031ModulePdhRxDe3OhThrshCfgAddress((Tha60210031ModulePdh)module) + DefaultSliceOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldIns(&regVal, cAf6_rxde3_oh_thrsh_cfg_RxSsm_Mask, cAf6_rxde3_oh_thrsh_cfg_RxSsm_Shift, times);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    }

static void RxSsmDefaultSet(ThaPdhDe3 self)
    {
    RxSsmDectionThresholdSet(self, 3);
    AtPdhDe3RxUdlBitOrderSet((AtPdhDe3)self, cAtBitOrderMsb);
    }

static void SlipBufferDefaultSet(AtChannel self)
    {
    ThaPdhRetimingControllerSlipBufferDefaultSet(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self));

    RxSsmDefaultSet((ThaPdhDe3)self);
    }

static void TxSsmDefaultSet(ThaPdhDe3 self)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset(self);
    mChannelHwWrite(self, regAddr, 0, cAtModulePdh);
    }

static void HwTxSsmEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    mRegFieldSet(regVal, cThaRegTxDs3CbitSsmEn, (enable ? 1:0));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    }

static eBool HwTxSsmIsEnabled(ThaPdhDe3 self)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 enabled = 0;

    mFieldGet(regVal, cThaRegTxDs3CbitSsmEnMask, cThaRegTxDs3CbitSsmEnShift, uint8, &enabled);
    return enabled == 1 ? cAtTrue : cAtFalse;
    }

static eAtRet TxDs3CbitSsmSet(ThaPdhDe3 self, uint8 ssm)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldIns(&regVal, cThaRegTxDs3CbitSsmMessMask, cThaRegTxDs3CbitSsmMessShift, ssm);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    
    return cAtOk;
    }

static uint8 TxDs3CbitSsmGet(ThaPdhDe3 self)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 ssm = 0;

    mFieldGet(regVal, cThaRegTxDs3CbitSsmMessMask, cThaRegTxDs3CbitSsmMessShift, uint8, &ssm);
    return ssm;
    }

static eAtRet TxDs3CbitSSmUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset((ThaPdhDe3)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 invert = (bitOrder == cAtBitOrderMsb) ? 0 : 1;

    mFieldIns(&regVal, cThaRegTxDs3CbitSsmInvMask, cThaRegTxDs3CbitSsmInvShift, invert);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }

static eAtBitOrder TxDs3CbitSSmUdlBitOrderGet(AtPdhDe3 self)
    {
    uint32 regAddr = cThaRegTxM23Ds3CbitSsmCtrl + ThaPdhDe3DefaultOffset((ThaPdhDe3)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 invert = 0;

    mFieldGet(regVal, cThaRegTxDs3CbitSsmInvMask, cThaRegTxDs3CbitSsmInvShift, uint8, &invert);
    return (invert == 0) ? cAtBitOrderMsb : cAtBitOrderLsb;
    }

static eAtModulePdhRet TxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
    	return ThaPdhDe3RetimingControllerTxUdlBitOrderSet((ThaPdhDe3RetimingController)ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self), bitOrder);

    if (mMethodsGet(mThis(self))->TxFramerSsmIsSupported(mThis(self)))
        return TxDs3CbitSSmUdlBitOrderSet(self, bitOrder);

    return cAtErrorModeNotSupport;
    }

static eAtBitOrder TxUdlBitOrderGet(AtPdhDe3 self)
    {
    if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
        return ThaPdhDe3RetimingControllerTxUdlBitOrderGet((ThaPdhDe3RetimingController)ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self));

    if (mMethodsGet(mThis(self))->TxFramerSsmIsSupported(mThis(self)))
        return TxDs3CbitSSmUdlBitOrderGet(self);

    return cAtFalse;
    }

static eAtRet TxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        return ThaPdhRetimingControllerSlipBufferEnable(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self), enable);

    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxSlipBufferIsEnabled(AtPdhChannel self)
    {
    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        return ThaPdhRetimingControllerSlipBufferIsEnabled(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self));
        
    return cAtFalse;
    }

static eBool SsmIsApplicable(AtPdhChannel self)
    {
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet(self);

    if (frameType == cAtPdhE3Frmg832            ||
        frameType == cAtPdhDs3FrmCbitChnl21E1s  || 
        frameType == cAtPdhDs3FrmCbitChnl28Ds1s ||
        frameType == cAtPdhDs3FrmCbitUnChn)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SsmIsSupported(AtPdhChannel self)
    {
    AtPw pw;

    if (!SsmIsApplicable(self))
        return cAtFalse;

    /* If it is bound to SAToP PW, slip buffer must be enabled first */
    pw = AtChannelBoundPwGet((AtChannel)self);
    if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
        return AtPdhChannelTxSlipBufferIsEnabled(self);

    /* SSM can be inserted for all of other cases */
    return cAtTrue;
    }

static eAtRet E8G832IntrStateChangeEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);

    regAddr = ThaModulePdhRxDs3E3OHProCtrlRegister(mThaModulePdh(self)) + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, cThaRegRxE3SSMIntrStatChangeEnMask, cThaRegRxE3SSMIntrStatChangeEnShift, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet SlipBufferSsmEnable(AtPdhChannel self, eBool enable)
    {
    eAtRet ret = ThaPdhRetimingControllerSlipBufferSsmEnable(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self), enable);

    /*enable state change for interrupt*/
    if (AtPdhChannelFrameTypeGet(self) == cAtPdhE3Frmg832)
        E8G832IntrStateChangeEnable((ThaPdhDe3)self, enable);

    return ret;
    }

static eAtRet TxFramerSsmEnable(AtPdhChannel self, eBool enable)
    {
    HwTxSsmEnable((ThaPdhDe3)self, enable);
    /*enable state change for interrupt*/
    if (AtPdhChannelFrameTypeGet(self) == cAtPdhE3Frmg832)
        E8G832IntrStateChangeEnable((ThaPdhDe3)self, enable);

    return cAtOk;
    }

static eAtRet SsmEnable(AtPdhChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    if (!SsmIsSupported(self))
        return cAtErrorModeNotSupport;

    /* If slip buffer is enabled, SSM is inserted from slip buffer engine */
    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        {
        ret = ThaPdhRetimingControllerSlipBufferSsmEnable(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self), enable);
        ret |= SlipBufferSsmEnable(self, enable);
        return ret;
        }

    if (mMethodsGet(mThis(self))->TxFramerSsmIsSupported(mThis(self)))
        return TxFramerSsmEnable(self, enable);

    return cAtErrorModeNotSupport;
    }

static eBool SsmIsEnabled(AtPdhChannel self)
    {
    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        return ThaPdhRetimingControllerSlipBufferSsmIsEnabled(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self));

    if (mMethodsGet(mThis(self))->TxFramerSsmIsSupported(mThis(self)))
        return HwTxSsmIsEnabled((ThaPdhDe3)self);

    return cAtFalse;
    }

static eAtRet SsmTxHwSet(AtPdhChannel self, uint8 ssm)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThaPdhDe3(self))->DefaultOffset(mThaPdhDe3(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cThaTxE3G832Ssm, ssm);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static uint8 SsmTxHwGet(AtPdhChannel self)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThaPdhDe3(self))->DefaultOffset(mThaPdhDe3(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return (uint8)mRegField(regVal, cThaTxE3G832Ssm);
    }

static eAtRet TxSsmSet(AtPdhChannel self, uint8 value)
    {
    if (!SsmIsSupported(self))
        return cAtErrorModeNotSupport;

    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        return ThaPdhRetimingControllerSlipBufferSsmBomSend(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self), value, 0);

    if (AtPdhChannelFrameTypeGet(self) == cAtPdhE3Frmg832)
        return SsmTxHwSet(self, value);

    if (mMethodsGet(mThis(self))->TxFramerSsmIsSupported(mThis(self)))
        return TxDs3CbitSsmSet((ThaPdhDe3)self, value);

    return cAtErrorModeNotSupport;
    }

static uint8 TxSsmGet(AtPdhChannel self)
    {
    if (!SsmIsSupported(self))
        return 0x0;

    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        return ThaPdhRetimingControllerSlipBufferTxSsmGet(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self));
        
    if (AtPdhChannelFrameTypeGet(self) == cAtPdhE3Frmg832)
    return SsmTxHwGet(self);

    if (mMethodsGet(mThis(self))->TxFramerSsmIsSupported(mThis(self)))
        return TxDs3CbitSsmGet((ThaPdhDe3)self);

    return 0;
    }

static uint8 RxSsmGet(AtPdhChannel self)
    {
    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        {
        uint32 regAddr = cAf6_Reg_upen_sta_ssm + mMethodsGet(mThaPdhDe3(self))->DefaultOffset(mThaPdhDe3(self));
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

        return (uint8)regVal;
        }

    return 0x0;
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);

    if (Tha60210031ModulePdhHasLiu(module))
        return Tha60210031PdhDe3IsDe3LiuMode((ThaPdhDe3) self);

    return cAtFalse;
    }

static eBool LiuIsLoc(AtPdhChannel self)
    {
	uint32 de3Id, addr, regVal;
	if (!AtPdhChannelHasLineLayer(self))
        return cAtFalse;
        
    de3Id = AtChannelIdGet((AtChannel)self);
    addr = (de3Id < 24) ? cFirst24PortsStickyRegAddr : cLast24PortsStickyRegAddr;
    regVal = mChannelHwRead(self, addr, cAtModulePdh);
        
    return (regVal & (cBit0 << (de3Id % 24))) ? cAtTrue : cAtFalse;
    }

static uint8 DefaultFrameMode(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 de3Id = AtChannelIdGet((AtChannel)self);
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet((AtModulePdh)module, de3Id);

    if (!Tha60210031ModulePdhHasLiu(module))
        return cAtPdhDs3Unfrm;

    if (AtPdhSerialLineModeGet(serialLine) == cAtPdhDe3SerialLineModeE3)
        return cAtPdhE3Unfrm;

    return cAtPdhDs3Unfrm;
    }

static void RxSsmDebug(ThaPdhDe3 self)
    {
    uint32 address, regVal, messVal, messCnt, messSta;
    /* show rx ssm status */
    address = cAf6_Reg_upen_sta_ssm + mMethodsGet(mThaPdhDe3(self))->DefaultOffset(mThaPdhDe3(self));
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldGet(regVal, cAf6_Reg_upen_sta_ssm_RxSsm_Mask, cAf6_Reg_upen_sta_ssm_RxSsm_Shift, uint32, &messVal);
    mFieldGet(regVal, cAf6_Reg_upen_sta_ssm_RxMsgCount_Mask, cAf6_Reg_upen_sta_ssm_RxMsgCount_Shift, uint32, &messCnt);
    mFieldGet(regVal, cAf6_Reg_upen_sta_ssm_RxMsgStat_Mask, cAf6_Reg_upen_sta_ssm_RxMsgStat_Shift, uint32, &messSta);

    AtPrintc(cSevInfo, "Rx DS3E3 SSM status: SSMMessVal = %d, SSMMessCnt = %d, SSMMessSta = %d \n", messVal, messCnt, messSta);
    }

static uint8 SwStuffModeToHw(eAtPdhDe3StuffMode mode)
    {
    if (mode==cAtPdhDe3StuffModeFineStuff)
        return 9;
    return 0;
    }

static eAtRet StuffModeSet(AtPdhDe3 self, eAtPdhDe3StuffMode mode)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    regAddr = cAf6Reg_stsvt_map_ctrl_Base + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Mask, cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Shift, SwStuffModeToHw(mode));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static uint8 HwStuffModeToSw(eAtPdhDe3StuffMode mode)
    {
    if (mode==9)
        return cAtPdhDe3StuffModeFineStuff;
    return cAtPdhDe3StuffModeFullStuff;
    }

static eAtPdhDe3StuffMode StuffModeGet(AtPdhDe3 self)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    uint8 hwMode = 0;

    regAddr = cAf6Reg_stsvt_map_ctrl_Base + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldGet(regVal, cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Mask, cAf6_stsvt_map_ctrl_StsVtMapJitterCfg_Shift,uint8, &hwMode);
    return HwStuffModeToSw(hwMode);
    }

static eAtRet Debug(AtChannel self)
    {
    ThaPdhDe3 thade3 = (ThaPdhDe3)self;
    m_AtChannelMethods->Debug(self);

    if (ThaPdhDe3RetimingIsSupported(thade3))
        {
        ThaPdhRetimingControllerSlipBufferTxHwDebug(ThaPdhDe3RetimingControllerGet(thade3));
        RxSsmDebug(thade3);
        }

    return cAtOk;
    }

static eAtRet HwMdlDefaultSet(AtPdhDe3 de3)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)PdhModule((AtChannel)de3);
    uint32 regAddr, regVal;

    /* Disable Tx */
    regAddr = mMethodsGet(pdhModule)->MdlTxCtrlReg(pdhModule, de3);
    regVal  = mChannelHwRead(de3, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_cfg_mdl_cfg_entx_, 0);
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);

    if (Tha60210031De3MdlRxFilterMessagePerTypeIsSupported(de3))
        {
        /* Disable Rx */
        regAddr = mMethodsGet(pdhModule)->MdlRxCfgReg(pdhModule, de3);
        regVal  = mChannelHwRead(de3, regAddr, cAtModulePdh);
        mFieldIns(&regVal, cBit2_0, 0, 0); /* Disable Rx */
        mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);
        }

    return cAtOk;
    }

static eAtRet MdlDefaultSet(AtChannel self)
    {
    /* Will not access HW if Device not support MDLs */
    if (!ThaModulePdhMdlIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return cAtOk;

    /* Add HW default clear enable and sticky buffer */
    HwMdlDefaultSet((AtPdhDe3)self);
    return AtPdhDe3MdlStandardSet((AtPdhDe3)self, cAtPdhMdlStandardAnsi);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        SlipBufferDefaultSet(self);

    if (mMethodsGet((mThis(self)))->TxFramerSsmIsSupported(mThis(self)))
        TxSsmDefaultSet((ThaPdhDe3)self);

    /* Default MDL standards */
    ret |= MdlDefaultSet(self);

    return ret;
    }

static eBool IsDiagnostic(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    if (ThaDeviceIsEp((ThaDevice)device))
        return cAtFalse;

    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static eBool NeedDisableTxExt1Timing(uint8 loopbackMode)
    {
    switch (loopbackMode)
        {
        case cAtPdhLoopbackModeRemoteLine   : return cAtTrue;
        case cAtPdhLoopbackModeRemotePayload: return cAtTrue;
        default : return cAtFalse;
        }
    }

static eBool IsDe3LiuMode(AtChannel self)
    {
    AtPdhSerialLine serLine = AtModulePdhDe3SerialLineGet((AtModulePdh)AtChannelModuleGet(self), AtChannelIdGet(self));
    return AtPdhSerialLineIsDe3Mode(serLine);
    }

static AtChannel SerialLineGet(AtChannel self)
    {
    AtModulePdh modulePdh = (AtModulePdh)AtChannelModuleGet(self);
    return (AtChannel)AtModulePdhDe3SerialLineGet(modulePdh, AtChannelIdGet(self));
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtTimingMode timingMode;
    eAtRet ret = m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    if (ret != cAtOk)
        return ret;

    if ((IsDiagnostic(self)) && Tha60210031ModulePdhHasLiu(PdhModule(self)))
        ThaModulePdhDiagLiuLoopbackSet(self, loopbackMode);

    timingMode = AtChannelTimingModeGet(self);
    if (timingMode == cAtTimingModeExt1Ref)
        {
        if (NeedDisableTxExt1Timing(loopbackMode))
            timingMode = cAtTimingModeSys;

        AtChannelTimingSet(SerialLineGet(self), timingMode, NULL);
        }

    return cAtOk;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    uint8 loopbackMode = m_AtChannelMethods->LoopbackGet(self);

    if (loopbackMode != cAtPdhLoopbackModeRelease)
        return loopbackMode;

    if (IsDiagnostic(self) && Tha60210031ModulePdhHasLiu(PdhModule(self)))
        return ThaModulePdhDiagLiuLoopbackGet(self);

    return loopbackMode;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtPdhDe3AlarmAis   |
            cAtPdhDe3AlarmSdBer |
            cAtPdhDe3AlarmSfBer |
            cAtPdhDe3AlarmBerTca|
            cAtPdhDs3AlarmFeacChange|
            cAtPdhDs3AlarmIdle  |
            cAtPdhDe3AlarmAis   |
            cAtPdhDe3AlarmLos   |
            cAtPdhDe3AlarmRai   |
            cAtPdhDe3AlarmLof   |
            cAtPdhDe3AlarmSsmChange|
            cAtPdhDe3AlarmClockStateChange|
            cAtPdhDs3AlarmMdlChange);
    }

static eAtRet HwInterruptEnable(AtChannel self, eBool enable)
    {
    eAtRet ret;
    uint8 sliceId;
    uint8 localId;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 regAddr, regVal;

    ret = ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &localId);
    if (ret)
        return ret;

    regAddr = mMethodsGet(pdhModule)->SliceBase(pdhModule, (uint32)sliceId) + ThaModulePdhRxm23e23ChnIntrCfgBase(pdhModule);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    if (enable)
        regVal |= cAf6_pdh_de3_intr_Mask(localId);
    else
        regVal &= ~cAf6_pdh_de3_intr_Mask(localId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HwInterruptEnable(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HwInterruptEnable(self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    eAtRet ret;
    uint8 sliceId;
    uint8 localId;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 regAddr, regVal;

    ret = ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &localId);
    if (ret != cAtOk)
        return cAtFalse;

    regAddr = mMethodsGet(pdhModule)->SliceBase(pdhModule, (uint32)sliceId) + ThaModulePdhRxm23e23ChnIntrCfgBase(pdhModule);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    return (regVal & cAf6_pdh_de3_intr_Mask(localId)) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 defect = m_AtChannelMethods->InterruptProcess(self, offset);

    AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly((AtPdhChannel)self, cAtTrue);
    return defect;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    if (ret != cAtOk)
        return ret;

    if (ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        ThaPdhRetimingControllerSlipBufferTxHwSsmFrameTypeUpdate(ThaPdhDe3RetimingControllerGet((ThaPdhDe3)self), frameType);

    return ret;
    }

static uint32 BpvCounterRead2Clear(ThaPdhDe3 self, eBool clear)
    {
    ThaModulePdh module;
    uint32 counterOffSet;
    uint32 offset;

    if (!mMethodsGet(mThis(self))->BpvCounterIsSupported(mThis(self)))
        return 0;

    module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    counterOffSet =  mMethodsGet(module)->De3CounterOffset(module, clear);
    offset = mMethodsGet(self)->DefaultOffset(self) + ThaModulePdhBaseAddress(module);
    return mChannelHwRead(self,  cAf6Reg_rxm23e23_lcv_cnt_Base + counterOffSet + offset, cAtModulePdh);
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    eAtRet ret;
    AtChannel serialLine = SerialLineGet(self);

    ret = m_AtChannelMethods->TxEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (AtChannelTxCanEnable(serialLine, enable))
        ret = AtChannelTxEnable(SerialLineGet(self), enable);

    return ret;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtChannel serialLine = SerialLineGet(self);
    eAtRet ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (ret != cAtOk)
        return ret;

    if (AtChannelTimingModeIsSupported(serialLine, timingMode))
        ret |= AtChannelTimingSet(serialLine, timingMode, timingSource);

    return ret;
    }

static AtBerController LineBerControllerGet(AtPdhChannel self)
    {
    if (IsDe3LiuMode((AtChannel)self))
        return m_AtPdhChannelMethods->LineBerControllerGet(self);

    return NULL;
    }

static eAtRet DataLinkEnable(AtPdhChannel self, eBool enable)
    {
	ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    eAtRet ret = cAtOk;

    /* Flush Buffer before enable */
    if (!AtPdhChannelDataLinkIsEnabled(self) && enable)
        ret |= Tha60210031PdhDe3MdlHwBufferFlush((AtPdhDe3)self);

    /* Call Supper for enable */
	if (ThaModulePdhMdlIsSupported(pdhModule))
		ret |= m_AtPdhChannelMethods->DataLinkEnable(self, enable);

    return ret;
    }

static eBool BpvCounterIsSupported(Tha60210031PdhDe3 self)
    {
    if (Tha60210031ModulePdhHasLiu(PdhModule((AtChannel)self)) == cAtFalse)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet NationalBitsSet(AtPdhChannel self, uint8 pattern)
    {
    if (!AtPdhChannelNationalBitsSupported(self))
        return cAtErrorModeNotSupport;
    return (pattern == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 NationalBitsGet(AtPdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxNationalBitsGet(AtPdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RetimingIsSupported(ThaPdhDe3 self)
    {
    return ThaModulePdhDe3SsmIsSupported(PdhModule((AtChannel)self));
    }

static ThaPdhRetimingController RetimingControllerCreate(ThaPdhDe3 self)
    {
    return (ThaPdhRetimingController)ThaPdhDe3RetimingControllerNew((AtPdhChannel)self);
    }

static eBool TxFramerSsmVersionSupported(Tha60210031PdhDe3 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4646);

    if (currentVersion >= supportedVersion)
        return cAtTrue;

    return cAtFalse;
    }

static eBool TxFramerSsmIsSupported(Tha60210031PdhDe3 self)
    {
    if (!ThaPdhDe3RetimingIsSupported((ThaPdhDe3)self))
        return cAtFalse;

    if (TxFramerSsmVersionSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static ThaCdrController CdrControllerCreate(ThaPdhDe3 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    AtSdhVc vc = AtPdhChannelVcInternalGet((AtPdhChannel)self);
    if (vc)
        return ThaModuleCdrVcDe3CdrControllerCreate(cdrModule, (AtPdhDe3)self);

    return ThaModuleCdrDe3CdrControllerCreate(cdrModule, (AtPdhDe3)self);
    }

static void OverrideAtPdhDe3(AtPdhDe3 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhDe3Override, mMethodsGet(self), sizeof(m_AtPdhDe3Override));

        /* Setup methods */
        mMethodOverride(m_AtPdhDe3Override, MdlStandardSet);
        mMethodOverride(m_AtPdhDe3Override, MdlStandardGet);
        mMethodOverride(m_AtPdhDe3Override, RxUdlBitOrderSet);
        mMethodOverride(m_AtPdhDe3Override, RxUdlBitOrderGet);
        mMethodOverride(m_AtPdhDe3Override, TxUdlBitOrderSet);
        mMethodOverride(m_AtPdhDe3Override, TxUdlBitOrderGet);
        mMethodOverride(m_AtPdhDe3Override, StuffModeGet);
        mMethodOverride(m_AtPdhDe3Override, StuffModeSet);
        }

    mMethodsSet(self, &m_AtPdhDe3Override);
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, LineBerControllerGet);
        mMethodOverride(m_AtPdhChannelOverride, TxSlipBufferEnable);
        mMethodOverride(m_AtPdhChannelOverride, TxSlipBufferIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, TxSsmSet);
        mMethodOverride(m_AtPdhChannelOverride, TxSsmGet);
        mMethodOverride(m_AtPdhChannelOverride, RxSsmGet);
        mMethodOverride(m_AtPdhChannelOverride, SsmEnable);
        mMethodOverride(m_AtPdhChannelOverride, SsmIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkEnable);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkFcsBitOrderSet);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkFcsBitOrderGet);
        mMethodOverride(m_AtPdhChannelOverride, NationalBitsSet);
        mMethodOverride(m_AtPdhChannelOverride, NationalBitsGet);
        mMethodOverride(m_AtPdhChannelOverride, RxNationalBitsGet);
        mMethodOverride(m_AtPdhChannelOverride, HasLineLayer);
        mMethodOverride(m_AtPdhChannelOverride, LiuIsLoc);
        mMethodOverride(m_AtPdhChannelOverride, DefaultFrameMode);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe3Methods = mMethodsGet(de3);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(de3), sizeof(m_ThaPdhDe3Override));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe3Override, BpvCounterRead2Clear);
        mMethodOverride(m_ThaPdhDe3Override, HwIdFactorGet);
        mMethodOverride(m_ThaPdhDe3Override, IsManagedByModulePdh);
        mMethodOverride(m_ThaPdhDe3Override, ShouldConsiderLofWhenLos);
        mMethodOverride(m_ThaPdhDe3Override, RetimingIsSupported);
        mMethodOverride(m_ThaPdhDe3Override, RetimingControllerCreate);
        mMethodOverride(m_ThaPdhDe3Override, CdrControllerCreate);
        }

    mMethodsSet(de3, &m_ThaPdhDe3Override);
    }

static void OverrideAtObject(AtPdhDe3 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideAtPdhDe3(self);
    OverrideThaPdhDe3(self);
    }

static void MethodsInit(Tha60210031PdhDe3 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BpvCounterIsSupported);
        mMethodOverride(m_methods, TxFramerSsmIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

AtPdhDe3 Tha60210031PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60210031PdhDe3));

    /* Supper constructor */
    if (ThaPdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60210031PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60210031PdhDe3));
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return Tha60210031PdhDe3ObjectInit(newDe3, channelId, module);
    }

eBool Tha60210031PdhDe3IsDe3LiuMode(ThaPdhDe3 self)
    {
    if (self)
        return IsDe3LiuMode((AtChannel)self);
    return cAtFalse;
    }

eBool Tha60210031De3MdlRxFilterMessagePerTypeIsSupported(AtPdhDe3 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    return (AtDeviceVersionNumber(device) >= Tha60210031ModulePdhStartVersionSupportMdlFilterMessageByType(PdhModule((AtChannel)self))) ? cAtTrue : cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210031PdhMdlController.h
 *
 * Created Date: Aug 11, 2015
 *
 * Description : PDH
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THA60210031MDLCONTROLLER_H_
#define _THA60210031MDLCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* For Inherent   */
AtPdhMdlController Tha60210031PdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType);
void Tha60210031PdhMdlBufferReceivedStickyClear(AtPdhDe3 self);

#endif /* _THA60210031MDLCONTROLLER_H_ */

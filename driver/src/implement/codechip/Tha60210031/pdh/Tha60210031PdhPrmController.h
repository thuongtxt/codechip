/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210031PdhPrmController.h
 * 
 * Created Date: Nov 30, 2015
 *
 * Description : PRM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210031PDHPRMCONTROLLER_H_
#define _THA60210031PDHPRMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210031PdhPrmController* Tha60210031PdhPrmController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha602PrmControllerInterruptOffset(ThaPdhDe1 self, uint8 *sliceId);
uint32 Tha602PrmControllerLBOffset(AtPdhPrmController self);

#endif /* _THA60210031PDHPRMCONTROLLER_H_ */

